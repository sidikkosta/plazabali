/**
 * 
 */
package com.unicore.ui.grid;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Vector;
import java.util.logging.Level;
import org.compiere.apps.IStatusBar;
import org.compiere.grid.CreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MDocType;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import com.uns.base.model.MInvoice;
import com.uns.model.MUNSVAT;
import com.uns.model.MUNSVATLine;

/**
 * @author Burhani Adam
 *
 */
public class CreateFromVAT extends CreateFrom {

	/**
	 * @param gridTab
	 */
	public CreateFromVAT(GridTab gridTab) {
		super(gridTab);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.ICreateFrom#getWindow()
	 */
	@Override
	public Object getWindow() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#dynInit()
	 */
	@Override
	public boolean dynInit() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#info(org.compiere.minigrid.IMiniTable, org.compiere.apps.IStatusBar)
	 */
	@Override
	public void info(IMiniTable miniTable, IStatusBar statusBar) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#save(org.compiere.minigrid.IMiniTable, java.lang.String)
	 */
	@Override
	public boolean save(IMiniTable miniTable, String trxName)
	{
		int UNS_VAT_ID = ((Integer) getGridTab().getValue("UNS_VAT_ID")).intValue();
		MUNSVAT vat = new MUNSVAT(Env.getCtx(), UNS_VAT_ID, trxName);
		
		for (int i = 0; i < miniTable.getRowCount(); i++)
		{
			if (((Boolean)miniTable.getValueAt(i, 0)).booleanValue())
			{
				KeyNamePair pp = (KeyNamePair) miniTable.getValueAt(i, 1); // Product
				int C_Invoice_ID = pp.getKey();
				MUNSVATLine line = new MUNSVATLine(vat);
				MInvoice inv = new MInvoice(Env.getCtx(), C_Invoice_ID, trxName);
				line.setAuto(true);
				line.setisReplacement(false);
				line.setInvoice(inv);
				if(!line.save())
				{
					log.saveError("Error", "Failed when trying save lines");
					return false;
				}
			}
		}
		
		return true;
	}

	/**
	 *  Load Data - Invoice Not Reported
	 *  @param UNS_VAT_ID
	 *  @param C_BPartner_ID
	 *  @param C_BPartner_Group_ID
	 */
	protected Vector<Vector<Object>> getInvoiceData (int UNS_VAT_ID, int C_BPartner_ID,
			int AD_Org_ID, int C_BankAccount_ID, boolean isReturn, boolean isPKP, boolean isReplacement,
			int M_Product_ID)
	{
		MUNSVAT vat = new MUNSVAT(Env.getCtx(), UNS_VAT_ID, null);

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT iv.DocumentNo, iv.C_Invoice_ID,"
				+ " iv.DateInvoiced, bp.Name, COALESCE(po.AttName, ''),"
				+ " iv.TotalLines, SUM(it.TaxAmt), iv.GrandTotal FROM C_Invoice iv"
				+ " INNER JOIN C_InvoiceTax it ON it.C_Invoice_ID = iv.C_Invoice_ID"
				+ " INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = iv.C_BPartner_ID"
				+ " INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_Location_ID = iv.C_BPartner_Location_ID"
				+ " LEFT JOIN UNS_PreOrder po ON po.C_Order_ID = iv.C_Order_ID"
				+ " WHERE (iv.DocStatus IN ('CO', 'CL') OR EXISTS (SELECT 1 FROM M_InOut io"
				+ " WHERE io.C_Invoice_ID = iv.C_Invoice_ID AND io.DocStatus IN ('CO', 'CL')))"
				+ " AND bp.isPKP = ? AND iv.DocStatus NOT IN ('VO', 'RE')"
				+ " AND iv.DateInvoiced BETWEEN ? AND ? AND iv.IsSOTrx = ? AND iv.C_DocTypeTarget_ID=?");
		
		if(!isReplacement) 
			sql.append(" AND NOT EXISTS (SELECT 1 FROM UNS_VATLine l WHERE l.C_Invoice_ID = iv.C_Invoice_ID"
						+ " AND EXISTS (SELECT 1 FROM UNS_VAT vat WHERE vat.UNS_VAT_ID = l.UNS_VAT_ID AND vat.DocStatus"
						+ " NOT IN ('VO', 'RE')))");
		else 
			sql.append(" AND EXISTS (SELECT 1 FROM UNS_VATLine l WHERE l.C_Invoice_ID = iv.C_Invoice_ID"
					+ " AND EXISTS (SELECT 1 FROM UNS_VAT vat WHERE vat.UNS_VAT_ID = l.UNS_VAT_ID AND vat.DocStatus"
					+ " NOT IN ('CO', 'CL')))");
		
		if(C_BPartner_ID > 0)
			sql.append(" AND iv.C_BPartner_ID = ").append(C_BPartner_ID);
		
//		if(C_BPartner_Group_ID > 0)
//			sql.append(" AND bp.C_BP_Group_ID = ").append(C_BPartner_Group_ID);
		
		if(C_BankAccount_ID > 0)
			sql.append(" AND EXISTS (SELECT 1 FROM C_PaymentAllocate pa WHERE"
					+ " pa.C_Invoice_ID = iv.C_Invoice_ID AND EXISTS"
					+ " (SELECT 1 FROM C_Payment p WHERE p.C_Payment_ID = pa.C_Payment_ID"
					+ " AND p.DocStatus IN ('CO', 'CL') AND p.C_BankAccount_ID = ").append(C_BankAccount_ID)
					.append("))");
//		if(Region_ID > 0)
//			sql.append(" AND iv.AD_Org_ID IN (SELECT sr.AD_Org_ID FROM AD_Org sr WHERE sr.C_SalesRegion_ID = ").append(Region_ID)
//			.append(")");
		if(AD_Org_ID > 0)
			sql.append(" AND iv.AD_Org_ID = ").append(AD_Org_ID);
		
		if(M_Product_ID > 0)
			sql.append(" AND EXISTS (SELECT 1 FROM C_InvoiceLine il WHERE il.C_Invoice_ID = iv.C_Invoice_ID AND"
					+ " il.M_Product_ID = ").append(M_Product_ID).append(")");
		
		sql.append(" GROUP BY iv.C_Invoice_ID, bp.C_BPartner_ID, bpl.C_BPartner_Location_ID, po.AttName");
		sql.append(" ORDER BY iv.DateInvoiced, iv.DocumentNo ASC");
		
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			int dt = 0;
			if(isSOTrx && !isReturn)
				dt = MDocType.getDocType(MDocType.DOCBASETYPE_ARInvoice);
			else if(!isSOTrx && !isReturn)
				dt = MDocType.getDocType(MDocType.DOCBASETYPE_APInvoice);
			else if(isSOTrx && isReturn)
				dt = MDocType.getDocType(MDocType.DOCBASETYPE_ARCreditMemo);
			else
				dt = MDocType.getDocType(MDocType.DOCBASETYPE_APCreditMemo);
			
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setString(1, isPKP ? "Y" : "N");
			pstmt.setTimestamp(2, vat.getDateFrom());
			pstmt.setTimestamp(3, vat.getDateTo());
			pstmt.setString(4, vat.isSOTrx() ? "Y" : "N");
			pstmt.setInt(5, dt);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));           //  0-Selection
				KeyNamePair pp = new KeyNamePair(rs.getInt(2), rs.getString(1).trim());
				line.add(pp);
				line.add(rs.getTimestamp(3));
				line.add(rs.getString(4));
				line.add(rs.getString(5));
				if(isReplacement)
				{
					String getTaxNo = "SELECT vl.TaxInvoiceNo FROM UNS_VatLine vl WHERE vl.C_Invoice_ID = ? AND vl.isReplacement = 'N'"
							+ " AND EXISTS (SELECT 1 FROM UNS_VAT vat WHERE vat.UNS_VAT_ID = vl.UNS_VAT_ID AND vat.DocStatus"
							+ " IN ('CO', 'CL'))";
					String taxNo = DB.getSQLValueString(null, getTaxNo, pp.getKey());
					line.add(taxNo);
				}
				line.add(rs.getBigDecimal(6));
				line.add(rs.getBigDecimal(7));
				line.add(rs.getBigDecimal(8));

				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			//throw new DBException(e, sql.toString());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}
	
	protected Vector<String> getOISColumnNames(boolean isReplacement)
	{
		int columns = isReplacement ? 9 : 8;
		Vector<String> columnNames = new Vector<String>(columns);
	    columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
	    columnNames.add(Msg.translate(Env.getCtx(), "DocumentNo"));
	    columnNames.add(Msg.translate(Env.getCtx(), "DateInvoiced"));
	    columnNames.add(Msg.translate(Env.getCtx(), "BPartnerName"));
	    columnNames.add(Msg.translate(Env.getCtx(), "Attention Name"));
	    if(isReplacement)
	    	columnNames.add(Msg.translate(Env.getCtx(), "TaxInvoiceNo"));
	    columnNames.add(Msg.translate(Env.getCtx(), "TotalLines"));
	    columnNames.add(Msg.translate(Env.getCtx(), "TaxAmt"));
	    columnNames.add(Msg.translate(Env.getCtx(), "GrandTotal"));
	    
	    return columnNames;	
	}
	
	protected void configureMiniTable (IMiniTable miniTable, boolean isReplacement)
	{
		int i = 0;
		miniTable.setColumnClass(i++, Boolean.class, false);	//  Selection
		miniTable.setColumnClass(i++, String.class, true);		//  DocumentNo
		miniTable.setColumnClass(i++, Timestamp.class, true);	//  DateInvoiced
		miniTable.setColumnClass(i++, String.class, true);		//  BPartnerName
		miniTable.setColumnClass(i++, String.class, true);		//  TaxName
		if(isReplacement)
			miniTable.setColumnClass(i++, String.class, true);	// TaxInvoiceNo (if is replacement)
		miniTable.setColumnClass(i++, BigDecimal.class, true);	//  TotalLines
		miniTable.setColumnClass(i++, BigDecimal.class, true);	//  TaxAmt
		miniTable.setColumnClass(i++, BigDecimal.class, true);	//  GrandTotal
		
		//  Table UI
		miniTable.autoSize();	
	}
}
