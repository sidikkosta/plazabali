/**
 * 
 */
package com.unicore.ui.grid;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.apps.IStatusBar;
import org.compiere.grid.CreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

import com.uns.model.MUNSPriceLabelPrint;
import com.uns.model.process.PrintPriceLabel;

/**
 * @author Burhani Adam
 *
 */
public class CreateFromPriceLabel extends CreateFrom {

	/**
	 * @param gridTab
	 */
	public CreateFromPriceLabel(GridTab gridTab) {
		super(gridTab);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.ICreateFrom#getWindow()
	 */
	@Override
	public Object getWindow() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#dynInit()
	 */
	@Override
	public boolean dynInit() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#info(org.compiere.minigrid.IMiniTable, org.compiere.apps.IStatusBar)
	 */
	@Override
	public void info(IMiniTable miniTable, IStatusBar statusBar) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#save(org.compiere.minigrid.IMiniTable, java.lang.String)
	 */
	@Override
	public boolean save(IMiniTable miniTable, String trxName)
	{
		int UNS_PriceLabelPrint_ID = ((Integer) getGridTab().getValue("UNS_PriceLabelPrint_ID")).intValue();
		MUNSPriceLabelPrint record = new MUNSPriceLabelPrint(Env.getCtx(), UNS_PriceLabelPrint_ID, trxName);
		for (int i = 0; i < miniTable.getRowCount(); i++)
		{
			if (((Boolean)miniTable.getValueAt(i, 0)).booleanValue())
			{
				Integer qtyPrint = ((BigDecimal) miniTable.getValueAt(i, 1)).intValue();
				if(qtyPrint == null || qtyPrint <= 0)
					continue;
				KeyNamePair pp = (KeyNamePair) miniTable.getValueAt(i, 4);
				PrintPriceLabel print = new PrintPriceLabel(record, getPrinterID(), getShopType(), getLabelType(),
						pp.getKey(), qtyPrint);
				print.run();
				String sql = "UPDATE UNS_ProductList SET PrintedQty = PrintedQty + ?"
						+ " WHERE UNS_ProductList_ID = ?";
				DB.executeUpdate(sql, new Object[]{qtyPrint, pp.getKey()}, false, trxName);
			}
		}
		return true;
	}
	
	/**
	 *  Load Data - Product List
	 *  @param UNS_PriceLabelPrint_ID
	 *  @param isShowAll
	 */
	protected Vector<Vector<Object>> getLinesRecord (int UNS_PriceLabelPrint_ID, boolean isShowAll)
	{
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT PrintedQty, UNS_ProductList_ID, SKU, ProductName,"
				+ " ETBBCode, CategoryCode, DateCode, Qty FROM UNS_ProductList"
				+ " WHERE UNS_PriceLabelPrint_ID = ?");
		if(!isShowAll)
			sql.append(" AND Qty > PrintedQty");
		
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{	
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, UNS_PriceLabelPrint_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));           //  0-Selection
				BigDecimal remaining = rs.getBigDecimal(8).subtract(rs.getBigDecimal(1));
				line.add(remaining);
				line.add(rs.getBigDecimal(1));
				line.add(rs.getBigDecimal(8));
				KeyNamePair pp = new KeyNamePair(rs.getInt(2), rs.getString(3));
				line.add(pp);
				line.add(rs.getString(4));
				line.add(rs.getString(5));
				line.add(rs.getString(6));
				line.add(rs.getString(7));
				
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}
	
	protected Vector<String> getOISColumnNames()
	{
		int columns = 9;
		Vector<String> columnNames = new Vector<String>(columns);
	    columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
	    columnNames.add("Qty Print");
	    columnNames.add(Msg.translate(Env.getCtx(), "PrintedQty"));
	    columnNames.add(Msg.translate(Env.getCtx(), "Qty"));
	    columnNames.add(Msg.translate(Env.getCtx(), "SKU"));
	    columnNames.add(Msg.translate(Env.getCtx(), "ProductName"));
	    columnNames.add(Msg.translate(Env.getCtx(), "ETBBCode"));
	    columnNames.add(Msg.translate(Env.getCtx(), "CategoryCode"));
	    columnNames.add(Msg.translate(Env.getCtx(), "DateCode"));
	    
	    return columnNames;	
	}
	
	protected void configureMiniTable (IMiniTable miniTable)
	{
		int i = 0;
		miniTable.setColumnClass(i++, Boolean.class, false);	//  Selection
		miniTable.setColumnClass(i++, Integer.class, false);
		miniTable.setColumnClass(i++, BigDecimal.class, true);
		miniTable.setColumnClass(i++, BigDecimal.class, true);
		miniTable.setColumnClass(i++, String.class, true);	
		miniTable.setColumnClass(i++, String.class, true);
		miniTable.setColumnClass(i++, String.class, true);
		miniTable.setColumnClass(i++, String.class, true);
		miniTable.setColumnClass(i++, String.class, true);
		
		//  Table UI
		miniTable.autoSize();	
	}
	
	private String m_shopType = null;
	private String m_labelType = null;
	private int m_printerID = 0;
	
	public void setShopType(String shopType)
	{
		m_shopType = shopType;
	}
	
	public void setLabelType(String labelType)
	{
		m_labelType = labelType;
	}
	
	public void setPrinterID(int printerID)
	{
		m_printerID = printerID;
	}
	
	private String getShopType()
	{
		return m_shopType;
	}
	
	private String getLabelType()
	{
		return m_labelType;
	}
	
	private int getPrinterID()
	{
		return m_printerID;
	}
}