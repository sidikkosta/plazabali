/**
 * 
 */
package com.unicore.ui.grid;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import org.compiere.apps.IStatusBar;
import org.compiere.grid.CreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MPaySelectionLine;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

/**
 * @author ALBURHANY
 *
 */
public class CreateFromPayment extends CreateFrom {
	
	/**
	 * 
	 */
	public CreateFromPayment(GridTab gridTab) {
		super(gridTab);
		if (log.isLoggable(Level.INFO)) log.info(gridTab.toString());
	}

	@Override
	public Object getWindow() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean dynInit() throws Exception {
		log.config("");
		setTitle(Msg.getElement(Env.getCtx(), "C_Payment", false) 
				+ " .. " + Msg.translate(Env.getCtx(), "CreateFrom"));
		return false;
	}

	@Override
	public void info(IMiniTable miniTable, IStatusBar statusBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean save(IMiniTable miniTable, String trxName)
	{	
		int C_BPartner_ID = 0;

		// Get Payment
		int C_Payment_ID = ((Integer) getGridTab().getValue("C_Payment_ID")).intValue();
		MPayment pay = new MPayment(Env.getCtx(), C_Payment_ID, trxName);
		if (log.isLoggable(Level.CONFIG)) log.config(pay + ", C_BPartner_ID=" + C_BPartner_ID);

		/**
		 *  Selected        		- 0
		 *  PaidAmt         		- 1
		 *  C_Invoice_ID    		- 2
		 *  DateInvoiced		    - 3
		 *  C_PaySelectionLine_ID	- 4
		 *  RequisitionLine 		- 5
		 */
		
		// Lines
		for (int i = 0; i < miniTable.getRowCount(); i++)
		{
			if (((Boolean)miniTable.getValueAt(i, 0)).booleanValue()) {
				// variable values
				BigDecimal PaidAmt = (BigDecimal) miniTable.getValueAt(i, 1); // PaidAmt			
				KeyNamePair pp = (KeyNamePair) miniTable.getValueAt(i, 6); // Invoice
				int C_PaySelectionLine_ID = pp.getKey();

				if (log.isLoggable(Level.FINE)) log.fine("Payment Selection=" + C_PaySelectionLine_ID 
						+ ", PaidAmt=" + PaidAmt);

				//	Create new Allocate
				MPaymentAllocate alloc = new MPaymentAllocate(Env.getCtx(), 0, trxName);
				MPaySelectionLine line = new MPaySelectionLine(Env.getCtx(), C_PaySelectionLine_ID, trxName);
				alloc.setC_Payment_ID(C_Payment_ID);
				alloc.setAD_Org_ID(line.getC_Invoice().getAD_Org_ID());
				alloc.setC_Invoice_ID(line.getC_Invoice_ID());
				alloc.setInvoiceAmt(line.getC_Invoice().getGrandTotal());
				alloc.setPayToOverUnderAmount(PaidAmt);
				alloc.setAmount(PaidAmt);
				alloc.setWriteOffAmt(alloc.getInvoiceAmt().subtract(PaidAmt));
				alloc.saveEx();
				
				line.setC_PaymentAllocate_ID(alloc.get_ID());
				line.saveEx();
				
			}   //   if selected
		}   //  for all rows

		return true;
	}
	
	protected ArrayList<KeyNamePair> loadPaySelectionData (int C_BPartner_ID)
	{
		ArrayList<KeyNamePair> list = new ArrayList<KeyNamePair>();
		
		StringBuffer display = new StringBuffer("ps.DocumentNo||' - '||")
		.append(DB.TO_CHAR("ps.DateDoc", DisplayType.Date, Env.getAD_Language(Env.getCtx())));
		//
		StringBuffer sql = new StringBuffer("SELECT ps.C_PaySelection_ID, ").append(display)
		.append(" FROM C_PaySelection ps "
				+ " INNER JOIN C_PaySelectionLine psl ON psl.C_PaySelection_ID = ps.C_PaySelection_ID"
				+ " INNER JOIN C_Invoice i ON i.C_Invoice_ID = psl.C_Invoice_ID"
				+ " WHERE ps.DocStatus IN ('CO', 'CL') AND i.isPaid = 'N'"
				+ " AND i.C_BPartner_ID = ?")
		.append(" GROUP BY ps.C_PaySelection_ID");

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, C_BPartner_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return list;
	}
	
	/**
	 *  Load Data - Payment Selection
	 *  @param C_PaySelection_ID Payment Selection
	 *  @param forInvoice true if for invoice vs. delivery qty
	 */
	protected Vector<Vector<Object>> getPaySelectionData (int C_PaySelection_ID, boolean isOnlyDueDate)
	{
		int C_BPartner_ID = ((Integer) getGridTab().getValue("C_BPartner_ID")).intValue();
		
		/**
		 *  Selected        - 0
		 *  Quantity        - 1	
		 *  AvalibaleStock	- 2
		 *  C_UOM_ID        - 3
		 *  M_Product_ID    - 4
		 *  Locator			- 5
		 *  ReqLine	        - 6
		 */
		if (log.isLoggable(Level.CONFIG)) log.config("C_PaySelection_ID=" + C_PaySelection_ID);

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT "
				+ " psl.OpenAmt, i.C_Invoice_ID, "	//	1..2
				+ " (SELECT invoiceopen(i.C_Invoice_ID, 0)),"  //3
				+ " i.GrandTotal, i.DocumentNo, "			//	4..5
				+ " i.DateInvoiced," //6
				+ " psl.C_PaySelectionLine_ID, psl.Line " //6..7
				+ "	FROM C_PaySelectionLine psl"
				+ " INNER JOIN C_PaySelection ps ON ps.C_PaySelection_ID = psl.C_PaySelection_ID"
				+ " INNER JOIN C_Invoice i ON i.C_Invoice_ID = psl.C_Invoice_ID");
		sql.append(" WHERE ps.DocStatus = 'CO' AND i.DocStatus IN ('CO', 'CL') AND i.isPaid = 'N'"
				+ " AND i.C_BPartner_ID = ? AND (psl.C_PaymentAllocate_ID IS NULL OR psl.C_PaymentAllocate_ID <= 0)");
		
		if(C_PaySelection_ID > 0)
			sql.append(" AND ps.C_PaySelection_ID=?");
		
		//
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, C_BPartner_ID);
			if(C_PaySelection_ID > 0) pstmt.setInt(2, C_PaySelection_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{			
//				if(isOnlyDueDate && rs.getBigDecimal(1).compareTo(qtyOnHand) > 0)
//					continue;
				
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));           //  0-Selection
				BigDecimal paidAmt = rs.getBigDecimal(1);
				line.add(paidAmt);  //  1-PaidAmt
				BigDecimal openAmt = rs.getBigDecimal(3);
				line.add(openAmt);  //  2-OpenAmt
				BigDecimal grandTotal = rs.getBigDecimal(4);
				line.add(grandTotal);                           //  3-GrandTotal
				KeyNamePair pp = new KeyNamePair(rs.getInt(2), rs.getString(5).trim());
				line.add(pp);                           //  4-DocumentNo
				Timestamp date = rs.getTimestamp(6); // 5-Date
				line.add(date);
				pp = new KeyNamePair(rs.getInt(7), rs.getString(8).trim()); //6-PaymentSelectionLine
				line.add(pp);
				
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			//throw new DBException(e, sql.toString());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}   //  LoadPaySelection
	
	protected Vector<String> getOISColumnNames()
	{
		Vector<String> columnNames = new Vector<String>(7);
	    columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
	    columnNames.add(Msg.getMsg(Env.getCtx(), "PayToOverUnderAmount"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "OpenAmt"));
	    columnNames.add(Msg.translate(Env.getCtx(), "GrandTotal"));
	    columnNames.add(Msg.translate(Env.getCtx(), "C_Invoice_ID"));
	    columnNames.add(Msg.translate(Env.getCtx(), "DateDoc"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "C_PaySelectionLine_ID", false));
	    
	    return columnNames;
	}
	
	protected void configureMiniTable (IMiniTable miniTable)
	{
		miniTable.setColumnClass(0, Boolean.class, false);     //  Selection
		miniTable.setColumnClass(1, BigDecimal.class, false);      //  PaidAmt
		miniTable.setColumnClass(2, BigDecimal.class, true);      //  OpenAmt
		miniTable.setColumnClass(3, BigDecimal.class, true);	// GrandTotal
		miniTable.setColumnClass(4, String.class, true);          //  DocumentNo
		miniTable.setColumnClass(5, Timestamp.class, true);          //  DocumentDate
		miniTable.setColumnClass(6, String.class, true);   	//  Line
		
		//  Table UI
		miniTable.autoSize();
	}
}