/**
 * UntaCore Customization
 * @Untasoft www.untasoft.com
 */
package com.unicore.ui.grid;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.apps.IStatusBar;
import org.compiere.grid.CreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPricing;
import org.compiere.model.MRequisitionLine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.MUNSHandleRequests;

public class CreateFromPO extends CreateFrom {

	/**  Loaded Invoice             */
	//private MInvoice		m_invoice = null;
//	private int defaultLocator_ID=0;
	
	public CreateFromPO(GridTab gridTab) {
		super(gridTab);
		if (log.isLoggable(Level.INFO)) log.info(gridTab.toString());
	}

	@Override
	public Object getWindow() {
		return null;
	}

	@Override
	public boolean dynInit() throws Exception {
		log.config("");
		setTitle(Msg.getElement(Env.getCtx(), "C_Order_ID", false) 
				+ " .. " + Msg.translate(Env.getCtx(), "CreateFrom"));
		return true;
	}

	@Override
	public void info(IMiniTable miniTable, IStatusBar statusBar) {
		
	}

	@Override
	public boolean save(IMiniTable miniTable, String trxName) 
	{
		int C_Order_ID = ((Integer) getGridTab().getValue("C_Order_ID")).intValue();
		MOrder order = new MOrder(Env.getCtx(), C_Order_ID, trxName);

		/**
		 *  Selected        - 0
		 *  Qty             - 1
		 *  C_UOM_ID        - 2
		 *  M_Locator_ID    - 3
		 *  M_Product_ID    - 4
		 *  OrderLine       - 5
		 *  InvoiceLine     - 6
		 */
		// Lines
		for (int i = 0; i < miniTable.getRowCount(); i++)
		{
			if (((Boolean)miniTable.getValueAt(i, 0)).booleanValue()) {
				// variable values
				BigDecimal QtyEntered = (BigDecimal) miniTable.getValueAt(i, 1); // Qty
				KeyNamePair pp = (KeyNamePair) miniTable.getValueAt(i, 2); // UOM
				int C_UOM_ID = pp.getKey();

				pp = (KeyNamePair) miniTable.getValueAt(i, 3); // Product
				int M_Product_ID = pp.getKey();
				int M_RequisitionLine_ID = 0;
				Timestamp dateRequired = (Timestamp) miniTable.getValueAt(i, 4); // DateRequired
				
				pp = (KeyNamePair) miniTable.getValueAt(i, 6);
				if (pp != null)
					M_RequisitionLine_ID = pp.getKey();
		
				int precision = 2;
				if (M_Product_ID != 0)
				{
					MProduct product = MProduct.get(Env.getCtx(), M_Product_ID);
					precision = product.getUOMPrecision();
				}
				QtyEntered = QtyEntered.setScale(precision, BigDecimal.ROUND_HALF_DOWN);
				//
				if (log.isLoggable(Level.FINE)) log.fine("Line QtyEntered=" + QtyEntered
						+ ", Product=" + M_Product_ID + ", RequisitionLine=" + M_RequisitionLine_ID);

				//	Create new Order Line
				String sql = "SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID=? AND M_Product_ID=?";
				int ExistingOrderLine_ID = DB.getSQLValueEx(trxName, sql, order.get_ID(), M_Product_ID);
				
				MOrderLine ol = null;
				MRequisitionLine reqLine = new MRequisitionLine(Env.getCtx(), M_RequisitionLine_ID, trxName);
				com.uns.model.MProduct p = null;
				BigDecimal qtyConvert = Env.ZERO;
				if(reqLine.getM_Product_ID() > 0)
				{
					p = com.uns.model.MProduct.get(reqLine.getCtx(), reqLine.getM_Product_ID());
					qtyConvert = p.convertTo(reqLine.getC_UOM_ID(), p.getC_UOM_ID(), QtyEntered);
				}
				
				if (ExistingOrderLine_ID > 0) {
					ol = new MOrderLine(order.getCtx(), ExistingOrderLine_ID, trxName);
					ol.setQty(ol.getQtyEntered().add(qtyConvert.signum() > 0 ? qtyConvert : QtyEntered));
					
					if (ol.getDatePromised().after(dateRequired))
						ol.setDatePromised(dateRequired);
				}
				else {
					ol = new MOrderLine(order);
					if(reqLine.getM_Product_ID() > 0)
						ol.setM_Product_ID(M_Product_ID);	//	Line UOM
					else
						ol.setC_Charge_ID(reqLine.getC_Charge_ID());
					ol.setQty(qtyConvert.signum() > 0 ? qtyConvert : QtyEntered);
					ol.setC_UOM_ID(C_UOM_ID);//	Movement/Entered
					ol.setDatePromised(dateRequired);
//					ol.setM_RequisitionLine_ID(M_RequisitionLine_ID);
					
					//Set prices.
					if (reqLine.getPriceActual().signum() > 0) {
						ol.setPrice(reqLine.getPriceActual());
					}
					else {
						MProductPricing pPrice = 
								new MProductPricing (M_Product_ID, order.getC_BPartner_ID(), QtyEntered, 
										true, order.getC_BPartner_Location_ID());
						//
						int M_PriceList_ID = order.getM_PriceList_ID();
						pPrice.setM_PriceList_ID(M_PriceList_ID);
						Timestamp orderDate = order.getDateOrdered();
						/** PLV is only accurate if PL selected in header */
						int M_PriceList_Version_ID = 0;
						if ( M_PriceList_Version_ID == 0 && M_PriceList_ID > 0)
						{
							sql = "SELECT plv.M_PriceList_Version_ID "
								+ "FROM M_PriceList_Version plv "
								+ "WHERE plv.M_PriceList_ID=? "						//	1
								+ " AND plv.ValidFrom <= ? "
								+ "ORDER BY plv.ValidFrom DESC";
							//	Use newest price list - may not be future
	
							M_PriceList_Version_ID = DB.getSQLValueEx(null, sql, M_PriceList_ID, orderDate);
						}
						pPrice.setM_PriceList_Version_ID(M_PriceList_Version_ID);
						pPrice.setPriceDate(orderDate);
						ol.setPriceActual(pPrice.getPriceStd());
						ol.setPriceEntered(pPrice.getPriceStd());
						ol.setPriceList(pPrice.getPriceList());
						ol.setPriceLimit(pPrice.getPriceLimit());
					}
					ol.setAD_Org_ID(order.getAD_Org_ID());
				}
				ol.setDescription(ol.getDescription() == null ?
						reqLine.getDescription() : ol.getDescription() + " || " + reqLine.getDescription());
				ol.saveEx();
				order.saveEx();
				
				reqLine.setC_OrderLine_ID(ol.getC_OrderLine_ID());
				reqLine.saveEx();
				
				MUNSHandleRequests hr = new MUNSHandleRequests(Env.getCtx(), 0, trxName);
				hr.setAD_Org_ID(ol.getAD_Org_ID());
				hr.setM_RequisitionLine_ID(M_RequisitionLine_ID);
				hr.setC_OrderLine_ID(ol.get_ID());
				hr.setQty(QtyEntered);
				hr.saveEx();
			}   //   if selected
		}   //  for all rows
		return true;	
	}
	
	/**
	 *  Load Data - Order
	 *  @param M_Requisition_ID Order
	 */
	protected Vector<Vector<Object>> getRequisitionData (int M_Requisition_ID)
	{
		/**
		 *  Selected        - 0
		 *  Qty             - 1
		 *  C_UOM_ID        - 2..3
		 *  M_Product_ID    - 4..5
		 *  DateRequired	- 6
		 */
		if (log.isLoggable(Level.CONFIG)) log.config("M_Requisition_ID=" + M_Requisition_ID);
		p_order = new MOrder (Env.getCtx(), M_Requisition_ID, null);      //  save

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql =new StringBuilder("SELECT "
				+ "rl.Qty  - COALESCE(SUM(hr.Qty), 0), " 											// 1
				+ "p.C_UOM_ID, COALESCE(uom.UOMSymbol,uom.Name), "			//	2..3
				+ "COALESCE(rl.M_Product_ID, rl.C_Charge_ID), COALESCE(p.Name, c.Name), " 					//	4..5
				+ "r.DateRequired, rl.Description, "											// 6
				+ "rl.M_RequisitionLine_ID, rl.Line "
				+ "FROM M_RequisitionLine rl "
				+ "INNER JOIN M_Requisition r ON rl.M_Requisition_ID=r.M_Requisition_ID "
				+ "LEFT OUTER JOIN M_Product p ON rl.M_Product_ID=p.M_Product_ID "
				+ "LEFT OUTER JOIN C_Charge c ON c.C_Charge_ID = rl.C_Charge_ID "
				+ "LEFT OUTER JOIN UNS_HandleRequests hr ON rl.M_RequisitionLine_ID=hr.M_RequisitionLine_ID "
				+ " 	AND EXISTS (SELECT 1 FROM C_OrderLine ol WHERE ol.C_OrderLine_ID=hr.C_OrderLine_ID AND "
				+ "				EXISTS (SELECT 1 FROM C_Order o WHERE ol.C_Order_ID=o.C_Order_ID "
				+ "				AND o.DocStatus NOT IN ('VO', 'RE')))");

		if (Env.isBaseLanguage(Env.getCtx(), "C_UOM"))
			sql.append(" LEFT OUTER JOIN C_UOM uom ON (rl.C_UOM_ID=uom.C_UOM_ID)");
		else
			sql.append(" LEFT OUTER JOIN C_UOM_Trl uom ON (rl.C_UOM_ID=uom.C_UOM_ID AND uom.AD_Language='")
			.append(Env.getAD_Language(Env.getCtx())).append("')");
		//
		sql.append(" WHERE rl.M_Requisition_ID=?"
				+ " AND rl.Qty > (SELECT COALESCE(SUM(Qty),0) FROM UNS_HandleRequests WHERE M_RequisitionLine_ID = rl.M_RequisitionLine_ID"
				+ " AND C_OrderLine_ID NOT IN (SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID IN"
				+ " (SELECT C_Order_ID FROM C_Order WHERE DocStatus IN ('VO', 'RE')))) "			//	#1
				+ "GROUP BY rl.C_UOM_ID, COALESCE(uom.UOMSymbol,uom.Name), p.M_Product_ID, c.C_Charge_ID, "
				+ "r.DateRequired, rl.M_RequisitionLine_ID, rl.line"
				+ " ORDER BY rl.Line");
		//
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, M_Requisition_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
//				String Qty = "SELECT COALESCE(SUM(Qty),0) FROM UNS_HandleRequests WHERE M_RequisitionLine_ID=? AND"
//						+ " C_OrderLine_ID NOT IN ";
//				BigDecimal totalQty = DB.getSQLValueBD(null, Qty, rs.getInt(9));
				
//				if(rs.getBigDecimal(2).compareTo(totalQty) <= 0)
//					continue;
				
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));           //  0-Selection
				BigDecimal qty = rs.getBigDecimal(1);
				line.add(qty);  //  1-Qty
				KeyNamePair pp = new KeyNamePair(rs.getInt(2), rs.getString(3).trim());
				line.add(pp);                           //  2-UOM
				
				// Add product
				pp = new KeyNamePair(rs.getInt(4), rs.getString(5));
				line.add(pp);
				
				Timestamp dateRequired = rs.getTimestamp(6);
				line.add(dateRequired);                           //  6-DateRequired
				
				String desc = rs.getString(7);
				line.add(desc);
				//Requisition Line
				pp = new KeyNamePair(rs.getInt(8), rs.getString(9));
				line.add(pp);
				
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			//throw new DBException(e, sql.toString());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}   //  LoadOrder

	
	protected Vector<String> getOISColumnNames()
	{
		Vector<String> columnNames = new Vector<String>(7);
	    columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
	    columnNames.add(Msg.translate(Env.getCtx(), "Quantity"));
	    columnNames.add(Msg.translate(Env.getCtx(), "C_UOM_ID"));
	    columnNames.add(Msg.translate(Env.getCtx(), "M_Product_ID"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "DateRequired"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "Description"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "M_RequisitionLine_ID"));
	    
	    return columnNames;	
	}
	
	protected void configureMiniTable (IMiniTable miniTable)
	{
		miniTable.setColumnClass(0, Boolean.class, false);     //  Selection
		miniTable.setColumnClass(1, BigDecimal.class, false);      //  Qty
		miniTable.setColumnClass(2, String.class, true);          //  UOM
		miniTable.setColumnClass(3, String.class, true);   //  Product
		miniTable.setColumnClass(4, Timestamp.class, true);   //  DateRequired
		miniTable.setColumnClass(5, String.class, true);   //  Requisition Line
		miniTable.setColumnClass(6, String.class, true);   //  Requisition Line
		
		//  Table UI
		miniTable.autoSize();
	}
	
//	protected Vector<Vector<Object>> getRequisitionData (int M_Requisition_ID, boolean forInvoice, int M_Locator_ID)
//	{
//		defaultLocator_ID = M_Locator_ID;
//		return getOrderData (M_Requisition_ID, forInvoice);
//	}
	
} //CreateFromPO
