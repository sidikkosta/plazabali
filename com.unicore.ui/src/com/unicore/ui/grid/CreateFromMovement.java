/**
 * 
 */
package com.unicore.ui.grid;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.apps.IStatusBar;
import org.compiere.grid.CreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLineMA;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MWarehouse;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;

import com.unicore.model.MUNSHandleRequests;
import com.unicore.model.MUNSRequisitionLineMA;

/**
 * @author ALBURHANY
 *
 */
public class CreateFromMovement extends CreateFrom {
	
	private String m_Requisition = null;
	private int m_DestinationWarehouseID = 0;
	private int m_WarehouseID = 0;

	public CreateFromMovement(GridTab gridTab) {
		super(gridTab);
		if (log.isLoggable(Level.INFO)) log.info(gridTab.toString());
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.ICreateFrom#getWindow()
	 */
	@Override
	public Object getWindow() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#dynInit()
	 */
	@Override
	public boolean dynInit() throws Exception {
		log.config("");
		setTitle(Msg.getElement(Env.getCtx(), "M_Movement", false) 
				+ " .. " + Msg.translate(Env.getCtx(), "CreateFrom"));
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#info(org.compiere.minigrid.IMiniTable, org.compiere.apps.IStatusBar)
	 */
	@Override
	public void info(IMiniTable miniTable, IStatusBar statusBar) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.CreateFrom#save(org.compiere.minigrid.IMiniTable, java.lang.String)
	 */
	@Override
	public boolean save(IMiniTable miniTable, String trxName) {

		// Get Payment
		int M_Movement_ID = ((Integer) getGridTab().getValue("M_Movement_ID")).intValue();
		MMovement move = new MMovement(Env.getCtx(), M_Movement_ID, trxName);
		int M_Requisition_ID = 0;
		
		if (log.isLoggable(Level.CONFIG)) log.config(move + ", M_Movement_ID=" + M_Movement_ID);

		/**
		 *  Selected        		- 0
		 *  Quantity         		- 1
		 *  Product					- 2
		 *  DateRequired		    - 3
		 *  User					- 4
		 *  Description 			- 5
		 */
		
		// Lines
		for (int i = 0; i < miniTable.getRowCount(); i++)
		{
			if (((Boolean)miniTable.getValueAt(i, 0)).booleanValue()) {
				// variable values
				BigDecimal QtyEntered = (BigDecimal) miniTable.getValueAt(i, 1); // PaidAmt			
				KeyNamePair pp = (KeyNamePair) miniTable.getValueAt(i, 6); // Invoice
				int M_RequisitionLine_ID = pp.getKey();

				//	Create new Movement Line
				com.unicore.base.model.MRequisitionLine reqLine = 
						new com.unicore.base.model.MRequisitionLine(Env.getCtx(), M_RequisitionLine_ID, trxName);
				if(Util.isEmpty(m_Requisition, true))
				{
					m_Requisition = reqLine.getM_Requisition().getDocumentNo();
					M_Requisition_ID = reqLine.getM_Requisition_ID();
					m_DestinationWarehouseID = reqLine.getM_Requisition().getM_Warehouse_ID();
					m_WarehouseID = reqLine.getM_Requisition().getDestinationWarehouse_ID();
					String sql_ = "SELECT DocumentNo FROM M_Movement WHERE M_Requisition_ID = ?"
							+ " AND Docstatus NOT IN ('VO', 'RE') AND M_Movement_ID <> ?";
					String otherDoc = DB.getSQLValueString(trxName, sql_, M_Requisition_ID, move.get_ID());
					if(!Util.isEmpty(otherDoc))
						throw new AdempiereException("Requisition has had a movement " + otherDoc);
				}
				MWarehouse whOrigin = MWarehouse.get(Env.getCtx(), reqLine.getParent().getDestinationWarehouse_ID());
				com.unicore.base.model.MMovementLine line = null;
				MWarehouse whTo = new MWarehouse(Env.getCtx(), m_DestinationWarehouseID, trxName);
				MLocator locTo = MLocator.getDefault(whTo);
				MStorageOnHand[] storage = MStorageOnHand.getWarehouse(Env.getCtx(), whOrigin.get_ID(),
						reqLine.getM_Product_ID(), 0, null, true, true, 0, trxName, false);
				for(int j=0;j<storage.length;j++)
				{
					BigDecimal moveQty = Env.ZERO;
					if(storage[j].getM_Locator().isInTransit())
						continue;
					
					if(storage[j].getQtyOnHand().compareTo(QtyEntered) >= 1)
						moveQty = QtyEntered;
					else
						moveQty = storage[j].getQtyOnHand();
					
					String sql = "SELECT M_MovementLine_ID FROM M_MovementLine WHERE M_Product_ID=? AND M_Movement_ID=?"
							+ " AND M_Locator_ID = ?";
					int ExistingMoveLine = DB.getSQLValue(trxName, sql, new Object[]{reqLine.getM_Product_ID(), 
							move.get_ID(), storage[j].getM_Locator_ID()});
					if(ExistingMoveLine > 0)
					{
						line = new com.unicore.base.model.MMovementLine(Env.getCtx(), ExistingMoveLine, trxName);
						line.setMovementQty(line.getMovementQty().add(moveQty));
						line.setDescription(line.getDescription() == null ?
								reqLine.getDescription() : line.getDescription() + " || " + reqLine.getDescription());
					}
					else
					{
						line = new com.unicore.base.model.MMovementLine(Env.getCtx(), 0, trxName);
						line.setAD_Org_ID(move.getAD_Org_ID());
						line.setM_Movement_ID(move.get_ID());
						line.setM_Product_ID(reqLine.getM_Product_ID());
						line.setMovementQty(moveQty);
						line.setM_Locator_ID(storage[j].getM_Locator_ID());
						line.setM_LocatorTo_ID(locTo.get_ID());
						line.setDescription(reqLine.getDescription());
					}
					line.setManual(false);
					line.saveEx(trxName);
					
					QtyEntered = QtyEntered.subtract(moveQty);
					if(QtyEntered.compareTo(Env.ZERO) == 0)
						break;
				}
				
				if(line != null)
				{
					MUNSHandleRequests hr = MUNSHandleRequests.getByMoveLine(Env.getCtx(), line.get_ID(), trxName);
					if(hr == null)
						hr = new MUNSHandleRequests(Env.getCtx(), 0, trxName);
					hr.setM_MovementLine_ID(line.get_ID());
					hr.setAD_Org_ID(line.getAD_Org_ID());
					hr.setM_RequisitionLine_ID(M_RequisitionLine_ID);
					hr.setQty(hr.getQty().add(line.getMovementQty()));
					hr.saveEx();
				}
				else
				{
					continue;
				}
				
				MUNSRequisitionLineMA[] mas = reqLine.getMAs();
				if(mas.length > 0)
				{
					String sql = "DELETE FROM M_MovementLineMA WHERE M_MovementLine_ID = ?";
					DB.executeUpdate(sql, line.get_ID(), trxName);
					BigDecimal moveQty = line.getMovementQty();
					for(int x=0;x<mas.length;x++)
					{
						MStorageOnHand available = MStorageOnHand.get(line.getCtx(), line.getM_Locator_ID(),
								line.getM_Product_ID(), mas[x].getM_AttributeSetInstance_ID(),
									mas[x].getDateMaterialPolicy(), line.get_TrxName());
						if(available == null || available.getQtyOnHand().signum() <= 0)
							continue;
						if(available.getQtyOnHand().compareTo(mas[x].getMovementQty()) == -1)
							mas[x].setMovementQty(available.getQtyOnHand());
						if(moveQty.compareTo(mas[x].getMovementQty()) == 1)
						{
							MMovementLineMA ma = MMovementLineMA.addOrCreate(line, mas[x].getM_AttributeSetInstance_ID(),
									mas[x].getMovementQty(), mas[x].getDateMaterialPolicy(), false);
							ma.saveEx();
							moveQty = moveQty.subtract(mas[x].getMovementQty());
						}
						else if(moveQty.compareTo(mas[x].getMovementQty()) <= 0)
						{
							MMovementLineMA ma = MMovementLineMA.addOrCreate(line, mas[x].getM_AttributeSetInstance_ID(),
									moveQty, mas[x].getDateMaterialPolicy(), false);
							ma.saveEx();
							break;
						}
					}
				}
			}   //   if selected
		}   //  for all rows
		
		if(!Util.isEmpty(m_Requisition, true))
		{
			move.setPOReference(m_Requisition);
			move.setDestinationWarehouse_ID(m_DestinationWarehouseID);
			move.setM_Warehouse_ID(m_WarehouseID);
			move.setM_Requisition_ID(M_Requisition_ID);
			move.saveEx();
		}
		return true;
	}
	
	protected ArrayList<KeyNamePair> loadRequisitionData (int AD_User_ID)
	{
		int DestinationWarehouse_ID = 0;
		if(getGridTab().getValue("DestinationWarehouse_ID") != null)
			 DestinationWarehouse_ID = ((Integer) getGridTab().getValue("DestinationWarehouse_ID")).intValue();
		int M_Warehouse_ID = 0;
		if(getGridTab().getValue("M_Warehouse_ID") != null)
			M_Warehouse_ID = ((Integer) getGridTab().getValue("M_Warehouse_ID")).intValue();
		ArrayList<KeyNamePair> list = new ArrayList<KeyNamePair>();
		
		StringBuffer display = new StringBuffer("(CASE WHEN (req.ReferenceNo <> '' OR req.ReferenceNo IS NOT NULL)"
				+ " THEN req.ReferenceNo || ' - ' ELSE '' END) ||"
				+ " req.DocumentNo||' - '||")
		.append(DB.TO_CHAR("req.DateRequired", DisplayType.Date, Env.getAD_Language(Env.getCtx())))
		.append("||' - '||").append("us.Name");
		//
		StringBuffer sql = new StringBuffer("SELECT req.M_Requisition_ID, ").append(display)
		.append(" FROM M_Requisition req "
				+ " INNER JOIN AD_User us ON us.AD_User_ID = req.AD_User_ID"
				+ " WHERE req.DocStatus = 'CO'"
				+ " AND EXISTS (SELECT 1 FROM M_RequisitionLine rl"
				+ " 			LEFT OUTER JOIN (SELECT COALESCE(SUM(Qty),0) As SumHandledQty, hr.M_RequisitionLine_ID" 
				+ "     						 FROM UNS_HandleRequests hr "
				+ "      						 WHERE hr.M_RequisitionLine_ID IN ("
				+ "									SELECT rl2.M_RequisitionLine_ID FROM M_RequisitionLine rl2" 
				+ "									WHERE rl2.M_Requisition_ID=req.M_Requisition_ID)"
				+ "      						 GROUP BY hr.M_RequisitionLine_ID) As shq"
				+ "					ON shq.M_RequisitionLine_ID = rl.M_RequisitionLine_ID"
				+ "				WHERE rl.M_Requisition_ID = req.M_Requisition_ID AND (rl.Qty > shq.SumHandledQty OR shq.SumHandledQty IS NULL))");
		
		if(DestinationWarehouse_ID > 0)
			sql.append(" AND req.M_Warehouse_ID = ").append(DestinationWarehouse_ID);
		if(M_Warehouse_ID > 0)
			sql.append(" AND req.DestinationWarehouse_ID = ").append(M_Warehouse_ID);
		if(AD_User_ID > 0)
			sql.append(" AND us.AD_User_ID= ").append(AD_User_ID);
		
		sql.append(" ORDER BY req.ReferenceNo, req.DocumentNo");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return list;
	}
	
	protected Vector<Vector<Object>> getRequisitionLine (int M_Requisition_ID, int AD_User_ID)
	{		
//		int DestinationWarehouse_ID = ((Integer) getGridTab().getValue("DestinationWarehouse_ID")).intValue();
		/**
		 *  Selected        - 0
		 *  Quantity        - 1	
		 *  AvalibaleStock	- 2
		 *  C_UOM_ID        - 3
		 *  M_Product_ID    - 4
		 *  Locator			- 5
		 *  ReqLine	        - 6
		 */
		if (log.isLoggable(Level.CONFIG)) log.config("M_Requisition_ID=" + M_Requisition_ID);

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuilder sql = new StringBuilder("SELECT "
				+ " rl.Qty, u.Name, "	//	1..2
				+ " p.Name, req.DateRequired, COALESCE(rl.Description, '--'),"  //3..4..5
				+ " rl.Line, rl.M_RequisitionLine_ID "			//	6..7
				+ "	FROM M_RequisitionLine rl"
				+ " INNER JOIN M_Product p ON p.M_Product_ID = rl.M_Product_ID"
				+ " INNER JOIN C_UOM u ON u.C_UOM_ID = rl.C_UOM_ID"
				+ " INNER JOIN M_Requisition req ON req.M_Requisition_ID = rl.M_Requisition_ID");
//		sql.append(" WHERE M_Warehouse_ID=?");
		
//		if(M_Requisition_ID > 0)
			sql.append(" WHERE req.M_Requisition_ID=?");
			sql.append(" ORDER BY rl.Line ASC");
//		if(AD_User_ID > 0 && M_Requisition_ID <= 0)
//			sql.append(" AND req.AD_User_ID=?");
		
		//
		if (log.isLoggable(Level.FINER)) log.finer(sql.toString());
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
//			pstmt.setInt(1, DestinationWarehouse_ID);
//			if(M_Requisition_ID > 0)
				pstmt.setInt(1, M_Requisition_ID);
//			if(AD_User_ID > 0 && M_Requisition_ID <= 0)
//				pstmt.setInt(2, AD_User_ID);

			rs = pstmt.executeQuery();
			while (rs.next())
			{							
				Vector<Object> line = new Vector<Object>();
				line.add(new Boolean(false));           	//  0-Selection
				KeyNamePair pp = new KeyNamePair(rs.getInt(7), rs.getString(6).trim());
				String qtyMove = "SELECT SUM(Qty) FROM UNS_HandleRequests"
						+ " WHERE M_RequisitionLine_ID = ? AND M_MovementLine_ID > 0";
				BigDecimal qty = DB.getSQLValueBD(null, qtyMove, pp.getKey());
				if(qty == null)
					qty = Env.ZERO;
				BigDecimal QtyEntered = rs.getBigDecimal(1).subtract(qty);
				line.add(QtyEntered);  						//  1-PaidAmt
				String UOM = rs.getString(2);
				line.add(UOM);  							//  2-UOM
				String product = rs.getString(3);
				line.add(product);                          //  3-Product
				Timestamp dateRequired = rs.getTimestamp(4); 
				line.add(dateRequired);						// 4-Date
				String description = rs.getString(5);
				line.add(description);						//  5-Description

				line.add(pp);                           	//  6-line
				
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return data;
	}   //  LoadReqLineSelection
	
	protected Vector<String> getOISColumnNames()
	{
		Vector<String> columnNames = new Vector<String>(7);
	    columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "Qty"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "C_UOM_ID"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "M_Product_ID"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "DateRequired"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "Description"));
	    columnNames.add(Msg.getElement(Env.getCtx(), "M_RequisitionLine_ID", false));
	    
	    return columnNames;
	}
	
	protected void configureMiniTable (IMiniTable miniTable)
	{
		miniTable.setColumnClass(0, Boolean.class, false);     		//  Selection
		miniTable.setColumnClass(1, BigDecimal.class, false);      	//  MovementQty
		miniTable.setColumnClass(2, String.class, true);      		//  UOM
		miniTable.setColumnClass(3, String.class, true);			// Product
		miniTable.setColumnClass(4, Timestamp.class, true);         //  DateRequired
		miniTable.setColumnClass(5, String.class, true);        	//  Description
		miniTable.setColumnClass(6, String.class, true);   			//  Line
		
		//  Table UI
		miniTable.autoSize();
	}
}
