/**
 * 
 */
package com.uns.tablemodel;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

/**
 * @author nurse
 *
 */
public class UNSSalesWatchTableModel 
{
	private Vector<Vector<UNSValuePair<Object, Object>>> m_rows;
	private String[] m_headers = new String[]{"Location", "Total", 
			"00:00", "00:00", "00:00", "00:00", "00:00"};
	private UNSValuePair<Object, Object>[] m_totals;
	/**
	 * 
	 */
	public UNSSalesWatchTableModel() 
	{
		m_rows = new Vector<>();
	}

	public void addRow (Vector<UNSValuePair<Object, Object>> row)
	{
		m_rows.add(row);
	}
	
	public Vector<Vector<UNSValuePair<Object, Object>>> getRows ()
	{
		return m_rows;
	}
	
	public UNSValuePair<Object, Object>[] getTotals ()
	{
		return m_totals;
	}
	
	public void setValueAt (int row, int col, Object obj1, Object obj2)
	{
		Vector<UNSValuePair<Object, Object>> r = m_rows.get(row);
		if (r == null)
			return;
		if (obj1 == null || obj2 == null)
			return;
		
		r.get(col).setV1(obj1);
		r.get(col).setV2(obj2);
	}
	
	public void setValueAt (int row, int col, UNSValuePair<Object, Object> value)
	{
		Vector<UNSValuePair<Object, Object>> r = m_rows.get(row);
		if (r == null)
			return;
		r.setElementAt(value, col);
	}
	
	public UNSValuePair<Object, Object> getValueAt (int row, int col)
	{
		Vector<UNSValuePair<Object, Object>> r = m_rows.get(row);
		if (r == null)
			return null;
		return r.get(col);
	}
	
	public String[] getHeaders ()
	{
		return m_headers;
	}
	
	@SuppressWarnings("unchecked")
	public void updateRecord (int orgID)
	{
		m_rows = new Vector<>();
		m_totals = new UNSValuePair[]{
				new UNSValuePair<>("Total", "Number of Sales"), 
				new UNSValuePair<>(Env.ZERO, Env.ZERO), 
				new UNSValuePair<>(Env.ZERO, Env.ZERO), 
				new UNSValuePair<>(Env.ZERO, Env.ZERO), 
				new UNSValuePair<>(Env.ZERO, Env.ZERO), 
				new UNSValuePair<>(Env.ZERO, Env.ZERO), 
				new UNSValuePair<>(Env.ZERO, Env.ZERO)};
		
		Timestamp current = new Timestamp(System.currentTimeMillis()); //Timestamp.valueOf("2018-10-03 18:31:00");
		Timestamp tradingDate = TimeUtil.trunc(current, TimeUtil.TRUNC_DAY);
		Calendar cal = Calendar.getInstance();
		cal.setTime(current);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		int minute = cal.get(Calendar.MINUTE);
		
		if (minute >= 30)
			cal.set(Calendar.MINUTE, 30);
		else
			cal.set(Calendar.MINUTE, 0);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if (hour < 3)
			tradingDate = TimeUtil.addDays(tradingDate, -1);
		
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		Timestamp time1From = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, 29);
		cal.set(Calendar.SECOND, 59);
		Timestamp time1To = new Timestamp(cal.getTimeInMillis());
		String h1 = format.format(time1From);
		
		cal.add(Calendar.MINUTE, -59);
		cal.set(Calendar.SECOND, 0);
		Timestamp time2From = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, 29);
		cal.set(Calendar.SECOND, 59);
		Timestamp time2To = new Timestamp(cal.getTimeInMillis());
		String h2 = format.format(time2From);
		
		cal.add(Calendar.MINUTE, -59);
		cal.set(Calendar.SECOND, 0);
		Timestamp time3From = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, 29);
		cal.set(Calendar.SECOND, 59);
		Timestamp time3To = new Timestamp(cal.getTimeInMillis());
		String h3 = format.format(time3From);
		
		cal.add(Calendar.MINUTE, -59);
		cal.set(Calendar.SECOND, 0);
		Timestamp time4From = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, 29);
		cal.set(Calendar.SECOND, 59);
		Timestamp time4To = new Timestamp(cal.getTimeInMillis());
		String h4 = format.format(time4From);
		
		cal.add(Calendar.MINUTE, -59);
		cal.set(Calendar.SECOND, 0);
		Timestamp time5From = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, 29);
		cal.set(Calendar.SECOND, 59);
		Timestamp time5To = new Timestamp(cal.getTimeInMillis());
		String h5 = format.format(time5From);
		m_headers[2] = h1;
		m_headers[3] = h2;
		m_headers[4] = h3;
		m_headers[5] = h4;
		m_headers[6] = h5;
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			int idx = 0;
			st = DB.prepareStatement(m_sql, null);
			st.setTimestamp(++idx, tradingDate);
			st.setTimestamp(++idx, time1From);
			st.setTimestamp(++idx, time1To);
			st.setTimestamp(++idx, time2From);
			st.setTimestamp(++idx, time2To);
			st.setTimestamp(++idx, time3From);
			st.setTimestamp(++idx, time3To);
			st.setTimestamp(++idx, time4From);
			st.setTimestamp(++idx, time4To);
			st.setTimestamp(++idx, time5From);
			st.setTimestamp(++idx, time5To);
			rs = st.executeQuery();
			while (rs.next())
			{
				if (orgID > 0 && rs.getInt(14) != orgID)
					continue;
				Vector<UNSValuePair<Object, Object>> row = new Vector<>();
				BigDecimal total = rs.getBigDecimal(2);
				BigDecimal trx = rs.getBigDecimal(8);
				BigDecimal amtTime1 = rs.getBigDecimal(3);
				BigDecimal trxTime1 = rs.getBigDecimal(9);
				BigDecimal amtTime2 = rs.getBigDecimal(4);
				BigDecimal trxTime2 = rs.getBigDecimal(10);
				BigDecimal amtTime3 = rs.getBigDecimal(5);
				BigDecimal trxTime3 = rs.getBigDecimal(11);
				BigDecimal amtTime4 = rs.getBigDecimal(6);
				BigDecimal trxTime4 = rs.getBigDecimal(12);
				BigDecimal amtTime5 = rs.getBigDecimal(7);
				BigDecimal trxTime5 = rs.getBigDecimal(13);
				
				UNSValuePair<Object, Object> col1 = new UNSValuePair<Object, Object>(rs.getString(1), "Number of Sales");
				UNSValuePair<Object, Object> col2 = new UNSValuePair<Object, Object>(total, trx);
				UNSValuePair<Object, Object> col3 = new UNSValuePair<Object, Object>(amtTime1, trxTime1);
				UNSValuePair<Object, Object> col4 = new UNSValuePair<Object, Object>(amtTime2, trxTime2);
				UNSValuePair<Object, Object> col5 = new UNSValuePair<Object, Object>(amtTime3, trxTime3);
				UNSValuePair<Object, Object> col6 = new UNSValuePair<Object, Object>(amtTime4, trxTime4);
				UNSValuePair<Object, Object> col7 = new UNSValuePair<Object, Object>(amtTime5, trxTime5);
				
				row.add(col1);
				row.add(col2);
				row.add(col3);
				row.add(col4);
				row.add(col5);
				row.add(col6);
				row.add(col7);
				
				m_rows.add(row);
				
				UNSValuePair<Object, Object> total1 = m_totals[1];
				if (total1 == null)
				{
					total1 = new UNSValuePair<Object, Object>(Env.ZERO, Env.ZERO);
					m_totals[1] = total1;
				}
				UNSValuePair<Object, Object> total2 = m_totals[2];
				if (total2 == null)
				{
					total2 = new UNSValuePair<Object, Object>(Env.ZERO, Env.ZERO);
					m_totals[2] = total2;
				}
				UNSValuePair<Object, Object> total3 = m_totals[3];
				if (total3 == null)
				{
					total3 = new UNSValuePair<Object, Object>(Env.ZERO, Env.ZERO);
					m_totals[3] = total3;
				}
				UNSValuePair<Object, Object> total4 = m_totals[4];
				if (total4 == null)
				{
					total4 = new UNSValuePair<Object, Object>(Env.ZERO, Env.ZERO);
					m_totals[4] = total4;
				}
				UNSValuePair<Object, Object> total5 = m_totals[5];
				if (total5 == null)
				{
					total5 = new UNSValuePair<Object, Object>(Env.ZERO, Env.ZERO);
					m_totals[5] = total5;
				}
				UNSValuePair<Object, Object> total6 = m_totals[6];
				if (total6 == null)
				{
					total6 = new UNSValuePair<Object, Object>(Env.ZERO, Env.ZERO);
					m_totals[6] = total6;
				}
				
				BigDecimal total11 = (BigDecimal) total1.getV1();
				BigDecimal total12 = (BigDecimal) total1.getV2();
				BigDecimal total21 = (BigDecimal) total2.getV1();
				BigDecimal total22 = (BigDecimal) total2.getV2();
				BigDecimal total31 = (BigDecimal) total3.getV1();
				BigDecimal total32 = (BigDecimal) total3.getV2();
				BigDecimal total41 = (BigDecimal) total4.getV1();
				BigDecimal total42 = (BigDecimal) total4.getV2();
				BigDecimal total51 = (BigDecimal) total5.getV1();
				BigDecimal total52 = (BigDecimal) total5.getV2();
				BigDecimal total61 = (BigDecimal) total6.getV1();
				BigDecimal total62 = (BigDecimal) total6.getV2();
				
				total11 = total11.add(total);
				total12 = total12.add(trx);
				total21 = total21.add(amtTime1);
				total22 = total22.add(trxTime1);
				total31 = total31.add(amtTime2);
				total32 = total32.add(trxTime2);
				total41 = total41.add(amtTime3);
				total42 = total42.add(trxTime3);
				total51 = total51.add(amtTime4);
				total52 = total52.add(trxTime4);
				total61 = total61.add(amtTime5);
				total62 = total62.add(trxTime5);
				
				total1.setV1(total11);
				total1.setV2(total12);
				total2.setV1(total21);
				total2.setV2(total22);
				total3.setV1(total31);
				total3.setV2(total32);
				total4.setV1(total41);
				total4.setV2(total42);
				total5.setV1(total51);
				total5.setV2(total52);
				total6.setV1(total61);
				total6.setV2(total62);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	private final String m_sql = "SELECT bpName AS Name, SUM(x.total) AS Total, SUM (x.amt1) AS Amt1"
			+ ", SUM (x.amt2) AS Amt2, SUM (x.amt3) AS Amt3, SUM (x.amt4) AS Amt4, SUM (x.amt5) AS Amt5"
			+ ", SUM(x.trx) AS Trx, SUM(x.trx1) AS Trx1, SUM(x.trx2) AS Trx2, SUM(x.trx3) AS Trx3"
			+ ", SUM(x.trx4) AS Trx4, SUM(x.trx5) AS Trx5, Org AS Org FROM (SELECT bp.Value AS bpName"
			+ ", COALESCE (SUM (pt.GrandTotal), 0) AS total, 0 AS amt1, 0 AS amt2, 0 AS amt3"
			+ ", 0 AS amt4, 0 AS amt5, COUNT (pt.UNS_POSTrx_ID) AS trx, 0 AS trx1, 0 AS trx2"
			+ ", 0 AS trx3, 0 AS trx4, 0 AS trx5, pt.AD_Org_ID AS Org FROM UNS_POSTrx pt INNER JOIN C_BPartner bp "
			+ " ON bp.C_BPartner_ID = pt.C_BPartner_ID WHERE pt.DateAcct = ? AND pt.DocStatus IN ('CO','CL') "
			+ " GROUP BY bp.value, pt.AD_Org_ID UNION SELECT bp.value AS bpName"
			+ ", 0 AS total, COALESCE (SUM (pt.GrandTotal), 0) AS amt1, 0 AS amt2, 0 AS amt3, 0 AS amt4"
			+ ", 0 AS amt5, 0 AS trx, COUNT (pt.UNS_POSTrx_ID) AS trx1, 0 AS trx2, 0 AS trx3, 0 AS trx4"
			+ ", 0 AS trx5, pt.AD_Org_ID AS Org FROM UNS_POSTrx pt INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pt.C_BPartner_ID "
			+ " WHERE pt.DateTrx BETWEEN ? AND ? AND pt.DocStatus IN ('CO','CL') "
			+ " GROUP BY bp.value, pt.AD_Org_ID UNION SELECT bp.value AS bpName, 0 AS total, 0 AS amt1, COALESCE (SUM (pt.GrandTotal), 0) "
			+ " AS amt2, 0 AS amt3, 0 AS amt4, 0 AS amt5, 0 AS trx, 0 AS trx1, COUNT (pt.UNS_POSTrx_ID) AS trx2"
			+ ", 0 AS trx3, 0 AS trx4, 0 AS trx5, pt.AD_Org_ID AS Org FROM UNS_POSTrx pt INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = "
			+ " pt.C_BPartner_ID WHERE pt.DateTrx BETWEEN ? AND ? AND pt.DocStatus IN ('CO','CL') GROUP BY bp.value, pt.AD_Org_ID "
			+ " UNION SELECT bp.value AS bpName, 0 AS total, 0 AS amt1, 0 AS amt2"
			+ ", COALESCE (SUM (pt.GrandTotal), 0) AS amt3, 0 AS amt4, 0 AS amt5, 0 AS trx, 0 AS trx1, 0 AS trx2"
			+ ", COUNT (pt.UNS_POSTrx_ID) AS trx3, 0 AS trx4, 0 AS trx5, pt.AD_Org_ID AS Org FROM UNS_POSTrx pt INNER JOIN C_BPartner "
			+ " bp ON bp.C_BPartner_ID = pt.C_BPartner_ID WHERE pt.DateTrx BETWEEN ? AND ? AND pt.DocStatus IN ('CO','CL') "
			+ " GROUP BY bp.value, pt.AD_Org_ID UNION SELECT bp.value AS bpName, 0 AS total, 0 AS amt1"
			+ ", 0 AS amt2, 0 AS amt3, COALESCE (SUM (pt.GrandTotal), 0) AS amt4, 0 AS amt5, 0 AS trx, 0 AS trx1, 0 AS trx2"
			+ ", 0 AS trx3, COUNT (pt.UNS_POSTrx_ID) AS trx4, 0 AS trx5, pt.AD_Org_ID AS Org FROM UNS_POSTrx pt INNER JOIN C_BPartner bp "
			+ " ON bp.C_BPartner_ID = pt.C_BPartner_ID WHERE pt.DateTrx BETWEEN ? AND ? AND pt.DocStatus IN ('CO','CL') GROUP BY bp.value, pt.AD_Org_ID "
			+ " UNION SELECT bp.value AS bpName, 0 AS total, 0 AS amt1, 0 AS amt2, 0 AS amt3, 0 AS amt4, COALESCE (SUM (pt.GrandTotal), 0) "
			+ " AS amt5, 0 AS trx, 0 AS trx1, 0 AS trx2, 0 AS trx3, 0 AS trx4, COUNT (pt.UNS_POSTrx_ID) AS trx5, pt.AD_Org_ID AS Org "
			+ " FROM UNS_POSTrx pt INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pt.C_BPartner_ID WHERE pt.DateTrx BETWEEN ? AND ? GROUP BY bp.value, pt.AD_Org_ID) "
			+ " x GROUP BY x.bpName, x.Org ORDER BY x.bpName";
}
