/**
 * 
 */
package com.uns.tablemodel;

import java.math.BigDecimal;
import java.text.NumberFormat;
import org.compiere.model.MConversionRate;
import org.compiere.util.Env;

/**
 * @author nurse
 *
 */
public class UNSValuePair<V1, V2> 
{
	private V1 m_v1;
	private V2 m_v2;
	
	public UNSValuePair(V1 v1, V2 v2) 
	{
		m_v1 = v1;
		m_v2 = v2;
	}

	public V1 getV1 ()
	{
		return m_v1;
	}
	
	public V2 getV2 ()
	{
		return m_v2;
	}
	
	public void setV1 (V1 v1)
	{
		m_v1 = v1;
	}
	
	public void setV2 (V2 v2)
	{
		m_v2 = v2;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder("");
		String info1 = m_v1.toString();
		String infoUSD = "";
		String info2 = m_v2.toString();
		if (m_v1 instanceof BigDecimal) 
		{
			BigDecimal usdAmt = MConversionRate.convert(
					Env.getCtx(), (BigDecimal) m_v1, 303, 100, 
					Env.getAD_Client_ID(Env.getCtx()), 
					Env.getAD_Org_ID(Env.getCtx()));
			if (usdAmt == null)
				usdAmt = Env.ZERO;
			NumberFormat format = NumberFormat.getInstance();
			info1 = format.format(m_v1);
			infoUSD = format.format(usdAmt.doubleValue());
		}
		if (m_v2 instanceof BigDecimal)
		{
			NumberFormat format = NumberFormat.getInstance();
			info2 = format.format(m_v2);
		}
		sb.append("IDR ").append(info1).append("\n$ ").append(infoUSD).append("\n").append(info2);
		String toString = sb.toString();
		return toString;
	}
}
