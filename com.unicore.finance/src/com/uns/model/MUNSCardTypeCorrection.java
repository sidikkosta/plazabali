/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Msg;

/**
 * @author Burhani Adam
 *
 */
public class MUNSCardTypeCorrection extends X_UNS_CardTypeCorrection implements DocAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6340412802887873696L;

	/**
	 * @param ctx
	 * @param UNS_CardTypeCorrection_ID
	 * @param trxName
	 */
	public MUNSCardTypeCorrection(Properties ctx,
			int UNS_CardTypeCorrection_ID, String trxName) {
		super(ctx, UNS_CardTypeCorrection_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCardTypeCorrection(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSCardTypeCorrection(MUNSDisbursReconLine line)
	{
		this(line.getCtx(), 0, line.get_TrxName());
		setAD_Org_ID(line.getAD_Org_ID());
		setUNS_DisbursRecon_Line_ID(line.get_ID());
		setPrevCardType_ID(line.getOrg_CardType_ID());
		setNewCardType_ID(line.getUNS_CardType_ID());
		setDateTrx(line.getDateTrx());
		setDateAcct(line.getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getDisbursementDate());
		setShop_ID(line.getShop_ID());
		setBatchNo(line.getUNS_DisbursRecon_Batch().getBatchNo());
		setTID(line.getTID());
		setAmount(line.getNetAmount());
		setDifferenceAmt(line.getDifferenceAmt());
		setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_CardTypeCorrection));
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSCardTypeCorrection[");
		sb.append(get_ID()).append("-").append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_CardTypeCorrection_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		
		return true;
	}	
	
	protected boolean beforeDelete()
	{
		
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		m_processMsg = createRecapitulation(getPrevCardType_ID(), true);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		else
		{
			m_processMsg = createRecapitulation(getNewCardType_ID(), false);
			if(m_processMsg != null)
				return DocAction.STATUS_Invalid;
		}
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 303;
	}
	
	private String createRecapitulation(int cardTypeID, boolean isPrevious)
	{
		BigDecimal netAmt = isPrevious ? getAmount().negate() : getAmount();
		BigDecimal OriAmt = isPrevious ? (getAmount().add(getDifferenceAmt())).negate() : getAmount().add(getDifferenceAmt());
		BigDecimal diff = isPrevious ? getDifferenceAmt().negate() : getDifferenceAmt(); 
		String sql = "SELECT UNS_EDC_ID FROM UNS_CardTrxDetail"
				+ " WHERE UNS_CardTrxDetail_ID = ?";
		int edcID = DB.getSQLValue(get_TrxName(), sql, getUNS_DisbursRecon_Line().getUNS_CardTrxDetail_ID());
		MUNSDailyBankCardTypeRecap daily = MUNSDailyBankCardTypeRecap.getCreate(
				getCtx(), getAD_Org_ID(), edcID, cardTypeID, getDateAcct(), get_TrxName());
		daily.setTotalARAmount((daily.getTotalARAmount().add(OriAmt)).setScale(2, RoundingMode.HALF_DOWN));
		daily.setTotalDisbursedAmt((daily.getTotalDisbursedAmt().add(netAmt)).setScale(2, RoundingMode.HALF_DOWN));
		daily.setDifferenceAmt(daily.getDifferenceAmt().add(diff));
		if(!daily.save())
			return CLogger.retrieveErrorString("Error when trying create EDC Recapitulation.");
		
		MUNSBankEDCBatchRecap batch = MUNSBankEDCBatchRecap.getCreate(
				getCtx(), getAD_Org_ID(), edcID, getDateAcct(), getBatchNo(), get_TrxName());
		batch.setTotalARAmount((batch.getTotalARAmount().add(OriAmt)).setScale(2));
		batch.setTotalDisbursedAmt((batch.getTotalDisbursedAmt().add(netAmt)).setScale(2, RoundingMode.HALF_DOWN));
		batch.setDifferenceAmt((batch.getDifferenceAmt().add(diff)).setScale(2, RoundingMode.HALF_DOWN));
		if(!batch.save())
			return CLogger.retrieveErrorString("Error when trying create EDC Recapitulation.");
		
		return null;
	}
}