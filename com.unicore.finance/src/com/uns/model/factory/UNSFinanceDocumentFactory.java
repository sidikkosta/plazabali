/**
 * 
 */
package com.uns.model.factory;

import java.sql.ResultSet;

import org.compiere.acct.Doc;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MDepreciationEntry;

import com.unicore.model.MUNSAssetLeaseSettlement;
import com.uns.base.UNSAbstractDocumentFactory;
import com.uns.model.acct.Doc_AssetLeaseSettlement;
import com.uns.model.acct.Doc_DepreciationEntry;

/**
 * @author menjangan
 *
 */
public class UNSFinanceDocumentFactory extends UNSAbstractDocumentFactory {
	
	private static final String EXTENSION_HANDLER = "UNSFinanceDocumentFactory";
	
	@Override
	public Doc getDocument(MAcctSchema as, int AD_Table_ID, ResultSet rs, String trxName) 
	{
		if (com.uns.model.MUNSDisbursementRecon.Table_ID == AD_Table_ID)
			return new com.uns.model.acct.Doc_UNSDisbursementRecon(as, com.uns.model.MUNSDisbursementRecon.class, rs, "DSR", trxName);
		else if (com.uns.model.MUNSCardTypeCorrection.Table_ID == AD_Table_ID)
			return new com.uns.model.acct.Doc_UNSCardTypeCorrection(as, com.uns.model.MUNSCardTypeCorrection.class, rs, "CTC", trxName);
		else if (AD_Table_ID == MDepreciationEntry.Table_ID)
			return new Doc_DepreciationEntry(as, rs, trxName);
		else if (AD_Table_ID == MUNSAssetLeaseSettlement.Table_ID)
			return new Doc_AssetLeaseSettlement(as, rs, trxName);
		
		return super.getDocument(as, AD_Table_ID, rs, trxName);
	}

	@Override
	protected Class<?> getInternalClass(String className)
			throws ClassNotFoundException {
		// TODO Auto-generated method stub
		
		Class<?> clazz = Class.forName(className);
		return clazz;
	}
	
	public static String getExtensionID()
	{
		String extensionID = EXTENSION_HANDLER;
		return extensionID;
	}
}
