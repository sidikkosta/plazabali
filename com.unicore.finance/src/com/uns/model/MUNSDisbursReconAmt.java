/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MBankStatement;

import com.unicore.base.model.MBankStatementLine;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSFinanceModelFactory;
import com.uns.util.UNSApps;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDisbursReconAmt extends X_UNS_DisbursRecon_Amt {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8429396713065313906L;

	/**
	 * @param ctx
	 * @param UNS_DisbursRecon_Amt_ID
	 * @param trxName
	 */
	public MUNSDisbursReconAmt(Properties ctx, int UNS_DisbursRecon_Amt_ID,
			String trxName) {
		super(ctx, UNS_DisbursRecon_Amt_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDisbursReconAmt(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSDisbursReconAmt[] getAmts(MUNSDisbursementRecon parent)
	{
		java.util.List<MUNSDisbursReconAmt> list = Query.get(parent.getCtx(),
				UNSFinanceModelFactory.EXTENSION_ID, Table_Name, MUNSDisbursementRecon.Table_Name + "_ID=?",
					parent.get_TrxName()).setParameters(parent.get_ID())
						.list();
		
		return list.toArray(new MUNSDisbursReconAmt[list.size()]);
	}
	
	public boolean createStatementLine(MBankStatement statement, String documentNo)
	{
		if(getC_BankStatementLine_ID() > 0)
		{
			MBankStatementLine sl = new MBankStatementLine(getCtx(), getC_BankStatementLine_ID(), get_TrxName());
			if(sl.getStmtAmt().compareTo(getDisbursementAmt()) != 0)
			{
				sl.setStmtAmt(getDisbursementAmt());
				return sl.save();
			}
			return true;
		}
		
		MBankStatementLine line = new MBankStatementLine(statement);
		line.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_ARTransaction);
		line.setStmtAmt(getDisbursementAmt());
		line.setIsManual(false);
		line.setDescription("Create from edc disbursement, No " + documentNo + ".");
		int chargeID = UNSApps.getRefAsInt(UNSApps.CHRG_INTRANSITCASH);
		line.setC_Currency_ID(303);
		line.setC_Charge_ID(chargeID);
		if(!line.save())
			return false;
		
		setC_BankStatementLine_ID(line.get_ID());
		return save();
	}
}