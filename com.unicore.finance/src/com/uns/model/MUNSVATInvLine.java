/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSVATInvLine extends X_UNS_VATInvLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4986322805702176433L;

	/**
	 * @param ctx
	 * @param UNS_VATInvLine_ID
	 * @param trxName
	 */
	public MUNSVATInvLine(Properties ctx, int UNS_VATInvLine_ID, String trxName) {
		super(ctx, UNS_VATInvLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSVATInvLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(!isAuto())
		{
			BigDecimal taxRate = getC_InvoiceLine().getC_Tax().getRate();
			if(is_ValueChanged(COLUMNNAME_RevisionBeforeTaxAmt) || is_ValueChanged(COLUMNNAME_RevisionDiscAmt))
			{
				BigDecimal revBefTaxAmt = getRevisionBeforeTaxAmt().multiply(getQtyInvoiced());
				revBefTaxAmt = revBefTaxAmt.subtract(getRevisionDiscAmt());
				BigDecimal revTaxAmt = Env.ZERO;
				if(taxRate.signum() != 0)
					revTaxAmt = revBefTaxAmt.divide(taxRate);
				else
					revTaxAmt = revBefTaxAmt;
				
				setRevisionTaxAmt(revTaxAmt.setScale(0, RoundingMode.HALF_UP));
				setRevisionAmt(revBefTaxAmt);
			}
		}
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		return upHeader(newRecord);
	}
	
	public void setInvoiceLine(org.compiere.model.MInvoiceLine line)
	{
		setC_InvoiceLine_ID(line.get_ID());
		setAD_Org_ID(line.getAD_Org_ID());
		setLineNo(line.getLine());
		if(line.getM_Product_ID() > 0)
			setM_Product_ID(line.getM_Product_ID());
		else
			setC_Charge_ID(line.getC_Charge_ID());
		setDescription(line.getDescription());
		setQtyInvoiced(line.getQtyInvoiced());
		setC_UOM_ID(line.getC_UOM_ID());
		setC_Tax_ID(line.getC_Tax_ID());
//		setBeforeTaxAmt(line.getPriceList());
		setDiscountAmt(line.getDiscountAmt());		
//		setLineNetAmt(line.getLineNetAmt());
//		setRevisionBeforeTaxAmt(line.getPriceList());
		setRevisionDiscAmt(line.getDiscountAmt());
//		setRevisionAmt(line.getLineNetAmt());
		
		//set Tax
		BigDecimal multiplier = (line.getC_Tax().getRate().divide(Env.ONEHUNDRED)).setScale(2, RoundingMode.UP);
		BigDecimal taxAmt = Env.ZERO;
		BigDecimal priceList = Env.ZERO;
		if(!isIncludeTax())
		{
			priceList = line.getPriceList();
			taxAmt = (line.getLineNetAmt().multiply(multiplier));
		}
		if(isIncludeTax())
		{
			BigDecimal divisor = Env.ONE.add(multiplier);
			BigDecimal lineNet = (line.getLineNetAmt().divide(divisor, 2, RoundingMode.UP));
			priceList = (line.getPriceList().divide(divisor, 2, RoundingMode.UP));
			setLineNetAmt(lineNet);
			setRevisionAmt(lineNet);
			taxAmt = (lineNet.multiply(multiplier));
		}
		
		setBeforeTaxAmt(priceList);
		setRevisionBeforeTaxAmt(priceList);
		setTaxAmt(taxAmt);
		setRevisionTaxAmt(taxAmt);
	}
	
	public static MUNSVATInvLine[] getByParent(Properties ctx, int UNS_VATLine_ID, String trxName)
	{
		ArrayList<MUNSVATInvLine> list = new ArrayList<MUNSVATInvLine>();
		
		String sql = "SELECT vl.UNS_VATInvLine_ID FROM UNS_VATInvLine vl"
						+ " WHERE vl.UNS_VATLine_ID=?"
						+ " ORDER BY vl.LineNo ASC";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, UNS_VATLine_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add(new MUNSVATInvLine(ctx, rs.getInt(1),
						trxName));
			rs.close();
			pstmt.close();
			pstmt = null;
		} catch (SQLException ex) {
			throw new AdempiereException("Unable to load VAT Invoice Lines ",
					ex);
		} finally {
			DB.close(rs, pstmt);
		}

		MUNSVATInvLine[] retValue = new MUNSVATInvLine[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	private boolean upHeader(boolean newRecord)
	{
		MUNSVATLine line = new MUNSVATLine(getCtx(), getUNS_VATLine_ID(), get_TrxName());
		if(newRecord)
		{
			line.setBeforeTaxAmt(line.getBeforeTaxAmt().add(getLineNetAmt()));
			line.setTaxAmt(line.getTaxAmt().add(getTaxAmt()));
			line.setRevisionBeforeTaxAmt(line.getBeforeTaxAmt());
			line.setRevisionTaxAmt(line.getTaxAmt());
			line.setRevisionAmt(line.getGrandTotal());
			line.saveEx();
		}
		else
		{
			BigDecimal oldRevBeforeTaxAmt = ((BigDecimal) get_ValueOld(COLUMNNAME_RevisionAmt));
			BigDecimal oldRevTaxAmt = (BigDecimal) get_ValueOld(COLUMNNAME_RevisionTaxAmt);
			line.setRevisionBeforeTaxAmt((line.getRevisionBeforeTaxAmt().subtract(oldRevBeforeTaxAmt))
					.add(getRevisionAmt()));
			line.setRevisionTaxAmt((line.getRevisionTaxAmt().subtract(oldRevTaxAmt))
					.add(getRevisionTaxAmt()));
		}
		
		return line.save();
	}
	
	private boolean m_auto = false;
	
	public boolean isAuto()
	{
		return m_auto;
	}
	
	public void setAuto(boolean auto)
	{
		m_auto = auto;
	}
	
	private boolean m_includeTax = false;
	
	public boolean isIncludeTax()
	{
		return m_includeTax;
	}
	
	public void setIncludeTax(boolean includeTax)
	{
		m_includeTax = includeTax;
	}
}
