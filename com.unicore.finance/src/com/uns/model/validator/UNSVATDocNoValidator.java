/**
 * 
 */
package com.uns.model.validator;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MClient;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceTax;
import org.compiere.model.MOrg;
import org.compiere.model.MOrgInfo;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.DB;

import com.unicore.model.MUNSVATDocInOut;
import com.unicore.model.MUNSVATDocNo;

/**
 * @author ALBURHANY
 *
 */
public class UNSVATDocNoValidator implements ModelValidator {
	
	private String m_processMsg = "No VAT Number Avaliable";

	/**
	 * 
	 */
	public UNSVATDocNoValidator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(ModelValidationEngine engine, MClient client)
	{
		engine.addDocValidate(MInvoice.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {

		if (po.get_TableName().equals(MInvoice.Table_Name) && timing == TIMING_BEFORE_PREPARE)
		{			
			if(po.get_ValueAsBoolean("isSOTrx"))
			{
				MInvoice inv = new MInvoice(po.getCtx(), po.get_ID(), po.get_TrxName());
				
				MOrg org = new MOrg(po.getCtx(), po.get_ValueAsInt("AD_Org_ID"), po.get_TrxName());
				MOrgInfo orgInfo = new MOrgInfo(org);
				
				if (inv.getC_BPartner().isPKP() && orgInfo.getOrgTax().getName() == "PPN")
				{
					StringBuffer vatSQL = new StringBuffer
							("SELECT UNS_VATDocNo_ID FROM UNS_VATDocNo WHERE UsageStatus <> 'U'");
					int noVAT = DB.getSQLValue(po.get_TrxName(), vatSQL.toString());
					
					if(noVAT <= -1)
						return m_processMsg;
					
					if (inv.getC_BPartner().isVATDocOnCreate())
					{						
						StringBuffer idInvTax = new StringBuffer
								("SELECT C_InvoiceTax_ID FROM C_InvoiceTax WHERE C_Invoice_ID = " + po.get_ID());
						int idTax = DB.getSQLValue(po.get_TrxName(), idInvTax.toString());
						
						MInvoiceTax invTax = new MInvoiceTax(po.getCtx(), idTax, po.get_TrxName());
						invTax.setUNS_VATDocNo_ID(noVAT);
						invTax.saveEx();
						
						MUNSVATDocInOut vatOut = new MUNSVATDocInOut(po.getCtx(), 0, po.get_TrxName());
						
						vatOut.setAD_Org_ID(inv.getAD_Org_ID());
						vatOut.setUNS_VATDocNo_ID(noVAT);
						vatOut.setIsSOTrx(true);
						vatOut.setC_BPartner_ID(inv.getC_BPartner_ID());
						vatOut.setDateInvoiced(inv.getDateInvoiced());
						vatOut.saveEx();
					}
				}
			}
			
			if(!po.get_ValueAsBoolean("isSOTrx"))
			{
				MBPartner bpartner = new MBPartner(po.getCtx(), po.get_ValueAsInt("C_BPartner_ID"), po.get_TrxName());
				MOrg org = new MOrg(po.getCtx(), po.get_ValueAsInt("AD_Org_ID"), po.get_TrxName());
				MOrgInfo orgInfo = new MOrgInfo(org);
				
				if (bpartner.isPKP() && orgInfo.getOrgTax().getName() == "PPN")
				{
					StringBuffer sql = new StringBuffer
							("SELECT UNS_VATDocInOut_ID FROM UNS_VATDocInOut WHERE DocStatus = 'CO'"
									+ " AND isSOTrx = 'N' AND C_Invoice_ID = " + po.get_ID());
					
					if(DB.getSQLValue(po.get_TrxName(), sql.toString()) <= -1)
						throw new AdempiereException("Please Create VAT Document Receival for Continue Process");
				}
			}
		}
		
		if (po.get_TableName().equals(MInvoice.Table_Name) && timing == TIMING_AFTER_VOID)
		{
			if (po.get_ValueAsBoolean("isSOTrx"))
			{
				StringBuffer sql = new StringBuffer
						("SELECT UNS_VATDocNo_ID FROM C_InvoiceTax"
								+ " WHERE C_Invoice_ID = " + po.get_ID());
				int idVATDoc = DB.getSQLValue(po.get_TrxName(), sql.toString());

				if (idVATDoc >= 1)
				{
					MUNSVATDocNo vatDoc = new MUNSVATDocNo(po.getCtx(), idVATDoc, po.get_TrxName());
					vatDoc.setUsageStatus("C");
					vatDoc.saveEx();
				}
			}
			
			if(!po.get_ValueAsBoolean("isSOTrx"))
				return null;
		}
		return null;
	}
}