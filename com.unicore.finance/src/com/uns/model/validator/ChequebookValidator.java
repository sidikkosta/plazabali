package com.uns.model.validator;



import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MPayment;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Msg;

import com.unicore.model.MUNSChequeList;
import com.unicore.model.MUNSTransferBalanceConfirm;
import com.unicore.model.MUNSTransferBalanceRequest;
import com.uns.model.MUNSChequeChangelog;
import com.uns.model.MUNSChequeReconciliation;
import com.uns.model.MUNSChequebook;


public class ChequebookValidator implements ModelValidator
{
	/**
	 *	Constructor.
	 *	The class is instantiated when logging in and client is selected/known
	 */
	public ChequebookValidator ()
	{
		super ();
	}	//	MyValidator
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(ChequebookValidator.class);
	/** Client			*/
	private int	m_AD_Client_ID = -1;
	
	/**
	 *	Initialize Validation
	 *	@param engine validation engine 
	 *	@param client client
	 */

	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		if (client != null) {	
			m_AD_Client_ID = client.getAD_Client_ID();
			log.info(client.toString());
		}
		else  {
			log.info("Initializing global validator: "+this.toString());
		}

		// Tables to be monitored (n/a)
		
		// Documents to be monitored
		//engine.addDocValidate(MPayment.Table_Name, this);
		engine.addModelChange(MPayment.Table_Name, this);
		engine.addModelChange(MUNSChequeChangelog.Table_Name, this);
		engine.addModelChange(MUNSChequeReconciliation.Table_Name, this);
		engine.addDocValidate(MPayment.Table_Name, this);
		engine.addDocValidate(MUNSTransferBalanceConfirm.Table_Name, this);
		engine.addDocValidate(MUNSTransferBalanceRequest.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		log.info("AD_User_ID=" + AD_User_ID);
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception 
	{
		log.info(po.get_TableName() + " Type: "+type);
		String msg = null;

		if(po instanceof MPayment)
		{
			MPayment pay = (MPayment) po;
			if(pay.getC_DocType().getDocBaseType().equals(MDocType.DOCBASETYPE_ARReceipt))
				return msg;	
		}
		
		if (po.get_TableName().equals(MPayment.Table_Name))
		{
			if (type == TYPE_AFTER_NEW || type == TYPE_AFTER_CHANGE || type == TYPE_BEFORE_CHANGE)
			{
				MPayment payment = (MPayment) po;
				if (!payment.isReceipt() && payment.getReversal_ID() == 0)
					msg = checkChequeNo(payment, type);
			}
			/*
			else if (type == TYPE_AFTER_CHANGE) 
			{	
				MUNSChequeReconciliation mcr = 
						MUNSChequeReconciliation.getInstace(po.get_ID(), po.getCtx(), po.get_TrxName());
				if (mcr  != null) 
				{
					//String chequeNo2 = mcr.getChequeNo();
					if (!mcr.getChequeNo().equals(chequeNo))
						msg = Msg.getMsg(po.getCtx(), "CantChangeCheque");
				}	
				else {
					msg = checkAndTrySaveChequeReconciliation((MPayment) po, type);
				}
			}
			*/
		}
		else if(po instanceof MUNSChequeChangelog)
		{
			MUNSChequeChangelog cc = (MUNSChequeChangelog) po;
			MUNSChequeReconciliation recon = MUNSChequeReconciliation.getChequeReconciliation(
					po.getCtx(), cc.getUNS_Cheque_Reconciliation_ID(), po.get_TrxName());
			MUNSChequebook cb = new MUNSChequebook(po.getCtx(), recon.getUNS_Chequebook_ID(), po.get_TrxName());
			if(recon.getC_Payment_ID() > 0)
				cb.getReconciliation((MPayment) recon.getC_Payment());
			else if(recon.getUNS_TransferBalance_Request_ID() > 0)
				cb.getReconciliation(
						new MUNSTransferBalanceRequest(
								po.getCtx(), recon.getUNS_TransferBalance_Request_ID(), po.get_TrxName()));
		}
		else if(po instanceof MUNSChequeReconciliation)
		{
			MUNSChequeReconciliation recon = (MUNSChequeReconciliation) po;
			if(recon.getStatus() == null)
				return null;
			if(recon.getUNS_TransferBalance_Request_ID() > 0)
			{
				MUNSTransferBalanceConfirm confirm = MUNSTransferBalanceConfirm.get(
						po.getCtx(), recon.getUNS_TransferBalance_Request_ID(), recon.get_TrxName());
				if(recon.getStatus().equals(MUNSChequeReconciliation.STATUS_Disbursement))
				{
					if(confirm != null && !confirm.isComplete())
					{
						try {
							confirm.processIt("CO"); 
							confirm.saveEx();
						} catch (Exception e) {
							throw new AdempiereException(e.getMessage());
						}
					}
				}
				else if(recon.getStatus().equals(MUNSChequeReconciliation.STATUS_Voided))
				{
					if(confirm != null)
					{
						try {
							confirm.setIsAuto(true);
							confirm.processIt("VO"); 
							confirm.saveEx();
						} catch (Exception e) {
							throw new AdempiereException(e.getMessage());
						}
					}
				}
			}
		}
		
		return msg;				
	}

	@Override
	public String docValidate(PO po, int timing) {
		log.info(po.get_TableName() + " Timing: "+timing);
		
		String msg = null;

		if(po instanceof MPayment)
		{
			MPayment pay = (MPayment) po;
			if(pay.getC_DocType().getDocBaseType().equals(MDocType.DOCBASETYPE_ARReceipt)
					||  pay.getReversal_ID() > 0)
				return msg;	
			
			if (timing == TIMING_BEFORE_COMPLETE && pay.getTenderType().equals(MPayment.TENDERTYPE_ChequeGiro)) 
			{
				msg = checkAndTrySaveChequeReconciliation(po);
			}
		}
		else if(po instanceof MUNSTransferBalanceConfirm)
		{
			MUNSTransferBalanceConfirm confirm = (MUNSTransferBalanceConfirm) po;
			if(timing == TIMING_BEFORE_COMPLETE && confirm.getChequeNo() != null && confirm.getAccountFrom_ID() > 0)
			{
				msg = checkAndTrySaveChequeReconciliation(
						(MUNSTransferBalanceRequest) confirm.getUNS_TransferBalance_Request());
			}
		}
		else if(po instanceof MUNSTransferBalanceRequest)
		{
			MUNSTransferBalanceRequest req = (MUNSTransferBalanceRequest) po;
			if(timing == TIMING_AFTER_PREPARE && req.getChequeNo() != null && req.getAccountFrom_ID() > 0)
			{
				msg = checkAndTrySaveChequeReconciliation(req);
			}
		}
		
		return msg;
	}

	/**
	 * 
	 * @param po
	 * @return
	 */
	private String checkAndTrySaveChequeReconciliation(PO po)
	{
		String msg = null;
		String chequeNo = (String) ((po instanceof MPayment) ?
				po.get_Value(MPayment.COLUMNNAME_CheckNo) : po.get_Value("ChequeNo"));
		String columnCheque = (po instanceof MPayment) ? "CheckNo" : "ChequeNo";
		int bankID = (Integer) ((po instanceof MUNSTransferBalanceRequest) ?
				po.get_Value(MUNSTransferBalanceRequest.COLUMNNAME_AccountFrom_ID) : po.get_Value("C_BankAccount_ID"));

		MUNSChequebook cb = (chequeNo == null || "".equals(chequeNo))? null :  
				MUNSChequebook.isRegistered(po.getCtx(), po.get_TrxName(), chequeNo, bankID); 
		if ((cb == null))
		{
			return Msg.getMsg(po.getCtx(), "ChequeNotRegistered");
		}
		
		String properNumber = MUNSChequebook.getProperChequeNumberFormat(chequeNo, cb);
		
		if (cb.isUsed(properNumber, po instanceof MPayment ? po.get_ID() : -1,
				po instanceof MUNSTransferBalanceRequest ? po.get_ID() : -1)) 
		{
			msg = Msg.getMsg(po.getCtx(), "ChequeHasBeenUsed");
		}
		else  
		{
			msg = cb.getCreateReconciliation(po);
			if (msg == null)
			{
				cb.setlast_used(properNumber);
				po.set_ValueNoCheck(columnCheque, 
						properNumber);
				cb.save();
			}
		}
		return msg;
	}

	/**
	 * 
	 * @param po
	 * @return
	 */
	private String checkChequeNo(MPayment po, int type)
	{
		String msg = null;
		if (po.getTenderType().equals(MPayment.TENDERTYPE_ChequeGiro))
			if (type == TYPE_AFTER_NEW || po.is_ValueChanged(MPayment.COLUMNNAME_CheckNo)
					|| po.is_ValueChanged(MPayment.COLUMNNAME_C_BankAccount_ID) 
					|| po.is_ValueChanged(MPayment.COLUMNNAME_UNS_Cheque_List_ID))
		{	
			if (po.get_ValueAsInt(MPayment.COLUMNNAME_UNS_Cheque_List_ID) > 0)
			{
				String chequeNoFromList = MUNSChequeList.getName(
						po.get_TrxName(), po.get_ValueAsInt(
								MPayment.COLUMNNAME_UNS_Cheque_List_ID));
				po.set_ValueOfColumn(MPayment.COLUMNNAME_CheckNo, chequeNoFromList);
			}

			String chequeNo = po.getCheckNo();
			MUNSChequebook cb = (chequeNo == null || "".equals(chequeNo))? null :  
					MUNSChequebook.isRegistered(po.getCtx(), po.get_TrxName(), po.getCheckNo(), po.getC_BankAccount_ID()); 
			if ((cb == null))
			{
				return Msg.getMsg(po.getCtx(), "ChequeNotRegistered");
			}
			
			String cekNum = MUNSChequebook.getProperChequeNumberFormat(chequeNo, cb);
			
			if (cb.isUsed(cekNum, po.get_ID(), -1)) {
				msg = Msg.getMsg(po.getCtx(), "ChequeHasBeenUsed");
			}
			else if (type == TYPE_BEFORE_CHANGE || type == TYPE_AFTER_NEW){
				//po.set_ValueNoCheck(MPayment.COLUMNNAME_CheckNo, cekNum);
				po.set_ValueOfColumn(MPayment.COLUMNNAME_CheckNo, cekNum);
			}
		}
		return msg;
	}

	/**
	 * 
	 * @param payID
	 * @param Cheque
	 * @param trxName
	 * @return
	 */
	public String updateToProperChequeNo(int payID, String Cheque, String trxName ) {
		
		String success = null;
		try {
			String sql = "UPDATE " + MPayment.Table_Name 
					+ " SET " + MPayment.COLUMNNAME_CheckNo + " =?" 
					+ " WHERE " + MPayment.COLUMNNAME_C_Payment_ID + " =?";
			PreparedStatement stm = DB.prepareStatement(sql, trxName);
			stm.setString(1, Cheque);
			stm.setInt(2, payID);
			stm.executeUpdate();
			stm.close();
		}catch (SQLException ex) {
			log.log(Level.SEVERE, "ERROR", ex);
			success = "SaveError";
		}
		return success;
	}
	
}
