/**
 * 
 */
package com.uns.model.validator;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankStatement;
import org.compiere.model.MBankStatementLine;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MJournal;
import org.compiere.model.MJournalLine;
import org.compiere.model.MOrder;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_C_PaymentAllocate;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MUNSPRAllocation;
import com.uns.model.MUNSChequeReconciliation;

/**
 * @author Haryadi
 *
 */
public class FinancialValidator implements ModelValidator 
{

	/**
	 * 
	 */
	private int m_AD_Client_ID;
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(ChequebookValidator.class);
	
	
	/**
	 * 
	 */
	public FinancialValidator() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#initialize(org.compiere.model.ModelValidationEngine, org.compiere.model.MClient)
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) 
	{
		if (client != null) {	
			m_AD_Client_ID = client.getAD_Client_ID();
			log.info(client.toString());
		}
		else  {
			log.info("Initializing global validator: "+this.toString());
		}
		engine.addModelChange(MJournalLine.Table_Name, this);
		engine.addDocValidate(MJournal.Table_Name, this);
		engine.addDocValidate(MPayment.Table_Name, this);
		engine.addModelChange(MPayment.Table_Name, this);
		engine.addModelChange(MBankStatement.Table_Name, this);
		engine.addModelChange(MPaymentAllocate.Table_Name, this);
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#getAD_Client_ID()
	 */
	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#login(int, int, int)
	 */
	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#modelChange(org.compiere.model.PO, int)
	 */
	@Override
	public String modelChange(PO po, int type) throws Exception 
	{
		String errMsg = null;
		
		//menghandel biaya transfer ditanggung perusahaan
		//pembayaran akan dikurangi dengan biaya transfer
		
		if(po.get_Table_ID() == MPayment.Table_ID)
		{
			if(type != TYPE_BEFORE_CHANGE && type != TYPE_BEFORE_NEW)
				return null;
			MPayment payment = (MPayment) po;
			if(payment.getC_Charge_ID() == 0)
				payment.setChargeAmt(Env.ZERO);
//			else if(!payment.isReceipt())
//				payment.setChargeAmt(Env.ZERO);
//			else if(MPaymentAllocate.get(payment).length == 0)
//				payment.setChargeAmt(Env.ZERO);
			BigDecimal chargeAmt = payment.getChargeAmt();

//			BigDecimal payAmt = payment.getPayAmt();
			
//			if(chargeAmt.signum() != 0 && chargeAmt.compareTo(payAmt) == 1)
//				return "Charge amount can't be more than pay amount";
			
			BigDecimal allocatedAmt = payment.getPayAmt();
			
			if (payment.getReversal_ID() <= 0 && ((payment.getC_Order_ID() == 0 
					&& payment.getC_Invoice_ID() == 0) || payment.is_ValueChanged("C_Charge_ID")
					|| payment.is_ValueChanged("PayAmt") || payment.is_ValueChanged("ChargeAmt")))
			{
				String sql = "SELECT SUM(Amount) " +
							" FROM C_PaymentAllocate WHERE C_Payment_ID=" + payment.get_ID();
				allocatedAmt = DB.getSQLValueBDEx(payment.get_TrxName(), sql);
				
				if (allocatedAmt == null)
				{
					if(payment.getC_Charge_ID() > 0)
					{
						allocatedAmt = payment.getPayAmt();
						payment.setChargeAmt(Env.ZERO);
						payment.setTotalAmt(Env.ZERO);
					}
					else
					{
						payment.setTotalAmt(allocatedAmt);
						payment.setPayAmt(payment.getTotalAmt().subtract(payment.getChargeAmt()));
					}
				}
				else
				{
					payment.setTotalAmt(allocatedAmt);
					payment.setPayAmt(allocatedAmt.subtract(chargeAmt));
				}
//				if (chargeAmt.signum()< 0)
//					payment.setTotalAmt(payment.getPayAmt());
			}
			
			return null;
			
//			BigDecimal totalAmt = payment.getTotalAmt();
//			if(totalAmt.signum() == 0)
//			{
//				totalAmt = payAmt;
//			}
//			else if(po.is_ValueChanged(MPayment.COLUMNNAME_PayAmt))
//			{
//				totalAmt = payAmt;
//			}
//			
//			payAmt = totalAmt.subtract(chargeAmt);
//			payment.setPayAmt(payAmt);
//			payment.setTotalAmt(totalAmt);
		}
		
		//hanya bisa buka 1 bank/cash statement untuk akun yang sama dalam waktu bersamaan.
		if(po.get_Table_ID() == MBankStatement.Table_ID)
		{
			if((type != TYPE_BEFORE_CHANGE && type != TYPE_BEFORE_NEW) 
					|| (po.get_Value("DocStatus").equals("CO") || po.get_Value("DocStatus").equals("CL")) )
				return null;
			
			String sql = "SELECT 1 FROM C_BankStatement WHERE DocStatus IN ('DR', 'IV')"
					+ " AND C_BankAccount_ID = " + po.get_ValueAsInt("C_BankAccount_ID")
					+ " AND C_BankStatement_ID <> " + po.get_ID();
			 
			 if (DB.getSQLValue(po.get_TrxName(), sql) >= 1)
				 throw new AdempiereException("Failed: Duplicate data is found."
				 		+ " Account Bank/Cash Statement cannot used. \n"
				 		+ "Please be sure you are completed previouse data before create new.");
		}
		//TODO JURNAL Cross balance
//		if (po.get_TableName().equals(MJournalLine.Table_Name)
//			&& (TYPE_BEFORE_NEW == type || TYPE_BEFORE_CHANGE == type))
//		{
//			MJournalLine jLine = (MJournalLine) po;
//			MJournal journal = jLine.getParent();
//			
//			if(!MJournal.JOURNALTYPE_VendorCrossBalance.equals(journal.getJournalType()))
//				return errMsg;
//			MJournalLine[] jLines = journal.getLines(false);
//
//			if (jLines == null)
//				return null;
//			
//			if (TYPE_BEFORE_NEW == type)
//			{
//				MJournalLine[] jLineTmps = new MJournalLine[jLines.length + 1];
//				for (int i=0; i < jLines.length; i++)
//					jLineTmps[i] = jLines[i];
//				
//				jLineTmps[jLineTmps.length - 1] = jLine;
//				jLines = jLineTmps;
//			}
//			return isValidBPartnerInJournalLines(jLines);
//		}
		
		if(po.get_TableName().equals(MPaymentAllocate.Table_Name))
		{
			String name = null;
			StringBuilder sqql = new StringBuilder("SELECT 1 FROM C_PaymentAllocate"
					+ " WHERE C_PaymentAllocate_ID <> ? AND"
					+ " C_Payment_ID = (SELECT C_Payment_ID FROM C_PaymentAllocate WHERE "
					+ " C_PaymentAllocate_ID = ?) AND isActive = 'Y'"); 
			if(po.get_ValueAsInt(MPaymentAllocate.COLUMNNAME_C_Order_ID) > 0 )
				{
					sqql.append(" AND C_Order_ID = "+po.get_ValueAsInt(MPaymentAllocate.COLUMNNAME_C_Order_ID));
					name = "Order";
				}
			else
				{
					sqql.append(" AND C_Invoice_ID = "+po.get_ValueAsInt(MPaymentAllocate.COLUMNNAME_C_Invoice_ID));
					name = "Invoice";
				}
			
			boolean exists = DB.getSQLValue(po.get_TrxName(), sqql.toString(), po.get_ID(), po.get_ID()) > 0;
			if(exists)
				throw new AdempiereException("Disallowed duplicate "+name+" of Payment Allocate");
			
			if(po.get_ValueAsInt(X_C_PaymentAllocate.COLUMNNAME_C_Invoice_ID) <= 0)
				return null;
			
			if(type == TYPE_AFTER_NEW || type == TYPE_AFTER_CHANGE)
			{	
				//disallowed paid amount greater than open amount for CN Invoice. (BASKOM)
				errMsg = validateAmountCN(po);
				if(!Util.isEmpty(errMsg))
					throw new AdempiereException(errMsg);
			}
			else if(type == TYPE_BEFORE_DELETE)
			{
				String sql = "SELECT COUNT(*) FROM C_PaySelectionLine WHERE C_PaymentAllocate_ID=?";
				int count = DB.getSQLValue(po.get_TrxName(), sql, po.get_ID());
				if(count > 0)
				{
					String upPSelection = "UPDATE C_PaySelectionLine SET C_PaymentAllocate_ID = null WHERE C_PaymentAllocate_ID=?";
					DB.executeUpdate(upPSelection, po.get_ID(), false, po.get_TrxName());
				}
			}
		}
		
		return errMsg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#docValidate(org.compiere.model.PO, int)
	 */
	@Override
	public String docValidate(PO po, int timing) 
	{
		String errMsg = null;
		//TODO Vendor Cross Balance
//		if (po.get_TableName().equals(MJournal.Table_Name) && (TIMING_BEFORE_COMPLETE == timing))
//		{
//			MJournal journal = (MJournal) po;
//			MJournalLine[] jLines = journal.getLines(false);
//			
//			errMsg = isValidBPartnerInJournalLines(jLines);
//			if (errMsg != null)
//				return errMsg;
//			
//			for (MJournalLine jLine : jLines)
//			{
//				BigDecimal amtDr = Env.ZERO;
//				if (jLine.getAmtAcctDr().signum() > 0)
//					amtDr = jLine.getAmtAcctDr();
//				else if (jLine.getAmtAcctCr().signum() < 0)
//					amtDr = jLine.getAmtAcctCr().abs();
//				
//				BigDecimal amtCr = Env.ZERO;
//				if (jLine.getAmtAcctCr().signum() > 0)
//					amtCr = jLine.getAmtAcctCr();
//				else if (jLine.getAmtAcctDr().signum() < 0)
//					amtCr = jLine.getAmtAcctDr().abs();
//				
//				MBPartner bp = (MBPartner) jLine.getC_BPartner();
//				
//				if (amtDr.signum() > 0)
//				{
//					if (MJournal.JOURNALTYPE_VendorCrossBalance.equals(journal.getJournalType()))
//					{
//						if (journal.getReversal_ID() == 0)
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().add(amtDr));
//						else
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().subtract(amtDr));
//					}
//					else if (MJournal.JOURNALTYPE_CustomerCrossBalance.equals(journal.getJournalType()))
//					{
//						if (journal.getReversal_ID() == 0)
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().add(amtDr));
//						else 
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().subtract(amtDr));
//					}
//				}
//				else if (amtCr.signum() > 0)
//				{
//					if (MJournal.JOURNALTYPE_VendorCrossBalance.equals(journal.getJournalType()))
//					{
//						if (journal.getReversal_ID() == 0)
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().subtract(amtCr));
//						else
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().add(amtCr));
//					}
//					else if (MJournal.JOURNALTYPE_CustomerCrossBalance.equals(journal.getJournalType()))
//					{
//						if (journal.getReversal_ID() == 0)
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().subtract(amtCr));
//						else
//							bp.setTotalOpenBalance(bp.getTotalOpenBalance().add(amtCr));
//					}
//				}
//				bp.saveEx();
//			}
//		}
//		else 
		if (po.get_TableName().equals(MPayment.Table_Name) && (TIMING_AFTER_COMPLETE == timing))
		{
			MPayment thePayment = (MPayment) po;
			if (thePayment.getC_Order_ID() > 0 
				&& thePayment.getPayAmt().signum() > 0 
				&& thePayment.getC_BPartner_ID() > 0)
			{
				X_C_Order theOrder = (X_C_Order) thePayment.getC_Order();
				
				String prevDesc = theOrder.getDescription() == null? "" : theOrder.getDescription();
				
				String theAmt = 
						NumberFormat.getInstance(new Locale("ID")).format(thePayment.getPayAmt().doubleValue());
				
				String theTrxDate = SimpleDateFormat.getDateInstance(SimpleDateFormat.MEDIUM)
						.format(thePayment.getDateTrx());
				
				theOrder.setDescription("*** Prepaid by finance for amount of " + 
										thePayment.getC_Currency().getISO_Code() + " " + 
										theAmt + " @" + theTrxDate + " ***\n" + prevDesc);
				theOrder.saveEx();
			}
			
			errMsg = createStatementOfPayment(thePayment);
		}
		else if(po.get_TableName().equals(MPayment.Table_Name) && TIMING_BEFORE_PREPARE == timing)
		{
			MPayment payment = (MPayment) po;
			
			if(payment.getTenderType().equals(MPayment.TENDERTYPE_ChequeGiro)
					&& payment.getCheckNo() == null)
			{
				return "Please define check no...";
			}
			
			MPaymentAllocate[] allocates = MPaymentAllocate.get(payment);
			for(int i=0; i<allocates.length; i++)
			{
				MPaymentAllocate allocate = allocates[i];
				int order_ID = allocate.getC_Order_ID();
				if(order_ID == 0)
				{
					//check order invoice is cash order or not
					if(allocate.getC_Invoice_ID() == 0)
						continue;
					
					String sql = "SELECT inv.C_Order_ID FROM C_Invoice inv WHERE C_Invoice_ID = ?"
							+ " AND EXISTS(SELECT * FROM C_Order WHERE C_Order_ID = inv.C_Order_ID"
							+ " AND C_DocType_ID IN (SELECT C_DocType_ID FROM C_DocType WHERE"
							+ " DocSubTypeSO IN (?,?)))";
					int order = DB.getSQLValue(allocate.get_TrxName(), sql,
							allocate.getC_Invoice_ID(),MDocType.DOCSUBTYPESO_CashOrder,
							MDocType.DOCSUBTYPESO_PrepayOrder);
					if(order <= 0)
						continue;
					
					order_ID = order;
				}
				
				MOrder order = new MOrder(allocate.getCtx(), order_ID, allocate.get_TrxName());
				order.setC_Payment_ID(payment.get_ID());
				String docSubTypeSo = order.getC_DocType().getDocSubTypeSO();
				if(!docSubTypeSo.equals(MDocType.DOCSUBTYPESO_CashOrder)
						&& !docSubTypeSo.equals(MDocType.DOCSUBTYPESO_PrepayOrder))
				{
					errMsg = "Can't process Order document. It is not cash / pre pay order.";
					break;
				}
				
				BigDecimal amount = allocate.getPayToOverUnderAmount()
						.add(allocate.getWriteOffAmt());
				if(amount.signum() == 0)
				{
					errMsg = "Invalid amount.";
					break;
				}
				if(order.getGrandTotal().compareTo(amount) == 1
						&& !docSubTypeSo.equals(MDocType.DOCSUBTYPESO_PrepayOrder))
				{
					errMsg = "Can't complete document. Grand Total of Order must less than or equal to payment amount";
					break;
				}
				
				if(!order.isComplete())
				{
					boolean ok = order.processIt(DocAction.ACTION_Complete);
					if(!ok || !order.isComplete() || !order.save())
					{
						errMsg = "Can't complete Order Document " + order.getProcessMsg();
						break;
					}
				}
				
				MInvoice invoice = null;
				int C_Invoice_ID = allocate.getC_Invoice_ID();
				if(C_Invoice_ID == 0)
				{
					C_Invoice_ID = order.getC_Invoice_ID();
				}
				if(C_Invoice_ID <= 0)
				{
					invoice = order.createInvoice (
							(MDocType) order.getC_DocType(), null, order.getDatePromised());
					if (invoice == null)
					{
						errMsg = "Failed when trying create invoice from order. " + order.getProcessMsg();
					}
					else if (invoice.getProcessMsg()!= null)
					{
						errMsg = "Failed when trying create invoice from order." + invoice.getProcessMsg();
					}
					else
					{
						C_Invoice_ID = invoice.get_ID();
					}
					
					if(!Util.isEmpty(errMsg, true))
					{
						break;
					}
				}
				else
				{
					invoice = new MInvoice(po.getCtx(), C_Invoice_ID, po.get_TrxName());
				}
				
				if(!invoice.isComplete())
				{
					boolean ok = invoice.processIt(DocAction.ACTION_Complete);
					if(!ok)
					{
						errMsg = "Can't complete Invoice Document " + invoice.getProcessMsg();
						break;
					}
				}
				allocate.setC_Invoice_ID(C_Invoice_ID);
				allocate.saveEx();
			}
		}
		else if (po.get_TableName().equals(MPayment.Table_Name) && timing == TIMING_AFTER_COMPLETE
				&& ((MPayment) po).getUNS_PR_Allocation_ID() > 0)
		{
			MUNSPRAllocation receiptAllocation = new MUNSPRAllocation(
					po.getCtx(), po.get_ValueAsInt("UNS_PR_Allocation_ID"), po.get_TrxName());
			MPayment[] paymentList = receiptAllocation.getLines();
			boolean fullyCompleted = true;
			for(int i=0; i<paymentList.length; i++)
			{
				if(paymentList[i].get_ID() == po.get_ID())
				{
					continue;
				}
				else if(paymentList[i].isComplete())
				{
					continue;
				}
				fullyCompleted = false;
			}
			
			if(!fullyCompleted)
			{
				return errMsg;
			}
			
			if(receiptAllocation.getDocStatus().equals("DR")
					|| receiptAllocation.getDocStatus().equals("IP")
					|| receiptAllocation.getDocStatus().equals("IN"))
			{
				receiptAllocation.m_force = true;
				boolean ok = receiptAllocation.processIt(DocAction.ACTION_Complete);
				if(!ok) 
				{
					errMsg = receiptAllocation.getProcessMsg();
				}
			}
		}
		else if(po.get_TableName().equals(MPayment.Table_Name)
				&& (timing == TIMING_BEFORE_VOID || timing == TIMING_BEFORE_REVERSEACCRUAL
						|| timing == TIMING_BEFORE_REVERSECORRECT))
		{
			MPayment payment = (MPayment) po;
			
			if(!payment.isOutAction() && payment.getTenderType().equals(MPayment.TENDERTYPE_ChequeGiro))
			{
				String whereClause = "UNS_ChequeBook_ID IN (SELECT UNS_Chequebook_ID FROM UNS_Chequebook"
						+ " WHERE C_BankAccount_ID = ?) AND ChequeNo = ? ";
				String sql = "SELECT 1 FROM UNS_Cheque_Reconciliation WHERE "+whereClause
						+ " AND Status NOT IN (?,?)";
				boolean exists = DB.getSQLValue(payment.get_TrxName(), sql, payment.getC_BankAccount_ID(),
						payment.getCheckNo(), MUNSChequeReconciliation.STATUS_HandoverVendor,
						MUNSChequeReconciliation.STATUS_Voided) > 0;
				if(exists)
				{
					String action = timing == TIMING_BEFORE_VOID ? "Void" : "Reverse";
					return "Cannot "+action+" from Payment. Please check Cheque Reconciliation";
				}
				
				sql = "SELECT UNS_Cheque_Reconciliation_ID FROM UNS_Cheque_Reconciliation WHERE "+whereClause
						+ " AND Status = (?)";
				int chequeID = DB.getSQLValue(payment.get_TrxName(), sql, payment.getC_BankAccount_ID(),
						payment.getCheckNo(), MUNSChequeReconciliation.STATUS_HandoverVendor);
				if(chequeID > 0)
				{
					MUNSChequeReconciliation cheque = new MUNSChequeReconciliation(payment.getCtx(), chequeID, payment.get_TrxName());
					if(cheque != null)
					{
						cheque.deleteEx(true);
					}
				}
			}
		}
		
		return errMsg;
	}
	
//	/**
//	 * 
//	 * @param journal
//	 * @return
//	 */
//	private String isValidBPartnerInJournalLines(MJournalLine[] jLines)
//	{
//		String errMsg = null;
//		
//		//MJournalLine[] jLines = journal.getLines(false);
//		
//		for (MJournalLine jLine : jLines)
//		{
//			int vFromID = jLine.getC_BPartner_ID();
//			if (vFromID == 0)
//				errMsg += "BPartner is not selected in line no:" + jLine.getLine() + " \n";
//			
//			if (jLine.getAmtSourceDr().signum() < 0 || jLine.getAmtSourceCr().signum() > 0)
//			{ // this is the line of vendor to reduce the AP.
//				for (MJournalLine jLineTo : jLines)
//				{
//					int vToID = jLineTo.getC_BPartner_ID();
//					if (jLine.getAmtSourceCr().signum() < 0 || jLine.getAmtSourceDr().signum() > 0)
//					{
//						if (vToID == 0)
//							errMsg += "BPartner is not selected in line no:" + jLineTo.getLine() + " \n";
//						else if (vToID == vFromID)
//							errMsg += "Cannot cross balance to the same BPartner in line no:" + jLine.getLine() + 
//							" with line no: " + jLineTo.getLine() + "\n";
//					}
//				}
//			}
//		}
//		
//		return errMsg;
//	}

	private String createStatementOfPayment(MPayment payment)
	{
		if (!payment.isAutoCreateStatement())
			return null;
		
		try
		{
			String sql = " SELECT 1 FROM C_BankStatementLine WHERE "
					+ " C_Payment_ID = ? AND EXISTS (SELECT * FROM "
					+ " C_BankStatement WHERE C_BankStatement_ID = "
					+ " C_BankStatementLine.C_BankStatement_ID AND "
					+ " DocStatus NOT IN ('RE','VO')) ";
			int exists = DB.getSQLValueEx(payment.get_TrxName(), sql, 
					payment.get_ID());
			boolean isExists = exists > 0;
			if (isExists)
				return null;
			
			MBankStatement bStmt = MBankStatement.getOpen(
					payment.getC_BankAccount_ID(), 
					payment.get_TrxName(), true);
			
			MBankStatementLine bStmtL = new MBankStatementLine(bStmt);
			bStmtL.setPayment(payment);
			bStmtL.saveEx();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		
		return null;
	}
	
	private String validateAmountCN(PO po)
	{
		StringBuilder msg = new StringBuilder();
		MPaymentAllocate allocate = new MPaymentAllocate(po.getCtx(), po.get_ID(), po.get_TrxName());
		int ARCreditMemo = MDocType.getDocType(MDocType.DOCBASETYPE_ARCreditMemo);
		int APCreditMemo = MDocType.getDocType(MDocType.DOCBASETYPE_APCreditMemo);
		if(allocate.getC_Invoice().getC_DocTypeTarget_ID() != ARCreditMemo &&
				allocate.getC_Invoice().getC_DocTypeTarget_ID() != APCreditMemo)
			return null;
		
		BigDecimal openAmt = DB.getSQLValueBD(null, "SELECT COALESCE(invoiceopen(?,0),0)", allocate.getC_Invoice_ID());
		if(openAmt.compareTo(allocate.getAmount()) == 1)
			msg.append("Paid Amount greater than Open Amount CN Invoice");
		
		return msg.toString();
	}
}