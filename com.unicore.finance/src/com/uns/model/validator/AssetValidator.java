/**
 * 
 */
package com.uns.model.validator;

import java.util.Iterator;
import java.util.logging.Level;

import org.compiere.model.MAssetAddition;
import org.compiere.model.MClient;
import org.compiere.model.MDepreciationEntry;
import org.compiere.model.MDepreciationExp;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import com.unicore.model.MUNSAssetMovement;
import com.unicore.model.MUNSAssetMovementLine;

/**
 * @author nurse
 *
 */
public class AssetValidator implements ModelValidator 
{
	private static CLogger log = CLogger.getCLogger(AssetValidator.class);
	private int p_clientID = 0;

	/**
	 * 
	 */
	public AssetValidator() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#initialize(org.compiere.model.ModelValidationEngine, org.compiere.model.MClient)
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) 
	{
		if (client != null)
		{
			log.log(Level.INFO, client.toString());
			p_clientID = client.getAD_Client_ID();
		}
		
		engine.addModelChange(MDepreciationEntry.Table_Name, this);
		engine.addDocValidate(MAssetAddition.Table_Name, this);
		engine.addModelChange(MWarehouse.Table_Name, this);
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#getAD_Client_ID()
	 */
	@Override
	public int getAD_Client_ID() 
	{
		return p_clientID;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#login(int, int, int)
	 */
	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#modelChange(org.compiere.model.PO, int)
	 */
	@Override
	public String modelChange(PO po, int type) throws Exception 
	{
		if (MDepreciationEntry.Table_Name.equals(po.get_TableName()))
		{
			if (type == ModelValidator.TYPE_AFTER_NEW)
			{
				MDepreciationEntry entry = (MDepreciationEntry) po;
				Iterator<MDepreciationExp> iter = entry.getLinesIterator(true);
				while (iter.hasNext())
				{
					MDepreciationExp exp = iter.next();
					String sql = "SELECT l.DepartmentTo_ID FROM UNS_AssetMovementLine l "
							+ " INNER JOIN UNS_AssetMovement h ON h.UNS_AssetMovement_ID = "
							+ " l.UNS_AssetMovement_ID AND h.DocStatus IN ('CO','CL') AND h.MovementDate < ?"
							+ " WHERE l.Processed = ? AND l.IsActive = ? AND l.A_Asset_ID = ? "
							+ " ORDER BY h.MovementDate DESC";
					int secOfDeptID = DB.getSQLValue(po.get_TrxName(), sql, entry.getDateDoc(), "Y", "Y", exp.getA_Asset_ID());
					if (secOfDeptID > 0)
						exp.setC_BPartner_ID(secOfDeptID);
					else
					{
						sql = "SELECT C_BPartner_ID FROM A_Asset WHERE A_Asset_ID = ?";
						secOfDeptID = DB.getSQLValue(po.get_TrxName(), sql, exp.getA_Asset_ID());
						exp.setC_BPartner_ID(secOfDeptID);
					}
					
					if (!exp.save())
					{
						return CLogger.retrieveErrorString("Can't save deprectiation expense");
					}
				}
			}
		}
		else if (MWarehouse.Table_Name.equals(po.get_TableName()))
		{
			if (type == ModelValidator.TYPE_BEFORE_CHANGE)
			{
				if (po.is_ValueChanged(MWarehouse.COLUMNNAME_IsAssetWarehouse))
				{
					String sql = "SELECT CONCAT (Value, '-',Name) FROM A_Asset WHERE M_Locator_ID IN "
							+ " (SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = ?)";
					String val = DB.getSQLValueString(po.get_TrxName(), sql, po.get_ID());
					if (val != null)
						return "An asset inventory is available in this warehouse " + val;
				}
			}
			
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#docValidate(org.compiere.model.PO, int)
	 */
	@Override
	public String docValidate(PO po, int timing) 
	{
		String msg = null;
		if (MAssetAddition.Table_Name.equals(po.get_TableName()))
		{
			if (timing == ModelValidator.TIMING_BEFORE_PREPARE)
			{
				if (((MAssetAddition) po).getC_BPartner_ID() == 0)
					return "Business Partner (Sect of Dept) must be define";
			}
			if (timing == ModelValidator.TIMING_AFTER_COMPLETE)
				msg = assetAdditionCreateMovement((MAssetAddition)po);
		}
		return msg;
	}

	private String assetAdditionCreateMovement (MAssetAddition add)
	{
		MUNSAssetMovement move = new MUNSAssetMovement(add);
		try
		{
			move.saveEx();
			MUNSAssetMovementLine line = new MUNSAssetMovementLine(move);
			line.setA_Asset_Addition_ID(add.getA_Asset_Addition_ID());
			line.setA_Asset_ID(add.getA_Asset_ID());
			line.setDepartmentFrom_ID(add.getC_BPartner_ID());
			line.setDepartmentTo_ID(add.getC_BPartner_ID());
			line.setDescription(move.getDescription());
			line.setM_Locator_ID(add.getM_Locator_ID());
			line.setM_LocatorTo_ID(add.getM_Locator_ID());
			line.saveEx();
			if (!move.processIt(DocAction.ACTION_Complete))
				return move.getProcessMsg();
			move.saveEx();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		
		return null;
	}
}
