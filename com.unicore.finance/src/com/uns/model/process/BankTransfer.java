/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2008 e-Evolution,SC. All Rights Reserved.               *
 * Contributor(s): Victor Perez www.e-evolution.com                           *
 *****************************************************************************/
package com.uns.model.process;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankAccount;
import org.compiere.model.MDocType;
import org.compiere.model.MPayment;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.wf.MWorkflow;

import com.unicore.model.MUNSChequeList;
import com.unicore.model.process.UNSBankTransfer;
import com.uns.model.MUNSChequebook;
 
/**
 *  Bank Transfer. Generate two Payments entry
 *  
 *  For Bank Transfer From Bank Account "A" 
 *                 
 *	@author victor.perez@e-evoltuion.com
 *	
 **/
public class BankTransfer extends SvrProcess
{
	
	public BankTransfer ()
	{
		super ();
	}
	
	private String 		p_DocumentNo= "";				// Document No
	private String 		p_Description= "";				// Description
	private int 		p_C_BPartner_ID = 0;   			// Business Partner to be used as bridge
	private int			p_C_Currency_ID = 0;			// Payment Currency
	private int 		p_C_ConversionType_ID = 0;		// Payment Conversion Type
	private int			p_C_Charge_ID = 0;				// Charge to be used as bridge
	private String 		p_ChequeNo = null;			
	private BigDecimal 	p_APAmount = new BigDecimal(0); // Amount to be transfered between the accounts
	private BigDecimal	p_ARAmount = Env.ZERO;
	private int 		p_From_C_BankAccount_ID = 0;	// Bank Account From
	private int 		p_To_C_BankAccount_ID= 0;		// Bank Account To
	private Timestamp	p_StatementDate = null;  		// Date Statement
	private Timestamp	p_DateAcct = null;  			// Date Account
	private int			p_ChequeList_ID = 0;			// Cheque List
	private int			p_AD_Org_ID = 0;				// Organization
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("From_C_BankAccount_ID"))
				p_From_C_BankAccount_ID = para[i].getParameterAsInt();
			else if (name.equals("To_C_BankAccount_ID"))
				p_To_C_BankAccount_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("AD_Org_ID"))
				p_AD_Org_ID = para[i].getParameterAsInt();
//			else if (name.equals("C_Currency_ID"))
//				p_C_Currency_ID = para[i].getParameterAsInt();
			else if (name.equals("C_ConversionType_ID"))
				p_C_ConversionType_ID = para[i].getParameterAsInt();
			else if (name.equals("C_Charge_ID"))
				p_C_Charge_ID = para[i].getParameterAsInt();
			else if (name.equals("DocumentNo"))
				p_DocumentNo = (String)para[i].getParameter();
			else if (name.equals("APAmount"))
				p_APAmount = ((BigDecimal)para[i].getParameter());
			else if (name.equals("ARAmount"))
				p_ARAmount = ((BigDecimal)para[i].getParameter());
			else if (name.equals("Description"))
				p_Description = (String)para[i].getParameter();
			else if (name.equals("StatementDate"))
				p_StatementDate = (Timestamp)para[i].getParameter();
			else if (name.equals("DateAcct"))
				p_DateAcct = (Timestamp)para[i].getParameter();
			else if (name.equals("ChequeNo"))
				p_ChequeNo = (String)para[i].getParameter();
			else if (name.equals("UNS_Cheque_List_ID"))
				p_ChequeList_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
		if ((p_ChequeNo != null && !p_ChequeNo.isEmpty())
				|| p_ChequeList_ID > 0)
		{
			if(p_ChequeList_ID > 0)
			{
				MUNSChequeList chequeL = new MUNSChequeList(getCtx(), p_ChequeList_ID, get_TrxName());
				p_ChequeNo = chequeL.getName();
			}
			String msg = checkChequeNo();
			if (msg != null)
				throw new AdempiereUserError (msg);
		}
	}	//	prepare

	/**
	 *  Perform process.
	 *  @return Message (translated text)
	 *  @throws Exception if not successful
	 */
	protected String doIt() throws Exception
	{
		log.info("From Bank="+p_From_C_BankAccount_ID+" - To Bank="+p_To_C_BankAccount_ID
				+ " - C_BPartner_ID="+p_C_BPartner_ID+"- C_Charge_ID= "+p_C_Charge_ID+" - Amount="+p_APAmount+" - DocumentNo="+p_DocumentNo
				+ " - Description="+p_Description+ " - Statement Date="+p_StatementDate+
				" - Date Account="+p_DateAcct);
		
		if (p_To_C_BankAccount_ID == 0 || p_From_C_BankAccount_ID == 0)
			throw new IllegalArgumentException("Banks required");

//		if (p_DocumentNo == null || p_DocumentNo.length() == 0)
//			throw new IllegalArgumentException("Document No required");

		if (p_To_C_BankAccount_ID == p_From_C_BankAccount_ID)
			throw new AdempiereUserError ("Banks From and To must be different");
		
		if (p_C_BPartner_ID == 0)
			throw new AdempiereUserError ("Business Partner required");
		
//		if (p_C_Currency_ID == 0)
//			throw new AdempiereUserError ("Currency required");
//		
		if (p_C_Charge_ID == 0)
			throw new AdempiereUserError ("Business Partner required");
	
		if (p_APAmount.compareTo(new BigDecimal(0)) == 0)
			throw new AdempiereUserError ("Amount required");
		
		MBankAccount accountFrom = MBankAccount.get(getCtx(), p_From_C_BankAccount_ID);
		MBankAccount accountTo = MBankAccount.get(getCtx(), p_To_C_BankAccount_ID);
		if(accountFrom.getC_Currency_ID() != accountTo.getC_Currency_ID() && p_ARAmount.signum() <= 0)
			throw new AdempiereUserError("Difference Currency, AR Amount cannot be zero.");
		
		int fromDocType_ID = 0;
		if(accountFrom.getBankAccountType().equals(MBankAccount.BANKACCOUNTTYPE_Cash))
			fromDocType_ID = MDocType.getOfSubTypePay(MDocType.DOCSUBTYPEPAY_CashPaymentCP);
		else
			fromDocType_ID = MDocType.getOfSubTypePay(MDocType.DOCSUBTYPEPAY_OtherPaymentOP);
		
		int toDocType_ID = MDocType.getOfSubTypePay(MDocType.DOCSUBTYPEPAY_CashReceiptCR);

		//	Login Date
		if (p_StatementDate == null)
			p_StatementDate = Env.getContextAsDate(getCtx(), "#Date");
		if (p_StatementDate == null)
			p_StatementDate = new Timestamp(System.currentTimeMillis());			

		if (p_DateAcct == null)
			p_DateAcct = p_StatementDate;
		
		UNSBankTransfer bt = new UNSBankTransfer(getCtx(), p_DocumentNo, 
				p_Description, p_C_BPartner_ID, p_C_Currency_ID, 
				p_C_ConversionType_ID, p_C_Charge_ID, p_ChequeNo, 
				p_APAmount, p_ARAmount, p_From_C_BankAccount_ID, p_To_C_BankAccount_ID, 
				p_StatementDate, p_DateAcct, fromDocType_ID, toDocType_ID, p_AD_Org_ID, get_TrxName());

		return bt.doIt();

		//all transfer mechanism, use UNSBankTransfer class.
//		return generateBankTransfer();
	}	//	doIt
	

	/**
	 * 
	 * @return
	 */
	private String checkChequeNo()
	{
		String msg = null;
		String chequeNo = p_ChequeNo;
		
		MUNSChequebook cb = MUNSChequebook.isRegistered(getCtx(), get_TrxName(), chequeNo, p_From_C_BankAccount_ID);
		
		if ((cb == null))
		{
			return Msg.getMsg(getCtx(), "Check-No of " + p_ChequeNo +  " is not registered yet.");
		}
		
		String cekNum = MUNSChequebook.getProperChequeNumberFormat(chequeNo, cb);
		
		if (cb.isUsed(cekNum,-1,-1)) {
			msg = Msg.getMsg(getCtx(), "Check-No of " + p_ChequeNo +  " has been used.");
		}
		else {
			p_ChequeNo = cekNum;
		}
		return msg;
	}

	/**
	 * Generate BankTransfer()
	 *
	 */
	@SuppressWarnings("unused")
	private String generateBankTransfer()
	{
		MBankAccount mBankFrom = new MBankAccount(getCtx(),p_From_C_BankAccount_ID, get_TrxName());
		MBankAccount mBankTo = new MBankAccount(getCtx(),p_To_C_BankAccount_ID, get_TrxName());
		
		MPayment paymentBankFrom = new MPayment(getCtx(), 0 ,  get_TrxName());
		paymentBankFrom.setAD_Org_ID(mBankFrom.getAD_Org_ID());
		paymentBankFrom.setC_BankAccount_ID(mBankFrom.getC_BankAccount_ID());
		
		if(p_DocumentNo != null && !p_DocumentNo.isEmpty())
			paymentBankFrom.setDocumentNo(p_DocumentNo);
		
		paymentBankFrom.setDateAcct(p_DateAcct);
		paymentBankFrom.setDateTrx(p_StatementDate);
		if (p_ChequeNo != null && !p_ChequeNo.isEmpty())
		{
			paymentBankFrom.setTenderType(MPayment.TENDERTYPE_ChequeGiro);
			//paymentBankFrom.setCheckNo(p_ChequeNo);
			paymentBankFrom.set_ValueNoCheck(MPayment.COLUMNNAME_CheckNo, p_ChequeNo);
			paymentBankFrom.setDisbursementDate(p_StatementDate);
		}
		else 
			paymentBankFrom.setTenderType(MPayment.TENDERTYPE_Transfer);
		
		paymentBankFrom.setDescription(p_Description);
		paymentBankFrom.setC_BPartner_ID (p_C_BPartner_ID);
		paymentBankFrom.setC_Currency_ID(p_C_Currency_ID);
		
		if (p_C_ConversionType_ID > 0)
			paymentBankFrom.setC_ConversionType_ID(p_C_ConversionType_ID);	
		paymentBankFrom.setTotalAmt(p_APAmount);
		paymentBankFrom.setPayAmt(p_APAmount);
		paymentBankFrom.setOverUnderAmt(Env.ZERO);
		paymentBankFrom.setC_DocType_ID(false);
		paymentBankFrom.setC_Charge_ID(p_C_Charge_ID);
		paymentBankFrom.saveEx();
		
		try
		{
			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(
					paymentBankFrom, DocAction.ACTION_Complete);
			if (pi.isError())
			{
				throw new AdempiereException(
						"Error when traying to complete payment" 
								+ pi.getSummary());
			}
		}
		catch (Exception e)
		{
			throw new AdempiereException(
					"Error when traying to complete payment" 
							+ e.getMessage());
		}
		
		MPayment paymentBankTo = new MPayment(getCtx(), 0 ,  get_TrxName());
		paymentBankTo.setAD_Org_ID(mBankTo.getAD_Org_ID());
		paymentBankTo.setC_BankAccount_ID(mBankTo.getC_BankAccount_ID());

		if(p_DocumentNo != null && !p_DocumentNo.isEmpty())
			paymentBankTo.setDocumentNo(p_DocumentNo);
		
		paymentBankTo.setDateAcct(p_DateAcct);
		paymentBankTo.setDateTrx(p_StatementDate);
		paymentBankTo.setTenderType(MPayment.TENDERTYPE_Transfer);
		paymentBankTo.setDescription(p_Description);
		paymentBankTo.setC_BPartner_ID (p_C_BPartner_ID);
		paymentBankTo.setC_Currency_ID(p_C_Currency_ID);
		
		if (p_C_ConversionType_ID > 0)
			paymentBankFrom.setC_ConversionType_ID(p_C_ConversionType_ID);	
		paymentBankTo.setPayAmt(p_APAmount);
		paymentBankTo.setOverUnderAmt(Env.ZERO);
		paymentBankTo.setC_DocType_ID(true);
		paymentBankTo.setC_Charge_ID(p_C_Charge_ID);
		paymentBankTo.set_ValueOfColumn("Reference_ID", paymentBankFrom.get_ID());
		paymentBankTo.saveEx();
		paymentBankFrom.set_ValueOfColumn("Reference_ID", paymentBankTo.get_ID());
		paymentBankFrom.saveEx();
		try
		{
			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(
					paymentBankTo, DocAction.ACTION_Complete);
			if (pi.isError())
			{
				throw new AdempiereException(
						"Error when traying to complete payment" 
								+ pi.getSummary());
			}
		}
		catch (Exception e)
		{
			throw new AdempiereException(
					"Error when traying to complete payment" 
							+ e.getMessage());
		}
		
		//set transferfrom and transferto
		paymentBankFrom.setReference_ID(paymentBankTo.getC_Payment_ID());
		paymentBankFrom.saveEx();
		paymentBankTo.setReference_ID(paymentBankFrom.getC_Payment_ID());
		paymentBankTo.saveEx();
		
		return "2 Payment has been Created. " + paymentBankFrom.getDocumentInfo()
				+ " and " + paymentBankTo.getDocumentInfo();

	}  //  createCashLines
	
}	//	ImmediateBankTransfer
