package com.uns.model.process;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;

import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.DB;
import org.compiere.util.Env;


public class BalancingOpenAmt extends SvrProcess {
	
	private int m_org;
	private Timestamp m_datefrom;
	private Timestamp m_dateto;
	private BigDecimal m_amountfrom;
	private BigDecimal m_amountto;
	private ArrayList<MPayment> listid = new ArrayList<MPayment>();
	private final Timestamp m_date = new Timestamp(System.currentTimeMillis());
	private String m_msg;
	
	public BalancingOpenAmt() {
		
	}

	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for(int i = 0; i < params.length; i++){
			String nama = params[i].getParameterName();
			if(nama.equals("AD_Org_ID"))
				m_org=params[i].getParameterAsInt();
			else if(nama.equals("AmountFrom"))
				m_amountfrom=params[i].getParameterAsBigDecimal();
			else if(nama.equals("AmountTo"))
				m_amountto=params[i].getParameterAsBigDecimal();
			else if(nama.equals("DateFrom"))
				m_datefrom=params[i].getParameterAsTimestamp();
			else if(nama.equals("DateTo"))
				m_dateto=params[i].getParameterAsTimestamp();
			else
				log.log(Level.SEVERE, "Unknown parameter: "+nama);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		try
		{
			if(m_amountfrom.compareTo(m_amountto) > 0)
			{
				BigDecimal tmp_amountfrom = m_amountfrom;
				m_amountfrom = m_amountto;
				m_amountto = tmp_amountfrom;	
			}
			
			if(m_datefrom.after(m_dateto))
			{
				Timestamp tmp_datefrom = m_datefrom;
				m_datefrom = m_dateto;
				m_dateto = tmp_datefrom;
			}
			
			String stdate = m_date.toString();
			String dateTime = stdate.substring(0,10)+" 00:00:00";
			Timestamp datenow = Timestamp.valueOf(dateTime);
			
			String sql = "SELECT c_bpartner_id , description , c_invoice_id, InvoiceOpen(C_Invoice_ID, 0), c_payment_id "
					+ "FROM C_Invoice WHERE InvoiceOpen(C_Invoice_ID, 0) "
					+ "BETWEEN ? AND ? AND InvoiceOpen(C_Invoice_ID, 0) <> 0 AND dateinvoiced BETWEEN ? AND ? "
					+ "AND DocStatus IN ('CO','CL')";
			PreparedStatement stat = DB.prepareStatement(sql, get_TrxName());
			stat.setBigDecimal(1, m_amountfrom);
			stat.setBigDecimal(2, m_amountto);
			stat.setTimestamp(3, m_datefrom);
			stat.setTimestamp(4, m_dateto);
			ResultSet rs = stat.executeQuery();
			
			
			while(rs.next()){
				MPayment payment = new MPayment(getCtx(), 0, get_TrxName());
				MPaymentAllocate alocate = new MPaymentAllocate(getCtx(), 0, get_TrxName());
				
				String sq = "SELECT c_payment_id FROM c_payment WHERE description = 'Automatic Balancing Amount' "
						+ "AND c_bpartner_id = ? AND datetrx = ?";
				int c_payment_id = DB.getSQLValueEx(get_TrxName(), sq, rs.getInt(1), datenow);
				
				if(c_payment_id <= 0)
				{
					payment.setAD_Org_ID(m_org);
					payment.setC_BankAccount_ID(1000069);
					payment.setDescription("Automatic Balancing Amount");
					payment.setC_BPartner_ID(rs.getInt(1));
					payment.setTenderType("A");
					payment.setC_DocType_ID(1000361);
					payment.setDateTrx(datenow);
					payment.setDateAcct(datenow);
					payment.setC_Currency_ID(303);
					payment.saveEx();
					c_payment_id = payment.getC_Payment_ID();
					listid.add(new MPayment(getCtx(), c_payment_id, get_TrxName()));
				}
				
				alocate.setAD_Org_ID(m_org);
				alocate.setC_Payment_ID(c_payment_id);
				alocate.setC_Invoice_ID(rs.getInt(3));
				alocate.setWriteOffAmt(rs.getBigDecimal(4));
				alocate.setInvoiceAmt(rs.getBigDecimal(4));
				alocate.setAmount(Env.ZERO);
				alocate.setDiscountAmt(Env.ZERO);
				alocate.setWithholdingAmt(Env.ZERO);
				alocate.setPayToOverUnderAmount(Env.ZERO);
				alocate.setOverUnderAmt(Env.ZERO);
				alocate.saveEx();
				m_msg = "You Did it";
			}
			
			
			for(MPayment list : listid)
			{
				
				list.processIt(DocAction.ACTION_Complete);					
				list.saveEx();				
				
				
			}
		}
		catch (Exception e)
		{
			throw new AdempiereSystemError("Gagal vroh", e);
			
		}
		
		return m_msg;
	}

}
