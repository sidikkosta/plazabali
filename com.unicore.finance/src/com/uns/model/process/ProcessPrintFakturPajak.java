/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceTax;
import org.compiere.process.DocAction;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSBillingGroup;
import com.unicore.model.MUNSVATDocInOut;
import com.unicore.model.MUNSVATDocNo;
import com.uns.base.model.Query;

/**
 * @author ALBURHANY
 *
 */
public class ProcessPrintFakturPajak extends SvrProcess {
	
	private MUNSVATDocNo m_VatDocNo = null;
	private List<MInvoice> invList = null;
	private String whereClause = null;
	/**
	 * 
	 */
	public ProcessPrintFakturPajak() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		createVATDocNo();
		
		return null;
	}
	
	private int newVAT(int AD_Org_ID)
	{
		StringBuffer sql = new StringBuffer
				("SELECT UNS_VATDocNo_ID FROM UNS_VATDocNo WHERE UsageStatus <> 'U'"
						+ " AND UNS_VATDocNo_ID NOT IN (SELECT UNS_VATDocNo_ID FROM UNS_VATDocInOut)"
						+ " AND AD_Org_ID = " + AD_Org_ID + " AND UNS_VATRegisteredSequences_ID ="
						+ " (SELECT UNS_VATRegisteredSequences_ID FROM UNS_VATRegisteredSequences ORDER BY DateReceived DESC)"
						+ " ORDER BY SequenceUsedNo");
		int idVAT = DB.getSQLValue(get_TrxName(), sql.toString());
		
		if(idVAT <= 0)
			throw new AdempiereException("No VAT avaliable for use");
		
		return idVAT;
	}
	
	private int oldVAT(int C_Invoice_ID)
	{
		String sql = "SELECT UNS_VATDocNo_ID FROM C_InvoiceTax WHERE C_Invoice_ID = " + C_Invoice_ID;
		int idVat = DB.getSQLValue(get_TrxName(), sql);
		
		return idVat;
	}
	
	private String createVATDocNo() throws Exception
	{
		boolean isIndependent = true;
		
		if(getTable_ID() == MUNSBillingGroup.Table_ID)
			isIndependent = false;
		
		if(!isIndependent)
		{			
			whereClause = "C_Invoice_ID IN (SELECT C_Invoice_ID FROM UNS_BillingLine"
									+ " WHERE UNS_Billing_ID IN (SELECT UNS_Billing_ID FROM UNS_Billing"
										+ " WHERE isActive = 'Y' AND UNS_BillingGroup_ID =?))";
		}
		
		if(isIndependent)
		{	
			whereClause = "C_Invoice_ID=?";
		}
		
			invList = new Query(getCtx(), MInvoice.Table_Name, whereClause, get_TrxName()).setParameters(getRecord_ID())
						.list();
		for (MInvoice inv : invList)
		{	
			MInvoiceTax invTax = MInvoiceTax.getByInvoice(getCtx(), inv.get_ID(), get_TrxName());
			
			if(invTax.getTaxAmt().compareTo(Env.ZERO) == 0 || invTax == null)
				if(isIndependent)
					throw new AdempiereException("Cannot print Document VAT, this document is not Tax");
				if(!isIndependent)
					continue;
	
			String sql = "SELECT C_Invoice_ID FROM UNS_VATDocInOut WHERE isSOTrx = 'Y' AND C_Invoice_ID = " + inv.get_ID();
			
			if(DB.getSQLValue(get_TrxName(), sql) >= 1)
				continue;
			
			MUNSVATDocInOut docVAT = new MUNSVATDocInOut(getCtx(), 0, get_TrxName());
			java.util.Date date = new java.util.Date();
			inv.setPrintedBy(getProcessInfo().getAD_User_ID());
			inv.setIsPrinted(true);
			inv.setDatePrinted(new Timestamp(date.getTime()));
			inv.saveEx();
				
			docVAT.setisReplacement(true);
			docVAT.setAD_Org_ID(inv.getAD_Org_ID());
			docVAT.setC_Invoice_ID(inv.getC_Invoice_ID());
			docVAT.setC_BPartner_ID(inv.getC_BPartner_ID());
			docVAT.setPrintedBy(inv.getPrintedBy());
			docVAT.setPrintedDate(inv.getDatePrinted());
			docVAT.setIsPrinted(true);
			docVAT.setIsSOTrx(true);
			
			if(inv.getM_RMA_ID() != 0)
			{		
				String sqlSO = "SELECT C_Invoice_ID FROM C_Invoice WHERE C_Order_ID = (SELECT C_Order_ID FROM M_InOut"
						+ " WHERE M_InOut_ID = (SELECT M_InOut FROM M_RMA WHERE M_RMA_ID = " + inv.getM_RMA_ID() + "))";
				int idOldInv = DB.getSQLValue(get_TrxName(), sqlSO);
				
				docVAT.setUNS_VATDocNo_ID(oldVAT(idOldInv));
				docVAT.processIt(DocAction.ACTION_Complete);
				
				m_VatDocNo = new MUNSVATDocNo(getCtx(),oldVAT(inv.get_ID()), get_TrxName());
			}
			
			if(inv.getM_RMA_ID() == 0)
			{
				docVAT.setUNS_VATDocNo_ID(newVAT(inv.getAD_Org_ID()));
				docVAT.processIt(DocAction.ACTION_Complete);
				
				m_VatDocNo = new MUNSVATDocNo(getCtx(), newVAT(inv.getAD_Org_ID()), get_TrxName());
			}
			
			m_VatDocNo.saveEx();
			docVAT.saveEx();
			
			invTax.setUNS_VATDocNo_ID(m_VatDocNo.get_ID());
			invTax.saveEx();
		}
		return null;
	}
}