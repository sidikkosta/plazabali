/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.report.MReportLine;
import org.compiere.util.DB;
import com.uns.model.MReportLineColumns;

/**
 * @author nurse
 *
 */
public class RLineSetReseqProcessor extends SvrProcess 
{

	/**
	 * 
	 */
	public RLineSetReseqProcessor() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("From".equals(params[i].getParameterName()))
				m_fromStr = params[i].getParameterAsString();
			else if ("To".equals(params[i].getParameterName()))
				m_to = params[i].getParameterAsInt();
			else if ("PA_ReportLineSet_ID".equals(params[i].getParameterName()))
				p_rLineSetID = params[i].getParameterAsInt();
			else if ("IsMove".equals(params[i].getParameterName()))
				p_isMove = params[i].getParameterAsBoolean();
		}
	}

	private String m_fromStr = null;
	private int m_to;
	private int p_rLineSetID = 0;
	private boolean p_isMove = false;
	private final String m_tmpDesc = "**DU**";
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (m_fromStr == null)
			throw new AdempiereException("Mandatory parameter Sequence From");
		if (p_isMove)
			move();
		else
			resequence();
		return null;
	}
	
	private void resequence ()
	{
		String sql = "UPDATE PA_ReportLine SET SeqNo = (SeqNo + ?) WHERE SeqNo >= ? AND PA_ReportLineSet_ID = ?";
		int from = 0;
		try
		{
			from = Integer.valueOf(m_fromStr);
		}
		catch (NumberFormatException ex)
		{
			throw new AdempiereException(ex);
		}
		int add = m_to - from;
		int ok = DB.executeUpdate(sql, new Object[]{add, from, p_rLineSetID}, false, get_TrxName());
		if (ok == -1)
			throw new AdempiereException();
		updateExcludedCols();
	}
	
	private void move ()
	{
		List<Integer> tobeReseqs = parseToListNumber(m_fromStr, ";");
		if (tobeReseqs.size() == 0)
			return;
		Collections.sort(tobeReseqs);
		Set<Integer> set = new HashSet<>(tobeReseqs);
		int newSeq = m_to;
		String sql = "UPDATE PA_ReportLine SET SeqNo = ?, Description = CONCAT(?,Description) WHERE SeqNo = ? AND PA_ReportLineSet_ID = ?";
		Iterator<Integer> iter = set.iterator();
		while (iter.hasNext())
		{
			int retVal = DB.executeUpdate(sql, new Object[]{newSeq++,m_tmpDesc, iter.next(), p_rLineSetID}, false, get_TrxName());
			if (retVal == -1)
				throw new AdempiereException();
		}
		int add = set.size() * 10;
		updateFinish(add);
	}
	
	private void updateFinish (int add)
	{
		String like = m_tmpDesc+ "%";
		String sql = "UPDATE PA_ReportLine SET SeqNo = (SeqNo + ?) WHERE PA_ReportLineSet_ID = ? AND (Description IS NULL OR Description NOT LIKE ? )AND SeqNo >= ?";
		int succ = DB.executeUpdate(sql, new Object[]{add, p_rLineSetID, like, m_to}, false, get_TrxName());
		if (succ == -1)
			throw new AdempiereException();
		sql = "UPDATE PA_ReportLine SET Description = REPLACE (Description, ?, '') WHERE PA_ReportLineSet_ID = ? AND Description LIKE ?";
		succ = DB.executeUpdate(sql, new Object[]{m_tmpDesc, p_rLineSetID, like}, false, get_TrxName());
		if (succ == -1)
			throw new AdempiereException();
		
		updateExcludedCols();
	}
	
	private void updateExcludedCols ()
	{
		List<MReportLine> rLines = new Query(
				getCtx(), MReportLine.Table_Name, "PA_ReportLineSet_ID = ? AND IsSummary = ?", 
				get_TrxName()).setParameters(p_rLineSetID, "Y").list();
		String sql = "SELECT SeqNo FROM PA_ReportLine WHERE PA_ReportLine_ID = ?";
		for (int i=0; i<rLines.size(); i++)
		{
			int startSeq = DB.getSQLValue(get_TrxName(), sql, rLines.get(i).getOper_1_ID());
			int endSeq = DB.getSQLValue(get_TrxName(), sql, rLines.get(i).getOper_2_ID());
			if (startSeq == 0 || endSeq == 0)
				continue;
			String __sql = "SELECT ARRAY_TO_STRING (ARRAY_AGG(SeqNo), ',') FROM PA_ReportLine "
					+ " WHERE SeqNo BETWEEN ? AND ? AND PA_ReportLineSet_ID = ? AND IsSummary = ?";
			String excluded = DB.getSQLValueString(get_TrxName(), __sql, startSeq, endSeq, p_rLineSetID, "y");
			MReportLineColumns columns = new Query(
					getCtx(), MReportLineColumns.Table_Name, 
					"PA_ReportLine_ID =? AND PA_ReportColumn_ID IS NULL", 
					get_TrxName()).setParameters(rLines.get(i).get_ID()).first();
			if (columns == null)
			{
				columns = createReportLineCols(rLines.get(i), -1);
				if (!columns.save())
					throw new AdempiereException();
			}
			columns.setOnlyExcludeLinesOf(excluded);
			if (!columns.save())
				throw new AdempiereException();			
		}
	}

	private MReportLineColumns createReportLineCols (MReportLine line, int reportColID)
	{
		MReportLineColumns col = new MReportLineColumns(getCtx(), 0, get_TrxName());
		col.setExcludedFromCalculation(true);
		col.setPA_ReportLine_ID(line.get_ID());
		col.setPA_ReportColumn_ID(reportColID);
		return col;
	}
	
	private List<Integer> parseToListNumber (String text, String split)
	{
		List<Integer> retVal = new ArrayList<>();
		String[] raws = text.split(split);
		
		for (int i=0; i<raws.length; i++)
		{
			String raw = raws[i];
			if (raw.contains("-"))
			{
				List<Integer> between = parseToListNumber(raw, "-");
				int from = 0;
				int to = 0;
				if (between.size() > 0)
					from = between.get(0);
				if (between.size() > 1)
				{
					to = between.get(1);
				}
				else
					continue;
				
				if (between.size() != 2)
					continue;
				retVal.addAll(getSequenceBetween(from, to));
				continue;
			}
			try 
			{
				int val = Integer.valueOf(raw);
				retVal.add(val);
			}
			catch (NumberFormatException ex) 
			{
				ex.printStackTrace();
			}
		}
		
		return retVal;
	}
	
	private List<Integer> getSequenceBetween (int from, int to)
	{
		String sql = "SELECT SeqNo FROM PA_ReportLine WHERE PA_ReportLineSet_ID = ?";
		List<Object> params = new ArrayList<>();
		params.add(p_rLineSetID);
		if (from > 0 && to > 0)
		{
			sql += " AND SeqNo BETWEEN ? AND ?";
			params.add(from);
			params.add(to);
		}
		else if (from > 0)
		{
			sql += " AND SeqNo >= ?";
			params.add(from);
		}
		else if (to > 0)
		{
			sql += " AND SeqNo <= ?";
			params.add(to);
		}
		List<Integer> sequences = new ArrayList<>();
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, p_rLineSetID);
			rs = st.executeQuery();
			while (rs.next())
				sequences.add(rs.getInt(1));
		}
		catch (SQLException ex)
		{
			throw new AdempiereException(ex);
		}
		
		return sequences;
	}

}
