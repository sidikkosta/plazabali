/**
 * 
 */
package com.uns.model.process;

import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MElementValue;
import org.compiere.model.MTree_Base;
import org.compiere.model.MTree_Node;
import org.compiere.model.Query;
import org.compiere.model.X_AD_TreeNode;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.report.MReportColumn;
import org.compiere.report.MReportColumnSet;
import org.compiere.report.MReportLine;
import org.compiere.report.MReportSource;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;

import com.uns.model.MReportLineColumns;
import com.uns.model.MReportLineSummary;

/**
 * @author nurse
 *
 */
public class TrialBalanceGenerator extends SvrProcess {

	/**
	 * 
	 */
	public TrialBalanceGenerator() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("C_ElementValue_ID".equals(params[i].getParameterName()))
				p_elementValueID = params[i].getParameterAsInt();
			else if ("StartSequence".equals(params[i].getParameterName()))
				p_startSequence = params[i].getParameterAsInt();
			else if ("IsDebit".equals(params[i].getParameterName()))
				p_isDebit = params[i].getParameterAsBoolean();
		}
	}

	private int p_elementValueID = -1;
	private int p_startSequence = 0;
	private boolean p_isDebit = false;
	private final String m_tmpDesc = "**DU**";
	private List<MReportColumn> m_cols = null;
	private int m_sequence = 0; 
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (p_elementValueID == -1)
			throw new AdempiereUserError("Mandatory parameter Account Element");
		String sql = "";//"DELETE FROM PA_ReportLine WHERE PA_ReportLineSummary_ID = ?";
//		int result = DB.executeUpdate(sql, getRecord_ID(), false, get_TrxName());
//		if (result == -1)
//			throw new AdempiereException();
		MReportLineSummary summary = new MReportLineSummary(getCtx(), getRecord_ID(), get_TrxName());
		MTree_Base tree = new Query(getCtx(), MTree_Base.Table_Name, 
				" AD_Tree_ID = (SELECT AD_Tree_ID FROM C_Element WHERE "
				+ " C_Element_ID = (SELECT C_Element_ID FROM C_ElementValue "
				+ " WHERE C_ElementValue_ID = ?))", get_TrxName()).
				setParameters(p_elementValueID).first();
		
		if (p_startSequence == 0) 
		{
			sql = "SELECT COALESCE(MAX(SeqNo),0) FROM PA_ReportLine WHERE PA_ReportLineSet_ID = ?";
			p_startSequence = DB.getSQLValue(get_TrxName(), sql, 
					summary.getPA_ReportLineSet_ID());
		}
		
		m_cols = new Query(getCtx(), MReportColumn.Table_Name, 
				MReportColumnSet.Table_Name + "_ID = ?", get_TrxName()).
				setParameters(summary.getPA_ReportColumnSet_ID()).setOrderBy("SeqNo").list();
		m_sequence = p_startSequence;
		if (m_sequence== 0)
			m_sequence = 10;
		processAccount(null, tree, summary, p_elementValueID, 0);
		
		if (p_startSequence > 0)
		{
			String like = m_tmpDesc+ "%";
			int add = m_sequence - p_startSequence + 10;
			sql = "UPDATE PA_ReportLine SET SeqNo = (SeqNo + ?) WHERE PA_ReportLineSet_ID = ? AND (Description IS NULL OR Description NOT LIKE ? )AND SeqNo >= ?";
			int succ = DB.executeUpdate(sql, new Object[]{add, summary.getPA_ReportLineSet_ID(), like, p_startSequence}, false, get_TrxName());
			if (succ == -1)
				throw new AdempiereException();
			sql = "UPDATE PA_ReportLine SET Description = REPLACE (Description, ?, '') WHERE PA_ReportLineSet_ID = ? AND Description LIKE ?";
			succ = DB.executeUpdate(sql, new Object[]{m_tmpDesc, summary.getPA_ReportLineSet_ID(), like}, false, get_TrxName());
			if (succ == -1)
				throw new AdempiereException();
			
		}
		return null;
	}
	
	private String  processAccount (MReportLine parent, MTree_Base tree, 
			MReportLineSummary summary, int accountID, int space)
	{
		String excluded = "";
		MElementValue eVal = new MElementValue(getCtx(), accountID, get_TrxName());
		MTree_Node node = MTree_Node.get(tree, eVal.get_ID());
		MReportLine line = createReportLine(summary, eVal, space);
		if (line == null)
			throw new AdempiereException();
		List<X_AD_TreeNode> childs = new Query(getCtx(), X_AD_TreeNode.Table_Name, 
				"Parent_ID = ? AND AD_TreeNode.IsActive = ? AND b.IsActive = ?",
				get_TrxName()).setParameters(node.getNode_ID(), "Y","Y").
				addJoinClause("INNER JOIN C_ElementValue b ON b.C_ElementValue_ID = AD_TreeNode.Node_ID").
				setOrderBy("b.Value").
				list();
		space += 2;
		for (int i=0; i<childs.size(); i++)
		{
			String childEx = processAccount(line, tree, summary, 
					childs.get(i).getNode_ID(), space);
			if (childEx.length() > 0)
			{
				if (excluded.length() > 0)
					excluded += ",";
				excluded += childEx;
			}
		}
		if (parent != null )
		{
			if (parent.getOper_1_ID() == 0)
				parent.setOper_1_ID(line.get_ID());
			int oper2ID = line.getOper_2_ID();
			if (oper2ID == 0)
				oper2ID = line.get_ID();
			parent.setOper_2_ID(oper2ID);
			if (!parent.save())
				throw new AdempiereException();
		}
		
		if (excluded.length() > 0)
		{ 
			MReportLineColumns columns = new Query(
					getCtx(), MReportLineColumns.Table_Name, 
					"PA_ReportLine_ID =? AND PA_ReportColumn_ID IS NULL", 
					get_TrxName()).setParameters(line.get_ID()).first();
			if (columns == null)
			{
				columns = createReportLineCols(line, -1);
				if (!columns.save())
					throw new AdempiereException();
			}
			columns.setOnlyExcludeLinesOf(excluded);
			if (!columns.save())
				throw new AdempiereException();
		}
		
		if (childs.size() > 0)
		{
			if (excluded.length() > 0)
				excluded += ",";
			excluded += line.getSeqNo();
		}
		return excluded;
	}
	
	private MReportLine createReportLine (MReportLineSummary summary, MElementValue eVal, int space)
	{
		
		String name = "";
		for (int i=0; i<space; i++)
		{
			name += " ";
		}
		name += eVal.getValue() + " " + eVal.getName();
		MReportLine	line = new MReportLine(getCtx(), 0, get_TrxName());
		line.setAD_Org_ID(0);
		line.setPA_ReportLineSet_ID(summary.getPA_ReportLineSet_ID());
		line.setPA_ReportLineSummary_ID(summary.get_ID());
		line.setName(name);
		String desc = "";
		if (p_startSequence > 0)
			desc += m_tmpDesc;
		if (eVal.getDescription() != null)
			desc += eVal.getDescription();
		line.setDescription(desc);
		line.setIsPrinted(true);
		line.setSeqNo(m_sequence);
		line.setUseSpecificColumns(true);
		if (eVal.isSummary())
		{
			line.setLineType(MReportLine.LINETYPE_Calculation);
			line.setCalculationType(MReportLine.CALCULATIONTYPE_AddRangeOp1ToOp2);
			line.setIsLineSubTotal("B");
		}
		else
		{
			line.setLineType(MReportLine.LINETYPE_SegmentValue);
			line.setPostingType(MReportLine.POSTINGTYPE_Actual);
			line.setIsLineSubTotal("N");
			line.setPAAmountType(MReportLine.PAAMOUNTTYPE_BalanceAccountedSign);
		}
		line.setDebugMode(true);
		if (!line.save())
			throw new AdempiereException();
		
		for (int i=0; i<m_cols.size(); i++)
		{
			if (MReportColumn.COLUMNTYPE_RelativePeriod.equals(m_cols.get(i).getColumnType()))
			{
				if (eVal.isSummary())
					continue;
				else if (p_isDebit && i == 0)
					continue;
				else if (!p_isDebit && i == 1)
					continue;
			}
			else if (MReportColumn.COLUMNTYPE_Calculation.equals(m_cols.get(i).getColumnType()))
			{
				if (!eVal.isSummary())
					continue;
			}
			else
			{
				continue;
			}				
			
			MReportLineColumns col = createReportLineCols(line,m_cols.get(i).get_ID());
			if (i==0 || i == 1)
				col.setPASpecificColumnType(MReportLineColumns.PASPECIFICCOLUMNTYPE_SetToZeroValue);
			else if (i == 2)
			{
				col.setPASpecificColumnType(MReportLineColumns.PASPECIFICCOLUMNTYPE_Natural);
				col.setPAAmountType(MReportLineColumns.PAAMOUNTTYPE_DebitOnly);
			}
			else if (i == 3)
			{
				col.setPASpecificColumnType(MReportLineColumns.PASPECIFICCOLUMNTYPE_Natural);
				col.setPAAmountType(MReportLineColumns.PAAMOUNTTYPE_CreditOnly);
			}
			if (!col.save())
				throw new AdempiereException();
		}
		
		if (!eVal.isSummary())
		{
			MReportSource source = new MReportSource(getCtx(), 0, get_TrxName());
			source.setC_ElementValue_ID(eVal.get_ID());
			source.setDescription(eVal.getDescription());
			source.setName(name);
			source.setPA_ReportLine_ID(line.get_ID());
//			source.setDisplayName1(name);
//			source.setDisplayName2(name);
//			source.setDisplayName3(name);
			source.setElementType(MReportSource.ELEMENTTYPE_Account);
			if (!source.save())
				throw new AdempiereException();
		}
		
		m_sequence += 10;
		return line;
	}
	
	private MReportLineColumns createReportLineCols (MReportLine line, int reportColID)
	{
		MReportLineColumns col = new MReportLineColumns(getCtx(), 0, get_TrxName());
		col.setExcludedFromCalculation(true);
		col.setPA_ReportLine_ID(line.get_ID());
		col.setPA_ReportColumn_ID(reportColID);
		return col;
	}
}
