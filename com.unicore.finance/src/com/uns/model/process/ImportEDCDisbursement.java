/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import com.unicore.model.MUNSImportSimpleColumn;
import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;
import com.uns.model.MUNSDisbursReconLine;
import com.uns.model.MUNSDisbursementRecon;
import com.uns.model.process.SimpleImportXLS;
import com.uns.util.MessageBox;

/**
 * @author Burhani Adam
 *
 */
public class ImportEDCDisbursement extends SvrProcess {

	/**
	 * 
	 */
	public ImportEDCDisbursement() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{	
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window EDC Disbursement");

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSDisbursementRecon recon = new MUNSDisbursementRecon(getCtx(), getRecord_ID(), get_TrxName());
		if(recon.isProcessed())
			throw new AdempiereException("Document has processed.");
		if(recon.getImportLines().equals("Y"))
		{
			int answer = MessageBox.showMsg(getCtx(), getProcessInfo(), "Re-Import file ?", "Confirmation",
					MessageBox.YESNO, MessageBox.ICONQUESTION);
			if(answer == MessageBox.RETURN_YES || MessageBox.RETURN_OK == answer)
			{
				answer = MessageBox.showMsg(getCtx(), getProcessInfo(), "Delete existing record ?", "Confirmation",
						MessageBox.YESNO, MessageBox.ICONQUESTION);
				if(answer == MessageBox.RETURN_YES || MessageBox.RETURN_OK == answer)
				{
					if(!recon.deleteExistingRecord())
						throw new AdempiereException("Failed when trying delete existing record.");
				}
			}
			else
				return "Cancelled.";
		}
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportEDCDisbursement'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with ImportEDCDisbursement name");
		
		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(recon.getFile_Directory());
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length > 1)
			throw new AdempiereException("more than one lines in Import Simple XLS " + simple.getName());
		
		MUNSImportSimpleTable table = tables[0];
		MUNSImportSimpleColumn[] columns = table.getLines(true);
		MUNSImportSimpleColumn column = null;
		for(int i=0;i<columns.length;i++)
		{
			if(columns[i].getAD_Column_ID() > 0 && columns[i].getAD_Column().getColumnName().equals("UNS_DisbursementRecon_ID"))
			{
				column = columns[i];
				break;
			}
		}
		
		if(column == null)
		{
			column = new MUNSImportSimpleColumn(getCtx(), 0, get_TrxName());
			column.setUNS_ImportSimpleTable_ID(table.get_ID());
			column.setName("UNT");
			column.setAD_Column_ID(MColumn.getColumn_ID(MUNSDisbursReconLine.Table_Name, "UNS_DisbursementRecon_ID"));
			column.setColumnNo(6969);
			column.setAD_Reference_ID(11);
			column.setIsEmptyCell(true);
		}
		
		column.setDefaultValue(String.valueOf(recon.get_ID()));
		column.saveEx();
		table.getLines(true);
		
		SimpleImportXLS si = new SimpleImportXLS(getCtx(), simple, table, false, 0, 0, 0, getAD_Client_ID(), get_TrxName());;
		try{
			 si.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		
		column.deleteEx(true);
		
		recon.setImportLines("Y");
		recon.saveEx();
		
		return si.getTotalRow() + " Records. " + si.getImportedRow() + " has created, " + si.getNotImportedRow() + " not created.";
	}
}