/**
 * 
 */
package com.uns.model.process;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;

import com.uns.model.MUNSChequeReconciliation;
import com.uns.model.MUNSChequebook;

/**
 * @author AzHaidar
 *
 */
public class VoidChequeNo extends SvrProcess {

	private String m_ChequeNo = null;
	private String m_Reason = null;
	private int m_chequeBook_ID = 0;
	
	/**
	 * 
	 */
	public VoidChequeNo() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] param = getParameter();
		for (int i=0; i<param.length; i++)
		{
			String paramName = param[i].getParameterName();
			if (paramName.equals("ChequeNo")) {
				m_ChequeNo = (String) param[i].getParameter();
			}
			else if (paramName.equals("Reason")){
				m_Reason = (String) param[i].getParameter();
			}
		}
		m_chequeBook_ID = getRecord_ID();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		String msg = null;
		
		MUNSChequebook cb = new MUNSChequebook(getCtx(), getRecord_ID(), get_TrxName());
		
		cb = (m_ChequeNo == null || "".equals(m_ChequeNo))? null :  
			MUNSChequebook.isRegistered(getCtx(), get_TrxName(), m_ChequeNo, cb.getC_BankAccount_ID()); 
		
		if ((cb == null))
		{
			return Msg.getMsg(getCtx(), "ChequeNotRegistered");
		}
		
		String properNum = MUNSChequebook.getProperChequeNumberFormat(m_ChequeNo, cb);
		
		if (cb.isUsed(properNum, -1, -1)) {
			msg = Msg.getMsg(getCtx(), "ChequeHasBeenUsed");
		}
		else {//if (type == TYPE_BEFORE_CHANGE || type == TYPE_AFTER_NEW){
			//po.set_ValueOfColumn(MPayment.COLUMNNAME_CheckNo, cekNum);
			MUNSChequeReconciliation cr = new MUNSChequeReconciliation(getCtx(), 0, get_TrxName());
			
			cr.setUNS_Chequebook_ID(m_chequeBook_ID);
			cr.setChequeNo(properNum);
			cr.setDescription(m_Reason);
			if (!cr.save())
				msg = "Failed when voiding a cheque no.";
		}
		
		
		return msg;
	}

}
