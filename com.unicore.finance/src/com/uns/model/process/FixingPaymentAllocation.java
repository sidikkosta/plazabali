/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAllocationHdr;
import org.compiere.model.MAllocationLine;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * @author AzHaidar
 *
 */
public class FixingPaymentAllocation extends SvrProcess 
{

	private static CLogger log = CLogger.getCLogger(FixingPaymentAllocation.class);
	
	private int 		m_DocType_ID;
	private Timestamp 	m_dateFrom;
	private Timestamp 	m_dateTo;
	
	/**
	 * 
	 */
	public FixingPaymentAllocation() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] param = getParameter();
		for (int i=0; i<param.length; i++)
		{
			String paramName = param[i].getParameterName();
			if (paramName.equals("DateFrom")) {
				m_dateFrom = param[i].getParameterAsTimestamp();
			}
			else if (paramName.equals("DateTo")) {
				m_dateTo = param[i].getParameterAsTimestamp();
			}
			else if (paramName.equals("C_DocType_ID")) {
				m_DocType_ID = param[i].getParameterAsInt();
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		StringBuilder whereClause = new StringBuilder("C_DocType_ID=? AND DateTrx BETWEEN ? AND ? ")
			.append("AND DocStatus IN ('CO', 'CL', 'IP', 'IN', 'DR')");
		
		List<MPayment> paymentList = 
				new Query(getCtx(), MPayment.Table_Name, whereClause.toString(), get_TrxName())
				.setParameters(m_DocType_ID, m_dateFrom, m_dateTo)
				.setOnlyActiveRecords(true)
				.setOrderBy(MPayment.COLUMNNAME_DateTrx + ", " + MPayment.COLUMNNAME_DocumentNo)
				.list();
		
		processUI.statusUpdate("Recompleting total #" + paymentList.size() + " of Payment.");
		
		for (MPayment payment : paymentList)
		{
			log.info("Recompleting Payment No : " + payment.getDocumentNo());
			String errMsg = reCompletePayment(payment);
			if (errMsg != null) {
				return errMsg;
			}
		}
		
		return "#" + paymentList.size() + " of Payments were recompleted.";
	}

	private String reCompletePayment(MPayment payment)
	{
		processUI.statusUpdate("Recompleting Payment No #" + payment.getDocumentNo() + " of date trx #" + payment.getDateTrx());
		MPaymentAllocate paList[] = MPaymentAllocate.get(payment);
		
		if (paList.length == 0)
			return null;
		
		payment.setDocStatus(DocAction.STATUS_Drafted);
		payment.setDocAction(DocAction.ACTION_Complete);
		payment.setProcessed(false);
		payment.disableModelValidation();
		payment.saveEx();
		
		MAllocationHdr alloc = 
				new Query(getCtx(), MAllocationHdr.Table_Name, 
						"C_AllocationHdr_ID = (SELECT DISTINCT (C_AllocationHdr_id) from C_AllocationLine WHERE C_Payment_ID=?)", 
						get_TrxName())
				.setParameters(payment.get_ID()).first();
		
		if (alloc == null)
		{
			alloc = new MAllocationHdr(getCtx(), false, 
				payment.getDateTrx(), payment.getC_Currency_ID(), 
					Msg.translate(getCtx(), "C_Payment_ID")	+ ": " + payment.getDocumentNo(), 
					payment.get_TrxName());
			alloc.setAD_Org_ID(payment.getAD_Org_ID());
			alloc.setDateAcct(payment.getDateAcct());
			alloc.disableModelValidation();
			if (!alloc.save()) {
				return "Payment Allocation header not created.";
			}
		}
			
		MBPartner bpartner = new MBPartner (getCtx(), payment.getC_BPartner_ID(), payment.get_TrxName());
		
		for (MPaymentAllocate pa : paList)
		{
			MInvoice invoice = (MInvoice) pa.getC_Invoice();
			
			String sql = "SELECT COALESCE(SUM(pa.Amount + pa.WriteOffAmt + pa.DiscountAmt + pa.WithholdingAmt),0) "
					+ "FROM C_PaymentAllocate pa INNER JOIN C_Payment pay ON pay.C_Payment_ID=pa.C_Payment_ID "
					+ "WHERE pa.C_Invoice_ID=? AND pay.DateTrx <= ? AND pa.C_PaymentAllocate_ID <> ?";
			
			BigDecimal totalInvPaid = 
					DB.getSQLValueBDEx(get_TrxName(), sql, 
							pa.getC_Invoice_ID(), payment.getDateTrx(), pa.get_ID());
			BigDecimal totalAllocated = 
					pa.getAmount().add(pa.getDiscountAmt()).add(pa.getWriteOffAmt()).add(pa.getWithholdingAmt());
			BigDecimal overUnderAmt = invoice.getGrandTotal().subtract(totalInvPaid).subtract(totalAllocated);
			
			pa.setInvoiceAmt(invoice.getGrandTotal().subtract(totalInvPaid));
			pa.setOverUnderAmt(overUnderAmt);
			pa.disableModelValidation();
			pa.saveEx();
			
			MAllocationLine aLine = 
					new Query(getCtx(), MAllocationLine.Table_Name, "C_AllocationHdr_ID=? AND C_Invoice_ID=?", get_TrxName())
					.setParameters(alloc.get_ID(), pa.getC_Invoice_ID()).first();
			
			BigDecimal multiplier = payment.isReceipt()? Env.ONE : Env.ONE.negate();
			
			if (aLine == null)
			{
//				if (payment.isReceipt())
//					aLine = new MAllocationLine (alloc, pa.getAmount(), 
//						pa.getDiscountAmt(), pa.getWriteOffAmt(), pa.getOverUnderAmt());
//				else
					aLine = new MAllocationLine (alloc, pa.getAmount().multiply(multiplier), 
						pa.getDiscountAmt().multiply(multiplier), pa.getWriteOffAmt().multiply(multiplier), 
						pa.getOverUnderAmt().multiply(multiplier));
				
				aLine.setDocInfo(invoice.getC_BPartner_ID(), 0, pa.getC_Invoice_ID());
				aLine.setPaymentInfo(payment.getC_Payment_ID(), 0);
			}
			else {
				aLine.setAmount(pa.getAmount().multiply(multiplier));
				aLine.setDiscountAmt(pa.getDiscountAmt().multiply(multiplier));
				aLine.setWriteOffAmt(pa.getWriteOffAmt().multiply(multiplier));
				aLine.setOverUnderAmt(pa.getOverUnderAmt().multiply(multiplier));
			}
			
			aLine.disableModelValidation();
			if (!aLine.save())
				log.warning("P.Allocations - line not saved");
			else {
				pa.setC_AllocationLine_ID(aLine.getC_AllocationLine_ID());
				pa.disableModelValidation();
				pa.saveEx();
			}
			
			invoice.testAllocation();
			invoice.disableModelValidation();
			invoice.saveEx();
			
			//Update BPartner Balance.
			int C_Payment_ID = aLine.getC_Payment_ID();
			int C_BPartner_ID = aLine.getC_BPartner_ID();
			int M_Invoice_ID = aLine.getC_Invoice_ID();

			if ((C_BPartner_ID == 0) || ((M_Invoice_ID == 0) && (C_Payment_ID == 0)))
				continue;
		}
		
		updatePaymentAmounts(payment);
		
		payment.addDescription("{**AtC**}");
		payment.setDocStatus(DocAction.STATUS_Completed);
		payment.setDocAction(DocAction.ACTION_Close);
		payment.setProcessed(true);
		payment.disableModelValidation();
		payment.saveEx();
		
		alloc.disableModelValidation();
		alloc.setDocStatus(DocAction.STATUS_Completed);
		alloc.setDocAction(DocAction.ACTION_Close);
		alloc.setProcessed(true);
		if (!alloc.save()) {
			return "Payment Allocation header not created.";
		}
			
		
		if (!bpartner.save())
		{
			return "Could not update Business Partner";
		}
 
//		String error = DocManager.postDocument(m_ass, MPayment.Table_ID, payment.get_ID(), true, true, payment.get_TrxName());
//		if(!Util.isEmpty(error, true)) {
//			return m_errorLog.concat(error).concat("\n ********************** \n");
//		}
//		
//		error = DocManager.postDocument(m_ass, MAllocationHdr.Table_ID, alloc.get_ID(), true, true, payment.get_TrxName());
//		if(!Util.isEmpty(error, true)) {
//			return m_errorLog.concat(error).concat("\n ********************** \n");
//		}
		
		return null;
	}

	String updatePaymentAmounts(MPayment payment)
	{
		BigDecimal payAmt = Env.ZERO;
		BigDecimal writeOffAmt = Env.ZERO;
		BigDecimal overUnderAmt = Env.ZERO;
		BigDecimal discountAmt = Env.ZERO;
		BigDecimal withholdingAmt = Env.ZERO;
		
		StringBuilder sql = new StringBuilder ("SELECT COALESCE(SUM(Amount), 0), COALESCE(SUM(WriteOffAmt), 0),  ")
		.append(" COALESCE(SUM(OverUnderAmt), 0), COALESCE(SUM(DiscountAmt), 0), COALESCE(SUM(WithholdingAmt), 0) ")
		.append(" FROM C_PaymentAllocate WHERE C_Payment_ID=").append(payment.get_ID());
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql.toString(), get_TrxName());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				payAmt = payAmt.add(rs.getBigDecimal(1));
				writeOffAmt = writeOffAmt.add(rs.getBigDecimal(2));
				overUnderAmt = overUnderAmt.add(rs.getBigDecimal(3));
				discountAmt = discountAmt.add(rs.getBigDecimal(4));
				withholdingAmt = withholdingAmt.add(rs.getBigDecimal(5));
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			return ex.getMessage();
		}
		finally {
			try {
				stmt.close();
				rs.close();
			}
			catch (SQLException ex) {
					ex.printStackTrace();
					return ex.getMessage();
			}
		}
		
		BigDecimal totalAmount = payAmt;
		if (payment.getChargeAmt().signum() != 0)
			payAmt = totalAmount.subtract(payment.getChargeAmt());
		
		payment.setPayAmt(payAmt);
		payment.setWriteOffAmt(writeOffAmt);
		payment.setOverUnderAmt(overUnderAmt);
		payment.setDiscountAmt(discountAmt);
		payment.setWithholdingAmt(withholdingAmt);
		
		if (overUnderAmt.compareTo(Env.ZERO) != 0)
			payment.setIsOverUnderPayment(true);
		
		payment.setVendorBeginningBalance(Env.ZERO);
		payment.setVendorEndingBalance(Env.ZERO);
		
		if (!payment.save())
			throw new AdempiereException("Failed when refreshing payment amount.");
		
		return null;
	}	
}
