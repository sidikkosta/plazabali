/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_DisbursRecon_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_DisbursRecon_Line 
{

    /** TableName=UNS_DisbursRecon_Line */
    public static final String Table_Name = "UNS_DisbursRecon_Line";

    /** AD_Table_ID=1000417 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CardNo */
    public static final String COLUMNNAME_CardNo = "CardNo";

	/** Set Card No	  */
	public void setCardNo (String CardNo);

	/** Get Card No	  */
	public String getCardNo();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name DifferenceAmt */
    public static final String COLUMNNAME_DifferenceAmt = "DifferenceAmt";

	/** Set Difference.
	  * Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt);

	/** Get Difference.
	  * Difference Amount
	  */
	public BigDecimal getDifferenceAmt();

    /** Column name ExpectedAmount */
    public static final String COLUMNNAME_ExpectedAmount = "ExpectedAmount";

	/** Set Expected Amount	  */
	public void setExpectedAmount (BigDecimal ExpectedAmount);

	/** Get Expected Amount	  */
	public BigDecimal getExpectedAmount();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsManual */
    public static final String COLUMNNAME_IsManual = "IsManual";

	/** Set Manual.
	  * This is a manual process
	  */
	public void setIsManual (boolean IsManual);

	/** Get Manual.
	  * This is a manual process
	  */
	public boolean isManual();

    /** Column name IsNeedCTCorrection */
    public static final String COLUMNNAME_IsNeedCTCorrection = "IsNeedCTCorrection";

	/** Set Need Card Type Correction	  */
	public void setIsNeedCTCorrection (boolean IsNeedCTCorrection);

	/** Get Need Card Type Correction	  */
	public boolean isNeedCTCorrection();

    /** Column name NetAmount */
    public static final String COLUMNNAME_NetAmount = "NetAmount";

	/** Set Net Amount	  */
	public void setNetAmount (BigDecimal NetAmount);

	/** Get Net Amount	  */
	public BigDecimal getNetAmount();

    /** Column name Org_CardType_ID */
    public static final String COLUMNNAME_Org_CardType_ID = "Org_CardType_ID";

	/** Set Original Card Type	  */
	public void setOrg_CardType_ID (int Org_CardType_ID);

	/** Get Original Card Type	  */
	public int getOrg_CardType_ID();

    /** Column name OriginalAmt */
    public static final String COLUMNNAME_OriginalAmt = "OriginalAmt";

	/** Set Original Amount	  */
	public void setOriginalAmt (BigDecimal OriginalAmt);

	/** Get Original Amount	  */
	public BigDecimal getOriginalAmt();

    /** Column name OriginalDifferenceAmt */
    public static final String COLUMNNAME_OriginalDifferenceAmt = "OriginalDifferenceAmt";

	/** Set Original Difference Amount	  */
	public void setOriginalDifferenceAmt (BigDecimal OriginalDifferenceAmt);

	/** Get Original Difference Amount	  */
	public BigDecimal getOriginalDifferenceAmt();

    /** Column name OriginalNetAmount */
    public static final String COLUMNNAME_OriginalNetAmount = "OriginalNetAmount";

	/** Set Original Net Amount	  */
	public void setOriginalNetAmount (BigDecimal OriginalNetAmount);

	/** Get Original Net Amount	  */
	public BigDecimal getOriginalNetAmount();

    /** Column name OverchargedAmount */
    public static final String COLUMNNAME_OverchargedAmount = "OverchargedAmount";

	/** Set Overcharged Amount	  */
	public void setOverchargedAmount (BigDecimal OverchargedAmount);

	/** Get Overcharged Amount	  */
	public BigDecimal getOverchargedAmount();

    /** Column name Shop_ID */
    public static final String COLUMNNAME_Shop_ID = "Shop_ID";

	/** Set Shop.
	  * Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID);

	/** Get Shop.
	  * Identifies a Shop
	  */
	public int getShop_ID();

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException;

    /** Column name Status */
    public static final String COLUMNNAME_Status = "Status";

	/** Set Status.
	  * Status of the currently running check
	  */
	public void setStatus (String Status);

	/** Get Status.
	  * Status of the currently running check
	  */
	public String getStatus();

    /** Column name TID */
    public static final String COLUMNNAME_TID = "TID";

	/** Set TID	  */
	public void setTID (String TID);

	/** Get TID	  */
	public String getTID();

    /** Column name UNS_CardTrxDetail_ID */
    public static final String COLUMNNAME_UNS_CardTrxDetail_ID = "UNS_CardTrxDetail_ID";

	/** Set Card Trx Detail	  */
	public void setUNS_CardTrxDetail_ID (int UNS_CardTrxDetail_ID);

	/** Get Card Trx Detail	  */
	public int getUNS_CardTrxDetail_ID();

    /** Column name UNS_CardType_ID */
    public static final String COLUMNNAME_UNS_CardType_ID = "UNS_CardType_ID";

	/** Set Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID);

	/** Get Card Type	  */
	public int getUNS_CardType_ID();

    /** Column name UNS_DisbursementRecon_ID */
    public static final String COLUMNNAME_UNS_DisbursementRecon_ID = "UNS_DisbursementRecon_ID";

	/** Set Disbursement Reconciliation	  */
	public void setUNS_DisbursementRecon_ID (int UNS_DisbursementRecon_ID);

	/** Get Disbursement Reconciliation	  */
	public int getUNS_DisbursementRecon_ID();

	public I_UNS_DisbursementRecon getUNS_DisbursementRecon() throws RuntimeException;

    /** Column name UNS_DisbursRecon_Batch_ID */
    public static final String COLUMNNAME_UNS_DisbursRecon_Batch_ID = "UNS_DisbursRecon_Batch_ID";

	/** Set Disburs. Reconciliation Batch	  */
	public void setUNS_DisbursRecon_Batch_ID (int UNS_DisbursRecon_Batch_ID);

	/** Get Disburs. Reconciliation Batch	  */
	public int getUNS_DisbursRecon_Batch_ID();

	public I_UNS_DisbursRecon_Batch getUNS_DisbursRecon_Batch() throws RuntimeException;

    /** Column name UNS_DisbursRecon_Line_ID */
    public static final String COLUMNNAME_UNS_DisbursRecon_Line_ID = "UNS_DisbursRecon_Line_ID";

	/** Set Disburs. Recon Line	  */
	public void setUNS_DisbursRecon_Line_ID (int UNS_DisbursRecon_Line_ID);

	/** Get Disburs. Recon Line	  */
	public int getUNS_DisbursRecon_Line_ID();

    /** Column name UNS_DisbursRecon_Line_UU */
    public static final String COLUMNNAME_UNS_DisbursRecon_Line_UU = "UNS_DisbursRecon_Line_UU";

	/** Set UNS_DisbursRecon_Line_UU	  */
	public void setUNS_DisbursRecon_Line_UU (String UNS_DisbursRecon_Line_UU);

	/** Get UNS_DisbursRecon_Line_UU	  */
	public String getUNS_DisbursRecon_Line_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
