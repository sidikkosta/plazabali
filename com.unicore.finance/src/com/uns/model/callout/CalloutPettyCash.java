/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.unicore.model.MUNSTransferBalanceRequest;

/**
 * @author Burhani Adam
 *
 */
public class CalloutPettyCash implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutPettyCash() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		if(mTab.getTableName().equals(MUNSTransferBalanceRequest.Table_Name)
				&& mField.getColumnName().equals(MUNSTransferBalanceRequest.COLUMNNAME_AD_Org_ID))
			return Org(ctx, WindowNo, mTab, mField, value, oldValue);
		return null;
	}
	
	private String Org(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MUNSTransferBalanceRequest.COLUMNNAME_AD_OrgTo_ID, null);
			return "";
		}
		
		mTab.setValue(MUNSTransferBalanceRequest.COLUMNNAME_AD_OrgTo_ID, (Integer) value);
		
		return null;
	}
}