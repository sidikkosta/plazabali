/**
 * 
 */
package com.uns.model.callout;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSDisbursReconBatch;
import com.uns.model.MUNSDisbursReconLine;

/**
 * @author Burhani Adam
 *
 */
public class CalloutDisbursementRecon implements IColumnCallout{

	/**
	 * 
	 */
	public CalloutDisbursementRecon() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(mField.getColumnName().equals(MUNSDisbursReconLine.COLUMNNAME_UNS_CardTrxDetail_ID))
			return CardTrxDetailID(ctx, WindowNo, mTab, mField, value, oldValue, false);
		else if(mField.getColumnName().equals(MUNSDisbursReconLine.COLUMNNAME_ExpectedAmount))
			return expectedAmt(ctx, WindowNo, mTab, mField, value, oldValue);
		return null;
	}
	
	public String CardTrxDetailID(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue, boolean fromExpectedAmt)
	{
		if(value == null || (Integer) value <= 0)
		{
			mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_Org_CardType_ID, -1);
			mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_Shop_ID, -1);
			if(!fromExpectedAmt)
			{
				mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_ExpectedAmount, Env.ZERO);
				mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_OverchargedAmount, Env.ZERO);
			}
			else
			{
				BigDecimal expectedAmt = (BigDecimal) mTab.getValue(MUNSDisbursReconLine.COLUMNNAME_ExpectedAmount);
				BigDecimal oriAmt = (BigDecimal) mTab.getValue(MUNSDisbursReconLine.COLUMNNAME_OriginalAmt);
				if(expectedAmt.compareTo(oriAmt) != 0)
				{
					mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_OverchargedAmount, oriAmt.subtract(expectedAmt));
				}
			}
			return null;
		}
		
		String sql = "SELECT ct.UNS_CardType_ID, pp.C_BPartner_ID, ABS(pt.TrxAmt) FROM UNS_CardTrxDetail ct"
				+ " INNER JOIN UNS_PaymentTrx pt ON pt.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
				+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
				+ " WHERE ct.UNS_CardTrxDetail_ID = ?";
		List<Object> oo = DB.getSQLValueObjectsEx(null, sql, (Integer) value);
		
		mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_Org_CardType_ID, oo.get(0));
		mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_Shop_ID, oo.get(1));
		BigDecimal trxAmt = (BigDecimal) oo.get(2);
		BigDecimal oriAmt = (BigDecimal) mTab.getValue(MUNSDisbursReconLine.COLUMNNAME_OriginalAmt);
		if(trxAmt.compareTo(oriAmt) != 0)
		{
			BigDecimal overCharge = oriAmt.subtract(trxAmt);
			mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_ExpectedAmount, trxAmt);
			mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_OverchargedAmount, overCharge);
		}
		if(fromExpectedAmt)
			mTab.setValue(MUNSDisbursReconLine.COLUMNNAME_UNS_CardTrxDetail_ID, value);
		return null;
	}
	
	public String expectedAmt(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(value == null || ((BigDecimal) value).signum() == 0)
		{
			return CardTrxDetailID(ctx, WindowNo, mTab, mField, null, null, true);
		}
		
		int parentID = (Integer) mTab.getValue(MUNSDisbursReconLine.COLUMNNAME_UNS_DisbursRecon_Batch_ID);
		Timestamp dateTrx = (Timestamp) mTab.getValue(MUNSDisbursReconLine.COLUMNNAME_DateTrx);
		MUNSDisbursReconBatch batch = new MUNSDisbursReconBatch(ctx, parentID, null);
		BigDecimal expectedAmt = (BigDecimal) value;
		int cardType = (Integer) mTab.getValue(MUNSDisbursReconLine.COLUMNNAME_UNS_CardType_ID);
		if(batch.isRefund())
		{
			String sql = "SELECT ref.UNS_CardType_ID, ref.UNS_CardTrxDetail_ID,"
					+ " l.UNS_DisbursRecon_Line_ID FROM UNS_DisbursRecon_Line l"
					+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
					+ " INNER JOIN UNS_DisbursementRecon r ON r.UNS_DisbursementRecon_ID = b.UNS_DisbursementRecon_ID"
					+ " INNER JOIN UNS_CardTrxDetail ct ON ct.UNS_CardTrxDetail_ID = l.UNS_CardTrxDetail_ID"
					+ " INNER JOIN UNS_PaymentTrx pt ON pt.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
					+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
					+ " INNER JOIN C_DocType dt ON dt.C_DocType_ID = pp.C_DocType_ID"
					+ " INNER JOIN UNS_POSTrx trx ON trx.UNS_POSTrx_ID = pp.UNS_POSTrx_ID"
					+ " INNER JOIN UNS_POSTrx rtrx ON rtrx.Reference_ID = trx.UNS_POSTrx_ID"
					+ " INNER JOIN UNS_POSPayment rpp ON rpp.UNS_POSTrx_ID = rtrx.UNS_POSTrx_ID"
					+ " INNER JOIN UNS_PaymentTrx rpt ON rpt.UNS_POSPayment_ID = rpp.UNS_POSPayment_ID"
					+ " INNER JOIN UNS_CardTrxDetail ref ON ref.UNS_PaymentTrx_ID = rpt.UNS_PaymentTrx_ID"
					+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ref.UNS_EDC_ID"
					+ " WHERE r.DocStatus IN ('CO', 'CL') AND edc.C_BankAccount_ID = ?"
					+ " AND dt.DocBaseType = 'PRR' AND ct.UNS_CardType_ID = ref.UNS_CardType_ID"
					+ " AND CAST(rpp.DateTrx AS Date) <= ? AND ct.UNS_EDC_ID = ref.UNS_EDC_ID"
					+ " AND (l.NetAmount = ? OR l.ExpectedAmount = ?)";
			int idCardTrx = DB.getSQLValue(null, sql,
					batch.getUNS_DisbursementRecon().getC_BankAccount_ID()
						, dateTrx, expectedAmt, expectedAmt);
			
			return CardTrxDetailID(ctx, WindowNo, mTab, mField, idCardTrx, null, true);
		}
		else
		{
			String sql = "SELECT ct.UNS_CardTrxDetail_ID FROM UNS_CardTrxDetail ct"
					+ " INNER JOIN UNS_PaymentTrx trx ON trx.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
					+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = trx.UNS_POSPayment_ID"
					+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ct.UNS_EDC_ID"
					+ " WHERE trx.TrxAmt = ? AND DATE_TRUNC('Day', pp.DateTrx) <= ?"
					+ " AND edc.C_BankAccount_ID = ? AND ct.UNS_CardType_ID = ?"
					+ " AND NOT EXISTS (SELECT 1 FROM UNS_DisbursRecon_Line l"
					+ " WHERE l.UNS_CardTrxDetail_ID = ct.UNS_CardTrxDetail_ID)"
					+ " ORDER BY pp.DateTrx ASC";
			
			int idCardTrx = DB.getSQLValue(null, sql,
					new Object[]{expectedAmt, 
						dateTrx, batch.getUNS_DisbursementRecon().getC_BankAccount_ID()
							, cardType});
			
			if(idCardTrx > 0)
			{
				return CardTrxDetailID(ctx, WindowNo, mTab, mField, idCardTrx, null, true);
			}
			else
			{
				sql = "SELECT ct.UNS_CardTrxDetail_ID FROM UNS_CardTrxDetail ct"
						+ " INNER JOIN UNS_PaymentTrx trx ON trx.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
						+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = trx.UNS_POSPayment_ID"
						+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ct.UNS_EDC_ID"
						+ " WHERE trx.TrxAmt = ? AND ct.BatchNo = ? AND DATE_TRUNC('Day', pp.DateTrx) <= ?"
						+ " AND edc.C_BankAccount_ID = ?"
						+ " AND NOT EXISTS (SELECT 1 FROM UNS_DisbursRecon_Line l"
						+ " WHERE l.UNS_CardTrxDetail_ID = ct.UNS_CardTrxDetail_ID)"
						+ " ORDER BY pp.DateTrx ASC";
				
				idCardTrx = DB.getSQLValue(null, sql,
						new Object[]{expectedAmt, batch.getBatchNo(),
							dateTrx, batch.getUNS_DisbursementRecon().getC_BankAccount_ID()});
				
				return CardTrxDetailID(ctx, WindowNo, mTab, mField, idCardTrx, null, true);
			}
		}
	}
}