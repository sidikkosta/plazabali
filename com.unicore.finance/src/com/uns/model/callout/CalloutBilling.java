package com.uns.model.callout;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.base.IColumnCallout;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.I_C_PaymentTerm;
import org.compiere.model.MInvoice;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.unicore.model.MUNSBillingLine;
import com.unicore.model.MUNSBillingResult;
import com.unicore.model.X_UNS_Billing;
import com.unicore.model.X_UNS_BillingLine;

/**
 *	Billing Callouts	
 *	
 *  @author YAKA
 */
public class CalloutBilling extends CalloutEngine implements IColumnCallout
{
	
	/**
	 * 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String bPartner (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		Integer C_BPartner_ID = (Integer)value;
		if (C_BPartner_ID == null || C_BPartner_ID.intValue() == 0)
			return "";

		String sql = "SELECT p.C_PaymentTerm_ID,l.C_BPartner_Location_ID "
			+ " FROM C_BPartner p "
			+ " LEFT OUTER JOIN C_BPartner_Location l "
			+ " ON (p.C_BPartner_ID=l.C_BPartner_ID AND l.IsBillTo='Y' AND l.IsActive='Y') "
			+ " WHERE p.C_BPartner_ID=? AND p.IsActive='Y'";		//	#1

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, C_BPartner_ID.intValue());
			rs = pstmt.executeQuery();
			//
			if (rs.next())
			{
				//	Location
				int locID = rs.getInt("C_BPartner_Location_ID");
				if (C_BPartner_ID.toString().equals(Env.getContext(ctx, WindowNo, Env.TAB_INFO, "C_BPartner_ID")))
				{
					String loc = Env.getContext(ctx, WindowNo, Env.TAB_INFO, "C_BPartner_Location_ID");
					if (loc.length() > 0)
						locID = Integer.parseInt(loc);
				}
				if (locID == 0)
					mTab.setValue("C_BPartner_Location_ID", null);
				else
					mTab.setValue("C_BPartner_Location_ID", new Integer(locID));
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, "bPartner", e);
			return e.getLocalizedMessage();
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return "";
	}	//	bPartner	
	
	
	/**
	 * 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String clear (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		mTab.setValue(mField.getColumnName(), null);
		return "";
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		String retValue = null;
		
		if (X_UNS_Billing.COLUMNNAME_C_BPartner_ID.equals(mField.getColumnName()))
		{
			retValue = bPartner(ctx, WindowNo, mTab, mField, value);
		}
		else if(X_UNS_BillingLine.COLUMNNAME_C_Invoice_ID.equals(mField.getColumnName()))
		{
			retValue = C_Invoic_ID(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		else if(mTab.getTableName().equals("UNS_BillingLine_Result") && mField.getColumnName().equals("UNS_BillingLine_ID"))
		{
			retValue = UNS_BillingLine_ID(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		else if(mTab.getTableName().equals("UNS_Billing") && mField.getColumnName().equals("OldBilling_ID"))
		{
			retValue = OldBilling_ID(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		
		return retValue;
	}
	
	
	/**
	 * 
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @param oldValue
	 * @return
	 */
	public String C_Invoic_ID(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	
	{
		if(null == value)
			return null;
		
		int C_Invoice_ID = (Integer) value;
		MInvoice invoice = new MInvoice(ctx, C_Invoice_ID, null);
		Timestamp dateInvoiced = invoice.getDateInvoiced();
		I_C_PaymentTerm paymentTerm = invoice.getC_PaymentTerm();
		if(null == paymentTerm)
			throw new AdempiereException("Invoice is not have payment term");
		
		int duedays = paymentTerm.getNetDays();
		Timestamp dueDate = TimeUtil.addDays(dateInvoiced, duedays);
		
		BigDecimal grandTotal = invoice.getGrandTotal();
		
		mTab.setValue(MUNSBillingLine.COLUMNNAME_DateInvoiced, dateInvoiced);
		mTab.setValue(MUNSBillingLine.COLUMNNAME_DueDate, dueDate);
		mTab.setValue(MUNSBillingLine.COLUMNNAME_C_PaymentTerm_ID, invoice.getC_PaymentTerm_ID());
		mTab.setValue(MUNSBillingLine.COLUMNNAME_NetAmtToInvoice, grandTotal);
		mTab.setValue(MUNSBillingLine.COLUMNNAME_Description, invoice.getDescription());
		mTab.setValue(MUNSBillingLine.COLUMNNAME_SalesRep_ID, invoice.getSalesRep_ID());
		
		String sql = "SELECT invoiceopen(?,0)";
		BigDecimal invoiceOpen = DB.getSQLValueBD(null, sql, C_Invoice_ID);
		
		mTab.setValue(MUNSBillingLine.COLUMNNAME_OpenAmt, invoiceOpen);
		mTab.setValue(MUNSBillingLine.COLUMNNAME_PaidAmt, grandTotal.subtract(invoiceOpen));
		return null;
	}
	
	/**
	 * Untuk billing result.
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @param oldValue
	 * @return
	 */
	public String UNS_BillingLine_ID(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(null == value)
			return null;
		MUNSBillingResult br = new MUNSBillingResult(ctx, (Integer) mTab.getValue("UNS_Billing_Result_ID"), null);
		X_UNS_BillingLine bill = new X_UNS_BillingLine(ctx, (Integer) value, null);
		if(br.getUNS_BillingGroup_Result().isAutoGenerated())
			mTab.setValue(MUNSBillingLine.COLUMNNAME_NetAmtToInvoice, bill.getC_Invoice().getGrandTotal());
		else
			mTab.setValue(MUNSBillingLine.COLUMNNAME_NetAmtToInvoice, DB.getSQLValueBD(null, "SELECT invoiceopen(?,0)",
					bill.getC_Invoice_ID()));
		mTab.setValue("C_Invoice_ID", bill.getC_Invoice_ID());
		return null;
	}
	
	public String OldBilling_ID(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(null == value)
			return null;
		X_UNS_Billing bill = new X_UNS_Billing(ctx, (Integer) value, null);
		
		mTab.setValue("C_DocType_ID", bill.getC_DocType_ID());
		mTab.setValue("DocumentNo", bill.getDocumentNo());
		mTab.setValue("GroupValue", bill.getGroupValue());
		mTab.setValue("C_BPartner_ID", bill.getC_BPartner_ID());
		mTab.setValue("C_BPartner_Location_ID", bill.getC_BPartner_Location_ID());
		mTab.setValue("TotalAmt", bill.getTotalAmt());
		return null;
	}
}	//	CalloutBilling
