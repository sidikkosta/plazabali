/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_CardTypeCorrection
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CardTypeCorrection extends PO implements I_UNS_CardTypeCorrection, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190524L;

    /** Standard Constructor */
    public X_UNS_CardTypeCorrection (Properties ctx, int UNS_CardTypeCorrection_ID, String trxName)
    {
      super (ctx, UNS_CardTypeCorrection_ID, trxName);
      /** if (UNS_CardTypeCorrection_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setBatchNo (null);
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
			setDateTrx (new Timestamp( System.currentTimeMillis() ));
			setDifferenceAmt (Env.ZERO);
// 0
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setNewCardType_ID (0);
			setPosted (false);
// N
			setPrevCardType_ID (0);
			setProcessed (false);
// N
			setShop_ID (0);
			setTID (null);
			setUNS_CardTypeCorrection_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CardTypeCorrection (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CardTypeCorrection[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Batch No.
		@param BatchNo Batch No	  */
	public void setBatchNo (String BatchNo)
	{
		set_Value (COLUMNNAME_BatchNo, BatchNo);
	}

	/** Get Batch No.
		@return Batch No	  */
	public String getBatchNo () 
	{
		return (String)get_Value(COLUMNNAME_BatchNo);
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_ValueNoCheck (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_Value (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Difference.
		@param DifferenceAmt 
		Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt)
	{
		set_Value (COLUMNNAME_DifferenceAmt, DifferenceAmt);
	}

	/** Get Difference.
		@return Difference Amount
	  */
	public BigDecimal getDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set New Card Type.
		@param NewCardType_ID New Card Type	  */
	public void setNewCardType_ID (int NewCardType_ID)
	{
		if (NewCardType_ID < 1) 
			set_Value (COLUMNNAME_NewCardType_ID, null);
		else 
			set_Value (COLUMNNAME_NewCardType_ID, Integer.valueOf(NewCardType_ID));
	}

	/** Get New Card Type.
		@return New Card Type	  */
	public int getNewCardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_NewCardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Posted.
		@param Posted 
		Posting status
	  */
	public void setPosted (boolean Posted)
	{
		set_Value (COLUMNNAME_Posted, Boolean.valueOf(Posted));
	}

	/** Get Posted.
		@return Posting status
	  */
	public boolean isPosted () 
	{
		Object oo = get_Value(COLUMNNAME_Posted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Prev. Card Type.
		@param PrevCardType_ID Prev. Card Type	  */
	public void setPrevCardType_ID (int PrevCardType_ID)
	{
		if (PrevCardType_ID < 1) 
			set_Value (COLUMNNAME_PrevCardType_ID, null);
		else 
			set_Value (COLUMNNAME_PrevCardType_ID, Integer.valueOf(PrevCardType_ID));
	}

	/** Get Prev. Card Type.
		@return Prev. Card Type	  */
	public int getPrevCardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PrevCardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getShop_ID(), get_TrxName());	}

	/** Set Shop.
		@param Shop_ID 
		Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID)
	{
		if (Shop_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, Integer.valueOf(Shop_ID));
	}

	/** Get Shop.
		@return Identifies a Shop
	  */
	public int getShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Shop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set TID.
		@param TID TID	  */
	public void setTID (String TID)
	{
		set_Value (COLUMNNAME_TID, TID);
	}

	/** Get TID.
		@return TID	  */
	public String getTID () 
	{
		return (String)get_Value(COLUMNNAME_TID);
	}

	/** Set Card Type Journal Correction.
		@param UNS_CardTypeCorrection_ID Card Type Journal Correction	  */
	public void setUNS_CardTypeCorrection_ID (int UNS_CardTypeCorrection_ID)
	{
		if (UNS_CardTypeCorrection_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardTypeCorrection_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardTypeCorrection_ID, Integer.valueOf(UNS_CardTypeCorrection_ID));
	}

	/** Get Card Type Journal Correction.
		@return Card Type Journal Correction	  */
	public int getUNS_CardTypeCorrection_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardTypeCorrection_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CardTypeCorrection_UU.
		@param UNS_CardTypeCorrection_UU UNS_CardTypeCorrection_UU	  */
	public void setUNS_CardTypeCorrection_UU (String UNS_CardTypeCorrection_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CardTypeCorrection_UU, UNS_CardTypeCorrection_UU);
	}

	/** Get UNS_CardTypeCorrection_UU.
		@return UNS_CardTypeCorrection_UU	  */
	public String getUNS_CardTypeCorrection_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CardTypeCorrection_UU);
	}

	public I_UNS_DisbursRecon_Line getUNS_DisbursRecon_Line() throws RuntimeException
    {
		return (I_UNS_DisbursRecon_Line)MTable.get(getCtx(), I_UNS_DisbursRecon_Line.Table_Name)
			.getPO(getUNS_DisbursRecon_Line_ID(), get_TrxName());	}

	/** Set Disburs. Recon Line.
		@param UNS_DisbursRecon_Line_ID Disburs. Recon Line	  */
	public void setUNS_DisbursRecon_Line_ID (int UNS_DisbursRecon_Line_ID)
	{
		if (UNS_DisbursRecon_Line_ID < 1) 
			set_Value (COLUMNNAME_UNS_DisbursRecon_Line_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_DisbursRecon_Line_ID, Integer.valueOf(UNS_DisbursRecon_Line_ID));
	}

	/** Get Disburs. Recon Line.
		@return Disburs. Recon Line	  */
	public int getUNS_DisbursRecon_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursRecon_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}