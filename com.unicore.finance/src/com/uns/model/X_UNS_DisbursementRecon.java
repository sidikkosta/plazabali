/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_DisbursementRecon
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_DisbursementRecon extends PO implements I_UNS_DisbursementRecon, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20200102L;

    /** Standard Constructor */
    public X_UNS_DisbursementRecon (Properties ctx, int UNS_DisbursementRecon_ID, String trxName)
    {
      super (ctx, UNS_DisbursementRecon_ID, trxName);
      /** if (UNS_DisbursementRecon_ID == 0)
        {
			setAllowNotMatchedTrx (false);
// N
			setAmountRefunded (Env.ZERO);
// 0
			setC_DocType_ID (0);
			setDifferenceAmt (Env.ZERO);
// 0
			setDisbursementAmt (Env.ZERO);
// 0
			setDisbursementDate (new Timestamp( System.currentTimeMillis() ));
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setEDCType (null);
			setFile_Directory (null);
			setImportLines (null);
// N
			setMatchedAmt (Env.ZERO);
// 0
			setNotMatchedAmt (Env.ZERO);
// 0
			setNotMatchedRefundAmt (Env.ZERO);
// 0
			setOverchargedAmount (Env.ZERO);
// 0
			setProcessed (false);
// N
			setUNS_DisbursementRecon_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_DisbursementRecon (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_DisbursementRecon[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Allow Not Matched Transactions ?.
		@param AllowNotMatchedTrx Allow Not Matched Transactions ?	  */
	public void setAllowNotMatchedTrx (boolean AllowNotMatchedTrx)
	{
		set_Value (COLUMNNAME_AllowNotMatchedTrx, Boolean.valueOf(AllowNotMatchedTrx));
	}

	/** Get Allow Not Matched Transactions ?.
		@return Allow Not Matched Transactions ?	  */
	public boolean isAllowNotMatchedTrx () 
	{
		Object oo = get_Value(COLUMNNAME_AllowNotMatchedTrx);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set AmountRefunded.
		@param AmountRefunded AmountRefunded	  */
	public void setAmountRefunded (BigDecimal AmountRefunded)
	{
		set_Value (COLUMNNAME_AmountRefunded, AmountRefunded);
	}

	/** Get AmountRefunded.
		@return AmountRefunded	  */
	public BigDecimal getAmountRefunded () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmountRefunded);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankAccount)MTable.get(getCtx(), org.compiere.model.I_C_BankAccount.Table_Name)
			.getPO(getC_BankAccount_ID(), get_TrxName());	}

	/** Set Cash / Bank Account.
		@param C_BankAccount_ID 
		Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID)
	{
		if (C_BankAccount_ID < 1) 
			set_Value (COLUMNNAME_C_BankAccount_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankAccount_ID, Integer.valueOf(C_BankAccount_ID));
	}

	/** Get Cash / Bank Account.
		@return Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BankStatement getC_BankStatement() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankStatement)MTable.get(getCtx(), org.compiere.model.I_C_BankStatement.Table_Name)
			.getPO(getC_BankStatement_ID(), get_TrxName());	}

	/** Set Bank Statement.
		@param C_BankStatement_ID 
		Bank Statement of account
	  */
	public void setC_BankStatement_ID (int C_BankStatement_ID)
	{
		if (C_BankStatement_ID < 1) 
			set_Value (COLUMNNAME_C_BankStatement_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankStatement_ID, Integer.valueOf(C_BankStatement_ID));
	}

	/** Get Bank Statement.
		@return Bank Statement of account
	  */
	public int getC_BankStatement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankStatement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Difference.
		@param DifferenceAmt 
		Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt)
	{
		set_Value (COLUMNNAME_DifferenceAmt, DifferenceAmt);
	}

	/** Get Difference.
		@return Difference Amount
	  */
	public BigDecimal getDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Disbursement Amount.
		@param DisbursementAmt Disbursement Amount	  */
	public void setDisbursementAmt (BigDecimal DisbursementAmt)
	{
		set_Value (COLUMNNAME_DisbursementAmt, DisbursementAmt);
	}

	/** Get Disbursement Amount.
		@return Disbursement Amount	  */
	public BigDecimal getDisbursementAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DisbursementAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Disbursement Date.
		@param DisbursementDate 
		Date when payment can be processed (Cek/Giro)
	  */
	public void setDisbursementDate (Timestamp DisbursementDate)
	{
		set_Value (COLUMNNAME_DisbursementDate, DisbursementDate);
	}

	/** Get Disbursement Date.
		@return Date when payment can be processed (Cek/Giro)
	  */
	public Timestamp getDisbursementDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DisbursementDate);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** We Chat = WCH */
	public static final String EDCTYPE_WeChat = "WCH";
	/** BCA = BCA */
	public static final String EDCTYPE_BCA = "BCA";
	/** MANDIRI = MAN */
	public static final String EDCTYPE_MANDIRI = "MAN";
	/** BNI = BNI */
	public static final String EDCTYPE_BNI = "BNI";
	/** CUP Indopay = CUP */
	public static final String EDCTYPE_CUPIndopay = "CUP";
	/** Set EDC Type.
		@param EDCType EDC Type	  */
	public void setEDCType (String EDCType)
	{

		set_Value (COLUMNNAME_EDCType, EDCType);
	}

	/** Get EDC Type.
		@return EDC Type	  */
	public String getEDCType () 
	{
		return (String)get_Value(COLUMNNAME_EDCType);
	}

	/** Set File_Directory.
		@param File_Directory File_Directory	  */
	public void setFile_Directory (String File_Directory)
	{
		set_Value (COLUMNNAME_File_Directory, File_Directory);
	}

	/** Get File_Directory.
		@return File_Directory	  */
	public String getFile_Directory () 
	{
		return (String)get_Value(COLUMNNAME_File_Directory);
	}

	/** Set Import Lines.
		@param ImportLines Import Lines	  */
	public void setImportLines (String ImportLines)
	{
		set_Value (COLUMNNAME_ImportLines, ImportLines);
	}

	/** Get Import Lines.
		@return Import Lines	  */
	public String getImportLines () 
	{
		return (String)get_Value(COLUMNNAME_ImportLines);
	}

	/** Set Matched Amount.
		@param MatchedAmt Matched Amount	  */
	public void setMatchedAmt (BigDecimal MatchedAmt)
	{
		set_Value (COLUMNNAME_MatchedAmt, MatchedAmt);
	}

	/** Get Matched Amount.
		@return Matched Amount	  */
	public BigDecimal getMatchedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MatchedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Not Matched Amount.
		@param NotMatchedAmt Not Matched Amount	  */
	public void setNotMatchedAmt (BigDecimal NotMatchedAmt)
	{
		set_Value (COLUMNNAME_NotMatchedAmt, NotMatchedAmt);
	}

	/** Get Not Matched Amount.
		@return Not Matched Amount	  */
	public BigDecimal getNotMatchedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NotMatchedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Not Matched Refund Amount.
		@param NotMatchedRefundAmt Not Matched Refund Amount	  */
	public void setNotMatchedRefundAmt (BigDecimal NotMatchedRefundAmt)
	{
		set_Value (COLUMNNAME_NotMatchedRefundAmt, NotMatchedRefundAmt);
	}

	/** Get Not Matched Refund Amount.
		@return Not Matched Refund Amount	  */
	public BigDecimal getNotMatchedRefundAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NotMatchedRefundAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Overcharged Amount.
		@param OverchargedAmount Overcharged Amount	  */
	public void setOverchargedAmount (BigDecimal OverchargedAmount)
	{
		set_Value (COLUMNNAME_OverchargedAmount, OverchargedAmount);
	}

	/** Get Overcharged Amount.
		@return Overcharged Amount	  */
	public BigDecimal getOverchargedAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OverchargedAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Posted.
		@param Posted 
		Posting status
	  */
	public void setPosted (boolean Posted)
	{
		set_Value (COLUMNNAME_Posted, Boolean.valueOf(Posted));
	}

	/** Get Posted.
		@return Posting status
	  */
	public boolean isPosted () 
	{
		Object oo = get_Value(COLUMNNAME_Posted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Print Disbursement.
		@param PrintDisbursement Print Disbursement	  */
	public void setPrintDisbursement (String PrintDisbursement)
	{
		set_Value (COLUMNNAME_PrintDisbursement, PrintDisbursement);
	}

	/** Get Print Disbursement.
		@return Print Disbursement	  */
	public String getPrintDisbursement () 
	{
		return (String)get_Value(COLUMNNAME_PrintDisbursement);
	}

	/** Set PrintDisbursementv.
		@param PrintDisbursementv PrintDisbursementv	  */
	public void setPrintDisbursementv (String PrintDisbursementv)
	{
		set_Value (COLUMNNAME_PrintDisbursementv, PrintDisbursementv);
	}

	/** Get PrintDisbursementv.
		@return PrintDisbursementv	  */
	public String getPrintDisbursementv () 
	{
		return (String)get_Value(COLUMNNAME_PrintDisbursementv);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Disbursement Reconciliation.
		@param UNS_DisbursementRecon_ID Disbursement Reconciliation	  */
	public void setUNS_DisbursementRecon_ID (int UNS_DisbursementRecon_ID)
	{
		if (UNS_DisbursementRecon_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_ID, Integer.valueOf(UNS_DisbursementRecon_ID));
	}

	/** Get Disbursement Reconciliation.
		@return Disbursement Reconciliation	  */
	public int getUNS_DisbursementRecon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursementRecon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_DisbursementRecon_UU.
		@param UNS_DisbursementRecon_UU UNS_DisbursementRecon_UU	  */
	public void setUNS_DisbursementRecon_UU (String UNS_DisbursementRecon_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_UU, UNS_DisbursementRecon_UU);
	}

	/** Get UNS_DisbursementRecon_UU.
		@return UNS_DisbursementRecon_UU	  */
	public String getUNS_DisbursementRecon_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_DisbursementRecon_UU);
	}
}