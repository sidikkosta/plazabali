/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_DisbursRecon_Batch
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_DisbursRecon_Batch extends PO implements I_UNS_DisbursRecon_Batch, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181226L;

    /** Standard Constructor */
    public X_UNS_DisbursRecon_Batch (Properties ctx, int UNS_DisbursRecon_Batch_ID, String trxName)
    {
      super (ctx, UNS_DisbursRecon_Batch_ID, trxName);
      /** if (UNS_DisbursRecon_Batch_ID == 0)
        {
			setAmountRefunded (Env.ZERO);
// 0
			setDifferenceAmt (Env.ZERO);
// 0
			setDisbursementAmt (Env.ZERO);
// 0
			setisRefund (false);
// N
			setNetAmount (Env.ZERO);
// 0
			setNotMatchedRefundAmt (Env.ZERO);
// 0
			setOriginalAmt (Env.ZERO);
// 0
			setUNS_DisbursRecon_Batch_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_DisbursRecon_Batch (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_DisbursRecon_Batch[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set AmountRefunded.
		@param AmountRefunded AmountRefunded	  */
	public void setAmountRefunded (BigDecimal AmountRefunded)
	{
		set_Value (COLUMNNAME_AmountRefunded, AmountRefunded);
	}

	/** Get AmountRefunded.
		@return AmountRefunded	  */
	public BigDecimal getAmountRefunded () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmountRefunded);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Batch No.
		@param BatchNo Batch No	  */
	public void setBatchNo (String BatchNo)
	{
		set_Value (COLUMNNAME_BatchNo, BatchNo);
	}

	/** Get Batch No.
		@return Batch No	  */
	public String getBatchNo () 
	{
		return (String)get_Value(COLUMNNAME_BatchNo);
	}

	/** Set Difference.
		@param DifferenceAmt 
		Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt)
	{
		set_Value (COLUMNNAME_DifferenceAmt, DifferenceAmt);
	}

	/** Get Difference.
		@return Difference Amount
	  */
	public BigDecimal getDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Disbursement Amount.
		@param DisbursementAmt Disbursement Amount	  */
	public void setDisbursementAmt (BigDecimal DisbursementAmt)
	{
		set_Value (COLUMNNAME_DisbursementAmt, DisbursementAmt);
	}

	/** Get Disbursement Amount.
		@return Disbursement Amount	  */
	public BigDecimal getDisbursementAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DisbursementAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Refund.
		@param isRefund Refund	  */
	public void setisRefund (boolean isRefund)
	{
		set_Value (COLUMNNAME_isRefund, Boolean.valueOf(isRefund));
	}

	/** Get Refund.
		@return Refund	  */
	public boolean isRefund () 
	{
		Object oo = get_Value(COLUMNNAME_isRefund);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Merchant.
		@param Merchant Merchant	  */
	public void setMerchant (String Merchant)
	{
		set_Value (COLUMNNAME_Merchant, Merchant);
	}

	/** Get Merchant.
		@return Merchant	  */
	public String getMerchant () 
	{
		return (String)get_Value(COLUMNNAME_Merchant);
	}

	/** Set Net Amount.
		@param NetAmount Net Amount	  */
	public void setNetAmount (BigDecimal NetAmount)
	{
		set_Value (COLUMNNAME_NetAmount, NetAmount);
	}

	/** Get Net Amount.
		@return Net Amount	  */
	public BigDecimal getNetAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Not Matched Refund Amount.
		@param NotMatchedRefundAmt Not Matched Refund Amount	  */
	public void setNotMatchedRefundAmt (BigDecimal NotMatchedRefundAmt)
	{
		set_Value (COLUMNNAME_NotMatchedRefundAmt, NotMatchedRefundAmt);
	}

	/** Get Not Matched Refund Amount.
		@return Not Matched Refund Amount	  */
	public BigDecimal getNotMatchedRefundAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NotMatchedRefundAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Amount.
		@param OriginalAmt Original Amount	  */
	public void setOriginalAmt (BigDecimal OriginalAmt)
	{
		set_Value (COLUMNNAME_OriginalAmt, OriginalAmt);
	}

	/** Get Original Amount.
		@return Original Amount	  */
	public BigDecimal getOriginalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_UNS_DisbursementRecon getUNS_DisbursementRecon() throws RuntimeException
    {
		return (I_UNS_DisbursementRecon)MTable.get(getCtx(), I_UNS_DisbursementRecon.Table_Name)
			.getPO(getUNS_DisbursementRecon_ID(), get_TrxName());	}

	/** Set Disbursement Reconciliation.
		@param UNS_DisbursementRecon_ID Disbursement Reconciliation	  */
	public void setUNS_DisbursementRecon_ID (int UNS_DisbursementRecon_ID)
	{
		if (UNS_DisbursementRecon_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_ID, Integer.valueOf(UNS_DisbursementRecon_ID));
	}

	/** Get Disbursement Reconciliation.
		@return Disbursement Reconciliation	  */
	public int getUNS_DisbursementRecon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursementRecon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Disburs. Reconciliation Batch.
		@param UNS_DisbursRecon_Batch_ID Disburs. Reconciliation Batch	  */
	public void setUNS_DisbursRecon_Batch_ID (int UNS_DisbursRecon_Batch_ID)
	{
		if (UNS_DisbursRecon_Batch_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Batch_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Batch_ID, Integer.valueOf(UNS_DisbursRecon_Batch_ID));
	}

	/** Get Disburs. Reconciliation Batch.
		@return Disburs. Reconciliation Batch	  */
	public int getUNS_DisbursRecon_Batch_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursRecon_Batch_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_DisbursRecon_Batch_UU.
		@param UNS_DisbursRecon_Batch_UU UNS_DisbursRecon_Batch_UU	  */
	public void setUNS_DisbursRecon_Batch_UU (String UNS_DisbursRecon_Batch_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Batch_UU, UNS_DisbursRecon_Batch_UU);
	}

	/** Get UNS_DisbursRecon_Batch_UU.
		@return UNS_DisbursRecon_Batch_UU	  */
	public String getUNS_DisbursRecon_Batch_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_DisbursRecon_Batch_UU);
	}
}