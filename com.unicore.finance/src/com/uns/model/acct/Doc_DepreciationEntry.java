/**
 * 
 */
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import org.compiere.acct.Doc;
import org.compiere.acct.DocLine;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MDepreciationEntry;
import org.compiere.model.MDepreciationExp;
import org.compiere.util.Env;

/**
 * @author nurse
 *
 */
public class Doc_DepreciationEntry extends Doc 
{

	private MDepreciationEntry m_model = null;
	/**
	 * 
	 * @param as
	 * @param rs
	 * @param trxName
	 */
	public Doc_DepreciationEntry (MAcctSchema as, ResultSet rs, String trxName)
	{
		super(as, MDepreciationEntry.class, rs, null, trxName);
	}	//	Doc_A_Depreciation_Entry


	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails() 
	{
		m_model = (MDepreciationEntry)getPO();		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() 
	{
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) 
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		//	Other Acct Schema
		if (as.getC_AcctSchema_ID() != m_model.getC_AcctSchema_ID())
			return facts;
		
		//  create Fact Header
		Fact fact = new Fact (this, as, m_model.getPostingType());

		MDepreciationEntry entry = (MDepreciationEntry)getPO();
		Iterator<MDepreciationExp> it = entry.getLinesIterator(false);
		List<Object[]> drRows = new ArrayList<>();
		List<Object[]> crRows = new ArrayList<>();
		while(it.hasNext())
		{
			MDepreciationExp depexp = it.next();
			if (!depexp.isProcessed())
				continue;
			BigDecimal expenseAmt = depexp.getExpense();
			//
			MAccount dr_acct = MAccount.get(getCtx(), depexp.getDR_Account_ID());
			MAccount cr_acct = MAccount.get(getCtx(), depexp.getCR_Account_ID());
			Object[] drrow = null;
			Object[] crrow = null;
			
			for (int i=0; i<drRows.size(); i++)
			{
				int sectOfDeptID = (Integer) drRows.get(i)[0];
				MAccount existsAcct = (MAccount) drRows.get(i)[1];
				if (sectOfDeptID == depexp.getC_BPartner_ID()
						&& existsAcct.get_ID() == dr_acct.get_ID())
				{
					drrow = crRows.get(i);
					break;
				}
			}
			
			for (int i=0; i<crRows.size(); i++)
			{
				int sectOfDeptID = (Integer) drRows.get(i)[0];
				MAccount existsAcct = (MAccount) drRows.get(i)[1];
				if (sectOfDeptID == depexp.getC_BPartner_ID()
						&& existsAcct.get_ID() == cr_acct.get_ID())
				{
					crrow = crRows.get(i);
					break;
				}
			}
			
			if (drrow == null)
			{
				drrow = new Object[3];
				drrow[0] = depexp.getC_BPartner_ID();
				drrow[1] = dr_acct;
				drrow[2] = Env.ZERO;
				drRows.add(drrow);
			}
			
			if (crrow == null)
			{
				crrow = new Object[3];
				crrow[0] = depexp.getC_BPartner_ID();
				crrow[1] = cr_acct;
				crrow[2] = Env.ZERO;
				crRows.add(crrow);
			}
			
			BigDecimal drAmt = (BigDecimal) drrow[2];
			BigDecimal crAmt = (BigDecimal) crrow[2];
			drAmt = drAmt.add(expenseAmt);
			crAmt = crAmt.add(expenseAmt);
			
			crrow[2] = crAmt;
			drrow[2] = drAmt;
		}
		
		for (int i=0; i<drRows.size(); i++)
		{
			DocLine dl = new DocLine(m_model, this);
			FactLine fl = fact.createLine(dl, (MAccount) drRows.get(i)[1], as.getC_Currency_ID(), 
					(BigDecimal)drRows.get(i)[2], null);
			if (fl == null)
			{
				p_Error = "Can't create DR " + dl.toString();
				log.log(Level.SEVERE, p_Error);
				return null;
			}

			fl.setC_BPartner_ID((Integer)crRows.get(i)[0]);
		}
		
		for (int i=0; i<crRows.size(); i++)
		{
			DocLine dl = new DocLine(m_model, this);
			FactLine fl = fact.createLine(dl, (MAccount) crRows.get(i)[1], as.getC_Currency_ID(), 
					null, (BigDecimal)crRows.get(i)[2]);
			if (fl == null)
			{
				p_Error = "Can't create CR " + dl.toString();
				log.log(Level.SEVERE, p_Error);
				return null;
			}
			
			fl.setC_BPartner_ID((Integer)crRows.get(i)[0]);
		}
		
		facts.add(fact);
		return facts;
	}
}
