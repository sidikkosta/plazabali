/**
 * 
 */
package com.uns.model.acct;

import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MAssetAcct;
import org.compiere.model.MConversionRate;
import org.compiere.model.MDepreciationExp;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSAssetLeaseSettlement;

/**
 * @author nurse
 *
 */
public class Doc_AssetLeaseSettlement extends Doc 
{
	public Doc_AssetLeaseSettlement (MAcctSchema as, ResultSet rs, String trxName)
	{
		this(as, MUNSAssetLeaseSettlement.class, rs, null, trxName);
	}
	
	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_AssetLeaseSettlement(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) 
	{
		super(as, clazz, rs, defaultDocumentType, trxName);
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() 
	{
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) 
	{
		MUNSAssetLeaseSettlement model = (MUNSAssetLeaseSettlement) getPO();
		ArrayList<Fact> facts = new ArrayList<>();
		Fact fact = new Fact(this, as, model.getPostingType());
		facts.add(fact);
		String sql = "SELECT C_Currency_ID, C_ConversionType_ID, AssetValueAmt, "
				+ " DateAcct, PostingType, C_BPartner_ID FROM A_Asset_Addition "
				+ " WHERE A_Asset_ID = ? AND "
				+ " Processed = ? AND DocStatus IN (?,?) AND PostingType = ? "
				+ " ORDER BY DateAcct";
		PreparedStatement st = null;
		ResultSet rs = null;
		Hashtable<Point, BigDecimal> drAcctAmt = new Hashtable<>();
		Hashtable<Point, BigDecimal> crAcctAmt = new Hashtable<>();
		try
		{
			st = DB.prepareStatement(sql, getTrxName());
			st.setInt(1, model.getA_Asset_ID());
			st.setString(2, "Y");
			st.setString(3, "CO");
			st.setString(4, "CL");
			st.setString(5, model.getPostingType());
			rs = st.executeQuery();
			while (rs.next())
			{
				int curID = rs.getInt(1);
				int convT = rs.getInt(2);
				BigDecimal amt = rs.getBigDecimal(3);
				Timestamp dateAcct = rs.getTimestamp(4);
				String postingType = rs.getString(5);
				int partnerID = rs.getInt(6);
				if (curID != model.getC_Currency_ID())
					amt = MConversionRate.convert(
							getCtx(), amt, curID, model.getC_Currency_ID(), 
							dateAcct, convT, getAD_Client_ID(), 
							getAD_Org_ID(), RoundingMode.HALF_EVEN, -1);
				
				MAssetAcct acct = MAssetAcct.forA_Asset_ID(
						getCtx(), model.getA_Asset_ID(), postingType, dateAcct, 
						getTrxName());
				
				int drAcctID = acct.getA_Asset_Acct();
				int crAcctID = acct.getUNS_AssetLease_Acct();
				Point pdr = new Point(partnerID, drAcctID);
				Point pcr = new Point(partnerID, crAcctID);
				BigDecimal tdb = drAcctAmt.get(pdr);
				BigDecimal tcr = crAcctAmt.get(pcr);
				if (tdb == null)
					tdb = Env.ZERO;
				if (tcr == null)
					tcr = Env.ZERO;
				tdb = tdb.add(amt);
				tcr = tcr.add(amt);
				drAcctAmt.put(pdr, tdb);
				crAcctAmt.put(pcr, tcr);
			}
		}
		catch (SQLException ex)
		{
			p_Error = ex.getMessage();
			return null;
		}
		
		List<MDepreciationExp> deps = model.getDepreciationBefore();
		
		for (int i=0; i<deps.size();i++)
		{
			if (!deps.get(i).isPosted())
				continue;
			String _sql = "SELECT C_Currency_ID FROM A_Depreciation_Entry WHERE A_Depreciation_Entry_ID = ?";
			int currencyID = DB.getSQLValue(getTrxName(), _sql, deps.get(i).getA_Depreciation_Entry_ID());
			BigDecimal amt = deps.get(i).getExpense();
			if (currencyID != model.getC_Currency_ID())
				amt = MConversionRate.convert(
						getCtx(), amt, currencyID, model.getC_Currency_ID(), 
						getAD_Client_ID(), getAD_Org_ID());
			
			MAssetAcct acct = MAssetAcct.forA_Asset_ID(
					getCtx(), model.getA_Asset_ID(), deps.get(i).getPostingType(), deps.get(i).getDateAcct(), 
					getTrxName());
			int drAcctID = acct.getUNS_DepreciationLease_Acct();
			int crAcctID = acct.getA_Accumdepreciation_Acct();
			Point pdr = new Point(deps.get(i).getC_BPartner_ID(), drAcctID);
			Point pcr = new Point(deps.get(i).getC_BPartner_ID(), crAcctID);
			BigDecimal tdb = drAcctAmt.get(pdr);
			BigDecimal tcr = crAcctAmt.get(pcr);
			if (tdb == null)
				tdb = Env.ZERO;
			if (tcr == null)
				tcr = Env.ZERO;
			tdb = tdb.add(amt);
			tcr = tcr.add(amt);
			drAcctAmt.put(pdr, tdb);
			crAcctAmt.put(pcr, tcr);
		}
		
		for (Point key : drAcctAmt.keySet())
		{
			FactLine dr = fact.createLine(
					null, MAccount.get(getCtx(), (int)key.getY()), model.getC_Currency_ID(), 
					drAcctAmt.get(key), null);
			if (dr == null)
			{
				p_Error = "could not create dr";
				return null;
			}
			dr.setA_Asset_ID(model.getA_Asset_ID());
			dr.setC_BPartner_ID((int) key.getX());
		}
		
		for (Point key : crAcctAmt.keySet())
		{
			FactLine cr = fact.createLine(
					null, MAccount.get(getCtx(), (int)key.getY()), model.getC_Currency_ID(), 
					null, crAcctAmt.get(key));
			if (cr == null)
			{
				p_Error = "could not create cr";
				return null;
			}
			cr.setA_Asset_ID(model.getA_Asset_ID());
			cr.setC_BPartner_ID((int) key.getX());
		}
		
		return facts;
	}

}
