/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.compiere.acct.Doc;
import org.compiere.acct.DocLine;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.util.Env;

import com.untaerp.model.MUNSCurrsBalance;
import com.untaerp.model.MUNSCurrsBalanceLine;

/**
 *  Post GL Journal Documents.
 *  <pre>
 *  Table:              GL_Journal (224)
 *  Document Types:     GLJ
 *  </pre>
 *  @author Jorg Janke
 *  @version  $Id: Doc_GLJournal.java,v 1.3 2006/07/30 00:53:33 jjanke Exp $
 */
public class Doc_UNSCurrsBalance extends Doc
{
	/**
	 *  Constructor
	 * 	@param as accounting schema
	 * 	@param rs record
	 * 	@param trxName trx
	 */
	public Doc_UNSCurrsBalance (MAcctSchema as, ResultSet rs, String trxName)
	{
		super(as, MUNSCurrsBalance.class, rs, null, trxName);
	}	//	Doc_GL_Journal

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_UNSCurrsBalance(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
	}
	
	private int				m_C_AcctSchema_ID = 0;

	/**
	 *  Load Specific Document Details
	 *  @return error message or null
	 */
	protected String loadDocumentDetails ()
	{
		MUNSCurrsBalance cb = (MUNSCurrsBalance)getPO();
		m_C_AcctSchema_ID = cb.getC_AcctSchema_ID();

		//	Contained Objects
		p_lines = loadLines(cb);
		if (log.isLoggable(Level.FINE)) log.fine("Lines=" + p_lines.length);
		return null;
	}   //  loadDocumentDetails


	/**
	 *	Load Invoice Line
	 *	@param journal journal
	 *  @return DocLine Array
	 */
	private DocLine[] loadLines(MUNSCurrsBalance cb)
	{
		ArrayList<DocLine> list = new ArrayList<DocLine>();
		MUNSCurrsBalanceLine[] lines = cb.getLines(false);
		for (int i = 0; i < lines.length; i++)
		{
			MUNSCurrsBalanceLine line = lines[i];
			DocLine docLine = new DocLine (line, this);
			
			// -- Quantity
			docLine.setQty(Env.ONE, false);
			//  --  Source Amounts
			docLine.setAmount (Env.ZERO, Env.ZERO);
			//  --  Converted Amounts
			if (line.getAccountType().equals(MUNSCurrsBalanceLine.ACCOUNTTYPE_Asset))
				docLine.setConvertedAmt (m_C_AcctSchema_ID, line.getExcRateGainLoss(), Env.ZERO);
			else 
				docLine.setConvertedAmt (m_C_AcctSchema_ID, Env.ZERO, line.getExcRateGainLoss().negate());
			//  --  Account
			MAccount account = MAccount.get(getCtx(), cb.getAD_Client_ID(), cb.getAD_Org_ID(), m_C_AcctSchema_ID, 
					line.getC_ElementValue_ID(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, getTrxName());
			docLine.setAccount (account);
		
			//	--	Organization of Line was set to Org of Account
			list.add(docLine);
		}
		//	Return Array
		int size = list.size();
		DocLine[] dls = new DocLine[size];
		list.toArray(dls);
		return dls;
	}	//	loadLines


	/**************************************************************************
	 *  Get Source Currency Balance - subtracts line and tax amounts from total - no rounding
	 *  @return positive amount, if total invoice is bigger than lines
	 */
	public BigDecimal getBalance()
	{
		BigDecimal retValue = Env.ZERO;
		return retValue;
	}   //  getBalance

	/**
	 *  Create Facts (the accounting logic) for
	 *  GLJ.
	 *  (only for the accounting scheme, it was created)
	 *  <pre>
	 *      account     DR          CR
	 *  </pre>
	 *  @param as acct schema
	 *  @return Fact
	 */
	public ArrayList<Fact> createFacts (MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		//	Other Acct Schema
		if (as.getC_AcctSchema_ID() != m_C_AcctSchema_ID)
			return facts;

		MUNSCurrsBalance cb = (MUNSCurrsBalance) p_po;
		//  create Fact Header
		Fact fact = new Fact (this, as, Fact.POST_Actual);
		
		//  account     DR      CR
		MAccount gainExcRateAcct = getAccount(Doc.ACCTTYPE_ExcRateRealizedGain, as);
		MAccount lossExcRateAcct = getAccount(Doc.ACCTTYPE_ExcRateRealizedGain, as);
		MAccount gainLossAcct = null;
		
		for (int i = 0; i < p_lines.length; i++)
		{
			MUNSCurrsBalanceLine cbl = (MUNSCurrsBalanceLine) p_lines[i].getPO();
			
			FactLine balancing = fact.createLine (p_lines[i],
							p_lines[i].getAccount (),
							p_lines[i].getC_Currency_ID(),
							p_lines[i].getAmtSourceDr(),
							p_lines[i].getAmtSourceCr());
			String desc = balancing.getDescription() != null? balancing.getDescription() + " " : "";
			balancing.setDescription(desc + cb.getDescription());
			
			BigDecimal gainLossAmt = cbl.getExcRateGainLoss();
			
			if (!cbl.getAccountType().equals(MUNSCurrsBalanceLine.ACCOUNTTYPE_Asset))
				gainLossAmt = gainLossAmt.negate();
			
			DocLine gainLossDL = new DocLine(cbl, this);
			// -- Quantity
			gainLossDL.setQty(Env.ONE, false);
			//  --  Source Amounts
			gainLossDL.setAmount (Env.ZERO, Env.ZERO);
			//  --  Converted Amounts
			
			if (gainLossAmt.signum() > 0) {
				gainLossDL.setConvertedAmt(m_C_AcctSchema_ID, gainLossAmt, Env.ZERO);
				gainLossAcct = gainExcRateAcct;
			}
			else { 
				gainLossDL.setConvertedAmt(m_C_AcctSchema_ID, Env.ZERO, gainLossAmt);
				gainLossAcct = lossExcRateAcct;
			}
			
			FactLine gainLossFL = fact.createLine(gainLossDL, 
					gainLossAcct, 
					as.getC_Currency_ID(), 
					Env.ZERO, Env.ZERO);
			
			gainLossFL.setAD_Org_ID(cb.getAD_Org_ID());
			gainLossFL.setDescription("Gain/Loss from account element of " + p_lines[i].getAccount().getAlias());
		}	//	for all lines
			
		facts.add(fact);
		return facts;
	}   //  createFact

}   //  Doc_UNSCurrsBalance
