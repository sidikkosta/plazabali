/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAcctSchema;
import org.compiere.util.Env;

import com.untaerp.model.MUNSEOMPLRecon;

/**
 *  Post GL Journal Documents.
 *  <pre>
 *  Table:              GL_Journal (224)
 *  Document Types:     GLJ
 *  </pre>
 *  @author Jorg Janke
 *  @version  $Id: Doc_GLJournal.java,v 1.3 2006/07/30 00:53:33 jjanke Exp $
 */
public class Doc_UNSEOMPLRecon extends Doc
{
	/**
	 *  Constructor
	 * 	@param as accounting schema
	 * 	@param rs record
	 * 	@param trxName trx
	 */
	public Doc_UNSEOMPLRecon (MAcctSchema as, ResultSet rs, String trxName)
	{
		super(as, MUNSEOMPLRecon.class, rs, null, trxName);
	}	//	Doc_GL_Journal

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_UNSEOMPLRecon(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
	}
	
	private int				m_C_AcctSchema_ID = 0;

	/**
	 *  Load Specific Document Details
	 *  @return error message or null
	 */
	protected String loadDocumentDetails ()
	{
		MUNSEOMPLRecon plRecon = (MUNSEOMPLRecon)getPO();
		m_C_AcctSchema_ID = plRecon.getC_AcctSchema_ID();

		return null;
	}   //  loadDocumentDetails


	/**************************************************************************
	 *  
	 */
	public BigDecimal getBalance()
	{
		BigDecimal retValue = Env.ZERO;
		return retValue;
	}   //  getBalance

	/**
	 * 
	 *  @param as acct schema
	 *  @return Fact
	 */
	public ArrayList<Fact> createFacts (MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		//	Other Acct Schema
		if (as.getC_AcctSchema_ID() != m_C_AcctSchema_ID)
			return facts;

		MUNSEOMPLRecon plRecon = (MUNSEOMPLRecon) p_po;
		
		//  create Fact Header
		Fact fact = new Fact (this, as, Fact.POST_Actual);
		
		FactLine Dr = fact.createLine(null, 
				getAccount(Doc.ACCTTYPE_IncomeSummary, as), 
				as.getC_Currency_ID(),
				plRecon.getPLAmt());
		Dr.setAD_Org_ID(plRecon.getAD_Org_ID());
		
		FactLine Cr = fact.createLine(null, 
				getAccount(Doc.ACCTTYPE_CurrentEarning, as), 
				as.getC_Currency_ID(),
				plRecon.getPLAmt().negate());
		Cr.setAD_Org_ID(plRecon.getAD_Org_ID());
		
		facts.add(fact);
		return facts;
	}   //  createFact

}   //  Doc_UNSCurrsBalance
