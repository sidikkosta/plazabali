/**
 * 
 */
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSCardTypeCorrection;

/**
 * @author Burhani Adam
 *
 */
public class Doc_UNSCardTypeCorrection extends Doc {

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_UNSCardTypeCorrection(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSCardTypeCorrection correction = null;

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails()
	{
		correction = (MUNSCardTypeCorrection) getPO();
		setDateAcct(correction.getDateAcct());
		setDateDoc(correction.getDateAcct());
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() {
		// TODO Auto-generated method stub
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		String sql = "SELECT EDCAR_Acct FROM UNS_CardType"
				+ " WHERE UNS_CardType_ID = ?";
		int combDr = DB.getSQLValue(getTrxName(), sql, correction.getPrevCardType_ID());
		int combCr = DB.getSQLValue(getTrxName(), sql, correction.getNewCardType_ID());
		MAccount crAcct = MAccount.get(getCtx(), combCr);
		MAccount drAcct = MAccount.get(getCtx(), combDr);
		
		FactLine dr = null;
		FactLine cr = null;

		dr = fact.createLine(null, drAcct, correction.getC_Currency_ID(), correction.getAmount(), null);
		dr.setAD_Org_ID(correction.getAD_Org_ID());
		cr = fact.createLine(null, crAcct, correction.getC_Currency_ID(), null, correction.getAmount());
		cr.setAD_Org_ID(correction.getAD_Org_ID());
		
		sql = sql.replace("EDCAR", "EDCCommission");
		combDr = DB.getSQLValue(getTrxName(), sql, correction.getPrevCardType_ID());
		combCr = DB.getSQLValue(getTrxName(), sql, correction.getNewCardType_ID());
		crAcct = MAccount.get(getCtx(), combCr);
		drAcct = MAccount.get(getCtx(), combDr);
		
		dr = fact.createLine(null, drAcct, correction.getC_Currency_ID(), correction.getDifferenceAmt(), null);
		dr.setAD_Org_ID(correction.getAD_Org_ID());
		cr = fact.createLine(null, crAcct, correction.getC_Currency_ID(), null, correction.getDifferenceAmt());
		cr.setAD_Org_ID(correction.getAD_Org_ID());
		
		
		facts.add(fact);
		
		return facts;
	}
}