/**
 * 
 */
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.util.Env;

import com.uns.model.MUNSDisbursReconAmt;
import com.uns.model.MUNSDisbursReconLine;
import com.uns.model.MUNSDisbursementRecon;

/**
 * @author Burhani Adam
 *
 */
public class Doc_UNSDisbursementRecon extends Doc {

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_UNSDisbursementRecon(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSDisbursementRecon recon = null;

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails()
	{
		recon = (MUNSDisbursementRecon) getPO();
		setDateAcct(recon.getDisbursementDate());
		setDateDoc(recon.getDateDoc());
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() {
		// TODO Auto-generated method stub
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) {
		ArrayList<Fact> facts = new ArrayList<Fact>();
		
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		MAccount refundAcct = getAccount(Doc.ACCTTYPE_EDCRefundPay, as);
		MAccount bankAcct = as.isDirectJournalOnPayment() ? getAccount(Doc.ACCTTYPE_BankAsset, as) 
				: getAccount(Doc.ACCTTYPE_BankInTransit, as);
		FactLine dr = null;
		FactLine cr = null;

		MUNSDisbursReconAmt[] receives = recon.getReceives();
		for(int i=0;i<receives.length;i++)
		{
			dr = fact.createLine(null, bankAcct, recon.getC_Currency_ID(), receives[i].getDisbursementAmt(), null);
			dr.setAD_Org_ID(recon.getAD_Org_ID());
			dr.setDescription(receives[i].getDescription());
		}
		if(recon.getAmountRefunded().signum() != 0)
		{
			dr = fact.createLine(null, refundAcct, recon.getC_Currency_ID(), recon.getAmountRefunded(), null);
			dr.setAD_Org_ID(recon.getAD_Org_ID());
		}
		
		Hashtable<Integer, BigDecimal> mapOC = MUNSDisbursReconLine.getNotMatchedRefundAcct(recon);
		Enumeration<Integer> mapKeyss = mapOC.keys();
		
		while (mapKeyss.hasMoreElements())
		{
			dr = fact.createLine(null, MAccount.get(getCtx(), (Integer) mapKeyss.nextElement()),
					recon.getC_Currency_ID(), mapOC.get((Integer) mapKeyss.nextElement()), null);
			dr.setAD_Org_ID(recon.getAD_Org_ID());
		}
		
		Hashtable<String, BigDecimal> mapCT = recon.getGroupingCardTypeByShop();
		Enumeration<String> mapKeys = mapCT.keys();
		
		while (mapKeys.hasMoreElements())
		{
			String key  = (String) mapKeys.nextElement();
			String[] keys = key.split(";");
			int shopID = new Integer (keys[0]);
			int acctID = new Integer (keys[1]); 
			BigDecimal diffAmt = mapCT.get(key);
			
			if(diffAmt.signum() != 0)
			{
				dr = fact.createLine(null, MAccount.get(getCtx(), acctID), recon.getC_Currency_ID(),
						diffAmt, null);
				dr.setAD_Org_ID(recon.getAD_Org_ID());
				dr.setC_BPartner_ID(shopID);
			}
			
//			if(netAmt.signum() != 0)
//			{
//				cr = fact.createLine(null, getARAccount(ctID), recon.getC_Currency_ID(),
//						null, netAmt);
//				cr.setAD_Org_ID(recon.getAD_Org_ID());
//				cr.setC_BPartner_ID(shopID);
//			}
//			
//			if(overAmt.signum() != 0)
//			{
//				cr = fact.createLine(null, getOverchargeAccount(ctID), recon.getC_Currency_ID(),
//						null, overAmt);
//				cr.setAD_Org_ID(recon.getAD_Org_ID());
//				cr.setC_BPartner_ID(shopID);
//			}
		}
		
		Hashtable<String, BigDecimal> mapCTS = recon.getGroupingCardType();
		Enumeration<String> map = mapCTS.keys();
		while (map.hasMoreElements())
		{
			String key  = (String) map.nextElement();
			String[] value = key.split(";");
			int acctID = new Integer(value[0]);
			BigDecimal amt = mapCTS.get(key);
			if(key.contains("AR") && amt.signum() != 0)
			{
				cr = fact.createLine(null, MAccount.get(getCtx(), acctID), recon.getC_Currency_ID(),
						null, amt);
				cr.setAD_Org_ID(recon.getAD_Org_ID());
			}
			else if(amt.signum() != 0)
			{
				cr = fact.createLine(null, MAccount.get(getCtx(), acctID), recon.getC_Currency_ID(),
						null, amt);
				cr.setAD_Org_ID(recon.getAD_Org_ID());
			}
		}
		
//		dr = fact.createLine(null, bankAcct, recon.getC_Currency_ID(), null, new BigDecimal(-212000));
//		dr.setAD_Org_ID(recon.getAD_Org_ID());
		
		facts.add(fact);
		
		return facts;
	}
	
//	private MAccount getARAccount(int UNS_CardType)
//	{
//		String sql = "SELECT EDCAR_Acct FROM UNS_CardType WHERE UNS_CardType_ID = ?";
//		int validCombination = DB.getSQLValue(get_TrxName(), sql, UNS_CardType);
//		
//		if(validCombination > 0)
//		{
//			return MAccount.get(getCtx(), validCombination);
//		}
//		else
//			return null;
//	}
//	
//	private MAccount getOverchargeAccount(int UNS_CardType)
//	{
//		String sql = "SELECT Overcharge_Acct FROM UNS_CardType WHERE UNS_CardType_ID = ?";
//		int validCombination = DB.getSQLValue(get_TrxName(), sql, UNS_CardType);
//		
//		if(validCombination > 0)
//		{
//			return MAccount.get(getCtx(), validCombination);
//		}
//		else
//			return null;
//	}
//	
//	private MAccount getCommissionAccount(int UNS_CardType)
//	{
//		String sql = "SELECT EDCCommission_Acct FROM UNS_CardType WHERE UNS_CardType_ID = ?";
//		int validCombination = DB.getSQLValue(get_TrxName(), sql, UNS_CardType);
//		
//		if(validCombination > 0)
//		{
//			return MAccount.get(getCtx(), validCombination);
//		}
//		else
//			return null;
//	}
}