/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_DisbursementRecon
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_DisbursementRecon 
{

    /** TableName=UNS_DisbursementRecon */
    public static final String Table_Name = "UNS_DisbursementRecon";

    /** AD_Table_ID=1000419 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AllowNotMatchedTrx */
    public static final String COLUMNNAME_AllowNotMatchedTrx = "AllowNotMatchedTrx";

	/** Set Allow Not Matched Transactions ?	  */
	public void setAllowNotMatchedTrx (boolean AllowNotMatchedTrx);

	/** Get Allow Not Matched Transactions ?	  */
	public boolean isAllowNotMatchedTrx();

    /** Column name AmountRefunded */
    public static final String COLUMNNAME_AmountRefunded = "AmountRefunded";

	/** Set AmountRefunded	  */
	public void setAmountRefunded (BigDecimal AmountRefunded);

	/** Get AmountRefunded	  */
	public BigDecimal getAmountRefunded();

    /** Column name C_BankAccount_ID */
    public static final String COLUMNNAME_C_BankAccount_ID = "C_BankAccount_ID";

	/** Set Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID);

	/** Get Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID();

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException;

    /** Column name C_BankStatement_ID */
    public static final String COLUMNNAME_C_BankStatement_ID = "C_BankStatement_ID";

	/** Set Bank Statement.
	  * Bank Statement of account
	  */
	public void setC_BankStatement_ID (int C_BankStatement_ID);

	/** Get Bank Statement.
	  * Bank Statement of account
	  */
	public int getC_BankStatement_ID();

	public org.compiere.model.I_C_BankStatement getC_BankStatement() throws RuntimeException;

    /** Column name C_DocType_ID */
    public static final String COLUMNNAME_C_DocType_ID = "C_DocType_ID";

	/** Set Document Type.
	  * Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID);

	/** Get Document Type.
	  * Document type or rules
	  */
	public int getC_DocType_ID();

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DifferenceAmt */
    public static final String COLUMNNAME_DifferenceAmt = "DifferenceAmt";

	/** Set Difference.
	  * Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt);

	/** Get Difference.
	  * Difference Amount
	  */
	public BigDecimal getDifferenceAmt();

    /** Column name DisbursementAmt */
    public static final String COLUMNNAME_DisbursementAmt = "DisbursementAmt";

	/** Set Disbursement Amount	  */
	public void setDisbursementAmt (BigDecimal DisbursementAmt);

	/** Get Disbursement Amount	  */
	public BigDecimal getDisbursementAmt();

    /** Column name DisbursementDate */
    public static final String COLUMNNAME_DisbursementDate = "DisbursementDate";

	/** Set Disbursement Date.
	  * Date when payment can be processed (Cek/Giro)
	  */
	public void setDisbursementDate (Timestamp DisbursementDate);

	/** Get Disbursement Date.
	  * Date when payment can be processed (Cek/Giro)
	  */
	public Timestamp getDisbursementDate();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EDCType */
    public static final String COLUMNNAME_EDCType = "EDCType";

	/** Set EDC Type	  */
	public void setEDCType (String EDCType);

	/** Get EDC Type	  */
	public String getEDCType();

    /** Column name File_Directory */
    public static final String COLUMNNAME_File_Directory = "File_Directory";

	/** Set File_Directory	  */
	public void setFile_Directory (String File_Directory);

	/** Get File_Directory	  */
	public String getFile_Directory();

    /** Column name ImportLines */
    public static final String COLUMNNAME_ImportLines = "ImportLines";

	/** Set Import Lines	  */
	public void setImportLines (String ImportLines);

	/** Get Import Lines	  */
	public String getImportLines();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name MatchedAmt */
    public static final String COLUMNNAME_MatchedAmt = "MatchedAmt";

	/** Set Matched Amount	  */
	public void setMatchedAmt (BigDecimal MatchedAmt);

	/** Get Matched Amount	  */
	public BigDecimal getMatchedAmt();

    /** Column name NotMatchedAmt */
    public static final String COLUMNNAME_NotMatchedAmt = "NotMatchedAmt";

	/** Set Not Matched Amount	  */
	public void setNotMatchedAmt (BigDecimal NotMatchedAmt);

	/** Get Not Matched Amount	  */
	public BigDecimal getNotMatchedAmt();

    /** Column name NotMatchedRefundAmt */
    public static final String COLUMNNAME_NotMatchedRefundAmt = "NotMatchedRefundAmt";

	/** Set Not Matched Refund Amount	  */
	public void setNotMatchedRefundAmt (BigDecimal NotMatchedRefundAmt);

	/** Get Not Matched Refund Amount	  */
	public BigDecimal getNotMatchedRefundAmt();

    /** Column name OverchargedAmount */
    public static final String COLUMNNAME_OverchargedAmount = "OverchargedAmount";

	/** Set Overcharged Amount	  */
	public void setOverchargedAmount (BigDecimal OverchargedAmount);

	/** Get Overcharged Amount	  */
	public BigDecimal getOverchargedAmount();

    /** Column name Posted */
    public static final String COLUMNNAME_Posted = "Posted";

	/** Set Posted.
	  * Posting status
	  */
	public void setPosted (boolean Posted);

	/** Get Posted.
	  * Posting status
	  */
	public boolean isPosted();

    /** Column name PrintDisbursement */
    public static final String COLUMNNAME_PrintDisbursement = "PrintDisbursement";

	/** Set Print Disbursement	  */
	public void setPrintDisbursement (String PrintDisbursement);

	/** Get Print Disbursement	  */
	public String getPrintDisbursement();

    /** Column name PrintDisbursementv */
    public static final String COLUMNNAME_PrintDisbursementv = "PrintDisbursementv";

	/** Set PrintDisbursementv	  */
	public void setPrintDisbursementv (String PrintDisbursementv);

	/** Get PrintDisbursementv	  */
	public String getPrintDisbursementv();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name UNS_DisbursementRecon_ID */
    public static final String COLUMNNAME_UNS_DisbursementRecon_ID = "UNS_DisbursementRecon_ID";

	/** Set Disbursement Reconciliation	  */
	public void setUNS_DisbursementRecon_ID (int UNS_DisbursementRecon_ID);

	/** Get Disbursement Reconciliation	  */
	public int getUNS_DisbursementRecon_ID();

    /** Column name UNS_DisbursementRecon_UU */
    public static final String COLUMNNAME_UNS_DisbursementRecon_UU = "UNS_DisbursementRecon_UU";

	/** Set UNS_DisbursementRecon_UU	  */
	public void setUNS_DisbursementRecon_UU (String UNS_DisbursementRecon_UU);

	/** Get UNS_DisbursementRecon_UU	  */
	public String getUNS_DisbursementRecon_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
