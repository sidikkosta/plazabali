/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_DisbursRecon_Amt
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_DisbursRecon_Amt extends PO implements I_UNS_DisbursRecon_Amt, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190516L;

    /** Standard Constructor */
    public X_UNS_DisbursRecon_Amt (Properties ctx, int UNS_DisbursRecon_Amt_ID, String trxName)
    {
      super (ctx, UNS_DisbursRecon_Amt_ID, trxName);
      /** if (UNS_DisbursRecon_Amt_ID == 0)
        {
			setDisbursementAmt (Env.ZERO);
// 0
			setUNS_DisbursRecon_Amt_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_DisbursRecon_Amt (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_DisbursRecon_Amt[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BankStatementLine getC_BankStatementLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankStatementLine)MTable.get(getCtx(), org.compiere.model.I_C_BankStatementLine.Table_Name)
			.getPO(getC_BankStatementLine_ID(), get_TrxName());	}

	/** Set Bank statement line.
		@param C_BankStatementLine_ID 
		Line on a statement from this Bank
	  */
	public void setC_BankStatementLine_ID (int C_BankStatementLine_ID)
	{
		if (C_BankStatementLine_ID < 1) 
			set_Value (COLUMNNAME_C_BankStatementLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankStatementLine_ID, Integer.valueOf(C_BankStatementLine_ID));
	}

	/** Get Bank statement line.
		@return Line on a statement from this Bank
	  */
	public int getC_BankStatementLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankStatementLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Disbursement Amount.
		@param DisbursementAmt Disbursement Amount	  */
	public void setDisbursementAmt (BigDecimal DisbursementAmt)
	{
		set_Value (COLUMNNAME_DisbursementAmt, DisbursementAmt);
	}

	/** Get Disbursement Amount.
		@return Disbursement Amount	  */
	public BigDecimal getDisbursementAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DisbursementAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_UNS_DisbursementRecon getUNS_DisbursementRecon() throws RuntimeException
    {
		return (I_UNS_DisbursementRecon)MTable.get(getCtx(), I_UNS_DisbursementRecon.Table_Name)
			.getPO(getUNS_DisbursementRecon_ID(), get_TrxName());	}

	/** Set Disbursement Reconciliation.
		@param UNS_DisbursementRecon_ID Disbursement Reconciliation	  */
	public void setUNS_DisbursementRecon_ID (int UNS_DisbursementRecon_ID)
	{
		if (UNS_DisbursementRecon_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursementRecon_ID, Integer.valueOf(UNS_DisbursementRecon_ID));
	}

	/** Get Disbursement Reconciliation.
		@return Disbursement Reconciliation	  */
	public int getUNS_DisbursementRecon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursementRecon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Disbursement Recon Amount.
		@param UNS_DisbursRecon_Amt_ID Disbursement Recon Amount	  */
	public void setUNS_DisbursRecon_Amt_ID (int UNS_DisbursRecon_Amt_ID)
	{
		if (UNS_DisbursRecon_Amt_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Amt_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Amt_ID, Integer.valueOf(UNS_DisbursRecon_Amt_ID));
	}

	/** Get Disbursement Recon Amount.
		@return Disbursement Recon Amount	  */
	public int getUNS_DisbursRecon_Amt_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursRecon_Amt_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_DisbursRecon_Amt_UU.
		@param UNS_DisbursRecon_Amt_UU UNS_DisbursRecon_Amt_UU	  */
	public void setUNS_DisbursRecon_Amt_UU (String UNS_DisbursRecon_Amt_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Amt_UU, UNS_DisbursRecon_Amt_UU);
	}

	/** Get UNS_DisbursRecon_Amt_UU.
		@return UNS_DisbursRecon_Amt_UU	  */
	public String getUNS_DisbursRecon_Amt_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_DisbursRecon_Amt_UU);
	}
}