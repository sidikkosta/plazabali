/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDisbursReconLine extends X_UNS_DisbursRecon_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8211455731555556026L;

	/**
	 * @param ctx
	 * @param UNS_DisbursRecon_Line_ID
	 * @param trxName
	 */
	public MUNSDisbursReconLine(Properties ctx, int UNS_DisbursRecon_Line_ID,
			String trxName) {
		super(ctx, UNS_DisbursRecon_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDisbursReconLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		setDifferenceAmt(getOriginalAmt().subtract(getNetAmount()));
		boolean isRefund = getUNS_DisbursRecon_Batch().isRefund();
		if(newRecord && !isRefund)
		{
			String cardNo = getCardNo();
			cardNo = cardNo.replace("X", "*");
			int bankID = getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getC_BankAccount_ID();
			String selectSQL = "SELECT ct.UNS_CardTrxDetail_ID FROM UNS_CardTrxDetail ct"
					+ " INNER JOIN UNS_PaymentTrx trx ON trx.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
					+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = trx.UNS_POSPayment_ID"
					+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ct.UNS_EDC_ID"
					+ " WHERE NOT EXISTS (SELECT 1 FROM UNS_DisbursRecon_Line l"
					+ " WHERE l.UNS_CardTrxDetail_ID = ct.UNS_CardTrxDetail_ID)"
					+ " AND ct.BatchNo <> 'VO' AND edc.C_BankAccount_ID = " + bankID
					+ " AND edc.EDCType = '" + getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getEDCType() + "'";
			//No Kartu, TID, Actual Date, Amount
			String whereClause = " AND trx.TrxNo = ? AND edc.IDNo = ? AND DATE_TRUNC('Day', pp.DateTrx) = ?"
					+ " AND trx.TrxAmt = ?";
			int idCardTrx = DB.getSQLValue(get_TrxName(), selectSQL + whereClause, cardNo, getTID(),
					getDateTrx(), getOriginalAmt());
			
			if(idCardTrx <= 0)
			{
				//TID, Actual Date, Batch, Amount
				whereClause = " AND edc.IDNo = ? AND DATE_TRUNC('Day', pp.DateTrx) = ?"
						+ " AND ct.BatchNo = ? AND trx.TrxAmt = ?";
				idCardTrx = DB.getSQLValue(get_TrxName(), selectSQL + whereClause, getTID(),
						getDateTrx(), getUNS_DisbursRecon_Batch().getBatchNo(), getOriginalAmt());
				if(idCardTrx <= 0)
				{
					//TID, Actual Date, Amount
					whereClause = " AND edc.IDNo = ? AND DATE_TRUNC('Day', pp.DateTrx) = ?"
							+ " AND trx.TrxAmt = ?";
					idCardTrx = DB.getSQLValue(get_TrxName(), selectSQL + whereClause, getTID(),
							getDateTrx(), getOriginalAmt());
					if(idCardTrx <= 0)
					{
						//Actual Date, Amount
						whereClause = " AND DATE_TRUNC('Day', pp.DateTrx) = ?"
								+ " AND trx.TrxAmt = ?";
						idCardTrx = DB.getSQLValue(get_TrxName(), selectSQL + whereClause,
								getDateTrx(), getOriginalAmt());
						if(idCardTrx <= 0)
						{
							//TID, Amount, MAX Date (Settle Date - 1)
							whereClause = " AND edc.IDNo = ? AND trx.TrxAmt = ?"
									+ " AND DATE_TRUNC('Day', pp.DateTrx) < ?"
									+ " ORDER BY pp.DateTrx DESC";
							idCardTrx = DB.getSQLValue(get_TrxName(), selectSQL + whereClause,
									getTID(), getOriginalAmt(), 
									getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getDisbursementDate());
							if(idCardTrx <= 0)
								setIsManual(true);
						}
					}
				}
			}
			if(idCardTrx > 0)
			{
				setUNS_CardTrxDetail_ID(idCardTrx);
				setOrg_CardType_ID(DB.getSQLValue(get_TrxName(), "SELECT UNS_CardType_ID FROM UNS_CardTrxDetail"
						+ " WHERE UNS_CardTrxDetail_ID = ?", idCardTrx));
				setShop_ID(DB.getSQLValue(get_TrxName(), "SELECT C_BPartner_ID FROM UNS_POSPayment"
						+ " WHERE UNS_POSPayment_ID = (SELECT UNS_POSPayment_ID FROM UNS_PaymentTrx WHERE"
						+ " UNS_PaymentTrx_ID = (SELECT UNS_PaymentTrx_ID FROM UNS_CardTrxDetail"
						+ " WHERE UNS_CardTrxDetail_ID = ?))", idCardTrx));
			}
		}
//		else if(is_ValueChanged(COLUMNNAME_ExpectedAmount) && !isRefund)
//		{
//			String sql = "SELECT ct.UNS_CardTrxDetail_ID FROM UNS_CardTrxDetail ct"
//					+ " INNER JOIN UNS_PaymentTrx trx ON trx.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
//					+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = trx.UNS_POSPayment_ID"
//					+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ct.UNS_EDC_ID"
//					+ " WHERE trx.TrxAmt = ? AND ct.BatchNo = ? AND DATE_TRUNC('Day', pp.DateTrx) <= ?"
//					+ " AND edc.C_BankAccount_ID = ? AND ct.UNS_CardType_ID = ?"
//					+ " AND NOT EXISTS (SELECT 1 FROM UNS_DisbursRecon_Line l"
//					+ " WHERE l.UNS_CardTrxDetail_ID = ct.UNS_CardTrxDetail_ID)"
//					+ " ORDER BY pp.DateTrx ASC";
//			
//			int idCardTrx = DB.getSQLValue(get_TrxName(), sql,
//					new Object[]{getExpectedAmount(), getUNS_DisbursRecon_Batch().getBatchNo(),
//						getDateTrx(), getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getC_BankAccount_ID()
//							, getUNS_CardType_ID()});
//			
//			if(idCardTrx > 0)
//			{
//				setUNS_CardTrxDetail_ID(idCardTrx);
//				setOrg_CardType_ID(getUNS_CardType_ID());
//			}
//			else
//			{
//				sql = "SELECT ct.UNS_CardTrxDetail_ID FROM UNS_CardTrxDetail ct"
//						+ " INNER JOIN UNS_PaymentTrx trx ON trx.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
//						+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = trx.UNS_POSPayment_ID"
//						+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ct.UNS_EDC_ID"
//						+ " WHERE trx.TrxAmt = ? AND ct.BatchNo = ? AND DATE_TRUNC('Day', pp.DateTrx) <= ?"
//						+ " AND edc.C_BankAccount_ID = ?"
//						+ " AND NOT EXISTS (SELECT 1 FROM UNS_DisbursRecon_Line l"
//						+ " WHERE l.UNS_CardTrxDetail_ID = ct.UNS_CardTrxDetail_ID)"
//						+ " ORDER BY pp.DateTrx ASC";
//				
//				idCardTrx = DB.getSQLValue(get_TrxName(), sql,
//						new Object[]{getExpectedAmount(), getUNS_DisbursRecon_Batch().getBatchNo(),
//							getDateTrx(), getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getC_BankAccount_ID()});
//				
//				if(idCardTrx > 0)
//				{
//					setUNS_CardTrxDetail_ID(idCardTrx);
//					setOrg_CardType_ID(DB.getSQLValue(get_TrxName(), "SELECT UNS_CardType_ID FROM UNS_CardTrxDetail"
//							+ " WHERE UNS_CardTrxDetail_ID = ?", idCardTrx));
//				}
//				else
//					setUNS_CardTrxDetail_ID(-1);
//			}
//			
//			setOverchargedAmount(getOriginalAmt().subtract(getExpectedAmount()));
//		}
		
		if(newRecord && isRefund)
		{
			String sql = "SELECT ref.UNS_CardType_ID, ref.UNS_CardTrxDetail_ID,"
					+ " l.UNS_DisbursRecon_Line_ID FROM UNS_DisbursRecon_Line l"
					+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
					+ " INNER JOIN UNS_DisbursementRecon r ON r.UNS_DisbursementRecon_ID = b.UNS_DisbursementRecon_ID"
					+ " INNER JOIN UNS_CardTrxDetail ct ON ct.UNS_CardTrxDetail_ID = l.UNS_CardTrxDetail_ID"
					+ " INNER JOIN UNS_PaymentTrx pt ON pt.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
					+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
					+ " INNER JOIN C_DocType dt ON dt.C_DocType_ID = pp.C_DocType_ID"
					+ " INNER JOIN UNS_POSTrx trx ON trx.UNS_POSTrx_ID = pp.UNS_POSTrx_ID"
					+ " INNER JOIN UNS_POSTrx rtrx ON rtrx.Reference_ID = trx.UNS_POSTrx_ID"
					+ " INNER JOIN UNS_POSPayment rpp ON rpp.UNS_POSTrx_ID = rtrx.UNS_POSTrx_ID"
					+ " INNER JOIN UNS_PaymentTrx rpt ON rpt.UNS_POSPayment_ID = rpp.UNS_POSPayment_ID"
					+ " INNER JOIN UNS_CardTrxDetail ref ON ref.UNS_PaymentTrx_ID = rpt.UNS_PaymentTrx_ID"
					+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ref.UNS_EDC_ID"
					+ " WHERE r.DocStatus IN ('CO', 'CL') AND edc.C_BankAccount_ID = ?"
					+ " AND dt.DocBaseType = 'PRR' AND ct.UNS_CardType_ID = ref.UNS_CardType_ID"
					+ " AND CAST(rpp.DateTrx AS Date) <= ? AND ct.UNS_EDC_ID = ref.UNS_EDC_ID"
					+ " AND (l.OriginalAmt = ? OR l.ExpectedAmount = ?) AND edc.EDCType = ?";
			
			List<Object> oo = DB.getSQLValueObjectsEx(get_TrxName(), sql, 
					new Object[]{
									getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getC_BankAccount_ID(),
									getDateTrx(), getOriginalAmt(), getOriginalAmt(),
									getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getEDCType()
								});
			if(oo != null)
			{
				setOrg_CardType_ID(Integer.valueOf(oo.get(0).toString()));
				setUNS_CardTrxDetail_ID(Integer.valueOf(oo.get(1).toString()));
				MUNSDisbursReconLine prevLine = new MUNSDisbursReconLine(getCtx(), 
						Integer.valueOf(oo.get(2).toString()), get_TrxName());
				setOriginalNetAmount(prevLine.getNetAmount());
				setOriginalDifferenceAmt(prevLine.getDifferenceAmt());
				setShop_ID(prevLine.getShop_ID());
			}
			else
				setIsManual(true);
			
//			int refID = DB.getSQLValue(get_TrxName(), sql,
//					getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getC_BankAccount_ID()
//						, getDateTrx(), getNetAmount(), getOriginalAmt());
			
//			if(refID > 0)
//			{
//				setExpectedAmt = false;
//				setUNS_CardTrxDetail_ID(idCardTrx);
//			}
		}
//		else if(is_ValueChanged(COLUMNNAME_ExpectedAmount) && isRefund)
//		{
//			String sql = "SELECT l.UNS_DisbursRecon_Line_ID FROM UNS_DisbursRecon_Line l"
//					+ " INNER JOIN UNS_CardTrxDetail ct ON ct.UNS_CardTrxDetail_ID = l.UNS_CardTrxDetail_ID"
//					+ " INNER JOIN UNS_EDC edc ON edc.UNS_EDC_ID = ct.UNS_EDC_ID"
//					+ " INNER JOIN UNS_PaymentTrx pt ON pt.UNS_PaymentTrx_ID = ct.UNS_PaymentTrx_ID"
//					+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
//					+ " WHERE edc.C_BankAccount_ID = ? AND DATE_TRUNC('Day', pp.DateTrx) <= ?"
//					+ " AND (l.NetAmt = ? OR l.ExpectedAmt = ?)";
//			int idCardTrx = DB.getSQLValue(get_TrxName(), sql,
//					getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon().getC_BankAccount_ID()
//						, getDateTrx(), getExpectedAmount(), getExpectedAmount());
//			
//			if(idCardTrx > 0)
//				setUNS_CardTrxDetail_ID(idCardTrx);
//			else
//				setUNS_CardTrxDetail_ID(-1);
//		}
//		
//		if(newRecord || is_ValueChanged(COLUMNNAME_UNS_CardTrxDetail_ID))
//		{
//			if(getUNS_CardTrxDetail_ID() <= 0)
//			{
//				setShop_ID(-1);
//				setOrg_CardType_ID(-1);
//				setExpectedAmount(Env.ZERO);
//				setOverchargedAmount(Env.ZERO);
//			}
//			else
//			{
//				String sql = "SELECT pt.C_BPartner_ID FROM UNS_POSTrx pt"
//						+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSTrx_ID = pt.UNS_POSTrx_ID"
//						+ " INNER JOIN UNS_PaymentTrx pay ON pay.UNS_POSPayment_ID = pp.UNS_POSPayment_ID"
//						+ " INNER JOIN UNS_CardTrxDetail ct ON ct.UNS_PaymentTrx_ID = pay.UNS_PaymentTrx_ID"
//						+ " WHERE ct.UNS_CardTrxDetail_ID = ?";
//				int bpID = DB.getSQLValue(get_TrxName(), sql, getUNS_CardTrxDetail_ID());
//				if(bpID > 0)
//					setShop_ID(bpID);
//				
//				setOrg_CardType_ID(DB.getSQLValue(get_TrxName(), "SELECT UNS_CardType_ID FROM UNS_CardTrxDetail"
//						+ " WHERE UNS_CardTrxDetail_ID = ?", getUNS_CardTrxDetail_ID()));
//				if(setExpectedAmt)
//				{
//					sql = "SELECT ABS(TrxAmt) FROM UNS_PaymentTrx WHERE UNS_PaymentTrx_ID = "
//							+ " (SELECT UNS_PaymentTrx_ID FROM UNS_CardTrxDetail"
//							+ " WHERE UNS_CardTrxDetail_ID = ?)";
//					BigDecimal trxAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_CardTrxDetail_ID());
//					if(trxAmt.compareTo(getOriginalAmt()) != 0)
//					{
//						setExpectedAmount(trxAmt);
//						setOverchargedAmount(getOriginalAmt().subtract(trxAmt));
//					}
//				}
//			}
//		}
		
		if(getExpectedAmount().signum() == 0)
			setOverchargedAmount(Env.ZERO);
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!success)
			return false;
		
		upHeader();
		
		return true;
	}
	
	protected boolean afterDelete(boolean success)
	{
		if(!success)
			return false;
		
		upHeader();
		
		return true;
	}
	
	private void upHeader()
	{
		String sql = "";
		if(!getUNS_DisbursRecon_Batch().isRefund())
		{
			sql = "UPDATE UNS_DisbursRecon_Batch b SET OriginalAmt ="
					+ " (SELECT COALESCE(SUM(l.OriginalAmt),0) FROM UNS_DisbursRecon_Line l"
					+ " WHERE b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID),"
					+ " NetAmount = (SELECT COALESCE(SUM(l.NetAmount),0) FROM UNS_DisbursRecon_Line l"
					+ " WHERE b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID),"
					+ " DisbursementAmt = COALESCE((SELECT COALESCE(SUM(l.NetAmount),0) FROM UNS_DisbursRecon_Line l"
					+ " WHERE b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID),0) - b.AmountRefunded,"
					+ " DifferenceAmt = (SELECT COALESCE(SUM(l.OriginalAmt),0)-COALESCE(SUM(l.NetAmount),0) FROM UNS_DisbursRecon_Line l"
					+ " WHERE b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID)"
					+ " WHERE b.UNS_DisbursRecon_Batch_ID = ?";
			if(DB.executeUpdate(sql, getUNS_DisbursRecon_Batch_ID(), get_TrxName()) < 0)
				throw new AdempiereException("Failed when trying update original amount.");
		}
		else
		{
			sql = "UPDATE UNS_DisbursRecon_Batch b SET NotMatchedRefundAmt ="
					+ " (SELECT COALESCE(SUM(l.OriginalAmt),0) FROM UNS_DisbursRecon_Line l"
					+ " WHERE l.UNS_DisbursRecon_Batch_ID = b.UNS_DisbursRecon_Batch_ID"
					+ " AND (l.UNS_CardTrxDetail_ID <= 0 OR l.UNS_CardTrxDetail_ID IS NULL)),"
					+ " AmountRefunded = "
					+ " (SELECT COALESCE(SUM(l.OriginalAmt),0) FROM UNS_DisbursRecon_Line l"
					+ " WHERE l.UNS_DisbursRecon_Batch_ID = b.UNS_DisbursRecon_Batch_ID"
					+ " AND l.UNS_CardTrxDetail_ID > 0)"
					+ " WHERE b.UNS_DisbursRecon_Batch_ID = ?";
			if(DB.executeUpdate(sql, getUNS_DisbursRecon_Batch_ID(), get_TrxName()) < 0)
				throw new AdempiereException("Failed when trying update original amount.");
		}
		
		sql = "UPDATE UNS_DisbursRecon_Batch b SET NotMatchedAmt ="
				+ " (SELECT COALESCE(SUM(l.OriginalAmt),0)"
				+ " FROM UNS_DisbursRecon_Line l WHERE l.UNS_DisbursRecon_Batch_ID = b.UNS_DisbursRecon_Batch_ID"
				+ " AND (l.UNS_CardTrxDetail_ID <= 0 OR l.UNS_CardTrxDetail_ID IS NULL)),"
				+ " MatchedAmt = "
				+ " (SELECT COALESCE(SUM(l.OriginalAmt),0)"
				+ " FROM UNS_DisbursRecon_Line l WHERE l.UNS_DisbursRecon_Batch_ID = b.UNS_DisbursRecon_Batch_ID"
				+ " AND l.UNS_CardTrxDetail_ID > 0)"
				+ " WHERE b.UNS_DisbursRecon_Batch_ID = ?";
		if(DB.executeUpdate(sql, getUNS_DisbursRecon_Batch_ID(), get_TrxName()) < 0)
			throw new AdempiereException("Failed when trying update matched or not matched amount.");
		
//		sql = "UPDATE UNS_DisbursementRecon r SET DisbursementAmt ="
//				+ " (SELECT SUM(l.NetAmount) FROM UNS_DisbursRecon_Line l"
//				+ " WHERE EXISTS (SELECT 1 FROM UNS_DisbursRecon_Batch b WHERE"
//				+ " b.IsRefund = 'N' AND b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID AND"
//				+ " b.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID))"
//				+ " - (SELECT SUM(b.AmountRefunded) FROM UNS_DisbursRecon_Batch b"
//				+ " WHERE b.IsRefund = 'N' AND b.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID)"
//				+ " WHERE r.UNS_DisbursementRecon_ID = ?";
		
		sql = "UPDATE UNS_DisbursementRecon r SET DisbursementAmt ="
				+ " (SELECT COALESCE(SUM(b.DisbursementAmt),0) FROM UNS_DisbursRecon_Batch b "
				+ " WHERE b.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID"
				+ " AND b.IsRefund = 'N'), AmountRefunded ="
				+ " (SELECT COALESCE(SUM(b.AmountRefunded),0) FROM UNS_DisbursRecon_Batch b "
				+ " WHERE b.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID"
				+ " AND b.IsRefund = 'Y'), NotMatchedRefundAmt ="
				+ " (SELECT COALESCE(SUM(b.NotMatchedRefundAmt),0) FROM UNS_DisbursRecon_Batch b "
				+ " WHERE b.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID"
				+ " AND b.IsRefund = 'Y'), OverchargedAmount ="
				+ " (SELECT COALESCE(SUM(l.OverchargedAmount),0) FROM UNS_DisbursRecon_Line l"
				+ " WHERE EXISTS (SELECT 1 FROM UNS_DisbursRecon_Batch b WHERE"
				+ " b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " AND b.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID)),"
				+ " NotMatchedAmt = (SELECT COALESCE(SUM(CASE WHEN b.IsRefund = 'Y' THEN"
				+ " b.NotMatchedAmt * -1 ELSE b.NotMatchedAmt END),0) FROM UNS_DisbursRecon_Batch b WHERE b.UNS_DisbursementRecon_ID"
				+ " = r.UNS_DisbursementRecon_ID), MatchedAmt ="
				+ " (SELECT COALESCE(SUM(CASE WHEN b.IsRefund = 'Y' THEN"
				+ " b.MatchedAmt * -1 ELSE b.MatchedAmt END),0) FROM UNS_DisbursRecon_Batch b WHERE b.UNS_DisbursementRecon_ID"
				+ " = r.UNS_DisbursementRecon_ID), DifferenceAmt ="
				+ " (SELECT COALESCE(SUM(DifferenceAmt),0) FROM UNS_DisbursRecon_Batch b WHERE b.UNS_DisbursementRecon_ID"
				+ "= r.UNS_DisbursementRecon_ID)"
				+ " WHERE r.UNS_DisbursementRecon_ID = ?";
		if(DB.executeUpdate(sql, getUNS_DisbursRecon_Batch().getUNS_DisbursementRecon_ID(), get_TrxName()) < 0)
			throw new AdempiereException("Failed when trying update original amount.");
	}
	
	public static MUNSDisbursReconLine[] getNeedCorrection(MUNSDisbursementRecon parent)
	{
		List<MUNSDisbursReconLine> list = new ArrayList<>();
		String sql = "SELECT l.* FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " WHERE l.IsNeedCTCorrection = 'Y' AND b.UNS_DisbursementRecon_ID = ?"
				+ " AND l.UNS_CardType_ID > 0 AND l.Org_CardType_ID > 0 AND l.UNS_CardType_ID <> l.Org_CardType_ID";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, parent.get_TrxName());
			stmt.setInt(1, parent.get_ID());
			rs = stmt.executeQuery();
			while (rs.next())
			{
				list.add(new MUNSDisbursReconLine(parent.getCtx(), rs, parent.get_TrxName()));
			}
		} 
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		return list.toArray(new MUNSDisbursReconLine[list.size()]);
	}
	
	public static MUNSDisbursReconLine[] getNotMatchedRefund(MUNSDisbursementRecon parent)
	{
		List<MUNSDisbursReconLine> list = new ArrayList<>();
		String sql = "SELECT l.* FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " WHERE b.IsRefund = 'Y' AND b.UNS_DisbursementRecon_ID = ? AND (l.UNS_CardTrxDetail_ID IS NULL"
				+ " OR l.UNS_CardTrxDetail_ID <=0)";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, parent.get_TrxName());
			stmt.setInt(1, parent.get_ID());
			rs = stmt.executeQuery();
			while (rs.next())
			{
				list.add(new MUNSDisbursReconLine(parent.getCtx(), rs, parent.get_TrxName()));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		return list.toArray(new MUNSDisbursReconLine[list.size()]);
	}
	
	public static Hashtable<Integer, BigDecimal> getNotMatchedRefundAcct(MUNSDisbursementRecon parent)
	{
		Hashtable<Integer, BigDecimal> mapAcct = new Hashtable<>();

		String sql = "SELECT ct.Overcharge_Acct, SUM(l.OriginalAmt) FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " INNER JOIN UNS_CardType ct ON ct.UNS_CardType_ID = l.UNS_CardType_ID"
				+ " WHERE b.IsRefund = 'Y' AND b.UNS_DisbursementRecon_ID = ? AND (l.UNS_CardTrxDetail_ID IS NULL"
				+ " OR l.UNS_CardTrxDetail_ID <=0)"
				+ " GROUP BY ct.Overcharge_Acct";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, parent.get_TrxName());
			stmt.setInt(1, parent.get_ID());
			rs = stmt.executeQuery();
			while (rs.next())
			{
				BigDecimal oriAmt = rs.getBigDecimal(2);
				BigDecimal totalOriAmt = mapAcct.get(rs.getInt(1));
				if(totalOriAmt != null)
					totalOriAmt = totalOriAmt.add(oriAmt);
				else
					totalOriAmt = oriAmt;
				mapAcct.put(rs.getInt(1), totalOriAmt);
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		return mapAcct;
	}
	
	public static MUNSDisbursReconLine[] getNotMatchedBatchNo(MUNSDisbursementRecon parent)
	{
		List<MUNSDisbursReconLine> list = new ArrayList<>();
		String sql = "SELECT l.* FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " INNER JOIN UNS_CardTrxDetail ct ON ct.UNS_CardTrxDetail_ID = l.UNS_CardTrxDetail_ID"
				+ " WHERE ct.BatchNo <> b.BatchNo AND b.UNS_DisbursementRecon_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, parent.get_TrxName());
			stmt.setInt(1, parent.get_ID());
			rs = stmt.executeQuery();
			while (rs.next())
			{
				list.add(new MUNSDisbursReconLine(parent.getCtx(), rs, parent.get_TrxName()));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		return list.toArray(new MUNSDisbursReconLine[list.size()]);
	}
}