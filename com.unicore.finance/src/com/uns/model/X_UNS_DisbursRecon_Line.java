/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_DisbursRecon_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_DisbursRecon_Line extends PO implements I_UNS_DisbursRecon_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20200102L;

    /** Standard Constructor */
    public X_UNS_DisbursRecon_Line (Properties ctx, int UNS_DisbursRecon_Line_ID, String trxName)
    {
      super (ctx, UNS_DisbursRecon_Line_ID, trxName);
      /** if (UNS_DisbursRecon_Line_ID == 0)
        {
			setDifferenceAmt (Env.ZERO);
// 0
			setExpectedAmount (Env.ZERO);
// 0
			setIsManual (false);
// N
			setIsNeedCTCorrection (false);
// N
			setNetAmount (Env.ZERO);
// 0
			setOriginalAmt (Env.ZERO);
// 0
			setOriginalDifferenceAmt (Env.ZERO);
// 0
			setOriginalNetAmount (Env.ZERO);
// 0
			setOverchargedAmount (Env.ZERO);
// 0
			setUNS_DisbursRecon_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_DisbursRecon_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_DisbursRecon_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Card No.
		@param CardNo Card No	  */
	public void setCardNo (String CardNo)
	{
		set_Value (COLUMNNAME_CardNo, CardNo);
	}

	/** Get Card No.
		@return Card No	  */
	public String getCardNo () 
	{
		return (String)get_Value(COLUMNNAME_CardNo);
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_Value (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Difference.
		@param DifferenceAmt 
		Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt)
	{
		set_Value (COLUMNNAME_DifferenceAmt, DifferenceAmt);
	}

	/** Get Difference.
		@return Difference Amount
	  */
	public BigDecimal getDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Expected Amount.
		@param ExpectedAmount Expected Amount	  */
	public void setExpectedAmount (BigDecimal ExpectedAmount)
	{
		set_Value (COLUMNNAME_ExpectedAmount, ExpectedAmount);
	}

	/** Get Expected Amount.
		@return Expected Amount	  */
	public BigDecimal getExpectedAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ExpectedAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Manual.
		@param IsManual 
		This is a manual process
	  */
	public void setIsManual (boolean IsManual)
	{
		set_Value (COLUMNNAME_IsManual, Boolean.valueOf(IsManual));
	}

	/** Get Manual.
		@return This is a manual process
	  */
	public boolean isManual () 
	{
		Object oo = get_Value(COLUMNNAME_IsManual);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Need Card Type Correction.
		@param IsNeedCTCorrection Need Card Type Correction	  */
	public void setIsNeedCTCorrection (boolean IsNeedCTCorrection)
	{
		set_Value (COLUMNNAME_IsNeedCTCorrection, Boolean.valueOf(IsNeedCTCorrection));
	}

	/** Get Need Card Type Correction.
		@return Need Card Type Correction	  */
	public boolean isNeedCTCorrection () 
	{
		Object oo = get_Value(COLUMNNAME_IsNeedCTCorrection);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Net Amount.
		@param NetAmount Net Amount	  */
	public void setNetAmount (BigDecimal NetAmount)
	{
		set_Value (COLUMNNAME_NetAmount, NetAmount);
	}

	/** Get Net Amount.
		@return Net Amount	  */
	public BigDecimal getNetAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Card Type.
		@param Org_CardType_ID Original Card Type	  */
	public void setOrg_CardType_ID (int Org_CardType_ID)
	{
		if (Org_CardType_ID < 1) 
			set_Value (COLUMNNAME_Org_CardType_ID, null);
		else 
			set_Value (COLUMNNAME_Org_CardType_ID, Integer.valueOf(Org_CardType_ID));
	}

	/** Get Original Card Type.
		@return Original Card Type	  */
	public int getOrg_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Org_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Original Amount.
		@param OriginalAmt Original Amount	  */
	public void setOriginalAmt (BigDecimal OriginalAmt)
	{
		set_Value (COLUMNNAME_OriginalAmt, OriginalAmt);
	}

	/** Get Original Amount.
		@return Original Amount	  */
	public BigDecimal getOriginalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Difference Amount.
		@param OriginalDifferenceAmt Original Difference Amount	  */
	public void setOriginalDifferenceAmt (BigDecimal OriginalDifferenceAmt)
	{
		set_Value (COLUMNNAME_OriginalDifferenceAmt, OriginalDifferenceAmt);
	}

	/** Get Original Difference Amount.
		@return Original Difference Amount	  */
	public BigDecimal getOriginalDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalDifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Net Amount.
		@param OriginalNetAmount Original Net Amount	  */
	public void setOriginalNetAmount (BigDecimal OriginalNetAmount)
	{
		set_Value (COLUMNNAME_OriginalNetAmount, OriginalNetAmount);
	}

	/** Get Original Net Amount.
		@return Original Net Amount	  */
	public BigDecimal getOriginalNetAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalNetAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Overcharged Amount.
		@param OverchargedAmount Overcharged Amount	  */
	public void setOverchargedAmount (BigDecimal OverchargedAmount)
	{
		set_Value (COLUMNNAME_OverchargedAmount, OverchargedAmount);
	}

	/** Get Overcharged Amount.
		@return Overcharged Amount	  */
	public BigDecimal getOverchargedAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OverchargedAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getShop_ID(), get_TrxName());	}

	/** Set Shop.
		@param Shop_ID 
		Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID)
	{
		if (Shop_ID < 1) 
			set_Value (COLUMNNAME_Shop_ID, null);
		else 
			set_Value (COLUMNNAME_Shop_ID, Integer.valueOf(Shop_ID));
	}

	/** Get Shop.
		@return Identifies a Shop
	  */
	public int getShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Shop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Status.
		@param Status 
		Status of the currently running check
	  */
	public void setStatus (String Status)
	{
		set_Value (COLUMNNAME_Status, Status);
	}

	/** Get Status.
		@return Status of the currently running check
	  */
	public String getStatus () 
	{
		return (String)get_Value(COLUMNNAME_Status);
	}

	/** Set TID.
		@param TID TID	  */
	public void setTID (String TID)
	{
		set_Value (COLUMNNAME_TID, TID);
	}

	/** Get TID.
		@return TID	  */
	public String getTID () 
	{
		return (String)get_Value(COLUMNNAME_TID);
	}

	/** Set Card Trx Detail.
		@param UNS_CardTrxDetail_ID Card Trx Detail	  */
	public void setUNS_CardTrxDetail_ID (int UNS_CardTrxDetail_ID)
	{
		if (UNS_CardTrxDetail_ID < 1) 
			set_Value (COLUMNNAME_UNS_CardTrxDetail_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_CardTrxDetail_ID, Integer.valueOf(UNS_CardTrxDetail_ID));
	}

	/** Get Card Trx Detail.
		@return Card Trx Detail	  */
	public int getUNS_CardTrxDetail_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardTrxDetail_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Card Type.
		@param UNS_CardType_ID Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID)
	{
		if (UNS_CardType_ID < 1) 
			set_Value (COLUMNNAME_UNS_CardType_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_CardType_ID, Integer.valueOf(UNS_CardType_ID));
	}

	/** Get Card Type.
		@return Card Type	  */
	public int getUNS_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_UNS_DisbursementRecon getUNS_DisbursementRecon() throws RuntimeException
    {
		return (I_UNS_DisbursementRecon)MTable.get(getCtx(), I_UNS_DisbursementRecon.Table_Name)
			.getPO(getUNS_DisbursementRecon_ID(), get_TrxName());	}

	/** Set Disbursement Reconciliation.
		@param UNS_DisbursementRecon_ID Disbursement Reconciliation	  */
	public void setUNS_DisbursementRecon_ID (int UNS_DisbursementRecon_ID)
	{
		if (UNS_DisbursementRecon_ID < 1) 
			set_Value (COLUMNNAME_UNS_DisbursementRecon_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_DisbursementRecon_ID, Integer.valueOf(UNS_DisbursementRecon_ID));
	}

	/** Get Disbursement Reconciliation.
		@return Disbursement Reconciliation	  */
	public int getUNS_DisbursementRecon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursementRecon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_UNS_DisbursRecon_Batch getUNS_DisbursRecon_Batch() throws RuntimeException
    {
		return (I_UNS_DisbursRecon_Batch)MTable.get(getCtx(), I_UNS_DisbursRecon_Batch.Table_Name)
			.getPO(getUNS_DisbursRecon_Batch_ID(), get_TrxName());	}

	/** Set Disburs. Reconciliation Batch.
		@param UNS_DisbursRecon_Batch_ID Disburs. Reconciliation Batch	  */
	public void setUNS_DisbursRecon_Batch_ID (int UNS_DisbursRecon_Batch_ID)
	{
		if (UNS_DisbursRecon_Batch_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Batch_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Batch_ID, Integer.valueOf(UNS_DisbursRecon_Batch_ID));
	}

	/** Get Disburs. Reconciliation Batch.
		@return Disburs. Reconciliation Batch	  */
	public int getUNS_DisbursRecon_Batch_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursRecon_Batch_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Disburs. Recon Line.
		@param UNS_DisbursRecon_Line_ID Disburs. Recon Line	  */
	public void setUNS_DisbursRecon_Line_ID (int UNS_DisbursRecon_Line_ID)
	{
		if (UNS_DisbursRecon_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Line_ID, Integer.valueOf(UNS_DisbursRecon_Line_ID));
	}

	/** Get Disburs. Recon Line.
		@return Disburs. Recon Line	  */
	public int getUNS_DisbursRecon_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DisbursRecon_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_DisbursRecon_Line_UU.
		@param UNS_DisbursRecon_Line_UU UNS_DisbursRecon_Line_UU	  */
	public void setUNS_DisbursRecon_Line_UU (String UNS_DisbursRecon_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_DisbursRecon_Line_UU, UNS_DisbursRecon_Line_UU);
	}

	/** Get UNS_DisbursRecon_Line_UU.
		@return UNS_DisbursRecon_Line_UU	  */
	public String getUNS_DisbursRecon_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_DisbursRecon_Line_UU);
	}
}