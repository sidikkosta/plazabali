/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankStatement;
import org.compiere.model.MBankStatementLine;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Msg;

import com.uns.base.model.Query;
import com.uns.util.UNSApps;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDisbursementRecon extends X_UNS_DisbursementRecon implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7665882306533039490L;

	/**
	 * @param ctx
	 * @param UNS_DisbursementRecon_ID
	 * @param trxName
	 */
	public MUNSDisbursementRecon(Properties ctx, int UNS_DisbursementRecon_ID,
			String trxName) {
		super(ctx, UNS_DisbursementRecon_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDisbursementRecon(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
//		String sql = "SELECT DocumentNo FROM UNS_DisbursementRecon"
//				+ " WHERE DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
//				+ " AND C_BankAccount_ID = ? AND UNS_DisbursementRecon_ID <> ?";
//		String docNo = DB.getSQLValueString(get_TrxName(), sql, getC_BankAccount_ID(), get_ID());
		
//		if(!Util.isEmpty(docNo, true)){
//			log.saveError("Error", "Duplicate record.! " + docNo);
//			return false;
//		}
		
		return true;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSDisbursementRecon[");
		sb.append(get_ID()).append("-").append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_DisbursementRecon_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	protected boolean beforeDelete()
	{
		if(!deleteExistingRecord())
		{
			log.saveError("Error", "Failed when trying delete lines.");
			return false;
		}
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		boolean needFull = MSysConfig.getBooleanValue(MSysConfig.EDC_SETTLEMENT_FULL_DISBURSED_AMOUNT, true);
		if(needFull)
		{
			BigDecimal ttAmt = getStmtAmt();
			if(ttAmt == null)
			{
				m_processMsg = "Define settlement amount on tab receives.";
				return DocAction.STATUS_Invalid;
			}
			else if(ttAmt.compareTo(getDisbursementAmt()) != 0)
			{
				m_processMsg = "Not same between settlement amount with receives amount defined. " + ttAmt;
				return DocAction.STATUS_Invalid;
			}
		}
		
		if(!isAllowNotMatchedTrx())
		{
			String sql = "SELECT COUNT(l.*) FROM UNS_DisbursRecon_Line l"
					+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
					+ " WHERE b.UNS_DisbursementRecon_ID = ? AND (l.UNS_CardTrxDetail_ID < 0 OR l.UNS_CardTrxDetail_ID IS NULL)";
			int count = DB.getSQLValue(get_TrxName(), sql, get_ID());
			if(count > 0)
			{
				m_processMsg = count + (count > 1 ? " transactions" : " transaction") + " not matched has found."
						+ " You can allowed this action with check on flag Allow Not Matched Transaction or"
						+ " check again this document.";
				return DocAction.STATUS_Invalid;
			}
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		MBankStatement stmt = null;
		if(getC_BankStatement_ID() > 0)
			stmt = new MBankStatement(getCtx(), getC_BankStatement_ID(), get_TrxName());
		else
			stmt = MBankStatement.getOpen(getC_BankAccount_ID(), get_TrxName(), true);
		
		setC_BankStatement_ID(stmt.get_ID());
		
		MUNSDisbursReconAmt[] amts = MUNSDisbursReconAmt.getAmts(this);
		for(int i=0;i<amts.length;i++)
		{
			if(!amts[i].createStatementLine(stmt, getDocumentNo()))
			{
				m_processMsg = CLogger.retrieveErrorString("Can't create statement line.");
				return DocAction.STATUS_Invalid;
			}
		}
		
//		m_processMsg = createStatement();
//		if(m_processMsg != null)
//			return DocAction.STATUS_Invalid;
		
		MUNSDisbursReconLine[] lines = MUNSDisbursReconLine.getNeedCorrection(this);
		for(int i=0;i<lines.length;i++)
		{	
			MUNSCardTypeCorrection corr = new MUNSCardTypeCorrection(lines[i]);
			corr.saveEx();
			
			boolean autoComplete = MSysConfig.getBooleanValue(MSysConfig.AUTO_COMPLETE_CARDTYPE_CORRECTION, false);
			if(autoComplete)
			{ 
				if(!corr.processIt(ACTION_Complete) || !corr.save())
				{
					m_processMsg = corr.getProcessMsg();
					return DocAction.STATUS_Invalid;
				}
			}
		}
		
		lines = MUNSDisbursReconLine.getNotMatchedBatchNo(this);
		for(int i=0;i<lines.length;i++)
		{
			String sql = "UPDATE UNS_CardTrxDetail SET BatchNo = ?, Description = "
					+ " 'Revisi Batch No FROM EDC Settlement :: " + getDocumentNo() + ". Previous Batch ' || BatchNo || '.'"
					+ " WHERE UNS_CardTrxDetail_ID = ?";
			DB.executeUpdate(sql, new Object[]{
					lines[i].getUNS_DisbursRecon_Batch().getBatchNo(), lines[i].getUNS_CardTrxDetail_ID()
					}, false, get_TrxName());
		}
		
		m_processMsg = createRecapitulation(false);
		if(null != m_processMsg)
			return DocAction.STATUS_Invalid;
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		String sql = "UPDATE UNS_DisbursRecon_Line l SET UNS_CardTrxDetail_ID = NULL"
				+ " WHERE EXISTS (SELECT 1 FROM UNS_DisbursRecon_Batch b WHERE b.UNS_DisbursementRecon_ID = ?"
				+ " AND b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID)";
		int count = DB.executeUpdate(sql, get_ID(), get_TrxName());
		if(count < 0)
		{
			m_processMsg = "Failed when trying unlink card transaction detail.";
			return false;
		}
		
		if(!getDocStatus().equals(DOCSTATUS_Completed))
			return true;
		
		m_processMsg = cancelledTrx();
		if(m_processMsg != null)
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		m_processMsg = cancelledTrx();
		if(m_processMsg != null)
			return false;
		
		setProcessed(false);

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 303;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public MUNSDisbursReconBatch[] getBatchs()
	{
		List<MUNSDisbursReconBatch> list = Query.get(getCtx(), com.uns.model.factory.UNSFinanceModelFactory.EXTENSION_ID,
				MUNSDisbursReconBatch.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSDisbursReconBatch[list.size()]);
	}
	
	public MUNSDisbursReconAmt[] getReceives()
	{
		List<MUNSDisbursReconAmt> list = Query.get(getCtx(), com.uns.model.factory.UNSFinanceModelFactory.EXTENSION_ID,
				MUNSDisbursReconAmt.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSDisbursReconAmt[list.size()]);
	}
	
	@SuppressWarnings("unused")
	private String createStatement()
	{	
		MBankStatementLine line = new MBankStatementLine((MBankStatement) getC_BankStatement());
		line.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_ARTransaction);
		line.setStmtAmt(getDisbursementAmt());
		line.setIsManual(false);
		line.setDescription("Create from edc disbursement, No " + getDocumentNo() + ".");
		int chargeID = UNSApps.getRefAsInt(UNSApps.CHRG_INTRANSITCASH);
		line.setC_Currency_ID(303);
		line.setC_Charge_ID(chargeID);
		if(!line.save())
			return "Failed when trying create statement line.";
		
//		setC_BankStatementLine_ID(line.get_ID());
		
		return null;
	}
	
	public static MUNSDisbursementRecon getOpen(Properties ctx, int C_BankAccount_ID, String trxName)
	{
		String whereClause = "C_BankAccount_ID = ? AND DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')";
		MUNSDisbursementRecon recon = Query.get(ctx, com.uns.model.factory.UNSFinanceModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(C_BankAccount_ID).firstOnly();
		
		return recon;
	}
	
	public boolean deleteExistingRecord()
	{
		String sql = "DELETE FROM UNS_DisbursRecon_Line l WHERE EXISTS (SELECT 1"
				+ " FROM UNS_DisbursRecon_Batch b WHERE l.UNS_DisbursRecon_Batch_ID"
				+ " = b.UNS_DisbursRecon_Batch_ID AND b.UNS_DisbursementRecon_ID = ?)";
		boolean success = DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
		
		if(success)
		{
			sql = "DELETE FROM UNS_DisbursRecon_Batch WHERE UNS_DisbursementRecon_ID = ?";
			success = DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
		}
		
		return success;
	}
	
	/**
	 * Delimiter ";"
	 * Key == Shop_ID, CardType
	 * Value == NetAmount, DifferenceAmt, OverchargedAmount
	 */
	public Hashtable<String, BigDecimal> getGroupingCardTypeByShop()
	{
		Hashtable<String, BigDecimal> mapCT = new Hashtable<>();
		
		String sql = "SELECT l.Shop_ID, ct.EDCCommission_Acct AS CardType,"
				+ " SUM(l.DifferenceAmt)"
				+ " FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_CardType ct ON ct.UNS_CardType_ID = COALESCE(l.Org_CardType_ID, l.UNS_CardType_ID)"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " WHERE b.UNS_DisbursementRecon_ID = ?"
				+ " GROUP BY l.Shop_ID, CardType";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				BigDecimal diffAmt = rs.getBigDecimal(3);
				diffAmt = diffAmt.setScale(2);
				BigDecimal totalAmt = mapCT.get(rs.getInt(1) + ";" + rs.getInt(2));
				if(totalAmt != null)
					totalAmt = totalAmt.add(diffAmt);
				else
					totalAmt = diffAmt;
				mapCT.put(rs.getInt(1) + ";" + rs.getInt(2),
						diffAmt);
			}
		} catch (SQLException e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString("Error when trying get summary card type."));
		}
		return mapCT;
	}
	
	/**
	 * Delimiter ";"
	 * Key == Shop_ID, CardType
	 * Value == NetAmount, DifferenceAmt, OverchargedAmount
	 */
	public Hashtable<String, BigDecimal> getGroupingCardType()
	{
		Hashtable<String, BigDecimal> mapCT = new Hashtable<>();
		
		String sql = "SELECT ct.EDCAR_Acct AS ARAcct,"
				+ " SUM(CASE WHEN (l.UNS_CardTrxDetail_ID < 0 OR l.UNS_CardTrxDetail_ID IS NULL) THEN 0"
				+ " WHEN l.ExpectedAmount > 0 THEN l.ExpectedAmount"
				+ " ELSE l.OriginalAmt END)"
				+ " FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " INNER JOIN UNS_CardType ct ON ct.UNS_CardType_ID = COALESCE(l.Org_CardType_ID,l.UNS_CardType_ID)"
				+ " WHERE b.UNS_DisbursementRecon_ID = ?"
				+ " GROUP BY ARAcct";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			
			while (rs.next())
			{ 
				BigDecimal netAmt = rs.getBigDecimal(2);
				netAmt = netAmt.setScale(2, RoundingMode.HALF_DOWN);
				BigDecimal totalNet = mapCT.get(rs.getInt(1) + ";AR");
				if(totalNet != null)
					totalNet = totalNet.add(netAmt);
				else
					totalNet = netAmt;
				mapCT.put(rs.getInt(1) + ";AR", totalNet);
			}
		} catch (SQLException e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString("Error when trying get summary card type."));
		}
		
		sql = "SELECT ct.Overcharge_Acct AS OCAcct, "
				+ " SUM(CASE WHEN (l.UNS_CardTrxDetail_ID < 0 OR l.UNS_CardTrxDetail_ID IS NULL) THEN"
				+ " l.OriginalAmt ELSE l.OverchargedAmount END)"
				+ " FROM UNS_DisbursRecon_Line l"
				+ " INNER JOIN UNS_DisbursRecon_Batch b ON b.UNS_DisbursRecon_Batch_ID = l.UNS_DisbursRecon_Batch_ID"
				+ " INNER JOIN UNS_CardType ct ON ct.UNS_CardType_ID = COALESCE(l.Org_CardType_ID,l.UNS_CardType_ID)"
				+ " WHERE b.UNS_DisbursementRecon_ID = ?"
				+ " GROUP BY OCAcct";
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			
			while (rs.next())
			{	
				BigDecimal overAmt = rs.getBigDecimal(2);
				overAmt = overAmt.setScale(2, RoundingMode.HALF_DOWN);
				BigDecimal totalOver = mapCT.get(rs.getInt(1) + ";OC");
				if(totalOver != null)
					totalOver = totalOver.add(overAmt);
				else
					totalOver = overAmt;
				mapCT.put(rs.getInt(1) + ";OC", totalOver);
			}
		} catch (SQLException e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString("Error when trying get summary card type."));
		}
		return mapCT;
	}
	
	private String createRecapitulation(boolean isVoid)
	{
		MUNSDisbursReconBatch[] batchs = getBatchs();
		
		for(int i=0;i<batchs.length;i++)
		{
			MUNSDisbursReconLine[] lines = batchs[i].getLines();
			
			for(int j=0;j<lines.length;j++)
			{
				if(lines[j].getUNS_CardTrxDetail_ID() > 0)
				{
					String sql = "SELECT UNS_EDC_ID FROM UNS_CardTrxDetail"
							+ " WHERE UNS_CardTrxDetail_ID = ?";
					int edcID = DB.getSQLValue(get_TrxName(), sql, lines[j].getUNS_CardTrxDetail_ID());
					MUNSDailyBankCardTypeRecap daily = MUNSDailyBankCardTypeRecap.getCreate(
							getCtx(), getAD_Org_ID(), edcID, lines[j].getOrg_CardType_ID(), getDisbursementDate(), get_TrxName());
					if(!isVoid)
					{
						daily.setTotalDisbursedAmt((daily.getTotalDisbursedAmt().add(lines[j].getNetAmount())).setScale(2, RoundingMode.HALF_DOWN));
						daily.setDifferenceAmt((daily.getDifferenceAmt().add(lines[j].getDifferenceAmt())).setScale(2, RoundingMode.HALF_DOWN));
					}
					else
					{
						daily.setTotalDisbursedAmt((daily.getTotalDisbursedAmt().subtract(lines[j].getNetAmount())).setScale(2, RoundingMode.HALF_DOWN));
						daily.setDifferenceAmt((daily.getDifferenceAmt().subtract(lines[j].getDifferenceAmt())).setScale(2, RoundingMode.HALF_DOWN));
					}
					if(!daily.save())
						return CLogger.retrieveErrorString("Error when trying create EDC Recapitulation.");
					
					MUNSBankEDCBatchRecap batch = MUNSBankEDCBatchRecap.getCreate(
							getCtx(), getAD_Org_ID(), edcID, getDisbursementDate(), batchs[i].getBatchNo(), get_TrxName());
					if(!isVoid)
					{
						batch.setTotalDisbursedAmt((batch.getTotalDisbursedAmt().add(lines[j].getNetAmount())).setScale(2, RoundingMode.HALF_DOWN));
						batch.setDifferenceAmt((batch.getDifferenceAmt().add(lines[j].getDifferenceAmt())).setScale(2, RoundingMode.HALF_DOWN));
					}
					else
					{
						batch.setTotalDisbursedAmt((batch.getTotalDisbursedAmt().subtract(lines[j].getNetAmount())).setScale(2));
						batch.setDifferenceAmt((batch.getDifferenceAmt().subtract(lines[j].getDifferenceAmt())).setScale(2));
					}
					if(!batch.save())
						return CLogger.retrieveErrorString("Error when trying create EDC Recapitulation.");
				}
			}
		}
		
		return null;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Re_Activate;
			options[index++] = DOCACTION_Void;
		}
		
		return index;
	}
	
	private BigDecimal getStmtAmt()
	{
		String sql = "SELECT SUM(DisbursementAmt) FROM UNS_DisbursRecon_Amt"
				+ " WHERE UNS_DisbursementRecon_ID = ?";
		return DB.getSQLValueBD(get_TrxName(), sql, get_ID());
	}
	
	private String cancelledTrx()
	{
		String msg = createRecapitulation(true);
		if(msg != null)
			return msg;
		
		MUNSDisbursReconAmt[] Amts = MUNSDisbursReconAmt.getAmts(this);
		for(int i=0;i<Amts.length;i++)
		{
			if(Amts[i].getC_BankStatementLine_ID() > 0)
			{
				MBankStatementLine line = new MBankStatementLine(getCtx(), 
						Amts[i].getC_BankStatementLine_ID(), get_TrxName());
				Amts[i].setC_BankStatementLine_ID(-1);
				Amts[i].saveEx();
				line.deleteEx(true);
			}
		}
		
		String sql = "DELETE FROM Fact_Acct fa WHERE fa.AD_Table_ID = (SELECT t.AD_Table_ID"
				+ " FROM AD_Table t WHERE t.TableName = ?)"
				+ " AND fa.Record_ID = ?";
		int count = DB.executeUpdate(sql, new Object[]{Table_Name, get_ID()}, false, get_TrxName());
		if(count < 0)
		{
			return "Failed when trying delete fact account.";
		}
		
		return null;
	}
}