/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.model.MUNSVATDocNo;
import com.unicore.model.X_UNS_VATDocNo;
import com.uns.base.model.MInvoice;

/**
 * @author Burhani Adam
 *
 */
public class MUNSVATLine extends X_UNS_VATLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9103910944548612580L;
	private static final int SERBASERBI_DK = 1013336;
	private static final int SERBASERBI_LK = 1013335;
	private static final int SERBASERBI = 1013841;
	private static final int SERBASERBI_JKT = 1015593;
	private static final int SERBASERBI_PKU = 1015517;
	

	/**
	 * @param ctx
	 * @param UNS_VATLine_ID
	 * @param trxName
	 */
	public MUNSVATLine(Properties ctx, int UNS_VATLine_ID, String trxName) {
		super(ctx, UNS_VATLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSVATLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSVATLine(MUNSVAT parent)
	{
		this(parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_VAT_ID(parent.get_ID());
	}
	
	public void setInvoice(MInvoice inv)
	{
		setAD_Org_ID(inv.getAD_Org_ID());
		setReferenceNo(inv.getDocumentNo());
		setC_Invoice_ID(inv.get_ID());
		setC_BPartner_ID(inv.getC_BPartner_ID());
		setC_BPartner_Location_ID(inv.getC_BPartner_Location_ID());
		if((getC_BPartner_ID() == SERBASERBI || getC_BPartner_ID() == SERBASERBI_DK ||
				getC_BPartner_ID() == SERBASERBI_JKT || getC_BPartner_ID() == SERBASERBI_LK ||
					getC_BPartner_ID() == SERBASERBI_PKU) && inv.getC_Order_ID() > 0)
		{
			String sql = "SELECT AttName FROM UNS_PreOrder WHERE C_Order_ID = ?";
			String attName = DB.getSQLValueString(get_TrxName(), sql, inv.getC_Order_ID());
			if(!Util.isEmpty(attName, true))
			{
				int indexOf = attName.indexOf(".");
				attName = attName.substring(indexOf + 1);
				attName = attName.trim();
				setTaxName(attName);
			}
			sql = "SELECT AttAddress FROM UNS_PreOrder WHERE C_Order_ID = ?";
			String attAddress = DB.getSQLValueString(get_TrxName(), sql, inv.getC_Order_ID());
			if(!Util.isEmpty(attAddress, true))
				setTaxAddress(attAddress);
			setTaxSerialNo("000000000000000");
		}
		else
		{
			setTaxName(getC_BPartner().getTaxName());
			setTaxAddress(getC_BPartner().getTaxAddress());
			setTaxSerialNo(getC_BPartner().getTaxSerialNo());
		}
		setGrandTotal(inv.getGrandTotal());
		
	}
	
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord)
		{
			String sql = "SELECT COALESCE(Max(LineNo),0) + 10 FROM UNS_VATLine WHERE UNS_VAT_ID = ?";
			int line = DB.getSQLValue(get_TrxName(), sql, getUNS_VAT_ID());
			setLineNo(line);
		}
		if(!newRecord)
			setRevisionAmt(getRevisionBeforeTaxAmt().add(getRevisionTaxAmt()));
		if(!getUNS_VAT().isSOTrx())
			return true;
		if(!m_auto && (newRecord || is_ValueChanged(COLUMNNAME_TaxInvoiceNo)))
		{
			MUNSVATDocNo newDn = null;
			MUNSVATDocNo oldDn = null;
			if(get_ValueOld(COLUMNNAME_TaxInvoiceNo) == null)
			{
				newDn = MUNSVATDocNo.getByTaxNo(getCtx(), getAD_Org_ID(), getTaxInvoiceNo().trim(), get_TrxName());
				if(newDn != null && !newDn.getUsageStatus().equals("U"))
				{
					newDn.setUNS_VATLine_ID(get_ID());
					newDn.setUsageStatus("U");
					newDn.setC_Invoice_ID(getC_Invoice_ID());
					if(!newDn.save())
					{
						log.saveError("Error", "failed update VAT Document No");
						return false;
					}
				}
				else
				{
					log.saveError("Error", "Tax Invoice No unregistered or has used, please recheck");
					return false;
				}
			}
			
			else if(!Util.isEmpty(getTaxInvoiceNo(), true))
			{
				newDn = MUNSVATDocNo.getByTaxNo(getCtx(), getAD_Org_ID(), getTaxInvoiceNo().trim(), get_TrxName());
				if(newDn != null && !newDn.getUsageStatus().equals("U"))
				{
					oldDn = MUNSVATDocNo.getByVATLine(getCtx(), get_ID(), get_TrxName());
					if(oldDn != null && newDn.get_ID() != oldDn.get_ID())
					{
						newDn.setUNS_VATLine_ID(get_ID());
						newDn.setUsageStatus("U");
						newDn.setC_Invoice_ID(getC_Invoice_ID());
						oldDn.setUsageStatus("NU");
						oldDn.setC_Invoice_ID(-1);
						oldDn.setUNS_VATLine_ID(-1);
						if(!newDn.save() || !oldDn.save())
						{
							log.saveError("Error", "failed update VAT Document No");
							return false;
						}
					}
				}
				else
				{
					log.saveError("Error", "Tax Invoice No unregistered or has used, please recheck");
					return false;
				}
			}
			else
			{
				oldDn = MUNSVATDocNo.getByVATLine(getCtx(), get_ID(), get_TrxName());
				if(oldDn != null)
				{
					oldDn.setUsageStatus("NU");
					oldDn.setC_Invoice_ID(-1);
					oldDn.setUNS_VATLine_ID(-1);
					if(!oldDn.save())
					{
						log.saveError("Error", "failed update VAT Document No");
						return false;
					}
				}
			}
		}
		
		return true;
	}
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!success)
			return false;
		
		if(newRecord && getC_Invoice_ID() > 0)
		{
			if(!isReplacement())
			{
				MInvoice inv = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
				for(org.compiere.model.MInvoiceLine line : inv.getLines(true))
				{
					MUNSVATInvLine vil = new MUNSVATInvLine(getCtx(), 0, get_TrxName());
					vil.setUNS_VATLine_ID(get_ID());
					if(getC_Invoice().getM_PriceList().isTaxIncluded())
						vil.setIncludeTax(true);
					vil.setInvoiceLine(line);
					vil.setAuto(true);
					if(!vil.save())
						throw new AdempiereException("Failed when trying create vat invoice lines");
				}
			}
			else
			{
				MUNSVATInvLine[] lines = MUNSVATInvLine.getByParent(getCtx(), get_ID(), get_TrxName());
				for(MUNSVATInvLine line : lines)
				{
					MUNSVATInvLine newLine = new MUNSVATInvLine(getCtx(), 0, get_TrxName());
					copyValues(line, newLine);
					newLine.setUNS_VATLine_ID(get_ID());
					if(!newLine.save())
						throw new AdempiereException("Failed when trying create vat invoice lines");
				}
			}
		}
		
		return upHeader(false, newRecord);
	}
	
	protected boolean beforeDelete()
	{
		MUNSVATDocNo dn = MUNSVATDocNo.getByVATLine(getCtx(), get_ID(), get_TrxName());
		if(dn != null)
		{
			dn.setUNS_VATLine_ID(-1);
			dn.setC_Invoice_ID(-1);
			dn.setUsageStatus(X_UNS_VATDocNo.USAGESTATUS_NotUsed);
			dn.saveEx();
		}
		String sql = "DELETE FROM UNS_VATInvLine WHERE UNS_VATLine_ID=?";
		if(DB.executeUpdate(sql, get_ID(), get_TrxName()) < 0)
			return false;
		
		return upHeader(true, false);
	}
	
	public boolean upHeader(boolean isDelete, boolean newRecord)
	{
		MUNSVAT vat = new MUNSVAT(getCtx(), getUNS_VAT_ID(), get_TrxName());
		
		if(isDelete || !newRecord)
		{
			vat.setBeforeTaxAmt(vat.getBeforeTaxAmt()
					.subtract((BigDecimal) get_ValueOld(X_UNS_VATLine.COLUMNNAME_BeforeTaxAmt)));
			vat.setTaxAmt(vat.getTaxAmt()
					.subtract((BigDecimal) get_ValueOld(X_UNS_VATLine.COLUMNNAME_TaxAmt)));
			vat.setGrandTotal(vat.getGrandTotal()
					.subtract((BigDecimal) get_ValueOld(X_UNS_VATLine.COLUMNNAME_GrandTotal)));
			vat.setRevisionBeforeTaxAmt(vat.getRevisionBeforeTaxAmt()
					.subtract((BigDecimal) get_ValueOld(X_UNS_VATLine.COLUMNNAME_RevisionBeforeTaxAmt)));
			vat.setRevisionTaxAmt(vat.getRevisionTaxAmt()
					.subtract((BigDecimal) get_ValueOld(X_UNS_VATLine.COLUMNNAME_RevisionTaxAmt)));
			vat.setRevisionAmt(vat.getRevisionAmt()
					.subtract((BigDecimal) get_ValueOld(X_UNS_VATLine.COLUMNNAME_RevisionAmt)));
		}
		if(!isDelete)
		{
			vat.setBeforeTaxAmt(vat.getBeforeTaxAmt().add(getBeforeTaxAmt()));
			vat.setTaxAmt(vat.getTaxAmt().add(getTaxAmt()));
			vat.setGrandTotal(vat.getGrandTotal().add(getGrandTotal()));
			vat.setRevisionBeforeTaxAmt(vat.getRevisionBeforeTaxAmt().add(getRevisionBeforeTaxAmt()));
			vat.setRevisionTaxAmt(vat.getRevisionTaxAmt().add(getRevisionTaxAmt()));
			vat.setRevisionAmt(vat.getRevisionAmt().add(getRevisionAmt()));
		}
		
		return vat.save();
	}
	
	public static MUNSVATLine[] getByParent(Properties ctx, int UNS_VAT_ID, String trxName)
	{
		ArrayList<MUNSVATLine> list = new ArrayList<MUNSVATLine>();
		
		String sql = "SELECT vl.UNS_VATLine_ID FROM UNS_VATLine vl"
						+ " INNER JOIN C_Invoice iv ON iv.C_Invoice_ID = vl.C_Invoice_ID"
						+ " WHERE vl.UNS_VAT_ID=?"
						+ " ORDER BY iv.DateInvoiced ASC, iv.DocumentNo ASC";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, UNS_VAT_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add(new MUNSVATLine(ctx, rs.getInt(1),
						trxName));
			rs.close();
			pstmt.close();
			pstmt = null;
		} catch (SQLException ex) {
			throw new AdempiereException("Unable to load VAT Lines ",
					ex);
		} finally {
			DB.close(rs, pstmt);
		}

		MUNSVATLine[] retValue = new MUNSVATLine[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	private boolean m_auto = false;
	
	public boolean isAuto()
	{
		return m_auto;
	}
	
	public void setAuto (boolean auto)
	{
		m_auto = auto;
	}
}