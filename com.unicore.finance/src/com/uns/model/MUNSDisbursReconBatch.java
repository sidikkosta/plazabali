/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDisbursReconBatch extends X_UNS_DisbursRecon_Batch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 659852599731639811L;

	/**
	 * @param ctx
	 * @param UNS_DisbursRecon_Batch_ID
	 * @param trxName
	 */
	public MUNSDisbursReconBatch(Properties ctx, int UNS_DisbursRecon_Batch_ID,
			String trxName) {
		super(ctx, UNS_DisbursRecon_Batch_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDisbursReconBatch(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		setDisbursementAmt(getNetAmount().subtract(getAmountRefunded()));
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_DisbursRecon_Line l WHERE l.UNS_DisbursRecon_Batch_ID = ?";
		boolean success = DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
		
		return success;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!newRecord)
		{
			String sql = "UPDATE UNS_DisbursementRecon r SET DisbursementAmt ="
					+ " (SELECT SUM(l.DisbursementAmt) FROM UNS_DisbursRecon_Batch l"
					+ " WHERE l.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID),"
					+ " AmountRefunded ="
					+ " (SELECT SUM(l.AmountRefunded) FROM UNS_DisbursRecon_Batch l"
					+ " WHERE l.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID),"
					+ " NotMatchedRefundAmt ="
					+ " (SELECT SUM(l.NotMatchedRefundAmt) FROM UNS_DisbursRecon_Batch l"
					+ " WHERE l.UNS_DisbursementRecon_ID = r.UNS_DisbursementRecon_ID)"
					+ " WHERE r.UNS_DisbursementRecon_ID = ?";
			if(DB.executeUpdate(sql, getUNS_DisbursementRecon_ID(), get_TrxName()) < 0)
			{
				log.saveError("Error", "Failed when trying update header.");
				return false;
			}
		}
		
		return true;
	}
	
	public MUNSDisbursReconLine[] getLines()
	{
		List<MUNSDisbursReconLine> list = Query.get(getCtx(), com.uns.model.factory.UNSFinanceModelFactory.EXTENSION_ID,
				MUNSDisbursReconLine.Table_Name, Table_Name +"_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSDisbursReconLine[list.size()]);
	}
	
	public static MUNSDisbursReconBatch getCreate(Properties ctx, String batchNo, int ParentID, String trxName)
	{
		String whereClause = "BatchNo = ? AND UNS_DisbursementRecon_ID = ?";
		MUNSDisbursReconBatch result = Query.get(ctx, com.uns.model.factory.UNSFinanceModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(batchNo, ParentID)
					.firstOnly();
		
		if(result == null)
		{
			result = new MUNSDisbursReconBatch(ctx, 0, trxName);
			result.setUNS_DisbursementRecon_ID(ParentID);
			result.setBatchNo(batchNo);
			result.saveEx();
		}
		
		return result;
	}
}