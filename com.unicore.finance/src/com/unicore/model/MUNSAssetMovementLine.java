/**
 * 
 */
package com.unicore.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MAsset;
import org.compiere.model.MAssetAddition;
import org.compiere.process.DocAction;
import org.compiere.util.DB;

/**
 * @author nurse
 *
 */
public class MUNSAssetMovementLine extends X_UNS_AssetMovementLine 
{
	private static final long serialVersionUID = 3335576884794804922L;
	private MUNSAssetMovement m_parent = null;
	
	public MUNSAssetMovement getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSAssetMovement(getCtx(), getUNS_AssetMovement_ID(), get_TrxName());
		return m_parent;
	}
	
	public MUNSAssetMovementLine (MUNSAssetMovement parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_AssetMovement_ID(parent.getUNS_AssetMovement_ID());
		m_parent = parent;
	}

	/**
	 * @param ctx
	 * @param UNS_AssetMovementLine_ID
	 * @param trxName
	 */
	public MUNSAssetMovementLine(Properties ctx, int UNS_AssetMovementLine_ID,
			String trxName) {
		super(ctx, UNS_AssetMovementLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAssetMovementLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (!initSectionAndLocator())
			return false;
		
		return super.beforeSave(newRecord);
	}
	
	private boolean initSectionAndLocator ()
	{
		String sql = "SELECT M_Locator_ID, C_BPartner_ID FROM A_Asset WHERE A_Asset_ID = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getA_Asset_ID());
			rs = st.executeQuery();
			if (rs.next())
			{
				setM_Locator_ID(rs.getInt(1));
				setDepartmentFrom_ID(rs.getInt(2));
			}
		}
		catch (SQLException ex)
		{
			log.log(Level.SEVERE, ex.getMessage());
			return false;
		}
		finally
		{
			DB.close(rs, st);
		}
		return true;
	}
	
	private String cancelAddition ()
	{
		MAssetAddition add = new MAssetAddition(getCtx(), getA_Asset_Addition_ID(), get_TrxName());
		try
		{
			if (!add.processIt(DocAction.ACTION_Void))
				return add.getProcessMsg();
			add.saveEx();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		return null;
	}
	
	String updateAsset (boolean onVoid)
	{
		int dept = getDepartmentTo_ID();
		int loct = getM_LocatorTo_ID();
		if (onVoid)
		{
			dept = getDepartmentFrom_ID();
			loct = getM_Locator_ID();
		}
		MAsset asset = new MAsset(getCtx(), getA_Asset_ID(), get_TrxName());
		asset.setC_BPartner_ID(dept);
		asset.setM_Locator_ID(loct);
		try
		{
			asset.saveEx();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		
		if (onVoid && getA_Asset_Addition_ID() != 0)
			return cancelAddition();
		
		return null;
	}
}
