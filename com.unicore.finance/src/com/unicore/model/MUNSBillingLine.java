package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.model.I_C_PaymentTerm;
import org.compiere.model.MInvoice;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.unicore.ui.ISortTabRecord;

/**
 *	Model class for BillingLine
 *	
 */

public class MUNSBillingLine extends X_UNS_BillingLine implements ISortTabRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MUNSBillingLine(Properties ctx, int UNS_Billing_Line_ID,
			String trxName) {
		super(ctx, UNS_Billing_Line_ID, trxName);
	}

	/**
	 * 
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSBillingLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean afterDelete(boolean success) 
	{
		updateHeaderAmount();
		
		return true;
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{		
		if (!success)
			return success;
		
		if (newRecord || is_ValueChanged(COLUMNNAME_C_Invoice_ID))
		{
			updateHeaderAmount();
		}
		
		return true;
	}

	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		if(getC_Invoice().getReferenceNo() != null)
			setReferenceNo(getC_Invoice().getReferenceNo());
		
		if(getLine() == 0)
		{
			String sql = "SELECT COALESCE(MAX(" + COLUMNNAME_Line + "), 0) FROM " + Table_Name
					+ " WHERE " + COLUMNNAME_UNS_Billing_ID + "= ?";
			int max = DB.getSQLValue(get_TrxName(), sql, getUNS_Billing_ID());
			setLine(max+10);
		}
		
		if (newRecord || is_ValueChanged(COLUMNNAME_C_Invoice_ID)) 
		{
			MInvoice theInvoice = (MInvoice) getC_Invoice();
	
			setC_PaymentTerm_ID(theInvoice.getC_PaymentTerm_ID());

			I_C_PaymentTerm paymentTerm = (I_C_PaymentTerm) getC_PaymentTerm();
			int dueDay = paymentTerm.getNetDays();
			Timestamp duedate = TimeUtil.addDays(theInvoice.getDateInvoiced(), dueDay);
			setDueDate(duedate);

			setAD_Org_ID(theInvoice.getAD_Org_ID());
			setDateInvoiced(theInvoice.getDateInvoiced());
			setC_PaymentTerm_ID(theInvoice.getC_PaymentTerm_ID());
			setNetAmtToInvoice(theInvoice.getGrandTotal());
			setPPh22Import(theInvoice.getPPh22Import());
			setImportDutyAmt(theInvoice.getImportDutyAmt());
			
			String sql = "SELECT invoiceOpen(?, 0)";
			BigDecimal openAmt = DB.getSQLValueBD(get_TrxName(), sql, getC_Invoice_ID());
			
			if (openAmt == null)
				openAmt = Env.ZERO;
			
			BigDecimal paidAmt = this.getNetAmtToInvoice().subtract(openAmt);
			setPaidAmt(paidAmt);
			setOpenAmt(openAmt);
		}
		
		return super.beforeSave(newRecord);
	}

	/**
	 * 
	 * @return
	 */
	private boolean updateHeaderAmount()
	{
		String sql = "UPDATE UNS_Billing b SET (TotalAmt, PaidAmt, OpenAmt) = "
				+ "(SELECT COALESCE(SUM(bl.NetAmtToInvoice), 0), COALESCE(SUM(bl.PaidAmt), 0), COALESCE(SUM(bl.OpenAmt), 0)"
				+ " FROM UNS_BillingLine bl WHERE bl.UNS_Billing_ID=b.UNS_Billing_ID) WHERE UNS_Billing_ID=?";
		
		int count = DB.executeUpdateEx(sql, new Object[]{getUNS_Billing_ID()}, get_TrxName());
		
		if (count <= 0) {
			log.saveError("Failed updating Billing amounts from it's lines", "Failed updating line's parent detail (Billing).");
			return false;
		}
		
		sql = "UPDATE UNS_BillingGroup gb SET (GrandTotal, PaidAmt, OpenAmt) = "
				+ "(SELECT COALESCE(SUM(b.TotalAmt), 0), COALESCE(SUM(b.PaidAmt), 0), COALESCE(SUM(b.OpenAmt), 0)"
				+ " FROM UNS_Billing b WHERE b.UNS_BillingGroup_ID=gb.UNS_BillingGroup_ID) WHERE UNS_BillingGroup_ID=?";
		
		count = DB.executeUpdateEx(sql, new Object[]{getParent().getUNS_BillingGroup_ID()}, get_TrxName());
		
		if (count <= 0) {
			log.saveError("Failed updating Grouping Billing amounts from it's Billings", 
						  "Failed updating Grouping Billing amounts from it's Billings.");
			return false;
		}
		
//		MUNSBilling billHdr = getParent();
//		
//		// Find Total Bill Amount 
//		String sql = "SELECT COALESCE(SUM(NetAmtToInvoice), 0) FROM UNS_BillingLine WHERE UNS_Billing_ID = ? AND IsActive='Y' ";
//		BigDecimal totalLineNetAmt = DB.getSQLValueBD(get_TrxName(), sql, billHdr.getUNS_Billing_ID());
//		
//		sql = "SELECT COALESCE(SUM(ImportDutyAmt), 0) FROM UNS_BillingLine WHERE UNS_Billing_ID = ? AND IsActive='Y' ";
//		BigDecimal importDuty = DB.getSQLValueBD(get_TrxName(), sql, billHdr.getUNS_Billing_ID());
//		
//		sql = "SELECT COALESCE(SUM(PPh22Import), 0) FROM UNS_BillingLine WHERE UNS_Billing_ID = ? AND IsActive='Y' ";
//		BigDecimal pph22Import = DB.getSQLValueBD(get_TrxName(), sql, billHdr.getUNS_Billing_ID());
//		
//		billHdr.setImportDutyAmt(importDuty);
//		billHdr.setPPh22Import(pph22Import);
//		billHdr.setGrandTotal(totalLineNetAmt);
//		billHdr.setTotalAmt(totalLineNetAmt);
//		
//		if(!billHdr.save(get_TrxName())){
//			log.saveError("Cannot Update Billing Header", "Cannot Save Billing Header");
//			return false;
//		}

		return true;
	}
	
	private MUNSBilling parent = null ;
	
	public MUNSBilling getParent()
	{
		if(parent == null)
			parent = MUNSBilling.getBilling(getCtx(), getUNS_Billing_ID(), get_TrxName());
		
		return parent;
	}

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) 
	{
		if(getC_Invoice_ID() <= 0)
			return null;
		
		MInvoice invoice = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
		MUNSBilling billing = new MUNSBilling(getCtx(), getUNS_Billing_ID(), get_TrxName());
		setAD_Org_ID(billing.getAD_Org_ID());
		setDateInvoiced(invoice.getDateInvoiced());
		setC_PaymentTerm_ID(invoice.getC_PaymentTerm_ID());
		setNetAmtToInvoice(invoice.getGrandTotal());
		setPPh22Import(invoice.getPPh22Import());
		setImportDutyAmt(invoice.getImportDutyAmt());

//		I_C_PaymentTerm paymentTerm = (I_C_PaymentTerm) getC_PaymentTerm();
//		int dueDay = paymentTerm.getNetDays();
//		Timestamp duedate = TimeUtil.addDays(getDateInvoiced(), dueDay);
//		setDueDate(duedate);
		
		return null;
	}

	@Override
	public String beforeRemoveSelection() {
		// TODO Auto-generated method stub
		return null;
	}
}