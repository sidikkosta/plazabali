/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MAssetAddition;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSFinanceModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSAssetMovement extends X_UNS_AssetMovement implements DocAction
{
	private static final long serialVersionUID = 1596531924542152479L;
	private MUNSAssetMovementLine[] m_lines = null;
	private boolean m_justPrepared = false;
	private String m_processMsg = null;
	
	public MUNSAssetMovementLine[] getLines (boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSAssetMovementLine> list = Query.get(
				getCtx(), 
				UNSFinanceModelFactory.EXTENSION_ID, 
				MUNSAssetMovementLine.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).
				list();
		m_lines = new MUNSAssetMovementLine[list.size()];
		list.toArray(m_lines);
		return m_lines;
	}
	
	public MUNSAssetMovementLine[] getLines ()
	{
		return getLines(false);
	}
	
	/*
	 *
	 * @param ctx
	 * @param UNS_AssetMovement_ID
	 * @param trxName
	 */
	public MUNSAssetMovement(Properties ctx, int UNS_AssetMovement_ID,
			String trxName) 
	{
		super(ctx, UNS_AssetMovement_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAssetMovement(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	public boolean processIt(String action) throws Exception 
	{
		writeLog(Level.INFO, "Process-It");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(getDocAction(), action);
	}

	@Override
	public boolean unlockIt() 
	{
		writeLog(Level.INFO, "Unlock-It");
		return true;
	}

	@Override
	public boolean invalidateIt() 
	{
		writeLog(Level.INFO, "Invalidate-It");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() 
	{
		writeLog(Level.INFO, "Prepare-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		

		m_justPrepared = true;
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		String sql = "UPDATE UNS_AssetMovementLine SET Processed = ? WHERE UNS_AssetMovement_ID = ?";
		int result = DB.executeUpdate(sql, new Object[]{"Y", getUNS_AssetMovement_ID()}, false, get_TrxName());
		if (result == -1)
			return DOCSTATUS_Invalid;
		
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() 
	{
		writeLog(Level.INFO, "Approve-It");
		setProcessed(true);
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() 
	{
		writeLog(Level.INFO, "Reeject-It");
		setProcessed(false);
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() 
	{
		writeLog(Level.INFO, "Complete-It");
		if (!m_justPrepared)
			prepareIt();
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		if (!updateAsset(false))
			return DOCSTATUS_Invalid;
		
		if (!isApproved())
			approveIt();
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() 
	{
		writeLog(Level.INFO, "Void-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		if (!updateAsset(true))
			return false;
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		return true;
	}
	
	private boolean updateAsset (boolean onVoid)
	{
		MUNSAssetMovementLine[] lines = getLines();
		for (int i=0; m_processMsg == null && i<lines.length; i++)
		{
			m_processMsg = lines[i].updateAsset(onVoid);
		}
		if (m_processMsg != null)
			return false;
		return true;
	}

	@Override
	public boolean closeIt() 
	{
		writeLog(Level.INFO, "Close-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Closed);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean reverseCorrectIt() 
	{
		m_processMsg = "Reverse Correct-It is not implemented";
		return false;
	}

	@Override
	public boolean reverseAccrualIt() 
	{
		m_processMsg = "Reverse Accrual-It is not implemented";
		return false;
	}

	@Override
	public boolean reActivateIt() 
	{
		m_processMsg = "Reactivate-It is not implemented";
		return false;
	}
	
	@Override
	public String toString ()
	{
		StringBuilder sb = new StringBuilder(Table_Name);
		sb.append("[DocumentNo:").append(getDocumentNo());
		sb.append("]");
		String str = sb.toString();
		return str;
	}

	@Override
	public String getSummary() 
	{
		return toString();
	}

	@Override
	public String getDocumentInfo() 
	{
		return toString();
	}

	@Override
	public File createPDF() 
	{
		return null;
	}

	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	@Override
	public int getC_Currency_ID() 
	{
		return 303;
	}

	@Override
	public BigDecimal getApprovalAmt() 
	{
		return Env.ZERO;
	}

	private void writeLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		log.log(level, msg);
	}
	
	public MUNSAssetMovement (MAssetAddition addition)
	{
		this (addition.getCtx(), 0, addition.get_TrxName());
		setClientOrg(addition);
		setDateAcct(addition.getDateAcct());
		setMovementDate(addition.getDateDoc());
		setDescription("###Generated From Asset Addition###");
	}
}
