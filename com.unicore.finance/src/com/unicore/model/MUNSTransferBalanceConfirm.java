/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankStatement;
import org.compiere.model.MPayment;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.wf.MWorkflow;

import com.unicore.base.model.MBankStatementLine;
import com.unicore.model.factory.UNSFinanceModelFactory;
import com.unicore.model.process.UNSBankTransfer;
import com.uns.base.model.Query;
import com.uns.model.MUNSChequeReconciliation;

/**
 * @author menjangan
 *
 */
public class MUNSTransferBalanceConfirm extends X_UNS_TransferBalance_Confirm 
												implements DocAction, DocOptions
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private X_UNS_PettyCash_Confirm[] m_pettyCashConfirm = null;

	/**
	 * @param ctx
	 * @param UNS_TransferBalance_Confirm_ID
	 * @param trxName
	 */
	public MUNSTransferBalanceConfirm(Properties ctx,
			int UNS_TransferBalance_Confirm_ID, String trxName) {
		super(ctx, UNS_TransferBalance_Confirm_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSTransferBalanceConfirm(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	public boolean processIt(String action) throws Exception {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(getAccountFrom_ID() == 0)
		{
			m_processMsg = "Field mandatory Account From!";
			return DocAction.STATUS_Invalid;
		}
		
		if(getAccountTo_ID() == 0)
		{
			m_processMsg = "Field mandatory Account To!";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		
//		setProcessed(true);
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(true);
		setProcessed(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		MUNSTransferBalanceRequest request = new MUNSTransferBalanceRequest(
				getCtx(), getUNS_TransferBalance_Request_ID(), get_TrxName());
		
		request.setAmountConfirmed(getAmountConfirmed());
		request.setAccountFrom_ID(getAccountFrom_ID());
		request.setChequeNo(getChequeNo());
		if(!request.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Error");
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

//		Implicit Approval
		if (!isApproved())
			approveIt();
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		if(getChequeNo() != null && !getChequeNo().trim().equals(""))
		{
			MUNSChequeReconciliation recon = MUNSChequeReconciliation.getInstance(getChequeNo(), getCtx(), get_TrxName());
			if(recon != null)
			{
				if(recon.getStatus() == null || !recon.getStatus().equals(MUNSChequeReconciliation.STATUS_Disbursement))
				{
					m_processMsg = "Please wait until the cheque was disbursed, and document will be completed automatically";;
					return DocAction.STATUS_InProgress;
				}
			}
		}
		
		if(REQUESTTYPE_BankCashRequest.equals(getRequestType()))
		{
			X_UNS_PettyCash_Confirm[] lines = getLines(true);
			for(X_UNS_PettyCash_Confirm line : lines)
			{
				X_UNS_PettyCashRequest reqLine = new X_UNS_PettyCashRequest(
						getCtx(), line.getUNS_PettyCashRequest_ID(), get_TrxName());
				reqLine.setAmountConfirmed(line.getAmountConfirmed());
				if(!reqLine.save())
				{
					m_processMsg = "Can't update Request Line !!!";
					return DocAction.STATUS_Invalid;
				}
			}
		}
		
		if(isFullyConfirmed())
		{			
			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(request, DocAction.ACTION_Complete);
			if(pi.isError())
			{
				m_processMsg = "Can't complete Request Document!. " +
						"please contact the administrator to fix it!";
				return DocAction.STATUS_Invalid;
			}
		}
		
//		String addDescription = "Generated from petty cash (" + request.getDocumentNo() + "-" + getDocumentNo() + ")";
//		UNSBankTransfer bt = new UNSBankTransfer(
//				getCtx(), "", addDescription, getC_BPartner_ID(), 
//				getC_Currency_ID(), 0, getC_Charge_ID(), null, 
//				getAmountConfirmed(), getAccountFrom_ID(), 
//				getAccountTo_ID(), getDateConfirm(), getDateConfirm(), 
//				getAD_Org_ID(), request.getAD_Org_ID(), true, get_TrxName());
		
		
		UNSBankTransfer bt = new UNSBankTransfer((MUNSTransferBalanceRequest) getUNS_TransferBalance_Request());
		
		m_processMsg = bt.doIt();
		if (null != m_processMsg && !bt.IsSuccess())
			return DocAction.STATUS_Invalid;
		
		setPaymentFrom_ID(bt.getPaymentBankFrom_ID());
		setPaymentTo_ID(bt.getPaymentBankTo_ID());
		setStatementLineFrom_ID(bt.getStatementFrom_ID());
		setStatementLineTo_ID(bt.getStatementTo_ID());
		saveEx();

		try {
			if(!request.processIt(DOCACTION_Complete) || !request.save())
			{
				m_processMsg = "Failed on trying complete petty cash request";
				return DOCSTATUS_Invalid;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return DOCSTATUS_Invalid;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(!isAuto() && getChequeNo() != null)
		{
			m_processMsg = "Please void from cheque reconciliation.";
			return false;
		}
		MUNSTransferBalanceRequest req = new MUNSTransferBalanceRequest(
				getCtx(), getUNS_TransferBalance_Request_ID(), get_TrxName());
		if(!req.getDocStatus().equals(STATUS_Voided) && !req.getDocStatus().equals(STATUS_Reversed))
		{
			String action = isComplete() ? "VO" : "RE";
			try {
				req.setIsAuto(true);
				req.processIt(action);
				req.saveEx();
			} catch (Exception e) {
				m_processMsg = req.getProcessMsg();
				return false;
			}
		}
		
		if(isComplete())
		{
			if(getStatementLineFrom_ID() > 0)
			{
				MBankStatement st = new MBankStatement(getCtx(), getStatementLineFrom().getC_BankStatement_ID(), get_TrxName());
				MBankStatementLine line = new MBankStatementLine(getCtx(), getStatementLineFrom_ID(), get_TrxName());
				if(st.isComplete())
				{
					MBankStatement newSt = MBankStatement.getOpen(getAccountFrom_ID(), get_TrxName(), true);
					MBankStatementLine newLine = new MBankStatementLine(newSt);
					newLine.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_APTransaction);
					newLine.setDateAcct(line.getDateAcct());
					newLine.setStatementLineDate(line.getStatementLineDate());
					newLine.setStmtAmt(line.getStmtAmt().negate());
					newLine.setAmount(line.getAmount().negate());
					newLine.setDescription(line.getDescription());
					newLine.setC_Currency_ID(line.getC_Currency_ID());
					newLine.setC_Charge_ID(line.getC_Charge_ID());
					if(line.getAD_OrgTrx_ID() > 0)
						newLine.setAD_OrgTrx_ID(line.getAD_Org_ID());
					newLine.saveEx();
				}
				else
				{
					setStatementLineFrom_ID(-1);
					saveEx();
					line.deleteEx(true);
				}
			}
			
			if(getStatementLineTo_ID() > 0)
			{
				MBankStatement st = new MBankStatement(getCtx(), getStatementLineTo().getC_BankStatement_ID(), get_TrxName());
				MBankStatementLine line = new MBankStatementLine(getCtx(), getStatementLineTo_ID(), get_TrxName());
				if(st.isComplete())
				{
					MBankStatement newSt = MBankStatement.getOpen(getAccountTo_ID(), get_TrxName(), true);
					MBankStatementLine newLine = new MBankStatementLine(newSt);
					newLine.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_APTransaction);
					newLine.setDateAcct(line.getDateAcct());
					newLine.setStatementLineDate(line.getStatementLineDate());
					newLine.setStmtAmt(line.getStmtAmt().negate());
					newLine.setAmount(line.getAmount().negate());
					newLine.setDescription(line.getDescription());
					newLine.setC_Currency_ID(line.getC_Currency_ID());
					newLine.setC_Charge_ID(line.getC_Charge_ID());
					if(line.getAD_OrgTrx_ID() > 0)
						newLine.setAD_OrgTrx_ID(line.getAD_Org_ID());
					newLine.saveEx();
				}
				else
				{
					setStatementLineTo_ID(-1);
					saveEx();
					line.deleteEx(true);
				}
			}
			
			if(getPaymentFrom_ID() > 0)
			{
				MPayment pay = new MPayment(getCtx(), getPaymentFrom_ID(), get_TrxName());
				String payStatus = pay.getDocStatus();
				if(!payStatus.equals("VO") && !payStatus.equals("RE"))
				{
					if(!pay.processIt("VO") || !pay.save())
					{
						m_processMsg = pay.getProcessMsg();
						return false;
					}
				}
			}
			
			if(getPaymentTo_ID() > 0)
			{
				MPayment pay = new MPayment(getCtx(), getPaymentTo_ID(), get_TrxName());
				String payStatus = pay.getDocStatus();
				if(!payStatus.equals("VO") && !payStatus.equals("RE"))
				{
					if(!pay.processIt("VO") || !pay.save())
					{
						m_processMsg = pay.getProcessMsg();
						return false;
					}
				}
			}
		}

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reverseCorrectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reActivateIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return getAmountConfirmed();
	}
	
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public String createFrom(MUNSTransferBalanceRequest request)
	{
		setAD_Org_ID(request.getAD_OrgTo_ID());
		setAD_OrgFrom_ID(request.getAD_Org_ID());
		setAccountTo_ID(request.getAccountTo_ID());
		setAccountFrom_ID(request.getAccountFrom_ID());
		setAmountConfirmed(request.getAmountRequested());
		setAmountRequested(request.getAmountRequested());
		setC_BPartner_ID(request.getC_BPartner_ID());
		setDateConfirm(request.getDateRequired());
		setDateRequired(request.getDateRequired());
		setDescription(request.getDescription());
		setUNS_TransferBalance_Request_ID(request.get_ID());
		setRequestType(request.getRequestType());
		if(request.getChequeNo() != null)
			setChequeNo(request.getChequeNo());
		
		if(!save())
		{
			return "Can't create confirmatioins!. " +
					"Please contact administrator to fix it";	
		}
		
		if(!request.getRequestType().equals(REQUESTTYPE_BankCashRequest))
			return null;
		
		X_UNS_PettyCashRequest[] reqLines = request.getLines(true);
		
		for(X_UNS_PettyCashRequest reqLine :reqLines)
		{
			X_UNS_PettyCash_Confirm confLine = new X_UNS_PettyCash_Confirm(
										getCtx(), 0, get_TrxName());
			confLine.setAD_Org_ID(getAD_Org_ID());
			confLine.setAmountConfirmed(reqLine.getAmountRequested());
			confLine.setAmountRequested(reqLine.getAmountRequested());
			confLine.setDescription(reqLine.getDescription());
			confLine.setUNS_TransferBalance_Confirm_ID(get_ID());
			confLine.setUNS_PettyCashRequest_ID(reqLine.get_ID());
			confLine.setC_Charge_ID(reqLine.getC_Charge_ID());
			confLine.saveEx();
		}
		
		//load statement line
		MBankStatementLine[] sLines = MBankStatementLine.getOf(
				request.get_ID(), 0, get_TrxName());
		
		for(MBankStatementLine sLine : sLines)
		{
			sLine.setUNS_TransferBalance_Confirm_ID(this.get_ID());
			sLine.saveEx();
		}
			
		return null;
	}
	
	/**
	 * return true if amount confirmed = amount requested
	 * @return
	 */
	public boolean isFullyConfirmed()
	{
		if(getRequestType().equals(REQUESTTYPE_Balance))
		{
			return getAmountConfirmed().compareTo(getAmountRequested()) == 0;
		}
		else if(getRequestType().equals(REQUESTTYPE_BankCashRequest))
		{
			X_UNS_PettyCash_Confirm[] confLines = getLines(true);
			for(X_UNS_PettyCash_Confirm confLine : confLines)
			{
				if(confLine.getAmountRequested().compareTo(
						confLine.getAmountConfirmed()) != 0)
					return false;
			}
		}
		else
		{
			log.log(Level.SEVERE, "Unknown Request Type : " + getRequestType());
		}
		
		return true;
	}
	
	public static MUNSTransferBalanceConfirm get(Properties ctx, int RequestID, String trxName)
	{
		MUNSTransferBalanceConfirm confirm = null;
		confirm = Query.get(ctx, UNSFinanceModelFactory.EXTENSION_ID, Table_Name,
					COLUMNNAME_UNS_TransferBalance_Request_ID + "=?", trxName)
						.setParameters(RequestID).firstOnly();
		
		return confirm;
	}
	
	@Override
	public boolean beforeSave(boolean newRecord)
	{
		if(!newRecord)
			if(getAccountFrom_ID() == 0)
				throw new AdempiereException("Field mandatory Account From");
		return super.beforeSave(newRecord);
	}
	
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public X_UNS_PettyCash_Confirm[] getLines(boolean requery)
	{
		if(null != m_pettyCashConfirm
				&& !requery)
			return m_pettyCashConfirm;
		
		List<X_UNS_PettyCash_Confirm> list = Query.get(
				getCtx(), UNSFinanceModelFactory.EXTENSION_ID
				, X_UNS_PettyCash_Confirm.Table_Name
				, X_UNS_PettyCash_Confirm.COLUMNNAME_UNS_TransferBalance_Confirm_ID	+ "=?"
				, get_TrxName()).setParameters(get_ID())
				.list();
		
		X_UNS_PettyCash_Confirm[]  conf = new X_UNS_PettyCash_Confirm[list.size()];
		m_pettyCashConfirm = list.toArray(conf);
		
		return m_pettyCashConfirm;
	}
	
	/**
	 * 
	 * @return
	 */
	public X_UNS_PettyCash_Confirm[] getLines()
	{
		return getLines(false);
	}
	
	public boolean isComplete()
	{
		return getDocStatus().equals(DOCSTATUS_Completed)
				|| getDocStatus().equals(DOCSTATUS_Closed)
					|| getDocStatus().equals(DOCSTATUS_Reversed)
						|| getDocStatus().equals(DOCSTATUS_Voided);
	}
	
	private boolean m_isAuto = false;
	
	private boolean isAuto()
	{
		return m_isAuto;
	}
	
	public void setIsAuto(boolean isAuto)
	{
		m_isAuto = isAuto;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		if(docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Void;
		}
		return index;
	}
}