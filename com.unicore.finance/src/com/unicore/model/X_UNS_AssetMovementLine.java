/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_AssetMovementLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_AssetMovementLine extends PO implements I_UNS_AssetMovementLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181226L;

    /** Standard Constructor */
    public X_UNS_AssetMovementLine (Properties ctx, int UNS_AssetMovementLine_ID, String trxName)
    {
      super (ctx, UNS_AssetMovementLine_ID, trxName);
      /** if (UNS_AssetMovementLine_ID == 0)
        {
			setA_Asset_ID (0);
			setDepartmentTo_ID (0);
			setM_LocatorTo_ID (0);
			setProcessed (false);
// N
			setUNS_AssetMovement_ID (0);
			setUNS_AssetMovementLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_AssetMovementLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_AssetMovementLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_A_Asset_Addition getA_Asset_Addition() throws RuntimeException
    {
		return (org.compiere.model.I_A_Asset_Addition)MTable.get(getCtx(), org.compiere.model.I_A_Asset_Addition.Table_Name)
			.getPO(getA_Asset_Addition_ID(), get_TrxName());	}

	/** Set Asset Addition.
		@param A_Asset_Addition_ID Asset Addition	  */
	public void setA_Asset_Addition_ID (int A_Asset_Addition_ID)
	{
		if (A_Asset_Addition_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_Addition_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_Addition_ID, Integer.valueOf(A_Asset_Addition_ID));
	}

	/** Get Asset Addition.
		@return Asset Addition	  */
	public int getA_Asset_Addition_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_Addition_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_A_Asset getA_Asset() throws RuntimeException
    {
		return (org.compiere.model.I_A_Asset)MTable.get(getCtx(), org.compiere.model.I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getDepartmentFrom() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getDepartmentFrom_ID(), get_TrxName());	}

	/** Set Sec Of Dept From.
		@param DepartmentFrom_ID Sec Of Dept From	  */
	public void setDepartmentFrom_ID (int DepartmentFrom_ID)
	{
		if (DepartmentFrom_ID < 1) 
			set_Value (COLUMNNAME_DepartmentFrom_ID, null);
		else 
			set_Value (COLUMNNAME_DepartmentFrom_ID, Integer.valueOf(DepartmentFrom_ID));
	}

	/** Get Sec Of Dept From.
		@return Sec Of Dept From	  */
	public int getDepartmentFrom_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DepartmentFrom_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getDepartmentTo() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getDepartmentTo_ID(), get_TrxName());	}

	/** Set Sec Of Dept To.
		@param DepartmentTo_ID Sec Of Dept To	  */
	public void setDepartmentTo_ID (int DepartmentTo_ID)
	{
		if (DepartmentTo_ID < 1) 
			set_Value (COLUMNNAME_DepartmentTo_ID, null);
		else 
			set_Value (COLUMNNAME_DepartmentTo_ID, Integer.valueOf(DepartmentTo_ID));
	}

	/** Get Sec Of Dept To.
		@return Sec Of Dept To	  */
	public int getDepartmentTo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DepartmentTo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_Locator getM_Locator() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Locator getM_LocatorTo() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getM_LocatorTo_ID(), get_TrxName());	}

	/** Set Locator To.
		@param M_LocatorTo_ID 
		Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID)
	{
		if (M_LocatorTo_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_LocatorTo_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_LocatorTo_ID, Integer.valueOf(M_LocatorTo_ID));
	}

	/** Get Locator To.
		@return Location inventory is moved to
	  */
	public int getM_LocatorTo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_LocatorTo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.unicore.model.I_UNS_AssetMovement getUNS_AssetMovement() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_AssetMovement)MTable.get(getCtx(), com.unicore.model.I_UNS_AssetMovement.Table_Name)
			.getPO(getUNS_AssetMovement_ID(), get_TrxName());	}

	/** Set Asset Movement.
		@param UNS_AssetMovement_ID Asset Movement	  */
	public void setUNS_AssetMovement_ID (int UNS_AssetMovement_ID)
	{
		if (UNS_AssetMovement_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AssetMovement_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AssetMovement_ID, Integer.valueOf(UNS_AssetMovement_ID));
	}

	/** Get Asset Movement.
		@return Asset Movement	  */
	public int getUNS_AssetMovement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AssetMovement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Asset Movement Line.
		@param UNS_AssetMovementLine_ID Asset Movement Line	  */
	public void setUNS_AssetMovementLine_ID (int UNS_AssetMovementLine_ID)
	{
		if (UNS_AssetMovementLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AssetMovementLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AssetMovementLine_ID, Integer.valueOf(UNS_AssetMovementLine_ID));
	}

	/** Get Asset Movement Line.
		@return Asset Movement Line	  */
	public int getUNS_AssetMovementLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AssetMovementLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_AssetMovementLine_UU.
		@param UNS_AssetMovementLine_UU UNS_AssetMovementLine_UU	  */
	public void setUNS_AssetMovementLine_UU (String UNS_AssetMovementLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_AssetMovementLine_UU, UNS_AssetMovementLine_UU);
	}

	/** Get UNS_AssetMovementLine_UU.
		@return UNS_AssetMovementLine_UU	  */
	public String getUNS_AssetMovementLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_AssetMovementLine_UU);
	}
}