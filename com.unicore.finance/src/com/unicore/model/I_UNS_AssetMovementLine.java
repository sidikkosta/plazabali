/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_AssetMovementLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_AssetMovementLine 
{

    /** TableName=UNS_AssetMovementLine */
    public static final String Table_Name = "UNS_AssetMovementLine";

    /** AD_Table_ID=1000493 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_Addition_ID */
    public static final String COLUMNNAME_A_Asset_Addition_ID = "A_Asset_Addition_ID";

	/** Set Asset Addition	  */
	public void setA_Asset_Addition_ID (int A_Asset_Addition_ID);

	/** Get Asset Addition	  */
	public int getA_Asset_Addition_ID();

	public org.compiere.model.I_A_Asset_Addition getA_Asset_Addition() throws RuntimeException;

    /** Column name A_Asset_ID */
    public static final String COLUMNNAME_A_Asset_ID = "A_Asset_ID";

	/** Set Asset.
	  * Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID);

	/** Get Asset.
	  * Asset used internally or by customers
	  */
	public int getA_Asset_ID();

	public org.compiere.model.I_A_Asset getA_Asset() throws RuntimeException;

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DepartmentFrom_ID */
    public static final String COLUMNNAME_DepartmentFrom_ID = "DepartmentFrom_ID";

	/** Set Sec Of Dept From	  */
	public void setDepartmentFrom_ID (int DepartmentFrom_ID);

	/** Get Sec Of Dept From	  */
	public int getDepartmentFrom_ID();

	public org.compiere.model.I_C_BPartner getDepartmentFrom() throws RuntimeException;

    /** Column name DepartmentTo_ID */
    public static final String COLUMNNAME_DepartmentTo_ID = "DepartmentTo_ID";

	/** Set Sec Of Dept To	  */
	public void setDepartmentTo_ID (int DepartmentTo_ID);

	/** Get Sec Of Dept To	  */
	public int getDepartmentTo_ID();

	public org.compiere.model.I_C_BPartner getDepartmentTo() throws RuntimeException;

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public org.compiere.model.I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_LocatorTo_ID */
    public static final String COLUMNNAME_M_LocatorTo_ID = "M_LocatorTo_ID";

	/** Set Locator To.
	  * Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID);

	/** Get Locator To.
	  * Location inventory is moved to
	  */
	public int getM_LocatorTo_ID();

	public org.compiere.model.I_M_Locator getM_LocatorTo() throws RuntimeException;

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name UNS_AssetMovement_ID */
    public static final String COLUMNNAME_UNS_AssetMovement_ID = "UNS_AssetMovement_ID";

	/** Set Asset Movement	  */
	public void setUNS_AssetMovement_ID (int UNS_AssetMovement_ID);

	/** Get Asset Movement	  */
	public int getUNS_AssetMovement_ID();

	public com.unicore.model.I_UNS_AssetMovement getUNS_AssetMovement() throws RuntimeException;

    /** Column name UNS_AssetMovementLine_ID */
    public static final String COLUMNNAME_UNS_AssetMovementLine_ID = "UNS_AssetMovementLine_ID";

	/** Set Asset Movement Line	  */
	public void setUNS_AssetMovementLine_ID (int UNS_AssetMovementLine_ID);

	/** Get Asset Movement Line	  */
	public int getUNS_AssetMovementLine_ID();

    /** Column name UNS_AssetMovementLine_UU */
    public static final String COLUMNNAME_UNS_AssetMovementLine_UU = "UNS_AssetMovementLine_UU";

	/** Set UNS_AssetMovementLine_UU	  */
	public void setUNS_AssetMovementLine_UU (String UNS_AssetMovementLine_UU);

	/** Get UNS_AssetMovementLine_UU	  */
	public String getUNS_AssetMovementLine_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
