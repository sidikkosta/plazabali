/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

import com.uns.model.MUNSChequebook;

/**
 * @author nurse
 *
 */
public class MUNSChequeList extends X_UNS_Cheque_List 
{
	private static final long serialVersionUID = 1024135222690978821L;
	private MUNSChequebook m_book = null;
	
	public MUNSChequebook getParent ()
	{
		if (m_book == null)
			m_book = new MUNSChequebook(
					getCtx(), getUNS_Chequebook_ID(), get_TrxName());
		return m_book;
	}
	
	public MUNSChequeList (MUNSChequebook book)
	{
		this (book.getCtx(), 0, book.get_TrxName());
		m_book = book;
		setClientOrg(book);
		setUNS_Chequebook_ID(book.get_ID());
	}
	
	/**
	 * @param ctx
	 * @param UNS_Cheque_List_ID
	 * @param trxName
	 */
	public MUNSChequeList(Properties ctx, int UNS_Cheque_List_ID, String trxName) 
	{
		super(ctx, UNS_Cheque_List_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSChequeList(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public static String getName (String trxName, int chequeListID)
	{
		String sql = "SELECT Name FROM UNS_Cheque_List WHERE UNS_Cheque_List_ID = ?";
		return DB.getSQLValueString(trxName, sql, chequeListID);
	}
}
