/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.DBException;
import org.compiere.model.MAssetAcct;
import org.compiere.model.MDepreciationEntry;
import org.compiere.model.MDepreciationExp;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.Env;

/**
 * @author nurse
 *
 */
public class MUNSAssetLeaseSettlement extends X_UNS_AssetLeaseSettlement
		implements DocAction 
{

	private static final long serialVersionUID = 1870871416113012404L;
	private boolean m_justPrepared = false;
	private String m_processMsg = null;
	private MDepreciationExp[] m_depreciations = null;

	/**
	 * @param ctx
	 * @param UNS_AssetLeaseSettlement_ID
	 * @param trxName
	 */
	public MUNSAssetLeaseSettlement(Properties ctx,
			int UNS_AssetLeaseSettlement_ID, String trxName) {
		super(ctx, UNS_AssetLeaseSettlement_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAssetLeaseSettlement(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String action) throws Exception 
	{
		trace(Level.INFO, "ProcessIt[" + action + "]");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() 
	{
		trace(Level.INFO, "unlockIt");
		setProcessing(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		trace(Level.INFO, "InvalidateIt");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		trace(Level.INFO, "PrepareIt");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		List<MDepreciationExp> befores = getDepreciationBefore();
		for (int i=0; i<befores.size(); i++)
		{
			if (!befores.get(i).isProcessed())
			{
				m_processMsg = "Please process depreciation document first";
				return DOCSTATUS_Invalid;
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		m_justPrepared = true;
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		trace(Level.INFO, "ApproveIt");
		setIsApproved(true);
		setProcessed(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() 
	{
		trace(Level.INFO, "RejectIt");
		setProcessed(false);
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		trace(Level.INFO, "CompleteIt");
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DOCSTATUS_InProgress.equals(status))
				return DOCSTATUS_Invalid;
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		try
		{
			List<MDepreciationExp> afters = getDepreciationAfter();
			for (int i=0; i<afters.size(); i++)
			{
				if (afters.get(i).isProcessed())
					MDepreciationEntry.deleteFacts(afters.get(i));
				int acctID = MAssetAcct.forA_Asset_ID(
						getCtx(), getA_Asset_ID(), afters.get(i).getPostingType(), 
						afters.get(i).getDateAcct(), get_TrxName()).getA_Accumdepreciation_Acct();
				afters.get(i).setCR_Account_ID(acctID);
				afters.get(i).saveEx();
			}
		}
		catch (Exception ex)
		{
			m_processMsg = ex.getMessage();
			return DOCSTATUS_Invalid;
		}
		if (!isApproved())
			approveIt();
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		trace(Level.INFO, "VoidIt");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		try
		{
			List<MDepreciationExp> afters = getDepreciationAfter();
			for (int i=0; i<afters.size(); i++)
			{
				if (afters.get(i).isProcessed())
					MDepreciationEntry.deleteFacts(afters.get(i));
				int acctID = MAssetAcct.forA_Asset_ID(
						getCtx(), getA_Asset_ID(), afters.get(i).getPostingType(), 
						afters.get(i).getDateAcct(), get_TrxName()).getUNS_DepreciationLease_Acct();
				afters.get(i).setCR_Account_ID(acctID);
				afters.get(i).saveEx();
			}
			
		}
		catch (DBException ex)
		{
			m_processMsg = ex.getMessage();
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt()
	{
		trace(Level.INFO, "CloseIt");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		m_processMsg = "not implemented action";
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() 
	{
		m_processMsg = "Not implemented action";
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt() 
	{
		trace(Level.INFO, "ReactivateIt");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		try
		{
			List<MDepreciationExp> afters = getDepreciationAfter();
			for (int i=0; i<afters.size(); i++)
			{
				if (afters.get(i).isProcessed())
					MDepreciationEntry.deleteFacts(afters.get(i));
				int acctID = MAssetAcct.forA_Asset_ID(
						getCtx(), getA_Asset_ID(), afters.get(i).getPostingType(), 
						afters.get(i).getDateAcct(), get_TrxName()).getUNS_DepreciationLease_Acct();
				afters.get(i).setCR_Account_ID(acctID);
				afters.get(i).saveEx();
			}
			
		}
		catch (DBException ex)
		{
			m_processMsg = ex.getMessage();
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}
	
	@Override
	public String toString ()
	{
		StringBuilder msgBuild = new StringBuilder(Table_Name);
		msgBuild.append("[").append("Document No : ").append(getDocumentNo())
		.append("]");
		String info = msgBuild.toString();
		return info;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary()
	{
		return toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() 
	{
		return toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID()
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() 
	{
		return Env.ZERO;
	}

	private void trace (Level level, String msg)
	{
		if (log.isLoggable(level))
			log.log(level, msg);
	}
	
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public MDepreciationExp[] getDepreciations (boolean requery)
	{
		if (m_depreciations != null && !requery)
		{
			set_TrxName(m_depreciations, get_TrxName());
			return m_depreciations;
		}
		
		List<MDepreciationExp> list = new Query(
				getCtx(), MDepreciationExp.Table_Name, 
				MDepreciationExp.COLUMNNAME_A_Asset_ID + "=? AND PostingType = ?", 
				get_TrxName()).setParameters(getA_Asset_ID(), getPostingType()).list();
		
		m_depreciations = new MDepreciationExp[list.size()];
		list.toArray(m_depreciations);
		return m_depreciations;
	}
	
	public List<MDepreciationExp> getDepreciationBefore ()
	{
		List<MDepreciationExp> result = new ArrayList<>();
		MDepreciationExp[] depreciations = getDepreciations(false);
		for (int i=0; i<depreciations.length; i++)
		{
			Calendar depreciationCal = Calendar.getInstance();
			Calendar leaseSettleCal = Calendar.getInstance();
			depreciationCal.setTimeInMillis(depreciations[i].getDateAcct().getTime());
			leaseSettleCal.setTimeInMillis(getDateAcct().getTime());
			if ((depreciationCal.get(Calendar.MONTH) <= leaseSettleCal.get(Calendar.MONTH) 
					&& depreciationCal.get(Calendar.YEAR) == leaseSettleCal.get(Calendar.YEAR))
					|| depreciationCal.get(Calendar.YEAR) < leaseSettleCal.get(Calendar.YEAR))
				result.add(depreciations[i]);
		}
		return result;
	}
	
	public List<MDepreciationExp> getDepreciationAfter ()
	{
		List<MDepreciationExp> result = new ArrayList<>();
		MDepreciationExp[] depreciations = getDepreciations(false);
		for (int i=0; i<depreciations.length; i++)
		{
			Calendar depreciationCal = Calendar.getInstance();
			Calendar leaseSettleCal = Calendar.getInstance();
			depreciationCal.setTimeInMillis(depreciations[i].getDateAcct().getTime());
			leaseSettleCal.setTimeInMillis(getDateAcct().getTime());
			if ((depreciationCal.get(Calendar.MONTH) > leaseSettleCal.get(Calendar.MONTH)
					&& depreciationCal.get(Calendar.YEAR) == leaseSettleCal.get(Calendar.YEAR))
					|| depreciationCal.get(Calendar.YEAR) > leaseSettleCal.get(Calendar.YEAR))
				result.add(depreciations[i]);
		}
		return result;
	}
}
