/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MBPBankAccount;
import org.compiere.model.MBankAccount;
import org.compiere.model.MOrg;
import org.compiere.model.MPeriod;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.unicore.base.model.MPayment;
import com.uns.model.IUNSApprovalInfo;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDeposit extends X_UNS_Deposit implements DocAction, DocOptions, IUNSApprovalInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3851542425654204247L;

	public MUNSDeposit(Properties ctx, int UNS_Deposit_ID, String trxName) {
		super(ctx, UNS_Deposit_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		if(getAD_Org_ID() == 0)
			throw new AdempiereUserError("Org * disallowed");
		
		return true;
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		
		return true;
	}
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_Deposit_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF


	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		MPeriod.testPeriodOpen(getCtx(), getDateTrx(), getC_DocType_ID(), getAD_Org_ID());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Approve Document
	 * 	@return true if success 
	 */
	public boolean  approveIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}	//	approveIt
	
	/**
	 * 	Reject Approval
	 * 	@return true if success 
	 */
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}	//	rejectIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		if(!validationDepositAccount(getBankAccount()))
			return DocAction.STATUS_Invalid;
		
		if(null != createDeposit())
			return DocAction.STATUS_Invalid;

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
//		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DocumentNo), '; ') FROM C_Payment WHERE DocStatus NOT IN ('RE', 'VO')"
//				+ " AND C_Payment_ID IN (SELECT C_Payment_ID FROM C_PaymentAllocate WHERE C_Invoice_ID=?)";
//		String listPay = DB.getSQLValueString(get_TrxName(), sql, getC_Invoice_ID());
//		if(!Util.isEmpty(listPay, true))
//		{
//			m_processMsg = "Invoice has processed in payment (" + listPay + ")";
//			return false;
//		}
		
		if(getC_Payment_ID() > 0)
		{
			try
			{
				MPayment pay = new MPayment(getCtx(), getC_Payment_ID(), get_TrxName());
				boolean ok = pay.processIt(DOCACTION_Void);
				pay.saveEx();
				if(!ok)
				{
					m_processMsg = pay.getProcessMsg();
					return false;
				}
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
			}
		}
		
		if(getPaymentTo_ID() > 0)
		{
			try
			{
				MPayment payTo = new MPayment(getCtx(), getPaymentTo_ID(), get_TrxName());
				boolean ok = payTo.processIt(DOCACTION_Void);
				payTo.saveEx();
				if(!ok)
				{
					m_processMsg = payTo.getProcessMsg();
					return false;
				}
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
			}
		}
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setProcessed(false);
		setDocAction(DOCACTION_Complete);
		return true;
	}	//	reActivateIt
	
	/*************************************************************************
	 * 	Get Summary
	 *	@return Summary of Document
	 */
	public String getSummary()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		//	 - Organization
		MOrg org = new MOrg(getCtx(), getAD_Org_ID(), get_TrxName());
		sb.append(" #Org : ").append(org.getName());
		//	 - Date
		sb.append(" #Date : ").append(getDateTrx());
		//	 - User
		sb.append(" #User : ").append(Env.getContext(getCtx(), "#AD_User_Name"));
		//	 - Priority
		sb.append(" #Priority : ").append(getTenderType());
		//	 - Warehouse
		sb.append(" #Warehouse : ").append(getC_BPartner().getName());
		
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		return sb.toString();
	}	//	getSummary
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	
	/**
	 * 	Get Document Owner
	 *	@return AD_User_ID
	 */
	public int getDoc_User_ID()
	{
		return getAD_User_ID();
	}
	
	/**
	 * 	Get Document Currency
	 *	@return C_Currency_ID
	 */
	public int getC_Currency_ID()
	{
		return super.getC_Currency_ID();
	}

	/**
	 * 	Get Document Approval Amount
	 *	@return amount
	 */
	public BigDecimal getApprovalAmt()
	{
		return getAmount();
	}
		
	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getDetailTableHeader() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object[]> getDetailTableContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_Void;
		}
		return index;
	}
	
//	public String createAndPaidInvoice()
//	{
//		m_processMsg = null;
//		MInvoice inv = null;
//		
//		if(getC_Invoice_ID() > 0)
//			inv = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName()); 
//		else
//		{
//			inv = new MInvoice(getCtx(), 0, get_TrxName());
//			
//			inv.setClientOrg(getAD_Client_ID(), getAD_Org_ID());
//			inv.setIsSOTrx(isSOTrx());
//			if(!isSOTrx())
//				inv.setC_DocTypeTarget_ID(MDocType.getDocType(MDocType.DOCBASETYPE_APCreditMemo));
//			else
//				inv.setC_DocTypeTarget_ID(MDocType.getDocType(MDocType.DOCBASETYPE_ARCreditMemo));
//			inv.setDateAcct(getDateTrx());
//			inv.setDateInvoiced(getDateTrx());
//			inv.setC_BPartner_ID(getC_BPartner_ID());
//			inv.setC_BPartner_Location_ID(getC_BPartner_Location_ID());
//			inv.setPaymentRule(X_C_Invoice.PAYMENTRULE_OnCredit);
//			inv.setM_PriceList_ID(getM_PriceList_ID());
//			inv.setisDeposit(true);
//			inv.setSalesRep_ID(getAD_User_ID());
//			inv.setAD_User_ID(Env.getContextAsInt(getCtx(), " #AD_User_ID"));
//			inv.setC_Currency_ID(getC_Currency_ID());
//			try
//			{	
//				boolean ok = inv.save();
//				if (!ok)
//				{
//					m_processMsg = inv.getProcessMsg();
//					return m_processMsg;
//				}
//				else
//				{
//					MInvoiceLine invL = new MInvoiceLine(inv);
//					invL.setC_Charge_ID(getC_Charge_ID());
//					invL.setQtyInvoiced(Env.ONE);
//					invL.setPriceActual(getAmount());
//					invL.setPriceEntered(getAmount());
//					invL.setPriceList(getAmount());
//					invL.setC_Tax_ID(getC_Tax_ID());
//					if(!invL.save())
//					{
//						m_processMsg = "Failed when trying save invoice line";
//						return m_processMsg;
//					}
//				}
//				
//				ok = inv.processIt(DOCACTION_Complete);
//				if(!ok)
//				{
//					m_processMsg = inv.getProcessMsg();
//					return m_processMsg;
//				}
//			}
//			catch (Exception ex)
//			{
//				m_processMsg = ex.getMessage();
//				return m_processMsg;
//			}
//			setC_Invoice_ID(inv.get_ID());
//		}
//		
//		if(getC_Payment_ID() <= 0)
//			return createPayment(inv.getGrandTotal());
//		
//		return m_processMsg;
//	}
//	
//	public String createPayment(BigDecimal ChargeAmt)
//	{
//		m_processMsg = null;
//		
//		if(getC_Payment_ID() > 0)
//			return m_processMsg;
//		
//		MPayment pay = new MPayment(getCtx(), 0, get_TrxName());
//		pay.setAD_Org_ID(getAD_Org_ID());
//		pay.setC_BankAccount_ID(getC_BankAccount_ID());
//		pay.setC_DocType_ID(isSOTrx());
//		pay.setDateAcct(getDateTrx());
//		pay.setDateTrx(getDateTrx());
//		pay.setC_BPartner_ID(getC_BPartner_ID());
//		pay.setC_Charge_ID(getC_Charge_ID());
//		pay.setChargeAmt(ChargeAmt);
//		pay.setC_Currency_ID(getC_Currency_ID());
//		pay.setTenderType(getTenderType());
//		pay.setCheckNo(getChequeNo());
//		pay.setDisbursementDate(getDisbursementDate());
//		try
//		{	
//			boolean ok = pay.save();
//			if (!ok)
//			{
//				m_processMsg = pay.getProcessMsg();
//				return m_processMsg;
//			}
////			MPayment payment = new MPayment(getCtx(), pay.get_ID(), get_TrxName());
//			ok = pay.processIt(DOCACTION_Complete);
//			if(!ok)
//			{
//				m_processMsg = pay.getProcessMsg();
//				return m_processMsg;
//			}
//		}
//		catch (Exception ex)
//		{
//			m_processMsg = ex.getMessage();
//			return m_processMsg;
//		}
//		
//		setC_Payment_ID(pay.get_ID());
//		
//		return m_processMsg;
//	}
	
	public String createDeposit()
	{
		m_processMsg = null;
		MPayment pay = null;
		
		if(getC_Payment_ID() > 0)
		{
			pay = new MPayment(getCtx(), getC_Payment_ID(), get_TrxName());
			if(!pay.isComplete())
			{
				if(!pay.processIt(DOCACTION_Complete))
					m_processMsg = pay.getProcessMsg();
			}
		}
		else
		{
			pay = new MPayment(getCtx(), 0, get_TrxName());
			pay.setAD_Org_ID(getAD_Org_ID());
			pay.setC_BankAccount_ID(getC_BankAccount_ID());
			pay.setIsReceipt(false);
			pay.setDateAcct(getDateTrx());
			pay.setDateTrx(getDateTrx());
			pay.setC_BPartner_ID(getC_BPartner_ID());
			pay.setC_Charge_ID(getC_Charge_ID());
			pay.setPayAmt(getAmount());
			pay.setC_Currency_ID(getC_Currency_ID());
			pay.setTenderType(getTenderType());
			pay.setCheckNo(getChequeNo());
			pay.setDisbursementDate(getDisbursementDate());
			try
			{	
				boolean ok = pay.save();
				if (!ok)
				{
					m_processMsg = pay.getProcessMsg();
					return m_processMsg;
				}
				setC_Payment_ID(pay.get_ID());
				
				ok = pay.processIt(DOCACTION_Complete);
				if(!ok)
				{
					m_processMsg = pay.getProcessMsg();
					return m_processMsg;
				}
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return m_processMsg;
			}
		}
		
		MPayment payTo = null;
		
//		if(getC_Payment_ID() > 0)
//		{
//			payTo = new MPayment(getCtx(), getC_Payment_ID(), get_TrxName());
//			if(!payTo.isComplete())
//			{
//				if(!payTo.processIt(DOCACTION_Complete))
//					m_processMsg = payTo.getProcessMsg();
//			}
//		}
//		else
//		{
			payTo = new MPayment(getCtx(), 0, get_TrxName());
			payTo.setAD_Org_ID(getAD_Org_ID());
			payTo.setC_BankAccount_ID(getBankAccount());
			payTo.setIsReceipt(true);
			payTo.setDateAcct(getDateTrx());
			payTo.setDateTrx(getDateTrx());
			payTo.setC_BPartner_ID(getC_BPartner_ID());
			payTo.setC_Charge_ID(getC_Charge_ID());
			payTo.setChargeAmt(getAmount());
			payTo.setPayAmt(getAmount());
			payTo.setC_Currency_ID(getC_Currency_ID());
			payTo.setTenderType(getTenderType());
			payTo.setCheckNo(getChequeNo());
			payTo.setDisbursementDate(getDisbursementDate());
			payTo.setReference_ID(pay.get_ID());
			try
			{	
				boolean ok = payTo.save();
				pay.setReference_ID(payTo.get_ID());
				pay.saveEx();
				if (!ok)
				{
					m_processMsg = payTo.getProcessMsg();
					return m_processMsg;
				}
				setPaymentTo_ID(payTo.get_ID());
				ok = payTo.processIt(DOCACTION_Complete);
				if(!ok)
				{
					m_processMsg = payTo.getProcessMsg();
					return m_processMsg;
				}
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return m_processMsg;
			}
//		}
		return m_processMsg;
	}
	
	public int getBankAccount()
	{
		int C_BankAccount_ID = 0;
		MBankAccount account = null;
		MBPBankAccount bpAccount = null;
		
		if(getC_BankAccountTo_ID() > 0)
		{
			C_BankAccount_ID = getC_BankAccountTo_ID();
			return C_BankAccount_ID;
		}
		
		if(getC_BP_BankAccount_ID() > 0 && getC_BankAccountTo_ID() < 0)
		{
			bpAccount = new MBPBankAccount(getCtx(), getC_BP_BankAccount_ID(), get_TrxName());
			if(bpAccount.getC_BankAccount_ID() > 0 )
			{
				account = new MBankAccount(getCtx(), bpAccount.getC_BankAccount_ID(), get_TrxName());
				if(!account.isActive())
					m_processMsg = "Deposit Account for this Business not actived";
			} 
			else
			{
				String sql = "SELECT C_Bank_ID FROM C_Bank WHERE Name LIKE '%DEPOSIT%' AND isActive = 'Y'";
				int bankID = DB.getSQLValue(get_TrxName(), sql);
				account = new MBankAccount(getCtx(), 0, get_TrxName());
				account.setAD_Org_ID(bpAccount.getC_Bank().getAD_Org_ID());
				account.setC_Bank_ID(bankID);
				account.setValue(getC_BPartner().getValue() + " " + bpAccount.getAccountNo());
				account.setName(bpAccount.getAD_User().getName());
				account.setAccountNo(bpAccount.getAccountNo());
				account.setBankAccountType(bpAccount.getBankAccountType());
				account.setC_Currency_ID(getC_Currency_ID());
				if(!account.save())
				{
					m_processMsg = "Failed when trying create Bank Account";
				}
				else
				{
					C_BankAccount_ID = account.get_ID();
					bpAccount.setC_BankAccount_ID(C_BankAccount_ID);
					bpAccount.saveEx();
				}
			}
		}
		
//		if(getC_BP_BankAccount_ID() < 0 && getC_BankAccountTo_ID() < 0)
//		{
//			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo()
//					, "Business Partner Account not have Bank Account. \n" +
//					  "Do you want to create account deposit?"
//					, "Create account deposit confirmation"
//					, MessageBox.YESNO
//					, MessageBox.ICONQUESTION);
//			if(retVal == MessageBox.RETURN_YES)
//			{
//				MBPartner bp = new MBPartner(getCtx(), getC_BPartner_ID(), get_TrxName());
//				
//			}
//		}
		
		return C_BankAccount_ID;
	}
	
	public boolean validationDepositAccount (int C_BankAccount_ID)
	{
		m_processMsg = null;
		
		if(C_BankAccount_ID > 0)
		{
			String checking = "SELECT 1 FROM C_Bank WHERE Name like '%DEPOSIT%' AND C_Bank_ID ="
					+ " (SELECT C_Bank_ID FROM C_BankAccount WHERE C_BankAccount_ID = ?)";
			boolean isDeposit = DB.getSQLValue(get_TrxName(), checking, C_BankAccount_ID) > 0 ? true : false;
			if(!isDeposit)
				m_processMsg = "this account not deposit account";
			return isDeposit;
		}
			
//			if(bpAccount.getC_BankAccount_ID() < 0)
//			{
//				int retVal = MessageBox.showMsg(getCtx(), getProcessInfo()
//						, "Business Partner Account not have Bank Account. \n" +
//						  "Do you want to create account deposit?"
//						, "Create account deposit confirmation"
//						, MessageBox.YESNO
//						, MessageBox.ICONQUESTION);
//				if(retVal == MessageBox.RETURN_YES)
//				{
//					String upOrder = "UPDATE C_Order SET DocStatus = 'CL', DocAction = '--' WHERE C_Order_ID IN";
//					int count = DB.executeUpdate(upOrder, get_TrxName());
//					if(count < 0)
//					{
//						
//					}
//				}
//			}
		return true;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}