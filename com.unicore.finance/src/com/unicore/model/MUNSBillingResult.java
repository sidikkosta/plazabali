/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

//import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSFinanceModelFactory;

/**
 * @author setyaka
 * 
 */
public class MUNSBillingResult extends X_UNS_Billing_Result 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4092460198142085656L;

	/**
	 * @param ctx
	 * @param UNS_Billing_Confirm_ID
	 * @param trxName
	 */
	public MUNSBillingResult(Properties ctx, int UNS_Billing_Confirm_ID, String trxName) {
		super(ctx, UNS_Billing_Confirm_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSBillingResult(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/**
	 * 
	 * @param parent
	 */
	public MUNSBillingResult(MUNSBillingGroupResult parent) {
		this(parent.getCtx(), 0, parent.get_TrxName());

		setClientOrg(parent);

		setIsApproved(false);
		setTotalAmt(Env.ZERO);
		setDifferenceAmt(Env.ZERO);
		setPaidAmt(Env.ZERO);
		setUNS_BillingGroup_Result_ID(parent.get_ID());
	}

	MUNSBillingLineResult[] m_lines = null;

	/**
	 * 
	 * @param query
	 * @return
	 */
	public MUNSBillingLineResult[] getLines(boolean query) {
		if (m_lines == null || query)
			m_lines = getLines(null);

		return m_lines;
	}

	/**
	 * @param whereClause
	 * @param UNS_BillingLine_Result_ID
	 * @return
	 */
	public MUNSBillingLineResult[] getLines(String whereClause) 
	{
		String whereClauseFinal = "UNS_Billing_Result_ID=? ";
		if (whereClause != null)
			whereClauseFinal += whereClause;

		List<MUNSBillingLineResult> list =
				Query.get(getCtx(), UNSFinanceModelFactory.EXTENSION_ID, MUNSBillingLineResult.Table_Name,
						whereClauseFinal, get_TrxName()).setParameters(new Object[] {getUNS_Billing_Result_ID()})
						.setOrderBy(MUNSBillingLineResult.COLUMNNAME_UNS_BillingLine_Result_ID).list();

		return list.toArray(new MUNSBillingLineResult[list.size()]);
	}
	
	/**
	 * 
	 * @return
	 */
	protected boolean updateHeader()
	{
//		String sql = "UPDATE UNS_BillingGroup_Result bgr "
//				+ "SET (GrandTotal, OpenAmt, DifferenceAmt, PaidAmt, PaidAmtGiro, ReceiptAmt, ReceiptAmtGiro) = "
//				+ " (SELECT COALESCE(SUM(br.TotalAmt),0), COALESCE(SUM(br.OpenAmt),0), "
//				+ "			COALESCE(SUM(br.DifferenceAmt),0), "
//				+ "			COALESCE(SUM(br.PaidAmt),0), COALESCE(SUM(br.PaidAmtGiro),0), "
//				+ "			COALESCE(SUM(br.ReceiptAmt),0), COALESCE(SUM(br.ReceiptAmtGiro),0 "
//				+ "	 FROM UNS_Billing_Result br "
//				+ "	 WHERE br.UNS_BillingGroup_Result_ID=bgr.UNS_BillingGroup_Result_ID) "
//				+ "WHERE br.UNS_BillingGroup_Result_ID=?";

		String sql = 
				"SELECT COALESCE(SUM(br.TotalAmt),0), COALESCE(SUM(br.OpenAmt),0), "
				+ "		COALESCE(SUM(br.DifferenceAmt),0), "
				+ "		COALESCE(SUM(br.PaidAmt),0), COALESCE(SUM(br.PaidAmtGiro),0), "
				+ "		COALESCE(SUM(br.ReceiptAmt),0), COALESCE(SUM(br.ReceiptAmtGiro),0) "
				+ "FROM UNS_Billing_Result br "
				+ "WHERE br.UNS_BillingGroup_Result_ID = ? AND br.IsActive='Y' ";
		
		List<List<Object>> lineAmtList = 
				DB.getSQLArrayObjectsEx(get_TrxName(), sql, getUNS_BillingGroup_Result_ID());
		
		if (lineAmtList == null)
			return false;
		
		BigDecimal grandtotal = (BigDecimal) lineAmtList.get(0).get(0);
		BigDecimal totalOpenAmt = (BigDecimal) lineAmtList.get(0).get(1);
		BigDecimal totalDifferent = (BigDecimal) lineAmtList.get(0).get(2);
		BigDecimal totalPaidAmt = (BigDecimal) lineAmtList.get(0).get(3);
		BigDecimal totalPaidGiroAmt = (BigDecimal) lineAmtList.get(0).get(4);
		BigDecimal totalReceiptAmt = (BigDecimal) lineAmtList.get(0).get(5);
		BigDecimal totalReceiptGiroAmt = (BigDecimal) lineAmtList.get(0).get(6);
		
				//DB.getSQLValueBD(get_TrxName(), sql, getUNS_BillingGroup_Result_ID());

		//sql = "SELECT SUM(PaidAmt) FROM UNS_Billing_Result WHERE UNS_BillingGroup_Result_ID = ? AND IsActive='Y' ";
//		BigDecimal paidAmt = (BigDecimal) lineAmtList.get(0).get(2);
//				//DB.getSQLValueBD(get_TrxName(), sql, bg.getUNS_BillingGroup_Result_ID());
//		
//		sql = "SELECT SUM(PaidAmtGiro) FROM UNS_Billing_Result WHERE UNS_BillingGroup_Result_ID = ? AND IsActive = 'Y'";
//		BigDecimal paidAmtGiro = DB.getSQLValueBD(get_TrxName(), sql, getUNS_BillingGroup_Result_ID());
//		
//		sql = "SELECT SUM(ReceiptAmt) FROM UNS_Billing_Result WHERE UNS_BillingGroup_Result_ID = ? AND IsActive='Y'";
//		BigDecimal receiptAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_BillingGroup_Result_ID());
//		
//		sql = "SELECT SUM(ReceiptAmtGiro) FROM UNS_Billing_Result WHERE UNS_BillingGroup_Result_ID = ? AND IsActive='Y'";
//		BigDecimal receiptAmtGiro = DB.getSQLValueBD(get_TrxName(), sql, getUNS_BillingGroup_Result_ID());
//		
//		BigDecimal totalPaid = paidAmt.add(paidAmtGiro);
//		BigDecimal differenceAmt = grandtotal.subtract(totalPaid);
		

		sql =
				"UPDATE UNS_BillingGroup_Result SET GrandTotal = " + grandtotal + ", paidAmt=" + totalPaidAmt
						+ ", OpenAmt=" + totalOpenAmt
						+ ", DifferenceAmt=" + totalDifferent + ", ReceiptAmt = "  + totalReceiptAmt 
						+ ", ReceiptAmtGiro = " + totalReceiptGiroAmt + ", PaidAmtGiro = " + totalPaidGiroAmt 
						+ " WHERE UNS_BillingGroup_Result_ID =" + getUNS_BillingGroup_Result_ID();
		int ok = DB.executeUpdate(sql, get_TrxName());

		return (ok > 0);
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{
//		MUNSBilling bl = new MUNSBilling(getCtx(), getUNS_Billing_ID(), get_TrxName());
//		
//		for(MUNSBillingLine billing : bl.getLines(true))
//		{
//			MUNSBillingLineResult blr = new MUNSBillingLineResult(getCtx(), 0, get_TrxName());
//			
//			BigDecimal OpenAmt = DB.getSQLValueBD(get_TrxName(), "SELECT invoiceopen(?,0)", billing.getC_Invoice_ID());
//			if(OpenAmt.compareTo(Env.ZERO) <= 0 )
//				continue;
//			if(DB.getSQLValue(get_TrxName(), "SELECT 1 FROM UNS_BillingLine_Result WHERE"
//					+ " C_Invoice_ID = " + billing.getC_Invoice_ID()
//					+ " AND UNS_Billing_Result_ID = " + get_ID()) > 0)
//				continue;
//			
//			blr.setAD_Org_ID(billing.getAD_Org_ID());
//			blr.setUNS_Billing_Result_ID(get_ID());
//			blr.setC_Invoice_ID(billing.getC_Invoice_ID());
//			blr.setPaymentStatus("NP");
//			blr.setUNS_BillingLine_ID(billing.get_ID());
//			blr.setNetAmtToInvoice(blr.getC_Invoice().getGrandTotal());
//			blr.setOpenAmt(OpenAmt);
//			blr.setDifferenceAmt(blr.getC_Invoice().getGrandTotal().subtract(OpenAmt));
//			if(!blr.save())
//				throw new AdempiereException("Error when trying auto create invoice");
//		}
		
		if (newRecord)
			return true;
		
		return updateHeader();
	}
	
	@Override
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_BillingLine_Result WHERE UNS_Billing_Result_ID=" + getUNS_Billing_Result_ID();
		
		DB.executeUpdateEx(sql, get_TrxName());
		
		return true;
	}
	
	@Override 
	protected boolean afterDelete(boolean success)
	{
		return updateHeader();
	}
}