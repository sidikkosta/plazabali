/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankAccount;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.Msg;

import com.unicore.base.model.MBankStatementLine;
import com.unicore.model.factory.UNSFinanceModelFactory;
import com.uns.base.model.Query;
import com.uns.model.MUNSChequebook;

/**
 * @author menjangan
 *
 */
public class MUNSTransferBalanceRequest extends X_UNS_TransferBalance_Request 
												implements DocAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MUNSTransferBalanceConfirm[] m_confirmations = null;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private X_UNS_PettyCashRequest[] m_pettycashreq = null;

	/**
	 * @param ctx
	 * @param UNS_TransferBalance_Request_ID
	 * @param trxName
	 */
	public MUNSTransferBalanceRequest(Properties ctx,
			int UNS_TransferBalance_Request_ID, String trxName) {
		super(ctx, UNS_TransferBalance_Request_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSTransferBalanceRequest(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean processIt(String action) throws Exception {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(getAccountTo_ID() == 0)
		{
			m_processMsg = "Field Account To is mandatory !";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = createConfirmation();
		if(null != m_processMsg)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		m_justPrepared = true;
		
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
//		Implicit Approval
		if (!isApproved())
			approveIt();
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		MUNSTransferBalanceConfirm[] confirms = getConfirmations(true);
		for(MUNSTransferBalanceConfirm confirm : confirms)
		{
			if(!confirm.isComplete())
			{
				m_processMsg = "Open: @Transfer Confirm@ - " 
						+ confirm.getDocumentNo();
					return DocAction.STATUS_InProgress;
			}
		}
		
		if(getAccountFrom_ID() == 0)
		{
			m_processMsg = "Field mandatory Account From!";
			return DocAction.STATUS_Invalid;
		}
		
		if(getAccountTo_ID() == 0)
		{
			m_processMsg = "Field mandatory Account To!";
			return DocAction.STATUS_Invalid;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(!isAuto())
		{	
			MUNSTransferBalanceConfirm confirm = MUNSTransferBalanceConfirm.get(getCtx(), get_ID(), get_TrxName());
			if(confirm != null && !confirm.getDocStatus().equals("VO")
					&& !confirm.getDocStatus().equals("RE"))
			{
				m_processMsg = "Void request confirmation first.";
				return false;
			}
		}

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reverseCorrectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reActivateIt()
	{
		if(!isAuto())
		{
			m_processMsg = "Reactivate not permitted.";
			return false;
		}
		return true;
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * Return null if success or return error message if not success.
	 * @return
	 */
	public String createConfirmation()
	{
		String errorMsg = null;
		
		MUNSTransferBalanceConfirm[] confirms = getConfirmations(true);
		
		if(null != confirms && confirms.length > 0)
			return errorMsg;
		
		MUNSTransferBalanceConfirm confirmation = 
				new MUNSTransferBalanceConfirm(getCtx(), 0, get_TrxName());
		errorMsg = confirmation.createFrom(this);
		
		if(null != errorMsg)
			return errorMsg;
		
		return errorMsg;
	}
	
	/**
	 * 
	 * @param requery
	 * @return {@link MUNSTransferBalanceConfirm}[]
	 */
	public MUNSTransferBalanceConfirm[] getConfirmations(boolean requery)
	{
		if(null != m_confirmations
				&& !requery)
			return m_confirmations;
		
		String whereClause = MUNSTransferBalanceConfirm
						.COLUMNNAME_UNS_TransferBalance_Request_ID + " =?";
		
		List<MUNSTransferBalanceConfirm> list = Query.get(
				getCtx(), UNSFinanceModelFactory.EXTENSION_ID
				, MUNSTransferBalanceConfirm.Table_Name, whereClause, get_TrxName())
				.setParameters(getUNS_TransferBalance_Request_ID()).list();
		
		m_confirmations = new MUNSTransferBalanceConfirm[list.size()];

		m_confirmations = list.toArray(m_confirmations);

		return m_confirmations;
	}
	
	@Override
	public boolean beforeSave(boolean newRecord)
	{
		if(getAccountTo_ID() == 0)
		{
			throw new AdempiereException("Field mandatory Bank Account To!");
		}
		
		MBankAccount ba = new MBankAccount(
				getCtx(), getAccountTo_ID(), get_TrxName());
		setPrevBalanceAmt(ba.getCurrentBalance());
		
		if(REQUESTTYPE_Balance.equals(getRequestType()))
		{
			X_UNS_PettyCashRequest[] lines = getLines(true);

			for(X_UNS_PettyCashRequest line : lines)
			{
				if(!line.delete(true))
					return false;
			}
							
			MBankStatementLine[] sLines = MBankStatementLine.getOf(
					getUNS_TransferBalance_Request_ID(), 0, get_TrxName());
			for(MBankStatementLine sLine : sLines)
			{
				sLine.setUNS_TransferBalance_Request_ID(0);
				sLine.save();
			}
		}
		
		if(getChequeNo() != null && getAccountFrom_ID() > 0)
		{
			String msg = checkChequeNo();
			if(msg != null)
			{
				log.saveError("Error", msg);
				return false;
			}
		}
		
		return super.beforeSave(newRecord);
	}
	
	private String checkChequeNo()
	{
		String msg = null;
		String chequeNo = getChequeNo();
		
		MUNSChequebook cb = MUNSChequebook.isRegistered(getCtx(), get_TrxName(), chequeNo, getAccountFrom_ID());
		
		if (cb == null)
		{
			return Msg.getMsg(getCtx(), "Cheque-No of " + getChequeNo() +  " is not registered yet.");
		}
		
		String cekNum = MUNSChequebook.getProperChequeNumberFormat(chequeNo, cb);
		
		if (cb.isUsed(cekNum,-1, get_ID())) {
			msg = Msg.getMsg(getCtx(), "Cheque-No of " + getChequeNo() +  " has been used.");
		}
		
		return msg;
	}
	
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public X_UNS_PettyCashRequest[] getLines(boolean requery)
	{
		if(null != m_pettycashreq
				&& !requery)
			return m_pettycashreq;
		
		List<X_UNS_PettyCashRequest> list = Query.get(
				getCtx(), UNSFinanceModelFactory.EXTENSION_ID
				, X_UNS_PettyCashRequest.Table_Name
				, X_UNS_PettyCashRequest.COLUMNNAME_UNS_TransferBalance_Request_ID + "=?"
				, get_TrxName()).setParameters(getUNS_TransferBalance_Request_ID())
				.list();
		
		X_UNS_PettyCashRequest[] req = new X_UNS_PettyCashRequest[list.size()];
		m_pettycashreq = list.toArray(req);
		
		return m_pettycashreq;
	}
	
	/**
	 * 
	 * @return
	 */
	public X_UNS_PettyCashRequest[] getLines()
	{
		return getLines(false);
	}
	
	@Override
	public boolean beforeDelete()
	{
		if(!REQUESTTYPE_PettyCash.equals(getRequestType()))
			return super.beforeDelete();
		
		X_UNS_PettyCashRequest[] lines = getLines(true);
		for(X_UNS_PettyCashRequest line : lines)
		{
			if(!line.delete(true))
				return false;
		}
		
		MBankStatementLine[] sLines = MBankStatementLine.getOf(
				getUNS_TransferBalance_Request_ID(), 0, get_TrxName());
		for(MBankStatementLine sLine : sLines)
		{
			sLine.setUNS_TransferBalance_Request_ID(0);
			sLine.save();
		}
		
		return super.beforeDelete();
	}
	
	private boolean m_isAuto = false;
	
	private boolean isAuto()
	{
		return m_isAuto;
	}
	
	public void setIsAuto(boolean isAuto)
	{
		m_isAuto = isAuto;
	}
}
