/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_Cheque_List
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Cheque_List extends PO implements I_UNS_Cheque_List, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191129L;

    /** Standard Constructor */
    public X_UNS_Cheque_List (Properties ctx, int UNS_Cheque_List_ID, String trxName)
    {
      super (ctx, UNS_Cheque_List_ID, trxName);
      /** if (UNS_Cheque_List_ID == 0)
        {
			setName (null);
			setUNS_Chequebook_ID (0);
			setUNS_Cheque_List_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Cheque_List (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Cheque_List[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public com.uns.model.I_UNS_Chequebook getUNS_Chequebook() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Chequebook)MTable.get(getCtx(), com.uns.model.I_UNS_Chequebook.Table_Name)
			.getPO(getUNS_Chequebook_ID(), get_TrxName());	}

	/** Set Cheque Book Registered.
		@param UNS_Chequebook_ID Cheque Book Registered	  */
	public void setUNS_Chequebook_ID (int UNS_Chequebook_ID)
	{
		if (UNS_Chequebook_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Chequebook_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Chequebook_ID, Integer.valueOf(UNS_Chequebook_ID));
	}

	/** Get Cheque Book Registered.
		@return Cheque Book Registered	  */
	public int getUNS_Chequebook_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Chequebook_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cheque List.
		@param UNS_Cheque_List_ID Cheque List	  */
	public void setUNS_Cheque_List_ID (int UNS_Cheque_List_ID)
	{
		if (UNS_Cheque_List_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Cheque_List_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Cheque_List_ID, Integer.valueOf(UNS_Cheque_List_ID));
	}

	/** Get Cheque List.
		@return Cheque List	  */
	public int getUNS_Cheque_List_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Cheque_List_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cheque List UU.
		@param UNS_Cheque_List_UU Cheque List UU	  */
	public void setUNS_Cheque_List_UU (String UNS_Cheque_List_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Cheque_List_UU, UNS_Cheque_List_UU);
	}

	/** Get Cheque List UU.
		@return Cheque List UU	  */
	public String getUNS_Cheque_List_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Cheque_List_UU);
	}
}