/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

/**
 * @author ALBURHANY
 *
 */
public class ProcessPrintFaktur extends SvrProcess {

//	private String m_DocumentNo; TODO comment, unused @Menjangan
	private String m_message;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("DocumentNo"));
//				m_DocumentNo = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {

//		String sql = "SELECT C_Invoice_ID FROM M_InOut WHERE DocumentNo = '" + m_DocumentNo + "'";
//		int idInv = DB.getSQLValue(get_TrxName(), sql);
//		
//		if(idInv <= 0)
//		{
//			m_message = "Invoice not found";
//			throw new AdempiereException(m_message);
//		}
//		else
//		{	
//			m_message = "Invoice Printed";
//			MInvoice inv = new MInvoice(getCtx(), idInv, get_TrxName());
//			inv.setIsPrinted(true);
//			inv.saveEx();
//		}
		
		
		
		return m_message;
	}
}
