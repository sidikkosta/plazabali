/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.model.MUNSChequeList;
import com.uns.model.MUNSChequebook;
import com.uns.util.MessageBox;

/**
 * @author nurse
 *
 */
public class ChequeListGenerator extends SvrProcess {

	/**
	 * 
	 */
	public ChequeListGenerator() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		//Do nothing
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSChequebook book = new MUNSChequebook(
				getCtx(), getRecord_ID(), get_TrxName());
		String sql = "SELECT COUNT (UNS_Cheque_List_ID) FROM "
				+ "UNS_Cheque_List WHERE UNS_Chequebook_ID = ?";
		int counter = DB.getSQLValue(get_TrxName(), sql, book.get_ID());
		if (counter > 0)
		{
			int result = MessageBox.showMsg(
					getCtx(), getProcessInfo(), 
					"Cheque list already generated before. This action will delete generated cheque list",  
					"Delete before regenerate", MessageBox.YESNO, 
					MessageBox.ICONWARNING);
			if (result == MessageBox.RETURN_YES)
			{
				sql = "DELETE FROM UNS_Cheque_List WHERE UNS_Chequebook_ID = ?";
				result = DB.executeUpdate(sql, book.get_ID(), get_TrxName());
				if (result == -1)
					throw new AdempiereException(
							CLogger.retrieveErrorString("Failed when try to delete Cheque List"));
			}
			else
				return "Process aborted";
		}
		
		int numberLength = book.getNumberCodeLegth();
		String letter1 = book.getletter_code1();
		String letter2 = book.getletter_code2();
		String start1 = book.getstart_number1();
		String start2 = book.getstart_number2();
		String end1 = book.getend_number1();
		String end2 = book.getend_number2();
		
		if (!Util.isEmpty(start1, true) && !Util.isEmpty(end1, true))
		{
			String msg = generateCheque(book, letter1, start1, end1, numberLength);
			if (msg != null)
				throw new AdempiereException("Cheque not configured properly");
		}
		
		if (!Util.isEmpty(start2, true) && !Util.isEmpty(end2, true))
		{
			String msg = generateCheque(book, letter2, start2, end2, numberLength);
			if (msg != null)
				throw new AdempiereException("Cheque not configured properly");
		}
		
		return null;
	}
	
	private String generateCheque (
			MUNSChequebook book, String letterCode, String startStr, 
			String endStr, int length)
	{
		String format = "%" + length + "s";
		Integer start = Integer.parseInt(startStr);
		Integer end = Integer.parseInt(endStr);
		
		for (int i=start; i<=end; i++)
		{
			String name = Integer.toString(i);
			name = String.format(format, name);
			name = name.replace(" ", "0");
			name = letterCode + name;
			MUNSChequeList chequeList = new MUNSChequeList(book);
			chequeList.setName(name);
			trace(Level.INFO, "Generating cheque " + name);
			try
			{
				chequeList.saveEx();
			}
			catch (Exception ex)
			{
				return ex.getMessage();
			}
		}
		
		return null;
	}
	
	private void trace (Level level, String msg)
	{
		processUI.statusUpdate(msg);
		if (!log.isLoggable(level))
			return;
		log.log(level, msg);
	}
}