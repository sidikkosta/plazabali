package com.unicore.model.process;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBankAccount;
import org.compiere.model.MBankStatement;
import org.compiere.model.MCharge;
import org.compiere.model.MConversionRate;
import org.compiere.model.MConversionType;
import org.compiere.model.MDocType;
import org.compiere.model.MGLCategory;
import org.compiere.model.MJournal;
import org.compiere.model.MJournalLine;
import org.compiere.model.MPayment;
import org.compiere.model.MPeriod;
import org.compiere.model.MSysConfig;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.unicore.base.model.MBankStatementLine;
import com.unicore.model.MUNSPettyCashRequest;
import com.unicore.model.MUNSTransferBalanceConfirm;
import com.unicore.model.MUNSTransferBalanceRequest;
import com.uns.util.UNSApps;
/**
 *  Bank Transfer. Generate two Payments entry
 *  
 *  For Bank Transfer From Bank Account "A" 
 *                 
 *	@author victor.perez@e-evoltuion.com
 *	modify by nurseha :D for suport Transfer Balance Confirmation
 *	
 **/
public class UNSBankTransfer
{
	
	public UNSBankTransfer ()
	{
		super();
	}
	
	protected CLogger			log = CLogger.getCLogger (getClass());
	private Properties m_ctx;
	private String m_trxname;
	private String 		p_DocumentNo= "";				// Document No
	private String 		p_Description= "";				// Description
	private int 		p_C_BPartner_ID = 0;   			// Business Partner to be used as bridge
	private int			p_C_Currency_ID = 0;			// Payment Currency
	private int 		p_C_ConversionType_ID = 0;		// Payment Conversion Type
	private int			p_C_Charge_ID = 0;				// Charge to be used as bridge
	private String 		p_ChequeNo = null;
	private BigDecimal 	p_APAmount = Env.ZERO;  		// AP Amount if diff currency to be transfered between the accounts
	private BigDecimal 	p_ARAmount = Env.ZERO;  		// AR Amount if diff currency to be transfered between the accounts
	private int 		p_From_C_BankAccount_ID = 0;	// Bank Account From
	private int 		p_To_C_BankAccount_ID= 0;		// Bank Account To
	private Timestamp	p_StatementDate = null;  		// Date Statement
	private Timestamp	p_DateAcct = null; 
	private int			m_AD_OrgFrom_ID = 0;
	private int			m_AD_OrgTo_ID = 0;
	private int			m_paymentFrom_ID = 0;
	private int			m_paymentTo_ID = 0;
	private int			m_statementFrom_ID = 0;
	private int			m_statementTo_ID = 0;
	private boolean		m_isAutoCompletePayment = true;
	private boolean 	m_isIndependent = true;
	private int			m_APDocType_ID = 0;
	private int			m_ARDocType_ID = 0;
	private int			m_ConversionType_ID = 0;
	
	public UNSBankTransfer(MUNSTransferBalanceRequest request)
	{
		this(request.getCtx(), "", request.getDescription(), request.getC_BPartner_ID()
				, request.getC_Currency_ID(), 0, ((MUNSTransferBalanceConfirm)
				MUNSTransferBalanceConfirm.get(request.getCtx(), request.get_ID(), request.get_TrxName()))
				.getC_Charge_ID(), null, request.getAmountConfirmed(), Env.ZERO
				, request.getAccountFrom_ID(), request.getAccountTo_ID()
				, request.getDateRequired(), request.getDateRequired(), 0, 0
				, request.getAD_Org_ID(), request.get_TrxName());
		m_APDocType_ID = MDocType.getOfSubTypePay(MDocType.DOCSUBTYPEPAY_OtherPaymentOP);
		m_ARDocType_ID = MDocType.getOfSubTypePay(MDocType.DOCSUBTYPEPAY_OtherReceiptOR);
		m_isIndependent = false;
	}
	
	public UNSBankTransfer(Properties ctx, String documentNo, String description
			, int C_BPartner_ID, int C_Currency_ID, int conversionType_ID, int C_Charge_ID
			, String chequeNo, BigDecimal APamt, BigDecimal ARamt, int C_BankAcctFrom_ID, int C_BankAcctTo_ID
			, Timestamp statementDate, Timestamp dateAcct, int APDocType_ID, int ARDocType_ID
			, int OrgID, String trxName)
	{
		m_ctx =  ctx;
		m_trxname = trxName;
		p_DocumentNo= documentNo;				// Document No
		p_Description= description;				// Description
		p_C_BPartner_ID = C_BPartner_ID;   			// Business Partner to be used as bridge
		p_C_Currency_ID = C_Currency_ID <= 0 ? Env.getContextAsInt(m_ctx, "$C_Currency_ID") : C_Currency_ID;			// Payment Currency
		p_C_ConversionType_ID = conversionType_ID;		// Payment Conversion Type
		p_C_Charge_ID = C_Charge_ID;				// Charge to be used as bridge
		p_ChequeNo = chequeNo;
		p_APAmount = APamt;  			// Amount to be transfered between the accounts
		p_ARAmount = ARamt;  			// Amount to be transfered between the accounts
		p_From_C_BankAccount_ID = C_BankAcctFrom_ID;	// Bank Account From
		p_To_C_BankAccount_ID= C_BankAcctTo_ID;		// Bank Account To
		p_StatementDate = statementDate;  		// Date Statement
		p_DateAcct = dateAcct;
		initOrg();
		m_isIndependent = false;
		m_APDocType_ID = APDocType_ID;
		m_ARDocType_ID = ARDocType_ID;
		m_AD_OrgFrom_ID = OrgID; 
	}

	/**
	 *  Perform process.
	 *  @return Message (translated text)
	 *  @throws Exception if not successful
	 */
	public String doIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("From Bank="+p_From_C_BankAccount_ID+" - To Bank="+p_To_C_BankAccount_ID
				+ " - C_BPartner_ID="+p_C_BPartner_ID+"- C_Charge_ID= "+p_C_Charge_ID+" - Amount="+p_APAmount+" - DocumentNo="+p_DocumentNo
				+ " - Description="+p_Description+ " - Statement Date="+p_StatementDate+
				" - Date Account="+p_DateAcct);

		if (p_To_C_BankAccount_ID == 0 || p_From_C_BankAccount_ID == 0)
			return "Banks required";

		if (p_To_C_BankAccount_ID == p_From_C_BankAccount_ID)
			return "Banks From and To must be different";
		
		if (p_C_BPartner_ID == 0)
			return "Business Partner required";
		
//		if (p_C_Currency_ID == 0)
//			return "Currency required";
		
		if (p_APAmount.signum() == 0)
			return "Amount required";

		//	Login Date
		if (p_StatementDate == null)
			p_StatementDate = Env.getContextAsDate(Env.getCtx(), "#Date");
		
		if (p_StatementDate == null)
			p_StatementDate = new Timestamp(System.currentTimeMillis());			

		if (p_DateAcct == null)
			p_DateAcct = p_StatementDate;
		
		if(m_isIndependent)
			return createBankCashReq();
		else
			return generateBankTransfer();
	}
	

	/**
	 * Generate BankTransfer()
	 *
	 */
	private String generateBankTransfer()
	{
		boolean isCreatePayment = MSysConfig.getBooleanValue(MSysConfig.BANKCASH_TRANSFER_CREATE_PAYMENT, true);
		if(isCreatePayment)
			return createPayments();
		else
			return createStatement(false);
	}  //  createCashLines
	
	public String createStatement(boolean isVoid)
	{
		MBankStatement statementFrom = MBankStatement.getOpen(p_From_C_BankAccount_ID, m_trxname, true);
		MBankStatement statementTo = MBankStatement.getOpen(p_To_C_BankAccount_ID, m_trxname, true);
		if(statementFrom.getC_Currency_ID() == statementTo.getC_Currency_ID())
			p_ARAmount = p_APAmount;
		
		MBankStatementLine lineFrom = new MBankStatementLine(statementFrom);
		lineFrom.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_APTransaction);
		lineFrom.setDateAcct(p_DateAcct);
		lineFrom.setStatementLineDate(p_DateAcct);
		lineFrom.setStmtAmt(isVoid ? p_APAmount : p_APAmount.negate());
		lineFrom.setAmount(isVoid ? p_APAmount.negate() : p_APAmount);
		lineFrom.setDescription(p_Description);
		lineFrom.setC_Currency_ID(p_C_Currency_ID);
		if(isDiffOrg())
		{
			lineFrom.setAD_OrgTrx_ID(m_AD_OrgTo_ID);
			lineFrom.setC_Charge_ID(UNSApps.getRef(UNSApps.CHRG_PiutangIntercomp));
		}
		else
			lineFrom.setC_Charge_ID(UNSApps.getRef(UNSApps.CHRG_POSAyatSilang));
		lineFrom.saveEx();
		
		MBankStatementLine lineTo = new MBankStatementLine(statementTo);
		lineTo.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_ARTransaction);
		lineTo.setDateAcct(p_DateAcct);
		lineTo.setStatementLineDate(p_DateAcct);
		lineTo.setStmtAmt(isVoid ? p_ARAmount.negate() : p_ARAmount);
		lineTo.setAmount(isVoid ? p_ARAmount.negate() : p_ARAmount);
		lineTo.setDescription(p_Description);
		lineTo.setReference_ID(lineFrom.get_ID());
		lineTo.setC_Currency_ID(p_C_Currency_ID);
		if(isDiffOrg())
		{
			lineTo.setAD_OrgTrx_ID(m_AD_OrgFrom_ID);
			lineTo.setC_Charge_ID(UNSApps.getRef(UNSApps.CHRG_HutangIntercomp));
		}
		else
			lineTo.setC_Charge_ID(UNSApps.getRef(UNSApps.CHRG_POSAyatSilang));
		lineTo.saveEx();
		
		lineFrom.setReference_ID(lineTo.get_ID());
		lineFrom.saveEx();
		
		m_statementFrom_ID = lineFrom.get_ID();
		m_statementTo_ID = lineTo.get_ID();
		setSuccess(true);
		return "2 Statement Lines has been Created. " + statementFrom.getDocumentInfo()
				+ " and " + statementTo.getDocumentInfo();
	}
	
	private String createPayments()
	{
		MBankAccount mBankFrom = new MBankAccount(m_ctx,p_From_C_BankAccount_ID, m_trxname);
		MBankAccount mBankTo = new MBankAccount(m_ctx,p_To_C_BankAccount_ID, m_trxname);
		if(mBankFrom.getC_Currency_ID() == mBankTo.getC_Currency_ID())
			p_ARAmount = p_APAmount;
		
		if(mBankFrom.getC_Currency_ID() != mBankTo.getC_Currency_ID())
		{
			if(p_ARAmount.signum() == 0)
				return "Difference currency must define AR Amount.";
			if(p_C_ConversionType_ID <= 0)
				return "Difference currency must define Conversion Type.";
		}
		
		MPayment paymentBankFrom = new MPayment(m_ctx, 0 ,  m_trxname);
		paymentBankFrom.setAD_Org_ID(m_AD_OrgFrom_ID);
		paymentBankFrom.setC_BankAccount_ID(mBankFrom.getC_BankAccount_ID());
		if(p_DocumentNo != null && !p_DocumentNo.equals(""))
			paymentBankFrom.setDocumentNo(p_DocumentNo);
		paymentBankFrom.setDateAcct(p_DateAcct);
		paymentBankFrom.setDateTrx(p_StatementDate);
		if (!Util.isEmpty(p_ChequeNo, true))
		{
			paymentBankFrom.setTenderType(MPayment.TENDERTYPE_ChequeGiro);
			paymentBankFrom.set_ValueNoCheck("CheckNo",p_ChequeNo);
			paymentBankFrom.setDisbursementDate(p_StatementDate);
		}
		else 
			paymentBankFrom.setTenderType(MPayment.TENDERTYPE_Transfer);
		
		paymentBankFrom.setDescription(p_Description);
		paymentBankFrom.setC_BPartner_ID (p_C_BPartner_ID);
		paymentBankFrom.setC_Currency_ID(mBankFrom.getC_Currency_ID());
		if (p_C_ConversionType_ID > 0)
			paymentBankFrom.setC_ConversionType_ID(p_C_ConversionType_ID);	
		paymentBankFrom.setPayAmt(p_APAmount);
		paymentBankFrom.setOverUnderAmt(Env.ZERO);
		if(m_APDocType_ID > 0)
			paymentBankFrom.setC_DocType_ID(m_APDocType_ID);
		else
			paymentBankFrom.setC_DocType_ID(false);
		paymentBankFrom.setC_Charge_ID(p_C_Charge_ID);
		if(!paymentBankFrom.save())
			return CLogger.retrieveErrorString("Could not save Payment Bank From");
		
		m_paymentFrom_ID = paymentBankFrom.get_ID();
		
		if(m_isAutoCompletePayment)
		{
			if(!paymentBankFrom.processIt(MPayment.DOCACTION_Complete)) {
				log.warning("Payment Process Failed: " + paymentBankFrom + " - " + paymentBankFrom.getProcessMsg());
				return "Payment Process Failed: " + paymentBankFrom + " - " + paymentBankFrom.getProcessMsg();
			}
		}
		else
		{
			if(!paymentBankFrom.processIt(MPayment.DOCACTION_Prepare))
			{
				log.warning("Payment Process Failed: " + paymentBankFrom + " - " + paymentBankFrom.getProcessMsg());
				return "Payment Process Failed: " + paymentBankFrom + " - " + paymentBankFrom.getProcessMsg();
			}
		}
		
		if(!paymentBankFrom.save())
			return CLogger.retrieveErrorString("Could not save Payment Bank From");
	

		MPayment paymentBankTo = new MPayment(m_ctx, 0 ,  m_trxname);
		paymentBankTo.setAD_Org_ID(m_AD_OrgFrom_ID);
		paymentBankTo.setC_BankAccount_ID(mBankTo.getC_BankAccount_ID());
		if(p_DocumentNo != null && !p_DocumentNo.equals(""))
			paymentBankTo.setDocumentNo(p_DocumentNo);
		paymentBankTo.setDateAcct(p_DateAcct);
		paymentBankTo.setDateTrx(p_StatementDate);
		paymentBankTo.setTenderType(MPayment.TENDERTYPE_Transfer);
		paymentBankTo.setDescription(p_Description);
		paymentBankTo.setC_BPartner_ID (p_C_BPartner_ID);
		paymentBankTo.setC_Currency_ID(mBankTo.getC_Currency_ID());
		if (p_C_ConversionType_ID > 0)
			paymentBankFrom.setC_ConversionType_ID(p_C_ConversionType_ID);
		paymentBankTo.setPayAmt(p_ARAmount);
		paymentBankTo.setOverUnderAmt(Env.ZERO);
		if(m_ARDocType_ID > 0)
			paymentBankTo.setC_DocType_ID(m_ARDocType_ID);
		else
			paymentBankTo.setC_DocType_ID(true);
		paymentBankTo.setC_Charge_ID(p_C_Charge_ID);
		paymentBankTo.set_ValueOfColumn("Reference_ID", paymentBankFrom.get_ID());
		if (!paymentBankTo.save())
			return CLogger.retrieveErrorString("Could not save Payment Bank To");
		
		paymentBankFrom.set_ValueOfColumn("Reference_ID", paymentBankTo.get_ID());
		
		if (!paymentBankFrom.save())
			return CLogger.retrieveErrorString("Could not save Payment Bank From");
		m_paymentTo_ID = paymentBankTo.get_ID();
		
		if(mBankFrom.getC_Currency_ID() != mBankTo.getC_Currency_ID())
		{
			BigDecimal convert = MConversionRate.convert(m_ctx, p_APAmount, mBankFrom.getC_Currency_ID(),
					mBankTo.getC_Currency_ID(), p_DateAcct, p_C_ConversionType_ID,
					paymentBankFrom.getAD_Client_ID(), mBankTo.getAD_Org_ID());
			if(convert.compareTo(p_ARAmount) != 0)
			{
				String description = "Forex-" + paymentBankFrom.getDocumentNo()
						+ "-" + paymentBankTo.getDocumentNo();
				BigDecimal diff = convert.subtract(p_ARAmount);
				MAcctSchema[] as = MAcctSchema.getClientAcctSchema(m_ctx, paymentBankFrom.getAD_Client_ID());
				boolean matchCurrencyAp = false;
				boolean matchCurrencyAR = false;
				for(int i=0;i<as.length;i++)
				{
					if(mBankFrom.getC_Currency_ID() == as[i].getC_Currency_ID())
						matchCurrencyAp = true;
					else if(mBankTo.getC_Currency_ID() == as[i].getC_Currency_ID())
						matchCurrencyAR = true;
				}
				boolean needNewRate = matchCurrencyAp && matchCurrencyAR;
				
				for(int i=0;i<as.length;i++)
				{
					if(needNewRate && mBankTo.getC_Currency_ID() != as[i].getC_Currency_ID())
					{
						paymentBankFrom.load(m_trxname);
						paymentBankTo.load(m_trxname);
//						String new_trxName = Trx.createTrxName();
						MConversionType ct = new MConversionType(m_ctx, 0, null);
						ct.setAD_Org_ID(0);
						ct.setValue(paymentBankTo.get_ID() + "");
						ct.setName("Auto generated from difference rate on bank transfer."
								+ paymentBankFrom.getC_Currency().getISO_Code() + "-"
								+ paymentBankTo.getC_Currency().getISO_Code() + ". #"
								+ paymentBankFrom.getDocumentNo() + " #"
								+ paymentBankTo.getDocumentNo());
						ct.setDescription("Auto generated from difference rate on bank transfer."
								+ paymentBankFrom.getC_Currency().getISO_Code() + "-"
								+ paymentBankTo.getC_Currency().getISO_Code() + ". #"
								+ paymentBankFrom.getDocumentNo() + " #"
								+ paymentBankTo.getDocumentNo());
						ct.setIsManual(false);
						ct.saveEx();
						m_ConversionType_ID = ct.get_ID();
						boolean setDivider = p_APAmount.compareTo(p_ARAmount) == 1 ? true : false;
						MConversionRate rate = null;
						rate = new MConversionRate(paymentBankTo,
								m_ConversionType_ID, paymentBankFrom.getC_Currency_ID(),
								paymentBankTo.getC_Currency_ID(), Env.ZERO, TimeUtil.addDays(p_DateAcct, -1));
						rate.set_TrxName(null);
						if(setDivider)
						{
							BigDecimal divider = p_APAmount.divide(p_ARAmount, 5, RoundingMode.HALF_DOWN);
							rate.setDivideRate(divider);
						}
						else
						{
							BigDecimal multiple = p_ARAmount.divide(p_APAmount, 5, RoundingMode.HALF_DOWN);
							rate.setMultiplyRate(multiple);
						}
						rate.saveEx();
//						try {
//							DB.commit(true, new_trxName);
//						} catch (IllegalStateException | SQLException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
						continue;
					}
					
					if(mBankTo.getC_Currency_ID() != as[i].getC_Currency_ID())
					{
						convert = MConversionRate.convert(m_ctx, diff, mBankTo.getC_Currency_ID(),
								as[i].getC_Currency_ID(), p_DateAcct, p_C_ConversionType_ID,
								paymentBankFrom.getAD_Client_ID(), mBankTo.getAD_Org_ID());
					}
					else
						convert = diff;
					MJournal journal = new MJournal(m_ctx, 0, m_trxname);
					journal.setAD_Org_ID(mBankTo.getAD_Org_ID());
					journal.setC_AcctSchema_ID(as[i].get_ID());
					journal.setDescription(description);
					journal.setPostingType(MJournal.POSTINGTYPE_Actual);
					journal.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_GLJournal));
					journal.setGL_Category_ID(MGLCategory.getDefault(m_ctx, null).get_ID());
					journal.setDateDoc(p_DateAcct);
					journal.setDateAcct(p_DateAcct);
					journal.setC_Period_ID(MPeriod.getC_Period_ID(m_ctx, p_DateAcct, mBankTo.getAD_Org_ID()));
					journal.setC_Currency_ID(as[i].getC_Currency_ID());
					journal.setControlAmt(convert);
					journal.setC_ConversionType_ID(p_C_ConversionType_ID);
					journal.setIsPostSingleSchema(true);
					journal.saveEx();
					
					MJournalLine line1 = new MJournalLine(journal);
					line1.setAccount_ID(MCharge.getAccount(p_C_Charge_ID, as[i]).getAccount_ID());
					line1.setAmtSourceCr(convert);
					line1.setAmtAcctCr(convert);
					line1.saveEx();
					
					//ora jadi
					MJournalLine line2 = new MJournalLine(journal);
					String sql = "SELECT UNS_Forex_Acct FROM C_AcctSchema_Default WHERE C_AcctSchema_ID=?";
					int acct = DB.getSQLValue(m_trxname, sql, as[i].get_ID());
					if(acct <= 0)
						throw new AdempiereUserError("Forex Account not set on accounting schema.");
					line2.setAccount_ID(MAccount.get(m_ctx, acct).getAccount_ID());
					line2.setAmtSourceDr(convert);
					line2.setAmtAcctDr(convert);
					line2.saveEx();
					
					try {
						if(!journal.processIt("CO") || !journal.save())
							throw new AdempiereException(journal.getProcessMsg());
					} catch (Exception e)
					{
						throw new AdempiereException(e.getMessage());
					}
				}
			}
		}
		
		if(m_ConversionType_ID > 0)
		{
			paymentBankTo.setC_ConversionType_ID(m_ConversionType_ID);
			paymentBankTo.saveEx();
		}
		if(m_isAutoCompletePayment)
		{
			if (!paymentBankTo.processIt(MPayment.DOCACTION_Complete)) {
				log.warning("Payment Process Failed: " + paymentBankTo + " - " + paymentBankTo.getProcessMsg());
				return "Payment Process Failed: " + paymentBankTo + " - " + paymentBankTo.getProcessMsg();
			}
//			MAcctSchema[] as = MAcctSchema.getClientAcctSchema(m_ctx, paymentBankFrom.getAD_Client_ID());
//			for(int i=0;i<as.length;i++)
//			{
//				DocManager.postDocument(as, MPayment.Table_ID, paymentBankTo.get_ID(),
//						true, true, m_trxname);
//			}
		}
		else
		{
			if(!paymentBankTo.processIt(MPayment.DOCACTION_Prepare))
			{
				log.warning("Payment Process Failed: " + paymentBankTo + " - " + paymentBankTo.getProcessMsg());
				return "Payment Process Failed: " + paymentBankTo + " - " + paymentBankTo.getProcessMsg();
			}
		}
		if (!paymentBankTo.save())
			return CLogger.retrieveErrorString("Could not save Payment Bank To");
		
		setSuccess(true);
		return "2 Payment has been Created. " + paymentBankFrom.getDocumentInfo()
				+ " and " + paymentBankTo.getDocumentInfo();
	}
	
	private boolean m_success = false;
	public boolean IsSuccess()
	{
		return m_success;
	}
	
	private void setSuccess(boolean success)
	{
		m_success = success;
	}
	
	public int getPaymentBankFrom_ID ()
	{
		return m_paymentFrom_ID;
	}
	
	public int getPaymentBankTo_ID ()
	{
		return m_paymentTo_ID;
	}
	
	public void setCheckNo (String checkNo)
	{
		p_ChequeNo = checkNo;
	}
	
	public int getStatementFrom_ID()
	{
		return m_statementFrom_ID;
	}
	
	public int getStatementTo_ID()
	{
		return m_statementTo_ID;
	}
	
	private boolean isDiffOrg()
	{
		initOrg();
		
		if(m_AD_OrgFrom_ID != m_AD_OrgTo_ID)
			return true;
		
		return false;
	}
	
	private void initOrg()
	{
		String sql = "SELECT AD_Org_ID FROM C_BankAccount WHERE C_BankAccount_ID = ?";
		if(m_AD_OrgFrom_ID <= 0)
			m_AD_OrgFrom_ID = DB.getSQLValue(m_trxname, sql, p_From_C_BankAccount_ID);
		
		if(m_AD_OrgTo_ID <=0 )
			m_AD_OrgTo_ID = DB.getSQLValue(m_trxname, sql, p_To_C_BankAccount_ID);
	}
	
	private String createBankCashReq()
	{
		MUNSTransferBalanceRequest req = new MUNSTransferBalanceRequest(m_ctx, 0, m_trxname);
		initOrg();
		req.setAD_Org_ID(m_AD_OrgFrom_ID);
		req.setAD_Org_ID(m_AD_OrgFrom_ID);
		req.setIsManual(false);
		req.setAccountFrom_ID(p_From_C_BankAccount_ID);
		req.setAccountTo_ID(p_To_C_BankAccount_ID);
		req.setDateRequired(p_DateAcct);
		if(p_ChequeNo != null)
			req.setChequeNo(p_ChequeNo);
		if(!req.save())
			return CLogger.retrieveErrorString("Error create Bank/Cash Request document.");
		
		MUNSPettyCashRequest line = new MUNSPettyCashRequest(m_ctx, 0, m_trxname);
		line.setUNS_TransferBalance_Request_ID(req.get_ID());
		line.setC_Charge_ID(p_C_Charge_ID);
		line.setAmountRequested(p_APAmount);
		if(!line.save())
			return CLogger.retrieveErrorString("Error create Bank/Cash Request document.");
		
		try {
			req.processIt("CO");
			req.saveEx();
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
		}
		
		MUNSTransferBalanceConfirm[] confirms = req.getConfirmations(true);
		for(MUNSTransferBalanceConfirm confirm : confirms)
		{
			if(!confirm.isComplete())
			{
				try {
					confirm.processIt("CO");
					confirm.saveEx();
				} catch (Exception e) {
					throw new AdempiereException(e.getMessage());
				}
			}
		}
		
		return null;
	}
}	//	ImmediateBankTransfer