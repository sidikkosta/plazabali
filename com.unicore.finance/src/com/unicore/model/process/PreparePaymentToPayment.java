/**
 * 
 */
package com.unicore.model.process;

import org.compiere.model.MPaySelection;
import org.compiere.model.MPaySelectionCheck;
import org.compiere.model.MPaySelectionLine;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.wf.MWorkflow;

import com.unicore.base.model.MPayment;
import com.unicore.base.model.MPaymentAllocate;
import com.uns.util.MessageBox;

/**
 * @author Burhani Adam
 *
 */
public class PreparePaymentToPayment extends SvrProcess {

	private String m_processMsg = null;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{ 
		if(getTable_ID() == MPaySelection.Table_ID)
		{
			MPaySelection ps = new MPaySelection(getCtx(), getRecord_ID(), get_TrxName());
			
			for(MPaySelectionCheck check : ps.getChecks())
			{
				if(null != createPayment(check, ps))
					throw new AdempiereUserError(m_processMsg);
			}
			ps.setCreatePayment("Y");
			ps.saveEx();
		}
		else
		{
			MPaySelectionCheck check = new MPaySelectionCheck(getCtx(), getRecord_ID(), get_TrxName());
			MPaySelection ps = (MPaySelection) check.getC_PaySelection();
			if(null != createPayment(check, ps))
				throw new AdempiereUserError(m_processMsg);
		}
		
		return "Payment has Created";
	}
	
	public String createPayment(MPaySelectionCheck check, MPaySelection ps)
	{
		m_processMsg = null;
		if(check.getC_Payment_ID() > 0)
			return m_processMsg;
		
		MPayment pay = new MPayment(getCtx(), 0, get_TrxName());
		pay.setAD_Org_ID(check.getAD_Org_ID());
		pay.setC_BankAccount_ID(ps.getC_BankAccount_ID());
		pay.setC_DocType_ID(check.isReceipt());
		pay.setDateAcct(ps.getPayDate());
		pay.setDateTrx(ps.getPayDate());
		pay.setC_BPartner_ID(check.getC_BPartner_ID());
		pay.setC_Currency_ID(ps.getC_Currency_ID());
		pay.setTenderType(check.getPaymentRule());
		pay.setCheckNo(check.getCheckNo());
		pay.setDisbursementDate(check.getDisbursementDate());
		try
		{	
			boolean ok = pay.save();
			if (!ok)
			{
				m_processMsg = pay.getProcessMsg();
				return m_processMsg;
			}
			else
			{
				for(MPaySelectionLine line : check.getLines())
				{
					MPaymentAllocate allocate = new MPaymentAllocate(getCtx(), 0, get_TrxName());
					allocate.setAD_Org_ID(line.getAD_Org_ID());
					allocate.setC_Payment_ID(pay.get_ID());
					allocate.setC_Invoice_ID(line.getC_Invoice_ID());
					allocate.setInvoiceAmt(line.getOpenAmt());
					allocate.setAmount(line.getPayAmt());
					allocate.setPayToOverUnderAmount(line.getPayAmt());
					allocate.setOverUnderAmt(line.getDifferenceAmt());
					allocate.saveEx(get_TrxName());
					line.setProcessed(true);
					line.saveEx();
				}
			}
			
			check.setCreatePayment("Y");
			check.setProcessed(true);
			check.setC_Payment_ID(pay.get_ID());
			check.saveEx();
			
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo()
					, "Payment has been created. \n" +
					  "Do you want to complete that document ?"
					, "Payment auto completion confirmation"
					, MessageBox.YESNO
					, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_YES)
			{
				try
				{
					MPayment payment = new MPayment(getCtx(), pay.get_ID(), get_TrxName());
					if (!payment.isComplete())
				    {
						ProcessInfo paymentInfo = MWorkflow.runDocumentActionWorkflow(payment, DocAction.ACTION_Complete);
						if(paymentInfo.isError())
						{
							throw new AdempiereUserError(paymentInfo.getSummary());
						}
				    }
				}
				catch (Exception e) {
					throw new AdempiereUserError(e.getMessage());
				}
			}
			else 
				return null;
		}
		catch (Exception ex)
		{
			m_processMsg = ex.getMessage();
			return m_processMsg;
		}
		
		return m_processMsg;
	}

}
