/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;

import org.compiere.model.X_C_BankStatementLine;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.unicore.base.model.MBankStatementLine;
import com.unicore.model.MUNSPettyCashRequest;
import com.unicore.model.MUNSTransferBalanceRequest;

/**
 * @author menjangan
 *
 */
public class TBLoadStatement extends SvrProcess {

	/**
	 * 
	 */
	public TBLoadStatement() 
	{
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		//Do nothing
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSTransferBalanceRequest req = new MUNSTransferBalanceRequest(
				getCtx(), getRecord_ID(), get_TrxName());
		
		if(req.isProcessed())
			return "";
		
		MBankStatementLine[] sLines = MBankStatementLine.getLastOf(
				req.getAccountTo_ID(), req.getDateRequired(), get_TrxName());
		BigDecimal requestAmt = Env.ZERO;
		for(MBankStatementLine sLine : sLines)
		{
//			if(!sLine.isProcessed())
//				continue;
			if(sLine.getTransactionType().equals(X_C_BankStatementLine.TRANSACTIONTYPE_ARTransaction))
				continue;
			sLine.setUNS_TransferBalance_Request_ID(req.get_ID());
			sLine.saveEx();
			requestAmt = requestAmt.add(sLine.getStmtAmt().negate());
		}
		
		MUNSPettyCashRequest cashPlan = new MUNSPettyCashRequest(getCtx(), 0, get_TrxName());
		cashPlan.setAD_Org_ID(req.getAD_Org_ID());
		cashPlan.setUNS_TransferBalance_Request_ID(req.get_ID());
		cashPlan.setAmountRequested(requestAmt);
		cashPlan.saveEx();
		
		return null;
	}
}
