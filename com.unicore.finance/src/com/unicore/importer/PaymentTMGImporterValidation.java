/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.util.IProcessUI;
import org.compiere.model.MBankAccount;
import org.compiere.model.MPayment;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.importer.ImporterValidation;

/**
 * @author ALBURHANY
 *
 */
public class PaymentTMGImporterValidation implements ImporterValidation {

	private Properties m_ctx = null;
	private String m_trxName = null;
//	private boolean m_IsCurrTrxImport = false; //TODO comment, not used @Menjangan
	Hashtable<String, PO> m_poRefMap = null;
	Hashtable<String, Object> m_freeColVals;
	
	static final String COL_Tanggal			= "Tanggal";
	static final String COL_KdPelanggan 	= "KdPelanggan";
	static final String cOL_NmPelanggan 	= "NmPelanggan";
	static final String COL_NoBon			= "NoBon";
	static final String COL_Giro			= "Giro";
	static final String COL_Keterangan		= "Keterangan";
	static final String COL_SubTotal		= "SubTotal";
	static final String COL_Discount		= "Discount";
	static final String COL_Discount2		= "Discount2";
	static final String COL_Total			= "Total";
	static final String COL_Ongkir			= "Ongkir";
	static final String COL_Nomor			= "Nomor";
	static final String COL_NoFaktur		= "NoFaktur";
	static final String COL_Jumlah			= "Jumlah";
	static final String COL_Tabel			= "Tabel";
	static final String COL_Cek				= "Cek";
	static final String COL_Tunai			= "Tunai";
	static final String COL_KU				= "KU";
	static final String COL_Returdll		= "Returdll";
	static final String COL_Org				= "Org";

	
	static final int CURRENCY_IDR			= 303;
	static final int DEFAULT_SALESREPs	= 1001027;
	static final int ORGANIZATION		= 1000039;
	
	static final int PRICELIST_TOKO		= 1000099;
	static final int PRICELIST_KONSUMEN	= 1000100;
	static final int AP_INVOICE 			= 1000358;
	static final int AR_INVOICE 			= 1000355;
	static final int AP_CREDITMEMO 	= 1000359;
	static final int AR_CREDITMEMO 	= 1000357;
	
	public static final int MM_RECEIPT = 1000367;
	public static final int MM_SHIPMENT = 1000364;
	public static final int MM_VENDOR_RETURN = 1000366;
	public static final int MM_CUSTOMER_RETURN = 1000368;
	
	static final int LOC_HO 	= 1000183;
	static final int LOC_AB 	= 1000184;
	static final int LOC_PVC 	= 1000185;
	
	static final String WH_AB 	= "W.AB";
	static final String WH_PVC 	= "W.PP";
	
	static final int CHARGE_ONGKIR 		= 1000043;
	static final int CHARGE_SALES_REVENUE = 1000063;
	
	private int idPayment = 0;
	
	IProcessUI m_processMonitor = null;
	
	public PaymentTMGImporterValidation(Properties ctx, Sheet sheet, String trxName) {
		this.m_ctx = ctx;
		//this.m_sheet = sheet;
		this.m_trxName = trxName;
		m_processMonitor = Env.getProcessUI(ctx);
	}
	
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		org.compiere.model.MPaymentAllocate pa = (org.compiere.model.MPaymentAllocate) po;
		
		if(pa.getC_Invoice_ID() == 0)
			return "Cannot find invoice of " + freeColVals.get(COL_NoFaktur) + " at row " + currentRow;

//		int a = (int) freeColVals.get(COL_KdPelanggan);
		
//		String sql = "SELECT C_BPartner_ID FROM C_BPartner WHERE"
//				+ " Name = '" + freeColVals.get(COL_KdPelanggan) + "' OR Name2 = '" + freeColVals.get(COL_KdPelanggan) + "'";
//		int BP_ID = DB.getSQLValue(m_trxName, sql);
		
		String sql = "SELECT C_BankAccount_ID FROM C_BankAccount WHERE BankAccountType=?";
		int bankAcc_ID = DB.getSQLValue(m_trxName, sql, MBankAccount.BANKACCOUNTTYPE_OffsetInitialBalance);
		
		pa.setInvoiceAmt(pa.getC_Invoice().getGrandTotal());
		pa.setOverUnderAmt(pa.getC_Invoice().getGrandTotal().subtract(((BigDecimal) freeColVals.get(COL_Tunai)))
				.subtract((BigDecimal) freeColVals.get(COL_Discount2)));
		
		String NoBon = (String) freeColVals.get(COL_NoBon);
		String idPay = "SELECT C_Payment_ID FROM C_Payment WHERE DocumentNo = '" + NoBon + "'";
		idPayment = DB.getSQLValue(m_trxName, idPay);
		
		MPayment pay = new MPayment(m_ctx, idPayment == -1 ? 0 : idPayment, m_trxName);
		if(idPayment <= -1)
		{
			pay.setAD_Org_ID(ORGANIZATION);
			pay.setC_DocType_ID(true);
			pay.setDocumentNo(NoBon);
			pay.setC_BPartner_ID((int)freeColVals.get(COL_KdPelanggan));
			pay.setTenderType("X");
			pay.setC_BankAccount_ID(bankAcc_ID);
			pay.setC_Currency_ID(CURRENCY_IDR);
			pay.saveEx();
		}
		
		idPayment = pay.get_ID();
		
		pa.setC_Payment_ID(idPayment);
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		MPayment pay = new MPayment(m_ctx, idPayment, m_trxName);
		pay.processIt(DocAction.ACTION_Complete);
		pay.setProcessed(true);
		pay.saveEx();
		
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
	}

}
