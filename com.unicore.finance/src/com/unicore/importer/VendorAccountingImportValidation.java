package com.unicore.importer;

import java.util.Hashtable;
import java.util.Properties;
import jxl.Sheet;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.PO;
import org.compiere.model.X_C_BP_Vendor_Acct;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import com.uns.importer.ImporterValidation;

public class VendorAccountingImportValidation implements ImporterValidation {

	private Properties m_ctx;
	private Sheet m_sheet;
	private String m_trxName;
	private final String COLUMN_LIABILITY = "Liability";
	private final String COLUMN_LIABILITY_SERVICE = "Liability Service";
	private final String COLUMN_PREPAYMENT = "Prepayment";
	private final String COLUMN_SUBSEQUENT_DISCOUNT = "Subsequent Discount";
	private final String COLUMN_UNCLAIMED_DISCOUNT = "Unclaimed Discount";
	
	public VendorAccountingImportValidation (Properties ctx, Sheet sheet, String trxName)
	{
		this ();
		m_ctx = ctx;
		m_sheet = sheet;
		m_trxName = trxName;
	}
	
	public Properties getCtx ()
	{
		return m_ctx;
	}
	
	public Sheet getSheet ()
	{
		return m_sheet;
	}
	
	public String get_TrxName ()
	{
		return m_trxName;
	}
	
	public VendorAccountingImportValidation() 
	{
		super ();
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		X_C_BP_Vendor_Acct acct = (X_C_BP_Vendor_Acct) po;
		for (String key : freeColVals.keySet())
		{
			String columnName = null;
			Object obj = freeColVals.get(key);
			if (obj == null || obj instanceof String == false)
				continue;
			String acctVal = (String) obj;
			
			if (COLUMN_LIABILITY.equals(key))
				columnName = X_C_BP_Vendor_Acct.COLUMNNAME_V_Liability_Acct;
			else if (COLUMN_LIABILITY_SERVICE.equals(key))
				columnName = X_C_BP_Vendor_Acct.COLUMNNAME_V_Liability_Services_Acct;
			else if (COLUMN_PREPAYMENT.equals(key))
				columnName = X_C_BP_Vendor_Acct.COLUMNNAME_V_Prepayment_Acct;
			else if (COLUMN_SUBSEQUENT_DISCOUNT.equals(key))
				columnName = X_C_BP_Vendor_Acct.COLUMNNAME_SubsequentDiscount_Acct;
			else if (COLUMN_UNCLAIMED_DISCOUNT.equals(key))
				columnName = X_C_BP_Vendor_Acct.COLUMNNAME_UnclaimedDiscount_Acct;
			
			if (columnName == null)
				continue;
			
			int accountID = getAccountID(acctVal);
			if (accountID <= 0)
				return buildNotFoundMsg("account", acctVal);
			int validCombiID = getValidCombinationID(
					accountID, acct.getC_AcctSchema_ID(), acct.getC_BPartner_ID(), 
					acct.getAD_Org_ID());
			if (validCombiID <= 0)
			{
				MAccount newAccount = new MAccount(MAcctSchema.get(m_ctx, acct.getC_AcctSchema_ID()));
				newAccount.setAccount_ID(accountID);
				if (!newAccount.save())
					return CLogger.retrieveErrorString("Failed when try to save new Account Combination");
				validCombiID = newAccount.getC_ValidCombination_ID();
			}
			acct.set_ValueOfColumn(columnName, validCombiID);
		}
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub

	}

	private int getAccountID (String value)
	{
		String sql = "SELECT C_ElementValue_ID FROM C_ElementValue WHERE Value = ? AND IsActive = ?";
		int accountID = DB.getSQLValue(get_TrxName(), sql, value, "Y");
		return accountID;
	}
	
	private int getValidCombinationID (int accountID, int acctSchemaID, int C_BPartner_ID, int AD_Org_ID)
	{
		String sql = "SELECT C_ValidCombination_ID FROM C_ValidCombination WHERE Account_ID = ? AND C_AcctSchema_ID = ? "
				+ " AND (C_BPartner_ID = ? OR C_BPartner_ID IS NULL) AND AD_Org_ID IN (?,?) ORDER BY AD_Org_ID, C_BPartner_ID DESC";
		int combinationID = DB.getSQLValue(get_TrxName(), sql, accountID, acctSchemaID, C_BPartner_ID, AD_Org_ID, 0);
		return combinationID;
	}
	
	private String buildNotFoundMsg (String find, String value)
	{
		StringBuilder msgBuild = new StringBuilder("Could not find ");
		msgBuild.append(find).append(" ");
		msgBuild.append(value);
		String msg = msgBuild.toString();
		return msg;
	}
}
