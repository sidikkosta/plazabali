/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.util.IProcessUI;
import org.compiere.model.MBankAccount;
import org.compiere.model.MInvoice;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.wf.MWorkflow;

import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author AzHaidar
 *
 */
public class UpdatePembayaranARInvoice implements ImporterValidation 
{

	private static CLogger log = CLogger.getCLogger(UpdatePembayaranARInvoice.class);
	
	private Properties m_ctx = null;
	private String m_trxName = null;
	Hashtable<String, PO> m_poRefMap = null;
	Hashtable<String, Object> m_freeColVals;
	Hashtable<String, MPayment> m_paymentMap = new Hashtable<String, MPayment>();
	private SimpleDateFormat m_dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	static final String COL_KdFaktur 	= "KdFaktur";
	static final String COL_DateLastTrx = "DateLastTrx";
	static final String COL_Sisa		= "Sisa";
	
	static final String PREFIX_DESC = "**Auto Sync (Open Amount)--";
	static final String SUFFIX_DESC = "--**";
	

	IProcessUI m_processMonitor = null;
	
	/**
	 * 
	 */
	public UpdatePembayaranARInvoice(Properties ctx, Sheet sheet, String trxName)
	{
		this.m_ctx = ctx;
		this.m_trxName = trxName;
		m_processMonitor = Env.getProcessUI(ctx);
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MInvoice theInvoice = (MInvoice) po;
		
		if (theInvoice.get_ID() == 0)
			return "The invoice #" + theInvoice.getDocumentNo() + " at row " + currentRow + " not found in system.";
		
		if (theInvoice.isComplete() && !theInvoice.getDocStatus().equals(MInvoice.DOCSTATUS_Reversed)
				&& !theInvoice.getDocStatus().equals(MInvoice.DOCSTATUS_Voided)) 
		{
			String sql = "UPDATE C_InvoiceLine SET Processed='N' WHERE C_Invoice_ID=" + theInvoice.get_ID();
			DB.executeUpdate(sql, m_trxName);
			
			sql = "UPDATE C_InvoiceTax SET Processed='N' WHERE C_Invoice_ID=" + theInvoice.get_ID();
			DB.executeUpdate(sql, m_trxName);
			
			theInvoice.processIt(MInvoice.DOCACTION_Complete);
			theInvoice.saveEx();
		}
		
		Timestamp dateLastTrx = (Timestamp) freeColVals.get(COL_DateLastTrx);
		BigDecimal sisa = freeColVals.get(COL_Sisa) == null? Env.ZERO : (BigDecimal) freeColVals.get(COL_Sisa);
		
		BigDecimal openAmt 		= theInvoice.getOpenAmt();
		
		if (openAmt.signum() == 0 || openAmt.compareTo(openAmt) == 0)
			return SimpleImportXLS.DONT_SAVE;
		
		BigDecimal syncPayAmt 	= openAmt.subtract(sisa);
		
		if (sisa.compareTo(openAmt) > 0) {
			syncPayAmt = openAmt;
		}
		
		String paymentMapKey = theInvoice.getC_BPartner_ID() + "-" + m_dateFormat.format(dateLastTrx);
		
		MPayment payment = m_paymentMap.get(paymentMapKey);
		
		if (payment == null) 
		{
			String generatedDesc = PREFIX_DESC + paymentMapKey + SUFFIX_DESC;
			
			String whereClause = "C_BPartner_ID=? AND Description LIKE '%" + generatedDesc + "%'";
			
			payment = new Query(m_ctx, MPayment.Table_Name, whereClause, m_trxName)
					.setParameters(theInvoice.getC_BPartner_ID())
					.first();
			if (payment == null) {
				payment = createNewPayment(theInvoice, syncPayAmt, dateLastTrx, generatedDesc);
			}
			else if (payment.getDocStatus().equals(MPayment.DOCSTATUS_Completed) 
					|| payment.getDocStatus().equals(MPayment.DOCSTATUS_Closed)) {
				payment.setDocStatus(MPayment.DOCSTATUS_Drafted);
				payment.setDocAction(MPayment.DOCACTION_Complete);
				payment.setProcessed(false);
				payment.saveEx();
			}
			
			m_paymentMap.put(paymentMapKey, payment);
		}
		
		MPaymentAllocate payAllocate = 
				new Query(m_ctx, MPaymentAllocate.Table_Name, "C_Payment_ID=? AND C_Invoice_ID=?", m_trxName)
				.setParameters(payment.get_ID(), theInvoice.get_ID()).first();
		
		if (payAllocate == null)
		{
			payAllocate = new MPaymentAllocate(m_ctx, 0, m_trxName);
			payAllocate.setAD_Org_ID(theInvoice.getAD_Org_ID());
			payAllocate.setC_Payment_ID(payment.get_ID());
			payAllocate.setC_Invoice_ID(theInvoice.get_ID());
		}
		
		payAllocate.setInvoiceAmt(openAmt);
		payAllocate.setAmount(syncPayAmt);
		payAllocate.saveEx();
		
		return SimpleImportXLS.DONT_SAVE;
	}

	/**
	 * 
	 * @param invoice
	 * @param paidAmt
	 * @return
	 */
	MPayment createNewPayment(MInvoice invoice, BigDecimal paidAmt, Timestamp dateTrx, String description)
	{
		MPayment payment = new MPayment(m_ctx, 0, m_trxName);
		
		payment.setC_DocType_ID(invoice.isSOTrx());
		payment.setAD_Org_ID(invoice.getAD_Org_ID());
		payment.setC_BPartner_ID(invoice.getC_BPartner_ID());
		payment.setPayAmt(invoice.getGrandTotal());
		payment.setC_Currency_ID(303);
		payment.setDescription(description);
		//payment.setDocumentNo(invoice.getDocumentNo());

		payment.setDateTrx(dateTrx);
		payment.setDateAcct(dateTrx);
		payment.setTenderType(MPayment.TENDERTYPE_DirectDeposit);
		
		String sql = "SELECT C_BankAccount_ID FROM C_BankAccount WHERE BankAccountType=?";
		int bankAcc_ID = DB.getSQLValue(m_trxName, sql, MBankAccount.BANKACCOUNTTYPE_OffsetInitialBalance);
		
		payment.setC_BankAccount_ID(bankAcc_ID);
		
		payment.saveEx();
		
		return payment;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) 
	{
		for (MPayment payment : m_paymentMap.values())
		{
			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(payment, DocAction.ACTION_Complete);

			log.info("(Re)Completing payment #" + payment.getDocumentNo() + " Date@" + payment.getDateTrx());
			m_processMonitor.statusUpdate(
					"(Re)Completing payment #" + payment.getDocumentNo() + " Date@" + payment.getDateTrx());
			
			if(pi.isError()) {
				return "Error. Caused by: " + pi.getSummary();
			}
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
	}

}
