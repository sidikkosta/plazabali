/**
 * 
 */
package com.unicore.importer;

import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.PO;
import org.compiere.model.X_M_Product_Acct;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import com.uns.importer.ImporterValidation;

/**
 * @author nurse
 *
 */
public class ProductAccountingImportValidation implements ImporterValidation 
{
	private Properties m_ctx;
	private Sheet m_sheet;
	private String m_trxName;
	private final String COLUMN_PRODUCT_ASSET = "Product Asset";
	private final String COLUMN_PRODUCT_EXPENSE = "Product Expense";
	private final String COLUMN_COST_ADJUSTMENT = "Cost Adjustment";
	private final String COLUMN_INVENTORY_CLEARING = "Inventory Clearing";
	private final String COLUMN_PRODUCT_COGS = "Product COGS";
	private final String COLUMN_PRODUCT_REVENUE = "Product Revenue";
	private final String COLUMN_PURCHASE_PRICE_VARIANCE = "Purchase Price Variance";
	private final String COLUMN_INVOICE_PRICE_VARIANCE = "Invoice Price Variance";
	private final String COLUMN_TRADE_DISCOUNT_RECEIVED = "Trade Discount Received";
	private final String COLUMN_TRADE_DISCOUNT_GRANTED = "Trade Discount Granted";
	private final String COLUMN_RATE_VARIANCE = "Rate Variance";
	private final String COLUMN_LANDED_COST_CLEARING = "Landed Cost Clearing";
	private final String COLUMN_REJECT_PRODUCT = "Reject Product";
	private final String COLUMN_PRODUCT_ASSET_INTRANSIT = "Product Asset Intransit";
	private final String COLUMN_PRODUCT_BONUS = "Product Bonus";
	private final String COLUMN_NOT_RECEIPT_PRODUCT_BONUS = "Not Receipt Product Bonus";
	private final String COLUMN_SALES_PRODUCT_BONUS = "Sales Product Bonus";
	private final String COLUMN_SALES_PRODUCT_BONUS_NOT_SHIP = "Sales Product Bonus Not Ship";
	
	public Sheet getCell ()
	{
		return m_sheet;
	}
	
	public Properties getCtx()
	{
		return m_ctx;
	}
	
	public ProductAccountingImportValidation (
			Properties ctx, Sheet sheet, String trxName)
	{
		this ();
		m_ctx = ctx;
		m_sheet = sheet;
		m_trxName = trxName;
	}
	
	public ProductAccountingImportValidation() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		X_M_Product_Acct acct = (X_M_Product_Acct) po;
		
		for (String key : freeColVals.keySet())
		{
			String columnName = null;
			Object obj = freeColVals.get(key);
			if (obj == null || obj instanceof String == false)
				continue;
			String acctVal = (String) obj;
			
			if (COLUMN_PRODUCT_ASSET.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_Asset_Acct;
			else if (COLUMN_PRODUCT_EXPENSE.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_Expense_Acct;
			else if (COLUMN_COST_ADJUSTMENT.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_CostAdjustment_Acct;
			else if (COLUMN_INVENTORY_CLEARING.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_InventoryClearing_Acct;
			else if (COLUMN_PRODUCT_COGS.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_COGS_Acct;
			else if (COLUMN_PRODUCT_REVENUE.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_Revenue_Acct;
			else if (COLUMN_PURCHASE_PRICE_VARIANCE.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_PurchasePriceVariance_Acct;
			else if (COLUMN_INVOICE_PRICE_VARIANCE.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_InvoicePriceVariance_Acct;
			else if (COLUMN_TRADE_DISCOUNT_RECEIVED.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_TradeDiscountRec_Acct;
			else if (COLUMN_TRADE_DISCOUNT_GRANTED.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_TradeDiscountGrant_Acct;
			else if (COLUMN_RATE_VARIANCE.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_RateVariance_Acct;
			else if (COLUMN_LANDED_COST_CLEARING.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_LandedCostClearing_Acct;
			else if (COLUMN_REJECT_PRODUCT.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_RejectProduct_Acct;
			else if (COLUMN_PRODUCT_ASSET_INTRANSIT.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_AssetIntransit_Acct;
			else if (COLUMN_PRODUCT_BONUS.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_Bonus_Acct;
			else if (COLUMN_NOT_RECEIPT_PRODUCT_BONUS.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_BonusNotReceipt_Acct;
			else if (COLUMN_SALES_PRODUCT_BONUS.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_BonusSales_Acct;
			else if (COLUMN_SALES_PRODUCT_BONUS_NOT_SHIP.equals(key))
				columnName = X_M_Product_Acct.COLUMNNAME_P_BonusSalesNotShip_Acct;
			
			if (columnName == null)
				continue;
			
			int accountID = getAccountID(acctVal);
			if (accountID <= 0)
				return buildNotFoundMsg("account", acctVal);
			int validCombiID = getValidCombinationID(
					accountID, acct.getC_AcctSchema_ID(), acct.getM_Product_ID(), 
					acct.getAD_Org_ID());
			if (validCombiID <= 0)
			{
				MAccount newAccount = new MAccount(MAcctSchema.get(m_ctx, acct.getC_AcctSchema_ID()));
				newAccount.setAccount_ID(accountID);
				if (!newAccount.save())
					return CLogger.retrieveErrorString("Failed when try to save new Account Combination");
				validCombiID = newAccount.getC_ValidCombination_ID();
			}
			acct.set_ValueOfColumn(columnName, validCombiID);
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) 
	{
		m_trxName = trxName;
	}
	
	private int getAccountID (String value)
	{
		String sql = "SELECT C_ElementValue_ID FROM C_ElementValue WHERE Value = ? AND IsActive = ?";
		int accountID = DB.getSQLValue(get_TrxName(), sql, value, "Y");
		return accountID;
	}
	
	private int getValidCombinationID (int accountID, int acctSchemaID, int M_Product_ID, int AD_Org_ID)
	{
		String sql = "SELECT C_ValidCombination_ID FROM C_ValidCombination WHERE Account_ID = ? AND C_AcctSchema_ID = ? "
				+ " AND (M_Product_ID = ? OR M_Product_ID IS NULL) AND AD_Org_ID IN (?,?) ORDER BY AD_Org_ID, M_Product_ID DESC";
		int combinationID = DB.getSQLValue(get_TrxName(), sql, accountID, acctSchemaID, M_Product_ID, AD_Org_ID, 0);
		return combinationID;
	}
	
	private String get_TrxName ()
	{
		return m_trxName;
	}
	
	private String buildNotFoundMsg (String find, String value)
	{
		StringBuilder msgBuild = new StringBuilder("Could not find ");
		msgBuild.append(find).append(" ");
		msgBuild.append(value);
		String msg = msgBuild.toString();
		return msg;
	}
}
