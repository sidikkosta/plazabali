/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.compiere.model.MInvoice;
import org.compiere.model.MPayment;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MPaymentAllocate;
import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Harry
 *
 */
public class WriteOffInvoiceTangerang implements ImporterValidation {

	private Properties m_Ctx = null;
	//private Sheet m_Sheet = null;
	private String m_trxName = null;
	private int m_C_Currency_ID = 303;

	/**
	 * 
	 */
	public WriteOffInvoiceTangerang() {
	}

	public WriteOffInvoiceTangerang(Properties ctx, Sheet sheet, String trxName)
	{
		m_Ctx = ctx;
		//m_Sheet = sheet;
		m_trxName = trxName;
	}
	
	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MInvoice invoice = (MInvoice) po;
		
		if(po.get_ID() == 0)
			return "No invoice for document number of " + freeColVals.get("DocumentNo") + " at row of " + currentRow;
		
		String sql = "SELECT InvoiceOpen(?, 0)";
		BigDecimal openAmt = DB.getSQLValueBDEx(m_trxName, sql, invoice.get_ID());
		
		if (openAmt.signum() == 0)
		{
			if (!invoice.isPaid())
			{
				invoice.setIsPaid(true);
				invoice.saveEx();
			}
			return SimpleImportXLS.DONT_SAVE;
		}
		
		String docNoBks = (String) freeColVals.get("DocumentNoBekasi");
		BigDecimal sisaFaktur = (BigDecimal) freeColVals.get("SisaFaktur");
		sisaFaktur = sisaFaktur.setScale(0, BigDecimal.ROUND_HALF_UP);
		
		MPayment payment = new MPayment(m_Ctx, 0, m_trxName);

		Timestamp now = new Timestamp(System.currentTimeMillis());
		
		//payment.setDocumentNo(new StringBuilder(kdPelunasan).append("-").append(kdRelasi).toString());
		payment.setDescription("***Write-off tangerang invoice of " + invoice.getDocumentNo() 
				+ " with bekasi's invoice of " + docNoBks + ". Writtten with saldo of " + sisaFaktur + "***");
		payment.setAD_Org_ID(invoice.getAD_Org_ID());
		payment.setDateAcct(now);
		payment.setDateTrx(now);
		payment.setC_DocType_ID(1000361);
		payment.setC_Currency_ID(m_C_Currency_ID);
		payment.setC_BPartner_ID(invoice.getC_BPartner_ID());
		payment.setC_BankAccount_ID(1000077);
		payment.setTenderType(MPayment.TENDERTYPE_Cash);
		payment.saveEx();
		
		MPaymentAllocate alloc = new MPaymentAllocate(m_Ctx, 0, m_trxName);
		alloc.setC_Payment_ID(payment.get_ID());
		alloc.setAD_Org_ID(payment.getAD_Org_ID());
		alloc.setC_Invoice_ID(invoice.get_ID());
		alloc.setInvoiceAmt(openAmt);
		alloc.setAmount(Env.ZERO);
		alloc.setWriteOffAmt(openAmt);
		alloc.setPayToOverUnderAmount(Env.ZERO);
		alloc.saveEx();
		
		if (!payment.processIt(MPayment.ACTION_Complete))
		{
			return "Error while completing write-off payment for invoice no of " + invoice.getDocumentNo() + ".\n "
					+ "With error msg: " + payment.getProcessMsg();
		}
		
		return SimpleImportXLS.DONT_SAVE;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub

	}

}
