/**
 * 
 */
package com.unicore.importer;

import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.util.IProcessUI;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MProduct;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author AzHaidar
 *
 */
public class ShipmentTMGImporter implements ImporterValidation 
{
	private Properties m_ctx = null;
	private String m_trxName = null;
	Hashtable<String, PO> m_poRefMap = null;
	Hashtable<String, Object> m_freeColVals;
	IProcessUI m_processMonitor = null;
	
	private static final String COL_NoSJ 		= "NoSJ";
	private static final String COL_TglSJ		= "TglSJ";
	private static final String COL_KdCustomer	= "KdCustomer";
//	private static final String COL_NmCustomer	= "NmCustomer";
//	private static final String COL_NoFaktur	= "NoFaktur";
	private static final String COL_Description = "Description";
	private static final String COL_NoDO		= "NoDO";
//	private static final String COL_StatusLunas	= "StatusLunas";
	private static final String COL_TipeCustomer 	= "TipeCustomer";
	private static final String COL_NmLokasi		= "NmLokasi";
	private static final String COL_AlamatLokasi	= "AlamatLokasi";
//	private static final String COL_LineNo			= "LineNo";
//	private static final String COL_KdBrg		 	= "KdBrg";
//	private static final String COL_Qty				= "Qty";
	private static final String COL_Warehouse		= "Warehouse";
	
//	static final int WH_HO = 0;
//	static final int WH_AB = 0;
	
	static final int DEFAULT_SALESREPs	= 1001027;
	
	private MWarehouse WH_HO;
	private MWarehouse WH_AB;
	private MWarehouse WH_PVC;
	private int HO_ID = 1000039;

	static final int LOC_HO 	= 1000183;
	static final int LOC_AB 	= 1000184;
	static final int LOC_PVC 	= 1000185;
	
	//static final String
	
	
	/**
	 * 
	 */
	public ShipmentTMGImporter(Properties ctx, Sheet sheet, String trxName)
	{
		this.m_ctx = ctx;
		//this.m_sheet = sheet;
		this.m_trxName = trxName;
		m_processMonitor = Env.getProcessUI(ctx);
		WH_HO = MWarehouse.get(m_ctx, 1000061);
		WH_AB = MWarehouse.get(m_ctx, 1000062);
		WH_PVC = MWarehouse.get(m_ctx, 1000063);
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		m_poRefMap = poRefMap;
		MInOutLine iol = (MInOutLine) po;
		
		boolean isSOTrx = true;
		
		MProduct product = MProduct.get(m_ctx, iol.getM_Product_ID());
		
		if (!product.getProductType().equals(MProduct.PRODUCTTYPE_Item) || iol.getQtyEntered().signum() == 0)
			return SimpleImportXLS.DONT_SAVE;
		
		String noSJ = (String) freeColVals.get(COL_NoSJ);
		String whKey = (String) freeColVals.get(COL_Warehouse);
		
		MWarehouse theWH = WH_HO;
		int locator_ID = LOC_HO;
		if (whKey.equals("W.AB")) {
			theWH = WH_AB;
			locator_ID = LOC_AB;
		}
		else if (whKey.equals("W.PVC")) {
			theWH = WH_PVC;
			locator_ID = LOC_PVC;
		}
		
		MInOut io = (MInOut) poRefMap.get(noSJ);
		
		if (io == null)
		{
			io = new Query(m_ctx, MInOut.Table_Name, "DocumentNo=?", m_trxName)
				.setParameters(noSJ).first();
			
			if (io == null)
			{
				io = new MInOut(m_ctx, 0, m_trxName);
				io.setAD_Org_ID(HO_ID);
				io.setC_BPartner_ID ((Integer) freeColVals.get(COL_KdCustomer));
				
				String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE C_BPartner_ID=?";
				int[] bpLoc_IDs = DB.getIDsEx(m_trxName, sql, io.getC_BPartner_ID());
				
				if (bpLoc_IDs != null && bpLoc_IDs.length > 0)
					io.setC_BPartner_Location_ID(bpLoc_IDs[0]);
				
				//io.setAD_User_ID(invoice.getAD_User_ID());
				//
				io.setC_DocType_ID();
				io.setIsSOTrx (isSOTrx);
				io.setMovementType (isSOTrx ? MInOut.MOVEMENTTYPE_CustomerShipment : MInOut.MOVEMENTTYPE_VendorReceipts);
				io.setSalesRep_ID(DEFAULT_SALESREPs);
				//io.saveEx();
			}
			io.setMovementDate((Timestamp) freeColVals.get(COL_TglSJ));
			io.setDateAcct(io.getMovementDate());
			io.setM_Warehouse_ID (theWH.get_ID());
			
			String keterangan = (String) freeColVals.get(COL_Description);
			String noDO = (String) freeColVals.get(COL_NoDO);
			String tipeCustomer = (String) freeColVals.get(COL_TipeCustomer);
			String nmLokasi = (String) freeColVals.get(COL_NmLokasi);
			String alamatLokasi = (String) freeColVals.get(COL_AlamatLokasi);
			
			io.setDescription(keterangan + "; " + noDO + "; " + tipeCustomer);
			if (!Util.isEmpty(nmLokasi, true) || !Util.isEmpty(alamatLokasi, true)) {
				io.addDescription(" \n PIC Lokasi:" + nmLokasi + " \n Alamat Lokasi:" + alamatLokasi);
			}
			
			io.saveEx();
			
			m_poRefMap.put(noSJ, io);
		}
		
		iol.setAD_Org_ID(io.getAD_Org_ID());
		iol.setM_Locator_ID(locator_ID);
		iol.setM_InOut_ID(io.getM_InOut_ID());
		iol.setC_UOM_ID(product.getC_UOM_ID());
		iol.setMovementQty(iol.getQtyEntered());
		iol.setConfirmedQty(iol.getQtyEntered());
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		return m_poRefMap;
	}

	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
	}

}
