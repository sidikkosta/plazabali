/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MBankAccount;
import org.compiere.model.MBankStatement;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.MProduct;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.model.X_C_Invoice;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.unicore.base.model.MBankStatementLine;
import com.unicore.model.factory.UNSFinanceModelFactory;
import com.uns.importer.ImporterValidation;
import com.uns.util.UNSTimeUtil;

/**
 * @author AzHaidar
 *
 */
public class InvoiceTMGImporterValidation implements ImporterValidation {

	private static CLogger log = CLogger.getCLogger(InvoiceTMGImporterValidation.class);

	//private Sheet m_sheet = null;
	private Properties m_ctx = null;
	private String m_trxName = null;
	private boolean m_IsCurrTrxImport = false;
	Hashtable<String, PO> m_poRefMap = null;
	Hashtable<String, Object> m_freeColVals;
	
	static final String COL_KdPelanggan 	= "KdPelanggan";
	static final String cOL_NmPelanggan 	= "NmPelanggan";
	static final String COL_NoFaktur		= "NoFaktur";
	static final String COL_NoSJ			= "NoSJ";
	static final String COL_TglSJ			= "TglSJ";
	static final String COL_TglFaktur		= "TglFaktur";
	static final String COL_TglJthTempo	= "TglJthTempo";
	static final String COL_SubTotal		= "SubTotal";
	static final String COL_Discount1		= "Discount1";
	static final String COL_Discount2		= "Discount2";
	static final String COL_TaxAmt		= "TaxAmt";
	static final String COL_Ongkir		= "Ongkir";
	static final String COL_GrandTotal	= "GrandTotal";
	static final String COL_Sisa			= "Sisa";
	static final String COL_LineNo		= "LineNo";
	static final String COL_KdBarang		= "KdBarang";
	static final String COL_NmBarang		= "NmBarang";
	static final String COL_QtyEntered	= "QtyEntered";
	static final String COL_QtyInvoiced	= "QtyInvoiced";
	static final String COL_Price			= "Price";
	static final String COL_DiscountLine	= "DiscountLine";
	static final String COL_TotalLine		= "TotalLine";
	static final String COL_IsSOTrx		= "IsSOTrx";
	static final String COL_PRICELIST		= "PriceList";
	static final String COL_Warehouse		= "Warehouse";
	
	static final int CURRENCY_IDR			= 303;
	static final int DEFAULT_SALESREPs	= 1001027;
	
	static final int PRICELIST_TOKO		= 1000099;
	static final int PRICELIST_KONSUMEN	= 1000100;
	static final int AP_INVOICE 			= 1000358;
	static final int AR_INVOICE 			= 1000355;
	static final int AP_CREDITMEMO 	= 1000359;
	static final int AR_CREDITMEMO 	= 1000357;
	
	public static final int MM_RECEIPT = 1000367;
	public static final int MM_SHIPMENT = 1000364;
	public static final int MM_VENDOR_RETURN = 1000366;
	public static final int MM_CUSTOMER_RETURN = 1000368;
	
	static final int LOC_HO 	= 1000183;
	static final int LOC_AB 	= 1000184;
	static final int LOC_PVC 	= 1000185;
	
	static final String WH_AB 	= "W.AB";
	static final String WH_PVC 	= "W.PP";
	
	static final int CHARGE_ONGKIR 		= 1000043;
	static final int CHARGE_SALES_REVENUE = 1000063;
	
	static final Timestamp CUTOFFSisaDAY;
	static final Timestamp CUTOFFShipmentTrx;
	static final Timestamp CUTOFFSaldo;
	
	IProcessUI m_processMonitor = null;
	
	//Hashtable<Integer, BigDecimal> m_PaymentAmtMap = new Hashtable<Integer, BigDecimal>();
	
	static {
		Calendar cal = Calendar.getInstance();
		cal.set(2015, 7, 3, 0, 0, 0);
		
		CUTOFFShipmentTrx = new Timestamp(cal.getTimeInMillis());
		
		cal.set(2015, 0, 1, 0, 0, 0);
		CUTOFFSisaDAY = new Timestamp(cal.getTimeInMillis());
		
		cal.set(2015, 7, 11, 0, 0, 0);
		CUTOFFSaldo = new Timestamp(cal.getTimeInMillis());
	}
	
	/**
	 * 
	 */
	public InvoiceTMGImporterValidation() {
	}

	public InvoiceTMGImporterValidation(Properties ctx, Sheet sheet, String trxName)
	{
		this.m_ctx = ctx;
		//this.m_sheet = sheet;
		this.m_trxName = trxName;
		m_processMonitor = Env.getProcessUI(ctx);
	}
	
	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		m_poRefMap = poRefMap;
		
		MInvoiceLine il = (MInvoiceLine) po;
		il.setAD_Org_ID(1000039); // Head Office.
		
		if (il.getM_Product_ID() == 0)
			return "Cannot find product of " + freeColVals.get(COL_NmBarang) + " at row " + currentRow;
		
		String noFaktur = (String) freeColVals.get(COL_NoFaktur);
		MInvoice theInvoice = null;
		boolean initInvoice = false;
		
		m_IsCurrTrxImport = ((Timestamp) freeColVals.get(COL_TglFaktur)).after(CUTOFFSisaDAY);
		
		MInvoiceHolder invHolder = (MInvoiceHolder) poRefMap.get(noFaktur);
		
		if (il.getC_Invoice_ID() == 0 || invHolder == null)
		{
			if (invHolder != null)
				theInvoice = invHolder.m_theInvoice;
			
			if (theInvoice == null) 
			{
				String sql = "SELECT C_Invoice_ID FROM C_Invoice WHERE DocumentNo=?";
				int inv_ID = DB.getSQLValueEx(m_trxName, sql, noFaktur);
				
				if (inv_ID > 0) {
					theInvoice = new MInvoice(m_ctx, inv_ID, m_trxName);
				}
				else {
					theInvoice = createNewHeader(il, freeColVals);
				}
				//poRefMap.put(noFaktur, theInvoice);
			}
			il.setC_Invoice_ID(theInvoice.get_ID());
			initInvoice = true;
		}
		else if (poRefMap.get(noFaktur) == null)
		{
			theInvoice = (MInvoice) il.getC_Invoice();
			//poRefMap.put(noFaktur, theInvoice);
			initInvoice = true;
		}
		
		BigDecimal grandTotal = (BigDecimal) freeColVals.get(COL_GrandTotal);
		BigDecimal sisa = (BigDecimal) freeColVals.get(COL_Sisa);
		BigDecimal porsiSisa = grandTotal.signum() == 0? 
				Env.ONE : sisa.divide(grandTotal, 10, BigDecimal.ROUND_UP);
		
		BigDecimal paidAmt = grandTotal.subtract(sisa);
		//m_PaymentAmtMap.put(theInvoice.get_ID(), paidAmt);
		
		if (invHolder == null) {
			String warehouse = freeColVals.get(COL_Warehouse) == null? null : (String) freeColVals.get(COL_Warehouse);
			invHolder = new MInvoiceHolder(m_ctx, theInvoice, warehouse, paidAmt, m_trxName);
			poRefMap.put(noFaktur, invHolder);
		}
		
		if (initInvoice) 
		{
			String priceListStr = (String) freeColVals.get(COL_PRICELIST);
			int priceList_ID = PRICELIST_KONSUMEN;
			
			if (priceListStr.equals("Toko")) {
				priceList_ID = PRICELIST_TOKO;
			}
			
			theInvoice.setM_PriceList_ID(priceList_ID);

			BigDecimal addDiscount = (BigDecimal) freeColVals.get(COL_Discount1);
			addDiscount = addDiscount.add((BigDecimal) freeColVals.get(COL_Discount2));
			
			if (!m_IsCurrTrxImport)
				addDiscount = porsiSisa.multiply(addDiscount).setScale(2, BigDecimal.ROUND_DOWN);
			
			theInvoice.setAddDiscountAmt(addDiscount);
			theInvoice.setDescription("No Faktur:#" + noFaktur + 
					"; Starting AR Amount=" + NumberFormat.getInstance().format(grandTotal) + 
					"; Current Balance=" + NumberFormat.getInstance().format(sisa));
			
			theInvoice.saveEx();
			
			BigDecimal ongkir = (BigDecimal) freeColVals.get(COL_Ongkir);
			
			if (ongkir.signum() > 0)
			{
				// Try to get the charge for ongkir if exists.
				MInvoiceLine il_ongkir = 
						new Query(m_ctx, MInvoiceLine.Table_Name, "C_Charge_ID=? AND C_Invoice_ID=?", m_trxName)
						.setParameters(CHARGE_ONGKIR, theInvoice.get_ID())
						.first();
				
				if (il_ongkir == null)
				{
					BigDecimal sisaOngkir = porsiSisa.multiply(ongkir).setScale(2, BigDecimal.ROUND_UP);
					BigDecimal currentOngkir = (m_IsCurrTrxImport) ? ongkir : sisaOngkir;
					
					il_ongkir = new MInvoiceLine(theInvoice);
					il_ongkir.setC_UOM_ID(100);
					il_ongkir.setC_Charge_ID(CHARGE_ONGKIR);
					il_ongkir.setQty(Env.ONE);
					il_ongkir.setPrice(currentOngkir);
					il_ongkir.setPriceEntered(currentOngkir);
					il_ongkir.setPriceList(currentOngkir);
					il_ongkir.setPriceLimit(currentOngkir);
					il_ongkir.setDescription("Total Tagihan Ongkir= " + 
											NumberFormat.getInstance().format(ongkir.doubleValue())
											+ "; Balance= " + NumberFormat.getInstance().format(sisaOngkir.doubleValue()));
					il_ongkir.saveEx();
				}
			}
		}
		
		//Calculate line.
		BigDecimal qty 		= (BigDecimal) freeColVals.get(COL_QtyEntered);
		BigDecimal price 	= (BigDecimal) freeColVals.get(COL_Price);
		BigDecimal disc		= (BigDecimal) freeColVals.get(COL_DiscountLine);
		BigDecimal sisaPrice= porsiSisa.multiply(price).setScale(2, BigDecimal.ROUND_UP);
		BigDecimal sisaDisc = porsiSisa.multiply(disc).setScale(2, BigDecimal.ROUND_UP);
		BigDecimal totalLine = qty.multiply(price);
		BigDecimal totalLineSisa = qty.multiply(sisaPrice);
		
		BigDecimal currentPrice = (m_IsCurrTrxImport)? price : sisaPrice;
		BigDecimal currentLineDisc = (m_IsCurrTrxImport)? disc : sisaDisc;
		
		//if (m_IsCurrTrxImport)
		MProduct product = MProduct.get(m_ctx, il.getM_Product_ID());
		il.setC_UOM_ID(product.getC_UOM_ID());
		il.setQty(qty);
		il.setPrice(currentPrice);
		il.setPriceEntered(currentPrice);
		il.setPriceList(currentPrice);
		il.setPriceLimit(currentPrice);
		il.setDiscountAmt(currentLineDisc);
		il.setDescription(product.getValue() + "_"+ product.getName()
				+  " Subtotal Tagihan= " + NumberFormat.getInstance().format(totalLine.doubleValue())
				+ "; Balance= " + NumberFormat.getInstance().format(totalLineSisa.doubleValue()));
		
		return null;
	}
	
	/**
	 * Create new Invoice Header.
	 * @param invLine
	 * @param freeColVals
	 * @return
	 */
	private MInvoice createNewHeader(MInvoiceLine invLine, Hashtable<String, Object> freeColVals)
	{
		MInvoice header = new MInvoice(m_ctx, 0, m_trxName);
		header.setAD_Org_ID(invLine.getAD_Org_ID());
		header.setDateAcct((Timestamp) freeColVals.get(COL_TglFaktur));
		header.setDateInvoiced((Timestamp) freeColVals.get(COL_TglFaktur));
		
		boolean isSOTrx = (Boolean) freeColVals.get(COL_IsSOTrx);
		
		int C_DocType_ID = AP_INVOICE;
		if (isSOTrx)
			C_DocType_ID = AR_INVOICE;
			
		String invReffNo = (String) freeColVals.get(COL_NoFaktur);
		
		header.setReferenceNo("SJ:" + (String) freeColVals.get(COL_NoSJ));
		header.setC_DocType_ID(C_DocType_ID);
		header.setC_DocTypeTarget_ID(C_DocType_ID);
		header.setDocumentNo(invReffNo);

		Timestamp docDate = (Timestamp) freeColVals.get(COL_TglFaktur);
		Timestamp dueDate = (Timestamp) freeColVals.get(COL_TglJthTempo);
		
		int TOP = UNSTimeUtil.getDaysBetween(docDate, dueDate);
		
		//String paymentRule = MInvoice.PAYMENTRULE_OnCredit;
		
		if (TOP == 0) {
			header.setPaymentRule(MInvoice.PAYMENTRULE_Cash);
			//paymentRule = MInvoice.PAYMENTRULE_Cash;
		}
		else {
			if(TOP < 0)
			{
				TOP = ~TOP + 1;
			}
			String topValue = "TOP-" + TOP + "D";
			String sql = "SELECT C_PaymentTerm_ID FROM C_PaymentTerm WHERE Value=?";
			
			int paytermID = DB.getSQLValueEx(m_trxName, sql, topValue);
			
			header.setPaymentRule(MInvoice.PAYMENTRULE_OnCredit);
			header.setC_PaymentTerm_ID(paytermID);
		}
		
		//header.setSalesRep_ID((Integer) freeColVals.get(COL_SALES));
		header.setSalesRep_ID(DEFAULT_SALESREPs);
		header.setC_Currency_ID(CURRENCY_IDR);
		header.setIsSOTrx(isSOTrx);
		//Object bp_Obj = freeColVals.get(COL_KdPelanggan);
		int bp_ID = (Integer) freeColVals.get(COL_KdPelanggan);
		header.setC_BPartner_ID(bp_ID);
		
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE C_BPartner_ID=?";
		int[] bpLoc_IDs = DB.getIDsEx(m_trxName, sql, header.getC_BPartner_ID());
		
		if (bpLoc_IDs != null && bpLoc_IDs.length > 0)
			header.setC_BPartner_Location_ID(bpLoc_IDs[0]);
		
//		BigDecimal grandTotal = (BigDecimal) freeColVals.get(COL_GrandTotal);
//		BigDecimal addDiscount = (BigDecimal) freeColVals.get(COL_Discount1);
//		addDiscount = addDiscount.add((BigDecimal) freeColVals.get(COL_Discount2));
//		BigDecimal sisa = (BigDecimal) freeColVals.get(COL_Sisa);
//		addDiscount = 
//				sisa.divide(grandTotal, 10, BigDecimal.ROUND_UP)
//				.multiply(addDiscount).setScale(2, BigDecimal.ROUND_DOWN);
//		
//		header.setAddDiscountAmt(addDiscount);
//		header.setDescription("No Faktur : " + invReffNo + 
//				"; Starting AR Amount=" + grandTotal + 
//				"; Current Balance=" + sisa);
		
		header.saveEx();
		
		return header;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) 
	{
		Collection<PO> values = poRefMap.values();
		Timestamp duaBelasAgustus = TimeUtil.addDays(CUTOFFSaldo, 1);
		Timestamp tigaAgustus = CUTOFFShipmentTrx;
		
		for (PO po : values)
		{
			MInvoiceHolder holder = (MInvoiceHolder) po;
			MInvoice inv = holder.m_theInvoice;
			
			if (inv.getDateInvoiced().before(CUTOFFSisaDAY))
				continue;
			
			BigDecimal paidAmt = holder.m_paidAmt;
			
			m_processMonitor.statusUpdate(
					"AfterSaveAllRow : processing invoice of : " + inv.getDocumentNo() + " #" + inv.getDateInvoiced());
			
			if (paidAmt != null && paidAmt.signum() > 0)
			{
				String whereClause = 
						"C_Invoice_ID=? AND C_Payment_ID=(SELECT C_Payment_ID FROM C_Payment WHERE DocumentNo=?)";
				
				MPaymentAllocate theAllocate =
						new Query(m_ctx, MPaymentAllocate.Table_Name, whereClause, m_trxName)
						.setParameters(inv.get_ID(), inv.getDocumentNo())
						.first();
				
				int payment_ID = 0;
				
				if (theAllocate == null) {
					MPayment thePayment = createNewPayment(inv, paidAmt);
					payment_ID= thePayment.get_ID();
				}
				else {
					BigDecimal kurangBayar = inv.getGrandTotal().subtract(paidAmt);
					
					theAllocate.setInvoiceAmt(inv.getGrandTotal());
					theAllocate.setAmount(paidAmt);
					theAllocate.setPayToOverUnderAmount(paidAmt);
					theAllocate.setOverUnderAmt(kurangBayar);
					theAllocate.saveEx();
					
					payment_ID = theAllocate.getC_Payment_ID();
				}
				
				String sql = "SELECT C_BankAccount_ID FROM C_BankAccount WHERE BankAccountType=?";
				int bankAcc_ID = DB.getSQLValue(m_trxName, sql, MBankAccount.BANKACCOUNTTYPE_OffsetInitialBalance);
				
				whereClause = "C_Payment_ID=?";
				
				MBankStatementLine bsl = 
						com.uns.base.model.Query.get(
								m_ctx, UNSFinanceModelFactory.EXTENSION_ID, 
								MBankStatementLine.Table_Name, whereClause, m_trxName)
						.setParameters(payment_ID)
						.first();
				
				if (bsl == null) 
				{
					sql = "SELECT C_BankStatement_ID FROM C_BankStatement "
							+ "WHERE C_BankAccount_ID=? AND DocStatus='DR'";// AND StatementDate >= ?";
					int stmt_ID = DB.getSQLValueEx(m_trxName, sql, bankAcc_ID);//, inv.getDateInvoiced());
					
					if (stmt_ID < 0)
						stmt_ID = 0;
					
					MBankStatement stmt = new MBankStatement(m_ctx, stmt_ID, m_trxName);
					
					if (stmt_ID == 0) {
						stmt.setName("Offset initial balance for Sales Revenue @ Date " + inv.getDateInvoiced());
						stmt.setC_BankAccount_ID(bankAcc_ID);
						stmt.setDateAcct(CUTOFFSaldo);
						stmt.setStatementDate(CUTOFFSaldo);
						stmt.saveEx();
					}
					
					bsl = new MBankStatementLine(stmt);
					String trxType = inv.isSOTrx()? 
							MBankStatementLine.TRANSACTIONTYPE_ARTransaction : 
								MBankStatementLine.TRANSACTIONTYPE_APTransaction;
					bsl.setAD_Org_ID(inv.getAD_Org_ID());
					bsl.setTransactionType(trxType);
					bsl.setDescription("Invoice#" + inv.getDocumentNo());
					bsl.setDateAcct(bsl.getStatementLineDate());
					bsl.setIsManual(true);
					bsl.setValutaDate(bsl.getStatementLineDate());
					bsl.setC_Payment_ID(payment_ID);
				}
				Timestamp stmtLineDate = tigaAgustus;
				
				if (inv.getDateInvoiced().before(tigaAgustus))
					;
				else if (inv.getDateInvoiced().before(duaBelasAgustus))
					stmtLineDate = duaBelasAgustus;
				
				BigDecimal signum = inv.isSOTrx()? Env.ONE : Env.ONE.negate();
				
				bsl.setDateAcct(stmtLineDate);
				bsl.setStatementLineDate(stmtLineDate);
				bsl.setValutaDate(stmtLineDate);
				bsl.setAmount(paidAmt);
				bsl.setStmtAmt(paidAmt.multiply(signum));
				bsl.setTrxAmt(paidAmt.multiply(signum));
				bsl.setC_Currency_ID(CURRENCY_IDR);
				bsl.saveEx();
			}
			
			if (inv.getDateInvoiced().before(CUTOFFShipmentTrx))
				continue;
			
			createShipReceiptFromInvoice(inv, holder.m_warehouseKey);
		}
		
		return null;
	}

	/**
	 * 
	 * @param invoice
	 * @return
	 */
	MPayment createNewPayment(MInvoice invoice, BigDecimal paidAmt)
	{
		MPayment payment = new MPayment(m_ctx, 0, m_trxName);
		
		payment.setC_DocType_ID(invoice.isSOTrx());
		payment.setAD_Org_ID(invoice.getAD_Org_ID());
		payment.setC_BPartner_ID(invoice.getC_BPartner_ID());
		payment.setPayAmt(invoice.getGrandTotal());
		payment.setC_Currency_ID(303);
		payment.setDescription("***Auto payment for initial AR Balance***");
		payment.setDocumentNo(invoice.getDocumentNo());

		payment.setDateTrx(invoice.getDateInvoiced());
		
		Timestamp dateAcct = TimeUtil.addDays(CUTOFFShipmentTrx, -1);
		payment.setDateAcct(dateAcct);
		payment.setTenderType(MPayment.TENDERTYPE_DirectDeposit);
		
		String sql = "SELECT C_BankAccount_ID FROM C_BankAccount WHERE BankAccountType=?";
		int bankAcc_ID = DB.getSQLValue(m_trxName, sql, MBankAccount.BANKACCOUNTTYPE_OffsetInitialBalance);
		
		payment.setC_BankAccount_ID(bankAcc_ID);
		
		payment.saveEx();
		
		BigDecimal kurangBayar = invoice.getGrandTotal().subtract(paidAmt);
		
		MPaymentAllocate payAllocate = new MPaymentAllocate(m_ctx, 0, m_trxName);
		payAllocate.setAD_Org_ID(invoice.getAD_Org_ID());
		payAllocate.setC_Payment_ID(payment.get_ID());
		payAllocate.setC_Invoice_ID(invoice.get_ID());
		payAllocate.setAmount(paidAmt);
		payAllocate.setInvoiceAmt(invoice.getGrandTotal());
		payAllocate.setPayToOverUnderAmount(paidAmt);
		payAllocate.setOverUnderAmt(kurangBayar);
		payAllocate.saveEx();
		
		return payment;
	}

	/**
	 * 
	 * @param theInvoice
	 * @param docType_ID
	 * @return
	 */
	static String createShipReceiptFromInvoice(MInvoice theInvoice, String whKey)
	{
		String trxName 	= theInvoice.get_TrxName();
		Properties ctx	= theInvoice.getCtx();
		
		MInOut io = new Query(ctx, MInOut.Table_Name, "C_Invoice_ID=? AND IsSOTrx=?", trxName)
		.setParameters(theInvoice.get_ID(), theInvoice.isSOTrx())
		.first();
	
		int invOrg_ID = theInvoice.getAD_Org_ID();
		int docType_ID = analyzeShipReceiptDocType(theInvoice);
		
		if (io == null) 
		{
			String sql = "SELECT M_Warehouse_ID FROM M_Warehouse WHERE Value=?";
			
			int wh_ID = DB.getSQLValueEx(trxName, sql, whKey);
			
			if (wh_ID <= 0)
				throw new AdempiereException("Cannot find warehouse which search key " + whKey);
					
			io = new MInOut(theInvoice, docType_ID, theInvoice.getDateAcct(), wh_ID);
			io.setMovementType(analyzeMovementType(theInvoice));
			io.setAD_Org_ID(invOrg_ID);
			io.setDocumentNo(theInvoice.getDocumentNo());
			io.setC_Invoice_ID(theInvoice.get_ID());
			io.saveEx();
		}
		else {
			String sql = "UPDATE C_InvoiceLine SET M_InOutLine_ID=NULL WHERE C_Invoice_ID=" + theInvoice.get_ID();
			int count = DB.executeUpdateEx(sql, trxName);
			
			sql = "DELETE FROM M_InOutLine WHERE M_InOut_ID=" + io.get_ID();
			count = DB.executeUpdateEx(sql, trxName);
			log.info("Deleting " + count + " ship/receipt line for invoice 0f " + theInvoice.getDocumentNo());
			
//			if (m_processMonitor != null)
//				m_processMonitor.statusUpdate(
//						"Deleting " + count + " ship/receipt line for invoice 0f " + theInvoice.getDocumentNo());
		}
		
		int M_Locator_ID = LOC_HO;
		
		if (whKey.equals(WH_AB))
			M_Locator_ID = LOC_AB;
		else if (whKey.equals(WH_PVC))
			M_Locator_ID = LOC_PVC;

		int count=0;
		
		for (MInvoiceLine il : theInvoice.getLines(true))
		{
			if (il.getQtyInvoiced().signum() == 0)
				continue;
			
			if (il.getM_Product_ID() > 0)
			{
				MProduct product = MProduct.get(ctx, il.getM_Product_ID());
				if (!product.getProductType().equals(MProduct.PRODUCTTYPE_Item))
					continue;
			}
			else {
				continue;
			}
			
			MInOutLine iol = new MInOutLine(io);
			
			iol.setM_Product_ID(il.getM_Product_ID());
			iol.setC_UOM_ID(il.getC_UOM_ID());

			if (!io.isSOTrx() || io.getC_DocType_ID() == MM_CUSTOMER_RETURN)
				MAttributeSetInstance.initAttributeValuesFrom(
						iol, MInOutLine.COLUMNNAME_M_Product_ID, 
						MInOutLine.COLUMNNAME_M_AttributeSetInstance_ID, trxName);
			
			iol.setInvoiceLine(il, M_Locator_ID, il.getQtyInvoiced());
			iol.setQty(il.getQtyInvoiced());
			
			iol.saveEx();
			
			il.setM_InOutLine_ID(iol.get_ID());
			//il.saveEx();
			if (!il.save())
				throw new AdempiereException("Cannot set inout line id.");
			
			count++;
		}
		
		if (count == 0) {
			io.deleteEx(true);
		}
		
		return null;
	} // createShipmentFromInvoice
	
	static int analyzeShipReceiptDocType(MInvoice inv)
	{
		int dt_id = MM_RECEIPT;
		if (inv.getC_DocType_ID() == AR_INVOICE)
			dt_id = MM_SHIPMENT;
		else if (inv.getC_DocType_ID() == AR_CREDITMEMO)
			dt_id = MM_CUSTOMER_RETURN;
		else if (inv.getC_DocType_ID() == AP_CREDITMEMO)
			dt_id = MM_VENDOR_RETURN;
		return dt_id;
	}
	
	static String analyzeMovementType(MInvoice inv)
	{
		String movementType = MInOut.MOVEMENTTYPE_VendorReceipts;
		if (inv.getC_DocType_ID() == AR_INVOICE)
			movementType = MInOut.MOVEMENTTYPE_CustomerShipment;
		else if (inv.getC_DocType_ID() == AR_CREDITMEMO)
			movementType = MInOut.MOVEMENTTYPE_CustomerReturns;
		else if (inv.getC_DocType_ID() == AP_CREDITMEMO)
			movementType = MInOut.MOVEMENTTYPE_VendorReturns;
		return movementType;
	} // analyzeIODocType
	
	
	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		return m_poRefMap;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
	}
}

class MInvoiceHolder extends X_C_Invoice
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 656821722073591777L;
	
	MInvoice 	m_theInvoice;
	BigDecimal 	m_paidAmt;
	String		m_warehouseKey;
	
	public MInvoiceHolder(Properties ctx, MInvoice inv, String warehouse, BigDecimal paidAmt, String trxName) {
		super(ctx, 0, trxName);
		m_theInvoice = inv;
		m_paidAmt = paidAmt;
		m_warehouseKey = warehouse;
	}	
}
