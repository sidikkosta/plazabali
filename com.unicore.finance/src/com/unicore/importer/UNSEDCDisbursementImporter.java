/**
 * 
 */
package com.unicore.importer;

import java.util.Calendar;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.uns.importer.ImporterValidation;
import com.uns.model.MUNSDisbursReconBatch;
import com.uns.model.MUNSDisbursReconLine;
import com.uns.model.process.SimpleImportXLS;
import com.uns.util.UNSCardType;

/**
 * @author Burhani Adam
 *
 */
public class UNSEDCDisbursementImporter implements ImporterValidation{

	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, PO> m_PORefMap = null;
	final String COL_BACTHNO		= "A";
	final String COL_CARDNUMBER		= "B";
	final String COL_DATE			= "C";
	final String COL_ORIGINALAMOUNT	= "D";
	final String COL_NETAMOUNT		= "E";
	final String COL_MERCHANTNAME	= "F";
	final String COL_STATUS			= "G";
	private String batchNo = null;
	private String status = null;
	private String cardNo = null;
	private String dateTrx = null;
	
	/**
	 * 
	 */
	public UNSEDCDisbursementImporter(Properties ctx, Sheet sheet, String trxName) {
		super();
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		if(freeColVals.get(COL_BACTHNO) == null)
		{
			throw new AdempiereException("Mandatory column batch No on line "+currentRow);
		}
		
		if(freeColVals.get(COL_CARDNUMBER) == null)
		{
			return SimpleImportXLS.DONT_SAVE;
//			throw new AdempiereException("Mandatory column card number on line "+currentRow);
		}
		
		batchNo = freeColVals.get(COL_BACTHNO).toString();
		String tmpBatch = batchNo.replace("0", "");
		if(tmpBatch.length() == 0)
			batchNo = "0";
		status = freeColVals.get(COL_STATUS).toString();
		cardNo = freeColVals.get(COL_CARDNUMBER).toString();
		
		if(!status.equals("PAID"))
			return SimpleImportXLS.DONT_SAVE;
		
		cardNo = cardNo.replace("X", "");
		String tmpCardNo = cardNo;
		for(int i=0;i<cardNo.length();i++)
		{
			if(tmpCardNo.startsWith("0"))
				tmpCardNo = cardNo.substring(i, cardNo.length());
		}
		cardNo = cardNo.replace("0", "");
		
		if(Util.isEmpty(cardNo, true))
			return SimpleImportXLS.DONT_SAVE;
		
		dateTrx = freeColVals.get(COL_DATE).toString();
		
		if(Util.isEmpty(dateTrx, true))
			return SimpleImportXLS.DONT_SAVE;
			
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, Integer.valueOf(dateTrx.substring(0, 4)));
		cal.set(Calendar.MONTH, (Integer.valueOf(dateTrx.substring(4, 6)) -1));
//		cal.add(Calendar.MONTH, -2);
		cal.set(Calendar.DATE, Integer.valueOf(dateTrx.substring(6, 8)));
		java.sql.Timestamp date = new java.sql.Timestamp(cal.getTimeInMillis());
		date = TimeUtil.trunc(date, TimeUtil.TRUNC_DAY);
		MUNSDisbursReconLine line = (MUNSDisbursReconLine) po;
		MUNSDisbursReconBatch batch = MUNSDisbursReconBatch.getCreate(m_ctx, batchNo,
				line.getUNS_DisbursementRecon_ID(), po.get_TrxName());
		batch.setMerchant(freeColVals.get(COL_MERCHANTNAME).toString());
		batch.setisRefund(batchNo.equals("0") ? true : false);
		batch.setAD_Org_ID(line.getUNS_DisbursementRecon().getAD_Org_ID());
		batch.saveEx();
		
		po.set_ValueNoCheck("UNS_DisbursRecon_Batch_ID", batch.get_ID());
		po.set_ValueNoCheck("DateTrx", date);
		po.set_ValueNoCheck("AD_Org_ID", batch.getAD_Org_ID());
		if (tmpCardNo.length() > 6)
			tmpCardNo = tmpCardNo.substring(0, 6);
		int idCT = 0;
		UNSCardType ct = new UNSCardType();
		for (int i=tmpCardNo.length(); i>0; i--)
		{
			Integer id = ct.get(tmpCardNo);
			if (id != null)
			{
				idCT = id.intValue();
				po.set_ValueNoCheck("UNS_CardType_ID", idCT);
				break;
			}
			tmpCardNo = tmpCardNo.substring(0,i-1);
		}
		if(idCT <= 0)
		{
			po.set_ValueNoCheck("UNS_CardType_ID", DB.getSQLValue(po.get_TrxName(), 
					"SELECT UNS_CardType_ID FROM UNS_CardType WHERE Value = ?", "08"));
		}
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main (String args[])
	{
		String a = "000000";
		boolean b = true;
		String c = a.replace("0", "");
		b = c.length() > 0;
		
		System.out.println(b ? "Y" : "N");
	}
}