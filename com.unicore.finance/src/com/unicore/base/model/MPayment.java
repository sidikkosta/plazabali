/**
 * 
 */
package com.unicore.base.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;

/**
 * @author setyaka
 *
 */
public class MPayment extends org.compiere.model.MPayment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2185046750335145854L;

	public MPayment(Properties ctx, int C_Payment_ID, String trxName) {
		super(ctx, C_Payment_ID, trxName);
	}

	public MPayment(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		String sql = "SELECT C_Currency_ID FROM C_BankAccount WHERE C_BankAccount_ID = ?";
		int curr = DB.getSQLValue(get_TrxName(), sql, getC_BankAccount_ID());
		if(curr <= 0)
			throw new AdempiereException("Currency of Bank Account can't found");
		
		if(getC_Currency_ID() != curr)
			throw new AdempiereException("Currency of Payment disallowed different with currency of Bank Account");
		
		return super.afterSave(newRecord, success);
	}
}
