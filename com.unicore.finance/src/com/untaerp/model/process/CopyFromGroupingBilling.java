package com.untaerp.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.unicore.model.MUNSBilling;
import com.unicore.model.MUNSBillingGroup;

public class CopyFromGroupingBilling extends SvrProcess {
	
	private int p_billingGroup;
	private MUNSBillingGroup m_BillingGroup = null;
	
	public CopyFromGroupingBilling() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for(int i=0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if(name.equals("UNS_BillingGroup_ID"))
				p_billingGroup = para[i].getParameterAsInt();
			else
				throw new AdempiereException("Unknown Parameter "+name);
		}
		
		m_BillingGroup = new MUNSBillingGroup(getCtx(), p_billingGroup, get_TrxName());
		if(m_BillingGroup == null)
			throw new AdempiereException("Cannot get Grouping Billing that want to copy");
	}

	@Override
	protected String doIt() throws Exception {
	
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Only can access from Window");
		
		MUNSBillingGroup currGroup = new MUNSBillingGroup(getCtx(), getRecord_ID(), get_TrxName());
	
		//copy Billing
		MUNSBilling[] lastBill =  m_BillingGroup.getLines(false);
		
		for(int i=0; i < lastBill.length; i++)
		{
			MUNSBilling newBill = new MUNSBilling(getCtx(), 0, get_TrxName());
			
			PO.copyValues(lastBill[i], newBill);
			
			newBill.setUNS_BillingGroup_ID(currGroup.getUNS_BillingGroup_ID());
			newBill.setAD_Org_ID(lastBill[i].getAD_Org_ID());
			newBill.setOldBilling_ID(lastBill[i].getUNS_Billing_ID());
			newBill.setDocumentNo(lastBill[i].getDocumentNo());
			newBill.setGenerateBillingLine(lastBill[i].getGenerateBillingLine());
			newBill.setTotalAmt(Env.ZERO);
			newBill.setPaidAmt(Env.ZERO);
			newBill.setOpenAmt(Env.ZERO);
			newBill.saveEx();
			
		}
			
		return "Success ::"+currGroup.getDocumentNo();
	}

}
