/**
 * 
 */
package com.untaerp.model.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MConversionRate;
import org.compiere.model.MCurrency;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.untaerp.model.MUNSCurrsBalance;
import com.untaerp.model.MUNSCurrsBalanceLine;


/**
 * @author AzHaidar, BurhaniAdam
 * @see www.untasoft.com
 */
public class GenerateCurrencyBalances extends SvrProcess 
{
	private boolean DeleteOld = false;
	
	//private IProcessUI m_processMonitor = Env.getProcessUI(getCtx());
	
	/**
	 * 
	 */
	public GenerateCurrencyBalances() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] para = getParameter();
		
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOld"))
				DeleteOld = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		//m_processMonitor = Env.getProcessUI(getCtx());
		
		//m_processMonitor.statusUpdate("Deleting previous loaded lines.");
		if (DeleteOld)
		{
			String sql = "DELETE FROM UNS_CurrsBalance_Line"
					+ " WHERE UNS_CurrsBalance_ID=" + getRecord_ID();
			
			int count = DB.executeUpdate(sql, get_TrxName());
			
			addLog("Previous lines deleted #" + count);
		}
		
		//m_processMonitor.statusUpdate("Reload & recalculate lines.");
		
		MUNSCurrsBalance cb = new MUNSCurrsBalance(getCtx(), getRecord_ID(), get_TrxName());
		
		String sql = "SELECT e.AccountType, f.Account_ID, e.Value, f.C_Currency_ID, "
				+ "	SUM(f.AmtAcctDr-f.AmtAcctCr), SUM(f.AmtSourceDr-f.AmtSourceCr)"
				+ " FROM Fact_Acct f"
				+ " INNER JOIN C_ElementValue e ON f.Account_ID=e.C_ElementValue_ID"
				+ " WHERE f.DateAcct <= ? AND f.C_AcctSchema_ID=? AND f.C_Currency_ID<>?"
				+ "		AND f.AD_Org_ID=? AND e.IsMonetary='Y'"
				+ "	GROUP BY e.AccountType, f.Account_ID, e.Value, f.C_Currency_ID"
				+ " ORDER BY e.AccountType, e.Value, f.C_Currency_ID";
		
		PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
		ResultSet rs = null;
		
		int count = 0;
		
		try {
			int currencyAS_ID = cb.getC_AcctSchema().getC_Currency_ID();
			
			stmt.setTimestamp(1, cb.getDateAcct());
			stmt.setInt(2, cb.getC_AcctSchema_ID());
			stmt.setInt(3, currencyAS_ID);
			stmt.setInt(4, cb.getAD_Org_ID());
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				String acctType = rs.getString(1);
				int Account_ID = rs.getInt(2);
				int Currency_ID = rs.getInt(4);
				MCurrency curr = MCurrency.get(getCtx(), Currency_ID);
				BigDecimal acctBalance = rs.getBigDecimal(5);
				acctBalance.setScale(curr.getStdPrecision(), RoundingMode.HALF_UP);
				BigDecimal oriBalance = rs.getBigDecimal(6);
				oriBalance.setScale(curr.getStdPrecision(), RoundingMode.HALF_UP);
				
				BigDecimal rate = 
						MConversionRate.getRate(
								Currency_ID,
								currencyAS_ID,
								cb.getDateAcct(),
								cb.getC_ConversionType_ID(),
								cb.getAD_Client_ID(),
								cb.getAD_Org_ID());
				if (rate == null || rate.signum() <= 0)
					throw new AdempiereException("You have not set currency rate from "
							+ MCurrency.getISO_Code(getCtx(), Currency_ID) + " to "
							+ MCurrency.getISO_Code(getCtx(), currencyAS_ID) + " of Conversion Type "
							+ cb.getC_ConversionType().getName() + " for date " + cb.getDateAcct());
				
				rate = rate.setScale(curr.getStdPrecision(), RoundingMode.HALF_UP);
				BigDecimal eomBalance = oriBalance.multiply(rate);
				eomBalance = eomBalance.setScale(curr.getStdPrecision(), RoundingMode.HALF_UP);
				BigDecimal gainLoss = eomBalance.subtract(acctBalance);
				gainLoss = gainLoss.setScale(curr.getStdPrecision(), RoundingMode.HALF_UP);
				
				MUNSCurrsBalanceLine cbl = new MUNSCurrsBalanceLine(getCtx(), 0, get_TrxName());
				cbl.setAD_Org_ID(cb.getAD_Org_ID());
				cbl.setUNS_CurrsBalance_ID(cb.get_ID());
				cbl.setC_ElementValue_ID(Account_ID);
				cbl.setC_Currency_ID(Currency_ID);
				cbl.setAccountType(acctType);
				cbl.setAccountedBalance(acctBalance);
				cbl.setOriginalBalance(oriBalance);
				cbl.setCurrencyRate(rate);
				cbl.setEOMBalance(eomBalance);
				cbl.setExcRateGainLoss(gainLoss);
				if (!cbl.save())
					throw new AdempiereException("Failed when saving a line.");
				count++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return e.getMessage();
		}
		finally {
			DB.close(stmt);
			DB.close(rs);
			stmt = null;
			rs = null;
		}
		
		//m_processMonitor.statusUpdate(count + "lines created.");
		
		addLog(count + " lines created.");
		
		return null;
	}
}