/**
 * 
 */
package com.untaerp.model.process;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MElementValue;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSFinanceModelFactory;
import com.uns.base.model.Query;
import com.untaerp.model.MUNSEOMPLRecon;
import com.untaerp.model.MUNSEOMPLReconLine;
import com.untaerp.model.X_UNS_PLRecon_Orgs_Conf;


/**
 * @author AzHaidar, BurhaniAdam
 * @see www.untasoft.com
 */
public class CalculateEOMProfitLoss extends SvrProcess 
{
	private boolean DeleteOld = false;
	
	//private IProcessUI m_processMonitor = Env.getProcessUI(getCtx());
	
	/**
	 * 
	 */
	public CalculateEOMProfitLoss() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] para = getParameter();
		
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOld"))
				DeleteOld = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		//m_processMonitor = Env.getProcessUI(getCtx());
		
		MUNSEOMPLRecon plRecon = new MUNSEOMPLRecon(getCtx(), getRecord_ID(), get_TrxName());
		
		//m_processMonitor.statusUpdate("Deleting previous loaded lines.");
		boolean reload = false;
		
		if (DeleteOld)
		{
			String sql = "DELETE FROM UNS_EOM_PLRecon_Line"
					+ " WHERE UNS_EOM_PLRecon_ID=" + getRecord_ID();
			
			int count = DB.executeUpdate(sql, get_TrxName());
			
			addLog("Previous lines deleted #" + count);
			
			reload = true;
		}
		else {
			int count = DB.getSQLValueEx(get_TrxName(), 
					"Select COUNT(*) FROM UNS_EOM_PLRecon_Line WHERE UNS_EOM_PLRecon_ID=" + getRecord_ID());
			
			reload = (count < 1);
		}
		
		if (reload)
		{
			List<X_UNS_PLRecon_Orgs_Conf> orgsConfList =
					Query.get(getCtx(), UNSFinanceModelFactory.EXTENSION_ID, 
							X_UNS_PLRecon_Orgs_Conf.Table_Name, 
							"UNS_PLRecon_Conf_ID=" + plRecon.getUNS_PLRecon_Conf_ID(), 
							get_TrxName())
					.list();
			
			boolean isMainOrgCreated = false;
			
			for (X_UNS_PLRecon_Orgs_Conf orgConf : orgsConfList)
			{
				MUNSEOMPLReconLine rl = new MUNSEOMPLReconLine(getCtx(), 0, get_TrxName());
				rl.setUNS_EOM_PLRecon_ID(getRecord_ID());
				rl.setAD_OrgTrx_ID(orgConf.getAD_OrgTrx_ID());
				rl.setPortion(orgConf.getPortion());
				
				if (!rl.save())
					throw new AdempiereException("Failed while saving Reconciliation Lines.");
				
				if (rl.getAD_OrgTrx_ID() == plRecon.getAD_Org_ID()) {
					isMainOrgCreated = true;
				}
			}
			
			if (!isMainOrgCreated)
			{
				MUNSEOMPLReconLine rl = new MUNSEOMPLReconLine(getCtx(), 0, get_TrxName());
				rl.setUNS_EOM_PLRecon_ID(getRecord_ID());
				rl.setAD_OrgTrx_ID(plRecon.getAD_Org_ID());
				rl.setPortion(Env.ONEHUNDRED);
				
				if (!rl.save())
					throw new AdempiereException("Failed while saving Reconciliation Lines.");
			}
		}
		
		List<MUNSEOMPLReconLine> rlList = Query.get(getCtx(), 
				UNSFinanceModelFactory.EXTENSION_ID, 
				MUNSEOMPLReconLine.Table_Name, 
				"UNS_EOM_PLRecon_ID=" + plRecon.get_ID(), 
				get_TrxName())
				.list();
		
		for (MUNSEOMPLReconLine reconLine : rlList)
		{
			String factSQL = "SELECT SUM(AmtAcctCr-AmtAcctDr) FROM Fact_Acct f"
					+ " INNER JOIN C_ElementValue ev ON ev.C_ElementValue_ID=f.Account_ID"
					+ " INNER JOIN C_Period p ON p.C_Period_ID=?"
					+ " WHERE ev.AccountType=? AND f.DateAcct BETWEEN p.StartDate AND p.EndDate"
					+ "		AND f.AD_Org_ID=? AND f.C_AcctSchema_ID=?";
			
			BigDecimal revenues = DB.getSQLValueBDEx(get_TrxName(), factSQL, 
					plRecon.getC_Period_ID(),
					MElementValue.ACCOUNTTYPE_Revenue,
					reconLine.getAD_OrgTrx_ID(),
					plRecon.getC_AcctSchema_ID());
			if(revenues == null)
				revenues = Env.ZERO;
			
			factSQL = "SELECT SUM(AmtAcctDr-AmtAcctCr) FROM Fact_Acct f"
					+ " INNER JOIN C_ElementValue ev ON ev.C_ElementValue_ID=f.Account_ID"
					+ " INNER JOIN C_Period p ON p.C_Period_ID=?"
					+ " WHERE ev.AccountType=? AND f.DateAcct BETWEEN p.StartDate AND p.EndDate"
					+ "		AND f.AD_Org_ID=? AND f.C_AcctSchema_ID=?";
			
			BigDecimal expenses = DB.getSQLValueBDEx(get_TrxName(), factSQL, 
					plRecon.getC_Period_ID(),
					MElementValue.ACCOUNTTYPE_Expense,
					reconLine.getAD_OrgTrx_ID(),
					plRecon.getC_AcctSchema_ID());
			if(expenses == null)
				expenses = Env.ZERO;
			
			reconLine.setTotalRevenues(revenues);
			reconLine.setTotalExpenses(expenses);
			
			if (!reconLine.save())
				throw new AdempiereException("Failed while saving Revenues/Expenses calculations.");
		}
		
		//m_processMonitor.statusUpdate(count + "lines created.");
		
		addLog(rlList.size() + " P/L lines calculated.");
		
		return null;
	}
}