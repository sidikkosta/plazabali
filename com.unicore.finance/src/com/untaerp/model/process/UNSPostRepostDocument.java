/**
 * 
 */
package com.untaerp.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.util.IProcessUI;
import org.compiere.acct.Doc;
import org.compiere.acct.DocManager;
import org.compiere.cm.StringUtil;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MProduct;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Trx;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSFinanceModelFactory;
import com.uns.base.model.Query;
import com.untaerp.model.MUNSRepostDocLine;
import com.untaerp.model.X_UNS_RepostDoc;


/**
 * @author root
 *
 */
public class UNSPostRepostDocument extends SvrProcess {

	private X_UNS_RepostDoc m_repostHeader = null;
	
	private MAcctSchema[] m_ass = null;
	private String m_resultLog = "";
	private IProcessUI m_processMonitor;
	private final boolean m_isPostFromReposting = true;
//	private Hashtable<String, StringBuilder> m_DocumentNoMap =
//			new Hashtable<String, StringBuilder>();
	
	/**
	 * 
	 */
	public UNSPostRepostDocument() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		int m_acctSchema_ID = 0;
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				continue;
			if(param.getParameterName().equals("DateFrom"))
				;//m_dateFrom = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("DateTo"))
				;//m_dateTo = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("IsForce"))
				;//m_force = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("C_AcctSchema_ID"))
				m_acctSchema_ID = param.getParameterAsInt();
			else
				log.log(Level.WARNING, "UNKNOWN PARAMETER " .concat(
						param.getParameterName()));
		}
		
		if(m_acctSchema_ID > 0)
			m_ass = new MAcctSchema[] {new MAcctSchema(getCtx(),
					m_acctSchema_ID, get_TrxName())};
		else
			m_ass = MAcctSchema.getClientAcctSchema(getCtx(), 
					getAD_Client_ID());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		m_processMonitor = Env.getProcessUI(getCtx());
		
		m_repostHeader = new X_UNS_RepostDoc(getCtx(), getRecord_ID(), get_TrxName());
		
		if(m_repostHeader.get_ID() <= 0)
		{
			log.severe("The repost-document header is null");
			return "Error: No Repost-Doc header record found.";
		}
		
		// clear all notice first.
		DB.executeUpdate("UPDATE UNS_RepostDoc_Line SET Notice=NULL WHERE UNS_RepostDoc_ID=" 
						 + getRecord_ID(), get_TrxName());
		
		List<MUNSRepostDocLine> docs = 
				Query.get(getCtx(), UNSFinanceModelFactory.EXTENSION_ID, MUNSRepostDocLine.Table_Name, 
						"UNS_RepostDoc_ID="+m_repostHeader.get_ID(), get_TrxName())
				.setOnlyActiveRecords(true)
				.setOrderBy(MUNSRepostDocLine.COLUMNNAME_SeqNo)
				.list();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(m_repostHeader.getDateTo().getTime());
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(
				Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getActualMaximum(
				Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getActualMaximum(
				Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(
				Calendar.MILLISECOND));
		
		m_repostHeader.setDateTo(new Timestamp(calendar.getTimeInMillis()));
		
		postSession(docs, m_repostHeader.getDateFrom());
		
		int errorCount = 0;
		
		for(MUNSRepostDocLine doc : docs)
		{
			if (!Util.isEmpty(doc.getNotice(), true))
			{
				int count = StringUtil.count(doc.getNotice(), ",") + 1;
				errorCount += count;
				doc.setNotice(doc.getNotice() + "\n" + " (" + count + " errors.)");
			}
			doc.saveEx();
		}
		
		if (errorCount == 0)  {
			m_resultLog = "Completed successfully.";
		}
		else {
			m_resultLog = "Posting Error documents: ";
		}
		
		m_resultLog += errorCount + " errors.";
		log.fine(m_resultLog);
		
		return m_resultLog;
	}
	
	/**
	 * 
	 * @param helpers
	 * @param date
	 * @throws InterruptedException 
	 */
	private void postSession(List<MUNSRepostDocLine> docs, Timestamp date) 
			throws InterruptedException
	{
		if(date.compareTo(m_repostHeader.getDateTo()) > 0)
			return;
		long sleepTimes = 1;//sleepTimes();
		if (sleepTimes > 0)
		{
			long timemilis = System.currentTimeMillis();
			timemilis = timemilis + sleepTimes;
			Timestamp endWait = new Timestamp(timemilis);
			String msg = "Process will be hold until " 
					+ endWait.toString() + " Last Processed date " 
					+ date.toString();
			
			log.log(Level.INFO, msg);
			m_processMonitor.statusUpdate(msg);
			
			Thread.sleep(sleepTimes);
		}
		
		for(int i=0; i< docs.size(); i++)
		{
			String errMsg = doPost(docs.get(i), date);
			if(!Util.isEmpty(errMsg, true))
			{
				errMsg = errMsg.concat(" \n Last processed date ") + date;
				log.severe(errMsg);
			}
		}
		
		date = TimeUtil.addDays(date, 1);
		postSession(docs, date);
	}
	
	/**
	 * Post Document
	 * @param helper
	 * @param date
	 */
	public String doPost(MUNSRepostDocLine doc, Timestamp date)
	{
		StringBuilder sb = new StringBuilder("SELECT ")
		.append(Util.isEmpty(doc.getSelectClause(), true)? "*" : new StringBuilder(doc.getSelectClause()));
		sb.append(" FROM ").append(doc.getTableName())
		.append(" WHERE IsActive='Y'")
		.append(" AND TRUNC(CAST(").append(doc.getDateColumn())
		.append(" AS DATE)) = TRUNC(CAST('").append(date).append("' AS DATE))");
		
		if (!doc.getTableName().equals("M_MatchInv"))
			sb.append(" AND DocStatus IN ('CO', 'CL', 'RE', 'VO')");
		
		String msg = "Date: " + date + ": Reposting " + doc.getName();
		m_processMonitor.statusUpdate(msg);
		
		if (doc.getC_DocType_ID() > 0) {
			sb.append(" AND C_DocType_ID=").append(doc.getC_DocType_ID());
		}
		
		if (!doc.isForce()) {
			sb.append(" AND Posted!='Y'");
		}
		else {
			getCtx().setProperty("ForceReposting", "Y");
		}
		
		if(!Util.isEmpty(doc.getWhereClause(), true))
		{
			sb.append(" AND ").append(doc.getWhereClause());
		}
		
		sb.append(" ORDER BY DocumentNo ");//MovementDate");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		String trxName = Trx.createTrxName("POST");
		Trx myTrx = Trx.get(trxName, true);
		String errMsg = null;
		String prevMsg = "";
		
		try
		{
			
			st = DB.prepareStatement(sb.toString(), trxName);
			rs = st.executeQuery();
			while (rs.next())
			{
				
				String posted = rs.getString("Posted");
				boolean repost = !posted.equals("N");
				
				//StringBuilder docNoList = m_DocumentNoMap.get(doc.getName());
				String docNo = rs.getString("DocumentNo");
				
				msg = prevMsg + "Date: " + date + ": Reposting " + doc.getName() + ":[" + docNo + "]";
				m_processMonitor.statusUpdate(msg);

				//posting document
				errMsg = DocManager.postDocument(m_ass, doc.getAD_Table_ID(),
						rs, doc.isForce(), repost, m_isPostFromReposting, null);
				
				
				if(!Util.isEmpty(errMsg, true))
				{	
					if (m_repostHeader.isResolveCost())
					{
						int Reversal_ID = doc.getTableName().equals("UNS_POSTrx")?
								rs.getInt("Void_ID") : rs.getInt("Reversal_ID");
						String desc = rs.getString("Description");
						int Record_ID = rs.getInt(doc.getTableName() + "_ID");
						int DocType_ID = doc.getC_DocType_ID() > 0? 
								doc.getC_DocType_ID() : rs.getInt("C_DocType_ID");
						
						errMsg = analyzeCost(doc, date, Record_ID, docNo, Reversal_ID, desc, DocType_ID);
						
						if(Util.isEmpty(errMsg, true))
							errMsg = DocManager.postDocument(
									m_ass, doc.getAD_Table_ID(), rs, doc.isForce(), repost, m_isPostFromReposting, null);
					}
					
					if(!Util.isEmpty(errMsg, true))
					{
						prevMsg = "(Error). ";
						if (Util.isEmpty(doc.getNotice(), true)) {
							//docNoList = new StringBuilder(docNoNotPosted);
							//m_DocumentNoMap.put(doc.getName(), docNoList);
							doc.setNotice("Posting Error:\n " + docNo);
						}
						else {
							//docNoList.append(", ").append(docNoNotPosted);
							doc.setNotice(doc.getNotice() + ", " + docNo);
						}
						m_resultLog = m_resultLog.concat(errMsg).concat("\n ********************** \n");
					}
				}
				
				if (Util.isEmpty(errMsg))
					prevMsg = "(Succeed). ";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
			try
			{
				myTrx.commit();
				myTrx.close();
				myTrx = null;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		doc.saveEx();
		
		if (doc.isForce())
			getCtx().remove("ForceReposting");
		
		return errMsg;
	}
	
	String analyzeCost(MUNSRepostDocLine doc, Timestamp date, 
			int Record_ID, String DocNo, int Reversal_ID, String desc, int DocType_ID)
	{
		String errMsg = null;
		
		boolean isReversal = Reversal_ID > 0;
		if (doc.getTableName().equals("UNS_POSTrx"))
			isReversal = DocNo.indexOf("Rev") > -1;
		else if(!Util.isEmpty(desc, true))
			isReversal = isReversal && (desc.indexOf("->")>=0);

		if (doc.getTableName().equals("M_Movement")) //Inventory Move.
		{
			errMsg = analyzeMovementCost(date, isReversal, Record_ID, get_TrxName());
		}
		else if (!isReversal && doc.DocTypeName.equals("Internal Use Inventory")) 
		{//Internal Use.
			errMsg = analyzeInternalUseCost(date, isReversal, Record_ID, get_TrxName());
		}
		else if (doc.DocTypeName.equals("Physical Inventory"))
		{//physical inventory
			analyzePhysicalInventoryCost(date, isReversal, Record_ID, get_TrxName());
		}
		else if (!isReversal && doc.getTableName().equals("M_InOut"))
		{//Customer Shipment,return to vendor.
			MDocType dt = MDocType.get(getCtx(), DocType_ID);
			
			if (dt.getDocBaseType().equals(Doc.DOCTYPE_MatShipment))
				errMsg = analyzeShipmentCost(date, isReversal, Record_ID, get_TrxName());
		}
		else if (doc.getTableName().equals("M_MatchInv"))
		{
			String sql = "SELECT MovementType FROM M_InOut WHERE M_InOut_ID = "
					+ "(SELECT M_InOut_ID FROM M_InOutLine iol INNER JOIN M_MatchInv mi "
					+ "ON iol.M_InOutLine_ID=mi.M_InOutLine_ID WHERE M_MatchInv_ID="+ Record_ID + ")";
			String movementType = DB.getSQLValueStringEx(get_TrxName(), sql);
			
			if ((!isReversal && movementType.equals(MInOut.MOVEMENTTYPE_VendorReturns))
					|| (isReversal && movementType.equals(MInOut.MOVEMENTTYPE_VendorReceipts)))
			{
				errMsg = analyzeMatchInvCost(date, isReversal, Record_ID, get_TrxName());
			}
		}
		else if (doc.getTableName().equals("UNS_POSTrx"))
		{
			MDocType dt = MDocType.get(getCtx(), DocType_ID);
			
			if ((!isReversal && dt.getDocBaseType().equals("POS")) 
					|| (isReversal && dt.getDocBaseType().equals("SPR")))
				errMsg = analyzePOSTrxCost(date, isReversal, Record_ID, get_TrxName());
		}
		
		return errMsg;
	}
	
	/** Analyze production cost.*/
	String analyzeProductionCost(Timestamp dateDoc, boolean isReversal, 
			int Production_ID, String trxName)
	{
		String isEndProduct = isReversal ? "Y" : "N";
		StringBuilder sqlBuilder = new StringBuilder("SELECT AD_Org_ID, ")
		.append("M_Product_ID, SUM (MovementQty) FROM UNS_Production_Detail ")
		.append(" WHERE UNS_Production_ID=").append(Production_ID )
		.append(" AND IsActive='Y' AND IsEndProduct='").append(isEndProduct)
		.append("' GROUP BY M_Product_ID, AD_Org_ID");
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze Inventory Movement cost.*/
	String analyzeMovementCost(Timestamp dateDoc, boolean isReversal, 
			int Movement_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT l.AD_Org_ID, ")
		.append("ml.M_Product_ID, SUM(ml.MovementQty) FROM M_MovementLine ml ")
		.append("INNER JOIN M_Locator l ON ");
		
		if (isReversal)
		{
			sqlBuilder.append("ml.M_LocatorTo_ID=l.M_Locator_ID ");
		}
		else
		{
			sqlBuilder.append("ml.M_Locator_ID=l.M_Locator_ID ");
		}
		
		sqlBuilder.append("WHERE ml.M_Movement_ID= ").append(Movement_ID)
		.append("AND ml.IsActive='Y' GROUP BY M_Product_ID, l.AD_Org_ID ");
		
		String sql = sqlBuilder.toString();
		
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	String analyzeMatchInvCost (Timestamp dateDoc, boolean isReversal, 
			int MatchInv_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT AD_Org_ID, ")
		.append("M_Product_ID, Qty FROM M_MatchInv WHERE M_MatchInv_ID = ")
		.append(MatchInv_ID);
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	String analyzePOSTrxCost (Timestamp dateDoc, boolean isReversal, 
			int POSTrx_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT AD_Org_ID, ")
		.append("M_Product_ID, SUM(QtyOrdered) FROM UNS_POSTrxLine ")
		.append("WHERE UNS_POSTrx_ID = ").append(POSTrx_ID)
		.append(" GROUP BY AD_Org_ID, M_Product_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze Inventory Movement cost.*/
	String analyzeInternalUseCost(Timestamp dateDoc, boolean isReversal, 
			int Inventory_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT loc.AD_Org_ID, ")
		.append("M_Product_ID,  SUM(QtyInternalUse) FROM M_InventoryLine il ")
		.append("INNER JOIN M_Locator loc ON il.M_Locator_ID=loc.M_Locator_ID ")
		.append("WHERE M_Inventory_ID= ").append(Inventory_ID )
		.append("AND IsActive='Y' GROUP BY M_Product_ID, loc.AD_Org_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	String analyzePhysicalInventoryCost(Timestamp dateDoc, boolean isReversal, 
			int Inventory_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT loc.AD_Org_ID, ")
		.append("M_Product_ID,  SUM(qtyCount-QtyBook) FROM M_InventoryLine il ")
		.append("INNER JOIN M_Locator loc ON il.M_Locator_ID=loc.M_Locator_ID ")
		.append("WHERE M_Inventory_ID= ").append(Inventory_ID )
		.append(" AND il.IsActive='Y' GROUP BY M_Product_ID, loc.AD_Org_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze Customer Shipment cost.*/
	String analyzeShipmentCost(Timestamp dateDoc, boolean isReversal, 
			int InOut_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT ml.AD_Org_ID, ")
		.append("ml.M_Product_ID, SUM(ml.MovementQty) FROM M_InOutLine ml ")
		.append("WHERE ml.M_InOut_ID=").append(InOut_ID)
		.append(" AND ml.IsActive='Y' GROUP BY M_Product_ID, ml.AD_Org_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze cost using the given query */
	String analyzeCost(Timestamp dateDoc, String sql, String trxName)
	{
		String errMsg = null;
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, trxName);
			rs = st.executeQuery();
			while (rs.next())
			{
				int AD_Org_ID = rs.getInt(1);
				int M_Product_ID = rs.getInt(2);
				BigDecimal expectedQty = rs.getBigDecimal(3).abs();
				errMsg = analyzeCost(dateDoc, AD_Org_ID, 
						M_Product_ID, expectedQty, trxName);
				
				if (errMsg != null)
					break;
			}
			//DB.commit(true, trxName);

			if(!DB.commit(false, trxName))
				errMsg = "Cannot committing transaction after analyzing cost.";
		}
		catch (Exception e) {
			e.printStackTrace();
			errMsg = e.getMessage();
		}
		finally {
			DB.close(rs, st);
		}
		
		return errMsg;
	}
	
	/**
	 * 
	 * @param AD_Org_ID
	 * @param M_Product_ID
	 * @param expectedQty
	 * @param trxName
	 * @return
	 */
	String analyzeCost(Timestamp dateDoc, int AD_Org_ID, int M_Product_ID, BigDecimal expectedQty, String trxName)
	{
		String errMsg = null;
		
		String sql = "SELECT CurrentQty, CurrentCostPrice, CumulatedQty, CumulatedAmt FROM M_Cost "
				+ " WHERE AD_Org_ID=? AND M_Product_ID=? AND M_CostElement_ID=?";
		PreparedStatement st = null;
		ResultSet rs = null;

		try
		{
			st = DB.prepareStatement(sql, null);
			st.setInt(1, AD_Org_ID);
			st.setInt(2, M_Product_ID);
			st.setInt(3, m_repostHeader.getM_CostElement_ID());
			rs = st.executeQuery();
			boolean found = false;
			
			while (rs.next())
			{
				found = true;
				BigDecimal currentQty = rs.getBigDecimal(1);
				//BigDecimal currentCost = rs.getBigDecimal(2);
				//BigDecimal cumulatedQty = rs.getBigDecimal(3);
				//BigDecimal cumulatedAmt = rs.getBigDecimal(4);
				
//				if (currentCost.signum() <= 0) {
//					errMsg = "Current Cost is zero for product of " + MProduct.get(getCtx(), M_Product_ID);
//					break;
//				}
				
				if (currentQty.compareTo(expectedQty) >= 0) {
					break;
				}
				
				//BigDecimal diffQty = expectedQty.subtract(currentQty);
				//BigDecimal totalDiffAmt = diffQty.multiply(currentCost);
				//BigDecimal newCumulatedQty = cumulatedQty.add(diffQty);
				//BigDecimal newCumulatedAmt = cumulatedAmt.add(totalDiffAmt);
				
				String updateCostSql = "UPDATE M_Cost SET CurrentQty=?, Description='**ManuallyUpdated**'" //, CumulatedAmt=?, CumulatedQty=? "
						+ " WHERE AD_Org_ID=? AND M_Product_ID=?";
				int count = DB.executeUpdateEx(updateCostSql, 
						new Object[]{expectedQty, AD_Org_ID, M_Product_ID}, trxName);
						//new Object[]{expectedQty, newCumulatedAmt, newCumulatedQty, AD_Org_ID, M_Product_ID}, trxName);
				if (count <= 0) {
					errMsg = "Failed when updating cost for product of " + MProduct.get(getCtx(), M_Product_ID);
					break;
				}
				/**
				else {
					MCost cost = MCost.get(
							MProduct.get(getCtx(), M_Product_ID), 0, this.m_ass[0], 
								AD_Org_ID, m_repostHeader.getM_CostElement_ID(), trxName);
					DB.getDatabase().forUpdate(cost, 120);
					System.out.println("locked");
				}
				**/
			}
			
			if (!found) {
				//throw new AdempiereException("Could not find cost for product " + );
				return "Could not find cost for product " + MProduct.get(getCtx(), M_Product_ID);
				//errMsg = "Cannot find MCost record for product of " + MProduct.get(getCtx(), M_Product_ID);
				
				/**
				BigDecimal currentCost = getCostOf(M_Product_ID, dateDoc, trxName);
				
				if (currentCost == null)
					errMsg = "Cannot find MCost record for product of " + MProduct.get(getCtx(), M_Product_ID);
				else {
					MCost cost = new MCost(MProduct.get(getCtx(), M_Product_ID), 0, m_ass[0], AD_Org_ID, 1000014);
					cost.setCurrentQty(expectedQty);
					cost.setCurrentCostPrice(currentCost);
					
					BigDecimal newCumulatedAmt = currentCost.multiply(expectedQty);
					cost.setCumulatedQty(expectedQty);
					cost.setCumulatedAmt(newCumulatedAmt);
					cost.saveEx();
				}
				**/
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errMsg = e.getMessage();
		}
		finally {
			DB.close(rs, st);
		}
		
		return errMsg;
	}
	
	/**
	 * 
	 * @param M_Product_ID
	 * @param dateDoc
	 * @param trxName
	 * @return
	 */
	BigDecimal getCostOf(int M_Product_ID, Timestamp dateDoc, String trxName)
	{
		int M_PriceList_ID_1 = 1000101; // Harga Pembelian
		int M_PriceList_ID_2 = 1000099; // Harga Toko.
		int M_PriceList_ID_3 = 1000104; // PL Auto Initial Purposes Only.
		int M_PriceList_ID_4 = 1000100; // Harga Konsumen.
		
		String sql = 
				"SELECT pp.PriceList FROM M_ProductPrice pp "
				+ " INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
				+ " INNER JOIN M_PriceList pl ON plv.M_PriceList_ID=pl.M_PriceList_ID "
				+ " WHERE pp.M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
				+ " ORDER BY plv.ValidFrom DESC";
		
		BigDecimal priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_1, dateDoc);				
		BigDecimal percentage = Env.ONE;
		
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_2, dateDoc);
			percentage = BigDecimal.valueOf(0.75);
		}
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_3, dateDoc);
			percentage = BigDecimal.valueOf(0.65);
		}
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_4, dateDoc);
			percentage = BigDecimal.valueOf(0.55);
		}
		
		if (priceList == null || priceList.signum() <= 0)
			return null;
		
		BigDecimal cost = priceList.multiply(percentage);

		return cost;
	}
	
	long sleepTimes ()
	{
		long sleepTimes = 0;
		int start = 6;
		int end = 17;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		int curHour = cal.get(Calendar.HOUR_OF_DAY);
		int curMinute = cal.get(Calendar.MINUTE);
		int day = cal.get(Calendar.DAY_OF_WEEK);
		
		if (day != 1 && curHour >= start && curHour < end)
		{
			sleepTimes = end - curHour;
			curMinute = curMinute * 60 * 1000;
			sleepTimes = sleepTimes * 60 *60 * 1000;
			sleepTimes = sleepTimes - curMinute;
		}
		
		return sleepTimes;
	}
}

class Helper
{
	String MODULE_NAME = null;
	String TABLE_NAME = null;
	String DATE_COLUMN = null;
	String WHERECLAUSE = null;
	String SELECT 	   = null;
	int TABLE_ID = 0;

	/**
	 * 
	 * @param tableName
	 * @param tableID
	 * @param dateColumn
	 * @param whereClause
	 */
	public Helper(String moduleName, String tableName,int tableID, 
			String dateColumn, String select, String whereClause)
	{
		this.MODULE_NAME = moduleName;
		this.TABLE_NAME = tableName; 
		this.DATE_COLUMN = dateColumn;
		this.SELECT = select;
		this.WHERECLAUSE = whereClause;
		this.TABLE_ID = tableID;
	}
}