/**
 * 
 */
package com.untaerp.model.process;

import java.util.Properties;

//import com.uns.util.MessageBox;

/**
 * @author AzHaidar, BurhaniAdam
 * @see www.untasoft.com
 */
public class ResetBankStatementBalances extends com.uns.model.process.ResetBankStatementBalances
{
	private Properties 		m_context = null;
	private String			m_trxName = null;
	
	/**
	 * 
	 */
	public ResetBankStatementBalances() {
	}
	
	/**
	 * 
	 */
	public ResetBankStatementBalances(Properties ctx, int bankStatement_ID, String trxName) 
	{	
		//m_bankStatement_ID = bankStatement_ID;
		super();
		setBankStatement_ID(bankStatement_ID);
		m_context = ctx;
		m_trxName = trxName;
	}
	
	public Properties getCtx()
	{
		if (m_context == null)
			m_context = super.getCtx();
		return m_context;
	}
	
	public String get_TrxName()
	{
		if (m_trxName == null)
			m_trxName = super.get_TrxName();
		return m_trxName;
	}
	
	protected void prepare() 
	{
		super.prepare();
	}
	
	@Override
	protected String doIt() throws Exception 
	{
		return super.doIt();
	}
}