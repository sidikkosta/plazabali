/**
 * 
 */
package com.untaerp.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBankAccount;
import org.compiere.model.MBankStatement;
import org.compiere.model.MBankStatementLine;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;


/**
 * @author AzHaidar
 * @see www.untasoft.com
 */
public class ResetPaymentReconciliation extends SvrProcess 
{
	private int				m_bankStatement_ID = 0;
	private int				m_bankAccount_ID;
	private boolean			m_isCreateStatement;
	
	/**
	 * 
	 */
	public ResetPaymentReconciliation() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("C_BankAccount_ID"))
				m_bankAccount_ID = param.getParameterAsInt();
			else if(param.getParameterName().equals("IsCreateStatement"))
				m_isCreateStatement = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("C_BankStatement_ID"))
				m_bankStatement_ID = param.getParameterAsInt();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MBankStatement bs = null;
		if (!m_isCreateStatement && m_bankStatement_ID > 0)
			bs = new MBankStatement(getCtx(), m_bankStatement_ID, get_TrxName());
		
		MBankAccount ba = new MBankAccount(getCtx(), m_bankAccount_ID, get_TrxName());
		
		String sql = "SELECT C_Payment_ID, DocumentNo, IsReceipt, PayAmt, " // 1..4
				+ " DateTrx, DateAcct, C_BPartner_ID, Description " // 5..8
				+ "FROM C_Payment "
				+ "WHERE DocStatus IN ('CO', 'CL', 'RE') AND C_BankAccount_ID=" + m_bankAccount_ID
				+ "ORDER BY DateAcct, DocumentNo";
		
		PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
		ResultSet rs = null;
		
		int countAll = 0;
		int countNotReconciled = 0;
		
		try {
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				countAll++;
				
				int C_Payment_ID = rs.getInt(1);
				String docNo = rs.getString(2);
				boolean isReceipt = rs.getString(3).equals("Y") ? true : false;
				BigDecimal payAmt = rs.getBigDecimal(4);
				Timestamp dateTrx = rs.getTimestamp(5);
				Timestamp dateAcct = rs.getTimestamp(6);
				int C_BPartner_ID = rs.getInt(7);
				String desc = rs.getString(8);
				
				List<MBankStatementLine> bsls = 
						new Query(getCtx(), 
								MBankStatementLine.Table_Name, 
								"C_Payment_ID=" + C_Payment_ID, 
								get_TrxName())
						.list();
				
				// Delete all duplicate allocated payment.
				for (int i=1; i < bsls.size(); i++) {
					MBankStatementLine bsl = bsls.get(i);
					bsl.deleteEx(true);
				}
				
				MBankStatementLine bsl = null;
				if (bsls.size() > 0) {
					bsl = bsls.get(0);
					if (bsl.getC_Charge_ID() == 0 && bsl.getChargeAmt().signum() != 0)
						bsl.setChargeAmt(Env.ZERO);
				}
				else {
					countNotReconciled++;
					if (bs == null) {
						bs = new MBankStatement(ba, false);
						bs.setForceCreation(true);
						bs.setName("Automatic statement to auto reconciled payments.");
						bs.saveEx();
					}
					
					bsl = new MBankStatementLine(bs);
					bsl.setDescription(Util.isEmpty(desc, true)? "Payment Docno: " + docNo : desc);
					bsl.setC_Currency_ID(ba.getC_Currency_ID());
					bsl.setProcessed(true);
				}
				bsl.setTransactionType(isReceipt? MBankStatementLine.TRANSACTIONTYPE_ARTransaction 
						: MBankStatementLine.TRANSACTIONTYPE_APTransaction);
				bsl.setC_Payment_ID(C_Payment_ID);
				bsl.setC_BPartner_ID(C_BPartner_ID);
				bsl.setStatementLineDate(dateTrx);
				bsl.setDateAcct(dateAcct);
				bsl.setValutaDate(dateAcct);
				bsl.setAmount(payAmt);
				bsl.setStmtAmt(isReceipt? payAmt : payAmt.negate());
				bsl.setTrxAmt(isReceipt? payAmt : payAmt.negate());
				bsl.setInterestAmt(Env.ZERO);
				bsl.saveEx();
			}		
		} catch (SQLException x) {
			x.printStackTrace();
		}
		finally {
			rs.close();
			stmt.close();
		}
		
		if (bs != null)
		{
			bs.setProcessed(true);
			bs.setDocAction(MBankStatement.DOCACTION_Close);
			bs.setDocStatus(MBankStatement.DOCSTATUS_Completed);
			//bs.processIt(MBankStatement.ACTION_Complete);
			bs.saveEx();
			
			MAcctSchema[] mass = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
			
			DocManager.postDocument(mass, MBankStatement.Table_ID, bs.get_ID(), true, true, get_TrxName());			
		}
		
		if (countNotReconciled > 0)
		{
			sql = "SELECT C_BankStatement_ID FROM C_BankStatement "
					+ " WHERE C_BankAccount_ID=" + m_bankAccount_ID
					+ " ORDER BY StatementDate, DocumentNo";
			
			int bankStmtToResetBalance = DB.getSQLValueEx(get_TrxName(), sql);
			ResetBankStatementBalances rbsb = 
					new ResetBankStatementBalances(getCtx(), bankStmtToResetBalance, get_TrxName());
			rbsb.prepare();
			rbsb.doIt();
		}
		
		return countNotReconciled + " newly Payment has been reconciled from total " + countAll + " Payments.";
	} // doIt
	
}