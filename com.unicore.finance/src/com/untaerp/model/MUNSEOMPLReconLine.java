/**
 * 
 */
package com.untaerp.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Harry
 *
 */
public class MUNSEOMPLReconLine extends X_UNS_EOM_PLRecon_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8896020480729664912L;

	/**
	 * @param ctx
	 * @param UNS_EOM_PLRecon_Line_ID
	 * @param trxName
	 */
	public MUNSEOMPLReconLine(Properties ctx, int UNS_EOM_PLRecon_Line_ID,
			String trxName) {
		super(ctx, UNS_EOM_PLRecon_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEOMPLReconLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_TotalExpenses) 
				|| is_ValueChanged(COLUMNNAME_TotalRevenues)
				|| is_ValueChanged(COLUMNNAME_Portion))
		{
			BigDecimal portion = getPortion().divide(Env.ONEHUNDRED, 4, BigDecimal.ROUND_HALF_UP);
			BigDecimal appliedRev = getTotalRevenues().multiply(portion);
			BigDecimal appliedExp = getTotalExpenses().multiply(portion);
			BigDecimal plAmt = appliedRev.signum() != 0 ? appliedRev.subtract(appliedExp) : Env.ZERO;
			setPLAmt(plAmt);
			setAppliedRevenues(appliedRev);
			setAppliedExpenses(appliedExp);
		}
			
		return true;
	}// beforeSave
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if (success)
		{
			String sql = "UPDATE UNS_EOM_PLRecon SET "
					+ " TotalRevenues=(SELECT SUM(AppliedRevenues) FROM UNS_EOM_PLRecon_Line"
					+ "		WHERE UNS_EOM_PLRecon_ID=" + getUNS_EOM_PLRecon_ID() + "), "
					+ "	TotalExpenses=(SELECT SUM(AppliedExpenses) FROM UNS_EOM_PLRecon_Line"
					+ "		WHERE UNS_EOM_PLRecon_ID=" + getUNS_EOM_PLRecon_ID() + ")"
					+ " WHERE UNS_EOM_PLRecon_ID=" + getUNS_EOM_PLRecon_ID();
			DB.executeUpdateEx(sql, get_TrxName());
			
			sql = "UPDATE UNS_EOM_PLRecon SET PLAmt = (TotalRevenues - TotalExpenses)"
					+ " WHERE UNS_EOM_PLRecon_ID=" + getUNS_EOM_PLRecon_ID();
			
			DB.executeUpdateEx(sql, get_TrxName());
		}
		return true;
	}
}