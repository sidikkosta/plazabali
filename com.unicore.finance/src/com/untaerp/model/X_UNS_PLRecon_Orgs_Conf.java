/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.untaerp.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PLRecon_Orgs_Conf
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PLRecon_Orgs_Conf extends PO implements I_UNS_PLRecon_Orgs_Conf, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190131L;

    /** Standard Constructor */
    public X_UNS_PLRecon_Orgs_Conf (Properties ctx, int UNS_PLRecon_Orgs_Conf_ID, String trxName)
    {
      super (ctx, UNS_PLRecon_Orgs_Conf_ID, trxName);
      /** if (UNS_PLRecon_Orgs_Conf_ID == 0)
        {
			setAD_OrgTrx_ID (0);
			setPortion (Env.ZERO);
			setUNS_PLRecon_Conf_ID (0);
			setUNS_PLRecon_Orgs_Conf_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PLRecon_Orgs_Conf (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PLRecon_Orgs_Conf[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Trx Department.
		@param AD_OrgTrx_ID 
		Performing or initiating Department
	  */
	public void setAD_OrgTrx_ID (int AD_OrgTrx_ID)
	{
		if (AD_OrgTrx_ID < 1) 
			set_Value (COLUMNNAME_AD_OrgTrx_ID, null);
		else 
			set_Value (COLUMNNAME_AD_OrgTrx_ID, Integer.valueOf(AD_OrgTrx_ID));
	}

	/** Get Trx Department.
		@return Performing or initiating Department
	  */
	public int getAD_OrgTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_OrgTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Portion (%).
		@param Portion 
		The portion (in percentage) of this item
	  */
	public void setPortion (BigDecimal Portion)
	{
		set_Value (COLUMNNAME_Portion, Portion);
	}

	/** Get Portion (%).
		@return The portion (in percentage) of this item
	  */
	public BigDecimal getPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Portion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.untaerp.model.I_UNS_PLRecon_Conf getUNS_PLRecon_Conf() throws RuntimeException
    {
		return (com.untaerp.model.I_UNS_PLRecon_Conf)MTable.get(getCtx(), com.untaerp.model.I_UNS_PLRecon_Conf.Table_Name)
			.getPO(getUNS_PLRecon_Conf_ID(), get_TrxName());	}

	/** Set Profit/Loss Recon Conf.
		@param UNS_PLRecon_Conf_ID Profit/Loss Recon Conf	  */
	public void setUNS_PLRecon_Conf_ID (int UNS_PLRecon_Conf_ID)
	{
		if (UNS_PLRecon_Conf_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Conf_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Conf_ID, Integer.valueOf(UNS_PLRecon_Conf_ID));
	}

	/** Get Profit/Loss Recon Conf.
		@return Profit/Loss Recon Conf	  */
	public int getUNS_PLRecon_Conf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PLRecon_Conf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Profit/Loss Recon Orgs-portion Conf.
		@param UNS_PLRecon_Orgs_Conf_ID Profit/Loss Recon Orgs-portion Conf	  */
	public void setUNS_PLRecon_Orgs_Conf_ID (int UNS_PLRecon_Orgs_Conf_ID)
	{
		if (UNS_PLRecon_Orgs_Conf_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Orgs_Conf_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Orgs_Conf_ID, Integer.valueOf(UNS_PLRecon_Orgs_Conf_ID));
	}

	/** Get Profit/Loss Recon Orgs-portion Conf.
		@return Profit/Loss Recon Orgs-portion Conf	  */
	public int getUNS_PLRecon_Orgs_Conf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PLRecon_Orgs_Conf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PLRecon_Orgs_Conf_UU.
		@param UNS_PLRecon_Orgs_Conf_UU UNS_PLRecon_Orgs_Conf_UU	  */
	public void setUNS_PLRecon_Orgs_Conf_UU (String UNS_PLRecon_Orgs_Conf_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Orgs_Conf_UU, UNS_PLRecon_Orgs_Conf_UU);
	}

	/** Get UNS_PLRecon_Orgs_Conf_UU.
		@return UNS_PLRecon_Orgs_Conf_UU	  */
	public String getUNS_PLRecon_Orgs_Conf_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PLRecon_Orgs_Conf_UU);
	}
}