/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.untaerp.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_RepostDoc
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_RepostDoc extends PO implements I_UNS_RepostDoc, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171118L;

    /** Standard Constructor */
    public X_UNS_RepostDoc (Properties ctx, int UNS_RepostDoc_ID, String trxName)
    {
      super (ctx, UNS_RepostDoc_ID, trxName);
      /** if (UNS_RepostDoc_ID == 0)
        {
			setDateFrom (new Timestamp( System.currentTimeMillis() ));
			setDateTo (new Timestamp( System.currentTimeMillis() ));
			setM_CostElement_ID (0);
// 1000016
			setName (null);
			setUNS_RepostDoc_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_RepostDoc (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_RepostDoc[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date From.
		@param DateFrom 
		Starting date for a range
	  */
	public void setDateFrom (Timestamp DateFrom)
	{
		set_Value (COLUMNNAME_DateFrom, DateFrom);
	}

	/** Get Date From.
		@return Starting date for a range
	  */
	public Timestamp getDateFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFrom);
	}

	/** Set Date To.
		@param DateTo 
		End date of a date range
	  */
	public void setDateTo (Timestamp DateTo)
	{
		set_Value (COLUMNNAME_DateTo, DateTo);
	}

	/** Get Date To.
		@return End date of a date range
	  */
	public Timestamp getDateTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTo);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_CostElement getM_CostElement() throws RuntimeException
    {
		return (org.compiere.model.I_M_CostElement)MTable.get(getCtx(), org.compiere.model.I_M_CostElement.Table_Name)
			.getPO(getM_CostElement_ID(), get_TrxName());	}

	/** Set Cost Element.
		@param M_CostElement_ID 
		Product Cost Element
	  */
	public void setM_CostElement_ID (int M_CostElement_ID)
	{
		if (M_CostElement_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_CostElement_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_CostElement_ID, Integer.valueOf(M_CostElement_ID));
	}

	/** Get Cost Element.
		@return Product Cost Element
	  */
	public int getM_CostElement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_CostElement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Resolve Cost.
		@param ResolveCost 
		Try to resolve cost if posting was error.
	  */
	public void setResolveCost (boolean ResolveCost)
	{
		set_Value (COLUMNNAME_ResolveCost, Boolean.valueOf(ResolveCost));
	}

	/** Get Resolve Cost.
		@return Try to resolve cost if posting was error.
	  */
	public boolean isResolveCost () 
	{
		Object oo = get_Value(COLUMNNAME_ResolveCost);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Repost Document.
		@param UNS_RepostDoc_ID Repost Document	  */
	public void setUNS_RepostDoc_ID (int UNS_RepostDoc_ID)
	{
		if (UNS_RepostDoc_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_ID, Integer.valueOf(UNS_RepostDoc_ID));
	}

	/** Get Repost Document.
		@return Repost Document	  */
	public int getUNS_RepostDoc_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_RepostDoc_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_RepostDoc_UU.
		@param UNS_RepostDoc_UU UNS_RepostDoc_UU	  */
	public void setUNS_RepostDoc_UU (String UNS_RepostDoc_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_UU, UNS_RepostDoc_UU);
	}

	/** Get UNS_RepostDoc_UU.
		@return UNS_RepostDoc_UU	  */
	public String getUNS_RepostDoc_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_RepostDoc_UU);
	}
}