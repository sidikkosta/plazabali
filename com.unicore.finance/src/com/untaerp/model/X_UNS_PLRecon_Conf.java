/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.untaerp.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_PLRecon_Conf
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PLRecon_Conf extends PO implements I_UNS_PLRecon_Conf, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190130L;

    /** Standard Constructor */
    public X_UNS_PLRecon_Conf (Properties ctx, int UNS_PLRecon_Conf_ID, String trxName)
    {
      super (ctx, UNS_PLRecon_Conf_ID, trxName);
      /** if (UNS_PLRecon_Conf_ID == 0)
        {
			setC_AcctSchema_ID (0);
			setUNS_PLRecon_Conf_ID (0);
			setValidFrom (new Timestamp( System.currentTimeMillis() ));
        } */
    }

    /** Load Constructor */
    public X_UNS_PLRecon_Conf (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PLRecon_Conf[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_AcctSchema getC_AcctSchema() throws RuntimeException
    {
		return (org.compiere.model.I_C_AcctSchema)MTable.get(getCtx(), org.compiere.model.I_C_AcctSchema.Table_Name)
			.getPO(getC_AcctSchema_ID(), get_TrxName());	}

	/** Set Accounting Schema.
		@param C_AcctSchema_ID 
		Rules for accounting
	  */
	public void setC_AcctSchema_ID (int C_AcctSchema_ID)
	{
		if (C_AcctSchema_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_AcctSchema_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_AcctSchema_ID, Integer.valueOf(C_AcctSchema_ID));
	}

	/** Get Accounting Schema.
		@return Rules for accounting
	  */
	public int getC_AcctSchema_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_AcctSchema_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Profit/Loss Recon Conf.
		@param UNS_PLRecon_Conf_ID Profit/Loss Recon Conf	  */
	public void setUNS_PLRecon_Conf_ID (int UNS_PLRecon_Conf_ID)
	{
		if (UNS_PLRecon_Conf_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Conf_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Conf_ID, Integer.valueOf(UNS_PLRecon_Conf_ID));
	}

	/** Get Profit/Loss Recon Conf.
		@return Profit/Loss Recon Conf	  */
	public int getUNS_PLRecon_Conf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PLRecon_Conf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PLRecon_Conf_UU.
		@param UNS_PLRecon_Conf_UU UNS_PLRecon_Conf_UU	  */
	public void setUNS_PLRecon_Conf_UU (String UNS_PLRecon_Conf_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PLRecon_Conf_UU, UNS_PLRecon_Conf_UU);
	}

	/** Get UNS_PLRecon_Conf_UU.
		@return UNS_PLRecon_Conf_UU	  */
	public String getUNS_PLRecon_Conf_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PLRecon_Conf_UU);
	}

	/** Set Valid from.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid from.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}
}