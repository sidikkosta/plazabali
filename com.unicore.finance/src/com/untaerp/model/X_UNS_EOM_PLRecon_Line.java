/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.untaerp.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_EOM_PLRecon_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EOM_PLRecon_Line extends PO implements I_UNS_EOM_PLRecon_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190131L;

    /** Standard Constructor */
    public X_UNS_EOM_PLRecon_Line (Properties ctx, int UNS_EOM_PLRecon_Line_ID, String trxName)
    {
      super (ctx, UNS_EOM_PLRecon_Line_ID, trxName);
      /** if (UNS_EOM_PLRecon_Line_ID == 0)
        {
			setAD_OrgTrx_ID (0);
			setAppliedExpenses (Env.ZERO);
			setAppliedRevenues (Env.ZERO);
			setPLAmt (Env.ZERO);
// 0
			setPortion (Env.ZERO);
			setTotalExpenses (Env.ZERO);
			setTotalRevenues (Env.ZERO);
			setUNS_EOM_PLRecon_ID (0);
			setUNS_EOM_PLRecon_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EOM_PLRecon_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EOM_PLRecon_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Trx Department.
		@param AD_OrgTrx_ID 
		Performing or initiating Department
	  */
	public void setAD_OrgTrx_ID (int AD_OrgTrx_ID)
	{
		if (AD_OrgTrx_ID < 1) 
			set_Value (COLUMNNAME_AD_OrgTrx_ID, null);
		else 
			set_Value (COLUMNNAME_AD_OrgTrx_ID, Integer.valueOf(AD_OrgTrx_ID));
	}

	/** Get Trx Department.
		@return Performing or initiating Department
	  */
	public int getAD_OrgTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_OrgTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Applied Expenses.
		@param AppliedExpenses 
		The expenses applied to the calculation
	  */
	public void setAppliedExpenses (BigDecimal AppliedExpenses)
	{
		set_Value (COLUMNNAME_AppliedExpenses, AppliedExpenses);
	}

	/** Get Applied Expenses.
		@return The expenses applied to the calculation
	  */
	public BigDecimal getAppliedExpenses () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AppliedExpenses);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Applied Revenues.
		@param AppliedRevenues 
		The revenues applied to the calculation
	  */
	public void setAppliedRevenues (BigDecimal AppliedRevenues)
	{
		set_Value (COLUMNNAME_AppliedRevenues, AppliedRevenues);
	}

	/** Get Applied Revenues.
		@return The revenues applied to the calculation
	  */
	public BigDecimal getAppliedRevenues () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AppliedRevenues);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Profit/Loss Amount.
		@param PLAmt 
		It is a Profit Amount if positive, Loss amount otherwise
	  */
	public void setPLAmt (BigDecimal PLAmt)
	{
		set_Value (COLUMNNAME_PLAmt, PLAmt);
	}

	/** Get Profit/Loss Amount.
		@return It is a Profit Amount if positive, Loss amount otherwise
	  */
	public BigDecimal getPLAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PLAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Portion (%).
		@param Portion 
		The portion (in percentage) of this item
	  */
	public void setPortion (BigDecimal Portion)
	{
		set_Value (COLUMNNAME_Portion, Portion);
	}

	/** Get Portion (%).
		@return The portion (in percentage) of this item
	  */
	public BigDecimal getPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Portion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Expenses.
		@param TotalExpenses 
		Total of the expenses
	  */
	public void setTotalExpenses (BigDecimal TotalExpenses)
	{
		set_Value (COLUMNNAME_TotalExpenses, TotalExpenses);
	}

	/** Get Total Expenses.
		@return Total of the expenses
	  */
	public BigDecimal getTotalExpenses () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalExpenses);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Revenues.
		@param TotalRevenues 
		Total of the revenues
	  */
	public void setTotalRevenues (BigDecimal TotalRevenues)
	{
		set_Value (COLUMNNAME_TotalRevenues, TotalRevenues);
	}

	/** Get Total Revenues.
		@return Total of the revenues
	  */
	public BigDecimal getTotalRevenues () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalRevenues);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.untaerp.model.I_UNS_EOM_PLRecon getUNS_EOM_PLRecon() throws RuntimeException
    {
		return (com.untaerp.model.I_UNS_EOM_PLRecon)MTable.get(getCtx(), com.untaerp.model.I_UNS_EOM_PLRecon.Table_Name)
			.getPO(getUNS_EOM_PLRecon_ID(), get_TrxName());	}

	/** Set EOM Profit/Loss Reconciliation.
		@param UNS_EOM_PLRecon_ID EOM Profit/Loss Reconciliation	  */
	public void setUNS_EOM_PLRecon_ID (int UNS_EOM_PLRecon_ID)
	{
		if (UNS_EOM_PLRecon_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EOM_PLRecon_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EOM_PLRecon_ID, Integer.valueOf(UNS_EOM_PLRecon_ID));
	}

	/** Get EOM Profit/Loss Reconciliation.
		@return EOM Profit/Loss Reconciliation	  */
	public int getUNS_EOM_PLRecon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EOM_PLRecon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EOM Profit/Loss Recon Line.
		@param UNS_EOM_PLRecon_Line_ID EOM Profit/Loss Recon Line	  */
	public void setUNS_EOM_PLRecon_Line_ID (int UNS_EOM_PLRecon_Line_ID)
	{
		if (UNS_EOM_PLRecon_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EOM_PLRecon_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EOM_PLRecon_Line_ID, Integer.valueOf(UNS_EOM_PLRecon_Line_ID));
	}

	/** Get EOM Profit/Loss Recon Line.
		@return EOM Profit/Loss Recon Line	  */
	public int getUNS_EOM_PLRecon_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EOM_PLRecon_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EOM_PLRecon_Line_UU.
		@param UNS_EOM_PLRecon_Line_UU UNS_EOM_PLRecon_Line_UU	  */
	public void setUNS_EOM_PLRecon_Line_UU (String UNS_EOM_PLRecon_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EOM_PLRecon_Line_UU, UNS_EOM_PLRecon_Line_UU);
	}

	/** Get UNS_EOM_PLRecon_Line_UU.
		@return UNS_EOM_PLRecon_Line_UU	  */
	public String getUNS_EOM_PLRecon_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EOM_PLRecon_Line_UU);
	}
}