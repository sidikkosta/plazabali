/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.untaerp.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_CurrsBalance_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CurrsBalance_Line extends PO implements I_UNS_CurrsBalance_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190123L;

    /** Standard Constructor */
    public X_UNS_CurrsBalance_Line (Properties ctx, int UNS_CurrsBalance_Line_ID, String trxName)
    {
      super (ctx, UNS_CurrsBalance_Line_ID, trxName);
      /** if (UNS_CurrsBalance_Line_ID == 0)
        {
			setAccountedBalance (Env.ZERO);
			setC_Currency_ID (0);
			setC_ElementValue_ID (0);
			setCurrencyRate (Env.ZERO);
			setEOMBalance (Env.ZERO);
			setExcRateGainLoss (Env.ZERO);
			setOriginalBalance (Env.ZERO);
			setUNS_CurrsBalance_ID (0);
			setUNS_CurrsBalance_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CurrsBalance_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CurrsBalance_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Accounted Balance.
		@param AccountedBalance 
		The balance amount accounted to the specified currency defined in the accounting schema
	  */
	public void setAccountedBalance (BigDecimal AccountedBalance)
	{
		set_Value (COLUMNNAME_AccountedBalance, AccountedBalance);
	}

	/** Get Accounted Balance.
		@return The balance amount accounted to the specified currency defined in the accounting schema
	  */
	public BigDecimal getAccountedBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AccountedBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** AccountType AD_Reference_ID=117 */
	public static final int ACCOUNTTYPE_AD_Reference_ID=117;
	/** Asset = A */
	public static final String ACCOUNTTYPE_Asset = "A";
	/** Liability = L */
	public static final String ACCOUNTTYPE_Liability = "L";
	/** Revenue = R */
	public static final String ACCOUNTTYPE_Revenue = "R";
	/** Expense = E */
	public static final String ACCOUNTTYPE_Expense = "E";
	/** Owner's Equity = O */
	public static final String ACCOUNTTYPE_OwnerSEquity = "O";
	/** Memo = M */
	public static final String ACCOUNTTYPE_Memo = "M";
	/** Set Account Type.
		@param AccountType 
		Indicates the type of account
	  */
	public void setAccountType (String AccountType)
	{

		set_ValueNoCheck (COLUMNNAME_AccountType, AccountType);
	}

	/** Get Account Type.
		@return Indicates the type of account
	  */
	public String getAccountType () 
	{
		return (String)get_Value(COLUMNNAME_AccountType);
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_ElementValue getC_ElementValue() throws RuntimeException
    {
		return (org.compiere.model.I_C_ElementValue)MTable.get(getCtx(), org.compiere.model.I_C_ElementValue.Table_Name)
			.getPO(getC_ElementValue_ID(), get_TrxName());	}

	/** Set Account Element.
		@param C_ElementValue_ID 
		Account Element
	  */
	public void setC_ElementValue_ID (int C_ElementValue_ID)
	{
		if (C_ElementValue_ID < 1) 
			set_Value (COLUMNNAME_C_ElementValue_ID, null);
		else 
			set_Value (COLUMNNAME_C_ElementValue_ID, Integer.valueOf(C_ElementValue_ID));
	}

	/** Get Account Element.
		@return Account Element
	  */
	public int getC_ElementValue_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ElementValue_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Rate.
		@param CurrencyRate 
		Currency Conversion Rate
	  */
	public void setCurrencyRate (BigDecimal CurrencyRate)
	{
		set_Value (COLUMNNAME_CurrencyRate, CurrencyRate);
	}

	/** Get Rate.
		@return Currency Conversion Rate
	  */
	public BigDecimal getCurrencyRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CurrencyRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set EOM Balance.
		@param EOMBalance 
		The expected balance amount in the end of month
	  */
	public void setEOMBalance (BigDecimal EOMBalance)
	{
		set_Value (COLUMNNAME_EOMBalance, EOMBalance);
	}

	/** Get EOM Balance.
		@return The expected balance amount in the end of month
	  */
	public BigDecimal getEOMBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EOMBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Exc-Rate Gain/Loss.
		@param ExcRateGainLoss 
		The gain or loss of the exchange rate in effect of EOM Currency Balancing
	  */
	public void setExcRateGainLoss (BigDecimal ExcRateGainLoss)
	{
		set_Value (COLUMNNAME_ExcRateGainLoss, ExcRateGainLoss);
	}

	/** Get Exc-Rate Gain/Loss.
		@return The gain or loss of the exchange rate in effect of EOM Currency Balancing
	  */
	public BigDecimal getExcRateGainLoss () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ExcRateGainLoss);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Balance.
		@param OriginalBalance 
		The balance amount in the original currency
	  */
	public void setOriginalBalance (BigDecimal OriginalBalance)
	{
		set_Value (COLUMNNAME_OriginalBalance, OriginalBalance);
	}

	/** Get Original Balance.
		@return The balance amount in the original currency
	  */
	public BigDecimal getOriginalBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.untaerp.model.I_UNS_CurrsBalance getUNS_CurrsBalance() throws RuntimeException
    {
		return (com.untaerp.model.I_UNS_CurrsBalance)MTable.get(getCtx(), com.untaerp.model.I_UNS_CurrsBalance.Table_Name)
			.getPO(getUNS_CurrsBalance_ID(), get_TrxName());	}

	/** Set Currency Balancing.
		@param UNS_CurrsBalance_ID Currency Balancing	  */
	public void setUNS_CurrsBalance_ID (int UNS_CurrsBalance_ID)
	{
		if (UNS_CurrsBalance_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CurrsBalance_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CurrsBalance_ID, Integer.valueOf(UNS_CurrsBalance_ID));
	}

	/** Get Currency Balancing.
		@return Currency Balancing	  */
	public int getUNS_CurrsBalance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CurrsBalance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Currency Balance Line.
		@param UNS_CurrsBalance_Line_ID Currency Balance Line	  */
	public void setUNS_CurrsBalance_Line_ID (int UNS_CurrsBalance_Line_ID)
	{
		if (UNS_CurrsBalance_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CurrsBalance_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CurrsBalance_Line_ID, Integer.valueOf(UNS_CurrsBalance_Line_ID));
	}

	/** Get Currency Balance Line.
		@return Currency Balance Line	  */
	public int getUNS_CurrsBalance_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CurrsBalance_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CurrsBalance_Line_UU.
		@param UNS_CurrsBalance_Line_UU UNS_CurrsBalance_Line_UU	  */
	public void setUNS_CurrsBalance_Line_UU (String UNS_CurrsBalance_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CurrsBalance_Line_UU, UNS_CurrsBalance_Line_UU);
	}

	/** Get UNS_CurrsBalance_Line_UU.
		@return UNS_CurrsBalance_Line_UU	  */
	public String getUNS_CurrsBalance_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CurrsBalance_Line_UU);
	}
}