/**
 * 
 */
package com.untaerp.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MColumn;
import org.compiere.model.MDocType;

/**
 * @author Harry
 *
 */
public class MUNSRepostDocLine extends X_UNS_RepostDoc_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5302033346709841504L;

	/**
	 * @param ctx
	 * @param UNS_RepostDoc_Line_ID
	 * @param trxName
	 */
	public MUNSRepostDocLine(Properties ctx, int UNS_RepostDoc_Line_ID,
			String trxName) {
		super(ctx, UNS_RepostDoc_Line_ID, trxName);
		loadDetail();
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSRepostDocLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		loadDetail();
	}
	
	public String DOCBASETYPE = "";
	public String DocTypeName = "";
	
	public void loadDetail()
	{
		if (getC_DocType_ID() > 0)
		{
			MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
			DOCBASETYPE = dt.getDocBaseType();
			DocTypeName = dt.getName();
		}
	}

	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_AD_DateColumn_ID))
			setDateColumn(new MColumn(getCtx(), getAD_DateColumn_ID(), get_TrxName()).getColumnName());
		
		if (newRecord || is_ValueChanged(COLUMNNAME_AD_Table_ID))
			setTableName(getAD_Table().getTableName());
		
		return true;
	}
	
	/**
	 * 
	 */
	public String toString()
	{
		return get_ID() + "-" + getName();
	}
}
