/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.untaerp.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_CurrsBalance_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_CurrsBalance_Line 
{

    /** TableName=UNS_CurrsBalance_Line */
    public static final String Table_Name = "UNS_CurrsBalance_Line";

    /** AD_Table_ID=1000499 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AccountedBalance */
    public static final String COLUMNNAME_AccountedBalance = "AccountedBalance";

	/** Set Accounted Balance.
	  * The balance amount accounted to the specified currency defined in the accounting schema
	  */
	public void setAccountedBalance (BigDecimal AccountedBalance);

	/** Get Accounted Balance.
	  * The balance amount accounted to the specified currency defined in the accounting schema
	  */
	public BigDecimal getAccountedBalance();

    /** Column name AccountType */
    public static final String COLUMNNAME_AccountType = "AccountType";

	/** Set Account Type.
	  * Indicates the type of account
	  */
	public void setAccountType (String AccountType);

	/** Get Account Type.
	  * Indicates the type of account
	  */
	public String getAccountType();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name C_ElementValue_ID */
    public static final String COLUMNNAME_C_ElementValue_ID = "C_ElementValue_ID";

	/** Set Account Element.
	  * Account Element
	  */
	public void setC_ElementValue_ID (int C_ElementValue_ID);

	/** Get Account Element.
	  * Account Element
	  */
	public int getC_ElementValue_ID();

	public org.compiere.model.I_C_ElementValue getC_ElementValue() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CurrencyRate */
    public static final String COLUMNNAME_CurrencyRate = "CurrencyRate";

	/** Set Rate.
	  * Currency Conversion Rate
	  */
	public void setCurrencyRate (BigDecimal CurrencyRate);

	/** Get Rate.
	  * Currency Conversion Rate
	  */
	public BigDecimal getCurrencyRate();

    /** Column name EOMBalance */
    public static final String COLUMNNAME_EOMBalance = "EOMBalance";

	/** Set EOM Balance.
	  * The expected balance amount in the end of month
	  */
	public void setEOMBalance (BigDecimal EOMBalance);

	/** Get EOM Balance.
	  * The expected balance amount in the end of month
	  */
	public BigDecimal getEOMBalance();

    /** Column name ExcRateGainLoss */
    public static final String COLUMNNAME_ExcRateGainLoss = "ExcRateGainLoss";

	/** Set Exc-Rate Gain/Loss.
	  * The gain or loss of the exchange rate in effect of EOM Currency Balancing
	  */
	public void setExcRateGainLoss (BigDecimal ExcRateGainLoss);

	/** Get Exc-Rate Gain/Loss.
	  * The gain or loss of the exchange rate in effect of EOM Currency Balancing
	  */
	public BigDecimal getExcRateGainLoss();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name OriginalBalance */
    public static final String COLUMNNAME_OriginalBalance = "OriginalBalance";

	/** Set Original Balance.
	  * The balance amount in the original currency
	  */
	public void setOriginalBalance (BigDecimal OriginalBalance);

	/** Get Original Balance.
	  * The balance amount in the original currency
	  */
	public BigDecimal getOriginalBalance();

    /** Column name UNS_CurrsBalance_ID */
    public static final String COLUMNNAME_UNS_CurrsBalance_ID = "UNS_CurrsBalance_ID";

	/** Set Currency Balancing	  */
	public void setUNS_CurrsBalance_ID (int UNS_CurrsBalance_ID);

	/** Get Currency Balancing	  */
	public int getUNS_CurrsBalance_ID();

	public com.untaerp.model.I_UNS_CurrsBalance getUNS_CurrsBalance() throws RuntimeException;

    /** Column name UNS_CurrsBalance_Line_ID */
    public static final String COLUMNNAME_UNS_CurrsBalance_Line_ID = "UNS_CurrsBalance_Line_ID";

	/** Set Currency Balance Line	  */
	public void setUNS_CurrsBalance_Line_ID (int UNS_CurrsBalance_Line_ID);

	/** Get Currency Balance Line	  */
	public int getUNS_CurrsBalance_Line_ID();

    /** Column name UNS_CurrsBalance_Line_UU */
    public static final String COLUMNNAME_UNS_CurrsBalance_Line_UU = "UNS_CurrsBalance_Line_UU";

	/** Set UNS_CurrsBalance_Line_UU	  */
	public void setUNS_CurrsBalance_Line_UU (String UNS_CurrsBalance_Line_UU);

	/** Get UNS_CurrsBalance_Line_UU	  */
	public String getUNS_CurrsBalance_Line_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
