/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.untaerp.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_PLRecon_Orgs_Conf
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_PLRecon_Orgs_Conf 
{

    /** TableName=UNS_PLRecon_Orgs_Conf */
    public static final String Table_Name = "UNS_PLRecon_Orgs_Conf";

    /** AD_Table_ID=1000501 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_OrgTrx_ID */
    public static final String COLUMNNAME_AD_OrgTrx_ID = "AD_OrgTrx_ID";

	/** Set Trx Department.
	  * Performing or initiating Department
	  */
	public void setAD_OrgTrx_ID (int AD_OrgTrx_ID);

	/** Get Trx Department.
	  * Performing or initiating Department
	  */
	public int getAD_OrgTrx_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Portion */
    public static final String COLUMNNAME_Portion = "Portion";

	/** Set Portion (%).
	  * The portion (in percentage) of this item
	  */
	public void setPortion (BigDecimal Portion);

	/** Get Portion (%).
	  * The portion (in percentage) of this item
	  */
	public BigDecimal getPortion();

    /** Column name UNS_PLRecon_Conf_ID */
    public static final String COLUMNNAME_UNS_PLRecon_Conf_ID = "UNS_PLRecon_Conf_ID";

	/** Set Profit/Loss Recon Conf	  */
	public void setUNS_PLRecon_Conf_ID (int UNS_PLRecon_Conf_ID);

	/** Get Profit/Loss Recon Conf	  */
	public int getUNS_PLRecon_Conf_ID();

	public com.untaerp.model.I_UNS_PLRecon_Conf getUNS_PLRecon_Conf() throws RuntimeException;

    /** Column name UNS_PLRecon_Orgs_Conf_ID */
    public static final String COLUMNNAME_UNS_PLRecon_Orgs_Conf_ID = "UNS_PLRecon_Orgs_Conf_ID";

	/** Set Profit/Loss Recon Orgs-portion Conf	  */
	public void setUNS_PLRecon_Orgs_Conf_ID (int UNS_PLRecon_Orgs_Conf_ID);

	/** Get Profit/Loss Recon Orgs-portion Conf	  */
	public int getUNS_PLRecon_Orgs_Conf_ID();

    /** Column name UNS_PLRecon_Orgs_Conf_UU */
    public static final String COLUMNNAME_UNS_PLRecon_Orgs_Conf_UU = "UNS_PLRecon_Orgs_Conf_UU";

	/** Set UNS_PLRecon_Orgs_Conf_UU	  */
	public void setUNS_PLRecon_Orgs_Conf_UU (String UNS_PLRecon_Orgs_Conf_UU);

	/** Get UNS_PLRecon_Orgs_Conf_UU	  */
	public String getUNS_PLRecon_Orgs_Conf_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
