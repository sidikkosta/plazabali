/**
 * 
 */
package com.untaerp.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Harry
 *
 */
public class MUNSCurrsBalanceLine extends X_UNS_CurrsBalance_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8676979015583751735L;

	/**
	 * @param ctx
	 * @param UNS_CurrsBalance_Line_ID
	 * @param trxName
	 */
	public MUNSCurrsBalanceLine(Properties ctx, int UNS_CurrsBalance_Line_ID,
			String trxName) {
		super(ctx, UNS_CurrsBalance_Line_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCurrsBalanceLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
}
