/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.untaerp.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_RepostDoc_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_RepostDoc_Line extends PO implements I_UNS_RepostDoc_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171116L;

    /** Standard Constructor */
    public X_UNS_RepostDoc_Line (Properties ctx, int UNS_RepostDoc_Line_ID, String trxName)
    {
      super (ctx, UNS_RepostDoc_Line_ID, trxName);
      /** if (UNS_RepostDoc_Line_ID == 0)
        {
			setAD_Table_ID (0);
			setIsForce (false);
// N
			setName (null);
			setSeqNo (0);
// @SQL=SELECT NVL(MAX(SeqNo),0)+10 AS DefaultValue FROM UNS_RepostDoc_Line WHERE UNS_RepostDoc_ID=@UNS_RepostDoc_ID@
			setUNS_RepostDoc_ID (0);
			setUNS_RepostDoc_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_RepostDoc_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_RepostDoc_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Column getAD_DateColumn() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Column)MTable.get(getCtx(), org.compiere.model.I_AD_Column.Table_Name)
			.getPO(getAD_DateColumn_ID(), get_TrxName());	}

	/** Set Date Column ID.
		@param AD_DateColumn_ID 
		The date column id
	  */
	public void setAD_DateColumn_ID (int AD_DateColumn_ID)
	{
		if (AD_DateColumn_ID < 1) 
			set_Value (COLUMNNAME_AD_DateColumn_ID, null);
		else 
			set_Value (COLUMNNAME_AD_DateColumn_ID, Integer.valueOf(AD_DateColumn_ID));
	}

	/** Get Date Column ID.
		@return The date column id
	  */
	public int getAD_DateColumn_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_DateColumn_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_Table getAD_Table() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Table)MTable.get(getCtx(), org.compiere.model.I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Column.
		@param DateColumn 
		Fully qualified date column
	  */
	public void setDateColumn (String DateColumn)
	{
		set_Value (COLUMNNAME_DateColumn, DateColumn);
	}

	/** Get Date Column.
		@return Fully qualified date column
	  */
	public String getDateColumn () 
	{
		return (String)get_Value(COLUMNNAME_DateColumn);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Is Force.
		@param IsForce 
		To indicate to force the process
	  */
	public void setIsForce (boolean IsForce)
	{
		set_Value (COLUMNNAME_IsForce, Boolean.valueOf(IsForce));
	}

	/** Get Is Force.
		@return To indicate to force the process
	  */
	public boolean isForce () 
	{
		Object oo = get_Value(COLUMNNAME_IsForce);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Notice.
		@param Notice 
		Contains last write notice
	  */
	public void setNotice (String Notice)
	{
		set_Value (COLUMNNAME_Notice, Notice);
	}

	/** Get Notice.
		@return Contains last write notice
	  */
	public String getNotice () 
	{
		return (String)get_Value(COLUMNNAME_Notice);
	}

	/** Set Sql SELECT.
		@param SelectClause 
		SQL SELECT clause
	  */
	public void setSelectClause (String SelectClause)
	{
		set_Value (COLUMNNAME_SelectClause, SelectClause);
	}

	/** Get Sql SELECT.
		@return SQL SELECT clause
	  */
	public String getSelectClause () 
	{
		return (String)get_Value(COLUMNNAME_SelectClause);
	}

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set DB Table Name.
		@param TableName 
		Name of the table in the database
	  */
	public void setTableName (String TableName)
	{
		set_ValueNoCheck (COLUMNNAME_TableName, TableName);
	}

	/** Get DB Table Name.
		@return Name of the table in the database
	  */
	public String getTableName () 
	{
		return (String)get_Value(COLUMNNAME_TableName);
	}

	public com.untaerp.model.I_UNS_RepostDoc getUNS_RepostDoc() throws RuntimeException
    {
		return (com.untaerp.model.I_UNS_RepostDoc)MTable.get(getCtx(), com.untaerp.model.I_UNS_RepostDoc.Table_Name)
			.getPO(getUNS_RepostDoc_ID(), get_TrxName());	}

	/** Set Repost Document.
		@param UNS_RepostDoc_ID Repost Document	  */
	public void setUNS_RepostDoc_ID (int UNS_RepostDoc_ID)
	{
		if (UNS_RepostDoc_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_ID, Integer.valueOf(UNS_RepostDoc_ID));
	}

	/** Get Repost Document.
		@return Repost Document	  */
	public int getUNS_RepostDoc_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_RepostDoc_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Repost Document Line.
		@param UNS_RepostDoc_Line_ID Repost Document Line	  */
	public void setUNS_RepostDoc_Line_ID (int UNS_RepostDoc_Line_ID)
	{
		if (UNS_RepostDoc_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_Line_ID, Integer.valueOf(UNS_RepostDoc_Line_ID));
	}

	/** Get Repost Document Line.
		@return Repost Document Line	  */
	public int getUNS_RepostDoc_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_RepostDoc_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_RepostDoc_Line_UU.
		@param UNS_RepostDoc_Line_UU UNS_RepostDoc_Line_UU	  */
	public void setUNS_RepostDoc_Line_UU (String UNS_RepostDoc_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_RepostDoc_Line_UU, UNS_RepostDoc_Line_UU);
	}

	/** Get UNS_RepostDoc_Line_UU.
		@return UNS_RepostDoc_Line_UU	  */
	public String getUNS_RepostDoc_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_RepostDoc_Line_UU);
	}

	/** Set Sql WHERE.
		@param WhereClause 
		Fully qualified SQL WHERE clause
	  */
	public void setWhereClause (String WhereClause)
	{
		set_Value (COLUMNNAME_WhereClause, WhereClause);
	}

	/** Get Sql WHERE.
		@return Fully qualified SQL WHERE clause
	  */
	public String getWhereClause () 
	{
		return (String)get_Value(COLUMNNAME_WhereClause);
	}
}