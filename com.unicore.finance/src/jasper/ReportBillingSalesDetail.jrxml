<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportBillingSales" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="65135200-7feb-47fc-92ef-2b60f5090568">
	<property name="ireport.zoom" value="1.2100000000000006"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SalesRep_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="AD_Org_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT us.Name AS Sales, bp.Name AS Customer, bpl.Name AS Address, bg.DocumentNo AS BillingNo, bg.DateDoc AS BillingDate, ci.DocumentNo AS NoInv, ci.DateInvoiced AS InvDate, ci.GrandTotal AS GrandTotal, bgr.DateDoc AS ResultDate, blr.paidamt AS Cash, blr.paidamtgiro AS Giro FROM UNS_BillingGroup bg

INNER JOIN AD_User us ON us.AD_User_ID = bg.SalesRep_ID
INNER JOIN UNS_Billing b ON b.UNS_BillingGroup_ID = bg.UNS_BillingGroup_ID
INNER JOIN UNS_BillingLine bl ON bl.UNS_Billing_ID = b.UNS_Billing_ID
INNER JOIN UNS_BillingGroup_Result bgr ON bgr.UNS_BillingGroup_ID = b.UNS_BillingGroup_ID
INNER JOIN UNS_Billing_Result br ON br.UNS_BillingGroup_Result_ID = bgr.UNS_BillingGroup_Result_ID AND br.UNS_Billing_ID = b.UNS_Billing_ID
INNER JOIN UNS_BillingLine_Result blr ON blr.UNS_Billing_Result_ID = br.UNS_Billing_Result_ID AND blr.UNS_BillingLine_ID = bl.UNS_BillingLine_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = b.C_BPartner_ID
INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_ID = bp.C_BPartner_ID
INNER JOIN C_Invoice ci ON ci.C_Invoice_ID = bl.C_Invoice_ID
INNER JOIN AD_Org ao ON ao.AD_Org_ID = bg.AD_Org_ID

WHERE blr.PaymentStatus <> 'NP' AND bg.DocStatus IN ('CO', 'CL') AND bgr.DocStatus IN ('CO', 'CL') AND CASE WHEN $P{SalesRep_ID} IS NOT NULL THEN us.AD_User_ID = $P{SalesRep_ID} ELSE 1=1 END AND (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN bp.C_BPartner_ID = $P{C_BPartner_ID} ELSE 1=1 END) AND (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NULL THEN bg.DateDoc >= $P{DateFrom}::timestamp WHEN $P{DateTo}::timestamp IS NOT NULL AND $P{DateFrom}::timestamp IS NULL THEN bg.DateDoc <= $P{DateTo} WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN bg.DateDoc >= $P{DateFrom}::timestamp AND bg.DateDoc <= $P{DateTo}::timestamp ELSE 1=1 END) AND CASE WHEN $P{AD_Org_ID} IS NOT NULL THEN ao.AD_Org_ID = $P{AD_Org_ID} ELSE 1=1 END

ORDER BY Sales, Customer, InvDate, BillingDate]]>
	</queryString>
	<field name="sales" class="java.lang.String"/>
	<field name="customer" class="java.lang.String"/>
	<field name="address" class="java.lang.String"/>
	<field name="billingno" class="java.lang.String"/>
	<field name="billingdate" class="java.sql.Timestamp"/>
	<field name="noinv" class="java.lang.String"/>
	<field name="invdate" class="java.sql.Timestamp"/>
	<field name="grandtotal" class="java.math.BigDecimal"/>
	<field name="resultdate" class="java.sql.Timestamp"/>
	<field name="cash" class="java.math.BigDecimal"/>
	<field name="giro" class="java.math.BigDecimal"/>
	<variable name="total_cash_inv" class="java.math.BigDecimal" resetType="Group" resetGroup="Invoice" calculation="Sum">
		<variableExpression><![CDATA[$F{cash}]]></variableExpression>
	</variable>
	<variable name="total_cash_customer" class="java.math.BigDecimal" resetType="Group" resetGroup="Customer" calculation="Sum">
		<variableExpression><![CDATA[$F{cash}]]></variableExpression>
	</variable>
	<variable name="total_cash_sales" class="java.math.BigDecimal" resetType="Group" resetGroup="Sales" calculation="Sum">
		<variableExpression><![CDATA[$F{cash}]]></variableExpression>
	</variable>
	<variable name="total_giro_inv" class="java.math.BigDecimal" resetType="Group" resetGroup="Invoice" calculation="Sum">
		<variableExpression><![CDATA[$F{giro}]]></variableExpression>
	</variable>
	<variable name="total_giro_customer" class="java.math.BigDecimal" resetType="Group" resetGroup="Customer" calculation="Sum">
		<variableExpression><![CDATA[$F{giro}]]></variableExpression>
	</variable>
	<variable name="total_giro_sales" class="java.math.BigDecimal" resetType="Group" resetGroup="Sales" calculation="Sum">
		<variableExpression><![CDATA[$F{giro}]]></variableExpression>
	</variable>
	<group name="Sales" isStartNewPage="true">
		<groupExpression><![CDATA[$F{sales}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement x="0" y="0" width="555" height="20" uuid="ec7bf670-e434-422a-9e15-8c9b679edece"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["#Sales : " + $F{sales}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="19" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement x="333" y="0" width="112" height="20" uuid="78980a2c-4555-494a-8b30-12bd79861649"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_cash_sales}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="444" y="0" width="111" height="20" uuid="7d6ae7b5-4a8e-4180-afcc-90d89fbedaca"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_giro_sales}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="19" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
				<line>
					<reportElement x="0" y="0" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="Customer">
		<groupExpression><![CDATA[$F{customer}]]></groupExpression>
		<groupHeader>
			<band height="40">
				<textField>
					<reportElement x="0" y="0" width="555" height="20" uuid="3c616d4e-d196-41c9-9e73-d8d1ecf98b02"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["#Customer : " + $F{customer} + " - #Address: " + $F{address}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="19" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
				<staticText>
					<reportElement x="0" y="20" width="111" height="20" uuid="a4f6f54b-d98d-40ca-ac32-9818c8946824"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Billing]]></text>
				</staticText>
				<staticText>
					<reportElement x="111" y="20" width="111" height="20" uuid="9bf2f73f-1f76-4038-a86a-4e88b8b8d65e"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Billing Date]]></text>
				</staticText>
				<staticText>
					<reportElement x="444" y="20" width="111" height="20" uuid="3825b7a9-53ed-4dc2-8ce8-084266877239"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Giro]]></text>
				</staticText>
				<staticText>
					<reportElement x="222" y="20" width="111" height="20" uuid="1b00f5dc-6e0b-4378-be69-a4c6c543bed4"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Result Date]]></text>
				</staticText>
				<staticText>
					<reportElement x="333" y="20" width="112" height="20" uuid="c3cf7bae-9fae-4c1f-a6e0-03ac54fd2908"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Cash]]></text>
				</staticText>
				<line>
					<reportElement x="0" y="39" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement x="333" y="0" width="112" height="20" uuid="3be50c6d-7118-4cf4-b233-ddfb6dd3208a"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_cash_customer}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="0" width="333" height="20" uuid="c1d699aa-a379-43e7-87ee-467712915a80"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="445" y="0" width="111" height="20" uuid="dbc2d084-b480-4a2a-afcd-7fb00fab9b8a"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_giro_customer}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="-1" y="-2" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="Invoice">
		<groupExpression><![CDATA[$F{noinv}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement x="0" y="0" width="555" height="20" uuid="0b00c220-19d5-491f-8454-af51f4f37a50"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA["#Invoice : " + $F{noinv} + " - #Date Invoiced : " + new SimpleDateFormat("dd-MM-yyy").format($F{invdate}) + " - #Amount : " + $F{grandtotal}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="19" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement x="444" y="0" width="111" height="20" uuid="d75f6a3a-32ab-4a19-9bcc-0c2aac301caf"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_giro_inv}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="333" y="0" width="112" height="20" uuid="17797d78-8049-4e03-a09d-7914969e18ce"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_cash_inv}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="0" width="333" height="20" uuid="52f147a2-75f9-4646-a388-e6fb3bf5bd63"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
				<line>
					<reportElement x="0" y="1" width="555" height="1" uuid="55ef5a46-db60-49bf-a623-7a580bc17234"/>
				</line>
				<staticText>
					<reportElement x="0" y="0" width="333" height="20" uuid="a2404060-1680-46a9-bda0-a0554520005d"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="555" height="20" uuid="989fef8e-d1a7-4048-8352-5eb1db720757"/>
				<textElement textAlignment="Center">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Billing Sales]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="20" splitType="Stretch">
			<textField pattern="#,##0.00">
				<reportElement x="333" y="0" width="112" height="20" uuid="b8b33597-53f1-4ed9-883b-d8a2df7cff23"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cash}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="222" y="0" width="111" height="20" uuid="051a4f18-5588-4862-9a8b-d323f998ebbb"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MM-yyyy").format($F{resultdate})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="111" height="20" uuid="837a9657-4743-46a6-a975-eff492879089"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billingno}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="111" y="0" width="111" height="20" uuid="6deacf8c-a40e-49dd-b756-2569f7caa7c8"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MM-yyyy").format($F{billingdate})]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="444" y="0" width="111" height="20" uuid="abf93fef-5d66-44fa-ab3c-f2cd96bf1552"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{giro}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
