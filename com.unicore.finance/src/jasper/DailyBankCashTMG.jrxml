<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DailyBankCash" language="groovy" pageWidth="612" pageHeight="792" columnWidth="572" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="34f5ba20-279a-483a-abcf-77457045aa83">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="StartDate" class="java.util.Date"/>
	<parameter name="EndDate" class="java.util.Date"/>
	<parameter name="AD_User_ID" class="java.lang.Integer"/>
	<parameter name="C_BankAccount_ID" class="java.lang.Integer"/>
	<parameter name="DocStatus" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Dibuat" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Diperiksa" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Disetujui" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT now() AS DateNow, a.Name AS AccountName, sl.StatementLineDate AS Date, COALESCE(c.Name, p.DocumentNo) AS ChargeORPayment,
CASE WHEN sl.TransactionType = 'APT' THEN sl.StmtAmt ELSE 0 END AS Debit, CASE WHEN sl.TransactionType = 'ART' THEN sl.StmtAmt ELSE 0 END AS Credit, sl.Description,
(a.CurrentBalance - (SELECT COALESCE (SUM(sltmp.StmtAmt), 0) FROM C_BankStatementLine sltmp WHERE EXISTS (SELECT * FROM C_BankStatement stmp WHERE stmp.DocStatus IN ('CO', 'CL') AND
stmp.C_BankStatement_ID = sltmp.C_BankStatement_ID AND stmp.c_bankaccount_id = a.c_bankaccount_id AND sltmp.statementlinedate >= $P{StartDate}))) AS BeginBalance, u.Name AS User,
s.docstatus as docstatus, coalesce(sl.referenceno,'-') as nobukti
FROM C_BankStatementLine sl

INNER JOIN C_BankStatement s ON s.C_BankStatement_ID = sl.C_BankStatement_ID
INNER JOIN C_BankAccount a ON a.C_BankAccount_ID = s.C_BankAccount_ID
LEFT JOIN AD_User u ON u.AD_User_ID = $P{AD_User_ID}
LEFT JOIN C_Charge c ON c.C_Charge_ID = sl.C_Charge_ID
LEFT JOIN C_Payment p ON p.C_Payment_ID = sl.C_Payment_ID

WHERE (Case When $P{DocStatus}='COM' then s.docstatus in ('CO','CL') when $P{DocStatus}='UNC' Then s.docstatus not in ('CO','CL','VO','RE') ELSE s.DocStatus NOT IN ('VO', 'RE') end) AND a.C_BankAccount_ID = $P{C_BankAccount_ID}
AND (sl.statementlinedate BETWEEN $P{StartDate} AND $P{EndDate})
ORDER BY sl.StatementLineDate, sl.Line]]>
	</queryString>
	<field name="datenow" class="java.sql.Timestamp"/>
	<field name="accountname" class="java.lang.String"/>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="chargeorpayment" class="java.lang.String"/>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<field name="description" class="java.lang.String"/>
	<field name="beginbalance" class="java.math.BigDecimal"/>
	<field name="user" class="java.lang.String"/>
	<field name="docstatus" class="java.lang.String"/>
	<field name="nobukti" class="java.lang.String"/>
	<variable name="sakhir" class="java.lang.String">
		<variableExpression><![CDATA[]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="Saldo" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{Saldo}+($F{debit}+$F{credit})]]></variableExpression>
		<initialValueExpression><![CDATA[$F{beginbalance}]]></initialValueExpression>
	</variable>
	<variable name="debit_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<variable name="credit_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<group name="Awal">
		<groupExpression><![CDATA[$F{beginbalance}]]></groupExpression>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="52" splitType="Stretch">
			<staticText>
				<reportElement x="473" y="0" width="100" height="15" uuid="8f88b6fb-78e6-41b5-954f-3900f221bd65"/>
				<textElement textAlignment="Right">
					<font size="8" isItalic="true"/>
				</textElement>
				<text><![CDATA[UntaCore Idempiere]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="0" width="571" height="15" uuid="0a0f9f67-4b1a-46af-aa6c-7e3d34205857"/>
				<textElement textAlignment="Center" markup="none">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Bank / Cash]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="32" width="572" height="13" uuid="019be73f-d044-4f3b-936f-e3697558e97b"/>
				<textElement textAlignment="Center">
					<font size="9" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Periode : "+
new SimpleDateFormat("dd/MM/yyyy").format($P{StartDate})+" up to "+
new SimpleDateFormat("dd/MM/yyyy").format($P{EndDate})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="15" width="562" height="15" uuid="eaee3fe0-a575-4e4b-8961-7cb009103cc9"/>
				<textElement textAlignment="Center">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{accountname}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20">
			<rectangle>
				<reportElement x="0" y="1" width="572" height="18" uuid="d8deee8d-eec1-4b10-a88a-082d5ae5f9b4"/>
				<graphicElement>
					<pen lineColor="#999999"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement x="18" y="2" width="50" height="18" uuid="bf5a1ebb-a799-462d-8565-25adb71af3fa"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="401" y="2" width="85" height="18" uuid="22fd88f5-060d-46da-9ae2-21ebbbcc3a97"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[Credit]]></text>
			</staticText>
			<staticText>
				<reportElement x="67" y="2" width="140" height="17" uuid="6586bbfd-79d2-4acd-9a28-b9636ae0962c"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[Description]]></text>
			</staticText>
			<staticText>
				<reportElement x="320" y="2" width="82" height="18" uuid="cdc03055-cf9b-4050-b0e4-a6958fe86736"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[Debit]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="2" width="18" height="18" uuid="7d2a42e8-cea3-4814-b4ff-12d4a76b904f"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[NO]]></text>
			</staticText>
			<staticText>
				<reportElement x="485" y="2" width="86" height="18" uuid="912885eb-94ad-48f5-bf7a-519ff1096055"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[Balance]]></text>
			</staticText>
			<staticText>
				<reportElement x="229" y="2" width="92" height="18" uuid="74998272-3174-433e-92b3-3f9468afc6bc"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<text><![CDATA[Charge / Payment]]></text>
			</staticText>
			<staticText>
				<reportElement x="206" y="2" width="24" height="17" uuid="85411ae7-8ada-4fb4-a520-e14a55cbbb19"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="14" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement x="67" y="-1" width="140" height="15" isPrintWhenDetailOverflows="true" uuid="68b1b19f-8cff-43c7-95ab-0aef26a81c0d"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Tahoma" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{description} == null ? " " : $F{description}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="-1" width="18" height="15" uuid="bf8b1837-16e9-4195-92d2-7807230e00c6"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[(38 * ($V{PAGE_NUMBER} - 1)) + $V{COLUMN_COUNT}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;(#,##0.00)">
				<reportElement x="320" y="-1" width="82" height="15" uuid="0da57380-5e3e-477d-a829-885c8d57d60c"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="17" y="-1" width="51" height="15" uuid="e7b74e5f-70a2-4b7f-b209-f78b2f629f92"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{date}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="485" y="-1" width="86" height="15" uuid="b077ec19-2e68-47d3-8bff-b283a551239f"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Saldo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="229" y="-1" width="92" height="15" isPrintWhenDetailOverflows="true" uuid="d76b3701-4d77-45af-812e-f4aa88d394da"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{chargeorpayment}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="206" y="-1" width="24" height="15" uuid="de8055c3-269f-4829-8ecb-6202f96fe494"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{docstatus}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;(#,##0.00)">
				<reportElement x="401" y="-1" width="85" height="15" uuid="de2a9aa5-c411-411e-b19b-42fad8ab2b5b"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="20">
			<textField>
				<reportElement x="0" y="7" width="264" height="11" uuid="8f632036-cda6-4432-971d-d5877c333f61"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Date Printed : " +new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format($F{datenow}) + " | " +$F{user}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="467" y="8" width="72" height="12" uuid="f139f6c2-29c4-4f80-9851-ce3a9988d67f"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="539" y="8" width="32" height="12" uuid="9dd339fe-3486-4426-98d5-714d213b75aa"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<summary>
		<band height="186">
			<line>
				<reportElement x="0" y="0" width="572" height="1" uuid="73257c6d-ac5a-472f-bd1c-f17104923eed"/>
				<graphicElement>
					<pen lineColor="#999999"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="-442" y="-14" width="52" height="13" uuid="403f792d-4f25-44f4-be0d-1462990966c9"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[User Print]]></text>
			</staticText>
			<staticText>
				<reportElement x="173" y="70" width="218" height="14" uuid="2125578c-eff9-4135-97a0-a860796836f0"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Ending Balance]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="465" y="70" width="100" height="14" uuid="444a9db7-579f-47be-93d4-870536dfd5f6"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{beginbalance} + ($V{debit_1}+$V{credit_1})]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;(#,##0.00)">
				<reportElement x="465" y="42" width="100" height="14" uuid="94f99fe4-32eb-4fbf-bbb4-1c529577d33b"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{credit_1}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="173" y="56" width="218" height="14" uuid="2f837839-e2be-42b8-84db-015b738e2a26"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Credit]]></text>
			</staticText>
			<textField pattern="#,##0.00;(#,##0.00)">
				<reportElement x="465" y="56" width="100" height="14" uuid="686cb4e8-f640-4d12-a580-2f143e3a7245"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{debit_1}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="173" y="42" width="218" height="14" uuid="d20d177e-0943-43a4-95d5-27116564ac52"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Debit]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="465" y="28" width="100" height="14" uuid="e06fe31d-52fc-4598-8954-71dbfc2ce1f9"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{beginbalance}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="173" y="28" width="218" height="14" uuid="2f9f2d1d-fe45-4724-9176-8a7081ea5266"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Beginning Balance]]></text>
			</staticText>
			<staticText>
				<reportElement x="425" y="28" width="13" height="14" uuid="68f29126-55de-4961-b11d-2cb342b35003"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="425" y="42" width="13" height="14" uuid="3f712e05-61c4-493e-b76d-b51710e0e0d5"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="425" y="56" width="13" height="14" uuid="9d98ce82-6b23-4273-9f89-e2206c074024"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="425" y="70" width="13" height="14" uuid="dd5957b0-79e7-43bd-9b29-71e73ab53acc"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="5" width="37" height="14" uuid="18126cc4-210a-42bf-b2ae-99718b3f106a"/>
				<textElement>
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="19" width="18" height="14" uuid="596478ca-20be-489c-b74e-7b2d61255a22"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[CO]]></text>
			</staticText>
			<staticText>
				<reportElement x="18" y="19" width="6" height="14" uuid="f6e91c6a-3069-47e3-affb-13880ec5a5ec"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="24" y="19" width="82" height="14" uuid="4ba88098-d95d-40c6-9cee-68ddbe2e17b8"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[Completed]]></text>
			</staticText>
			<staticText>
				<reportElement x="24" y="33" width="82" height="14" uuid="68be4172-265d-4a6a-b0e1-73a3e84ccf57"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[In Progress]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="33" width="18" height="14" uuid="49a8a418-f113-473a-9415-2e7f0cf2e246"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[IP]]></text>
			</staticText>
			<staticText>
				<reportElement x="18" y="33" width="6" height="14" uuid="9b702b68-c565-40d3-bf2e-9d78e9a52282"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="18" y="44" width="6" height="14" uuid="d59f20de-af12-446c-bdb2-a3c1d345d912"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="44" width="18" height="14" uuid="dbdfe610-7cc4-4913-af66-3596289cfa1d"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[CL]]></text>
			</staticText>
			<staticText>
				<reportElement x="24" y="44" width="82" height="14" uuid="14324afa-1f2c-41dc-a230-d4fc270e0930"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[Closed]]></text>
			</staticText>
			<staticText>
				<reportElement x="18" y="57" width="6" height="14" uuid="d3633658-c84c-478c-9627-221764d5b5fa"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="57" width="18" height="14" uuid="ca433b34-6d28-475f-8c49-ce167d06def7"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[DR]]></text>
			</staticText>
			<staticText>
				<reportElement x="24" y="57" width="82" height="14" uuid="00ac69f6-1b20-4de5-99af-bb11b7a056d7"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[Drafted]]></text>
			</staticText>
			<staticText>
				<reportElement x="18" y="71" width="6" height="14" uuid="c161abdc-4831-4375-bd59-fb96b4473a19"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="24" y="71" width="82" height="14" uuid="c4b2272b-9a0b-4552-b73e-53f05d0b55e7"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[Invalid]]></text>
			</staticText>
			<staticText>
				<reportElement x="1" y="71" width="18" height="14" uuid="ef4085df-7124-4ec4-b27e-ed1c0de52f0d"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<text><![CDATA[IN]]></text>
			</staticText>
			<staticText>
				<reportElement x="18" y="104" width="100" height="20" uuid="feca6b46-9f4f-46b9-b65f-70ebc07fed45"/>
				<textElement textAlignment="Center">
					<font size="10" isUnderline="true"/>
				</textElement>
				<text><![CDATA[Dibuat,]]></text>
			</staticText>
			<staticText>
				<reportElement x="458" y="104" width="100" height="20" uuid="f7ba3b2d-c11e-4d92-a07b-d9e3488a8fbd"/>
				<textElement textAlignment="Center">
					<font isUnderline="true"/>
				</textElement>
				<text><![CDATA[DiSetujui,]]></text>
			</staticText>
			<staticText>
				<reportElement x="248" y="104" width="100" height="20" uuid="fc1df670-753e-4cfc-ac6c-baeedc479979"/>
				<textElement textAlignment="Center">
					<font isUnderline="true"/>
				</textElement>
				<text><![CDATA[DiPeriksa,]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="18" y="166" width="100" height="20" uuid="2c299d03-e809-4635-bf7d-83356e68b3e0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{Dibuat}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="248" y="166" width="100" height="20" uuid="70ac61e2-3f69-4398-b55c-0de5b6dbddf4"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{Diperiksa}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="459" y="166" width="99" height="20" uuid="efd98dd3-a6a0-4ed0-9258-991779c68ea7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{Disetujui}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
