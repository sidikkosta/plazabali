<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="NotaPembayaranTBS" language="groovy" pageWidth="792" pageHeight="684" orientation="Landscape" columnWidth="752" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="50b41e93-3b47-4a3d-a588-40ff9612ecfa">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["D:\\Data\\dev\\UntaCore_TMG\\com.unicore.finance\\src\\jasper\\"]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="BiayaAdm" class="java.math.BigDecimal">
		<defaultValueExpression><![CDATA[$P{BiayaAdm}==null ? 0 : $P{BiayaAdm}]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT bp.name AS vendor,
fk.documentno AS no,
odc.beneficiary AS an,
odc.accountno AS acct,
b.name AS bank,
wt.datedoc AS tgl,
wt.documentno AS notiket,
wt.referenceno AS nodo,
wt.vehicleno AS nopol,
wt.grossweight AS bruto,
fk.referenceno AS nofkv,
bp.taxid AS taxid,
bp.taxname AS taxnm,
bp.taxaddress AS taxad,
wt.tare AS tara,
wt.nettoi AS netto,
COALESCE (rev.revisionqty, wt.reflection) AS pot,
COALESCE (rev.revisionpercent, wt.reflection/wt.nettoi * 100) AS potpersen,
COALESCE (rev.qtypayment, wt.nettoii) AS terima,
fkl.pricelist AS satuan,
fkl.linenetamt AS total,
(fkl.linenetamt * tax.Rate) / 100 AS ppn,
wh.percent AS persen,
wh.taxamt AS pph,
fkl.qtyinvoiceticket AS QtyInvoiceTicket,
COALESCE(ull.amount,0) AS ongkos,
CASE WHEN cml.C_InvoiceLine_ID > 0 THEN COALESCE(cml.linenetamt,0) ELSE 0 END AS SUM

FROM C_Invoice fk
LEFT JOIN C_Invoice cm ON cm.CN_Invoice_ID=fk.C_Invoice_ID
LEFT JOIN C_InvoiceLine fkl ON fkl.C_Invoice_ID=fk.C_Invoice_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID=fk.C_BPartner_ID
LEFT JOIN C_Order od ON od.C_Order_ID=fk.C_Order_ID
LEFT JOIN UNS_Order_Contract odc ON odc.C_Order_ID=od.C_Order_ID
LEFT JOIN C_Bank b ON b.C_Bank_ID=odc.C_Bank_ID
INNER JOIN UNS_WeighbridgeTicket wt ON wt.C_InvoiceLine_ID=fkl.C_InvoiceLine_ID
LEFT JOIN  UNS_ReflectionRevision_Line rev ON rev.UNS_WeighbridgeTicket_ID=wt.UNS_WeighbridgeTicket_ID
LEFT JOIN LCO_InvoiceWithholding wh ON wh.C_InvoiceLine_ID=fkl.C_InvoiceLine_ID
LEFT JOIN UNS_UnLoadingCost_Line ull ON ull.UNS_UnLoadingCost_Line_ID=wt.UNS_UnLoadingCost_Line_ID
LEFT JOIN C_InvoiceLine cml ON cml.C_Invoice_ID=cm.C_Invoice_ID AND cml.C_Charge_ID IS NOT NULL
INNER JOIN C_Tax tax ON tax.C_Tax_ID = fkl.C_Tax_ID

WHERE rev.uns_reflectionrevision_line_id IN
(select rev.uns_reflectionrevision_line_id from uns_reflectionrevision_line rev where rev.uns_reflectionrevision_id =
(select rl.uns_reflectionrevision_id from uns_reflectionrevision_line rl
inner join uns_reflectionrevision r on r.uns_reflectionrevision_id = rl.uns_reflectionrevision_id where rl.uns_weighbridgeticket_id = rev.uns_weighbridgeticket_id order by r.created desc limit 1)) AND fk.dateinvoiced BETWEEN $P{DateFrom} AND $P{DateTo} AND fk.isSOTrx='N' AND fk.docstatus IN ('CO','CL') AND (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN fk.C_BPartner_ID = $P{C_BPartner_ID} ELSE 1=1 END)
ORDER BY tgl, notiket]]>
	</queryString>
	<field name="vendor" class="java.lang.String"/>
	<field name="no" class="java.lang.String"/>
	<field name="an" class="java.lang.String"/>
	<field name="acct" class="java.lang.String"/>
	<field name="bank" class="java.lang.String"/>
	<field name="tgl" class="java.sql.Timestamp"/>
	<field name="notiket" class="java.lang.String"/>
	<field name="nodo" class="java.lang.String"/>
	<field name="nopol" class="java.lang.String"/>
	<field name="bruto" class="java.math.BigDecimal"/>
	<field name="nofkv" class="java.lang.String"/>
	<field name="taxid" class="java.lang.String"/>
	<field name="taxnm" class="java.lang.String"/>
	<field name="taxad" class="java.lang.String"/>
	<field name="tara" class="java.math.BigDecimal"/>
	<field name="netto" class="java.math.BigDecimal"/>
	<field name="pot" class="java.math.BigDecimal"/>
	<field name="potpersen" class="java.math.BigDecimal"/>
	<field name="terima" class="java.math.BigDecimal"/>
	<field name="satuan" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="ppn" class="java.math.BigDecimal"/>
	<field name="persen" class="java.math.BigDecimal"/>
	<field name="pph" class="java.math.BigDecimal"/>
	<field name="qtyinvoiceticket" class="java.math.BigDecimal"/>
	<field name="ongkos" class="java.math.BigDecimal"/>
	<field name="sum" class="java.math.BigDecimal"/>
	<variable name="bruto_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{bruto}]]></variableExpression>
	</variable>
	<variable name="tara_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{tara}]]></variableExpression>
	</variable>
	<variable name="netto_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{netto}]]></variableExpression>
	</variable>
	<variable name="pot_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{pot}]]></variableExpression>
	</variable>
	<variable name="potpersen_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{potpersen}]]></variableExpression>
	</variable>
	<variable name="terima_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{terima}]]></variableExpression>
	</variable>
	<variable name="total_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<variable name="ppn_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{ppn}]]></variableExpression>
	</variable>
	<variable name="persen_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{persen}]]></variableExpression>
	</variable>
	<variable name="ongkos_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{ongkos}]]></variableExpression>
	</variable>
	<variable name="pph_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{pph}]]></variableExpression>
	</variable>
	<variable name="variable1" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{total_1}.add($V{ppn_1}).subtract($V{pph_1}).subtract($V{ongkos_1})]]></variableExpression>
	</variable>
	<variable name="variable2" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{variable1}.subtract($F{sum})]]></variableExpression>
	</variable>
	<variable name="sum_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Vendor" calculation="Sum">
		<variableExpression><![CDATA[$F{sum}]]></variableExpression>
	</variable>
	<variable name="variable3" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{variable1}-($P{BiayaAdm})]]></variableExpression>
	</variable>
	<variable name="variable4" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{total_1}.add($V{ppn_1})]]></variableExpression>
	</variable>
	<group name="Vendor">
		<groupExpression><![CDATA[$F{vendor}]]></groupExpression>
		<groupHeader>
			<band height="112">
				<staticText>
					<reportElement x="2" y="5" width="253" height="20" uuid="8dd4f1a9-e123-4091-a746-507379e4d0c6"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isUnderline="true"/>
					</textElement>
					<text><![CDATA[INFORMASI SUPPLIER]]></text>
				</staticText>
				<textField>
					<reportElement x="2" y="25" width="401" height="20" uuid="6da066d0-a86d-498f-b58b-f43fbd6d0781"/>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{vendor}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="41" y="43" width="362" height="20" uuid="7cf1bed9-a51a-4304-9407-52a2ddd962c6"/>
					<textFieldExpression><![CDATA[" :  " +$F{an}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="41" y="59" width="362" height="20" uuid="8ff10ab2-1785-42ef-a1d9-4f66adf09641"/>
					<textFieldExpression><![CDATA[" :  " +$F{acct}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="2" y="43" width="100" height="20" uuid="16f1c4fe-a5f0-416d-b4b5-09829d7167e3"/>
					<text><![CDATA[A/N]]></text>
				</staticText>
				<staticText>
					<reportElement x="2" y="59" width="100" height="20" uuid="b430952f-b9a1-40ab-a53a-c808646a9beb"/>
					<text><![CDATA[Account]]></text>
				</staticText>
				<staticText>
					<reportElement x="2" y="76" width="100" height="20" uuid="503edbeb-bac7-49cb-a48b-3925b2a49326"/>
					<text><![CDATA[Bank]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="41" y="76" width="362" height="20" uuid="f1fbfc91-1391-4393-a160-baee181100f2"/>
					<textFieldExpression><![CDATA[$F{bank}==null ? " :  -" : " :  " +$F{bank}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="475" y="43" width="100" height="20" uuid="81b27da1-d5a4-4270-ab92-9db45b3e2d16"/>
					<text><![CDATA[A/N                ]]></text>
				</staticText>
				<textField>
					<reportElement x="500" y="60" width="240" height="20" uuid="dfc67da5-ad1e-442d-9e15-af81ee009309"/>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[" :  " +$F{taxid}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="475" y="62" width="100" height="20" uuid="16257c03-0a24-417c-b0ee-781119bc1af6"/>
					<text><![CDATA[NPWP ID  ]]></text>
				</staticText>
				<textField>
					<reportElement x="500" y="45" width="240" height="20" uuid="d70e234b-9624-4912-93d7-1d509b2ca5d8"/>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[" :  " +$F{taxnm}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="146">
				<textField pattern="#,##0;(#,##0)">
					<reportElement x="604" y="39" width="137" height="13" uuid="abe29747-f4bb-4bfd-b38b-e3f3b2600610"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ppn_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="508" y="101" width="101" height="13" uuid="7f015d75-9820-4903-8d6e-4dddc5550401"/>
					<textElement>
						<font size="9"/>
					</textElement>
					<text><![CDATA[Biaya Administrasi]]></text>
				</staticText>
				<staticText>
					<reportElement x="508" y="123" width="98" height="20" uuid="c0ca39b7-39de-41ae-9566-7bf36cc1589b"/>
					<textElement>
						<font size="9" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Invoice]]></text>
				</staticText>
				<textField pattern="(#,##0)">
					<reportElement x="603" y="101" width="137" height="13" uuid="833ca986-bdcb-4c5e-a318-5ec4c58f213f"/>
					<textElement textAlignment="Right">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{BiayaAdm}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="602" y="123" width="137" height="20" uuid="a88e3b16-e787-497b-8520-5c49517a584d"/>
					<textElement textAlignment="Right">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{variable3}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="604" y="26" width="137" height="13" uuid="d8cd1a82-ff46-4919-9f61-402fbaba27db"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="507" y="26" width="101" height="13" uuid="1504e301-93ab-46d8-a119-96421eb91ded"/>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Total]]></text>
				</staticText>
				<staticText>
					<reportElement x="507" y="39" width="101" height="13" uuid="2040264e-d7ae-4fbe-b2a0-a75e39a3da85"/>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[PPN]]></text>
				</staticText>
				<line>
					<reportElement x="488" y="55" width="252" height="1" uuid="4077132c-403a-4df6-bf6e-4a63306f4b83"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement x="489" y="39" width="18" height="20" uuid="7277853b-14e7-44ea-b373-cca5b63bda24"/>
					<text><![CDATA[+]]></text>
				</staticText>
				<staticText>
					<reportElement x="508" y="61" width="101" height="13" uuid="06d2fb3e-42fb-4cf5-b052-0ed7ed97eee0"/>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<text><![CDATA[Sub Total]]></text>
				</staticText>
				<textField pattern="#,##0">
					<reportElement x="603" y="61" width="137" height="13" uuid="e3152385-aef2-4aa7-af9d-3affbb98729e"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{variable4}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="508" y="75" width="101" height="13" uuid="3ada952f-de0a-4093-9a4a-e1177b7d8c0a"/>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[PPh]]></text>
				</staticText>
				<textField>
					<reportElement x="517" y="75" width="54" height="13" uuid="91396483-b151-4d86-8ddb-97686b12adb6"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{persen}.setScale( 2, java.math.RoundingMode.HALF_UP )]]></textFieldExpression>
				</textField>
				<textField pattern="(#,##0)">
					<reportElement x="603" y="75" width="137" height="13" uuid="80317ad7-7344-4c10-b866-038af01cb854"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{pph_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="508" y="88" width="101" height="13" uuid="c6fea0d8-42c3-4d52-9e05-0956b2b6cdf8"/>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Ongkos Bongkar]]></text>
				</staticText>
				<textField pattern="(#,##0)">
					<reportElement x="604" y="88" width="136" height="13" uuid="6c19721e-707c-4ba0-87fa-9ca874716cad"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ongkos_1}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="488" y="118" width="252" height="1" uuid="4a03f1e3-0585-4e2b-8276-1ab033c39e57"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement x="490" y="103" width="18" height="20" uuid="4141f27f-6751-41e3-8547-fe55bbb2dc31"/>
					<text><![CDATA[-]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="TGL">
		<groupExpression><![CDATA[$F{tgl}]]></groupExpression>
		<groupHeader>
			<band height="29">
				<rectangle>
					<reportElement x="1" y="0" width="49" height="29" uuid="fd8d922f-9172-4c1c-a74a-7a47cdda5900"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="49" y="0" width="41" height="29" uuid="b989c418-b40b-4ef1-9948-6d3d174d8975"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="1" y="1" width="49" height="25" uuid="ded06047-28f5-4640-bd47-650cd8d04edf"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[TGL.]]></text>
				</staticText>
				<staticText>
					<reportElement x="49" y="1" width="41" height="25" uuid="e2894eb3-da88-414a-85c8-492e010640c3"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[No Tiket]]></text>
				</staticText>
				<rectangle>
					<reportElement x="146" y="0" width="49" height="29" uuid="6446771d-8939-4314-b6ac-1037d6605d58"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="89" y="0" width="57" height="29" uuid="2daf0e1c-ffee-43cf-9340-c1a1c6311253"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="146" y="1" width="49" height="25" uuid="d3695f21-3fc3-435d-ace9-580f9039c3ea"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[No Inv]]></text>
				</staticText>
				<staticText>
					<reportElement x="90" y="1" width="58" height="25" uuid="e8a814b5-feb8-4572-9907-6097b24da166"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[No DO]]></text>
				</staticText>
				<rectangle>
					<reportElement x="421" y="0" width="42" height="29" uuid="d7025593-c683-4f41-aa1e-82f45a5559f2"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="379" y="0" width="42" height="29" uuid="4726aa6a-8f94-4cbf-9348-f1df92dc28e4"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="340" y="0" width="39" height="29" uuid="1579c99c-12cc-43da-abb1-603b3f2fa203"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="253" y="0" width="45" height="29" uuid="ab98b3e8-9738-4d13-8969-b6e170a6fa27"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="253" y="-1" width="45" height="17" uuid="ca56fc5e-04e0-4058-ab93-068b6dbcc62d"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Bruto]]></text>
				</staticText>
				<staticText>
					<reportElement x="379" y="-1" width="42" height="15" uuid="d2b5920e-a32e-4dd1-a628-1b9178e9da2c"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Pot]]></text>
				</staticText>
				<staticText>
					<reportElement x="422" y="-1" width="41" height="15" uuid="9c038ab1-2a43-44a0-ada2-8879340eade0"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Pot]]></text>
				</staticText>
				<staticText>
					<reportElement x="340" y="-1" width="39" height="15" uuid="38faf49c-1d93-42a2-931a-74eb20ab1062"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Netto I]]></text>
				</staticText>
				<rectangle>
					<reportElement x="463" y="0" width="40" height="29" uuid="b9fb4ab8-914a-47b5-940a-0885c2536094"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="463" y="-1" width="40" height="15" uuid="9b4b4821-7aab-46c4-98a4-e69fd6d51daa"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Netto II]]></text>
				</staticText>
				<rectangle>
					<reportElement x="503" y="0" width="47" height="29" uuid="f50de942-c669-48b2-80cf-2ea1a67b3c1a"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="503" y="-1" width="47" height="15" uuid="9ad3a38f-c0bb-4724-bca5-c56cfd2f8a52"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Harga @]]></text>
				</staticText>
				<rectangle>
					<reportElement x="549" y="0" width="67" height="29" uuid="4d123b9f-a0a0-4d8f-9c18-5758d6f70869"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="550" y="-1" width="67" height="15" uuid="5878abbf-5867-418f-918b-c2cbc8c4e1a0"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
				<rectangle>
					<reportElement x="616" y="0" width="54" height="29" uuid="caed48d3-602e-4a70-a241-ee53482adead"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="670" y="0" width="77" height="29" uuid="47d61b2d-25a7-4043-90fd-442eea58ef4f"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="670" y="1" width="77" height="12" uuid="7c52b941-8066-41a0-bd13-fb4f86327613"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Ongkos Bongkar]]></text>
				</staticText>
				<rectangle>
					<reportElement x="298" y="0" width="42" height="29" uuid="347763af-9dfb-446d-a2f6-fb91c62590ab"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="298" y="-1" width="42" height="17" uuid="5969f2e8-9f21-46df-a159-ac546618fbc2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Tara]]></text>
				</staticText>
				<staticText>
					<reportElement x="253" y="15" width="45" height="13" uuid="37f95171-8f55-41ba-9682-bee31cf97b21"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kg]]></text>
				</staticText>
				<staticText>
					<reportElement x="298" y="15" width="42" height="13" uuid="cdc6188b-902a-4edf-88f9-ed72bbfc2e1a"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kg]]></text>
				</staticText>
				<staticText>
					<reportElement x="340" y="15" width="38" height="13" uuid="44574d92-d710-4db2-b25e-b8ab297fb00d"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kg]]></text>
				</staticText>
				<staticText>
					<reportElement x="379" y="15" width="42" height="13" uuid="623f0ca4-b7f2-4aa3-9d8a-6628026e767e"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kg]]></text>
				</staticText>
				<staticText>
					<reportElement x="421" y="16" width="42" height="13" uuid="e9097e1b-4b9a-4f51-9ead-82c68e9fbda6"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[%]]></text>
				</staticText>
				<staticText>
					<reportElement x="461" y="15" width="42" height="13" uuid="0c315779-860a-4ab3-8368-5618cf832d70"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kg]]></text>
				</staticText>
				<staticText>
					<reportElement x="507" y="16" width="38" height="13" uuid="6d7bf957-afaf-437f-80fc-fc7f95d79ba8"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Rp]]></text>
				</staticText>
				<staticText>
					<reportElement x="551" y="16" width="66" height="13" uuid="6ddb8ef8-5210-4168-9bb7-65fb8b2dcb1b"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Rp]]></text>
				</staticText>
				<textField>
					<reportElement x="616" y="16" width="54" height="13" uuid="f0cc465f-2800-42cd-a457-7981fa7e2771"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{persen}.setScale( 2, java.math.RoundingMode.HALF_UP ) +"%"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="670" y="15" width="77" height="13" uuid="3b9f6272-bc21-49ff-9638-fdcd7e8374c4"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Rp]]></text>
				</staticText>
				<line>
					<reportElement x="233" y="16" width="514" height="1" uuid="c27900f5-76b3-4c3e-9266-bcb5b04c6942"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement x="617" y="1" width="54" height="15" uuid="014f7f6f-71c9-443a-9334-d3ab7938a188"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[PPH]]></text>
				</staticText>
				<rectangle>
					<reportElement x="195" y="0" width="58" height="29" uuid="f77d4826-a15d-424d-9240-13911311533f"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="195" y="0" width="58" height="28" uuid="c7f097ef-68a8-4aa0-a9d8-868d9910d46f"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[NOPOL]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="16">
				<textField pattern="#,##0">
					<reportElement x="255" y="1" width="40" height="15" uuid="4e5e6600-add0-4219-9fee-09b9a46a1765"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{bruto_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="300" y="0" width="36" height="15" uuid="e2f3d9f9-d79c-4751-99c0-4f95f54f82c1"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{tara_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.0">
					<reportElement x="422" y="0" width="38" height="15" uuid="1091da8c-3386-4dfa-a79a-c6f6bb886343"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{potpersen_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="546" y="1" width="67" height="15" uuid="44d46009-5ead-41d0-9511-ae344cb6fb3b"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="670" y="0" width="74" height="16" uuid="ac6c5448-820a-4e46-9006-cc6b81634ad8"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ongkos_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="2" y="0" width="275" height="15" uuid="91cc8d23-2104-49be-8921-bf00f1ba09b5"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
				<line>
					<reportElement x="1" y="15" width="746" height="1" uuid="2e1abf09-3673-429d-82a8-920bd7142751"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0">
					<reportElement x="466" y="0" width="35" height="15" uuid="ab69147a-63a0-4cb7-b995-5eb81c5284f4"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{terima_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="379" y="0" width="39" height="15" uuid="b75f06d5-14e3-4951-a87d-925df864f67f"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{pot_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="341" y="0" width="34" height="15" uuid="05a1a2a2-ced1-4e4c-b93d-646fc9aeba1f"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{netto_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="616" y="0" width="52" height="15" uuid="345b6697-fd19-4755-a1f2-775232f47026"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{pph_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="66">
			<staticText>
				<reportElement x="0" y="0" width="752" height="20" uuid="6713df69-5aab-44c7-bd76-4d6a33e1e289"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[PT. SUTOPO LESTARI JAYA]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="752" height="20" uuid="76dd00fe-086d-464f-9456-c828cd1c66db"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[NOTA PEMBAYARAN TBS]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="41" width="752" height="20" uuid="ddbaeb9d-67a3-4279-aac7-1b79ecc72d59"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[" ("+$F{nofkv}+ ") "]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<textField pattern="#,##0">
				<reportElement x="341" y="0" width="34" height="15" uuid="24554d29-deb6-49a4-9d50-2f6dda0e4a2c"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{netto}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="465" y="1" width="35" height="15" uuid="fcf240af-4bd7-4108-bf49-507fb4d025c4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{terima}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="615" y="1" width="52" height="15" uuid="a5df5a3c-7f01-4458-8563-6e1b12890574"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pph}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="300" y="0" width="36" height="15" uuid="9478a8dc-aac7-4b73-99db-db82507c9b81"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tara}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="255" y="1" width="39" height="15" uuid="de3780c7-70f8-4f1d-b9e6-2c4c54b116fc"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{bruto}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="48" y="1" width="40" height="15" uuid="6fd3b06d-9f4c-463c-bc89-456060dcf876"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{notiket}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="1" y="1" width="47" height="15" uuid="c6945975-0a86-4bd7-beaf-e03e3fce618b"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tgl}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="88" y="1" width="56" height="15" uuid="b8f9b899-2b90-46ff-b51a-6e240a0e7957"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nodo}==null ? "0" :$F{nodo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="195" y="1" width="56" height="15" uuid="8ed7b2be-d451-4741-8505-f89d1d182ed9"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nopol}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="378" y="1" width="39" height="15" uuid="85a52f84-8fc2-4d34-bf9b-26e0fb574267"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pot}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="550" y="1" width="62" height="15" uuid="a38fc229-be33-45a3-99bb-bef0099da2f7"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0">
				<reportElement x="421" y="1" width="38" height="15" uuid="d6ce18fd-b10c-457c-a157-7e2d7b2bf338"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{potpersen}.setScale( 2, java.math.RoundingMode.HALF_UP )]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="670" y="1" width="73" height="15" uuid="ad9070c2-6713-4e8c-bf5b-3abe6f49102c"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ongkos}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="504" y="1" width="44" height="15" uuid="f7d9b11a-252e-471f-9a4d-32a6b84df53d"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{satuan}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="1" y="15" width="745" height="1" uuid="9765b8dd-19e3-48a9-b29e-0161335a0009"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement x="147" y="0" width="44" height="15" uuid="e4e94206-2927-4d83-bc5d-d8c2c6ee8e95"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{no}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
