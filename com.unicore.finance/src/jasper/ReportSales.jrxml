<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportSales" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="2b903df2-50e3-40ca-bf67-9d07a7f74d3a">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.sql.Timestamp"/>
	<parameter name="DateTo" class="java.sql.Timestamp"/>
	<parameter name="AD_Org_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SalesRep_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT * FROM (SELECT
bp.name AS namatoko,
bg.name AS nobukti,
fk.documentno AS nofk,
fk.grandtotal AS nilaifk,
fk.dateinvoiced AS tglfk,
bg.receiptdate AS tgl,
fklist.paidamt AS paid,
(Case When bga.action = 'DS' Then 'Giro Cair'
      when bga.action = 'HB' Then 'Giro Ditahan'
      when bga.action = 'NN' Then 'Giro Belum Diproses'
      when bga.action = 'RF' Then 'Giro Ditolak'
      when bga.action = 'RG' Then 'Giro Dikembalikan'
  end) as des,
ba.name AS anbank,
bk.name AS bank,
bg.limitamt AS total,
'Giro' As tipepenerimaan,
au.name AS sales


FROM c_invoice fk
INNER JOIN uns_customerbg_invlist fklist ON fklist.c_invoice_id=fk.c_invoice_id
INNER JOIN uns_customerbg bg ON bg.uns_customerbg_id=fklist.uns_customerbg_id
INNER JOIN uns_customerbg_action bga ON bga.uns_customerbg_id=bg.uns_customerbg_id
INNER JOIN c_bpartner bp ON bp.c_bpartner_id=bg.c_bpartner_id
LEFT JOIN c_bankaccount ba ON ba.c_bankaccount_id=bga.c_bankaccount_id
LEFT JOIN c_bank bk ON bk.c_bank_id=ba.c_bank_id
INNER JOIN ad_user au ON au.ad_user_id=bg.salesrep_id

WHERE
fk.AD_Org_ID=$P{AD_Org_ID} AND
fk.isSOtrx='Y' AND
au.ad_user_id=$P{SalesRep_ID} AND
bg.receiptdate BETWEEN $P{DateFrom} AND $P{DateTo}

UNION ALL

SELECT
bpr.name AS namatoko,
py.documentno AS nobukti,
fkp.documentno AS nofk,
fkp.grandtotal AS nilaifk,
fkp.dateinvoiced AS tglfk,
py.datetrx AS tgl,
pa.paytooverunderamount AS paid,
py.description AS des,
ba.name AS anbank,
bk.name AS bank,
py.payamt AS total,
'C/T' As tipepenerimaan,
au.name AS sales


FROM c_invoice fkp
INNER JOIN C_PaymentAllocate pa ON pa.c_invoice_id=fkp.c_invoice_id
INNER JOIN c_payment py ON py.c_payment_id=pa.c_payment_id AND py.tendertype='X'
INNER JOIN c_bpartner bpr ON bpr.c_bpartner_id=py.c_bpartner_id
LEFT JOIN c_bankaccount ba ON ba.c_bankaccount_id=py.c_bankaccount_id
LEFT JOIN c_bank bk ON bk.c_bank_id=ba.c_bank_id
INNER JOIN ad_user au ON au.ad_user_id=py.salesrep_id

WHERE
fkp.AD_Org_ID=$P{AD_Org_ID} AND
fkp.isSOtrx='Y' AND
py.docstatus IN ('CO', 'CL') AND
au.ad_user_id=$P{SalesRep_ID} AND
py.datetrx BETWEEN $P{DateFrom} AND $P{DateTo})
AS PenerimaanPiutangBulanan,
now () AS tglprint
ORDER BY sales, tgl, namatoko, nobukti, tipepenerimaan, tglfk]]>
	</queryString>
	<field name="namatoko" class="java.lang.String"/>
	<field name="nobukti" class="java.lang.String"/>
	<field name="nofk" class="java.lang.String"/>
	<field name="nilaifk" class="java.math.BigDecimal"/>
	<field name="tglfk" class="java.sql.Timestamp"/>
	<field name="tgl" class="java.sql.Timestamp"/>
	<field name="paid" class="java.math.BigDecimal"/>
	<field name="des" class="java.lang.String"/>
	<field name="anbank" class="java.lang.String"/>
	<field name="bank" class="java.lang.String"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="tipepenerimaan" class="java.lang.String"/>
	<field name="sales" class="java.lang.String"/>
	<field name="tglprint" class="java.sql.Timestamp"/>
	<variable name="total_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Customer" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<variable name="paid_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{paid}]]></variableExpression>
	</variable>
	<variable name="total_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<group name="Customer">
		<groupExpression><![CDATA[$F{namatoko}]]></groupExpression>
		<groupHeader>
			<band height="17">
				<textField>
					<reportElement x="0" y="0" width="506" height="16" uuid="bac1fc9d-ac2d-4892-ad54-9c59c584b0aa"/>
					<textElement>
						<font isBold="true" isItalic="true" isUnderline="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{namatoko}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="4">
				<line>
					<reportElement x="0" y="1" width="553" height="1" uuid="779fe346-e739-44f4-a2ca-8514aef284b9"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="Bukti">
		<groupExpression><![CDATA[$F{nobukti}]]></groupExpression>
		<groupHeader>
			<band height="16">
				<textField pattern="dd/MM/yyyy">
					<reportElement x="126" y="0" width="88" height="15" uuid="11c95e18-7f74-4bcf-b7a0-0c6e81f313a2"/>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{tgl}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="0" y="0" width="120" height="15" uuid="ce2fa700-6491-4f4f-9968-1d0af145356b"/>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{nobukti}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="342" y="0" width="126" height="15" uuid="47d5ca37-3a77-46be-83af-8caabd2f2e5f"/>
					<textElement textAlignment="Right">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{des}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="469" y="0" width="82" height="15" uuid="c0e31865-3f8c-4c75-9b21-e76ea22525d5"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="220" y="0" width="115" height="15" uuid="a6d2b3d1-3299-4192-b0e8-d1b7e641836a"/>
					<textElement>
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{bank}!=null ? $F{bank}+' - '+$F{anbank} : ' ']]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="55">
			<staticText>
				<reportElement x="0" y="0" width="555" height="20" uuid="a00723b4-049c-47da-909c-e8ae568366e7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="13" isBold="true" isUnderline="true"/>
				</textElement>
				<text><![CDATA[DAFTAR PENERIMAAN TAGIHAN SALES]]></text>
			</staticText>
			<staticText>
				<reportElement x="337" y="34" width="44" height="20" uuid="df1818a4-d7b7-40bc-9e38-46c9a87fa349"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periode :]]></text>
			</staticText>
			<textField>
				<reportElement x="380" y="34" width="175" height="20" uuid="d117b065-4688-4e18-a4dd-bd2aacb9fb1c"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MM-yyyy").format($P{DateFrom}) + ' s/d ' +new SimpleDateFormat("dd-MM-yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="455" y="0" width="100" height="15" uuid="7c941a83-8e89-436d-a71c-2bb477e5ec3b"/>
				<textElement textAlignment="Right">
					<font size="8" isItalic="true"/>
				</textElement>
				<text><![CDATA[UntaCore Idempiere]]></text>
			</staticText>
			<textField>
				<reportElement x="36" y="35" width="186" height="20" uuid="da07c270-5fa4-4f6e-b24d-bede1420b303"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{sales}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="35" width="37" height="20" uuid="53c1cda3-b8cc-4e89-84fb-fe39e6651f47"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Sales :]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="49">
			<staticText>
				<reportElement x="0" y="16" width="120" height="15" uuid="14d24827-482a-49e2-bfd6-5f50d053a5d3"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[No. Bukti]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="31" width="120" height="15" uuid="19efe985-6ae0-4785-b513-4e70bcb17744"/>
				<textElement verticalAlignment="Top"/>
				<text><![CDATA[No. Faktur]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="0" width="555" height="1" uuid="c6690bab-a518-42ab-a1a3-8f3d25743aec"/>
			</line>
			<line>
				<reportElement x="0" y="47" width="555" height="1" uuid="f0793ae9-c831-4a81-a869-144d088794cb"/>
			</line>
			<staticText>
				<reportElement x="127" y="31" width="88" height="15" uuid="a9741fb7-ea66-4baf-9bc8-e74a7f11e431"/>
				<textElement verticalAlignment="Top"/>
				<text><![CDATA[Tgl. Faktur]]></text>
			</staticText>
			<staticText>
				<reportElement x="127" y="16" width="88" height="15" uuid="6288817a-9e67-458d-b397-a3662953af43"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Tgl]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="2" width="342" height="15" uuid="14f69b58-2891-4169-b719-8e115b776d55"/>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[Pelanggan]]></text>
			</staticText>
			<staticText>
				<reportElement x="342" y="17" width="81" height="15" uuid="28c7d114-d6ab-4de4-8390-a8a2b11e146c"/>
				<textElement verticalAlignment="Top">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Keterangan]]></text>
			</staticText>
			<staticText>
				<reportElement x="472" y="17" width="81" height="15" uuid="47329706-1627-4bc8-a79f-dbb5fa210b8a"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Grand Total]]></text>
			</staticText>
			<staticText>
				<reportElement x="361" y="32" width="90" height="15" uuid="cce2e897-628e-49f3-838b-a9bfa0f849aa"/>
				<textElement verticalAlignment="Top">
					<font isBold="false"/>
				</textElement>
				<text><![CDATA[Tipe Penerimaan]]></text>
			</staticText>
			<staticText>
				<reportElement x="241" y="31" width="88" height="15" uuid="96bf5e19-8972-4de6-80f0-3a9cc0998cc7"/>
				<textElement verticalAlignment="Top"/>
				<text><![CDATA[Nilai Faktur]]></text>
			</staticText>
			<staticText>
				<reportElement x="472" y="31" width="81" height="15" uuid="da1fb9ec-381a-47cb-ba9b-4fa03bb7b587"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font isBold="false"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<textField pattern="dd/MM/yyyy">
				<reportElement x="126" y="1" width="91" height="15" uuid="c4f90149-2d30-4a9b-9ce6-38caba2c4322"/>
				<textFieldExpression><![CDATA[$F{tglfk}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="1" width="120" height="15" uuid="8c1fcb02-4c09-4f7e-8c1a-019b8e7374e0"/>
				<textFieldExpression><![CDATA[$F{nofk}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="464" y="0" width="87" height="15" uuid="2c802d0b-3540-483f-a805-7922a5e94178"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{paid}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="414" y="1" width="46" height="15" uuid="a779126c-7c6f-439e-b12f-37e0237a6207"/>
				<textFieldExpression><![CDATA[$F{tipepenerimaan}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="220" y="1" width="77" height="15" uuid="a25ddabb-cfce-4077-9a5a-7908ac76dcfc"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{nilaifk}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="16">
			<textField>
				<reportElement x="0" y="0" width="172" height="15" uuid="f018d216-7df2-4ded-98de-1830fb86be39"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Print Date : " +new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format($F{tglprint})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="452" y="0" width="78" height="15" uuid="87437022-9a73-482c-9274-38a6ebe7939d"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="532" y="0" width="38" height="15" uuid="933c3de3-fe1e-4e3a-9c55-bc9327f35763"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<summary>
		<band height="43">
			<staticText>
				<reportElement x="397" y="10" width="68" height="20" uuid="a1a5e2c1-dba7-4419-be49-9e6f04f828c0"/>
				<text><![CDATA[Total Tagihan :]]></text>
			</staticText>
			<textField pattern="#,##0.-">
				<reportElement x="464" y="10" width="91" height="20" uuid="1ec02b85-d5d5-49fa-805b-b4e6b218a2cd"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$V{total_2}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
