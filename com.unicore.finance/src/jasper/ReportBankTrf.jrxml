<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportBankTrf" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="eeaa9cc6-3185-4454-bb72-7d3c5bf5ea59">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="91"/>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="ARBankAccount_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="APBankAccount_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="AD_Org_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
AP.DateTrx AS DateAP,
AP.DocumentNo AS AP,
AR.DocumentNo AS AR,
APBank.Value AS APAccount,
ARBank.Value AS ARAccount,
AP.Description AS Desc,
AP.Payamt AS apamt,
C.Name AS Client,
Ch.Name AS apcharge,
BP.Name AS user

FROM C_Payment AP
INNER JOIN AD_Client C ON C.AD_Client_ID=AP.AD_Client_ID
INNER JOIN C_Charge Ch ON Ch.C_Charge_ID=AP.C_Charge_ID
INNER JOIN C_Payment AR ON AR.C_Payment_ID=AP.Reference_ID
INNER JOIN C_BankAccount APBank ON APBank.C_BankAccount_ID=AP.C_BankAccount_ID
INNER JOIN C_BankAccount ARBank ON ARBank.C_BankAccount_ID=AR.C_BankAccount_ID
INNER JOIN C_BPartner BP ON BP.C_BPartner_ID=AP.C_BPartner_ID

WHERE AP.DocStatus IN ('CO', 'CL') AND (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN AP.DateTrx BETWEEN $P{DateFrom}::timestamp AND $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NOT NULL THEN AP.DateTrx >= $P{DateFrom}::timestamp WHEN $P{DateTo}::timestamp IS NOT NULL THEN AP.DateTrx <= $P{DateTo}::timestamp ELSE 1=1 END) AND (CASE WHEN $P{ARBankAccount_ID} IS NOT NULL THEN AR.C_BankAccount_id=$P{ARBankAccount_ID} ELSE 1=1 END) AND (CASE WHEN $P{APBankAccount_ID} IS NOT NULL THEN AP.C_BankAccount_id=$P{APBankAccount_ID} ELSE 1=1 END)  AND (CASE WHEN  $P{C_BPartner_ID} IS NOT NULL THEN $P{C_BPartner_ID}=BP.C_BPartner_ID ElSE 1=1 END)]]>
	</queryString>
	<field name="dateap" class="java.sql.Timestamp"/>
	<field name="ap" class="java.lang.String"/>
	<field name="ar" class="java.lang.String"/>
	<field name="apaccount" class="java.lang.String"/>
	<field name="araccount" class="java.lang.String"/>
	<field name="desc" class="java.lang.String"/>
	<field name="apamt" class="java.math.BigDecimal"/>
	<field name="client" class="java.lang.String"/>
	<field name="apcharge" class="java.lang.String"/>
	<field name="user" class="java.lang.String"/>
	<variable name="number" class="java.lang.Integer" calculation="Count">
		<variableExpression><![CDATA[$V{number} == null ? 1 : $V{number} + 1]]></variableExpression>
	</variable>
	<variable name="apamt_1" class="java.math.BigDecimal" resetType="Group" resetGroup="C_BPartner_ID" calculation="Sum">
		<variableExpression><![CDATA[$F{apamt}]]></variableExpression>
	</variable>
	<group name="C_BPartner_ID">
		<groupExpression><![CDATA[$F{user}]]></groupExpression>
		<groupHeader>
			<band height="69">
				<textField>
					<reportElement x="71" y="23" width="100" height="20" uuid="de465cf3-6425-4b50-8b42-ea328677433e"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{DateFrom} != null && $P{DateTo} != null ? new SimpleDateFormat("dd-MM-yyyy").format($P{DateFrom})
+ " - " + new SimpleDateFormat("dd-MM-yyyy").format($P{DateTo}) :

$P{DateFrom} == null && $P{DateTo} != null ? "Beginning Times Until "
+ new SimpleDateFormat("dd-MM-yyyy").format($P{DateTo}) :

$P{DateFrom} != null && $P{DateTo} == null ?
new SimpleDateFormat("dd-MM-yyyy").format($P{DateFrom}) + " Until Now"
: "All Times"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="61" y="23" width="10" height="20" uuid="c3595d88-9de6-4449-a787-fb747e3ec151"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="83" y="49" width="51" height="20" uuid="d9657f8a-ef60-44e0-9771-7b2d5c0c3b59"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Payment AP]]></text>
				</staticText>
				<staticText>
					<reportElement x="134" y="49" width="53" height="20" uuid="5598dac2-6498-4408-b9e9-e619b2a1b286"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Payment AR]]></text>
				</staticText>
				<staticText>
					<reportElement x="486" y="49" width="69" height="20" uuid="21b78163-e92d-4279-b636-88e8f17a912a"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Ammount]]></text>
				</staticText>
				<staticText>
					<reportElement x="187" y="49" width="113" height="20" uuid="6b801c96-fcc0-4f5f-86c3-774bd8dcf2a7"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Description]]></text>
				</staticText>
				<staticText>
					<reportElement x="300" y="49" width="91" height="20" uuid="b9ed10e6-df6a-4b85-92e8-03e992079f44"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Account From]]></text>
				</staticText>
				<staticText>
					<reportElement x="391" y="49" width="95" height="20" uuid="8a815e1a-d293-4008-861c-3cb3c03d5796"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Account To]]></text>
				</staticText>
				<staticText>
					<reportElement x="21" y="49" width="62" height="20" uuid="c422aa2f-d9aa-46cc-a306-cf03e6fe38ab"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Date Payment]]></text>
				</staticText>
				<staticText>
					<reportElement x="5" y="23" width="53" height="20" uuid="b39af968-a362-47d3-9498-7db481215c0b"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Date Range  ]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="49" width="21" height="20" uuid="597fe11b-63b7-41d8-94a4-abfc08d6c6c2"/>
					<box leftPadding="3" rightPadding="3">
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[No.]]></text>
				</staticText>
				<staticText>
					<reportElement x="401" y="23" width="10" height="20" uuid="22464bf3-2db8-4e60-9eb4-5a018aac01e4"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<textField>
					<reportElement x="412" y="23" width="131" height="20" uuid="488e01e8-73ee-4e0d-b7a4-593e91047517"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{apcharge}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="348" y="23" width="53" height="20" uuid="187be45c-ed90-43e4-a373-89c86357ed90"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Charge]]></text>
				</staticText>
				<staticText>
					<reportElement x="61" y="3" width="10" height="20" uuid="9f85115e-b32f-4a68-9a2a-27eac733fd00"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="5" y="3" width="53" height="20" uuid="13a112d1-1b78-4f24-963a-d2ec983b0db2"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[User]]></text>
				</staticText>
				<textField>
					<reportElement x="71" y="3" width="100" height="20" uuid="8412846b-e0b5-4757-9a73-9b1d17990e3e"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{user}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement x="455" y="0" width="100" height="20" uuid="f3e11657-d544-4197-b32b-0eecd8b555cd"/>
					<box>
						<bottomPen lineWidth="0.75" lineStyle="Double"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{apamt_1}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="0" y="0" width="457" height="20" uuid="f9ef1d9e-2749-4276-ab91-ce82b436ecc9"/>
					<box>
						<bottomPen lineWidth="0.75" lineStyle="Double"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA["Total Transfer #"+$F{apaccount}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="60" splitType="Stretch">
			<textField>
				<reportElement x="0" y="15" width="555" height="25" uuid="8dd72767-3a07-4884-bb1c-931e03a74096"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="12" isBold="true" isUnderline="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{client}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="40" width="555" height="20" uuid="86347c7d-8c9c-48c0-9556-b49e84b26cf8"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="8" isItalic="true"/>
				</textElement>
				<text><![CDATA[Report Transfer Bank/Cash]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="30" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement x="83" y="0" width="51" height="30" uuid="cf7ae45c-a371-4990-bcd6-5d8792413b9d"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ap}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="134" y="0" width="53" height="30" uuid="9030acc8-2b71-4262-8217-83fe9840e412"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ar}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00">
				<reportElement x="486" y="0" width="69" height="30" uuid="1ae23172-e178-4f57-b9b6-55dca67744b4"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{apamt}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="0" y="0" width="21" height="30" uuid="be3c05df-f3d8-44eb-813e-f58eb73ac780"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="187" y="0" width="113" height="30" uuid="3ff3dd97-bd44-4861-b543-935cefe1baa0"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{desc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="300" y="0" width="91" height="30" uuid="b14ed78d-0aa4-462f-a49a-37c680030970"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{apaccount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="391" y="0" width="95" height="30" uuid="5f7b242c-a638-46c8-8c08-1690dbdcdecd"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{araccount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd MMMMM yyyy">
				<reportElement x="21" y="0" width="62" height="30" uuid="9bc22dfe-5a81-4021-8e01-fd3e053406bd"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dateap}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="50">
			<textField>
				<reportElement x="435" y="30" width="80" height="20" uuid="9ba91b58-e72a-4db7-8d33-c73bc35e6316"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="515" y="30" width="40" height="20" uuid="ac36299e-2497-4b5c-926d-def503a7c196"/>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="30" width="100" height="20" uuid="1d5e877e-987c-488b-ae71-9db5c10d7632"/>
				<textElement>
					<font size="7" isItalic="true"/>
				</textElement>
				<text><![CDATA[UntaCoreERPSystem]]></text>
			</staticText>
		</band>
	</pageFooter>
</jasperReport>
