<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Report1" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="9006cafc-6e59-438e-8a65-1e0a44b215ae">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Ad_Org_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="IsSOTrx" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_Charge_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_BankAccount_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
cc.Name AS Name,
CEILING(SUM(bsl.ChargeAmt) * -1),
CEILING(SUM(pay.ChargeAmt)),
CEILING(SUM(invL.LineNetAmt)),
ag.Name AS Organization,
cba.name AS Kas,
cbk.name AS Bank

FROM C_BankStatementLine bsl

INNER JOIN C_BankStatement cb ON cb.C_BankStatement_ID=bsl.C_BankStatement_ID
INNER JOIN Ad_Org ag ON ag.Ad_Org_ID = bsl.Ad_Org_ID
LEFT JOIN C_Payment pay ON pay.C_Payment_ID = bsl.C_Payment_ID
LEFT JOIN C_PaymentAllocate alloc ON alloc.C_Payment_ID = pay.C_Payment_ID
LEFT JOIN C_Invoice inv ON inv.C_Invoice_ID = alloc.C_Invoice_ID
LEFT JOIN C_InvoiceLine invL ON invL.C_Invoice_ID = inv.C_Invoice_ID
LEFT JOIN C_Charge cc ON cc.C_Charge_ID = COALESCE(bsl.C_Charge_ID, pay.C_Charge_ID, invL.C_Charge_ID)
INNER JOIN C_BankAccount cba ON cba.C_BankAccount_ID=cb.C_BankAccount_ID
INNER JOIN C_Bank cbk ON cbk.C_bank_ID=cba.C_Bank_ID

WHERE
(CASE WHEN $P{Ad_Org_ID} IS NOT NULL THEN bsl.Ad_Org_ID = $P{Ad_Org_ID} ELSE 1=1 END)AND
(CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN bsl.statementlinedate >= $P{DateFrom} AND bsl.statementlinedate <= $P{DateTo} WHEN $P{DateFrom}::timestamp IS NOT NULL and $P{DateTo}::timestamp IS NULL THEN bsl.statementlinedate >= $P{DateFrom} WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN bsl.statementlinedate <= $P{DateTo} ELSE 1=1 END) AND
(CASE WHEN $P{C_Charge_ID} IS NOT NULL THEN cc.C_Charge_ID = $P{C_Charge_ID} ELSE 1=1 END) AND
(CASE WHEN $P{C_BankAccount_ID} IS NOT NULL THEN cba.C_BankAccount_ID = $P{C_BankAccount_ID} ELSE 1=1 END) AND cc.isActive='Y'

GROUP BY ag.name,cc.name,cba.name,cbk.name
ORDER BY ag.name ASC, cbk.name]]>
	</queryString>
	<field name="name" class="java.lang.String"/>
	<field name="ceiling" class="java.math.BigDecimal"/>
	<field name="organization" class="java.lang.String"/>
	<field name="kas" class="java.lang.String"/>
	<field name="bank" class="java.lang.String"/>
	<variable name="num" class="java.lang.Integer" resetType="Group" resetGroup="Organization">
		<variableExpression><![CDATA[($V{num} == null) ? 1 : $V{num} + 1]]></variableExpression>
	</variable>
	<group name="Organization" isStartNewPage="true">
		<groupExpression><![CDATA[$F{organization}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<staticText>
					<reportElement x="32" y="0" width="188" height="20" uuid="02bed25b-9318-44ec-a038-f821d5f0deef"/>
					<box leftPadding="2">
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Charge]]></text>
				</staticText>
				<staticText>
					<reportElement x="431" y="0" width="124" height="20" uuid="d548e89a-9152-4f75-9cdb-e0f1666d3eda"/>
					<box leftPadding="2">
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Amount]]></text>
				</staticText>
				<staticText>
					<reportElement x="220" y="0" width="211" height="20" uuid="b25cdf1c-2b2f-4e3c-8e1a-2f04f9a99b7e"/>
					<box leftPadding="2">
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Cash / Bank]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="0" width="32" height="20" uuid="1e0eb939-d744-421a-98b3-03ae3bea59ee"/>
					<box leftPadding="2">
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[No]]></text>
				</staticText>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="47" splitType="Stretch">
			<staticText>
				<reportElement x="1" y="0" width="555" height="30" uuid="3f81ea34-5468-4fde-9295-8a5cc2072fe0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[REKAP PEMAKAIAN CHARGE]]></text>
			</staticText>
			<staticText>
				<reportElement x="455" y="0" width="100" height="20" uuid="dae00612-cb92-4d79-a70a-747ce2880d7e"/>
				<textElement textAlignment="Right">
					<font size="8" isItalic="true"/>
				</textElement>
				<text><![CDATA[UntaCoreERP]]></text>
			</staticText>
			<textField>
				<reportElement x="1" y="30" width="555" height="17" uuid="4a9afea1-bf8f-46b1-ac9f-b57e1fd45a2b"/>
				<box>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="11" isBold="true" isUnderline="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{organization}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="46" width="555" height="1" uuid="974a148a-9e88-41c1-a35d-f21217dfa3d1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
		</band>
	</title>
	<pageHeader>
		<band height="20">
			<textField pattern="dd MMMMM yyyy">
				<reportElement x="209" y="0" width="346" height="20" uuid="35a7331e-8f35-4132-abeb-ec1d0d14e12f"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{DateFrom} == null && $P{DateTo} == null ? "All Times":
$P{DateFrom} != null && $P{DateTo} == null ? new SimpleDateFormat("dd MMMMM yyyy").format$P{DateFrom} +"- Today":
$P{DateFrom} != null && $P{DateTo} != null ? new SimpleDateFormat("dd MMMMM yyyy").format$P{DateFrom}+ " - " +new SimpleDateFormat("dd MMMMM yyyy").format$P{DateTo}:
$P{DateFrom} == null && $P{DateTo} != null ? "Until " +new SimpleDateFormat("dd MMMMM yyyy").format$P{DateTo}: ""]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement x="32" y="0" width="188" height="20" uuid="8cded1b5-8b4e-4907-b306-650cd8fdaa45"/>
				<box leftPadding="2">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="220" y="0" width="211" height="20" uuid="9641a9e2-f05e-43ab-837d-f7ad9a1949e7"/>
				<box leftPadding="2">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{bank}+"_"+$F{kas}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="32" height="20" uuid="a68ae2bf-ed21-4146-9606-702eba1efe40"/>
				<box leftPadding="2">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{num}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="431" y="0" width="24" height="20" uuid="ed0ba885-51f1-42ae-8d07-7fef37c9ca9a"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Rp.]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="456" y="0" width="100" height="20" uuid="15138728-4fc4-4408-8ef0-ea50a274a6ff"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ceiling}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="20">
			<textField>
				<reportElement x="435" y="0" width="80" height="20" uuid="9546ef90-4dc5-4bc1-aa4b-314e0a865cdb"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="515" y="0" width="40" height="20" uuid="bd476642-a95e-4cac-8d3b-783e88e5798d"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
