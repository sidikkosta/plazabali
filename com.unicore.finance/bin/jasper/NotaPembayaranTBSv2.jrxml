<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="NotaPembayaranTBS" language="groovy" pageWidth="936" pageHeight="595" orientation="Landscape" columnWidth="896" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="50b41e93-3b47-4a3d-a588-40ff9612ecfa">
	<property name="ireport.zoom" value="0.75"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DocumentNo" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["D:\\Data\\dev\\UntaCore_TMG\\com.unicore.finance\\src\\jasper\\"]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT bp.name AS vendor,
fk.documentno AS no,
odc.beneficiary AS an,
odc.accountno AS acct,
b.name AS bank,
wt.datedoc AS tgl,
wt.documentno AS notiket,
od.documentno AS nodo,
wt.vehicleno AS nopol,
wt.grossweight AS bruto,
fk.referenceno AS nofkv,
wt.tare AS tara,
wt.nettoi AS netto,
COALESCE (rev.revisionqty, wt.reflection) AS pot,
COALESCE (rev.revisionpercent, wt.reflection/wt.nettoi * 100) AS potpersen,
COALESCE (rev.qtypayment, wt.nettoii) AS terima,
fkl.pricelist AS satuan,
fkl.linenetamt AS total,
(fkl.linenetamt * tax.Rate) / 100 AS ppn,
wh.percent AS persen,
wh.taxamt AS pph,
fkl.qtyinvoiceticket AS QtyInvoiceTicket,
COALESCE(ull.amount,0) AS ongkos,
CASE WHEN cml.C_InvoiceLine_ID > 0 THEN COALESCE(cml.linenetamt,0) ELSE 0 END AS SUM

FROM C_Invoice fk
LEFT JOIN C_Invoice cm ON cm.CN_Invoice_ID=fk.C_Invoice_ID
LEFT JOIN C_InvoiceLine fkl ON fkl.C_Invoice_ID=fk.C_Invoice_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID=fk.C_BPartner_ID
LEFT JOIN C_Order od ON od.C_Order_ID=fk.C_Order_ID
LEFT JOIN UNS_Order_Contract odc ON odc.C_Order_ID=od.C_Order_ID
LEFT JOIN C_Bank b ON b.C_Bank_ID=odc.C_Bank_ID
INNER JOIN UNS_WeighbridgeTicket wt ON wt.C_InvoiceLine_ID=fkl.C_InvoiceLine_ID
LEFT JOIN  UNS_ReflectionRevision_Line rev ON rev.UNS_WeighbridgeTicket_ID=wt.UNS_WeighbridgeTicket_ID
LEFT JOIN LCO_InvoiceWithholding wh ON wh.C_InvoiceLine_ID=fkl.C_InvoiceLine_ID
LEFT JOIN UNS_UnLoadingCost_Line ull ON ull.UNS_UnLoadingCost_Line_ID=wt.UNS_UnLoadingCost_Line_ID
LEFT JOIN C_InvoiceLine cml ON cml.C_Invoice_ID=cm.C_Invoice_ID AND cml.C_Charge_ID IS NOT NULL
INNER JOIN C_Tax tax ON tax.C_Tax_ID = fkl.C_Tax_ID

WHERE fk.dateinvoiced BETWEEN $P{DateFrom} AND $P{DateTo} AND fk.isSOTrx='N' AND fk.docstatus IN ('CO','CL') AND (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN fk.C_BPartner_ID = $P{C_BPartner_ID} ELSE 1=1 END)
ORDER BY tgl, notiket]]>
	</queryString>
	<field name="vendor" class="java.lang.String"/>
	<field name="no" class="java.lang.String"/>
	<field name="an" class="java.lang.String"/>
	<field name="acct" class="java.lang.String"/>
	<field name="bank" class="java.lang.String"/>
	<field name="tgl" class="java.sql.Timestamp"/>
	<field name="notiket" class="java.lang.String"/>
	<field name="nodo" class="java.lang.String"/>
	<field name="nopol" class="java.lang.String"/>
	<field name="bruto" class="java.math.BigDecimal"/>
	<field name="nofkv" class="java.lang.String"/>
	<field name="tara" class="java.math.BigDecimal"/>
	<field name="netto" class="java.math.BigDecimal"/>
	<field name="pot" class="java.math.BigDecimal"/>
	<field name="potpersen" class="java.math.BigDecimal"/>
	<field name="terima" class="java.math.BigDecimal"/>
	<field name="satuan" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="ppn" class="java.math.BigDecimal"/>
	<field name="persen" class="java.math.BigDecimal"/>
	<field name="pph" class="java.math.BigDecimal"/>
	<field name="qtyinvoiceticket" class="java.math.BigDecimal"/>
	<field name="ongkos" class="java.math.BigDecimal"/>
	<field name="sum" class="java.math.BigDecimal"/>
	<variable name="bruto_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{bruto}]]></variableExpression>
	</variable>
	<variable name="tara_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{tara}]]></variableExpression>
	</variable>
	<variable name="netto_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{netto}]]></variableExpression>
	</variable>
	<variable name="pot_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{pot}]]></variableExpression>
	</variable>
	<variable name="potpersen_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{potpersen}]]></variableExpression>
	</variable>
	<variable name="terima_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{terima}]]></variableExpression>
	</variable>
	<variable name="total_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<variable name="ppn_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{ppn}]]></variableExpression>
	</variable>
	<variable name="persen_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{persen}]]></variableExpression>
	</variable>
	<variable name="ongkos_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{ongkos}]]></variableExpression>
	</variable>
	<variable name="pph_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{pph}]]></variableExpression>
	</variable>
	<variable name="variable1" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{total_1}.add($V{ppn_1}).subtract($V{pph_1}).subtract($V{ongkos_1})]]></variableExpression>
	</variable>
	<variable name="variable2" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{variable1}.subtract($F{sum})]]></variableExpression>
	</variable>
	<variable name="sum_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Vendor" calculation="Sum">
		<variableExpression><![CDATA[$F{sum}]]></variableExpression>
	</variable>
	<group name="Vendor">
		<groupExpression><![CDATA[$F{vendor}]]></groupExpression>
		<groupFooter>
			<band height="70">
				<textField pattern="#,##0">
					<reportElement uuid="4e5e6600-add0-4219-9fee-09b9a46a1765" x="232" y="1" width="63" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{bruto_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="e2f3d9f9-d79c-4751-99c0-4f95f54f82c1" x="301" y="0" width="50" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{tara_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.0">
					<reportElement uuid="1091da8c-3386-4dfa-a79a-c6f6bb886343" x="468" y="0" width="43" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{potpersen_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="44d46009-5ead-41d0-9511-ae344cb6fb3b" x="638" y="1" width="67" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="abe29747-f4bb-4bfd-b38b-e3f3b2600610" x="710" y="0" width="54" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ppn_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="ac6c5448-820a-4e46-9006-cc6b81634ad8" x="819" y="0" width="74" height="16"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ongkos_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="91cc8d23-2104-49be-8921-bf00f1ba09b5" x="1" y="0" width="183" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="0ee9681a-e34f-4c38-8248-d76218695ced" x="656" y="23" width="101" height="13"/>
					<textElement>
						<font size="9"/>
					</textElement>
					<text><![CDATA[Sub Total]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="7f015d75-9820-4903-8d6e-4dddc5550401" x="656" y="36" width="101" height="14"/>
					<textElement>
						<font size="9"/>
					</textElement>
					<text><![CDATA[Biaya Administrasi]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="c0ca39b7-39de-41ae-9566-7bf36cc1589b" x="656" y="50" width="101" height="20"/>
					<textElement>
						<font size="9"/>
					</textElement>
					<text><![CDATA[Total]]></text>
				</staticText>
				<textField pattern="#,##0">
					<reportElement uuid="78d4051b-e91c-4769-a57b-d036afed3198" x="757" y="23" width="137" height="13"/>
					<textElement textAlignment="Right">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{variable1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="833ca986-bdcb-4c5e-a318-5ec4c58f213f" x="757" y="36" width="137" height="14"/>
					<textElement textAlignment="Right">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="a88e3b16-e787-497b-8520-5c49517a584d" x="757" y="50" width="137" height="20"/>
					<textElement textAlignment="Right">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{variable2}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement uuid="2e1abf09-3673-429d-82a8-920bd7142751" x="0" y="15" width="896" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0">
					<reportElement uuid="ab69147a-63a0-4cb7-b995-5eb81c5284f4" x="515" y="0" width="59" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{terima_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="b75f06d5-14e3-4951-a87d-925df864f67f" x="416" y="1" width="50" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{pot_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="05a1a2a2-ced1-4e4c-b93d-646fc9aeba1f" x="357" y="0" width="54" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{netto_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement uuid="345b6697-fd19-4755-a1f2-775232f47026" x="765" y="0" width="52" height="15"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{pph_1}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="e5efcb17-0954-4e72-8445-659a8c04628c" x="146" y="48" width="100" height="20"/>
					<textElement/>
					<textFieldExpression><![CDATA[$V{sum_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="63" splitType="Stretch">
			<staticText>
				<reportElement uuid="6713df69-5aab-44c7-bd76-4d6a33e1e289" x="0" y="0" width="896" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[PT. SUTOPO LESTARI JAYA]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="76dd00fe-086d-464f-9456-c828cd1c66db" x="0" y="20" width="896" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[NOTA PEMBAYARAN TBS]]></text>
			</staticText>
			<textField>
				<reportElement uuid="ddbaeb9d-67a3-4279-aac7-1b79ecc72d59" x="0" y="40" width="896" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{no}+" ("+$F{nofkv}+ ") "]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="102" splitType="Stretch">
			<staticText>
				<reportElement uuid="8dd4f1a9-e123-4091-a746-507379e4d0c6" x="0" y="0" width="253" height="20"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true" isUnderline="true"/>
				</textElement>
				<text><![CDATA[INFORMASI SUPPLIER]]></text>
			</staticText>
			<textField>
				<reportElement uuid="6da066d0-a86d-498f-b58b-f43fbd6d0781" x="0" y="20" width="516" height="20"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{vendor}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="7cf1bed9-a51a-4304-9407-52a2ddd962c6" x="39" y="38" width="362" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[" :  " +$F{an}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="8ff10ab2-1785-42ef-a1d9-4f66adf09641" x="39" y="54" width="362" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[" :  " +$F{acct}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="f1fbfc91-1391-4393-a160-baee181100f2" x="39" y="71" width="362" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{bank}==null ? " :  -" : " :  " +$F{bank}]]></textFieldExpression>
			</textField>
			<elementGroup/>
			<staticText>
				<reportElement uuid="16f1c4fe-a5f0-416d-b4b5-09829d7167e3" x="0" y="38" width="100" height="20"/>
				<textElement/>
				<text><![CDATA[A/N]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b430952f-b9a1-40ab-a53a-c808646a9beb" x="0" y="54" width="100" height="20"/>
				<textElement/>
				<text><![CDATA[Account]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="503edbeb-bac7-49cb-a48b-3925b2a49326" x="0" y="71" width="100" height="20"/>
				<textElement/>
				<text><![CDATA[Bank]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<rectangle>
				<reportElement uuid="fd8d922f-9172-4c1c-a74a-7a47cdda5900" x="0" y="1" width="60" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="b989c418-b40b-4ef1-9948-6d3d174d8975" x="59" y="1" width="55" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="ded06047-28f5-4640-bd47-650cd8d04edf" x="0" y="2" width="60" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[TGL.]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e2894eb3-da88-414a-85c8-492e010640c3" x="59" y="2" width="55" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[No Tiket]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="6446771d-8939-4314-b6ac-1037d6605d58" x="171" y="1" width="61" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="2daf0e1c-ffee-43cf-9340-c1a1c6311253" x="114" y="1" width="57" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="2f33807d-69ef-4bbe-b07e-4f2b5604bd8f" x="398" y="15" width="200" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d3695f21-3fc3-435d-ace9-580f9039c3ea" x="171" y="2" width="61" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[NOPOL]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e8a814b5-feb8-4572-9907-6097b24da166" x="115" y="2" width="58" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[No DO]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="d7025593-c683-4f41-aa1e-82f45a5559f2" x="468" y="1" width="47" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="4726aa6a-8f94-4cbf-9348-f1df92dc28e4" x="416" y="1" width="53" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="1579c99c-12cc-43da-abb1-603b3f2fa203" x="357" y="1" width="59" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="ab98b3e8-9738-4d13-8969-b6e170a6fa27" x="232" y="1" width="69" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="ca56fc5e-04e0-4058-ab93-068b6dbcc62d" x="232" y="0" width="69" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Bruto]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d2b5920e-a32e-4dd1-a628-1b9178e9da2c" x="416" y="0" width="53" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Pot]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="9c038ab1-2a43-44a0-ada2-8879340eade0" x="469" y="0" width="46" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Pot]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="38faf49c-1d93-42a2-931a-74eb20ab1062" x="357" y="0" width="59" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Netto I]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="b9fb4ab8-914a-47b5-940a-0885c2536094" x="515" y="1" width="65" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="9b4b4821-7aab-46c4-98a4-e69fd6d51daa" x="515" y="0" width="65" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Netto II]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="f50de942-c669-48b2-80cf-2ea1a67b3c1a" x="580" y="1" width="60" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="9ad3a38f-c0bb-4724-bca5-c56cfd2f8a52" x="580" y="0" width="60" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Harga Satuan]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="4d123b9f-a0a0-4d8f-9c18-5758d6f70869" x="640" y="1" width="67" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="5878abbf-5867-418f-918b-c2cbc8c4e1a0" x="641" y="0" width="67" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="f719d415-2f4f-475a-8f1c-775f9099b102" x="707" y="1" width="58" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="caed48d3-602e-4a70-a241-ee53482adead" x="765" y="1" width="54" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement uuid="47d61b2d-25a7-4043-90fd-442eea58ef4f" x="819" y="1" width="77" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="7c52b941-8066-41a0-bd13-fb4f86327613" x="819" y="2" width="77" height="12"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Ongkos Bongkar]]></text>
			</staticText>
			<rectangle>
				<reportElement uuid="347763af-9dfb-446d-a2f6-fb91c62590ab" x="301" y="1" width="56" height="29"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement uuid="5969f2e8-9f21-46df-a159-ac546618fbc2" x="301" y="0" width="56" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Tara]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="37f95171-8f55-41ba-9682-bee31cf97b21" x="232" y="16" width="69" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Kg]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="cdc6188b-902a-4edf-88f9-ed72bbfc2e1a" x="301" y="16" width="56" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Kg]]></text>
			</staticText>
			<elementGroup/>
			<staticText>
				<reportElement uuid="44574d92-d710-4db2-b25e-b8ab297fb00d" x="361" y="16" width="54" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Kg]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="623f0ca4-b7f2-4aa3-9d8a-6628026e767e" x="416" y="16" width="53" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Kg]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e9097e1b-4b9a-4f51-9ead-82c68e9fbda6" x="468" y="17" width="47" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[%]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="0c315779-860a-4ab3-8368-5618cf832d70" x="513" y="16" width="65" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Kg]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="6d7bf957-afaf-437f-80fc-fc7f95d79ba8" x="584" y="17" width="51" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rp]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="6ddb8ef8-5210-4168-9bb7-65fb8b2dcb1b" x="652" y="17" width="56" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rp]]></text>
			</staticText>
			<textField>
				<reportElement uuid="f0cc465f-2800-42cd-a457-7981fa7e2771" x="765" y="17" width="54" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{persen}.setScale( 2, java.math.RoundingMode.HALF_UP ) +"%"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="3b9f6272-bc21-49ff-9638-fdcd7e8374c4" x="819" y="17" width="77" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rp]]></text>
			</staticText>
			<line>
				<reportElement uuid="c27900f5-76b3-4c3e-9266-bcb5b04c6942" x="232" y="17" width="664" height="1"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="014f7f6f-71c9-443a-9334-d3ab7938a188" x="765" y="1" width="54" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[PPH]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="414751c1-5c70-4b96-a771-37e7844996ad" x="712" y="1" width="53" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[PPN]]></text>
			</staticText>
			<textField>
				<reportElement uuid="40797d48-06ac-473a-a23f-56b915900fe4" x="403" y="10" width="100" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{sum}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="f7b16257-c555-4b44-840f-5c71d0e95419" x="28" y="1" width="100" height="20"/>
				<textElement/>
				<text><![CDATA[sum]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<textField pattern="#,##0">
				<reportElement uuid="24554d29-deb6-49a4-9d50-2f6dda0e4a2c" x="357" y="0" width="54" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{netto}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="fcf240af-4bd7-4108-bf49-507fb4d025c4" x="515" y="1" width="59" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{terima}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="a5df5a3c-7f01-4458-8563-6e1b12890574" x="765" y="1" width="52" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pph}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="9478a8dc-aac7-4b73-99db-db82507c9b81" x="301" y="0" width="50" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tara}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="de3780c7-70f8-4f1d-b9e6-2c4c54b116fc" x="232" y="1" width="63" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{bruto}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="6fd3b06d-9f4c-463c-bc89-456060dcf876" x="59" y="1" width="54" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{notiket}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="c6945975-0a86-4bd7-beaf-e03e3fce618b" x="1" y="1" width="58" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tgl}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="b8f9b899-2b90-46ff-b51a-6e240a0e7957" x="114" y="1" width="56" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nodo}==null ? "0" :$F{nodo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="8ed7b2be-d451-4741-8505-f89d1d182ed9" x="172" y="1" width="56" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nopol}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="85a52f84-8fc2-4d34-bf9b-26e0fb574267" x="416" y="1" width="50" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pot}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="a38fc229-be33-45a3-99bb-bef0099da2f7" x="641" y="1" width="67" height="15"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0">
				<reportElement uuid="d6ce18fd-b10c-457c-a157-7e2d7b2bf338" x="469" y="1" width="43" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{potpersen}.setScale( 2, java.math.RoundingMode.HALF_UP )]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="ad9070c2-6713-4e8c-bf5b-3abe6f49102c" x="820" y="1" width="73" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ongkos}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="f7d9b11a-252e-471f-9a4d-32a6b84df53d" x="580" y="1" width="57" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{satuan}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="9765b8dd-19e3-48a9-b29e-0161335a0009" x="1" y="15" width="895" height="1"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField pattern="#,##0">
				<reportElement uuid="9a8fb353-19f5-4535-a2f3-929cd1f7cf64" x="710" y="0" width="54" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ppn}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="dd675b03-4805-44c0-9ad2-98d8379e11cf" x="28" y="0" width="100" height="15">
					<printWhenExpression><![CDATA[1==2]]></printWhenExpression>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{sum}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
