<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportChequeHasUsed" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="06c1e384-0ace-48c3-b759-9e8d557f81ac">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="C_BankAccount_ID" class="java.lang.Integer"/>
	<queryString>
		<![CDATA[SELECT b.Name AS Bank, ba.Name AS AccountName, cb.Name, ba.AccountNo AS AccountNo, cb.Letter_Code1 AS Code, Start_Number1 AS NumberFrom, End_Number1 AS NumberTo, COALESCE(cb.Date_Received, cb.created) AS DateReceived, cr.ChequeNo AS ChequeNo, cr.UNS_Cheque_DisbursementDate AS DisbursementDate, rl.Name AS Status, pay.DocumentNo AS Payment, pay.DateTrx AS PaymentDate FROM UNS_ChequeBook cb

INNER JOIN C_BankAccount ba ON ba.C_BankAccount_ID = cb.C_BankAccount_ID
INNER JOIN C_Bank b ON b.C_Bank_ID = ba.C_Bank_ID
INNER JOIN UNS_Cheque_Reconciliation cr ON cr.UNS_ChequeBook_ID = cb.UNS_ChequeBook_ID
INNER JOIN C_Payment pay ON pay.C_Payment_ID = cr.C_Payment_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pay.C_BPartner_ID
INNER JOIN AD_Reference rf ON rf.Name = '_UNS_ChequeStatus'
INNER JOIN AD_Ref_List rl ON rf.AD_Reference_ID = rl.AD_Reference_ID AND rl.Value = cr.Status

WHERE CASE WHEN $P{C_BankAccount_ID} IS NOT NULL THEN ba.C_BankAccount_ID = $P{C_BankAccount_ID} ELSE 1=1 END

ORDER BY Bank, AccountName, NumberFrom, ChequeNo, Status desc]]>
	</queryString>
	<field name="bank" class="java.lang.String"/>
	<field name="accountname" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="accountno" class="java.lang.String"/>
	<field name="code" class="java.lang.String"/>
	<field name="numberfrom" class="java.lang.String"/>
	<field name="numberto" class="java.lang.String"/>
	<field name="datereceived" class="java.sql.Timestamp"/>
	<field name="chequeno" class="java.lang.String"/>
	<field name="disbursementdate" class="java.sql.Timestamp"/>
	<field name="status" class="java.lang.String"/>
	<field name="payment" class="java.lang.String"/>
	<field name="paymentdate" class="java.sql.Timestamp"/>
	<variable name="count_giro" class="java.lang.Integer" resetType="Group" resetGroup="Cheque">
		<variableExpression><![CDATA[$V{count_giro} == null ? 1 : $V{count_giro} + 1]]></variableExpression>
	</variable>
	<variable name="number" class="java.lang.Integer" resetType="Group" resetGroup="Cheque">
		<variableExpression><![CDATA[$V{number} == null ? 1 : $V{number} + 1]]></variableExpression>
	</variable>
	<group name="Bank" isStartNewPage="true">
		<groupExpression><![CDATA[$F{bank}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement uuid="17b81378-d7e3-4571-ae62-ff286a536c6f" x="0" y="0" width="555" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["#Bank :: " + $F{bank}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="2" y="1" width="552" height="1"/>
				</line>
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="1" y="19" width="552" height="1"/>
				</line>
			</band>
		</groupHeader>
	</group>
	<group name="Account">
		<groupExpression><![CDATA[$F{accountno}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement uuid="bf28a094-46d3-40d8-97c7-85dd04a02bc6" x="0" y="0" width="555" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["#Account :: " + $F{accountname} + " - " + $F{accountno}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="3" y="19" width="552" height="1"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="13"/>
		</groupFooter>
	</group>
	<group name="Cheque">
		<groupExpression><![CDATA[$F{name}]]></groupExpression>
		<groupHeader>
			<band height="61">
				<textField>
					<reportElement uuid="cbdae078-de23-4954-af06-3fd49a47cce4" x="0" y="0" width="555" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["#Cheque :: " + $F{name}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="e9c0fed9-05cd-4162-a689-64308d6a4e8f" x="0" y="20" width="555" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA["#Code :: " + $F{code} + " #From :: " + $F{numberfrom} + " #To :: " + $F{numberto} + " #Date :: " + new SimpleDateFormat("dd-MM-yyyy").format($F{datereceived})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="65039fe4-4017-41cb-92aa-e96b1817b493" x="30" y="40" width="111" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Cheque/Giro No]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="3416880f-b9b9-457e-b6f7-e29954701795" x="141" y="40" width="111" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Disbursement]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="ae45e296-c989-4a34-b4ea-4ed9b92cb8c1" x="252" y="40" width="111" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="7e9f2a9b-5f2e-4c90-8c44-d21905f2ce91" x="363" y="40" width="192" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Payment]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="2589a520-ace1-4b55-b954-a5a86dd3204d" x="0" y="40" width="30" height="20"/>
					<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[No]]></text>
				</staticText>
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="3" y="20" width="552" height="1"/>
				</line>
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="3" y="39" width="552" height="1"/>
				</line>
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="3" y="60" width="552" height="1"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="8">
				<line>
					<reportElement uuid="f380b9af-2ab6-426d-bf3a-2710e48ec820" x="2" y="0" width="552" height="1"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="32" splitType="Stretch">
			<staticText>
				<reportElement uuid="9713aa0f-af45-4bb5-89f7-bfcd5d426746" x="0" y="0" width="555" height="32"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[List Of Cheque Has Been Used	]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="17" splitType="Stretch">
			<textField>
				<reportElement uuid="7a6e8482-9fca-4137-abc4-482453efa4e8" x="30" y="0" width="111" height="17"/>
				<box topPadding="1" leftPadding="1" bottomPadding="1" rightPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{chequeno}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="52124439-6ede-4c7d-af36-b567602a7a69" x="141" y="0" width="111" height="17"/>
				<box topPadding="1" leftPadding="1" bottomPadding="1" rightPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MM-yyyy").format($F{disbursementdate})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="390e9f8a-2678-45fe-a720-043bb0f10065" x="252" y="0" width="111" height="17"/>
				<box topPadding="1" leftPadding="1" bottomPadding="1" rightPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{status}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="ac733106-9970-498e-9fa8-5793f2aa83fe" x="363" y="0" width="192" height="17"/>
				<box topPadding="1" leftPadding="1" bottomPadding="1" rightPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{payment} + " - " + new SimpleDateFormat("dd-MM-yyyy").format($F{paymentdate})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="b61a2a12-fc3b-4ee5-bd4e-ab5fefe9aca5" x="0" y="0" width="30" height="17"/>
				<box topPadding="1" leftPadding="1" bottomPadding="1" rightPadding="1"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{number}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
