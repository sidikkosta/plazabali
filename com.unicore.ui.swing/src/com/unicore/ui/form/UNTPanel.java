/**
 * 
 */
package com.unicore.ui.form;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.RenderingHints;

import org.compiere.swing.CPanel;

/**
 * @author nurse
 *
 */
public class UNTPanel extends CPanel {

	private int p_strokeSize = 4;
	private Dimension p_cornerArcs = new Dimension(20, 20);
	private Color p_strokColor = new Color(0,110,0);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5425444401766309000L;

	public UNTPanel (LayoutManager layout)
	{
		super (layout);
	}
	
	/**
	 * 
	 */
	public UNTPanel() 
	{
		super ();
	}
	
	public void paint (Graphics g)
	{
		int width = getWidth();
	    int height = getHeight();
	    Graphics2D graphics = (Graphics2D) g;
    	graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    			RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setColor(getBackground());
		graphics.fillRoundRect(0, 0, width,
				height, p_cornerArcs.width, p_cornerArcs.height);
		graphics.setColor(p_strokColor);
		graphics.setStroke(new BasicStroke(p_strokeSize));
		graphics.drawRoundRect(0, 0, width,
				height, p_cornerArcs.width, p_cornerArcs.height);
		
		super.paintComponents(g);
	}
	
	public void setStroke (int size, Color color)
	{
		setStrokeColor(color);
		setStrokeSize(size);
	}
	
	public void setStrokeSize (int size)
	{
		p_strokeSize = size;
	}

	public void setCornerArcs (Dimension dim)
	{
		p_cornerArcs = dim;
	}
	
	public void setStrokeColor (Color color)
	{
		p_strokColor = color;
	}
	
	public void updateInfo ()
	{
		
	}
}
