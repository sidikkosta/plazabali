/**
 * 
 */
package com.unicore.ui.grid;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.compiere.apps.AEnv;
import org.compiere.grid.VCreateFromDialog;
import org.compiere.grid.ed.VLookup;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.swing.CPanel;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import static org.compiere.model.SystemIDs.COLUMN_AD_User_AD_User_ID;;

/**
 * @author Burhani Adam
 *
 */
public class VCreateFromMovementUI extends CreateFromMovement implements ActionListener, VetoableChangeListener {

	private VCreateFromDialog dialog;
	/** Window No               */
	private int p_WindowNo;
	private boolean m_actionActive = false;
	private JLabel requisitionLabel = new JLabel();
	private JComboBox<KeyNamePair> requisitionField = new JComboBox<KeyNamePair>();
	private JLabel userLabel = new JLabel();
	private VLookup userField;
	
	/**
	 * @param gridTab
	 */
	public VCreateFromMovementUI(GridTab gridTab) {
		super(gridTab);
		
		if (log.isLoggable(Level.INFO)) log.info(getGridTab().toString());
		
		dialog = new VCreateFromDialog(this, getGridTab().getWindowNo(), true);
		
		p_WindowNo = getGridTab().getWindowNo();
		
		try
		{
			if (!dynInit())
				return;
			jbInit();

			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
		}
		AEnv.positionCenterWindow(AEnv.getWindow(p_WindowNo), dialog);
	}

	@Override
	public void vetoableChange(PropertyChangeEvent arg0)
			throws PropertyVetoException {
		// TODO Auto-generated method stub
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (m_actionActive)
			return;
		m_actionActive = true;
		if (log.isLoggable(Level.CONFIG)) log.config("Action=" + e.getActionCommand());
		
		KeyNamePair pp = (KeyNamePair) requisitionField.getSelectedItem();
		if (e.getSource().equals(requisitionField))
		{
			loadRequisitionLine(pp.getKey(), userField.getValue() == null ? 0 : (Integer) userField.getValue());
		}
		else
		{
			initRequisition();
			pp = (KeyNamePair) requisitionField.getSelectedItem();
			loadRequisitionLine(pp.getKey(), userField.getValue() == null ? 0 : (Integer) userField.getValue());
		}
		
		m_actionActive = false;	
	}
	
	 private void jbInit() throws Exception
	 {    	
    	requisitionLabel.setText("Requisition");
		userLabel.setText("User");
		
    	CPanel parameterPanel = dialog.getParameterPanel();
    	parameterPanel.setLayout(new BorderLayout());
    	CPanel parameterStdPanel = new CPanel(new GridBagLayout());
    	parameterPanel.add(parameterStdPanel, BorderLayout.CENTER);
    
    	parameterStdPanel.add(userLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
    			,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    	parameterStdPanel.add(userField,  new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
    			,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 5), 0, 0));
    	
    	parameterStdPanel.add(requisitionLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
    			,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    	parameterStdPanel.add(requisitionField,  new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
    			,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 5), 0, 0));
	 }//  jbInit
	 
	public boolean dynInit() throws Exception
	{
		log.config("");
		super.dynInit();
		dialog.setTitle(getTitle());
		initUser(); initRequisition();
		
		return true;
	}   //  dynInit
	
	@Override
	public Object getWindow() {
		return dialog;
	}
	
	public void showWindow()
	{
		dialog.setVisible(true);
	}
	
	public void closeWindow()
	{
		dialog.dispose();
	}

	@Override
	protected void configureMiniTable(IMiniTable miniTable) {
		super.configureMiniTable(miniTable);
	}
	
	protected void initUser()
	{
		int AD_Column_ID = COLUMN_AD_User_AD_User_ID;      
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		userField = new VLookup ("AD_User_ID", false, false, true, lookup);
		userField.addActionListener(this);
	}
	
	protected void initRequisition()
	{
		KeyNamePair pp = new KeyNamePair(0,"");
		//  load PO Orders - Closed, Completed
		requisitionField.removeActionListener(this);
		requisitionField.removeAllItems();
		requisitionField.addItem(pp);
		
		int AD_User_ID = userField.getValue() == null ? 0 : (Integer) userField.getValue();
		ArrayList<KeyNamePair> list = loadRequisitionData(AD_User_ID);
		for(KeyNamePair knp : list)
			requisitionField.addItem(knp);
		
		requisitionField.addActionListener(this);
	}
	
	protected void loadTableOIS (Vector<?> data)
	{
		//  Remove previous listeners
		dialog.getMiniTable().getModel().removeTableModelListener(dialog);
		DefaultTableModel model = new DefaultTableModel(data, getOISColumnNames());
		model.addTableModelListener(dialog);
		dialog.getMiniTable().setModel(model);
		// 
		
		configureMiniTable(dialog.getMiniTable());
	}   //  loadOrder
	
	protected void loadRequisitionLine (int M_Requisition_ID, int AD_User_ID)
	{
		loadTableOIS(getRequisitionLine(M_Requisition_ID, AD_User_ID));
	}   //  LoadInvoices
}