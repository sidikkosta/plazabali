/**
 * 
 */
package com.unicore.ui.grid;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.VetoableChangeListener;
import java.util.ArrayList;
import java.util.logging.Level;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.compiere.apps.AEnv;
import org.compiere.grid.VCreateFromDialog;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.swing.CPanel;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;

/**
 * @author Burhani Adam
 *
 */
public class VCreateFromPaymentUI extends CreateFromPayment implements ActionListener, VetoableChangeListener {

	private VCreateFromDialog dialog;
	
	public VCreateFromPaymentUI(GridTab gridTab)
	{
		super(gridTab);
		if (log.isLoggable(Level.INFO)) log.info(getGridTab().toString());
		
		dialog = new VCreateFromDialog(this, getGridTab().getWindowNo(), true);
		
		p_WindowNo = getGridTab().getWindowNo();

		try
		{
			if (!dynInit())
				return;
			jbInit();

			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
		}
		AEnv.positionCenterWindow(AEnv.getWindow(p_WindowNo), dialog);

	}
	
	/** Window No               */
	private int p_WindowNo;

	/**	Logger			*/
	private CLogger log = CLogger.getCLogger(getClass());
	
	//
	private JLabel paySelectionLabel = new JLabel();
	private JComboBox<KeyNamePair> paySelectionField = new JComboBox<KeyNamePair>();
	private JCheckBox onlyDueDateCb = new JCheckBox();

	/**
	 *  Dynamic Init
	 *  @throws Exception if Lookups cannot be initialized
	 *  @return true if initialized
	 */
	public boolean dynInit() throws Exception
	{
		log.config("");
		super.dynInit();
		dialog.setTitle(getTitle());
		onlyDueDateCb.setSelected(true);
		onlyDueDateCb.addActionListener(this);	
		
		return true;
	}   //  dynInit
    
	/**
	 *  Static Init.
	 *  <pre>
	 *  parameterPanel
	 *      parameterBankPanel
	 *      parameterStdPanel
	 *          bPartner/order/invoice/shopment/licator Label/Field
	 *  dataPane
	 *  southPanel
	 *      confirmPanel
	 *      statusBar
	 *  </pre>
	 *  @throws Exception
	 */
    private void jbInit() throws Exception
    {
    	paySelectionLabel.setText(Msg.getElement(Env.getCtx(), "C_PaySelection_ID", false));
    	onlyDueDateCb.setText(Msg.getMsg(Env.getCtx(), "OnylDueDate", true));
    	onlyDueDateCb.setToolTipText(Msg.getMsg(Env.getCtx(), "OnlyDueDate", false));

    	CPanel parameterPanel = dialog.getParameterPanel();
    	parameterPanel.setLayout(new BorderLayout());
    	CPanel parameterStdPanel = new CPanel(new GridBagLayout());
    	parameterPanel.add(parameterStdPanel, BorderLayout.CENTER);

    	parameterStdPanel.add(paySelectionLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
    			,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
    	parameterStdPanel.add(paySelectionField,  new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
    			,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 5), 0, 0));
    	parameterStdPanel.add(onlyDueDateCb, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
    			,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 5), 0, 0));    	
    }   //  jbInit

	/*************************************************************************/

	private boolean 	m_actionActive = false;
	
	/**
	 *  Action Listener
	 *  @param e event
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("Action=" + e.getActionCommand());
		
		if (m_actionActive)
			return;
		m_actionActive = true;
		if (log.isLoggable(Level.CONFIG)) log.config("Action=" + e.getActionCommand());
		//  Order
		if (e.getSource().equals(paySelectionField))
		{
			KeyNamePair pp = (KeyNamePair)paySelectionField.getSelectedItem();
			int C_PaySelection_ID = 0;
			if (pp != null)
				C_PaySelection_ID = pp.getKey();
			loadOrder(C_PaySelection_ID, onlyDueDateCb.isSelected());
		}
		//onlyDueDateCb
		else if (e.getSource().equals(onlyDueDateCb))
		{
			KeyNamePair pp = (KeyNamePair)paySelectionField.getSelectedItem();
			int C_PaySelection_ID = 0;
			if (pp != null)
				C_PaySelection_ID = pp.getKey();
			loadOrder(C_PaySelection_ID, onlyDueDateCb.isSelected());
		}
		m_actionActive = false;
	}   //  actionPerformed

	/**
	 *  Change Listener
	 *  @param e event
	 */
	public void vetoableChange (PropertyChangeEvent e)
	{
		if (log.isLoggable(Level.CONFIG)) log.config(e.getPropertyName() + "=" + e.getNewValue());
		dialog.tableChanged(null);
	}   //  vetoableChange
	
	/**
	 *  Load Data - Order
	 *  @param C_PaySelection_ID Order
	 *  @param forInvoice true if for invoice vs. delivery qty
	 *  @param M_Locator_ID
	 */
	protected void loadOrder (int C_PaySelection_ID, boolean isOnlyDueDate)
	{
		loadTableOIS(getPaySelectionData(C_PaySelection_ID, isOnlyDueDate));
	}   //  LoadOrder
	
	/**
	 *  Load Order/Invoice/Shipment data into Table
	 *  @param data data
	 */
	protected void loadTableOIS (java.util.Vector<?> data)
	{
		//  Remove previous listeners
		dialog.getMiniTable().getModel().removeTableModelListener(dialog);
		DefaultTableModel model = new DefaultTableModel(data, getOISColumnNames());
		model.addTableModelListener(dialog);
		dialog.getMiniTable().setModel(model);
		// 
		
		configureMiniTable(dialog.getMiniTable());
	}   //  loadOrder
	
	public void showWindow()
	{
		dialog.setVisible(true);
	}
	
	public void closeWindow()
	{
		dialog.dispose();
	}

	@Override
	protected void configureMiniTable(IMiniTable miniTable) {
		super.configureMiniTable(miniTable);
		
//		MiniTable swingTable = (MiniTable) miniTable;
//		TableColumn col = swingTable.getColumn(3);
//		col.setCellEditor(new InnerLocatorTableCellEditor());
	}

//	/**
//	 * Custom cell editor for setting locator from minitable.
//	 *
//	 * @author Daniel Tamm
//	 *
//	 */
//	public class InnerLocatorTableCellEditor extends AbstractCellEditor implements TableCellEditor {
//
//		/**
//		 *
//		 */
//		private static final long serialVersionUID = -7143484413792778213L;
//		KeyNamePair currentValue;
//		JTextField 	editor;
//
//		public Object getCellEditorValue() {
//			String locatorValue = editor.getText();
//			MLocator loc = null;
//			try {
//				// Lookup locator using value
//				loc = new Query(Env.getCtx(), MLocator.Table_Name, "value=?", null)
//									.setParameters(locatorValue)
//									.setClient_ID()
//									.first();
//				// Set new keyNamePair for minitable
//				currentValue = getLocatorKeyNamePair(loc.get_ID());
//
//			} catch (Exception e) {
//				String message = Msg.getMsg(Env.getCtx(), "Invalid") + " " + editor.getText();
//				JOptionPane.showMessageDialog(null, message);
//			}
//			return(currentValue);
//
//		}
//
//		public Component getTableCellEditorComponent(JTable table,
//				Object value, boolean isSelected, int row, int column) {
//
//			currentValue = (KeyNamePair)value;
//			editor = new JTextField();
//			editor.setText(currentValue.getName());
//			return(editor);
//
//		}
//
//		@Override
//		public Component getTableCellEditorComponent(JTable table,
//				Object value, boolean isSelected, int row, int column) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//	}

	@Override
	public Object getWindow() {
		return dialog;
	}
	
//	protected void initBPartner (boolean forInvoice) throws Exception
//	{
//		//  load BPartner
//		int AD_Column_ID = COLUMN_C_INVOICE_C_BPARTNER_ID;        //  C_Invoice.C_BPartner_ID
//		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.Search);
//		bPartnerField = new VLookup ("C_BPartner_ID", true, false, true, lookup);
//		int C_BPartner_ID = -1;
//		int C_Order_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, MUNSPackingListOrder.COLUMNNAME_C_Order_ID);
//		int C_Invoice_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, MUNSPackingListOrder.COLUMNNAME_C_Invoice_ID);
//		if(C_Order_ID > 0)
//		{
//			C_BPartner_ID = DB.getSQLValue(null, "SELECT C_BPartner_ID FROM C_Order WHERE C_Order_ID = ?", C_Order_ID);
//		}
//		else if(C_Invoice_ID > 0)
//		{
//			C_BPartner_ID = DB.getSQLValue(null, "SELECT C_BPartner_ID FROM C_Order WHERE C_Invoice_ID = ?", C_Invoice_ID);
//		}
//
//		bPartnerField.setValue(new Integer(C_BPartner_ID));
//		initBPOrderDetails(C_BPartner_ID, forInvoice);
//	}   //  initBPartner
//
	protected void initBPOrderDetails (int C_BPartner_ID)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID=" + C_BPartner_ID);
		KeyNamePair pp = new KeyNamePair(0,"");
		//  load PO Orders - Closed, Completed
		paySelectionField.removeActionListener(this);
		paySelectionField.removeAllItems();
		paySelectionField.addItem(pp);
		
		ArrayList<KeyNamePair> list = loadPaySelectionData(C_BPartner_ID);
		int C_paySelection_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "C_Order_ID");
		for(KeyNamePair knp : list)
		{
			paySelectionField.addItem(knp);
			if(knp.getKey() == C_paySelection_ID)
			{
				paySelectionField.setSelectedItem(knp);
			}
		}
		
		if(C_paySelection_ID > 0)
		{
			loadTableOIS(getOrderData(C_paySelection_ID, onlyDueDateCb.isSelected()));
		}
		paySelectionField.addActionListener(this);
		dialog.pack();

		initBPDetails(C_BPartner_ID);
	}   //  initBPartnerOIS
	
	public void initBPDetails(int C_BPartner_ID) 
	{
		initBPInvoiceDetails(C_BPartner_ID);
	}
	
	private void initBPInvoiceDetails(int C_BPartner_ID)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID" + C_BPartner_ID);
		
		ArrayList<KeyNamePair> list = loadPaySelectionData(C_BPartner_ID);
		for(KeyNamePair knp : list)
			paySelectionField.addItem(knp);
	}
//	
//	protected void loadInvoice (int C_Invoice_ID, int M_Locator_ID)
//	{
//		loadTableOIS(getInvoiceData(C_Invoice_ID, M_Locator_ID));
//	}
//	
//	private void checkProductUsingUPC()
//	{
//		String upc = upcField.getText();
//		DefaultTableModel model = (DefaultTableModel)dialog.getMiniTable().getModel();
//		// Lookup UPC
//		List<MProduct> products = MProduct.getByUPC(Env.getCtx(), upc, null);
//		for (MProduct product : products)
//		{
//			int row = findProductRow(product.get_ID());
//			if (row >= 0)
//			{
//				BigDecimal qty = (BigDecimal)model.getValueAt(row, 1);
//				model.setValueAt(qty, row, 1);
//				model.setValueAt(Boolean.TRUE, row, 0);
//				model.fireTableRowsUpdated(row, row);
//			}
//		}
//		upcField.setText("");
//		upcField.requestFocusInWindow();
//	}
//	
//	private int findProductRow(int M_Product_ID)
//	{
//		DefaultTableModel model = (DefaultTableModel)dialog.getMiniTable().getModel();
//		KeyNamePair kp;
//		for (int i=0; i<model.getRowCount(); i++) {
//			kp = (KeyNamePair)model.getValueAt(i, 4);
//			if (kp.getKey()==M_Product_ID) {
//				return(i);
//			}
//		}
//		return(-1);
//	}
}