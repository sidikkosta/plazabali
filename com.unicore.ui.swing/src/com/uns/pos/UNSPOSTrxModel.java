package com.uns.pos;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.MDocType;

import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPOSTerminal;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPOSTrxLine;
import com.uns.util.ErrorMsg;

import org.compiere.model.MProduct;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class UNSPOSTrxModel extends MUNSPOSTrx {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1003842730029076900L;
	
	public UNSPOSTrxModel (Properties ctx, int id, String trxName)
	{
		super(ctx, id, trxName);
	}

	public UNSPOSTrxModel (MUNSPOSSession session)
	{
		super(session);
	}
	
	public boolean deleteTransaction ()
	{
		return delete(true);
	}
	
	public void reload ()
	{
		super.load(get_TrxName());
		getLines(true);
	}
	
	@SuppressWarnings("nls")
	public static UNSPOSTrxModel createTransaction (MUNSPOSSession session, UNSPOSPanel panel)
	{
		panel.checkSession();
		UNSPOSTrxModel model = new UNSPOSTrxModel(session);
		if (null == panel.m_infoPanel)
		{
			ErrorMsg.setErrorMsg(session.getCtx(), "SaveError", "Mandatory Field Business Partner");
			return null;
		}
		
		model.setC_BPartner_ID(panel.m_infoPanel.getBPartner().get_ID());
		model.setM_Warehouse_ID(panel.getM_Warehouse_ID());
		model.setSalesRep_ID(panel.m_infoPanel.getSales().getKey());
		
		if (MUNSPOSTerminal.STORETYPE_RetailDutyFree.equals(panel.m_terminal.getStoreType()))
			model.setUNS_CustomerInfo_ID(panel.m_infoPanel.getCustomer().get_ID());
		
		model.setCustomerName(panel.m_infoPanel.getCustomer().getName());
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE "
				+ " C_BPartner_ID = ? AND IsActive = ?";
		
		int location_ID = DB.getSQLValue(session.get_TrxName(), sql, 
				panel.m_infoPanel.getBPartner().get_ID(), "Y");
		
		if (location_ID <= 0)
		{
			
			ErrorMsg.setErrorMsg(session.getCtx(), "SaveError", "Store has not been set location. Please Contact administrator");
			return null;
//			sql = "SELECT MIN(C_Location_ID) FROM C_Location";
//			int C_Location_ID = DB.getSQLValue(session.get_TrxName(), sql);
//			MBPartnerLocation bpLocation = new MBPartnerLocation(panel.m_infoPanel.getBPartner());
//			bpLocation.setC_Location_ID(C_Location_ID);
//			if (!bpLocation.save())
//			{
//				return null;
//			}
		}
		
		String docBaseType = MDocType.DOCBASETYPE_POSSales;
		if (panel.isOnRefund())
		{
			docBaseType = MDocType.DOCBASETYPE_POSReturn;
			model.setReference_ID(panel.getOriginalTrx().get_ID());
		}
		sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType = ?";
		int doctypeID = DB.getSQLValue(panel.get_TrxName(), sql, docBaseType);
		model.setC_DocType_ID(doctypeID);
		model.setC_BPartner_Location_ID(location_ID);
		model.setDateAcct(panel.m_session.getDateAcct());
		model.setDateDoc(panel.m_session.getDateDoc());
		model.setDateTrx(panel.getToday());
		model.setGrandTotal(Env.ZERO);
		model.setTotalAmt(Env.ZERO);
		model.setDiscount(Env.ZERO);
		model.setDiscountAmt(Env.ZERO);
		model.setDocAction("CO");
		model.setDocStatus("DR");
		model.setIsActive(true);
		
		if (!model.save())
		{
			return null;
		}
		
		return model;
	}
	
	@SuppressWarnings("nls")
	public MUNSPOSTrxLine createLine (MProduct product, BigDecimal qtyOrdered, BigDecimal price)
	{
		MUNSPOSTrxLine line = new MUNSPOSTrxLine(this);
		line.setM_Product_ID(product.get_ID());
		line.setC_UOM_ID(product.getC_UOM_ID());
		line.setPrice(price);
		line.setQtyOrdered(qtyOrdered);
		line.setQtyEntered(qtyOrdered);
		
		return line;
	}
}
