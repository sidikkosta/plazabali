/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import net.miginfocom.swing.MigLayout;

import org.adempiere.plaf.AdempierePLAF;
import org.compiere.model.MConversionRate;
import org.compiere.model.MRefList;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.swing.CTextArea;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSCustomerInfo;
import com.unicore.model.MUNSPOSTrxLine;
import com.unicore.model.MUNSPaymentTrx;
import com.unicore.model.X_UNS_DiscountTrx;
import com.unicore.ui.form.UNTPanel;

/**
 * @author nurse
 *
 */
public class UNSPOSRightPanel extends UNSPOSSubPanel 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5350640478314349854L;
	private CLabel l_base1 = null;
	private CLabel l_docDisc = null;
	private CLabel l_linDisc = null;
	private CLabel l_tax = null;
	private CLabel l_svcCharge = null;
	protected CLabel l_total = null;
	private CTextArea l_discInfo = null;
	private UNSPOSTable m_table;
	private CScrollPane m_scroll;
	private CLabel l_totalOriDoc;
	private CLabel ll_docDisc;
	private CLabel ll_linDisc;
	private CLabel ll_tax;
	private CLabel ll_svcCharge;
	private CLabel ll_discInfo;
	private JSeparator separator;
	
	private JPanel p_info;
	private UNTPanel p_total;
	private UNTPanel p_discInf;
	private UNTPanel p_totalOriDoc;
	private UNTPanel m_refundCustInfo;
	private JTextArea m_area;
	private CPanel p_button;
	public static enum KeyTableDisplay 
	{
		KEYBOARD, TABLE
	}
	
	protected KeyTableDisplay m_currentDisplay = KeyTableDisplay.KEYBOARD;
	private UNSPOSKeyboardPanel m_keyboard;
	private CScrollPane m_fnbTable;
	
	/**
	 * 
	 */
	public UNSPOSRightPanel(UNSPOSPanel pos) {
		super (pos);
		setLayout(new MigLayout("insets 0", "[fill]", "[]"));
	}
	
	public void initComponent ()
	{
		p_button = new CPanel(new MigLayout("insets 0, fill", "[fill]", "[fill]"));
		p_total = new UNTPanel(new MigLayout("","[]","[][][]"));
		p_total.setStroke(4, new Color(0,110,0));
		
		p_totalOriDoc = new UNTPanel(new MigLayout("fill","[fill]","[fill]"));
		p_totalOriDoc.setBackground(Color.RED);
		p_totalOriDoc.setStroke(4, new Color(0,110,0));
		l_totalOriDoc = new CLabel();
		p_totalOriDoc.setBackground(new Color(192, 0, 0));
		
		m_refundCustInfo = new UNTPanel(new MigLayout("fill", "[fill]", "[]"));
		m_refundCustInfo.setBackground(new Color(204, 193, 218));
		m_refundCustInfo.setStroke(4, new Color(0, 176, 80));
		m_area = new JTextArea();
		m_area.setEditable(false);
		m_area.setFocusable(false);
		m_area.setFont(new Font("Calibri", Font.BOLD, 12));
		m_refundCustInfo.add(m_area,"wrap, growx, growy, flowx, flowy");
		m_area.setBackground(new Color(204, 193, 218));
		m_area.setBorder(null);
		m_refundCustInfo.setPreferredSize(new Dimension(250, 150));
		m_refundCustInfo.setMaximumSize(new Dimension(250, 150));
		
		Font fTotal = new Font("Calibri", Font.BOLD, 14);
		Font fRupiah = new Font("Calibri", Font.BOLD, 28);
		Font fDetail = new Font("Calibri", Font.BOLD, 12);

		l_totalOriDoc.setFont(fRupiah);

		l_totalOriDoc.setHorizontalAlignment(SwingConstants.CENTER);
		l_totalOriDoc.setVerticalAlignment(SwingConstants.CENTER);
		l_totalOriDoc.setForeground(Color.white);
		p_totalOriDoc.add(l_totalOriDoc, "growx, center, growy, flowx, flowy, wrap");
		
		p_info = new JPanel(new MigLayout("insets 0, fill","[fill]",""));
		l_total = new CLabel("");
		l_total.setFont(fTotal);
		l_total.setForeground(Color.WHITE);
		l_total.setHorizontalAlignment(SwingConstants.LEFT);
		p_total.add(l_total, "wrap, spanx 2,pushx, flowx, growx, left");
		
		l_base1 = new CLabel("0");
		l_base1.setFont(fRupiah);
		l_base1.setHorizontalAlignment(SwingConstants.RIGHT);
		l_base1.setForeground(Color.WHITE);
		p_total.add(l_base1, "wrap,spanx 2, pushx, flowx, growx");
		
		separator = new JSeparator();
		separator.setForeground(Color.WHITE);
		ll_docDisc = new CLabel("Doc, Disc");
		ll_docDisc.setFont(fDetail);
		ll_docDisc.setHorizontalAlignment(SwingConstants.LEFT);
		ll_docDisc.setForeground(Color.WHITE);

		l_docDisc = new CLabel("0");
		l_docDisc.setFont(fDetail);
		l_docDisc.setHorizontalAlignment(SwingConstants.RIGHT);
		l_docDisc.setForeground(Color.WHITE);	
		
		ll_linDisc = new CLabel("Line Disc");
		ll_linDisc.setFont(fDetail);
		ll_linDisc.setHorizontalAlignment(SwingConstants.LEFT);
		ll_linDisc.setForeground(Color.WHITE);

		l_linDisc = new CLabel("0");
		l_linDisc.setFont(fDetail);
		l_linDisc.setHorizontalAlignment(SwingConstants.RIGHT);
		l_linDisc.setForeground(Color.WHITE);	
		
		ll_tax = new CLabel("Tax/PB1");
		ll_tax.setFont(fDetail);
		ll_tax.setHorizontalAlignment(SwingConstants.LEFT);
		ll_tax.setForeground(Color.WHITE);
		
		l_tax = new CLabel("0");
		l_tax.setFont(fDetail);
		l_tax.setHorizontalAlignment(SwingConstants.RIGHT);
		l_tax.setForeground(Color.WHITE);
		
		ll_svcCharge = new CLabel("Service Charge");
		ll_svcCharge.setFont(fDetail);
		ll_svcCharge.setHorizontalAlignment(SwingConstants.LEFT);
		ll_svcCharge.setForeground(Color.WHITE);
		
		l_svcCharge = new CLabel("0");
		l_svcCharge.setFont(fDetail);
		l_svcCharge.setHorizontalAlignment(SwingConstants.RIGHT);
		l_svcCharge.setForeground(Color.WHITE);
		add(p_total, "growx, growy, pushx, , wrap");
		
		p_discInf = new UNTPanel(new MigLayout("fill","[fill]",""));
		p_discInf.setBackground(new Color(232,59,123));
		p_discInf.setStroke(4, new Color(0,110,0));
		
		ll_discInfo = new CLabel("Discount Summary");
		ll_discInfo.setFont(fTotal);
		ll_discInfo.setForeground(Color.WHITE);
		ll_discInfo.setHorizontalAlignment(SwingConstants.CENTER);
		p_discInf.add(ll_discInfo, "wrap,pushx,spanx, flowx, growx");
		
		l_discInfo = new CTextArea();
		l_discInfo.setFocusable(false);
		l_discInfo.setEditable(false);
		l_discInfo.setLineWrap(true);
		l_discInfo.setText(null);
		l_discInfo.setBackground(Color.WHITE);
		l_discInfo.setForeground(Color.RED);
		l_discInfo.setFont(new Font("sansserif",Font.BOLD, 12));
		p_discInf.add(l_discInfo, "growx, growy, flowx, flowy,pushy,pushx, wrap");
		p_info.setBackground(getBackground());
		p_info.setBorder(null);
		add (p_info, "growx, growy, flowy, pushx, pushy, growx, wrap");
		
		m_table = new UNSPOSTable();
		m_table.setEnabled(false);
		m_table.setEvenColor(new Color(222, 231, 209));
		m_table.setOddColor(new Color(239, 243, 234));
		DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new String[]{"NO", "PAYMENT TYPE", "TOTAL"});
		m_table.setModel(model);
		m_table.setFillsViewportHeight(true);
		m_table.setFocusable(false);
		m_table.setRowHeight(30);
		m_table.getTableHeader().setBackground(new Color(155, 187, 89));
		m_table.getTableHeader().setForeground(Color.WHITE);
		m_scroll = new CScrollPane(m_table);
		this.m_table.getTableHeader().setPreferredSize(new Dimension(m_scroll.getWidth(), 30));
		m_table.getTableHeader().setFont(AdempierePLAF.getFont_Field().deriveFont(12f));
		m_table.getTableHeader().setBackground(new Color(128, 100, 162));
		m_table.setEvenColor(new Color(237, 234, 240));
		m_table.setOddColor(new Color(216, 211, 224));
		
		Dimension dim = new Dimension(250, 300);
		UNSPOSFNBTable fnbTable = new UNSPOSFNBTable(m_posPanel);
		fnbTable.init();
		m_fnbTable = new CScrollPane(fnbTable);
		m_fnbTable.setBorder(null);
		m_fnbTable.setMaximumSize(dim);
		m_fnbTable.setPreferredSize(dim);
		m_keyboard = new UNSPOSKeyboardPanel(m_posPanel);
		m_keyboard.init();
		m_keyboard.setFocusable(false);
		m_keyboard.setVisible(true);
		p_button.add(m_keyboard, "h 300px!, growx, growy, flowy, pushx, pushy, growx,");
		add (p_button, "");
		loadSaleDisplay();
		updateInfo();
	}

	@Override
	protected void init() 
	{
		initComponent();
	}
	
	public void updateInfo ()
	{
		BigDecimal totalOri = Env.ZERO;
		BigDecimal svcChrg = Env.ZERO;
		BigDecimal tax = Env.ZERO;
		BigDecimal totalB1 = Env.ZERO;
		BigDecimal lineDisc = Env.ZERO;
		BigDecimal docDisc = Env.ZERO;
		UNSPOSTrxModel model = m_posPanel.m_posTrx;
		if (model != null)
		{
			model.reload();
			totalOri = model.getGrandTotal();
			svcChrg = model.getServiceCharge();
			tax = model.getTaxAmt();
			if (totalOri == null)
				totalOri = Env.ZERO;
			if (svcChrg == null)
				svcChrg = Env.ZERO;
			if (tax == null)
				tax = Env.ZERO;
			
			totalB1 = MConversionRate.convert(
					getCtx(), totalOri, model.getC_Currency_ID(), 
					m_session.getBase1Currency_ID(), model.getDateTrx(), 0, 
					model.getAD_Client_ID(), model.getAD_Org_ID());
			
			docDisc = model.getDiscountAmt();
			MUNSPOSTrxLine[] lines = model.getLines(false);
			for (int i=0; i<lines.length; i++)
			{
				lineDisc = lineDisc.add(lines[i].getDiscountAmt());
			}
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					loadInfoDiscount();
				}
			});
			t.setName("Discount-Info-Loader");
			t.start();
		}
		else
			l_discInfo.setText("");

		String totalB1Format = NumberFormat.getInstance(Locale.ENGLISH).format(totalB1);
		String sql = "SELECT CurSymbol FROM C_Currency WHERE C_Currency_ID = ?";
		String idrSymbol = DB.getSQLValueString(get_TrxName(), sql, m_session.getBase1Currency_ID());
		StringBuilder t2Build = new StringBuilder("Total ");
		if (m_posPanel.isOnRefund())
			t2Build.append("Return / Refund ");
		t2Build.append(idrSymbol).append(" : ");
		String totalLabel = t2Build.toString();
		l_total.setText(totalLabel);
		l_base1.setText(totalB1Format);
		String svcChgStr = NumberFormat.getInstance().format(svcChrg.doubleValue());
		String taxStr = NumberFormat.getInstance().format(tax.doubleValue());
		String docDiscStr = NumberFormat.getInstance().format(docDisc.doubleValue());
		String lineDiscStr = NumberFormat.getInstance().format(lineDisc.doubleValue());
		l_svcCharge.setText(svcChgStr);
		l_tax.setText(taxStr);
		l_docDisc.setText(docDiscStr);
		l_linDisc.setText(lineDiscStr);
	}
	
	protected void loadInfoDiscount ()
	{
		if (this.m_posPanel.m_posTrx == null)
			return;
		StringBuilder infoDiscB = new StringBuilder();
		int h_ID = this.m_posPanel.m_posTrx.get_ID();
		String sql = "SELECT * FROM UNS_DiscountTrx WHERE UNS_POSTrx_ID = ? "
				+ " OR UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine "
				+ " WHERE UNS_POSTrx_ID = ?) AND RequirementType <> ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, h_ID);
			st.setInt(2, h_ID);
			st.setString(3, "PO");
			rs = st.executeQuery();
			while (rs.next())
			{
				String discountType = rs.getString("discounttype");
				String msg = "";
				if (X_UNS_DiscountTrx.DISCOUNTTYPE_Bonus.equals(discountType))
				{
					sql = "SELECT Name FROM M_Product WHERE M_Product_ID = ?";
					String productName = DB.getSQLValueString(get_TrxName(), sql, 
							rs.getInt("productbonus_id"));
					msg += "Product Bonuses " + productName + " @" 
							+ rs.getBigDecimal("QtyBonuses").setScale(2, RoundingMode.HALF_UP);
				}
				else if (X_UNS_DiscountTrx.DISCOUNTTYPE_Value.equals(discountType)
						|| X_UNS_DiscountTrx.DISCOUNTTYPE_Percent.equals(discountType))
				{
					BigDecimal d1 = rs.getBigDecimal("flatvaluediscount");
					BigDecimal d2 = rs.getBigDecimal("discountvalue1st");
					BigDecimal d3 = rs.getBigDecimal("discountvalue2nd");
					BigDecimal d4 = rs.getBigDecimal("discountvalue3rd");
					BigDecimal d5 = rs.getBigDecimal("discountvalue4th");
					BigDecimal d6 = rs.getBigDecimal("discountvalue5th");
					if (null == d1) d1 = Env.ZERO;
					if (null == d2) d2 = Env.ZERO;
					if (null == d3) d3 = Env.ZERO;
					if (null == d4) d4 = Env.ZERO;
					if (null == d5) d5 = Env.ZERO;
					if (null == d6) d6 = Env.ZERO;
					
					BigDecimal total = d1.add(d2).add(d3).add(d4).add(d5).add(d6);
					total = total.setScale(2, RoundingMode.HALF_UP);
					msg += "Diskon Amount " + total; 
				}
				
				infoDiscB.append("- ");
				infoDiscB.append(msg);
				infoDiscB.append("\n");
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		String infoDisc = infoDiscB.toString();
		l_discInfo.setText(infoDisc);
	}

	public void clearDiscountInfo ()
	{
		l_discInfo.setText("");
	}
	
	public void loadRefundDisplay (UNSPOSTrxModel refundModel)
	{
		p_total.remove(separator);
		p_total.remove(ll_docDisc);
		p_total.remove(l_docDisc);	
		p_total.remove(ll_linDisc);
		p_total.remove(l_linDisc);	
		p_total.remove(ll_tax);
		p_total.remove(l_tax);
		p_total.remove(ll_svcCharge);
		p_total.remove(l_svcCharge);
		p_info.remove(p_discInf);
		p_total.setBackground(new Color(192, 0, 0));
		String srcRefundAmt = NumberFormat.getInstance().format(refundModel.getGrandTotal().doubleValue());
		l_totalOriDoc.setText(srcRefundAmt);
		p_info.add(p_totalOriDoc, "wrap, growx, flowx");
		p_info.add(m_scroll, "wrap");
		p_info.add(m_refundCustInfo);
		loadCustomerInfo(refundModel);
		updateTable(refundModel.get_ID());
	}
	
	public void loadSaleDisplay ()
	{
		p_info.remove(p_totalOriDoc);
		p_info.remove(m_scroll);
		p_info.remove(m_refundCustInfo);
		
		p_total.setBackground(new Color(232,59,123));
		p_total.add(separator, "spanx 2, wrap, pushx, flowx, growx");
		p_total.add(ll_docDisc, ", pushx, flowx, growx");
		p_total.add(l_docDisc, "wrap, pushx, flowx, growx");	
		p_total.add(ll_linDisc, ", pushx, flowx, growx");
		p_total.add(l_linDisc, "wrap, pushx, flowx, growx");	
		p_total.add(ll_tax, ", pushx, flowx, growx");
		p_total.add(l_tax, "wrap, pushx, flowx, growx");
		p_total.add(ll_svcCharge, ", pushx, flowx, growx");
		p_total.add(l_svcCharge, "wrap, pushx, flowx, growx");
		p_info.add(p_discInf, "growx, growy, flowy, pushx, pushy, growx, wrap");
	}
	
	private void updateTable (int posTrxID)
	{
		if (m_table == null)
			return;
		String sql = "SELECT UNS_POSPayment_ID FROM UNS_POSPayment WHERE UNS_POSTrx_ID = ?";
		int id = DB.getSQLValue(get_TrxName(), sql, posTrxID);
		sql = "SELECT paymentmethod, trxamt FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID = ? ";
		PreparedStatement st = null;
		ResultSet rs = null;
		m_table.setRowCount(0);
		int no = 1;
		DefaultTableModel model = (DefaultTableModel) m_table.getModel();
		try
		{
			st = DB.prepareStatement(sql, m_posPanel.get_TrxName());
			st.setInt(1, id);
			rs = st.executeQuery();
			while (rs.next())
			{
				String pm = rs.getString(1);
				pm = MRefList.getListName(getCtx(), MUNSPaymentTrx.REFERENCE_TRXTYPE_ID, pm);
				BigDecimal total = rs.getBigDecimal(2);
				if (total == null)
					total = Env.ZERO;
				String totalStr = NumberFormat.getInstance().format(total.doubleValue());
				model.addRow(new Object[]{no++, pm, totalStr});
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		for (int i=model.getRowCount(); i<15;i++)
		{
			model.addRow(new Object[]{null, null, null, null, null, null});
		}		
	}
	
	private void loadCustomerInfo (UNSPOSTrxModel model)
	{
		MUNSCustomerInfo info = new MUNSCustomerInfo(getCtx(), model.getUNS_CustomerInfo_ID(), get_TrxName());
		if (info.getUNS_CustomerInfo_ID() == 0)
			info.setName(model.getCustomerName());
		
		StringBuilder sb = new StringBuilder();
		String sql = "SELECT CountryCode3 FROM C_Country WHERE C_Country_ID = ?";
		String country = DB.getSQLValueString(get_TrxName(), sql, info.getC_Country_ID());
		String customerType = MRefList.getListName(getCtx(), MUNSCustomerInfo.REFERENCE_CUSTOMER_TYPE, info.getCustomerType());
		String vipType = MRefList.getListName(getCtx(), MUNSCustomerInfo.REFERENCE_VIP_TYPE, info.getVIPType());
		if (vipType == null || "null".equals(vipType))
			vipType = " -";
		if (customerType == null || "null".equals(customerType))
			customerType = " -";
		String nik = info.getNIK();
		if (nik == null)
			nik = " -";
		if (country == null)
			country = "-";
		sb.append("Customer\t: ");
		sb.append(info.getName() == null ? "-" : info.getName()).append("\n");
		sb.append("Customer Type\t: ");
		sb.append(customerType).append("\n");
		sb.append("Member Type\t: ");
		sb.append(vipType).append("\n");
		sb.append("Passpor No\t: ");
		sb.append(info.getPassportNo() == null ? "-" : info.getPassportNo()).append("\n");
		sb.append("Nationality\t: ");
		sb.append(country).append("\n");
		sb.append("Flight\t: ");
		sb.append(info.getFlightNo() == null ? "-" : info.getFlightNo()).append("\n");
		sb.append("NIK/VIP No\t: ");
		sb.append(nik);
		String infoMsg = sb.toString();
		m_area.setText(infoMsg);
	}
	
	public void keyTableUpdate ()
	{
		if (m_currentDisplay.equals(KeyTableDisplay.KEYBOARD))
		{
			p_button.remove(m_keyboard);
			p_button.add(m_fnbTable,"growx, growy, flowy, pushx, pushy, growx,");
			m_currentDisplay = KeyTableDisplay.TABLE;
		}
		else
		{
			p_button.remove(m_fnbTable);
			p_button.add(m_keyboard, "h 300px!, growx, growy, flowy, pushx, pushy, growx,");
			m_currentDisplay = KeyTableDisplay.KEYBOARD;
		}
		updateUI();
	}
	
	public void stopReservationThread ()
	{
		JViewport vp = m_fnbTable.getViewport();
		Component[] comps = vp.getComponents();
		for (int i=0; i<comps.length; i++)
		{
			if (comps[i] instanceof UNSPOSFNBTable)
				((UNSPOSFNBTable) comps[i]).stopReservationThread();
		}
	}
}
