package com.uns.pos;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import javax.swing.KeyStroke;
import org.compiere.apps.AppsAction;
import org.compiere.swing.CButton;
import org.compiere.swing.CPanel;
import com.unicore.model.MUNSPOSSession;

public abstract class UNSPOSSubPanel extends CPanel implements ActionListener {
	
	protected UNSPOSPanel		m_posPanel = null;
	protected MUNSPOSSession	m_session = null;
	private Properties		m_ctx = null;
	private String 				m_trxName = null;
	private final int			m_width = 50;
	private final int			m_height = 50;

	public String get_TrxName ()
	{
		return this.m_trxName;
	}
	
	public Properties getCtx ()
	{
		return this.m_ctx;
	}
	
	public UNSPOSSubPanel(UNSPOSPanel posPanel) 
	{
		super ();
		this.m_posPanel = posPanel;
		this.m_session = posPanel.m_session;
		this.m_ctx = posPanel.getCtx();
		this.m_trxName = posPanel.get_TrxName();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7483515118515406428L;

	protected abstract void init();

	public void printTicket ()
	{
		//TODO print struk
	}
	
	public void dispose ()
	{
		this.m_posPanel = null;
		this.m_session = null;
		this.m_ctx = null;
		this.m_trxName = null;
	}
	
	protected CButton createButtonAction (String action, KeyStroke accelerator)
	{
		AppsAction act = new AppsAction(action, accelerator, false);
		act.setDelegate(this);
		CButton button = (CButton)act.getButton();
		button.setPreferredSize(new Dimension(this.m_width, this.m_height));
		button.setMinimumSize(getPreferredSize());
		button.setMaximumSize(getPreferredSize());
		button.setFocusable(false);
		return button;
	}
	
	protected CButton createButton (String text)
	{
		CButton button = new CButton(text);
		button.addActionListener(this);
		button.setPreferredSize(new Dimension(this.m_width, this.m_height));
		button.setMinimumSize(getPreferredSize());
		button.setMaximumSize(getPreferredSize());
		button.setFocusable(false);
		return button;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		//Do nothing
	}
}
