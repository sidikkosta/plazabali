/**
 * 
 */
package com.uns.pos;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.compiere.apps.ADialog;
import org.compiere.apps.AEnv;
import org.compiere.model.MSysConfig;
import org.compiere.swing.CButton;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CDialog;
import org.compiere.swing.CPanel;
import org.compiere.swing.CPassword;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Login;
import org.compiere.util.ValueNamePair;

import com.uns.model.MUNSJobCareTaker;

/**
 * @author nurse
 *
 */
public class UNSPOSSpvConfirmDialog extends CDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1968769406847730764L;
	private UNSPOSPanel m_panel;
	private CButton b_ok;
	private CButton b_cancel;
	private CComboBox f_users;
	private CPassword f_pass;
	private boolean m_approved = false;
	
	public boolean isApproved ()
	{
		return m_approved;
	}

	/**
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public UNSPOSSpvConfirmDialog(UNSPOSPanel panel, boolean modal) throws HeadlessException 
	{
		super(AEnv.getFrame(panel), modal);
		m_panel = panel;
		setPreferredSize(new Dimension(400,150));
		setMinimumSize(getPreferredSize());
		setResizable(false);
		setLocationByPlatform(true);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
		int y = (screenSize.height - getHeight()) / 2;
		setLocation(x, y);
		init();
	}
	
	private void init ()
	{
		Vector<ValueNamePair> users = getUserApproval();
		CPanel panel = new CPanel(new MigLayout("", "[fill]", "[][]"));
		panel.setBorder(new TitledBorder("Spv Confirmation"));
		f_users = new CComboBox(users);
		f_users.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) 
			{
				//DO nothing
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) 
			{
				if (arg0.isConsumed())
					return;
				if (arg0.getKeyCode() != KeyEvent.VK_ENTER)
					return;
				
				EventQueue.invokeLater(new Runnable() {
					
					@Override
					public void run() 
					{
						f_pass.requestFocus();
					}
				});
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) 
			{
				//Do nothing
			}
		});
		f_pass = new CPassword();
		b_ok = new CButton("OK");
		b_cancel = new CButton("CANCEL");
		b_ok.addActionListener(this);
		b_cancel.addActionListener(this);
		panel.add(f_users, "spanx, flowx, growx, pushx, wrap");
		panel.add(f_pass,  "spanx, flowx, growx, pushx, wrap");
		panel.add(b_ok,"split 2, right");
		panel.add(b_cancel, "right");
		getContentPane().add(panel);
		
		f_pass.addActionListener(this);
	}
	
	private Vector<ValueNamePair> getUserApproval ()
	{
		String sql = "SELECT AD_User_ID, Name, RealName FROM AD_User WHERE AD_User_ID IN "
				+ " (SELECT AD_User_ID FROM AD_User_Roles WHERE AD_Role_ID = ? OR AD_Role_ID IN "
				+ " (SELECT AD_Role_ID FROM AD_Role_Included WHERE Included_role_ID = ?))"
				+ " AND IsActive = 'Y'";
		PreparedStatement st = null;
		ResultSet rs = null;
		int userApprovalRoleID = MSysConfig.getIntValue("ROLE_POS_SPV", 0, Env.getAD_Client_ID(Env.getCtx()));
		Vector<ValueNamePair> retVal = new Vector<>();
		try
		{
			st = DB.prepareStatement(sql, m_panel.get_TrxName());
			st.setInt(1, userApprovalRoleID);
			st.setInt(2, userApprovalRoleID);
			rs = st.executeQuery();
			while (rs.next())
			{
				StringBuilder nb = new StringBuilder(rs.getString(2));
				if (rs.getString(3) != null)
					nb.append(" - ").append(rs.getString(3));
				String name = nb.toString();
				ValueNamePair pair = new ValueNamePair(rs.getString(2), name);
				retVal.add(pair);
				List<Integer> careTakers = MUNSJobCareTaker.getCareTakerRecursively(
						m_panel.get_TrxName(), rs.getInt(1), m_panel.getToday());
				for (int i=0; i<careTakers.size(); i++)
				{
					sql = "SELECT Concat(Name,';',RealName) FROM AD_User WHERE AD_User_ID  = ?";
					String keyVal = DB.getSQLValueString(m_panel.get_TrxName(), sql, careTakers.get(i));
					if (keyVal == null)
						continue;
					String[] split = keyVal.split(";");
					if (split == null || split.length != 2)
						continue;
					nb = new StringBuilder(split[0]);
					if (split[1] != null)
						nb.append(" - ").append(split[1]);
					
					name = nb.toString();
					ValueNamePair pair2 = new ValueNamePair(split[0], name);
					retVal.add(pair2);
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		AWTEvent currEvent = EventQueue.getCurrentEvent();
		if (currEvent instanceof FocusEvent)
			return;
		
		if (e.getSource().equals(b_cancel))
		{
			dispose();
		}
		else if (e.getSource().equals(b_ok) || e.getSource().equals(f_pass))
		{
			Object selected = f_users.getSelectedItem();
			if (selected == null)
			{
				ADialog.error(this.m_panel.getWindowNo(), this, "Please select user approval first");
				return;
			}
			ValueNamePair pair = (ValueNamePair) selected;
			String name = (String) pair.getValue();
			String inPwd = (String) f_pass.getValue();
			Login login = new Login(this.m_panel.getCtx());
			KeyNamePair[] clients = login.getClients(name, inPwd);
			m_approved = clients != null && clients.length > 0;
			if (!m_approved)
			{
				ADialog.error(this.m_panel.getWindowNo(), this, "Invalid Password");
				return;
			}
			dispose();
		}
	}
}
