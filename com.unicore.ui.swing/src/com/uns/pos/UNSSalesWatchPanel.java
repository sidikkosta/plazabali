/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import net.miginfocom.swing.MigLayout;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.util.DB;
import org.compiere.util.KeyNamePair;
import com.unicore.ui.form.UNTPanel;

/**
 * @author nurse
 *
 */
public class UNSSalesWatchPanel extends CPanel 
{

	private static final long serialVersionUID = 7848200020200343552L;
	private CLabel l_title;
	private UNTPanel m_titlepanel;
	private UNSSalesWatchTable m_table;
	Timer m_timer;
	private int m_orgID = 0;
	private boolean m_onManualUpdate = false;
	private CComboBox f_org;
	
	/**
	 * @param layout
	 */
	public UNSSalesWatchPanel(LayoutManager layout) 
	{
		super(layout);
		setBackground(Color.WHITE);
	}
	
	public void init ()
	{
		l_title = new CLabel("");
		Font cFont = new Font("Calibri", Font.BOLD, 24);
		l_title.setFont(cFont);
		l_title.setForeground(Color.WHITE);
		l_title.setHorizontalAlignment(SwingConstants.CENTER);
		f_org = new CComboBox(initOrgs());
//		f_org.setFont(cFont);
		f_org.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				m_onManualUpdate = true;
				m_orgID = ((KeyNamePair) f_org.getSelectedItem()).getKey();
				updateInfo();
				m_onManualUpdate = false;
			}
		});
		m_titlepanel  = new UNTPanel(new MigLayout("fill", "[fill]", "[]"));
		m_titlepanel.setBackground(new Color(75,172,198));
		m_titlepanel.setStrokeColor(new Color(53,125,145));
		m_titlepanel.add(l_title, "right,flowy, growx, pushx");
		m_titlepanel.add(f_org, "pushx,left, wrap");
		add(m_titlepanel, "top,wrap, pushx, spanx");
		m_table = new UNSSalesWatchTable();
		CScrollPane scroll = new CScrollPane(m_table);
		add(scroll, "pushx, wrap, growx, flowx, pushy, spanx, top");
		m_timer = new Timer(60*1000, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (m_onManualUpdate)
					return;
				updateInfo();
			}
		});
		m_timer.start();
		updateInfo();
	}
	
	private void updateInfo ()
	{
		StringBuilder msgBuild = new StringBuilder("Sales Watch (Net) ");
		Timestamp current = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy HH:mm");
		String dateStr = df.format(current);
		msgBuild.append(dateStr);
		String title = msgBuild.toString();
		l_title.setText(title);
		m_table.init(m_orgID);
	}
	
	public void dispose ()
	{
		if (m_timer != null)
			m_timer.stop();
	}
	
	private KeyNamePair[] initOrgs ()
	{
		String sql = "SELECT AD_Org_ID, Name FROM AD_Org WHERE IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		List<KeyNamePair> list = new ArrayList<>();
		try
		{
			st = DB.prepareStatement(sql, null);
			st.setString(1, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		KeyNamePair[] pairs = new KeyNamePair[list .size()];
		list.toArray(pairs);
		return pairs;
	}
}
