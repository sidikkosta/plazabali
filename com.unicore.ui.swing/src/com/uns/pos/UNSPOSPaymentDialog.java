package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;

import org.compiere.apps.AEnv;
import org.compiere.swing.CDialog;

/**
 * 
 * @author menjangan
 *
 */
public class UNSPOSPaymentDialog extends CDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7673531606502297282L;
	
	private UNSPOSPanel				m_panel 		= null;
	
	public UNSPOSPaymentDialog() throws HeadlessException {
		super ();
	}
	
	public UNSPOSPaymentDialog (UNSPOSPanel panel)
	{
		super(AEnv.getFrame(panel), true);
		this.m_panel = panel;
		setBackground(new Color(243, 250, 236));
		setMinimumSize(new Dimension(700,700));
		setPreferredSize(new Dimension(700,700));
		setResizable(false);
		setTitle("PAYMENT");		
		init ();
		pack();

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
		int y = (screenSize.height - getHeight()) / 2;
		setLocation(x, y);
	}

	private boolean init ()
	{
		UNSPOSPaymentPanel payPanel = new UNSPOSPaymentPanel(m_panel, this);
		payPanel.init();
		getContentPane().add(payPanel);
		return true;
	}
	
		
	public void closePanel ()
	{
		printStruk();
		dispose();
	}
	
	public void dispose ()
	{	
		m_panel.m_form.resetPayableOnCustomerDisplay();
		super.dispose();
	}
	
	private boolean printStruk ()
	{
		return this.m_panel.printTransaction(false);
	}
}
