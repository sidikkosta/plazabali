/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import net.miginfocom.swing.MigLayout;
import org.compiere.swing.CLabel;
import org.compiere.util.DB;
import com.unicore.ui.form.UNTPanel;

/**
 * @author nurse
 *
 */
public class UNSPOSTopPanel extends UNSPOSSubPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 760266258250328871L;
	/**
	 * 
	 */
	public UNSPOSTopPanel(UNSPOSPanel panel) {
		super(panel);
	}

	@Override
	protected void init() 
	{
		setLayout(new MigLayout("insets 0", "[260px!,fill][fill]", ""));
		ImageIcon icon = new ImageIcon(getClass().getResource("/com/unicore/images/plazabali2.png"));
		JLabel l = new JLabel();
		l.setIcon(icon);
		add(l, "flowy, growx, spany,");
		UNTPanel panel = new UNTPanel(new MigLayout("fill", "[]", "fill"));
		panel.setBackground(new Color(33,102,99));
		Font font = new Font("Calibri", Font.BOLD, 16);
		Font fontTime = new Font("Calibri", Font.BOLD, 20);
		panel.setStroke(4, new Color(126,192,181));
		String sql = "SELECT CONCAT (c.Name, ' (',o.Name,') ', ' - ', t.Name) FROM "
				+ " UNS_POSTerminal t INNER JOIN AD_Org o ON o.AD_Org_ID = t.AD_Org_ID "
				+ " INNER JOIN AD_Client c ON c.AD_Client_ID = t.AD_Client_ID WHERE "
				+ " t.UNS_POSTerminal_ID = ?";
		String name = DB.getSQLValueString(get_TrxName(), sql, 
				m_posPanel.m_session.getUNS_POSTerminal_ID());
		sql = "SELECT CONCAT(UPPER(rl.Name), ' - ', TO_CHAR(ss.DateAcct, 'DD Mon YYYY')) FROM UNS_POS_Session ss"
				+ " INNER JOIN AD_Ref_List rl ON rl.Value = ss.Shift AND AD_Reference_ID ="
				+ " (SELECT AD_Reference_ID FROM AD_Reference WHERE AD_Reference_UU = '23e74272-60b2-4d85-a9f8-15f375f6e734')"
				+ " WHERE ss.UNS_POS_Session_ID = ?";
		String time = DB.getSQLValueString(get_TrxName(), sql, 
				m_posPanel.m_session.get_ID());
		CLabel label = new CLabel(name);
		label.setFont(font);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.WHITE);
		CLabel labelTime = new CLabel(time);
		labelTime.setFont(fontTime);
		labelTime.setHorizontalAlignment(SwingConstants.CENTER);
		labelTime.setForeground(Color.YELLOW);
		panel.add(label, "spanx, pushx, pushy, growx, growy, flowx, flowy");
		panel.add(labelTime, "spanx, pushx, pushy, growx, growy, flowx, flowy");
		add(panel, "pushx, pushy, growx, growy");
	}
}
