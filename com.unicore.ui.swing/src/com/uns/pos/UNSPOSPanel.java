/**
 * 
 */
package com.uns.pos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import org.adempiere.util.ProcessUtil;
import org.compiere.apps.ADialog;
import org.compiere.model.MBPartner;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.MProduct;
import org.compiere.pos.POSKeyboard;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.swing.CFrame;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Trx;

import com.unicore.model.MUNSCardType;
import com.unicore.model.MUNSCashierTandem;
import com.unicore.model.MUNSCustomerInfo;
import com.unicore.model.MUNSPOSConfiguration;
import com.unicore.model.MUNSPOSFNBTable;
import com.unicore.model.MUNSPOSFNBTableLine;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPOSTerminal;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPOSTrxLine;
import com.unicore.model.MUNSStoreConfiguration;
import com.unicore.ui.form.UNTPanel;
import com.uns.util.UNSCardType;

/**
 * @author menjangan
 *
 */
public class UNSPOSPanel extends CPanel {

	/**
	 * 
	 */
	private static final long 			serialVersionUID = -3054963890123271654L;
	private int         				m_WindowNo = 0;
	private CFrame 						m_frame = null;
	private CLogger						m_log = CLogger.getCLogger(getClass());
	private Properties					m_ctx = null;
	private int							m_SalesRep_ID = 0;
	protected MUNSPOSSession			m_session = null;
	protected UNSSubCurrentLine 		f_curLine;
	protected UNSPOSTrxModel 			m_posTrx = null;
	private HashMap<Integer, POSKeyboard> m_mapKeyBoard = 
			new HashMap<Integer, POSKeyboard>();
	private String						m_trxName = null;
	private int							m_AD_Org_ID = 0;
	private MBPartner					m_partner = null;
	private int							m_warehouse_ID = 0;
	private UNSPOSTopPanel				m_topPanel = null;
	protected UNSPOSBotPanel				m_botPanel = null;
	protected UNSPOSRightPanel			m_rightPanel = null;
	protected UNSPOSInfoPanel				m_infoPanel = null;
	private CLabel						l_cashier = null;
	protected UNSPOSForm					m_form;
	private boolean 					m_onRefund = false;
	private UNSPOSTrxModel				m_originalTrx = null;
	private UNTPanel 					m_cashierPanel;	
	protected MUNSPOSConfiguration		m_posConfiguration = null;
	protected MUNSStoreConfiguration	m_storeConfig = null;
	protected MUNSPOSTerminal			m_terminal = null;
	protected MUNSPOSFNBTableLine[]		m_fnbTables = null;
	
	public boolean isOnRefund ()
	{
		return m_onRefund;
	}
	
	public void onRefund ()
	{
		m_onRefund = true;
	}
	
	public void cancelRefund ()
	{
		m_onRefund = false;
		m_originalTrx = null;
		createSaleDisplay();
	}
	
	public void setRefund (int postrxID)
	{
		m_originalTrx = new UNSPOSTrxModel(getCtx(), postrxID, get_TrxName());
		onRefund();
		UNSPOSQueryTicketLoader serverLoader = new UNSPOSQueryTicketLoader (
				null, null, null, null, m_originalTrx.getUNS_POSTrx_UU());
		serverLoader.getData();
		String sql = "SELECT UNS_POSTrx_ID FROM UNS_POSTrx WHERE Reference_ID = ? AND DocStatus IN (?,?,?)";
		int posID = DB.getSQLValue(get_TrxName(), sql, postrxID, "DR","IP","IN");
		boolean autoLoad = false;
		if (m_originalTrx.getUNS_POS_Session_ID() == m_session.get_ID())
		{
			int choose = JOptionPane.showConfirmDialog(
					this, "Do you want to auto load from original transaction?", "Auto Load", 
					JOptionPane.YES_NO_OPTION);
			if (JOptionPane.YES_OPTION == choose)
				autoLoad = true;
		}
		
		MUNSCustomerInfo info = new MUNSCustomerInfo(getCtx(), 
				m_originalTrx.getUNS_CustomerInfo_ID(), get_TrxName());
		if (info.getUNS_CustomerInfo_ID() == 0)
			info.setName(m_originalTrx.getCustomerName());
		m_infoPanel.setCustomer(info);
		String salesName = DB.getSQLValueString(get_TrxName(), 
				"SELECT COALESCE (RealName, Name ) FROM AD_User WHERE AD_User_ID = ?", 
				m_originalTrx.getSalesRep_ID());
		m_infoPanel.setSales(m_originalTrx.getSalesRep_ID(), salesName);
		if (posID <= 0 && autoLoad)
		{
			m_posTrx = UNSPOSTrxModel.createTransaction(m_session, this);
			if (m_posTrx == null)
			{
				ADialog.error(getWindowNo(), this,  CLogger.retrieveErrorString("Failed when try to create new transaction"));
				try 
				{
					DB.rollback(true, get_TrxName());
				}
				catch (SQLException e) 
				{
					e.printStackTrace();
				}
				cancelRefund();
				return;
			}
			
			posID = m_posTrx.get_ID();
		}
		else
		{
			m_posTrx = new UNSPOSTrxModel(getCtx(), posID, get_TrxName());
		}
		
		if (autoLoad)
		{
			MUNSPOSTrxLine[] oLines = m_originalTrx.getLines(true);
			MUNSPOSTrxLine[] lines = m_posTrx.getLines(true);
			for (int i=0; i<oLines.length; i++)
			{
				MUNSPOSTrxLine newLine = null;
				for (int j=0; j<lines.length; j++)
				{
					if (oLines[i].getM_Product_ID() == lines[j].getM_Product_ID()
							&& oLines[i].getBarcode().equals(lines[j].getBarcode()))
					{
						newLine = lines[j];
						break;
					}
				}
				if (newLine == null)
				{
					newLine = m_posTrx.createLine(MProduct.get(getCtx(), oLines[i].getM_Product_ID()), 
							oLines[i].getQtyOrdered().negate(), oLines[i].getPriceActual());
					newLine.setQtyBonuses(oLines[i].getQtyBonuses().negate());
					newLine.setQtyEntered(oLines[i].getQtyEntered().negate());
					newLine.setisProductBonuses(oLines[i].isProductBonuses());
					newLine.setBarcode(oLines[i].getBarcode());
					newLine.setETBBCode(oLines[i].getETBBCode());
					newLine.setSKU(oLines[i].getSKU());
					newLine.setIsBOM(oLines[i].isBOM());
					if (!newLine.save())
					{
						ADialog.error(getWindowNo(), this,  CLogger.retrieveErrorString("Failed when try to create new transaction"));
						try 
						{
							DB.rollback(true, get_TrxName());
						}
						catch (SQLException e) 
						{
							e.printStackTrace();
						}
						cancelRefund();
						return;
					}
				}
			}
			
			try 
			{
				DB.commit(true, get_TrxName());
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			
			if (m_posTrx.getDiscount().compareTo(m_originalTrx.getDiscount()) != 0)
			{
				m_posTrx.load(get_TrxName());
				m_posTrx.setDiscount(m_originalTrx.getDiscount());
				if (!m_posTrx.save())
				{
					ADialog.error(getWindowNo(), this, CLogger.retrieveErrorString("Failed when try to create new transaction"));
					try 
					{
						DB.rollback(true, get_TrxName());
					}
					catch (SQLException e) 
					{
						e.printStackTrace();
					}
					cancelRefund();
					return;
				}
			}
			f_curLine.m_table.stopEditor(false);
		}
		else
			setPOSTrx(posID);
		
		updateInfo();
		if (posID <= 0)
		{
			m_infoPanel.setCustomer(info);
			m_infoPanel.setSales(m_originalTrx.getSalesRep_ID(), salesName);
		}
		f_curLine.m_table.setEnabled(true);
		f_curLine.m_table.requestFocus();
		for (int i=0; i<f_curLine.m_table.getRowCount(); i++)
		{
			if (f_curLine.m_table.getValueAt(i, 0) == null)
			{
				f_curLine.m_table.getSelectionModel().setSelectionInterval(i, i);
				f_curLine.m_table.editCellAt(i, 2);
				break;
			}
		}
		createRefundDisplay();
	}
	
	public UNSPOSTrxModel getOriginalTrx ()
	{
		return m_originalTrx;
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("nls")
	public UNSPOSPanel() 
	{
		super (new MigLayout(" fill","[fill][250px!, fill]",""));		
		this.m_ctx = Env.getCtx();
		this.m_AD_Org_ID = Env.getAD_Org_ID(getCtx());
	}

	@SuppressWarnings({ "nls" })
	public void init (int windowNo, CFrame frame, UNSPOSForm form)
	{
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setMinimumSize(new Dimension(980,600));
		frame.setResizable(true);
		
		m_form = form;
		this.m_SalesRep_ID = Env.getAD_User_ID(this.m_ctx);
		if (this.m_log.isLoggable(Level.INFO)) this.m_log.info(
				"init - SalesRep_ID=" + this.m_SalesRep_ID);
		this.m_WindowNo = windowNo;
		this.m_frame = frame;
		this.m_frame.setJMenuBar(null);
		//
		try
		{
			m_posConfiguration = MUNSPOSConfiguration.getActive(get_TrxName(), Env.getAD_Org_ID(Env.getCtx()));
			if (m_posConfiguration == null)
			{
				m_form.dispose();
				return;
			}
			if (!dynInit())
			{
				m_form.dispose();
				return;
			}
			
			this.m_frame.getContentPane().add(this, BorderLayout.CENTER);
		}
		catch(Exception e)
		{
			this.m_log.log(Level.SEVERE, "init", e);
		}
		if (this.m_log.isLoggable(Level.CONFIG)) this.m_log.config( 
				"PosPanel.init - " + getPreferredSize());
	}
	
	public void dispose ()
	{	
		if (m_session != null)
		{
			MUNSCashierTandem tandem = MUNSCashierTandem.get(get_TrxName(), m_session.get_ID(), m_SalesRep_ID);
			tandem.setIsSessionActive(false);
			tandem.save();
			try 
			{
				DB.commit(true, get_TrxName());
			}
			catch (IllegalStateException | SQLException e) 
			{
				e.printStackTrace();
			}
		}
		
		if (m_mapKeyBoard != null)
			this.m_mapKeyBoard.clear();
		this.m_mapKeyBoard = null;
		if (this.f_curLine != null)
		{
			this.f_curLine.dispose();
		}
		if (m_rightPanel != null)
			m_rightPanel.stopReservationThread();
		this.f_curLine = null;
		if (this.m_frame != null)
			this.m_frame.dispose();
		this.m_frame = null;
		this.m_ctx = null;
		if (m_trxName != null)
		{
			Trx trx = Trx.get(m_trxName, false);
			trx.commit();
			trx.close();
		}
		
		this.m_trxName = null;
	}

	public String get_TrxName ()
	{
		return this.m_trxName;
	}
	
	@SuppressWarnings("nls")
	private boolean dynInit()
	{
		m_trxName = Trx.createTrxName("POS");
		this.setBackground(new Color(243, 250, 236));
		if (!initSession())
		{
			ADialog.error(this.m_WindowNo, this.m_frame, "No POS Session.");
			return false;
		}
		
		this.m_partner = MBPartner.get(getCtx(), m_terminal.getStore_ID());
		if (null == this.m_partner)
		{
			ADialog.error(this.m_WindowNo, this.m_frame, "Unidentified Non Member Partner.");
			return false;
		}
		
		this.m_warehouse_ID = Env.getContextAsInt(getCtx(), "#M_Warehouse_ID");
		if (this.m_warehouse_ID <= 0)
		{
			ADialog.error(this.m_WindowNo, this.m_frame, "Unidentified Warehouse.");
			return false;
		}
		MUNSPOSFNBTable fnbTable = MUNSPOSFNBTable.get(get_TrxName(), m_partner.get_ID());
		if (fnbTable == null)
		{
			m_fnbTables = new MUNSPOSFNBTableLine[0];
		}
		else
			m_fnbTables = fnbTable.getLines();
		this.m_frame.setTitle("UntaCore POS: " + this.m_session.getName());
		//Top Panel
		m_topPanel = new UNSPOSTopPanel(this);
		m_topPanel.init();
		m_topPanel.setVisible(true);
		this.add(m_topPanel,"split 5, flowy, growx, spany,");
		
		m_infoPanel = new UNSPOSInfoPanel(this);
		m_infoPanel.init();
		add (m_infoPanel, "flowy, growx, spany, spanx, pushx,");
		
		String cashier = "Cashier";
		String sql2 = "SELECT COALESCE(RealName,Name) FROM AD_User WHERE AD_User_ID = ? AND IsActive = 'Y'";
		String cashierName = DB.getSQLValueString(get_TrxName(), sql2, m_SalesRep_ID);
		cashier += " : " + cashierName;
		l_cashier = new CLabel(cashier);
		Font cFont = new Font("Calibri", Font.BOLD, 18);
		l_cashier.setFont(cFont);
		l_cashier.setForeground(Color.WHITE);
		l_cashier.setHorizontalAlignment(SwingConstants.CENTER);
		
		m_cashierPanel  = new UNTPanel(new MigLayout("fill", "[fill]", "[]"));
		m_cashierPanel.setBackground(new Color(33,102,99));
		m_cashierPanel.add(l_cashier, "flowy, growx, spany, spanx, pushx");
		m_cashierPanel.setStroke(4, new Color(126, 192, 181));
		add(m_cashierPanel);
		
		this.f_curLine = new UNSSubCurrentLine(this);
		this.f_curLine.init();
		add (this.f_curLine, "h 60%, growx, growy, flowy, gaptop 2");
		m_botPanel = new UNSPOSBotPanel(this);
		m_botPanel.init();
		this.add(m_botPanel, "growx, growy,flowx, flowy");
		
		m_rightPanel = new UNSPOSRightPanel(this);
		m_rightPanel.initComponent();
		add (m_rightPanel, ", split 2,pushx, pushy, growy, flowy, wrap");	
		m_infoPanel.reset();
//		initCardType();
		return true;
	}
	
	private boolean initSession ()
	{
		if (this.m_SalesRep_ID == 0)
		{
			return false;
		}
		else if (this.m_AD_Org_ID == 0)
		{
			return false;
		}
		
		this.m_session = MUNSPOSSession.get(this.m_ctx, this.m_trxName, 
				this.m_AD_Org_ID, this.m_SalesRep_ID);
		if (this.m_session == null)
		{
			return false;
		}
		MUNSCashierTandem[] tandems = m_session.getTandems(false);
		MUNSCashierTandem me = null;
		for (int i=0; i<tandems.length; i++)
		{
			if (tandems[i].getCashier_ID() == m_SalesRep_ID)
			{
				me = tandems[i];
				continue;
			}
			if (tandems[i].isSessionActive())
				return false;
		}
		if (me == null)
		{
			me = new MUNSCashierTandem(m_session);
			me.setCashier_ID(m_SalesRep_ID);
			if (m_session.getCashier_ID() == me.getCashier_ID())
				me.setIsMainCashier(true);
		}
		me.setIsSessionActive(true);
		me.setIsLogin(true);
		try
		{
			me.saveEx();
		}
		catch (Exception ex)
		{
			try 
			{
				DB.rollback(true, get_TrxName());
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				return false;
			}
			return false;
		}
		
		try 
		{
			DB.commit(true, get_TrxName());
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return false;
		}
		
		if (m_session.getUNS_POSTerminal_ID() == 0)
			return false;
		m_terminal = new MUNSPOSTerminal(getCtx(), m_session.getUNS_POSTerminal_ID(), get_TrxName());
		m_storeConfig = MUNSStoreConfiguration.get(getCtx(), m_terminal.getStore_ID(), get_TrxName());
		if (m_storeConfig == null)
			return false;
		
		return true;
	}
	
	public Properties getCtx ()
	{
		return this.m_ctx;
	}
	
	public void updateInfo ()
	{
		if (this.m_posTrx != null && this.m_posTrx.get_ID() > 0)
		{
			this.m_posTrx.reload();
			m_form.updateCustomerDisplay(m_posTrx.get_ID(), m_posTrx.getGrandTotal());
		}
		else
			m_form.m_customerDisplay.showAdvertisement();
		if (this.f_curLine != null)
		{
			this.f_curLine.updateTable(this.m_posTrx);
		}
		if (this.m_infoPanel != null)
		{
			this.m_infoPanel.updateInfo();
		}
//		if (m_rightPanel != null)
//			m_rightPanel.updateInfo();
		enableComponent();
	}
	
	public void setOldPOSTrx (int UNS_POSTrx_ID)
	{
		if (this.m_posTrx != null)
		{
			this.m_posTrx.deleteEx(true);
		}
		if (UNS_POSTrx_ID == 0)
		{
			this.m_posTrx = null;
		}
		else
		{
			this.m_posTrx = new UNSPOSTrxModel(this.m_session);
		}
	}
	
	public void setPOSTrx (int UNS_POSTrx_ID)
	{
		if (UNS_POSTrx_ID <= 0)
		{
			this.m_posTrx = null;
		}
		else
		{
			this.m_posTrx = new UNSPOSTrxModel(getCtx(), UNS_POSTrx_ID, get_TrxName());
			f_curLine.m_table.stopEditor(false);
		}
		
		if (m_posTrx != null && m_posTrx.isReturn() && !isOnRefund() && m_posTrx.getReference_ID() > 0)
		{
			setRefund(m_posTrx.getReference_ID());
		}
	}
	
	/**
	 * 
	 * @param keyLayoutId
	 * @return
	 */
	public POSKeyboard getKeyboard(int keyLayoutId) 
	{
		if ( this.m_mapKeyBoard.containsKey(keyLayoutId) )
		{
			return this.m_mapKeyBoard.get(keyLayoutId);
		}
		else
		{
			POSKeyboard keyboard = new POSKeyboard(this, this.m_ctx, keyLayoutId);
			this.m_mapKeyBoard.put(keyLayoutId, keyboard);
			return keyboard;
		}
	}
	
	public Timestamp getToday ()
	{
		return new Timestamp(System.currentTimeMillis());
	}
	
	@SuppressWarnings("nls")
	public void newTransaction ()
	{
		this.m_log.info("Create POS Transaction");
		this.m_posTrx = null;
		if (m_infoPanel != null)
		{
			m_infoPanel.setC_BPartner_ID(0);
			m_infoPanel.reset();
		}
		f_curLine.m_table.stopEditor(false);
		cancelRefund();
		
		updateInfo();
	}
	
	public int getWindowNo ()
	{
		return this.m_WindowNo;
	}
	
	public MBPartner getNonMemberPartner ()
	{
		return this.m_partner;
	}
	
	public int getM_Warehouse_ID ()
	{
		return this.m_warehouse_ID;
	}
	
	private boolean printDocument (int recordID, int tableID, String columnName, 
			ProcessInfoParameter[] params, Trx trx)
	{
		String sql = "SELECT AD_Process_ID FROM AD_Column WHERE ColumnName = ? AND AD_Table_ID = ?";
		int processID = DB.getSQLValue(get_TrxName(), sql, columnName, tableID);
		if (processID <= 0)
		{
			return false;
		}
		
		MProcess process = new MProcess(getCtx (), processID , get_TrxName());
		MPInstance instance = new MPInstance(process, recordID);
		instance.saveEx();
		
		ProcessInfo poInfo = new ProcessInfo(process.getName(), process.getAD_Process_ID());
	    poInfo.setParameter(params);
	    poInfo.setRecord_ID(recordID);
	    poInfo.setAD_Process_ID(process.getAD_Process_ID());
	    poInfo.setAD_PInstance_ID(instance.getAD_PInstance_ID());
	    
	    return ProcessUtil.startJavaProcess(getCtx (), poInfo, trx);
	}
	
	public boolean printCurrencyRate ()
	{
		int record_ID = m_session.get_ID();
		ProcessInfoParameter[] params = new ProcessInfoParameter[1];
		params[0] = new ProcessInfoParameter("UNS_POS_Session_ID", record_ID, null, null, null);
	    return printDocument(record_ID, MUNSPOSSession.Table_ID, "RepCurrRate", 
	    		params, Trx.get(Trx.createTrxName("PPrint_"), true));
	}
	
	
	public boolean printTransaction (boolean reprint)
	{
		int record_ID = m_posTrx.get_ID();	
		ProcessInfoParameter[] params = new ProcessInfoParameter[2];
		params[0] = new ProcessInfoParameter("UNS_POSTrx_ID", record_ID, null, null, null);
		params[1] = new ProcessInfoParameter("CopyOfReceupt", reprint, null, null, null);
	    return printDocument(record_ID, MUNSPOSTrx.Table_ID, MUNSPOSTrx.COLUMNNAME_PrintDocument, 
	    		params, Trx.get(Trx.createTrxName("PPrint_"), true));
	}
	
	public boolean deleteTransaction ()
	{
		if (m_posTrx != null && !m_posTrx.delete(true))
			return false;
		newTransaction();
		return true;
	}
	
	private boolean p_onPaymentProcess = false;
	protected void doPay ()
	{
		if (p_onPaymentProcess)
			return;
		p_onPaymentProcess = true;
		UNSPOSPaymentDialog dialog = new UNSPOSPaymentDialog(this);
		dialog.setVisible(true);
		p_onPaymentProcess = false;
		updateInfo();
	}
	
	protected void doCancelPay ()
	{
		
	}
	
	private void enableComponent ()
	{
		if (m_posTrx == null)
		{
			m_infoPanel.setReadWrite(true);
			f_curLine.m_table.setEnabled(false);
		}
		else if (m_posTrx.isProcessed())
		{
			m_infoPanel.setReadWrite(false);
			f_curLine.m_table.setEnabled(false);
		}	
		else
		{
			m_infoPanel.setReadWrite(true);
			f_curLine.m_table.setEnabled(true);
		}
	}
	
	public void createRefundDisplay ()
	{
		String cashier = l_cashier.getText();
		cashier += " (Return / Refund)";
		l_cashier.setText(cashier);
		m_cashierPanel.setBackground(new Color(192, 0, 0));
		m_rightPanel.loadRefundDisplay(m_originalTrx);
		f_curLine.loadRefundDisplay();
		m_botPanel.loadRefundDisplay(m_originalTrx);
		updateUI();
	}
	
	public void createSaleDisplay ()
	{
		String cashier = "Cashier";
		String sql2 = "SELECT COALESCE(RealName,Name) FROM AD_User WHERE AD_User_ID = ?";
		String cashierName = DB.getSQLValueString(get_TrxName(), sql2, m_SalesRep_ID);
		cashier += " : " + cashierName;
		m_infoPanel.reset();
		l_cashier.setText(cashier);
		m_cashierPanel.setBackground(new Color(0,110,0));
		f_curLine.loadSaleDisplay();
		m_botPanel.loadSalesDisplay();
		m_rightPanel.loadSaleDisplay();
		setPOSTrx(0);
		updateInfo();
		updateUI();
	}
	
	public void reserveTable (KeyNamePair table)
	{
		if (table == null)
			return;
		
		String sql = "SELECT UNS_POSTrx_ID FROM UNS_POSTrx WHERE DocStatus IN ('DR','IP','IN') AND UNS_POSFNBTableLine_ID = ?";
		int ID = DB.getSQLValue(get_TrxName(), sql, table.getKey());
		if (ID > 0)
		{
			if (isOnRefund())
				cancelRefund();
			setPOSTrx(ID);
			updateInfo();
			updateUI();
		}
		else if (m_posTrx != null)
		{
			m_posTrx.setUNS_POSFNBTableLine_ID(table.getKey());
			try
			{
				m_posTrx.saveEx();
				DB.commit(true, m_posTrx.get_TrxName());
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				ADialog.error(getWindowNo(), this, ex.getMessage());
			}
		}
	}
	
//	private static Hashtable<String, Integer> m_cardType;
	
//	private void initCardType ()
//	{
//		m_cardType = new Hashtable<>();
//		MUNSCardType[] cardType = MUNSCardType.get(m_trxName);
//		for (int i=0; i<cardType.length; i++)
//		{
//			String[] values = cardType[i].getParsedValue();
//			for (int j=0; j<values.length; j++)
//			{
//				m_cardType.put(values[j], cardType[i].get_ID());
//			}
//		}
//	}
	
	public int getEDCCardType (String cardNo)
	{
		int retVal = -1;
		if (cardNo == null)
		{
			return MUNSCardType.getIDByNameOrValue(get_TrxName(), "Other");
		}
		if (cardNo.length() > 6)
			cardNo = cardNo.substring(0, 6);
		
		UNSCardType ct = new UNSCardType();
		for (int i=cardNo.length(); i>0; i--)
		{
//			Integer id = m_cardType.get(cardNo);
			Integer id = ct.get(cardNo);
			if (id != null)
			{
				retVal = id.intValue();
				break;
			}
			cardNo = cardNo.substring(0,i-1);
		}
		return retVal;
	}
	
	public void checkSession()
	{
		m_session.load(m_trxName);
		if (MUNSPOSSession.DOCSTATUS_Completed.equals(m_session.getDocStatus())
				|| MUNSPOSSession.DOCSTATUS_Closed.equals(m_session.getDocStatus())
				|| MUNSPOSSession.DOCSTATUS_Reversed.equals(m_session.getDocStatus())
				|| MUNSPOSSession.DOCSTATUS_Voided.equals(m_session.getDocStatus()))
		{
			ADialog.error(getWindowNo(), this,  CLogger.retrieveErrorString(
					"Could not continue transaction. session completed. Session name " + m_session.getName()));
			m_form.dispose();
		}
	}
}