/**
 * 
 */
package com.uns.pos;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.math.BigDecimal;
import org.compiere.apps.form.FormFrame;
import org.compiere.apps.form.FormPanel;

/**
 * @author menjangan
 *
 */
public class UNSPOSForm implements FormPanel {
	
	private UNSPOSPanel m_panel = null;
	UNSPOSCustomerDisplay m_customerDisplay;

	/* (non-Javadoc)
	 * @see org.compiere.apps.form.FormPanel#init(int, org.compiere.apps.form.FormFrame)
	 */
	@Override
	public void init(int WindowNo, FormFrame frame) 
	{
		this.m_panel = new UNSPOSPanel();
		this.m_panel.init(WindowNo, frame, this);
		if (m_panel.m_session != null)
		{
//			Thread thread = new Thread(new Runnable() {
//				
//				@Override
//				public void run() 
//				{
//					try 
//					{
//						Thread.sleep(3000);
//					} 
//					catch (InterruptedException e) 
//					{
//						e.printStackTrace();
//					}
					m_customerDisplay = new UNSPOSCustomerDisplay(m_panel);
					GraphicsEnvironment ge = GraphicsEnvironment
						        .getLocalGraphicsEnvironment();
				    GraphicsDevice[] gs = ge.getScreenDevices();
				    if (gs == null || gs.length <= 1)
				    {
				    	m_customerDisplay.setVisible(true);
				    }
				    else
				    {
				    	Rectangle rec = gs[1].getDefaultConfiguration().getBounds();
				    	m_customerDisplay.setLocation(rec.x, rec.y);
				    	m_customerDisplay.setVisible(true);
				    }
//				}
//			});
//			thread.setName("CustomerDisplayForm");
//			thread.start();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.apps.form.FormPanel#dispose()
	 */
	@Override
	public void dispose() 
	{
		if (m_customerDisplay != null)
			this.m_customerDisplay.dispose ();
		if (this.m_panel != null)
			m_panel.dispose();
	}
	
	public void updateCustomerDisplay (int postrxID, BigDecimal total)
	{
		if (m_customerDisplay == null)
			return;
		
		m_customerDisplay.updateInfo(postrxID, total);
	}
	
	public void updatePayableOnCustomerDisplay (int currencyID, BigDecimal payable)
	{
		if (m_customerDisplay == null)
			return;
		m_customerDisplay.setPayable(currencyID, payable);
	}
	
	public void resetPayableOnCustomerDisplay ()
	{
		if (m_customerDisplay == null)
			return;
		m_customerDisplay.resetPayable();
	}
}
