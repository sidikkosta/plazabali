/**
 * 
 */
package com.uns.pos;

import java.awt.Dimension;
import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import org.compiere.apps.form.FormFrame;
import org.compiere.apps.form.FormPanel;
import org.compiere.swing.CFrame;

/**
 * @author nurse
 *
 */
public class UNSSalesWatchFormPanel implements FormPanel {

	private CFrame m_frame;
	private UNSSalesWatchPanel m_panel;
	
	/**
	 * 
	 */
	public UNSSalesWatchFormPanel() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.apps.form.FormPanel#init(int, org.compiere.apps.form.FormFrame)
	 */
	@Override
	public void init(int WindowNo, FormFrame frame) 
	{
		m_frame = frame;
		this.m_frame.setResizable(true);
		this.m_frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.m_frame.setPreferredSize(new Dimension(900,600));
		this.m_frame.setMinimumSize(new Dimension(900,100));
		this.m_frame.setJMenuBar(null);
		m_panel = new UNSSalesWatchPanel(new MigLayout("fill, insets 0 0", "4%[fill]4%",""));
		m_panel.init();
		m_frame.getContentPane().add(m_panel);
		m_panel.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see org.compiere.apps.form.FormPanel#dispose()
	 */
	@Override
	public void dispose() 
	{
		if (m_panel != null)
			m_panel.dispose ();
		if (m_frame != null)
			m_frame.dispose();
		m_frame = null;
	}

}
