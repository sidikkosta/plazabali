/**
 * 
 */
package com.uns.pos;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.MediaTracker;
import javax.swing.border.TitledBorder;
import org.compiere.swing.CPanel;

/**
 * @author nurse
 *
 */
public class UNSAdsImagePanel extends CPanel {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1347434835333476462L;
	private Image m_image;
	/**
	 * @param layout
	 */
	public UNSAdsImagePanel(LayoutManager layout) 
	{
		super(layout);
	}
	
	public void init ()
	{
		setBorder(new TitledBorder("Finger"));
	}

	/**
	 * 
	 */
	public UNSAdsImagePanel() 
	{
		super ();
	}
	
	public void showImage (Image image)
	{
		m_image = image;
		MediaTracker mt = new MediaTracker(this);
		mt.addImage(m_image, 0);
		try 
		{
			mt.waitForID(0);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		repaint();
	}
	
	public void paint(Graphics g) 
	{
		super.paint(g);
		g.drawImage(m_image, 0, 0, this.getWidth(), this.getHeight(), null);
	}
	
	public Image getImage ()
	{
		return m_image;
	}
}
