/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import net.miginfocom.swing.MigLayout;
import org.compiere.apps.ADialog;
import org.compiere.apps.AEnv;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CLabel;
import org.compiere.swing.CTextField;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;
import org.jfree.ui.Align;
import com.unicore.model.MUNSCustomerInfo;

/**
 * @author nurse
 *
 */
public class UNSCustomerInfo extends JDialog implements KeyListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8359612788588689585L;
	private UNSPOSPanel m_parent = null;
	private CComboBox f_customerType = null;
	private CComboBox f_passengerType = null;
	private CComboBox f_vipType = null;
	private CTextField f_nik = null;
	private CComboBox f_airLine = null;
	private CTextField f_pasportNo = null;
	private CTextField f_Name = null;
	private CComboBox f_nationality = null;
	private CTextField f_boardingPas = null;
	private CTextField f_flightNo = null;
	private UNSCaptureImagePanel f_finger = null;
	private JButton b_ok = null;
	private JButton b_cancel = null;
	private MUNSCustomerInfo m_customer;
	private CLabel l_nik;
	private CLabel l_airLine;
	private CLabel l_passport;
	private CLabel l_boardingPass;
	private CLabel l_flightNo;
	private JPanel m_pLNIK;
	private JPanel m_pFNIK;
	private JPanel m_pLAirLine;
	private JPanel m_pFAirLine;
	private JPanel m_pLPassport;
	private JPanel m_pFPassport;
	private JPanel m_pLBoardingPass;
	private JPanel m_pFBoardingPass;
	private JPanel m_pLFlightNo;
	private JPanel m_pFFlightNo;
	private JPanel m_pFFinger;
	private CTextField f_gate;
	private JPanel m_panel;

	/**
	 * @param posPanel
	 */
	public UNSCustomerInfo(UNSPOSPanel posPanel, boolean modal, 
			MUNSCustomerInfo customer) 
	{
		super(AEnv.getFrame(posPanel), modal);
		m_parent = posPanel;
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setMinimumSize(new Dimension(400, 400));
        setPreferredSize(new Dimension(400, 400));
        setResizable(true);
        m_customer = customer;
		init();
	}

	/* (non-Javadoc)
	 * @see com.uns.pos.UNSPOSSubPanel#init()
	 */
	protected void init() 
	{
		m_panel = new JPanel(new MigLayout("fill", "[][fill]",""));
		m_panel.setBackground(new Color(243, 250, 236));
		m_panel.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
		Font font = new Font("Calibri", Font.BOLD, 12);
		
		m_pLNIK = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pLNIK.setBackground(new Color(243, 250, 236));
		m_pFNIK = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pFNIK.setBackground(new Color(243, 250, 236));
		m_pLAirLine = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pLAirLine.setBackground(new Color(243, 250, 236));
		m_pFAirLine = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pFAirLine.setBackground(new Color(243, 250, 236));
		m_pLBoardingPass = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pLBoardingPass.setBackground(new Color(243, 250, 236));
		m_pFBoardingPass = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pFBoardingPass.setBackground(new Color(243, 250, 236));
		m_pFFinger = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pFFinger.setBackground(new Color(243, 250, 236));
		m_pLFlightNo = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pLFlightNo.setBackground(new Color(243, 250, 236));
		m_pFFlightNo = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pFFlightNo.setBackground(new Color(243, 250, 236));
		m_pLPassport = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pLPassport.setBackground(new Color(243, 250, 236));
		m_pFPassport = new JPanel(new MigLayout("insets 0", "[100%]", ""));
		m_pFPassport.setBackground(new Color(243, 250, 236));
		
		f_boardingPas = new CTextField();
		f_boardingPas.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				String bPass = f_boardingPas.getText();
				if (bPass.length() < 43)
				{
					f_flightNo.requestFocus();
					return;
				}
				
				String flightNo = bPass.substring(36, 44);
				flightNo = flightNo.toUpperCase();
				flightNo = flightNo.replace(" ", "");
				//flightNo = flightNo.substring(0, 6);
				f_flightNo.setText(flightNo);
				if (!Util.isEmpty(flightNo, true))
				{
					if (f_gate.getParent() != null)
						f_gate.requestFocus();
					else if (f_finger.getParent() != null)
						onFingerPanelClicked();
					else onOK();
				}
			}
		});
		
		f_customerType = new CComboBox(getCustomerTypes());
		f_customerType.getEditor().getEditorComponent().addKeyListener(this);
		f_customerType.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	enableFields();
		    }
		});
		
		
		f_pasportNo = new CTextField();
		f_pasportNo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				String value = f_pasportNo.getText();
				String[] split = value.split("#");
				for (int i=0; split != null && i<split.length 
						&& split.length == 3; i++)
				{
					if (i==0)
						f_Name.setText(split[i]);
					else if (i==1)
						f_pasportNo.setText(split[i]);
					else if (i==2) 
					{
						String countryCode = split[2].replace("$", "");
						for (int x=1; x<f_nationality.getItemCount(); x++)
						{
							if ((((KeyNamePair)f_nationality.getItemAt(x)).getName().substring(0, 3)).equals(countryCode))
							{
								f_nationality.setSelectedIndex(x);
								break;
							}
						}
					}
				}
				if (split.length == 1)
					f_Name.requestFocus();
				else if (!Util.isEmpty(f_Name.getText(), true) && f_nationality.getSelectedIndex() > 0)
					{
					String customerType = ((ValueNamePair)f_customerType.getSelectedItem()).getID();
					if (MUNSCustomerInfo.CUSTOMERTYPE_AirCrew.equals(customerType))
						f_flightNo.requestFocus();	
					else
						f_boardingPas.requestFocus();
					}
			}
		});
		
		f_Name = new CTextField();
		f_Name.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				String name = f_Name.getText();
				if (name == null)
					name = "";
				name = name.toUpperCase();
				f_Name.setText(name);
				f_nationality.requestFocus();
			}
		});
		f_nationality = new CComboBox(getCountries());
		f_nationality.getEditor().getEditorComponent().addKeyListener(this);
		f_finger = new UNSCaptureImagePanel();
		f_finger.init();
		f_finger.setPreferredSize(new Dimension(100, 100));
		f_finger.setMinimumSize(new Dimension(100, 100));
		f_finger.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent me) 
			{ 
				onFingerPanelClicked();
			} 
		});
		f_passengerType = new CComboBox(getPassengerTypes());
		f_passengerType.getEditor().getEditorComponent().addKeyListener(this);
		f_airLine = new CComboBox(loadAirLines());
		f_airLine.getEditor().getEditorComponent().addKeyListener(this);
		f_flightNo = new CTextField();
		f_flightNo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String flight = f_flightNo.getText();
				if (flight == null)
					flight = "";
				flight = flight.toUpperCase();
				f_flightNo.setText(flight);
				if (f_gate.getParent() != null)
					f_gate.requestFocus();
				else if (f_finger.getParent() != null)
					onFingerPanelClicked();
				else
					onOK();
			}
		});
		f_gate = new CTextField("Gate");
		f_gate.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (f_finger.getParent() != null)
				{
					onFingerPanelClicked();
				}
				else
					onOK();
			}
		});
		f_nik = new CTextField();
		f_vipType = new CComboBox(getVIPTypes());
		f_vipType.getEditor().getEditorComponent().addKeyListener(this);

		f_boardingPas.setFont(font);
		f_customerType.setFont(font);
		f_pasportNo.setFont(font);
		f_Name.setFont(font);
		f_nationality.setFont(font);
		f_passengerType.setFont(font);
		f_airLine.setFont(font);
		f_flightNo.setFont(font);
		f_gate.setFont(font);
		f_nik.setFont(font);
		f_vipType.setFont(font);

		CLabel lCustomerType = new CLabel("CUSTOMER TYPE");
		CLabel lPassengerType = new CLabel("PASSENGER TYPE");
		CLabel lVPType = new CLabel("VIP TYPE");
		l_nik = new CLabel("NIK");
		l_airLine = new CLabel("AIR LINE");
		l_passport= new CLabel("PASSPORT NO");
		CLabel lName = new CLabel("NAME");
		CLabel lNationality = new CLabel("NATIONALITY", SwingConstants.LEFT);
		l_boardingPass = new CLabel("BOARDING PASS");
		l_flightNo = new CLabel("FLIGHT NO");
		lNationality.setHorizontalAlignment(Align.LEFT);
		l_boardingPass.setFont(font);
		lCustomerType.setFont(font);
		l_passport.setFont(font);
		lName.setFont(font);
		lNationality.setFont(font);
		lPassengerType.setFont(font);
		l_airLine.setFont(font);
		l_flightNo.setFont(font);
		l_nik.setFont(font);
		lVPType.setFont(font);
				
		m_panel.add(lCustomerType, "");
		m_panel.add(f_customerType, "pushx, wrap");
		m_panel.add(lPassengerType, "");
		m_panel.add(f_passengerType, "pushx, wrap");
		m_panel.add(lVPType, "");
		m_panel.add(f_vipType, "pushx, wrap");
		m_panel.add(m_pLNIK, "");
		m_panel.add(m_pFNIK, "wrap");
		m_panel.add(m_pLAirLine, "");
		m_panel.add(m_pFAirLine, "wrap");
		m_panel.add(m_pLPassport, "");
		m_panel.add(m_pFPassport, "wrap");
		m_panel.add(lName, "");
		m_panel.add(f_Name, "pushx, wrap");
		m_panel.add(lNationality, "");
		m_panel.add(f_nationality, "pushx, wrap");
		m_panel.add(m_pLBoardingPass, "");
		m_panel.add(m_pFBoardingPass, "wrap");
		m_panel.add(m_pLFlightNo, "");
		m_panel.add(m_pFFlightNo, "wrap");
		
		m_panel.add(m_pFFinger, "wrap, pushy,spanx,flowx, growx, pushx, top");
		
		m_panel.add(new CLabel(" "), " ");
		b_ok = new JButton("OK");
		b_cancel = new JButton("Cancel");
		m_panel.add(b_ok, ", right, split 2");
		m_panel.add(b_cancel, "right, wrap");
		
		b_cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				m_parent.checkSession();
				if (m_customer == null)
					m_parent.newTransaction();
				dispose();
			}
		});
		
		b_ok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				onOK();
			}
		});
		
		f_gate.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				if (Util.isEmpty(f_gate.getText(), true))
					f_gate.setText("Gate");
			}
			
			@Override
			public void focusGained(FocusEvent arg0) 
			{
				if ("Gate".equals(f_gate.getText()))
					f_gate.setText("");
			}
		});
		getContentPane().add(m_panel);
		pack();
		
		initValue ();
		enableFields();
		
		if (f_customerType.getSelectedIndex() == -1)
			f_customerType.requestFocus();
		else if (f_passengerType.getSelectedIndex() == -1)
			f_passengerType.requestFocus();
		else if (f_vipType.getSelectedIndex() == -1)
			f_vipType.requestFocus();
		else if (f_nik.getParent() != null)
			f_nik.requestFocus();
		else 
			f_pasportNo.requestFocus();
		
		f_nik.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (f_airLine.getParent() != null)
					f_airLine.requestFocus();
				else
					f_pasportNo.requestFocus();
			}
		});
	}
	
	private void onFingerPanelClicked ()
	{
		UNSFingerCapture finger = new UNSFingerCapture(UNSCustomerInfo.this, true);
		finger.init();
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - finger.getWidth()) / 2;
		int y = (screenSize.height - finger.getHeight()) / 2;
		finger.setLocation(x, y);
		finger.setVisible(true); 
	}
	
	private void initValue ()
	{
		if (m_customer != null)
		{
			for (int i=0; i<f_airLine.getItemCount(); i++)
			{
				KeyNamePair pair = (KeyNamePair) f_airLine.getItemAt(i);
				if (pair.getKey() == m_customer.getUNS_AirLine_ID())
				{
					f_airLine.setSelectedIndex(i);
					break;
				}
			}
			f_boardingPas.setText(m_customer.getBoardingPass());
			for (int i=0; i<f_customerType.getItemCount(); i++)
			{
				ValueNamePair pair = (ValueNamePair) f_customerType.getItemAt(i);
				if (pair.getValue().equals(m_customer.getCustomerType()))
				{
					f_customerType.setSelectedIndex(i);
					break;
				}
			}
			if (m_customer.getFinger() != null)
			{
				InputStream in = new ByteArrayInputStream(m_customer.getFinger());
				try {
					BufferedImage img = ImageIO.read(in);
					f_finger.showImage(img);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			f_flightNo.setText(m_customer.getFlightNo());
			f_Name.setText(m_customer.getName());
			for (int i=0; i<f_nationality.getItemCount(); i++)
			{
				KeyNamePair pair = (KeyNamePair) f_nationality.getItemAt(i);
				if (pair.getKey() == m_customer.getC_Country_ID())
				{
					f_nationality.setSelectedIndex(i);
					break;
				}
			}
			f_nik.setText(m_customer.getNIK());
			f_pasportNo.setText(m_customer.getPassportNo());
			for (int i=0; i<f_passengerType.getItemCount(); i++)
			{
				ValueNamePair pair = (ValueNamePair) f_passengerType.getItemAt(i);
				if (pair.getValue().equals(m_customer.getPassengerType()))
				{
					f_passengerType.setSelectedIndex(i);
					break;
				}
			}
			for (int i=0; i<f_vipType.getItemCount(); i++)
			{
				ValueNamePair pair = (ValueNamePair) f_vipType.getItemAt(i);
				if (pair.getValue().equals(m_customer.getVIPType()))
				{
					f_vipType.setSelectedIndex(i);
					break;
				}
			}
			
			f_gate.setText(m_customer.getGateNo());
		}
		else
		{
			f_vipType.setSelectedIndex(1);
			for (int i=0; i<f_passengerType.getItemCount(); i++)
			{
				ValueNamePair pair = (ValueNamePair) f_passengerType.getItemAt(i);
				if (pair.getValue().equals(m_parent.m_storeConfig.getDefaultPassengerType()))
				{
					f_passengerType.setSelectedIndex(i);
					break;
				}
			}
			f_customerType.setSelectedIndex(2);
		}
	}

	public void onOK ()
	{
		if (m_customer == null)
		{
			m_customer = new MUNSCustomerInfo(m_parent.getCtx(), 0, m_parent.get_TrxName());
			m_customer.setAD_Org_ID(m_parent.m_session.getAD_Org_ID());
		}
		if (f_airLine.getSelectedIndex() != -1)
			m_customer.setUNS_AirLine_ID(((KeyNamePair)f_airLine.getSelectedItem()).getKey());
		else
			m_customer.setUNS_AirLine_ID(-1);
		m_customer.setC_Country_ID(((KeyNamePair)f_nationality.getSelectedItem()).getKey());
		m_customer.setCustomerType(((ValueNamePair)f_customerType.getSelectedItem()).getID());
		m_customer.setFlightNo(f_flightNo.getText());
		m_customer.setName(f_Name.getText());
		m_customer.setNIK(f_nik.getText());
		m_customer.setPassengerType(((ValueNamePair)f_passengerType.getSelectedItem()).getID());
		m_customer.setPassportNo(f_pasportNo.getText());
		m_customer.setVIPType(((ValueNamePair)f_vipType.getSelectedItem()).getID());
		m_customer.setBoardingPass(f_boardingPas.getText());
		m_customer.setGateNo(f_gate.getText());
		if ((MUNSCustomerInfo.PASSENGERTYPE_Arrival.equals(m_customer.getPassengerType())
				|| MUNSCustomerInfo.CUSTOMERTYPE_AirCrew.equals(m_customer.getCustomerType()) 
				|| MUNSCustomerInfo.CUSTOMERTYPE_Employee.equals(m_customer.getCustomerType())) 
				&& f_finger.getImage() != null)
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			byte[] bytes = null;
			try {
				ImageIO.write(f_finger.getImage(), "jpg", stream);
				bytes = stream.toByteArray();
			} catch (IOException e) {
				e.printStackTrace();
			}
			m_customer.setFinger(bytes);
		}
		
		if (!m_customer.save())
		{
			try 
			{
				DB.rollback(true, m_customer.get_TrxName());
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			ADialog.error(m_parent.getWindowNo(), this, CLogger.retrieveErrorString("Failed when try to save customer"));
			m_customer = null;
			return;
		}
		
		try 
		{
			DB.commit(true, m_customer.get_TrxName());
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		m_parent.m_infoPanel.setCustomer(m_customer);
		dispose();
	}
	
	
	private Vector<KeyNamePair> getCountries ()
	{
		Vector<KeyNamePair> retVal = new Vector<>();
		retVal.add(new KeyNamePair(-1, ""));
		String sql = "SELECT C_Country_ID, CONCAT (COALESCE(CountryCode3, 'UNK'),'-',Name) AS "
				+ " Country FROM C_Country WHERE IsActive = ? ORDER BY Country";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_parent.get_TrxName());
			st.setString(1, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				retVal.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
		
	}
	
	private Vector<ValueNamePair> getVIPTypes ()
	{
		Vector<ValueNamePair> retVal = new Vector<>();
		String sql = "SELECT Value, Name FROM AD_Ref_List WHERE AD_Reference_ID = ("
				+ " SELECT AD_Reference_Value_ID FROM AD_Column WHERE ColumnName = ? "
				+ " AND AD_Table_ID = ?) AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_parent.get_TrxName());
			st.setString(1, MUNSCustomerInfo.COLUMNNAME_VIPType);
			st.setInt(2, MUNSCustomerInfo.Table_ID);
			st.setString(3, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				retVal.add(new ValueNamePair(rs.getString(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
	}
	
	private Vector<ValueNamePair> getCustomerTypes ()
	{
		Vector<ValueNamePair> retVal = new Vector<>();
		String sql = "SELECT Value, Name FROM AD_Ref_List WHERE AD_Reference_ID = ("
				+ " SELECT AD_Reference_Value_ID FROM AD_Column WHERE ColumnName = ? "
				+ " AND AD_Table_ID = ?) AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_parent.get_TrxName());
			st.setString(1, MUNSCustomerInfo.COLUMNNAME_CustomerType);
			st.setInt(2, MUNSCustomerInfo.Table_ID);
			st.setString(3, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				retVal.add(new ValueNamePair(rs.getString(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
	}
	
	private Vector<ValueNamePair> getPassengerTypes ()
	{
		Vector<ValueNamePair> retVal = new Vector<>();
		String sql = "SELECT Value, Name FROM AD_Ref_List WHERE AD_Reference_ID = ("
				+ " SELECT AD_Reference_Value_ID FROM AD_Column WHERE ColumnName = ? "
				+ " AND AD_Table_ID = ?) AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_parent.get_TrxName());
			st.setString(1, MUNSCustomerInfo.COLUMNNAME_PassengerType);
			st.setInt(2, MUNSCustomerInfo.Table_ID);
			st.setString(3, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				retVal.add(new ValueNamePair(rs.getString(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
	}
	
	public void setFinger (BufferedImage image)
	{
		f_finger.showImage(image);
	}
	
	private Vector<KeyNamePair> loadAirLines ()
	{
		Vector<KeyNamePair> retVal = new Vector<>();
		String sql = "SELECT UNS_AirLine_ID, Name FROM UNS_AirLine WHERE IsActive = ? ORDER BY Name";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_parent.get_TrxName());
			st.setString(1, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				retVal.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		return retVal;
	}
	
	private void enableFields ()
	{
		m_pLAirLine.remove(l_airLine);
		m_pFAirLine.remove(f_airLine);
		m_pLNIK.remove(l_nik);
		m_pFNIK.remove(f_nik);
		m_pLFlightNo.remove(l_flightNo);
		m_pFFlightNo.remove(f_flightNo);
		m_pLBoardingPass.remove(l_boardingPass);
		m_pFBoardingPass.remove(f_boardingPas);
		m_pLPassport.remove(l_passport);
		m_pFPassport.remove(f_pasportNo);
		m_pFFinger.remove(f_finger);
		m_pFFlightNo.remove(f_gate);
		
		String customerType = ((ValueNamePair)f_customerType.getSelectedItem()).getID();
		if (MUNSCustomerInfo.CUSTOMERTYPE_AirCrew.equals(customerType))
		{
			m_pLAirLine.add(l_airLine);
			m_pFAirLine.add(f_airLine, "growx, flowx,spanx");
			m_pLNIK.add(l_nik);
			m_pFNIK.add(f_nik, "growx, flowx,spanx");
			m_pLPassport.add(l_passport);
			m_pFPassport.add(f_pasportNo, "growx, flowx,spanx");
			m_pLFlightNo.add(l_flightNo);
			m_pFFlightNo.add(f_flightNo, "growx, flowx,split 2");
		}
		else if (MUNSCustomerInfo.CUSTOMERTYPE_Employee.equals(customerType))
		{
			m_pLNIK.add(l_nik);
			m_pFNIK.add(f_nik, "growx, flowx,spanx");
			m_pLBoardingPass.add(l_boardingPass, "");
			m_pFBoardingPass.add(f_boardingPas, "growx, flowx,spanx");
			m_pLFlightNo.add(l_flightNo);
			m_pFFlightNo.add(f_flightNo, "growx, flowx,split 2");
			m_pLPassport.add(l_passport);
			m_pFPassport.add(f_pasportNo, "growx, flowx,spanx");
		}
		else if (MUNSCustomerInfo.CUSTOMERTYPE_General.equals(customerType))
		{
			m_pLBoardingPass.add(l_boardingPass, "");
			m_pFBoardingPass.add(f_boardingPas, "growx, flowx,spanx");
			m_pLPassport.add(l_passport);
			m_pFPassport.add(f_pasportNo, "growx, flowx,spanx");
			m_pLFlightNo.add(l_flightNo);
			m_pFFlightNo.add(f_flightNo, "growx, flowx,split 2");
		}
		else
		{
			f_flightNo.setText("");
			f_pasportNo.setText("");
			f_boardingPas.setText("");
			f_airLine.setSelectedIndex(-1);
		}
		
		String passengerType = ((ValueNamePair)f_passengerType.getSelectedItem()).getID();
		if (MUNSCustomerInfo.PASSENGERTYPE_Arrival.equals(passengerType))
		{
			m_pFFinger.add(f_finger, ", right,spanx");
		}
		else if (MUNSCustomerInfo.PASSENGERTYPE_Departure.equals(passengerType))
		{
			if (MUNSCustomerInfo.CUSTOMERTYPE_Employee.equals(customerType)
					|| MUNSCustomerInfo.CUSTOMERTYPE_AirCrew.equals(customerType))
			{
				m_pFFinger.add(f_finger, ", right,spanx");
			}
			else
			{
				f_finger.showImage((BufferedImage)null);
			}
			m_pFFlightNo.add(f_gate);
		}
		else
		{
			f_finger.showImage((BufferedImage)null);
		}
		
		if (MUNSCustomerInfo.PASSENGERTYPE_In_Transit.equals(passengerType))
			m_pFFlightNo.add(f_gate);
		
		String vipType = ((ValueNamePair)f_vipType.getSelectedItem()).getID();
		if (MUNSCustomerInfo.VIPTYPE_Membership.equals(vipType))
		{
			if (MUNSCustomerInfo.CUSTOMERTYPE_General.equals(customerType))
			{
				m_pLNIK.add(l_nik);
				m_pFNIK.add(f_nik, "growx, flowx,spanx");
			}
		}
		m_panel.updateUI();
	}

	@Override
	public void keyPressed(KeyEvent arg0) 
	{
		final int keyCode = arg0.getKeyCode();
		if (KeyEvent.VK_ENTER != keyCode)
			return;
		
		final Object instance = ((Component)arg0.getSource()).getParent();
		if (instance instanceof JComboBox == false)
			return;

		if (instance.equals(f_airLine))
		{
			f_pasportNo.requestFocus();
		}
		else if (instance.equals(f_customerType))
		{
			enableFields();
			f_passengerType.requestFocus();
		}
		else if (instance.equals(f_vipType))
		{
			enableFields();
			if (f_nik.getParent() != null)
				f_nik.requestFocus();
			else
				f_pasportNo.requestFocus();
		}
		else if (instance.equals(f_passengerType))
		{
			enableFields();
			f_vipType.requestFocus();
		}
		else if (instance.equals(f_nationality))
		{
			if (f_boardingPas.getParent() != null)
				f_boardingPas.requestFocus();
			else
				f_flightNo.requestFocus();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) 
	{
//		final int keyCode = arg0.getKeyCode();
//		if (KeyEvent.VK_ENTER != keyCode)
//			return;
//		
//		final Object instance = ((Component)arg0.getSource()).getParent();
//		if (instance instanceof JComboBox == false)
//			return;
//		
//		EventQueue.invokeLater(new Runnable() {
//			
//			@Override
//			public void run() 
//			{
//				if (instance.equals(f_airLine))
//				{
//					f_pasportNo.requestFocus();
//				}
//				else if (instance.equals(f_customerType))
//				{
//					enableFields();
//					f_passengerType.requestFocus();
//				}
//				else if (instance.equals(f_vipType))
//				{
//					enableFields();
//					if (f_nik.getParent() != null)
//						f_nik.requestFocus();
//					else
//						f_pasportNo.requestFocus();
//				}
//				else if (instance.equals(f_passengerType))
//				{
//					enableFields();
//					f_vipType.requestFocus();
//				}
//				else if (instance.equals(f_nationality))
//				{
//					f_boardingPas.requestFocus();
//				}
//			}
//		});
	}

	@Override
	public void keyTyped(KeyEvent arg0) 
	{
	}
}
