package com.uns.pos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import org.adempiere.plaf.AdempierePLAF;
import org.compiere.minigrid.MiniTable;

public class UNSPOSTable extends MiniTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3369455122541012644L;
	public static final Color DEFAULT_EVEN_COLOR = new Color(239, 243, 234);
	public static final Color DEFAULT_ODD_COLOR = AdempierePLAF.getFieldBackground_Selected();
	public static final Color DEFAULT_SELECTED_COLOR = new Color(222, 231, 209); 
	private Color m_oddColor = DEFAULT_ODD_COLOR;
	private Color m_evenColor = DEFAULT_EVEN_COLOR;
	private Color m_selectedColor = DEFAULT_SELECTED_COLOR;
	
	public void setOddColor (Color color)
	{
		m_oddColor = color;
	}
	
	public void setEvenColor (Color color)
	{
		m_evenColor = color;
	}
	
	public void setSelectedColor (Color color)
	{
		m_selectedColor = color;
	}


	public UNSPOSTable() {

		super();
		setRowSelectionAllowed(true);
		setColumnSelectionAllowed(false);
		setMultiSelection(false);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setRowHeight(30);
		setFillsViewportHeight(true);
		setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
	}
	
	public void growScrollbars() 
	{
		Container p = getParent();
		if (p instanceof JViewport) 
		{
			Container gp = p.getParent();
			if (gp instanceof JScrollPane) 
			{
				JScrollPane scrollPane = (JScrollPane) gp;             
				scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(30,0));
				scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0,30));
			}
		}
	}
	
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int rowIndex,
			int vColIndex) 
	{
		Component c = super.prepareRenderer(renderer, rowIndex, vColIndex);
		if (c==null) return c;
		if (isCellSelected(rowIndex, vColIndex)) c.setBackground(m_selectedColor); 
		else if (rowIndex % 2 == 0) c.setBackground(m_evenColor);
		else c.setBackground(m_oddColor);
		
		return c; 
	}
}
