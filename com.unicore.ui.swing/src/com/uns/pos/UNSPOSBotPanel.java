/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

import org.adempiere.plaf.AdempierePLAF;
import org.compiere.apps.ADialog;
import org.compiere.process.DocAction;
import org.compiere.swing.CButton;
import org.compiere.swing.CScrollPane;
import org.compiere.swing.CTextArea;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import net.miginfocom.swing.MigLayout;
import com.unicore.model.MUNSPOSTrxLine;
import com.unicore.model.UNSPOSTrxDiscountModel;
import com.unicore.model.process.CalculateDiscount;
import com.unicore.ui.form.UNTPanel;

/**
 * @author nurse
 *
 */
public class UNSPOSBotPanel extends UNSPOSSubPanel implements ActionListener 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4112870107661812944L;
	private UNTPanel m_panelInfo = null;
	private JPanel m_panelButton = null;
	protected CButton[] m_buttons = new CButton[16];
	private boolean[] m_clickables = new boolean[]{
			true, true, true, true, true, true, true, 
			true, true, true, true, true, true, true, true, true};
	private UNSPOSTable m_refundTable;
	private CScrollPane m_scroll;
	private CTextArea f_discInfo;
	
	public void setDiscountInfo (String info)
	{
		f_discInfo.setText(info);
	}
	
	/**
	 * @param posPanel
	 */
	public UNSPOSBotPanel(UNSPOSPanel posPanel) 
	{
		super(posPanel);
	}
	
	private final KeyStroke[] m_keyStroke = new KeyStroke[] {
		KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0),	
		KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0),
		KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_MASK),
		KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.ALT_MASK),
		KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.ALT_MASK),
		KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.ALT_MASK),
	};
	
	private final String[] m_actionCommands = new String[] {
			Integer.toString(KeyEvent.VK_F1),
			Integer.toString(KeyEvent.VK_F2),
			Integer.toString(KeyEvent.VK_F3),
			Integer.toString(KeyEvent.VK_F4),
			Integer.toString(KeyEvent.VK_F5),
			Integer.toString(KeyEvent.VK_F6),
			Integer.toString(KeyEvent.VK_F7),
			Integer.toString(KeyEvent.VK_F8),
			Integer.toString(KeyEvent.VK_F9),
			Integer.toString(KeyEvent.VK_F10),
			Integer.toString(KeyEvent.VK_F11),
			Integer.toString(KeyEvent.VK_F12),
			"Deposit",
			"Exchange",
			"CurrencyRate",
			"TKeyboard"
	};
	
	private final ImageIcon[] m_icons = new ImageIcon[]{
			new ImageIcon(getClass().getResource("/com/unicore/images/F1.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F2.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F3.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F4.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F5.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F6.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F7.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F8.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F9.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F10.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F11.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/F12.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/AddCapital.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/Exchange.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/Close.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/TKeyboard.png"))
	}; 

	/* (non-Javadoc)
	 * @see com.uns.pos.UNSPOSSubPanel#init()
	 */
	@Override
	protected void init() 
	{
		setLayout(new MigLayout("fill, insets 0", "[][]", "fill"));
		m_panelInfo = new UNTPanel(new MigLayout("fill", "[fill]", ""));
		m_panelInfo.setBackground(new Color(232,59,123));
		m_panelInfo.setStroke(4, new Color (0,110,0));
		JTextArea area = new JTextArea();
		area.setFont(new Font("sansserif",Font.BOLD, 16));
		f_discInfo = new CTextArea(area);
		f_discInfo.setFocusable(false);
		f_discInfo.setEditable(false);
		f_discInfo.setLineWrap(true);
		f_discInfo.setText(null);
		f_discInfo.setBackground(Color.WHITE);
		f_discInfo.setForeground(Color.RED);
		m_panelInfo.add(f_discInfo, "h 200px, growx, growy, flowx, flowy,pushy,pushx, wrap");
		
		m_panelButton = new JPanel(new MigLayout("insets 0", "[][][][]", "0px![]0px![]0px![]0px![]0px!"));
		
		for (int i=0; i<m_actionCommands.length; i++)
		{
			String style = "pushy,growx, growy, flowx, flowy, pushx";
			if ((i+1) % 4 == 0)
				style += ", wrap";
			Image img = m_icons[i].getImage().getScaledInstance(60, 30, Image.SCALE_SMOOTH);
			CButton button = new CButton(new ImageIcon(img));
			button.setActionCommand(m_actionCommands[i]);
			button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(m_keyStroke[i], m_actionCommands[i]);
			button.getActionMap().put(m_actionCommands[i], new AbstractAction() {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = -6333879882950798774L;

				@Override
				public void actionPerformed(ActionEvent arg0) {
					UNSPOSBotPanel.this.actionPerformed(arg0);
				}
			});
			button.setIcon(new ImageIcon(img));
			button.setBorderPainted(false);
			button.setMargin(new Insets(0, 0, 0, 0));
			button.addActionListener(this);
			m_buttons[i] = button;
			m_panelButton.add(button, style);
		}

		m_refundTable = new UNSPOSTable();
		m_refundTable.setEnabled(false);
		m_refundTable.setEvenColor(new Color(237, 234, 240));
		m_refundTable.setOddColor(new Color(216, 211, 224));
		DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new String[]{"NO", "SKU", "PRPDUCT", "QTY", "PRICE", "SVCCHR", "TAX", "TOTAL"});
		m_refundTable.setModel(model);
		m_refundTable.setFillsViewportHeight(true);
		m_refundTable.setFocusable(false);
		m_refundTable.setRowHeight(30);
		m_refundTable.getTableHeader().setForeground(Color.WHITE);
		m_refundTable.growScrollbars();
		m_scroll = new CScrollPane(m_refundTable);
		m_refundTable.getTableHeader().setPreferredSize(new Dimension(m_scroll.getWidth(), 30));
		m_refundTable.getTableHeader().setFont(AdempierePLAF.getFont_Field().deriveFont(12f));
		m_refundTable.getTableHeader().setBackground(new Color(128, 100, 162));
		loadSalesDisplay();
		enableComponent(null);
	}

	@Override
	public void actionPerformed (ActionEvent e)
	{
		m_posPanel.checkSession();
		m_posPanel.f_curLine.m_table.stopEditor(false);
		String command = ((CButton) e.getSource()).getActionCommand();
		if (m_actionCommands[0].equals(command))
		{
//			try
//			{
//				if (!this.m_posPanel.m_posTrx.isProcessed())
//				{
//					UNSPOSTrxDiscountModel model = new UNSPOSTrxDiscountModel(this.m_posPanel.m_posTrx);
//					CalculateDiscount calculator = new CalculateDiscount(model, false, 0);
//					calculator.run();
//					this.m_posPanel.m_posTrx.load(get_TrxName());
//				}
//			}
//			catch (Exception ex)
//			{
//				ADialog.error(this.m_posPanel.getWindowNo(), this, ex.getMessage());
//				return;
//			}
			
			if (!m_posPanel.isOnRefund())
			{
				String sql = "SELECT COUNT(*) FROM UNS_DiscountTrx WHERE IsNeedRecalculate = ? AND (UNS_POSTrx_ID = ? OR "
						+ " UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine "
						+ "WHERE UNS_POSTrx_ID = ?))";
				int counter = DB.getSQLValue(get_TrxName(), sql, "Y", m_posPanel.m_posTrx.getUNS_POSTrx_ID(),
						m_posPanel.m_posTrx.getUNS_POSTrx_ID());
				if (counter > 0)
				{
					ADialog.warn(m_posPanel.getWindowNo(), UNSPOSBotPanel.this, "Please run calculate discount before do next process");
					return;
				}
				
				sql = "SELECT COUNT (*) FROM UNS_DiscountTrx WHERE UNS_POSTrx_ID = ? OR UNS_POSTrxLine_ID IN ("
						+" SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?)";
				counter = DB.getSQLValue(get_TrxName(), sql, m_posPanel.m_posTrx.getUNS_POSTrx_ID(),
						m_posPanel.m_posTrx.getUNS_POSTrx_ID());
				if (counter == 0)
				{
					if (!ADialog.ask(m_posPanel.getWindowNo(), UNSPOSBotPanel.this, 
							"Are you sure you will pay without defining a discount?"))
						return;
				}
			}
			
			m_posPanel.doPay();
		}
		else if (m_actionCommands[1].equals(command))
		{
			m_posPanel.newTransaction();
		}
		else if (m_actionCommands[2].equals(command))
		{
			try 
			{
    			Runtime.getRuntime().exec("calc");
    		}
			catch (IOException ex) 
			{
    			ex.printStackTrace();
    		}			
		}
		else if (m_actionCommands[3].equals(command))
		{
			UNSPOSQueryTicket ticket = new UNSPOSQueryTicket(m_posPanel);
			ticket.setVisible(true);
			return;
		}
		else if (m_actionCommands[4].equals(command))
		{
			UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
			confirm.setVisible(true);
			if (!confirm.isApproved())
			{
				EventQueue.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						m_posPanel.f_curLine.m_table.requestFocus();
						for (int i=0; i<m_posPanel.f_curLine.m_table.getRowCount(); i++)
						{
							if (m_posPanel.f_curLine.m_table.getValueAt(i, 0) == null)
							{
								m_posPanel.f_curLine.m_table.getSelectionModel().setSelectionInterval(i, i);
								m_posPanel.f_curLine.m_table.editCellAt(i, 2);
								break;
							}
						}
					}
				});
				return;
			}
			m_posPanel.printTransaction(true);
		}
		else if (m_actionCommands[5].equals(command))
		{
			m_posPanel.onRefund();
			UNSPOSQueryTicket ticket = new UNSPOSQueryTicket(m_posPanel);
			ticket.setVisible(true);
		}
		else if (m_actionCommands[6].equals(command))
		{
			if (m_posPanel.m_posTrx == null)
				return;
			UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
			confirm.setVisible(true);
			if (!confirm.isApproved())
			{
				EventQueue.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						m_posPanel.f_curLine.m_table.requestFocus();
						for (int i=0; i<m_posPanel.f_curLine.m_table.getRowCount(); i++)
						{
							if (m_posPanel.f_curLine.m_table.getValueAt(i, 0) == null)
							{
								m_posPanel.f_curLine.m_table.getSelectionModel().setSelectionInterval(i, i);
								m_posPanel.f_curLine.m_table.editCellAt(i, 2);
								break;
							}
						}
					}
				});
				return;
			}
			if (m_posPanel.f_curLine.m_table.getSelectedRow() != -1)
			{
				int posTrxLineID = m_posPanel.f_curLine.m_table.getSelectedRowKey();
				if (!m_posPanel.isOnRefund())
				{
					MUNSPOSTrxLine line = new MUNSPOSTrxLine(getCtx(), posTrxLineID, get_TrxName());
					String _sql = "UPDATE UNS_DiscountTrx SET IsNeedRecalculate = 'Y' WHERE (UNS_POSTrx_ID = ? OR UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?)) AND Name NOT LIKE 'Special%'";
					int ok = DB.executeUpdate(_sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrx_ID()}, false, get_TrxName());
					if (ok == -1)
					{
						ADialog.error(this.m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not update POS Trx"));
						return ;
					}
					String sql = "SELECT COUNT(*) FROM UNS_DiscountTrx WHERE UNS_POSTrxLine_ID = ? OR UNS_POSTrx_ID = ?";
					int exists = DB.getSQLValue(get_TrxName(), sql, line.getUNS_POSTrxLine_ID(),line.getUNS_POSTrx_ID());
					if (exists > 0)
					{
						if (ADialog.ask(m_posPanel.getWindowNo(), UNSPOSBotPanel.this, "This action will delete the discount that was previously defined, Continue this process?"))
						{
							sql = "DELETE FROM UNS_DiscountTrx WHERE UNS_POStrx_ID = ? OR UNS_POStrxLine_ID = ?";
							int executeOK = DB.executeUpdate(sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrxLine_ID()}, 
									false, get_TrxName());
							if (executeOK == -1)
							{
								return;
							}
							line.setDiscountAmt(Env.ZERO);
							line.getParent().setDiscountAmt(Env.ZERO);
							if (!line.getParent().save())
							{
								ADialog.error(this.m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not update POS Trx"));
								return ;
							}
						}
						else
						{
							return;
						}
					}
				}
				
				if (!m_posPanel.m_posTrx.deleteLine(posTrxLineID))
				{
					String error = CLogger.retrieveErrorString("Unknown Error");
					try 
					{
						DB.rollback(true, get_TrxName());
					} 
					catch (IllegalStateException e1) 
					{
						e1.printStackTrace();
					} 
					catch (SQLException e1) 
					{
						e1.printStackTrace();
					}
					ADialog.error(this.m_posPanel.getWindowNo(), this, error);
					return;
				}
				try 
				{
					DB.commit(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				m_posPanel.updateInfo();
			}
		}
		else if (m_actionCommands[7].equals(command))
		{
			if (m_posPanel.m_posTrx == null)
				return;
			Cursor tmp = getCursor();
			setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			UNSPOSTrxDiscountModel model = new UNSPOSTrxDiscountModel(m_posPanel.m_posTrx);
			model.setIsTrialMode(m_session.isTrialMode());
			CalculateDiscount calc = new CalculateDiscount(model);
			try
			{
				calc.run();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				ADialog.error(m_posPanel.getWindowNo(), this, ex.getMessage());
				try 
				{
					DB.rollback(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				return;
			}
			try 
			{
				DB.commit(true, get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			m_posPanel.updateInfo();
			m_posPanel.m_rightPanel.updateInfo();
			setCursor(tmp);
		}
		else if (m_actionCommands[8].equals(command))
		{
			UNSPOSSpcDiscount spcDisc = new UNSPOSSpcDiscount(m_posPanel, true);
			spcDisc.setVisible(true);
		}
		else if (m_actionCommands[9].equals(command))
		{
			if (m_posPanel.m_posTrx == null)
				return ;
			UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
			confirm.setVisible(true);
			if (!confirm.isApproved())
			{
				EventQueue.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						m_posPanel.f_curLine.m_table.requestFocus();
						for (int i=0; i<m_posPanel.f_curLine.m_table.getRowCount(); i++)
						{
							if (m_posPanel.f_curLine.m_table.getValueAt(i, 0) == null)
							{
								m_posPanel.f_curLine.m_table.getSelectionModel().setSelectionInterval(i, i);
								m_posPanel.f_curLine.m_table.editCellAt(i, 2);
								break;
							}
						}
					}
				});
				return;
			}
			if (m_posPanel.m_posTrx.isProcessed())
			{
				String error = null;
				try 
				{
					if (m_posPanel.m_posTrx.processIt(DocAction.ACTION_Void))
					{
						error = m_posPanel.m_posTrx.getProcessMsg();
					}
				} 
				catch (Exception e1) 
				{
					error = e1.getMessage();
				}
				if (error != null)
				{
					try 
					{
						DB.rollback(true, get_TrxName());
					} 
					catch (SQLException e1) 
					{
						error += e1.getMessage();
					}
					ADialog.error(m_posPanel.getWindowNo(), this, error);
					return;
				}
				
				try 
				{
					DB.commit(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
					ADialog.error(this.m_posPanel.getWindowNo(), this, e1.getMessage());
					return;
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					ADialog.error(this.m_posPanel.getWindowNo(), this, e1.getMessage());
					return;
				}

				m_posPanel.newTransaction();
			}
			else
			{
				if (!m_posPanel.deleteTransaction())
				{
					String error = CLogger.retrieveErrorString("Unknown Error");
					try 
					{
						DB.rollback(true, get_TrxName());
					} 
					catch (IllegalStateException e1) 
					{
						error += e1.getMessage();
					} 
					catch (SQLException e1) 
					{
						error += e1.getMessage();
					}
					ADialog.error(this.m_posPanel.getWindowNo(), this, error);
					return;
				}
				try 
				{
					DB.commit(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
					ADialog.error(this.m_posPanel.getWindowNo(), this, e1.getMessage());
					return;
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					ADialog.error(this.m_posPanel.getWindowNo(), this, e1.getMessage());
					return;
				}
			}
		}
		else if (m_actionCommands[10].equals(command))
		{
			m_posPanel.printCurrencyRate();
		}
		else if (m_actionCommands[11].equals(command))
		{
			//No Action
		}
		else if (m_actionCommands[12].equals(command))
		{
			UNSPOSAddCapDialog exchange = new UNSPOSAddCapDialog(m_posPanel, true);
			exchange.setVisible(true);
		}
		else if (m_actionCommands[13].equals(command))
		{
			UNSPOSExchangeDialog exchange = new UNSPOSExchangeDialog(m_posPanel, true);
			exchange.setVisible(true);
		}
		else if (m_actionCommands[14].equals(command))
		{
			this.m_posPanel.m_form.dispose();
		}
		else if (m_actionCommands[15].equals(command))
		{
			m_posPanel.m_rightPanel.keyTableUpdate();
		}
	}
	
	private void setClickableButton (boolean[] clickable)
	{
		for (int i=0; i<clickable.length; i++)
		{
			if (i > (m_buttons.length-1))
				break;
			m_buttons[i].setEnabled(clickable[i]);
		}
	}
	
	public void enableComponent (UNSPOSTrxModel model)
	{
		if (model == null)
		{
			m_clickables[0] = false;
			m_clickables[1] = false;
			m_clickables[6] = false;
			m_clickables[9] = false;
			m_clickables[4] = false;
			m_clickables[8] = false;
			m_clickables[7] = false;
		}
		else
		{
			if (!model.isProcessed())
			{
				m_clickables[0] = true;
				m_clickables[6] = true;
				m_clickables[9] = true;
				m_clickables[8] = true;
				m_clickables[7] = true;
			}
			if (model.isProcessed())
			{
				m_clickables[6] = false;
				m_clickables[9] = true;
				m_clickables[8] = false;
				m_clickables[7] = false;
			}
			if (model.isPaid())
			{
				m_clickables[0] = false;
				m_clickables[9] = false;
				m_clickables[4] = true;
			}
			else
			{
				m_clickables[0] = true;
				m_clickables[4] = false;
			}
			
			m_clickables[1] = true;
		}
		
		m_clickables[11] = false;
		
		setClickableButton(m_clickables);
	}
	
	public void loadRefundDisplay (UNSPOSTrxModel refundModel)
	{
		if (refundModel == null || refundModel.get_ID() == 0)
			return;
		
		remove(m_panelButton);
		remove(m_panelInfo);
		add (m_scroll, "h 150px!,growx, growy, flowy, pushx, growx");
		add(m_panelButton, "wrap,h 150px!");
		updateTable(refundModel);
	}
	
	public void loadSalesDisplay ()
	{
		resetTable();
		remove(m_scroll);
		add (m_panelInfo, "growx, growy, flowy, pushx, pushy, growx");
		add(m_panelButton,"wrap");
	}
	
	public void resetTable ()
	{
		m_refundTable.setRowCount(0);
	}
	
	public void updateTable (UNSPOSTrxModel refundmoModel)
	{
		if (m_refundTable == null)
			return;
		String sql = "SELECT tl.Line, tl.barcode, p.Name, tl.QtyOrdered, tl.PriceActual, tl.ServiceCharge, tl.TaxAmt, tl.LineAmt "
				+ " FROM UNS_POSTrxLine tl INNER JOIN M_Product p ON "
				+ " p.M_Product_ID = tl.M_Product_ID WHERE UNS_POSTrx_ID = ? AND tl.IsBOM = 'N' ORDER BY tl.Line";
		PreparedStatement st = null;
		ResultSet rs = null;
		resetTable();
		DefaultTableModel model = (DefaultTableModel) m_refundTable.getModel();
		try
		{
			st = DB.prepareStatement(sql, m_posPanel.get_TrxName());
			st.setInt(1, refundmoModel.get_ID());
			rs = st.executeQuery();
			while (rs.next())
			{
				int no = rs.getInt(1);
				String sku = rs.getString(2);
				String product = rs.getString(3);
				BigDecimal qty = rs.getBigDecimal(4);
				if (qty == null)
					qty = Env.ZERO;
				BigDecimal price = rs.getBigDecimal(5);
				if (price == null)
					price = Env.ZERO;
				BigDecimal serviceCharge = rs.getBigDecimal(6);
				if (serviceCharge == null)
					serviceCharge = Env.ZERO;
				BigDecimal taxamt = rs.getBigDecimal(7);
				if (taxamt == null)
					taxamt = Env.ZERO;
				BigDecimal total = rs.getBigDecimal(8);
				if (total == null)
					total = Env.ZERO;
				String qtyStr = NumberFormat.getInstance().format(qty.doubleValue());
				String priceStr = NumberFormat.getInstance().format(price.doubleValue());
				String svcChrgStr = NumberFormat.getInstance().format(serviceCharge.doubleValue());
				String taxAmtStr = NumberFormat.getInstance().format(taxamt.doubleValue());
				String totalStr = NumberFormat.getInstance().format(total.doubleValue());
				model.addRow(new Object[]{no, sku, product, qtyStr, priceStr, svcChrgStr, taxAmtStr, totalStr});
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		for (int i=model.getRowCount(); i<15;i++)
		{
			model.addRow(new Object[]{null, null, null, null, null, null, null, null});
		}
	}
}
