/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.NumberFormat;

import org.compiere.swing.CButton;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import net.miginfocom.swing.MigLayout;


/**
 * @author nurse
 *
 */
public class UNSPOSFNBTable extends UNSPOSSubPanel 
{
	private Color m_background = new Color (0,110,0);
	private Color m_foreground = Color.WHITE;
	private Color m_holdBackground = Color.RED;
	private CButton[] m_tables;
	private Thread m_reservationThread;
	private boolean m_stopReservation = false;
	private String[] m_originalName;
	
	public UNSPOSFNBTable(UNSPOSPanel posPanel) 
	{
		super(posPanel);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1958872046957562588L;

	@Override
	protected void init() 
	{
		setLayout(new MigLayout("fill, insets 0 0 0 0","[fill][fill][fill][fill]","fill"));
		m_tables = new CButton[m_posPanel.m_fnbTables.length];
		m_originalName = new String[m_tables.length];
		for (int i=0; i<m_posPanel.m_fnbTables.length; i++)
		{
			String style = "growx, growy, pushy";
			if ((i+1) % 4 == 0)
				style += ",wrap";
			String name = m_posPanel.m_fnbTables[i].getName();
			CButton button = new CButton(name);
			button.setActionCommand(Integer.toString(m_posPanel.m_fnbTables[i].get_ID()));
			button.setFocusable(false);
			button.setBorderPainted(false);
			button.setBackground(m_background);
			button.setForeground(m_foreground);
			if (i==2 || i==8 || i==7 || i == 13)
				button.setBackground(Color.RED);
			button.setFont(getFont().deriveFont(Font.TRUETYPE_FONT, (float) 12.0)); 
			ActionListener[] listeners = button.getActionListeners();
			for (int j=0; j<listeners.length; j++)
			{
				button.removeActionListener(listeners[j]);
			}
			button.addActionListener(this);
			add(button, style);
			m_tables[i] = button;
			m_originalName[i] = name;
		}
		
		m_reservationThread = new Thread(new Runnable() 
		{	
			@Override
			public void run() 
			{
				while (!m_stopReservation)
				{
					for (int i=0; i<m_tables.length; i++)
					{
						if (m_stopReservation)
							break;
						String sql = "SELECT IsReserved FROM UNS_POSFNBTableLine WHERE UNS_POSFNBTableLine_ID = ?";
						String rsvd = DB.getSQLValueString(null, sql, Integer.valueOf(m_tables[i].getActionCommand()));
						if ("Y".equals(rsvd))
						{
							sql = "SELECT COALESCE (GrandTotal, 0) FROM UNS_POSTrx WHERE UNS_POSFNBTableLine_ID = ? AND DocStatus IN ('DR','IP','IN')";
							BigDecimal gt = DB.getSQLValueBD(get_TrxName(), sql, Integer.valueOf(m_tables[i].getActionCommand()));
							if (gt == null)
								gt = Env.ZERO;
							StringBuilder nameBUild = new StringBuilder("<html>");
							String name = m_originalName[i];
							nameBUild.append(name).append("<br/>").append(NumberFormat.getInstance().format(gt));
							nameBUild.append("</html>");
							String nm = nameBUild.toString();
							m_tables[i].setText(nm);
							m_tables[i].setBackground(m_holdBackground);
						}
						else
						{
							m_tables[i].setText(m_originalName[i]);
							m_tables[i].setBackground(m_background);
							m_tables[i].updateUI();
						}
						if (!"Y".equals(rsvd) && (m_posPanel.m_posTrx == null || m_posPanel.m_posTrx.isPaid() || m_posPanel.isOnRefund()))
							m_tables[i].setEnabled(false);
						else
							m_tables[i].setEnabled(true);
					}
					
					try 
					{
						Thread.sleep(1000);
					} 
					catch (InterruptedException e) 
					{
						m_stopReservation = true;
						e.printStackTrace();
					}
				}
			}
		});
		m_reservationThread.setName("TableReservation");
		m_reservationThread.start();
	}
	
	@Override
	public void actionPerformed (ActionEvent e)
	{
		if (e.getSource() instanceof CButton == false)
			return;
		CButton b = (CButton) e.getSource();
		int id = Integer.valueOf(b.getActionCommand());
		String name = b.getName();
		KeyNamePair pair = new KeyNamePair(id, name);
		m_posPanel.reserveTable(pair);
	}
	
	public void setBackground (Color background)
	{
		m_background = background;
	}
	
	public void setForeground (Color color)
	{
		m_foreground = color;
	}
	
	public void setReseverdColor (Color color)
	{
		m_holdBackground = color;
	}
	
	public void stopReservationThread ()
	{
		m_stopReservation = true;
	}
}