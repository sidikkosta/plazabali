package com.uns.pos;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import org.compiere.swing.CButton;
import org.compiere.util.ValueNamePair;
import net.miginfocom.swing.MigLayout;

public class UNSPOSKeyboardPanel extends UNSPOSSubPanel implements ActionListener {

	private Color m_background = new Color (5, 132,63);
	private Color m_foreground = Color.WHITE;
	@SuppressWarnings("nls")
	static final ValueNamePair[] m_buttonName = new ValueNamePair[] {
		new ValueNamePair(KeyEvent.VK_1+"", "1"),
		new ValueNamePair(KeyEvent.VK_2+"", "2"),
		new ValueNamePair(KeyEvent.VK_3+"", "3"),
		new ValueNamePair(KeyEvent.VK_BACK_SPACE+"", "<html>Back<br/>Space</html>"),
		new ValueNamePair(KeyEvent.VK_4+"", "4"),
		new ValueNamePair(KeyEvent.VK_5+"", "5"),
		new ValueNamePair(KeyEvent.VK_6+"", "6"),
		new ValueNamePair(KeyEvent.VK_7+"", "7"),
		new ValueNamePair(KeyEvent.VK_8+"", "8"),
		new ValueNamePair(KeyEvent.VK_9+"", "9"),
		new ValueNamePair(KeyEvent.VK_DELETE+"", "Delete"),
		new ValueNamePair(KeyEvent.VK_ALT+"", "Alt"),
		new ValueNamePair(KeyEvent.VK_0+"", "0"),
		new ValueNamePair(KeyEvent.VK_CONTROL+"", "Ctrl"),
		new ValueNamePair(KeyEvent.VK_PAGE_UP+"", "<html>Page<br/>Up</html>"),
		new ValueNamePair(KeyEvent.VK_UP+"", "<html>\u2191</html>"),
		new ValueNamePair(KeyEvent.VK_PAGE_DOWN+"", "<html>Page<br/>Down</html>"),
		new ValueNamePair(KeyEvent.VK_ENTER+"", "<html>\u21b5</html>"),
		new ValueNamePair(KeyEvent.VK_LEFT+"", "<html>\u2190</html>"),
		new ValueNamePair(KeyEvent.VK_DOWN+"", "<html>\u2193</html>"),
		new ValueNamePair(KeyEvent.VK_RIGHT+"", "<html>\u2192</html>")
	};
	
	static final String[] m_buttonTooltip = new String[] {
		"1", //$NON-NLS-1$
		"2", //$NON-NLS-1$
		"3", //$NON-NLS-1$
		"Back Space", //$NON-NLS-1$
		"4", //$NON-NLS-1$
		"5", //$NON-NLS-1$
		"6", //$NON-NLS-1$
		"7", //$NON-NLS-1$
		"8", //$NON-NLS-1$
		"9", //$NON-NLS-1$
		"Delete", //$NON-NLS-1$
		"Alt", //$NON-NLS-1$
		"0", //$NON-NLS-1$
		"Ctrl", //$NON-NLS-1$
		"Page Up",//$NON-NLS-1$
		"Up",//$NON-NLS-1$
		"Page Down",//$NON-NLS-1$
		"Enter", //$NON-NLS-1$
		"Left",//$NON-NLS-1$
		"Down",//$NON-NLS-1$
		"Right"//$NON-NLS-1$
	};

	/**
	 * 
	 */
	private static final long serialVersionUID = 6097507458932168553L;

	public UNSPOSKeyboardPanel(UNSPOSPanel posPanel) 
	{
		super(posPanel);
	}
	
	public UNSPOSKeyboardPanel (UNSPOSPanel posPanel, Color background, Color foreground)
	{
		this(posPanel);
		m_background = background;
		m_foreground = foreground;
	}
	
	private int m_lastEvent = -1;

	@SuppressWarnings("nls")
	@Override
	protected void init() {
		setLayout(new MigLayout("fill, insets 0","[25%][25%][25%][25%]","fill"));
		setFocusable(false);
		int add = 0;
		for (int i=0; i<m_buttonName.length; i++)
		{
			CButton button = new CButton(m_buttonName[i].getName());
			button.setActionCommand(m_buttonName[i].getID());
			button.setBackground(m_background);
			button.setForeground(m_foreground);
			String wrap = ",h 10%";
			if ((i+1+add) % 4 == 0)
			{
				wrap = ",wrap,";
			}
			if (m_buttonName[i].getName().equals("<html>\u21b5</html>")
					|| m_buttonName[i].getName().equals("Delete")
					|| m_buttonName[i].getName().equals("<html>Back<br/>Space</html>"))
			{
				add++;
				wrap = ",spany 2, wrap,h 20%";
			}
			button.setToolTipText(m_buttonTooltip[i]);
			button.setFocusable(false);
			button.addActionListener(this);
			button.setBorderPainted(false);
			button.setFont(getFont().deriveFont(Font.TRUETYPE_FONT, (float) 18.0)); 
			add(button, "growx, growy, pushy" + wrap);
		}
	}
	
	@Override
	public void actionPerformed (ActionEvent e)
	{		
		try 
		{
			int command = new Integer(e.getActionCommand());
			if(command == KeyEvent.VK_ALT || command == KeyEvent.VK_CONTROL)
			{
				this.m_lastEvent = command;
				return;
			}

			Robot robot = new Robot();
			if (this.m_lastEvent != -1)
			{
				robot.keyPress(this.m_lastEvent);
			}
			
			robot.keyPress(command);

			if (this.m_lastEvent != -1)
			{
				robot.keyRelease(this.m_lastEvent);
			}
			
			robot.keyRelease(command);
			this.m_lastEvent = -1;
		} 
		catch (AWTException e1) 
		{
			e1.printStackTrace();
		}
	}
}
