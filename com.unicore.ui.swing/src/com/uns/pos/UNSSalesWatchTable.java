/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Font;
import java.util.Vector;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import com.uns.tablemodel.UNSSalesWatchTableModel;
import com.uns.tablemodel.UNSValuePair;

/**
 * @author nurse
 *
 */
public class UNSSalesWatchTable extends CPanel
{
	UNSSalesWatchTableModel m_model;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8421873284717483504L;

	/**
	 * 
	 */
	public UNSSalesWatchTable() 
	{
		super(new MigLayout("fill, insets 0 0", "[300px!,fill][fill][fill][fill][fill][fill][fill]", ""));
		m_model = new UNSSalesWatchTableModel();
	}
	
	public void init (int orgID)
	{
		removeAll();
		String wrap = "";
		String hStyle = "top, pushy,h 30px!";
		String cStyle = "top, pushy,h 65px!";
		String tStyle = "top, pushy,h 65px!";
		Font hFont = new Font("Calibri", Font.BOLD, 18);
		Font cFont = new Font("Calibri", Font.BOLD, 14);
		Font tFont = new Font("Calibri", Font.BOLD, 16);
		m_model.updateRecord(orgID);
		Border border = new EmptyBorder(5, 5, 5, 5);
		String[] headers = m_model.getHeaders();
		for (int i=0; i<headers.length; i++)
		{
			if (i==6)
				wrap = ",wrap";
			else
				wrap = "";
			CLabel h = new CLabel(m_model.getHeaders()[i]);
			h.setBorder(border);
			h.setFont(hFont);
			h.setOpaque(true);
			h.setForeground(Color.white);
			h.setBackground(new Color(79,129,189));
			h.setHorizontalAlignment(SwingConstants.CENTER);
			h.setText(headers[i]);
			add(h, hStyle + wrap);
		}
		
		Vector<Vector<UNSValuePair<Object, Object>>> contents = m_model.getRows();
		for (int i=0; i<contents.size(); i++)
		{
			Vector<UNSValuePair<Object, Object>> row = contents.get(i);
			for (int j=0; j<row.size(); j++)
			{
				if (j== 6 && i< contents.size())
					wrap = ",wrap";
				else
					wrap = "";
				CLabel c = new CLabel("");
				c.setFont(cFont);
				c.setOpaque(true);
				c.setBorder(border);
				StringBuilder sb = new StringBuilder("<html>");
				if (i%2==0)
					c.setBackground(new Color(233,237,244));
				else
					c.setBackground(new Color(208,216, 232));
				if (j== 0)
				{
					c.setHorizontalAlignment(SwingConstants.LEFT);
					sb.append("<p align=left>");
				}
				else
				{
					c.setHorizontalAlignment(SwingConstants.RIGHT);
					sb.append("<p align=right>");
				}
				
				String oriMsg = row.get(j).toString();
				oriMsg = oriMsg.replace("\n", "<br/>");
				sb.append(oriMsg).append("</p></html>");
				String msg = sb.toString();
				c.setText(msg);
				add(c, cStyle + wrap);
			}
		}
		
		UNSValuePair<Object, Object>[] totals = m_model.getTotals();
		wrap = "";
		for (int i=0; i<totals.length; i++)
		{
			if (i== 6)
				wrap = ",wrap";
			CLabel t = new CLabel("");
			t.setBorder(border);
			t.setFont(tFont);
			t.setOpaque(true);
			if (contents.size() % 2 == 0)
				t.setBackground(new Color(233,237,244));
			else
				t.setBackground(new Color(208,216, 232));

			StringBuilder sb = new StringBuilder("<html>");
			if (i== 0)
			{
				t.setHorizontalAlignment(SwingConstants.LEFT);
				sb.append("<p align=left>");
			}
			else
			{
				t.setHorizontalAlignment(SwingConstants.RIGHT);
				sb.append("<p align=right>");
			}
			
			String oriMsg = totals[i].toString();
			oriMsg = oriMsg.replace("\n", "<br/>");
			sb.append(oriMsg).append("</p></html>");
			String msg = sb.toString();
			t.setText(msg);
			add(t, tStyle + wrap);
		}
		
		updateUI();
	}
}
