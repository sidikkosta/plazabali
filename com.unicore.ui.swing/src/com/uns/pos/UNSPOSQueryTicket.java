package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.compiere.apps.ADialog;
import org.compiere.grid.ed.VDate;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.PO;
import org.compiere.swing.CButton;
import org.compiere.swing.CCheckBox;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

public class UNSPOSQueryTicket extends UNSPOSQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1487067464540688770L;

	/**
	 * 	Constructor
	 */
	public UNSPOSQueryTicket (UNSPOSPanel posPanel)
	{
		super(posPanel);
	}	//	PosQueryProduct

	
	private UNSPOSTextField		f_documentno;
	private UNSPOSTextField		f_trxNo;
	private VDate			f_from;
	private VDate			f_to;

	private int				m_postrx_id;
	private CCheckBox 		f_processed;
	private CButton f_refresh;
	private CButton f_ok;
	private CButton f_cancel;
	private List<PO> m_onLocation = null;
	
	/**	Table Column Layout Info			*/
	@SuppressWarnings("nls")
	private static ColumnInfo[] s_layout = new ColumnInfo[] 
	{
		new ColumnInfo(" ", "UNS_POSTrx_ID", IDColumn.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "DocumentNo"), "DocumentNo", String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "TrxNo"), "TrxNo", String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "C_BPartner_ID"), 
				"(SELECT Name FROM C_BPartner WHERE C_BPartner_ID = UNS_POSTrx.C_BPartner_ID)", 
				String.class),

		new ColumnInfo(Msg.translate(Env.getCtx(), "AD_User_ID"), 
				"(SELECT Name FROM AD_User WHERE AD_User_ID = UNS_POSTrx.SalesRep_ID)", 
				String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Date"), "DateTrx", Timestamp.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "GrandTotal"), "GrandTotal", BigDecimal.class)
	};

	/**
	 * 	Set up Panel
	 */
	@SuppressWarnings("nls")
	@Override
	protected void init()
	{
		CPanel panel = new CPanel();
		panel.setBackground(new Color(243, 250, 236));
		panel.setLayout(new MigLayout("fill","[70%][30%]",""));
		getContentPane().add(panel);
		if (p_posPanel.isOnRefund())
			setTitle("Refundable Transaction");
		else
			setTitle("Previous Transaction");
		//	North
		this.northPanel = new CPanel(new MigLayout("fill","", "[50][50]"));
		panel.add (this.northPanel, "north");
		this.northPanel.setBorder(new TitledBorder(Msg.getMsg(getCtx (), "Query")));
		
		CLabel ldoc = new CLabel(Msg.translate(getCtx (), "DocumentNo"));
		this.northPanel.add (ldoc, " growy");
		this.f_documentno = new UNSPOSTextField("", this.p_posPanel, 0);
		ldoc.setLabelFor(this.f_documentno);
		this.northPanel.add(this.f_documentno, "h 30, w 200");
		this.f_documentno.addActionListener(this);
		
		CLabel lTrxNo = new CLabel(Msg.translate(getCtx(), "TrxNo"));
		this.northPanel.add(lTrxNo, "growy");
		this.f_trxNo = new UNSPOSTextField("", this.p_posPanel, 0);
		lTrxNo.setLabelFor(this.f_trxNo);
		this.northPanel.add(this.f_trxNo, "h 30, w 200");
		this.f_trxNo.addActionListener(this);
		
		this.f_processed = new CCheckBox(Msg.translate(getCtx (), "Processed"));
		this.f_processed.setSelected(false);
		this.northPanel.add(this.f_processed, "wrap");
		//
		CLabel lfrom = new CLabel(Msg.translate(getCtx (), "From"));
		this.northPanel.add (lfrom, "growy");
		this.f_from = new VDate();
		this.f_from.setValue(p_posPanel.m_session.getDateAcct());
		lfrom.setLabelFor(this.f_from);
		this.northPanel.add(this.f_from, "h 30, w 200");
		this.f_from.addActionListener(this);
		
		CLabel lto = new CLabel(Msg.translate(getCtx (), "To"));
		this.northPanel.add (lto, "growy");
		this.f_to = new VDate();
		this.f_to.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		lto.setLabelFor(this.f_to);
		this.northPanel.add(this.f_to, "h 30, w 200");
		this.f_to.addActionListener(this);
		
		this.f_refresh = createButtonAction("Refresh", KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		this.northPanel.add(this.f_refresh, "w 50!, h 50!, wrap, alignx trailing");
		
		this.f_up = createButtonAction("Previous", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		this.northPanel.add(this.f_up, "w 50!, h 50!, span, split 4");
		this.f_down = createButtonAction("Next", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		this.northPanel.add(this.f_down, "w 50!, h 50!");
		
		this.f_ok = createButtonAction("Ok", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, Event.CTRL_MASK));
		this.northPanel.add(this.f_ok, "w 50!, h 50!");
		
		this.f_cancel = createButtonAction("Cancel", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		this.northPanel.add(this.f_cancel, "w 50!, h 50!");
		f_cancel.setBorder(null);
		
		//	Center
		this.m_table = new UNSPOSTable();
		
		this.m_table.prepareTable (s_layout, "UNS_POSTrx", 
				null, false, "UNS_POSTrx");
		this.m_table.addMouseListener(this);
		this.m_table.getSelectionModel().addListSelectionListener(this);
		enableButtons();
		this.centerScroll = new CScrollPane(this.m_table);
		panel.add (this.centerScroll, "growx, growy");
		this.m_table.growScrollbars();
		panel.setPreferredSize(new Dimension(800,600));

		this.f_documentno.requestFocus();
		
		UNSPOSKeyboardPanel kbp = new UNSPOSKeyboardPanel(this.p_posPanel);
		kbp.init();
		panel.add(kbp, "spanx, wrap, growx, growy,flowx, flowy,");
		setMinimumSize(new Dimension(900,600));
		setResizable(true);
		if (p_posPanel.isOnRefund())
		{
			f_processed.setSelected(true);
			f_processed.setEnabled(false);	
		}
		else
		{
			f_processed.setEnabled(true);
			f_processed.setSelected(false);
		}
		setResults(getCtx (), this.f_processed.isSelected(), this.f_documentno.getText(), 
				this.f_trxNo.getText(), this.f_from.getTimestamp(), this.f_to.getTimestamp());
	}	//	init

	
	/**
	 * 	Action Listener
	 *	@param e event
	 */
	@SuppressWarnings("nls")
	@Override
	public void actionPerformed (ActionEvent e)
	{
		if (log.isLoggable(Level.INFO)) log.info("PosQueryProduct.actionPerformed - " + e.getActionCommand());
		if ("Refresh".equals(e.getActionCommand())
			|| e.getSource() == this.f_processed || e.getSource() == this.f_documentno
			|| e.getSource() == this.f_from || e.getSource().equals(this.f_trxNo))
		{
			setResults(getCtx (), this.f_processed.isSelected(), this.f_documentno.getText(), 
					this.f_trxNo.getText(), this.f_from.getTimestamp(), this.f_to.getTimestamp());
			return;
		}
		else if ("Reset".equals(e.getActionCommand()))
		{
			reset();
			return;
		}
		else if ("Previous".equalsIgnoreCase(e.getActionCommand()))
		{
			int rows = this.m_table.getRowCount();
			if (rows == 0)
				return;
			int row = this.m_table.getSelectedRow();
			row--;
			if (row < 0)
				row = 0;
			this.m_table.getSelectionModel().setSelectionInterval(row, row);
			return;
		}
		else if ("Next".equalsIgnoreCase(e.getActionCommand()))
		{
			int rows = this.m_table.getRowCount();
			if (rows == 0)
				return;
			int row = this.m_table.getSelectedRow();
			row++;
			if (row >= rows)
				row = rows - 1;
			this.m_table.getSelectionModel().setSelectionInterval(row, row);
			return;
		}
		else if ("Cancel".equalsIgnoreCase(e.getActionCommand()))
		{
			dispose();
			return;
		}
		//	Exit
		close();
	}	//	actionPerformed
	
	
	/**
	 * 
	 */
	@SuppressWarnings("nls")
	@Override
	public void reset()
	{
		this.f_processed.setSelected(false);
		this.f_documentno.setText(null);
		this.f_from.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		setResults(getCtx (), this.f_processed.isSelected(), this.f_documentno.getText(), 
				this.f_trxNo.getText(), this.f_from.getTimestamp(), this.f_to.getTimestamp());
	}


	/**
	 * 	Set/display Results
	 *	@param results results
	 */
	@SuppressWarnings("nls")
	public void setResults (Properties ctx, boolean processed, String doc, String trxNo, 
			Timestamp from, Timestamp to)
	{
		StringBuilder builder = new StringBuilder("SELECT ");
		
		for (int i=0; i<s_layout.length; i++)
		{
			String columnSQL = s_layout[i].getColSQL();
			log.info("Add column SQL :: " + columnSQL);
			if (i > 0 && i < s_layout.length)
			{
				builder.append(", ");
			}
			builder.append(columnSQL);
		}
		
		builder.append(" FROM UNS_POSTrx WHERE CAST(TRUNC(DateTrx) AS DATE) BETWEEN ? AND ? AND Processed = ?");
		
		if (!Util.isEmpty(doc, true))
		{
			builder.append(" AND UPPER(DocumentNo) LIKE ? ");
		}
		
		if (!Util.isEmpty(trxNo, true))
		{
			builder.append(" AND UPPER(TrxNo) LIKE ? ");
		}
		
		if (p_posPanel.isOnRefund())
		{
			builder.append(" AND IsPaid = 'Y' AND C_DocType_ID IN (SELECT C_DocType_ID FROM C_DocType "
					+ " WHERE DocBaseType = 'POS') ");
		}
		else
		{
			builder.append(" AND UNS_POS_Session_ID = ?");
		}
		
		PreparedStatement st = null;
		ResultSet rs = null;
		String sql = builder.toString();
		
		try 
		{	
			int i=0;
			st = DB.prepareStatement(sql, this.p_posPanel.get_TrxName());
			st.setTimestamp(++i, null == from ? TimeUtil.trunc(from, TimeUtil.TRUNC_DAY) : from);
			st.setTimestamp(++i, null == to ? TimeUtil.trunc(to, TimeUtil.TRUNC_DAY) : to);
			st.setString(++i, processed ? "Y" : "N");
			
			if (!Util.isEmpty(doc, true))
			{
				st.setString(++i, doc.toUpperCase());
			}
			
			if (!Util.isEmpty(trxNo, true))
			{
				st.setString(++i, trxNo.toUpperCase());
			}
			
			if (!p_posPanel.isOnRefund())
			{
				st.setInt(++i, p_posPanel.m_session.get_ID());
			}
			
			rs = st.executeQuery();
			this.m_table.loadTable(rs);
			if (this.m_table.getRowCount() > 0 )
			{
//				this.m_table.setRowSelectionInterval(0, 0);
				m_onLocation = null;
			}
			else if (p_posPanel.isOnRefund() && (!Util.isEmpty(f_documentno.getText(), true) 
					|| !Util.isEmpty(f_trxNo.getText(), true)))
			{
				UNSPOSQueryTicketLoader serverLoader = new UNSPOSQueryTicketLoader (
						f_from.getTimestamp(), f_to.getTimestamp(), f_documentno.getText(), f_trxNo.getText(), null);
				m_onLocation = serverLoader.getData();
				if (m_onLocation != null)
				{
					PO[] pos = new PO[m_onLocation.size()];
					m_onLocation.toArray(pos);
					m_table.loadTable(pos);
				}
			}
			
			enableButtons();
		}
		catch(Exception e)
		{
			log.severe("QueryTicket.setResults: " + e + " -> " + sql);
			ADialog.error(super.p_posPanel.getWindowNo(), this, e.getMessage());
		}
		finally 
		{
			DB.close(rs, st);
		}
	}	//	setResults

	/**
	 * 	Enable/Set Buttons and set ID
	 */
	@SuppressWarnings("nls")
	protected void enableButtons()
	{
		this.m_postrx_id = -1;
		int row = this.m_table.getSelectedRow();
		boolean enabled = row != -1;
		if (enabled)
		{
			Integer ID = this.m_table.getSelectedRowKey();
			if (ID != null)
			{
				this.m_postrx_id = ID.intValue();
			}
		}
		
		this.f_ok.setEnabled(enabled);
		
		if (log.isLoggable(Level.INFO)) log.info("ID=" + this.m_postrx_id); 
	}	//	enableButtons

	/**
	 * 	Close.
	 * 	Set Values on other panels and close
	 */
	@SuppressWarnings("nls")
	@Override
	protected void close()
	{
		if (log.isLoggable(Level.INFO)) log.info("UNS_POSTrx_ID=" + this.m_postrx_id); 
		
		if (this.m_postrx_id > 0)
		{
			if (p_posPanel.isOnRefund())
			{
				UNSPOSSpvConfirmDialog dialog = new UNSPOSSpvConfirmDialog(p_posPanel, true);
				dialog.setVisible(true);
				if (!dialog.isApproved())
					p_posPanel.cancelRefund();
				else
				{
					p_posPanel.setRefund(m_postrx_id);
				}
			}
			else
			{
				this.p_posPanel.setPOSTrx(this.m_postrx_id);
				this.p_posPanel.updateInfo();
//				this.p_posPanel.p_bottom.loadInfoDiscount();
			}
		}
		else
			p_posPanel.cancelRefund();
		
		m_onLocation = null;
		super.dispose();
	}	//	close

	@Override
	public void dispose ()
	{
		if (p_posPanel.isOnRefund() && isVisible())
			p_posPanel.cancelRefund();
		if (this.m_postrx_id <= 0 && !p_posPanel.isOnRefund())
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					p_posPanel.f_curLine.m_table.requestFocus();
					for (int i=0; i<p_posPanel.f_curLine.m_table.getRowCount(); i++)
					{
						if (p_posPanel.f_curLine.m_table.getValueAt(i, 0) == null)
						{
							p_posPanel.f_curLine.m_table.getSelectionModel().setSelectionInterval(i, i);
							p_posPanel.f_curLine.m_table.editCellAt(i, 2);
							break;
						}
					}
				}
			});
		}
		super.dispose();
	}
}
