package com.uns.pos;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.swing.MigLayout;

import org.adempiere.plaf.AdempierePLAF;
import org.compiere.apps.ADialog;
import org.compiere.grid.ed.VEditor;
import org.compiere.grid.ed.VString;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.minigrid.MiniCellEditor;
import org.compiere.model.MProduct;
import org.compiere.pos.SubCurrentLine;
import org.compiere.swing.CScrollPane;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

import com.unicore.model.I_UNS_POSTrxLine;
import com.unicore.model.MUNSPOSTerminal;
import com.unicore.model.MUNSPOSTrxLine;

public class UNSSubCurrentLine extends UNSPOSSubPanel implements ListSelectionListener {
	
	private int m_posdetail_ID = 0;
	private static CLogger log = CLogger.getCLogger(SubCurrentLine.class);
	protected MyTable m_table = null;
	private String m_sql = null;
	private static String s_sqlFrom = "UNS_POSTrxLine";
	private static String s_sqlWhere = "UNS_POSTrx_ID=? AND IsActive = 'Y' AND IsBOM = 'N'"; 
	private MProduct m_product = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 411204298952131418L;

	public UNSSubCurrentLine(UNSPOSPanel posPanel) {
		super(posPanel);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if ( e.getValueIsAdjusting() )
			return;
		
		int x = e.getFirstIndex();
		int y = e.getLastIndex();
		if (m_table.getRowCount() > x && m_table.getValueAt(x, 0) == null)
		{
			if (m_table.getValueAt(x, 2) != null)
				m_table.setValueAt(null, x, 2);
			if (m_table.getValueAt(x, 3) != null)
				m_table.setValueAt(null, x, 3);
			if (m_table.getValueAt(x, 5) != null)
				m_table.setValueAt(null, x, 5);
		}
		if (m_table.getRowCount() > y && m_table.getValueAt(y, 0) == null)
		{
			if (m_table.getValueAt(y, 2) != null)
				m_table.setValueAt(null, y, 2);
			if (m_table.getValueAt(y, 3) != null)
				m_table.setValueAt(null, y, 3);
			if (m_table.getValueAt(y, 5) != null)
				m_table.setValueAt(null, y, 5);
		}
		int row = this.m_table.getSelectedRow();
		if (row != -1 )
		{
			Object data = this.m_table.getModel().getValueAt(row, 0);
			if ( data != null )
			{
				Integer id = (Integer) ((IDColumn)data).getRecord_ID();
				this.m_posdetail_ID = id;
			}
			else
			{
				this.m_posdetail_ID = 0;
				m_table.requestFocus();
			}
		}
		else
			this.m_posdetail_ID = 0;
	}

	public void dispose ()
	{
		super.dispose();
	}
	
	@SuppressWarnings("nls")
	private static final ColumnInfo[] m_layouts = new ColumnInfo[]
	{
		new ColumnInfo(Msg.translate(Env.getCtx(), I_UNS_POSTrxLine.COLUMNNAME_UNS_POSTrxLine_ID), 
				I_UNS_POSTrxLine.COLUMNNAME_UNS_POSTrxLine_ID, IDColumn.class),
		new ColumnInfo("NO", 
				"Line", Integer.class, true, false, null),
		new ColumnInfo("SKU", "Barcode",String.class, false, false, null),
		new ColumnInfo("PRODUCT", 
				"(SELECT Name FROM M_Product WHERE M_Product_ID = UNS_POSTrxLine.M_Product_ID) ",
				String.class, true, false, null),
		new ColumnInfo("QTY", "QtyOrdered", Integer.class, false, false, null),
		new ColumnInfo("PRICE", "PriceList", BigDecimal.class, true, false, null), 
		new ColumnInfo("DISCOUNT", "DiscountAmt", BigDecimal.class, true, false, null), 
		new ColumnInfo("SERCHG", "ServiceCharge", BigDecimal.class, true, false, null),
		new ColumnInfo("PB-1/TAX", "TaxAmt", BigDecimal.class, true, false, null),
		new ColumnInfo(Msg.translate(Env.getCtx(), "TOTAL"), "lineAmt", 
				BigDecimal.class, true, false, null)
	};
	
	
	@SuppressWarnings({ "nls", "static-access" })
	public void init ()
	{
		setLayout(new MigLayout("fill, ins 0 0"));
		this.m_table = new MyTable();
		
		CScrollPane scroll = new CScrollPane(this.m_table);
		this.m_sql = this.m_table.prepareTable (this.m_layouts, this.s_sqlFrom, 
				this.s_sqlWhere, false, "UNS_POSTrxLine")
				+ " ORDER BY Line";
		m_table.addPropertyChangeListener(new PropertyChangeListener() 
		{
			@Override
			public void propertyChange(PropertyChangeEvent arg0) 
			{
				if (!"tableCellEditor".equals(arg0.getPropertyName()))
					return;
				if (m_table.getSelectedRow() == -1)
					return;
				int selected = m_table.getSelectedColumn();
				if (selected == 4 && m_posdetail_ID > 0 && m_table.isCellEditable(m_table.getSelectedRow(), selected))
				{
					MUNSPOSTrxLine line = new MUNSPOSTrxLine(getCtx (), m_posdetail_ID, get_TrxName ());
					
					if ( line != null )
					{
						BigDecimal qty = new BigDecimal(m_table.getValueAt(m_table.getSelectedRow(), 4).toString());
						if (m_posPanel.isOnRefund())
							qty = qty.abs().negate();
						if (line.getQtyOrdered().compareTo(qty) == 0)
							return;
						String _sql = "UPDATE UNS_DiscountTrx SET IsNeedRecalculate = 'Y' WHERE (UNS_POSTrx_ID = ? OR UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?)) AND Name NOT LIKE 'Special%'";
						int ok = DB.executeUpdate(_sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrx_ID()}, false, get_TrxName());
						if (ok == -1)
						{
							m_table.setValueAt(line.getQtyOrdered(), m_table.getSelectedRow(), 4);
							ADialog.error(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, CLogger.retrieveErrorString("Could not update POS Trx"));
							return ;
						}
						String sql = "SELECT COUNT(*) FROM UNS_DiscountTrx WHERE UNS_POSTrxLine_ID = ? OR UNS_POSTrx_ID = ?";
						int exists = DB.getSQLValue(get_TrxName(), sql, line.getUNS_POSTrxLine_ID(),line.getUNS_POSTrx_ID());
						if (exists > 0)
						{
							if (!ADialog.ask(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, 
									"This action will delete the discount that was previously defined, Continue this process?"))
							{
								m_table.setValueAt(line.getQtyOrdered(), m_table.getSelectedRow(), 4);
								return;
							}
						}
						UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
						confirm.setVisible(true);
						if (!confirm.isApproved())
						{
							m_table.setValueAt(line.getQtyOrdered(), m_table.getSelectedRow(), 4);
							return;
						}
						
						if (exists > 0)
						{
							sql = "DELETE FROM UNS_DiscountTrx WHERE UNS_POStrx_ID = ? OR UNS_POStrxLine_ID = ?";
							int executeOK = DB.executeUpdate(sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrxLine_ID()}, 
									false, get_TrxName());
							if (executeOK == -1)
							{
								m_table.setValueAt(line.getQtyOrdered(), m_table.getSelectedRow(), 4);
								String error = CLogger.retrieveErrorString("Unknown-Error");
								ADialog.error(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, error);
								return;
							}
							line.setDiscountAmt(Env.ZERO);
							line.getParent().setDiscountAmt(Env.ZERO);
							if (!line.getParent().save())
							{
								m_table.setValueAt(line.getQtyOrdered(), m_table.getSelectedRow(), 4);
								String error = CLogger.retrieveErrorString("Unknown-Error");
								ADialog.error(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, error);
								return;
							}
						}
						
						line.setQtyOrdered(qty);
						line.setQtyEntered(line.getQtyOrdered().subtract(line.getQtyBonuses()));
						
						if (!line.save())
						{
							m_table.setValueAt(line.getQtyOrdered(), m_table.getSelectedRow(), 4);
							String error = CLogger.retrieveErrorString("Unknown-Error");
							ADialog.error(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, error);
							return;
						}
						m_posPanel.updateInfo();
					}
				}
				else if (m_posdetail_ID == 0)
					m_table.setValueAt(null, m_table.getSelectedRow(), 4);
			}
		});
		boolean isServiceCharge = m_posPanel.m_storeConfig.isServiceCharge();
		boolean isPB1Tax = m_posPanel.m_storeConfig.isPB1Tax();
		this.m_table.getSelectionModel().addListSelectionListener(this);
		this.m_table.getColumn(0).setPreferredWidth(25);
		this.m_table.getColumn(1).setPreferredWidth(50);
		this.m_table.getColumn(2).setPreferredWidth(500);
		this.m_table.getColumn(3).setPreferredWidth(75);
		this.m_table.getColumn(4).setPreferredWidth(75);
		this.m_table.getColumn(5).setPreferredWidth(75);
		if (!isServiceCharge)
			m_table.setColumnVisibility(m_table.getColumn(7), false);
		if (!isPB1Tax)
			m_table.setColumnVisibility(m_table.getColumn(8), false);
		this.m_table.getColumn(6).setPreferredWidth(75);
		this.m_table.getColumn(7).setPreferredWidth(75);
		this.m_table.getColumn(8).setPreferredWidth(75);
		this.m_table.getColumn(9).setPreferredWidth(75);
		this.m_table.setColumnVisibility(m_table.getColumn(0), false);
		this.m_table.growScrollbars();
		this.m_table.getTableHeader().setPreferredSize(new Dimension(scroll.getWidth(), 24));
		this.m_table.getTableHeader().setFont(AdempierePLAF.getFont_Field().deriveFont(12f));
		this.m_table.getTableHeader().setBackground(new Color(155,187,89));
		add (scroll, "growx, spanx, growy, pushy");
		updateTable(null);
		final MiniCellEditor editor = (MiniCellEditor)m_table.getColumn(2).getCellEditor();
		editor.addEditorActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				findProduct((String)editor.getCellEditorValue());
			}
		});
		
		VEditor editorOfeditor = editor.getEditor();
		if (editorOfeditor instanceof VString)
		{
			VString sEdit = (VString) editorOfeditor;
			sEdit.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent arg0) 
				{
					//DO Nothing
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) 
				{
					//DO Nothing
				}
				
				@Override
				public void keyPressed(KeyEvent arg0) 
				{
					int keyCode = arg0.getKeyCode();
					if (KeyEvent.VK_F1 == keyCode)
					{
						m_posPanel.m_botPanel.requestFocus();
//						EventQueue.invokeLater(new Runnable() {
//							public void run() {
//								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
//										m_posPanel.m_botPanel.m_buttons[0], ActionEvent.ACTION_PERFORMED, "CButton"));
//							}
//						});
					}
					else if (KeyEvent.VK_F2 == keyCode)
					{
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
										m_posPanel.m_botPanel.m_buttons[1], ActionEvent.ACTION_PERFORMED, "CButton"));
							}
						});
					}
					else if (KeyEvent.VK_F3 == keyCode)
					{
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
										m_posPanel.m_botPanel.m_buttons[2], ActionEvent.ACTION_PERFORMED, "CButton"));
							}
						});
					}
					else if (KeyEvent.VK_F4 == keyCode)
					{
						m_posPanel.m_botPanel.requestFocus();
//						EventQueue.invokeLater(new Runnable() {
//							public void run() {
//								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
//										m_posPanel.m_botPanel.m_buttons[3], ActionEvent.ACTION_PERFORMED, "CButton"));
//							}
//						});
					}
					else if (KeyEvent.VK_F5 == keyCode)
					{
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
										m_posPanel.m_botPanel.m_buttons[4], ActionEvent.ACTION_PERFORMED, "CButton"));
							}
						});
					}
					else if (KeyEvent.VK_F6 == keyCode)
					{
						m_posPanel.m_botPanel.requestFocus();
//						EventQueue.invokeLater(new Runnable() {
//							public void run() {
//								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
//										m_posPanel.m_botPanel.m_buttons[5], ActionEvent.ACTION_PERFORMED, "CButton"));
//							}
//						});
					}
					else if (KeyEvent.VK_F7 == keyCode)
					{
						m_posPanel.m_botPanel.requestFocus();
//						EventQueue.invokeLater(new Runnable() {
//							public void run() {
//								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
//										m_posPanel.m_botPanel.m_buttons[6], ActionEvent.ACTION_PERFORMED, "CButton"));
//							}
//						});
					}
					else if (KeyEvent.VK_F8 == keyCode)
					{
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
										m_posPanel.m_botPanel.m_buttons[7], ActionEvent.ACTION_PERFORMED, "CButton"));
							}
						});
					}
					else if (KeyEvent.VK_F9 == keyCode)
					{
						m_posPanel.m_botPanel.requestFocus();
//						EventQueue.invokeLater(new Runnable() {
//							public void run() {
//								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
//										m_posPanel.m_botPanel.m_buttons[8], ActionEvent.ACTION_PERFORMED, "CButton"));
//							}
//						});
					}
					else if (KeyEvent.VK_F10 == keyCode)
					{
						m_posPanel.m_botPanel.requestFocus();
//						EventQueue.invokeLater(new Runnable() {
//							public void run() {
//								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
//										m_posPanel.m_botPanel.m_buttons[9], ActionEvent.ACTION_PERFORMED, "CButton"));
//							}
//						});
					}
					else if (KeyEvent.VK_F11 == keyCode)
					{
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
										m_posPanel.m_botPanel.m_buttons[10], ActionEvent.ACTION_PERFORMED, "CButton"));
							}
						});
					}
					else if (KeyEvent.VK_F12 == keyCode)
					{
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								m_posPanel.m_botPanel.actionPerformed(new ActionEvent (
										m_posPanel.m_botPanel.m_buttons[11], ActionEvent.ACTION_PERFORMED, "CButton"));
							}
						});
					}
				}
			});
		}
	}	
	
	@SuppressWarnings("nls")
	private void findProduct (String query)
	{
		if (query == null)
			query = "";
		if (query.length() > 8)
			query = query.substring(0, 8);
		if (query == null)
			query = "";
		query = query.toUpperCase();
		//	Test Number
		boolean allNumber = true;
		try
		{
			Integer.getInteger(query);
		}
		catch (Exception e)
		{
			allNumber = false;
		}
		String Value = query;
		String Name = query;
		String UPC = (allNumber ? query : null);
		String SKU = (allNumber ? query : null);
		
		String sql = "SELECT p.M_Product_ID, p.Value, p.Name, p.upc, p.sku,pp.priceStd FROM M_productprice pp "
				+ " inner join m_product p on p.m_product_id = pp.m_product_id AND p.IsActive = 'Y' "
				+ " inner join m_pricelist_version pv on pv.m_pricelist_version_id "
				+ " = pp.m_Pricelist_version_ID AND pv.validfrom = (select max (validfrom) from "
				+ " m_pricelist_version where validfrom <= ?::timestamp and IsActive = 'Y'"
				+ " and m_pricelist_version_ID in (select "
				+ " m_pricelist_version_id from m_productprice where m_product_id = "
				+ " pp.M_product_ID) AND M_PriceList_ID = ?) AND pv.M_PriceList_ID = ? "
				+ " WHERE pp.isActive = 'Y' ";
		if (MUNSPOSTerminal.STORETYPE_RetailDutyFree.equals(m_posPanel.m_terminal.getStoreType())
				|| MUNSPOSTerminal.STORETYPE_RetailDutyPaid.equals(m_posPanel.m_terminal.getStoreType()))
		{
			sql += " AND p.ProcurementType  = '" + MProduct.PROCUREMENTTYPE_Merchandising + "'";
		}
		else
			sql += " AND p.ProcurementType  = '" + MProduct.PROCUREMENTTYPE_FoodAndBeverages + "'";
		
		String customWhere = "";
		final String percent = "%";
		
		if (!Util.isEmpty(Value, true))
		{
			customWhere += " UPPER (p.value) like ? ";
			Value = percent + Value + percent;
		}
		if (!Util.isEmpty(Name, true))
		{
			if (customWhere.length() > 0)
				customWhere += " OR ";
			customWhere += " UPPER (p.Name) like ? ";
			Name = percent + Name + percent;
		}
		if (!Util.isEmpty(UPC, true))
		{
			if (customWhere.length() > 0)
				customWhere += " OR ";
			customWhere += " UPPER (p.UPC) like ? ";
			UPC = percent + UPC + percent;
		}
		if (!Util.isEmpty(SKU, true))
		{
			if (customWhere.length() > 0)
				customWhere += " OR ";
			customWhere += "UPPER (p.SKU) like ? ";
			SKU = percent + SKU + percent;
		}
		if (customWhere.length() > 0)
		{
			sql += " AND (" + customWhere + ")";
		}
		
		String refundWhere = "pp.M_Product_ID IN (SELECT M_Product_ID FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?)";
		if (m_posPanel.isOnRefund())
			sql += " AND " + refundWhere;
		
		sql += " group by p.M_Product_id, p.value, p.name, pp.pricestd "
				+ " order by p.Name";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			int idx = 0;
			st = DB.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE, get_TrxName());
			st.setTimestamp(++idx, this.m_session.getDateAcct());
			st.setInt(++idx, this.m_session.getM_PriceList_ID());
			st.setInt(++idx, this.m_session.getM_PriceList_ID());
			if (!Util.isEmpty(Value, true))
			{
				st.setString(++idx, Value.toUpperCase());
			}
			if (!Util.isEmpty(Name, true))
			{
				st.setString(++idx, Name.toUpperCase());
			}
			if (!Util.isEmpty(UPC, true))
			{
				st.setString(++idx, UPC.toUpperCase());
			}
			if (!Util.isEmpty(SKU, true))
			{
				st.setString(++idx, SKU.toUpperCase());
			}
			if (m_posPanel.isOnRefund())
				st.setInt(++idx, m_posPanel.getOriginalTrx().get_ID());
			
			rs = st.executeQuery();
			if (!rs.next() || !rs.last())
			{
				String message = Msg.translate(getCtx (),  "search.product.notfound");
				ADialog.warn(0, this.m_posPanel, message + query);
				m_table.setValueAt(null, m_table.getSelectedRow(), 3);
				m_table.setValueAt(null, m_table.getSelectedRow(), 4);				
			}
			else if (rs.getRow() == 1 || this.m_product != null)
			{
				addItem(rs.getInt(1), rs.getString(3), rs.getBigDecimal(6));
			}
			else	//	more than one
			{
				UNSPOSQueryProduct qt = new UNSPOSQueryProduct(this.m_posPanel);
				rs.beforeFirst();
				qt.setResults(rs);
				if (m_posPanel.isOnRefund())
					qt.setWhere(refundWhere, m_posPanel.getOriginalTrx().get_ID());
				
				qt.setVisible(true);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	

	public boolean saveLine() {
		Object objectPID = m_table.getValueAt(m_table.getSelectedRow(), 0);
		if (objectPID == null)
			return false;
		int productID = (int) objectPID;
		if (productID == 0)
			return false;
		MProduct product = MProduct.get(getCtx(), productID);
		if (product == null)
			return false;
		
		Object upcObj = m_table.getColumn(2).getCellEditor().getCellEditorValue();
		String upc = (String) upcObj;
		if (upc == null || upc.length() < product.getSKU().length())
			upc =product.getSKU();
		if (upc.length() < m_posPanel.m_posConfiguration.getMinBarcodeLength())
		{
			int selected = m_table.getSelectedRow();
			m_table.setValueAt(null,selected, 0);
			m_table.getColumn(2).getCellEditor().getTableCellEditorComponent(m_table, upc, true, selected, 2);
			ADialog.info(m_posPanel.getWindowNo(), this, "Please complete SKU first");
			return true;
		}
		else
			m_table.stopEditor(true);
		
		BigDecimal QtyOrdered = (BigDecimal) m_table.getValueAt(m_table.getSelectedRow(), 4);
		BigDecimal PriceActual = (BigDecimal) m_table.getValueAt(m_table.getSelectedRow(), 5);
		if (QtyOrdered == null || QtyOrdered.signum() == 0)
		{
			QtyOrdered = Env.ONE;
		}
		if (m_posPanel.isOnRefund())
			QtyOrdered = QtyOrdered.abs().negate();
		if ( this.m_posPanel.m_posTrx == null )
		{
			this.m_posPanel.m_posTrx = UNSPOSTrxModel.createTransaction(this.m_session, 
					this.m_posPanel);
			if (null == this.m_posPanel.m_posTrx)
			{
				return false;
			}
			if (m_posPanel.isOnRefund())
			{
				if (m_posPanel.m_posTrx.getDiscount().compareTo(m_posPanel.getOriginalTrx().getDiscount()) != 0)
				{
					m_posPanel.m_posTrx.setDiscount(m_posPanel.getOriginalTrx().getDiscount());
					if (!m_posPanel.m_posTrx.save())
						return false;
				}
			}
		}
		
		boolean found = false;
		for (MUNSPOSTrxLine line : this.m_posPanel.m_posTrx.getLines(false))
		{
			if (line.getM_Product_ID() == product.getM_Product_ID()
					&& line.getBarcode().equals(m_table.getValueAt(m_table.getSelectedRow(), 2)))
			{
				BigDecimal add = Env.ONE;
				if (m_posPanel.isOnRefund())
					add = add.negate();
				line.setQtyOrdered(line.getQtyOrdered().add(add));
				line.setQtyEntered(line.getQtyOrdered().subtract(line.getQtyBonuses()));
				if (!m_posPanel.isOnRefund())
				{
					String _sql = "UPDATE UNS_DiscountTrx SET IsNeedRecalculate = 'Y' WHERE (UNS_POSTrx_ID = ? OR UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?)) AND Name NOT LIKE 'Special%'";
					int ok = DB.executeUpdate(_sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrx_ID()}, false, get_TrxName());
					if (ok == -1)
					{
						return false;
					}
					String sql = "SELECT COUNT(*) FROM UNS_DiscountTrx WHERE UNS_POSTrxLine_ID = ? OR UNS_POSTrx_ID = ?";
					int exists = DB.getSQLValue(get_TrxName(), sql, line.getUNS_POSTrxLine_ID(),line.getUNS_POSTrx_ID());
					if (exists > 0)
					{
						if (ADialog.ask(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, "This action will delete the discount that was previously defined, Continue this process?"))
						{
							sql = "DELETE FROM UNS_DiscountTrx WHERE UNS_POStrx_ID = ? OR UNS_POStrxLine_ID = ?";
							int executeOK = DB.executeUpdate(sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrxLine_ID()}, 
									false, get_TrxName());
							if (executeOK == -1)
							{
								return false;
							}
							line.setDiscountAmt(Env.ZERO);
							line.getParent().setDiscountAmt(Env.ZERO);
							if (!line.getParent().save())
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
				}
				
				if (!line.save())
					return false;
				found = true;
				this.m_posdetail_ID = line.getUNS_POSTrxLine_ID();
				break;
			}
		}
		
		if (!found)
		{
			MUNSPOSTrxLine line = this.m_posPanel.m_posTrx.createLine(
					product, QtyOrdered, PriceActual);
			if (line == null)
				return false;

			String barcode = (String) m_table.getValueAt(m_table.getSelectedRow(), 2);
			line.setBarcode(barcode);
			
			if (!m_posPanel.isOnRefund())
			{
				String _sql = "UPDATE UNS_DiscountTrx SET IsNeedRecalculate = 'Y' WHERE (UNS_POSTrx_ID = ? OR UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?)) AND Name NOT LIKE 'Special%'";
				int ok = DB.executeUpdate(_sql, new Object[]{line.getUNS_POSTrx_ID(), line.getUNS_POSTrx_ID()}, false, get_TrxName());
				if (ok == -1)
				{
					return false;
				}
				String sql = "SELECT COUNT(*) FROM UNS_DiscountTrx WHERE UNS_POSTrx_ID = ?";
				int exists = DB.getSQLValue(get_TrxName(), sql, line.getUNS_POSTrx_ID());
				if (exists > 0)
				{
					if (ADialog.ask(m_posPanel.getWindowNo(), UNSSubCurrentLine.this, "This action will delete the discount that was previously defined, Continue this process?"))
					{
						sql = "DELETE FROM UNS_DiscountTrx WHERE UNS_POStrx_ID = ?";
						int executeOK = DB.executeUpdate(sql, new Object[]{line.getUNS_POSTrx_ID()}, 
								false, get_TrxName());
						if (executeOK == -1)
						{
							return false;
						}
						line.getParent().setDiscountAmt(Env.ZERO);
						if (!line.getParent().save())
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
			}
			
			if (!line.save())
				return false;
			this.m_posdetail_ID = line.getUNS_POSTrxLine_ID();
		}
		
		try {
			DB.commit(true, get_TrxName());
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateTable(this.m_posPanel.m_posTrx);
		return true;
	} //	saveLine
	
	public MProduct getProduct()
	{
		return this.m_product;
	}
	
	public void updateTable (UNSPOSTrxModel model)
	{
		int UNS_POSTrx_ID = 0;
		if (model != null)
			UNS_POSTrx_ID = model.getUNS_POSTrx_ID();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (this.m_sql, get_TrxName ());
			pstmt.setInt (1, UNS_POSTrx_ID);
			rs = pstmt.executeQuery ();
			this.m_table.loadTable(rs);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, this.m_sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		if (m_table.getRowCount() < 12)
			m_table.setRowCount(12);
		else 
			m_table.setRowCount(m_table.getRowCount()+1);
		
		for (int i=0; i<m_table.getRowCount(); i++)
		{
			if (m_table.getValueAt(i, 0) == null && m_table.getSelectedRow() == -1 && m_table.isEnabled())
			{
				m_table.getSelectionModel().setSelectionInterval(i, i);
				break;
			}
		}
		
		if (this.m_posPanel.m_rightPanel != null)
		{
			this.m_posPanel.m_rightPanel.updateInfo();
		}
		if (model != null)
		{
			m_posPanel.m_form.updateCustomerDisplay(model.get_ID(), model.getGrandTotal());
		}

		if (m_posPanel.m_botPanel != null)
		{
			m_posPanel.m_botPanel.enableComponent(model);
		}
		if (UNS_POSTrx_ID > 0)
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					for (int i=0; i<m_table.getRowCount(); i++)
					{
						if (m_table.getValueAt(i, 0) == null)
						{
							m_table.getSelectionModel().setSelectionInterval(i, i);
							m_table.editCellAt(i, 2);
							break;
						}
					}
				}
			});
		}
	}	//	updateTable
	
	public void onPageUp (BigDecimal qty)
	{
		if ( this.m_posdetail_ID > 0 )
		{
			MUNSPOSTrxLine line = new MUNSPOSTrxLine(getCtx (), this.m_posdetail_ID, get_TrxName ());
			if (m_posPanel.isOnRefund())
				qty = qty.abs().negate();
			line.setQtyOrdered(line.getQtyOrdered().add(qty));
			line.setQtyEntered(line.getQtyOrdered().subtract(line.getQtyBonuses()));
			if (!line.save())
			{
				String error = CLogger.retrieveErrorString("Unknown-Error");
				ADialog.error(this.m_posPanel.getWindowNo(), this, error);
				return;
			}
			this.m_posPanel.updateInfo();
		}
	}
	
	public void onPageDown (BigDecimal qty)
	{
		if ( this.m_posdetail_ID > 0 )
		{
			MUNSPOSTrxLine line = new MUNSPOSTrxLine(getCtx (), this.m_posdetail_ID, get_TrxName ());
			if (m_posPanel.isOnRefund())
				qty = qty.abs().negate();
			line.setQtyOrdered(line.getQtyOrdered().subtract(qty));
			line.setQtyEntered(line.getQtyOrdered().subtract(line.getQtyBonuses()));
			if (!line.save())
			{
				String error = CLogger.retrieveErrorString("Unknown-Error");
				ADialog.error(this.m_posPanel.getWindowNo(), this, error);
				return;
			}
			this.m_posPanel.updateInfo();	
		}
	}
	
	public void onUp ()
	{
		int rows = this.m_table.getRowCount();
		if (rows == 0) 
			return;
		int row = this.m_table.getSelectedRow();
		row--;
		if (row < 0)
			row = 0;
		this.m_table.getSelectionModel().setSelectionInterval(row, row);
		this.m_table.scrollRectToVisible(this.m_table.getCellRect(row, 1, true));
	}
	
	public void onDown ()
	{
		int rows = this.m_table.getRowCount();
		if (rows == 0)
			return;
		int row = this.m_table.getSelectedRow();
		row++;
		if (row >= rows)
			row = rows - 1;
		this.m_table.getSelectionModel().setSelectionInterval(row, row);
		this.m_table.scrollRectToVisible(this.m_table.getCellRect(row, 1, true)); 
	}
	
	public void onCancel ()
	{
		int rows = this.m_table.getRowCount();
		if (rows != 0)
		{
			int row = this.m_table.getSelectedRow();
			if (row != -1)
			{
				if (this.m_posPanel.m_posTrx != null 
						&& !this.m_posPanel.m_posTrx.deleteLine(this.m_table.getSelectedRowKey()))
				{
					String error = CLogger.retrieveErrorString("Unknown Error");
					ADialog.error(this.m_posPanel.getWindowNo(), this, error);
				}
				
				this.m_posdetail_ID = 0;
				m_posPanel.updateInfo();
			}
		}
	}
	
	public void addItem (int id, String name, BigDecimal price)
	{
		if (id == 0)
		{
			return;
		}
		
		m_table.setValueAt(id, m_table.getSelectedRow(), 0);
		m_table.setValueAt(name, m_table.getSelectedRow(), 3);
		m_table.setValueAt(price, m_table.getSelectedRow(), 5);
		if (!saveLine())
		{
			m_table.setValueAt(null, m_table.getSelectedRow(), 0);
			m_table.setValueAt(null, m_table.getSelectedRow(), 3);
			m_table.setValueAt(null, m_table.getSelectedRow(), 5);
			String error = CLogger.retrieveErrorString("Unknown-Error");
			ADialog.error(this.m_posPanel.getWindowNo(), this, error);
		}
	}
	
	public void loadRefundDisplay ()
	{
		m_table.getTableHeader().setBackground(new Color(247,150,70));
		m_table.setEvenColor(new Color(253, 239, 233));
		m_table.setOddColor(new Color(252, 221, 207));
		m_table.repaint();
	}
	
	public void loadSaleDisplay ()
	{
		this.m_table.getTableHeader().setBackground(new Color(155,187,89));
		m_table.setOddColor(UNSPOSTable.DEFAULT_ODD_COLOR);
		m_table.setEvenColor(UNSPOSTable.DEFAULT_EVEN_COLOR);
		m_table.setSelectedColor(UNSPOSTable.DEFAULT_SELECTED_COLOR);
		m_table.repaint();
	}
}

class MyTable extends UNSPOSTable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4913855286262498696L;

	public MyTable ()
	{
		super ();
		setAutoResizeMode(AUTO_RESIZE_LAST_COLUMN);
	}
	
	@Override
	public void growScrollbars() 
	{
		Container p = getParent();
		if (p instanceof JViewport) 
		{
			Container gp = p.getParent();
			if (gp instanceof JScrollPane) 
			{
				JScrollPane scrollPane = (JScrollPane) gp;             
				scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(20,0));
				scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0,20));
			}
		}
	}
	
	@Override
	public boolean isCellEditable (int row, int col)
	{
		boolean editable = true;
		if (col == 2)
		{
			editable = getValueAt(row, 0) == null;
		}
		else if (col == 4)
		{
			editable = getValueAt(row, 2) != null;
		}
		else
		{
			editable =  super.isCellEditable(row, col);
		}
		
		return editable;
	}
}
