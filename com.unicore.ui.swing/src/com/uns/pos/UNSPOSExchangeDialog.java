/**
 * 
 */
package com.uns.pos;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import net.miginfocom.swing.MigLayout;

import org.compiere.apps.ADialog;
import org.compiere.apps.AEnv;
import org.compiere.grid.ed.VNumber;
import org.compiere.model.MConversionRate;
import org.compiere.swing.CButton;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CDialog;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import com.unicore.model.MUNSPaymentTrx;

/**
 * @author nurse
 *
 */
public class UNSPOSExchangeDialog extends CDialog 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9025574524168863587L;
	private UNSPOSPanel m_posPanel;
	private CComboBox f_currencyFrom;
	private CComboBox f_currencyTo;
	private VNumber f_amtFrom;
	private VNumber f_amtTo;
	private CButton b_ok;
	private CButton b_cancel;

	/**
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public UNSPOSExchangeDialog(UNSPOSPanel panel, boolean modal) throws HeadlessException 
	{
		super(AEnv.getFrame(panel), modal);
		m_posPanel = panel;
		setTitle("Exchange");
		setPreferredSize(new Dimension(400,250));
		setMinimumSize(getPreferredSize());
		setResizable(false);
		setLocationByPlatform(true);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
		int y = (screenSize.height - getHeight()) / 2;
		setLocation(x, y);
		init();
	}
	
	private void init ()
	{
		f_currencyFrom = new CComboBox(loadCurrency());
		f_currencyTo = new CComboBox(loadCurrency());
		f_amtFrom = new VNumber();
		f_amtFrom.setDisplayType(DisplayType.Amount);
		f_amtTo = new VNumber();
		f_amtTo.setDisplayType(DisplayType.Amount);
		f_amtTo.setReadWrite(false);
		b_ok = new CButton("OK");
		b_cancel = new CButton("CANCEL");
		b_ok.addActionListener(this);
		b_cancel.addActionListener(this);
		f_amtFrom.addActionListener(this);
		
		CPanel panel = new CPanel(new MigLayout("fill","[][fill]",""));
		CLabel lcurrFrom = new CLabel("Currency From");
		CLabel lcurrTo = new CLabel("Currency To");
		CLabel lAmtFrom = new CLabel("Amount From");
		CLabel lAmtTo = new CLabel("Amount To");
		
		panel.add(lcurrFrom, "");
		panel.add(f_currencyFrom, "wrap, pushx, pushy, spanx, flowx, growx, growy, flowy");
		panel.add(lcurrTo, "");
		panel.add(f_currencyTo, "wrap, pushx, pushy, spanx, flowx, growx, growy, flowy");
		panel.add(lAmtFrom, "");
		panel.add(f_amtFrom, "wrap, pushx, pushy, spanx, flowx, growx, growy, flowy");
		panel.add(lAmtTo, "");
		panel.add(f_amtTo, "wrap, pushx, pushy, spanx, flowx, growx, growy, flowy");
		panel.add(new CLabel());
		panel.add(b_ok, "split 2");
		panel.add(b_cancel, "wrap, ");
		
		getContentPane().add(panel);
	}
	
	private Vector<KeyNamePair> loadCurrency ()
	{
		Vector<KeyNamePair> retVal = new Vector<>();
		String sql = "SELECT C_Currency_ID, (SELECT Description FROM C_Currency WHERE C_Currency_ID = "
				+ " UNS_SessionCashAccount.C_Currency_ID), IsUsedForChange FROM UNS_SessionCashAccount "
				+ "WHERE UNS_POS_Session_ID = ? AND IsActive = ? ";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_posPanel.get_TrxName());
			st.setInt(1, m_posPanel.m_session.getUNS_POS_Session_ID());
			st.setString(2, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				KeyNamePair curr = new KeyNamePair(rs.getInt(1), rs.getString(2));
				retVal.add(curr);
			}
		}
		catch (SQLException ex)
		{
			ADialog.error(m_posPanel.getWindowNo(), this, ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource().equals(b_ok))
		{
			UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
			confirm.setVisible(true);
			if (!confirm.isApproved())
			{
				return;
			}
			KeyNamePair from = (KeyNamePair) f_currencyFrom.getSelectedItem();
			if (from == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSExchangeDialog.this, "Mandatory field Currency From");
				return;
			}
			KeyNamePair to = (KeyNamePair) f_currencyTo.getSelectedItem();
			if (to == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSExchangeDialog.this, "Mandatory field Currency To");
				return;
			}
			
			int currencyFromID = from.getKey();
			int currencyToID = to.getKey();
			BigDecimal amtfrom = (BigDecimal) f_amtFrom.getValue();
			BigDecimal amtTo = (BigDecimal) f_amtTo.getValue();
			
			try
			{	
				MUNSPaymentTrx trxFrom = new MUNSPaymentTrx(m_posPanel.getCtx(), 0, m_posPanel.get_TrxName());
				trxFrom.setAD_Org_ID(m_posPanel.m_session.getAD_Org_ID());
				trxFrom.setAmount(amtfrom);
				trxFrom.setC_Currency_ID(currencyFromID);
				trxFrom.setConvertedAmt(amtfrom.negate());
				trxFrom.setIsReceipt(false);
				trxFrom.setIsReconciled(true);
				trxFrom.setPaymentMethod(MUNSPaymentTrx.PAYMENTMETHOD_Cash);
				trxFrom.setTrxType(MUNSPaymentTrx.TRXTYPE_ExchangeOut);
				trxFrom.setProcessed(true);
				trxFrom.setUNS_POS_Session_ID(m_posPanel.m_session.get_ID());
				trxFrom.saveEx();
				trxFrom.processExchange();
				
				MUNSPaymentTrx trxTo = new MUNSPaymentTrx(m_posPanel.getCtx(), 0, m_posPanel.get_TrxName());
				trxTo.setAD_Org_ID(m_posPanel.m_session.getAD_Org_ID());
				trxTo.setAmount(amtTo);
				trxTo.setC_Currency_ID(currencyToID);
				trxTo.setConvertedAmt(amtTo);
				trxTo.setIsReceipt(true);
				trxTo.setIsReconciled(true);
				trxTo.setPaymentMethod(MUNSPaymentTrx.PAYMENTMETHOD_Cash);
				trxTo.setTrxType(MUNSPaymentTrx.TRXTYPE_ExchangeOut);
				trxTo.setUNS_POS_Session_ID(m_posPanel.m_session.get_ID());
				trxTo.setProcessed(true);
				trxTo.saveEx();
				trxTo.processExchange();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				try 
				{
					DB.rollback(true, m_posPanel.get_TrxName());
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				return;
			}
			
			try 
			{
				DB.commit(true, m_posPanel.get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
			}
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			dispose();
		}
		else if (e.getSource().equals(b_cancel))
		{
			dispose();
		}
		else if (e.getSource().equals(f_amtFrom))
		{
			KeyNamePair from = (KeyNamePair) f_currencyFrom.getSelectedItem();
			if (from == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSExchangeDialog.this, "Mandatory field Currency From");
				return;
			}
			KeyNamePair to = (KeyNamePair) f_currencyTo.getSelectedItem();
			if (to == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSExchangeDialog.this, "Mandatory field Currency To");
				return;
			}
			
			int currencyFromID = from.getKey();
			int currencyToID = to.getKey();
			BigDecimal amtFrom = (BigDecimal) f_amtFrom.getValue();
			BigDecimal amtTo = MConversionRate.convert(m_posPanel.getCtx(), amtFrom, 
					currencyFromID, currencyToID, Env.getAD_Client_ID(Env.getCtx()), 
					Env.getAD_Org_ID(Env.getCtx()));
			f_amtTo.setValue(amtTo);
		}
	}
}
