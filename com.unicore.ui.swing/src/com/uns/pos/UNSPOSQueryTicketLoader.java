/**
 * 
 */
package com.uns.pos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.apps.ADialog;
import org.compiere.model.MColumn;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;

/**
 * @author nurse
 *
 */
public class UNSPOSQueryTicketLoader 
{

	private Timestamp m_from = null;
	private Timestamp m_to = null;
	private String m_docNo = null;
	private String m_trxNo = null;
	private String m_locServerDBUrl = MSysConfig.getValue("LOCATION_DB_URL","jdbc:postgresql://192.168.1.69:5432/PlazaBali");
	private String m_locServerDBUser = MSysConfig.getValue("LOCATION_DB_USER", "adempiere");
	private String m_locServerDBPass = MSysConfig.getValue("LOCATION_DB_PASS", "adempiere");
	public String m_uu = null;
	
	/**
	 * 
	 */
	public UNSPOSQueryTicketLoader(Timestamp from, Timestamp to, String documentNo, 
			String trxNo, String uu) 
	{
		m_from = from;
		m_to = to;
		m_docNo = documentNo;
		m_trxNo = trxNo;
		if (Util.isEmpty(m_docNo, true))
			m_docNo = null;
		if (Util.isEmpty(m_trxNo, true))
			m_trxNo = null;
		m_uu = uu;
	}
	
	private String buildQuery (MTable table)
	{
		StringBuilder docBuild = new StringBuilder("SELECT ");
		MColumn[] cols = table.getColumns(false);
		int start = 0;
		for (int i=0; i<cols.length; i++)
		{
			if (cols[i].isKey())
			{
				if (i == 0)
					start++;
				continue;
			}
			if (i> start && i<cols.length)
				docBuild.append(",");
			String tableName = cols[i].getReferenceTableName();
			
			if (cols[i].getColumnName().endsWith("_ID") && tableName != null)
			{
				docBuild.append("(SELECT x.").append(tableName).append("_UU FROM ").append(tableName)
				.append(" x WHERE x.").append(tableName).append("_ID = ").append(table.getTableName())
				.append(".").append(cols[i].getColumnName()).append(") AS ")
				.append(cols[i].getColumnName().replace("ID", "UU"));
			}
			else
				docBuild.append(cols[i].getColumnName());
		}
		docBuild.append(" FROM ").append(table.getTableName());
		String sql = docBuild.toString();
		return sql;
	}
	
	private PO parseToPO (ResultSet rs, MTable table, String trxName) throws SQLException, AdempiereException
	{
		String uuColName = PO.getUUIDColumnName(table.getTableName());
		int id = 0;
		if (uuColName != null)
		{
			String uu = rs.getString(uuColName);
			if (uu != null)
			{
				StringBuilder sb = new StringBuilder("SELECT ");
				sb.append(table.getTableName()).append("_ID FROM ").append(table.getTableName()).
				append(" WHERE ").append(uuColName).append(" = ?");
				String sql = sb.toString();
				id = DB.getSQLValue(trxName, sql, uu);
				if (id == -1)
					id = 0;
			}
		}
		
		PO model = table.getPO(id, trxName);
		MColumn[] cols = table.getColumns(false);
		for (int i=0; i<cols.length; i++)
		{
			if (cols[i].isKey())
				continue;
			if (model.get_ID() > 0 && !cols[i].isUpdateable())
				continue;
				
			String refTableName = cols[i].getReferenceTableName();
			Object value = null;
			if (cols[i].getColumnName().equals("Created") || cols[i].getColumnName().equals("CreatedBy")
					|| cols[i].getColumnName().equals("Updated") ||cols[i].getColumnName().equals("UpdatedBy"))
				continue;
			if (cols[i].getColumnName().endsWith("_ID") && refTableName != null)
			{
				String colName = cols[i].getColumnName().replace("ID", "UU");
				value = rs.getString(colName);
				if (value == null)
					continue;
				
				StringBuilder build = new StringBuilder("SELECT ");
				build.append(refTableName).append("_ID FROM ").append(refTableName)
				.append(" WHERE ").append(refTableName).append("_UU").append(" = ?");
				String sql2 = build.toString();
				int valInt = DB.getSQLValue(trxName, sql2, value);
				if (valInt <= 0)
				{
					String where = refTableName+ "_UU = '" + value + "'";
					List<PO> data = getData(refTableName, trxName, where);
					if (data.size() > 0)
						value = data.get(0).get_ID();
				}
				else
					value = valInt;
			}
			else
				value = rs.getObject(cols[i].getColumnName());
			
			if (value == null)
				continue;
			model.set_ValueOfColumn(cols[i].getColumnName(), value);
		}

		model.saveReplica(true);
		return model;
	}
	
	public List<PO> getData () 
	{
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			ADialog.error(0, null, "Can't load data from location server " + e1.getMessage());
			return null;
		}
		StringBuilder docBuild = new StringBuilder("DocStatus IN ('CO','CL') AND EXISTS (SELECT ");
		docBuild.append("UNS_POS_SESSION_ID FROM UNS_POS_Session WHERE UNS_POS_Session_ID = ")
		.append("UNS_POSTrx.UNS_POS_Session_ID AND DocStatus IN ('CO','CL')) AND IsPaid = 'Y'");
		if (m_from != null && m_to != null)
			docBuild.append(" AND ").append(UNSPOSTrxModel.COLUMNNAME_DateDoc).append(" BETWEEN '").
			append(m_from).append("' AND '").append(m_to).append("'");
		else if (m_from != null)
			docBuild.append(" AND ").append(UNSPOSTrxModel.COLUMNNAME_DateDoc).append(" >= '").append(m_from).append("'");
		else if (m_to != null)
			docBuild.append(" AND ").append(UNSPOSTrxModel.COLUMNNAME_DateDoc).append(" <= '").append(m_to).append("'");
		
		if (m_docNo != null)
		{
			docBuild.append(" AND ").append(UNSPOSTrxModel.COLUMNNAME_DocumentNo).append(" = '").append(m_docNo).append("'");
		}
		if (m_trxNo != null)
		{
			docBuild.append(" AND ").append(UNSPOSTrxModel.COLUMNNAME_TrxNo).append(" = '").append(m_trxNo).append("'");
		}
		if (m_uu != null)
			docBuild.append(" AND ").append(UNSPOSTrxModel.COLUMNNAME_Reference_ID).
			append(" = (").append("SELECT UNS_POSTrx_ID FROM UNS_POSTrx WHERE UNS_POSTrx_UU = '").
			append(m_uu).append("')");
		
		String where = docBuild.toString();
		String trxName = Trx.createTrxName("QueryData");
		List<PO> result = null;
		try
		{
			 result = getData("UNS_POSTrx", trxName, where);	
			 for (int i=0; i<result.size(); i++)
			 {
				 String childw1 = "UNS_POSTrx_ID = (SELECT UNS_POSTrx_ID FROM UNS_POSTrx "
				 		+ " WHERE UNS_POSTrx_UU = '" + result.get(i).get_ValueAsString("UNS_POSTrx_UU") + "')";
				 getData("UNS_POSTrxLine", trxName, childw1);
				 List<PO> posPayment = getData("UNS_POSPayment", trxName, childw1);
				 String childw2 = "UNS_POSPayment_ID = (SELECT UNS_POSPayment_ID FROM UNS_POSPayment "
				 		+ " WHERE UNS_POSPayment_UU ='" + posPayment.get(0).
				 		get_ValueAsString("UNS_POSPayment_UU")+ "')";
				 List<PO> payTrx = getData("UNS_PaymentTrx", trxName, childw2);
				 for (int x=0; x<payTrx.size(); x++)
				 {
					String childw3 =  "UNS_PaymentTrx_ID = (SELECT UNS_PaymentTrx_ID FROM "
					 		+ "UNS_PaymentTrx WHERE UNS_PaymentTrx_UU ='" 
							+ payTrx.get(x).get_ValueAsString("UNS_PaymentTrx_UU") + "')";
					getData("UNS_CardTrxDetail", trxName, childw3);
				 }
			 }
			 DB.commit(true, trxName);
		}
		catch (AdempiereException | IllegalStateException | SQLException ex)
		{
			try 
			{
				DB.rollback(true, trxName);
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			ADialog.error(0, null, "Can't load data from location server " + ex.getMessage());
		}
		
		return result;
	}

	public List<PO> getData (String tableName, String trxName, String where) throws AdempiereException
	{
		List<PO> list = new ArrayList<>();
		MTable table = MTable.get(Env.getCtx(), tableName);
		StringBuilder docBuild = new StringBuilder(buildQuery(table));
		if (where != null)
			docBuild.append(" WHERE ").append(where);
		
		String sql = docBuild.toString();
		String url = m_locServerDBUrl;
		String user = m_locServerDBUser;
		String pass = m_locServerDBPass;
		Properties prop = new Properties();
		prop.put("user", user);
		prop.put("password", pass);
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try 
		{
			connection = DriverManager.getConnection(url, prop);
			st = connection.prepareStatement(sql);
			rs = st.executeQuery();
			while (rs.next())
			{
				PO po = parseToPO(rs, table, trxName);
				list.add(po);
			}
		} 
		catch (SQLException | AdempiereException e) 
		{
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			try 
			{
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				if (connection != null)
					connection.close();
			} 	
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		
		return list;
	}
}
