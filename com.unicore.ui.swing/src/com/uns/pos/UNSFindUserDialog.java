/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import org.compiere.apps.AEnv;
import org.compiere.util.DB;
import org.compiere.util.Env;

import net.miginfocom.swing.MigLayout;

/**
 * @author nurse
 *
 */
public class UNSFindUserDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 51008625984973127L;
	private JTextField f_search;
	private JButton b_search;
	private JButton b_select;
	private JButton b_cancel;
	private UNSPOSTable m_table;
	private JPanel m_panel;
	private UNSPOSPanel m_parent;
	
	public UNSFindUserDialog (UNSPOSPanel parent, boolean modal)
	{
		super(AEnv.getFrame(parent), modal);
		m_parent = parent;
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setMinimumSize(new Dimension(400, 400));
        setPreferredSize(new Dimension(400, 400));
		init();
	}
	
	private void init ()
	{
		m_panel = new JPanel(new MigLayout("", "[80%, fill][20%, fill]",""));
		m_panel.setBackground(new Color(243, 250, 236));
		m_panel.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
		f_search = new JTextField();
		f_search.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
            	if(evt.getKeyCode() == 10)
            		search();
            	else if (evt.getKeyCode() == KeyEvent.VK_DOWN)
            		m_table.requestFocus();
            }
        });
		b_search = new JButton("Find");
		b_select = new JButton("Select");
		b_cancel = new JButton("Cancel");
		DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new String[]{"Partner ID", "Search Key","Name", "Real Name"});
		m_table = new UNSPOSTable();
		m_table.setModel(model);
		m_table.setFillsViewportHeight(true);
		m_table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		m_table.setRowHeight(30);
		m_table.setFocusable(true);
		m_table.requestFocus();
		m_table.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) 
			{
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_LEFT)
				{
					m_table.clearSelection();
					f_search.requestFocus();
				}
				if (arg0.getKeyCode() != KeyEvent.VK_RIGHT)
					return;
				int selected = m_table.getSelectedRow();
				if (selected == -1)
					return;
				select();
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		m_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked (MouseEvent e) {
				if (e.getClickCount() == 2) {
					select();
				}
			}
		});
		JScrollPane scroll = new JScrollPane(m_table);
		m_panel.add(f_search, "");
		m_panel.add(b_search, "wrap");
		m_panel.add(scroll, "spanx, wrap, w 100%");
		m_panel.add(b_select, "spanx, split 2, right");
		m_panel.add(b_cancel, "wrap, right");
		getContentPane().add(m_panel);
		b_cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		b_select.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				select();
			}
		});
		
		b_search.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				search();
			}
		});
		
		f_search.requestFocus();
		pack();
	}
	
	public JTextField getTxtSearchValue () {
		return f_search;
	}
	
	public void search ()
	{
		String sql = "SELECT AD_User_ID, Value, Name, RealName FROM AD_User WHERE "
				+ " (UPPER (Value) LIKE ? OR UPPER (Name) LIKE ? OR UPPER (RealName) LIKE ?) "
				+ " AND IsActive = ? AND AD_Client_ID = ? AND EXISTS (SELECT C_BPartner_ID "
				+ " FROM C_BPartner WHERE C_BPartner_ID = AD_User.C_BPartner_ID AND "
				+ " C_BPartner.IsSalesRep = 'Y')";
		String findValue = f_search.getText();
		if (findValue == null)
			findValue = "";
		if (!findValue.startsWith("%"))
			findValue = "%"+findValue;
		if (!findValue.endsWith("%"))
			findValue += "%";
		PreparedStatement st;
		ResultSet rs;
		try
		{
			st = DB.prepareStatement(sql, null);
			st.setString(1, findValue.toUpperCase());
			st.setString(2, findValue.toUpperCase());
			st.setString(3, findValue.toUpperCase());
			st.setString(4, "Y");
			st.setInt(5, Env.getAD_Client_ID(Env.getCtx()));
			rs = st.executeQuery();
			DefaultTableModel model = ((DefaultTableModel) m_table.getModel());
			model.setRowCount(0);
			int row = m_table.getRowCount();
			while (rs.next()) {
				model.setRowCount(row+1);
				int id = rs.getInt(1);
				String key = rs.getString(2);
				String name = rs.getString(3);
				String rName = rs.getString(4);
				m_table.setValueAt(id, row, 0);
				m_table.setValueAt(key, row, 1);
				m_table.setValueAt(name, row, 2);
				m_table.setValueAt(rName, row, 3);
				row++;
			}
			if (row == 1) {
				--row;
				m_table.getSelectionModel().setSelectionInterval(row, row);
				select();
			}
			st.close();
			rs.close();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void select ()
	{
		int selected = m_table.getSelectedRow();
		if (selected <= -1)
			return;
		m_parent.m_infoPanel.setSales((int) m_table.getValueAt(selected, 0), 
				(String)m_table.getValueAt(selected, 3));		
		dispose();
	}
	
	public int getRowCount() {
		int row = 0;
		if (m_table != null)
			row = m_table.getRowCount();
		return row;
	}
}
