package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.logging.Level;

import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MBPartnerInfo;
import org.compiere.pos.QueryBPartner;
import org.compiere.swing.CButton;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.swing.CTextField;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class UNSPOSQueryBPartner extends UNSPOSQuery
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4500636033505855695L;

	/**
	 * 	Constructor
	 */
	public UNSPOSQueryBPartner (UNSPOSPanel posPanel)
	{
		super(posPanel);
	}	//	UNSPOSQueryBPartner
	
	private UNSPOSTextField		f_value;
	private UNSPOSTextField		f_name;
	private UNSPOSTextField		f_contact;
	private UNSPOSTextField		f_email;
	private UNSPOSTextField		f_phone;
	private CTextField		f_city;

	private int				m_C_BPartner_ID;
	private CButton f_refresh;
	private CButton f_ok;
	private CButton f_cancel;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(QueryBPartner.class);
	
	
	/**	Table Column Layout Info			*/
	@SuppressWarnings("nls")
	private static ColumnInfo[] s_layout = new ColumnInfo[] 
	{
		new ColumnInfo(" ", "C_BPartner_ID", IDColumn.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Value"), "Value", String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name", String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Email"), "Email", String.class), 
		new ColumnInfo(Msg.translate(Env.getCtx(), "Phone"), "Phone", String.class), 
		new ColumnInfo(Msg.translate(Env.getCtx(), "Postal"), "Postal", String.class), 
		new ColumnInfo(Msg.translate(Env.getCtx(), "City"), "City", String.class) 
	};
	/**	From Clause							*/
	@SuppressWarnings("nls")
	private static String s_sqlFrom = "RV_BPartner";
	/** Where Clause						*/
	@SuppressWarnings("nls")
	private static String s_sqlWhere = "IsActive='Y'"; 

	/**
	 * 	Set up Panel
	 */
	@SuppressWarnings("nls")
	protected void init()
	{
		CPanel panel = new CPanel();
		panel.setBackground(new Color(16, 106, 166));
		panel.setLayout(new MigLayout("fill","[70%][30%]",""));
		getContentPane().add(panel);
		//	North
		this.northPanel = new CPanel(new MigLayout("fill","", "[50][50][]"));
		panel.add (this.northPanel, "north");
		this.northPanel.setBorder(new TitledBorder(Msg.getMsg(getCtx (), "Query")));
		
		CLabel lvalue = new CLabel(Msg.translate(getCtx (), "Value"));
		this.northPanel.add (lvalue, " growy");
		this.f_value = new UNSPOSTextField("", this.p_posPanel, 0);
		lvalue.setLabelFor(this.f_value);
		this.northPanel.add(this.f_value, "h 30, w 200");
		this.f_value.addActionListener(this);
		
		//
		CLabel lcontact = new CLabel(Msg.translate(getCtx (), "Contact"));
		this.northPanel.add (lcontact, " growy");
		this.f_contact = new UNSPOSTextField("", this.p_posPanel, 0);
		lcontact.setLabelFor(this.f_contact);
		this.northPanel.add(this.f_contact, "h 30, w 200");
		this.f_contact.addActionListener(this);
		
		//
		CLabel lphone = new CLabel(Msg.translate(getCtx (), "Phone"));
		this.northPanel.add (lphone, " growy");
		this.f_phone = new UNSPOSTextField("", this.p_posPanel, 0);
		lphone.setLabelFor(this.f_phone);
		this.northPanel.add(this.f_phone, "h 30, w 200, wrap");
		this.f_phone.addActionListener(this);
		
		//
		CLabel lname = new CLabel(Msg.translate(getCtx (), "Name"));
		this.northPanel.add (lname, " growy");
		this.f_name = new UNSPOSTextField("", this.p_posPanel, 0);
		lname.setLabelFor(this.f_name);
		this.northPanel.add(this.f_name, "h 30, w 200");
		this.f_name.addActionListener(this);
		//
		CLabel lemail = new CLabel(Msg.translate(getCtx (), "Email"));
		this.northPanel.add (lemail, " growy");
		this.f_email = new UNSPOSTextField("", this.p_posPanel, 0);
		lemail.setLabelFor(this.f_email);
		this.northPanel.add(this.f_email, "h 30, w 200");
		this.f_email.addActionListener(this);
		//
		CLabel lcity = new CLabel(Msg.translate(getCtx (), "City"));
		this.northPanel.add (lcity, " growy");
		this.f_city = new CTextField(10);
		lcity.setLabelFor(this.f_city);
		this.northPanel.add(this.f_city, "h 30, w 200");
		this.f_city.addActionListener(this);
		//
		
		this.f_refresh = createButtonAction("Refresh", KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		this.northPanel.add(this.f_refresh, "w 50!, h 50!, wrap, alignx trailing");
		this.f_refresh.setToolTipText("Refresh | Shortcut F5");
		
		this.f_up = createButtonAction("Previous", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		this.northPanel.add(this.f_up, "w 50!, h 50!, span, split 4");
		this.f_up.setToolTipText("Previous | Shortcut Up");
		this.f_down = createButtonAction("Next", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		this.northPanel.add(this.f_down, "w 50!, h 50!");
		this.f_down.setToolTipText("Next | Shortcut Down");
		
		this.f_ok = createButtonAction("Ok", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, Event.CTRL_MASK));
		this.northPanel.add(this.f_ok, "w 50!, h 50!");
		this.f_ok.setToolTipText("Select/OK | Shortcut Ctrl Enter");
		
		this.f_cancel = createButtonAction("Cancel", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		this.northPanel.add(this.f_cancel, "w 50!, h 50!");
		this.f_up.setToolTipText("Close/cancel | Shortcut Esc");
		
		//	Center
		this.m_table = new UNSPOSTable();
		this.m_table.prepareTable (s_layout, s_sqlFrom, 
			s_sqlWhere, false, "RV_BPartner");
		this.m_table.addMouseListener(this);
		this.m_table.getSelectionModel().addListSelectionListener(this);
		enableButtons();
		this.centerScroll = new CScrollPane(this.m_table);
		panel.add (this.centerScroll, "growx, growy");
		this.m_table.growScrollbars();
		UNSPOSKeyboardPanel kbp = new UNSPOSKeyboardPanel(this.p_posPanel);
		kbp.init();
		panel.add(kbp, "spanx, wrap, growx, growy,flowx, flowy,");
		setMinimumSize(new Dimension(900,600));
		setResizable(true);
		
		panel.setPreferredSize(new Dimension(800,600));
		this.f_value.requestFocus();
	}	//	init
	
	/**
	 * 	Action Listener
	 *	@param e event
	 */
	@SuppressWarnings("nls")
	public void actionPerformed (ActionEvent e)
	{
		if (log.isLoggable(Level.INFO)) log.info(e.getActionCommand());
		if ("Refresh".equals(e.getActionCommand())
			|| e.getSource() == this.f_value // || e.getSource() == f_upc
			|| e.getSource() == this.f_name // || e.getSource() == f_sku
			)
		{
			setResults(MBPartnerInfo.find (getCtx (),
				this.f_value.getText(), this.f_name.getText(), 
				null, this.f_email.getText(),
				this.f_phone.getText(), this.f_city.getText()));
			return;
		}
		else if ("Reset".equals(e.getActionCommand()))
		{
			reset();
			return;
		}
		else if ("Previous".equalsIgnoreCase(e.getActionCommand()))
		{
			int rows = this.m_table.getRowCount();
			if (rows == 0)
				return;
			int row = this.m_table.getSelectedRow();
			row--;
			if (row < 0)
				row = 0;
			this.m_table.getSelectionModel().setSelectionInterval(row, row);
			return;
		}
		else if ("Next".equalsIgnoreCase(e.getActionCommand()))
		{
			int rows = this.m_table.getRowCount();
			if (rows == 0)
				return;
			int row = this.m_table.getSelectedRow();
			row++;
			if (row >= rows)
				row = rows - 1;
			this.m_table.getSelectionModel().setSelectionInterval(row, row);
			return;
		}
		else if ("Ok".equals(e.getActionCommand()))
		{
			close();
		}
		//	Exit
		dispose();
	}	//	actionPerformed
	
	
	/**
	 * 	Set/display Results
	 *	@param results results
	 */
	public void setResults (MBPartnerInfo[] results)
	{
		this.m_table.loadTable(results);
		enableButtons();
	}	//	setResults

	/**
	 * 	Enable/Set Buttons and set ID
	 */
	@SuppressWarnings("nls")
	protected void enableButtons()
	{
		this.m_C_BPartner_ID = -1;
		int row = this.m_table.getSelectedRow();
		boolean enabled = row != -1;
		if (enabled)
		{
			Integer ID = this.m_table.getSelectedRowKey();
			if (ID != null)
			{
				this.m_C_BPartner_ID = ID.intValue();
			}
		}
		this.f_ok.setEnabled(enabled);
		if (log.isLoggable(Level.FINE)) log.fine("C_BPartner_ID=" + this.m_C_BPartner_ID); 
	}	//	enableButtons

	/**
	 * 	Close.
	 * 	Set Values on other panels and close
	 */
	@SuppressWarnings("nls")
	protected void close()
	{
		if (log.isLoggable(Level.FINE)) log.fine("C_BPartner_ID=" + this.m_C_BPartner_ID); 
		
		if (this.m_C_BPartner_ID > 0)
		{
			this.p_posPanel.m_infoPanel.setC_BPartner_ID(this.m_C_BPartner_ID);
		}
		else
		{
			this.p_posPanel.m_infoPanel.setC_BPartner_ID(0);
		}
		
		dispose();
	}	//	close


	@Override
	public void reset() {
		this.f_value.setText(null);
		this.f_name.setText(null);
		this.f_contact.setText(null);
		this.f_email.setText(null);
		this.f_phone.setText(null);
		this.f_city.setText(null);
		setResults(new MBPartnerInfo[0]);
	}

}
