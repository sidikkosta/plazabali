package com.uns.pos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.Format;

import javax.swing.JFormattedTextField;

import org.compiere.pos.POSKeyboard;

public class UNSPOSTextField extends JFormattedTextField implements
		MouseListener {
	
	private UNSPOSPanel m_posPanel = null;
	private int m_keyLayoutID = 0;
	private String m_title;

	/**
	 * 
	 */
	private static final long serialVersionUID = -7587624806749059898L;

	public UNSPOSTextField(String title, UNSPOSPanel pos, final int posKeyLayout_ID,
			Format format ) {
		super(format);
		
		if ( posKeyLayout_ID > 0 )
			addMouseListener(this);
		
		this.m_keyLayoutID = posKeyLayout_ID;
		this.m_posPanel = pos;
		this.m_title = title;
		
	}
	
	public UNSPOSTextField(String title, UNSPOSPanel pos, final int posKeyLayout_ID) {
		super();
		
		if ( posKeyLayout_ID > 0 )
			addMouseListener(this);
		
		this.m_keyLayoutID = posKeyLayout_ID;
		this.m_posPanel = pos;
		this.m_title = title;
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		if ( isEnabled() && isEditable() )
		{
			POSKeyboard keyboard = this.m_posPanel.getKeyboard(this.m_keyLayoutID); 
			keyboard.setTitle(this.m_title);
			keyboard.setPosTextField(this);
			keyboard.setVisible(true);
			fireActionPerformed();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {/** */}

	@Override
	public void mouseExited(MouseEvent arg0) {/** */}

	@Override
	public void mousePressed(MouseEvent arg0) {/** */}

	@Override
	public void mouseReleased(MouseEvent arg0) {/** */}

}
