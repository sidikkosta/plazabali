package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.PO;
import org.compiere.pos.QueryProduct;
import org.compiere.swing.CButton;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

public class UNSPOSQueryProduct extends UNSPOSQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2618144050021362472L;

	/**
	 * 	Constructor
	 */
	public UNSPOSQueryProduct (UNSPOSPanel posPanel)
	{
		super(posPanel);
	}	//	PosQueryProduct
	
	private UNSPOSTextField		f_value;
	private UNSPOSTextField		f_name;
	private UNSPOSTextField		f_upc;
	private UNSPOSTextField		f_sku;

	private int				m_M_Product_ID;
	private String			m_ProductName;
	private BigDecimal		m_Price;
	private CButton f_refresh;
	private CButton f_ok;
	private CButton f_cancel;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(QueryProduct.class);
	
	
	/**	Table Column Layout Info			*/
	@SuppressWarnings("nls")
	private static ColumnInfo[] s_layout = new ColumnInfo[] 
	{
		new ColumnInfo(" ", "p.M_Product_ID", IDColumn.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Value"), "p.Value", String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "p.Name", String.class),
		new ColumnInfo(Msg.translate(Env.getCtx(), "UPC"), "p.UPC", String.class), 
		new ColumnInfo(Msg.translate(Env.getCtx(), "SKU"), "p.SKU", String.class), 
		new ColumnInfo(Msg.translate(Env.getCtx(), "Price"), "pp.priceStd", BigDecimal.class)
	};

	/**
	 * 	Set up Panel
	 */
	@SuppressWarnings("nls")
	protected void init()
	{
		CPanel panel = new CPanel();
		panel.setBackground(new Color(16, 106, 166));
		panel.setLayout(new MigLayout("fill", "[70%][30%]", ""));
		getContentPane().add(panel);
		//	North
		this.northPanel = new CPanel(new MigLayout("fill", "", "[50][50][]"));
		panel.add (this.northPanel, "north");
		this.northPanel.setBorder(new TitledBorder(Msg.getMsg(getCtx (), "Query")));
		
		//
		CLabel lvalue = new CLabel(Msg.translate(getCtx (), "Value"));
		this.northPanel.add (lvalue, "growy");
		this.f_value = new UNSPOSTextField("", this.p_posPanel, 0);
		this.f_value.setName("ProductKey"); //red1 for UISpec4J testing call
		lvalue.setLabelFor(this.f_value);
		this.northPanel.add(this.f_value,  "h 30, w 200");
		this.f_value.addActionListener(this);
		//
		CLabel lupc = new CLabel(Msg.translate(getCtx (), "UPC"));
		this.northPanel.add (lupc, "growy");
		this.f_upc = new UNSPOSTextField("", this.p_posPanel, 0);
		lupc.setLabelFor(this.f_upc);
		this.northPanel.add(this.f_upc,  "h 30, w 200, wrap");
		this.f_upc.addActionListener(this);
		//
		CLabel lname = new CLabel(Msg.translate(getCtx (), "Name"));
		this.northPanel.add (lname, "growy");
		this.f_name = new UNSPOSTextField("", this.p_posPanel, 0);
		this.f_name.setName("ProductName"); //red1 for UISpec4J testing call
		lname.setLabelFor(this.f_name);
		this.northPanel.add(this.f_name,  "h 30, w 200");
		this.f_name.addActionListener(this);
		//
		CLabel lsku = new CLabel(Msg.translate(getCtx (), "SKU"));
		this.northPanel.add (lsku, "growy");
		this.f_sku = new UNSPOSTextField("", this.p_posPanel, 0);
		lsku.setLabelFor(this.f_sku);
		this.northPanel.add(this.f_sku,  "h 30, w 200");
		this.f_sku.addActionListener(this);
		//
		

		this.f_refresh = createButtonAction("Refresh", KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		this.northPanel.add(this.f_refresh, "w 50!, h 50!, wrap, alignx trailing");
		
		this.f_up = createButtonAction("Previous", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		this.northPanel.add(this.f_up, "w 50!, h 50!, span, split 4");
		this.f_down = createButtonAction("Next", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		this.northPanel.add(this.f_down, "w 50!, h 50!");
		
		this.f_ok = createButtonAction("Ok", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, Event.CTRL_MASK));
		this.northPanel.add(this.f_ok, "w 50!, h 50!");
		
		this.f_cancel = createButtonAction("Cancel", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		this.northPanel.add(this.f_cancel, "w 50!, h 50!");

		//	Center
		this.m_table = new UNSPOSTable();
		this.m_table.prepareTable (s_layout, "M_Product", 
			"IsActive = 'Y'", false, "M_Product");
		this.m_table.addMouseListener(this);
		this.m_table.getSelectionModel().addListSelectionListener(this);
		this.m_table.setColumnVisibility(this.m_table.getColumn(0), false);
		this.m_table.getColumn(1).setPreferredWidth(175);
		this.m_table.getColumn(2).setPreferredWidth(175);
		this.m_table.getColumn(3).setPreferredWidth(100);
		this.m_table.getColumn(4).setPreferredWidth(75);
		this.m_table.getColumn(5).setPreferredWidth(75);
		enableButtons();
		this.m_table.setFillsViewportHeight( true ); //@Trifon
		this.m_table.growScrollbars();
		this.centerScroll = new CScrollPane(this.m_table);
		panel.add (this.centerScroll, "growx, growy");
		panel.setPreferredSize(new Dimension(800,600));
		
		UNSPOSKeyboardPanel kbp = new UNSPOSKeyboardPanel(this.p_posPanel);
		kbp.init();
		panel.add(kbp, "spanx, wrap, growx, growy,flowx, flowy,");
		setMinimumSize(new Dimension(900,600));
		setResizable(true);
		
		this.f_value.requestFocus();
	}	//	init
	
	/**
	 * 	Action Listener
	 *	@param e event
	 */
	@SuppressWarnings("nls")
	public void actionPerformed (ActionEvent e)
	{
		if (log.isLoggable(Level.INFO)) log.info(e.getActionCommand());
		if ("Refresh".equals(e.getActionCommand())
			|| e.getSource() == this.f_value || e.getSource() == this.f_upc
			|| e.getSource() == this.f_name || e.getSource() == this.f_sku)
		{
			setResults();
			return;
		}
		else if ("Reset".equals(e.getActionCommand()))
		{
			reset();
			return;
		}
		else if ("Previous".equalsIgnoreCase(e.getActionCommand()))
		{
			int rows =this. m_table.getRowCount();
			if (rows == 0)
				return;
			int row = this.m_table.getSelectedRow();
			row--;
			if (row < 0)
				row = 0;
			this.m_table.getSelectionModel().setSelectionInterval(row, row);
			this.m_table.scrollRectToVisible(this.m_table.getCellRect(row, 1, true));
			return;
		}
		else if ("Next".equalsIgnoreCase(e.getActionCommand()))
		{
			int rows = this.m_table.getRowCount();
			if (rows == 0)
				return;
			int row =this.m_table.getSelectedRow();
			row++;
			if (row >= rows)
				row = rows - 1;
			this.m_table.getSelectionModel().setSelectionInterval(row, row);
			
			this.m_table.scrollRectToVisible(this.m_table.getCellRect(row, 1, true)); 
			return;
		}
		if ("Ok".equals(e.getActionCommand()))
		{
			close();
			return;
		}
		//	Exit
//		close();
		dispose();
	}	//	actionPerformed
	
	@SuppressWarnings("nls")
	public void setResults ()
	{
		String sql = "SELECT ";
		for (int i=0; i<s_layout.length; i++)
		{
			sql += s_layout[i].getColSQL();
			if (i != s_layout.length -1)
			{
				sql += ", ";
			}
		}
		
		sql += " FROM M_productprice pp inner join m_product p on p.m_product_id = pp.m_product_id "
				+ " AND p.IsActive = 'Y' inner join m_pricelist_version pv on pv.m_pricelist_version_id "
				+ " = pp.m_Pricelist_version_ID and pv.isActive = 'Y' AND pv.validfrom "
				+ " = (select max (validfrom) from "
				+ " m_pricelist_version where validfrom <= ?::TIMESTAMP AND IsActive = 'Y' "
				+ " AND m_pricelist_version_ID in (select "
				+ " m_pricelist_version_id from m_productprice where m_product_id = "
				+ " pp.M_product_ID) AND M_PriceList_ID = ?) AND pv.M_PriceList_ID = ? "
				+ " WHERE pp.IsActive = 'Y' ";
		
		String customWhere = "";
		final String percent = "%";
		String value = this.f_value.getText();
		String name = this.f_name.getText();
		String upc = this.f_upc.getText();
		String sku = this.f_sku.getText();
		
		if (!Util.isEmpty(value, true))
		{
			customWhere += " UPPER (p.value) like ? ";
			value = percent + value + percent;
			
		}
		if (!Util.isEmpty(name, true))
		{
			if (customWhere.length() > 0)
				customWhere += " OR ";
			customWhere += " UPPER (p.Name) like ? ";
			name = percent + name + percent;
		}
		if (!Util.isEmpty(upc, true))
		{
			if (customWhere.length() > 0)
				customWhere += " OR ";
			customWhere += " UPPER (p.UPC) like ? ";
			upc = percent + upc + percent;
		}
		if (!Util.isEmpty(sku, true))
		{
			if (customWhere.length() > 0)
				customWhere += " OR ";
			customWhere += "UPPER (p.SKU) like ? ";
			sku = percent + sku + percent;
		}
		if (customWhere.length() > 0)
		{
			sql += " AND (" + customWhere + ")";
		}
		if (m_where != null)
			sql += "AND " + m_where;
		
		sql += " group by p.M_Product_id, p.value, p.name, pp.pricestd "
				+ " order by p.Name";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			int idx = 0;
			st = DB.prepareStatement(sql, get_TrxName());
			st.setTimestamp(++idx, p_posPanel.m_session.getDateAcct());
			st.setInt(++idx,  super.p_posPanel.m_session.getM_PriceList_ID());
			st.setInt(++idx, this.p_posPanel.m_session.getM_PriceList_ID());
			
			if (!Util.isEmpty(value, true))
			{
				st.setString(++idx, value.toUpperCase());
			}
			if (!Util.isEmpty(name, true))
			{
				st.setString(++idx, name.toUpperCase());
			}
			if (!Util.isEmpty(upc, true))
			{
				st.setString(++idx, upc.toUpperCase());
			}
			if (!Util.isEmpty(sku, true))
			{
				st.setString(++idx, sku.toUpperCase());
			}
			
			for (int i=0; m_params != null && i<m_params.length; i++)
			{
				st.setObject(++idx, m_params[i]);
			}
			
			rs = st.executeQuery();
			setResults(rs);
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	public void setResults (ResultSet rs)
	{
		this.m_table.loadTable(rs);
		if (this.m_table.getRowCount() >0 )
			this.m_table.setRowSelectionInterval(0, 0);
		enableButtons();
	}
	
	private String m_where = null;
	private Object m_params[] = null;
	
	public void setWhere (String where, Object... params)
	{
		m_where = where;
		m_params = params;
	}
	
	/**
	 * 	Enable/Set Buttons and set ID
	 */
	@SuppressWarnings("nls")
	protected void enableButtons()
	{
		this.m_M_Product_ID = -1;
		this.m_ProductName = null;
		this.m_Price = null;
		int row = this.m_table.getSelectedRow();
		boolean enabled = row != -1;
		if (enabled)
		{
			Integer ID = this.m_table.getSelectedRowKey();
			if (ID != null)
			{
				this.m_M_Product_ID = ID.intValue();
				this.m_ProductName = (String)this.m_table.getValueAt(row, 2);
				this.m_Price = (BigDecimal)this.m_table.getValueAt(row, 5);
			}
		}
		this.f_ok.setEnabled(enabled);
		if (log.isLoggable(Level.FINE)) log.fine("M_Product_ID=" + this.m_M_Product_ID + " - " 
					+ this.m_ProductName + " - " + this.m_Price); 
	}	//	enableButtons



	/**
	 * 	Close.
	 * 	Set Values on other panels and close
	 */
	@SuppressWarnings("nls")
	protected void close()
	{
		if (log.isLoggable(Level.FINE)) log.fine("M_Product_ID=" + this.m_M_Product_ID); 
		
		if (this.m_M_Product_ID > 0)
		{
			this.p_posPanel.f_curLine.addItem(this.m_M_Product_ID, this.m_ProductName, this.m_Price);
		}
		else
		{
			this.p_posPanel.f_curLine.addItem(0, null, null);
		}
		
		m_params = null;
		m_where = null;
		dispose();
	}	//	close


	@Override
	public void reset() {

		this.f_value.setText(null);
		this.f_name.setText(null);
		this.f_sku.setText(null);
		this.f_upc.setText(null);
		this.m_table.loadTable(new PO[0]);
	}

}
