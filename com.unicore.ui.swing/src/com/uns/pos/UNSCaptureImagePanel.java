/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import javax.swing.border.TitledBorder;
import org.compiere.swing.CPanel;
import com.digitalpersona.uareu.Fid;
import com.digitalpersona.uareu.Fid.Fiv;

/**
 * @author nurse
 *
 */
public class UNSCaptureImagePanel extends CPanel {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1347434835333476462L;
	private BufferedImage m_image;
	/**
	 * @param layout
	 */
	public UNSCaptureImagePanel(LayoutManager layout) 
	{
		super(layout);
	}
	
	public void init ()
	{
		setBorder(new TitledBorder("Finger"));
		setBackground(Color.WHITE);
	}

	/**
	 * 
	 */
	public UNSCaptureImagePanel() 
	{
		super ();
	}
	
	public void showImage (BufferedImage image)
	{
		m_image = image;
		repaint();
	}
	
	public void showImage(Fid image)
	{
		if (image == null)
		{
			m_image = null;
		}
		else
		{
			Fiv view = image.getViews()[0];
			m_image = new BufferedImage(view.getWidth(), view.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
			m_image.getRaster().setDataElements(0, 0, view.getWidth(), view.getHeight(), view.getImageData());
		}
		repaint();
	} 
	
	public void paint(Graphics g) 
	{
		super.paint(g);
		g.drawImage(m_image, 0, 0, this.getWidth(), this.getHeight(), null);
	}
	
	public BufferedImage getImage ()
	{
		return m_image;
	}
}
