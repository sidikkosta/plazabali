/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerInfo;
import org.compiere.swing.CLabel;
import org.compiere.swing.CTextField;
import org.compiere.util.DB;
import org.compiere.util.KeyNamePair;

import com.unicore.model.MUNSCustomerInfo;
import com.unicore.model.MUNSPOSTerminal;

import net.miginfocom.swing.MigLayout;

/**
 * @author nurse
 *
 */
public class UNSPOSInfoPanel extends UNSPOSSubPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8458035528219299711L;
	private CTextField f_sesionNo = null;
	private CTextField f_sales = null;
	private CTextField f_trxNo = null;
	private CTextField f_customer = null;
	private KeyNamePair m_sales;
	private MUNSCustomerInfo m_customer = null;
	
	
	/**
	 * @param posPanel
	 */
	public UNSPOSInfoPanel(UNSPOSPanel posPanel) 
	{
		super(posPanel);
	}

	/* (non-Javadoc)
	 * @see com.uns.pos.UNSPOSSubPanel#init()
	 */
	@Override
	protected void init() 
	{
		setLayout(new MigLayout("fill","10%![][150px][][150px]10%!","fill"));
		Font fDetail = new Font("Calibri", Font.BOLD, 12);
		
		CLabel lSesNo = new CLabel("");
		ImageIcon icon = new ImageIcon(getClass().getResource("/com/unicore/images/SessionNo.png"));
		Image img = icon.getImage().getScaledInstance(100, 26, Image.SCALE_SMOOTH);
		lSesNo.setIcon(new ImageIcon(img));
		lSesNo.setFont(fDetail);
		lSesNo.setHorizontalAlignment(SwingConstants.TRAILING);
		add(lSesNo, "center");
		
		f_sesionNo = new CTextField(m_session.getDocumentNo());
		f_sesionNo.setMargin(new Insets(0, 5, 0, 0));
		f_sesionNo.setRounded(new Color(146,208,80),15, 15);
		f_sesionNo.setFont(fDetail);
		f_sesionNo.setForeground(Color.BLACK);
		f_sesionNo.setHorizontalAlignment(SwingConstants.LEFT);
		f_sesionNo.setReadWrite(false);
		f_sesionNo.setBackground(Color.WHITE);
		add(f_sesionNo, "flowx, growx,pushx");
		
		JLabel lSales = new CLabel("");
		ImageIcon icon2 = new ImageIcon(getClass().getResource("/com/unicore/images/Sales.png"));
		Image img2 = icon2.getImage().getScaledInstance(100, 26, Image.SCALE_SMOOTH);
		lSales.setIcon(new ImageIcon(img2));
		lSales.setBackground(new Color(0,110,0));
		lSales.setFont(fDetail);
		lSales.setForeground(Color.black);
		lSales.setHorizontalAlignment(SwingConstants.LEFT);
		add(lSales, "center");
		
		f_sales = new CTextField("");
		f_sales.setMargin(new Insets(0, 5, 0, 0));
		f_sales.setRounded(new Color(146,208,80),15, 15);
		f_sales.setFont(fDetail);
		f_sales.setForeground(Color.BLACK);
		f_sales.setHorizontalAlignment(SwingConstants.LEFT);
		add(f_sales, "flowx, growx,pushx, wrap");
		
		f_sales.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				m_posPanel.checkSession();
				if (m_sales == null || m_sales.getKey() == -1)
					findUser();
				if (m_sales != null && m_customer == null)
				{
					f_customer.requestFocus();
					m_posPanel.f_curLine.m_table.setEnabled(false);
				}
				else if (m_sales != null && m_customer != null)
				{
					MyTable table = m_posPanel.f_curLine.m_table;
					table.setEnabled(true);
					table.requestFocus();
					for (int i=0; i<table.getRowCount(); i++)
					{
						if (table.getValueAt(i, 0) == null)
						{
							table.getSelectionModel().setSelectionInterval(i, i);
							table.editCellAt(i, 2);
							break;
						}
					}
				}
				else
					m_posPanel.f_curLine.m_table.setEnabled(false);
			}
		});
		
		CLabel lTrxNo = new CLabel("");
		ImageIcon icon3 = new ImageIcon(getClass().getResource("/com/unicore/images/TrxNo.png"));
		Image img3 = icon3.getImage().getScaledInstance(100, 26, Image.SCALE_SMOOTH);
		lTrxNo.setIcon(new ImageIcon(img3));
		lTrxNo.setFont(fDetail);
		lTrxNo.setBackground(new Color(0,110,0));
		lTrxNo.setForeground(Color.WHITE);
		lTrxNo.setHorizontalAlignment(SwingConstants.LEFT);
		add(lTrxNo, "center");
		
		f_trxNo = new CTextField("");
		f_trxNo.setMargin(new Insets(0, 5, 0, 0));
		f_trxNo.setRounded(new Color(146,208,80),15, 15);
		f_trxNo.setFont(fDetail);
		f_trxNo.setReadWrite(false);
		f_trxNo.setBackground(Color.WHITE);
		f_trxNo.setForeground(Color.BLACK);
		f_trxNo.setHorizontalAlignment(SwingConstants.LEFT);
		add(f_trxNo, "flowx, growx,pushx,");
		
		CLabel lCust = new CLabel("");
		ImageIcon icon4 = new ImageIcon(getClass().getResource("/com/unicore/images/Customer.png"));
		Image img4 = icon4.getImage().getScaledInstance(100, 26, Image.SCALE_SMOOTH);
		lCust.setIcon(new ImageIcon(img4));
		lCust.setBackground(new Color(0,110,0));
		lCust.setFont(fDetail);
		lCust.setForeground(Color.black);
		lCust.setHorizontalAlignment(SwingConstants.LEFT);
		add(lCust, "center");
		
		f_customer = new CTextField("");
		f_customer.setMargin(new Insets(0, 5, 0, 0));
		f_customer.setRounded(new Color(146,208,80),15, 15);
		f_customer.setFont(fDetail);
		f_customer.setForeground(Color.BLACK);
		f_customer.setHorizontalAlignment(SwingConstants.LEFT);
		f_customer.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void focusGained(FocusEvent arg0) 
			{
				m_posPanel.checkSession();
				if (m_posPanel.f_curLine.m_table.isEnabled())
					return;
				else if (m_sales == null || m_sales.getKey() == -1)
				{
					f_sales.requestFocus();
					return ;
				}
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						if (MUNSPOSTerminal.STORETYPE_RetailDutyFree.equals(m_posPanel.m_terminal.getStoreType()))
						{
							UNSCustomerInfo info = new UNSCustomerInfo(m_posPanel, true, m_customer);
							Point p = f_customer.getLocationOnScreen();
					        p.setLocation(p.getX(), p.getY()+28);
					        info.setLocation(p);
							info.setVisible(true);
						}
						else
						{
							m_customer = new MUNSCustomerInfo(getCtx(), 0, get_TrxName());
							m_customer.setName(f_customer.getText());
						}
						if (m_sales != null && m_customer != null)
						{
							MyTable table = m_posPanel.f_curLine.m_table;
							table.setEnabled(true);
							table.requestFocus();
							for (int i=0; i<table.getRowCount(); i++)
							{
								if (table.getValueAt(i, 0) == null)
								{
									table.getSelectionModel().setSelectionInterval(i, i);
									table.editCellAt(i, 2);
									break;
								}
							}
						}
						else
							m_posPanel.f_curLine.m_table.setEnabled(false);
					}
				});
			}
		});
		f_customer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				m_posPanel.checkSession();
				if (MUNSPOSTerminal.STORETYPE_RetailDutyFree.equals(m_posPanel.m_terminal.getStoreType()))
				{
					UNSCustomerInfo info = new UNSCustomerInfo(m_posPanel, true, m_customer);
					Point p = f_customer.getLocationOnScreen();
			        p.setLocation(p.getX(), p.getY()+28);
			        info.setLocation(p);
					info.setVisible(true);
				}
				else
				{
					m_customer = new MUNSCustomerInfo(getCtx(), 0, get_TrxName());
					m_customer.setName(f_customer.getText());
				}
				if (m_sales != null && m_customer != null)
				{
					MyTable table = m_posPanel.f_curLine.m_table;
					table.setEnabled(true);
					table.requestFocus();
					for (int i=0; i<table.getRowCount(); i++)
					{
						if (table.getValueAt(i, 0) == null)
						{
							table.getSelectionModel().setSelectionInterval(i, i);
							table.editCellAt(i, 2);
							break;
						}
					}
				}
				else
					m_posPanel.f_curLine.m_table.setEnabled(false);
			}
		});
		
		add(f_customer, "flowx, growx,pushx, wrap");
	}
	
	public void setSales (int id, String name)
	{
		m_sales = new KeyNamePair(id, name);
		f_sales.setText(m_sales.getName());
	}
	
	public void setCustomer (MUNSCustomerInfo customer)
	{
		m_customer = customer;
		if (m_customer == null)
			f_customer.setText("");
		else
			f_customer.setText(m_customer.getName());
	}
	
	public void reset ()
	{
		m_customer = null;
		f_customer.setText("");
		m_sales = null;
		f_sales.setText("");
		if (m_posPanel.f_curLine.m_table != null)
			m_posPanel.f_curLine.m_table.setEnabled(false);
		f_trxNo.setText("");
		f_sales.requestFocus();
	}
	
	private void findUser ()
	{
		UNSFindUserDialog dialog = new UNSFindUserDialog(m_posPanel, true);
		Point p = f_sales.getLocationOnScreen();
        p.setLocation(p.getX(), p.getY()+28);
        dialog.setLocation(p);
        dialog.setFocusable(false);
        dialog.getTxtSearchValue().setText(f_sales.getText());
        if (f_sales.getText().length() > 0)
        	dialog.search();
        if (dialog.getRowCount() != 1)
        	dialog.setVisible(true);
	}
	
	protected void findBPartner()
	{
		
		String query = this.f_customer.getText();
		
		if (query == null || query.length() == 0)
			return;
		
		// unchanged
		if ( m_partner!= null && m_partner.getName().equals(query))
			return;
		
		query = query.toUpperCase();
		//	Test Number
		boolean allNumber = true;
		boolean noNumber = true;
		char[] qq = query.toCharArray();
		for (int i = 0; i < qq.length; i++)
		{
			if (Character.isDigit(qq[i]))
			{
				noNumber = false;
				break;
			}
		}
		try
		{
			Integer.parseInt(query);
		}
		catch (Exception e)
		{
			allNumber = false;
		}
		
		String memberIDCard = noNumber ? null : query;
		String Value = query;
		String Name = (allNumber ? null : query);
		String EMail = (query.indexOf('@') != -1 ? query : null); 
		String Phone = (noNumber ? null : query);
		String City = null;
		//
		//TODO: contact have been remove from rv_bpartner
		MBPartnerInfo[] results = MBPartnerInfo.find(getCtx (), Value, Name, 
			/*Contact, */null, EMail, Phone, City, memberIDCard);
		
		//	Set Result
		if (results.length == 0)
		{
			setC_BPartner_ID(0);
		}
		else if (results.length == 1)
		{
			setC_BPartner_ID(results[0].getC_BPartner_ID());
			f_customer.setText(results[0].getName());
		}
		else	//	more than one
		{
			UNSPOSQueryBPartner qt = new UNSPOSQueryBPartner(this.m_posPanel);
			qt.setResults (results);
			qt.setVisible(true);
		}
	}
	
	private MBPartner m_partner = null;
	
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID == 0)
			m_partner = this.m_posPanel.getNonMemberPartner();
		else
		{
			m_partner = new MBPartner(getCtx (), C_BPartner_ID, get_TrxName ());
			if (m_partner.get_ID() == 0)
				m_partner = null;
		}

		if ( this.m_posPanel.m_posTrx != null && m_partner != null )
		{
			this.m_posPanel.m_posTrx.setC_BPartner_ID(m_partner.get_ID());
			this.m_posPanel.m_posTrx.saveEx();
		}
	}	//	setC_BPartner_ID
	
	public MBPartner getBPartner ()
	{
		if (null == m_partner)
		{
			m_partner = this.m_posPanel.getNonMemberPartner();
		}
		
		return m_partner;
	}
	
	public KeyNamePair getSales ()
	{
		return m_sales;
	}
	
	public MUNSCustomerInfo getCustomer ()
	{
		return m_customer;
	}
	
	public void updateInfo ()
	{
		if (m_posPanel.m_posTrx != null)
		{
			String sql = "SELECT RealName FROM AD_User WHERE AD_User_ID = ?";
			String user = DB.getSQLValueString(get_TrxName(), sql, m_posPanel.m_posTrx.getSalesRep_ID());
			MUNSCustomerInfo info = new MUNSCustomerInfo(getCtx(), m_posPanel.m_posTrx.getUNS_CustomerInfo_ID(), get_TrxName());
			if (info.getUNS_CustomerInfo_ID() == 0)
				info.setName(m_posPanel.m_posTrx.getCustomerName());
			setCustomer(info);
			setSales(m_posPanel.m_posTrx.getSalesRep_ID(), user);
			setTrxNo(m_posPanel.m_posTrx.getTrxNo());
		}
		else
		{
			setCustomer(null);
			setSales(-1, "");
		}
	}
	
	public void setReadWrite (boolean rw)
	{
		f_customer.setEditable(rw);
		f_sales.setEditable(rw);
	}
	
	public void setTrxNo (String trxNo)
	{
		f_trxNo.setText(trxNo);
	}
}
