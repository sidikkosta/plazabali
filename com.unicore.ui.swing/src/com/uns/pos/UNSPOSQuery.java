package com.uns.pos;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Properties;

import javax.swing.KeyStroke;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.compiere.apps.AEnv;
import org.compiere.apps.AppsAction;
import org.compiere.apps.ConfirmPanel;
import org.compiere.pos.QueryProduct;
import org.compiere.swing.CButton;
import org.compiere.swing.CDialog;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.util.CLogger;

public abstract class UNSPOSQuery extends CDialog implements MouseListener,
		ListSelectionListener, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6227838436743951898L;

	private Properties p_ctx;
	private String p_trxName;
	protected UNSPOSPanel p_posPanel = null;
	protected UNSPOSTrxModel p_pos = null;
	protected UNSPOSTable m_table;
	protected CPanel northPanel;
	protected CScrollPane centerScroll;
	protected ConfirmPanel confirm;
	protected CButton f_up;
	protected CButton f_down;
	protected static CLogger log = CLogger.getCLogger(QueryProduct.class);

	public UNSPOSQuery() throws HeadlessException {
		super();
	}

	protected abstract void close();

	public abstract void reset();

	public abstract void actionPerformed(ActionEvent e);

	public void dispose() {
		removeAll();
		this.northPanel = null;
		this.centerScroll = null;
		this.confirm = null;
		this.m_table = null;
		super.dispose();
	}

	protected abstract void init();
	protected abstract void enableButtons();

	/**
	 * 	Constructor
	 */
	public UNSPOSQuery (UNSPOSPanel posPanel)
	{
		super(AEnv.getFrame(posPanel), true);
		this.p_posPanel = posPanel;
		this.p_pos = posPanel.m_posTrx;
		this.p_ctx = posPanel.getCtx();
		this.p_trxName = posPanel.get_TrxName();
		init();
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
		int y = (screenSize.height - getHeight()) / 2;
		setLocation(x, y);
		pack();
	}	//	PosQueryBPartner

	public String get_TrxName ()
	{
		return this.p_trxName;
	}
	
	public Properties getCtx ()
	{
		return this.p_ctx;
	}
	/**
	 *  Mouse Clicked
	 *  @param e event
	 */
	public void mouseClicked(MouseEvent e)
	{
		//  Single click with selected row => exit
		if (e.getClickCount() > 1 && this.m_table.getSelectedRow() != -1)
		{
			enableButtons();
			close();
		}
	}   //  mouseClicked

	public void mouseEntered (MouseEvent e) {/**Do nothing */}
	public void mouseExited (MouseEvent e) {/**Do nothing */}
	public void mousePressed (MouseEvent e) {/**Do nothing */}
	public void mouseReleased (MouseEvent e) {/**Do nothing */}
	
	/**
	 * 	Table selection changed
	 *	@param e event
	 */
	public void valueChanged (ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting())
			return;
		enableButtons();
	}	//	valueChanged
	
	/**
	 * 	Create Action Button
	 *	@param action action 
	 *	@return button
	 */
	protected CButton createButtonAction(String action, KeyStroke accelerator) {
		AppsAction act = new AppsAction(action, accelerator, false);
		act.setDelegate(this);
		CButton button = (CButton)act.getButton();
		button.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		button.setMinimumSize(getPreferredSize());
		button.setMaximumSize(getPreferredSize());
		button.setFocusable(false);
		return button;
	}	//	getButtonAction

}
