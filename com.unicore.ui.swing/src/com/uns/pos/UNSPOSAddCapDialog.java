/**
 * 
 */
package com.uns.pos;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import net.miginfocom.swing.MigLayout;

import org.compiere.apps.ADialog;
import org.compiere.apps.AEnv;
import org.compiere.grid.ed.VNumber;
import org.compiere.swing.CButton;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CDialog;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.KeyNamePair;

import com.unicore.model.MUNSPaymentTrx;

/**
 * @author nurse
 *
 */
public class UNSPOSAddCapDialog extends CDialog 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9025574524168863587L;
	private UNSPOSPanel m_posPanel;
	private CComboBox f_currency;
	private VNumber f_amt;
	private CButton b_ok;
	private CButton b_cancel;

	/**
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public UNSPOSAddCapDialog(UNSPOSPanel panel, boolean modal) throws HeadlessException 
	{
		super(AEnv.getFrame(panel), modal);
		m_posPanel = panel;
		setTitle("Add Capital");
		setPreferredSize(new Dimension(400,150));
		setMinimumSize(getPreferredSize());
		setResizable(false);
		setLocationByPlatform(true);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
		int y = (screenSize.height - getHeight()) / 2;
		setLocation(x, y);
		init();
	}
	
	private void init ()
	{
		f_currency = new CComboBox(loadCurrency());
		f_currency = new CComboBox(loadCurrency());
		f_amt = new VNumber();
		f_amt.setDisplayType(DisplayType.Amount);
		b_ok = new CButton("OK");
		b_cancel = new CButton("CANCEL");
		b_ok.addActionListener(this);
		b_cancel.addActionListener(this);
		
		CPanel panel = new CPanel(new MigLayout("fill","[][fill]",""));
		CLabel lcurrFrom = new CLabel("Currency");
		CLabel lAmtFrom = new CLabel("Amount");
		
		panel.add(lcurrFrom, "");
		panel.add(f_currency, "wrap, pushx, pushy, spanx, flowx, growx, growy, flowy");
		panel.add(lAmtFrom, "");
		panel.add(f_amt, "wrap, pushx, pushy, spanx, flowx, growx, growy, flowy");
		panel.add(new CLabel());
		panel.add(b_ok, "split 2");
		panel.add(b_cancel, "wrap, ");
		
		getContentPane().add(panel);
	}
	
	private Vector<KeyNamePair> loadCurrency ()
	{
		Vector<KeyNamePair> retVal = new Vector<>();
		String sql = "SELECT C_Currency_ID, (SELECT Description FROM C_Currency WHERE C_Currency_ID = "
				+ " UNS_SessionCashAccount.C_Currency_ID), IsUsedForChange FROM UNS_SessionCashAccount "
				+ "WHERE UNS_POS_Session_ID = ? AND IsActive = ? ";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_posPanel.get_TrxName());
			st.setInt(1, m_posPanel.m_session.getUNS_POS_Session_ID());
			st.setString(2, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				KeyNamePair curr = new KeyNamePair(rs.getInt(1), rs.getString(2));
				retVal.add(curr);
			}
		}
		catch (SQLException ex)
		{
			ADialog.error(m_posPanel.getWindowNo(), this, ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return retVal;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource().equals(b_ok))
		{
			UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
			confirm.setVisible(true);
			if (!confirm.isApproved())
			{
				return;
			}
			KeyNamePair currency = (KeyNamePair) f_currency.getSelectedItem();
			if (currency == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSAddCapDialog.this, "Mandatory field Currency");
				return;
			}
			
			
			int currencyID = currency.getKey();
			BigDecimal amt = (BigDecimal) f_amt.getValue();
			
			try
			{	
				MUNSPaymentTrx trx = new MUNSPaymentTrx(m_posPanel.getCtx(), 0, m_posPanel.get_TrxName());
				trx.setAD_Org_ID(m_posPanel.m_session.getAD_Org_ID());
				trx.setAmount(amt);
				trx.setC_Currency_ID(currencyID);
				trx.setConvertedAmt(amt);
				trx.setIsReceipt(true);
				trx.setIsReconciled(true);
				trx.setPaymentMethod(MUNSPaymentTrx.PAYMENTMETHOD_Cash);
				trx.setTrxType(MUNSPaymentTrx.TRXTYPE_CapitalIn);
				trx.setProcessed(true);
				trx.setUNS_POS_Session_ID(m_posPanel.m_session.get_ID());
				trx.saveEx();
				trx.processExchange();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				try 
				{
					DB.rollback(true, m_posPanel.get_TrxName());
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				return;
			}
			
			try 
			{
				DB.commit(true, m_posPanel.get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
			}
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			dispose();
		}
		else if (e.getSource().equals(b_cancel))
		{
			dispose();
		}
	}
}
