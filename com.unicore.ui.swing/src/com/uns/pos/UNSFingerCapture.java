/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

import org.compiere.swing.CButton;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CDialog;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;

import com.digitalpersona.uareu.Fid.Format;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.Reader.ImageProcessing;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;

/**
 * @author nurse
 *
 */
public class UNSFingerCapture extends CDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 297369752792135303L;
	private CComboBox f_readers;
	private JButton b_ok;
	private JButton b_cancel;
	private JLabel l_info;
	private UNSCustomerInfo m_parent;
	private UNSCaptureImagePanel m_image;
	private Reader m_reader = null;
	private Thread m_readerListener;
	private ReaderCollection m_collections = null;
	private volatile boolean m_stopRead = false;
	private volatile boolean m_onCancel = false;
	

	/**
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public UNSFingerCapture(UNSCustomerInfo parent, boolean modal) throws HeadlessException 
	{
		super(parent, modal);
		m_parent = parent;
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(400, 400));
        setPreferredSize(new Dimension(400, 400));
        setResizable(false);
	}
	
	public void init ()
	{
		CPanel panel = new CPanel(new MigLayout("fill", "[][]", ""));
		panel.setBackground(new Color(243, 250, 236));
		try 
		{
			m_collections = UareUGlobal.GetReaderCollection();
			m_collections.GetReaders();
		} 
		catch (UareUException e) 
		{
			e.printStackTrace();
		}
		
		CLabel lReader = new CLabel("Reader");
		Vector<String> vector = new Vector<>();
		for (int i=0; m_collections != null && i<m_collections.size(); i++)
			vector.add(m_collections.get(i).GetDescription().name);
		f_readers = new CComboBox(vector);
		f_readers.setMaximumSize(new Dimension(300, 30));
		
		f_readers.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				m_stopRead = true;
				m_onCancel = true;
				if (m_reader != null)
				{
					try 
					{
						m_reader.CancelCapture();
					} 
					catch (UareUException e) 
					{
						e.printStackTrace();
					}
				}
				while (m_readerListener != null && m_readerListener.isAlive())
				{
					continue;
				}
				m_reader = null;
				m_readerListener = null;
				if (f_readers.getSelectedIndex() != -1)
				{
					m_stopRead = false;
					m_onCancel = false;
					m_reader = m_collections.get(f_readers.getSelectedIndex());
					m_readerListener = new Thread(new Runnable() 
					{
						public void listen ()
						{
							try
							{
								m_reader.Open(Reader.Priority.COOPERATIVE);
								while(!m_stopRead)
								{
									Reader.Status rs = m_reader.GetStatus();
									if(Reader.ReaderStatus.BUSY == rs.status)
									{
										try
										{
											Thread.sleep(1000);
										} 
										catch(InterruptedException e) 
										{
											e.printStackTrace(); 
											break;
										}
									}
									else if(Reader.ReaderStatus.READY == rs.status || Reader.ReaderStatus.NEED_CALIBRATION == rs.status)
									{
										Reader.CaptureResult cr = m_reader.Capture(Format.ANSI_381_2004, ImageProcessing.IMG_PROC_DEFAULT, 500, -1);
										if (m_onCancel)
											m_image.showImage((BufferedImage) null);
										else
											m_image.showImage(cr.image);
									}
								}
								m_reader.Close();
							}
							catch(UareUException e)
							{
								e.printStackTrace();
							}
							
							m_reader = null;
						}
						
						@Override
						public void run() 
						{
							listen();
						}
					});
					
					m_readerListener.start();
				}
			}
		});
		
		if (f_readers.getItemCount() > 0)
			f_readers.setSelectedIndex(0);
		lReader.setLabelFor(f_readers);
		panel.add(lReader, "");
		panel.add(f_readers, "wrap");
		l_info = new CLabel("");
		if (f_readers.getSelectedIndex() == -1)
			l_info.setText("Please select the reader first");
		else
			l_info.setText("Put your finger on the reader please");
		
		panel.add(l_info, "spanx, wrap, left");
		m_image = new UNSCaptureImagePanel();
		m_image.init();
		panel.add(m_image, ",wrap, span 2, flowy, growy, flowx, growx, pushy");
		
		b_ok = new CButton("Save ");
		b_cancel = new CButton("Cancel");
		panel.add(new CLabel());
		panel.add(b_ok, ", right, split 2");
		panel.add(b_cancel, ",right, wrap");
		
		b_cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		
		b_ok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				BufferedImage img = m_image.getImage();
				m_parent.setFinger(img);
				dispose();
				m_parent.onOK();
			}
		});
		
		f_readers.setFocusable(false);
		getContentPane().add(panel);
		b_ok.requestFocus();
		b_ok.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) 
			{
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) 
			{
				if (arg0.isConsumed())
					return;
				int keyCode = arg0.getKeyCode();
				if (keyCode != KeyEvent.VK_ENTER)
					return;
				BufferedImage img = m_image.getImage();
				if (img == null)
					return;
				m_parent.setFinger(img);
				dispose();
				m_parent.onOK();
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) 
			{
				
			}
		});
	}
	
	@Override
	public void dispose ()
	{
		m_stopRead = true;
		m_onCancel = true;
		if (m_reader != null) 
		{
			try 
			{
				m_reader.CancelCapture();
			} 
			catch (UareUException e) 
			{
				e.printStackTrace();
			}
			
		}
		while (m_readerListener != null && m_readerListener.isAlive())
		{
			continue;
		}
		super.dispose();
	}
}
