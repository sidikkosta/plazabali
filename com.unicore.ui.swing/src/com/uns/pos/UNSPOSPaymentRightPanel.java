/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Font;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import net.miginfocom.swing.MigLayout;
import org.compiere.model.MConversionRate;
import org.compiere.swing.CLabel;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.unicore.model.MUNSPOSPayment;
import com.unicore.ui.form.UNTPanel;

/**
 * @author nurse
 *
 */
public class UNSPOSPaymentRightPanel extends UNSPOSSubPanel 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5350640478314349854L;
	private CLabel l_base1 = null;
	private CLabel l_payAmt = null;
	private CLabel l_roundAmt = null;
	private CLabel l_remaining = null;
	protected CLabel l_total = null;
	private UNSPOSPaymentPanel m_payPanel;
	private UNTPanel p_totalPanel;

	/**
	 * 
	 */
	public UNSPOSPaymentRightPanel(UNSPOSPanel pos, UNSPOSPaymentPanel payPanel) {
		super (pos);
		m_payPanel = payPanel;
		setLayout(new MigLayout("insets 0", "[fill,100%]", "[]"));
	}
	
	public void initComponent ()
	{
		p_totalPanel = new UNTPanel(new MigLayout("","[]","[][]"));
		
		Font fTotal = new Font("Calibri", Font.BOLD, 14);
		Font fRupiah = new Font("Calibri", Font.BOLD, 28);
		
		l_total = new CLabel("");
		l_total.setFont(fTotal);
		l_total.setForeground(Color.WHITE);
		l_total.setHorizontalAlignment(SwingConstants.LEFT);
		p_totalPanel.add(l_total, "wrap, spanx 2,pushx, flowx, growx, left");
		
		l_base1 = new CLabel("0");
		l_base1.setFont(fRupiah);
		l_base1.setHorizontalAlignment(SwingConstants.RIGHT);
		l_base1.setForeground(Color.WHITE);
		p_totalPanel.add(l_base1, "wrap,spanx 2, pushx, flowx, growx");
		
		CLabel lPaid = new CLabel("Paid");
		lPaid.setFont(fTotal);
		lPaid.setForeground(Color.WHITE);
		lPaid.setHorizontalAlignment(SwingConstants.LEFT);
		p_totalPanel.add(lPaid, "wrap, spanx 2,pushx, flowx, growx, left");
		
		l_payAmt = new CLabel("0");
		l_payAmt.setFont(fRupiah);
		l_payAmt.setHorizontalAlignment(SwingConstants.RIGHT);
		l_payAmt.setForeground(Color.WHITE);
		p_totalPanel.add(l_payAmt, "wrap,spanx 2, pushx, flowx, growx");
		
		CLabel lRound = new CLabel("Round Amt");
		lRound.setFont(fTotal);
		lRound.setForeground(Color.WHITE);
		lRound.setHorizontalAlignment(SwingConstants.LEFT);
		p_totalPanel.add(lRound, "wrap, spanx 2,pushx, flowx, growx, left");
		
		l_roundAmt = new CLabel("0");
		l_roundAmt.setFont(fRupiah);
		l_roundAmt.setHorizontalAlignment(SwingConstants.RIGHT);
		l_roundAmt.setForeground(Color.WHITE);
		p_totalPanel.add(l_roundAmt, "wrap,spanx 2, pushx, flowx, growx");
		
		add(p_totalPanel, "growx, growy, pushx, , wrap");

		JSeparator separator = new JSeparator();
		separator.setForeground(Color.WHITE);
		p_totalPanel.add(separator, "spanx 2, wrap, pushx, flowx, growx");
		
		CLabel lRemaining = new CLabel("Remaining");
		lRemaining.setFont(fTotal);
		lRemaining.setForeground(Color.WHITE);
		lRemaining.setHorizontalAlignment(SwingConstants.LEFT);
		p_totalPanel.add(lRemaining, "wrap, spanx 2,pushx, flowx, growx, left");
		
		l_remaining = new CLabel("0");
		l_remaining.setFont(fRupiah);
		l_remaining.setHorizontalAlignment(SwingConstants.RIGHT);
		l_remaining.setForeground(Color.WHITE);
		p_totalPanel.add(l_remaining, "wrap,spanx 2, pushx, flowx, growx");
		UNSPOSKeyboardPanel keyboard = new UNSPOSKeyboardPanel(m_posPanel, new Color(79, 129, 189), Color.WHITE);
		keyboard.init();
		keyboard.setFocusable(false);
		keyboard.setVisible(true);
		add (keyboard, "h 300px, w 300px, growx, growy, flowy, pushx, pushy, growx, wrap");
		updateInfo();
	}

	@Override
	protected void init() 
	{
		initComponent();
	}
	
	public void updateInfo ()
	{
		UNSPOSTrxModel model = m_posPanel.m_posTrx;
		model.reload();
		BigDecimal totalOri = model.getGrandTotal();
		BigDecimal remaining = Env.ZERO;
		BigDecimal totalB1 = Env.ZERO;
		BigDecimal payAmt = Env.ZERO;
		BigDecimal roundAmt = Env.ZERO;
		MUNSPOSPayment pay = m_payPanel.getPayment();
		if (pay != null)
		{
			pay.load(get_TrxName());
			totalOri = pay.getPOSAmount();
			payAmt = pay.getPaidAmt();
			roundAmt = pay.getRoundingAmt();
			remaining = totalOri.subtract(payAmt).add(roundAmt);
		}
		
		totalB1 = MConversionRate.convert(
				getCtx(), totalOri, model.getC_Currency_ID(), 
				m_session.getBase1Currency_ID(), model.getDateTrx(), 0, 
				model.getAD_Client_ID(), model.getAD_Org_ID());
		
		String totalB1Format = NumberFormat.getInstance(Locale.ENGLISH).format(totalB1);
		String remainingAmtFormat = NumberFormat.getInstance(Locale.ENGLISH).format(remaining);
		String payAmtFormat = NumberFormat.getInstance(Locale.ENGLISH).format(payAmt);
		String roundAmtFormat = NumberFormat.getInstance(Locale.ENGLISH).format(roundAmt);
		
		String sql = "SELECT CurSymbol FROM C_Currency WHERE C_Currency_ID = ?";
		String idrSymbol = DB.getSQLValueString(get_TrxName(), sql, m_session.getBase1Currency_ID());
		StringBuilder t2Build = new StringBuilder("Total ");
		t2Build.append(idrSymbol).append(" : ");
		String totalLabel = t2Build.toString();
		l_total.setText(totalLabel);
		l_base1.setText(totalB1Format);
		l_remaining.setText(remainingAmtFormat);
		l_payAmt.setText(payAmtFormat);
		l_roundAmt.setText(roundAmtFormat);
	}
	
	public void loadRefundDisplay ()
	{
		p_totalPanel.setBackground(new Color(192, 0, 0));
		p_totalPanel.setStroke(4, new Color(0, 176, 80));
	}
	
	public void loadSaleDisplay ()
	{
		p_totalPanel.setBackground(new Color(232,59,123));
		p_totalPanel.setStroke(4,  new Color(0,110,0));
	}
}
