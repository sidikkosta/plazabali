/**
 * 
 */
package com.uns.pos;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.compiere.apps.ADialog;
import org.compiere.grid.ed.VNumber;
import org.compiere.model.MConversionRate;
import org.compiere.model.MDocType;
import org.compiere.model.X_C_DocType;
import org.compiere.process.DocAction;
import org.compiere.swing.CButton;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CTabbedPane;
import org.compiere.swing.CTextField;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;

import com.unicore.model.MUNSCardTrxDetail;
import com.unicore.model.MUNSCardType;
import com.unicore.model.MUNSEDC;
import com.unicore.model.MUNSPOSConfigurationCurr;
import com.unicore.model.MUNSPOSPayment;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPaymentTrx;
import com.unicore.model.MUNSSessionCashAccount;
import com.unicore.model.X_UNS_POSPayment;
import com.uns.edc.IEDCPaymentProcessor;
import com.uns.model.MUNSEDCProcessorAction;

import net.miginfocom.swing.MigLayout;

/**
 * @author nurse
 *
 */
public class UNSPOSPaymentPanel extends UNSPOSSubPanel implements ListSelectionListener, ChangeListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2380215919061028083L;
	private MUNSPOSPayment m_posPayment = null;
	private UNSPOSPaymentDialog m_payDialog = null;
	private CTabbedPane m_tab;
	private CPanel p_cash;
	private CPanel p_card;
	private CPanel p_dCard;
	private CPanel p_wechat;
	private CPanel p_voucher;
	private CLabel l_remainingAmt;
	private CLabel l_edc;
	private CLabel l_payAmout;
	private CTextField f_accountNo;
	private CLabel l_accountNo;
	private CLabel l_voucherNo;
	private CLabel l_batchNo;
	private CLabel l_cardType;
	private CLabel l_currency;
	private VNumber f_remainingAmt;
	private CComboBox f_edc;
	private VNumber f_payAmount;
	private CTextField f_batchNo;
	private CComboBox f_cardType;
	private JList<AmountCurrencyPair> f_currencies;
	private Vector<KeyNamePair> m_currencies = null;
	private Vector<KeyNamePair> m_changeCurrencies = null;
	private UNSPOSTable m_table = null;
	private ImageIcon[] m_icons = new ImageIcon[] {
			new ImageIcon(getClass().getResource("/com/unicore/images/BOK.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/BCancelPayment.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/BDelete.png")),
			new ImageIcon(getClass().getResource("/com/unicore/images/BPrint.png"))	
	};
	UNSPOSPaymentRightPanel m_right;
	private int m_paymentTrxID = -1;
	long lastWhen = 0;

	/**
	 * @param posPanel
	 */
	public UNSPOSPaymentPanel(UNSPOSPanel posPanel, UNSPOSPaymentDialog dialog) 
	{
		super(posPanel);
		m_payDialog = dialog;
	}

	/* (non-Javadoc)
	 * @see com.uns.pos.UNSPOSSubPanel#init()
	 */
	@Override
	protected void init() 
	{
		setLayout(new MigLayout("fill", "[fill][fill, 300px!]", "[fill][fill]"));
		setBackground(new Color(221, 240, 200));
		
		loadCurrencies();
		f_edc = new CComboBox();
		ActionListener[] edclisListeners = f_edc.getActionListeners();
		for (int i=0; i<edclisListeners.length; i++)
			f_edc.removeActionListener(edclisListeners[i]);
		f_edc.addActionListener(this);
/*roby	f_edc.addActionListener (new ActionListener() {
			

			public void actionPerformed(ActionEvent e) {
			      
			KeyNamePair edcPair = (KeyNamePair) f_edc.getSelectedItem();
			        String edcname = edcPair.getName();
			     if  (edcname=="QRIS*") {
			    	 
			   for (int i=0; i<f_cardType.getItemCount(); i++)
				{
					KeyNamePair pair = (KeyNamePair) f_cardType.getItemAt(i);
					String name = pair.getName();
					if (name == null)
						name = "";
					if (name.toUpperCase().startsWith("QRIS") || name.toUpperCase().startsWith("QRIS"))
					{
						f_cardType.setSelectedIndex(i);
						break;
					}
				}
			     }
		    }
		}
		);

*/
		f_edc.addKeyListener(new MyKeyAdapter());
		f_remainingAmt = new VNumber();
		f_payAmount = new VNumber();
		f_accountNo = new CTextField();
		f_batchNo = new CTextField();
		f_cardType = new CComboBox();
		f_currencies = new JList<>();
		f_currencies.addListSelectionListener(this);
		f_remainingAmt.setReadWrite(false);
		Font fDetail = new Font("Calibri", Font.BOLD, 12);
		
		f_cardType.addActionListener(this);
			
		f_batchNo.addActionListener(this);
		f_accountNo.addKeyListener(new MyKeyAdapter());
		
		l_currency = new CLabel("");
		ImageIcon icon = new ImageIcon(getClass().getResource("/com/unicore/images/Currency.png"));
		Image img = icon.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_currency.setIcon(new ImageIcon(img));
		l_currency.setHorizontalAlignment(SwingConstants.TRAILING);
		
		l_edc = new CLabel("");
		ImageIcon icon4 = new ImageIcon(getClass().getResource("/com/unicore/images/EDC.png"));
		Image img4 = icon4.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_edc.setIcon(new ImageIcon(img4));
		l_edc.setHorizontalAlignment(SwingConstants.TRAILING);
		
		l_accountNo = new CLabel("");
		ImageIcon icon5 = new ImageIcon(getClass().getResource("/com/unicore/images/AccountNo.png"));
		Image img5 = icon5.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_accountNo.setIcon(new ImageIcon(img5));
		l_accountNo.setHorizontalAlignment(SwingConstants.TRAILING);
	
		l_batchNo = new CLabel("");
		ImageIcon icon6 = new ImageIcon(getClass().getResource("/com/unicore/images/BatchNo.png"));
		Image img6 = icon6.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_batchNo.setIcon(new ImageIcon(img6));
		l_batchNo.setHorizontalAlignment(SwingConstants.TRAILING);
		
		l_voucherNo = new CLabel("");
		ImageIcon icon7 = new ImageIcon(getClass().getResource("/com/unicore/images/VoucherNo.png"));
		Image img7 = icon7.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_voucherNo.setIcon(new ImageIcon(img7));
		l_voucherNo.setHorizontalAlignment(SwingConstants.TRAILING);
		
		f_currencies.setFont(fDetail);
		f_currencies.setForeground(Color.BLACK);
		
		f_currencies.addKeyListener(new MyKeyAdapter());
		
		ActionListener[] f_pListeners = f_payAmount.getListeners(ActionListener.class);
		for (int i=0; i<f_pListeners.length; i++)
			f_payAmount.removeActionListener(f_pListeners[i]);
		f_payAmount.addActionListener(this);
		
		l_remainingAmt = new CLabel("");
		ImageIcon icon2 = new ImageIcon(getClass().getResource("/com/unicore/images/RemainingAmt.png"));
		Image img2 = icon2.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_remainingAmt.setIcon(new ImageIcon(img2));
		l_remainingAmt.setHorizontalAlignment(SwingConstants.TRAILING);

		l_cardType = new CLabel("");
		ImageIcon icon8 = new ImageIcon(getClass().getResource("/com/unicore/images/CardType.png"));
		Image img8 = icon8.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_cardType.setIcon(new ImageIcon(img8));
		l_cardType.setHorizontalAlignment(SwingConstants.TRAILING);
		
		l_payAmout = new CLabel("");
		ImageIcon icon3 = new ImageIcon(getClass().getResource("/com/unicore/images/PaymentAmt.png"));
		Image img3 = icon3.getImage().getScaledInstance(100, 24, Image.SCALE_SMOOTH);
		l_payAmout.setIcon(new ImageIcon(img3));
		l_payAmout.setHorizontalAlignment(SwingConstants.TRAILING);
		
		m_tab = new CTabbedPane();
		p_cash = new CPanel(new MigLayout("", "[fill][fill]", ""));
		p_cash.setBackground(new Color(79, 129, 189));
		p_card = new CPanel(new MigLayout("","",""));
		p_card.setBackground(new Color(79, 129, 189));
		p_dCard = new CPanel(new MigLayout("","",""));
		p_dCard.setBackground(new Color(79, 129, 189));
		p_wechat = new CPanel(new MigLayout("", "", ""));
		p_wechat.setBackground(new Color(79, 129, 189));
		p_voucher = new CPanel(new MigLayout("", "", ""));
		p_voucher.setBackground(new Color(79, 129, 189));
		m_tab.addChangeListener(this);
		
		p_cash.setName("Cash");
		p_card.setName("Credit Card");
		p_dCard.setName ("Debit Card");
		p_wechat.setName("e-Money");
		p_voucher.setName("Voucher");
		
		m_tab.addTab("Cash    ", p_cash);
		m_tab.addTab("Credit Card", p_card);
		m_tab.addTab("Debit Card", p_dCard);
		m_tab.addTab("e-Money  ", p_wechat);
		m_tab.addTab("Voucher ", p_voucher);
		
		m_tab.setToolTipTextAt(0, "Alt + 1");
		m_tab.setToolTipTextAt(1, "Alt + 2");
		m_tab.setToolTipTextAt(2, "Alt + 3");
		m_tab.setToolTipTextAt(3, "Alt + 4");
		m_tab.setToolTipTextAt(4, "Alt + 5");
		
		Font hFont = new Font("Carlito", Font.ITALIC, 14);
		m_tab.setFont(hFont);
		
		m_tab.updateUI();
		add(m_tab);
		
		m_tab.setMnemonicAt(0, KeyEvent.VK_1);
		m_tab.setMnemonicAt(1, KeyEvent.VK_2);
		m_tab.setMnemonicAt(2, KeyEvent.VK_3);
		m_tab.setMnemonicAt(3, KeyEvent.VK_4);
		m_tab.setMnemonicAt(4, KeyEvent.VK_5);
		
		m_right = new UNSPOSPaymentRightPanel(m_posPanel, this);
		m_right.init();
		add(m_right, "wrap,span 0 3 , pushy");
		
		DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new String[]{"ID", "Pay Method","Currency", "Amount"});
		m_table = new UNSPOSTable();
		m_table.setModel(model);
		m_table.setFillsViewportHeight(true);
		m_table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		m_table.setRowHeight(30);
		m_table.setFocusable(true);
		m_table.setColumnVisibility(m_table.getColumn(0), false);
		m_table.getTableHeader().setBackground(new Color(79, 129, 189));
		JScrollPane scroll = new JScrollPane(m_table);
		m_table.getTableHeader().setPreferredSize(new Dimension(scroll.getWidth(), 24));
		m_table.getTableHeader().setForeground(Color.WHITE);
		m_table.setOddColor(new Color(208,216,232));
		m_table.setEvenColor(new Color(233, 237, 244));
		m_table.getSelectionModel().addListSelectionListener(this);
		
		add(scroll,"pushx, wrap");
		
		CPanel bPanel = new CPanel(new MigLayout("fill","[][][][]","fill"));
		int ke = 112;
		for (int i=0; i<m_icons.length; i++)
		{
			Image image = m_icons[i].getImage().getScaledInstance(80, 40, Image.SCALE_SMOOTH);
			CButton button = new CButton(new ImageIcon(image));
			button.setBorderPainted(false);
			button.setBackground(getBackground());
			button.setMargin(new Insets(0, 0, 0, 0));
			button.setActionCommand(Integer.toString(i));
			button.setToolTipText("F" + (i+1));
			button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(ke++, 0), Integer.toString(i));
			button.getActionMap().put(Integer.toString(i), new AbstractAction() {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = -6333879882950798774L;

				@Override
				public void actionPerformed(ActionEvent arg0) {
					UNSPOSPaymentPanel.this.actionPerformed(arg0);
				}
			});
			
			ActionListener[] listeners = button.getActionListeners();
			for (int x=0; x<listeners.length; x++) {
				button.removeActionListener(listeners[x]);
			}
			button.addActionListener(this);
			bPanel.add(button, "");
		}
		if (m_posPanel.isOnRefund())
			loadRefundDisplay();
		else
			loadSaleDisplay();
		updateTable();
		add(bPanel, "pushx");
	}
	
	private void onTabSelected ()
	{
		clear();
		if (m_tab.getSelectedIndex() == 0)
		{
			p_cash.removeAll();
			p_cash.add(l_currency, "center, top");
			p_cash.add(f_currencies, "flowx, growx,pushx, wrap, top");
			p_cash.add(l_remainingAmt, "center, top");
			p_cash.add(f_remainingAmt, "flowx, growx,pushx, wrap, top");
			p_cash.add(l_payAmout, "center, top");
			p_cash.add(f_payAmount, "flowx, growx,pushx, wrap, top");
			m_tab.setForegroundAt(0, Color.RED);
			p_cash.updateUI();
			f_currencies.requestFocus();
			initAmountCurrency();
		}
		else if (m_tab.getSelectedIndex() == 1)
		{
			loadEDC(MUNSPaymentTrx.PAYMENTMETHOD_Card);
			loadCardType(MUNSPaymentTrx.PAYMENTMETHOD_Card);
			p_card.removeAll();
			p_card.add(l_remainingAmt, "center, top");
			p_card.add(f_remainingAmt, "flowx, growx,pushx, wrap, top");
			p_card.add(l_edc, "center, top");
			p_card.add(f_edc, "flowx, growx,pushx, wrap, top");
			p_card.add(l_payAmout, "center, top");
			p_card.add(f_payAmount, "flowx, growx,pushx, wrap, top");
			p_card.add(l_cardType, "center, top");
			p_card.add(f_cardType, "flowx, growx,pushx, wrap, top");
			p_card.add(l_batchNo, "center, top");
			p_card.add(f_batchNo, "flowx, growx,pushx, wrap, top");
			f_edc.requestFocus();
			m_tab.setForegroundAt(1, Color.RED);
			p_card.updateUI();
		}
		else if (m_tab.getSelectedIndex() == 2)
		{
			loadEDC(MUNSPaymentTrx.PAYMENTMETHOD_Card);
			loadCardType(MUNSPaymentTrx.PAYMENTMETHOD_Card);
			p_dCard.removeAll();
			p_dCard.add(l_remainingAmt, "center, top");
			p_dCard.add(f_remainingAmt, "flowx, growx,pushx, wrap, top");
			p_dCard.add(l_edc, "center, top");
			p_dCard.add(f_edc, "flowx, growx,pushx, wrap, top");
			p_dCard.add(l_payAmout, "center, top");
			p_dCard.add(f_payAmount, "flowx, growx,pushx, wrap, top");
//			p_dCard.add(l_cardType, "center, top");
//			p_card.add(f_cardType, "flowx, growx,pushx, wrap, top");
			p_dCard.add(l_batchNo, "center, top");
			p_dCard.add(f_batchNo, "flowx, growx,pushx, wrap, top");
			f_edc.requestFocus();
			m_tab.setForegroundAt(2, Color.RED);
			p_dCard.updateUI();
		}
		else if (m_tab.getSelectedIndex() == 3)
		{
			loadEDC(MUNSPaymentTrx.PAYMENTMETHOD_Wechat);
			loadCardType(MUNSPaymentTrx.PAYMENTMETHOD_Wechat);
			p_wechat.removeAll();
			p_wechat.add(l_remainingAmt, "center, top");
			p_wechat.add(f_remainingAmt, "flowx, growx,pushx, wrap, top");
			p_wechat.add(l_edc, "center, top");
			p_wechat.add(f_edc, "flowx, growx,pushx, wrap, top");
			p_wechat.add(l_accountNo, "center, top");
			p_wechat.add(f_accountNo, "flowx, growx,pushx, wrap, top");
			p_wechat.add(l_payAmout, "center, top");
			p_wechat.add(f_payAmount, "flowx, growx,pushx, wrap, top");
/*roby		p_wechat.add(l_cardType, "center, top");
			p_wechat.add(f_cardType, "flowx, growx,pushx, wrap, top");
*/			p_wechat.add(l_batchNo, "center, top");
			p_wechat.add(f_batchNo, "flowx, growx,pushx, wrap, top");
			m_tab.setForegroundAt(3, Color.RED);
			p_wechat.updateUI();
			for (int i=0; i<f_edc.getItemCount(); i++)
			{
				KeyNamePair pair = (KeyNamePair) f_edc.getItemAt(i);
				String name = pair.getName();
				if (name == null)
					name = "";
				if (name.toUpperCase().startsWith("QRIS") || name.toUpperCase().startsWith("QRIS"))
				{
					f_edc.setSelectedIndex(i);
					break;
				}
			}
			for (int i=0; i<f_cardType.getItemCount(); i++)
			{
				KeyNamePair pair = (KeyNamePair) f_cardType.getItemAt(i);
				String name = pair.getName();
				if (name == null)
					name = "";
				if (name.toUpperCase().startsWith("QRIS") || name.toUpperCase().startsWith("QRIS"))
				{
					f_cardType.setSelectedIndex(i);
					break;
				}
			}
			if (f_edc.getSelectedIndex() != -1)
				f_accountNo.requestFocus();
			else 
				f_edc.requestFocus();
		}
		else if (m_tab.getSelectedIndex() == 4)
		{
			p_voucher.removeAll();
			p_voucher.add(l_currency, "center, top");
			p_voucher.add(f_currencies, "flowx, growx,pushx, wrap, top");
			p_voucher.add(l_remainingAmt, "center, top");
			p_voucher.add(f_remainingAmt, "flowx, growx,pushx, wrap, top");
			p_voucher.add(l_voucherNo, "center, top");
			p_voucher.add(f_accountNo, "flowx, growx,pushx, wrap, top");
			p_voucher.add(l_payAmout, "center, top");
			p_voucher.add(f_payAmount, "flowx, growx,pushx, wrap, top");
			m_tab.setForegroundAt(4, Color.RED);
			p_voucher.updateUI();
			
			BigDecimal remaining = m_posPanel.m_posTrx.getGrandTotal();
			int from = m_posPanel.m_posTrx.getC_Currency_ID();
			if (getPayment() != null)
			{
				getPayment().load(get_TrxName());
				remaining = getPayment().getPOSAmount();
				remaining = remaining.subtract(getPayment().getPaidAmt()).add(getPayment().getRoundingAmt());
				from = m_posPayment.getC_Currency_ID();
			}
			BigDecimal payable = MConversionRate.convert(
					getCtx(), remaining, from, 303, m_posPanel.m_posTrx.getDateTrx(), 0, 
					m_posPanel.m_posTrx.getAD_Client_ID(), m_posPanel.m_posTrx.getAD_Org_ID(), RoundingMode.UP, 0);
			f_remainingAmt.setValue(payable);
			f_currencies.requestFocus();
			initAmountCurrency();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadEDC (String paymethod)
	{
		f_edc.removeAllItems();
		String sql = "SELECT UNS_EDC_ID, Name FROM UNS_EDC WHERE IsActive = ? AND UNS_POSTerminal_ID = ?";
		if (!Util.isEmpty(paymethod, true))
		{
			if (paymethod.equals(MUNSPaymentTrx.PAYMENTMETHOD_Card))
				sql += " AND UPPER (Name) Not Like '%WECHAT%' AND UPPER (Name) Not Like '%WE CHAT%' AND UPPER (Name) Not Like '%*'";
			else if (paymethod.equals(MUNSPaymentTrx.PAYMENTMETHOD_Wechat))
				sql += " AND (UPPER (Name) like '%WECHAT%' OR UPPER (Name) Like '%WE CHAT%' OR UPPER (Name) Like '%*') ";
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setString(1, "Y");
			st.setInt(2, m_posPanel.m_terminal.getUNS_POSTerminal_ID());
			rs = st.executeQuery();
			while (rs.next())
			{
				f_edc.addItem(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ADialog.error(m_posPanel.getWindowNo(), this, ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadCardType(String paymethod)
	{
		f_cardType.removeAllItems();
		String sql = "SELECT UNS_CardType_ID, Name FROM UNS_CardType WHERE IsActive = ?";
		if (!Util.isEmpty(paymethod, true))
		{
			if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(paymethod))
			{
				sql += " AND UPPER (Name) Not LIKE '%WECHAT%' AND UPPER (Name) Not Like '%WE CHAT%' AND UPPER (Name) Not Like '%*'";
			}
			else if (MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(paymethod))
			{
				sql += " AND (UPPER (Name) LIKE '%WECHAT%' OR UPPER (Name) Like '%WE CHAT%' OR UPPER (Name) Like '%*')";
			}
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setString(1, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				f_cardType.addItem(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ADialog.error(m_posPanel.getWindowNo(), this, ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	private void initAmountCurrency ()
	{
		DefaultListModel<AmountCurrencyPair> model = new DefaultListModel<>();
		BigDecimal remaining = m_posPanel.m_posTrx.getGrandTotal();
		int from = m_posPanel.m_posTrx.getC_Currency_ID();
		if (getPayment() != null)
		{
			getPayment().load(get_TrxName());
			remaining = getPayment().getPOSAmount();
			remaining = remaining.subtract(getPayment().getPaidAmt()).add(getPayment().getRoundingAmt());
			from = m_posPayment.getC_Currency_ID();
		}
		if (m_posPanel.isOnRefund() && m_posPanel.getOriginalTrx().getDateAcct().equals(m_session.getDateAcct())
				&& remaining.signum() == -1)
		{
			String paymethod = "0";
			int selected = m_tab.getSelectedIndex();
			if (selected == 4)
				paymethod = "3";
			
			String sql = "SELECT C_Currency_ID, SUM(trxAmt) FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID = ("
					+ " SELECT UNS_POSPayment_ID FROM UNS_POSPayment WHERE UNS_POSTrx_ID = ?) AND isReceipt = 'Y' "
					+ " AND PaymentMethod = ? GROUP BY C_Currency_ID";
			String sql2 = "SELECT C_Currency_ID, SUM (trxAmt) FROM UNS_PaymentTrx "
					+ " WHERE UNS_POSPayment_ID IN (SELECT UNS_POSPayment_ID FROM UNS_POSPayment "
					+ " WHERE UNS_POSTrx_ID IN (SELECT UNS_POSTrx_ID FROM UNS_POSTrx "
					+ " WHERE Reference_ID = ?)) AND PaymentMethod = ? AND IsReceipt = 'N' GROUP BY C_Currency_ID";
			
			List<List<Object>> objs = DB.getSQLArrayObjectsEx(
					get_TrxName(), sql, m_posPanel.getOriginalTrx().get_ID(), paymethod);
			List<List<Object>> objs2 = DB.getSQLArrayObjectsEx(
					get_TrxName(), sql2, m_posPanel.getOriginalTrx().get_ID(), paymethod);
			if (objs == null)
				objs = new ArrayList<>();
			if (objs2 == null)
				objs2 = new ArrayList<>();
				
			for (int i=0; i<m_currencies.size(); i++)
			{
				boolean allow = false;
				BigDecimal convertedAmt = MConversionRate.convert(getCtx(), remaining, 
						from, m_currencies.get(i).getKey(), 
						m_posPanel.m_posTrx.getDateTrx(), 0, 
						m_posPanel.m_posTrx.getAD_Client_ID(), 
						m_posPanel.m_posTrx.getAD_Org_ID());
				if (convertedAmt == null)
					convertedAmt = Env.ZERO;
				
				for (int j=0; j<objs.size(); j++)
				{
					if (m_currencies.get(i).getKey() == Integer.valueOf(objs.get(j).get(0).toString()))
					{
						BigDecimal refundable = (BigDecimal) objs.get(j).get(1);
						for (int k=0; k<objs2.size(); k++)
						{
							int x = Integer.valueOf(objs2.get(k).get(0).toString());
							int y = Integer.valueOf(objs.get(j).get(0).toString());
							if (x == y)
							{
								refundable = refundable.abs().subtract(((BigDecimal) objs2.get(k).get(1)).abs());
								break;
							}
						}
						if (convertedAmt.abs().compareTo(refundable.abs()) == 1)
							convertedAmt = refundable;
						allow = true;
						break;
					}
				}

				if (!allow)
					continue;

				convertedAmt = convertedAmt.abs().negate();
				
				BigDecimal balance = MUNSSessionCashAccount.getBalance(
						get_TrxName(), m_session.get_ID(), m_currencies.get(i).getKey());
				if (balance.signum() == -1)
					balance = Env.ZERO;
				if (convertedAmt.abs().compareTo(balance) == 1)
					convertedAmt = balance.negate();
				
				AmountCurrencyPair pair = new AmountCurrencyPair(
						m_currencies.get(i).getKey(), m_currencies.get(i).getName(), 
						convertedAmt);
				model.addElement(pair);
			}
		}
		if (remaining.signum() == -1 || m_posPanel.isOnRefund())
		{
			boolean allAmountIsZero = true;
			if (m_posPanel.isOnRefund())
			{
				for (int j=0; j<model.size(); j++)
					if (model.get(j).getAmount().signum() != 0)
					{
						allAmountIsZero = false;
						break;
					}
			}
			for (int i=0; i<m_changeCurrencies.size() && allAmountIsZero; i++)
			{
				boolean tobeContinued = false;
				for (int j=0; j<model.size(); j++)
					if (model.get(j).getKey() == m_changeCurrencies.get(i).getKey())
					{
						tobeContinued = true;
						break;
					}
				if (tobeContinued)
					continue;
				BigDecimal convertedAmt = MConversionRate.convert(getCtx(), remaining, 
						from, m_changeCurrencies.get(i).getKey(), 
						m_posPanel.m_posTrx.getDateTrx(), 0, 
						m_posPanel.m_posTrx.getAD_Client_ID(), 
						m_posPanel.m_posTrx.getAD_Org_ID(), RoundingMode.DOWN, 0);
				if (convertedAmt == null)
					convertedAmt = Env.ZERO;
				BigDecimal minAmt = MUNSPOSConfigurationCurr.getMinimumTrxAmt(
						m_posPanel.m_posTrx.getAD_Org_ID(), m_changeCurrencies.get(i).getKey(), get_TrxName());
				if (minAmt.signum() > 0 && convertedAmt.abs().compareTo(minAmt) == -1)
				{
					convertedAmt = Env.ZERO;
				}
				if (m_tab.getSelectedIndex() == 0 )
				{
					BigDecimal balance = MUNSSessionCashAccount.getBalance(
							get_TrxName(), m_session.get_ID(), m_changeCurrencies.get(i).getKey());
					if (balance.signum() == -1)
						balance = Env.ZERO;
					if (convertedAmt.abs().compareTo(balance) == 1)
						convertedAmt = balance.negate();
				}
				AmountCurrencyPair pair = new AmountCurrencyPair(
						m_changeCurrencies.get(i).getKey(), m_changeCurrencies.get(i).getName(), 
						convertedAmt);
				model.addElement(pair);
			}
		}
		else
		{
			for (int i=0; i<m_currencies.size(); i++)
			{
				BigDecimal convertedAmt = MConversionRate.convert(getCtx(), remaining, 
						from, m_currencies.get(i).getKey(), 
						m_posPanel.m_posTrx.getDateTrx(), 0, 
						m_posPanel.m_posTrx.getAD_Client_ID(), 
						m_posPanel.m_posTrx.getAD_Org_ID(), RoundingMode.UP, 0);
				if (convertedAmt == null)
					convertedAmt = Env.ZERO;
				AmountCurrencyPair pair = new AmountCurrencyPair(
						m_currencies.get(i).getKey(), m_currencies.get(i).getName(), 
						convertedAmt);
				model.addElement(pair);
			}
		}
		
		f_currencies.setModel(model);
	}
	
	public void loadCurrencies ()
	{
		m_currencies = new Vector<>();
		m_changeCurrencies = new Vector<>();
		String sql = "SELECT C_Currency_ID, (SELECT Description FROM C_Currency WHERE C_Currency_ID = "
				+ " UNS_SessionCashAccount.C_Currency_ID), IsUsedForChange FROM UNS_SessionCashAccount "
				+ "WHERE UNS_POS_Session_ID = ? AND IsActive = ? ";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, m_session.getUNS_POS_Session_ID());
			st.setString(2, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				KeyNamePair curr = new KeyNamePair(rs.getInt(1), rs.getString(2));
				m_currencies.add(curr);
				if ("Y".equals(rs.getString(3)))
					m_changeCurrencies.add(curr);
			}
		}
		catch (SQLException ex)
		{
			ADialog.error(m_posPanel.getWindowNo(), this, ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	public MUNSPOSPayment getPayment ()
	{
		if (m_posPayment == null)
		{
			m_posPayment = MUNSPOSPayment.get(getCtx(), 
					m_posPanel.m_posTrx.getUNS_POSTrx_ID(), get_TrxName());
			if (m_posPayment != null)
			{
				m_posPayment.setPOSAmount(m_posPanel.m_posTrx.getGrandTotal());
				m_posPayment.save();
			}
		}
		
		return m_posPayment;
	}
	
	public MUNSPOSPayment getCreatePayment ()
	{
		if (getPayment() == null)
		{
			m_posPayment = new MUNSPOSPayment(m_posPanel.m_posTrx);
			m_posPayment.setSalesRep_ID(m_posPanel.m_posTrx.getSalesRep_ID());
			int trxDocType_ID = m_posPanel.m_posTrx.getC_DocType_ID();
			String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?"; //$NON-NLS-1$
			String docBaseType = DB.getSQLValueString(get_TrxName(), sql, trxDocType_ID);
			int C_DocTYpe_ID = -1;
			if (X_C_DocType.DOCBASETYPE_POSSales.equals(docBaseType))
			{
				C_DocTYpe_ID = MDocType.getDocType(X_C_DocType.DOCBASETYPE_ARPOSReceipt);
			}
			else if (X_C_DocType.DOCBASETYPE_POSReturn.equals(docBaseType))
			{
				C_DocTYpe_ID = MDocType.getDocType(X_C_DocType.DOCBASETYPE_ARPOSCreditMemo);
			}
			m_posPayment.setC_DocType_ID (C_DocTYpe_ID);
			m_posPayment.setDateAcct (m_posPanel.m_posTrx.getDateAcct());
			m_posPayment.setDateDoc (m_posPanel.m_posTrx.getDateDoc());
			m_posPayment.setDateTrx (m_posPanel.m_posTrx.getDateTrx());
			m_posPayment.setDocAction (X_UNS_POSPayment.DOCACTION_Complete);
			m_posPayment.setDocStatus (X_UNS_POSPayment.DOCSTATUS_Drafted);
			m_posPayment.setIsSOTrx (true);
			m_posPayment.setPOSAmount(m_posPanel.m_posTrx.getGrandTotal());
			if (!m_posPayment.save())
			{
				m_posPayment = null;
			}
		}
		
		return m_posPayment;
	}
	
	private String createUpdateTransaction (int paymentTrxID)
	{
		MUNSPaymentTrx trx = MUNSPaymentTrx.get(get_TrxName(), paymentTrxID);
		if (trx == null)
		{
			MUNSPOSPayment payment = getCreatePayment();
			if (payment == null)
				return "Failed when try to create payment";
			trx = new MUNSPaymentTrx(payment);
		}
		String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?";
		String docBaseType = DB.getSQLValueString(get_TrxName(), sql, getPayment().getC_DocType_ID());
		String paymentMethod = null;
		int selected = m_tab.getSelectedIndex();
		String trxType = null;
		switch (selected)
		{
			case 1 : 
				paymentMethod = MUNSPaymentTrx.PAYMENTMETHOD_Card;
				trxType = MUNSPaymentTrx.TRXTYPE_EDCIn;
				break;
			case 2 : 
				paymentMethod = MUNSPaymentTrx.PAYMENTMETHOD_Card;
				trxType = MUNSPaymentTrx.TRXTYPE_EDCIn;
				break;
			case 3 : 
				paymentMethod = MUNSPaymentTrx.PAYMENTMETHOD_Wechat;
				trxType = MUNSPaymentTrx.TRXTYPE_WechatIn;
				break;
			case 4 :
				paymentMethod = MUNSPaymentTrx.PAYMENTMETHOD_Voucher;
				trxType = MUNSPaymentTrx.TRXTYPE_VoucherSales;
				break;
			default :
				paymentMethod = MUNSPaymentTrx.PAYMENTMETHOD_Cash;
				trxType = MUNSPaymentTrx.TRXTYPE_CashIn;
				break;
		}
		
		BigDecimal amount = (BigDecimal) f_payAmount.getValue();
		if (amount == null || amount.signum() == 0)
			return null;
		int currencyID = 0;
		if (MUNSPaymentTrx.PAYMENTMETHOD_Cash.equals(paymentMethod)
				|| MUNSPaymentTrx.PAYMENTMETHOD_Voucher.equals(paymentMethod))
		{
			AmountCurrencyPair amtPair = ((AmountCurrencyPair) f_currencies.getSelectedValue());
			if (amtPair != null)
			{
				currencyID = amtPair.getKey();
			}
		}
		else if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(paymentMethod)
				|| MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(paymentMethod))
		{
			KeyNamePair edcPair = (KeyNamePair) f_edc.getSelectedItem();
			if (edcPair == null)
				return "No EDC Selected";
			String sql2 = "SELECT C_Currency_ID FROM C_BankAccount WHERE C_BankAccount_ID = "
					+ " (SELECT C_BankAccount_ID FROM UNS_EDC WHERE UNS_EDC_ID = ?)";
			currencyID = DB.getSQLValue(get_TrxName(), sql2, edcPair.getKey());
		}

		if (currencyID <= 0)
			return "No currency selected";
		
		boolean isReceipt = true;
		amount = amount.abs();
		
		if (MDocType.DOCBASETYPE_ARPOSReceipt.equals(docBaseType))
		{
			BigDecimal posAmt = getPayment().getPOSAmount();
			BigDecimal roundAmt = getPayment().getRoundingAmt();
			BigDecimal paidAmt = getPayment().getPaidAmt();
			BigDecimal remaining = posAmt.subtract(paidAmt).add(roundAmt);
			if (remaining.signum() == -1)
			{
				if (!MUNSPaymentTrx.PAYMENTMETHOD_Cash.equals(paymentMethod))
				{
					return "Payment Method of change must be set to Cash";
				}

				trxType = MUNSPaymentTrx.TRXTYPE_Change;
				isReceipt = false;
			}
		}
		else
		{
			if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(paymentMethod))
			{
				trxType = MUNSPaymentTrx.TRXTYPE_RefundEDC;
			}
			else if (MUNSPaymentTrx.PAYMENTMETHOD_Voucher.equals(paymentMethod))
			{
				trxType =  MUNSPaymentTrx.TRXTYPE_VoucherSales;
			}
			else if (MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(paymentMethod))
			{
				trxType = MUNSPaymentTrx.TRXTYPE_RefundWechat;
			}
			else
			{
				trxType = MUNSPaymentTrx.TRXTYPE_Refund;
			}
			

			isReceipt = false;
			if (getPayment() != null)
			{
				BigDecimal remaining = getPayment().getPOSAmount().subtract(getPayment().getPaidAmt()).
						add(getPayment().getRoundingAmt());
				if (remaining.signum() == 1)
					isReceipt = true;
			}
		}	
		
		trx.setUNS_POS_Session_ID(m_session.get_ID());
		trx.setPaymentMethod(paymentMethod);
		trx.setC_Currency_ID(currencyID);
		trx.setTrxType(trxType);
		trx.setAmount(amount);
		trx.setIsReceipt(isReceipt);
		trx.setTrxNo(f_accountNo.getText());
		
		boolean isSameSession = true;
		boolean isManualWechatRefund = false;
		if (m_posPanel.isOnRefund())
		{
			int oriSession = m_posPanel.getOriginalTrx().getUNS_POS_Session_ID();
			int curSession = m_posPanel.m_posTrx.getUNS_POS_Session_ID();
			isSameSession = oriSession == curSession;
			if (MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(paymentMethod))
			{
				isManualWechatRefund = ADialog.ask(m_posPanel.getWindowNo(), this, 
						"Manualy refund transaction ?\nIf manual, you must process refund on wechat server manualy. QR Code : " + f_accountNo.getText());
			}
		}
		
		if (!isSameSession || isManualWechatRefund)
			trx.setIsManual(true);
	
		
		if (!MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(paymentMethod)
				&& !MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(paymentMethod))
		{
			if (!trx.save())
				return CLogger.retrieveErrorString("Failed when try to save payment transaction");
			return null;
		}
		
		
		MUNSCardTrxDetail detail = MUNSCardTrxDetail.get(trx);
		if (detail == null)
		{
			detail = new MUNSCardTrxDetail(trx);
		}
		
		int edcID = ((KeyNamePair) f_edc.getSelectedItem()).getKey();
		detail.setUNS_EDC_ID(edcID);

		MUNSCardTrxDetail originalDetail = null;
		if (m_posPanel.isOnRefund())
		{
			MUNSPOSTrx originalTrx = m_posPanel.getOriginalTrx();
			MUNSPOSPayment originalPay = MUNSPOSPayment.get(getCtx(), originalTrx.get_ID(), get_TrxName());
			MUNSPaymentTrx[] originalPayTrxs = originalPay.getTrx();
			for (int i=0; i<originalPayTrxs.length; i++)
			{
				if (!MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(originalPayTrxs[i].getPaymentMethod())
						&&!MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(originalPayTrxs[i].getPaymentMethod()))
					continue;
				originalDetail = MUNSCardTrxDetail.get(originalPayTrxs[i]);
				if (detail == null)
					continue;
				if (originalDetail.getUNS_EDC_ID() == detail.getUNS_EDC_ID())
				{
					break;
				}
			}
			if (originalDetail == null)
			{
				return "Could not find original payment document";
			}
			detail.setOriginalInvoiceNo(originalDetail.getInvoiceNo());
			detail.setUNS_CardType_ID(originalDetail.getUNS_CardType_ID());
		}
		if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(paymentMethod))
		{
			KeyNamePair pair = (KeyNamePair) f_cardType.getSelectedItem();
			if (pair != null)
			{
				int id = pair.getKey();
				detail.setUNS_CardType_ID(id);
			}
		}

		detail.setBatchNo(f_batchNo.getText());
		
		if (edcID > 0 && !trx.isManual() && !MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(paymentMethod) )
		{
			MUNSEDC edc = new MUNSEDC(getCtx(), edcID, get_TrxName());
			IEDCPaymentProcessor processor = edc.getOnlinePayProcessor();
			String action = MUNSEDCProcessorAction.ACTION_SALE;
			if (m_posPanel.isOnRefund())
				action = MUNSEDCProcessorAction.ACTION_VOID;
			if (processor == null)
				return "Could not find EDC processor for " + edc.getName();
			try
			{
				if (!processor.process(detail, action))
				{
					int retVal = JOptionPane.showConfirmDialog(
							this, "Could not process online transaction, manualy process?", "Can't process online payment", JOptionPane.YES_NO_OPTION);
					if (retVal != JOptionPane.YES_OPTION)
					{
						return "Could not process online transaction";
					}
					
					trx.setIsManual(true);
					
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				return "Fatal Error || " + ex.getMessage();
			}
			
			if (!trx.save())
				return CLogger.retrieveErrorString("Failed when try to save payment transaction");
			if (detail.getUNS_PaymentTrx_ID() == 0)
				detail.setUNS_PaymentTrx_ID(trx.get_ID());
			m_paymentTrxID = trx.get_ID();
			try 
			{
				DB.commit(true, get_TrxName());
			} 
			catch (IllegalStateException e) 
			{
				e.printStackTrace();
			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		
		if (originalDetail != null)
		{
			if (isSameSession)
			{
				originalDetail.setBatchNo("VO");
				if (!originalDetail.save())
					return "Could not update original transaction";
				detail.setBatchNo("VO");
			}
			else
			{
				detail.setBatchNo("0");
			}
		}
		else
		{
			if (selected == 2)
			{
				int ctID = MUNSCardType.getIDByNameOrValue(get_TrxName(), "DC");
				detail.setUNS_CardType_ID(ctID);
			}
			else if (selected == 3)
			{

			KeyNamePair edcPair = (KeyNamePair) f_edc.getSelectedItem();
		    String edcname = edcPair.getName();
		    
		    
		     if  (edcname.toUpperCase().startsWith(edcname)) {

		    	sql = "SELECT UNS_CardType_ID FROM UNS_CardType WHERE  Name Like ?";
		    	
				int ctID = DB.getSQLValue(get_TrxName(), sql,  edcname + "%");
				detail.setUNS_CardType_ID(ctID);
		     }
		/*     if  (edcname.toUpperCase().startsWith("QRIS")) {

			    	sql = "SELECT UNS_CardType_ID FROM UNS_CardType WHERE Value = ? OR Name Like ?";
					int ctID = DB.getSQLValue(get_TrxName(), sql, "QR", "%QR%");
				detail.setUNS_CardType_ID(ctID);
		     }
*/
		     /*roby		sql = "SELECT UNS_CardType_ID FROM UNS_CardType WHERE Value = ? OR Name Like ?";
				int ctID = DB.getSQLValue(get_TrxName(), sql, "WC", "%Wechat%");
				if (ctID > 0)
					detail.setUNS_CardType_ID(ctID);
	*/			
/*				int ctID = m_posPanel.getEDCCardType(detail.getAccountNo());

				if (ctID == -1 && f_cardType.getSelectedIndex() != -1)
					ctID = ((KeyNamePair)f_cardType.getSelectedItem()).getKey();
				
				detail.setUNS_CardType_ID(ctID);
*/
				
			}
			else if(!trx.isManual())
			{
				int ctID = m_posPanel.getEDCCardType(detail.getAccountNo());
				if (ctID == -1 && f_cardType.getSelectedIndex() != -1)
					ctID = ((KeyNamePair)f_cardType.getSelectedItem()).getKey();
				
				detail.setUNS_CardType_ID(ctID);
			}
		}
		
		if (!trx.save())
			return CLogger.retrieveErrorString("Failed when try to save payment transaction");
		if (detail.getUNS_PaymentTrx_ID() == 0)
			detail.setUNS_PaymentTrx_ID(trx.get_ID());
		
		if (!detail.save())
		{
			if (detail.getUNS_CardType_ID() == 0)
				f_cardType.requestFocus();
			else if (Util.isEmpty(detail.getBatchNo(), true))
				f_batchNo.requestFocus();
			return CLogger.retrieveErrorString("Could not save Card Trx Datail");
		}
		
		return null;
	}
	
	private void updateTable ()
	{
		DefaultTableModel model = ((DefaultTableModel) m_table.getModel());
		model.setRowCount(0);
		if (getPayment() != null)
		{
			String sql = "SELECT UNS_PaymentTrx_ID, PaymentMethod, C_Currency_ID, Amount, isReceipt FROM UNS_PaymentTrx "
					+ " WHERE UNS_POSPayment_ID = ?";
			PreparedStatement st;
			ResultSet rs;
			try
			{
				st = DB.prepareStatement(sql, null);
				st.setInt(1, getPayment().get_ID());
				rs = st.executeQuery();
				int row = m_table.getRowCount();
				while (rs.next()) {
					model.setRowCount(row+1);
					int id = rs.getInt(1);
					String payMethod = rs.getString(2);
					int currencyID = rs.getInt(3);
					BigDecimal amount = rs.getBigDecimal(4);
					boolean isReceipt = "Y".equals(rs.getString(5));
					for (int i=0; i<m_tab.getTabCount(); i++) 
					{
						int pmi = i;
						if (i > 1)
							pmi = i-1;
						String x = Integer.toString(pmi);
						if (x.equals(payMethod))
						{
							if (x.equals("1"))
								payMethod = "C/D Card";
							else if (x.equals("0") && !m_posPanel.isOnRefund() && !isReceipt)
								payMethod = "Change";
							else if (x.equals("0") && m_posPanel.isOnRefund() && isReceipt)
								payMethod = "Refund Change";
							else
								payMethod = m_tab.getTitleAt(i);
							break;
						}
					}
					String currency = "";
					for (int i=0; i<m_currencies.size(); i++) {
						KeyNamePair pair = (KeyNamePair)m_currencies.get(i);
						if (pair.getKey() == currencyID)
						{
							currency = pair.getName();
							break;
						}
					}
					String amtStr = NumberFormat.getInstance().format(amount);
					m_table.setValueAt(id, row, 0);
					m_table.setValueAt(payMethod, row, 1);
					m_table.setValueAt(currency, row, 2);
					m_table.setValueAt(amtStr, row, 3);
					row++;
				}
				st.close();
				rs.close();
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
		}
		
		int rowcount = model.getRowCount();
		while (rowcount < 12)
		{
			model.setRowCount(rowcount+1);
			m_table.setValueAt(null, rowcount, 0);
			m_table.setValueAt(null, rowcount, 1);
			m_table.setValueAt(null, rowcount, 2);
			m_table.setValueAt(null, rowcount, 3);
			rowcount = model.getRowCount();
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) 
	{
		if (arg0.getSource().equals(f_currencies))
		{
			onCurrencySelected();
			return;
		}
//		if (!arg0.getValueIsAdjusting())
//			return;
		int selected = m_table.getSelectedRow();
		if (selected == -1)
		{
			clear();
			return;
		}
		Object oo = m_table.getValueAt(selected, 0);
		int id = -1;
		if (oo == null)
		{
			clear();
			return;
		}
		id = (int) oo;
		MUNSPaymentTrx trx = MUNSPaymentTrx.get(get_TrxName(), id);
		String payMethod = trx.getPaymentMethod();
		if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(payMethod))
		{
			String sql = "SELECT CONCAT(Value, '#', Name) FROM UNS_CardType WHERE "
					+ " UNS_CardType_ID IN (SELECT UNS_CardType_ID FROM UNS_CardTrxDetail "
					+ " WHERE UNS_PaymentTrx_ID = ?)";
			String valName = DB.getSQLValueString(get_TrxName(), sql, trx.getUNS_PaymentTrx_ID());
			int idx = -1;
			String[] valNames;
			if (valName != null && (valNames = valName.split("#")).length == 2)
			{
				String val = valNames[0];
				String name = valNames[1];
				if ("DC".equals(val) || "DEBIT CARD".equals(name.toUpperCase()))
					idx = 2;
			}
			if (idx == -1)
				idx = 1;
			m_tab.setSelectedIndex(idx);
		}
		else if (MUNSPaymentTrx.PAYMENTMETHOD_Cash.equals(payMethod))
		{
			m_tab.setSelectedIndex(0);
		}
		else if (MUNSPaymentTrx.PAYMENTMETHOD_Voucher.equals(payMethod))
		{
			m_tab.setSelectedIndex(4);
		}
		else if (MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(payMethod))
		{
			m_tab.setSelectedIndex(3);
		}
		for (int i=0; i<f_currencies.getModel().getSize(); i++)
		{
			if (((AmountCurrencyPair) f_currencies.getModel().getElementAt(i)).getKey() == trx.getC_Currency_ID())
			{
				f_currencies.setSelectedIndex(i);
				break;
			}
		}
		
		f_accountNo.setText(trx.getTrxNo());
		MUNSCardTrxDetail detail = MUNSCardTrxDetail.get(trx);
		if (detail != null)
		{
			for (int i=0; i<f_edc.getItemCount(); i++)
			{
				if (((KeyNamePair) f_edc.getItemAt(i)).getKey() == detail.getUNS_EDC_ID())
				{
					f_edc.setSelectedIndex(i);
					break;
				}
			}
			for (int i=0; i<f_cardType.getItemCount(); i++)
			{
				if (((KeyNamePair) f_cardType.getItemAt(i)).getKey() == detail.getUNS_CardType_ID())
				{
					f_cardType.setSelectedIndex(i);
					break;
				}
			}
			f_batchNo.setText(detail.getBatchNo());
		}

		setPayAmount(trx.getC_Currency_ID(), trx.getAmount());
		m_paymentTrxID = id;
	}
	
	private boolean isOnClear = false;
	
	private void clear ()
	{
		for (int i=0; i<m_tab.getTabCount(); i++)
			if (m_tab.getSelectedIndex() != i)
				m_tab.setForegroundAt(i, Color.BLACK);
		isOnClear = true;
		f_batchNo.setText("");
		f_cardType.setSelectedIndex(-1);
		f_currencies.setSelectedIndex(-1);
		f_edc.setSelectedIndex(-1);
		f_payAmount.setValue(0);
		f_remainingAmt.setValue(0);
		f_accountNo.setText("");
		m_paymentTrxID = -1;
		isOnClear = false;
	}
	
	private void onCurrencySelected ()
	{
		AmountCurrencyPair pair = (AmountCurrencyPair) f_currencies.getSelectedValue();
		if (pair == null)
			return;
		f_remainingAmt.setValue(pair.getAmount());
		setPayAmount(pair.getKey(), pair.getAmount());
	}
	
	@Override
	public void actionPerformed (ActionEvent e)
	{
		m_posPanel.checkSession();
		if (isOnClear)
			return;
		AWTEvent currentEvent = EventQueue.getCurrentEvent();
		if (currentEvent instanceof FocusEvent)
			return;
		String actionCommand = e.getActionCommand();
		if (e.getSource() instanceof JButton)
		{
			actionCommand = ((JButton) e.getSource()).getActionCommand();
		}
		else if (currentEvent instanceof KeyEvent)
		{
			KeyEvent event = (KeyEvent) currentEvent;
			if (event.isConsumed())
				return;
			long when = event.getWhen();
			if ((when - lastWhen) < 300)
				return;
			lastWhen = when;
			
			int keyCode = event.getKeyCode();
			if (keyCode != KeyEvent.VK_ENTER)
				return;
		}
		
		if (actionCommand == null)
			actionCommand = "";
		
		if (e.getSource().equals(f_currencies))
		{
			onCurrencySelected();
		}
		else if (actionCommand.equals("0") || e.getSource().equals(f_payAmount) || (e.getSource().equals(f_batchNo) && !Util.isEmpty(f_batchNo.getText(), true))) //OK
		{
			if (f_currencies.getSelectedIndex() == -1 && m_tab.getSelectedIndex() == 0)
				return;
			String error = createUpdateTransaction(m_paymentTrxID);
			if (error != null)
			{
				ADialog.error(m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString(error));
				try 
				{
					DB.rollback(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				m_posPayment = null;
				return;
			}
			
			try 
			{
				DB.commit(true, get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			
			updateTable();
			m_right.updateInfo();
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					if (m_tab.getSelectedIndex() == 0)
					{
						clear();
						initAmountCurrency();
					}
					m_tab.setSelectedIndex(0);
					f_currencies.requestFocus();
				}
			});
		}
		else if (actionCommand.equals("1")) //cancel payment
		{
			if (getPayment() != null && !getPayment().delete (true))
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSPaymentPanel.this, "Failed when try to remove payment");
				try 
				{
					DB.rollback(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				return;
			}
			try 
			{
				DB.commit(true, get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			dispose();
		}
		else if (actionCommand.equals("2")) // delete line
		{
			int selected = m_table.getSelectedRow();
			if (selected == -1)
				return;
			Object oo = m_table.getValueAt(selected, 0);
			if (oo == null)
				return;
			int id = (Integer) oo;
			MUNSPaymentTrx trx = MUNSPaymentTrx.get(get_TrxName(), id);
			if (!trx.delete(true))
			{
				try 
				{
					DB.rollback(true, get_TrxName());
				} 
				catch (IllegalStateException e1) 
				{
					e1.printStackTrace();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSPaymentPanel.this, "Failed when try to remove payment");
				return;
			}
			String error = null;
			try 
			{
				DB.commit(true, get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				error = e1.getMessage();
			} 
			catch (SQLException e1) 
			{
				error = e1.getMessage();
			}
			if (error != null)
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSPaymentPanel.this, error);
				return;
			}
			updateTable();
			m_right.updateInfo();
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					if (m_tab.getSelectedIndex() == 0)
					{
						clear();
						initAmountCurrency();
					}
					m_tab.setSelectedIndex(0);
					f_currencies.requestFocus();
				}
			});
		}
		else if (actionCommand.equals("3")) //process payment
		{
			m_posPanel.m_posTrx.reload();
			m_posPanel.m_posTrx.set_TrxName(get_TrxName());
			MUNSPOSPayment payment = getCreatePayment();
			if (payment == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), this, "Failed when try to create payment"
						+ " Failed create payment");
				return;
			}
			payment.load(get_TrxName());
			BigDecimal remaining = (BigDecimal) f_remainingAmt.getValue();
			if (remaining.signum() != 0)
			{	
				ADialog.error(m_posPanel.getWindowNo(), this, "Please complete payment first, "
						+ " the remaining amount must be zero");
				return;
			}
			try 
			{
				if (!m_posPanel.m_posTrx.isComplete() && !m_posPanel.m_posTrx.processIt (DocAction.ACTION_Complete))
				{
					ADialog.error(m_posPanel.getWindowNo(), UNSPOSPaymentPanel.this, "Failed when try to process Transaction || " + m_posPanel.m_posTrx.getProcessMsg());
					DB.rollback(true, get_TrxName());
					return;
				}
				m_posPanel.m_posTrx.saveEx();
				payment.getPOSTransaction().load(get_TrxName());
				if (!payment.processIt (DocAction.ACTION_Complete))
				{
					ADialog.error(m_posPanel.getWindowNo(), UNSPOSPaymentPanel.this, "Failed when try to process payment || " + payment.getProcessMsg());
					DB.rollback(true, get_TrxName());
					return;
				}
				payment.saveEx();
				m_posPanel.m_infoPanel.setTrxNo(m_posPanel.m_posTrx.getTrxNo());
			} 
			catch (Exception ex) 
			{
				ADialog.error(m_posPanel.getWindowNo(), UNSPOSPaymentPanel.this, "Failed when try to process payment || " + ex.getMessage());
				try {
					DB.rollback(true, get_TrxName());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				ex.printStackTrace();
				return;
			}
			
			try 
			{
				DB.commit(true, get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
				return;
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
				return;
			}
			
			CloseOK();
		}
		else if (e.getSource().equals(f_edc))
		{
			if (m_tab.getSelectedIndex() != 1 && m_tab.getSelectedIndex() != 2 && m_tab.getSelectedIndex() != 3)
				return;
			KeyNamePair pair = (KeyNamePair) f_edc.getSelectedItem();
			if (pair == null)
				return;
			if (m_posPanel.isOnRefund())
			{
				MUNSCardTrxDetail originalDetail = null;
				MUNSPOSTrx originalTrx = m_posPanel.getOriginalTrx();
				MUNSPOSPayment originalPay = MUNSPOSPayment.get(getCtx(), originalTrx.get_ID(), get_TrxName());
				MUNSPaymentTrx[] originalPayTrxs = originalPay.getTrx();
				MUNSPaymentTrx originalPayTrx = null;
				for (int i=0; i<originalPayTrxs.length; i++)
				{
					if (!MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(originalPayTrxs[i].getPaymentMethod())
							&& !MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(originalPayTrxs[i].getPaymentMethod()))
						continue;
					MUNSCardTrxDetail detail = MUNSCardTrxDetail.get(originalPayTrxs[i]);
					if (detail == null)
						continue;
					if (detail.getUNS_EDC_ID() == pair.getKey())
					{
						originalDetail = detail;
						originalPayTrx = originalPayTrxs[i];
						break;
					}
				}
				if (originalDetail == null)
				{
					ADialog.error(m_posPanel.getWindowNo(), this, "Could not find orignal card transaction");
					return;
				}
				
				int oriSessionID = originalTrx.getUNS_POS_Session_ID();
				int currencSessionID = m_posPanel.m_posTrx.getUNS_POS_Session_ID();
				boolean isSameSession = oriSessionID == currencSessionID;
				if (isSameSession)
					f_batchNo.setText("VO");
				else
					f_batchNo.setText("0");
				
				for (int i=0; i<f_cardType.getItemCount(); i++)
				{
					if (((KeyNamePair) f_cardType.getItemAt(i)).getKey() == originalDetail.getUNS_CardType_ID())
					{
						f_cardType.setSelectedIndex(i);
						break;
					}
				}
				setPayAmount(originalDetail.getParent().getC_Currency_ID(), originalDetail.getParent().getAmount());
				f_accountNo.setText(originalPayTrx.getTrxNo());
				f_payAmount.setEnabled(false);
				f_batchNo.setEnabled(false);
				f_cardType.setEnabled(false);
				f_accountNo.setEnabled(false);
			}
			else
			{
				f_cardType.setEnabled(true);
				f_payAmount.setEnabled(true);
				f_batchNo.setEnabled(true);
				String sql = "SELECT C_Currency_ID FROM C_BankAccount WHERE C_BankAccount_ID = "
						+ " (SELECT C_BankAccount_ID FROM UNS_EDC WHERE UNS_EDC_ID = ?)";
				int currencyID = DB.getSQLValue(get_TrxName(), sql, pair.getKey());
				BigDecimal remaining = m_posPanel.m_posTrx.getGrandTotal();
				int from = m_posPanel.m_posTrx.getC_Currency_ID();
				if (getPayment() != null)
				{
					getPayment().load(get_TrxName());
					remaining = getPayment().getPOSAmount();
					remaining = remaining.subtract(getPayment().getPaidAmt()).add(getPayment().getRoundingAmt());
					from = m_posPayment.getC_Currency_ID();
				}
				BigDecimal convertedAmt = MConversionRate.convert(getCtx(), remaining, 
						from, currencyID, 
						m_posPanel.m_posTrx.getDateTrx(), 0, 
						m_posPanel.m_posTrx.getAD_Client_ID(), 
						m_posPanel.m_posTrx.getAD_Org_ID(), RoundingMode.UP, 0);
				if (convertedAmt == null)
					convertedAmt = Env.ZERO;
				f_remainingAmt.setValue(convertedAmt);
				setPayAmount(currencyID, convertedAmt);
			}
			EventQueue.invokeLater(new Runnable() 
			{	
				@Override
				public void run() 
				{
					if ((m_tab.getSelectedIndex() == 3 && Util.isEmpty(f_accountNo.getText(), true)) && !m_posPanel.isOnRefund())
						f_accountNo.requestFocus();
					else
						f_payAmount.requestFocus();	
				}
			});
		}
		else if (f_cardType.equals(e.getSource()) && !m_posPanel.isOnRefund())
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					f_batchNo.requestFocus();	
				}
			});	
		}
	}

	@Override
	public void stateChanged(ChangeEvent arg0) 
	{
		if (arg0.getSource().equals(m_tab))
			onTabSelected();
	}
	
	public void CloseOK ()
	{
		Trx tx = Trx.get(get_TrxName(), false);
		if (tx != null)
			tx.commit();

		m_posPanel.m_form.resetPayableOnCustomerDisplay();
		super.dispose();
		m_payDialog.closePanel();
	}
	
	public void dispose ()
	{
		m_payDialog.dispose();
	}
	
	private void loadRefundDisplay ()
	{
		m_tab.setBackground(new Color(247, 150, 70));
		m_table.getTableHeader().setBackground(new Color(247, 150, 70));
		m_table.setOddColor(new Color(252, 221, 207));
		m_table.setEvenColor(new Color(253, 239, 233));
		m_right.loadRefundDisplay();
		p_cash.setBackground(new Color(247, 150, 70));
		p_card.setBackground(new Color(247, 150, 70));
		p_wechat.setBackground(new Color(247, 150, 70));
		updateUI();
	}
	
	private void loadSaleDisplay ()
	{
		m_tab.setBackground(new Color(79, 129, 189));
		m_table.setOddColor(UNSPOSTable.DEFAULT_ODD_COLOR);
		m_table.setEvenColor(UNSPOSTable.DEFAULT_EVEN_COLOR);
		m_table.getTableHeader().setBackground(new Color(79, 129, 189));
		m_right.loadSaleDisplay();
		p_cash.setBackground(new Color(79, 129, 189));
		p_card.setBackground(new Color(79, 129, 189));
		p_wechat.setBackground(new Color(79, 129, 189));
		updateUI();
	}
	
	/**
	 * 
	 * @param payAmount
	 */
	public void setPayAmount (int currencyID, BigDecimal payAmount)
	{
		f_payAmount.setValue(payAmount);
		m_posPanel.m_form.updatePayableOnCustomerDisplay(currencyID, payAmount);
	}
	
	class MyKeyAdapter implements KeyListener
	{

		@Override
		public void keyPressed(KeyEvent arg0) 
		{	
		}

		@Override
		public void keyReleased(KeyEvent arg0) 
		{
			final Object source = arg0.getSource();
			int keyCode = arg0.getKeyCode();
			if (!arg0.isConsumed() && keyCode ==KeyEvent.VK_ENTER)
			{
				EventQueue.invokeLater(new Runnable() 
				{	
					@Override
					public void run() 
					{
						if (source.equals(f_currencies))
						{
							if (m_tab.getSelectedIndex() == 4)
								f_accountNo.requestFocus();
							else
								f_payAmount.requestFocus();
						}
						else if (source.equals(f_accountNo))
							f_payAmount.requestFocus();
					}
				});
			}
		}

		@Override
		public void keyTyped(KeyEvent arg0) 
		{
		}
		
	}
}

class AmountCurrencyPair 
{
	private int m_key;
	private String m_name;
	private BigDecimal m_amount;
	private String m_amtStr;
	
	AmountCurrencyPair (int key, String name, BigDecimal amount)
	{
		m_key = key;
		m_name = name;
		m_amount = amount;
		if (amount == null)
			amount = Env.ZERO;
		m_amtStr = NumberFormat.getInstance().format(amount.doubleValue());
	}
	
	public int getKey ()
	{
		return m_key;
	}
	
	public BigDecimal getAmount ()
	{
		return m_amount;
	}
	
	public String getName ()
	{
		return m_name;
	}
	
	public String getAmtStr ()
	{
		return m_amtStr;
	}
	
	@Override
	public String toString ()
	{
		StringBuilder sb = new StringBuilder("<html><pre>");
		sb.append(m_name);
		int curLength = m_name.length() + m_amtStr.length();
		for (int i=curLength; i<33; i++)
		{
			sb.append(" ");
		}
		sb.append(m_amtStr);
		sb.append("</pre></html>");
		String tostring = sb.toString();
		return tostring;
	}
}