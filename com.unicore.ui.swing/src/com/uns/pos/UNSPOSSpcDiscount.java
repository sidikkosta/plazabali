/**
 * 
 */
package com.uns.pos;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import net.miginfocom.swing.MigLayout;

import org.compiere.apps.ADialog;
import org.compiere.apps.AEnv;
import org.compiere.grid.ed.VNumber;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.PO;
import org.compiere.swing.CButton;
import org.compiere.swing.CCheckBox;
import org.compiere.swing.CComboBox;
import org.compiere.swing.CDialog;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CTextArea;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import com.unicore.model.MUNSCustomerInfo;
import com.unicore.model.MUNSDiscountTrx;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPOSTrxLine;

/**
 * @author nurse
 *
 */
public class UNSPOSSpcDiscount extends CDialog implements KeyListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8851743391512916548L;
	private UNSPOSPanel m_posPanel = null;
	private CComboBox f_discountType;
	private CComboBox f_discountCalc;
	private CLabel l_amt;
	private VNumber f_amount;
	private CButton b_ok;
	private CButton b_cancel;
	private CLabel l_discRef;
	private CComboBox f_discRef;
	private CLabel l_discNote;
	private CTextArea f_discNote;
	private CCheckBox f_OverwriteDisc;
	/**
	 * @param owner
	 * @param modal
	 * @throws HeadlessException
	 */
	public UNSPOSSpcDiscount(UNSPOSPanel panel, boolean modal) throws HeadlessException 
	{
		super(AEnv.getFrame(panel), modal);
		m_posPanel = panel;
		setPreferredSize(new Dimension(400,300));
		setMinimumSize(getPreferredSize());
		setResizable(false);
		setLocationByPlatform(true);
		setTitle("Special Discount");
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
		int y = (screenSize.height - getHeight()) / 2;
		setLocation(x, y);
		init();
	}
	
	private void init ()
	{
		CLabel lDiscountType = new CLabel ("Discount Type");
		CLabel lDiscountCalc = new CLabel ("Discount Calc");
		l_amt = new CLabel("Amount");
		f_discountType = new CComboBox(new Object[]{"Amount", "Percent"});
		f_discountType.addActionListener(this);
		f_discountCalc = new CComboBox(new Object[]{"Total", "Item"});
		f_amount = new VNumber();
		f_amount.setDisplayType(DisplayType.Amount);
		b_ok = new CButton("OK");
		b_ok.addActionListener(this);
		b_cancel = new CButton("CANCEL");
		b_cancel.addActionListener(this);
		l_discRef = new CLabel("Disc Reff");
		f_discRef = new CComboBox(loadDiscountReff());
		l_discNote = new CLabel("Disc Note");
		f_discNote = new CTextArea();
		
		f_OverwriteDisc = new CCheckBox("Overwrite Discount");
		f_OverwriteDisc.setSelected(true);
		
		CPanel panel = new CPanel(new MigLayout("fill","[][fill]",""));
		panel.add(lDiscountType, "");
		panel.add(f_discountType, "wrap,pushx, flowx, growx, growy");
		panel.add(lDiscountCalc, "");
		panel.add(f_discountCalc, "wrap, growy");
		panel.add(l_amt, "");
		panel.add(f_amount, "wrap, pushx, flowx, growx, growy");
		panel.add(l_discRef, "");
		panel.add(f_discRef, "wrap, pushx, flowx, growx, growy");
		panel.add(new CLabel(""), "");
		panel.add(f_OverwriteDisc, "wrap, pushx, flowx, growx, growy");
		panel.add(l_discNote, "");
		panel.add(f_discNote, "wrap, pushy, pushx, flowx, flowy, growx, growy");
		panel.add(new CLabel(), "");
		panel.add(b_ok, "split 2");
		panel.add(b_cancel, "wrap");
		
		f_discountType.getEditor().getEditorComponent().addKeyListener(this);
		f_discountCalc.getEditor().getEditorComponent().addKeyListener(this);
		f_amount.addActionListener(this);
		f_discRef.getEditor().getEditorComponent().addKeyListener(this);
		f_discNote.addKeyListener(this);
		getContentPane().add(panel);
	}
	
	@Override
	public void actionPerformed (ActionEvent e)
	{
		AWTEvent currentEvent = EventQueue.getCurrentEvent();
		if (currentEvent instanceof FocusEvent)
			return;
		if (e.getSource().equals(f_amount))
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() 
				{
					f_discRef.requestFocus();
				}
			});
		}
		else if (e.getSource().equals(b_ok))
		{
			KeyNamePair selectedReff = (KeyNamePair) f_discRef.getSelectedItem();
			if (selectedReff == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), this, "Mandatory Discount Refference");
				return;
			}
			
			int discReffID = selectedReff.getKey();
			
			MUNSCustomerInfo info = m_posPanel.m_infoPanel.getCustomer();
			if (info != null && !MUNSCustomerInfo.CUSTOMERTYPE_AirCrew.equals(info.getCustomerType()))
			{
				String sql = "SELECT COUNT(*) FROM UNS_DiscountReff WHERE UNS_DiscountReff_ID = ? "
						+ " AND Description LIKE ?";
				int counter = DB.getSQLValue(getName(), sql, discReffID,"%#NP#%");
				if (counter < 1)
				{
					UNSPOSSpvConfirmDialog confirm = new UNSPOSSpvConfirmDialog(m_posPanel, true);
					confirm.setVisible(true);
					if (!confirm.isApproved())
					{
						return;
					}
				}
			}
			
			String dType = (String) f_discountType.getSelectedItem();
			if (dType == null)
			{
				ADialog.error(m_posPanel.getWindowNo(), this, "Mandatory Discount Type");
				return;
			}
			String dCalc = (String) f_discountCalc.getSelectedItem();
			if (null == dCalc)
			{
				ADialog.error(m_posPanel.getWindowNo(), this, "Mandatory Discount Calculation");
				return;
			}
			
			String discNote = f_discNote.getText();
			
			PO trx = null;
			
			if ("Total".equals(dCalc))
			{
				trx = m_posPanel.m_posTrx;
			}
			else 
			{
				int selected = m_posPanel.f_curLine.m_table.getSelectedRow();
				if (selected == -1)
				{
					ADialog.error(m_posPanel.getWindowNo(), this, "No transaction selected");
					return;
				}
				Object oo = m_posPanel.f_curLine.m_table.getValueAt(selected, 0);
				if (oo == null)
				{
					ADialog.error(m_posPanel.getWindowNo(), this, "No transaction selected");
					return;
				}
				if (oo instanceof IDColumn == false)
				{
					ADialog.error(m_posPanel.getWindowNo(), this, "No transaction selected");
					return;
				}
				int id = (int) ((IDColumn)oo).getRecord_ID() ;
				if (id <= 0)
				{
					ADialog.error(m_posPanel.getWindowNo(), this, "No transaction selected");
					return;
				}
				
				trx = new MUNSPOSTrxLine(m_posPanel.getCtx(), id, m_posPanel.get_TrxName());
			}
			if (trx == null || trx.get_ID() == 0)
			{
				ADialog.error(m_posPanel.getWindowNo(), this, "No transaction selected");
				return;
			}
			
			if ("Item".equals(dCalc))
			{
				String sql = "SELECT COUNT(*) FROM UNS_DiscountTrx WHERE UNS_POSTrx_ID = ?";
				int exists = DB.getSQLValue(m_posPanel.get_TrxName(), sql, trx.get_ValueAsInt("UNS_POSTrx_ID"));
				if (exists > 0)
				{
					if (ADialog.ask(m_posPanel.getWindowNo(), UNSPOSSpcDiscount.this, "This action will delete the discount that was previously defined, Continue this process?"))
					{
						sql = "DELETE FROM UNS_DiscountTrx WHERE UNS_POStrx_ID = ?";
						int executeOK = DB.executeUpdate(sql, new Object[]{trx.get_ValueAsInt("UNS_POSTrx_ID")}, 
								false, m_posPanel.get_TrxName());
						if (executeOK == -1)
						{
							String error = CLogger.retrieveErrorString("Unknown-Error");
							ADialog.error(m_posPanel.getWindowNo(), UNSPOSSpcDiscount.this, error);
							return;
						}
						
						MUNSPOSTrx p = m_posPanel.m_posTrx;
						p.setDiscountAmt(Env.ZERO);
						if (!p.save())
						{
							String error = CLogger.retrieveErrorString("Unknown-Error");
							ADialog.error(m_posPanel.getWindowNo(), UNSPOSSpcDiscount.this, error);
							return;
						}
						m_posPanel.m_posTrx.load(m_posPanel.get_TrxName());
					}
					else
					{
						return;
					}
				}
			}
			
			BigDecimal discAmt = (BigDecimal) f_amount.getValue();
			BigDecimal totalAmt = Env.ZERO;
			
			if (f_OverwriteDisc.isSelected())
			{
				MUNSDiscountTrx[] dTrxs = MUNSDiscountTrx.get(trx);
				for (int i=0; i<dTrxs.length; i++)
				{
					if (!dTrxs[i].delete(true))
					{
						try 
						{
							DB.rollback(true, m_posPanel.get_TrxName());
						} 
						catch (SQLException e1) 
						{
							e1.printStackTrace();
						}
						ADialog.error(m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not save transaction"));
						return;
					}
				}
				
				trx.set_ValueOfColumn("Discount", Env.ZERO);
				trx.set_ValueOfColumn("DiscountAmt", Env.ZERO);
				if (trx instanceof MUNSPOSTrxLine)
				{
					MUNSPOSTrxLine line = (MUNSPOSTrxLine) trx;
					line.setQtyEntered(line.getQtyEntered().add(line.getQtyBonuses()));
					line.setQtyBonuses(Env.ZERO);
				}
				if (!trx.save())
				{
					try 
					{
						DB.rollback(true, m_posPanel.get_TrxName());
					} 
					catch (SQLException e1) 
					{
						e1.printStackTrace();
					}
					ADialog.error(m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not save transaction"));
					return;
				}
			}
			
			if (!trx.load(trx.get_TrxName()))
			{
				try 
				{
					DB.rollback(true, m_posPanel.get_TrxName());
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				ADialog.error(m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not save transaction"));
				return;
			}
			
			if (trx instanceof MUNSPOSTrx)
			{
				totalAmt = (BigDecimal) trx.get_Value(MUNSPOSTrx.COLUMNNAME_TotalAmt);
			}
			else if (trx instanceof MUNSPOSTrxLine)
			{
				totalAmt = ((BigDecimal) trx.get_Value(MUNSPOSTrxLine.COLUMNNAME_PriceList))
						.multiply(((BigDecimal) trx.get_Value(MUNSPOSTrxLine.COLUMNNAME_QtyOrdered)));
			}
			
			if (totalAmt == null)
				totalAmt = Env.ZERO;
			
			BigDecimal gainedAmt = (BigDecimal) trx.get_Value("DiscountAmt");
			if (gainedAmt == null)
			{
				gainedAmt = Env.ZERO;
			}

			MUNSDiscountTrx dtrx = MUNSDiscountTrx.getNotSchema(trx, discReffID);
			if (dtrx == null)
			{
				dtrx = new MUNSDiscountTrx(trx.getCtx(), 0, trx.get_TrxName());
				dtrx.setAD_Org_ID(trx.getAD_Org_ID());
				dtrx.setName("Special Disc");
				dtrx.set_ValueOfColumn(trx.get_TableName() + "_ID", trx.get_ID());
				dtrx.setRequirementType("PO");
			}
			if (gainedAmt.signum() != 0 && !f_OverwriteDisc.isSelected())
			{
				gainedAmt = gainedAmt.subtract(dtrx.getFlatValueDiscount());
				totalAmt = totalAmt.subtract(gainedAmt);
			}
			
			String discountType = null;
			if ("Percent".equals(dType))
			{
				discAmt = totalAmt.multiply(discAmt).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_UP);
				discountType = MUNSDiscountTrx.DISCOUNTTYPE_Percent;
			}
			else
			{
				discountType = MUNSDiscountTrx.DISCOUNTTYPE_Value;				
			}
			
			dtrx.setUNS_DiscountReff_ID(discReffID);
			dtrx.setDiscountedAmt(totalAmt);
			dtrx.setFlatValueDiscount(discAmt);
			dtrx.setFlatPercentDiscount(discAmt.divide(totalAmt, 5, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED));
			dtrx.setDiscountType(discountType);
			if (!dtrx.save())
			{
				try 
				{
					DB.rollback(true, m_posPanel.get_TrxName());
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				ADialog.error(m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not save transaction"));
				return;
			}
			
			discAmt = discAmt.add(gainedAmt);
			trx.set_ValueOfColumn("UNS_DiscountReff_ID", discReffID);
			trx.set_ValueOfColumn("DiscNote", discNote);
			trx.set_ValueOfColumn("DiscountAmt", discAmt);
			
			if (!trx.save())
			{
				try 
				{
					DB.rollback(true, m_posPanel.get_TrxName());
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				ADialog.error(m_posPanel.getWindowNo(), this, CLogger.retrieveErrorString("Could not save transaction"));
				return;
			}
			
			try 
			{
				DB.commit(true, m_posPanel.get_TrxName());
			} 
			catch (IllegalStateException e1) 
			{
				e1.printStackTrace();
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			
			close();
		}
		else if (e.getSource().equals(b_cancel))
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					m_posPanel.f_curLine.m_table.requestFocus();
					for (int i=0; i<m_posPanel.f_curLine.m_table.getRowCount(); i++)
					{
						if (m_posPanel.f_curLine.m_table.getValueAt(i, 0) == null)
						{
							m_posPanel.f_curLine.m_table.getSelectionModel().setSelectionInterval(i, i);
							m_posPanel.f_curLine.m_table.editCellAt(i, 2);
							break;
						}
					}
				}
			});
			dispose();
		}
	}
	
	/**
	 * 
	 * @param po
	 * @return
	 *
	private boolean deleteDiscountedSchema (PO po)
	{
		MUNSDiscountTrx[] dTrxs = MUNSDiscountTrx.get(po);
		for (int i=0; i<dTrxs.length; i++)
		{
			if (!dTrxs[i].delete(true))
				return false;
		}
		
		po.set_ValueOfColumn("Discount", Env.ZERO);
		po.set_ValueOfColumn("DiscountAmt", Env.ZERO);
		if (!po.save())
			return false;
		
		if (po instanceof MUNSPOSTrx)
		{
			MUNSPOSTrxLine[] lines = ((MUNSPOSTrx) po).getLines(false);
			for (int i=0; i<lines.length; i++)
			{
				deleteDiscountedSchema(lines[i]);
				lines[i].set_ValueOfColumn("Discount", Env.ZERO);
				lines[i].set_ValueOfColumn("DiscountAmt", Env.ZERO);
				lines[i].setQtyEntered(lines[i].getQtyEntered().add(lines[i].getQtyBonuses()));
				lines[i].setQtyBonuses(Env.ZERO);
				if (!lines[i].save())
					return false;
			}
		}
		return true;
	}
	*/
	
	private void close ()
	{
		m_posPanel.updateInfo();
		dispose();
	}
	
	private Vector<KeyNamePair> loadDiscountReff ()
	{
		Vector<KeyNamePair> results = new Vector<>();
		String sql = "SELECT UNS_DiscountReff_ID, CONCAT (Value, '-', Name) AS Name FROM UNS_DiscountReff WHERE IsActive = ? ORDER BY Name";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, m_posPanel.get_TrxName());
			st.setString(1, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				String name = rs.getString(2);
				if (name.length() > 32)
					name = name.substring(0, 32);
				results.add(new KeyNamePair(rs.getInt(1), name));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return results;
	}

	@Override
	public void keyPressed(KeyEvent arg0) 
	{
		//Do nothing
	}

	@Override
	public void keyReleased(KeyEvent arg0) 
	{
		if (arg0.isConsumed())
			return;
		int keyCode = arg0.getKeyCode();
		if (keyCode != KeyEvent.VK_ENTER)
			return;
		final Object source = ((Component)arg0.getSource()).getParent();
		if (source.equals(f_discountType))
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() 
				{
					String dType = (String) f_discountType.getSelectedItem();
					if (dType == null)
						return;
					if ("Amount".equals(dType))
						l_amt.setText("Amount");
					else 
						l_amt.setText("Percent");
					f_discountCalc.requestFocus();
				}
			});
		}
		else if (source.equals(f_discountCalc))
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() 
				{
					f_amount.requestFocus();
				}
			});
		}
		else if (source.equals(f_discRef))
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() 
				{
					f_discNote.getTextArea().requestFocus();
				}
			});
		}
		else
		{
			EventQueue.invokeLater(new Runnable() {
				
				@Override
				public void run() 
				{
					actionPerformed(new ActionEvent(b_ok, ActionEvent.ACTION_PERFORMED, "CButton"));
				}
			});
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) 
	{
		//Do nothing
	}
}
