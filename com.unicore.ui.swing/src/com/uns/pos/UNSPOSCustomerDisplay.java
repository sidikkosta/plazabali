/**
 * 
 */
package com.uns.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import net.miginfocom.swing.MigLayout;

import org.adempiere.plaf.AdempierePLAF;
import org.compiere.swing.CFrame;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;
import org.compiere.swing.CTextArea;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MDiscountSchema;
import com.unicore.model.MUNSAdvertisement;
import com.unicore.model.UNSPOSTrxDiscountModel;
import com.unicore.model.X_UNS_DiscountTrx;
import com.unicore.ui.form.UNTPanel;

/**
 * @author nurse
 *
 */
public class UNSPOSCustomerDisplay extends CFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 895645640514477648L;
	private CPanel m_mainPanel;
	private UNSAdsImagePanel m_advertisement;
	private CPanel m_topPanel;
	private CPanel m_leftPanel;
	private UNTPanel m_rightPanel;
	private MUNSAdvertisement[] m_ads;
	private UNSPOSTable m_table;
	private CLabel l_total;
	private CLabel l_base1;
	private CLabel l_pay;
	private CLabel l_payAmt;
	private CTextArea f_discountInfo;
	private boolean m_stopAdvert = false;
	private Thread m_advertThread = null;
	private Thread m_infThread = null;
	private UNSPOSPanel m_posPanel = null;

	public UNSPOSCustomerDisplay (UNSPOSPanel panel)
	{
		super();
		m_posPanel = panel;
		setBackground(new Color(243, 250, 236));
		setMinimumSize(new Dimension(700,600));
		setExtendedState(MAXIMIZED_BOTH);
		setResizable(true);
		setTitle("Welcome to our convenience store :)");
		String sql = "SELECT COALESCE(RealName, Name) FROM AD_User WHERE AD_User_ID = ?";
		String user = DB.getSQLValueString(panel.get_TrxName(), sql, panel.m_session.getCashier_ID());
		init(user);
    	setUndecorated(true);
		pack();
	}
	
	public void init (String user)
	{
		m_mainPanel = new CPanel(new MigLayout("fill,insets 0", "[fill][300px!, fill]", "fill"));
		m_mainPanel.setBackground(new Color(243, 250, 236));
		m_advertisement = new UNSAdsImagePanel(new MigLayout("insets 0, fill", "[fill]", "[fill]"));
		m_topPanel = new CPanel(new MigLayout("fill, insets 0", "[][fill]", "[]"));
		m_leftPanel = new CPanel(new MigLayout("fill, insets 0", "[fill]", "[fill][fill]"));
		m_rightPanel = new UNTPanel(new MigLayout("fill", "[fill]", "[fill]"));
		m_rightPanel.setBackground(new Color(5,132,63));
		m_rightPanel.setStrokeColor(new Color(5, 132, 63));
		JTextArea area = new JTextArea();
		area.setFont(new Font("sansserif",Font.BOLD, 16));
		f_discountInfo = new CTextArea(area);
		f_discountInfo.setFocusable(false);
		f_discountInfo.setEditable(false);
		f_discountInfo.setLineWrap(true);
		m_rightPanel.add(f_discountInfo, "growx, growy, flowx, flowy, pushx, pushy");
		f_discountInfo.setBackground(new Color(166,211,204));
		
		Font fTotal = new Font("Calibri", Font.BOLD, 14);
		Font fRupiah = new Font("Calibri", Font.BOLD, 28);
		
		ImageIcon icon = new ImageIcon(getClass().getResource("/com/unicore/images/plazabali2.png"));
		JLabel l = new JLabel();
		l.setIcon(icon);
		m_topPanel.add(l, "split 2");
		String headerMsg = "Hi, Welcome to our convenience store :-)";
		StringBuilder cashierInfoMsg = new StringBuilder(" I am ");
		cashierInfoMsg.append(user);
		
		CLabel lheaderMsg = new CLabel(headerMsg);
		lheaderMsg.setFont(fTotal);
		lheaderMsg.setHorizontalAlignment(SwingConstants.CENTER);
		lheaderMsg.setForeground(Color.WHITE);
		lheaderMsg.setBorder(null);
		CLabel lCasherInfo = new CLabel(cashierInfoMsg.toString());
		lCasherInfo.setFont(fTotal);
		lCasherInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lCasherInfo.setForeground(Color.WHITE);
		lCasherInfo.setBorder(null);
		lCasherInfo.setVerticalAlignment(SwingConstants.CENTER);
		lheaderMsg.setVerticalAlignment(SwingConstants.CENTER);
		
		UNTPanel topRight = new UNTPanel(new MigLayout("fill, insets 10", "[fill]", "fill"));
		topRight.setBackground(new Color(33, 102, 99));
		topRight.setStrokeColor(new Color(166,211,204));
		topRight.add(lheaderMsg, "growx, flowx, wrap, pushx");
		topRight.add(lCasherInfo, "growx, flowx, wrap, pushx");
		m_topPanel.add(topRight, "growx, flowx, growy, flowy,pushx, wrap");
		m_mainPanel.add(m_topPanel, "spanx, wrap, pushx, top");
		
		m_table = new UNSPOSTable();
		m_table.setEnabled(false);
		m_table.setEvenColor(new Color(222, 231, 209));
		m_table.setOddColor(new Color(239, 243, 234));
		DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new String[]{"NO", "PRODUCT", "QTY", "PRICE", "Discount", "TOTAL"});
		m_table.setModel(model);
		m_table.getColumn(0).setPreferredWidth(30);
		m_table.getColumn(0).setResizable(false);
		m_table.getColumn(1).setPreferredWidth(300);
		m_table.getColumn(2).setPreferredWidth(50);
		m_table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		m_table.setFillsViewportHeight(true);
		m_table.setFocusable(false);
		m_table.setRowHeight(30);
		m_table.getTableHeader().setBackground(new Color(155, 187, 89));
		m_table.getTableHeader().setForeground(Color.WHITE);
		CScrollPane scroll = new CScrollPane(m_table);
		this.m_table.getTableHeader().setPreferredSize(new Dimension(scroll.getWidth(), 24));
		this.m_table.getTableHeader().setFont(AdempierePLAF.getFont_Field().deriveFont(16f));
		this.m_table.setFont(AdempierePLAF.getFont_Field().deriveFont(14f));
		
		for (int i=2; i<=5; i++)
		{
			DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) m_table.getColumn(i).getCellRenderer();
			if (renderer == null)
				renderer = new DefaultTableCellRenderer();
			renderer.setHorizontalAlignment(SwingConstants.RIGHT);
			m_table.getColumn(i).setCellRenderer(renderer);			
		}
		m_leftPanel.add(scroll, ", wrap, pushy, pushx");
		m_mainPanel.add(m_leftPanel, "growx, flowx, pushx");
		
		UNTPanel totalPanel = new UNTPanel(new MigLayout("fill","[]","[fill]"));
		totalPanel.setBackground(new Color(232, 59, 123));
		totalPanel.setStroke(4, new Color(0,110,0));
		
		l_total = new CLabel("Total Rp. ");
		l_total.setFont(fTotal);
		l_total.setForeground(Color.WHITE);
		l_total.setHorizontalAlignment(SwingConstants.LEFT);
		totalPanel.add(l_total, "wrap,pushx, flowx, growx, left");
		
		l_base1 = new CLabel("0");
		l_base1.setFont(fRupiah);
		l_base1.setHorizontalAlignment(SwingConstants.RIGHT);
		l_base1.setForeground(Color.WHITE);
		totalPanel.add(l_base1, "wrap, pushx, flowx, growx");
		
		l_pay = new CLabel("");
		l_pay.setFont(fTotal);
		l_pay.setForeground(Color.WHITE);
		l_pay.setHorizontalAlignment(SwingConstants.LEFT);
		totalPanel.add(l_pay, "wrap,pushx, flowx, growx, left");
		
		l_payAmt = new CLabel("");
		l_payAmt.setFont(fRupiah);
		l_payAmt.setHorizontalAlignment(SwingConstants.RIGHT);
		l_payAmt.setForeground(Color.WHITE);
		totalPanel.add(l_payAmt, "wrap, pushx, flowx, growx");
		
		m_leftPanel.add(totalPanel, "wrap,  pushy");
		
		m_mainPanel.add(m_rightPanel, "right, spany, flowx, flowy, growy, growx, pushy");
		getContentPane().add(m_mainPanel);
		
		m_mainPanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked (MouseEvent e) {
				if (e.getClickCount() == 2) {
					boolean value = !isUndecorated();
					boolean reRunAdvert = !m_stopAdvert;
					dispose();
					setUndecorated(value);
					setVisible(true);
					if (reRunAdvert)
						showAdvertisement();
				}
			}
		});
		updateTable(0);
		loadImages();
		showAdvertisement();
	}
	
	private synchronized void updateTable (int posTrxID)
	{
		if (m_table == null)
			return;
		String sql = "SELECT tl.Line, p.Name, tl.QtyOrdered, tl.PriceList, tl.DiscountAmt, tl.LineAmt "
				+ " FROM UNS_POSTrxLine tl INNER JOIN M_Product p ON "
				+ " p.M_Product_ID = tl.M_Product_ID WHERE UNS_POSTrx_ID = ? ORDER BY tl.Line";
		PreparedStatement st = null;
		ResultSet rs = null;
		m_table.setRowCount(0);
		DefaultTableModel model = (DefaultTableModel) m_table.getModel();
		try
		{
			st = DB.prepareStatement(sql, m_posPanel.get_TrxName());
			st.setInt(1, posTrxID);
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal qty = rs.getBigDecimal(3);
				if (qty == null)
					qty = Env.ZERO;
				BigDecimal price = rs.getBigDecimal(4);
				if (price == null)
					price = Env.ZERO;
				BigDecimal discount = rs.getBigDecimal(5);
				if (discount == null)
					discount = Env.ZERO;
				BigDecimal total = rs.getBigDecimal(6);
				if (total == null)
					total = Env.ZERO;
				String qtyStr = NumberFormat.getInstance().format(qty.doubleValue());
				String priceStr = NumberFormat.getInstance().format(price.doubleValue());
				String discStr = NumberFormat.getInstance().format(discount.doubleValue());
				String totalStr = NumberFormat.getInstance().format(total.doubleValue());
				model.addRow(new Object[]{rs.getObject(1), rs.getObject(2), qtyStr, 
						priceStr, discStr, totalStr});
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		for (int i=model.getRowCount(); i<15;i++)
		{
			model.addRow(new Object[]{null, null, null, null, null,null, null});
		}
	}
	
	public void updateInfo (final int posTrxID, final BigDecimal total)
	{	
		if (m_infThread != null && m_infThread.isAlive())
			m_infThread.interrupt();
		m_infThread = new Thread(new Runnable() 
		{	
			@Override
			public void run() 
			{
				while (m_advertThread != null && m_advertThread.isAlive())
				{
					stopAdvertisement();
					m_advertThread.interrupt();
					try 
					{
						Thread.sleep(1000);
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
					continue;
				}
				
				updateTable(posTrxID);
				String totalStr = NumberFormat.getInstance().format(total.doubleValue());
				l_base1.setText(totalStr);
				if (!m_posPanel.isOnRefund())
				{
					String info = loadDiscountInfo();
					f_discountInfo.setText(info);
					f_discountInfo.updateUI();
					m_posPanel.m_botPanel.setDiscountInfo(info);
				}
			}
		});
		m_infThread.setName("InfoUpdateProcessor");
		m_infThread.start();
	}
	
	private  void loadImages ()
	{
		m_ads = MUNSAdvertisement.get(
				m_posPanel.m_session.getAD_Org_ID(), 
				m_posPanel.getNonMemberPartner().get_ID(), 
				m_posPanel.get_TrxName());
	}
	
	public void stopAdvertisement ()
	{
		m_stopAdvert = true;
	}
	
	public void showAdvertisement ()
	{
		if(m_advertThread != null && m_advertThread.isAlive())
		{
			m_stopAdvert = false;
			return;
		}
		m_stopAdvert = false;
		m_advertThread = new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				m_mainPanel.remove(m_leftPanel);
				m_mainPanel.remove(m_topPanel);
				m_mainPanel.remove(m_rightPanel);
				m_mainPanel.add(m_advertisement, "spanx, center, flowx, flowy, pushx, pushy, growx, growy");
				m_mainPanel.repaint();
				
				while (!m_stopAdvert)
				{
					if (m_ads == null || m_ads.length == 0)
					{
						m_stopAdvert = true;
						continue;
					}
					for (int i=0; !m_stopAdvert && i<m_ads.length; i++)
					{
						Image[] imgs = m_ads[i].getImages(false);
						if (imgs.length == 0)
							continue;
						long sleep = m_ads[i].getDisplayTime();
						sleep *= 1000;
						for (int x=0; !m_stopAdvert && x<imgs.length; x++)
						{
							m_advertisement.showImage(imgs[x]);
							try 
							{
								Thread.sleep(sleep);
							} 
							catch (InterruptedException e) 
							{
								
							}
						}
					}
				}
				
				m_mainPanel.remove(m_advertisement);
				m_mainPanel.add(m_topPanel, "wrap, spanx, pushx, top");
				m_mainPanel.add(m_leftPanel, "growx, flowx, pushx");
				m_mainPanel.add(m_rightPanel, "spany, flowx, flowy, growy, growx, pushx, pushy");
				m_mainPanel.repaint();
			}
		});
		
		m_advertThread.setName("Advertisement");
		m_advertThread.start();
	}
	
	@Override
	public void dispose ()
	{
		if (m_advertThread != null && m_advertThread.isAlive())
			m_stopAdvert = true;
		if (m_infThread != null && m_infThread.isAlive())
		{
			m_infThread.interrupt();
			try 
			{
				while (m_infThread.isAlive())
					Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		super.dispose();
	}
	
	private String loadDiscountInfo ()
	{
		UNSPOSTrxModel model = m_posPanel.m_posTrx; 
		if (model == null)
			return null;
		if (model.isReturn() || m_posPanel.isOnRefund())
			return null;
		
		StringBuilder infoDiscB = new StringBuilder();
		int h_ID = model.get_ID();
		String sql = "SELECT * FROM UNS_DiscountTrx WHERE UNS_POSTrx_ID = ? "
				+ " OR UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine "
				+ " WHERE UNS_POSTrx_ID = ? AND RequirementType <> ?)";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql,null);
			st.setInt(1, h_ID);
			st.setInt(2, h_ID);
			st.setString(3, "PO");
			rs = st.executeQuery();
			while (rs.next())
			{
				if (infoDiscB.length() == 0)
					infoDiscB.append("Gained Discount :\n");
				String discountType = rs.getString("discounttype");
				String msg = "";
				if (X_UNS_DiscountTrx.DISCOUNTTYPE_Bonus.equals(discountType))
				{
					sql = "SELECT Name FROM M_Product WHERE M_Product_ID = ?";
					String productName = DB.getSQLValueString(null, sql, 
							rs.getInt("productbonus_id"));
					msg += "Product Bonuses " + productName + " @" 
							+ rs.getBigDecimal("QtyBonuses").setScale(2, RoundingMode.HALF_UP);
				}
				else if (X_UNS_DiscountTrx.DISCOUNTTYPE_Value.equals(discountType)
						|| X_UNS_DiscountTrx.DISCOUNTTYPE_Percent.equals(discountType))
				{
					BigDecimal d1 = rs.getBigDecimal("flatvaluediscount");
					BigDecimal d2 = rs.getBigDecimal("discountvalue1st");
					BigDecimal d3 = rs.getBigDecimal("discountvalue2nd");
					BigDecimal d4 = rs.getBigDecimal("discountvalue3rd");
					BigDecimal d5 = rs.getBigDecimal("discountvalue4th");
					BigDecimal d6 = rs.getBigDecimal("discountvalue5th");
					if (null == d1) d1 = Env.ZERO;
					if (null == d2) d2 = Env.ZERO;
					if (null == d3) d3 = Env.ZERO;
					if (null == d4) d4 = Env.ZERO;
					if (null == d5) d5 = Env.ZERO;
					if (null == d6) d6 = Env.ZERO;
					
					BigDecimal total = d1.add(d2).add(d3).add(d4).add(d5).add(d6);
					total = total.setScale(2, RoundingMode.HALF_UP);
					msg += "Diskon Amount " + total; 
				}
				
				infoDiscB.append("- ");
				infoDiscB.append(msg);
				infoDiscB.append("\n");
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		if (infoDiscB.length() > 1)
			infoDiscB.append("\n");
		
		MDiscountSchema[] schemas = MDiscountSchema.getSalesDiscount(
					model.getCtx(), model.getDateAcct(), 
					model.getAD_Org_ID(), null);
		UNSPOSTrxDiscountModel discModel = new UNSPOSTrxDiscountModel(model);
		boolean isDiscountAvailable = false;
		for (int i=0; i<schemas.length; i++)
		{
			String estimated = schemas[i].loadInfoDiscount(discModel);
			if (!Util.isEmpty(estimated, true) && !isDiscountAvailable)
			{
				isDiscountAvailable = true;
				infoDiscB.append("Estimated Discount : \n");
			}
			infoDiscB.append(estimated);
		}
		
		String infoDisc = infoDiscB.toString();
		return infoDisc;
	}
	
	public void setPayable (int currencyID, BigDecimal payable)
	{
		NumberFormat format = NumberFormat.getInstance();
		String strAmt = format.format(payable);
		String sql = "SELECT ISO_Code FROM C_Currency WHERE C_Currency_ID = ?";
		String currency = DB.getSQLValueString(null, sql, currencyID);
		l_pay.setText("Payable " + currency);
		l_payAmt.setText(strAmt);
	}
	
	public void resetPayable ()
	{
		l_pay.setText("");
		l_payAmt.setText("");
	}
}
