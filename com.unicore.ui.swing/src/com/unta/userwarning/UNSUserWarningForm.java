/**
 * 
 */
package com.unta.userwarning;

import org.compiere.apps.form.FormFrame;
import org.compiere.apps.form.FormPanel;

/**
 * @author nurse
 *
 */
public class UNSUserWarningForm implements FormPanel {

	private UNSUserWarningMainPanel m_mainPanel = null;
	/**
	 * 
	 */
	public UNSUserWarningForm() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.apps.form.FormPanel#init(int, org.compiere.apps.form.FormFrame)
	 */
	@Override
	public void init(int WindowNo, FormFrame frame) 
	{
		m_mainPanel = new UNSUserWarningMainPanel();
		if (!m_mainPanel.init(frame))
		{
			dispose();
			frame.setEnabled(false);
			return;
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.apps.form.FormPanel#dispose()
	 */
	@Override
	public void dispose() 
	{
		if (m_mainPanel != null)
			m_mainPanel.dispose();
	}

}
