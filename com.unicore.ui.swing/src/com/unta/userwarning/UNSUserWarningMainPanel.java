/**
 * 
 */
package com.unta.userwarning;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;

import org.compiere.swing.CFrame;
import org.compiere.swing.CPanel; 
import org.compiere.util.CLogger;
import org.compiere.util.Env;

/**
 * @author nurse
 *
 */
public class UNSUserWarningMainPanel extends CPanel implements ActionListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CFrame m_frame = null;
	private UNSUserWarningHeaderPanel m_hPanel = null;
	private UNSUserWarningContentPanel m_cPanel = null;
	private UNSUserWarningButtonPanel m_bPanel = null;
	private String m_trxName = null;
	private Properties m_ctx = null;
	private int m_AD_Client_ID = -1;
	private int m_AD_Role_ID = -1;

	/**
	 * 
	 */
	public UNSUserWarningMainPanel() 
	{
		super (new MigLayout("fill", "100%, fill", "[min!][fill][min!]"));
	}

	public boolean init (CFrame frame)
	{
		this.m_ctx = Env.getCtx();
		this.m_AD_Role_ID = Env.getAD_Role_ID(this.m_ctx);
		this.m_AD_Client_ID = Env.getAD_Client_ID(this.m_ctx);
		this.m_frame = frame;
		this.m_frame.setResizable(true);
		this.m_frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.m_frame.setPreferredSize(new Dimension(900,600));
		this.m_frame.setMinimumSize(new Dimension(900,600));
		this.m_frame.setJMenuBar(null);
		this.m_frame.setTitle("Warning!!! [Some Record Need Your Attention]");
		this.m_hPanel = new UNSUserWarningHeaderPanel();
		
		final String style = ",w 100%, growx, flowx, spanx, wrap,";
		if (!this.m_hPanel.init(this))
			return false;
		this.add(m_hPanel, style);
		this.m_cPanel = new UNSUserWarningContentPanel();
		if (!this.m_cPanel.init(this))
			return false;
				
		this.add(m_cPanel, style);
		this.m_bPanel = new UNSUserWarningButtonPanel();
		if (!this.m_bPanel.init(this))
			return false;
		this.add(m_bPanel, style);
		this.m_frame.getContentPane().add(this, BorderLayout.CENTER);
		enableButton();
		return true;
	}
	
	protected void updateInfo ()
	{
		this.m_hPanel.setTitle(this.m_cPanel.getTitle());
		this.m_hPanel.setDescription(this.m_cPanel.getDescription());
		enableButton();
	}
	
	private void enableButton ()
	{
		if (this.m_bPanel == null)
			return;
		
		this.m_bPanel.m_bFirst.setEnabled(true);
		this.m_bPanel.m_bPrev.setEnabled(true);
		this.m_bPanel.m_bNext.setEnabled(true);
		this.m_bPanel.m_bLast.setEnabled(true);
		
		if (this.m_cPanel.isFirst() && this.m_cPanel.isLast())
		{
			this.m_bPanel.m_bFirst.setEnabled(false);
			this.m_bPanel.m_bPrev.setEnabled(false);
			this.m_bPanel.m_bNext.setEnabled(false);
			this.m_bPanel.m_bLast.setEnabled(false);
		}
		else if (this.m_cPanel.isFirst())
		{
			this.m_bPanel.m_bFirst.setEnabled(false);
			this.m_bPanel.m_bPrev.setEnabled(false);
		}
		else if (this.m_cPanel.isLast())
		{
			this.m_bPanel.m_bNext.setEnabled(false);
			this.m_bPanel.m_bLast.setEnabled(false);
		}
		
		this.m_bPanel.updatePage(this.m_cPanel.getActiveIdx()+1, 
				this.m_cPanel.getTotalContent());
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource().equals(this.m_bPanel.m_bFirst))
		{
			this.m_cPanel.loadFirst();
		}
		else if (e.getSource().equals(this.m_bPanel.m_bPrev))
		{
			this.m_cPanel.loadPrev();
		}
		else if (e.getSource().equals(this.m_bPanel.m_bNext))
		{
			this.m_cPanel.loadNext();
		}
		else if (e.getSource().equals(this.m_bPanel.m_bLast))
		{
			this.m_cPanel.loadLast();
		}
		else if (e.getSource().equals(this.m_bPanel.m_bExit))
		{
			this.dispose();
		}
		else
			CLogger.getCLogger(getClass()).warning("Unknown Event " + e.toString());
	}
	
	public String get_TrxName ()
	{
		return this.m_trxName;
	}
	
	public Properties getCtx ()
	{
		return this.m_ctx;
	}
	
	public int getAD_Client_ID ()
	{
		return this.m_AD_Client_ID;
	}
	
	public int getAD_Role_ID ()
	{
		return this.m_AD_Role_ID;
	}

	public void dispose ()
	{
		if (this.m_cPanel != null)
			m_cPanel.dispose();
		if (this.m_frame != null)
			this.m_frame.dispose();
	}
}
