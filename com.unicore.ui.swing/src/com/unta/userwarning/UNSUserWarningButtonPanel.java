/**
 * 
 */
package com.unta.userwarning;

import java.awt.Color;

import javax.swing.border.LineBorder;

import org.compiere.swing.CButton;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;

/**
 * @author nurse
 *
 */
public class UNSUserWarningButtonPanel extends CPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UNSUserWarningMainPanel m_main = null;
	CButton m_bFirst = null;
	CButton m_bPrev = null;
	CButton m_bNext = null;
	CButton m_bLast = null;
	CButton m_bExit = null;
	CLabel m_lPage = null;

	/**
	 * 
	 */
	public UNSUserWarningButtonPanel() 
	{
		super ();
		setBorder(new LineBorder(Color.gray));
	}
	
	public boolean init (UNSUserWarningMainPanel mainPanel)
	{
		this.m_main = mainPanel;
		m_bFirst = new CButton("First [<<]");
		m_bFirst.addActionListener(this.m_main);
		m_bPrev = new CButton("Prev [<]");
		m_bPrev.addActionListener(this.m_main);
		m_bNext = new CButton("Next [>]");
		m_bNext.addActionListener(this.m_main);
		m_bLast = new CButton("Last [>>]");
		this.m_bLast.addActionListener(this.m_main);
		m_bExit = new CButton("Close [x]");
		this.m_bExit.addActionListener(this.m_main);
		
		m_lPage = new CLabel("0/0");
		this.add(m_bFirst);
		this.add(m_bPrev);
		this.add(m_bNext);
		this.add(m_bLast);
		this.add(m_bExit);
		this.add(m_lPage);
		return true;
	}
	
	public void updatePage (int current, int total)
	{
		String page = "Page" + Integer.toString(current) + " of " 
					+ Integer.toString(total);
		this.m_lPage.setText(page.toString());
	}
}
