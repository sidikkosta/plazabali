/**
 * 
 */
package com.unta.userwarning;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import net.miginfocom.swing.MigLayout;

import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;

/**
 * @author nurse
 *
 */
public class UNSUserWarningHeaderPanel extends CPanel {
	
	@SuppressWarnings("unused")
	private UNSUserWarningMainPanel m_main = null;
	private CLabel l_title = new CLabel("");
	private JTextPane l_description = new JTextPane();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * 
	 */
	public UNSUserWarningHeaderPanel() 
	{
		super (new MigLayout("fillx"));
		setBorder(new LineBorder(Color.black));
	}
	
	public boolean init (UNSUserWarningMainPanel mainPanel)
	{
		this.m_main = mainPanel;
		l_title.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 24));
		l_description.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 14));
		l_description.enableInputMethods(false);
		l_description.setEditable(false);
		l_description.setBackground(null);
		SimpleAttributeSet attr = new SimpleAttributeSet();
		StyleConstants.setAlignment(attr, StyleConstants.ALIGN_CENTER);
		l_description.setParagraphAttributes(attr, false);
		this.add(l_title, "growy, center, wrap, w ::90%,");
		this.add(l_description, "growy, growx, flowx, wrap, center, w ::90%");
		return true;
	}
	
	public void setTitle (String title)
	{
		this.l_title.setText(title);
	}
	
	public void setDescription (String description)
	{
		this.l_description.setText(description);
	}
}
