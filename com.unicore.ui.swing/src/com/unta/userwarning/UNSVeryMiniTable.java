/**
 * 
 */
package com.unta.userwarning;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import org.adempiere.plaf.AdempierePLAF;
import org.compiere.minigrid.MiniTable;

/**
 * @author nurse
 *
 */
public class UNSVeryMiniTable extends MiniTable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9112387031375272703L;

	public UNSVeryMiniTable() {

		super();
		setRowSelectionAllowed(true);
		setColumnSelectionAllowed(false);
		setMultiSelection(false);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setRowHeight(30);
		setFillsViewportHeight(true);
		setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
	}
	
	public void growScrollbars() 
	{
		Container p = getParent();
		if (p instanceof JViewport) 
		{
			Container gp = p.getParent();
			if (gp instanceof JScrollPane) 
			{
				JScrollPane scrollPane = (JScrollPane) gp;             
				scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(30,0));
				scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0,30));
			}
		}
	}
	
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int rowIndex,
			int vColIndex) 
	{
		Component c = super.prepareRenderer(renderer, rowIndex, vColIndex);
		if (c==null) return c;
		if (isCellSelected(rowIndex, vColIndex)) c.setBackground(new Color(235, 211, 50)); 
		else if (rowIndex % 2 == 0) c.setBackground(new Color(255, 230, 244));
		else c.setBackground(AdempierePLAF.getFieldBackground_Selected());
		
		return c; 
	}

}
