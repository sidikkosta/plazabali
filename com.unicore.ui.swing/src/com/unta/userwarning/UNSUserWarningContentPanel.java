/**
 * 
 */
package com.unta.userwarning;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import net.miginfocom.swing.MigLayout;

import org.adempiere.plaf.AdempierePLAF;
import org.compiere.swing.CPanel;
import org.compiere.swing.CScrollPane;

import com.uns.util.UNSUserWarningContentLoader;
import com.uns.util.UNSUserWarningLoader;

/**
 * @author nurse
 *
 */
public class UNSUserWarningContentPanel extends CPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UNSUserWarningMainPanel m_main = null;
	private UNSUserWarningContentLoader[] m_contents = null;
	private int m_activeContentIdx = -1;
	private UNSVeryMiniTable m_table = null;
	private Timer m_timer = null;

	/**
	 * 
	 */
	public UNSUserWarningContentPanel() 
	{
		super (new MigLayout("wrap 1, ins 0 0", "[grow,fill]"));
	}
	
	public void loadData ()
	{
		DefaultTableModel model = new DefaultTableModel(
				this.m_contents[this.m_activeContentIdx].getContents(false),
				this.m_contents[this.m_activeContentIdx].getHeaders(false));
		this.m_table.setModel(model);
		this.m_main.updateInfo();
	}
	
	public void updateData ()
	{
		DefaultTableModel model = new DefaultTableModel(
				this.m_contents[this.m_activeContentIdx].getContents(true),
				this.m_contents[this.m_activeContentIdx].getHeaders(false));
		this.m_table.setModel(model);
		this.m_main.updateInfo();
//		m_table.updateUI();
//		m_main.updateUI();
	}
	
	public boolean init (UNSUserWarningMainPanel mainPanel)
	{
		this.m_main = mainPanel;
		UNSUserWarningLoader loader = new UNSUserWarningLoader(this.m_main.getCtx(), 
				this.m_main.getAD_Client_ID(), this.m_main.getAD_Role_ID(), 
				this.m_main.get_TrxName());
		this.m_contents = loader.load(false);
		if (m_contents.length == 0)
			return false;

		this.m_table = new UNSVeryMiniTable();
		
		CScrollPane scroll = new CScrollPane(this.m_table);
		this.m_table.getTableHeader().setFont(AdempierePLAF.getFont_Field().
				deriveFont(16f));
		this.m_table.getTableHeader().setAlignmentX(JTableHeader.CENTER_ALIGNMENT);
		this.m_table.getTableHeader().setAlignmentY(JTableHeader.CENTER_ALIGNMENT);
		this.m_table.setFocusable(false);
		this.m_table.setFillsViewportHeight( true ); 
		this.m_table.growScrollbars();
		this.m_table.getTableHeader().setPreferredSize(
				new Dimension(scroll.getWidth(), 30));
		loadFirst();
		this.add (scroll, "growx, spanx, growy, pushy");
		m_timer = new Timer(60000, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				updateData();
			}
		});
		m_timer.start();
		
		return true;
	}
	
	public void loadFirst ()
	{
		this.m_activeContentIdx = 0;
		loadData();
	}
	
	public void loadLast ()
	{
		this.m_activeContentIdx = this.m_contents.length -1;
		loadData();
	}
	
	public void loadNext ()
	{
		if (this.m_activeContentIdx >= m_contents.length-1)
			return;
		this.m_activeContentIdx++;
		loadData();
	}
	
	public void loadPrev ()
	{
		if (this.m_activeContentIdx <= 0)
			return;
		this.m_activeContentIdx--;
		loadData();
	}
	
	public boolean isFirst ()
	{
		return this.m_activeContentIdx == 0;
	}
	
	public boolean isLast ()
	{
		return this.m_activeContentIdx == this.m_contents.length-1;
	}
	
	public String getTitle ()
	{
		return this.m_contents[this.m_activeContentIdx].getTitle();
	}
	
	public String getDescription ()
	{
		return this.m_contents[this.m_activeContentIdx].getDescription();
	}
	
	public int getActiveIdx ()
	{
		return this.m_activeContentIdx;
	}
	
	public int getCurrentIdx ()
	{
		return this.m_activeContentIdx;
	}
	
	public int getTotalContent ()
	{
		return this.m_contents.length;
	}
	
	public void dispose ()
	{
		if (m_timer != null)
			m_timer.stop();
	}
}
