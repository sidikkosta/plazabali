package com.pb.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MAttributeSetInstance;
//import com.pb.plugins.model.MAttributeSetInstance;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import com.pb.model.MImportStockAdjustment;
/**
 * @author febriansyah.s
 */

public class ImportStockAdjustment extends SvrProcess{
	
	static PreparedStatement ps;
	static ResultSet rs;
	String ErrorMsg, HeadMsg, LineMsg ;
	static ArrayList<Integer> ID_List = new ArrayList<Integer>();
	static MInventoryLine mil = null;
	static MInventory mi = null;
	static MImportStockAdjustment importSA = null; 
	static MAttributeSetInstance masi = null;
	String sql; int ID_;
	boolean head, line, saveSA;
	
	@Override
	protected void prepare() {}

	@Override
	protected String doIt() throws Exception {
		try {
			String sqlImp = "SELECT count(I_Inventory_id) FROM I_Inventory WHERE isactive='Y' AND processed='N' AND i_isimported='N' ";
			int ImpEntry = DB.getSQLValue(get_TrxName(), sqlImp);
			if((Integer)ImpEntry == null ){
				ErrorMsg = "Data Import Kosong"; }
			if(ImpEntry == 0){
				ErrorMsg = "Seluruh Data Telah Di Import"; }
			if((Integer)ImpEntry != null || ImpEntry > 1){
			
				String _record = "SELECT I_Inventory_id FROM I_Inventory WHERE isactive='Y' AND processed='N' AND i_isimported='N' ORDER BY M_Warehouse_ID, movementdate ";
				ps = DB.prepareStatement(_record.toString(), get_TrxName());				
				rs = ps.executeQuery();
				while(rs.next()){
					ID_List.add(rs.getInt("I_Inventory_id")); }

				int i, i_ID;
				for (i = 0; i < ImpEntry; i++){
					i_ID = ID_List.get(i);
					if(( Integer )i_ID != null && i_ID > 0 ){
//						m_process.statusUpdate("Sedang proses data ke " + i + " dari " + ImpEntry + " .");
						saveSA = saveData(i_ID, get_TrxName());
						if(saveSA == true) {
							ErrorMsg = "Berhasil Import Stock Adjustment"; 
							sql  = "DELETE FROM i_inventory WHERE isactive='Y' AND processed='Y' AND i_isimported='Y' ";
							ID_ = DB.executeUpdate(sql, get_TrxName());
						}else {
							ErrorMsg = "GAGAL Import Stock Adjustment"; 
						}
					}
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Error Disini ImportStockAdjustment", e); }
		return ErrorMsg;
	}
	
	protected boolean saveData(int ImportI_D, String trx) {
		importSA = new MImportStockAdjustment(getCtx(), ImportI_D, trx);
		mi = new MInventory(getCtx(), 0, trx);
		ID_ = 0;
		
		if((Integer)importSA.getM_Inventory_ID() != null || importSA.getM_Inventory_ID() > 0){
			line =saveDataLine(importSA.getM_Inventory_ID(), ImportI_D, trx);
			if(line == true) {
				log.log(Level.WARNING, "BERHASIL SAVE LINE ");
				importSA.setI_IsImported(true);
				importSA.setProcessed(true);
				importSA.setM_Inventory_ID(mi.get_ID());
				importSA.save();
				head = true; 
			}else {
				log.log(Level.WARNING, "GAGAL SAVE LINE ");
				head = false;}
		}else{
			sql  = "SELECT M_Inventory_ID FROM M_Inventory WHERE M_Warehouse_ID = "+importSA.getM_Warehouse_ID()+" and docstatus  not in ('RE', 'CO') ";
			ID_ = DB.getSQLValue(get_TrxName(), sql);
			if( ID_  > 0 ) {
				line = saveDataLine(ID_, ImportI_D, trx);
				if(line == true){
					log.log(Level.WARNING, "BERHASIL SAVE LINE ");
					importSA.setI_IsImported(true);
					importSA.setProcessed(true);
					importSA.setM_Inventory_ID(mi.get_ID());
					importSA.save();
					head = true; 
				}else{ 
					log.log(Level.WARNING, "GAGAL SAVE LINE ");
					head = false;
				}
			}if( ID_  <= 0 ) {
//				mi.setAD_Client_ID()(misa.getAD_Client_ID());
				mi.setAD_Org_ID(importSA.getAD_Org_ID());
				mi.setC_DocType_ID(importSA.getC_DocType_ID());
				mi.setM_Warehouse_ID(importSA.getM_Warehouse_ID());
				mi.setDateAcct(importSA.getMovementDate());
				mi.setMovementDate(importSA.getMovementDate());
				mi.setDescription(importSA.getDescription());
				mi.setDocStatus("DR");
				mi.setDocAction("DR");
				mi.setProcessed(false);
				mi.setIsApproved(false);
				mi.setIsActive(true);
				if(mi!=null){
					if(mi.save() == true){
						mi.get_ID();
						line =saveDataLine(mi.get_ID(), ImportI_D, trx);
						if(line == true) {
							ErrorMsg = "BERHASIL Save Line";
							importSA.setI_IsImported(true);
							importSA.setProcessed(true);
							importSA.setM_Inventory_ID(mi.get_ID());
							importSA.save();
							head = true; 
						}else {
							ErrorMsg = "GAGAL Save Line";
							head = false;
						}
					}
				}
			}			
		}
		return head;
	}
		
	protected boolean saveDataLine(int Inventory_ID, int Import_ID, String trx) {
		importSA = new MImportStockAdjustment(getCtx(), Import_ID, trx);
		sql  = "SELECT m_inventoryline_id FROM m_inventoryline "
			+ "WHERE m_inventory_id = "+Inventory_ID+" AND m_locator_id = "+importSA.getM_Locator_ID()+" "
			+ "AND m_product_id = "+getProduct_ID(importSA.getValue())+" AND m_attributesetinstance_id = "+getASI_ID(importSA.getSerNo(), trx)+" ";
		ID_ = DB.getSQLValue(trx, sql);
		if( ID_  > 0 ) {
			mil = new MInventoryLine(getCtx(), ID_ , trx);
			mil.setQtyInternalUse(mil.getQtyInternalUse().add(importSA.getQtyInternalUse()));
			mil.setUOMQtyInternalUseL1(mil.getUOMQtyInternalUseL1().add(importSA.getQtyInternalUse()));
			if(mil!=null){
				if(mil.save() == true) {
					line= true; 
					importSA.setM_InventoryLine_ID(mil.get_ID());
					importSA.save();
				}else {
					line= false;
				}
			}
		}if( ID_  <= 0 ) {
			mil = new MInventoryLine(getCtx(), 0, trx);
			mil.setAD_Org_ID(importSA.getAD_Org_ID());
			mil.setM_Inventory_ID(Inventory_ID);
			mil.setM_Product_ID(getProduct_ID(importSA.getValue()));
			mil.setM_Locator_ID(importSA.getM_Locator_ID());
			mil.setM_AttributeSetInstance_ID(getASI_ID(importSA.getSerNo(), trx));
			mil.setQtyInternalUse(importSA.getQtyInternalUse());
			mil.setUOMQtyInternalUseL1(importSA.getQtyInternalUse());
			if(mil!=null){
				if(mil.save() == true) {
					line= true; 
					importSA.setM_InventoryLine_ID(mil.get_ID());
					importSA.save();
				}else {
					line= false;
				}
			}
		}return line;
	}

	protected int getProduct_ID(String Value ){
		if(Value.length() == 6){
			sql  = "SELECT M_Product_ID FROM M_Product WHERE value = '00"+Value+"'  ";
		}else if(Value.length() == 7){
			sql  = "SELECT M_Product_ID FROM M_Product WHERE value = '0"+Value+"'  ";
		}else{
			sql  = "SELECT M_Product_ID FROM M_Product WHERE value = '"+Value+"'  ";
		}
		ID_ = DB.getSQLValue(get_TrxName(), sql);
		return ID_;
	}
	
	protected int getASI_ID(String ASI_Desc, String trx ) {
		int asi_id = 0;
		masi = new MAttributeSetInstance(getCtx(), 0, trx);

		sql = "select m_attributesetinstance_id from m_attributesetinstance where description = '"+ASI_Desc+"' ";
		ID_ = DB.getSQLValue(get_TrxName(), sql);
		if( ID_  > 0 ) {
			asi_id = ID_;
		}if( ID_  <= 0 ) {
			masi.setDescription(ASI_Desc);
			masi.setM_AttributeSet_ID(1000036);
			if(masi!=null){
				if(masi.save() == true){
					asi_id = masi.get_ID();
				}
			}
		}return asi_id;
	}
}
