package com.pb.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Util;

public class MAttributeSetInstance extends org.compiere.model.MAttributeSetInstance {


	public MAttributeSetInstance(Properties ctx, int M_AttributeSetInstance_ID, int M_AttributeSet_ID, String trxName) {
		super(ctx, M_AttributeSetInstance_ID, M_AttributeSet_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MAttributeSetInstance(Properties ctx, int M_AttributeSetInstance_ID, String trxName) {
		super(ctx, M_AttributeSetInstance_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MAttributeSetInstance(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if((newRecord || is_ValueChanged(COLUMNNAME_Description)) && !Util.isEmpty(getDescription(), true))
		{
//			String sql = "SELECT COUNT(*) FROM M_AttributeSetInstance"
//					+ " WHERE Description = ? AND M_AttributeSetInstance_ID <> ?";
//			int count = DB.getSQLValue(get_TrxName(), sql, getDescription(), get_ID());
//			if(count > 0)
//			{
//				log.saveError("Error", "Record has already exists.");
//				return false;
//			}
		}
		
		if((newRecord || is_ValueChanged(COLUMNNAME_Description)) && getM_AttributeSet_ID() == 1000036)
		{
			log.saveError("Error", getDescription());
//			if(getDescription() == null || Util.isEmpty(getDescription(), true))
//			{
//				log.saveError("Error", "Description can't null :: General Attribute.!");
//				return false;
//			}
		}
		return true;
	}
	
	

}
