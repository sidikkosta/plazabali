package com.pb.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.X_I_Inventory;
/**
 * @author febriansyah.s
 */

public class MImportStockAdjustment extends X_I_Inventory{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MImportStockAdjustment(Properties ctx, int I_Inventory_ID, String trxName) {
		super(ctx, I_Inventory_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MImportStockAdjustment(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
}
