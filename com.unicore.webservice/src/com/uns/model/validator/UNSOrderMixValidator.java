package com.uns.model.validator;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MClient;
import org.compiere.model.MWebServiceType;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.idempiere.adInterface.x10.ADLoginRequest;
import org.idempiere.adInterface.x10.DataField;
import org.idempiere.webservices.IWSValidator;
import org.idempiere.webservices.fault.IdempiereServiceFault;
import com.unicore.model.MUNSVoucherCode;
import com.uns.model.process.VoucherOnlineTransaction;
import com.uns.util.UNSApps;

public class UNSOrderMixValidator implements ModelValidator, IWSValidator 
{

	private int p_clienID = 0;
	private CLogger log = CLogger.getCLogger(UNSOrderMixValidator.class);
	
	public UNSOrderMixValidator() 
	{
		super ();
	}

	@Override
	public void validate(PO po, MWebServiceType m_webserviceType,
			DataField[] fields, int time, String trxName,
			Map<String, Object> requestCtx) throws IdempiereServiceFault 
	{
		if (!po.get_TableName().equals("UNS_VoucherCode"))
			return;
		if (time != IWSValidator.TIMING_BEFORE_SAVE)
			return ;
		MUNSVoucherCode vc = (MUNSVoucherCode) po;
		BigDecimal usedAmt = vc.getUsedAmt();
		String requestedAmtStr = getValueFromFields("UsedAmt", fields);
		if (requestedAmtStr == null)
			throw new IdempiereServiceFault("Request amount is not defined", null);

		BigDecimal requestedAmt = Env.ZERO;
		try
		{
			requestedAmt = new BigDecimal(requestedAmtStr);
		}
		catch (NumberFormatException ex)
		{
			throw new IdempiereServiceFault(ex);
		}
		
		usedAmt = usedAmt.add(requestedAmt);
		vc.setUsedAmt(usedAmt);
	}

	@Override
	public void login(ADLoginRequest loginRequest, Properties ctx,
			MWebServiceType m_webserviceType, int time)
			throws IdempiereServiceFault 
	{
		//Do nothing
	}

	@Override
	public void initialize(ModelValidationEngine engine, MClient client) 
	{
		if (client != null)
		{
			p_clienID = client.get_ID();
			log.log(Level.INFO, client.toString());
		}
		else
			log.log(Level.INFO, "Initializing global validator -" + this.toString());
		engine.addModelChange("UNS_PaymentTrx", this);
	}

	@Override
	public int getAD_Client_ID() 
	{
		return p_clienID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) 
	{
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception 
	{
		if (po.isReplication())
			return null;
		if (po.get_TableName().equals("UNS_PaymentTrx") && UNSApps.SERVER_TYPE.equals(UNSApps.SERVER_POS)
				&& !po.get_ValueAsBoolean("IsManual"))
		{
			String paymethod = po.get_ValueAsString("PaymentMethod");
			if (!"3".equals(paymethod))
				return null;
			String vc = (String )po.get_Value("TrxNo");
			int currencyID = (Integer) po.get_ValueAsInt("C_Currency_ID");		

			PO parent = po.getParent();
			Timestamp date = (Timestamp) parent.get_Value("DateAcct");
			
			if (type == ModelValidator.TYPE_BEFORE_CHANGE
					|| type == ModelValidator.TYPE_BEFORE_NEW)
			{
				if (!po.is_ValueChanged("TrxAmt")
						&& !po.is_new())
					return null;
				if (Util.isEmpty(vc, true))
					return "Invalid Voucher Code";
				if (currencyID <= 0)
					return "Undefined Currency";
				BigDecimal oldUsed = (BigDecimal) po.get_ValueOld("TrxAmt");
				BigDecimal newUsed = (BigDecimal) po.get_Value("TrxAmt");
				if (oldUsed == null)
					oldUsed = Env.ZERO;
				if (newUsed == null)
					newUsed = Env.ZERO;
				BigDecimal amount = newUsed.subtract(oldUsed);
				if (amount.signum() == 0)
					return null;
				VoucherOnlineTransaction vo = new VoucherOnlineTransaction(
						po.getAD_Org_ID(), date, currencyID, vc, amount);
				if (!vo.transact())
					return vo.getErrorMsg();
			}
			else if (type == ModelValidator.TYPE_AFTER_DELETE)
			{
				if (Util.isEmpty(vc, true))
					return null;
				if (currencyID <= 0)
					return null;
				BigDecimal amount = (BigDecimal) po.get_ValueOld("TrxAmt");
				if (amount == null)
					amount = Env.ZERO;
				amount = amount.negate();
				if (amount.signum() == 0)
					return null;
				VoucherOnlineTransaction vo = new VoucherOnlineTransaction(
						po.getAD_Org_ID(), date, currencyID, vc, amount);
				if (!vo.transact())
					return vo.getErrorMsg();
			}
		}
		
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) 
	{
		return null;
	}

	private String getValueFromFields (String key, DataField[] fields)
	{
		String value = null;
		for (int i=0; i< fields.length; i++)
		{
			DataField field = fields[i];
			if (field.getColumn().equals(key))
			{
				value = field.getVal();
				break;
			}
		}
		
		return value;
	}
}
