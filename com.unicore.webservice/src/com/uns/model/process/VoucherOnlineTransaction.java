/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.apache.axis.soap.MessageFactoryImpl;
import org.apache.xmlbeans.XmlException;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;
import org.idempiere.adInterface.x10.ADLoginRequest;
import org.idempiere.adInterface.x10.DataField;
import org.idempiere.adInterface.x10.DataRow;
import org.idempiere.adInterface.x10.DataSet;
import org.idempiere.adInterface.x10.ModelCRUD;
import org.idempiere.adInterface.x10.ModelCRUDRequest;
import org.idempiere.adInterface.x10.ModelCRUDRequestDocument;
import org.idempiere.adInterface.x10.WindowTabData;
import org.idempiere.adInterface.x10.WindowTabDataDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author nurse
 *
 */
public class VoucherOnlineTransaction 
{

	private int p_currencyID = 0;
	private String p_voucherCode = null;
	private BigDecimal p_amount = Env.ZERO; String p_error = null;
	private boolean p_isError = false;
	private Timestamp p_date = null;
	private int p_orgID = 0;
	private final String ENDPOINT = MSysConfig.getValue("VCR_WS_ENDPOINT", 
			"http://172.16.1.188:8080/ADInterface/services/ModelADService");
	
	private final String WS_USER = "VOUCHER_WS_USER_NAME";
	private final String WS_PASS = "VOUCHER_WS_PASSWORD";
	private final String WS_LANG = "VOUCHER_WS_LANG";
	private final String WS_CLIENT = "VOUCHER_WS_CLIENT";
	private final String WS_ORG = "VOUCHER_WS_ORG";
	private final String WS_ROLE = "VOUCHER_WS_ROLE";
	private final String WS_WHS = "VOUCHER_WS_WHS";
	
	private final String[] m_errorIndicator = new String[] {
			"IsError", "IsRolledBack", "Error"	
		};
	
	/**
	 * 
	 * @param orgID
	 * @param date
	 * @param currencyID
	 * @param voucherCode
	 * @param amount
	 */
	public VoucherOnlineTransaction(int orgID, Timestamp date, int currencyID, 
			String voucherCode, BigDecimal amount) 
	{
		p_orgID = orgID;
		p_date = date;
		p_currencyID = currencyID;
		p_voucherCode = voucherCode;
		p_amount = amount;
	}
	
	private ADLoginRequest initLoginValue (ADLoginRequest loginReq)
	{
		int clientID = Env.getAD_Client_ID(Env.getCtx());
		int wsCliID = MSysConfig.getIntValue(WS_CLIENT, clientID, clientID);
		String userName = MSysConfig.getValue(WS_USER, "WSCashier", clientID);
		String password = MSysConfig.getValue(WS_PASS, "WSCashier10101988", clientID);
		String lang = MSysConfig.getValue(WS_LANG, "en_US", clientID);
		int orgID = MSysConfig.getIntValue(WS_ORG, 1000054, clientID);
		int roleID = MSysConfig.getIntValue(WS_ROLE, 1000211, clientID);
		int whsID = MSysConfig.getIntValue(WS_WHS, 1000139, clientID);
		loginReq.setUser(userName);
		loginReq.setPass(password);
		loginReq.setLang(lang);
		loginReq.setClientID(wsCliID);
		loginReq.setOrgID(orgID);
		loginReq.setRoleID(roleID);
		loginReq.setWarehouseID(whsID);
		return loginReq;
	}
	
	private SOAPMessage buildCheckMessage ()
	{
		SOAPMessage message = null;
		try 
		{
			MessageFactory factory = new MessageFactoryImpl();
			message = factory.createMessage();
			SOAPPart part = message.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			ModelCRUDRequestDocument crudDocument = ModelCRUDRequestDocument.Factory.newInstance();
			ModelCRUDRequest crudRequest = crudDocument.addNewModelCRUDRequest();
			crudRequest.setADLoginRequest(initLoginValue(crudRequest.addNewADLoginRequest()));
			
			
			ModelCRUD crud = crudRequest.addNewModelCRUD();
			crud.setTableName("UNS_VoucherCode");
			crud.setAction(ModelCRUD.Action.READ);
			crud.setServiceType("Msrv_VoucherCodeQ");
			DataRow row = crud.addNewDataRow();
			DataField field = row.addNewField();
			field.setColumn("Name");
			field.setVal(p_voucherCode);
			field = row.addNewField();
			field.setColumn("C_Currency_ID");
			field.setVal(Integer.toString(p_currencyID));
			field = row.addNewField();
			field.setColumn("AD_Org_ID");
			field.setVal(Integer.toString(p_orgID));

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = docFactory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element element = document.createElementNS("http://idempiere.org/ADInterface/1_0", "queryData");
			Node domNode = document.importNode(crudDocument.getDomNode().getFirstChild(), true);
			document.appendChild(element);
			element.appendChild(domNode);
			body.addDocument(document);
		} 
		catch (SOAPException e) 
		{
			setError(e.getMessage());
		} 
		catch (ParserConfigurationException e) 
		{
			setError(e.getMessage());
		}
		return message;
	}
	
	private SOAPMessage sendRequest (SOAPMessage request, String endpoint)
	{
		SOAPMessage response = null;
		try 
		{
			SOAPConnectionFactory cFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection connection = cFactory.createConnection();
			response = connection.call(request, endpoint);
		}
		catch (UnsupportedOperationException e) 
		{
			setError(e.getMessage());
		} 
		catch (SOAPException e) 
		{
			setError(e.getMessage());
		}
		
		return response;
	}
	
	private SOAPMessage buildTransacMessage (int recordID)
	{
		SOAPMessage message = null;
		try 
		{
			MessageFactory factory = new MessageFactoryImpl();
			message = factory.createMessage();
			SOAPPart part = message.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			ModelCRUDRequestDocument crudDocument = ModelCRUDRequestDocument.Factory.newInstance();
			ModelCRUDRequest crudRequest = crudDocument.addNewModelCRUDRequest();
			crudRequest.setADLoginRequest(initLoginValue(crudRequest.addNewADLoginRequest()));
			
			
			ModelCRUD crud = crudRequest.addNewModelCRUD();
			crud.setTableName("UNS_VoucherCodeU");
			crud.setRecordID(recordID);
			crud.setAction(ModelCRUD.Action.UPDATE);
			crud.setServiceType("Msrv_VoucherCodeU");
			DataRow row = crud.addNewDataRow();
			DataField field = row.addNewField();
			field.setColumn("UsedAmt");
			field.setVal(p_amount.toString());

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = docFactory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element element = document.createElementNS("http://idempiere.org/ADInterface/1_0", "updateData");
			Node domNode = document.importNode(crudDocument.getDomNode().getFirstChild(), true);
			document.appendChild(element);
			element.appendChild(domNode);
			body.addDocument(document);
		} 
		catch (SOAPException e) 
		{
			setError(e.getMessage());
		} 
		catch (ParserConfigurationException e) 
		{
			setError(e.getMessage());
		}
		return message;
	}
	
	public boolean transact ()
	{
		SOAPMessage response = sendRequest(buildCheckMessage(), ENDPOINT);
		if (response == null)
		{
			if (!isError())
				setError("Unknown Error");
			return false;
		}
		try 
		{
			parseError(response);
			if (isError())
				return false;
			WindowTabDataDocument responseDoc = WindowTabDataDocument.Factory.parse(
					response.getSOAPBody().getFirstChild().getFirstChild());
			WindowTabData windowTabData = responseDoc.getWindowTabData();
			if (windowTabData.isSetError()) 
			{
				setError(windowTabData.getError());
				return false;
			}
			int id = -1;
			DataSet dataset = windowTabData.getDataSet();
			if (dataset == null)
			{
				setError("Data Set is not returned from server");
				return false;
			}
			DataRow[] dataRows = dataset.getDataRowArray();
			for(DataRow dataRow : dataRows) 
			{
				DataField[] dataFields = dataRow.getFieldArray();
				Timestamp validFrom = null;
				Timestamp validTo = null;
				boolean isInvalidate = false;
				for (int i=0; i<dataFields.length; i++)
				{
					if  (dataFields[i].getColumn().equals("ValidFrom"))
					{
						String obj = dataFields[i].getVal();
						validFrom = Timestamp.valueOf(obj);
					}
					else if (dataFields[i].getColumn().equals("ValidTo"))
					{
						String obj = dataFields[i].getVal();
						validTo = Timestamp.valueOf(obj);
					}
					else if (dataFields[i].getColumn().equals("isInvalidate"))
					{
						isInvalidate = "Y".equals(dataFields[i].getVal());
					}
				}
				if (isInvalidate && p_amount.signum() == 1)
					continue;
				if (p_date.after(validTo) || p_date.before(validFrom))
					continue;
				id = Integer.valueOf(dataFields[0].getVal());
				break;
			}
			if (id <= 0)
			{
				setError("Invalid voucher code");
				return false;
			}
			
			response = sendRequest(buildTransacMessage(id), ENDPOINT);
			if (response == null)
			{
				if (!isError())
					setError("Unknown Error");
				return false;
			}
			parseError(response);
		} 
		catch (SOAPException e) 
		{
			setError(e.getMessage());
		} 
		catch (XmlException e) 
		{
			setError(e.getMessage());
		}
		
		return !isError();
	}
	
	public String getErrorMsg ()
	{
		if (p_error == null && isError())
			p_error = "Unknown Error. " + toString() ;
		return p_error;
	}
	
	public boolean isError ()
	{
		return p_isError;
	}
	
	public String toString ()
	{
		StringBuilder builder = new StringBuilder("[Voucher Code : ")
		.append(p_voucherCode).append(" | Currency : ").append(p_currencyID)
		.append(" | Amount : ").append(p_amount).append("]");
		String ts = builder.toString();
		return ts;
	}
	
	private void setError (String error)
	{
		p_error = error;
		p_isError = true;
	}
	
	public SOAPElement getElement (@SuppressWarnings("rawtypes") Iterator attributes, String[] names)
	{
		while (attributes.hasNext())
		{
			Object attribute = attributes.next();
			if (attribute instanceof SOAPElement == false)
			{
				continue;
			}
			
			SOAPElement child = (SOAPElement) attribute;
			
			for (int i=0; i<names.length; i++)
			{
				if (child.getElementQName().getLocalPart().equals(names[i]))
				{
					return child;
				}
			}
			
			child = getElement(child.getChildElements(), names);
			
			if (null != child)
			{
				return child;
			}
		}
		
		return null;
	}
	
	
	private void parseError (SOAPMessage response) throws SOAPException
	{
		SOAPFault fault = response.getSOAPBody().getFault();
		if (null != fault)
		{
			String faultCode = fault.getFaultCode();
			String faultString = fault.getFaultString();
			String msg = faultCode + "::" + faultString;
			setError(msg);
			return;
		}
		
		@SuppressWarnings("rawtypes")
		Iterator attributes = response.getSOAPBody().getChildElements();
		SOAPElement element = getElement(attributes, m_errorIndicator);
		
		if (null != element)
		{
			setError(element.getTextContent());
		}
	}
}
