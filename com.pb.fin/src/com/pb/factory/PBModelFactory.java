package com.pb.factory;

import java.sql.ResultSet;
import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

import com.pb.model.MAttributeSetInstance;
import com.pb.model.MImportStockAdjustment;
/**
 * @author febriansyah.s
 */

public class PBModelFactory implements IModelFactory{

	@Override
	public Class<?> getClass(String tableName) {
		// Warehouse Access
		if(tableName.equalsIgnoreCase("I_Inventory")) {
			return MImportStockAdjustment.class;
		}else if(tableName.equalsIgnoreCase("X_M_AttributeSetInstance")) {
			return MAttributeSetInstance.class;
		}

		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		if(tableName.equalsIgnoreCase("I_Inventory")) {
			return new MImportStockAdjustment(Env.getCtx(), Record_ID, trxName);
		}else if(tableName.equalsIgnoreCase("X_M_AttributeSetInstance")) {
			return new MAttributeSetInstance(Env.getCtx(), Record_ID, trxName);
		}

		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		if( tableName.equalsIgnoreCase( MImportStockAdjustment.Table_Name ) ) {
            return new MImportStockAdjustment(Env.getCtx(), rs, trxName);
		}else if( tableName.equalsIgnoreCase( MAttributeSetInstance.Table_Name ) ) {
            return new MAttributeSetInstance(Env.getCtx(), rs, trxName);
		}

		return null;
	}

}
