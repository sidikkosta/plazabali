package com.pb.factory;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import com.pb.process.ImportStockAdjustment;
/**
 * @author febriansyah.s
 */

public class PBReportProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		if (className.equals("com.pb.process.ImportStockAdjustment")) {
			return new ImportStockAdjustment();
		}
	return null;
	}
}
