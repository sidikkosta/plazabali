/**
 * 
 */
package com.uns.model.validator;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.process.rpl.exp.ExportHelper;
import org.compiere.model.MClient;
import org.compiere.model.MMovement;
import org.compiere.model.MReplicationStrategy;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTable;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.model.X_AD_ReplicationDocument;
import org.compiere.model.X_AD_ReplicationTable;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author nurse
 *
 */
public class UNSExportModelValidator implements ModelValidator 
{	
	public static final String	CTX_IsReplicationEnabled = "#IsReplicationEnabled";
	private static CLogger 		log = CLogger.getCLogger(UNSExportModelValidator.class);
	private int					m_AD_Client_ID = -1;
	List<String> 				modelTables = new ArrayList<String>();
	List<String> 				docTables = new ArrayList<String>();
	Hashtable<Integer, ExportHelper> helpers = new Hashtable<Integer, ExportHelper>();
	private final String DEFAULT_ORG_IDS = "0,1000054,1000055,1000056";
	
	
	/**
	 *	Constructor.
	 *	The class is instantiated when logging in and client is selected/known
	 */
	public UNSExportModelValidator() 
	{
		super ();
	}

	/**
	 *	Initialize Validation
	 *	@param engine validation engine
	 *	@param client client
	 */
	public void initialize (ModelValidationEngine engine, MClient client) 
	{
		if (client != null) 
		{
			m_AD_Client_ID = client.getAD_Client_ID();
			if (log.isLoggable(Level.INFO)) log.info(client.toString());
			loadReplicationStrategy(engine);
		} 
		else 
		{
			log.warning("Export Model Validator cannot be used as a global validator, it needs to be defined in a per-client (tenant) basis");
			return;
		}
	}

	public void loadReplicationStrategy(ModelValidationEngine engine)
	{
		MClient client = MClient.get(Env.getCtx(), m_AD_Client_ID);
		String where = " AD_Client_ID = ?";
		List<MReplicationStrategy> rss = new Query(Env.getCtx(), MReplicationStrategy.Table_Name, where, null)
			.setOnlyActiveRecords(true)
			.setParameters(m_AD_Client_ID)
			.list();
		
		for (MReplicationStrategy rplStrategy : rss) 
		{
			ExportHelper expClientHelper = new ExportHelper(client, rplStrategy);
			helpers.put(rplStrategy.getAD_ReplicationStrategy_ID(), expClientHelper);
			for (X_AD_ReplicationTable rplTable : rplStrategy.getReplicationTables()) 
			{
				if (   X_AD_ReplicationTable.REPLICATIONTYPE_Merge.equals(rplTable.getReplicationType())
					|| X_AD_ReplicationTable.REPLICATIONTYPE_Broadcast.equals(rplTable.getReplicationType())
					|| X_AD_ReplicationTable.REPLICATIONTYPE_Reference.equals(rplTable.getReplicationType()))
				{
					String tableName = MTable.getTableName(client.getCtx(), rplTable.getAD_Table_ID());
					if (! modelTables.contains(tableName)) 
					{
						engine.addModelChange(tableName, this);
						modelTables.add(tableName);
					}
				}
			}
			
			for (X_AD_ReplicationDocument rplDocument : rplStrategy.getReplicationDocuments()) 
			{
				if (X_AD_ReplicationDocument.REPLICATIONTYPE_Merge.equals(rplDocument.getReplicationType())
					|| X_AD_ReplicationDocument.REPLICATIONTYPE_Reference.equals(rplDocument.getReplicationType())
					|| X_AD_ReplicationDocument.REPLICATIONTYPE_Broadcast.equals(rplDocument.getReplicationType()))
				{
					String tableName = MTable.getTableName(client.getCtx(), rplDocument.getAD_Table_ID());
					if (! docTables.contains(tableName)) 
					{
						engine.addDocValidate(tableName, this);
						docTables.add(tableName);
					}
				}
			}
		}
	}

	/**
	 *	Model Change of a monitored Table.
	 *	Called after PO.beforeSave/PO.beforeDelete
	 *	@param po persistent object
	 *	@param type TYPE_
	 *	@return error message or null
	 *	@exception Exception if the recipient wishes the change to be not accept.
	 */
	public String modelChange (PO po, int type) throws Exception 
	{
		if (po.isReplication()) 
		{
			return null;
		}
		
		String orgStr = MSysConfig.getValue("REPLICATION_ORG", DEFAULT_ORG_IDS);
		String orgs[] = orgStr.split(",");
		boolean allowReplica = false;
		for (int i=0; i<orgs.length; i++)
		{
			Integer org = Integer.valueOf(orgs[i]);
			if (org == po.getAD_Org_ID())
			{
				allowReplica = true;
				break;
			}
		}
		if (!allowReplica)
			return null;
		
		if (log.isLoggable(Level.INFO)) log.info("po.get_TableName() = " + po.get_TableName());
		List<Integer> rsIDs = getReplicationStrategy(po);
		for (int i=0; i<rsIDs.size(); i++)
		{
			ExportHelper expHelper = helpers.get(rsIDs.get(i));
			if (expHelper != null) 
			{
				if (   type == TYPE_AFTER_CHANGE
						|| type == TYPE_AFTER_NEW
						|| type == TYPE_BEFORE_DELETE) 
				{
					X_AD_ReplicationTable replicationTable = MReplicationStrategy.getReplicationTable(
							po.getCtx(), rsIDs.get(i), po.get_Table_ID());
					if (replicationTable != null) 
					{
						expHelper.exportRecord(
								po,
								MReplicationStrategy.REPLICATION_TABLE,
								replicationTable.getReplicationType(),
								type);
					}
				}
			}
		}
		
		return null;
	}

	/**
	 *	Validate Document.
	 *	Called as first step of DocAction.prepareIt
	 *	when you called addDocValidate for the table.
	 *	@param po persistent object
	 *	@param type see TIMING_ constants
	 *	@return error message or null
	 * @throws Exception
	 */
	public String docValidate (PO po, int type)
	{
		if (po.isReplication()) 
		{
			return null;
		}
		
		String orgStr = MSysConfig.getValue("REPLICATION_ORG", DEFAULT_ORG_IDS);
		String orgs[] = orgStr.split(",");
		boolean allowReplica = false;
		for (int i=0; i<orgs.length; i++)
		{
			Integer org = Integer.valueOf(orgs[i]);
			if (org == po.getAD_Org_ID())
			{
				allowReplica = true;
				break;
			}
		}
		if (!allowReplica)
			return null;
		
		if (log.isLoggable(Level.INFO)) log.info("Replicate the Document = " + po.get_TableName() + " with Type = " + type);
		String result = null;
		List<Integer> rsIDs = getReplicationStrategy(po);
		for (int i=0; i<rsIDs.size(); i++)
		{
			ExportHelper expHelper = helpers.get(rsIDs.get(i));
			if (expHelper != null) 
			{
				try 
				{
					if (   type == TIMING_AFTER_COMPLETE
						|| type == TIMING_AFTER_CLOSE
						|| type == TIMING_AFTER_REVERSECORRECT
						|| type == TIMING_AFTER_REVERSEACCRUAL
						|| type == TIMING_AFTER_VOID
						|| type == TIMING_AFTER_REACTIVATE
						|| (type == TIMING_AFTER_PREPARE && "UNS_POS_Session".equals(po.get_TableName()) 
						&& "DR".equals(po.get_ValueAsString("DocStatus")))
						) 
					{
						X_AD_ReplicationDocument replicationDocument = null;
						int C_DocType_ID = po.get_ValueAsInt("C_DocType_ID");
						if (C_DocType_ID > 0) 
						{
							replicationDocument = MReplicationStrategy.getReplicationDocument(
									po.getCtx(), rsIDs.get(i), po.get_Table_ID(), C_DocType_ID);
						} 
						else 
						{
							replicationDocument = MReplicationStrategy.getReplicationDocument(
									po.getCtx(), rsIDs.get(i), po.get_Table_ID());
						}

						if (replicationDocument != null) 
						{
							expHelper.exportRecord(
									po,
									MReplicationStrategy.REPLICATION_DOCUMENT,
									replicationDocument.getReplicationType(),
									type);
						}
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
					result = e.toString();
				}
			}
		}
		return result;
	}
	
	private List<Integer> getReplicationStrategy (PO po)
	{
		List<Integer> list = new ArrayList<>();
		int colIdx = po.get_ColumnIndex(MMovement.COLUMNNAME_DestinationWarehouse_ID);
		if (colIdx != -1 && po.get_ValueAsInt(colIdx) > 0)
		{
			String sql = "SELECT AD_ReplicationStrategy_ID FROM M_Warehouse WHERE M_Warehouse_ID = ?";
			int value = DB.getSQLValue(po.get_TrxName(), sql, po.get_ValueAsInt(colIdx));
			if (value > 0)
				list.add(value);
		}
		if (list.size() == 0)
		{
			String sql = "SELECT ARRAY_TO_STRING (ARRAY_AGG(DISTINCT AD_ReplicationStrategy_ID), ';') FROM AD_ReplicationStrategy "
					+ " WHERE AD_ReplicationStrategy_ID IN (SELECT AD_ReplicationStrategy_ID FROM "
					+ " AD_ReplicationTable WHERE AD_Table_ID = ? AND IsActive = 'Y' UNION SELECT AD_ReplicationStrategy_ID "
					+ " FROM AD_ReplicationDocument WHERE AD_Table_ID = ? AND IsActive = 'Y') AND IsActive = 'Y'";
			String results = DB.getSQLValueString(po.get_TrxName(), sql, po.get_Table_ID(), po.get_Table_ID());
			if (results != null)
			{
				String[] result = results.split(";");
				for (int i=0; i<result.length; i++)
				{
					Integer value = Integer.valueOf(result[i]);
					list.add(value);
				}
			}
		}
		
		return list;
	}

	/**
	 *	User Login.
	 *	Called when preferences are set
	 *	@param AD_Org_ID org
	 *	@param AD_Role_ID role
	 *	@param AD_User_ID user
	 *	@return error message or null
	 */
	public String login (int AD_Org_ID, int AD_Role_ID, int AD_User_ID) 
	{
		Env.setContext(Env.getCtx(), CTX_IsReplicationEnabled, true);
		return null;
	}

	/**
	 *	Get Client to be monitored
	 *	@return AD_Client_ID client
	 */
	public int getAD_Client_ID()
	{
		return m_AD_Client_ID;
	}

	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString () 
	{
		StringBuilder sb = new StringBuilder (UNSExportModelValidator.class.getName());
		return sb.toString();
	}
}
