/**
 * 
 */
package test;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Enumeration;

/**
 * @author Harry
 *
 */
public class TestingPurposes {

	/**
	 * 
	 */
	public TestingPurposes() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//testingMathOperation();
		//testingDate();
		getMacAddresses();
	}

	static void testingMathOperation()
	{
		System.out.println("20 % 12 = " + (20 % 12));
		System.out.println("12 % 12 = " + (12 % 12));
		System.out.println("11 % 12 = " + (11 % 12));
		System.out.println("8 % 12 = " + (8 % 12));
		System.out.println("(int) (4 / 12) = " + ((int)(4 / 12)));
		System.out.println("(int) (12 / 12) = " + ((int)(12 / 12)));
	}
	
	static void testingDate()
	{
		Calendar cal = Calendar.getInstance();
		cal.set(2019, 0, 1);
		
		System.out.println("Calendar value : " + cal.getTime());
		cal.add(Calendar.MONTH, -16);
		System.out.println("Calendar - 16 month = " + cal.getTime());
	}
	
	static void getMacAddresses()
	{
		try {
			Enumeration<NetworkInterface> netItList = NetworkInterface.getNetworkInterfaces();
			
			while (netItList.hasMoreElements())
			{
				NetworkInterface netIf = netItList.nextElement();
				
				byte[] mac = netIf.getHardwareAddress();
				StringBuilder macSB = new StringBuilder();
				
				if(mac == null)
					continue;
				
				for (int i=0; i < mac.length; i++)
				{
					macSB.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
				}
				
				System.out.println(macSB);
			}
		}
		catch (SocketException ex) {
			ex.printStackTrace();
		}
	}
}
