/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author AzHaidar
 *
 */
public class MUNSFFBLoadUnloadCost extends X_UNS_FFB_LoadUnload_Cost {

	/**
	 * 
	 */
	private static final long serialVersionUID = -112064370895434252L;

	/**
	 * @param ctx
	 * @param UNS_FFB_LoadUnload_Cost_ID
	 * @param trxName
	 */
	public MUNSFFBLoadUnloadCost(Properties ctx,
			int UNS_FFB_LoadUnload_Cost_ID, String trxName) {
		super(ctx, UNS_FFB_LoadUnload_Cost_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSFFBLoadUnloadCost(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	public boolean beforeSave(boolean newRecord)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_UNS_ArmadaType_ID))
		{
			setVehicleType(new X_UNS_ArmadaType(getCtx(), getUNS_ArmadaType_ID(), get_TrxName()).getVehicleType());
		}
		
		return true;
	}
}
