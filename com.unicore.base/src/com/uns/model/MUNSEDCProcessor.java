/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSEDCProcessor extends X_UNS_EDCProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5890620901805340104L;
	private MUNSEDCProcessorAction[] m_actions = null;

	/**
	 * @param ctx
	 * @param UNS_EDCProcessor_ID
	 * @param trxName
	 */
	public MUNSEDCProcessor(Properties ctx, int UNS_EDCProcessor_ID,
			String trxName) 
	{
		super(ctx, UNS_EDCProcessor_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEDCProcessor(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public MUNSEDCProcessorAction[] getActions (boolean requery)
	{
		if (m_actions != null && !requery)
		{
			set_TrxName(m_actions, get_TrxName());
			return m_actions;
		}
		
		List<MUNSEDCProcessorAction> list = Query.get(
				getCtx(), UNSDefaultModelFactory.EXTENSION_ID, 
				MUNSEDCProcessorAction.Table_Name, Table_Name + "_ID=?", 
				get_TrxName()).setParameters(get_ID()).setOrderBy("Name").
				list();
		
		m_actions = new MUNSEDCProcessorAction[list.size()];
		list.toArray(m_actions);
		
		return m_actions;
	}
	
	public MUNSEDCProcessorAction getAction (String actionName)
	{
		if (actionName == null)
			return null;
		MUNSEDCProcessorAction action = null;
		getActions(false);
		for (int i=0; i<m_actions.length; i++)
		{
			if (m_actions[i].getValue().equals(actionName))
			{
				action = m_actions[i];
				break;
			}
		}
		
		return action;
	}
	
	@Override
	public int getConnectionTimeOut ()
	{
		int timeout =  super.getConnectionTimeOut();
		timeout = timeout * 1000;
		return timeout;
	}
	
	@Override
	public int getReadTimeOut ()
	{
		int timeout = super.getReadTimeOut();
		timeout = timeout * 1000;
		return timeout;
	}
}
