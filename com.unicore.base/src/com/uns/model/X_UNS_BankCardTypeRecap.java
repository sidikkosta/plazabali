/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_BankCardTypeRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_BankCardTypeRecap extends PO implements I_UNS_BankCardTypeRecap, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190517L;

    /** Standard Constructor */
    public X_UNS_BankCardTypeRecap (Properties ctx, int UNS_BankCardTypeRecap_ID, String trxName)
    {
      super (ctx, UNS_BankCardTypeRecap_ID, trxName);
      /** if (UNS_BankCardTypeRecap_ID == 0)
        {
			setDifferenceAmt (Env.ZERO);
// 0
			setTotalARAmount (Env.ZERO);
// 0
			setTotalDisbursedAmt (Env.ZERO);
// 0
			setTotalOutstandingAmt (Env.ZERO);
// 0
			setUNS_BankCardTypeRecap_ID (0);
			setUNS_CardType_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_BankCardTypeRecap (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_BankCardTypeRecap[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Difference.
		@param DifferenceAmt 
		Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt)
	{
		set_Value (COLUMNNAME_DifferenceAmt, DifferenceAmt);
	}

	/** Get Difference.
		@return Difference Amount
	  */
	public BigDecimal getDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total AR Amount.
		@param TotalARAmount Total AR Amount	  */
	public void setTotalARAmount (BigDecimal TotalARAmount)
	{
		set_Value (COLUMNNAME_TotalARAmount, TotalARAmount);
	}

	/** Get Total AR Amount.
		@return Total AR Amount	  */
	public BigDecimal getTotalARAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalARAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Disbursement Amount.
		@param TotalDisbursedAmt Total Disbursement Amount	  */
	public void setTotalDisbursedAmt (BigDecimal TotalDisbursedAmt)
	{
		set_Value (COLUMNNAME_TotalDisbursedAmt, TotalDisbursedAmt);
	}

	/** Get Total Disbursement Amount.
		@return Total Disbursement Amount	  */
	public BigDecimal getTotalDisbursedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalDisbursedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Outstanding Amount.
		@param TotalOutstandingAmt Total Outstanding Amount	  */
	public void setTotalOutstandingAmt (BigDecimal TotalOutstandingAmt)
	{
		set_Value (COLUMNNAME_TotalOutstandingAmt, TotalOutstandingAmt);
	}

	/** Get Total Outstanding Amount.
		@return Total Outstanding Amount	  */
	public BigDecimal getTotalOutstandingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalOutstandingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Bank-Card Type Recap.
		@param UNS_BankCardTypeRecap_ID Bank-Card Type Recap	  */
	public void setUNS_BankCardTypeRecap_ID (int UNS_BankCardTypeRecap_ID)
	{
		if (UNS_BankCardTypeRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BankCardTypeRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BankCardTypeRecap_ID, Integer.valueOf(UNS_BankCardTypeRecap_ID));
	}

	/** Get Bank-Card Type Recap.
		@return Bank-Card Type Recap	  */
	public int getUNS_BankCardTypeRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BankCardTypeRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_BankCardTypeRecap_UU.
		@param UNS_BankCardTypeRecap_UU UNS_BankCardTypeRecap_UU	  */
	public void setUNS_BankCardTypeRecap_UU (String UNS_BankCardTypeRecap_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_BankCardTypeRecap_UU, UNS_BankCardTypeRecap_UU);
	}

	/** Get UNS_BankCardTypeRecap_UU.
		@return UNS_BankCardTypeRecap_UU	  */
	public String getUNS_BankCardTypeRecap_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_BankCardTypeRecap_UU);
	}

	public I_UNS_BankEDCBalance getUNS_BankEDCBalance() throws RuntimeException
    {
		return (I_UNS_BankEDCBalance)MTable.get(getCtx(), I_UNS_BankEDCBalance.Table_Name)
			.getPO(getUNS_BankEDCBalance_ID(), get_TrxName());	}

	/** Set Bank EDC Balance.
		@param UNS_BankEDCBalance_ID Bank EDC Balance	  */
	public void setUNS_BankEDCBalance_ID (int UNS_BankEDCBalance_ID)
	{
		if (UNS_BankEDCBalance_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BankEDCBalance_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BankEDCBalance_ID, Integer.valueOf(UNS_BankEDCBalance_ID));
	}

	/** Get Bank EDC Balance.
		@return Bank EDC Balance	  */
	public int getUNS_BankEDCBalance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BankEDCBalance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Card Type.
		@param UNS_CardType_ID Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID)
	{
		if (UNS_CardType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, Integer.valueOf(UNS_CardType_ID));
	}

	/** Get Card Type.
		@return Card Type	  */
	public int getUNS_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}