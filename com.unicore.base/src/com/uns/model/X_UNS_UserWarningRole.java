/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_UserWarningRole
 *  @author iDempiere (generated) 
 *  @version Release 3.0 - $Id$ */
public class X_UNS_UserWarningRole extends PO implements I_UNS_UserWarningRole, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170725L;

    /** Standard Constructor */
    public X_UNS_UserWarningRole (Properties ctx, int UNS_UserWarningRole_ID, String trxName)
    {
      super (ctx, UNS_UserWarningRole_ID, trxName);
      /** if (UNS_UserWarningRole_ID == 0)
        {
			setAD_Role_ID (0);
			setUNS_UserWarning_ID (0);
			setUNS_UserWarningRole_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UserWarningRole (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UserWarningRole[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Role getAD_Role() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Role)MTable.get(getCtx(), org.compiere.model.I_AD_Role.Table_Name)
			.getPO(getAD_Role_ID(), get_TrxName());	}

	/** Set Role.
		@param AD_Role_ID 
		Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID)
	{
		if (AD_Role_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, Integer.valueOf(AD_Role_ID));
	}

	/** Get Role.
		@return Responsibility Role
	  */
	public int getAD_Role_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Role_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public com.uns.model.I_UNS_UserWarning getUNS_UserWarning() throws RuntimeException
    {
		return (com.uns.model.I_UNS_UserWarning)MTable.get(getCtx(), com.uns.model.I_UNS_UserWarning.Table_Name)
			.getPO(getUNS_UserWarning_ID(), get_TrxName());	}

	/** Set UNS_UserWarning_ID.
		@param UNS_UserWarning_ID UNS_UserWarning_ID	  */
	public void setUNS_UserWarning_ID (int UNS_UserWarning_ID)
	{
		if (UNS_UserWarning_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarning_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarning_ID, Integer.valueOf(UNS_UserWarning_ID));
	}

	/** Get UNS_UserWarning_ID.
		@return UNS_UserWarning_ID	  */
	public int getUNS_UserWarning_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UserWarning_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set User Warning Role.
		@param UNS_UserWarningRole_ID User Warning Role	  */
	public void setUNS_UserWarningRole_ID (int UNS_UserWarningRole_ID)
	{
		if (UNS_UserWarningRole_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarningRole_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarningRole_ID, Integer.valueOf(UNS_UserWarningRole_ID));
	}

	/** Get User Warning Role.
		@return User Warning Role	  */
	public int getUNS_UserWarningRole_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UserWarningRole_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UserWarningRole_UU.
		@param UNS_UserWarningRole_UU UNS_UserWarningRole_UU	  */
	public void setUNS_UserWarningRole_UU (String UNS_UserWarningRole_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UserWarningRole_UU, UNS_UserWarningRole_UU);
	}

	/** Get UNS_UserWarningRole_UU.
		@return UNS_UserWarningRole_UU	  */
	public String getUNS_UserWarningRole_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UserWarningRole_UU);
	}
}