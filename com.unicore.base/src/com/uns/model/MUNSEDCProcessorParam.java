/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.DB;

/**
 * @author nurse
 *
 */
public class MUNSEDCProcessorParam extends X_UNS_EDCProcessorParam 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5142749923350700969L;

	/**
	 * @param ctx
	 * @param UNS_EDCProcessorParam_ID
	 * @param trxName
	 */
	public MUNSEDCProcessorParam(Properties ctx, int UNS_EDCProcessorParam_ID,
			String trxName) 
	{
		super(ctx, UNS_EDCProcessorParam_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEDCProcessorParam(Properties ctx, ResultSet rs, 
			String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	protected boolean beforeSave (boolean newRecord)
	{
		if (getLine() == 0)
		{
			String sql = "SELECT COALESCE (MAX (Line), 0) FROM UNS_EDCProcessorParam "
					+ " WHERE UNS_EDCProcessorAction_ID = ?";
			int line = DB.getSQLValue(get_TrxName(), sql, getUNS_EDCProcessorAction_ID());
			line += 10;
			setLine(line);
		}
		return super.beforeSave(newRecord);
	}
}
