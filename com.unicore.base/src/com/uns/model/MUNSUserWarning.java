package com.uns.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;

public class MUNSUserWarning extends X_UNS_UserWarning {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2196972415339294968L;

	public MUNSUserWarning (Properties ctx, int UNS_UserWarning_ID, String trxName ){
		super(ctx, UNS_UserWarning_ID, trxName);
		
	}

	public MUNSUserWarning (Properties ctx, ResultSet rs, String trxName ) {
		super(ctx, rs, trxName);
		
	}
	
	private MUNSUserWarningColumn[]  m_lines;
	private String m_sql = null;
	
	public static MUNSUserWarning[] getValid (Properties ctx, String trxName, 
			int AD_Client_ID)
	{
		String whereClause = "AD_Client_ID = ? AND IsActive = 'Y' AND IsValid = 'Y'";
		List<MUNSUserWarning> list = new Query(ctx, Table_Name, whereClause, trxName).
				setParameters(AD_Client_ID).list();
		MUNSUserWarning[] records =  new MUNSUserWarning[list.size()];
		return list.toArray(records);	
	}
	
	public static MUNSUserWarning[] getValid (Properties ctx, String trxName,
			int AD_Client_ID, int AD_Role_ID)
	{
		String whereClause = "AD_Client_ID = ? AND IsActive = 'Y' AND IsValid = 'Y'"
				+ " AND UNS_UserWarning_ID IN (SELECT UNS_UserWarning_ID FROM "
				+ " UNS_UserWarningRole WHERE AD_Role_ID = ?)";
		List<MUNSUserWarning> list = new Query(ctx, Table_Name, whereClause, trxName).
				setParameters(AD_Client_ID, AD_Role_ID).list();
		MUNSUserWarning[] records =  new MUNSUserWarning[list.size()];
		return list.toArray(records);	
	}
	
	private MUNSUserWarningColumn [] getLines (String whereClause)
	{	
		String whereClauseFinal = "UNS_UserWarning_ID=? ";
		if (whereClause != null)
		whereClauseFinal += whereClause;
		List <MUNSUserWarningColumn> list = new Query(getCtx(), 
				I_UNS_UserWarningColumn.Table_Name, whereClauseFinal, 
				get_TrxName()).setParameters(getUNS_UserWarning_ID()).
				setOrderBy(I_UNS_UserWarningColumn.COLUMNNAME_Line)
											.list();
		return list.toArray(new MUNSUserWarningColumn[list.size()]);
	}
	
	public MUNSUserWarningColumn[] getLines (boolean requery)
	{
		if (m_lines == null || m_lines.length == 0 || requery)
			m_lines = getLines(null);
		set_TrxName(m_lines, get_TrxName());
		return m_lines;
	}
	
	public MUNSUserWarningColumn[] getLines()
	{
		return getLines(false);
	}	//	getLines
	
	public String getSQL (boolean reload)
	{
		if (m_sql != null && !reload)
		{
			return m_sql;
		}
		
		StringBuilder sql = new StringBuilder("SELECT ");
		getLines();
		for(int i = 0; i < m_lines.length ; i++)
		{
			MUNSUserWarningColumn line = m_lines[i];
			sql.append(line.getColumnSQL());
			if (line.getColumnAlias() != null)
				sql.append(" AS ").append(line.getColumnAlias()).append(" ");
			if(i < m_lines.length-1)
				sql.append(" , ");
		}
		sql.append(" FROM ").append(getSQLFrom()).append(" ");
		if(getSQLJoin() != null)
			sql.append(getSQLJoin()).append(" ");
	
		if(getSQLWhere() != null)
			sql.append(" WHERE ").append(getSQLWhere()).append(" ");
		
		m_sql = sql.toString();
		m_sql = Env.parseVariable(m_sql, this, get_TrxName(), true);
		return m_sql;
	}
	
	public String getSQL ()
	{
		return getSQL(false);
	}

	public boolean beforeSave (boolean newRecord)
	{
		if (getAD_Table_ID() > 0 )
		{
			StringBuffer SQL = new StringBuffer
			("SELECT tableName FROM AD_Table WHERE AD_Table_ID=?");
			String tableName = DB.getSQLValueStringEx(get_TrxName(),
					SQL.toString(), getAD_Table_ID());
			setSQLFrom(tableName);
		}
		
		if (is_ValueChanged(COLUMNNAME_SQLFrom) || is_ValueChanged(COLUMNNAME_SQLJoin) 
				|| is_ValueChanged(COLUMNNAME_SQLWhere))
			setIsValid(false);
		
		return true;
	}
	
	public String validate ()
	{
		PreparedStatement st = null;
		try
		{
			st = DB.prepareStatement(getSQL(true), get_TrxName());
			st.executeQuery();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			if (isValid())
			{
				setIsValid(false);
				save(null);
			}
			return ex.getMessage();
		}
		finally
		{
			DB.close(st);
		}
		
		setIsValid(true);
		saveEx();
		return null;
	}
}

