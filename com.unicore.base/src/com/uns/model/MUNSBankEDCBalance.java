/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.Env;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSBankEDCBalance extends X_UNS_BankEDCBalance {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8847953507621660116L;

	/**
	 * @param ctx
	 * @param UNS_BankEDCBalance_ID
	 * @param trxName
	 */
	public MUNSBankEDCBalance(Properties ctx, int UNS_BankEDCBalance_ID,
			String trxName) {
		super(ctx, UNS_BankEDCBalance_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSBankEDCBalance(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSBankEDCBalance getCreate(Properties ctx, int OrgID, int BankID, String trxName)
	{
		MUNSBankEDCBalance result = null;
		
		result = Query.get(ctx, UNSDefaultModelFactory.EXTENSION_ID, Table_Name,
				"AD_Org_ID = ? AND C_Bank_ID = ?", trxName).setParameters(OrgID, BankID).firstOnly();
		
		if(result == null)
		{
			result = new MUNSBankEDCBalance(ctx, 0, trxName);
			result.setAD_Org_ID(OrgID);
			result.setC_Bank_ID(BankID);
			result.setTotalARAmount(Env.ZERO);
			result.setTotalDisbursedAmt(Env.ZERO);
			result.setTotalOutstandingAmt(Env.ZERO);
			result.saveEx();
		}
		
		return result;
	}
}