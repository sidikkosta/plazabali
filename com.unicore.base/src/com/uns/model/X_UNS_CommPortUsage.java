/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_CommPortUsage
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CommPortUsage extends PO implements I_UNS_CommPortUsage, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171102L;

    /** Standard Constructor */
    public X_UNS_CommPortUsage (Properties ctx, int UNS_CommPortUsage_ID, String trxName)
    {
      super (ctx, UNS_CommPortUsage_ID, trxName);
      /** if (UNS_CommPortUsage_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_CommPortUsage (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CommPortUsage[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Window getAD_Window() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Window)MTable.get(getCtx(), org.compiere.model.I_AD_Window.Table_Name)
			.getPO(getAD_Window_ID(), get_TrxName());	}

	/** Set Window.
		@param AD_Window_ID 
		Data entry or display window
	  */
	public void setAD_Window_ID (int AD_Window_ID)
	{
		if (AD_Window_ID < 1) 
			set_Value (COLUMNNAME_AD_Window_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Window_ID, Integer.valueOf(AD_Window_ID));
	}

	/** Get Window.
		@return Data entry or display window
	  */
	public int getAD_Window_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Window_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set UNS_CommPortUsage_ID.
		@param UNS_CommPortUsage_ID UNS_CommPortUsage_ID	  */
	public void setUNS_CommPortUsage_ID (int UNS_CommPortUsage_ID)
	{
		if (UNS_CommPortUsage_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CommPortUsage_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CommPortUsage_ID, Integer.valueOf(UNS_CommPortUsage_ID));
	}

	/** Get UNS_CommPortUsage_ID.
		@return UNS_CommPortUsage_ID	  */
	public int getUNS_CommPortUsage_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CommPortUsage_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CommPortUsage_UU.
		@param UNS_CommPortUsage_UU UNS_CommPortUsage_UU	  */
	public void setUNS_CommPortUsage_UU (String UNS_CommPortUsage_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CommPortUsage_UU, UNS_CommPortUsage_UU);
	}

	/** Get UNS_CommPortUsage_UU.
		@return UNS_CommPortUsage_UU	  */
	public String getUNS_CommPortUsage_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CommPortUsage_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}