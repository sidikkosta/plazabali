/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_ListIP
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ListIP extends PO implements I_UNS_ListIP, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190129L;

    /** Standard Constructor */
    public X_UNS_ListIP (Properties ctx, int UNS_ListIP_ID, String trxName)
    {
      super (ctx, UNS_ListIP_ID, trxName);
      /** if (UNS_ListIP_ID == 0)
        {
			setDBAddress (null);
			setPassword (null);
			setUNS_ListIP_ID (0);
			setUserName (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_ListIP (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ListIP[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set DB Address.
		@param DBAddress 
		JDBC URL of the database server
	  */
	public void setDBAddress (String DBAddress)
	{
		set_Value (COLUMNNAME_DBAddress, DBAddress);
	}

	/** Get DB Address.
		@return JDBC URL of the database server
	  */
	public String getDBAddress () 
	{
		return (String)get_Value(COLUMNNAME_DBAddress);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Password.
		@param Password 
		Password of any length (case sensitive)
	  */
	public void setPassword (String Password)
	{
		set_Value (COLUMNNAME_Password, Password);
	}

	/** Get Password.
		@return Password of any length (case sensitive)
	  */
	public String getPassword () 
	{
		return (String)get_Value(COLUMNNAME_Password);
	}

	/** Set List IP.
		@param UNS_ListIP_ID List IP	  */
	public void setUNS_ListIP_ID (int UNS_ListIP_ID)
	{
		if (UNS_ListIP_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ListIP_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ListIP_ID, Integer.valueOf(UNS_ListIP_ID));
	}

	/** Get List IP.
		@return List IP	  */
	public int getUNS_ListIP_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ListIP_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ListIP_UU.
		@param UNS_ListIP_UU UNS_ListIP_UU	  */
	public void setUNS_ListIP_UU (String UNS_ListIP_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ListIP_UU, UNS_ListIP_UU);
	}

	/** Get UNS_ListIP_UU.
		@return UNS_ListIP_UU	  */
	public String getUNS_ListIP_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ListIP_UU);
	}

	/** Set User Name.
		@param UserName User Name	  */
	public void setUserName (String UserName)
	{
		set_Value (COLUMNNAME_UserName, UserName);
	}

	/** Get User Name.
		@return User Name	  */
	public String getUserName () 
	{
		return (String)get_Value(COLUMNNAME_UserName);
	}
}