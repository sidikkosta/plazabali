/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_CommPort
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CommPort extends PO implements I_UNS_CommPort, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171102L;

    /** Standard Constructor */
    public X_UNS_CommPort (Properties ctx, int UNS_CommPort_ID, String trxName)
    {
      super (ctx, UNS_CommPort_ID, trxName);
      /** if (UNS_CommPort_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_CommPort (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CommPort[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set BautRate.
		@param BautRate BautRate	  */
	public void setBautRate (int BautRate)
	{
		set_Value (COLUMNNAME_BautRate, Integer.valueOf(BautRate));
	}

	/** Get BautRate.
		@return BautRate	  */
	public int getBautRate () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_BautRate);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set DataBits.
		@param DataBits DataBits	  */
	public void setDataBits (int DataBits)
	{
		set_Value (COLUMNNAME_DataBits, Integer.valueOf(DataBits));
	}

	/** Get DataBits.
		@return DataBits	  */
	public int getDataBits () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DataBits);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Mode.
		@param Mode Mode	  */
	public void setMode (int Mode)
	{
		set_Value (COLUMNNAME_Mode, Integer.valueOf(Mode));
	}

	/** Get Mode.
		@return Mode	  */
	public int getMode () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Mode);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set ParityBit.
		@param ParityBit ParityBit	  */
	public void setParityBit (int ParityBit)
	{
		set_Value (COLUMNNAME_ParityBit, Integer.valueOf(ParityBit));
	}

	/** Get ParityBit.
		@return ParityBit	  */
	public int getParityBit () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ParityBit);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** PORT SERIAL = 1 */
	public static final String PORTTYPE_PORTSERIAL = "1";
	/** PORT PARALLEL = 2 */
	public static final String PORTTYPE_PORTPARALLEL = "2";
	/** PORT I2C = 3 */
	public static final String PORTTYPE_PORTI2C = "3";
	/** PORT RS485 = 4 */
	public static final String PORTTYPE_PORTRS485 = "4";
	/** PORT RAW = 5 */
	public static final String PORTTYPE_PORTRAW = "5";
	/** Set PortType.
		@param PortType PortType	  */
	public void setPortType (String PortType)
	{

		set_Value (COLUMNNAME_PortType, PortType);
	}

	/** Get PortType.
		@return PortType	  */
	public String getPortType () 
	{
		return (String)get_Value(COLUMNNAME_PortType);
	}

	/** Set StopBits.
		@param StopBits StopBits	  */
	public void setStopBits (int StopBits)
	{
		set_Value (COLUMNNAME_StopBits, Integer.valueOf(StopBits));
	}

	/** Get StopBits.
		@return StopBits	  */
	public int getStopBits () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_StopBits);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set TestPort.
		@param TestPort TestPort	  */
	public void setTestPort (String TestPort)
	{
		set_Value (COLUMNNAME_TestPort, TestPort);
	}

	/** Get TestPort.
		@return TestPort	  */
	public String getTestPort () 
	{
		return (String)get_Value(COLUMNNAME_TestPort);
	}

	/** Set UNS_CommPort_ID.
		@param UNS_CommPort_ID UNS_CommPort_ID	  */
	public void setUNS_CommPort_ID (int UNS_CommPort_ID)
	{
		if (UNS_CommPort_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CommPort_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CommPort_ID, Integer.valueOf(UNS_CommPort_ID));
	}

	/** Get UNS_CommPort_ID.
		@return UNS_CommPort_ID	  */
	public int getUNS_CommPort_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CommPort_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CommPort_UU.
		@param UNS_CommPort_UU UNS_CommPort_UU	  */
	public void setUNS_CommPort_UU (String UNS_CommPort_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CommPort_UU, UNS_CommPort_UU);
	}

	/** Get UNS_CommPort_UU.
		@return UNS_CommPort_UU	  */
	public String getUNS_CommPort_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CommPort_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}