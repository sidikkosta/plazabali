/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_EDCProcessorAction
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EDCProcessorAction extends PO implements I_UNS_EDCProcessorAction, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180906L;

    /** Standard Constructor */
    public X_UNS_EDCProcessorAction (Properties ctx, int UNS_EDCProcessorAction_ID, String trxName)
    {
      super (ctx, UNS_EDCProcessorAction_ID, trxName);
      /** if (UNS_EDCProcessorAction_ID == 0)
        {
			setName (null);
			setUNS_EDCProcessor_ID (0);
			setUNS_EDCProcessorAction_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EDCProcessorAction (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EDCProcessorAction[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public com.uns.model.I_UNS_EDCProcessor getUNS_EDCProcessor() throws RuntimeException
    {
		return (com.uns.model.I_UNS_EDCProcessor)MTable.get(getCtx(), com.uns.model.I_UNS_EDCProcessor.Table_Name)
			.getPO(getUNS_EDCProcessor_ID(), get_TrxName());	}

	/** Set EDC Processor.
		@param UNS_EDCProcessor_ID EDC Processor	  */
	public void setUNS_EDCProcessor_ID (int UNS_EDCProcessor_ID)
	{
		if (UNS_EDCProcessor_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessor_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessor_ID, Integer.valueOf(UNS_EDCProcessor_ID));
	}

	/** Get EDC Processor.
		@return EDC Processor	  */
	public int getUNS_EDCProcessor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCProcessor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDC Processor Action.
		@param UNS_EDCProcessorAction_ID EDC Processor Action	  */
	public void setUNS_EDCProcessorAction_ID (int UNS_EDCProcessorAction_ID)
	{
		if (UNS_EDCProcessorAction_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorAction_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorAction_ID, Integer.valueOf(UNS_EDCProcessorAction_ID));
	}

	/** Get EDC Processor Action.
		@return EDC Processor Action	  */
	public int getUNS_EDCProcessorAction_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCProcessorAction_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EDCProcessorAction_UU.
		@param UNS_EDCProcessorAction_UU UNS_EDCProcessorAction_UU	  */
	public void setUNS_EDCProcessorAction_UU (String UNS_EDCProcessorAction_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorAction_UU, UNS_EDCProcessorAction_UU);
	}

	/** Get UNS_EDCProcessorAction_UU.
		@return UNS_EDCProcessorAction_UU	  */
	public String getUNS_EDCProcessorAction_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EDCProcessorAction_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}