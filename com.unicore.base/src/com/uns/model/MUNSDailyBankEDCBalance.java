/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDailyBankEDCBalance extends X_UNS_DailyBankEDCBalance {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6628312859003888054L;

	/**
	 * @param ctx
	 * @param UNS_DailyBankEDCBalance_ID
	 * @param trxName
	 */
	public MUNSDailyBankEDCBalance(Properties ctx,
			int UNS_DailyBankEDCBalance_ID, String trxName) {
		super(ctx, UNS_DailyBankEDCBalance_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDailyBankEDCBalance(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSDailyBankEDCBalance(MUNSBankEDCBalance parent)
	{
		this(parent.getCtx(), 0, parent.get_TrxName());
		setAD_Client_ID(parent.getAD_Client_ID());
		setAD_Org_ID(parent.getAD_Org_ID());
		setUNS_BankEDCBalance_ID(parent.get_ID());
	}
	
	public static MUNSDailyBankEDCBalance getCreate(Properties ctx, int orgID, int edcID,
			Timestamp date, String trxName)
	{
		String sql = "SELECT ba.C_Bank_ID FROM C_BankAccount ba WHERE ba.C_BankAccount_ID = "
				+ "(SELECT edc.C_BankAccount_ID FROM UNS_EDC edc WHERE edc.UNS_EDC_ID = ?)";
		int bankID = DB.getSQLValue(trxName, sql, edcID);
		
		MUNSBankEDCBalance parent = MUNSBankEDCBalance.getCreate(ctx, orgID, bankID, trxName);
		
		MUNSDailyBankEDCBalance result = null;
		
		result = Query.get(ctx, UNSDefaultModelFactory.EXTENSION_ID, Table_Name,
					"UNS_BankEDCBalance_ID = ? AND DateTrx = ?",
						trxName).setParameters(parent.get_ID(), date).firstOnly();
		
		if(result == null)
		{
			result = new MUNSDailyBankEDCBalance(parent);
			result.setDateTrx(date);
			result.setTotalARAmount(Env.ZERO);
			result.setTotalDisbursedAmt(Env.ZERO);
			result.setTotalOutstandingAmt(Env.ZERO);
			result.saveEx();
		}
		
		return result;
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		return upHeader(get_TrxName(), getUNS_BankEDCBalance_ID()) >= 0;
	}
	
	public static int upHeader(String trxName, int parentID)
	{
		String sql = "UPDATE UNS_BankEDCBalance p SET"
				+ " TotalARAmount ="
				+ " (SELECT SUM(t.TotalARAmount) FROM UNS_DailyBankEDCBalance t"
				+ " WHERE t.UNS_BankEDCBalance_ID = p.UNS_BankEDCBalance_ID),"
				+ " TotalDisbursedAmt ="
				+ " (SELECT SUM(t.TotalDisbursedAmt) FROM UNS_DailyBankEDCBalance t"
				+ " WHERE t.UNS_BankEDCBalance_ID = p.UNS_BankEDCBalance_ID),"
				+ " TotalOutstandingAmt ="
				+ " (SELECT SUM(t.TotalOutstandingAmt) FROM UNS_DailyBankEDCBalance t"
				+ " WHERE t.UNS_BankEDCBalance_ID = p.UNS_BankEDCBalance_ID),"
				+ " DifferenceAmt ="
				+ " (SELECT SUM(t.DifferenceAmt) FROM UNS_DailyBankEDCBalance t"
				+ " WHERE t.UNS_BankEDCBalance_ID = p.UNS_BankEDCBalance_ID)"
				+ " WHERE p.UNS_BankEDCBalance_ID = ?";
		
		return (int) DB.executeUpdate(sql, parentID, trxName);
	}
}