/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_FFB_LoadUnload_Cost
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_FFB_LoadUnload_Cost extends PO implements I_UNS_FFB_LoadUnload_Cost, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20151117L;

    /** Standard Constructor */
    public X_UNS_FFB_LoadUnload_Cost (Properties ctx, int UNS_FFB_LoadUnload_Cost_ID, String trxName)
    {
      super (ctx, UNS_FFB_LoadUnload_Cost_ID, trxName);
      /** if (UNS_FFB_LoadUnload_Cost_ID == 0)
        {
			setCost (Env.ZERO);
// 0
			setIsVolumeBased (false);
// N
			setUNS_ArmadaType_ID (0);
			setUNS_FFB_LoadUnload_Cost_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_FFB_LoadUnload_Cost (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_FFB_LoadUnload_Cost[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Cost.
		@param Cost 
		Cost information
	  */
	public void setCost (BigDecimal Cost)
	{
		set_Value (COLUMNNAME_Cost, Cost);
	}

	/** Get Cost.
		@return Cost information
	  */
	public BigDecimal getCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Cost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Is Volume Based.
		@param IsVolumeBased 
		To indicates if the record will be calculated based on volume of the product
	  */
	public void setIsVolumeBased (boolean IsVolumeBased)
	{
		set_Value (COLUMNNAME_IsVolumeBased, Boolean.valueOf(IsVolumeBased));
	}

	/** Get Is Volume Based.
		@return To indicates if the record will be calculated based on volume of the product
	  */
	public boolean isVolumeBased () 
	{
		Object oo = get_Value(COLUMNNAME_IsVolumeBased);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Armada Type.
		@param UNS_ArmadaType_ID Armada Type	  */
	public void setUNS_ArmadaType_ID (int UNS_ArmadaType_ID)
	{
		if (UNS_ArmadaType_ID < 1) 
			set_Value (COLUMNNAME_UNS_ArmadaType_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_ArmadaType_ID, Integer.valueOf(UNS_ArmadaType_ID));
	}

	/** Get Armada Type.
		@return Armada Type	  */
	public int getUNS_ArmadaType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ArmadaType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FFB Load Unload Cost.
		@param UNS_FFB_LoadUnload_Cost_ID FFB Load Unload Cost	  */
	public void setUNS_FFB_LoadUnload_Cost_ID (int UNS_FFB_LoadUnload_Cost_ID)
	{
		if (UNS_FFB_LoadUnload_Cost_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_FFB_LoadUnload_Cost_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_FFB_LoadUnload_Cost_ID, Integer.valueOf(UNS_FFB_LoadUnload_Cost_ID));
	}

	/** Get FFB Load Unload Cost.
		@return FFB Load Unload Cost	  */
	public int getUNS_FFB_LoadUnload_Cost_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_FFB_LoadUnload_Cost_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_FFB_LoadUnload_Cost_UU.
		@param UNS_FFB_LoadUnload_Cost_UU UNS_FFB_LoadUnload_Cost_UU	  */
	public void setUNS_FFB_LoadUnload_Cost_UU (String UNS_FFB_LoadUnload_Cost_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_FFB_LoadUnload_Cost_UU, UNS_FFB_LoadUnload_Cost_UU);
	}

	/** Get UNS_FFB_LoadUnload_Cost_UU.
		@return UNS_FFB_LoadUnload_Cost_UU	  */
	public String getUNS_FFB_LoadUnload_Cost_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_FFB_LoadUnload_Cost_UU);
	}

	/** Dump Truck = DUMP */
	public static final String VEHICLETYPE_DumpTruck = "DUMP";
	/** Vesel Truck = BAKT */
	public static final String VEHICLETYPE_VeselTruck = "BAKT";
	/** Tank Truck = TANK */
	public static final String VEHICLETYPE_TankTruck = "TANK";
	/** Unknown = UNKN */
	public static final String VEHICLETYPE_Unknown = "UNKN";
	/** Set Vehicle Type.
		@param VehicleType 
		The type of vehicle
	  */
	public void setVehicleType (String VehicleType)
	{

		set_Value (COLUMNNAME_VehicleType, VehicleType);
	}

	/** Get Vehicle Type.
		@return The type of vehicle
	  */
	public String getVehicleType () 
	{
		return (String)get_Value(COLUMNNAME_VehicleType);
	}
}