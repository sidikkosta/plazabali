/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_BankEDCBatchRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_BankEDCBatchRecap extends PO implements I_UNS_BankEDCBatchRecap, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190517L;

    /** Standard Constructor */
    public X_UNS_BankEDCBatchRecap (Properties ctx, int UNS_BankEDCBatchRecap_ID, String trxName)
    {
      super (ctx, UNS_BankEDCBatchRecap_ID, trxName);
      /** if (UNS_BankEDCBatchRecap_ID == 0)
        {
			setDateTrx (new Timestamp( System.currentTimeMillis() ));
			setDifferenceAmt (Env.ZERO);
// 0
			setTotalARAmount (Env.ZERO);
// 0
			setTotalDisbursedAmt (Env.ZERO);
// 0
			setTotalOutstandingAmt (Env.ZERO);
// 0
			setUNS_BankEDCBatchRecap_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_BankEDCBatchRecap (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_BankEDCBatchRecap[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Batch No.
		@param BatchNo Batch No	  */
	public void setBatchNo (String BatchNo)
	{
		set_Value (COLUMNNAME_BatchNo, BatchNo);
	}

	/** Get Batch No.
		@return Batch No	  */
	public String getBatchNo () 
	{
		return (String)get_Value(COLUMNNAME_BatchNo);
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_ValueNoCheck (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Difference.
		@param DifferenceAmt 
		Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt)
	{
		set_Value (COLUMNNAME_DifferenceAmt, DifferenceAmt);
	}

	/** Get Difference.
		@return Difference Amount
	  */
	public BigDecimal getDifferenceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TID.
		@param TID TID	  */
	public void setTID (String TID)
	{
		set_Value (COLUMNNAME_TID, TID);
	}

	/** Get TID.
		@return TID	  */
	public String getTID () 
	{
		return (String)get_Value(COLUMNNAME_TID);
	}

	/** Set Total AR Amount.
		@param TotalARAmount Total AR Amount	  */
	public void setTotalARAmount (BigDecimal TotalARAmount)
	{
		set_Value (COLUMNNAME_TotalARAmount, TotalARAmount);
	}

	/** Get Total AR Amount.
		@return Total AR Amount	  */
	public BigDecimal getTotalARAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalARAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Disbursement Amount.
		@param TotalDisbursedAmt Total Disbursement Amount	  */
	public void setTotalDisbursedAmt (BigDecimal TotalDisbursedAmt)
	{
		set_Value (COLUMNNAME_TotalDisbursedAmt, TotalDisbursedAmt);
	}

	/** Get Total Disbursement Amount.
		@return Total Disbursement Amount	  */
	public BigDecimal getTotalDisbursedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalDisbursedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Outstanding Amount.
		@param TotalOutstandingAmt Total Outstanding Amount	  */
	public void setTotalOutstandingAmt (BigDecimal TotalOutstandingAmt)
	{
		set_Value (COLUMNNAME_TotalOutstandingAmt, TotalOutstandingAmt);
	}

	/** Get Total Outstanding Amount.
		@return Total Outstanding Amount	  */
	public BigDecimal getTotalOutstandingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalOutstandingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Bank-EDC-Batch Recap.
		@param UNS_BankEDCBatchRecap_ID Bank-EDC-Batch Recap	  */
	public void setUNS_BankEDCBatchRecap_ID (int UNS_BankEDCBatchRecap_ID)
	{
		if (UNS_BankEDCBatchRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BankEDCBatchRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BankEDCBatchRecap_ID, Integer.valueOf(UNS_BankEDCBatchRecap_ID));
	}

	/** Get Bank-EDC-Batch Recap.
		@return Bank-EDC-Batch Recap	  */
	public int getUNS_BankEDCBatchRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BankEDCBatchRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_BankEDCBatchRecap_UU.
		@param UNS_BankEDCBatchRecap_UU UNS_BankEDCBatchRecap_UU	  */
	public void setUNS_BankEDCBatchRecap_UU (String UNS_BankEDCBatchRecap_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_BankEDCBatchRecap_UU, UNS_BankEDCBatchRecap_UU);
	}

	/** Get UNS_BankEDCBatchRecap_UU.
		@return UNS_BankEDCBatchRecap_UU	  */
	public String getUNS_BankEDCBatchRecap_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_BankEDCBatchRecap_UU);
	}

	public I_UNS_DailyBankEDCBalance getUNS_DailyBankEDCBalance() throws RuntimeException
    {
		return (I_UNS_DailyBankEDCBalance)MTable.get(getCtx(), I_UNS_DailyBankEDCBalance.Table_Name)
			.getPO(getUNS_DailyBankEDCBalance_ID(), get_TrxName());	}

	/** Set Daily Bank EDC Balance.
		@param UNS_DailyBankEDCBalance_ID Daily Bank EDC Balance	  */
	public void setUNS_DailyBankEDCBalance_ID (int UNS_DailyBankEDCBalance_ID)
	{
		if (UNS_DailyBankEDCBalance_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DailyBankEDCBalance_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DailyBankEDCBalance_ID, Integer.valueOf(UNS_DailyBankEDCBalance_ID));
	}

	/** Get Daily Bank EDC Balance.
		@return Daily Bank EDC Balance	  */
	public int getUNS_DailyBankEDCBalance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DailyBankEDCBalance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}