/**
 * 
 */
package com.uns.model.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MTable;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MProduct;

/**
 * @author Burhani Adam
 *
 */
public class ChangeTransactionProduct extends SvrProcess {

	/**
	 * 
	 */
	public ChangeTransactionProduct() {
		// TODO Auto-generated constructor stub
	}
	
	private IProcessUI m_ui = null;
	private Properties m_ctx = null;
	private String m_trxName = null;
	private List<String> m_tableRetric =  new ArrayList<>();
	private int p_ProductFromID = 0;
	private int p_ProductToID = 0;
	private boolean success = false;
	static final int TableProductID = MProduct.Table_ID;
	public Properties getCtx () {
		if (m_ctx == null)
			m_ctx = super.getCtx();
		return m_ctx;
	}
	
	@Override
	public String get_TrxName () {
		if (m_trxName == null)
			m_trxName = super.get_TrxName();
		return m_trxName;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("M_Product_ID"))
				p_ProductFromID = param.getParameterAsInt();
			else if(param.getParameterName().equals("M_ProductTo_ID"))
				p_ProductToID = param.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
		
		if(p_ProductFromID == p_ProductToID)
			throw new AdempiereException("Same record.!");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_ui = Env.getProcessUI(getCtx());
		m_tableRetric.add("M_Product_PO");
		m_tableRetric.add("M_StorageOnHand");
		m_tableRetric.add("M_Product_Trl");
		m_tableRetric.add("M_Product_Acct");
		m_tableRetric.add("C_BPartner_Product");
		
		String[] tableIDs = tableIDs();
		String tableName = null;
		int i = 0;
		
		for(i = 0; i < tableIDs.length; i++)
		{
			int table_ID = new Integer (tableIDs[i]);
			tableName = MTable.getTableName(getCtx(), table_ID);
//			
//			String checkColumn = "SELECT COUNT(*) FROM AD_Table co WHERE co.AD_Table_ID=? AND co.AD_Table_ID IN"
//					+ " (SELECT c.AD_Table_ID FROM AD_Column c WHERE c.ColumnName = 'M_Product_ID' AND c.AD_Table_ID = co.AD_Table_ID)"
//					+ " AND co.AD_Table_ID IN (SELECT o.AD_Table_ID FROM AD_Column o WHERE o.ColumnName = 'Posted' AND o.AD_Table_ID = co.AD_Table_ID)";
//			boolean needPost = DB.getSQLValue(get_TrxName(), checkColumn, table_ID) > 0 ? true : false;
//			
//			String StringIDs = null;
//			if(needPost)
//			{
//				String idList = "SELECT Array_To_String(Array_Agg(" + tableName + "_ID),';') FROM " + tableName
//						 		+ " WHERE C_BPartner_ID = ?";
//				StringIDs = DB.getSQLValueString(get_TrxName(), idList, p_ProductFromID);
//			}
//			
			boolean isContinue = false;
			for (int x=0; x<m_tableRetric.size(); x++) {
				if (tableName.equals(m_tableRetric.get(x))) {
					isContinue = true;
					break;
				}
			}
			if(isContinue)
				continue;
			
			if(tableName.equals("M_ProductPrice"))
			{
				String sql1 = "UPDATE M_ProductPrice v SET M_Product_ID = ? WHERE M_Product_ID = ?"
						+ " AND NOT EXISTS (SELECT 1 FROM M_ProductPrice vv WHERE vv.M_Product_ID = ?"
						+ " AND vv.M_PriceList_Version_ID = v.M_PriceList_Version_ID)";
				DB.executeUpdate(sql1, new Object[]{p_ProductToID, p_ProductFromID, p_ProductToID}, false, get_TrxName());
				continue;
			}
//							
			String[] column = null;
			
			column = upProduct(table_ID);
			if(null != column)
			{
				for(int i3 = 0; i3 < column.length; i3++)
				{
					m_ui.statusUpdate("lagi update Product di " + tableName + " sebanyak " + column.length + " data");
					
					String upBPartner = "UPDATE " + tableName + " SET " + column[i3] + " = ? WHERE " + column[i3] + " = ?" ;
					success = DB.executeUpdate(upBPartner, new Object[]{p_ProductToID, p_ProductFromID}, false, get_TrxName())
										< 0 ? false : true;
					if(!success)
						throw new AdempiereException("Failed when trying update Table " + tableName);
				}
				
//				if ("UNS_Memberloan_Installment".equals(tableName) || "UNS_Member_Loan".equals(tableName)) {
//					String sql = "UPDATE " + tableName + " SET NIP = C_BPartner.Value FROM C_BPartner "
//							+ " WHERE C_BPartner.C_BPartner_ID = " + tableName + ".C_BPartner_ID AND "
//							+  tableName + ".C_BPartner_ID = ?";
//					int result = DB.executeUpdate(sql, p_ProductToID, false, get_TrxName());
//					if (result == -1) {
//						throw new AdempiereException("Failed when try to update NIP");
//					}
//				}
//				else if ("uns_savingswithdrawal".equalsIgnoreCase(tableName) || "".equalsIgnoreCase("uns_membersaving")) {
//					String sql = "UPDATE " + tableName + " SET NIP = C_BPartner.Value, Name = C_BPartner.Name FROM "
//							+ " C_BPartner WHERE C_BPartner.C_BPartner_ID = " + tableName + ".C_BPartner_ID AND " 
//							+ tableName + ".C_BPartner_ID = ?";
//					int result = DB.executeUpdate(sql,p_ProductToID, false, get_TrxName());
//					if (result == -1) {
//						throw new AdempiereException("Failed when try to update NIP");
//					}
//				}
			}
			
//			if(needPost)
//			{
//				if(null != StringIDs)
//				{
//					String[] StringID = StringIDs.split(";");
//					for(int i4 = 0; i4 < StringID.length;)
//					{
//						m_ui.statusUpdate("lagi Repost Dokumen ke " + i4 + " untuk " + tableName + " sebanyak " + StringID.length + " data");
//						int intID = new Integer (StringID[i4]);
//						DocManager.postDocument(
//								MAcctSchema.getClientAcctSchema(getCtx(), Env.getAD_Client_ID(getCtx()), 
//										get_TrxName()), table_ID, intID, 
//								true, true, get_TrxName());
//						i4++;
//					}
//				}
//			}
		}
		
		String upStock = "UPDATE M_StorageOnHand soh SET QtyOnHand = COALESCE((SELECT SUM(trx.MovementQty)"
				+ " FROM M_Transaction trx WHERE trx.M_Product_ID = soh.M_Product_ID AND"
				+ " trx.M_AttributeSetInstance_ID = soh.M_AttributeSetInstance_ID AND"
				+ " trx.M_Locator_ID = soh.M_Locator_ID),0) WHERE soh.M_Product_ID = ?";
		DB.executeUpdate(upStock, p_ProductFromID, m_trxName, 0);
		
		upStock = "UPDATE M_StorageOnHand soh SET QtyOnHand = COALESCE((SELECT SUM(trx.MovementQty)"
				+ " FROM M_Transaction trx WHERE trx.M_Product_ID = soh.M_Product_ID AND"
				+ " trx.M_AttributeSetInstance_ID = soh.M_AttributeSetInstance_ID AND"
				+ " trx.M_Locator_ID = soh.M_Locator_ID),0) WHERE soh.M_Product_ID = ?";
		DB.executeUpdate(upStock, p_ProductToID, m_trxName, 0);
		
		MProduct p = new MProduct(m_ctx, p_ProductFromID, m_trxName);
		p.setIsActive(false);
		p.saveEx();
		
		return "Has Updated " + i + " Table";
	}
	
	public String[] tableIDs()
	{
		String value[];
		
		String sql = "SELECT Array_To_String(Array_Agg(AD_Table_ID),';') FROM AD_Table WHERE AD_Table_ID IN (SELECT AD_Table_ID FROM AD_Column"
				+ " WHERE ColumnSQL IS NULL AND (ColumnName = 'M_Product_ID' OR AD_Reference_ID IN (SELECT AD_Reference_ID FROM AD_Reference"
				+ " WHERE ValidationType = 'T' AND AD_Reference_ID IN (SELECT AD_Reference_ID FROM AD_Ref_Table WHERE AD_Table_ID=?))"
				+ " OR AD_Reference_Value_ID IN (SELECT AD_Reference_ID FROM AD_Reference"
				+ " WHERE ValidationType = 'T' AND AD_Reference_ID IN (SELECT AD_Reference_ID FROM AD_Ref_Table WHERE AD_Table_ID=?))))"
				+ " AND AD_Table_ID <> ? AND isView = 'N'";
		String values = DB.getSQLValueString(get_TrxName(), sql, TableProductID, TableProductID, TableProductID);
		
		value = values.split(";");
		
		return value;		
	}
	
	public String[] upProduct(int AD_Table_ID)
	{
		String value[];
		
		String sql = "SELECT Array_To_String(Array_Agg(ColumnName),';') FROM AD_Column WHERE ColumnSQL IS NULL"
				+ " AND (ColumnName = 'M_Product_ID' OR AD_Reference_ID IN (SELECT AD_Reference_ID FROM AD_Reference"
				+ " WHERE ValidationType = 'T' AND AD_Reference_ID IN (SELECT AD_Reference_ID FROM AD_Ref_Table WHERE AD_Table_ID=?))"
				+ " OR AD_Reference_Value_ID IN (SELECT AD_Reference_ID FROM AD_Reference"
				+ " WHERE ValidationType = 'T' AND AD_Reference_ID IN (SELECT AD_Reference_ID FROM AD_Ref_Table WHERE AD_Table_ID=?)))"
				+ " AND AD_Table_ID = ?";
		String values = DB.getSQLValueString(get_TrxName(), sql, TableProductID, TableProductID, AD_Table_ID);
		
		if(null != values)
		{
			value = values.split(";");
			return value;
		}
		
		return null;
	}
}