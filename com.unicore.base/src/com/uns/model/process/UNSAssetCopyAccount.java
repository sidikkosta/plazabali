/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAsset;
import org.compiere.model.MAssetAcct;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * @author Burhani Adam
 *
 */
public class UNSAssetCopyAccount extends SvrProcess {

	/**
	 * 
	 */
	public UNSAssetCopyAccount() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MAssetAcct acct = MAssetAcct.get(getCtx(), getRecord_ID());
		MAsset asset = MAsset.get(getCtx(), acct.getA_Asset_ID(), get_TrxName());
		String sql = "UPDATE A_Depreciation_Exp SET DR_Account_ID = ?,"
				+ " CR_Account_ID = ? WHERE A_Asset_ID = ?";
		int crAcct = asset.isFromLease() ? acct.getUNS_DepreciationLease_Acct() : acct.getA_Accumdepreciation_Acct();
		int exe = DB.executeUpdate(sql, new Object[]{acct.getA_Depreciation_Acct(), crAcct, asset.get_ID()}, false, get_TrxName());
		if(exe < 0)
			throw new AdempiereException(CLogger.retrieveErrorString("Failed.!!"));
		
		return "Success";
	}
}