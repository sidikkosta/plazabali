/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.compiere.model.MClient;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.SecureEngine;
import org.compiere.util.Util;

import com.uns.util.MessageBox;

/**
 * @author nurse
 *
 */
public class PasswordGenerator extends SvrProcess {

	/**
	 * 
	 */
	public PasswordGenerator() 
	{
		super ();
	}
	
	private String p_password;
	private String p_fileName;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("Password".equals(params[i].getParameterName()))
				p_password = params[i].getParameterAsString();
			else if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(p_fileName, true) && Util.isEmpty(p_password, true))
			throw new AdempiereUserError("Please define file name or password first");
		if (p_fileName != null)
		{
			try 
			{
				byte[] bytes = Files.readAllBytes(Paths.get(p_fileName));
				p_password = new String(bytes, Charset.forName("UTF-8"));
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		if (p_password.contains("#"))
			throw new AdempiereUserError("Disallowed character #");
		
		MClient client = MClient.get(Env.getCtx());
		String oldPwd = client.getPayrollPassword();
		if (!Util.isEmpty(oldPwd, true))
		{
			int result = MessageBox.showMsg(
					getCtx(), getProcessInfo(), 
					"Payroll password already defined before. are you sure to override the password?", 
					"Password already generated before!", MessageBox.YESNO, 
					MessageBox.ICONWARNING);
			if (result != MessageBox.RETURN_YES)
				return "Process Aborted";
		}
		
		String hashPwd = SecureEngine.encrypt(p_password, getAD_Client_ID());
		client.setPayrollPassword(hashPwd);
		client.saveEx();
		
		if (p_fileName != null && !Ini.isClient())
		{
			File file = new File(p_fileName);
			if (file.exists())
				file.delete();
		}
		
		return "success";
	}

}
