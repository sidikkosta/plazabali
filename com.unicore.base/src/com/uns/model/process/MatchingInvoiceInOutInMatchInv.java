/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MMatchInv;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

/**
 * @author Burhani Adam
 *
 */
public class MatchingInvoiceInOutInMatchInv extends SvrProcess {

	private static final int Doc_PurchasingContract = 1000464;
	private static final int Doc_APInvoice = 1000358;
	private String m_processMsg = null;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
//		if(null != createNewMatchInv())
//			throw new AdempiereException(m_processMsg);
//		if(null != upMatchInv())
//			throw new AdempiereException(m_processMsg);
		if(null != repostDocument())
			throw new AdempiereException(m_processMsg);
		
		return null;
	}
	
	/**
	 * @param C_DocType_ID (Order)
	 * @return
	 */
	public MMatchInv[] getMatchInvoice(int C_DocType_ID) throws SQLDataException
	{
		ArrayList<MMatchInv> list = new ArrayList<MMatchInv>();
		
		String sql = "SELECT M_MatchInv_ID FROM M_MatchInv WHERE C_InvoiceLine_ID IN"
				+ " (SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_Invoice_ID IN"
				+ " (SELECT C_Invoice_ID FROM C_Invoice WHERE C_DocType_ID= ? AND C_Order_ID IN"
				+ " (SELECT C_Order_ID FROM C_Order WHERE C_DocType_ID=?)))";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, Doc_APInvoice);
			stmt.setInt(2, C_DocType_ID);
			rs = stmt.executeQuery();
			while (rs.next())
				list.add(new MMatchInv(getCtx(), rs.getInt(1), get_TrxName()));
			rs.close();
			stmt.close();
			stmt = null;
		}
		catch (SQLException ex)
		{
			m_processMsg = ex.getMessage();
			throw new AdempiereException(m_processMsg);
			
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		MMatchInv[] retValue = new MMatchInv[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	public MInvoiceLine[] invoiceNotExistsMatchInv(int C_DocType_ID) throws SQLDataException
	{
		ArrayList<MInvoiceLine> list = new ArrayList<MInvoiceLine>();
		
		String sql = "SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_InvoiceLine_ID NOT IN (SELECT C_InvoiceLine_ID FROM M_MatchInv)"
				+ " AND C_Invoice_ID IN (SELECT C_Invoice_ID FROM C_Invoice WHERE Processed = 'Y' AND C_DocType_ID = ? AND "
				+ " C_Order_ID IN (SELECT C_Order_ID FROM C_Order WHERE C_DocType_ID=?))";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, Doc_APInvoice);
			stmt.setInt(2, C_DocType_ID);
			rs = stmt.executeQuery();
			while (rs.next())
				list.add(new MInvoiceLine(getCtx(), rs.getInt(1), get_TrxName()));
			rs.close();
			stmt.close();
			stmt = null;
		}
		catch (SQLException ex)
		{
			m_processMsg = ex.getMessage();
			throw new AdempiereException(m_processMsg);
			
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		MInvoiceLine[] retValue = new MInvoiceLine[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	public String createNewMatchInv() throws SQLDataException
	{
		for(MInvoiceLine il : invoiceNotExistsMatchInv(Doc_PurchasingContract))
		{
			String sqlGetInOut = "SELECT M_InOut_ID FROM UNS_WeighbridgeTicket WHERE C_InvoiceLine_ID=?";
			int idInOut = DB.getSQLValue(get_TrxName(), sqlGetInOut, il.get_ID());
			if(idInOut < 0)
				continue;
			String sqlGetInOutLine = "SELECT M_InOutLine_ID FROM M_InOutLine WHERE M_InOut_ID =?";
			int idInOutLine = DB.getSQLValue(get_TrxName(), sqlGetInOutLine, idInOut);
			String existMatchInv = "SELECT COUNT(*) FROM M_MatchInv WHERE M_InOutLine_ID = ?";
			int exists = DB.getSQLValue(get_TrxName(), existMatchInv, idInOutLine);
			if(exists > 0)
			{
				String upMatchInv = "UPDATE M_MatchInv SET C_InvoiceLine_ID=?, Qty=? WHERE M_InOutLine_ID=?";
				DB.executeUpdate(upMatchInv, new Object[]{il.get_ID(), il.getQtyInvoiced(), idInOutLine}, false, get_TrxName());
				String upInOutLine = "UPDATE C_InvoiceLine SET M_InOutLine_ID = ? WHERE C_InvoiceLine_ID=?";
				int count = DB.executeUpdate(upInOutLine, new Object[]{idInOutLine, il.get_ID()}, false, get_TrxName());
				if(count < 0)
					m_processMsg = "Failed when trying update invoice line in inout line";
			}
			else
			{
				MMatchInv mi = new MMatchInv(il, il.getC_Invoice().getDateInvoiced(), il.getQtyInvoiced());
				mi.setM_InOutLine_ID(idInOutLine);
				if(!mi.save())
					m_processMsg = "Failed when trying create new Match Invoices";
			}
			if(m_processMsg != null)
				return m_processMsg;
			
			DocManager.postDocument(
					MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID(), 
							get_TrxName()), MInOut.Table_ID, idInOut, 
					true, true, get_TrxName());
			
			DocManager.postDocument(
					MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID(), 
							get_TrxName()), MInvoice.Table_ID, il.getC_Invoice_ID(), 
					true, true, get_TrxName());
		}
		return m_processMsg;
	}
	
	public String upMatchInv() throws SQLDataException
	{
		for(MMatchInv mi : getMatchInvoice(Doc_PurchasingContract))
		{
			if(mi.getQty().compareTo(mi.getC_InvoiceLine().getQtyEntered()) == 0)
				continue;
			String sqlGetInOut = "SELECT M_InOut_ID FROM UNS_WeighbridgeTicket WHERE C_InvoiceLine_ID=?";
			int idInOut = DB.getSQLValue(get_TrxName(), sqlGetInOut, mi.getC_InvoiceLine_ID());
			if(idInOut < 0)
				continue;
			String sqlUpInvoiceLine = "UPDATE C_InvoiceLine SET M_InOutLine_ID = ? WHERE C_InvoiceLine_ID = ?";
			String sqlGetInOutLine = "SELECT M_InOutLine_ID FROM M_InOutLine WHERE M_InOut_ID =?";
			int idInOutLine = DB.getSQLValue(get_TrxName(), sqlGetInOutLine, idInOut);
			int count = DB.executeUpdate(sqlUpInvoiceLine, new Object[]{idInOutLine, mi.getC_InvoiceLine_ID()},
					false, get_TrxName());
			if(count < 0)
				m_processMsg = "Failed when trying update inout line to invoice line";
			
			String sqlUpMatchInv = "UPDATE M_MatchInv SET Qty = (SELECT QtyInvoiced FROM C_InvoiceLine WHERE C_invoiceLine_ID=?)"
					+ " WHERE M_MatchInv_ID = ?";
			count = DB.executeUpdate(sqlUpMatchInv, new Object[]{mi.getC_InvoiceLine_ID(), mi.get_ID()}, false, get_TrxName());
			if(count < 0)
				m_processMsg = "Failed when trying update quantity to match invoices";
			
			DocManager.postDocument(
					MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID(), 
							get_TrxName()), MInOut.Table_ID, idInOut, 
					true, true, get_TrxName());
			
			DocManager.postDocument(
					MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID(), 
							get_TrxName()), MInvoice.Table_ID, mi.getC_InvoiceLine().getC_Invoice_ID(), 
					true, true, get_TrxName());
		}
		return m_processMsg;
	}
	
	public String repostDocument()
	{
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(C_Invoice_ID), ';') FROM UNS_WeighbridgeTicket";
		String value = DB.getSQLValueString(get_TrxName(), sql);
		String[] values = value.split(";");
		
		for(int i = 0; i < values.length;)
		{
			int idASint = new Integer (values[i]);
			
			m_processMsg = DocManager.postDocument(MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID(),
					get_TrxName()), MInvoice.Table_ID, idASint, true, true, get_TrxName());
			
			i++;
		}
		
		sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(M_InOut_ID), ';') FROM UNS_WeighbridgeTicket";
		value = DB.getSQLValueString(get_TrxName(), sql);
		values = value.split(";");
		
		for(int i = 0; i < values.length;)
		{
			int idASint = new Integer (values[i]);
			
			m_processMsg = DocManager.postDocument(MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID(),
					get_TrxName()), MInOut.Table_ID, idASint, true, true, get_TrxName());
			
			i++;
		}
		return m_processMsg;
	}
}