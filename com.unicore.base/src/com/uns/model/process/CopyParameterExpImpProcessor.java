package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MEXPProcessor;
import org.compiere.model.MEXPProcessorParameter;
import org.compiere.model.MIMPProcessor;
import org.compiere.model.X_IMP_ProcessorParameter;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

public class CopyParameterExpImpProcessor extends SvrProcess {

	public CopyParameterExpImpProcessor() {
		
	}
	
	private boolean p_isExport = false;
	private int p_ProcessorFrom_ID = 0;
	private int p_ProcessorTo_ID = 0;

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] param = getParameter();
		
		for(ProcessInfoParameter para : param)
		{
			String name = para.getParameterName();
			if(name.equals("Processor_ID"))
				p_ProcessorFrom_ID = para.getParameterAsInt();
			else if(name.equals("IsExport"))
				p_isExport = para.getParameterAsBoolean();
			else
				throw new AdempiereException("Unknown parameter name : "+name);
		}
		
		p_ProcessorTo_ID = getRecord_ID();

	}

	@SuppressWarnings("unused")
	@Override
	protected String doIt() throws Exception {
		
		if(p_ProcessorTo_ID <= 0)
			throw new AdempiereException("Only Can process from window");
		
		if(p_ProcessorFrom_ID <= 0)
			throw new AdempiereException("Mandatory Processor ID");
		
		if(p_isExport)
		{
			MEXPProcessor expFrom = new MEXPProcessor(getCtx(), p_ProcessorFrom_ID, get_TrxName());
			if(expFrom == null)
				throw new AdempiereException("Cannot found Export Processor Copied from");
			
			MEXPProcessor expTo = new MEXPProcessor(getCtx(), p_ProcessorTo_ID, get_TrxName());
			if(expTo == null)
				throw new AdempiereException("Cannot found Export Process Copied to");
			
			//delete all parameter first
			MEXPProcessorParameter[] expParams = expTo.getParameters();
			for(int i=0; i<expParams.length ; i++)
			{	
				expParams[i].deleteEx(true, get_TrxName());
			}
			
			//copying paramater
			MEXPProcessorParameter[] expParamsFrom = expFrom.getParameters();
			for(int i=0; i<expParamsFrom.length; i++)
			{
				MEXPProcessorParameter expParamTo = new MEXPProcessorParameter(expTo);
				expParamTo.setValue(expParamsFrom[i].getValue());
				expParamTo.setName(expParamsFrom[i].getName());
				expParamTo.setDescription(expParamsFrom[i].getDescription());
				expParamTo.setHelp(expParamsFrom[i].getHelp());
				expParamTo.setParameterValue(expParamsFrom[i].getParameterValue());
				
				expParamTo.saveEx();
			}
		}
		else
		{
			MIMPProcessor impFrom = new MIMPProcessor(getCtx(), p_ProcessorFrom_ID, get_TrxName());
			if(impFrom == null)
				throw new AdempiereException("Cannot found Import Processor Copied from");
			
			MIMPProcessor impTo = new MIMPProcessor(getCtx(), p_ProcessorTo_ID, get_TrxName());
			if(impTo == null)
				throw new AdempiereException("Cannot found Import Process Copied to");
			
			//delete all parameter first
			X_IMP_ProcessorParameter[] impParamsTo = impTo.getIMP_ProcessorParameters(get_TrxName());
			for(int i=0; i<impParamsTo.length ; i++)
			{	
				impParamsTo[i].deleteEx(true, get_TrxName());
			}
			
			//copying paramater
			X_IMP_ProcessorParameter[] impParamsFrom = impFrom.getIMP_ProcessorParameters(get_TrxName());
			for(int i=0; i<impParamsFrom.length; i++)
			{
				X_IMP_ProcessorParameter impParamTo = new X_IMP_ProcessorParameter(getCtx(), 0, get_TrxName());
				impParamTo.setAD_Org_ID(impTo.getAD_Org_ID());
				impParamTo.setIMP_Processor_ID(impTo.get_ID());
				impParamTo.setValue(impParamsFrom[i].getValue());
				impParamTo.setName(impParamsFrom[i].getName());
				impParamTo.setDescription(impParamsFrom[i].getDescription());
				impParamTo.setHelp(impParamsFrom[i].getHelp());
				impParamTo.setParameterValue(impParamsFrom[i].getParameterValue());
				
				impParamTo.saveEx();
			}
		}
		
		return "@DONE@";
	}

}
