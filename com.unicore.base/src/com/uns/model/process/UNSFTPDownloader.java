/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;


/**
 * @author Menjangan
 *
 */
public class UNSFTPDownloader extends SvrProcess {

	private String p_host = null;
	private int	p_port = 21;
	private String p_user = null;
	private String p_password = null;
	private String p_dPath = null;
	private String p_outDPath = null;
	private String p_fPrefix = "uns_";
	private String p_fExt = ".xls";

	/**
	 * 
	 */
	public UNSFTPDownloader() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			ProcessInfoParameter para = params[i];
			String name = para.getParameterName();
			if ("Host".equals(name))
				this.p_host = para.getParameterAsString();
			else if ("Port".equals(name))
				this.p_port = para.getParameterAsInt();
			if ("User".equals(name))
				this.p_user = para.getParameterAsString();
			else if ("Password".equals(name))
				this.p_password = para.getParameterAsString();
			else if ("DirPath".equals(name))
				this.p_dPath = para.getParameterAsString();
			else if ("OutDirPath".equals(name))
				this.p_outDPath = para.getParameterAsString();
			else if ("PrefixFile".equals(name))
				this.p_fPrefix = para.getParameterAsString();
			else if ("FileExtension".equals(name))
				this.p_fExt = para.getParameterAsString();
			else
				log.info("Unknown parameter " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{ 
		int succCount = 0;
		int failCount = 0;
		if (!this.p_fExt.startsWith("."))
			this.p_fExt = "." + this.p_fExt;
		if (!this.p_dPath.endsWith("/"))
			this.p_dPath += "/";
		if (!this.p_outDPath.endsWith("/"))
			this.p_outDPath += "/";
		String message = null;
		FTPClient client = new FTPClient();
		try
		{
			if (this.p_port <= 0)
				this.p_port = 21;
			client.connect(this.p_host, this.p_port);
			if (!client.login(this.p_user, this.p_password))
			{
				client.disconnect();
				log.log(Level.SEVERE, "Could not connect to FTP Server.");
				return "Process aborted, Could not connect to FTP Server.";
			}
			
			FTPFile[] ftpFiles = client.listFiles(this.p_dPath);
			failCount = ftpFiles.length;
			
			for (int i=0; i<ftpFiles.length; i++)
			{
				FTPFile ftpFile = ftpFiles[i];
				if (!ftpFile.isFile())
				{
					--failCount;
					continue;
				}
				
				String fileName = ftpFile.getName();
				if (!fileName.endsWith(this.p_fExt))
				{
					--failCount;
					continue;
				}
				else if (this.p_fPrefix != null && !fileName.startsWith(this.p_fPrefix))
				{
					--failCount;
					continue;
				}
				String filePath = this.p_dPath + fileName;
				String outFilePath = p_outDPath + ftpFile.getName();
				File outDir = new File(p_outDPath);
				if (!outDir.exists())
					outDir.mkdir();
				File outFile = new File(outFilePath);
				outFile.createNewFile();
				OutputStream outStream = new FileOutputStream(outFile);
				
				if (!client.retrieveFile(filePath, outStream))
				{
					log.log(Level.SEVERE, "Could not retrieve input stream.");
					outStream.close();
					if (outFile.exists())
						outFile.delete();
					continue;
				}
				
				outStream.flush();
				outStream.close();
				client.deleteFile(filePath);
				++succCount;
				--failCount;
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if (client.isConnected())
				client.disconnect();
		}
		
		message = succCount + " success and " + failCount + " failed";
		log.fine(message);
		return message;
	}
	
	public static void main (String[] args) throws Exception
	{
		UNSFTPDownloader instance = new UNSFTPDownloader();//test
		instance.p_dPath ="TestFTP";
		instance.p_fExt="xls";
		instance.p_fPrefix="uns_";
		instance.p_host="localhost";
		instance.p_outDPath="D:/";
		instance.p_password="R41nC1ty";
		instance.p_user="menjangan";
		instance.doIt();
	}
}
