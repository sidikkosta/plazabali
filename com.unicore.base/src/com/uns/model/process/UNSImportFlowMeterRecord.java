/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSImportSimpleTable;

/**
 * @author Menjangan
 *
 */
public class UNSImportFlowMeterRecord extends SvrProcess 
{
	private String p_dPath = null;
	private String p_fPrefix = "uns_";
	private int p_impTblID = 0;
	private final String STORAGE_FORMATED_XLS = "FORMATED/";
	private final String FILE_EXTENSION = ".xls";

	/**
	 * 
	 */
	public UNSImportFlowMeterRecord() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			ProcessInfoParameter p = params[i];
			String name = p.getParameterName();
			if ("DirPath".equals(name))
				this.p_dPath = p.getParameterAsString();
			else if ("PrefixFile".equals(name))
				this.p_fPrefix = p.getParameterAsString();
			else if ("UNS_ImportSimpleTable_ID".equals(name))
				this.p_impTblID = p.getParameterAsInt();
			else
				log.info("Unknown parameter " + name);
		}
	}
	
	private File getFormatedWorkbookDir (File dir)
	{
		if (!dir.exists())
			return null;
		String formatedDirPath = dir.getAbsolutePath();
		if (!formatedDirPath.endsWith("/"))
			formatedDirPath += "/";
		formatedDirPath += STORAGE_FORMATED_XLS;
		File formatedDir = new File(formatedDirPath);
		if (!formatedDir.exists())
		{
			if (!dir.canWrite())
				dir.setWritable(true);
			if (!formatedDir.mkdir())
				return null;
			formatedDir.setWritable(true);
		}
		File[] files = dir.listFiles();
		if (files == null)
			return null;
		for (int i=0; i<files.length; i++)
		{
			if (files[i].isDirectory())
			{
				log.info("Ignore dir " + files[i].getAbsolutePath());
				continue;
			}
			if (!files[i].getName().endsWith(FILE_EXTENSION))
			{
				log.info("Ignore file " + files[i].getAbsolutePath());
				continue;
			}
			
			try 
			{
				Workbook original = Workbook.getWorkbook(files[i]);
				String copyFileName = formatedDirPath + files[i].getName();
				File copyFile = new File(copyFileName);
				WritableWorkbook copy = Workbook.createWorkbook(copyFile, original);
				WritableSheet[] sheets = copy.getSheets();
				for (int j=0; j<sheets.length; j++)
				{
					WritableSheet sheet = sheets[j];
					int max = sheet.getRows();
					WritableCell flowMeterName = sheet.getWritableCell(0,0);
					Label label = new Label(19, 1, "AD_Org_ID");
					sheet.addCell(label);
					
					for (int x=4; x<max; x++)
					{
						sheet.addCell(flowMeterName.copyTo(18, x));
						sheet.addCell(sheet.getWritableCell(19, x));
					}
				}
				
				copy.write();
				copy.close();
				original.close();
			} 
			catch (BiffException | IOException | WriteException e) 
			{
				e.printStackTrace();
				return null;
			}
			
			files[i].delete();
		}
		
		return formatedDir;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (this.p_dPath == null)
			throw new AdempiereException("Undefined parameter Directory Path!!!");
		if (this.p_impTblID == 0)
			throw new AdempiereException("Undefined parameter Import Simple Table");
		
		File dir = new File(p_dPath);
		if (!dir.exists())
			return "Directory not exists " + p_dPath;
		dir = getFormatedWorkbookDir(dir);
		if (dir == null)
			throw new AdempiereException("Could not format workbook before imported to system!!!");
		File[] files = dir.listFiles();
		if (files == null)
			return "0 file success & 0 file failed";
		int okCount = 0;
		int failCount = files.length;
		for (int i=0; i<files.length; i++)
		{
			if (files[i].isDirectory())
			{
				--failCount;
				continue;
			}
			else if (!doImport(files[i]))
			{
				continue;
			}
				
			++okCount;
			--failCount;
		}
		
		return okCount + "files success & " + failCount + " failed";
	}

	private boolean doImport (File file)
	{
		String fName = file.getName();
		boolean ok = false;
		if (p_fPrefix != null && !fName.startsWith(p_fPrefix))
			return ok;
		else if (!fName.endsWith(FILE_EXTENSION))
			return ok;
		
		SimpleImportXLS xlsImport = new SimpleImportXLS(getCtx(), 5, 0, get_TrxName());
		MUNSImportSimpleTable table = new MUNSImportSimpleTable(getCtx(), p_impTblID, get_TrxName());
		RandomAccessFile accessFile =  null;
		FileChannel chanel = null;
		FileLock lock = null;
		Workbook book = null;
		try {
			accessFile = new RandomAccessFile(file, "rw");
			chanel = accessFile.getChannel();
			lock = chanel.lock();
			if (lock != null)
				lock.release();
			book = Workbook.getWorkbook(file);
			String[] sheetsName = book.getSheetNames();
			for (int i=0; sheetsName != null && i<sheetsName.length; i++)
			{
				table.setName(sheetsName[i]);
				table.saveEx();
				if (!(ok = xlsImport.importSimpleTable(book, table)))
					break;
			}
		} catch (BiffException e) {
			e.printStackTrace();
			ok = false;
		} catch (IOException e) {
			e.printStackTrace();
			ok = false;
		} finally {
			try {
				if (lock != null)
					lock.close();
				if (chanel != null)
					chanel.close();
				if (accessFile != null)
					accessFile.close();
				if (book != null)
					book.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (ok)
			ok = file.delete();
		
		return ok;
	}
	
	public static void main (String[] args) throws Exception
	{

		UNSImportFlowMeterRecord i = new UNSImportFlowMeterRecord();
		i.p_dPath = "D:/TestImport";
		System.out.println(i.doIt());
	}
}
