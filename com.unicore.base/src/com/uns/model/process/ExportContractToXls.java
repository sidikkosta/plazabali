/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.logging.Level;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.CellFormat;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.compiere.Adempiere;
import org.compiere.model.MColumn;
import org.compiere.model.MSysConfig;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.SecureEngine;
import org.compiere.util.Util;

/**
 * @author nurse
 *
 */
public class ExportContractToXls extends SvrProcess 
{

	private int p_orgID = 0;
	private int p_sectionDeptID = 0;
	private Timestamp p_start = null;
	private Timestamp p_end = null;
	private Hashtable<String, Integer> m_mapHeader = new Hashtable<>();
	private int m_maxCol = 0;
	private String p_level = null;
	
	/**
	 * 
	 */
	public ExportContractToXls() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("AD_Org_ID".equals(params[i].getParameterName()))
				p_orgID = params[i].getParameterAsInt();
			else if ("SectionOfDept_ID".equals(params[i].getParameterName()))
				p_sectionDeptID = params[i].getParameterAsInt();
			else if ("StartDate".equals(params[i].getParameterName()))
				p_start = params[i].getParameterAsTimestamp();
			else if ("EndDate".equals(params[i].getParameterName()))
				p_end = params[i].getParameterAsTimestamp();
			else if ("PayrollLevel".equals(params[i].getParameterName()))
				p_level = params[i].getParameterAsString();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (p_start != null && p_end != null && p_end.before(p_start))
			throw new AdempiereUserError("End date must be greater than start date");
		String fileName = Adempiere.getAdempiereHome().trim();
		if (!fileName.endsWith("/") && !fileName.endsWith("\\"))
			fileName += File.separator;
		fileName += "Contract";
		fileName += System.currentTimeMillis();
		fileName += ".xls";
		File file = new File(fileName);
		WritableWorkbook book = Workbook.createWorkbook(file);
		WritableSheet sheet = book.createSheet("Contract", 0);
		createDefaultHeader(sheet);
		loadDataToExcel(sheet);
		
		Label l = new Label(++m_maxCol,0, "Date Approval");
		sheet.addCell(l);
		CellFormat format = sheet.getCell(20, 0).getCellFormat();
		WritableCellFormat wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(m_maxCol, 0, m_maxCol, 1);
		
		l = new Label(++m_maxCol,0, "Effective Date");
		sheet.addCell(l);
		format = sheet.getCell(20, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(m_maxCol, 0, m_maxCol, 1);
		
		book.write();
		book.close();

		processUI.download(file);
		if (!Ini.isClient())
			fileName = "";
		
		return "File generated " + fileName;
	}
	
	public void createDefaultHeader (WritableSheet sheet) throws RowsExceededException, WriteException
	{	
		//no
		Label l = new Label(0,0, "No");
		sheet.addCell(l);
		CellFormat format = sheet.getCell(0, 0).getCellFormat();
		WritableCellFormat wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(0, 0, 0, 1);
		
		//COntract ID
		l = new Label(1,0, "Unique ID");
		sheet.addCell(l);
		format = sheet.getCell(1, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		WritableCellFeatures wcfet = new WritableCellFeatures();
		wcfet.setComment("***DON'T CHANGE IT***");
		l.setCellFeatures(wcfet);
		sheet.mergeCells(1, 0, 1, 1);
		
		//Contract UU
		l = new Label(2,0, "Unique UU");
		sheet.addCell(l);
		format = sheet.getCell(2, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		wcfet = new WritableCellFeatures();
		wcfet.setComment("***DON'T CHANGE IT***");
		l.setCellFeatures(wcfet);
		sheet.mergeCells(2, 0, 2, 1);
		
		//org
		l = new Label(3,0, "Organization");
		
		sheet.addCell(l);
		format = sheet.getCell(3, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(3, 0, 4, 0);
		
		l = new Label(3,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(3, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(4,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(4, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//NIK
		l = new Label(5,0, "NIK");
		sheet.addCell(l);
		format = sheet.getCell(5, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(5, 0, 6, 0);
		
		l = new Label(5,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(5, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(6,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(6, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//name
		l = new Label(7,0, "Name");
		sheet.addCell(l);
		format = sheet.getCell(7, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(7, 0, 7, 1);
		
		//Section of dept
		l = new Label(8,0, "Section Of Dept");
		sheet.addCell(l);
		format = sheet.getCell(8, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(8, 0, 9, 0);
		
		l = new Label(8,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(8, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(9,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(9, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Contract Type
		l = new Label(10,0, "Contract Type");
		sheet.addCell(l);
		format = sheet.getCell(10, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(10, 0, 11, 0);
		
		l = new Label(10,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(10, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(11,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(11, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//PayrollTerm
		l = new Label(12,0, "Payroll Term");
		sheet.addCell(l);
		format = sheet.getCell(12, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(12, 0, 13, 0);
		
		l = new Label(12,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(12, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(13,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(13, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Position
		l = new Label(14,0, "Position");
		sheet.addCell(l);
		format = sheet.getCell(14, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(14, 0, 15, 0);
		
		l = new Label(14,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(14, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(15,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(15, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Payroll Level
		l = new Label(16,0, "Payroll Level");
		sheet.addCell(l);
		format = sheet.getCell(16, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(16, 0, 17, 0);
		
		l = new Label(16,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(16, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(17,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(17, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Shift Type
		l = new Label(18,0, "Shift Type");
		sheet.addCell(l);
		format = sheet.getCell(18, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(18, 0, 19, 0);
		
		l = new Label(18,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(18, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(19,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(19, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Gender
		l = new Label(20,0, "Gender");
		sheet.addCell(l);
		format = sheet.getCell(20, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(20, 0, 20, 1);
		
		//Date Contract start
		l = new Label(21,0, "Date Contract Start");
		sheet.addCell(l);
		format = sheet.getCell(21, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(21, 0, 22, 0);
		
		l = new Label(21,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(21, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(22,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(22, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Date Contract End
		l = new Label(23,0, "Date Contract End");
		sheet.addCell(l);
		format = sheet.getCell(23, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(23, 0, 24, 0);
		
		l = new Label(23,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(23, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(24,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(24, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//PPH21PaidByCompany
		l = new Label(25,0, "PPh21 Paid By Company");
		sheet.addCell(l);
		format = sheet.getCell(25, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(25, 0, 26, 0);
		
		l = new Label(25,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(25, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(26,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(26, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//Gaji Pokok
		l = new Label(27,0, "Basic Salary");
		sheet.addCell(l);
		format = sheet.getCell(27, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(27, 0, 28, 0);
		
		l = new Label(27,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(27, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(28,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(28, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		//OT Basic Amount
		l = new Label(29,0, "OT Basic Amount");
		sheet.addCell(l);
		format = sheet.getCell(29, 0).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		sheet.mergeCells(29, 0, 30, 0);
		
		l = new Label(29,1, "Current");
		sheet.addCell(l);
		format = sheet.getCell(29, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
		
		l = new Label(30,1, "New");
		sheet.addCell(l);
		format = sheet.getCell(30, 1).getCellFormat();
		wcf = new WritableCellFormat(format);
		wcf.setAlignment(Alignment.CENTRE);
		wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
		l.setCellFormat(wcf);
	}
	
	private void loadDataToExcel (WritableSheet sheet) throws RowsExceededException, WriteException
	{
		String _sqlContract = buildContractSQL();
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(_sqlContract, get_TrxName());
			rs = st.executeQuery();
			int row = 2;
			while (rs.next())
			{
				int rsIdx = 0;
				int contractID = rs.getInt(++rsIdx);
				String contractUU = rs.getString(++rsIdx);
				String organization = rs.getString(++rsIdx);
				String nik = rs.getString(++rsIdx);
				String name = rs.getString(++rsIdx);
				String sectOfDept = rs.getString(++rsIdx);
				String contractType = rs.getString(++rsIdx);
				String payrollTerm = rs.getString(++rsIdx);
				String position = rs.getString(++rsIdx);
				String payrollLevel = rs.getString(++rsIdx);
				String shift = rs.getString(++rsIdx);
				String gender = rs.getString(++rsIdx);
				String dateContractStart = rs.getString(++rsIdx);
				String dateContractEnd = rs.getString(++rsIdx);
				String pph21PaidByCompany = rs.getString(++rsIdx);
				BigDecimal basicSalary = rs.getBigDecimal(++rsIdx);
				BigDecimal otBasicAmt = rs.getBigDecimal(++rsIdx);
				
				MColumn column = MColumn.get(getCtx(), "UNS_Contract_Recommendation", "New_G_Pokok");
				if (column.isEncrypted()  && !Util.isEmpty(Env.getContext(Env.getCtx(), MSysConfig.PAYROLL_PASSWORD), true))
				{
					basicSalary = (BigDecimal) SecureEngine.decrypt(basicSalary, getAD_Client_ID());
				}
				column = MColumn.get(getCtx(), "UNS_Contract_Recommendation", "OTBasicAmt");
				if (column.isEncrypted() && !Util.isEmpty(Env.getContext(Env.getCtx(), MSysConfig.PAYROLL_PASSWORD), true))
				{
					otBasicAmt = (BigDecimal) SecureEngine.decrypt(otBasicAmt, getAD_Client_ID());
				}
				int no = row -1;
				if (basicSalary == null)
					basicSalary = Env.ZERO;
				if (otBasicAmt == null)
					otBasicAmt = Env.ZERO;
				int col = 0;
				//number
				Number num = new Number(col++, row, no);
				sheet.addCell(num);
				// unique ID
				num = new Number(col++, row, contractID);
				sheet.addCell(num);
				//unique uu
				Label l = new Label(col++, row, contractUU);
				sheet.addCell(l);
				//org
				l = new Label(col++, row, organization);
				sheet.addCell(l);
				l = new Label(col++, row, organization);
				sheet.addCell(l);
				
				//nik
				l = new Label(col++, row, nik);
				sheet.addCell(l);
				l = new Label(col++, row, nik);
				sheet.addCell(l);
				//name
				l = new Label(col++, row, name);
				sheet.addCell(l);
				//sectOfDept
				l = new Label(col++, row, sectOfDept);
				sheet.addCell(l);
				l = new Label(col++, row, sectOfDept);
				sheet.addCell(l);
				//COntractType
				l = new Label(col++, row, contractType);
				sheet.addCell(l);
				l = new Label(col++, row, contractType);
				sheet.addCell(l);
				//Payroll Term
				l = new Label(col++, row, payrollTerm);
				sheet.addCell(l);
				l = new Label(col++, row, payrollTerm);
				sheet.addCell(l);
				//Position
				l = new Label(col++, row, position);
				sheet.addCell(l);
				l = new Label(col++, row, position);
				sheet.addCell(l);
				//Payroll Level
				l = new Label(col++, row, payrollLevel);
				sheet.addCell(l);
				l = new Label(col++, row, payrollLevel);
				sheet.addCell(l);
				//Shift Type
				l = new Label(col++, row, shift);
				sheet.addCell(l);
				l = new Label(col++, row, shift);
				sheet.addCell(l);
				//Gender
				l = new Label(col++, row, gender);
				sheet.addCell(l);
				//Date Contract Start
				l = new Label(col++, row, dateContractStart);
				sheet.addCell(l);
				l = new Label(col++, row, dateContractStart);
				sheet.addCell(l);
				//Date Contract End
				l = new Label(col++, row, dateContractEnd);
				sheet.addCell(l);
				l = new Label(col++, row, dateContractEnd);
				sheet.addCell(l);
				//PPh21PaidByCompany
				l = new Label(col++, row, pph21PaidByCompany);
				sheet.addCell(l);
				l = new Label(col++, row, pph21PaidByCompany);
				sheet.addCell(l);
				//Basic Salary
				num = new Number(col++, row, basicSalary.doubleValue());
				sheet.addCell(num);
				num = new Number(col++, row, basicSalary.doubleValue());
				sheet.addCell(num);
				//OT Basic AMount
				num = new Number(col++, row, otBasicAmt.doubleValue());
				sheet.addCell(num);
				num = new Number(col++, row, otBasicAmt.doubleValue());
				sheet.addCell(num);
				if (m_maxCol < col-1)
					m_maxCol = col-1;
				loadComponentData(contractID, sheet, row);
				row++;
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	private String buildContractSQL ()
	{
		StringBuilder builder = new StringBuilder("SELECT ")
		.append("c.UNS_Contract_recommendation_ID, c.UNS_Contract_Recommendation_UU, ")
		.append("o.Value AS organization, e.Value AS nik, e.Name AS Name, s.value AS SectionOfDept, ")
		.append("c.NextContractType AS ContractType, c.NextPayrollTerm AS PayrollTerm, ")
		.append("p.Name AS Position,  c.NewPayrollLevel AS PayrollLevel, c.NewShift AS Shift, ")
		.append("e.Gender AS Gender, c.DateContractStart::DATE AS DateContractStart, c.DateContractEnd::DATE AS DateContractEnd")
		.append(",PPh21PaidByCompany, c.New_G_Pokok AS BasicSalary, c.OTBasicAmt   FROM UNS_Contract_Recommendation c INNER JOIN AD_Org o ")
		.append("ON o.AD_Org_ID = c.AD_Org_ID INNER JOIN UNS_Employee e ON e.UNS_Employee_ID ")
		.append("= c.UNS_Employee_ID INNER JOIN C_BPartner s ON s.C_BPartner_ID = c.NewSectionOfDept_ID ")
		.append("INNER JOIN C_Job p ON p.C_Job_ID = c.NewJob_ID WHERE c.IsActive = 'Y' AND DocStatus IN ('CO','CL')");
		
		if (p_level != null)
			builder.append(" AND  e.PayrollLevel = '").append(p_level).append("'");
		if (p_orgID > 0)
			builder.append(" AND c.AD_Org_ID =").append(p_orgID);
		if (p_sectionDeptID > 0)
			builder.append(" AND c.NewSectionOfDept_ID = ").append(p_sectionDeptID);
		if (p_start != null && p_end != null)
			builder.append(" AND c.DateContractEnd BETWEEN '").append(p_start).
			append("' AND '").append(p_end).append("'");
		else if (p_start != null)
			builder.append(" AND c.DateContractEnd >= '").append(p_start).append("'");
		else if (p_end != null)
			builder.append(" AND c.DateContractEnd <= '").append(p_end).append("'");
		
		String result = builder.toString();
		return result;
	}
	
	private void loadComponentData (int contractID, WritableSheet sheet, int row) throws RowsExceededException, WriteException
	{
		String sql = "SELECT Name, Amount FROM UNS_Payroll_Component_Conf WHERE "
				+ " IsActive = 'Y' AND UNS_Contract_Recommendation_ID = ? "
				+ " AND CostBenefitType IS NULL ORDER BY Name";
		PreparedStatement st = null;
		ResultSet rs = null;
		MColumn column = MColumn.get(getCtx(), "UNS_Payroll_Component_Conf", "Amount");
		boolean isEncrypted = column.isEncrypted();
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, contractID);
			rs = st.executeQuery();
			while (rs.next())
			{
				String name = rs.getString(1);
				BigDecimal amount = rs.getBigDecimal(2);
				Integer hIdx = m_mapHeader.get(name);
				if (hIdx == null)
				{
					hIdx = ++m_maxCol;
					m_mapHeader.put(name, hIdx);
					Label l = new Label(m_maxCol, 0, name);
					sheet.addCell(l);
					CellFormat format = sheet.getCell(m_maxCol, 0).getCellFormat();
					WritableCellFormat wcf = new WritableCellFormat(format);
					wcf.setAlignment(Alignment.CENTRE);
					wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
					l.setCellFormat(wcf);
					sheet.mergeCells(m_maxCol, 0, m_maxCol + 1, 0);

					l = new Label(m_maxCol,1, "Current");
					sheet.addCell(l);
					format = sheet.getCell(m_maxCol, 1).getCellFormat();
					wcf = new WritableCellFormat(format);
					wcf.setAlignment(Alignment.CENTRE);
					wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
					l.setCellFormat(wcf);
					
					l = new Label(++m_maxCol,1, "New");
					sheet.addCell(l);
					format = sheet.getCell(m_maxCol, 1).getCellFormat();
					wcf = new WritableCellFormat(format);
					wcf.setAlignment(Alignment.CENTRE);
					wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
					l.setCellFormat(wcf);
				}
				if (isEncrypted && !Util.isEmpty(Env.getContext(Env.getCtx(), MSysConfig.PAYROLL_PASSWORD), true))
					amount = (BigDecimal)SecureEngine.decrypt(amount, getAD_Client_ID());
				if (amount == null)
					amount = Env.ZERO;
				
				Number num = new Number(hIdx, row, amount.doubleValue());
				sheet.addCell(num);
				num = new Number(++hIdx, row, amount.doubleValue());
				sheet.addCell(num);
				if (m_maxCol < hIdx)
					m_maxCol = hIdx;
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
}
