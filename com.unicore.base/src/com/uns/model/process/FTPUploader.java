/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.compiere.util.CLogger;

/**
 * @author nurse
 *
 */
public class FTPUploader 
{

	private String p_host;
	private String p_user;
	private String p_password;
	private String p_dir;
	private int p_port;
	private List<File> p_files;
	
	public FTPUploader ()
	{
		p_files = new ArrayList<>();
	}
	
	/**
	 * 
	 */
	public FTPUploader(String host, int port, String user, String password, String dir) 
	{
		this();
		p_host = host;
		p_port = port;
		p_user = user;
		p_password = password;
		p_dir = dir;
		
		if (p_port <= 0)
			p_port = 21;
		if (!p_dir.endsWith("\\") && !p_dir.endsWith("/"))
			p_dir += File.separator;
	}
	
	public void addFile (File file)
	{
		p_files.add(file);
	}
	
	public void addFile (String fileName)
	{
		File file = new File(fileName);
		addFile(file);
	}
	
	public boolean doUpload (CLogger log)
	{
		if (p_files.size() == 0)
			return true;
		FTPClient client = new FTPClient();
		try 
		{
			log.log(Level.INFO, "Connecting to FTP Server");
			client.connect(p_host, p_port);
			int reply = client.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply))
			{
				client.disconnect();
				return false;
			}
			
			log.log(Level.INFO, "Loged in to FTP Server");
			client.login(p_user, p_password);

			log.log(Level.INFO, "Enter local passive mode");
			client.enterLocalPassiveMode();
			
			log.log(Level.INFO, "Using binary mode to transfer files");
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			for (int i=0; i<p_files.size(); i++)
			{
				String fileName = p_files.get(i).getName();
				String destinationFileName = p_dir + fileName;
				FileInputStream fis = new FileInputStream(p_files.get(i));
				log.log(Level.INFO, "Sending file " + fileName);
				if (!client.storeFile(destinationFileName, fis))
				{
					fis.close();
					return false;
				}
				log.log(Level.INFO, "Send success " + fileName);
				fis.close();
			}
			log.log(Level.INFO, "Loged out from FTP Server");
			client.logout();
			log.log(Level.INFO, "Disconecting from FTP Server");
			client.disconnect();
		} 
		catch (SocketException e) 
		{
			e.printStackTrace();
			return false;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}
