/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;

import org.compiere.model.MClient;
import org.compiere.model.MSysConfig;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.compiere.util.SecureEngine;
import org.compiere.util.Util;

/**
 * @author nurse
 *
 */
public class DecryptPayrollEncryption extends SvrProcess 
{
	private String p_password = null;
	private String p_dirr = null;
	public static final String I_M_UNLIMITED = "IM_UNLIMITED";

	/**
	 * 
	 */
	public DecryptPayrollEncryption() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
			{
				p_dirr = params[i].getParameterAsString();
			}
			else if ("PrivateKey".equals(params[i].getParameterName()))
			{
				p_password = params[i].getParameterAsString();
			}
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (!Util.isEmpty(p_dirr, true))
		{
			byte[] bytes = Files.readAllBytes(Paths.get(p_dirr));
			p_password = new String(bytes, Charset.forName("UTF-8"));
			if (!Ini.isClient())
			{
				File file = new File(p_dirr);
				if (file.exists() && file.isFile())
				{
					file.delete();
				}
			}
		}
		String sysPassword = MClient.get(getCtx()).getPayrollPassword();
		sysPassword = SecureEngine.decrypt(sysPassword, getAD_Client_ID());
		
		if (!p_password.equals(sysPassword))
		{
			Env.getCtx().remove(MSysConfig.PAYROLL_PASSWORD);
			Env.getCtx().remove(I_M_UNLIMITED);
			return "Invalid Password";
		}
		
		Env.setContext(Env.getCtx(), MSysConfig.PAYROLL_PASSWORD, p_password);
		Env.setContext(Env.getCtx(), I_M_UNLIMITED, "Y");
		return "Private key has registered to environment";
	}

}
