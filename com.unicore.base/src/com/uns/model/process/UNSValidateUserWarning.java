/**
 * 
 */
package com.uns.model.process;

import org.compiere.process.SvrProcess;

import com.uns.model.MUNSUserWarning;

/**
 * @author nurse
 *
 */
public class UNSValidateUserWarning extends SvrProcess {

	/**
	 * 
	 */
	public UNSValidateUserWarning() 
	{
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		MUNSUserWarning warning = new MUNSUserWarning(
				getCtx(), getRecord_ID(), get_TrxName());
		String msg = warning.validate();

		if (msg != null)
			return "User Warning Not Valid :: "+msg;
		
		return "User Warning Valid";
	}

}
