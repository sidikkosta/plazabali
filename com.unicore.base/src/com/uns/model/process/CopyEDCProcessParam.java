/**
 * 
 */
package com.uns.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;

import com.uns.model.MUNSEDCProcessorAction;
import com.uns.model.MUNSEDCProcessorParam;
import com.uns.util.MessageBox;

/**
 * @author nurse
 *
 */
public class CopyEDCProcessParam extends SvrProcess {
	
	private int sourceActionID = -1;
	/**
	 * 
	 */
	public CopyEDCProcessParam() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("SourceAction_ID".equals(params[i].getParameterName()))
				sourceActionID = params[i].getParameterAsInt();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (sourceActionID == -1)
			throw new AdempiereUserError("Mandatory parameter Source Action");
		MUNSEDCProcessorAction action = new MUNSEDCProcessorAction(getCtx(), getRecord_ID(), get_TrxName());
		String sql = "SELECT COUNT(*) FROM UNS_EDCProcessorParam WHERE UNS_EDCProcessorAction_ID = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, getRecord_ID());
		if (count > 0)
		{
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), 
					"Do you want to remove existing records?", "Delete Confirmation", 
					MessageBox.YESNO, MessageBox.ICONQUESTION);
			if (retVal == MessageBox.RETURN_YES)
			{
				sql = "DELETE FROM UNS_EDCProcessorParam WHERE UNS_EDCProcessorAction_ID = ?";
				int deleteOk = DB.executeUpdate(sql, action.get_ID(), false, get_TrxName());
				if (deleteOk == -1)
					throw new AdempiereException();
			}
		}
		MUNSEDCProcessorAction sourceAction = new MUNSEDCProcessorAction(getCtx(), sourceActionID, get_TrxName());
		MUNSEDCProcessorParam[] sourceRequeses = sourceAction.getRequestFields(false);
		MUNSEDCProcessorParam[] sourceResponses = sourceAction.getResponseField(false);
		MUNSEDCProcessorParam[] sourceConn = sourceAction.getConnectionParams(false);
		for (int i=0; i<sourceRequeses.length; i++)
		{
			MUNSEDCProcessorParam request = new MUNSEDCProcessorParam(getCtx(), 0, get_TrxName());
			PO.copyValues(sourceRequeses[i], request);
			request.setAD_Org_ID(action.getAD_Org_ID());
			request.setUNS_EDCProcessorAction_ID(action.get_ID());
			request.setName(sourceRequeses[i].getName());
			request.save();
		}
		for (int i=0; i<sourceResponses.length; i++)
		{
			MUNSEDCProcessorParam response = new MUNSEDCProcessorParam(getCtx(), 0, get_TrxName());
			PO.copyValues(sourceResponses[i], response);
			response.setAD_Org_ID(action.getAD_Org_ID());
			response.setUNS_EDCProcessorAction_ID(action.get_ID());
			response.setName(sourceResponses[i].getName());
			response.save();
		}
		for (int i=0; i<sourceConn.length; i++)
		{
			MUNSEDCProcessorParam response = new MUNSEDCProcessorParam(getCtx(), 0, get_TrxName());
			PO.copyValues(sourceConn[i], response);
			response.setAD_Org_ID(action.getAD_Org_ID());
			response.setUNS_EDCProcessorAction_ID(action.get_ID());
			response.setName(sourceConn[i].getName());
			response.save();
		}
		return null;
	}

}
