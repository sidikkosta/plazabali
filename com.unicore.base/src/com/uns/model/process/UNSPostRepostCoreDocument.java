/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.util.IProcessUI;
import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCost;
import org.compiere.model.MInOut;
import org.compiere.model.MInventory;
import org.compiere.model.MInvoice;
import org.compiere.model.MMatchInv;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementConfirm;
import org.compiere.model.MProduct;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Trx;
import org.compiere.util.Util;


/**
 * @author root
 *
 */
public class UNSPostRepostCoreDocument extends SvrProcess {

	private Timestamp m_dateFrom = null;
	private Timestamp m_dateTo = null;
	private boolean m_force = false;
	private MAcctSchema[] m_ass = null;
	private String m_errorLog = "";
	private IProcessUI m_processMonitor;
	private Hashtable<String, StringBuilder> m_DocumentNoMap =
			new Hashtable<String, StringBuilder>();
	
	private static final String[] m_resources = new String[]{
		"RSC-TFG-401", "RSC-TFG-401a", "RSC-TFG-402", "RSC-TFG-403", 
		"RSC-TFG-404", "RSC-TFG-405", "RSC-TFG-405a", "RSC-TFG-405b", 
		"RSC-TFG-406a", "RSC-TFG-406", "RSC-TFG-407", "RSC-TFG-408", 
		"RSC-TFG-409", "RSC-TFG-409a", "RSC-TFG-410", "RSC-TFG-412", 
		"RSC-TFG-413", "RSC-TFG-414", "RSC-TFG-415", "RSC-TFG-416", 
		"RSC-TFG-417", "RSC-TFG-418", "RSC-TFG-419", "RSC-TFG-420", 
		"RSC-TFG-421", "RSC-TFG-422", "RSC-TFG-423", "RSC-TFG-424", 
		"RSC-TFG-425", "RSC-TFG-426", "RSC-TFG-427", "RSC-TPF-401",
		"RSC-TPF-402", "RSC-TPF-403", "RSC-TPF-404", "RSC-TPF-405", 
		"RSC-TPF-406", "RSC-TPF-407", "RSC-TPF-408", "RSC-TPF-409"
	};

	/**
	 * 
	 */
	public UNSPostRepostCoreDocument() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		int m_acctSchema_ID = 0;
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				continue;
			if(param.getParameterName().equals("DateFrom"))
				m_dateFrom = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("DateTo"))
				m_dateTo = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("IsForce"))
				m_force = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("C_AcctSchema_ID"))
				m_acctSchema_ID = param.getParameterAsInt();
			else
				log.log(Level.WARNING, "UNKNOWN PARAMETER " .concat(
						param.getParameterName()));
		}
		
		if(m_acctSchema_ID > 0)
			m_ass = new MAcctSchema[] {new MAcctSchema(getCtx(),
					m_acctSchema_ID, get_TrxName())};
		else
			m_ass = MAcctSchema.getClientAcctSchema(getCtx(), 
					getAD_Client_ID());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		m_processMonitor = Env.getProcessUI(getCtx());
		
		List<Helper> helpers = new ArrayList<>();
		
		String atc = "";//" AND Description LIKE '%{**AtC**}%' ";

		/** Material Physical Inventory (Import) **/
		helpers.add(new Helper("Initial Stock", MInventory.Table_Name, 
				MInventory.Table_ID,
				MInventory.COLUMNNAME_MovementDate, "SELECT * ", 
				"C_DocType_ID=1000433 AND DocStatus IN ('CO', 'CL', 'RE')"
					+ atc));

		/** MM Receipt or Vendor Return **/
		helpers.add(new Helper("Invoice AP",MInvoice.Table_Name, 
				MInvoice.Table_ID, 
				MInvoice.COLUMNNAME_DateInvoiced, "SELECT * ", 
				MInvoice.COLUMNNAME_IsSOTrx.concat("='N'").
				concat(" AND DocStatus IN ('CO', 'CL', 'RE')")));

		helpers.add(new Helper("Match Invoice", MMatchInv.Table_Name, 
				MMatchInv.Table_ID, MMatchInv.COLUMNNAME_DateTrx,
				"SELECT *, NULL AS C_DocType_ID ",  
				"M_InoutLine_ID IN (SELECT M_InoutLine_ID FROM "
				+ " M_InoutLine WHERE M_Inout_ID IN "
				+ "(SELECT M_Inout_ID FROM M_Inout WHERE IsSOTrx = 'N'))"));

		helpers.add(new Helper("Material Receipt", MInOut.Table_Name, 
				MInOut.Table_ID, MInOut.COLUMNNAME_MovementDate, "SELECT * ", 
				"(".concat(MInOut.COLUMNNAME_IsSOTrx)
				.concat("='N' OR C_DocType_ID = 1000367) ")
				.concat(" AND DocStatus IN ('CO', 'CL', 'RE')" + atc)));
		
		/** Production **/
 		String productionWhereClause = 
				"  DocStatus IN ('CO', 'CL', 'RE') AND UNS_Resource_ID = "
				+ " (SELECT UNS_Resource_ID FROM UNS_Resource "
				+ "WHERE DocumentNo=";
		String wsWhereClause = "";
		
		for (String ws : m_resources)
		{
			wsWhereClause = productionWhereClause + "'" + ws + "')" + atc;
			helpers.add(new Helper("Production", "UNS_Production", 1000058, 
					"MovementDate", "SELECT * ", wsWhereClause));
		}

		/** Intransit Material Movement **/
		helpers.add(new Helper("Intransit Movement", MMovement.Table_Name, 
				MMovement.Table_ID, MMovement.
				COLUMNNAME_MovementDate, "SELECT * ", 
				" C_DocType_ID=1000449 AND DocStatus IN ('CO', 'CL', 'RE')" + 
				atc));
		
		helpers.add(new Helper("Movement Confirmation",
				MMovementConfirm.Table_Name, MMovementConfirm.Table_ID, 
				MMovementConfirm.COLUMNNAME_ReceiptDate, "SELECT * ", 
				" DocStatus IN ('CO', 'CL', 'RE')" + atc));
 
		/** Material Movement **/
		helpers.add(new Helper("Inventory Move", MMovement.Table_Name, 
				MMovement.Table_ID, MMovement.COLUMNNAME_MovementDate, 
				"SELECT * ", " C_DocType_ID=1000375 AND DocStatus IN "
						+ " ('CO', 'CL', 'RE')" + atc));

		/** MM Shipment or Customer Return **/
		helpers.add(new Helper("Customer Shipment/Return", MInOut.Table_Name, 
				MInOut.Table_ID, MInOut.COLUMNNAME_MovementDate, "SELECT * ", 
				MInOut.COLUMNNAME_IsSOTrx
				.concat("='Y'  AND DocStatus IN ('CO', 'CL', 'RE')" + atc)));	

		helpers.add(new Helper("Physical Inventory", MInventory.Table_Name, 
				MInventory.Table_ID, MInventory.COLUMNNAME_MovementDate, 
				"SELECT * ", "C_DocType_ID=1000376 AND DocStatus IN "
						+ " ('CO', 'CL', 'RE')" + atc));

		
		/** Internal Use Inventory **/
		helpers.add(new Helper("Internal Use Inventory", MInventory.Table_Name, 
				MInventory.Table_ID, 
				MInventory.COLUMNNAME_MovementDate, "SELECT * ", 
				"C_DocType_ID=1000379  AND DocStatus IN ('CO', 'CL', 'RE')"));
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(m_dateTo.getTime());
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(
				Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getActualMaximum(
				Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getActualMaximum(
				Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(
				Calendar.MILLISECOND));
		m_dateTo = new Timestamp(calendar.getTimeInMillis());
		postSession(helpers, m_dateFrom);
		
		if (m_DocumentNoMap.size() == 0) 
		{
			m_errorLog = "Completed.";
		}
		else 
		{
			m_errorLog = "Posting Error documents:";
			
			for(String moduleName : m_DocumentNoMap.keySet())
			{
				m_errorLog += "\n " + moduleName + ":: " 
						+ m_DocumentNoMap.get(moduleName);
			}
		}
		log.fine(m_errorLog);
		
		return m_errorLog;
	}
	
	/**
	 * 
	 * @param helpers
	 * @param date
	 * @throws InterruptedException 
	 */
	private void postSession(List<Helper> helpers, Timestamp date) 
			throws InterruptedException
	{
		if(date.compareTo(m_dateTo) > 0)
			return;
		long sleepTimes = sleepTimes();
		if (sleepTimes > 0)
		{
			long timemilis = System.currentTimeMillis();
			timemilis = timemilis + sleepTimes;
			Timestamp endWait = new Timestamp(timemilis);
			String msg = "Process will be hold until " 
					+ endWait.toString() + " Last Processed date " 
					+ date.toString();
			
			log.log(Level.INFO, msg);
			m_processMonitor.statusUpdate(msg);
			
			Thread.sleep(sleepTimes);
		}
		for(int i=0; i< helpers.size(); i++)
		{
			String errMsg = doPost(helpers.get(i), date);
			if(!Util.isEmpty(errMsg, true))
			{
				errMsg = errMsg.concat(" \n Last processed date ") + date;
				log.severe(errMsg);
			}
		}
		
		date = TimeUtil.addDays(date, 1);
		postSession(helpers, date);
	}
	
	/**
	 * Post Document
	 * @param helper
	 * @param date
	 */
	public String doPost(Helper helper, Timestamp date)
	{
		StringBuilder sb = new StringBuilder(helper.SELECT).append(" FROM ")
				.append(helper.TABLE_NAME).append(" WHERE IsActive='Y' AND ")
				.append(" TRUNC(CAST(").append(helper.DATE_COLUMN)
				.append(" AS DATE)) = TRUNC(CAST('").append(date)
				.append("' AS DATE))");
		
		String msg = "Date: " + date + ": Reposting " + helper.TABLE_NAME;
		m_processMonitor.statusUpdate(msg);
		
		if (!m_force) {
			sb.append(" AND Posted !='Y' ");
		}
		
		if(!Util.isEmpty(helper.WHERECLAUSE, true))
		{
			sb.append(" AND ").append(helper.WHERECLAUSE);
		}
		
		sb.append(" ORDER BY DocumentNo ");//MovementDate");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		String trxName = Trx.createTrxName("POST");
		Trx myTrx = Trx.get(trxName, true);
		String errMsg = null;
		
		try
		{
			
			st = DB.prepareStatement(sb.toString(), trxName);
			rs = st.executeQuery();
			while (rs.next())
			{
				
				String posted = rs.getString("Posted");
				boolean repost = !posted.equals("N");
				//posting document
				errMsg = DocManager.postDocument(m_ass, helper.TABLE_ID,
						rs, m_force, repost, trxName);
				
				if(!Util.isEmpty(errMsg, true))
				{	
					boolean isReversal = rs.getInt("Reversal_ID") > 0;
					if (helper.TABLE_ID == 1000058) //Production
					{
						errMsg = analyzeProductionCost(date,  isReversal,
								rs.getInt("UNS_Production_ID"), trxName);
					}
					else if (helper.TABLE_ID == MMovement.Table_ID) //Inventory Move.
					{
						errMsg = analyzeMovementCost(
								date, isReversal, 
								rs.getInt("M_Movement_ID"), trxName);
					}
					else if (helper.TABLE_ID == MInventory.Table_ID
							&& rs.getString("C_DocType_ID").equals("1000376")) 
					{//Internal Use.
						errMsg = analyzeInternalUseCost(
								date, isReversal, 
								rs.getInt("M_Inventory_ID"), trxName);
					}
					else if (helper.TABLE_ID == MInventory.Table_ID
							&& rs.getString("C_DocType_ID").equals("1000379"))
					{//physical inventory
						analyzePhysicalInventoryCost(date, isReversal, 
								rs.getInt("M_Inventory_ID"), trxName);
					}
					else if (helper.TABLE_ID == MInOut.Table_ID
							&& ((rs.getString("MovementType").endsWith("-")
							&& !isReversal) || (rs.getString("MovementType").
									endsWith("+") && isReversal))) 
					{//Customer Shipment,return to vendor.
						errMsg = analyzeShipmentCost(
								date, isReversal, 
								rs.getInt("M_InOut_ID"), trxName);
					}
					else if (helper.TABLE_ID == MMatchInv.Table_ID)
					{
						errMsg = analyzeMatchInvCost(date, isReversal, 
								rs.getInt("M_MatchInv_ID"), trxName);
					}
					
					if(!Util.isEmpty(errMsg, true))
					{
						StringBuilder docNoList = m_DocumentNoMap.get(
								helper.MODULE_NAME);
						if (docNoList == null) {
							docNoList = new StringBuilder(
									rs.getString("DocumentNo"));
							m_DocumentNoMap.put(helper.MODULE_NAME, docNoList);
						}
						else {
							docNoList.append(", ").append(
									rs.getString("DocumentNo"));
						}
						m_errorLog = m_errorLog.concat(errMsg).
								concat("\n ********************** \n");
					}
					else {
						errMsg = DocManager.postDocument(
								m_ass, helper.TABLE_ID, rs, m_force, repost, 
								trxName);
						
						if(!Util.isEmpty(errMsg, true))
						{
							StringBuilder docNoList = m_DocumentNoMap.get(
									helper.MODULE_NAME);
							if (docNoList == null) {
								docNoList = new StringBuilder(
										rs.getString("DocumentNo"));
								m_DocumentNoMap.put(helper.MODULE_NAME, 
										docNoList);
							}
							else {
								docNoList.append(", ").append(
										rs.getString("DocumentNo"));
							}
							m_errorLog = m_errorLog.concat(errMsg).
									concat("\n ********************** \n");
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
			try
			{
				myTrx.commit();
				myTrx.close();
				myTrx = null;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return errMsg;
	}
	
	/** Analyze production cost.*/
	String analyzeProductionCost(Timestamp dateDoc, boolean isReversal, 
			int Production_ID, String trxName)
	{
		String isEndProduct = isReversal ? "Y" : "N";
		StringBuilder sqlBuilder = new StringBuilder("SELECT AD_Org_ID, ")
		.append("M_Product_ID, SUM (MovementQty) FROM UNS_Production_Detail ")
		.append(" WHERE UNS_Production_ID=").append(Production_ID )
		.append(" AND IsActive='Y' AND IsEndProduct='").append(isEndProduct)
		.append("' GROUP BY M_Product_ID, AD_Org_ID");
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze Inventory Movement cost.*/
	String analyzeMovementCost(Timestamp dateDoc, boolean isReversal, 
			int Movement_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT l.AD_Org_ID, ")
		.append("ml.M_Product_ID, SUM(ml.MovementQty) FROM M_MovementLine ml ")
		.append("INNER JOIN M_Locator l ON ");
		
		if (isReversal)
		{
			sqlBuilder.append("ml.M_LocatorTo_ID=l.M_Locator_ID ");
		}
		else
		{
			sqlBuilder.append("ml.M_Locator_ID=l.M_Locator_ID ");
		}
		
		sqlBuilder.append("WHERE ml.M_Movement_ID= ").append(Movement_ID)
		.append("AND ml.IsActive='Y' GROUP BY M_Product_ID, l.AD_Org_ID ");
		
		String sql = sqlBuilder.toString();
		
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	String analyzeMatchInvCost (Timestamp dateDoc, boolean isReversal, 
			int MatchInv_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT AD_Org_ID, ")
		.append("M_Product_ID, Qty FROM M_MatchInv WHERE M_MatchInv_ID = ")
		.append(MatchInv_ID);
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze Inventory Movement cost.*/
	String analyzeInternalUseCost(Timestamp dateDoc, boolean isReversal, 
			int Inventory_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT loc.AD_Org_ID, ")
		.append("M_Product_ID,  SUM(QtyInternalUse) FROM M_InventoryLine il ")
		.append("INNER JOIN M_Locator loc ON il.M_Locator_ID=loc.M_Locator_ID ")
		.append("WHERE M_Inventory_ID= ").append(Inventory_ID )
		.append("AND IsActive='Y' GROUP BY M_Product_ID, loc.AD_Org_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	String analyzePhysicalInventoryCost(Timestamp dateDoc, boolean isReversal, 
			int Inventory_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT loc.AD_Org_ID, ")
		.append("M_Product_ID,  SUM(qtyCount-QtyBook) FROM M_InventoryLine il ")
		.append("INNER JOIN M_Locator loc ON il.M_Locator_ID=loc.M_Locator_ID ")
		.append("WHERE M_Inventory_ID= ").append(Inventory_ID )
		.append("AND IsActive='Y' GROUP BY M_Product_ID, loc.AD_Org_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze Customer Shipment cost.*/
	String analyzeShipmentCost(Timestamp dateDoc, boolean isReversal, 
			int InOut_ID, String trxName)
	{
		StringBuilder sqlBuilder = new StringBuilder("SELECT ml.AD_Org_ID, ")
		.append("ml.M_Product_ID, SUM(ml.MovementQty) FROM M_InOutLine ml ")
		.append("WHERE ml.M_InOut_ID=").append(InOut_ID)
		.append(" AND ml.IsActive='Y' GROUP BY M_Product_ID, ml.AD_Org_ID");
		
		String sql = sqlBuilder.toString();
		return analyzeCost(dateDoc, sql, trxName);
	}
	
	/** Analyze cost using the given query */
	String analyzeCost(Timestamp dateDoc, String sql, String trxName)
	{
		String errMsg = null;
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, trxName);
			rs = st.executeQuery();
			while (rs.next())
			{
				int AD_Org_ID = rs.getInt(1);
				int M_Product_ID = rs.getInt(2);
				BigDecimal expectedQty = rs.getBigDecimal(3).abs();
				errMsg = analyzeCost(dateDoc, AD_Org_ID, 
						M_Product_ID, expectedQty, trxName);
				
				if (errMsg != null)
					break;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			DB.close(rs, st);
		}
		
		return errMsg;
	}
	
	/**
	 * 
	 * @param AD_Org_ID
	 * @param M_Product_ID
	 * @param expectedQty
	 * @param trxName
	 * @return
	 */
	String analyzeCost(Timestamp dateDoc, int AD_Org_ID, int M_Product_ID, BigDecimal expectedQty, String trxName)
	{
		String errMsg = null;
		
		String sql = "SELECT CurrentQty, CurrentCostPrice, CumulatedAmt, CumulatedQty FROM M_Cost "
				+ " WHERE AD_Org_ID=? AND M_Product_ID=? AND M_CostElement_ID=?";
		PreparedStatement st = null;
		ResultSet rs = null;

		try
		{
			st = DB.prepareStatement(sql, null);
			st.setInt(1, AD_Org_ID);
			st.setInt(2, M_Product_ID);
			st.setInt(3, 1000014);
			rs = st.executeQuery();
			boolean found = false;
			
			while (rs.next())
			{
				found = true;
				BigDecimal currentQty = rs.getBigDecimal(1);
				BigDecimal currentCost = rs.getBigDecimal(2);
				BigDecimal cumulatedQty = rs.getBigDecimal(3);
				BigDecimal cumulatedAmt = rs.getBigDecimal(4);
				
//				if (currentCost.signum() <= 0) {
//					errMsg = "Current Cost is zero for product of " + MProduct.get(getCtx(), M_Product_ID);
//					break;
//				}
				
				if (currentQty.compareTo(expectedQty) >= 0) {
					break;
				}
				
				BigDecimal diffQty = expectedQty.subtract(currentQty);
				BigDecimal totalDiffAmt = diffQty.multiply(currentCost);
				BigDecimal newCumulatedQty = cumulatedQty.add(diffQty);
				BigDecimal newCumulatedAmt = cumulatedAmt.add(totalDiffAmt);
				
				String updateCostSql = "UPDATE M_Cost SET CurrentQty=?, CumulatedAmt=?, CumulatedQty=? "
						+ " WHERE AD_Org_ID=? AND M_Product_ID=?";
				int count = DB.executeUpdateEx(updateCostSql, 
						new Object[]{expectedQty, newCumulatedAmt, newCumulatedQty, AD_Org_ID, M_Product_ID}, trxName);
				if (count <= 0) {
					errMsg = "Failed when updating cost for product of " + MProduct.get(getCtx(), M_Product_ID);
					break;
				}
			}
			
			if (!found) {
				//errMsg = "Cannot find MCost record for product of " + MProduct.get(getCtx(), M_Product_ID);
				
				BigDecimal currentCost = getCostOf(M_Product_ID, dateDoc, trxName);
				
				if (currentCost == null)
					errMsg = "Cannot find MCost record for product of " + MProduct.get(getCtx(), M_Product_ID);
				else {
					MCost cost = new MCost(MProduct.get(getCtx(), M_Product_ID), 0, m_ass[0], AD_Org_ID, 1000014);
					cost.setCurrentQty(expectedQty);
					cost.setCurrentCostPrice(currentCost);
					
					BigDecimal newCumulatedAmt = currentCost.multiply(expectedQty);
					cost.setCumulatedQty(expectedQty);
					cost.setCumulatedAmt(newCumulatedAmt);
					cost.saveEx();
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			DB.close(rs, st);
		}
		
		return errMsg;
	}
	
	/**
	 * 
	 * @param M_Product_ID
	 * @param dateDoc
	 * @param trxName
	 * @return
	 */
	BigDecimal getCostOf(int M_Product_ID, Timestamp dateDoc, String trxName)
	{
		int M_PriceList_ID_1 = 1000101; // Harga Pembelian
		int M_PriceList_ID_2 = 1000099; // Harga Toko.
		int M_PriceList_ID_3 = 1000104; // PL Auto Initial Purposes Only.
		int M_PriceList_ID_4 = 1000100; // Harga Konsumen.
		
		String sql = 
				"SELECT pp.PriceList FROM M_ProductPrice pp "
				+ " INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
				+ " INNER JOIN M_PriceList pl ON plv.M_PriceList_ID=pl.M_PriceList_ID "
				+ " WHERE pp.M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
				+ " ORDER BY plv.ValidFrom DESC";
		
		BigDecimal priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_1, dateDoc);				
		BigDecimal percentage = Env.ONE;
		
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_2, dateDoc);
			percentage = BigDecimal.valueOf(0.75);
		}
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_3, dateDoc);
			percentage = BigDecimal.valueOf(0.65);
		}
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_4, dateDoc);
			percentage = BigDecimal.valueOf(0.55);
		}
		
		if (priceList == null || priceList.signum() <= 0)
			return null;
		
		BigDecimal cost = priceList.multiply(percentage);

		return cost;
	}
	
	private long sleepTimes ()
	{
		long sleepTimes = 0;
		int start = 6;
		int end = 17;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		int curHour = cal.get(Calendar.HOUR_OF_DAY);
		int curMinute = cal.get(Calendar.MINUTE);
		int day = cal.get(Calendar.DAY_OF_WEEK);
		
		if (day != 1 && curHour >= start && curHour < end)
		{
			sleepTimes = end - curHour;
			curMinute = curMinute * 60 * 1000;
			sleepTimes = sleepTimes * 60 *60 * 1000;
			sleepTimes = sleepTimes - curMinute;
		}
		
		return sleepTimes;
	}
}

class Helper
{
	String MODULE_NAME = null;
	String TABLE_NAME = null;
	String DATE_COLUMN = null;
	String WHERECLAUSE = null;
	String SELECT 	   = null;
	int TABLE_ID = 0;

	/**
	 * 
	 * @param tableName
	 * @param tableID
	 * @param dateColumn
	 * @param whereClause
	 */
	public Helper(String moduleName, String tableName,int tableID, 
			String dateColumn, String select, String whereClause)
	{
		this.MODULE_NAME = moduleName;
		this.TABLE_NAME = tableName; 
		this.DATE_COLUMN = dateColumn;
		this.SELECT = select;
		this.WHERECLAUSE = whereClause;
		this.TABLE_ID = tableID;
	}
}