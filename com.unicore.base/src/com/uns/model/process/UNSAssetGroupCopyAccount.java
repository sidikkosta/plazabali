/**
 * 
 */
package com.uns.model.process;

import org.compiere.model.MAsset;
import org.compiere.model.MAssetAcct;
import org.compiere.model.MAssetGroup;
import org.compiere.model.MAssetGroupAcct;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;

/**
 * @author Burhani Adam
 *
 */
public class UNSAssetGroupCopyAccount extends SvrProcess {

	/**
	 * 
	 */
	public UNSAssetGroupCopyAccount() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MAssetGroupAcct gAcct = new MAssetGroupAcct(getCtx(), getRecord_ID(), get_TrxName());
		MAssetGroup group = new MAssetGroup(getCtx(), gAcct.getA_Asset_Group_ID(), get_TrxName());
		
		MAsset[] asset = group.getAssets();
		
		for(int i=0;i<asset.length;i++)
		{
			MAssetAcct[] aAcct = asset[i].getAcct();
			
			for(int j=0;j<aAcct.length;j++)
			{
				if(aAcct[j].getC_AcctSchema_ID() != gAcct.getC_AcctSchema_ID())
					continue;
				
				aAcct[j].setA_Asset_Acct(gAcct.getA_Asset_Acct());
				aAcct[j].setA_Accumdepreciation_Acct(gAcct.getA_Accumdepreciation_Acct());
				aAcct[j].setA_Depreciation_Acct(gAcct.getA_Depreciation_Acct());
				aAcct[j].setA_Disposal_Revenue(gAcct.getA_Disposal_Revenue());
				aAcct[j].setA_Disposal_Loss(gAcct.getA_Disposal_Loss());
				if(!aAcct[j].save())
					return CLogger.retrieveErrorString("Failed when trying update asset account on " + asset[i].getName());
				
				break;
			}
		}
		
		return "Success copy to " + asset.length + " records.";
	}
}