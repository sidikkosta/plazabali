/**
 * 
 */
package com.uns.model.process;

import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MBPartnerLocation;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author menjangan
 *
 */
public class GenerateDefaultPartnerLocation extends SvrProcess {

	private int m_partnerGroup_ID = 0;
	private int m_defaultLocation_ID = 0;
	
	/**
	 * 
	 */
	public GenerateDefaultPartnerLocation() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter params[] = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if (null == name)
			{
				continue;
			}
			else if ("C_BP_Group_ID".equals(name))
			{
				this.m_partnerGroup_ID = params[i].getParameterAsInt();
			}
			else if ("C_Location_ID".equals(name))
			{
				this.m_defaultLocation_ID = params[i].getParameterAsInt();
			}
			else
			{
				log.log(Level.WARNING, "Unknown parameter " + name);
			}
		}
	}
	
	private List<Point> getOrgPartner ()
	{
		List<Point> points = new ArrayList<>();
		StringBuilder pr = new StringBuilder("SELECT AD_Org_ID, C_BPartner_ID FROM ")
		.append(" C_BPartner WHERE IsActive = 'Y' AND NOT EXISTS (SELECT * FROM ")
		.append(" C_BPartner_Location WHERE C_BPartner_ID = C_BPartner.C_BPartner_ID) ");
		if (m_partnerGroup_ID > 0)
		{
			pr.append(" AND C_BPartner_Group_ID =  ").append(m_partnerGroup_ID);
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(pr.toString(), get_TrxName());
			rs = st.executeQuery();
			while (rs.next())
			{
				points.add(new Point(rs.getInt(1), rs.getInt(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return points;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		IProcessUI ui = Env.getProcessUI(getCtx());
		ui.statusUpdate("Load Partner, please wait...");
		List<Point> points = getOrgPartner();
		int maxRecord = points.size();
		
		for (int i=0; i<maxRecord; i++)
		{
			MBPartnerLocation location = new MBPartnerLocation(getCtx(), 0, get_TrxName());
			location.setAD_Org_ID((int)points.get(i).getX());
			location.setC_BPartner_ID((int)points.get(i).getY());
			location.setC_Location_ID(m_defaultLocation_ID);
			location.setName("Undefined");
			
			if (!location.save())
			{
				throw new AdempiereException(CLogger.retrieveErrorString("Could not save location"));
			}
			
			int count = i+1;
			ui.statusUpdate("Created " + count + " of " + maxRecord);
		}		
		
		return null;
	}
}
