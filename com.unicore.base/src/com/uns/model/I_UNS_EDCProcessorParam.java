/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_EDCProcessorParam
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_EDCProcessorParam 
{

    /** TableName=UNS_EDCProcessorParam */
    public static final String Table_Name = "UNS_EDCProcessorParam";

    /** AD_Table_ID=1000420 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DataType */
    public static final String COLUMNNAME_DataType = "DataType";

	/** Set Data Type.
	  * Type of data
	  */
	public void setDataType (String DataType);

	/** Get Data Type.
	  * Type of data
	  */
	public String getDataType();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsHexDump */
    public static final String COLUMNNAME_IsHexDump = "IsHexDump";

	/** Set Hex Dump ?	  */
	public void setIsHexDump (boolean IsHexDump);

	/** Get Hex Dump ?	  */
	public boolean isHexDump();

    /** Column name Length */
    public static final String COLUMNNAME_Length = "Length";

	/** Set Length	  */
	public void setLength (int Length);

	/** Get Length	  */
	public int getLength();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name ParameterType */
    public static final String COLUMNNAME_ParameterType = "ParameterType";

	/** Set Parameter Type	  */
	public void setParameterType (String ParameterType);

	/** Get Parameter Type	  */
	public String getParameterType();

    /** Column name ParameterValue */
    public static final String COLUMNNAME_ParameterValue = "ParameterValue";

	/** Set Parameter Value	  */
	public void setParameterValue (String ParameterValue);

	/** Get Parameter Value	  */
	public String getParameterValue();

    /** Column name SourceValue */
    public static final String COLUMNNAME_SourceValue = "SourceValue";

	/** Set Source Value	  */
	public void setSourceValue (String SourceValue);

	/** Get Source Value	  */
	public String getSourceValue();

    /** Column name TransactionType */
    public static final String COLUMNNAME_TransactionType = "TransactionType";

	/** Set Transaction Type	  */
	public void setTransactionType (String TransactionType);

	/** Get Transaction Type	  */
	public String getTransactionType();

    /** Column name UNS_EDCProcessorAction_ID */
    public static final String COLUMNNAME_UNS_EDCProcessorAction_ID = "UNS_EDCProcessorAction_ID";

	/** Set EDC Processor Action	  */
	public void setUNS_EDCProcessorAction_ID (int UNS_EDCProcessorAction_ID);

	/** Get EDC Processor Action	  */
	public int getUNS_EDCProcessorAction_ID();

	public com.uns.model.I_UNS_EDCProcessorAction getUNS_EDCProcessorAction() throws RuntimeException;

    /** Column name UNS_EDCProcessorParam_ID */
    public static final String COLUMNNAME_UNS_EDCProcessorParam_ID = "UNS_EDCProcessorParam_ID";

	/** Set EDC Processor Parameter	  */
	public void setUNS_EDCProcessorParam_ID (int UNS_EDCProcessorParam_ID);

	/** Get EDC Processor Parameter	  */
	public int getUNS_EDCProcessorParam_ID();

    /** Column name UNS_EDCProcessorParam_UU */
    public static final String COLUMNNAME_UNS_EDCProcessorParam_UU = "UNS_EDCProcessorParam_UU";

	/** Set UNS_EDCProcessorParam_UU	  */
	public void setUNS_EDCProcessorParam_UU (String UNS_EDCProcessorParam_UU);

	/** Get UNS_EDCProcessorParam_UU	  */
	public String getUNS_EDCProcessorParam_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
