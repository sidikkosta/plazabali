/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_CommPort
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_CommPort 
{

    /** TableName=UNS_CommPort */
    public static final String Table_Name = "UNS_CommPort";

    /** AD_Table_ID=1000339 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BautRate */
    public static final String COLUMNNAME_BautRate = "BautRate";

	/** Set BautRate	  */
	public void setBautRate (int BautRate);

	/** Get BautRate	  */
	public int getBautRate();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DataBits */
    public static final String COLUMNNAME_DataBits = "DataBits";

	/** Set DataBits	  */
	public void setDataBits (int DataBits);

	/** Get DataBits	  */
	public int getDataBits();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Mode */
    public static final String COLUMNNAME_Mode = "Mode";

	/** Set Mode	  */
	public void setMode (int Mode);

	/** Get Mode	  */
	public int getMode();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name ParityBit */
    public static final String COLUMNNAME_ParityBit = "ParityBit";

	/** Set ParityBit	  */
	public void setParityBit (int ParityBit);

	/** Get ParityBit	  */
	public int getParityBit();

    /** Column name PortType */
    public static final String COLUMNNAME_PortType = "PortType";

	/** Set PortType	  */
	public void setPortType (String PortType);

	/** Get PortType	  */
	public String getPortType();

    /** Column name StopBits */
    public static final String COLUMNNAME_StopBits = "StopBits";

	/** Set StopBits	  */
	public void setStopBits (int StopBits);

	/** Get StopBits	  */
	public int getStopBits();

    /** Column name TestPort */
    public static final String COLUMNNAME_TestPort = "TestPort";

	/** Set TestPort	  */
	public void setTestPort (String TestPort);

	/** Get TestPort	  */
	public String getTestPort();

    /** Column name UNS_CommPort_ID */
    public static final String COLUMNNAME_UNS_CommPort_ID = "UNS_CommPort_ID";

	/** Set UNS_CommPort_ID	  */
	public void setUNS_CommPort_ID (int UNS_CommPort_ID);

	/** Get UNS_CommPort_ID	  */
	public int getUNS_CommPort_ID();

    /** Column name UNS_CommPort_UU */
    public static final String COLUMNNAME_UNS_CommPort_UU = "UNS_CommPort_UU";

	/** Set UNS_CommPort_UU	  */
	public void setUNS_CommPort_UU (String UNS_CommPort_UU);

	/** Get UNS_CommPort_UU	  */
	public String getUNS_CommPort_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}
