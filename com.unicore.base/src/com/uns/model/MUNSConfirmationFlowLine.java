package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

public class MUNSConfirmationFlowLine extends X_UNS_ConfirmationFlow_Line {

	/**
	 * Mr Thunder
	 */
	private static final long serialVersionUID = 6470402382003712273L;

	public MUNSConfirmationFlowLine(Properties ctx,
			int UNS_ConfirmationFlow_Line_ID, String trxName) {
		super(ctx, UNS_ConfirmationFlow_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSConfirmationFlowLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(getSeqNo() <= 0)
		{
			log.saveError("Could Not Save Change", "Disallowed Sequence Number is zero or negative");
			return false;
		}
		
		if(isGeneral())
		{
			String sql = "SELECT 1 FROM UNS_ConfirmationFlow_Line WHERE SeqNo < ? AND UNS_ConfirmationFlow_Line_ID <> ? AND isActive = 'Y'";
			boolean exists = DB.getSQLValue(get_TrxName(), sql,getSeqNo(), getUNS_ConfirmationFlow_Line_ID()) > 0;
			if(exists)
			{
				log.saveError("Could Not Save Change", "isGeneral must define on first sequence");
				return false;
			}
		}
		else
		{
			String sql = "SELECT COALESCE(SeqNo,0) FROM UNS_ConfirmationFlow_Line WHERE isGeneral = 'Y' AND isActive = 'Y' ";
			int seq = DB.getSQLValue(get_TrxName(), sql);
			if(seq > getSeqNo())
			{
				log.saveError("Could Not Save Change", "There was isGeneral Line more than this sequence. IsGeneral must define on first sequence");
				return false;
			}
				
		}
				
		String whereclause = "";
		
		if(getAD_User_ID() > 0)
			whereclause = "AD_User_ID = "+getAD_User_ID()+" AND ";
		else if(getAD_Role_ID() > 0)
			whereclause = "AD_Role_ID = "+getAD_Role_ID()+" AND ";
		else
			whereclause = "isGeneral = 'Y' AND ";
			
		StringBuilder sql = new StringBuilder("SELECT COALESCE(SeqNo,0) FROM UNS_ConfirmationFlow_Line WHERE ")
				.append(whereclause);
		sql.append(" isActive = 'Y' AND UNS_ConfirmationFlow_Line_ID <> ?");
		int no = DB.getSQLValue(get_TrxName(), sql.toString(), getUNS_ConfirmationFlow_Line_ID());
		if(no > 0)
		{
			log.saveError("Could Not Save Change", "Duplicate line. #Sequence No = "+no);
			return false;
		}
		
		return super.beforeSave(newRecord);
	}
}
