/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSBankEDCBatchRecap extends X_UNS_BankEDCBatchRecap {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1784696051278605615L;

	/**
	 * @param ctx
	 * @param UNS_BankEDCBatchRecap_ID
	 * @param trxName
	 */
	public MUNSBankEDCBatchRecap(Properties ctx, int UNS_BankEDCBatchRecap_ID,
			String trxName) {
		super(ctx, UNS_BankEDCBatchRecap_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSBankEDCBatchRecap(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSBankEDCBatchRecap getCreate(Properties ctx, int orgID, int edcID,
			Timestamp date, String batchNo, String trxName)
	{
		MUNSDailyBankEDCBalance parent = MUNSDailyBankEDCBalance.getCreate(ctx, orgID, edcID, date, trxName);
		
		MUNSBankEDCBatchRecap result = Query.get(ctx, UNSDefaultModelFactory.EXTENSION_ID,
				Table_Name, "UNS_DailyBankEDCBalance_ID = ? AND BatchNo = ? AND DateTrx = ?", trxName)
					.setParameters(parent.get_ID(), batchNo, date).firstOnly();
		
		if(result == null)
		{
			result = new MUNSBankEDCBatchRecap(ctx, 0, trxName);
			result.setUNS_DailyBankEDCBalance_ID(parent.get_ID());
			result.setAD_Org_ID(parent.getAD_Org_ID());
			result.setBatchNo(batchNo);
			result.setDateTrx(date);
			result.setTID(DB.getSQLValueString(trxName, "SELECT IDNo FROM UNS_EDC WHERE UNS_EDC_ID = ?", edcID));
			result.setTotalARAmount(Env.ZERO);
			result.setTotalDisbursedAmt(Env.ZERO);
			result.setTotalOutstandingAmt(Env.ZERO);
			result.saveEx();
		}
		
		return result;
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		String sql = "UPDATE UNS_DailyBankEDCBalance p SET"
				+ " TotalARAmount ="
				+ " (SELECT SUM(t.TotalARAmount) FROM UNS_BankEDCBatchRecap t"
				+ " WHERE t.UNS_DailyBankEDCBalance_ID = p.UNS_DailyBankEDCBalance_ID),"
				+ " TotalDisbursedAmt ="
				+ " (SELECT SUM(t.TotalDisbursedAmt) FROM UNS_BankEDCBatchRecap t"
				+ " WHERE t.UNS_DailyBankEDCBalance_ID = p.UNS_DailyBankEDCBalance_ID),"
				+ " TotalOutstandingAmt ="
				+ " (SELECT SUM(t.TotalOutstandingAmt) FROM UNS_BankEDCBatchRecap t"
				+ " WHERE t.UNS_DailyBankEDCBalance_ID = p.UNS_DailyBankEDCBalance_ID),"
				+ " DifferenceAmt ="
				+ " (SELECT SUM(t.DifferenceAmt) FROM UNS_BankEDCBatchRecap t"
				+ " WHERE t.UNS_DailyBankEDCBalance_ID = p.UNS_DailyBankEDCBalance_ID)"
				+ " WHERE p.UNS_DailyBankEDCBalance_ID = ?";
		
		return DB.executeUpdate(sql, getUNS_DailyBankEDCBalance_ID(), get_TrxName()) >= 0
				&& MUNSDailyBankEDCBalance.upHeader(get_TrxName(), getUNS_DailyBankEDCBalance().getUNS_BankEDCBalance_ID()) >= 0;
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		setTotalOutstandingAmt(getTotalARAmount().subtract(getTotalDisbursedAmt()).subtract(getDifferenceAmt()));
		
		return true;
	}
}