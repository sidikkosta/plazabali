/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author nurse
 *
 */
public class MUNSUserWarningRole extends X_UNS_UserWarningRole {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2633321438809838592L;

	/**
	 * @param ctx
	 * @param UNS_UserWarningRole_ID
	 * @param trxName
	 */
	public MUNSUserWarningRole(Properties ctx, int UNS_UserWarningRole_ID,
			String trxName) {
		super(ctx, UNS_UserWarningRole_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUserWarningRole(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
