/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ConfirmationFlow_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ConfirmationFlow_Line 
{

    /** TableName=UNS_ConfirmationFlow_Line */
    public static final String Table_Name = "UNS_ConfirmationFlow_Line";

    /** AD_Table_ID=1000335 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_Role_ID */
    public static final String COLUMNNAME_AD_Role_ID = "AD_Role_ID";

	/** Set Role.
	  * Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID);

	/** Get Role.
	  * Responsibility Role
	  */
	public int getAD_Role_ID();

	public org.compiere.model.I_AD_Role getAD_Role() throws RuntimeException;

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name isGeneral */
    public static final String COLUMNNAME_isGeneral = "isGeneral";

	/** Set isGeneral	  */
	public void setisGeneral (boolean isGeneral);

	/** Get isGeneral	  */
	public boolean isGeneral();

    /** Column name SeqNo */
    public static final String COLUMNNAME_SeqNo = "SeqNo";

	/** Set Sequence.
	  * Method of ordering records;
 lowest number comes first
	  */
	public void setSeqNo (int SeqNo);

	/** Get Sequence.
	  * Method of ordering records;
 lowest number comes first
	  */
	public int getSeqNo();

    /** Column name UNS_ConfirmationFlow_ID */
    public static final String COLUMNNAME_UNS_ConfirmationFlow_ID = "UNS_ConfirmationFlow_ID";

	/** Set UNS_ConfirmationFlow_ID	  */
	public void setUNS_ConfirmationFlow_ID (int UNS_ConfirmationFlow_ID);

	/** Get UNS_ConfirmationFlow_ID	  */
	public int getUNS_ConfirmationFlow_ID();

	public com.uns.model.I_UNS_ConfirmationFlow getUNS_ConfirmationFlow() throws RuntimeException;

    /** Column name UNS_ConfirmationFlow_Line_ID */
    public static final String COLUMNNAME_UNS_ConfirmationFlow_Line_ID = "UNS_ConfirmationFlow_Line_ID";

	/** Set UNS_ConfirmationFlow_Line_ID	  */
	public void setUNS_ConfirmationFlow_Line_ID (int UNS_ConfirmationFlow_Line_ID);

	/** Get UNS_ConfirmationFlow_Line_ID	  */
	public int getUNS_ConfirmationFlow_Line_ID();

    /** Column name UNS_ConfirmationFlow_Line_UU */
    public static final String COLUMNNAME_UNS_ConfirmationFlow_Line_UU = "UNS_ConfirmationFlow_Line_UU";

	/** Set UNS_ConfirmationFlow_Line_UU	  */
	public void setUNS_ConfirmationFlow_Line_UU (String UNS_ConfirmationFlow_Line_UU);

	/** Get UNS_ConfirmationFlow_Line_UU	  */
	public String getUNS_ConfirmationFlow_Line_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
