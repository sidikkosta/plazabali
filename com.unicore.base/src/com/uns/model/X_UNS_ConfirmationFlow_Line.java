/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_ConfirmationFlow_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ConfirmationFlow_Line extends PO implements I_UNS_ConfirmationFlow_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171012L;

    /** Standard Constructor */
    public X_UNS_ConfirmationFlow_Line (Properties ctx, int UNS_ConfirmationFlow_Line_ID, String trxName)
    {
      super (ctx, UNS_ConfirmationFlow_Line_ID, trxName);
      /** if (UNS_ConfirmationFlow_Line_ID == 0)
        {
			setSeqNo (0);
// @SQL=SELECT COALESCE(MAX(SeqNo),0)+1 AS DefaultValue FROM AD_Tab WHERE AD_Window_ID=@AD_Window_ID@
			setUNS_ConfirmationFlow_ID (0);
			setUNS_ConfirmationFlow_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ConfirmationFlow_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ConfirmationFlow_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Role getAD_Role() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Role)MTable.get(getCtx(), org.compiere.model.I_AD_Role.Table_Name)
			.getPO(getAD_Role_ID(), get_TrxName());	}

	/** Set Role.
		@param AD_Role_ID 
		Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID)
	{
		if (AD_Role_ID < 0) 
			set_Value (COLUMNNAME_AD_Role_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Role_ID, Integer.valueOf(AD_Role_ID));
	}

	/** Get Role.
		@return Responsibility Role
	  */
	public int getAD_Role_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Role_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_Value (COLUMNNAME_AD_User_ID, null);
		else 
			set_Value (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set isGeneral.
		@param isGeneral isGeneral	  */
	public void setisGeneral (boolean isGeneral)
	{
		set_Value (COLUMNNAME_isGeneral, Boolean.valueOf(isGeneral));
	}

	/** Get isGeneral.
		@return isGeneral	  */
	public boolean isGeneral () 
	{
		Object oo = get_Value(COLUMNNAME_isGeneral);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_ConfirmationFlow getUNS_ConfirmationFlow() throws RuntimeException
    {
		return (com.uns.model.I_UNS_ConfirmationFlow)MTable.get(getCtx(), com.uns.model.I_UNS_ConfirmationFlow.Table_Name)
			.getPO(getUNS_ConfirmationFlow_ID(), get_TrxName());	}

	/** Set UNS_ConfirmationFlow_ID.
		@param UNS_ConfirmationFlow_ID UNS_ConfirmationFlow_ID	  */
	public void setUNS_ConfirmationFlow_ID (int UNS_ConfirmationFlow_ID)
	{
		if (UNS_ConfirmationFlow_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_ID, Integer.valueOf(UNS_ConfirmationFlow_ID));
	}

	/** Get UNS_ConfirmationFlow_ID.
		@return UNS_ConfirmationFlow_ID	  */
	public int getUNS_ConfirmationFlow_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ConfirmationFlow_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ConfirmationFlow_Line_ID.
		@param UNS_ConfirmationFlow_Line_ID UNS_ConfirmationFlow_Line_ID	  */
	public void setUNS_ConfirmationFlow_Line_ID (int UNS_ConfirmationFlow_Line_ID)
	{
		if (UNS_ConfirmationFlow_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_Line_ID, Integer.valueOf(UNS_ConfirmationFlow_Line_ID));
	}

	/** Get UNS_ConfirmationFlow_Line_ID.
		@return UNS_ConfirmationFlow_Line_ID	  */
	public int getUNS_ConfirmationFlow_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ConfirmationFlow_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ConfirmationFlow_Line_UU.
		@param UNS_ConfirmationFlow_Line_UU UNS_ConfirmationFlow_Line_UU	  */
	public void setUNS_ConfirmationFlow_Line_UU (String UNS_ConfirmationFlow_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_Line_UU, UNS_ConfirmationFlow_Line_UU);
	}

	/** Get UNS_ConfirmationFlow_Line_UU.
		@return UNS_ConfirmationFlow_Line_UU	  */
	public String getUNS_ConfirmationFlow_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ConfirmationFlow_Line_UU);
	}
}