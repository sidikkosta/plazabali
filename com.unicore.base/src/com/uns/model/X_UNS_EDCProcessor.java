/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_EDCProcessor
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EDCProcessor extends PO implements I_UNS_EDCProcessor, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180925L;

    /** Standard Constructor */
    public X_UNS_EDCProcessor (Properties ctx, int UNS_EDCProcessor_ID, String trxName)
    {
      super (ctx, UNS_EDCProcessor_ID, trxName);
      /** if (UNS_EDCProcessor_ID == 0)
        {
			setClassname (null);
			setConnectionTimeOut (0);
// 30
			setIsNeedFlush (false);
// N
			setIsSendACK (false);
// N
			setName (null);
			setReadTimeOut (0);
// 30
			setUNS_EDCProcessor_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EDCProcessor (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EDCProcessor[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Classname.
		@param Classname 
		Java Classname
	  */
	public void setClassname (String Classname)
	{
		set_Value (COLUMNNAME_Classname, Classname);
	}

	/** Get Classname.
		@return Java Classname
	  */
	public String getClassname () 
	{
		return (String)get_Value(COLUMNNAME_Classname);
	}

	/** Set Connection Time Out.
		@param ConnectionTimeOut Connection Time Out	  */
	public void setConnectionTimeOut (int ConnectionTimeOut)
	{
		set_Value (COLUMNNAME_ConnectionTimeOut, Integer.valueOf(ConnectionTimeOut));
	}

	/** Get Connection Time Out.
		@return Connection Time Out	  */
	public int getConnectionTimeOut () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ConnectionTimeOut);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Need Flush ?.
		@param IsNeedFlush Need Flush ?	  */
	public void setIsNeedFlush (boolean IsNeedFlush)
	{
		set_Value (COLUMNNAME_IsNeedFlush, Boolean.valueOf(IsNeedFlush));
	}

	/** Get Need Flush ?.
		@return Need Flush ?	  */
	public boolean isNeedFlush () 
	{
		Object oo = get_Value(COLUMNNAME_IsNeedFlush);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Send ACK ?.
		@param IsSendACK Send ACK ?	  */
	public void setIsSendACK (boolean IsSendACK)
	{
		set_Value (COLUMNNAME_IsSendACK, Boolean.valueOf(IsSendACK));
	}

	/** Get Send ACK ?.
		@return Send ACK ?	  */
	public boolean isSendACK () 
	{
		Object oo = get_Value(COLUMNNAME_IsSendACK);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Read Time Out.
		@param ReadTimeOut 
		Read Time Out
	  */
	public void setReadTimeOut (int ReadTimeOut)
	{
		set_Value (COLUMNNAME_ReadTimeOut, Integer.valueOf(ReadTimeOut));
	}

	/** Get Read Time Out.
		@return Read Time Out
	  */
	public int getReadTimeOut () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ReadTimeOut);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDC Processor.
		@param UNS_EDCProcessor_ID EDC Processor	  */
	public void setUNS_EDCProcessor_ID (int UNS_EDCProcessor_ID)
	{
		if (UNS_EDCProcessor_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessor_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessor_ID, Integer.valueOf(UNS_EDCProcessor_ID));
	}

	/** Get EDC Processor.
		@return EDC Processor	  */
	public int getUNS_EDCProcessor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCProcessor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EDCProcessor_UU.
		@param UNS_EDCProcessor_UU UNS_EDCProcessor_UU	  */
	public void setUNS_EDCProcessor_UU (String UNS_EDCProcessor_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessor_UU, UNS_EDCProcessor_UU);
	}

	/** Get UNS_EDCProcessor_UU.
		@return UNS_EDCProcessor_UU	  */
	public String getUNS_EDCProcessor_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EDCProcessor_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}