package com.uns.model;

import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

public class MUNSConfirmationFlow extends X_UNS_ConfirmationFlow {

	/**
	 * Mr Thunder
	 */
	private static final long serialVersionUID = 1895424542366360050L;
	
	MUNSConfirmationFlowLine[] m_lines = null;

	public MUNSConfirmationFlow(Properties ctx, int UNS_ConfirmationFlow_ID,
			String trxName) {
		super(ctx, UNS_ConfirmationFlow_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSConfirmationFlow(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSConfirmationFlow get(Properties ctx, int AD_Table_ID, int C_DocType_ID, String trxName){
		
		String sql = "SELECT UNS_ConfirmationFlow_ID FROM UNS_ConfirmationFlow WHERE AD_Table_ID = ? AND C_DocType_ID = ? AND isActive = 'Y'";
		int confirmFlowID = DB.getSQLValue(null, sql, AD_Table_ID, C_DocType_ID);
		if(confirmFlowID < 0)
			return null;
		
		return new MUNSConfirmationFlow(ctx, confirmFlowID, trxName);
	}
	
	public MUNSConfirmationFlowLine[] getLines() {
		
		if(m_lines != null)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		List<MUNSConfirmationFlowLine> lines = new org.compiere.model.Query(
				getCtx(), X_UNS_ConfirmationFlow_Line.Table_Name, "isActive = 'Y' AND UNS_ConfirmationFlow_ID = ?", get_TrxName())
				.setParameters(getUNS_ConfirmationFlow_ID()).setOrderBy("SeqNo ASC").list();
		
		m_lines = new MUNSConfirmationFlowLine[lines.size()];
		lines.toArray(m_lines);
		
		return m_lines;
	}
	
	
	public MUNSConfirmationFlowLine getLinebySequence(int no){
		
		m_lines = getLines();
		
		Hashtable<Integer, MUNSConfirmationFlowLine> maps = new Hashtable<Integer, MUNSConfirmationFlowLine>();

		for(int i=1; i<=m_lines.length;i++)
			maps.put(i, m_lines[i-1]);
		
		MUNSConfirmationFlowLine line = maps.get(no);
		if(line==null)
			return null;
		
		return line;
	}

}
