package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.DB;

public class MUNSUserWarningColumn extends X_UNS_UserWarningColumn {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6268671706822687875L;

	public MUNSUserWarningColumn (Properties ctx, int UNS_UserWarningColumn_ID, 
			String trxName ){
		super(ctx, UNS_UserWarningColumn_ID, trxName);
		
	}

	public MUNSUserWarningColumn (Properties ctx, ResultSet rs, String trxName ) {
		super(ctx, rs, trxName);
	}
	
	public boolean beforeSave (boolean newRecord)
	{
		if (getLine() == 0)
		{
			String sql = "SELECT COALESCE (MAX (Line), 0) + 10 FROM "
					+ " UNS_UserWarningColumn WHERE UNS_UserWarning_ID = ?";
			int current = DB.getSQLValue(get_TrxName(), sql, getUNS_UserWarning_ID());
			setLine(current);
		}
		
		if (getAD_Column_ID() > 0 )
		{
			String sqql = "SELECT SQLFrom FROM UNS_UserWarning WHERE UNS_UserWarning_ID = ?";
			String tableName = DB.getSQLValueString(get_TrxName(), sqql, getUNS_UserWarning_ID());
			
			StringBuffer SQL = new StringBuffer
			("SELECT ColumnName FROM AD_Column WHERE AD_Column_ID=?");
			String columnName = DB.getSQLValueStringEx(get_TrxName(), SQL.toString(), 
					getAD_Column_ID());
			
			setColumnSQL(tableName+"."+columnName);
			if (null == getColumnAlias())
				setColumnAlias(columnName);
		}
		
		if (is_ValueChanged(COLUMNNAME_ColumnSQL) 
				|| is_ValueChanged(COLUMNNAME_ColumnAlias)
				|| is_ValueChanged(COLUMNNAME_Description))
		{
			String sql = "UPDATE UNS_UserWarning SET IsValid = 'N' WHERE "
					+ " UNS_UserWarning_ID = ?";
			int ok = DB.executeUpdate(sql, getUNS_UserWarning_ID(), get_TrxName());
			if (ok == -1)
				return false;
		}
		
		return true;
	}
}
