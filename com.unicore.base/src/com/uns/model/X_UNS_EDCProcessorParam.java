/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_EDCProcessorParam
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EDCProcessorParam extends PO implements I_UNS_EDCProcessorParam, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181001L;

    /** Standard Constructor */
    public X_UNS_EDCProcessorParam (Properties ctx, int UNS_EDCProcessorParam_ID, String trxName)
    {
      super (ctx, UNS_EDCProcessorParam_ID, trxName);
      /** if (UNS_EDCProcessorParam_ID == 0)
        {
			setDataType (null);
// 02
			setIsHexDump (false);
// N
			setLength (0);
			setLine (0);
			setName (null);
			setParameterType (null);
// C
			setTransactionType (null);
			setUNS_EDCProcessorAction_ID (0);
			setUNS_EDCProcessorParam_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EDCProcessorParam (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EDCProcessorParam[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Hexadecimal = 01 */
	public static final String DATATYPE_Hexadecimal = "01";
	/** Numeric = 02 */
	public static final String DATATYPE_Numeric = "02";
	/** Alphanumeric = 03 */
	public static final String DATATYPE_Alphanumeric = "03";
	/** Set Data Type.
		@param DataType 
		Type of data
	  */
	public void setDataType (String DataType)
	{

		set_Value (COLUMNNAME_DataType, DataType);
	}

	/** Get Data Type.
		@return Type of data
	  */
	public String getDataType () 
	{
		return (String)get_Value(COLUMNNAME_DataType);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Hex Dump ?.
		@param IsHexDump Hex Dump ?	  */
	public void setIsHexDump (boolean IsHexDump)
	{
		set_Value (COLUMNNAME_IsHexDump, Boolean.valueOf(IsHexDump));
	}

	/** Get Hex Dump ?.
		@return Hex Dump ?	  */
	public boolean isHexDump () 
	{
		Object oo = get_Value(COLUMNNAME_IsHexDump);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Length.
		@param Length Length	  */
	public void setLength (int Length)
	{
		set_Value (COLUMNNAME_Length, Integer.valueOf(Length));
	}

	/** Get Length.
		@return Length	  */
	public int getLength () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Length);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** ParameterType AD_Reference_ID=53288 */
	public static final int PARAMETERTYPE_AD_Reference_ID=53288;
	/** Constant = C */
	public static final String PARAMETERTYPE_Constant = "C";
	/** Free = F */
	public static final String PARAMETERTYPE_Free = "F";
	/** Set Parameter Type.
		@param ParameterType Parameter Type	  */
	public void setParameterType (String ParameterType)
	{

		set_Value (COLUMNNAME_ParameterType, ParameterType);
	}

	/** Get Parameter Type.
		@return Parameter Type	  */
	public String getParameterType () 
	{
		return (String)get_Value(COLUMNNAME_ParameterType);
	}

	/** Set Parameter Value.
		@param ParameterValue Parameter Value	  */
	public void setParameterValue (String ParameterValue)
	{
		set_Value (COLUMNNAME_ParameterValue, ParameterValue);
	}

	/** Get Parameter Value.
		@return Parameter Value	  */
	public String getParameterValue () 
	{
		return (String)get_Value(COLUMNNAME_ParameterValue);
	}

	/** Batch No = 01 */
	public static final String SOURCEVALUE_BatchNo = "01";
	/** Invoice No = 02 */
	public static final String SOURCEVALUE_InvoiceNo = "02";
	/** Trace No = 03 */
	public static final String SOURCEVALUE_TraceNo = "03";
	/** Transaction Amount = 04 */
	public static final String SOURCEVALUE_TransactionAmount = "04";
	/** Other Amount = 05 */
	public static final String SOURCEVALUE_OtherAmount = "05";
	/** Cardholder Name = 06 */
	public static final String SOURCEVALUE_CardholderName = "06";
	/** Original Invoice No = 07 */
	public static final String SOURCEVALUE_OriginalInvoiceNo = "07";
	/** Currency = 08 */
	public static final String SOURCEVALUE_Currency = "08";
	/** Outlate Inv No = 09 */
	public static final String SOURCEVALUE_OutlateInvNo = "09";
	/** Card Type = 10 */
	public static final String SOURCEVALUE_CardType = "10";
	/** Account No = 11 */
	public static final String SOURCEVALUE_AccountNo = "11";
	/** Set Source Value.
		@param SourceValue Source Value	  */
	public void setSourceValue (String SourceValue)
	{

		set_Value (COLUMNNAME_SourceValue, SourceValue);
	}

	/** Get Source Value.
		@return Source Value	  */
	public String getSourceValue () 
	{
		return (String)get_Value(COLUMNNAME_SourceValue);
	}

	/** Request = 01 */
	public static final String TRANSACTIONTYPE_Request = "01";
	/** Response = 02 */
	public static final String TRANSACTIONTYPE_Response = "02";
	/** Connection = 03 */
	public static final String TRANSACTIONTYPE_Connection = "03";
	/** Set Transaction Type.
		@param TransactionType Transaction Type	  */
	public void setTransactionType (String TransactionType)
	{

		set_Value (COLUMNNAME_TransactionType, TransactionType);
	}

	/** Get Transaction Type.
		@return Transaction Type	  */
	public String getTransactionType () 
	{
		return (String)get_Value(COLUMNNAME_TransactionType);
	}

	public com.uns.model.I_UNS_EDCProcessorAction getUNS_EDCProcessorAction() throws RuntimeException
    {
		return (com.uns.model.I_UNS_EDCProcessorAction)MTable.get(getCtx(), com.uns.model.I_UNS_EDCProcessorAction.Table_Name)
			.getPO(getUNS_EDCProcessorAction_ID(), get_TrxName());	}

	/** Set EDC Processor Action.
		@param UNS_EDCProcessorAction_ID EDC Processor Action	  */
	public void setUNS_EDCProcessorAction_ID (int UNS_EDCProcessorAction_ID)
	{
		if (UNS_EDCProcessorAction_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorAction_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorAction_ID, Integer.valueOf(UNS_EDCProcessorAction_ID));
	}

	/** Get EDC Processor Action.
		@return EDC Processor Action	  */
	public int getUNS_EDCProcessorAction_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCProcessorAction_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDC Processor Parameter.
		@param UNS_EDCProcessorParam_ID EDC Processor Parameter	  */
	public void setUNS_EDCProcessorParam_ID (int UNS_EDCProcessorParam_ID)
	{
		if (UNS_EDCProcessorParam_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorParam_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorParam_ID, Integer.valueOf(UNS_EDCProcessorParam_ID));
	}

	/** Get EDC Processor Parameter.
		@return EDC Processor Parameter	  */
	public int getUNS_EDCProcessorParam_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCProcessorParam_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EDCProcessorParam_UU.
		@param UNS_EDCProcessorParam_UU UNS_EDCProcessorParam_UU	  */
	public void setUNS_EDCProcessorParam_UU (String UNS_EDCProcessorParam_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EDCProcessorParam_UU, UNS_EDCProcessorParam_UU);
	}

	/** Get UNS_EDCProcessorParam_UU.
		@return UNS_EDCProcessorParam_UU	  */
	public String getUNS_EDCProcessorParam_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EDCProcessorParam_UU);
	}
}