/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.uns.model.I_UNS_Charge_RS;
import com.uns.model.MUNSChargeRS;

/**
 * @author root
 *
 */
public class CalloutSettlement implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutSettlement() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		if(mField.getColumnName().equals(MUNSChargeRS.COLUMNNAME_Reference_ID))
			return onReferenceCange(ctx, WindowNo, mTab, mField, value, oldValue);
		return null;
	}
	
	private String onReferenceCange(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		I_UNS_Charge_RS settlement = GridTabWrapper.create(mTab, I_UNS_Charge_RS.class);
		if(null == value)
		{
			settlement.setC_BPartner_ID(-1);
			settlement.setDateTrx(null);
			return null;
		}
		
		MUNSChargeRS cs = (MUNSChargeRS) settlement.getReference();
		settlement.setC_BPartner_ID(cs.getC_BPartner_ID());
		return null;
	}
}
