/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MAsset;
import org.compiere.model.MAssetGroup;
import org.compiere.util.DB;

/**
 * @author Burhani Adam
 *
 */
public class CalloutAsset implements IColumnCallout{

	/**
	 * 
	 */
	public CalloutAsset() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (mTab.getTableName().equals("A_Asset") 
				&& mField.getColumnName().equals(MAsset.COLUMNNAME_M_Product_ID))
			return onAssetProductSelected(ctx, WindowNo, mTab, mField, value, oldValue);
		if(mField.getColumnName().equals(MAssetGroup.COLUMNNAME_TaxGroupType))
			return onTaxGroupType(ctx, WindowNo, mTab, mField, value, oldValue);
		return null;
	}
	
	private String onAssetProductSelected (
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, 
			Object value, Object oldValue)
	{
		if (value == null)
			return null;
		String sql = "SELECT A_Asset_Group_ID FROM M_Product_Category WHERE "
				+ " M_Product_Category_ID = (SELECT M_Product_Category_Parent_ID "
				+ "FROM M_Product WHERE M_Product_ID = ?)";
		Integer assetGroupID = DB.getSQLValue(null, sql, (Integer) value);
		if (assetGroupID == 0)
			assetGroupID = null;
		
		mTab.setValue(MAsset.COLUMNNAME_A_Asset_Group_ID, assetGroupID);
		
		return null;
	}
	
	private String onTaxGroupType(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null)
			return null;
		
		String val = (String) value;
		
		if(val.equals(MAssetGroup.TAXGROUPTYPE_HartaBerwujud))
		{
			mTab.setValue(MAssetGroup.COLUMNNAME_IsDepreciated, true);
			mTab.setValue(MAssetGroup.COLUMNNAME_IsFixedAsset, true);
			mTab.setValue(MAssetGroup.COLUMNNAME_IsAmortization, false);
		}
		else if(val.equals(MAssetGroup.TAXGROUPTYPE_HartaTakBerwujud))
		{
			mTab.setValue(MAssetGroup.COLUMNNAME_IsDepreciated, false);
			mTab.setValue(MAssetGroup.COLUMNNAME_IsFixedAsset, false);
			mTab.setValue(MAssetGroup.COLUMNNAME_IsAmortization, true);
		}

		return null;
	}
}