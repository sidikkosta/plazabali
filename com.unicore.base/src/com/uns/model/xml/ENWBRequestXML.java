/**
 * 
 */
package com.uns.model.xml;

import java.io.ByteArrayInputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author menjangan
 *
 */
@XmlRootElement (name = "WeighbridgeRequest")
public class ENWBRequestXML {

	@XmlElement (name = "partner_key")
	private String m_key;

	@XmlElement (name = "timeout")
	private long m_timeOut;

	@XmlElement (name = "calculate_deduction")
	private String m_caluclateDeduction;

	@XmlElement (name = "ffb_condition")
	private String m_ffbCondition;
	
	@XmlElement (name = "wateray")
	private String m_waterway;
	
	/**
	 * 
	 */
	public ENWBRequestXML() {
		super ();
	}

	public ENWBRequestXML (String partnerKey, long timeout, boolean isEmptyVehicle, String ffbCondition, boolean isWaterway) {
		this ();
		m_key = partnerKey;
		m_timeOut = timeout;
		setCalculateDeduction(isEmptyVehicle);
		m_ffbCondition = ffbCondition;
		setWaterway(isWaterway);
	}
	
	public void setPartnerKey (String partnerKey) {
		m_key = partnerKey;
	}
	
	public String getPartnerKey () {
		return m_key;
	}
	
	public void setTimeout (long timeout) {
		m_timeOut = timeout;
	}
	
	public long getTimeOut () {
		return m_timeOut;
	}
	
	public void setCalculateDeduction (boolean calculateDeduction) {
		m_caluclateDeduction = calculateDeduction ? "Y" : "N";
	}
	
	public boolean isCalculateDeduction () {
		return "Y".equals(m_caluclateDeduction);
	}
	
	public void setWaterway (boolean isWaterway) {
		m_waterway = isWaterway ? "Y" : "N";
	}
	
	public boolean isWaterway () {
		return "Y".equals(m_waterway);
	}
	
	public void setFFBCondition (String ffbCondition) {
		m_ffbCondition = ffbCondition;
	}
	
	public String getFFBCondition () {
		return m_ffbCondition;
	}
	
	public static ENWBRequestXML newInstance (byte[] databyte)  throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ENWBRequestXML.class);
		Unmarshaller encoder = context.createUnmarshaller();
		ByteArrayInputStream instream = new ByteArrayInputStream(databyte);
		ENWBRequestXML model = (ENWBRequestXML) encoder.unmarshal(instream);
		return model;
	}
	
	public static ENWBRequestXML newInstance (String xml)  throws JAXBException {
		return newInstance(xml.getBytes());
	} 
}
