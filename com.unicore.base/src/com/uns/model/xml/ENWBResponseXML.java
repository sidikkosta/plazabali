package com.uns.model.xml;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "WeighbridgeValue")
public class ENWBResponseXML {

	@XmlElement (name = "value")
	private BigDecimal m_value;
	
	public ENWBResponseXML () {
		super ();
	}
	
	public void setValue (BigDecimal value) {
		m_value = value;
	}
	
	public BigDecimal getValue () {
		return m_value;
	}
	
	public static ENWBResponseXML newInstance (byte[] bytes)  throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ENWBResponseXML.class);
		Unmarshaller unmarsh = context.createUnmarshaller();
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		ENWBResponseXML response = (ENWBResponseXML) unmarsh.unmarshal(in);
		return response;
	}
	
	public static ENWBResponseXML newInstance (String response) throws JAXBException {
		return newInstance(response.getBytes());
	}
}
