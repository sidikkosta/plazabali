/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_DailyBankEDCBalance
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_DailyBankEDCBalance 
{

    /** TableName=UNS_DailyBankEDCBalance */
    public static final String Table_Name = "UNS_DailyBankEDCBalance";

    /** AD_Table_ID=1000492 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name DifferenceAmt */
    public static final String COLUMNNAME_DifferenceAmt = "DifferenceAmt";

	/** Set Difference.
	  * Difference Amount
	  */
	public void setDifferenceAmt (BigDecimal DifferenceAmt);

	/** Get Difference.
	  * Difference Amount
	  */
	public BigDecimal getDifferenceAmt();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name TotalARAmount */
    public static final String COLUMNNAME_TotalARAmount = "TotalARAmount";

	/** Set Total AR Amount	  */
	public void setTotalARAmount (BigDecimal TotalARAmount);

	/** Get Total AR Amount	  */
	public BigDecimal getTotalARAmount();

    /** Column name TotalDisbursedAmt */
    public static final String COLUMNNAME_TotalDisbursedAmt = "TotalDisbursedAmt";

	/** Set Total Disbursement Amount	  */
	public void setTotalDisbursedAmt (BigDecimal TotalDisbursedAmt);

	/** Get Total Disbursement Amount	  */
	public BigDecimal getTotalDisbursedAmt();

    /** Column name TotalOutstandingAmt */
    public static final String COLUMNNAME_TotalOutstandingAmt = "TotalOutstandingAmt";

	/** Set Total Outstanding Amount	  */
	public void setTotalOutstandingAmt (BigDecimal TotalOutstandingAmt);

	/** Get Total Outstanding Amount	  */
	public BigDecimal getTotalOutstandingAmt();

    /** Column name UNS_BankEDCBalance_ID */
    public static final String COLUMNNAME_UNS_BankEDCBalance_ID = "UNS_BankEDCBalance_ID";

	/** Set Bank EDC Balance	  */
	public void setUNS_BankEDCBalance_ID (int UNS_BankEDCBalance_ID);

	/** Get Bank EDC Balance	  */
	public int getUNS_BankEDCBalance_ID();

	public I_UNS_BankEDCBalance getUNS_BankEDCBalance() throws RuntimeException;

    /** Column name UNS_DailyBankEDCBalance_ID */
    public static final String COLUMNNAME_UNS_DailyBankEDCBalance_ID = "UNS_DailyBankEDCBalance_ID";

	/** Set Daily Bank EDC Balance	  */
	public void setUNS_DailyBankEDCBalance_ID (int UNS_DailyBankEDCBalance_ID);

	/** Get Daily Bank EDC Balance	  */
	public int getUNS_DailyBankEDCBalance_ID();

    /** Column name UNS_DailyBankEDCBalance_UU */
    public static final String COLUMNNAME_UNS_DailyBankEDCBalance_UU = "UNS_DailyBankEDCBalance_UU";

	/** Set UNS_DailyBankEDCBalance_UU	  */
	public void setUNS_DailyBankEDCBalance_UU (String UNS_DailyBankEDCBalance_UU);

	/** Get UNS_DailyBankEDCBalance_UU	  */
	public String getUNS_DailyBankEDCBalance_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
