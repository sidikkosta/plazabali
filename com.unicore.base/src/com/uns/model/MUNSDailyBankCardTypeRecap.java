/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSDailyBankCardTypeRecap extends X_UNS_DailyBankCardTypeRecap {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8795501757553650721L;

	/**
	 * @param ctx
	 * @param UNS_DailyBankCardTypeRecap_ID
	 * @param trxName
	 */
	public MUNSDailyBankCardTypeRecap(Properties ctx,
			int UNS_DailyBankCardTypeRecap_ID, String trxName) {
		super(ctx, UNS_DailyBankCardTypeRecap_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDailyBankCardTypeRecap(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSDailyBankCardTypeRecap getCreate(Properties ctx, int orgID, int edcID, 
			int cardTypeID, Timestamp date, String trxName)
	{
		MUNSBankCardTypeRecap parent = MUNSBankCardTypeRecap.getCreate(ctx, orgID, edcID, cardTypeID, trxName);
		
		String whereClause = "UNS_BankCardTypeRecap_ID = ? AND UNS_CardType_ID = ? AND DateTrx = ?";
		
		MUNSDailyBankCardTypeRecap result = Query.get(ctx, UNSDefaultModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(parent.get_ID(), cardTypeID, date)
					.firstOnly();
		
		if(result == null)
		{
			result = new MUNSDailyBankCardTypeRecap(ctx, 0, trxName);
			result.setAD_Org_ID(parent.getAD_Org_ID());
			result.setUNS_BankCardTypeRecap_ID(parent.get_ID());
			result.setDateTrx(date);
			result.setUNS_CardType_ID(cardTypeID);
			result.setTotalARAmount(Env.ZERO);
			result.setTotalDisbursedAmt(Env.ZERO);
			result.setTotalOutstandingAmt(Env.ZERO);
			result.saveEx();
		}
		
		return result;
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		String sql = "UPDATE UNS_BankCardTypeRecap p SET"
				+ " TotalARAmount ="
				+ " (SELECT SUM(t.TotalARAmount) FROM UNS_DailyBankCardTypeRecap t"
				+ " WHERE t.UNS_BankCardTypeRecap_ID = p.UNS_BankCardTypeRecap_ID),"
				+ " TotalDisbursedAmt ="
				+ " (SELECT SUM(t.TotalDisbursedAmt) FROM UNS_DailyBankCardTypeRecap t"
				+ " WHERE t.UNS_BankCardTypeRecap_ID = p.UNS_BankCardTypeRecap_ID),"
				+ " TotalOutstandingAmt ="
				+ " (SELECT SUM(t.TotalOutstandingAmt) FROM UNS_DailyBankCardTypeRecap t"
				+ " WHERE t.UNS_BankCardTypeRecap_ID = p.UNS_BankCardTypeRecap_ID),"
				+ " DifferenceAmt ="
				+ " (SELECT SUM(t.DifferenceAmt) FROM UNS_DailyBankCardTypeRecap t"
				+ " WHERE t.UNS_BankCardTypeRecap_ID = p.UNS_BankCardTypeRecap_ID)"
				+ " WHERE p.UNS_BankCardTypeRecap_ID = ?";
		
		return DB.executeUpdate(sql, getUNS_BankCardTypeRecap_ID(), get_TrxName()) >= 0
				&& MUNSBankCardTypeRecap.upHeader(get_TrxName(), getUNS_BankCardTypeRecap().getUNS_BankEDCBalance_ID()) >= 0;
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		setTotalOutstandingAmt(getTotalARAmount().subtract(getTotalDisbursedAmt().subtract(getDifferenceAmt())));
		
		return true;
	}
}