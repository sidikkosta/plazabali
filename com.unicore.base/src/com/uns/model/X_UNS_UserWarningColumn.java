/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_UserWarningColumn
 *  @author iDempiere (generated) 
 *  @version Release 3.0 - $Id$ */
public class X_UNS_UserWarningColumn extends PO implements I_UNS_UserWarningColumn, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170726L;

    /** Standard Constructor */
    public X_UNS_UserWarningColumn (Properties ctx, int UNS_UserWarningColumn_ID, String trxName)
    {
      super (ctx, UNS_UserWarningColumn_ID, trxName);
      /** if (UNS_UserWarningColumn_ID == 0)
        {
			setDescription (null);
			setLine (0);
// 0
			setUNS_UserWarning_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UserWarningColumn (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UserWarningColumn[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Column getAD_Column() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Column)MTable.get(getCtx(), org.compiere.model.I_AD_Column.Table_Name)
			.getPO(getAD_Column_ID(), get_TrxName());	}

	/** Set Column.
		@param AD_Column_ID 
		Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID)
	{
		if (AD_Column_ID < 1) 
			set_Value (COLUMNNAME_AD_Column_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Column_ID, Integer.valueOf(AD_Column_ID));
	}

	/** Get Column.
		@return Column in the table
	  */
	public int getAD_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set ColumnAlias.
		@param ColumnAlias ColumnAlias	  */
	public void setColumnAlias (String ColumnAlias)
	{
		set_Value (COLUMNNAME_ColumnAlias, ColumnAlias);
	}

	/** Get ColumnAlias.
		@return ColumnAlias	  */
	public String getColumnAlias () 
	{
		return (String)get_Value(COLUMNNAME_ColumnAlias);
	}

	/** Set Column SQL.
		@param ColumnSQL 
		Virtual Column (r/o)
	  */
	public void setColumnSQL (String ColumnSQL)
	{
		set_Value (COLUMNNAME_ColumnSQL, ColumnSQL);
	}

	/** Get Column SQL.
		@return Virtual Column (r/o)
	  */
	public String getColumnSQL () 
	{
		return (String)get_Value(COLUMNNAME_ColumnSQL);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_UserWarning getUNS_UserWarning() throws RuntimeException
    {
		return (com.uns.model.I_UNS_UserWarning)MTable.get(getCtx(), com.uns.model.I_UNS_UserWarning.Table_Name)
			.getPO(getUNS_UserWarning_ID(), get_TrxName());	}

	/** Set UNS_UserWarning_ID.
		@param UNS_UserWarning_ID UNS_UserWarning_ID	  */
	public void setUNS_UserWarning_ID (int UNS_UserWarning_ID)
	{
		if (UNS_UserWarning_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarning_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarning_ID, Integer.valueOf(UNS_UserWarning_ID));
	}

	/** Get UNS_UserWarning_ID.
		@return UNS_UserWarning_ID	  */
	public int getUNS_UserWarning_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UserWarning_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UserWarningColumn_ID.
		@param UNS_UserWarningColumn_ID UNS_UserWarningColumn_ID	  */
	public void setUNS_UserWarningColumn_ID (int UNS_UserWarningColumn_ID)
	{
		if (UNS_UserWarningColumn_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarningColumn_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UserWarningColumn_ID, Integer.valueOf(UNS_UserWarningColumn_ID));
	}

	/** Get UNS_UserWarningColumn_ID.
		@return UNS_UserWarningColumn_ID	  */
	public int getUNS_UserWarningColumn_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UserWarningColumn_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UserWarningColumn_UU.
		@param UNS_UserWarningColumn_UU UNS_UserWarningColumn_UU	  */
	public void setUNS_UserWarningColumn_UU (String UNS_UserWarningColumn_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UserWarningColumn_UU, UNS_UserWarningColumn_UU);
	}

	/** Get UNS_UserWarningColumn_UU.
		@return UNS_UserWarningColumn_UU	  */
	public String getUNS_UserWarningColumn_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UserWarningColumn_UU);
	}
}