/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_ConfirmationFlow
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ConfirmationFlow extends PO implements I_UNS_ConfirmationFlow, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171011L;

    /** Standard Constructor */
    public X_UNS_ConfirmationFlow (Properties ctx, int UNS_ConfirmationFlow_ID, String trxName)
    {
      super (ctx, UNS_ConfirmationFlow_ID, trxName);
      /** if (UNS_ConfirmationFlow_ID == 0)
        {
			setAD_Table_ID (0);
			setC_DocType_ID (0);
			setName (null);
			setUNS_ConfirmationFlow_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ConfirmationFlow (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ConfirmationFlow[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Table getAD_Table() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Table)MTable.get(getCtx(), org.compiere.model.I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set UNS_ConfirmationFlow_ID.
		@param UNS_ConfirmationFlow_ID UNS_ConfirmationFlow_ID	  */
	public void setUNS_ConfirmationFlow_ID (int UNS_ConfirmationFlow_ID)
	{
		if (UNS_ConfirmationFlow_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_ID, Integer.valueOf(UNS_ConfirmationFlow_ID));
	}

	/** Get UNS_ConfirmationFlow_ID.
		@return UNS_ConfirmationFlow_ID	  */
	public int getUNS_ConfirmationFlow_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ConfirmationFlow_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ConfirmationFlow_UU.
		@param UNS_ConfirmationFlow_UU UNS_ConfirmationFlow_UU	  */
	public void setUNS_ConfirmationFlow_UU (String UNS_ConfirmationFlow_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ConfirmationFlow_UU, UNS_ConfirmationFlow_UU);
	}

	/** Get UNS_ConfirmationFlow_UU.
		@return UNS_ConfirmationFlow_UU	  */
	public String getUNS_ConfirmationFlow_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ConfirmationFlow_UU);
	}
}