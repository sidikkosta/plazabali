/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSEDCProcessorAction extends X_UNS_EDCProcessorAction 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6399096920715264875L;
	private MUNSEDCProcessor m_parent = null;
	private MUNSEDCProcessorParam[] m_request = null;
	private MUNSEDCProcessorParam[] m_response = null;
	private MUNSEDCProcessorParam[] m_connParam = null;
	public static final String ACTION_SALE = "Sale";
	public static final String ACTION_VOID = "Void";
	
	/**
	 * @param ctx
	 * @param UNS_EDCProcessorAction_ID
	 * @param trxName
	 */
	public MUNSEDCProcessorAction(Properties ctx,
			int UNS_EDCProcessorAction_ID, String trxName) 
	{
		super(ctx, UNS_EDCProcessorAction_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEDCProcessorAction(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public MUNSEDCProcessor getParent ()
	{
		if (m_parent != null)
			return m_parent;
		
		m_parent = new MUNSEDCProcessor(getCtx(), getUNS_EDCProcessor_ID(), get_TrxName());
		return m_parent;
	}
	
	public MUNSEDCProcessorParam[] getResponseField (boolean requery)
	{
		if (m_response != null && !requery)
		{
			set_TrxName(m_response, get_TrxName());
			return m_response;
		}
		
		List<MUNSEDCProcessorParam> list = Query.get(
				getCtx(), UNSDefaultModelFactory.EXTENSION_ID, 
				MUNSEDCProcessorParam.Table_Name, 
				MUNSEDCProcessorParam.COLUMNNAME_UNS_EDCProcessorAction_ID + " =? AND TransactionType = ?", 
				get_TrxName()).setParameters(get_ID(), MUNSEDCProcessorParam.TRANSACTIONTYPE_Response).
				setOrderBy(MUNSEDCProcessorParam.COLUMNNAME_Line).list();
		m_response = new MUNSEDCProcessorParam[list.size()];
		list.toArray(m_response);
		return m_response;
	}
	
	
	public MUNSEDCProcessorParam[] getRequestFields (boolean requery)
	{
		if (m_request != null && !requery)
		{
			set_TrxName(m_request, get_TrxName());
			return m_request;
		}
		
		List<MUNSEDCProcessorParam> list = Query.get(
				getCtx(), UNSDefaultModelFactory.EXTENSION_ID, 
				MUNSEDCProcessorParam.Table_Name, 
				MUNSEDCProcessorParam.COLUMNNAME_UNS_EDCProcessorAction_ID + " =? AND TransactionType = ?", 
				get_TrxName()).setParameters(get_ID(), MUNSEDCProcessorParam.TRANSACTIONTYPE_Request).
				setOrderBy(MUNSEDCProcessorParam.COLUMNNAME_Line).list();
		m_request = new MUNSEDCProcessorParam[list.size()];
		list.toArray(m_request);
		return m_request;
	}
	
	public MUNSEDCProcessorParam[] getConnectionParams (boolean requery)
	{
		if (m_connParam != null && !requery)
		{
			set_TrxName(m_connParam, get_TrxName());
			return m_connParam;
		}
		
		List<MUNSEDCProcessorParam> list = Query.get(
				getCtx(), UNSDefaultModelFactory.EXTENSION_ID, 
				MUNSEDCProcessorParam.Table_Name, 
				MUNSEDCProcessorParam.COLUMNNAME_UNS_EDCProcessorAction_ID + " =? AND TransactionType = ?", 
				get_TrxName()).setParameters(get_ID(), "03").
				setOrderBy(MUNSEDCProcessorParam.COLUMNNAME_Line).list();
		m_connParam = new MUNSEDCProcessorParam[list.size()];
		list.toArray(m_connParam);
		return m_connParam;
	}
}
