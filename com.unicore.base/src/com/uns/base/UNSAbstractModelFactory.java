/**
 * 
 */
package com.uns.base;

import java.lang.reflect.Constructor;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import jxl.Sheet;

import org.adempiere.base.DefaultModelFactory;
import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.CCache;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.importer.AbstractDataImporter;
import com.uns.importer.ImporterValidation;

/**
 * @author AzHaidar
 *
 */
public abstract class  UNSAbstractModelFactory 
		extends DefaultModelFactory 
		implements IModelFactory 
{

	protected static CCache<String,Class<?>> s_classCache = new CCache<String,Class<?>>(null, "PO_Class", 20, false);
	private final static CLogger s_log = CLogger.getCLogger(DefaultModelFactory.class);

	/**
	 * 
	 */
	public UNSAbstractModelFactory() {
	}
	
	/* (non-Javadoc)
	 * @see org.adempiere.base.IModelFactory#getClass(java.lang.String)
	 */
	@Override
	public Class<?> getClass(String tableName) 
	{
		if (tableName == null || tableName.endsWith("_Trl"))
			return null;

		String className = tableName;
		int index = className.indexOf('_');
		if (index > 0)
		{
			if (index < 3)		//	AD_, A_
				 className = className.substring(index+1);
		}
		//	Remove underlines
		className = Util.replace(className, "_", "");
		
		// First, try to get it from unicore.base.model for the customized org.adempiere.model or 
		// org.compiere.model M model type.
		Class<?> clazz = getClass(tableName, "M" + className, "com.untacore.base.model");
		if (clazz != null)
			return clazz;
		
		// Then try to get it from com.unicore.model.feature for the M model type.
		clazz = getClass(tableName, "M" + className, "com.untacore.model");
		if (clazz != null)
			return clazz;
		
		// It is possible doesn't have M model, then try to get the default from com.unicore.model for the X model type.
		clazz = getClass(tableName, "X_" + tableName, "com.untacore.model");
		if (clazz != null)
			return clazz;

		clazz = getClass(tableName, "M" + className, "com.unicore.base.model");
		if (clazz != null)
			return clazz;
		
		// Then try to get it from com.unicore.model.feature for the M model type.
		clazz = getClass(tableName, "M" + className, "com.unicore.model");
		if (clazz != null)
			return clazz;
		
		// It is possible doesn't have M model, then try to get the default from com.unicore.model for the X model type.
		clazz = getClass(tableName, "X_" + tableName, "com.unicore.model");
		if (clazz != null)
			return clazz;

		// We old untasoft package model, that is com.uns, try to get it from uns.base.model for the customized 
		// org.adempiere.model or org.compiere.model M model type.
		clazz = getClass(tableName, "M" + className, "com.uns.base.model");
		if (clazz != null)
			return clazz;
		
		// Then try to get it from com.uns.model.feature for the M model type.
		clazz = getClass(tableName, "M" + className, "com.uns.model");
		if (clazz != null)
			return clazz;
		
		// It is possible doesn't have M model, then try to get the default from com.uns.model for the X model type.
		clazz = getClass(tableName, "X_" + tableName, "com.uns.model");
		if (clazz != null)
			return clazz;

		clazz = getClass(tableName, "M" + className, "com.untaerp.model");
		if (clazz != null)
			return clazz;
		
		// It is possible doesn't have M model, then try to get the default from com.uns.model for the X model type.
		clazz = getClass(tableName, "X_" + tableName, "com.untaerp.model");
		if (clazz != null)
			return clazz;

		// At last, try to get it from the adempiere default model factory.
		DefaultModelFactory defaultFactory = new DefaultModelFactory();
		clazz = defaultFactory.getClass(tableName);
		if (clazz != null)
		{
			s_classCache.put(tableName, clazz);
			return clazz;
		}

		return null;
	}

	/**
	 * 
	 * @param tableName
	 * @param packageName
	 * @return
	 */
	public Class<?> getClass(String tableName, String className, String packageName) 
	{
		// First, try to get it from uns.base.model for the customized org.adempiere.model or 
		// org.compiere.model M model type.
		StringBuffer name = new StringBuffer(packageName).append(".").append(className);

		Class<?> cache = s_classCache.get(name);
		if (cache != null)
		{
			//Object.class indicate no generated PO class for tableName
			if (cache.equals(Object.class))
				return null;
			else
				return cache;
		}
		Class<?> clazz = getPOclass(name.toString(), tableName);
		if (clazz != null)
		{
			s_classCache.put(name.toString(), clazz);
			return clazz;
		}
		
		//Object.class to indicate no PO class for tableName
		//s_classCache.put(tableName, Object.class);
		return null;
	}

	/**
	 * Get PO class
	 * @param className fully qualified class name
	 * @param tableName Optional. If specified, the loaded class will be validated for that table name
	 * @return class or null
	 */
	protected Class<?> getPOclass (String className, String tableName)
	{
		try
		{
			Class<?> clazz = getInternalClass(className);
			// Validate if the class is for specified tableName
			if (tableName != null)
			{
				String classTableName = clazz.getField("Table_Name").get(null).toString();
				if (!tableName.equals(classTableName))
				{
					s_log.finest("Invalid class for table: " + className+" (tableName="+tableName+", classTableName="+classTableName+")");
					return null;
				}
			}
			//	Make sure that it is a PO class
			Class<?> superClazz = clazz.getSuperclass();
			while (superClazz != null)
			{
				if (superClazz == PO.class)
				{
					s_log.fine("Use: " + className);
					return clazz;
				}
				superClazz = superClazz.getSuperclass();
			}
		}
		catch (Exception e)
		{
		}
		s_log.finest("Not found: " + className);
		return null;
	}	//	getPOclass

	@Override
	public PO getPO (String tableName, int Record_ID, String trxName) 
	{
		Class<?> clazz = getClass(tableName);
		if (clazz == null)
		{
			clazz = super.getClass(tableName);
			if (clazz == null)
				return null;
		}

		boolean errorLogged = false;
		try
		{
			Constructor<?> constructor = null;
			try
			{
				constructor = clazz.getDeclaredConstructor(new Class[]{Properties.class, int.class, String.class});
			}
			catch (Exception e)
			{
				String msg = e.getMessage();
				if (msg == null)
					msg = e.toString();
				s_log.warning("No transaction Constructor for " + clazz + " (" + msg + ")");
			}

			PO po = (PO)constructor.newInstance(new Object[] {Env.getCtx(), new Integer(Record_ID), trxName});
			return po;
		}
		catch (Exception e)
		{
			if (e.getCause() != null)
			{
				Throwable t = e.getCause();
				s_log.log(Level.SEVERE, "(id) - Table=" + tableName + ",Class=" + clazz, t);
				errorLogged = true;
				if (t instanceof Exception)
					s_log.saveError("Error", (Exception)e.getCause());
				else
					s_log.saveError("Error", "Table=" + tableName + ",Class=" + clazz);
			}
			else
			{
				s_log.log(Level.SEVERE, "(id) - Table=" + tableName + ",Class=" + clazz, e);
				errorLogged = true;
				s_log.saveError("Error", "Table=" + tableName + ",Class=" + clazz);
			}
		}
		if (!errorLogged)
			s_log.log(Level.SEVERE, "(id) - Not found - Table=" + tableName
				+ ", Record_ID=" + Record_ID);
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		Class<?> clazz = getClass(tableName);
		if (clazz == null)
		{
			clazz = super.getClass(tableName);
			if (clazz == null)
				return null;
		}

		boolean errorLogged = false;
		try
		{
			Constructor<?> constructor = clazz.getDeclaredConstructor(new Class[]{Properties.class, ResultSet.class, String.class});
			PO po = (PO)constructor.newInstance(new Object[] {Env.getCtx(), rs, trxName});
			return po;
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, "(rs) - Table=" + tableName + ",Class=" + clazz, e);
			errorLogged = true;
			s_log.saveError("Error", "Table=" + tableName + ",Class=" + clazz);
		}
		if (!errorLogged)
			s_log.log(Level.SEVERE, "(rs) - Not found - Table=" + tableName);
		return null;
	}
	
	/**
	 * To make sure it is also calling class from the implemented class loader (classpath).
	 * @param className
	 * @return
	 * @throws ClassNotFoundException
	 */
	protected abstract Class<?> getInternalClass(String className) throws ClassNotFoundException;
	
	/**
	 * 
	 * @param fullyQualifiedClassName
	 * @param sheet
	 * @param trxName
	 * @return
	 */
	public AbstractDataImporter getImporter(String fullyQualifiedClassName, Sheet sheet, String trxName)
	{
		/*
		AbstractDataImporter importer = null;
		Class<?> clazz = null; 
		try {
			clazz = getInternalClass(fullyQualifiedClassName);
		}
		catch (ClassNotFoundException ex)
		{
			s_log.log(Level.SEVERE, "Could not find class " + fullyQualifiedClassName, ex);
			s_log.saveError("Error", "Could not find class " + fullyQualifiedClassName);
			return null;
		}
		try {
			Constructor<?> constructor = clazz.getDeclaredConstructor(new Class[]{Properties.class, Sheet.class, String.class});
			importer = (AbstractDataImporter) constructor.newInstance(new Object[] {Env.getCtx(), sheet, trxName});
		}
		catch (Exception ex)
		{
			s_log.log(Level.SEVERE, "Could instantiate class of " + fullyQualifiedClassName, ex);
			s_log.saveError("Error", "Could instantiate class of " + fullyQualifiedClassName);
		}		
		return importer;
		*/
		return (AbstractDataImporter) getInstanceOf(
				fullyQualifiedClassName, new Object[] {Env.getCtx(), sheet, trxName});
	}
	
	/**
	 * Get the instance of Importer Validation class.
	 * @param fullyQualifiedClassName
	 * @param ctx
	 * @param trxName
	 * @return null if not found.
	 */
	public ImporterValidation getImporterValidation(String fullyQualifiedClassName, Properties ctx, Sheet sheet, String trxName)
	{
		return (ImporterValidation) getInstanceOf(fullyQualifiedClassName, new Object[] {ctx, sheet, trxName});
	}
	
	/**
	 * Get the instance of a class of the given fully qualified class name with spefied parameters.
	 * @param fullyQualifiedClassName
	 * @param params
	 * @return the instance of the expected class, 
	 * 		   or null if this class loader cannot find the class in its class path.
	 */
	public Object getInstanceOf(String fullyQualifiedClassName, Object[] params)
	{
  		Object theInstance = null;
		Class<?> clazz = null; 
		try {
			clazz = getInternalClass(fullyQualifiedClassName);
		}
		catch (ClassNotFoundException ex)
		{
			s_log.log(Level.SEVERE, "Could not find class " + fullyQualifiedClassName, ex);
			s_log.saveError("Error", "Could not find class " + fullyQualifiedClassName);
			return null;
		}
		try {
			Constructor<?> constructor = clazz.getDeclaredConstructor(new Class[]{Properties.class, Sheet.class, String.class});
			theInstance = constructor.newInstance(params);
		}
		catch (Exception ex)
		{
			s_log.log(Level.SEVERE, "Could not instantiate class of " + fullyQualifiedClassName, ex);
			s_log.saveError("Error", "Could not instantiate class of " + fullyQualifiedClassName);
		}
		
		return theInstance;
	}
}
