/**
 * 
 */
package com.uns.base.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Properties;

import org.compiere.model.MDocType;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MWarehouse;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.util.AutoCompletion;

/**
 * @author AzHaidar
 *
 */
public class MInventory extends org.compiere.model.MInventory implements AutoCompletion
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3235252200692492286L;

	/**
	 * 
	 */
	public MInventory(Properties ctx, int M_Inventory_ID, String trxName) {
		super(ctx, M_Inventory_ID, trxName);
	}


	@Override
	public void resetDocStatus() 
	{
		set_ValueNoCheck("DocStatus", "DR");
		set_ValueNoCheck("DocAction", "CO");
		set_ValueNoCheck("Processed", "N");
		disableModelValidation();
		saveEx();
		enableModelValidation();
	}

	@Override
	public String doAutoComplete() 
	{
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		
		if (!dt.getDocSubTypeInv().equals(MDocType.DOCSUBTYPEINV_InternalUseInventory))
		{
			try {
				if (!processIt(DOCACTION_Complete))
					return getProcessMsg();
			}
			catch (Exception ex) {
				ex.printStackTrace();
				return ex.getMessage();
			}
			return null;
		}		
		
		Hashtable<String, BigDecimal> requiredMap = new Hashtable<String, BigDecimal>();
				
		MInventoryLine[] iolList = getLines(true);
		
		for (MInventoryLine inl : iolList)
		{
			MProduct product = MProduct.get(getCtx(), inl.getM_Product_ID());
			
			if (!product.isActive()) {
				return "Product [" + product + "] is inactivated but still used at physical inventory #" + getDocumentNo();
			}
			
			String key = inl.getM_Product_ID() + "__" + inl.getM_Locator_ID() 
					+ "__" + inl.getM_AttributeSetInstance_ID() ;
			
			BigDecimal usedQty = requiredMap.get(key);
			if (usedQty == null) {
				usedQty = inl.getQtyInternalUse();
			}
			else {
				usedQty = usedQty.add(inl.getQtyInternalUse());
			}
			requiredMap.put(key, usedQty);
		}

		MMovement movement = null;
		Hashtable<String, BigDecimal> insufficientQtyMap = new Hashtable<String, BigDecimal>();
		
		for (String key : requiredMap.keySet())
		{
			String[] ids = key.split("__");
			int productID = Integer.valueOf(ids[0]);
			int locatorID = Integer.valueOf(ids[1]);
			int asiID = Integer.valueOf(ids[2]);
			
			BigDecimal availableQty = 
					MStorageOnHand.getQtyOnHandForLocator(productID, locatorID, asiID, get_TrxName());
			
			BigDecimal usedtQty = requiredMap.get(key);
			
			BigDecimal differentQty = availableQty.subtract(usedtQty);
			
			if (differentQty.signum() >= 0)
				continue;
			
			MLocator loc = MLocator.get(getCtx(), locatorID);
			
			differentQty = differentQty.abs();
			
			MStorageOnHand[] sohList = 
					MStorageOnHand.getOfProduct(p_ctx, getAD_Org_ID(), productID, get_TrxName());
			
			int orgFrom = getAD_Org_ID();
			boolean isFromOtherOrg = false;
			
			if(sohList.length == 0) 
			{
				orgFrom = 1000039; // HO at where the WH3,5 located.
				
				sohList = MStorageOnHand.getOfProduct(p_ctx, orgFrom, productID, get_TrxName());
				isFromOtherOrg = true;
			}

			if(sohList.length == 0) {
				insufficientQtyMap.put(key, differentQty);
				continue;
			}
			
			while (true)
			{
				Hashtable<Integer, BigDecimal> locQtyMap = new Hashtable<Integer, BigDecimal>();
				
				for (MStorageOnHand soh : sohList)
				{
					if (soh.getQtyOnHand().signum() <= 0 || soh.getM_Locator_ID() == locatorID
							|| soh.getM_Locator_ID() == 1000244 || soh.getM_Locator_ID() == 1000248
							|| soh.getM_Locator_ID() == 1000250 || soh.getM_Locator_ID() == 1000245
							|| soh.getM_Locator_ID() == 1000249 || soh.getM_Locator_ID() == 1000246)
//					if (soh.getQtyOnHand().signum() <= 0)
						continue;
					
					BigDecimal locQty = locQtyMap.get(soh.getM_Locator_ID());
					
					if (locQty == null)
						locQty = Env.ZERO;
					locQty = locQty.add(soh.getQtyOnHand());
					locQtyMap.put(soh.getM_Locator_ID(), locQty);
				}
				
				
				if (locQtyMap.size() == 0 && isFromOtherOrg)
					break;
				
				for (Integer availableLoc_ID : locQtyMap.keySet())
				{
					availableQty = (BigDecimal) locQtyMap.get(availableLoc_ID);
	
					if (availableLoc_ID == locatorID || availableQty.signum() <= 0)
						continue;
					
					if (movement == null)
					{
						String sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType=? AND IsInTransit='N'";
						int movementDT_ID = DB.getSQLValueEx(get_TrxName(), sql, MDocType.DOCBASETYPE_MaterialMovement);
						
						movement = new MMovement(getCtx(), 0, get_TrxName());
						movement.setAD_Org_ID(orgFrom);
						movement.setMovementDate(getMovementDate());
						movement.setC_DocType_ID(movementDT_ID);
						movement.setDestinationWarehouse_ID(loc.getM_Warehouse_ID());
						movement.setSalesRep_ID(Env.getAD_User_ID(getCtx()));
						movement.setDescription("{**AtC**} Auto Move** for Internal Use Document#" + getDocumentNo());
						movement.saveEx();
					}
					
					BigDecimal qtyToUse = differentQty;
					
					if (availableQty.compareTo(qtyToUse) < 0)
						qtyToUse = availableQty;
					
					MMovementLine ml = new MMovementLine(movement);
					ml.setM_Product_ID(productID);
					ml.setM_Locator_ID(availableLoc_ID);
					ml.setM_LocatorTo_ID(locatorID);
					ml.setMovementQty(qtyToUse);
					ml.setConfirmedQty(qtyToUse);
					ml.saveEx();
					
					differentQty = differentQty.subtract(qtyToUse);
				}

				if (differentQty.signum() > 0 && !isFromOtherOrg)
				{
					orgFrom = 1000039; // HO at where the WH3,5 located.
					
					sohList = MStorageOnHand.getOfProduct(p_ctx, orgFrom, productID, get_TrxName());					
					isFromOtherOrg = true;
					if(sohList.length == 0)
						break;
				}
				else 
					break;
			}
			
			if (differentQty.compareTo(Env.ZERO) > 0)
				insufficientQtyMap.put(key, differentQty);
		}
		
		if (movement != null) {
//			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(movement, DocAction.ACTION_Complete);
//			if(pi.isError())
//				return pi.getSummary();
			if (!movement.processIt(DOCACTION_Complete))
				return "Failed when auto moving qty for inventory#" + getDocumentNo();
			movement.saveEx();

		}
		
		if (insufficientQtyMap.size() > 0)
		{
			//Create initial inventory.
			org.compiere.model.MInventory inventory = 
					new org.compiere.model.MInventory((MWarehouse) getM_Warehouse(), get_TrxName());
			inventory.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventoryImport));
			inventory.setMovementDate(getMovementDate());
			inventory.setDescription("{**AtC**} Auto initialized for InternalUse#" + getDocumentNo());
			if (!inventory.save()) {
				log.saveError("Failed while initilizing stock for InternalUse#" + getDocumentNo(), 
								"Failed while initilizing stock for InternalUse#" + getDocumentNo());
				return "Failed while initilizing stock for InternalUse#" + getDocumentNo();
			}
			
			for (String key : insufficientQtyMap.keySet())
			{
				String[] ids = key.split("__");
				int M_Product_ID = Integer.valueOf(ids[0]);
				int locatorID = Integer.valueOf(ids[1]);
				
				BigDecimal requiredQty = insufficientQtyMap.get(key);
				
				MInventoryLine il = 
						new MInventoryLine(inventory, locatorID, M_Product_ID, 0, Env.ZERO, requiredQty, Env.ZERO);
				int M_PriceList_ID_1 = 1000101; // Harga Pembelian
				int M_PriceList_ID_2 = 1000099; // Harga Toko.
				int M_PriceList_ID_3 = 1000104; // PL Auto Initial Purposes Only.
				int M_PriceList_ID_4 = 1000100; // Harga Konsumen.
				Timestamp dateDoc = getMovementDate();
				
				String sql = 
						"SELECT pp.PriceList FROM M_ProductPrice pp "
						+ " INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
						+ " INNER JOIN M_PriceList pl ON plv.M_PriceList_ID=pl.M_PriceList_ID "
						+ " WHERE pp.M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
						+ " ORDER BY plv.ValidFrom DESC";
				
				BigDecimal priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_1, dateDoc);				
				BigDecimal percentage = Env.ONE;
				
				if (priceList == null || priceList.signum() <= 0)
				{
					String sqlCost = "SELECT CurrentCostPrice FROM M_Cost WHERE M_Product_ID=? AND M_CostElement_ID=1000014";
					priceList = DB.getSQLValueBDEx(get_TrxName(), sqlCost, M_Product_ID);
				}
					
				if (priceList == null || priceList.signum() <= 0) 
				{ 
					// Get it from previous initial stock.
					MInventoryLine invLine = 
							new Query(getCtx(), MInventoryLine.Table_Name, 
									"QtyCount>0 AND NewCostPrice>0 AND M_Product_ID=?", get_TrxName())
							.setParameters(M_Product_ID).first();
					if (invLine != null)
						priceList = invLine.getNewCostPrice().divide(invLine.getQtyCount(), 2, BigDecimal.ROUND_HALF_UP);
					percentage = Env.ONE;
				}
				
				if (priceList == null || priceList.signum() <= 0) {
					priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_2, dateDoc);
					percentage = BigDecimal.valueOf(0.75);
				}
				if (priceList == null || priceList.signum() <= 0) {
					priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_3, dateDoc);
					percentage = BigDecimal.valueOf(0.65);
				}
				if (priceList == null || priceList.signum() <= 0) {
					priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_4, dateDoc);
					percentage = BigDecimal.valueOf(0.55);
				}
				
				if (priceList == null || priceList.signum() <= 0)
					return "Cannot find price list when completing Internal Use Doc #" + getDocumentNo() 
							+ " for product of " + MProduct.get(getCtx(), M_Product_ID);
				
				BigDecimal cost = priceList.multiply(percentage);
				
				BigDecimal totalCost = cost.multiply(requiredQty);
				
				il.setNewCostPrice(totalCost);
				if (!il.save()) {
					String msg = "Error while saving initial inventory line for product of " 
								+ MProduct.get(getCtx(), M_Product_ID);
					log.saveError(msg, msg);
					return msg;
				}				
			}

//			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(inventory, DocAction.ACTION_Complete);
//			if(pi.isError())
//				return pi.getSummary();
			if (!inventory.processIt(DOCACTION_Complete))
				return "Failed when fulfilling qty for inventory#" + getDocumentNo();
			inventory.saveEx();
		}
		
		try {
//			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(this, DocAction.ACTION_Complete);
//			if(pi.isError())
//			{
//				return pi.getSummary() + " Inventory No: " + getDocumentNo();
//			}
			
			if (!processIt(DOCACTION_Complete))
				return getProcessMsg();
			this.saveEx();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return ex.getMessage();
		}
		return null;
	}
}