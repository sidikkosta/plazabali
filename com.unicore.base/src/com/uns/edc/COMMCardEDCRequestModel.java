/**
 * 
 */
package com.uns.edc;

import org.adempiere.exceptions.AdempiereException;

import com.uns.model.MUNSEDCProcessorAction;
import com.uns.model.MUNSEDCProcessorParam;

/**
 * @author nurse
 *
 */
public class COMMCardEDCRequestModel extends DefaultEDCModel
{
	private final String STX = "STX";
	private final String LRC = "LRC";
	private final String ETX = "ETX";
	private String m_stx = null;
	private String m_etx = null;
	
	/**
	 * 
	 */
	public COMMCardEDCRequestModel(MUNSEDCProcessorAction action, ISourceEDCModel source, int byteLength) 
	{
		super(new byte[byteLength]);
		MUNSEDCProcessorParam[] params = action.getRequestFields(false);
		int lrc = -1;
		int byteGen = 0;
		for (int i=0; i<params.length; i++)
		{
			Object value = "";
			if (LRC.equals(params[i].getName()))
				value = Integer.toHexString(lrc);
			else if (MUNSEDCProcessorParam.PARAMETERTYPE_Constant.equals(params[i].getParameterType()))
				value = params[i].getParameterValue();
			else
			{
				if (MUNSEDCProcessorParam.SOURCEVALUE_OriginalInvoiceNo.equals(params[i].getSourceValue()))
					value = source.getOriginalInvoiceNo();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_TransactionAmount.equals(params[i].getSourceValue()))
					value = source.getTransactionAmount();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_OtherAmount.equals(params[i].getSourceValue()))
					value = source.getOtherAmount();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_AccountNo.equals(params[i].getSourceValue()))
					value = source.getAccountNo();
				else
					value = null;
			}
			
			byte[] byteValue = null;
			int length = params[i].getLength();
			System.out.println("Process param " + params[i].getLine() + " " + params[i].getName());
			if (MUNSEDCProcessorParam.DATATYPE_Hexadecimal.equals(params[i].getDataType()))
			{
				if (value == null)
					value = "";
				length = length * 2;
				String valStr = value.toString();
				if (valStr.length() > length && length > 0)
					valStr = valStr.substring(0,length);
				if (length <= 0)
					length = 1;
				String format = "%" + length + "s";
				valStr = String.format(format, valStr).replace(" ", "0");
				byteValue = hexToByte(valStr);
			}
			else if (MUNSEDCProcessorParam.DATATYPE_Alphanumeric.equals(params[i].getDataType()))
			{
				if (value == null)
					value = "";
				String valStr = value.toString();
				if (valStr.length() > length && length > 0)
					valStr = valStr.substring(0,params[i].getLength());
				if (length <= 0)
					length = 1;
				String format = "%1$-" + length + "s";
				valStr = String.format(format, valStr);
				byteValue = valStr.getBytes();
			}
			else if (MUNSEDCProcessorParam.DATATYPE_Numeric.equals(params[i].getDataType()))
			{
				if (value == null)
					value = Double.valueOf(0);
				else if (value instanceof String)
				{
					if ("LRC".equals(params[i].getName()))
					{
						value = Integer.parseInt((String)value, 16);
						value = new Double((int)value);
					}
					else
						value = Double.valueOf((String)value);
				}
				if (length <= 0)
					length = 1;
				String sufix = "";
				if (length > 10)
				{
					sufix = "00";
					length = 10;
				}
				if ("LRC".equals(params[i].getName()) && params[i].isHexDump())
				{
					String hexStr = Integer.toHexString(((Double)value).intValue()).toUpperCase();
					hexStr = String.format("%" + length + "s", hexStr).replace(" ", "0");
					char[] chars = hexStr.toCharArray();
					String val = "";
					for (int j=0; j<chars.length; j++)
					{
						int c = (int) chars[j];
						val += Integer.toHexString(c).toUpperCase();
					}
					byteValue = hexToByte(val);
				}
				else
				{
					String format = "%0"+ length + ".0f" + sufix;
					String strAmt = String.format(format, value);
					byteValue = strAmt.getBytes();
				}
			}
			else
			{
				throw new AdempiereException("Unkown Data Type");
			}
			
			for (int x=0; x<byteValue.length; x++)
			{
				setByteValueAt(byteValue[x], byteGen++);
				if (STX.equals(params[i].getName()) || LRC.equals(params[i].getName()))
					continue;
				String hex = String.format("%02X", byteValue[x]);
				if (lrc == -1)
				{
					lrc = Integer.parseInt(hex, 16);
					continue;
				}
				int xor = Integer.parseInt(hex, 16);
				lrc = lrc ^ xor;
			}
			if (STX.equals(params[i].getName()))
				m_stx = value.toString();
			else if (ETX.equals(params[i].getName()))
				m_etx = value.toString();
		}
	}
	
	public String getSTX ()
	{
		return m_stx;
	}
	
	public String getETX ()
	{
		return m_etx;
	}
	
	public static void main (String[] args)
	{
		String hexStr = Integer.toHexString(10);
		hexStr = String.format("%2s", hexStr).replace(" ", "0");
		System.out.println(hexStr);
		int int1 = 10;
		String hex = Integer.toHexString(int1).toUpperCase();
//		if (hex.length() < 2)
//			hex = "0" + hex;
		char[] chrs = hex.toCharArray();
		String result = "";
		for (int i=0; i<chrs.length; i++)
		{
			 int c = (int) chrs[i];
			result += Integer.toHexString(c).toUpperCase();
		}
		System.out.println(Integer.toHexString(329175));
		
		System.out.println(result);
		System.out.println(String.format("%H", 10));
		System.out.println(Integer.valueOf("2", 16));
		String s = String.format("%X", 10);
		System.out.println(Integer.parseInt("10", 8));
		System.out.println(s);
	}
	
}
