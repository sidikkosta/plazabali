/**
 * 
 */
package com.uns.edc;


/**
 * @author nurse
 *
 */
public interface IEDCPaymentProcessor 
{
	public boolean process (ISourceEDCModel model, String action);
	public void forceStop ();
}
