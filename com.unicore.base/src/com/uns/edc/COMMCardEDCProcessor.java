/**
 * 
 */
package com.uns.edc;

import jssc.SerialPortException;

import org.compiere.util.DB;

import com.uns.model.MUNSEDCProcessor;
import com.uns.model.MUNSEDCProcessorAction;
import com.uns.model.MUNSEDCProcessorParam;

/**
 * @author nurse
 *
 */
public class COMMCardEDCProcessor implements IEDCPaymentProcessor
{
	private MUNSEDCProcessor m_config;
	private String m_comm;
	
	/**
	 * 
	 */
	public COMMCardEDCProcessor(String comm, MUNSEDCProcessor processor) 
	{
		super ();
		m_config = processor;
		m_comm = comm;
	}

	/* (non-Javadoc)
	 * @see com.uns.model.process.IEDCPaymentProcessor#process(com.uns.model.ISourceEDCModel)
	 */
	public boolean process(ISourceEDCModel model, String action) 
	{
		if (action == null)
			return false;
		MUNSEDCProcessorAction processAction = m_config.getAction(action);
		if (processAction == null)
			return false;
		if (model.getTransactionAmount() <= 0)
			return false;
		
		String sql = "SELECT SUM (Length) FROM UNS_EDCProcessorParam WHERE UNS_EDCPRocessorAction_ID = ? AND TransactionType = ?";
		int length = DB.getSQLValue(null, sql, processAction.get_ID(), MUNSEDCProcessorParam.TRANSACTIONTYPE_Request);
		COMMCardEDCRequestModel request = new COMMCardEDCRequestModel(processAction, model, length);
		System.out.println(new String(request.getBytes()));
		COMMCardEDCClient client = new COMMCardEDCClient(m_comm);
		MUNSEDCProcessorParam[] conParams = processAction.getConnectionParams(false);
		for (int i=0; i<conParams.length; i++)
		{
			if ("BaudRate".equals(conParams[i].getName()))
				client.setBaudRate(Integer.valueOf(conParams[i].getParameterValue()));
			else if ("DataBit".equals(conParams[i].getName()))
				client.setDataBit(Integer.valueOf(conParams[i].getParameterValue()));
			else if ("StopBit".equals(conParams[i].getName()))
				client.setStopBit(Integer.valueOf(conParams[i].getParameterValue()));
			else if ("Parity".equals(conParams[i].getName()))
				client.setParity(Integer.valueOf(conParams[i].getParameterValue()));
		}
		try 
		{
			if (!client.connect("UntaERP", processAction.getParent().getConnectionTimeOut()))
				return false;
			
			MUNSEDCProcessorParam[] responseFields = processAction.getResponseField(false);
			
			COMMCardEDCResponseModel response =client.sendRequest(request.getBytes(), m_config.getReadTimeOut(),request.getSTX());
			if (m_config.isSendACK())
			{
				byte[] bytes = request.hexToByte("06");
				client.getPort().writeBytes(bytes);
			}
			if (!response.initModel(responseFields))
				return false;
			System.out.println(response.getValue("ResponseCode"));
			if (!"00".equals(response.getValue("ResponseCode")))
				return false;
			
			for (int i=0; i<responseFields.length; i++)
			{
				if (responseFields[i].getSourceValue() == null)
					continue;
				MUNSEDCProcessorParam field = responseFields[i];
				String name = field.getName();
				Object value = response.getValue(name);
				if (MUNSEDCProcessorParam.SOURCEVALUE_BatchNo.equals(field.getSourceValue()))
					model.setBatchNo((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_CardholderName.equals(field.getSourceValue()))
					model.setCardholderName((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_InvoiceNo.equals(field.getSourceValue()))
					model.setInvoiceNo((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_OtherAmount.equals(field.getSourceValue()))
					model.setOtherAmount((double)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_TraceNo.equals(field.getSourceValue()))
					model.setTraceNo((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_TransactionAmount.equals(field.getSourceValue()))
					model.setTransactionAmount((double)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_CardType.equals(field.getSourceValue()))
					model.setCardType((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_AccountNo.equals(field.getSourceValue()))
					model.setAccountNo((String) value); 
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			if (client != null)
				try {
					client.close();
				} catch (SerialPortException e) {
					e.printStackTrace();
				}
		}
		
		return true;
	}

	@Override
	public void forceStop() 
	{
		//do nothing;
	}
}
