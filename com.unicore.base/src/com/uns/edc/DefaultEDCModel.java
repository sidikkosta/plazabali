/**
 * 
 */
package com.uns.edc;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @author nurse
 *
 */
public abstract class DefaultEDCModel 
{	
	private byte[] m_bytes;
	
	public DefaultEDCModel ()
	{
		super ();
	}
	
	/**
	 * 
	 */
	public DefaultEDCModel(byte[] bytes) 
	{
		this ();
		m_bytes = bytes;
	}

	public byte[] getBytes ()
	{
		return m_bytes;
	}
	
	public String getStringValue (int offsite, String separator)
	{
		String value = new String(getBytes(), offsite, m_bytes.length-offsite);
		int sepIdx = value.indexOf(separator);
		if (sepIdx != -1)
			value = value.substring(0, sepIdx);
		return value;
	}
	
	public String getStringValue (int offsite, int length)
	{
		String value = new String(getBytes(), offsite, length);
		return value;
	}
	
	public String getHexValue (int offsite, String separator)
	{
		String value = new String(m_bytes, offsite, m_bytes.length - offsite);
		int sepIdx = value.indexOf(separator);
		value = value.substring(0, sepIdx);
		return value;
	}
	
	public String getHexValue (int offsite, int length)
	{
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<length; i++)
		{
			sb.append(byteToHex(m_bytes[offsite++]));
		}
		String value = sb.toString();
		return value;
	}
	
	public double getDoubleValue (int offsite, int length)
	{
		String strVal = getStringValue(offsite, length);
		double value = Double.parseDouble(strVal);
		return value;
	}
	
	public BigDecimal getBigDecimalValue (int offsite, int length)
	{
		return new BigDecimal(getDoubleValue(offsite, length));
	}
	
	public String stringToHex(String arg0) 
	{
		return String.format("%02X", arg0);
	}
	
	public String byteToHex (byte byt)
	{
		return String.format("%02X", byt);
	}

	public String hexToString(String arg0) 
	{
		byte[] bytes = hexToByte(arg0);
		String s = new String(bytes, StandardCharsets.UTF_8);
		return s;
	}

	public byte[] hexToByte(String arg0) 
	{
		byte[] bytes = new byte[arg0.length() / 2];
		for (int i = 0; i < bytes.length; i++) 
		{
			int index = i * 2;
			int v = Integer.parseInt(arg0.substring(index, index + 2), 16);
			bytes[i] = (byte) v;
		}
		return bytes;	
	}
	
	public int hexToInt (String hex)
	{
		return Integer.parseInt(hex, 16);
	}
	
	public String intToHex (int arg0)
	{
		return Integer.toHexString(arg0);
	}
	
	public void setByteValue (byte[] bytes, int offset)
	{
		int newLength = bytes.length + offset;
		if (m_bytes.length < newLength)
			m_bytes = Arrays.copyOf(m_bytes, newLength);
		for (int i=0; i<bytes.length; i++)
			m_bytes[offset++] = bytes[i];
	}
	
	public void setByteValueAt (byte byt, int idx)
	{
		int newLength = idx+1;
		if (m_bytes.length < newLength)
			m_bytes = Arrays.copyOf(m_bytes, newLength);
		m_bytes[idx] = byt;
	}
}
