/**
 * 
 */
package com.uns.edc;

import jssc.SerialPort;
import jssc.SerialPortException;



/**
 * @author nurse
 *
 */
public class COMMCardEDCClient extends UNSDefaultCOMMEDCClient 
{
	private int p_baudRate = 9600;
	private int p_databit = SerialPort.DATABITS_8;
	private int p_stopbit = SerialPort.STOPBITS_1;
	private int p_parity = SerialPort.PARITY_NONE;
	
	public void setBaudRate (int baudRate)
	{
		p_baudRate = baudRate;
	}
	
	public void setDataBit (int dataBit)
	{
		p_databit = dataBit;
	}
	
	public void setStopBit (int stopBit)
	{
		p_stopbit = stopBit;
	}
	
	public void setParity (int parity)
	{
		p_parity = parity;
	}
	
	/**
	 * @param comm
	 */
	public COMMCardEDCClient(String comm) 
	{
		super(comm);
	}

	@Override
	public void setSerialPortParams() 
	{
		try {
			getPort().setParams(p_baudRate, p_databit, p_stopbit, p_parity);
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}
}
