/**
 * 
 */
package com.uns.edc;

/**
 * @author nurse
 *
 */
public interface ISourceEDCModel 
{
	public void setBatchNo (String batchNo);
	public String getBatchNo ();
	public void setInvoiceNo (String invoiceNo);
	public String getInvoiceNo ();
	public void setTraceNo (String traceNo);
	public String getTraceNo ();
	public void setTransactionAmount (double transactionAmt);
	public double getTransactionAmount ();
	public void setOtherAmount (double otherAmount);
	public double getOtherAmount ();
	public void setOriginalInvoiceNo (String originalInvoiceNo);
	public String getOriginalInvoiceNo ();
	public void setCardholderName (String cardHolderName);
	public String getCardholderName ();
	public String getOutlateInvNo ();
	public String getISOCurrency ();
	public int getC_Currency_ID ();
	public void setCardType (String cardType);
	public void setAccountNo (String accountNo);
	public String getAccountNo ();
}
