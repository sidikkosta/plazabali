/**
 * 
 */
package com.uns.edc;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import org.apache.http.ParseException;
import com.uns.model.MUNSEDCProcessor;
import com.uns.model.MUNSEDCProcessorAction;
import com.uns.model.MUNSEDCProcessorParam;

/**
 * @author nurse
 *
 */
public class UNSWXPayProcessor implements IEDCPaymentProcessor 
{
	private String m_conn;
	private MUNSEDCProcessor m_config;
	private Thread m_thread = null;
	
	/**
	 * 
	 */
	public UNSWXPayProcessor(String connection, MUNSEDCProcessor processor) 
	{
		m_conn = connection;
		m_config = processor;
	}

	/* (non-Javadoc)
	 * @see com.uns.edc.IEDCPaymentProcessor#process(com.uns.edc.ISourceEDCModel, java.lang.String)
	 */
	@Override
	public boolean process(ISourceEDCModel model, String action) 
	{
		MUNSEDCProcessorAction processAction = m_config.getAction(action);
		if (action == null)
			return false;
		if (model.getTransactionAmount() <= 0)
			return false;
	
		Hashtable<String, String> connection = new Hashtable<>();
		Hashtable<String, String> data = new Hashtable<>();
		MUNSEDCProcessorParam[] reqParams = processAction.getRequestFields(false);
		MUNSEDCProcessorParam[] conParams = processAction.getConnectionParams(false);
		for (int i=0; i<conParams.length; i++)
		{
			connection.put(conParams[i].getName(), conParams[i].getParameterValue());
		}
		
		for (int i=0; i<reqParams.length; i++)
		{
			Object value = null;
			if (MUNSEDCProcessorParam.PARAMETERTYPE_Constant.equals(reqParams[i].getParameterType()))
			{
				value = reqParams[i].getParameterValue();
			}
			else
			{
				if (MUNSEDCProcessorParam.SOURCEVALUE_Currency.equals(reqParams[i].getSourceValue()))
					value = model.getISOCurrency();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_BatchNo.equals(reqParams[i].getSourceValue()))
					value = model.getBatchNo();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_OutlateInvNo.equals(reqParams[i].getSourceValue()))
					value = model.getOutlateInvNo();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_TransactionAmount.equals(reqParams[i].getSourceValue()))
					value = model.getTransactionAmount();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_OtherAmount.equals(reqParams[i].getSourceValue()))
					value = model.getOtherAmount();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_AccountNo.equals(reqParams[i].getSourceValue()))
					value = model.getAccountNo();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_TraceNo.equals(reqParams[i].getSourceValue()))
					value = model.getAccountNo();
				else if (MUNSEDCProcessorParam.SOURCEVALUE_OriginalInvoiceNo.equals(reqParams[i].getSourceValue()))
					value = model.getOriginalInvoiceNo();
				else
					value = "";
			}			
			data.put(reqParams[i].getName(), value.toString());
		}
		
		String sufix = connection.get("connection_sufix");
		String signType = connection.get("sign_type");
		String privKey = connection.get("private_key");
		String okCol = connection.get("ok_column");
		String okStat = connection.get("ok_status");
		boolean isLoopExec = "Y".equals(connection.get("loop_exec"));
		String url = m_conn;
		if (!url.endsWith("/") && ! sufix.startsWith("/"))
			url += "/";
		url += sufix;
		Map<String, String> responseMap = null;
		
		try 
		{
			if (isLoopExec)
			{
				QueryThread qt = new QueryThread(url, signType, privKey, data, okCol, okStat);
				m_thread = new Thread(qt);
				m_thread.setName("Wechat-Check-Pay-Status");
				m_thread.start();
				while (m_thread.isAlive())
				{
					Thread.sleep(1000);
				}
				responseMap = qt.getResponseMap();
			}
			else
			{
				UNSWXPay wxPay = new UNSWXPay(url, data, signType, privKey);
				String response = wxPay.request(m_config.getConnectionTimeOut());
				responseMap = wxPay.jsonToMap(response);
			}
			
			if (!okStat.equals(responseMap.get(okCol)))
				return false;	
			
			MUNSEDCProcessorParam responses[] = processAction.getResponseField(false);
			for (int i=0; i<responses.length; i++)
			{
				Object value = null;
				if (responses[i].getParameterType().equals(MUNSEDCProcessorParam.PARAMETERTYPE_Constant))
					value = responses[i].getParameterValue();
				else
				{
					value = responseMap.get(responses[i].getName());
				}
				
				if (MUNSEDCProcessorParam.SOURCEVALUE_BatchNo.equals(responses[i].getSourceValue()))
					model.setBatchNo((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_BatchNo.equals(responses[i].getSourceValue()))
					model.setTraceNo((String) value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_InvoiceNo.equals(responses[i].getSourceValue()))
					model.setInvoiceNo((String)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_TransactionAmount.equals(responses[i].getSourceValue()))
					model.setTransactionAmount((double)value);
				else if (MUNSEDCProcessorParam.SOURCEVALUE_CardType.equals(responses[i].getSourceValue()))
					model.setCardType((String)value);
					
			}
			
			String nextAction = action + "_Query";
			if (m_config.getAction(nextAction) != null)
				return process(model, nextAction);
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return false;
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		return true;
	}

	@Override
	public void forceStop() 
	{
		if (m_thread != null)
			m_thread.interrupt();
	}

}

class QueryThread implements Runnable
{
	private String m_url;
	private String m_signType;
	private String m_privKey;
	private Hashtable<String, String> m_data;
	private int m_connTimeout = 30000;
	private int m_readTimeout = 30000;
	private Map<String, String> m_responseMap = null;
	String m_ok_column = "trade_status";
	String m_ok_value = "1";
	
	public QueryThread (String url, String signType, String privateKey, Hashtable<String, String> data, String okCol, String okVal)
	{
		m_url = url;
		m_signType = signType;
		m_privKey = privateKey;
		m_data = data;
		m_ok_column = okCol;
		m_ok_value = okVal;
	}
	
	public void setConnectionTimeout (int timeout)
	{
		m_connTimeout = timeout;
	}
	
	public void setReadTimeout (int timeout)
	{
		m_readTimeout = timeout;
	}
	
	@Override
	public void run() 
	{
		String ok = "xxxx";
		UNSWXPay query = new UNSWXPay(m_url, m_data, m_signType, m_privKey);
		String response;
		long start = System.currentTimeMillis();
		while (!m_ok_value.equals(ok))
		{
			try 
			{
				Thread.sleep(1000);
				response = query.request(m_connTimeout);
				long current = System.currentTimeMillis();
				long diff = current - start;
				if (diff >= m_readTimeout)
					break;
			} 
			catch (ParseException e) 
			{
				e.printStackTrace();
				break;
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
				break;
			} 
			catch (InterruptedException e) 
			{
				break;
			}
			
			m_responseMap = query.jsonToMap(response);
			ok = m_responseMap.get(m_ok_column);
		}
	}	
	
	public Map<String, String> getResponseMap ()
	{
		return m_responseMap;
	}
}
