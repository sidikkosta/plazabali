/**
 * 
 */
package com.uns.edc;

import java.io.IOException;
import javax.swing.JOptionPane;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import org.compiere.util.CLogger;

/**
 * @author nurse
 *
 */
public abstract class UNSDefaultCOMMEDCClient 
{
	protected final CLogger log = CLogger.getCLogger(this.getClass());
	private String m_comm; 
	private SerialPort m_port;
	private final boolean[] m_stopConnManager = new boolean[]{false};
	private COMMCardEDCResponseModel m_response = null;
	
	/**
	 * 
	 */
	public UNSDefaultCOMMEDCClient(String comm) 
	{
		m_comm = comm;
	}
	
	public boolean connect (String owner, long timeout) throws SerialPortException
	{
		m_port = new SerialPort(m_comm);
		if (m_port == null)
			return false;
		
		return m_port.openPort();
	}
	
	public SerialPort getPort ()
	{
		return m_port;
	}
	
	public String getCOMM ()
	{
		return m_comm;
	}
	
	/**
	 * 
	 */
	public abstract void setSerialPortParams ();
	
	public void close () throws SerialPortException
	{
		if (m_port != null)
		{
			if (m_port.isOpened())
			{
				m_port.removeEventListener();
				m_port.closePort();
			}
			synchronized (m_stopConnManager) {
				m_stopConnManager[0] = true;
			}
		}
	}
	
	public COMMCardEDCResponseModel sendRequest (
			byte[] request, final long timeout, final String stx) throws IOException, SerialPortException
	{
		m_port.purgePort(SerialPort.PURGE_RXCLEAR);
		m_port.purgePort(SerialPort.PURGE_TXCLEAR);
		
		getPort().addEventListener(new SerialPortEventListener() {
			
			@Override
			public void serialEvent(SerialPortEvent arg0) {
				if (!arg0.isRXCHAR())
					return;
				byte[] bytes;
				try {
					bytes = getPort().readBytes();
				} catch (SerialPortException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}
				byte b = bytes[0];
				String hex = String.format("%02X",b);
				if (!stx.equals(hex))
					return;				
				m_response = new COMMCardEDCResponseModel(bytes);
				System.out.println(new String(bytes));
			}
		});
		
		m_port.writeBytes(request);
		
		JOptionPane.showMessageDialog(null, "Please click ok when transaction is done!");
		
		return m_response;
	}
}
