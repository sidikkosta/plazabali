/**
 * 
 */
package com.uns.edc;

import java.util.HashMap;
import java.util.Map;



import com.uns.model.MUNSEDCProcessorParam;

/**
 * @author nurse
 *
 */
public class COMMCardEDCResponseModel extends DefaultEDCModel 
{

	private Map<String, Object> m_map;
	
	/**
	 * 
	 */
	public COMMCardEDCResponseModel(byte[] bytes) 
	{
		super(bytes);
		m_map = new HashMap<>();
	}
	
	public Object getValue (String key)
	{
		return m_map.get(key);
	}
	
	public boolean initModel (MUNSEDCProcessorParam[] fields)
	{
		int offsite = 0;
		for (int i=0; i<fields.length; i++)
		{
			MUNSEDCProcessorParam field = fields[i];
			System.out.println(field.getName());
			Object value;
			if (MUNSEDCProcessorParam.PARAMETERTYPE_Constant.equals(field.getParameterType()))
			{
				value = field.getParameterValue();
				offsite = offsite + field.getLength();
			}
			else
			{
				if (offsite > getBytes().length)
					break;
				int length = field.getLength();
				String separator = null;
				if (length == 0)
				{
					MUNSEDCProcessorParam nextField = fields[i+1];
					if (nextField.getParameterType().equals(MUNSEDCProcessorParam.PARAMETERTYPE_Constant))
					{
						separator = nextField.getParameterValue();
					}
				}
				if (separator != null)
				{
					if (MUNSEDCProcessorParam.DATATYPE_Hexadecimal.equals(field.getDataType()))
					{
						if (field.isHexDump())
						{
							value = getStringValue(offsite, fields[i].getLength());
						}
						else
						{
							value = getHexValue(offsite, separator);
						}
						offsite = offsite + value.toString().length()/2;
					}
					else if (MUNSEDCProcessorParam.DATATYPE_Numeric.equals(field.getDataType()))
					{
						String strVal = getStringValue(offsite, separator);
						if (field.isHexDump())
						{
							strVal = hexToString(strVal);
						}
						offsite = offsite + strVal.length();
						strVal = strVal.replace(",", "");
						value = Double.parseDouble(strVal);
					}
					else
					{
						value = getStringValue(offsite, separator);
						if (field.isHexDump() && value != null)
							value = hexToString(value.toString());
						offsite = offsite + value.toString().length();
					}
				}
				else if (MUNSEDCProcessorParam.DATATYPE_Hexadecimal.equals(field.getDataType()))
				{
					if (field.isHexDump())
						value = getStringValue(offsite, field.getLength());
					else
						value = getHexValue(offsite, field.getLength());
					offsite = offsite + field.getLength();
				}
				else if (MUNSEDCProcessorParam.DATATYPE_Numeric.equals(field.getDataType()))
				{
					value = getDoubleValue(offsite, field.getLength(), field.isHexDump());
					offsite = offsite + field.getLength();
				}
				else
				{
					value = getStringValue(offsite, field.getLength());
					if (field.isHexDump() && value != null)
						value = hexToString(value.toString());
					offsite = offsite + field.getLength();
				}
			}

			m_map.put(field.getName(), value);
		}
		
		return true;
	}
	
	public double getDoubleValue (int offsite, int length, boolean isHexDump)
	{
		String strVal = getStringValue(offsite, length);
		if (isHexDump)
			strVal = hexToString(strVal);
		int subsIdx = 10;
		if (length < subsIdx)
			subsIdx = length;
		strVal = strVal.substring(0, subsIdx);
		double value = Double.parseDouble(strVal);
		return value;
	}
	
	@Override
	public double getDoubleValue (int offsite, int length)
	{
		String strVal = getStringValue(offsite, length);
		strVal = strVal.substring(0, 9);
		double value = Double.parseDouble(strVal);
		return value;
	}
}
