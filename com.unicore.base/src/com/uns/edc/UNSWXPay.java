package com.uns.edc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.compiere.Adempiere;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class UNSWXPay 
{

	private String m_url;
	private Hashtable<String, String> m_data;
	private String m_dataString;
	private String m_requestMsg;
	private String m_signType;
	private String m_sign;
	private String m_dataEncoder;
	private String m_privateKey;
	private static final String USER_AGENT = Adempiere.getVersion() + " (" + System.getProperty("os.arch") 
			+ " " + System.getProperty("os.name") + " " + System.getProperty("os.version") + ") Java/" 
			+ System.getProperty("java.version") + " HttpClient/" 
			+ HttpClient.class.getPackage().getImplementationVersion(); 
	
	public String getURL ()
	{
		return m_url;
	}
	
	public String getDataString ()
	{
		return m_dataString;
	}
	
	public String getDataEncoder ()
	{
		return m_dataEncoder;
	}
	
	public String getRequestMsg ()
	{
		return m_requestMsg;
	}
	
	public String getSignType ()
	{
		return m_signType;
	}
	
	public String getSign ()
	{
		return m_sign;
	}
	
	public Hashtable<String, String> getDataMapping ()
	{
		return m_data;
	}
	
	public UNSWXPay(String url, Hashtable<String, String> data, String signType, String privateKey) 
	{
		m_url = url;
		m_data = data;
		m_signType = signType;
		m_privateKey = privateKey;
	}
	
	private String doEncode (String data)
	{
		String encoded = null;
		try 
		{
			encoded = URLEncoder.encode(data, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		return encoded;
	}
	
	private String doEncode ()
	{
		StringBuilder builder = new StringBuilder("{");
		for (String key : m_data.keySet())
		{
			if (builder.length() > 1)
				builder.append(",");
			builder.append("\"").append(key).append("\":");
			String value = m_data.get(key);
			if (value == null)
				value = "";
			builder.append("\"").append(value).append("\"");
		}
		builder.append("}");
		m_dataString = builder.toString();
		
		m_dataEncoder = doEncode(m_dataString);
		
		return m_dataEncoder;
	}
	
	private String generateSignature ()
	{
		try 
		{
			PrivateKey privateKey = generatePrivateKey();
	        byte[] bytes = m_dataString.getBytes("UTF-8");
	        Signature sign = Signature.getInstance("MD5WithRSA");
	        sign.initSign(privateKey);
	        sign.update(bytes);
	        byte[] signByte = sign.sign();
	        m_sign = Base64.encode(signByte);
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		} 
		catch (InvalidKeyException e) 
		{
			e.printStackTrace();
		}
		catch (SignatureException e) 
		{
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		} 
		catch (InvalidKeySpecException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (Base64DecodingException e) 
		{
			e.printStackTrace();
		}
		
		m_sign = doEncode(m_sign);
		return m_sign;
	}

	public String request (int timeout) throws ParseException, IOException
	{
		BasicHttpClientConnectionManager connMgr = new BasicHttpClientConnectionManager(
				RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", PlainConnectionSocketFactory.getSocketFactory())
				.register("https", SSLConnectionSocketFactory.getSocketFactory())
				.build(), null, null, null);
		HttpClient httpClient = HttpClientBuilder.create()
				.setConnectionManager(connMgr)
                .build();
	   
		HttpPost httpPost = new HttpPost(m_url);

		RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).build();
		httpPost.setConfig(requestConfig);
		buildRequest();
		StringEntity postEntity = new StringEntity(m_requestMsg, "UTF-8");
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		httpPost.addHeader("User-Agent", USER_AGENT + m_data.get("mch_id"));
		httpPost.setEntity(postEntity);

		HttpResponse httpResponse = httpClient.execute(httpPost);
		HttpEntity httpEntity = httpResponse.getEntity();
		String response = EntityUtils.toString(httpEntity, "UTF-8"); 
		return response;
	}
	
	private String buildRequest ()
	{
		StringBuilder builder = new StringBuilder("sign_type=");
		String data = doEncode();
		String sign = generateSignature();
		builder.append(m_signType).append("&data=").append(data)
		.append("&sign=").append(sign);
		m_requestMsg = builder.toString();
		return m_requestMsg;
	}
	
	public static void main (String[] args)
	{
		String url = "https://pay.altopay.co.id/mapi/pay/pay";
		final String signType = "RSA";
		
		final String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJvb3wm"
				+ "1Gt8wP0fY1sz9QCyVL7k6Nw/zF7jYY0anslunGoHp1WiFPK+sat2HOZQfly42nkYsbF7V"
				+ "xmLmCY9VLmTqL/L3I7lUt63X1KaXfjCzIYOOmW1xjwaoprYzZOEVUjGULF2Pc4Z5vIR6Ou"
				+ "F4zl05CPElcP7QEhrRUC/lzCjxAgMBAAECgYB/Jt9M8h0cHCZkHPkQf0NNm+A1QtMN"
				+ "BgtCV/aV+9W8hBVSC0x5WslpoEYGuH025JbXsuNo6HD45XpV0MnXz2xqzcUXNJ5EYXgF"
				+ "SpX4OCm97kmTJRYw0Dzg4VMov7v4T0EBZbQXeGl2jaTJ0usZKzuQKzDJk2bB+Jd2w5xRL"
				+ "dtBqQJBAMzYxwtyL1BGtM30Z6Xa4KqVG/quzz0iA8TPh37560T9NfvYx9FuXkxRNUCM"
				+ "1P5iEiReAcxM6rbB2C5CDRB40UcCQQDCx3KVrsupz3eHBWDhmRgqc1PKTYL8u3Q5HcIu"
				+ "5z1aAJ0Qrq/kdFdh94HdlNMdhWBMRaJr/q1NPP8BO1T0RhAHAkAZ7jcEE+g5WTn5+D5Gm"
				+ "HZoIYfxK7/AFVY0y7BzOl+10/fJcZ+Zu0bkkkMTcMBlkzSItf20RuhPSip7cJEGzMRxAkB"
				+ "NOca0khktupQpdHh0+b4bFp6iFxlYGvp5qtLSxdwNUzDy7E/QxE/hB8D1mYwaqEcF9pXvO"
				+ "4p6lTSFyIBmWWEBAkBobn+9vCbUgdvvkbV62ivqI0SRPk9yLnxZ5XVhfTXfslDfBPVJZg"
				+ "ijmiybZylR/b06wbsn2SATalgqPXF5mCki";
		
		String mchID = "100663";
		Hashtable<String, String> data = new Hashtable<>();
		data.put("mch_id", mchID);
		data.put("out_trade_no", "10101990");
		data.put("subject", "Test WX Payment");
		data.put("amount", "1200");
		data.put("currency", "IDR");
		data.put("auth_code", "134847116340859620");
		
		UNSWXPay test = new UNSWXPay(url, data, signType, privateKey);
		try 
		{
			String response = test.request(10000);
			Map<String, String> respMap = test.jsonToMap(response);
			for (String key : respMap.keySet())
			{
				System.out.println(key);
				System.out.println(respMap.get(key));
			}
			System.out.println(response.toString());
			if (!"0".equals(respMap.get("code")))
				return;	
			final String mch_ID = data.get("mch_id");
			final String tradeNo = respMap.get("trade_no");
			final String out_trade_no = data.get("out_trade_no");
			Thread thread = new Thread(new Runnable() {
				
				@Override
				public void run() 
				{
					boolean ok = false;
					while (!ok)
					{
						Hashtable<String, String> queryData = new Hashtable<>();
						queryData.put("mch_id", mch_ID);
						queryData.put("out_trade_no", out_trade_no);
						queryData.put("trade_no", tradeNo);
						String url2 = "https://pay.altopay.co.id/mapi/pay/query";
						UNSWXPay query = new UNSWXPay(url2, queryData, signType, privateKey);
						String response2;
						try 
						{
							response2 = query.request(10000);
						} 
						catch (ParseException e) 
						{
							e.printStackTrace();
							break;
						} 
						catch (IOException e) 
						{
							e.printStackTrace();
							break;
						}
						Map<String, String> respMap2 = query.jsonToMap(response2);
						for (String key : respMap2.keySet())
						{
							System.out.print(key);
							System.out.print(":");
							System.out.println(respMap2.get(key));
						}

						System.out.println(response2.toString());
						try 
						{
							Thread.sleep(5000);
						} 
						catch (InterruptedException e) 
						{
							e.printStackTrace();
						}
					}
				}
			});
			thread.setName("Wechat-Check-Pay-Status");
			thread.start();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public Map<String, String> jsonToMap (String text)
	{
		Map<String, String> map = new HashMap<>();
		String[] split = text.split("\\{");
		if (split == null)
			return map;
		for (int i=0; i<split.length; i++)
		{
			String[] content = split[i].split(",");
			if (content == null)
				continue;
			for (int x=0; x<content.length; x++)
			{
				content[x] = content[x].replace("\"", "").replace("\\", "").replace("}", "");
				String[] keyVal = content[x].split(":");
				if (keyVal == null || keyVal.length != 2)
					continue;
				String key = keyVal[0].replace("\"", "").replace("\\", "").replace("}", "");
				String value = keyVal[1].replace("\"", "").replace("\\", "").replace("}", "");
				map.put(key, value);
			}
		}
		
		return map;
	}
	
	public PrivateKey generatePrivateKey () throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, Base64DecodingException
	{
		StringBuilder pkcs8Lines = new StringBuilder();
        BufferedReader rdr = new BufferedReader(new StringReader(m_privateKey));
        String line;
        while ((line = rdr.readLine()) != null) 
        {
            pkcs8Lines.append(line);
        }

        String pkcs8Pem = pkcs8Lines.toString();
        byte [] pkcs8EncodedBytes = Base64.decode(pkcs8Pem);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = KeyFactory.getInstance(m_signType);
        PrivateKey privKey = kf.generatePrivate(keySpec);
        return privKey;
	}
}