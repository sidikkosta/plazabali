/**
 * 
 */
package com.uns.edc;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * @author nurse
 *
 */
public class WechatNotifyListener 
{

	private int m_port;
	private String m_urlSufix = "/en/wx-pay";
	private ExecutorService m_executor;
	private HttpServer m_server;
	/**
	 * 
	 */
	public WechatNotifyListener(int port, String urlSufix) 
	{
		super();
		m_port = port;
		if (urlSufix != null)
			m_urlSufix = urlSufix;
	}
	
	public void stop ()
	{
		m_server.stop(0);
		m_executor.shutdownNow();
	}
	
	public boolean start ()
	{
		InetSocketAddress address = new InetSocketAddress(m_port);
		m_executor = Executors.newFixedThreadPool(1);
		try 
		{
			m_server = HttpServer.create(address, 0);
			m_server.createContext(m_urlSufix, new HttpHandler() 
			{
				
				@Override
				public void handle(HttpExchange arg0) throws IOException 
				{
					String response = "SUCCESS";
					arg0.sendResponseHeaders(200, response.length());
					OutputStream respStream = arg0.getResponseBody();
					respStream.write(response.getBytes());
					respStream.flush();
					respStream.close();
				}
			});
			m_server.setExecutor(m_executor);
			m_server.start();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return true;
	}
	
	public static void main (String[] args)
	{
		WechatNotifyListener listener = new WechatNotifyListener(1010, null);
		listener.start();
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		listener.stop();
	}

}
