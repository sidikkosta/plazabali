/**
 * 
 */
package com.uns.util;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAccount;
import org.compiere.model.MActivity;
import org.compiere.model.MBPartner;
import org.compiere.model.MCharge;
import org.compiere.model.MCostElement;
import org.compiere.model.MLocator;
import org.compiere.model.MOrg;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUOM;
import org.compiere.model.MWarehouse;
import org.compiere.process.SvrProcess;
import org.compiere.util.CCache;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Trx;

/**
 * 
 * This Class use for minimizing hard code ID for {@link MProduct},
 * {@link MCharge}, {@link MCostElement}, {@link MUOM}.
 * 
 * @author Az's. Modified by ITD-Andy
 * 
 *         SOME ADDITIONAL CAN WRITE LAST COUNT (DO NOT INSERT) MAKE SURE ADD
 *         SOME CODE AT METHOD STATIC
 * 
 */
public class UNSApps extends SvrProcess {

	private static int count = 0;

	/** Charge Bunga Angsuran Pinjaman */
	public static final int CHRG_BungaAngsuran = ++count;
	/** Denda keterlambatan pembayaran angsuran. */
	public static final int CHRG_DendaAngsuran = ++count;
	/** Pembayaran Angsuran Pinjaman. */
	public static final int CHRG_AngsuranP = ++count;
	/** Charge Voucher BBM */
	public static final int CHRG_VBBM = ++count;
	/** Charge Voucher Ship */
	public static final int CHRG_VSHIP = ++count;
	/**Charge Claim discount */
	public static final int CHRG_DISKONSUSULAN = ++count;
	/**Charge Intransit Cash/Bank */
	public static final int CHRG_INTRANSITCASH = ++count;
	/**Charge Hutang Gaji */
	public static final int CHRG_HUTANG_GAJI = ++count;
	/**Charge Pos Silang**/
	public static final int CHRG_PSL = ++count;
	/**Locator Tumpukan (Pile) Cangkang **/
	public static final int LOC_PILE_CS = ++count;
	/** Potongan Buah Busuk FFB*/
	public static final int POT_BH_BUSUK = ++count;
	/** Inventory Difference from The In transit Inventory Move*/
	public static final int CHRG_INTRANSITINVDIFF = ++count;
	
	/** Charge Pengembalian Bunga Angsuran Pinjaman */
	public static final int CHRG_PiutangPokokAngsuran = ++count;
	/** Charge Bunga Angsuran Pinjaman */
	public static final int CHRG_PiutangBungaAngsuran = ++count;
	/** Pembayaran denda di belakang. */
	public static final int CHRG_PiutangDendaAngsuran = ++count;
	
	/** Setoran Simpanan Wajib Anggota */
	public static final int CHRG_SimpananWajib = ++count;
	/** Setoran Simpanan Pokok Anggota */
	public static final int CHRG_SimpananPokok = ++count;
	/** Setoran Simpanan Suka Rela Anggota */
	public static final int CHRG_SimpananSukaRela = ++count;
	
	/** Setoran Simpanan Wajib Anggota */
	public static final int CHRG_PiutangSimpananWajib = ++count;
	/** Setoran Simpanan Pokok Anggota */
	public static final int CHRG_PiutangSimpananPokok = ++count;
	/** Setoran Simpanan Suka Rela Anggota */
	public static final int CHRG_PiutangSimpananSukaRela = ++count;
	
	/** Charge Pencairan Pinjaman */
	public static final int CHRG_PencairanPinjaman = ++count;
	
	/** Pengambilan Simpanan Wajib Anggota. */
	public static final int CHRG_PSimpananWajib = ++count;
	/** Charge Pengembalian simpanan SukaRela */
	public static final int CHRG_PSimpananSukaRela = ++count;
	
	/** Hutang SHU Account */
	public static final int ACCT_HSHU = ++count;
	
	/** Pembayaran Denda **/
	public static final int CHRG_PmbayranDenda = ++count;
	/** Pemutihan denda **/
	public static final int CHRG_PmutihanDenda = ++count;
	/** Piutang Anggota di POS Belum di tagih **/
	public static final int CHRG_PtgBlmDtgh = ++count;
	/** Cash di POS */
	public static final int CHRG_CashDPOS = ++count;
	/** Voucher Return */
	public static final int ACCT_ReturnVcr = ++count;
	/** Charge Cash Voucher */
	public static final int CHRG_CashVoucher = ++count;
	/** Account for cash voucher */
	public static final int ACCT_CashVoucher = ++count;
	/** Account for Dana Anggota */
	public static final int ACCT_DanaAnggota = ++count;
	/** Charge Pendapatan Penjualan */
	public static final int CHRG_PendptnPnjualn = ++count;
	/** Charge Piutang InterCompany */
	public static final int CHRG_PiutangIntercomp = ++ count;
	/** Charge Hutang InterCompany */
	public static final int CHRG_HutangIntercomp = ++ count;
	/** Charge POS Ayat Silang Kas */
	public static final int CHRG_POSAyatSilang = ++ count;
	/** Charge Biaya PPH 21 **/
	public static final int CHRG_BiayaPPH21 = ++count;
	
	/**
	 * SOME ADDITIONAL STATIC CONTEXT CAN BE ADDED RIGHT ABOVE THIS COMMENT MAKE SURE ALSO ADD SOME CODE
	 * AT THE STATIC BLOCK.
	 */

	private static final int JML_CONTEXT = ++count;

	static final String[] contexts = new String[JML_CONTEXT];
	static final int[] reference_type = new int[JML_CONTEXT];
	static final int[] table_id = new int[JML_CONTEXT];
	static final String[] column_name = new String[]
	{
		I_UNS_AppsContext.COLUMNNAME_M_Product_ID,
		I_UNS_AppsContext.COLUMNNAME_C_Charge_ID,
		I_UNS_AppsContext.COLUMNNAME_M_CostElement_ID,
		I_UNS_AppsContext.COLUMNNAME_C_UOM_ID,
		I_UNS_AppsContext.COLUMNNAME_AD_OrgTrx_ID,
		I_UNS_AppsContext.COLUMNNAME_M_Warehouse_ID,
		I_UNS_AppsContext.COLUMNNAME_M_Locator_ID,
		I_UNS_AppsContext.COLUMNNAME_C_BPartner_ID,
		I_UNS_AppsContext.COLUMNNAME_C_Activity_ID,
		"C_ValidCombination_ID"
	};
	
	/** POS SERVER TYPE */
	public static final String SERVER_TYPE = MSysConfig.getValue(MSysConfig.SERVER_TYPE, 
			MSysConfig.getValue(MSysConfig.POS_SERVER, "POS"));
	public static final String SERVER_MAIN = MSysConfig.getValue(MSysConfig.MAIN_SERVER, "MAIN");
	public static final String SERVER_LOCATION = MSysConfig.getValue(MSysConfig.LOCATION_SERVER, "TERMINAL");
	public static final String SERVER_POS = MSysConfig.getValue(MSysConfig.POS_SERVER, "POS");
	/** END POS SERVER TYPE */
	
	/**
	 * Store and cache the loaded reference id here.
	 */
	public static CCache<Integer, Integer> i_Cache = new CCache<Integer, Integer>(
			null, "AppsContext", JML_CONTEXT, 120, false);
	static final int[] cache = new int[JML_CONTEXT];

	static final int reference_type_product = 0;
	static final int reference_type_charge = 1;
	static final int reference_type_costElement = 2;
	static final int reference_type_UOM = 3;
	static final int reference_type_ORG = 4;
	static final int reference_type_WHS = 5;
	static final int reference_type_Locator = 6;
	static final int reference_type_Business_Partner = 7;
	static final int reference_type_ACTIVITY = 8;
	static final int reference_type_Account = 9;

	/**
	 * Store and cache the loaded table id here.
	 */
	static final int table_Product = MProduct.Table_ID;
	static final int table_Charge = MCharge.Table_ID;
	static final int table_CostElememt = MCostElement.Table_ID;
	static final int table_UOM = MUOM.Table_ID;
	static final int table_ORG = MOrg.Table_ID;
	static final int table_WHS = MWarehouse.Table_ID;
	static final int table_Locator = MLocator.Table_ID;
	static final int table_BP = MBPartner.Table_ID;
	static final int table_ACTIVITY = MActivity.Table_ID;
	static final int table_Account = MAccount.Table_ID;

	static {
		table_id[CHRG_AngsuranP] = table_Charge;
		reference_type[CHRG_AngsuranP] = reference_type_charge;
		contexts[CHRG_AngsuranP] = "Angsuran Pinjaman";
		
		table_id[CHRG_BungaAngsuran] = table_Charge;
		reference_type[CHRG_BungaAngsuran] = reference_type_charge;
		contexts[CHRG_BungaAngsuran] = "Bunga Angsuran Pinjaman";
		
		table_id[CHRG_PiutangPokokAngsuran] = table_Charge;
		reference_type[CHRG_PiutangPokokAngsuran] = reference_type_charge;
		contexts[CHRG_PiutangPokokAngsuran] = "Piutang Pokok Angsuran";
		
		table_id[CHRG_PiutangBungaAngsuran] = table_Charge;
		reference_type[CHRG_PiutangBungaAngsuran] = reference_type_charge;
		contexts[CHRG_PiutangBungaAngsuran] = "Piutang Bunga Angsuran";
		table_id[CHRG_VBBM] = table_Charge;
		reference_type[CHRG_VBBM] = reference_type_charge;
		contexts[CHRG_VBBM] = "CHARGE_VBBM";

		table_id[CHRG_VSHIP] = table_Charge;
		reference_type[CHRG_VSHIP] = reference_type_charge;
		contexts[CHRG_VSHIP] = "CHARGE_VSHIP";
		
		table_id[CHRG_DISKONSUSULAN] = table_Charge;
		reference_type[CHRG_DISKONSUSULAN] = reference_type_charge;
		contexts[CHRG_DISKONSUSULAN] = "Charge Diskon Susulan";
		
		table_id[CHRG_PencairanPinjaman] = table_Charge;
		reference_type[CHRG_PencairanPinjaman] = reference_type_charge;
		contexts[CHRG_PencairanPinjaman] = "Charge Pencairan Dana Pinjaman";
		
		table_id[CHRG_SimpananWajib] = table_Charge;
		reference_type[CHRG_SimpananWajib] = reference_type_charge;
		contexts[CHRG_SimpananWajib] = "Charge Setoran Simpanan Wajib";
		
		table_id[CHRG_SimpananPokok] = table_Charge;
		reference_type[CHRG_SimpananPokok] = reference_type_charge;
		contexts[CHRG_SimpananPokok] = "Charge Setoran Simpanan Pokok";
		
		table_id[CHRG_SimpananSukaRela] = table_Charge;
		reference_type[CHRG_SimpananSukaRela] = reference_type_charge;
		contexts[CHRG_SimpananSukaRela] = "Charge Setoran Simpanan Sukarela";
		
		table_id[CHRG_PiutangSimpananWajib] = table_Charge;
		reference_type[CHRG_PiutangSimpananWajib] = reference_type_charge;
		contexts[CHRG_PiutangSimpananWajib] = "Charge Piutang Simpanan Wajib";
		
		table_id[CHRG_PiutangSimpananPokok] = table_Charge;
		reference_type[CHRG_PiutangSimpananPokok] = reference_type_charge;
		contexts[CHRG_PiutangSimpananPokok] = "Charge Piutang Simpanan Pokok";
		
		table_id[CHRG_PiutangSimpananSukaRela] = table_Charge;
		reference_type[CHRG_PiutangSimpananSukaRela] = reference_type_charge;
		contexts[CHRG_PiutangSimpananSukaRela] = "Charge Piutang Simpanan Suka Rela";
		
		table_id[CHRG_DendaAngsuran] = table_Charge;
		reference_type[CHRG_DendaAngsuran] = reference_type_charge;
		contexts[CHRG_DendaAngsuran] = "Charge Pembayaran Denda Angsuran";
		
		table_id[CHRG_PiutangDendaAngsuran] = table_Charge;
		reference_type[CHRG_PiutangDendaAngsuran] = reference_type_charge;
		contexts[CHRG_PiutangDendaAngsuran] = "Charge Piutang Denda Angsuran";
		
		table_id[CHRG_PSimpananWajib] = table_Charge;
		reference_type[CHRG_PSimpananWajib] = reference_type_charge;
		contexts[CHRG_PSimpananWajib] = "Charge Pengembalian Simpanan Wajib";

		table_id[CHRG_INTRANSITCASH] = table_Charge;
		reference_type[CHRG_INTRANSITCASH] = reference_type_charge;
		contexts[CHRG_INTRANSITCASH] = "Charge Intransit Cash";
		
		table_id[CHRG_PSimpananSukaRela] = table_Charge;
		reference_type[CHRG_PSimpananSukaRela] = reference_type_charge;
		contexts[CHRG_PSimpananSukaRela] = "Charge Pengembalian Simpanan Suka Rela";
		table_id[CHRG_HUTANG_GAJI] = table_Charge;
		reference_type[CHRG_HUTANG_GAJI] = reference_type_charge;
		contexts[CHRG_HUTANG_GAJI] = "CHRG_HUTANG_GAJI";
		
		table_id[CHRG_PSL] = table_Charge;
		reference_type[CHRG_PSL] = reference_type_charge;
		contexts[CHRG_PSL] = "Charge Pos Silang";
		
		table_id[ACCT_HSHU] = table_Account;
		reference_type[ACCT_HSHU] = reference_type_Account;
		contexts[ACCT_HSHU] = "Account SHU";
		table_id[LOC_PILE_CS] = table_Locator;
		reference_type[LOC_PILE_CS] = reference_type_Locator;
		contexts[LOC_PILE_CS] = "LOC_PILE_CS";
		
		table_id[CHRG_PmbayranDenda] = table_Charge;
		reference_type[CHRG_PmbayranDenda] = reference_type_charge;
		contexts[CHRG_PmbayranDenda] = "Charge Pembayaran Denda";
		table_id[POT_BH_BUSUK] = table_Charge;
		reference_type[POT_BH_BUSUK] = reference_type_charge;
		contexts[POT_BH_BUSUK] = "POT_BH_BUSUK";

		table_id[CHRG_INTRANSITINVDIFF] = table_Charge;
		reference_type[CHRG_INTRANSITINVDIFF] = reference_type_charge;
		contexts[CHRG_INTRANSITINVDIFF] = "CHRG_INTRANSITINVDIFF";
		
		table_id[CHRG_PmutihanDenda] = table_Charge;
		reference_type[CHRG_PmutihanDenda] = reference_type_charge;
		contexts[CHRG_PmutihanDenda] = "Charge Pemutihan Denda";
		
		table_id[CHRG_PtgBlmDtgh] = table_Charge;
		reference_type[CHRG_PtgBlmDtgh] = reference_type_charge;
		contexts[CHRG_PtgBlmDtgh] = "Charge Piutang Toko Belum Ditagih";
		
		table_id[CHRG_CashDPOS] = table_Charge;
		reference_type[CHRG_CashDPOS] = reference_type_charge;
		contexts[CHRG_CashDPOS] = "Charge Cash Di POS";
		
		table_id[ACCT_ReturnVcr] = table_Account;
		reference_type[ACCT_ReturnVcr] = reference_type_Account;
		contexts[ACCT_ReturnVcr] = "Acount Voucher Return";
		
		table_id[CHRG_CashVoucher] = table_Charge;
		reference_type[CHRG_CashVoucher] = reference_type_charge;
		contexts[CHRG_CashVoucher] = "Charge Cash Voucher";
		
		table_id[ACCT_CashVoucher] = table_Account;
		reference_type[ACCT_CashVoucher] = reference_type_Account;
		contexts[ACCT_CashVoucher] = "Acount Voucher Cash";
		
		table_id[ACCT_DanaAnggota] = table_Account;
		reference_type[ACCT_DanaAnggota] = reference_type_Account;
		contexts[ACCT_DanaAnggota] = "Account Dana Anggota";
		
		table_id[CHRG_PendptnPnjualn] = table_Charge;
		reference_type[CHRG_PendptnPnjualn] = reference_type_charge;
		contexts[CHRG_PendptnPnjualn] = "Charge Pendapatan Penjualan";
		
		table_id[CHRG_PiutangIntercomp] = table_Charge;
		reference_type[CHRG_PiutangIntercomp] = reference_type_charge;
		contexts[CHRG_PiutangIntercomp] = "Piutang Intercompany";
		
		table_id[CHRG_HutangIntercomp] = table_Charge;
		reference_type[CHRG_HutangIntercomp] = reference_type_charge;
		contexts[CHRG_HutangIntercomp] = "Hutang Intercompany";
		
		table_id[CHRG_POSAyatSilang] = table_Charge;
		reference_type[CHRG_POSAyatSilang] = reference_type_charge;
		contexts[CHRG_POSAyatSilang] = "POS Ayat Silang";
		
		table_id[CHRG_BiayaPPH21] = table_Charge;
		reference_type[CHRG_BiayaPPH21] = reference_type_charge;
		contexts[CHRG_BiayaPPH21] = "Biaya PPH 21";		
	}

	/**
	 * 
	 * @param context
	 * @return
	 */
	public static Integer getRef(int contextID) {
		return Integer.valueOf(getRefAsInt(contextID));
	}

	/**
	 * 
	 * @param context
	 * @return
	 */
	public static int getRefAsInt(int contextID) {
		/** First try to get it from the cache. reference */
		// int referenceID = cache[contextID];
		Integer referenceID = i_Cache.get(contextID);

		if (referenceID != null && referenceID != 0) {
			return referenceID;
		}
		/** If not in cache yet, then try to get it from database. */

		MUNSAppsContext mAppsCtx = null;
		String TrxName = Trx.createTrxName();

		mAppsCtx = MUNSAppsContext.get(contextID, Env.getCtx(), TrxName);

		if (mAppsCtx == null) {
			return -1;
//			loadContextToDB(contextID, Env.getCtx(), TrxName);
//			mAppsCtx = MUNSAppsContext.get(contextID, Env.getCtx(), TrxName);
		}
		
//		switch (reference_type[contextID]) {
//		
//		case reference_type_product:
//			referenceID = mAppsCtx.getM_Product_ID();
//			break;
//		case reference_type_charge:
//			referenceID = mAppsCtx.getC_Charge_ID();
//			break;
//		case reference_type_costElement:
//			referenceID = mAppsCtx.getM_CostElement_ID();
//			break;
//		case reference_type_UOM:
//			referenceID = mAppsCtx.getC_UOM_ID();
//			break;
//		case reference_type_ORG:
//			referenceID = mAppsCtx.getAD_OrgTrx_ID();
//			break;
//		case reference_type_WHS:
//			referenceID = mAppsCtx.getM_Warehouse_ID();
//			break;
//		case reference_type_Locator:
//			referenceID = mAppsCtx.getM_Locator_ID();
//			break;
//		case reference_type_Business_Partner:
//			referenceID = mAppsCtx.getC_BPartner_ID();
//			break;
//		case reference_type_ACTIVITY:
//			referenceID = mAppsCtx.getC_Activity_ID();
//			break;
//		case reference_type_Account :
//			referenceID = mAppsCtx.getC_ValidCombination_ID();
//		}
		
		referenceID = mAppsCtx.get_ValueAsInt(column_name[reference_type[contextID]]);
		
		if (referenceID != 0) {
			/** Put the loaded referenceID to the cache. **/
			i_Cache.put(contextID, referenceID);
			// cache[contextID] = referenceID;
		} else {
			throw new AdempiereException(
					"Fatal Error: You have not defined the ReferenceID in DB for "
							+ "the UNS-Context of:[" + contexts[contextID]
							+ "].");
		}
		return referenceID;
	}

	public int testReference() {
		Integer testGetProduct = UNSApps.getRef(CHRG_VBBM);

		return testGetProduct;
	}

	/**
	 * 
	 * @return
	 */
	public String loadUndefinedUNSContext(Properties ctx, String trxName) {
		String sql = "Delete FROM UNS_AppsContext";
		boolean success = DB.executeUpdate(sql, trxName) != -1;
		sql = "Delete FROM UNS_AppsReff";
		if(success)
			success = DB.executeUpdate(sql, trxName) != -1;
		if(!success)
		{
			throw new AdempiereException("Failed on delete previous context");
		}
		
		int n = 0;
		String msg = "The number of records in Apps Contect is ";
		for (int i = 1; i < contexts.length; i++) {
			loadContextToDB(i, ctx, trxName);
			n++;
		}
		return msg + n;
	}

	/**
	 * 
	 * @param DefinedContextID
	 */
	protected void loadContextToDB(int DefinedContextID, Properties ctx,
			String TrxName) {
		String DefinedContext = contexts[DefinedContextID];
		if (DefinedContext == null) {
			throw new AdempiereException(
					"Fatal Error: You have not defined the context name "
							+ "for id of:[" + DefinedContextID + "]");
		}
		int appsRefID = DB
				.getSQLValue(
						TrxName,
						"SELECT UNS_AppsReff_ID FROM UNS_AppsReff WHERE UNS_AppsReff_ID=?",
						DefinedContextID);
		
		MUNSAppsReff UNSAppsReff = null;
		MUNSAppsContext AppsContext = null;
		if (appsRefID == -1) {
			// create single record
			UNSAppsReff = new MUNSAppsReff(ctx, 0, TrxName);
			AppsContext = new MUNSAppsContext(ctx, 0, TrxName);
		} else {
			UNSAppsReff = new MUNSAppsReff(ctx, MUNSAppsReff.getAllIDs(
					MUNSAppsReff.Table_Name, "UNS_AppsReff_ID=" + appsRefID,
					TrxName)[0], TrxName);
			AppsContext = new MUNSAppsContext(ctx, MUNSAppsContext.getAllIDs(
					MUNSAppsContext.Table_Name, "UNS_AppsReff_ID=" + appsRefID,
					TrxName)[0], TrxName);
		}

		UNSAppsReff.setName(contexts[DefinedContextID]);
		UNSAppsReff.setUNS_AppsReff_ID(DefinedContextID);
		if (!UNSAppsReff.save()) {
			throw new AdempiereException(
					"Fatal Error: Problem when initializing Apps-Ref for context of:["
							+ DefinedContext + "]");
		}

		AppsContext.setUNS_AppsReff_ID(DefinedContextID);
		AppsContext.setAD_Table_ID(table_id[DefinedContextID]);
		String columnName = column_name[reference_type[DefinedContextID]];
		int value = getLastContextValue(UNSAppsReff.getName());
		AppsContext.set_ValueOfColumn(columnName, value == -1 ? null :value);
		if (!AppsContext.save()) {
			throw new AdempiereException(
					"Fatal Error: Problem when initializing Apps-Context  for context of:["
							+ DefinedContext + "]" + CLogger.retrieveErrorString("unknown"));
		}
	}

	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		// No action
	}

	@Override
	protected String doIt() throws Exception {
		loadLastContext();
		return this.loadUndefinedUNSContext(getCtx(), get_TrxName());
	}
	
	private int getLastContextValue (String name)
	{
		int lastctxvalue_ID = -1;
		for (int i=0; m_lastContext != null && i<m_lastContext.size(); i++)
		{
			if (m_lastContext.get(i).getName().equals(name))
			{
				lastctxvalue_ID = m_lastContext.get(i).getKey();
				break;
			}
		}
		
		return lastctxvalue_ID;
	}
	
	private List<KeyNamePair> m_lastContext;
	
	public  void loadLastContext ()
	{
		m_lastContext = new ArrayList<>();
		String sql = "SELECT UNS_AppsReff_ID, Name FROM UNS_AppsReff" ;
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			rs = st.executeQuery();
			while (rs.next())
			{
				sql = "SELECT UNS_AppsContext_ID FROM UNS_AppsContext WHERE UNS_AppsReff_ID = ?"
						+ " AND IsActive = ?";
				int record_ID = DB.getSQLValue(get_TrxName(), sql, rs.getInt(1), "Y");
				int columnnameIdx = getColumnNameIdx(rs.getString(2));
				if (columnnameIdx == -1) continue;
				String columnName = column_name[columnnameIdx];
				sql = "SELECT " + columnName + " FROM UNS_AppsContext WHERE UNS_AppsContext_ID = ?";
				int value = DB.getSQLValue(get_TrxName(), sql, record_ID);
				if (value <=0)
					continue;
				
				m_lastContext.add(new KeyNamePair(value, rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	private int getColumnNameIdx (String name)
	{
		for (int i=1; contexts != null && i<contexts.length; i++)
		{
			if (!contexts[i].equals(name))
				continue;
			
			return reference_type[i];
		}
		
		return -1;
	}
}
