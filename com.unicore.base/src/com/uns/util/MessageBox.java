package com.uns.util;

import java.util.List;
import java.util.Properties;

import javax.swing.JComponent;

import org.adempiere.base.Service;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Ini;

/**
 * 
 * @author Haryadi
 *
 */
public class MessageBox {

	public static final int OK = it.businesslogic.ireport.gui.MessageBox.OK;
	public static final int OKCANCEL = it.businesslogic.ireport.gui.MessageBox.OKCANCEL;
	public static final int YESNO = it.businesslogic.ireport.gui.MessageBox.YESNO;
	public static final int YESNOCANCEL = it.businesslogic.ireport.gui.MessageBox.YESNOCANCEL;

	public static final int ICONERROR = it.businesslogic.ireport.gui.MessageBox.ICONERROR;
	public static final int ICONINFORMATION = it.businesslogic.ireport.gui.MessageBox.ICONINFORMATION;
	public static final int ICONQUESTION = it.businesslogic.ireport.gui.MessageBox.ICONQUESTION;
	public static final int ICONWARNING = it.businesslogic.ireport.gui.MessageBox.ICONWARNING;
	
	public static final int RETURN_OK = 0;
	public static final int RETURN_YES = 0;
	public static final int RETURN_NO = 1;
	public static final int RETURN_CANCEL = 2;
	public static final int RETURN_NONE = -1;
	
	public MessageBox() {
	}

	/**
	 * 
	 * @param msg
	 * @param title
	 * @param btn
	 * @param icon
	 * @return
	 * @throws InterruptedException
	 */
	public static int showMsg(PO po, Properties ctx, String msg, String title, int btn, int icon)
	{
		IMsgBoxFactory msgFactory = getFactory();
		if (msgFactory == null)
			throw new AdempiereException("Fatal Error: Cannot found message factory for " +
										 (Ini.isClient()? "Desktop Client." : "Web Client."));
		
		return msgFactory.showMsg(po, ctx, msg, title, btn, icon);
	}
	
	/**
	 * Create a message box linked with ProcessInfo instance.
	 * This method is to enable message dialog-box on web client. 
	 * 
	 * @param ctx
	 * @param pi
	 * @param msg
	 * @param title
	 * @param btn
	 * @param icon
	 * @return
	 */
	public static int showMsg(Properties ctx, ProcessInfo pi, String msg, String title, int btn, int icon)
	{
		IMsgBoxFactory msgFactory = getFactory();
		if (msgFactory == null)
			throw new AdempiereException("Fatal Error: Cannot found message factory for " +
										 (Ini.isClient()? "Desktop Client." : "Web Client."));
		
		return msgFactory.showMsg(ctx, pi, msg, title, btn, icon);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param component
	 * @param msg
	 * @param title
	 * @param btn
	 * @param icon
	 * @return
	 */
	public static int showMsg(PO po, Properties ctx, JComponent component, String msg, String title, int btn, int icon)
	{
		IMsgBoxFactory msgFactory = getFactory();
		if (msgFactory == null)
			throw new AdempiereException("Fatal Error: Cannot found message factory for " +
										 (Ini.isClient()? "Desktop Client." : "Web Client."));
		
		return msgFactory.showMsg(po, ctx, component, msg, title, btn, icon);
	}
	
	/**
	 * Get the proper message factory to return a message.
	 * @return
	 */
	static IMsgBoxFactory getFactory()
	{
		IMsgBoxFactory msgFactory = null;
		boolean isClient = Ini.isClient();
		List<IMsgBoxFactory> factories = Service.locator().list(IMsgBoxFactory.class).getServices();
		for (IMsgBoxFactory factory : factories) 
		{
			msgFactory = factory;
			if (isClient) {
				if (factory.isDesktopClient())
					break;
			}
			else if (!factory.isDesktopClient())
				break;
				
		}
		return msgFactory;
	}	
}
