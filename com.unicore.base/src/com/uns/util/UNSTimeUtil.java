/**
 * 
 */
package com.uns.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;
import org.compiere.util.TimeUtil;

/**
 * @author AzHaidar
 *
 */
public class UNSTimeUtil extends TimeUtil {

	/**
	 * Since Indonesia locale is the same with UK, then we just use it.
	 * 1 week is days range from mon-sun. 
	 */
	public static Locale DEFAULT_LOCALE = Locale.UK;
	
	
	/**
	 * 
	 */
	public UNSTimeUtil() {
	}
	
	/**
	 * Production week in a year has values 1 to 53.
	 * Week 1 has date range from 1-Jan to the date of Saturday.
	 * Week 53 has date range from date of Sunday of latest week of December to date of 31-Dec.
	 * Since Indonesia's locale is the same with UK, so we use Locale.UK.
	 * 1 week is days range from mon-sun. 
	 * 
	 * @param year
	 * @param weekNo
	 * @return the first date of the given week considered as production week.
	 */
	public static Timestamp getProductionWeekStartDate(int year, int weekNo)
	{
		Calendar cal = Calendar.getInstance(DEFAULT_LOCALE); 
		Timestamp theDate = null;
				
		if (weekNo == 1) {
			cal.set(cal.get(Calendar.YEAR), 0, 1);
		}
		else if (weekNo == 53) {
			cal.set(cal.get(Calendar.YEAR), 11, 31);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			int addBy = (day>1)? (day-2) * -1 : -6;
			theDate = addDays(new Timestamp(cal.getTimeInMillis()), addBy);
			return theDate;
		}
		else {
			cal.set(cal.get(Calendar.YEAR), 0, 1);
			int weekNoTmp = cal.get(Calendar.WEEK_OF_YEAR);
			if (weekNoTmp == 53)
				weekNo--;
			
			cal.set(Calendar.WEEK_OF_YEAR, weekNo);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			int addBy = (day>1)? (day-2) * -1 : -6;
			theDate = addDays(new Timestamp(cal.getTimeInMillis()), addBy);
			return theDate;
		}
		theDate = new Timestamp (cal.getTimeInMillis());
		return theDate;
	}
	
	
	/**
	 * Production week in a year has values 1 to 53.
	 * Week 1 has date range from 1-Jan to the date of Saturday.
	 * Week 53 has date range from date of Sunday of latest week of December to date of 31-Dec.
	 * Since Indonesia's locale is the same with UK, so we use Locale.UK.
	 * 1 week is days range from mon-sun. 
	 * 
	 * @param year
	 * @param weekNo
	 * @return the end date of the given week considered as production week.
	 */
	public static Timestamp getProductionWeekEndDate(int year, int weekNo)
	{
		Calendar cal = Calendar.getInstance(DEFAULT_LOCALE);
		Timestamp theDate = null;
		
		if (weekNo == 1) {
			cal.set(cal.get(Calendar.YEAR), 0, 1);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			int addBy = (day>1)? (8-day): 0;
			theDate = addDays(new Timestamp(cal.getTimeInMillis()), addBy);
			return theDate;
		}
		else if (weekNo == 53) {
			cal.set(cal.get(Calendar.YEAR), 11, 31);
		}
		else {
			cal.set(cal.get(Calendar.YEAR), 0, 1);
			int weekNoTmp = cal.get(Calendar.WEEK_OF_YEAR);
			if (weekNoTmp == 53)
				weekNo--;			
			cal.set(Calendar.WEEK_OF_YEAR, weekNo);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			int addBy = (day>1)? (8-day): 0;
			theDate = addDays(new Timestamp(cal.getTimeInMillis()), addBy);
			return theDate;
		}
		theDate = new Timestamp (cal.getTimeInMillis());
		return theDate;
	}
	
	/**
	 * Since Indonesia's locale is the same with UK, so we use Locale.UK.
	 * 1 week is days range from mon-sun.
	 *  
	 * @param ts
	 * @return
	 */
	public static int getProductionWeekNo(Timestamp ts)
	{
		int weekNo;
		Calendar cal = Calendar.getInstance(DEFAULT_LOCALE);
		cal.setTimeInMillis(ts.getTime());
		
		weekNo = cal.get(Calendar.WEEK_OF_YEAR);
		int month = cal.get(Calendar.MONTH);
		if (weekNo == 1 && month == 11) {
			weekNo = 53;
		}
		else if (weekNo == 53 && month == 0) {
			weekNo = 1;
		}
		else {
			//Test 1-Jan, what is the weekno considered by system?
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DATE, 1);
			ts = new Timestamp (cal.getTimeInMillis());
			int weekNoTmp = cal.get(Calendar.WEEK_OF_YEAR);
			if(weekNoTmp == 53) { 
				if(weekNo == 53)
					weekNo = 1;
				else
					weekNo++;	
			}
			else if(weekNo == 54)
				weekNo--;
		}		
		return weekNo;
	}
	
	/**
	 * Get the cut off week date for the given date.
	 *  
	 * @param date
	 * @param cutOffWeekDay
	 * @return
	 */
	public static Timestamp getNextCutOffWeekDay(Timestamp date, int cutOffWeekDay)
	{
		Timestamp theNextCutOffWeekDay = date;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		int currDay = cal.get(Calendar.DAY_OF_WEEK);
		
		if (currDay == cutOffWeekDay)
			return theNextCutOffWeekDay;
		
		int currToNextCutOffWeekDay = 0;
		
		if (currDay < cutOffWeekDay)
			currToNextCutOffWeekDay = cutOffWeekDay - currDay;
		else 
			currToNextCutOffWeekDay = 7 - (currDay - cutOffWeekDay);
		
		 cal.add(Calendar.DATE, currToNextCutOffWeekDay);
		 
		 theNextCutOffWeekDay = new Timestamp(cal.getTimeInMillis());
		
		return theNextCutOffWeekDay;
	}
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public BigDecimal getHoursBetween (Timestamp start, Timestamp end)
	{
		long startMilis = start.getTime();
		long endMillis = end.getTime();
		long durationMilis = endMillis - startMilis;
		double durationHours = (double) durationMilis / 1000 / 60 / 60;
		BigDecimal hours = new BigDecimal(durationHours);
		return hours;
	}
}
