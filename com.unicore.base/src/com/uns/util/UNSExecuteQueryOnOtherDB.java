/**
 * 
 */
package com.uns.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.base.UNSDefaultModelFactory;
import com.uns.base.model.Query;
import com.uns.model.X_UNS_ListIP;

/**
 * @author Burhani Adam
 *
 */
public class UNSExecuteQueryOnOtherDB extends SvrProcess {

	/**
	 * 
	 */
	public UNSExecuteQueryOnOtherDB() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_listIPID = 0;
	private String p_sql = null;
	private String p_Path = null;
	private IProcessUI m_process;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("UNS_ListIP_ID"))
				p_listIPID = param.getParameterAsInt();
			else if(param.getParameterName().equals("SQLStatement"))
				p_sql = param.getParameterAsString();
			else if(param.getParameterName().equals("File_Directory"))
				p_Path = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_process = Env.getProcessUI(getCtx());
		X_UNS_ListIP[] records = getList();
		String sql = null;
		if(!Util.isEmpty(p_sql, true))
			sql = p_sql;
		else
		{
			File newFile = new File(p_Path);
			FileReader fr = new FileReader(newFile);
			BufferedReader br = new BufferedReader(fr);
			int count = 0;
			String txt;
			while((txt = br.readLine()) != null)
			{
				if(count == 0)
					sql = txt;
				else
					sql += txt;
				count = 1;
			}
			br.close();
		}
		for(int i=0;i<records.length;i++)
		{
			m_process.statusUpdate("" + records.length + " Database. #" + i + "::" 
					+ records[i].getDBAddress().replace("jdbc:postgresql://", ""));
			String url = records[i].getDBAddress();
			String user = records[i].getUserName();
			String pass = records[i].getPassword();
			Properties prop = new Properties();
			prop.put("user", user);
			prop.put("password", pass);
			Connection connection = null;
			PreparedStatement st = null;
			try
			{
				Class.forName("org.postgresql.Driver");
				connection = DriverManager.getConnection(url, prop);
				st = connection.prepareStatement(sql);
				st.execute();
			}
			catch (SQLException | AdempiereException e) 
			{
				throw new AdempiereException(records[i].getDBAddress() + " - " + e.getMessage());
			}
			finally
			{
				try 
				{
					if (st != null)
						st.close();
					if (connection != null)
						connection.close();
				}
				catch (SQLException e) 
				{
					e.printStackTrace();
				}
			}
		}
		
		return "Success";
	}
	
	private X_UNS_ListIP[] getList()
	{
		String whereClause = "IsActive = 'Y'";
		if(p_listIPID > 0)
			whereClause = "UNS_ListIP_ID = " + p_listIPID;
		List<X_UNS_ListIP> list = Query.get(getCtx(), UNSDefaultModelFactory.EXTENSION_ID,
				X_UNS_ListIP.Table_Name, whereClause, get_TrxName()).list();
		
		return list.toArray(new X_UNS_ListIP[list.size()]);
	}
}