/**
 * 
 */
package com.uns.util;

import java.util.Properties;
import com.uns.model.MUNSUserWarning;

/**
 * @author nurse
 *
 */
public class UNSUserWarningLoader 
{
	private Properties m_ctx = null;
	private int m_clientID = 0;
	private int m_roleID = 0;
	private String m_trxName = null;
	private UNSUserWarningContentLoader[] m_contents = null;
	
	/**
	 * 
	 */
	public UNSUserWarningLoader(Properties ctx, int AD_Client_ID, int AD_Role_ID, 
			String trxName) 
	{
		super ();
		this.m_ctx = ctx;
		this.m_clientID = AD_Client_ID;
		this.m_roleID = AD_Role_ID;
		this.m_trxName = trxName;
	}

	public UNSUserWarningContentLoader[] load (boolean reload)
	{
		if (!reload && null != m_contents && m_contents.length > 0)
			return m_contents;
		
		MUNSUserWarning[] contents = MUNSUserWarning.getValid(m_ctx, m_trxName, 
				m_clientID, m_roleID);
		m_contents = new UNSUserWarningContentLoader[contents.length];
		for (int i=0; i<contents.length; i++)
		{
			MUNSUserWarning content = contents[i];
			UNSUserWarningContentLoader theContent = 
					new UNSUserWarningContentLoader(content);
			Thread t = new Thread(theContent);
			t.setName(theContent.getTitle());
			t.start();
			m_contents[i] = theContent;
		}
		
		for (int i=0; i<m_contents.length;)
		{
			if (m_contents[i].isRunning())
				continue;
			i++;
		}
		
		return m_contents;
	}
}