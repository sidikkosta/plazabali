package com.uns.util;

import java.util.Properties;

import javax.swing.JComponent;

import org.compiere.model.PO;
import org.compiere.process.ProcessInfo;

/**
 * 
 * @author Haryadi
 *
 */
public interface IMsgBoxFactory 
{
	/**
	 * Create a message box.
	 * @param po TODO
	 * @param ctx
	 * @param msg
	 * @param title
	 * @param btn
	 * @param icon
	 * 
	 * @return
	 */
	public int showMsg(PO po, Properties ctx, String msg, String title, int btn, int icon);
	
	/**
	 * Create a message box linked with ProcessInfo instance.
	 * 
	 * @param ctx
	 * @param pi
	 * @param msg
	 * @param title
	 * @param btn
	 * @param icon
	 * @return
	 */
	public int showMsg(Properties ctx, ProcessInfo pi, String msg, String title, int btn, int icon);
	
	/**
	 * Create a message box.
	 * @param po TODO
	 * @param ctx
	 * @param component
	 * @param msg
	 * @param title
	 * @param btn
	 * @param icon
	 * 
	 * @return
	 */
	public int showMsg(PO po, Properties ctx, JComponent component, String msg, String title, int btn, int icon);
	
	/**
	 * To indicate if this instance of IMsgBoxFactory is a desktop client or not.
	 * 
	 * @return true if it is a desktop client, false for web client/server.
	 */
	public boolean isDesktopClient();
}
