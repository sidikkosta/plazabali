/**
 * 
 */
package com.uns.util;

import java.util.Properties;

import org.compiere.util.Msg;
import org.compiere.util.ValueNamePair;

/**
 * @author Az's
 *
 */
public class ErrorMsg {

	
	private ErrorMsg(){	
	}
	
	/**
	 * Prepare message to show on error alert.
	 * 
	 * @param ctx
	 * @param AD_Message_Title
	 * @param AD_Message_Detail
	 */
	public static void setErrorMsg (Properties ctx, 
									String AD_Message_Title, String AD_Message_Detail){
		ValueNamePair vnp = 
				new ValueNamePair(Msg.getMsg(ctx, AD_Message_Title),//"Save Error"), 
								Msg.getMsg(ctx, AD_Message_Detail));//"NotAllowedBudgetAmount"));//m_sMsg);
		ctx.put("org.compiere.util.CLogger.lastError", vnp);
	}
}
