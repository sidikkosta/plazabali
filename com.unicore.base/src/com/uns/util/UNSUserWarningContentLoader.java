/**
 * 
 */
package com.uns.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSUserWarning;
import com.uns.model.MUNSUserWarningColumn;

/**
 * @author nurse
 *
 */
public class UNSUserWarningContentLoader implements Runnable 
{
	MUNSUserWarning m_config = null;
	private Vector<String> m_headers = null;
	private Vector<Vector<Object>> m_contents = null;
	private boolean m_running = false;
	private String m_msg = null;
	private boolean m_isError = false;
	private String m_title = null;
	private String m_description = null;
	
	public UNSUserWarningContentLoader (MUNSUserWarning content)
	{
		super ();
		this.m_config = content;
	}
	
	public synchronized void load (boolean reload)
	{
		if (!reload && m_contents != null && m_contents.size() > 0)
			return ;
		
		m_contents = new Vector<>();
		MUNSUserWarningColumn[] columns = m_config.getLines();
		int totalColumn = columns.length;
		this.m_headers = new Vector<>();
		for (int i=0; i<totalColumn; i++)
			m_headers.add(columns[i].getDescription());
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(this.m_config.getSQL(), null);
			rs = st.executeQuery();
			while (rs.next())
			{
				int x = 1;
				Vector<Object> columnValues = new Vector<>();
				while (x <= totalColumn)
				{
					columnValues.add(rs.getObject(x++));
				}
				m_contents.add(columnValues);
			}
		}
		catch (SQLException ex)
		{
			this.m_msg = ex.getMessage();
			this.m_isError = true;
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}

	@Override
	public void run() {
		this.m_running = true;
		load(false);
		this.m_running = false;
	}
	
	public String getSQLFrom ()
	{
		return this.m_config.getSQLFrom();
	}
	
	public String getSQLWhere ()
	{
		return this.m_config.getSQLWhere();
	}
	
	public String getSQLJoin ()
	{
		return this.m_config.getSQLJoin();
	}
	
	public String getSQL ()
	{
		return this.m_config.getSQL();
	}
	
	public Vector<Vector<Object>> getContents (boolean reload)
	{
		load(reload);
		return this.m_contents;
	}
	
	public Vector<String> getHeaders (boolean reload)
	{
		load(reload);
		return this.m_headers;
	}
	
	public String getTitle ()
	{
		if (this.m_title != null)
			return this.m_title;
		
		return m_title = Env.parseVariable(this.m_config.getName(), null, null, true);
	}
	
	public String getDescription ()
	{
		if (null != this.m_description)
			return this.m_description;
		
		return this.m_description = Env.parseVariable(this.m_config.getDescription(), 
				null, null, true);
	}
	
	public boolean isError ()
	{
		return this.m_isError;
	}
	
	public String getErrorMsg ()
	{
		if (!isError())
			return null;
		
		return this.m_msg;
	}
	
	public boolean isRunning ()
	{
		return this.m_running;
	}
}
