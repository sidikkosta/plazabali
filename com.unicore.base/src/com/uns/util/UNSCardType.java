/**
 * 
 */
package com.uns.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.compiere.util.DB;

/**
 * @author Burhani Adam
 *
 */
public class UNSCardType {
	
	private final static String m_trxName = "**InitializationCardTypeBaseValue**";

	/**
	 * 
	 */
	public UNSCardType() {
		super();
		initCardType();
	}
	
	private static Hashtable<String, Integer> m_cardType;
	
	public Integer get(String cardNo)
	{
		Integer id = m_cardType.get(cardNo);
		return id;	
	}
	
	public void initCardType ()
	{
		m_cardType = new Hashtable<>();
		String sql = "SELECT UNS_CardType_ID, Value FROM UNS_CardType WHERE IsActive = 'Y'";
		List<List<Object>> oo = DB.getSQLArrayObjectsEx(m_trxName, sql);
		if(oo == null)
			return;
		for (int i=0; i<oo.size(); i++)
		{
			String[] valuesPrefix = getParsedValue((String) oo.get(i).get(1));
			for (int j=0; j<valuesPrefix.length; j++)
			{
				Integer id = ((BigDecimal) oo.get(i).get(0)).intValue();
				m_cardType.put(valuesPrefix[j].trim(), id);
			}
		}
	}
	
	public String[] getParsedValue (String value)
	{
		List<String> values = parseValue(value, ",");
		String[] retVals = new String[values.size()];
		values.toArray(retVals);
		Arrays.sort(retVals);
		return retVals;
	}
	
	private List<String> parseValue (String value, String splited)
	{
		List<String> list = new ArrayList<>();
		String[] values = value.split(splited);
		for (int i=0; i<values.length; i++)
		{
			if (values[i].contains("-"))
				list.addAll(parseValue(values[i], "-"));
			else
				list.add(values[i]);
		}
		
		if (splited.equals("-") && list.size() == 2)
		{
			String startStr = list.get(0);
			String endStr = list.get(1);
			int start = -1;
			int end = -1;
			try
			{
				start = Integer.valueOf(startStr.trim());
				end = Integer.valueOf(endStr.trim());
			}
			catch (NumberFormatException ex)
			{
				ex.printStackTrace();
			}
			
			end--;
			
			while (end > start)
			{
				list.add(Integer.toString(end));
				end--;
			}
		}
		
		return list;
	}
}