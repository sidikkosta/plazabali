/**
 * 
 */
package com.uns.importer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MProduct;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.process.SimpleImportXLS;

/**
 * @author AzHaidar
 *
 */
public class ProductModificationImporter implements ImporterValidation {

	//private static CLogger log = CLogger.getCLogger(ProductModificationImporter.class);

	//private Properties m_ctx = null;
	private String m_trxName = null;
	Hashtable<String, PO> m_poRefMap = null;
	Hashtable<String, Object> m_freeColVals;

	static final String COL_KdProduk 	= "KdProduk";
	static final String COL_NmProduk	= "NmProduk";
	static final String COL_IsActive	= "IsActive";
	static final String COL_NewProductCategory_ID = "NewProductCategory_ID";
	static final String COL_NewUOM			= "NewUOM";
	static final String COL_NewUOMConvL1	= "NewUOMConvL1";
	static final String COL_NewUOMConvL2	= "NewUOMConvL2";
	
	static final int	UOM_Element_ID = 215;
	static final int	UOMConvL1_Element_ID = 1001181;
	static final int	UOMConvL2_Element_ID = 1001182;
	
	static final String[] UOMUsedMap = new String[]{"C_OrderLine", "UNS_PreOrder_Line", "M_InOutLine", 
		"C_InvoiceLine", "UNS_PackingList_Line", "UNS_PL_ConfirmProduct"};
	static final Hashtable<String, Integer> UOMConversionUsedMap = new Hashtable<String, Integer>();
	
	static 
	{
//		StringBuilder sql = 
//				new StringBuilder("SELECT Distinct(t.TableName) FROM AD_Column c ")
//				.append("INNER JOIN AD_Table t ON c.AD_Table_ID=t.AD_Table_ID WHERE c.AD_Element_ID = ")
//				.append(UOM_Element_ID).append(" AND c.AD_Table_ID != 208 AND c.ColumnSQL IS NULL ")
//				.append(" AND t.isView='N'");
//		
//		PreparedStatement stmt = DB.prepareStatement(sql.toString(), null);
//		
//		try {
//			ResultSet rs = stmt.executeQuery();
//			
//			while (rs.next()) {
//				UOMUsedMap.put(rs.getString(1), UOM_Element_ID);
//			}
//		}
//		catch (SQLException ex) {
//			ex.printStackTrace();
//			throw new AdempiereException(ex.getMessage(), ex);
//		}
		
		StringBuilder sql = new StringBuilder("SELECT Distinct(t.TableName) FROM AD_Column c ")
				.append("INNER JOIN AD_Table t ON c.AD_Table_ID=t.AD_Table_ID WHERE c.AD_Element_ID = ")
				.append(UOMConvL1_Element_ID).append(" AND c.AD_Table_ID != 208 AND c.ColumnSQL IS NULL ")
				.append(" AND t.isView='N'");
		
		PreparedStatement stmt = DB.prepareStatement(sql.toString(), null);
		ResultSet rs = null;

		try {
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				UOMConversionUsedMap.put(rs.getString(1), UOMConvL1_Element_ID);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			throw new AdempiereException(ex.getMessage(), ex);
		}
		finally {
			rs = null;
			stmt = null;
		}
	}
	
	IProcessUI m_processMonitor = null;
	
	/**
	 * 
	 */
	public ProductModificationImporter(Properties ctx, Sheet sheet, String trxName)
	{
		//this.m_ctx = ctx;
		this.m_trxName = trxName;
		m_processMonitor = Env.getProcessUI(ctx);
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MProduct p = (MProduct) po;
		
		if (p.get_ID() == 0)
			return "It is modification not for creating product.";
		
		String name = (String) freeColVals.get(COL_NmProduk);
		String isActive = ((Boolean) freeColVals.get(COL_IsActive))? "Y" : "N";
		int	newProductCategory_ID = (Integer) freeColVals.get(COL_NewProductCategory_ID);

		StringBuilder sql = new StringBuilder("UPDATE M_Product p SET Name=").append(DB.TO_STRING(name))
				.append(", IsActive=").append(DB.TO_STRING(isActive))
				.append(", M_Product_Category_ID=").append(newProductCategory_ID);
		
		Integer newUOM = freeColVals.get(COL_NewUOM) instanceof String? 
				p.getC_UOM_ID() : (Integer) freeColVals.get(COL_NewUOM);
		Integer newUOMConvL1 = freeColVals.get(COL_NewUOMConvL1) instanceof String ?//freeColVals.get(COL_NewUOMConvL1) == null?
				(p.getUOMConversionL1_ID() != 0? p.getUOMConversionL1_ID() : newUOM) 
				: (Integer) freeColVals.get(COL_NewUOMConvL1);
		Integer newUOMConvL2 = freeColVals.get(COL_NewUOMConvL2) instanceof String ?//freeColVals.get(COL_NewUOMConvL2) == null?
				(freeColVals.get(COL_NewUOM) instanceof String ? 
						(p.getUOMConversionL2_ID() != 0? p.getUOMConversionL2_ID() : null) : null) 
				: (Integer) freeColVals.get(COL_NewUOMConvL2);
		String UOMBaseLevel = newUOM == newUOMConvL1? 
				MProduct.UOMBASELEVEL_ConversionLevel1 : MProduct.UOMBASELEVEL_ConversionLevel2;
		
		if (newUOM != null && newUOM != p.getC_UOM_ID()) {
			sql.append(", C_UOM_ID=").append(newUOM);
		}
		
		if (newUOMConvL1 != null && newUOMConvL1 != p.getUOMConversionL1_ID()) {
//			if (newUOMConvL1 != newUOM)
//				UOMBaseLevel = MProduct.UOMBASELEVEL_ConversionLevel1;
			if (newUOMConvL1 != 0)
				sql.append(", UOMConversionL1_ID=").append(newUOMConvL1);			
		}
		
		sql.append(", UOMBaseLevel=").append(DB.TO_STRING(UOMBaseLevel));
		sql.append(", UOMConversionL2_ID=").append(newUOMConvL2)
			.append(" WHERE M_Product_ID=").append(p.getM_Product_ID());
		
		int count = DB.executeUpdate(sql.toString(), m_trxName);
		
		if (count <= 0)
			return "Error while updating product " + p + ".\n Reason: " + CLogger.retrieveError();
		
		m_processMonitor.statusUpdate(" Updating product on line : " + currentRow);
		
		for (String tableName : UOMUsedMap)
		{
			sql = new StringBuilder("UPDATE ").append(tableName)
					.append(" SET C_UOM_ID=").append(newUOM);
			
			if (UOMConversionUsedMap.get(tableName) != null)
				sql.append(", UOMConversionL1_ID=").append(newUOMConvL1)
				.append(", UOMConversionL2_ID=").append(newUOMConvL2);
			
			sql.append(" WHERE M_Product_ID=").append(p.getM_Product_ID());
			
			count = DB.executeUpdate(sql.toString(), m_trxName);
		}
		
		return SimpleImportXLS.DONT_SAVE;
	}
	
	String updateColumnOf(int element_ID, int ID_Value)
	{
//		StringBuilder sql = 
//				new StringBuilder("SELECT Distinct(t.TableName) FROM AD_Column c ")
//				.append("INNER JOIN AD_Table t ON c.AD_Table_ID=t.AD_Table_ID WHERE c.AD_Element_ID IN (")
//				.append(UOM_Element_ID).append(", ").append(UOMConvL1_Element_ID)
//				.append(", ").append(UOMConvL2_Element_ID).append(") AND c.AD_Table_ID != 208");
		
		
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
	}

}
