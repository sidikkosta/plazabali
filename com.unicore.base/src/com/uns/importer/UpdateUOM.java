package com.uns.importer;

import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.util.DB;

import com.uns.model.process.SimpleImportXLS;

public class UpdateUOM implements ImporterValidation {

	private final String COLUMN_UOM = "UOM";
	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	
	public UpdateUOM(Properties ctx, Sheet sheet, String trxName) {
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		int product_id = po.get_ID();
		String uom = freeColVals.get(COLUMN_UOM).toString();
		if(uom == null)
			throw new AdempiereException("Null UOM");
		
		String sql = "SELECT C_UOM_ID FROM C_UOM WHERE Name = ?";
		int uom_id = DB.getSQLValue(po.get_TrxName(), sql, uom);
		if(uom_id <= 0)
			throw new AdempiereException("Cannot found uom");
		
		String tableNames[] = new String[]{"M_Product", "C_OrderLine", "M_InOutLine", "M_RequisitionLine" 
				, "M_RequisitionLine_Confirm", "C_InvoiceLine"
				, "M_StorageOnHand", "UNS_UnnoticedMReceiptLine"}; 
		
		for(int i=0;i<tableNames.length;i++)
		{
			String tableName = tableNames[i];
			
			String uomconv = ", UOMConversionL1_ID = "+uom_id;
			if(tableName.equals("M_RequisitionLine") || tableName.equals("M_RequisitionLine_Confirm")
					|| tableName.equals("UNS_UnnoticedMReceiptLine"))
			{
				uomconv = "";
			}
			
			String update = "UPDATE "
					+ tableName
					+ " SET C_UOM_ID = ? "+uomconv
					+ " WHERE M_Product_ID = ?";
			boolean ok = DB.executeUpdateEx(update, new Object[]{uom_id, product_id}, po.get_TrxName()) != -1;
			if(!ok)
				throw new AdempiereException("Error update "+tableName);
		}
		
//		//update Product
//		String product = "UPDATE M_Product SET C_UOM_ID = ? , UOMConversionL1_ID = ?"
//				+ " WHERE M_Product_ID = ?";
//		boolean ok = DB.executeUpdateEx(product, new Object[]{uom_id, uom_id, product_id}, po.get_TrxName()) > 0;
//		if(!ok)
//			throw new AdempiereException("Error update M_Product");
//		
//		//update Order
//		String order = "UPDATE C_OrderLine SET C_UOM_ID = ? , UOMConversionL1_ID = ?"
//				+ " WHERE M_Product_ID = ?";
//		ok = DB.executeUpdateEx(order, new Object[]{uom_id, uom_id, product_id}, po.get_TrxName()) > 0;
//		if(!ok)
//			throw new AdempiereException("Error update M_Product");
//		
//		//update inout
//		String inout = "UPDATE M_InOutLine SET C_UOM_ID = ? , UOMConversionL1_ID = ?"
//				+ " WHERE M_Product_ID = ?";
//		ok = DB.executeUpdateEx(inout, new Object[]{uom_id, uom_id, product_id}, po.get_TrxName()) > 0;
//		if(!ok)
//			throw new AdempiereException("Error update M_Product");
//		
//		//update inout Confirm
//		String inoutConfirm = "UPDATE M_InOutLineConfirm SET C_UOM_ID = ? , UOMConversionL1_ID = ?"
//				+ " WHERE M_Product_ID = ?";
//		ok = DB.executeUpdateEx(inoutConfirm, new Object[]{uom_id, uom_id, product_id}, po.get_TrxName()) > 0;
//		if(!ok)
//			throw new AdempiereException("Error update M_Product");
//		
		
		
		return SimpleImportXLS.DONT_SAVE;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		
		return SimpleImportXLS.DONT_SAVE;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub

	}

}
