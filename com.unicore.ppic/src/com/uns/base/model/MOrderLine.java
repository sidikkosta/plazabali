/**
 * 
 */
package com.uns.base.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSPSSOAllocation;
import com.uns.model.MUNSSER;
import com.uns.util.ErrorMsg;



/**
 * @author menjangan
 *
 */
public class MOrderLine extends org.compiere.model.MOrderLine {
	
	private MOrder m_parent = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param ctx
	 * @param C_OrderLine_ID
	 * @param trxName
	 */
	public MOrderLine(Properties ctx, int C_OrderLine_ID, String trxName) {
		super(ctx, C_OrderLine_ID, trxName);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MOrderLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}
	
	@Override
	public MOrder getParent()
	{
		if (null == m_parent)
			m_parent = new MOrder(getCtx(), getC_Order_ID(), get_TrxName());
		
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		String msg =
				MUNSSER.cekProductAvalability(this);
		if (msg != null)
		{
			ErrorMsg.setErrorMsg(getCtx(), "SaveError", msg);
			return false;
		}
		
		//FIXME SET Estimation at 20FT
		//set Estimation(20FT) container
//		BigDecimal EstimasiContainer = BigDecimal.ZERO;
//		EstimasiContainer = getQtyEntered().divide(getUOMInCtn());
//		setEstimationCtn(EstimasiContainer);
		
		//MAttributeSetInstance.initAttributeValuesFrom(
		//		this, COLUMNNAME_M_Product_ID, COLUMNNAME_M_AttributeSetInstance_ID, get_TrxName());
		
		return super.beforeSave(newRecord);
	}
	
	/**
	 * 
	 */
	public MUNSPSSOAllocation[] getAllocation() {
		ArrayList<MUNSPSSOAllocation> list = new ArrayList<MUNSPSSOAllocation>();

		String sql = "SELECT UNS_PSSOAllocation_ID FROM UNS_PSSOAllocation "
				+ "WHERE C_OrderLine_ID = "+getC_OrderLine_ID();

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, get_TrxName());
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add(new MUNSPSSOAllocation(getCtx(), rs.getInt(1),
						get_TrxName()));
			rs.close();
			pstmt.close();
			pstmt = null;
		} catch (SQLException ex) {
			throw new AdempiereException("Unable to load SO Allocation.",
					ex);
		} finally {
			DB.close(rs, pstmt);
		}

		MUNSPSSOAllocation[] retValue = new MUNSPSSOAllocation[list
				.size()];
		list.toArray(retValue);
		return retValue;
	}
	
	public static MOrderLine[] getUnAllocatedPS(Properties ctx, int AD_Org_ID, int C_Order_ID, int C_OrderLine_ID,
			int M_Product_ID, int M_Product_Category_ID, String trxName)
	{	
		List<MOrderLine> list = new ArrayList<>();
		
		String sql = "SELECT * FROM C_OrderLine ol"
				+ " INNER JOIN C_Order o ON o.C_Order_ID = ol.C_Order_ID"
				+ " INNER JOIN M_Product p ON p.M_Product_ID = ol.M_Product_ID"
				+ " INNER JOIN M_Product_Category pc ON pc.M_Product_Category_ID = p.M_Product_Category_ID"
				+ " WHERE o.IsSOTrx = 'Y' AND o.DocStatus IN ('CO') AND ol.QtyOrdered-ol.QtyDelivered > "
				+ " (SELECT COALESCE(SUM(QtyUOM),0) FROM UNS_PSSOAllocation ps"
				+ " WHERE ps.C_OrderLine_ID = ol.C_OrderLine_ID) AND o.AD_Org_ID = ?";
		
		if(C_Order_ID > 0)
			sql += " AND o.C_Order_ID = " + C_Order_ID;
		if(C_OrderLine_ID > 0)
			sql += " AND ol.C_OrderLine_ID = " + C_OrderLine_ID;
		if(M_Product_ID > 0)
			sql += " AND p.M_Product_ID = " + M_Product_ID;
		if(M_Product_Category_ID > 0)
			sql += " AND pc.M_Product_Category_ID = " + M_Product_Category_ID;
		
		sql += " ORDER BY o.DatePromised, o.DateOrdered, o.Created";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, trxName);
			stmt.setInt(1, AD_Org_ID);;
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				list.add(new MOrderLine(ctx, rs, trxName));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}			
		
		return list.toArray(new MOrderLine[list.size()]);
	}
	
	public BigDecimal getUnAllocatedQtyOnPS()
	{
		BigDecimal unAllocated = Env.ZERO;
		String sql = "SELECT COALESCE(SUM(QtyUOM),0) FROM UNS_PSSOAllocation ps"
				+ " WHERE ps.C_OrderLine_ID = ?";
		BigDecimal allocated = DB.getSQLValueBD(get_TrxName(), sql, get_ID());
		if(allocated == null)
			allocated = Env.ZERO;
		unAllocated = (getQtyOrdered().subtract(getQtyDelivered()).subtract(allocated));
		
		return unAllocated;
	}
}