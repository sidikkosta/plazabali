/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_PriceLabelPrint
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PriceLabelPrint extends PO implements I_UNS_PriceLabelPrint, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190508L;

    /** Standard Constructor */
    public X_UNS_PriceLabelPrint (Properties ctx, int UNS_PriceLabelPrint_ID, String trxName)
    {
      super (ctx, UNS_PriceLabelPrint_ID, trxName);
      /** if (UNS_PriceLabelPrint_ID == 0)
        {
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocAction (null);
// CO
			setDocSourceType (null);
			setDocStatus (null);
// DR
			setPrintPriceLabel (null);
// N
			setProcessed (false);
// N
			setUNS_LabelName_ID (0);
			setUNS_PriceLabelPrint_ID (0);
			setValidFrom (new Timestamp( System.currentTimeMillis() ));
// @#Date@
        } */
    }

    /** Load Constructor */
    public X_UNS_PriceLabelPrint (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PriceLabelPrint[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** Material Receipt = MR */
	public static final String DOCSOURCETYPE_MaterialReceipt = "MR";
	/** Price List Conversion = PC */
	public static final String DOCSOURCETYPE_PriceListConversion = "PC";
	/** Manual = MN */
	public static final String DOCSOURCETYPE_Manual = "MN";
	/** Price List Version = PV */
	public static final String DOCSOURCETYPE_PriceListVersion = "PV";
	/** SKU Conversion = SC */
	public static final String DOCSOURCETYPE_SKUConversion = "SC";
	/** Set Doc Source Type.
		@param DocSourceType Doc Source Type	  */
	public void setDocSourceType (String DocSourceType)
	{

		set_Value (COLUMNNAME_DocSourceType, DocSourceType);
	}

	/** Get Doc Source Type.
		@return Doc Source Type	  */
	public String getDocSourceType () 
	{
		return (String)get_Value(COLUMNNAME_DocSourceType);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	public org.compiere.model.I_M_PriceList getM_PriceList() throws RuntimeException
    {
		return (org.compiere.model.I_M_PriceList)MTable.get(getCtx(), org.compiere.model.I_M_PriceList.Table_Name)
			.getPO(getM_PriceList_ID(), get_TrxName());	}

	/** Set Price List.
		@param M_PriceList_ID 
		Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID)
	{
		if (M_PriceList_ID < 1) 
			set_Value (COLUMNNAME_M_PriceList_ID, null);
		else 
			set_Value (COLUMNNAME_M_PriceList_ID, Integer.valueOf(M_PriceList_ID));
	}

	/** Get Price List.
		@return Unique identifier of a Price List
	  */
	public int getM_PriceList_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_PriceList_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_PriceList_Version getM_PriceList_Version() throws RuntimeException
    {
		return (org.compiere.model.I_M_PriceList_Version)MTable.get(getCtx(), org.compiere.model.I_M_PriceList_Version.Table_Name)
			.getPO(getM_PriceList_Version_ID(), get_TrxName());	}

	/** Set Price List Version.
		@param M_PriceList_Version_ID 
		Identifies a unique instance of a Price List
	  */
	public void setM_PriceList_Version_ID (int M_PriceList_Version_ID)
	{
		if (M_PriceList_Version_ID < 1) 
			set_Value (COLUMNNAME_M_PriceList_Version_ID, null);
		else 
			set_Value (COLUMNNAME_M_PriceList_Version_ID, Integer.valueOf(M_PriceList_Version_ID));
	}

	/** Get Price List Version.
		@return Identifies a unique instance of a Price List
	  */
	public int getM_PriceList_Version_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_PriceList_Version_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Print Price Label.
		@param PrintPriceLabel Print Price Label	  */
	public void setPrintPriceLabel (String PrintPriceLabel)
	{
		set_Value (COLUMNNAME_PrintPriceLabel, PrintPriceLabel);
	}

	/** Get Print Price Label.
		@return Print Price Label	  */
	public String getPrintPriceLabel () 
	{
		return (String)get_Value(COLUMNNAME_PrintPriceLabel);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Source Document No.
		@param SrcDocumentNo Source Document No	  */
	public void setSrcDocumentNo (String SrcDocumentNo)
	{
		set_Value (COLUMNNAME_SrcDocumentNo, SrcDocumentNo);
	}

	/** Get Source Document No.
		@return Source Document No	  */
	public String getSrcDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_SrcDocumentNo);
	}

	public com.uns.model.I_UNS_LabelName getUNS_LabelName() throws RuntimeException
    {
		return (com.uns.model.I_UNS_LabelName)MTable.get(getCtx(), com.uns.model.I_UNS_LabelName.Table_Name)
			.getPO(getUNS_LabelName_ID(), get_TrxName());	}

	/** Set Label Name.
		@param UNS_LabelName_ID Label Name	  */
	public void setUNS_LabelName_ID (int UNS_LabelName_ID)
	{
		if (UNS_LabelName_ID < 1) 
			set_Value (COLUMNNAME_UNS_LabelName_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_LabelName_ID, Integer.valueOf(UNS_LabelName_ID));
	}

	/** Get Label Name.
		@return Label Name	  */
	public int getUNS_LabelName_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_LabelName_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Price Label Printing.
		@param UNS_PriceLabelPrint_ID Price Label Printing	  */
	public void setUNS_PriceLabelPrint_ID (int UNS_PriceLabelPrint_ID)
	{
		if (UNS_PriceLabelPrint_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrint_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrint_ID, Integer.valueOf(UNS_PriceLabelPrint_ID));
	}

	/** Get Price Label Printing.
		@return Price Label Printing	  */
	public int getUNS_PriceLabelPrint_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PriceLabelPrint_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PriceLabelPrint_UU.
		@param UNS_PriceLabelPrint_UU UNS_PriceLabelPrint_UU	  */
	public void setUNS_PriceLabelPrint_UU (String UNS_PriceLabelPrint_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrint_UU, UNS_PriceLabelPrint_UU);
	}

	/** Get UNS_PriceLabelPrint_UU.
		@return UNS_PriceLabelPrint_UU	  */
	public String getUNS_PriceLabelPrint_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PriceLabelPrint_UU);
	}

	/** Set Valid from.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid from.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}
}