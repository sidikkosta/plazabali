/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_SoundingLab
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_SoundingLab extends PO implements I_UNS_SoundingLab, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171115L;

    /** Standard Constructor */
    public X_UNS_SoundingLab (Properties ctx, int UNS_SoundingLab_ID, String trxName)
    {
      super (ctx, UNS_SoundingLab_ID, trxName);
      /** if (UNS_SoundingLab_ID == 0)
        {
			setAVGPressFFB (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_UNS_SoundingLab (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_SoundingLab[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Average Press FFB.
		@param AVGPressFFB Average Press FFB	  */
	public void setAVGPressFFB (BigDecimal AVGPressFFB)
	{
		set_Value (COLUMNNAME_AVGPressFFB, AVGPressFFB);
	}

	/** Get Average Press FFB.
		@return Average Press FFB	  */
	public BigDecimal getAVGPressFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AVGPressFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CondFFB.
		@param CondFFB CondFFB	  */
	public void setCondFFB (BigDecimal CondFFB)
	{
		set_Value (COLUMNNAME_CondFFB, CondFFB);
	}

	/** Get CondFFB.
		@return CondFFB	  */
	public BigDecimal getCondFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CondFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CondSample.
		@param CondSample CondSample	  */
	public void setCondSample (BigDecimal CondSample)
	{
		set_Value (COLUMNNAME_CondSample, CondSample);
	}

	/** Get CondSample.
		@return CondSample	  */
	public BigDecimal getCondSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CondSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Dirt Kernel.
		@param DirtKernel 
		How Much Kernel with Dirt Quality
	  */
	public void setDirtKernel (BigDecimal DirtKernel)
	{
		throw new IllegalArgumentException ("DirtKernel is virtual column");	}

	/** Get Dirt Kernel.
		@return How Much Kernel with Dirt Quality
	  */
	public BigDecimal getDirtKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DirtKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Dust Cyclone.
		@param DustCycloneFFB Dust Cyclone	  */
	public void setDustCycloneFFB (BigDecimal DustCycloneFFB)
	{
		set_Value (COLUMNNAME_DustCycloneFFB, DustCycloneFFB);
	}

	/** Get Dust Cyclone.
		@return Dust Cyclone	  */
	public BigDecimal getDustCycloneFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DustCycloneFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Dust Cyclone Sample.
		@param DustCycloneSample Dust Cyclone Sample	  */
	public void setDustCycloneSample (BigDecimal DustCycloneSample)
	{
		set_Value (COLUMNNAME_DustCycloneSample, DustCycloneSample);
	}

	/** Get Dust Cyclone Sample.
		@return Dust Cyclone Sample	  */
	public BigDecimal getDustCycloneSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DustCycloneSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set EffEkstraksiCPO.
		@param EffEkstraksiCPO EffEkstraksiCPO	  */
	public void setEffEkstraksiCPO (BigDecimal EffEkstraksiCPO)
	{
		set_Value (COLUMNNAME_EffEkstraksiCPO, EffEkstraksiCPO);
	}

	/** Get EffEkstraksiCPO.
		@return EffEkstraksiCPO	  */
	public BigDecimal getEffEkstraksiCPO () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EffEkstraksiCPO);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set EffEkstraksiKernel.
		@param EffEkstraksiKernel EffEkstraksiKernel	  */
	public void setEffEkstraksiKernel (BigDecimal EffEkstraksiKernel)
	{
		set_Value (COLUMNNAME_EffEkstraksiKernel, EffEkstraksiKernel);
	}

	/** Get EffEkstraksiKernel.
		@return EffEkstraksiKernel	  */
	public BigDecimal getEffEkstraksiKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EffEkstraksiKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FCycloneFFB.
		@param FCycloneFFB FCycloneFFB	  */
	public void setFCycloneFFB (BigDecimal FCycloneFFB)
	{
		set_Value (COLUMNNAME_FCycloneFFB, FCycloneFFB);
	}

	/** Get FCycloneFFB.
		@return FCycloneFFB	  */
	public BigDecimal getFCycloneFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FCycloneFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FCycloneFFB (Oil).
		@param FCycloneFFB_Oil FCycloneFFB (Oil)	  */
	public void setFCycloneFFB_Oil (BigDecimal FCycloneFFB_Oil)
	{
		set_Value (COLUMNNAME_FCycloneFFB_Oil, FCycloneFFB_Oil);
	}

	/** Get FCycloneFFB (Oil).
		@return FCycloneFFB (Oil)	  */
	public BigDecimal getFCycloneFFB_Oil () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FCycloneFFB_Oil);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FCycloneSample.
		@param FCycloneSample FCycloneSample	  */
	public void setFCycloneSample (BigDecimal FCycloneSample)
	{
		set_Value (COLUMNNAME_FCycloneSample, FCycloneSample);
	}

	/** Get FCycloneSample.
		@return FCycloneSample	  */
	public BigDecimal getFCycloneSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FCycloneSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FCycloneSample (Oil).
		@param FCycloneSample_Oil FCycloneSample (Oil)	  */
	public void setFCycloneSample_Oil (BigDecimal FCycloneSample_Oil)
	{
		set_Value (COLUMNNAME_FCycloneSample_Oil, FCycloneSample_Oil);
	}

	/** Get FCycloneSample (Oil).
		@return FCycloneSample (Oil)	  */
	public BigDecimal getFCycloneSample_Oil () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FCycloneSample_Oil);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Fruit In EB.
		@param FruitInEB_FFB Fruit In EB	  */
	public void setFruitInEB_FFB (BigDecimal FruitInEB_FFB)
	{
		set_Value (COLUMNNAME_FruitInEB_FFB, FruitInEB_FFB);
	}

	/** Get Fruit In EB.
		@return Fruit In EB	  */
	public BigDecimal getFruitInEB_FFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FruitInEB_FFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Fruit In EB Sample.
		@param FruitInEBSample Fruit In EB Sample	  */
	public void setFruitInEBSample (BigDecimal FruitInEBSample)
	{
		set_Value (COLUMNNAME_FruitInEBSample, FruitInEBSample);
	}

	/** Get Fruit In EB Sample.
		@return Fruit In EB Sample	  */
	public BigDecimal getFruitInEBSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FruitInEBSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set HPhaseFFB.
		@param HPhaseFFB 
		HPhaseFFB
	  */
	public void setHPhaseFFB (BigDecimal HPhaseFFB)
	{
		set_Value (COLUMNNAME_HPhaseFFB, HPhaseFFB);
	}

	/** Get HPhaseFFB.
		@return HPhaseFFB
	  */
	public BigDecimal getHPhaseFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HPhaseFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set HPhaseSample.
		@param HPhaseSample HPhaseSample	  */
	public void setHPhaseSample (BigDecimal HPhaseSample)
	{
		set_Value (COLUMNNAME_HPhaseSample, HPhaseSample);
	}

	/** Get HPhaseSample.
		@return HPhaseSample	  */
	public BigDecimal getHPhaseSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HPhaseSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set JKFFB.
		@param JKFFB JKFFB	  */
	public void setJKFFB (BigDecimal JKFFB)
	{
		set_ValueNoCheck (COLUMNNAME_JKFFB, JKFFB);
	}

	/** Get JKFFB.
		@return JKFFB	  */
	public BigDecimal getJKFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JKFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JKSample.
		@param JKSample JKSample	  */
	public void setJKSample (BigDecimal JKSample)
	{
		set_Value (COLUMNNAME_JKSample, JKSample);
	}

	/** Get JKSample.
		@return JKSample	  */
	public BigDecimal getJKSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JKSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set KER Netto-I.
		@param KERNettoI 
		Kernel Extraction Rate of Netto-I
	  */
	public void setKERNettoI (BigDecimal KERNettoI)
	{
		throw new IllegalArgumentException ("KERNettoI is virtual column");	}

	/** Get KER Netto-I.
		@return Kernel Extraction Rate of Netto-I
	  */
	public BigDecimal getKERNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KERNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set LabSupervisor.
		@param LabSupervisor LabSupervisor	  */
	public void setLabSupervisor (String LabSupervisor)
	{
		set_Value (COLUMNNAME_LabSupervisor, LabSupervisor);
	}

	/** Get LabSupervisor.
		@return LabSupervisor	  */
	public String getLabSupervisor () 
	{
		return (String)get_Value(COLUMNNAME_LabSupervisor);
	}

	/** Set LTDS1FFB.
		@param LTDS1FFB LTDS1FFB	  */
	public void setLTDS1FFB (BigDecimal LTDS1FFB)
	{
		set_Value (COLUMNNAME_LTDS1FFB, LTDS1FFB);
	}

	/** Get LTDS1FFB.
		@return LTDS1FFB	  */
	public BigDecimal getLTDS1FFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LTDS1FFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set LTDS1Sample.
		@param LTDS1Sample LTDS1Sample	  */
	public void setLTDS1Sample (BigDecimal LTDS1Sample)
	{
		set_Value (COLUMNNAME_LTDS1Sample, LTDS1Sample);
	}

	/** Get LTDS1Sample.
		@return LTDS1Sample	  */
	public BigDecimal getLTDS1Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LTDS1Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set LTDS2FFB.
		@param LTDS2FFB LTDS2FFB	  */
	public void setLTDS2FFB (BigDecimal LTDS2FFB)
	{
		set_Value (COLUMNNAME_LTDS2FFB, LTDS2FFB);
	}

	/** Get LTDS2FFB.
		@return LTDS2FFB	  */
	public BigDecimal getLTDS2FFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LTDS2FFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set LTDS2Sample.
		@param LTDS2Sample LTDS2Sample	  */
	public void setLTDS2Sample (BigDecimal LTDS2Sample)
	{
		set_Value (COLUMNNAME_LTDS2Sample, LTDS2Sample);
	}

	/** Get LTDS2Sample.
		@return LTDS2Sample	  */
	public BigDecimal getLTDS2Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LTDS2Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Moist Kernel.
		@param MoistKernel 
		How Much Kernel with Moist Quality
	  */
	public void setMoistKernel (BigDecimal MoistKernel)
	{
		throw new IllegalArgumentException ("MoistKernel is virtual column");	}

	/** Get Moist Kernel.
		@return How Much Kernel with Moist Quality
	  */
	public BigDecimal getMoistKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MoistKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set NutFFB.
		@param NutFFB NutFFB	  */
	public void setNutFFB (BigDecimal NutFFB)
	{
		set_Value (COLUMNNAME_NutFFB, NutFFB);
	}

	/** Get NutFFB.
		@return NutFFB	  */
	public BigDecimal getNutFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NutFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set NutSample.
		@param NutSample NutSample	  */
	public void setNutSample (BigDecimal NutSample)
	{
		set_Value (COLUMNNAME_NutSample, NutSample);
	}

	/** Get NutSample.
		@return NutSample	  */
	public BigDecimal getNutSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NutSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set OER Netto-I.
		@param OERNettoI 
		Oil Ekstraction Rate of Netto-I
	  */
	public void setOERNettoI (BigDecimal OERNettoI)
	{
		throw new IllegalArgumentException ("OERNettoI is virtual column");	}

	/** Get OER Netto-I.
		@return Oil Ekstraction Rate of Netto-I
	  */
	public BigDecimal getOERNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OERNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Press1Sample.
		@param Press1Sample Press1Sample	  */
	public void setPress1Sample (BigDecimal Press1Sample)
	{
		set_Value (COLUMNNAME_Press1Sample, Press1Sample);
	}

	/** Get Press1Sample.
		@return Press1Sample	  */
	public BigDecimal getPress1Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Press1Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Press2Sample.
		@param Press2Sample Press2Sample	  */
	public void setPress2Sample (BigDecimal Press2Sample)
	{
		set_Value (COLUMNNAME_Press2Sample, Press2Sample);
	}

	/** Get Press2Sample.
		@return Press2Sample	  */
	public BigDecimal getPress2Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Press2Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Press3Sample.
		@param Press3Sample Press3Sample	  */
	public void setPress3Sample (BigDecimal Press3Sample)
	{
		set_Value (COLUMNNAME_Press3Sample, Press3Sample);
	}

	/** Get Press3Sample.
		@return Press3Sample	  */
	public BigDecimal getPress3Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Press3Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set RecFFB.
		@param RecFFB RecFFB	  */
	public void setRecFFB (BigDecimal RecFFB)
	{
		set_Value (COLUMNNAME_RecFFB, RecFFB);
	}

	/** Get RecFFB.
		@return RecFFB	  */
	public BigDecimal getRecFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RecFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set RecSample.
		@param RecSample RecSample	  */
	public void setRecSample (BigDecimal RecSample)
	{
		set_Value (COLUMNNAME_RecSample, RecSample);
	}

	/** Get RecSample.
		@return RecSample	  */
	public BigDecimal getRecSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RecSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set RMill1Sample.
		@param RMill1Sample RMill1Sample	  */
	public void setRMill1Sample (BigDecimal RMill1Sample)
	{
		set_Value (COLUMNNAME_RMill1Sample, RMill1Sample);
	}

	/** Get RMill1Sample.
		@return RMill1Sample	  */
	public BigDecimal getRMill1Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RMill1Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set RMill2Sample.
		@param RMill2Sample RMill2Sample	  */
	public void setRMill2Sample (BigDecimal RMill2Sample)
	{
		set_Value (COLUMNNAME_RMill2Sample, RMill2Sample);
	}

	/** Get RMill2Sample.
		@return RMill2Sample	  */
	public BigDecimal getRMill2Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RMill2Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set RMill3Sample.
		@param RMill3Sample RMill3Sample	  */
	public void setRMill3Sample (BigDecimal RMill3Sample)
	{
		set_Value (COLUMNNAME_RMill3Sample, RMill3Sample);
	}

	/** Get RMill3Sample.
		@return RMill3Sample	  */
	public BigDecimal getRMill3Sample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RMill3Sample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set SolidFFB.
		@param SolidFFB SolidFFB	  */
	public void setSolidFFB (BigDecimal SolidFFB)
	{
		set_Value (COLUMNNAME_SolidFFB, SolidFFB);
	}

	/** Get SolidFFB.
		@return SolidFFB	  */
	public BigDecimal getSolidFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SolidFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set SolidSample.
		@param SolidSample SolidSample	  */
	public void setSolidSample (BigDecimal SolidSample)
	{
		set_Value (COLUMNNAME_SolidSample, SolidSample);
	}

	/** Get SolidSample.
		@return SolidSample	  */
	public BigDecimal getSolidSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SolidSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TotalCPOLosses.
		@param TotalCPOLosses TotalCPOLosses	  */
	public void setTotalCPOLosses (BigDecimal TotalCPOLosses)
	{
		set_Value (COLUMNNAME_TotalCPOLosses, TotalCPOLosses);
	}

	/** Get TotalCPOLosses.
		@return TotalCPOLosses	  */
	public BigDecimal getTotalCPOLosses () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalCPOLosses);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TotalKernelLosses.
		@param TotalKernelLosses TotalKernelLosses	  */
	public void setTotalKernelLosses (BigDecimal TotalKernelLosses)
	{
		set_Value (COLUMNNAME_TotalKernelLosses, TotalKernelLosses);
	}

	/** Get TotalKernelLosses.
		@return TotalKernelLosses	  */
	public BigDecimal getTotalKernelLosses () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalKernelLosses);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set UnderFlowSample.
		@param UnderFlowSample UnderFlowSample	  */
	public void setUnderFlowSample (BigDecimal UnderFlowSample)
	{
		set_Value (COLUMNNAME_UnderFlowSample, UnderFlowSample);
	}

	/** Get UnderFlowSample.
		@return UnderFlowSample	  */
	public BigDecimal getUnderFlowSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UnderFlowSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Sounding getUNS_Sounding() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Sounding)MTable.get(getCtx(), com.uns.model.I_UNS_Sounding.Table_Name)
			.getPO(getUNS_Sounding_ID(), get_TrxName());	}

	/** Set Sounding.
		@param UNS_Sounding_ID Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID)
	{
		if (UNS_Sounding_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, Integer.valueOf(UNS_Sounding_ID));
	}

	/** Get Sounding.
		@return Sounding	  */
	public int getUNS_Sounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Sounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_SoundingLab_ID.
		@param UNS_SoundingLab_ID UNS_SoundingLab_ID	  */
	public void setUNS_SoundingLab_ID (int UNS_SoundingLab_ID)
	{
		if (UNS_SoundingLab_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SoundingLab_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SoundingLab_ID, Integer.valueOf(UNS_SoundingLab_ID));
	}

	/** Get UNS_SoundingLab_ID.
		@return UNS_SoundingLab_ID	  */
	public int getUNS_SoundingLab_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SoundingLab_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_SoundingLab_UU.
		@param UNS_SoundingLab_UU UNS_SoundingLab_UU	  */
	public void setUNS_SoundingLab_UU (String UNS_SoundingLab_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_SoundingLab_UU, UNS_SoundingLab_UU);
	}

	/** Get UNS_SoundingLab_UU.
		@return UNS_SoundingLab_UU	  */
	public String getUNS_SoundingLab_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_SoundingLab_UU);
	}

	public com.uns.model.I_UNS_TankSounding getUNS_TankSounding() throws RuntimeException
    {
		return (com.uns.model.I_UNS_TankSounding)MTable.get(getCtx(), com.uns.model.I_UNS_TankSounding.Table_Name)
			.getPO(getUNS_TankSounding_ID(), get_TrxName());	}

	/** Set Tank Sounding.
		@param UNS_TankSounding_ID Tank Sounding	  */
	public void setUNS_TankSounding_ID (int UNS_TankSounding_ID)
	{
		if (UNS_TankSounding_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_TankSounding_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_TankSounding_ID, Integer.valueOf(UNS_TankSounding_ID));
	}

	/** Get Tank Sounding.
		@return Tank Sounding	  */
	public int getUNS_TankSounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_TankSounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set WetSellFFB.
		@param WetSellFFB WetSellFFB	  */
	public void setWetSellFFB (BigDecimal WetSellFFB)
	{
		set_Value (COLUMNNAME_WetSellFFB, WetSellFFB);
	}

	/** Get WetSellFFB.
		@return WetSellFFB	  */
	public BigDecimal getWetSellFFB () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WetSellFFB);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set WetSellSample.
		@param WetSellSample WetSellSample	  */
	public void setWetSellSample (BigDecimal WetSellSample)
	{
		set_Value (COLUMNNAME_WetSellSample, WetSellSample);
	}

	/** Get WetSellSample.
		@return WetSellSample	  */
	public BigDecimal getWetSellSample () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WetSellSample);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}