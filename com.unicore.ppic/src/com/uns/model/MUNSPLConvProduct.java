/**
 * 
 */
package com.uns.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPLConvProduct extends X_UNS_PLConv_Product {

	/**
	 * 
	 */
	private static final long serialVersionUID = -607846767405969496L;

	/**
	 * @param ctx
	 * @param UNS_PLConv_Product_ID
	 * @param trxName
	 */
	public MUNSPLConvProduct(Properties ctx, int UNS_PLConv_Product_ID,
			String trxName) {
		super(ctx, UNS_PLConv_Product_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPLConvProduct(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	protected boolean beforeSave(boolean newRecord)
	{
		MUNSPriceListConv parent = new MUNSPriceListConv(getCtx(), getUNS_PriceListConv_ID(), get_TrxName());
		if(parent.isProcessed())
		{
			log.saveError("Error", "Parent has processed.");
			return false;
		}
		
//		if(newRecord && getM_Product_Category_ID() > 0)
//		{
//			org.compiere.model.MProduct[] p = getProductsByCategory();
//			if(p.length > 0)
//				setM_Product_ID(p[0].get_ID());
//		}
		
		return true;
	}

	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(newRecord && getM_Product_Category_ID() > 0)
		{
//			org.compiere.model.MProduct[] p = getProductsByCategory();
//			for(int i=1;i<p.length;i++)
//			{
//				String sql = "SELECT COUNT(*) FROM UNS_PLConv_Product WHERE"
//						+ " M_Product_ID = ? AND UNS_PriceListConv_ID = ?";
//				int count = DB.getSQLValue(get_TrxName(), sql, p[i].get_ID(), getUNS_PriceListConv_ID());
//				if(count > 0)
//					continue;
//				
//				MUNSPLConvProduct cp = new MUNSPLConvProduct(getCtx(), 0, get_TrxName());
//				cp.setUNS_PriceListConv_ID(getUNS_PriceListConv_ID());
//				cp.setAD_Org_ID(getAD_Org_ID());
//				cp.setM_Product_ID(p[i].get_ID());
//				cp.saveEx();
//			}
			generateLines();
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		MUNSPriceListConv parent = new MUNSPriceListConv(getCtx(), getUNS_PriceListConv_ID(), get_TrxName());
		if(parent.isProcessed())
		{
			log.saveError("Error", "Parent has processed.");
			return false;
		}
		
		if(getM_Product_Category_ID() > 0)
		{
			String sql = "DELETE FROM UNS_PLConv_Product pcp"
					+ " WHERE EXISTS (SELECT 1 FROM M_Product p"
					+ " WHERE p.M_Product_ID = pcp.M_Product_ID"
					+ " AND p.M_Product_Category_ID IN"
					+ " (SELECT getproductcategoryrecursively(?)))"
					+ " AND pcp.UNS_PriceListConv_ID = ?";
			DB.executeUpdate(sql, new Object[]{getM_Product_Category_ID(), getUNS_PriceListConv_ID()},
					false, get_TrxName());
		}
		
		return true;
	}
	
	private org.compiere.model.MProduct[] m_Products = null;
	
	@SuppressWarnings("unused")
	//karena terlalu lama, jadi langsung hit ke DB pakai "INSERT INTO"
	private org.compiere.model.MProduct[] getProductsByCategory()
	{
		if(m_Products != null)
			return m_Products;
		
		String whereClause = "M_Product_Category_ID IN (SELECT getproductcategoryrecursively("
				+ getM_Product_Category_ID() + "))"
				+ " AND IsActive = 'Y'";
		if(getUNS_PriceListConv().isSOTrx())
			whereClause += " AND IsSold = 'Y'";
		else
			whereClause += " AND IsPurchased = 'Y'";
		
		org.compiere.model.MProduct[] p = MProduct.get(getCtx(), whereClause, get_TrxName());
		m_Products = p;
		
		return m_Products;
	}
	
	private boolean generateLines()
	{
		String sql = "SELECT DISTINCT(M_Product_ID) FROM M_Product"
				+ " WHERE M_Product_Category_ID IN (SELECT getproductcategoryrecursively(?))"
				+ " AND IsActive = 'Y' AND NOT EXISTS (SELECT 1 FROM UNS_PLConv_Product pl"
				+ " WHERE pl.M_Product_ID = M_Product.M_Product_ID AND pl.UNS_PriceListConv_ID = ?)";
		if(getUNS_PriceListConv().isSOTrx())
			sql += " AND IsSold = 'Y'";
		else
			sql += " AND IsPurchased = 'Y'";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, getM_Product_Category_ID());
			stmt.setInt(2, getUNS_PriceListConv_ID());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				String update = "INSERT INTO UNS_PLConv_Product (AD_Client_ID, AD_Org_ID, Created, CreatedBy, Updated, UpdatedBy"
						+ ", M_Product_ID, UNS_PriceListConv_ID, IsActive, UNS_PLConv_Product_ID, UNS_PLConv_Product_UU) VALUES"
						+ " (?,?,?,?,?,?,?,?,?,getnextid('UNS_PLConv_Product'),generate_uuid())";
				Timestamp dateNow = new Timestamp (System.currentTimeMillis());
				int user = Env.getAD_User_ID(getCtx());
				if(DB.executeUpdate(update, 
						new Object[]{getAD_Client_ID(), getAD_Org_ID(), dateNow, user,
						dateNow, user, rs.getInt(1), getUNS_PriceListConv_ID(), "Y"},
						false, get_TrxName()) < 0)
					throw new AdempiereException(CLogger.retrieveErrorString("Error"));
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		return true;
	}
}