/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_FlowMeter
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_FlowMeter extends PO implements I_UNS_FlowMeter, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171012L;

    /** Standard Constructor */
    public X_UNS_FlowMeter (Properties ctx, int UNS_FlowMeter_ID, String trxName)
    {
      super (ctx, UNS_FlowMeter_ID, trxName);
      /** if (UNS_FlowMeter_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_FlowMeter (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_FlowMeter[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set UNS_FlowMeter_ID.
		@param UNS_FlowMeter_ID UNS_FlowMeter_ID	  */
	public void setUNS_FlowMeter_ID (int UNS_FlowMeter_ID)
	{
		if (UNS_FlowMeter_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_FlowMeter_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_FlowMeter_ID, Integer.valueOf(UNS_FlowMeter_ID));
	}

	/** Get UNS_FlowMeter_ID.
		@return UNS_FlowMeter_ID	  */
	public int getUNS_FlowMeter_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_FlowMeter_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_FlowMeter_UU.
		@param UNS_FlowMeter_UU UNS_FlowMeter_UU	  */
	public void setUNS_FlowMeter_UU (String UNS_FlowMeter_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_FlowMeter_UU, UNS_FlowMeter_UU);
	}

	/** Get UNS_FlowMeter_UU.
		@return UNS_FlowMeter_UU	  */
	public String getUNS_FlowMeter_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_FlowMeter_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}