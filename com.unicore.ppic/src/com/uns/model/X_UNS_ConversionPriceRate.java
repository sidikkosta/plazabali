/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_ConversionPriceRate
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ConversionPriceRate extends PO implements I_UNS_ConversionPriceRate, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181023L;

    /** Standard Constructor */
    public X_UNS_ConversionPriceRate (Properties ctx, int UNS_ConversionPriceRate_ID, String trxName)
    {
      super (ctx, UNS_ConversionPriceRate_ID, trxName);
      /** if (UNS_ConversionPriceRate_ID == 0)
        {
			setCurrencyFrom_ID (0);
			setCurrencyTo_ID (0);
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setProcessed (false);
// N
			setRate (Env.ZERO);
// 0
			setUNS_ConversionPriceRate_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ConversionPriceRate (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ConversionPriceRate[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getCurrencyFrom() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getCurrencyFrom_ID(), get_TrxName());	}

	/** Set Currency From.
		@param CurrencyFrom_ID Currency From	  */
	public void setCurrencyFrom_ID (int CurrencyFrom_ID)
	{
		if (CurrencyFrom_ID < 1) 
			set_Value (COLUMNNAME_CurrencyFrom_ID, null);
		else 
			set_Value (COLUMNNAME_CurrencyFrom_ID, Integer.valueOf(CurrencyFrom_ID));
	}

	/** Get Currency From.
		@return Currency From	  */
	public int getCurrencyFrom_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CurrencyFrom_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getCurrencyTo() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getCurrencyTo_ID(), get_TrxName());	}

	/** Set Currency To.
		@param CurrencyTo_ID Currency To	  */
	public void setCurrencyTo_ID (int CurrencyTo_ID)
	{
		if (CurrencyTo_ID < 1) 
			set_Value (COLUMNNAME_CurrencyTo_ID, null);
		else 
			set_Value (COLUMNNAME_CurrencyTo_ID, Integer.valueOf(CurrencyTo_ID));
	}

	/** Get Currency To.
		@return Currency To	  */
	public int getCurrencyTo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CurrencyTo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Rate.
		@param Rate 
		Rate or Tax or Exchange
	  */
	public void setRate (BigDecimal Rate)
	{
		set_Value (COLUMNNAME_Rate, Rate);
	}

	/** Get Rate.
		@return Rate or Tax or Exchange
	  */
	public BigDecimal getRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Rate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Conversion Price Rate.
		@param UNS_ConversionPriceRate_ID Conversion Price Rate	  */
	public void setUNS_ConversionPriceRate_ID (int UNS_ConversionPriceRate_ID)
	{
		if (UNS_ConversionPriceRate_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ConversionPriceRate_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ConversionPriceRate_ID, Integer.valueOf(UNS_ConversionPriceRate_ID));
	}

	/** Get Conversion Price Rate.
		@return Conversion Price Rate	  */
	public int getUNS_ConversionPriceRate_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ConversionPriceRate_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ConversionPriceRate_UU.
		@param UNS_ConversionPriceRate_UU UNS_ConversionPriceRate_UU	  */
	public void setUNS_ConversionPriceRate_UU (String UNS_ConversionPriceRate_UU)
	{
		set_Value (COLUMNNAME_UNS_ConversionPriceRate_UU, UNS_ConversionPriceRate_UU);
	}

	/** Get UNS_ConversionPriceRate_UU.
		@return UNS_ConversionPriceRate_UU	  */
	public String getUNS_ConversionPriceRate_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ConversionPriceRate_UU);
	}

	/** Set Valid from.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid from.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}
}