package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

public class MUNSFlowMeterLine extends X_UNS_FlowMeterLine implements
		I_UNS_FlowMeterLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5550488555573726197L;

	public MUNSFlowMeterLine(Properties ctx, int UNS_FlowMeterLine_ID,
			String trxName) {
		super(ctx, UNS_FlowMeterLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSFlowMeterLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		if (getStartDate() == null && getStartTime() == null)
		{
			log.saveError("Save Error", "Field Mandatory StartDate and StartTime" );
			return false;
		}
		else
			{
			String startDateStr = getStartDate().toString().substring(0,10);
			String startTimeStr = getStartTime().toString().substring(11, 19);
			
			String startStr = startDateStr+" "+startTimeStr;
			setStartTime(Timestamp.valueOf(startStr));
			}
		
		if(getStopDate() == null && getStopTime() == null)
		{
			log.saveError("Save Error", "Field Mandatory StopDate and StopTime");
			return false;
		}
		else
			{
			String stopDateStr = getStopDate().toString().substring(0,10);
			String stopTimeStr = getStopTime().toString().substring(11,19);
			
			String stopStr = stopDateStr+" "+stopTimeStr;
			setStopTime(Timestamp.valueOf(stopStr));
			}
		return super.beforeSave(newRecord);
	}
}
