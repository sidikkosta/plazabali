package com.uns.model.validator;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MClient;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRequisition;
import org.compiere.model.MRequisitionLine;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import com.uns.model.MUNSConversionPriceRate;
import com.uns.model.MUNSMRP;
import com.uns.model.MUNSMRPWeekly;

public class PPICValidatorV2 implements ModelValidator {

	public PPICValidatorV2 () {
		super ();
	}
	
	private static CLogger log = CLogger.getCLogger(PPICValidatorV2.class);
	private int p_AD_Client_ID = -1;
	
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		if (log.isLoggable(Level.INFO))
			log.info("Initialize....");
		if (client != null) {	
			p_AD_Client_ID = client.getAD_Client_ID();
			log.info(client.toString());
		}
		else  {
			log.info("Initializing global validator: "+this.toString());
		}
		
		engine.addDocValidate(MOrder.Table_Name, this);
		engine.addDocValidate(MRequisition.Table_Name, this);
		engine.addDocValidate(MPriceListVersion.Table_Name, this);
		engine.addModelChange(MProductPrice.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		return p_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		log.info("AD_User_ID=" + AD_User_ID);
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception
	{	
		if(MProductPrice.Table_Name.equals(po.get_TableName()))
		{
			copyProductPrice(po);
		}
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {
		if (MOrder.Table_Name.equals(po.get_TableName())) {
			MOrder order = (MOrder) po;
			if (timing == TIMING_AFTER_COMPLETE) {
				MOrderLine[] lines = order.getLines();
				int weekNoASOR = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", order.getDatePromised());
				int WeekNoAORE = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", order.getDateOrdered());
				for (int i=0; i<lines.length; i++) {
					MUNSMRP mrpASOR = MUNSMRP.get(po.get_TrxName(), order.getDatePromised(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpASOR != null) {
						MUNSMRPWeekly weeklyASOR = MUNSMRPWeekly.get(mrpASOR.get_ID(), weekNoASOR, po.get_TrxName());
						if (weeklyASOR != null) {
							weeklyASOR.setASOR(weeklyASOR.getASOR().add(lines[i].getQtyOrdered()));
							weeklyASOR.save();
							mrpASOR.setNeedToUpdate(true);
							mrpASOR.save(); 
						}
					}
					MUNSMRP mrpAORE = MUNSMRP.get(po.get_TrxName(), order.getDateOrdered(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpAORE != null) {
						MUNSMRPWeekly weeklyAORe = MUNSMRPWeekly.get(mrpAORE.get_ID(), WeekNoAORE, po.get_TrxName());
						if (weeklyAORe != null) {
							weeklyAORe.setAORE(weeklyAORe.getAORE().add(lines[i].getQtyOrdered()));
							weeklyAORe.save();
							mrpAORE.setNeedToUpdate(true);
							mrpAORE.save(); 
						}
					}
				}
			} else if (timing == TIMING_BEFORE_VOID || timing == TIMING_AFTER_REACTIVATE) {
				MOrderLine[] lines = order.getLines();
				int weekNoASOR = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", order.getDatePromised());
				int WeekNoAORE = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", order.getDateOrdered());
				for (int i=0; i<lines.length; i++) {
					MUNSMRP mrpASOR = MUNSMRP.get(po.get_TrxName(), order.getDatePromised(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpASOR != null) {
						MUNSMRPWeekly weeklyASOR = MUNSMRPWeekly.get(mrpASOR.get_ID(), weekNoASOR, po.get_TrxName());
						if (weeklyASOR != null) {
							weeklyASOR.setASOR(weeklyASOR.getASOR().subtract(lines[i].getQtyOrdered()));
							weeklyASOR.save();
							mrpASOR.setNeedToUpdate(true);
							mrpASOR.save(); 
						}
					}
					MUNSMRP mrpAORE = MUNSMRP.get(po.get_TrxName(), order.getDateOrdered(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpAORE != null) {
						MUNSMRPWeekly weeklyAORe = MUNSMRPWeekly.get(mrpAORE.get_ID(), WeekNoAORE, po.get_TrxName());
						if (weeklyAORe != null) {
							weeklyAORe.setAORE(weeklyAORe.getAORE().subtract(lines[i].getQtyOrdered()));
							weeklyAORe.save();
							mrpAORE.setNeedToUpdate(true);
							mrpAORE.save(); 
						}
					}
				}
			} 
		} else if (MRequisition.Table_Name.equals(po.get_TableName())) {
			MRequisition req = (MRequisition) po;
			if (timing == TIMING_AFTER_COMPLETE) {
				MRequisitionLine[] lines = req.getLines();
				int weekNoASOR = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", req.getDateRequired());
				int WeekNoAORE = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", req.getDateDoc());
				for (int i=0; i<lines.length; i++) {
					MUNSMRP mrpASOR = MUNSMRP.get(po.get_TrxName(), req.getDateRequired(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpASOR != null) {
						MUNSMRPWeekly weeklyASOR = MUNSMRPWeekly.get(mrpASOR.get_ID(), weekNoASOR, po.get_TrxName());
						if (weeklyASOR != null) {
							weeklyASOR.setSOR(weeklyASOR.getSOR().add(lines[i].getQty()));
							weeklyASOR.save();
							mrpASOR.setNeedToUpdate(true);
							mrpASOR.save(); 
						}
					}
					MUNSMRP mrpAORE = MUNSMRP.get(po.get_TrxName(), req.getDateDoc(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpAORE != null) {
						MUNSMRPWeekly weeklyAORe = MUNSMRPWeekly.get(mrpAORE.get_ID(), WeekNoAORE, po.get_TrxName());
						if (weeklyAORe != null) {
							weeklyAORe.setAOREQ(weeklyAORe.getAOREQ().add(lines[i].getQty()));
							weeklyAORe.save();
							mrpAORE.setNeedToUpdate(true);
							mrpAORE.save(); 
						}
					}
				}
			} else if (timing == TIMING_BEFORE_VOID || timing == TIMING_AFTER_REACTIVATE) {
				MRequisitionLine[] lines = req.getLines();
				int weekNoASOR = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", req.getDateRequired());
				int WeekNoAORE = DB.getSQLValue(po.get_TrxName(), "SELECT getproductionweekno(?)", req.getDateDoc());
				for (int i=0; i<lines.length; i++) {
					MUNSMRP mrpASOR = MUNSMRP.get(po.get_TrxName(), req.getDateRequired(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpASOR != null) {
						MUNSMRPWeekly weeklyASOR = MUNSMRPWeekly.get(mrpASOR.get_ID(), weekNoASOR, po.get_TrxName());
						if (weeklyASOR != null) {
							weeklyASOR.setSOR(weeklyASOR.getSOR().subtract(lines[i].getQty()));
							weeklyASOR.save();
							mrpASOR.setNeedToUpdate(true);
							mrpASOR.save(); 
						}
					}
					MUNSMRP mrpAORE = MUNSMRP.get(po.get_TrxName(), req.getDateDoc(), lines[i].getM_Product_ID(), po.getAD_Org_ID());
					if (mrpAORE != null) {
						MUNSMRPWeekly weeklyAORe = MUNSMRPWeekly.get(mrpAORE.get_ID(), WeekNoAORE, po.get_TrxName());
						if (weeklyAORe != null) {
							weeklyAORe.setAOREQ(weeklyAORe.getAOREQ().subtract(lines[i].getQty()));
							weeklyAORe.save();
							mrpAORE.setNeedToUpdate(true);
							mrpAORE.save(); 
						}
					}
				}
			} 
		}
		else if(MPriceListVersion.Table_ID == po.get_Table_ID()
				&& timing == TIMING_AFTER_COMPLETE)
		{
			return copyToReference(po);
		}
		
		return null;
	}
	
	private String copyProductPrice(PO po)
	{
		if(po.isReplication())
			return null;
		MProductPrice pp = (MProductPrice) po;
		
		MPriceListVersion v = new MPriceListVersion(pp.getCtx(), pp.getM_PriceList_Version_ID(), pp.get_TrxName());
		if(!v.isDefault())
			return null;
		
		String sql = "SELECT p.Reference_ID FROM M_PriceList p"
				+ " INNER JOIN M_PriceList_Version v ON v.M_PriceList_ID = p.M_PriceList_ID"
				+ " WHERE v.M_PriceList_Version_ID = ?";
		int ref = DB.getSQLValue(pp.get_TrxName(), sql, v.get_ID());
		
		if(ref > 0)
		{
			MPriceListVersion vNew = MPriceListVersion.getSameValidFrom(pp.getCtx(), ref, v.getValidFrom(),
					pp.get_TrxName());
			if(vNew == null)
			{
				vNew = new MPriceListVersion(new MPriceList(pp.getCtx(), ref, pp.get_TrxName()));
				vNew.setValidFrom(v.getValidFrom());
				vNew.saveEx();
			}
			
			BigDecimal rate = MUNSConversionPriceRate.getRate(v.getM_PriceList().getC_Currency_ID(),
					vNew.getPriceList().getC_Currency_ID(), v.getValidFrom(), pp.getAD_Org_ID(), pp.get_TrxName());
			
			if(rate == null)
				return "";
			MProductPrice ppNew = MProductPrice.get(pp.getCtx(), vNew.get_ID(), pp.getM_Product_ID(), pp.get_TrxName());
			if(ppNew == null)
			{
				ppNew = new MProductPrice(pp.getCtx(), vNew.get_ID(), pp.getM_Product_ID(), pp.get_TrxName());
			}
			ppNew.setPriceList(pp.getPriceList().multiply(rate));
			ppNew.saveEx();
		}
		
		return null;
	}
	
	private String copyToReference(PO po)
	{
		if(po.isReplication())
			return null;
		MPriceListVersion plv = (MPriceListVersion) po;
		int refID = plv.getPriceList().getReference_ID();
		if(refID <= 0)
			return null;
		
		MPriceList ref = new MPriceList(plv.getCtx(), refID, plv.get_TrxName());
		MPriceListVersion plvNew = new MPriceListVersion(ref);
		plvNew.setValidFrom(plv.getValidFrom());
		plvNew.setDocumentNo(plv.getDocumentNo().replace(plv.getPriceList().getC_Currency().getISO_Code(), ref.getC_Currency().getISO_Code()));
		plvNew.setManual(false);
		if(!plvNew.save())
			return "Failed when trying create version conversion.";
		
		MProductPrice[] pp = plv.getProductPrice(true);
		BigDecimal rate = MUNSConversionPriceRate.getRate(plv.getPriceList().getC_Currency_ID(),
				ref.getC_Currency_ID(), plv.getValidFrom(), plv.getAD_Org_ID(), plv.get_TrxName());
		
		for(int i=0;i<pp.length;i++)
		{
			MProductPrice ppNew = new MProductPrice(plv.getCtx(), plvNew.get_ID(), pp[i].getM_Product_ID(), plv.get_TrxName());
			ppNew.setPriceList(pp[i].getPriceList().multiply(rate));
			if(!ppNew.save())
				return CLogger.retrieveErrorString("Failed when trying copy product price.");
		}
		
		String sql = "UPDATE M_PriceList_Version SET DocStatus = 'CO', DocAction = 'CL', Processed = 'Y'"
				+ " WHERE M_PriceList_Version_ID = ?";
		if(DB.executeUpdate(sql, plvNew.get_ID(), plv.get_TrxName()) < 0)
			return "Failed when trying complete new version conversion.";
		
		return null;
	}
}