package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;


import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

public class MUNSTankSounding extends X_UNS_TankSounding{

	/**
	 * 
	 */
	private static final long serialVersionUID = 465191801284267123L;
	private BigDecimal m_DeliveredCPO = Env.ZERO;
	
	public MUNSTankSounding(Properties ctx, int UNS_TankSounding_ID,
			String trxName) {
		super(ctx, UNS_TankSounding_ID, trxName);
		
	}

	public MUNSTankSounding(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		MUNSTank tank = new MUNSTank(getCtx(), getUNS_Tank_ID(), get_TrxName());
		
		if(getHeightMeasured().signum() < 0)
		{
			log.saveError("Error", "negate weight disallowed");
			return false;
		}
		
		if(getUNS_SoundingBy_ID() > 0 && getSoundingBy() == null)
			setSoundingBy(getUNS_SoundingBy().getRealName() == null ?
							getUNS_SoundingBy().getName() : getUNS_SoundingBy().getRealName());
		
		initDeliveredQty();
		setDeliveredQty(m_DeliveredCPO == null ? Env.ZERO : m_DeliveredCPO);
		
		BigDecimal weight = Env.ZERO;
		if(!isEmpty())
			weight = MUNSTank.calculate(getCtx(), getUNS_Tank_ID(), getHeightMeasured(), getTemperature(), get_TrxName());
		
		setWeight(weight.subtract(getRecycledQty()));	
		setLastSoundingQty(tank.getLastSoundingDate() == null ? tank.getInitialLastSounding() : tank.getLastSoundingQty());
		setDifferenceQty(getWeight().add(getDeliveredQty()).subtract(getLastSoundingQty()));
		
		//		setDifferenceQty(getWeight().subtract(getLastSoundingQty()));
		
		
		return true;
	}	
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
//		if(success)
//			if(!upHeaderQty(true, newRecord))
//				return false;
//		
		if(success){
			
			String sql = "UPDATE uns_sounding so SET CPOProductionQty = (SELECt COALESCE(SUM(ts.DifferenceQty),0) FROM UNS_TankSounding ts WHERE"
						+ " ts.UNS_Sounding_ID = so.UNS_Sounding_ID) - CPOReturnQty, DeliveredQty = (SELECT COALESCE(SUM(ts1.DeliveredQty),0) FROM UNS_TankSounding ts1 "
						+ "WHERE ts1.UNS_Sounding_ID = so.UNS_Sounding_ID), LastSoundingQty = (SELECT COALESCE(SUM(ts2.LastSoundingQty),0) FROM UNS_TankSounding ts2 "
						+ "WHERE ts2.UNS_Sounding_ID = so.UNS_Sounding_ID), CPOCurrentSounding = (SELECT COALESCE(SUM(ts3.Weight),0) FROM UNS_TankSounding ts3 "
						+ "WHERE ts3.UNS_Sounding_ID = so.UNS_Sounding_ID) WHERE so.UNS_Sounding_ID = ?";
			DB.executeUpdate(sql, getUNS_Sounding_ID(), get_TrxName());
			
			String sql2= "UPDATE UNS_Sounding SET OERNettoI = CASE WHEN FFBProcessedNettoI = 0 THEN 0 ELSE ROUND(CPOProductionQty / FFBProcessedNettoI * 100 , 2) END"
					+ " ,OERNettoII = CASE WHEN FFBProcessedNettoII = 0 THEN 0 ELSE ROUND(CPOProductionQty / FFBProcessedNettoII * 100 , 2) END "
					+ "WHERE UNS_Sounding_ID = ?";
			DB.executeUpdate(sql2, getUNS_Sounding_ID(), get_TrxName());

			String sql3 = "SELECT CPOProductionQty FROM UNS_Sounding WHERE UNS_Sounding_ID = ? ";
			BigDecimal productionQtySounding = DB.getSQLValueBD(get_TrxName(), sql3, getUNS_Sounding_ID());
			
			//update qty all tank by flowmeter
			String sql4 = "UPDATE UNS_TankSounding ts SET WeightByFlowMeter = ( CASE WHEN ts.DifferenceQty = 0 THEN 0 ELSE ROUND((DifferenceQty / "+productionQtySounding
					+ ") * (SELECT FlowMeterQty FROM UNS_Sounding WHERE UNS_Sounding_ID = ts.UNS_Sounding_ID), 0) END ) WHERE ts.UNS_Sounding_ID = ?";
			DB.executeUpdate(sql4, getUNS_Sounding_ID(), get_TrxName());
			
			String sqll = "SELECT COALESCE(CPOReturnQty,0) FROM UNS_Sounding WHERE UNS_Sounding_ID = (SELECT UNS_PrevSounding_ID FROM"
					+ " UNS_Sounding WHERE UNS_Sounding_ID = ?)";
			BigDecimal	prevRetrunQty = DB.getSQLValueBD(get_TrxName(), sqll, getUNS_Sounding_ID());
			
			String sql5 = "UPDATE UNS_Sounding SET CPOProductionQty = CPOProductionQty - "+prevRetrunQty
					+ " WHERE UNS_Sounding_ID = ?";
			DB.executeUpdate(sql5, getUNS_Sounding_ID(), get_TrxName());
		}
		
		return true;
	}
	
//	protected boolean beforeDelete()
//	{
//		return upHeaderQty(false, true);
//	}

	@Override
	protected boolean afterDelete(boolean success) {
		
		if(success)
		{
			String sql = "UPDATE uns_sounding so SET CPOProductionQty = (SELECt COALESCE(SUM(ts.DifferenceQty),0) FROM UNS_TankSounding ts WHERE"
					+ " ts.UNS_Sounding_ID = so.UNS_Sounding_ID), DeliveredQty = (SELECT COALESCE(SUM(ts1.DeliveredQty),0) FROM UNS_TankSounding ts1 "
					+ "WHERE ts1.UNS_Sounding_ID = so.UNS_Sounding_ID), LastSoundingQty = (SELECT COALESCE(SUM(ts2.LastSoundingQty),0) FROM UNS_TankSounding ts2 "
					+ "WHERE ts2.UNS_Sounding_ID = so.UNS_Sounding_ID), CPOCurrentSounding = (SELECT COALESCE(SUM(ts3.Weight),0) FROM UNS_TankSounding ts3 "
					+ "WHERE ts3.UNS_Sounding_ID = so.UNS_Sounding_ID) WHERE so.UNS_Sounding_ID = ?";
			DB.executeUpdate(sql, getUNS_Sounding_ID(), get_TrxName());
			
			String sqll = "UPDATE UNS_Sounding SET OERNettoI = CASE WHEN FFBProcessedNettoI = 0 THEN 0 ELSE ROUND(CPOProductionQty / FFBProcessedNettoI * 100 , 2) END"
					+ " ,OERNettoII = CASE WHEN FFBProcessedNettoII = 0 THEN 0 ELSE ROUND(CPOProductionQty / FFBProcessedNettoII * 100 , 2) END "
					+ "WHERE UNS_Sounding_ID = ?";
			DB.executeUpdate(sqll, getUNS_Sounding_ID(), get_TrxName());
			
			String sql3 = "SELECT CPOProductionQty FROM UNS_Sounding WHERE UNS_Sounding_ID = ? ";
			BigDecimal productionQtySounding = DB.getSQLValueBD(get_TrxName(), sql3, getUNS_Sounding_ID());
			
			//update qty all tank by flowmeter
			String sql4 = "UPDATE UNS_TankSounding ts SET WeightByFlowMeter = ( CASE WHEN ts.DifferenceQty = 0 THEN 0 ELSE ROUND((DifferenceQty / "+productionQtySounding
					+ ") * (SELECT FlowMeterQty FROM UNS_Sounding WHERE UNS_Sounding_ID = ts.UNS_Sounding_ID), 0) END ) WHERE ts.UNS_Sounding_ID = ?";
			DB.executeUpdate(sql4, getUNS_Sounding_ID(), get_TrxName());
		}
		return true;
	}
	
	public BigDecimal getResultProduction()
	{
		BigDecimal result = Env.ZERO;
//		MUNSTank tank = new MUNSTank(getCtx(), getUNS_Tank_ID(), get_TrxName());
//		
//		BigDecimal qtyOnHand = MStorageOnHand.getQtyOnHandForLocator(get_ValueAsInt("M_Product_ID"),
//				tank.get_ValueAsInt("M_Locator_ID"), 0, get_TrxName());
		
		result = (getWeight().add(getDeliveredQty())).subtract(getLastSoundingQty());
		
		return result;
	}
	
	
//	public boolean upHeaderQty(boolean add, boolean newRecord)
//	{
//		MUNSSounding s = new MUNSSounding(getCtx(), getUNS_Sounding_ID(), get_TrxName());
//		
//		if(add && !newRecord)
//		{
//			s.setCPOProductionQty(s.getCPOProductionQty().add(getDifferenceQty())
//								.subtract((BigDecimal) get_ValueOld(COLUMNNAME_DifferenceQty)));
//			s.setDeliveredQty(s.getDeliveredQty().add(getDeliveredQty())
//								.subtract((BigDecimal) get_ValueOld(COLUMNNAME_DeliveredQty)));
//			s.setLastSoundingQty(s.getLastSoundingQty().add(getLastSoundingQty())
//								.subtract((BigDecimal) get_ValueOld(COLUMNNAME_LastSoundingQty)));
//		}
//		else if(add && newRecord)
//		{
//			s.setCPOProductionQty(s.getCPOProductionQty().add(getDifferenceQty()));
//			s.setDeliveredQty(s.getDeliveredQty().add(getDeliveredQty()));
//			s.setLastSoundingQty(s.getLastSoundingQty().add(getLastSoundingQty()));
//		}
//		else
//		{
//			s.setCPOProductionQty(s.getCPOProductionQty().subtract(getDifferenceQty()));
//			s.setDeliveredQty(s.getDeliveredQty().subtract(getDeliveredQty()));
//			s.setLastSoundingQty(s.getLastSoundingQty().subtract(getLastSoundingQty()));
//		}
//		if(!s.save())
//		{
//			log.log(Level.WARNING, "Failed when update header quantities");
//			return false;
//		}
//		
//		return true;
//	}
	
	private void initDeliveredQty()
	{
		MUNSSounding sound = new MUNSSounding(getCtx(), getUNS_Sounding_ID(), get_TrxName());
		MUNSTank tank = new MUNSTank(getCtx(), getUNS_Tank_ID(), get_TrxName());
		int idLoc = tank.getM_Locator_ID();
		
//		String sql = "SELECT SUM(dn.QtyLoc1) FROM UNS_Despatch_Notes dn WHERE dn.Locator1_ID=?"
//				+ " OR dn.Locator2_ID=? OR dn.Locator3_ID=? AND EXISTS (SELECT 1 FROM UNS_WeighbridgeTicket wt"
//				+ " WHERE wt.UNS_WeighbridgeTicket_ID = dn.UNS_WeighbridgeTicket_ID"
//				+ " AND wt.M_Product_ID=(SELECT M_Product_ID FROM M_Product WHERE Value=?"
//				+ " AND dn.EndDespatch BETWEEN '2016-12-08 00:00:00' AND ?::timestamp))";
		Timestamp startDate = 
				tank.getLastSoundingDate() == null ? TimeUtil.addDays(sound.getDateAcct(), -1) : tank.getLastSoundingDate();
		
		
		String sql = "SELECT COALESCE(SUM(tr.movementqty),0) FROM M_Transaction tr WHERE tr.movementtype = 'T-' AND m_locator_id = ? "
				+ " AND tr.M_Product_ID = ? AND tr.MovementDate BETWEEN ? AND ?";
		
		m_DeliveredCPO = DB.getSQLValueBD(get_TrxName(), sql,
				new Object[]{idLoc, tank.getM_Product_ID(), startDate, TimeUtil.addDays(sound.getDateAcct(), -1)});
		m_DeliveredCPO = m_DeliveredCPO.negate();
	}
}
