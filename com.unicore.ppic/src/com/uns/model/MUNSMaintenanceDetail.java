package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class MUNSMaintenanceDetail extends X_UNS_MaintenanceDetail implements
		DocAction, IUNSApprovalInfo {

	/**
	 * @author Thunder
	 */
	private static final long serialVersionUID = 8027896747494702976L;

	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private MUNSMaintenanceMaterial[] m_lines = null;
	
	public MUNSMaintenanceDetail(Properties ctx, int UNS_MaintenanceDetail_ID,
			String trxName) {
		super(ctx, UNS_MaintenanceDetail_ID, trxName);

	}

	public MUNSMaintenanceDetail(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	public MUNSMaintenanceMaterial[] getLines() {
		
		if(m_lines != null)
			return m_lines;
		
		List<MUNSMaintenanceMaterial> list = new Query(getCtx(), I_UNS_MaintenanceMaterial.Table_Name,
				"UNS_MaintenanceDetail_ID =?", get_TrxName())
			.setParameters(getUNS_MaintenanceDetail_ID())
			.list();
		
		m_lines = new MUNSMaintenanceMaterial[list.size()];
		list.toArray(m_lines);
		return m_lines;
	}

	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		
		List<Object[]> list = new ArrayList<>();
		list.add(new Object[]{Date.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{Date.class, true});
		list.add(new Object[]{Date.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		
		return list;
	}

	@Override
	public String[] getDetailTableHeader() {
		
		String header[] = new String[]{"Document Date","Leader","Problem","Solution","Start Time"
				,"End Time","Life Time of Machine","Description"};
		
		return header;
	}

	@Override
	public List<Object[]> getDetailTableContent() {
		
		List<Object[]> list = new ArrayList<>();
		String sql = "SELECT DateDoc, Leader, Problem, Solution, StartTime, EndTime,"
				+ " LifeTime, Description FROM UNS_MaintenanceDetail"
				+ " WHERE UNS_MaintenanceDetail_ID = ?";
		
		PreparedStatement sta = null;
		ResultSet rs = null;
		try {
			sta = DB.prepareStatement(sql, get_TrxName());
			sta.setInt(1, get_ID());
			rs = sta.executeQuery();
			while(rs.next())
			{
				int count = 0;
				Object[] rowData = new Object[8];
				rowData[count] = rs.getObject(count+1); //DateDoc
				rowData[++count] = rs.getObject(count+1); //Leader
				rowData[++count] = rs.getObject(count+1); //Proble
				rowData[++count] = rs.getObject(count+1); //Solution
				rowData[++count] = rs.getObject(count+1); //Start Time
				rowData[++count] = rs.getObject(count+1); //End Time
				rowData[++count] = rs.getObject(count+1); //LifeTime of Machine
				rowData[++count] = rs.getObject(count+1); //Description
				
				list.add(rowData);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally{
			DB.close(rs, sta);
		}
		return list;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(parentIsSummary())
			throw new AdempiereException("Disallowed create or update if the parent isStation");
		
		if(getEndTime().before(getStartTime()))
			throw new AdempiereException("Disallowed StartTime greater than EndTime");
		
		return true;
	}
	
	public boolean parentIsSummary() {
		
		String sql = "SELECT 1 FROM UNS_MaintenanceAct WHERE UNS_MaintenanceAct_ID = ?"
				+ " AND IsSummary = 'Y' ";
		int val = DB.getSQLValue(get_TrxName(), sql, getUNS_MaintenanceAct_ID());
		
		return val == 1;
	}

	@Override
	public boolean processIt(String processAction) throws Exception {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}

	@Override
	public String prepareIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		getLines();
		if(m_lines == null || m_lines.length == 0)
		{
			m_processMsg = "No Material";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	@Override
	public String completeIt() {
		
		if(!m_justPrepared)
		{
			String status = prepareIt();
			if(!DocAction.STATUS_InProgress.equals(status))
				return status; 
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		setDocAction(STATUS_Closed);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public boolean closeIt() {
		
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
//		if(m_processMsg != null)
//			return false;
//		
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
//		if(m_processMsg != null)
//			return false;
		
		m_processMsg = "DisAllowed Close";
		return false;
	}

	@Override
	public boolean reverseCorrectIt() {
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSECORRECT);
//		if(m_processMsg != null)
//			return false;
//		
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSECORRECT);
//		if(m_processMsg != null)
//			return false;
		
		m_processMsg = "DisAllowed Reverse CorrectIt";
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
//		if(m_processMsg != null)
//			return false;
//		
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
//		if(m_processMsg != null)
//			return false;
		
		m_processMsg = "DisAllowed Reverse AccrualIt";
		return false;
	}

	@Override
	public boolean reActivateIt() {
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REACTIVATE);
//		if(m_processMsg != null)
//			return false;
//		
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REACTIVATE);
//		if(m_processMsg != null)
//			return false;
//		
		m_processMsg = "DisAllowed Reactive";
		return false;
	}

	@Override
	public String getSummary() {
		StringBuffer sb = new StringBuffer();
		sb.append(getLine());
		String sql = "SELECT Name FROM UNS_MaintenanceAct WHERE"
				+ " UNS_MaintenanceAct_ID = ?";
		String machineName = DB.getSQLValueString(get_TrxName(), sql,
				getUNS_MaintenanceAct_ID());
		
		sb.append(" : ").append(machineName);
		
		if(!parentIsSummary())
		{
			sql = "SELECT Name FROM UNS_MaintenanceAct WHERE"
					+ " UNS_MaintenanceAct_ID = (SELECT Link_MaintenanceAct_ID"
					+ " FROM UNS_MaintenanceAct WHERE UNS_MaintenanceAct_ID = ?)";
			String stationName = DB.getSQLValueString(get_TrxName(), sql, getUNS_MaintenanceAct_ID());
			sb.append(" On ").append(stationName);
		}
		return sb.toString();
	}

	@Override
	public String getDocumentInfo() {
		
		return Msg.getElement(getCtx(), "M_UNSMaintenanceDetail") + " " + getDocumentNo();
	}

	@Override
	public File createPDF() {
		
		return null;
	}

	@Override
	public String getProcessMsg() {
		
		return m_processMsg;
	}

	@Override
	public String getDocumentNo() {
		
		return null;
	}

	@Override
	public int getDoc_User_ID() {
		
		return Env.getContextAsInt(getCtx(), "AD_User_ID");
	}

	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		
		return null;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}

}
