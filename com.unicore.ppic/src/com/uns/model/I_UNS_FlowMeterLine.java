/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_FlowMeterLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_FlowMeterLine 
{

    /** TableName=UNS_FlowMeterLine */
    public static final String Table_Name = "UNS_FlowMeterLine";

    /** AD_Table_ID=1000334 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Duration */
    public static final String COLUMNNAME_Duration = "Duration";

	/** Set Duration.
	  * Normal Duration in Duration Unit
	  */
	public void setDuration (Timestamp Duration);

	/** Get Duration.
	  * Normal Duration in Duration Unit
	  */
	public Timestamp getDuration();

    /** Column name FlowMeterAverageKG */
    public static final String COLUMNNAME_FlowMeterAverageKG = "FlowMeterAverageKG";

	/** Set FlowMeterAverageKG	  */
	public void setFlowMeterAverageKG (BigDecimal FlowMeterAverageKG);

	/** Get FlowMeterAverageKG	  */
	public BigDecimal getFlowMeterAverageKG();

    /** Column name FlowMeterLimit */
    public static final String COLUMNNAME_FlowMeterLimit = "FlowMeterLimit";

	/** Set FlowMeterLimit	  */
	public void setFlowMeterLimit (String FlowMeterLimit);

	/** Get FlowMeterLimit	  */
	public String getFlowMeterLimit();

    /** Column name FlowMeterMaxDate */
    public static final String COLUMNNAME_FlowMeterMaxDate = "FlowMeterMaxDate";

	/** Set FlowMeterMaxDate	  */
	public void setFlowMeterMaxDate (Timestamp FlowMeterMaxDate);

	/** Get FlowMeterMaxDate	  */
	public Timestamp getFlowMeterMaxDate();

    /** Column name FlowMeterMaxKG */
    public static final String COLUMNNAME_FlowMeterMaxKG = "FlowMeterMaxKG";

	/** Set FlowMeterMaxKG	  */
	public void setFlowMeterMaxKG (BigDecimal FlowMeterMaxKG);

	/** Get FlowMeterMaxKG	  */
	public BigDecimal getFlowMeterMaxKG();

    /** Column name FlowMeterMaxTimestamp */
    public static final String COLUMNNAME_FlowMeterMaxTimestamp = "FlowMeterMaxTimestamp";

	/** Set FlowMeterMaxTimestamp	  */
	public void setFlowMeterMaxTimestamp (Timestamp FlowMeterMaxTimestamp);

	/** Get FlowMeterMaxTimestamp	  */
	public Timestamp getFlowMeterMaxTimestamp();

    /** Column name FlowMeterMinDate */
    public static final String COLUMNNAME_FlowMeterMinDate = "FlowMeterMinDate";

	/** Set FlowMeterMinDate	  */
	public void setFlowMeterMinDate (Timestamp FlowMeterMinDate);

	/** Get FlowMeterMinDate	  */
	public Timestamp getFlowMeterMinDate();

    /** Column name FlowMeterMinKG */
    public static final String COLUMNNAME_FlowMeterMinKG = "FlowMeterMinKG";

	/** Set FlowMeterMinKG	  */
	public void setFlowMeterMinKG (BigDecimal FlowMeterMinKG);

	/** Get FlowMeterMinKG	  */
	public BigDecimal getFlowMeterMinKG();

    /** Column name FlowMeterMinTimestamp */
    public static final String COLUMNNAME_FlowMeterMinTimestamp = "FlowMeterMinTimestamp";

	/** Set FlowMeterMinTimestamp	  */
	public void setFlowMeterMinTimestamp (Timestamp FlowMeterMinTimestamp);

	/** Get FlowMeterMinTimestamp	  */
	public Timestamp getFlowMeterMinTimestamp();

    /** Column name FlowMeterStatus */
    public static final String COLUMNNAME_FlowMeterStatus = "FlowMeterStatus";

	/** Set FlowMeterStatus	  */
	public void setFlowMeterStatus (String FlowMeterStatus);

	/** Get FlowMeterStatus	  */
	public String getFlowMeterStatus();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name StartDate */
    public static final String COLUMNNAME_StartDate = "StartDate";

	/** Set Start Date.
	  * First effective day (inclusive)
	  */
	public void setStartDate (Timestamp StartDate);

	/** Get Start Date.
	  * First effective day (inclusive)
	  */
	public Timestamp getStartDate();

    /** Column name StartTime */
    public static final String COLUMNNAME_StartTime = "StartTime";

	/** Set Start Time.
	  * Time started
	  */
	public void setStartTime (Timestamp StartTime);

	/** Get Start Time.
	  * Time started
	  */
	public Timestamp getStartTime();

    /** Column name StopDate */
    public static final String COLUMNNAME_StopDate = "StopDate";

	/** Set StopDate	  */
	public void setStopDate (Timestamp StopDate);

	/** Get StopDate	  */
	public Timestamp getStopDate();

    /** Column name StopTime */
    public static final String COLUMNNAME_StopTime = "StopTime";

	/** Set StopTime	  */
	public void setStopTime (Timestamp StopTime);

	/** Get StopTime	  */
	public Timestamp getStopTime();

    /** Column name TotalCounter */
    public static final String COLUMNNAME_TotalCounter = "TotalCounter";

	/** Set TotalCounter	  */
	public void setTotalCounter (BigDecimal TotalCounter);

	/** Get TotalCounter	  */
	public BigDecimal getTotalCounter();

    /** Column name TotalLimit */
    public static final String COLUMNNAME_TotalLimit = "TotalLimit";

	/** Set TotalLimit	  */
	public void setTotalLimit (String TotalLimit);

	/** Get TotalLimit	  */
	public String getTotalLimit();

    /** Column name TotalStatus */
    public static final String COLUMNNAME_TotalStatus = "TotalStatus";

	/** Set TotalStatus	  */
	public void setTotalStatus (String TotalStatus);

	/** Get TotalStatus	  */
	public String getTotalStatus();

    /** Column name UNS_FlowMeter_ID */
    public static final String COLUMNNAME_UNS_FlowMeter_ID = "UNS_FlowMeter_ID";

	/** Set UNS_FlowMeter_ID	  */
	public void setUNS_FlowMeter_ID (int UNS_FlowMeter_ID);

	/** Get UNS_FlowMeter_ID	  */
	public int getUNS_FlowMeter_ID();

	public com.uns.model.I_UNS_FlowMeter getUNS_FlowMeter() throws RuntimeException;

    /** Column name UNS_FlowMeterLine_ID */
    public static final String COLUMNNAME_UNS_FlowMeterLine_ID = "UNS_FlowMeterLine_ID";

	/** Set UNS_FlowMeterLine_ID	  */
	public void setUNS_FlowMeterLine_ID (int UNS_FlowMeterLine_ID);

	/** Get UNS_FlowMeterLine_ID	  */
	public int getUNS_FlowMeterLine_ID();

    /** Column name UNS_FlowMeterLine_UU */
    public static final String COLUMNNAME_UNS_FlowMeterLine_UU = "UNS_FlowMeterLine_UU";

	/** Set UNS_FlowMeterLine_UU	  */
	public void setUNS_FlowMeterLine_UU (String UNS_FlowMeterLine_UU);

	/** Get UNS_FlowMeterLine_UU	  */
	public String getUNS_FlowMeterLine_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
