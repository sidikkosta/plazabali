package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MTree_Base;
import org.compiere.model.MTree_Node;
import org.compiere.util.DB;

import com.uns.base.model.Query;

public class MUNSMaintenanceAct extends X_UNS_MaintenanceAct {

	/**
	 * @author Thunder
	 */
	private static final long serialVersionUID = -2999451170349645732L;
	
	private MUNSMaintenanceDetail[] m_lines = null;

	public MUNSMaintenanceAct(Properties ctx, int UNS_MaintenanceAct_ID,
			String trxName) {
		super(ctx, UNS_MaintenanceAct_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSMaintenanceAct(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(!newRecord && 
				(is_ValueChanged(COLUMNNAME_IsSummary) || is_ValueChanged(COLUMNNAME_IsActive)
						|| is_ValueChanged(COLUMNNAME_Link_MaintenanceAct_ID)))
		{
			getLines();
			for(MUNSMaintenanceDetail line : m_lines)
			{
				if(line.isActive())
					throw new AdempiereException("Cannot change. There was detail still active");
			}
		}
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		if(newRecord)
			insert_Tree(MTree_Base.TREETYPE_MaintenanceActivities);
		
		updateNode();
		
		return success;
	}
	
	public MUNSMaintenanceDetail[] getLines() {
		
		if (m_lines != null) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		  
 	 	String whereClause = I_UNS_MaintenanceDetail.COLUMNNAME_UNS_MaintenanceAct_ID+"=?";
	 	List <MUNSMaintenanceDetail> list = new Query(getCtx(), I_UNS_MaintenanceDetail.Table_Name, whereClause, get_TrxName())
			.setParameters(get_ID())
			.setOrderBy(I_UNS_MaintenanceDetail.COLUMNNAME_Line)
			.list();

		m_lines = new MUNSMaintenanceDetail[list.size ()];
		list.toArray (m_lines);
		return m_lines;
	}
	
	public void updateNode()
	{
		int parent_ID = 0;
		if(!isSummary())
		{
			parent_ID = getLink_MaintenanceAct_ID();
		}
		
		//get Name of Table UNS_MaintenanceAct
		String sqql = "SELECT Name FROM AD_Table WHERE AD_Table_ID = ?";
		String nameTab = DB.getSQLValueString(get_TrxName(), sqql, Table_ID);
		
		//get Tree ID
		String sql = "SELECT AD_Tree_ID FROM AD_Tree WHERE Name = ? AND TreeType = ?";
		int tree_ID = DB.getSQLValue(get_TrxName(), sql, nameTab , MTree_Base.TREETYPE_MaintenanceActivities);
		if(tree_ID <= 0)
			throw new AdempiereException("Cannot found Tree ID");
		
		MTree_Base tree = MTree_Base.get(getCtx(), tree_ID, get_TrxName());
		MTree_Node node = MTree_Node.get(getCtx(), tree, get_ID(), get_TrxName());
		node.setParent_ID(parent_ID);
		node.saveEx();
	}
	
	@Override
	protected boolean afterDelete(boolean success) {
		
		if(success)
			delete_Tree(MTree_Base.TREETYPE_MaintenanceActivities);
		
		return success;
	}
}
