/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_TankSounding
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_TankSounding extends PO implements I_UNS_TankSounding, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180219L;

    /** Standard Constructor */
    public X_UNS_TankSounding (Properties ctx, int UNS_TankSounding_ID, String trxName)
    {
      super (ctx, UNS_TankSounding_ID, trxName);
      /** if (UNS_TankSounding_ID == 0)
        {
			setDeliveredQty (Env.ZERO);
// 0
			setDOBI (Env.ZERO);
// 0
			setFFA (Env.ZERO);
// 0
			setHeightMeasured (Env.ZERO);
			setisEmpty (false);
// N
			setLastSoundingQty (Env.ZERO);
// 0
			setMI (Env.ZERO);
// 0
			setRecycledQty (Env.ZERO);
// 0
			setTemperature (Env.ZERO);
// 0
			setUNS_Tank_ID (0);
			setUNS_TankSounding_ID (0);
			setWeight (Env.ZERO);
// 0
			setWeightByFlowMeter (Env.ZERO);
// 0
        } */
    }

    /** Load Constructor */
    public X_UNS_TankSounding (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_TankSounding[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Delivered Quantity.
		@param DeliveredQty 
		Quantity of Delivered
	  */
	public void setDeliveredQty (BigDecimal DeliveredQty)
	{
		set_Value (COLUMNNAME_DeliveredQty, DeliveredQty);
	}

	/** Get Delivered Quantity.
		@return Quantity of Delivered
	  */
	public BigDecimal getDeliveredQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeliveredQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Difference.
		@param DifferenceQty 
		Difference Quantity
	  */
	public void setDifferenceQty (BigDecimal DifferenceQty)
	{
		set_Value (COLUMNNAME_DifferenceQty, DifferenceQty);
	}

	/** Get Difference.
		@return Difference Quantity
	  */
	public BigDecimal getDifferenceQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set DOBI.
		@param DOBI DOBI	  */
	public void setDOBI (BigDecimal DOBI)
	{
		set_Value (COLUMNNAME_DOBI, DOBI);
	}

	/** Get DOBI.
		@return DOBI	  */
	public BigDecimal getDOBI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DOBI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFA.
		@param FFA 
		Free Fatty Acid
	  */
	public void setFFA (BigDecimal FFA)
	{
		set_Value (COLUMNNAME_FFA, FFA);
	}

	/** Get FFA.
		@return Free Fatty Acid
	  */
	public BigDecimal getFFA () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFA);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Measured (cm).
		@param HeightMeasured Height Measured (cm)	  */
	public void setHeightMeasured (BigDecimal HeightMeasured)
	{
		set_Value (COLUMNNAME_HeightMeasured, HeightMeasured);
	}

	/** Get Height Measured (cm).
		@return Height Measured (cm)	  */
	public BigDecimal getHeightMeasured () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightMeasured);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Empty.
		@param isEmpty Empty	  */
	public void setisEmpty (boolean isEmpty)
	{
		set_Value (COLUMNNAME_isEmpty, Boolean.valueOf(isEmpty));
	}

	/** Get Empty.
		@return Empty	  */
	public boolean isEmpty () 
	{
		Object oo = get_Value(COLUMNNAME_isEmpty);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Last Sounding Quantity.
		@param LastSoundingQty Last Sounding Quantity	  */
	public void setLastSoundingQty (BigDecimal LastSoundingQty)
	{
		set_Value (COLUMNNAME_LastSoundingQty, LastSoundingQty);
	}

	/** Get Last Sounding Quantity.
		@return Last Sounding Quantity	  */
	public BigDecimal getLastSoundingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LastSoundingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set MI.
		@param MI 
		Moisture & Impurity
	  */
	public void setMI (BigDecimal MI)
	{
		set_Value (COLUMNNAME_MI, MI);
	}

	/** Get MI.
		@return Moisture & Impurity
	  */
	public BigDecimal getMI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Recycled Qty.
		@param RecycledQty Recycled Qty	  */
	public void setRecycledQty (BigDecimal RecycledQty)
	{
		set_Value (COLUMNNAME_RecycledQty, RecycledQty);
	}

	/** Get Recycled Qty.
		@return Recycled Qty	  */
	public BigDecimal getRecycledQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RecycledQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sounding By.
		@param SoundingBy Sounding By	  */
	public void setSoundingBy (String SoundingBy)
	{
		set_Value (COLUMNNAME_SoundingBy, SoundingBy);
	}

	/** Get Sounding By.
		@return Sounding By	  */
	public String getSoundingBy () 
	{
		return (String)get_Value(COLUMNNAME_SoundingBy);
	}

	/** Set Temperature.
		@param Temperature 
		The temperature
	  */
	public void setTemperature (BigDecimal Temperature)
	{
		set_Value (COLUMNNAME_Temperature, Temperature);
	}

	/** Get Temperature.
		@return The temperature
	  */
	public BigDecimal getTemperature () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Temperature);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_AD_User getUNS_SoundingBy() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getUNS_SoundingBy_ID(), get_TrxName());	}

	/** Set Sounding By.
		@param UNS_SoundingBy_ID Sounding By	  */
	public void setUNS_SoundingBy_ID (int UNS_SoundingBy_ID)
	{
		if (UNS_SoundingBy_ID < 1) 
			set_Value (COLUMNNAME_UNS_SoundingBy_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_SoundingBy_ID, Integer.valueOf(UNS_SoundingBy_ID));
	}

	/** Get Sounding By.
		@return Sounding By	  */
	public int getUNS_SoundingBy_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SoundingBy_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Sounding getUNS_Sounding() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Sounding)MTable.get(getCtx(), com.uns.model.I_UNS_Sounding.Table_Name)
			.getPO(getUNS_Sounding_ID(), get_TrxName());	}

	/** Set Sounding.
		@param UNS_Sounding_ID Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID)
	{
		if (UNS_Sounding_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, Integer.valueOf(UNS_Sounding_ID));
	}

	/** Get Sounding.
		@return Sounding	  */
	public int getUNS_Sounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Sounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Tank getUNS_Tank() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Tank)MTable.get(getCtx(), com.uns.model.I_UNS_Tank.Table_Name)
			.getPO(getUNS_Tank_ID(), get_TrxName());	}

	/** Set Tank.
		@param UNS_Tank_ID Tank	  */
	public void setUNS_Tank_ID (int UNS_Tank_ID)
	{
		if (UNS_Tank_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, Integer.valueOf(UNS_Tank_ID));
	}

	/** Get Tank.
		@return Tank	  */
	public int getUNS_Tank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Tank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tank Sounding.
		@param UNS_TankSounding_ID Tank Sounding	  */
	public void setUNS_TankSounding_ID (int UNS_TankSounding_ID)
	{
		if (UNS_TankSounding_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_TankSounding_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_TankSounding_ID, Integer.valueOf(UNS_TankSounding_ID));
	}

	/** Get Tank Sounding.
		@return Tank Sounding	  */
	public int getUNS_TankSounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_TankSounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_TankSounding_UU.
		@param UNS_TankSounding_UU UNS_TankSounding_UU	  */
	public void setUNS_TankSounding_UU (String UNS_TankSounding_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_TankSounding_UU, UNS_TankSounding_UU);
	}

	/** Get UNS_TankSounding_UU.
		@return UNS_TankSounding_UU	  */
	public String getUNS_TankSounding_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_TankSounding_UU);
	}

	/** Set Weight.
		@param Weight 
		Weight of a product
	  */
	public void setWeight (BigDecimal Weight)
	{
		set_ValueNoCheck (COLUMNNAME_Weight, Weight);
	}

	/** Get Weight.
		@return Weight of a product
	  */
	public BigDecimal getWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Weight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Weight By Flow Meter.
		@param WeightByFlowMeter Weight By Flow Meter	  */
	public void setWeightByFlowMeter (BigDecimal WeightByFlowMeter)
	{
		set_Value (COLUMNNAME_WeightByFlowMeter, WeightByFlowMeter);
	}

	/** Get Weight By Flow Meter.
		@return Weight By Flow Meter	  */
	public BigDecimal getWeightByFlowMeter () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WeightByFlowMeter);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}