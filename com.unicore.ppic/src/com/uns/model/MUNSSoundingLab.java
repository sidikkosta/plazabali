package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;



import org.compiere.util.Env;

import com.uns.model.X_UNS_SoundingLab;

public class MUNSSoundingLab extends X_UNS_SoundingLab implements DocAction, DocOptions
{

	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = 272793684916165927L;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	
	
	
	public MUNSSoundingLab(Properties ctx, int UNS_SoundingLab_ID,
			String trxName) {
		super(ctx, UNS_SoundingLab_ID, trxName);
	}

	public MUNSSoundingLab(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		if(newRecord)
			calcLossesFFB(null, newRecord);
		if(is_ValueChanged(COLUMNNAME_HPhaseSample) || is_ValueChanged(COLUMNNAME_SolidSample) || is_ValueChanged(COLUMNNAME_CondSample) || is_ValueChanged(COLUMNNAME_RecSample)
				|| is_ValueChanged(COLUMNNAME_Press1Sample) || is_ValueChanged(COLUMNNAME_Press2Sample) || is_ValueChanged(COLUMNNAME_Press3Sample) || is_ValueChanged(COLUMNNAME_NutSample)
				|| is_ValueChanged(COLUMNNAME_JKSample))
			calcLossesFFB(MUNSSounding.PRODUCT_CPO, newRecord);
		if(is_ValueChanged(COLUMNNAME_LTDS1Sample) || is_ValueChanged(COLUMNNAME_LTDS2Sample) || is_ValueChanged(COLUMNNAME_WetSellSample) || is_ValueChanged(COLUMNNAME_FCycloneSample))
			calcLossesFFB(MUNSSounding.PRODUCT_KERNEL, newRecord);
			
			
		return true;
	}
	@Override
	public boolean processIt(String action) throws Exception {
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		return true;
	}

	@Override
	public boolean invalidateIt() {
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_justPrepared = true;
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		setProcessed(true);
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		setProcessed(false);
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DOCSTATUS_InProgress.equals(status))
			{
				return DOCSTATUS_Invalid;
			}
		}
		if (DOCACTION_Prepare.equals(getDocAction()))
		{
			return DOCSTATUS_InProgress;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		if (!isApproved())
		{
			setIsApproved(true);
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		addDescription("::Voided::");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		setProcessed(true);
		
		return true;
	}

	@Override
	public boolean closeIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (null == m_processMsg)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		return voidIt();
	}

	@Override
	public boolean reverseAccrualIt() {
		return voidIt();
	}

	@Override
	public boolean reActivateIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setDocAction(DOCACTION_Complete);
		rejectIt();
		return true;
	}

	@Override
	public String getSummary() {
		
		return getDocumentNo();
	}

	@Override
	public String getDocumentInfo() {
		return getSummary();
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void addDescription (String description)
	{
		String desc = getDescription();
		if (null == desc)
			desc = "";
		else desc += " | ";
		
		desc += description;
		setDescription(description);
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		if (DOCSTATUS_Completed.equals(getDocStatus()))
		{
			options[index++] = DOCACTION_Re_Activate;
		}
		
		return index;
	}
	
	private String calcLossesFFB(String ProductName, boolean newRecord)
	{
		BigDecimal multhphase = BigDecimal.valueOf(15);
		BigDecimal multsolid = BigDecimal.valueOf(8);
		BigDecimal multcond = BigDecimal.valueOf(10);
		BigDecimal multrec = BigDecimal.valueOf(35);
		BigDecimal multpress = BigDecimal.valueOf(12);
		BigDecimal multnut = BigDecimal.valueOf(6);
		BigDecimal multjk = BigDecimal.valueOf(20);
		BigDecimal multltds1 = BigDecimal.valueOf(7);
		BigDecimal multltds2 = BigDecimal.valueOf(7);
		BigDecimal multwetshell = BigDecimal.valueOf(9);
		BigDecimal multfcyclone = BigDecimal.valueOf(12);
		BigDecimal multfcycloneoil = BigDecimal.valueOf(8);
		
		m_processMsg = null;
		MUNSSounding sound = new MUNSSounding(getCtx(), getUNS_Sounding_ID(), get_TrxName());
		
		if(newRecord || ProductName.equals(MUNSSounding.PRODUCT_CPO))
		{
			setCondFFB((getCondSample().multiply(multcond)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setHPhaseFFB((getHPhaseSample().multiply(multhphase)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setSolidFFB((getSolidSample().multiply(multsolid)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setRecFFB((getRecSample().multiply(multrec)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setAVGPressFFB(((getPress1Sample().add(getPress2Sample().add(getPress3Sample())).divide(BigDecimal.valueOf(3), 2, RoundingMode.HALF_UP)).multiply(multpress))
					.divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setNutFFB((getNutSample().multiply(multnut)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setJKFFB((getJKSample().multiply(multjk)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setFCycloneFFB_Oil((getFCycloneSample_Oil().multiply(multfcycloneoil)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setTotalCPOLosses(getHPhaseFFB().add(getSolidFFB()).add(getCondFFB()).add(getRecFFB())
					.add(getAVGPressFFB()).add(getNutFFB()).add(getJKFFB()).add(getFCycloneFFB_Oil()));
			if(getOERNettoI().signum()==0)
				setEffEkstraksiCPO(Env.ZERO);
			else
				setEffEkstraksiCPO(sound.getOERNettoI().divide((sound.getOERNettoI().add(getTotalCPOLosses())), 4, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED));
			
		} 
		
		if(newRecord || ProductName.equals(MUNSSounding.PRODUCT_KERNEL))	
		{
			setLTDS1FFB((getLTDS1Sample().multiply(multltds1)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setLTDS2FFB((getLTDS2Sample().multiply(multltds2)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setWetSellFFB((getWetSellSample().multiply(multwetshell)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setFCycloneFFB((getFCycloneSample().multiply(multfcyclone)).divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP));
			setTotalKernelLosses(getLTDS1FFB().add(getLTDS2FFB().add(getWetSellFFB().add(getFCycloneFFB()))));
			if(getKERNettoI().signum()==0)
				setEffEkstraksiKernel(Env.ZERO);
			else
				setEffEkstraksiKernel((((Env.ONEHUNDRED.subtract(sound.getDirtKernel())).multiply(getKERNettoI())).divide( (Env.ONEHUNDRED.subtract(sound.getDirtKernel()))
					.multiply(sound.getKERNettoI()).add(getTotalKernelLosses()), 4, RoundingMode.HALF_UP)).multiply(Env.ONEHUNDRED));
			
		}
			
		return null;
	}
	
}
