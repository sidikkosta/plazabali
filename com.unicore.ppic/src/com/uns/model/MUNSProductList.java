/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MCurrency;
import org.compiere.model.MProductCategory;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSProductList extends X_UNS_ProductList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2846925406619896815L;

	/**
	 * @param ctx
	 * @param UNS_ProductList_ID
	 * @param trxName
	 */
	public MUNSProductList(Properties ctx, int UNS_ProductList_ID,
			String trxName) {
		super(ctx, UNS_ProductList_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSProductList(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSProductList get(MUNSPriceLabelPrint parent, int ETBBCode, String SKU)
	{
		String whereClause = "UNS_PriceLabelPrint_ID = ? AND ETBBCode = ? AND SKU = ?";
		MUNSProductList list = Query.get(parent.getCtx(), UNSPPICModelFactory.getExtensionID(),
				Table_Name, whereClause, parent.get_TrxName()).setParameters(parent.get_ID(), ETBBCode, SKU)
					.firstOnly();
		
		return list;
	} 
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(isManual())
		{
			if(newRecord)
			{
				org.compiere.model.MProduct p = MProduct.getOfSKU(getCtx(), getSKU(), get_TrxName());
				if(p == null)
				{
					log.saveError("Error", "Not found product for SKU " + getSKU());
					return false;
				}
				
				String sql1 = "SELECT getproductcategorytopparent(?)";
				int topParentCategoryID = DB.getSQLValue(get_TrxName(), sql1, p.get_ID());
				MProductCategory category = new MProductCategory(getCtx(), topParentCategoryID, get_TrxName());
				setCategoryCode(category.getValue());
				setProductName(p.getName());
			}
			if(getQty().signum() <= 0)
			{
				log.saveError("Error", "Please set Qty.");
				return false;
			}
			
			if(getIDRAmount().signum() == 0
					&& getUNS_PriceLabelPrint().getDocSourceType().equals(MUNSPriceLabelPrint.DOCSOURCETYPE_Manual))
			{
				String sql = "SELECT pp.PriceList FROM M_ProductPrice pp"
						+ " INNER JOIN M_PriceList_Version plv ON plv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID"
						+ " INNER JOIN M_Product p ON p.M_Product_ID = pp.M_Product_ID"
						+ " INNER JOIN M_PriceList pl ON pl.M_PriceList_ID = plv.M_PriceList_ID"
						+ " WHERE pl.IsSOPriceList = 'Y' AND plv.ValidFrom <= ? AND TRIM(p.SKU) = TRIM(?) AND pl.C_Currency_ID = 303";
				if(getUNS_PriceLabelPrint().getM_PriceList_ID() > 0)
					sql += " AND pl.M_PriceList_ID = " + getUNS_PriceLabelPrint().getM_PriceList_ID();
						
				sql += " ORDER BY plv.ValidFrom DESC";
				BigDecimal price = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PriceLabelPrint().getValidFrom(), getSKU());
				
				if(price == null)
				{
					setIDRAmount(Env.ZERO);
					setUSDAmount(Env.ZERO);
				}
				else
				{
					BigDecimal rate = MUNSConversionPriceRate.getRate(MCurrency.get(getCtx(), "USD").get_ID(),
							MCurrency.get(getCtx(), "IDR").get_ID(), getUNS_PriceLabelPrint().getValidFrom(),
							getAD_Org_ID(), get_TrxName());
					if(rate != null)
					{
						setUSDAmount(price.divide(rate, 2));
						setIDRAmount(price);
					}
				}
			}
		}
		
		return true;
	}	
}