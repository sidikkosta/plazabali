/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPriceLabelPrinterConf extends X_UNS_PriceLabelPrinterConf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4859031617063960186L;

	/**
	 * @param ctx
	 * @param UNS_PriceLabelPrinterConf_ID
	 * @param trxName
	 */
	public MUNSPriceLabelPrinterConf(Properties ctx,
			int UNS_PriceLabelPrinterConf_ID, String trxName) {
		super(ctx, UNS_PriceLabelPrinterConf_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPriceLabelPrinterConf(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSPrintLabelTemplate[] getTemplates()
	{
		List<MUNSPrintLabelTemplate> list = Query.get(getCtx(),
				UNSPPICModelFactory.getExtensionID(), MUNSPrintLabelTemplate.Table_Name,
					COLUMNNAME_UNS_PriceLabelPrinterConf_ID + "=?", get_TrxName())
						.setParameters(get_ID()).list();
		return list.toArray(new MUNSPrintLabelTemplate[list.size()]);
	}
}