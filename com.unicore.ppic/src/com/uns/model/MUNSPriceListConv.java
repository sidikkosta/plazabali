/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPrice;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;
import com.uns.util.MessageBox;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPriceListConv extends X_UNS_PriceListConv implements DocAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3343468870224452701L;

	public MUNSPriceListConv(Properties ctx, int UNS_PriceListConv_ID,
			String trxName) {
		super(ctx, UNS_PriceListConv_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSPriceListConv[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_PriceListConv_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		if(getCurrencyFrom_ID() == getCurrencyTo_ID())
		{
			MessageBox.showMsg(this, getCtx(), "The same currency will make a duplicate only.",
					"Same Currency.", MessageBox.OK, MessageBox.ICONINFORMATION);
		}
	
		BigDecimal rate = MUNSConversionPriceRate.getRate(getCurrencyFrom_ID(),
				getCurrencyTo_ID(), getValidFrom(), getAD_Org_ID(), get_TrxName());
		
		if(rate == null)
			rate = Env.ZERO;
		
		setRate(rate);
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if((newRecord || is_ValueChanged(COLUMNNAME_IsAllProduct)))
		{
			if(!beforeDelete())
			{
				log.saveError("Error", "Failed when trying delete product's.");
				return false;
			}
			
			if(isAllProduct())
			{
				MProduct[] p = getProductsByPriceList();
				for(int i=0;i<p.length;i++)
				{
					MUNSPLConvProduct cp = new MUNSPLConvProduct(getCtx(), 0, get_TrxName());
					cp.setUNS_PriceListConv_ID(get_ID());
					cp.setAD_Org_ID(getAD_Org_ID());
					cp.setM_Product_ID(p[i].get_ID());
					cp.saveEx();
				}
			}
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_PLConv_Product WHERE UNS_PriceListConv_ID = ?";
		return DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		MPriceListVersion plv = MPriceListVersion.getSameValidFrom(
				getCtx(), getPriceListTo_ID(), getValidFrom(), get_TrxName());
		MUNSPLConvProduct[] p = getProducts();
		for(int i=0;i<p.length;i++)
		{
			MProductPrice ppFrom = MProductPrice.getByPriceList(getCtx(), getPriceListFrom_ID(), 
					p[i].getM_Product_ID(), getValidFrom(), get_TrxName());
			if(ppFrom == null)
			{
				if(isIgnoreValidation())
					continue;
				else
				{
					m_processMsg = "Product is not on price list. " + p[i].getM_Product().getName();
					return DocAction.STATUS_Invalid;
				}
			}
			
			MPriceListVersion versionFrom = new MPriceListVersion(getCtx(), ppFrom.getM_PriceList_Version_ID(), get_TrxName());
			if(plv == null)
			{
				plv = new MPriceListVersion((MPriceList) getPriceListTo());
				plv.setValidFrom(getValidFrom());
				if(!plv.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Failed create new version price.");
					return DocAction.STATUS_Invalid;
				}
			}
			
			MProductPrice pp = MProductPrice.get(getCtx(), plv.get_ID(), p[i].getM_Product_ID(), get_TrxName());
			BigDecimal priceList = getPriceList(versionFrom, p[i].getM_Product_ID());
			BigDecimal priceLimit = getPriceLimit(versionFrom, p[i].getM_Product_ID());
			BigDecimal priceStd = getPriceStd(versionFrom, p[i].getM_Product_ID());
			if(pp == null)
			{				
				pp = new MProductPrice(plv, p[i].getM_Product_ID(), priceList, priceStd, priceLimit);
			}
			else
			{
				pp.setPriceLimit(priceLimit);
				pp.setPriceList(priceList);
				pp.setPriceStd(priceStd);
			}
			
			pp.saveEx();
			
			p[i].setPriceLimit(priceLimit);
			p[i].setPriceList(priceList);
			p[i].setPriceStd(priceStd);
			p[i].setPriceListSource(ppFrom.getPriceList());
			p[i].saveEx();
		}
		if(plv == null && isIgnoreValidation())
		{
			int retVal = MessageBox.showMsg(this, getCtx(), "Selected product is not on price list. Continue?",
					"Confirmation action", MessageBox.YESNO, MessageBox.ICONWARNING);
			if(retVal == MessageBox.RETURN_NO || MessageBox.RETURN_CANCEL == retVal ||
					retVal == MessageBox.RETURN_NONE)
			{
				m_processMsg = "Action has cancelled.";
				return DocAction.STATUS_Invalid;
			}
		}
		else
			setPL_VersionTo_ID(plv.get_ID());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public MUNSPLConvProduct[] getProducts()
	{
		List<MUNSPLConvProduct> list = Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(),
				MUNSPLConvProduct.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSPLConvProduct[list.size()]);
				
//		String whereClause = "M_Product.M_Product_ID IN (SELECT p.M_Product_ID FROM UNS_PLConv_Product p"
//				+ " WHERE p.M_Product_ID > 0 AND p.UNS_PriceListConv_ID = ?)"
//				+ " OR M_Product.M_Product_Category_ID IN (SELECT p.M_Product_Category_ID FROM UNS_PLConv_Product p"
//				+ " WHERE p.M_Product_Category_ID > 0 AND p.UNS_PriceListConv_ID = ?)";
//		int[] IDs = Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(),
//				MProduct.Table_Name, whereClause, get_TrxName()).setParameters(get_ID(), get_ID()).getIDs();
//		
//		return IDs;
	}
	
	private BigDecimal getPriceStd(MPriceListVersion plv, int M_Product_ID)
	{
		BigDecimal priceStd = Env.ZERO;
		
		priceStd = getProductPrice(plv, M_Product_ID).getPriceStd().multiply(getRate());
		
		return priceStd;
	}
	
	private BigDecimal getPriceLimit(MPriceListVersion plv, int M_Product_ID)
	{
		BigDecimal priceLimit = Env.ZERO;
		
		priceLimit = getProductPrice(plv, M_Product_ID).getPriceLimit().multiply(getRate());
		
		return priceLimit;
	}
	
	private BigDecimal getPriceList(MPriceListVersion plv, int M_Product_ID)
	{
		BigDecimal priceList = Env.ZERO;

		priceList = getProductPrice(plv, M_Product_ID).getPriceList().multiply(getRate());
		
		return priceList;
	}
	
	private MProductPrice pp = null;
	
	private MProductPrice getProductPrice(MPriceListVersion plv, int M_Product_ID)
	{
		pp = MProductPrice.get(getCtx(), plv.get_ID(), M_Product_ID, get_TrxName());
		
		return pp;
	}
	
	private MProduct[] getProductsByPriceList()
	{
		List<MProduct> list = new ArrayList<>();
		String sql = "SELECt DISTINCT(pp.M_Product_ID) FROM M_ProductPrice pp"
				+ " INNER JOIN M_PriceList_Version plv ON plv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID"
				+ " WHERE plv.M_PriceList_ID = ? AND pp.IsActive = 'Y'";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, getPriceListFrom_ID());
			rs = stmt.executeQuery();
			while (rs.next())
			{
				list.add(MProduct.get(getCtx(), rs.getInt(1)));
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		return list.toArray(new MProduct[list.size()]);
	}
}