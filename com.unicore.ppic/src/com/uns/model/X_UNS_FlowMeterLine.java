/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_FlowMeterLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_FlowMeterLine extends PO implements I_UNS_FlowMeterLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171012L;

    /** Standard Constructor */
    public X_UNS_FlowMeterLine (Properties ctx, int UNS_FlowMeterLine_ID, String trxName)
    {
      super (ctx, UNS_FlowMeterLine_ID, trxName);
      /** if (UNS_FlowMeterLine_ID == 0)
        {
			setUNS_FlowMeter_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_FlowMeterLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_FlowMeterLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Duration.
		@param Duration 
		Normal Duration in Duration Unit
	  */
	public void setDuration (Timestamp Duration)
	{
		set_Value (COLUMNNAME_Duration, Duration);
	}

	/** Get Duration.
		@return Normal Duration in Duration Unit
	  */
	public Timestamp getDuration () 
	{
		return (Timestamp)get_Value(COLUMNNAME_Duration);
	}

	/** Set FlowMeterAverageKG.
		@param FlowMeterAverageKG FlowMeterAverageKG	  */
	public void setFlowMeterAverageKG (BigDecimal FlowMeterAverageKG)
	{
		set_Value (COLUMNNAME_FlowMeterAverageKG, FlowMeterAverageKG);
	}

	/** Get FlowMeterAverageKG.
		@return FlowMeterAverageKG	  */
	public BigDecimal getFlowMeterAverageKG () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FlowMeterAverageKG);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FlowMeterLimit.
		@param FlowMeterLimit FlowMeterLimit	  */
	public void setFlowMeterLimit (String FlowMeterLimit)
	{
		set_Value (COLUMNNAME_FlowMeterLimit, FlowMeterLimit);
	}

	/** Get FlowMeterLimit.
		@return FlowMeterLimit	  */
	public String getFlowMeterLimit () 
	{
		return (String)get_Value(COLUMNNAME_FlowMeterLimit);
	}

	/** Set FlowMeterMaxDate.
		@param FlowMeterMaxDate FlowMeterMaxDate	  */
	public void setFlowMeterMaxDate (Timestamp FlowMeterMaxDate)
	{
		set_Value (COLUMNNAME_FlowMeterMaxDate, FlowMeterMaxDate);
	}

	/** Get FlowMeterMaxDate.
		@return FlowMeterMaxDate	  */
	public Timestamp getFlowMeterMaxDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_FlowMeterMaxDate);
	}

	/** Set FlowMeterMaxKG.
		@param FlowMeterMaxKG FlowMeterMaxKG	  */
	public void setFlowMeterMaxKG (BigDecimal FlowMeterMaxKG)
	{
		set_Value (COLUMNNAME_FlowMeterMaxKG, FlowMeterMaxKG);
	}

	/** Get FlowMeterMaxKG.
		@return FlowMeterMaxKG	  */
	public BigDecimal getFlowMeterMaxKG () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FlowMeterMaxKG);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FlowMeterMaxTimestamp.
		@param FlowMeterMaxTimestamp FlowMeterMaxTimestamp	  */
	public void setFlowMeterMaxTimestamp (Timestamp FlowMeterMaxTimestamp)
	{
		set_Value (COLUMNNAME_FlowMeterMaxTimestamp, FlowMeterMaxTimestamp);
	}

	/** Get FlowMeterMaxTimestamp.
		@return FlowMeterMaxTimestamp	  */
	public Timestamp getFlowMeterMaxTimestamp () 
	{
		return (Timestamp)get_Value(COLUMNNAME_FlowMeterMaxTimestamp);
	}

	/** Set FlowMeterMinDate.
		@param FlowMeterMinDate FlowMeterMinDate	  */
	public void setFlowMeterMinDate (Timestamp FlowMeterMinDate)
	{
		set_Value (COLUMNNAME_FlowMeterMinDate, FlowMeterMinDate);
	}

	/** Get FlowMeterMinDate.
		@return FlowMeterMinDate	  */
	public Timestamp getFlowMeterMinDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_FlowMeterMinDate);
	}

	/** Set FlowMeterMinKG.
		@param FlowMeterMinKG FlowMeterMinKG	  */
	public void setFlowMeterMinKG (BigDecimal FlowMeterMinKG)
	{
		set_Value (COLUMNNAME_FlowMeterMinKG, FlowMeterMinKG);
	}

	/** Get FlowMeterMinKG.
		@return FlowMeterMinKG	  */
	public BigDecimal getFlowMeterMinKG () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FlowMeterMinKG);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FlowMeterMinTimestamp.
		@param FlowMeterMinTimestamp FlowMeterMinTimestamp	  */
	public void setFlowMeterMinTimestamp (Timestamp FlowMeterMinTimestamp)
	{
		set_Value (COLUMNNAME_FlowMeterMinTimestamp, FlowMeterMinTimestamp);
	}

	/** Get FlowMeterMinTimestamp.
		@return FlowMeterMinTimestamp	  */
	public Timestamp getFlowMeterMinTimestamp () 
	{
		return (Timestamp)get_Value(COLUMNNAME_FlowMeterMinTimestamp);
	}

	/** Set FlowMeterStatus.
		@param FlowMeterStatus FlowMeterStatus	  */
	public void setFlowMeterStatus (String FlowMeterStatus)
	{
		set_Value (COLUMNNAME_FlowMeterStatus, FlowMeterStatus);
	}

	/** Get FlowMeterStatus.
		@return FlowMeterStatus	  */
	public String getFlowMeterStatus () 
	{
		return (String)get_Value(COLUMNNAME_FlowMeterStatus);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Start Date.
		@param StartDate 
		First effective day (inclusive)
	  */
	public void setStartDate (Timestamp StartDate)
	{
		set_Value (COLUMNNAME_StartDate, StartDate);
	}

	/** Get Start Date.
		@return First effective day (inclusive)
	  */
	public Timestamp getStartDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartDate);
	}

	/** Set Start Time.
		@param StartTime 
		Time started
	  */
	public void setStartTime (Timestamp StartTime)
	{
		set_Value (COLUMNNAME_StartTime, StartTime);
	}

	/** Get Start Time.
		@return Time started
	  */
	public Timestamp getStartTime () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartTime);
	}

	/** Set StopDate.
		@param StopDate StopDate	  */
	public void setStopDate (Timestamp StopDate)
	{
		set_Value (COLUMNNAME_StopDate, StopDate);
	}

	/** Get StopDate.
		@return StopDate	  */
	public Timestamp getStopDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StopDate);
	}

	/** Set StopTime.
		@param StopTime StopTime	  */
	public void setStopTime (Timestamp StopTime)
	{
		set_Value (COLUMNNAME_StopTime, StopTime);
	}

	/** Get StopTime.
		@return StopTime	  */
	public Timestamp getStopTime () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StopTime);
	}

	/** Set TotalCounter.
		@param TotalCounter TotalCounter	  */
	public void setTotalCounter (BigDecimal TotalCounter)
	{
		set_Value (COLUMNNAME_TotalCounter, TotalCounter);
	}

	/** Get TotalCounter.
		@return TotalCounter	  */
	public BigDecimal getTotalCounter () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalCounter);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TotalLimit.
		@param TotalLimit TotalLimit	  */
	public void setTotalLimit (String TotalLimit)
	{
		set_Value (COLUMNNAME_TotalLimit, TotalLimit);
	}

	/** Get TotalLimit.
		@return TotalLimit	  */
	public String getTotalLimit () 
	{
		return (String)get_Value(COLUMNNAME_TotalLimit);
	}

	/** Set TotalStatus.
		@param TotalStatus TotalStatus	  */
	public void setTotalStatus (String TotalStatus)
	{
		set_Value (COLUMNNAME_TotalStatus, TotalStatus);
	}

	/** Get TotalStatus.
		@return TotalStatus	  */
	public String getTotalStatus () 
	{
		return (String)get_Value(COLUMNNAME_TotalStatus);
	}

	public com.uns.model.I_UNS_FlowMeter getUNS_FlowMeter() throws RuntimeException
    {
		return (com.uns.model.I_UNS_FlowMeter)MTable.get(getCtx(), com.uns.model.I_UNS_FlowMeter.Table_Name)
			.getPO(getUNS_FlowMeter_ID(), get_TrxName());	}

	/** Set UNS_FlowMeter_ID.
		@param UNS_FlowMeter_ID UNS_FlowMeter_ID	  */
	public void setUNS_FlowMeter_ID (int UNS_FlowMeter_ID)
	{
		if (UNS_FlowMeter_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_FlowMeter_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_FlowMeter_ID, Integer.valueOf(UNS_FlowMeter_ID));
	}

	/** Get UNS_FlowMeter_ID.
		@return UNS_FlowMeter_ID	  */
	public int getUNS_FlowMeter_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_FlowMeter_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_FlowMeterLine_ID.
		@param UNS_FlowMeterLine_ID UNS_FlowMeterLine_ID	  */
	public void setUNS_FlowMeterLine_ID (int UNS_FlowMeterLine_ID)
	{
		if (UNS_FlowMeterLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_FlowMeterLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_FlowMeterLine_ID, Integer.valueOf(UNS_FlowMeterLine_ID));
	}

	/** Get UNS_FlowMeterLine_ID.
		@return UNS_FlowMeterLine_ID	  */
	public int getUNS_FlowMeterLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_FlowMeterLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_FlowMeterLine_UU.
		@param UNS_FlowMeterLine_UU UNS_FlowMeterLine_UU	  */
	public void setUNS_FlowMeterLine_UU (String UNS_FlowMeterLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_FlowMeterLine_UU, UNS_FlowMeterLine_UU);
	}

	/** Get UNS_FlowMeterLine_UU.
		@return UNS_FlowMeterLine_UU	  */
	public String getUNS_FlowMeterLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_FlowMeterLine_UU);
	}
}