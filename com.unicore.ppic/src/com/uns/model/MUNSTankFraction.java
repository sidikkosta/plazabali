package com.uns.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

public class MUNSTankFraction extends X_UNS_TankFraction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7620585930281535198L;


	public MUNSTankFraction(Properties ctx, int UNS_TankFraction_ID,
			String trxName) {
		super(ctx, UNS_TankFraction_ID, trxName);
		
	}

	public MUNSTankFraction(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	
	public static BigDecimal getFraction(int UNS_Tank_ID, BigDecimal grossheight, String trxName){
		BigDecimal fraction = null;
		BigDecimal firstfract = null;
		BigDecimal secondfract = new BigDecimal(0);
		
		BigDecimal first = grossheight.setScale(0, RoundingMode.DOWN);
		
		BigDecimal second = grossheight.subtract(first);
		String sql = "SELECT Volume FROM UNS_TankFraction WHERE HeightValue = ? AND UNS_Tank_ID = ?";
		firstfract = DB.getSQLValueBDEx(trxName, sql, first, UNS_Tank_ID);
		
		if(second.compareTo(Env.ZERO) == 1){
			int Ring_ID = MUNSTankRing.getRingID(grossheight, UNS_Tank_ID, trxName);
			String sql2 = "SELECT Volume FROM UNS_TankFraction WHERE UNS_TankRing_ID = ?"
					+ "AND HeightValue = ?";
			secondfract = DB.getSQLValueBDEx(trxName, sql2, Ring_ID, second);
		}
			
		
		fraction = firstfract.add(secondfract);
		return fraction;
	}
}
