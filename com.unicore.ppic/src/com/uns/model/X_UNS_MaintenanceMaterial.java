/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_MaintenanceMaterial
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_MaintenanceMaterial extends PO implements I_UNS_MaintenanceMaterial, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180117L;

    /** Standard Constructor */
    public X_UNS_MaintenanceMaterial (Properties ctx, int UNS_MaintenanceMaterial_ID, String trxName)
    {
      super (ctx, UNS_MaintenanceMaterial_ID, trxName);
      /** if (UNS_MaintenanceMaterial_ID == 0)
        {
			setC_UOM_ID (0);
			setUNS_MaintenanceDetail_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_MaintenanceMaterial (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_MaintenanceMaterial[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Product.
		@param Product Product	  */
	public void setProduct (String Product)
	{
		set_Value (COLUMNNAME_Product, Product);
	}

	/** Get Product.
		@return Product	  */
	public String getProduct () 
	{
		return (String)get_Value(COLUMNNAME_Product);
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_MaintenanceDetail getUNS_MaintenanceDetail() throws RuntimeException
    {
		return (com.uns.model.I_UNS_MaintenanceDetail)MTable.get(getCtx(), com.uns.model.I_UNS_MaintenanceDetail.Table_Name)
			.getPO(getUNS_MaintenanceDetail_ID(), get_TrxName());	}

	/** Set UNS_MaintenanceDetail.
		@param UNS_MaintenanceDetail_ID UNS_MaintenanceDetail	  */
	public void setUNS_MaintenanceDetail_ID (int UNS_MaintenanceDetail_ID)
	{
		if (UNS_MaintenanceDetail_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceDetail_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceDetail_ID, Integer.valueOf(UNS_MaintenanceDetail_ID));
	}

	/** Get UNS_MaintenanceDetail.
		@return UNS_MaintenanceDetail	  */
	public int getUNS_MaintenanceDetail_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_MaintenanceDetail_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Maintenance Material.
		@param UNS_MaintenanceMaterial_ID Maintenance Material	  */
	public void setUNS_MaintenanceMaterial_ID (int UNS_MaintenanceMaterial_ID)
	{
		if (UNS_MaintenanceMaterial_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceMaterial_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceMaterial_ID, Integer.valueOf(UNS_MaintenanceMaterial_ID));
	}

	/** Get Maintenance Material.
		@return Maintenance Material	  */
	public int getUNS_MaintenanceMaterial_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_MaintenanceMaterial_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_MaintenanceMaterial_UU.
		@param UNS_MaintenanceMaterial_UU UNS_MaintenanceMaterial_UU	  */
	public void setUNS_MaintenanceMaterial_UU (String UNS_MaintenanceMaterial_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceMaterial_UU, UNS_MaintenanceMaterial_UU);
	}

	/** Get UNS_MaintenanceMaterial_UU.
		@return UNS_MaintenanceMaterial_UU	  */
	public String getUNS_MaintenanceMaterial_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_MaintenanceMaterial_UU);
	}
}