package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

public class MUNSFlowMeter extends X_UNS_FlowMeter implements I_UNS_FlowMeter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5159393155511471361L;

	public MUNSFlowMeter(Properties ctx, int UNS_FlowMeter_ID, String trxName) {
		super(ctx, UNS_FlowMeter_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSFlowMeter(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSFlowMeter get(Properties ctx, String Name, String trxName) {
		
		String sql = "SELECT UNS_FlowMeter_ID FROM UNS_FlowMeter WHERE Name = ? ";
		int id = DB.getSQLValue(trxName, sql, Name);
		if(id < 0)
			return null;
		
		MUNSFlowMeter flowmeter = new MUNSFlowMeter(ctx, id, trxName);
		
		return flowmeter;
	}
}
