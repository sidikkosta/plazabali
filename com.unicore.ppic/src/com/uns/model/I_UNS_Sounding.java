/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Sounding
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Sounding 
{

    /** TableName=UNS_Sounding */
    public static final String Table_Name = "UNS_Sounding";

    /** AD_Table_ID=1000326 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name Acc_Fibre */
    public static final String COLUMNNAME_Acc_Fibre = "Acc_Fibre";

	/** Set Acces Product Fibre.
	  * Acces Product Fibre/Empty Bunch
	  */
	public void setAcc_Fibre (BigDecimal Acc_Fibre);

	/** Get Acces Product Fibre.
	  * Acces Product Fibre/Empty Bunch
	  */
	public BigDecimal getAcc_Fibre();

    /** Column name Acc_Shell */
    public static final String COLUMNNAME_Acc_Shell = "Acc_Shell";

	/** Set Acces Product Shell	  */
	public void setAcc_Shell (BigDecimal Acc_Shell);

	/** Get Acces Product Shell	  */
	public BigDecimal getAcc_Shell();

    /** Column name Acc_SludgeOil */
    public static final String COLUMNNAME_Acc_SludgeOil = "Acc_SludgeOil";

	/** Set Acces Product Sludge Oil	  */
	public void setAcc_SludgeOil (BigDecimal Acc_SludgeOil);

	/** Get Acces Product Sludge Oil	  */
	public BigDecimal getAcc_SludgeOil();

    /** Column name Acc_SolidDecanter */
    public static final String COLUMNNAME_Acc_SolidDecanter = "Acc_SolidDecanter";

	/** Set Acces Product Solid Decanter	  */
	public void setAcc_SolidDecanter (BigDecimal Acc_SolidDecanter);

	/** Get Acces Product Solid Decanter	  */
	public BigDecimal getAcc_SolidDecanter();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BulkingSilo */
    public static final String COLUMNNAME_BulkingSilo = "BulkingSilo";

	/** Set Bulking Silo (kg)	  */
	public void setBulkingSilo (BigDecimal BulkingSilo);

	/** Get Bulking Silo (kg)	  */
	public BigDecimal getBulkingSilo();

    /** Column name CaCO3Used_Shift1 */
    public static final String COLUMNNAME_CaCO3Used_Shift1 = "CaCO3Used_Shift1";

	/** Set CaCO3 Used (Shift1)	  */
	public void setCaCO3Used_Shift1 (BigDecimal CaCO3Used_Shift1);

	/** Get CaCO3 Used (Shift1)	  */
	public BigDecimal getCaCO3Used_Shift1();

    /** Column name CaCO3Used_Shift2 */
    public static final String COLUMNNAME_CaCO3Used_Shift2 = "CaCO3Used_Shift2";

	/** Set CaCO3 Used (Shift2)	  */
	public void setCaCO3Used_Shift2 (BigDecimal CaCO3Used_Shift2);

	/** Get CaCO3 Used (Shift2)	  */
	public BigDecimal getCaCO3Used_Shift2();

    /** Column name CangkangBoiler */
    public static final String COLUMNNAME_CangkangBoiler = "CangkangBoiler";

	/** Set Cangkang Boiler (kg)	  */
	public void setCangkangBoiler (BigDecimal CangkangBoiler);

	/** Get Cangkang Boiler (kg)	  */
	public BigDecimal getCangkangBoiler();

    /** Column name CangkangInHopper */
    public static final String COLUMNNAME_CangkangInHopper = "CangkangInHopper";

	/** Set Cangkang In Hopper (kg)	  */
	public void setCangkangInHopper (BigDecimal CangkangInHopper);

	/** Get Cangkang In Hopper (kg)	  */
	public BigDecimal getCangkangInHopper();

    /** Column name CPOCurrentSounding */
    public static final String COLUMNNAME_CPOCurrentSounding = "CPOCurrentSounding";

	/** Set CPO Current Sounding Qty	  */
	public void setCPOCurrentSounding (BigDecimal CPOCurrentSounding);

	/** Get CPO Current Sounding Qty	  */
	public BigDecimal getCPOCurrentSounding();

    /** Column name CPOProductionQty */
    public static final String COLUMNNAME_CPOProductionQty = "CPOProductionQty";

	/** Set CPO Production Qty	  */
	public void setCPOProductionQty (BigDecimal CPOProductionQty);

	/** Get CPO Production Qty	  */
	public BigDecimal getCPOProductionQty();

    /** Column name CPOReturnQty */
    public static final String COLUMNNAME_CPOReturnQty = "CPOReturnQty";

	/** Set CPO Return Qty	  */
	public void setCPOReturnQty (BigDecimal CPOReturnQty);

	/** Get CPO Return Qty	  */
	public BigDecimal getCPOReturnQty();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateAcct */
    public static final String COLUMNNAME_DateAcct = "DateAcct";

	/** Set Account Date.
	  * Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct);

	/** Get Account Date.
	  * Accounting Date
	  */
	public Timestamp getDateAcct();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DeliveredFibreQty */
    public static final String COLUMNNAME_DeliveredFibreQty = "DeliveredFibreQty";

	/** Set Fibre Delivered Qty.
	  * Delivered Product of Fibre / Empty Bunch
	  */
	public void setDeliveredFibreQty (BigDecimal DeliveredFibreQty);

	/** Get Fibre Delivered Qty.
	  * Delivered Product of Fibre / Empty Bunch
	  */
	public BigDecimal getDeliveredFibreQty();

    /** Column name DeliveredQty */
    public static final String COLUMNNAME_DeliveredQty = "DeliveredQty";

	/** Set CPO Delivered Quantity.
	  * Quantity of Delivered
	  */
	public void setDeliveredQty (BigDecimal DeliveredQty);

	/** Get CPO Delivered Quantity.
	  * Quantity of Delivered
	  */
	public BigDecimal getDeliveredQty();

    /** Column name DeliveredShellQty */
    public static final String COLUMNNAME_DeliveredShellQty = "DeliveredShellQty";

	/** Set Shell Delivered Qty	  */
	public void setDeliveredShellQty (BigDecimal DeliveredShellQty);

	/** Get Shell Delivered Qty	  */
	public BigDecimal getDeliveredShellQty();

    /** Column name DeliveredSludgeOilQty */
    public static final String COLUMNNAME_DeliveredSludgeOilQty = "DeliveredSludgeOilQty";

	/** Set Sludge Oil Delivered Qty.
	  * Delivered Product of Sludge Oil
	  */
	public void setDeliveredSludgeOilQty (BigDecimal DeliveredSludgeOilQty);

	/** Get Sludge Oil Delivered Qty.
	  * Delivered Product of Sludge Oil
	  */
	public BigDecimal getDeliveredSludgeOilQty();

    /** Column name DeliveredSolidQty */
    public static final String COLUMNNAME_DeliveredSolidQty = "DeliveredSolidQty";

	/** Set Solid Decanter Delivered Qty.
	  * Delivered Product of Solid Decanter
	  */
	public void setDeliveredSolidQty (BigDecimal DeliveredSolidQty);

	/** Get Solid Decanter Delivered Qty.
	  * Delivered Product of Solid Decanter
	  */
	public BigDecimal getDeliveredSolidQty();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DirtKernel */
    public static final String COLUMNNAME_DirtKernel = "DirtKernel";

	/** Set Dirt Kernel.
	  * How Much Kernel with Dirt Quality
	  */
	public void setDirtKernel (BigDecimal DirtKernel);

	/** Get Dirt Kernel.
	  * How Much Kernel with Dirt Quality
	  */
	public BigDecimal getDirtKernel();

    /** Column name DirtKernelOnBunker */
    public static final String COLUMNNAME_DirtKernelOnBunker = "DirtKernelOnBunker";

	/** Set Dirt Kernel On Bunker.
	  * How Much Kernel On Bunker with Dirt Quality
	  */
	public void setDirtKernelOnBunker (BigDecimal DirtKernelOnBunker);

	/** Get Dirt Kernel On Bunker.
	  * How Much Kernel On Bunker with Dirt Quality
	  */
	public BigDecimal getDirtKernelOnBunker();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EndTime */
    public static final String COLUMNNAME_EndTime = "EndTime";

	/** Set End Time.
	  * End of the time span
	  */
	public void setEndTime (Timestamp EndTime);

	/** Get End Time.
	  * End of the time span
	  */
	public Timestamp getEndTime();

    /** Column name FFBProcessedNettoI */
    public static final String COLUMNNAME_FFBProcessedNettoI = "FFBProcessedNettoI";

	/** Set FFB Processed Netto-I (kg)	  */
	public void setFFBProcessedNettoI (BigDecimal FFBProcessedNettoI);

	/** Get FFB Processed Netto-I (kg)	  */
	public BigDecimal getFFBProcessedNettoI();

    /** Column name FFBProcessedNettoII */
    public static final String COLUMNNAME_FFBProcessedNettoII = "FFBProcessedNettoII";

	/** Set FFB Processed Netto-II (kg).
	  * Quantity of FFB Processed Netto-II
	  */
	public void setFFBProcessedNettoII (BigDecimal FFBProcessedNettoII);

	/** Get FFB Processed Netto-II (kg).
	  * Quantity of FFB Processed Netto-II
	  */
	public BigDecimal getFFBProcessedNettoII();

    /** Column name FFBReceiptNettoI */
    public static final String COLUMNNAME_FFBReceiptNettoI = "FFBReceiptNettoI";

	/** Set FFB Receipt Netto-I(kg)	  */
	public void setFFBReceiptNettoI (BigDecimal FFBReceiptNettoI);

	/** Get FFB Receipt Netto-I(kg)	  */
	public BigDecimal getFFBReceiptNettoI();

    /** Column name FFBReceiptNettoII */
    public static final String COLUMNNAME_FFBReceiptNettoII = "FFBReceiptNettoII";

	/** Set FFB Receipt Netto-II (kg).
	  * Quantity of FFB Receipt Netto-I 
	  */
	public void setFFBReceiptNettoII (BigDecimal FFBReceiptNettoII);

	/** Get FFB Receipt Netto-II (kg).
	  * Quantity of FFB Receipt Netto-I 
	  */
	public BigDecimal getFFBReceiptNettoII();

    /** Column name FFBRemainingQty */
    public static final String COLUMNNAME_FFBRemainingQty = "FFBRemainingQty";

	/** Set FFB Remaining Netto-I (kg)	  */
	public void setFFBRemainingQty (BigDecimal FFBRemainingQty);

	/** Get FFB Remaining Netto-I (kg)	  */
	public BigDecimal getFFBRemainingQty();

    /** Column name FFBRemainingQtyII */
    public static final String COLUMNNAME_FFBRemainingQtyII = "FFBRemainingQtyII";

	/** Set FFB Remaining Netto-II (kg)	  */
	public void setFFBRemainingQtyII (BigDecimal FFBRemainingQtyII);

	/** Get FFB Remaining Netto-II (kg)	  */
	public BigDecimal getFFBRemainingQtyII();

    /** Column name FlowMeterQty */
    public static final String COLUMNNAME_FlowMeterQty = "FlowMeterQty";

	/** Set Flow Meter Qty.
	  * Qty Production from Flow Meter
	  */
	public void setFlowMeterQty (BigDecimal FlowMeterQty);

	/** Get Flow Meter Qty.
	  * Qty Production from Flow Meter
	  */
	public BigDecimal getFlowMeterQty();

    /** Column name FuelUsed_Shift1 */
    public static final String COLUMNNAME_FuelUsed_Shift1 = "FuelUsed_Shift1";

	/** Set Fuel Used (Shift1)	  */
	public void setFuelUsed_Shift1 (BigDecimal FuelUsed_Shift1);

	/** Get Fuel Used (Shift1)	  */
	public BigDecimal getFuelUsed_Shift1();

    /** Column name FuelUsed_Shift2 */
    public static final String COLUMNNAME_FuelUsed_Shift2 = "FuelUsed_Shift2";

	/** Set Fuel Used (Shift2)	  */
	public void setFuelUsed_Shift2 (BigDecimal FuelUsed_Shift2);

	/** Get Fuel Used (Shift2)	  */
	public BigDecimal getFuelUsed_Shift2();

    /** Column name GetFlowMeterQty */
    public static final String COLUMNNAME_GetFlowMeterQty = "GetFlowMeterQty";

	/** Set Get Flow Meter Qty.
	  * Get Qty Production From Flow Meter
	  */
	public void setGetFlowMeterQty (String GetFlowMeterQty);

	/** Get Get Flow Meter Qty.
	  * Get Qty Production From Flow Meter
	  */
	public String getGetFlowMeterQty();

    /** Column name HeightBulkingSilo */
    public static final String COLUMNNAME_HeightBulkingSilo = "HeightBulkingSilo";

	/** Set Height Bulking Silo (cm).
	  * Height Measured Bulking in Silo
	  */
	public void setHeightBulkingSilo (BigDecimal HeightBulkingSilo);

	/** Get Height Bulking Silo (cm).
	  * Height Measured Bulking in Silo
	  */
	public BigDecimal getHeightBulkingSilo();

    /** Column name HeightKernelSilo1 */
    public static final String COLUMNNAME_HeightKernelSilo1 = "HeightKernelSilo1";

	/** Set Height Kernel Silo 1 (cm).
	  * Height Measured Kernel in Silo 1
	  */
	public void setHeightKernelSilo1 (BigDecimal HeightKernelSilo1);

	/** Get Height Kernel Silo 1 (cm).
	  * Height Measured Kernel in Silo 1
	  */
	public BigDecimal getHeightKernelSilo1();

    /** Column name HeightKernelSilo2 */
    public static final String COLUMNNAME_HeightKernelSilo2 = "HeightKernelSilo2";

	/** Set Height Kernel Silo 2 (cm).
	  * Height Measured Kernel in Silo 2
	  */
	public void setHeightKernelSilo2 (BigDecimal HeightKernelSilo2);

	/** Get Height Kernel Silo 2 (cm).
	  * Height Measured Kernel in Silo 2
	  */
	public BigDecimal getHeightKernelSilo2();

    /** Column name HeightKernelSilo3 */
    public static final String COLUMNNAME_HeightKernelSilo3 = "HeightKernelSilo3";

	/** Set Height Kernel Silo 3 (cm).
	  * Height Measured Kernel in Silo 3
	  */
	public void setHeightKernelSilo3 (BigDecimal HeightKernelSilo3);

	/** Get Height Kernel Silo 3 (cm).
	  * Height Measured Kernel in Silo 3
	  */
	public BigDecimal getHeightKernelSilo3();

    /** Column name HeightNutSilo */
    public static final String COLUMNNAME_HeightNutSilo = "HeightNutSilo";

	/** Set Height Nut Silo (cm).
	  * Height Measured Nut in Silo
	  */
	public void setHeightNutSilo (BigDecimal HeightNutSilo);

	/** Get Height Nut Silo (cm).
	  * Height Measured Nut in Silo
	  */
	public BigDecimal getHeightNutSilo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name isManualSounding */
    public static final String COLUMNNAME_isManualSounding = "isManualSounding";

	/** Set Manual Sounding	  */
	public void setisManualSounding (boolean isManualSounding);

	/** Get Manual Sounding	  */
	public boolean isManualSounding();

    /** Column name KernelDeliveredQty */
    public static final String COLUMNNAME_KernelDeliveredQty = "KernelDeliveredQty";

	/** Set Kernel Delivered Qty (kg)	  */
	public void setKernelDeliveredQty (BigDecimal KernelDeliveredQty);

	/** Get Kernel Delivered Qty (kg)	  */
	public BigDecimal getKernelDeliveredQty();

    /** Column name KernelFloor */
    public static final String COLUMNNAME_KernelFloor = "KernelFloor";

	/** Set Good Kernel on Floor (kg)	  */
	public void setKernelFloor (BigDecimal KernelFloor);

	/** Get Good Kernel on Floor (kg)	  */
	public BigDecimal getKernelFloor();

    /** Column name KernelFloorBad */
    public static final String COLUMNNAME_KernelFloorBad = "KernelFloorBad";

	/** Set Bad Kernel on Floor (kg)	  */
	public void setKernelFloorBad (BigDecimal KernelFloorBad);

	/** Get Bad Kernel on Floor (kg)	  */
	public BigDecimal getKernelFloorBad();

    /** Column name KernelReturnQty */
    public static final String COLUMNNAME_KernelReturnQty = "KernelReturnQty";

	/** Set Kernel Return Qty (kg)	  */
	public void setKernelReturnQty (BigDecimal KernelReturnQty);

	/** Get Kernel Return Qty (kg)	  */
	public BigDecimal getKernelReturnQty();

    /** Column name KernelSilo1 */
    public static final String COLUMNNAME_KernelSilo1 = "KernelSilo1";

	/** Set Kernel Silo 1 (kg)	  */
	public void setKernelSilo1 (BigDecimal KernelSilo1);

	/** Get Kernel Silo 1 (kg)	  */
	public BigDecimal getKernelSilo1();

    /** Column name KernelSilo2 */
    public static final String COLUMNNAME_KernelSilo2 = "KernelSilo2";

	/** Set Kernel Silo 2 (kg)	  */
	public void setKernelSilo2 (BigDecimal KernelSilo2);

	/** Get Kernel Silo 2 (kg)	  */
	public BigDecimal getKernelSilo2();

    /** Column name KernelSilo3 */
    public static final String COLUMNNAME_KernelSilo3 = "KernelSilo3";

	/** Set Kernel Silo 3 (kg)	  */
	public void setKernelSilo3 (BigDecimal KernelSilo3);

	/** Get Kernel Silo 3 (kg)	  */
	public BigDecimal getKernelSilo3();

    /** Column name KERNettoI */
    public static final String COLUMNNAME_KERNettoI = "KERNettoI";

	/** Set KER Netto-I.
	  * Kernel Extraction Rate of Netto-I
	  */
	public void setKERNettoI (BigDecimal KERNettoI);

	/** Get KER Netto-I.
	  * Kernel Extraction Rate of Netto-I
	  */
	public BigDecimal getKERNettoI();

    /** Column name KERNettoII */
    public static final String COLUMNNAME_KERNettoII = "KERNettoII";

	/** Set KER Netto-II.
	  * Kernel Extraction Rate of Netto-II
	  */
	public void setKERNettoII (BigDecimal KERNettoII);

	/** Get KER Netto-II.
	  * Kernel Extraction Rate of Netto-II
	  */
	public BigDecimal getKERNettoII();

    /** Column name LastSoundingQty */
    public static final String COLUMNNAME_LastSoundingQty = "LastSoundingQty";

	/** Set Last Sounding Quantity	  */
	public void setLastSoundingQty (BigDecimal LastSoundingQty);

	/** Get Last Sounding Quantity	  */
	public BigDecimal getLastSoundingQty();

    /** Column name M_Inventory_ID */
    public static final String COLUMNNAME_M_Inventory_ID = "M_Inventory_ID";

	/** Set Phys.Inventory.
	  * Parameters for a Physical Inventory
	  */
	public void setM_Inventory_ID (int M_Inventory_ID);

	/** Get Phys.Inventory.
	  * Parameters for a Physical Inventory
	  */
	public int getM_Inventory_ID();

	public org.compiere.model.I_M_Inventory getM_Inventory() throws RuntimeException;

    /** Column name M_Movement_ID */
    public static final String COLUMNNAME_M_Movement_ID = "M_Movement_ID";

	/** Set Inventory Move.
	  * Movement of Inventory
	  */
	public void setM_Movement_ID (int M_Movement_ID);

	/** Get Inventory Move.
	  * Movement of Inventory
	  */
	public int getM_Movement_ID();

	public org.compiere.model.I_M_Movement getM_Movement() throws RuntimeException;

    /** Column name MoistKernel */
    public static final String COLUMNNAME_MoistKernel = "MoistKernel";

	/** Set Moist Kernel.
	  * How Much Kernel with Moist Quality
	  */
	public void setMoistKernel (BigDecimal MoistKernel);

	/** Get Moist Kernel.
	  * How Much Kernel with Moist Quality
	  */
	public BigDecimal getMoistKernel();

    /** Column name MoistKernelOnBunker */
    public static final String COLUMNNAME_MoistKernelOnBunker = "MoistKernelOnBunker";

	/** Set Moist Kernel On Bunker.
	  * How Much Kernel On Bunker with Moist Quality
	  */
	public void setMoistKernelOnBunker (BigDecimal MoistKernelOnBunker);

	/** Get Moist Kernel On Bunker.
	  * How Much Kernel On Bunker with Moist Quality
	  */
	public BigDecimal getMoistKernelOnBunker();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name NutFloor */
    public static final String COLUMNNAME_NutFloor = "NutFloor";

	/** Set Nut on Floor (kg)	  */
	public void setNutFloor (BigDecimal NutFloor);

	/** Get Nut on Floor (kg)	  */
	public BigDecimal getNutFloor();

    /** Column name NutPercentage */
    public static final String COLUMNNAME_NutPercentage = "NutPercentage";

	/** Set Nut Percentage (%)	  */
	public void setNutPercentage (BigDecimal NutPercentage);

	/** Get Nut Percentage (%)	  */
	public BigDecimal getNutPercentage();

    /** Column name NutSilo */
    public static final String COLUMNNAME_NutSilo = "NutSilo";

	/** Set Nut Silo (kg)	  */
	public void setNutSilo (BigDecimal NutSilo);

	/** Get Nut Silo (kg)	  */
	public BigDecimal getNutSilo();

    /** Column name OERNettoI */
    public static final String COLUMNNAME_OERNettoI = "OERNettoI";

	/** Set OER Netto-I.
	  * Oil Ekstraction Rate of Netto-I
	  */
	public void setOERNettoI (BigDecimal OERNettoI);

	/** Get OER Netto-I.
	  * Oil Ekstraction Rate of Netto-I
	  */
	public BigDecimal getOERNettoI();

    /** Column name OERNettoII */
    public static final String COLUMNNAME_OERNettoII = "OERNettoII";

	/** Set OER Netto-II.
	  * Oil Extraction Rate Netto-II
	  */
	public void setOERNettoII (BigDecimal OERNettoII);

	/** Get OER Netto-II.
	  * Oil Extraction Rate Netto-II
	  */
	public BigDecimal getOERNettoII();

    /** Column name PreviousFFBQty */
    public static final String COLUMNNAME_PreviousFFBQty = "PreviousFFBQty";

	/** Set FFB Previous Netto-I (kg)	  */
	public void setPreviousFFBQty (BigDecimal PreviousFFBQty);

	/** Get FFB Previous Netto-I (kg)	  */
	public BigDecimal getPreviousFFBQty();

    /** Column name PreviousFFBQtyII */
    public static final String COLUMNNAME_PreviousFFBQtyII = "PreviousFFBQtyII";

	/** Set FFB Previous Netto-II (kg)	  */
	public void setPreviousFFBQtyII (BigDecimal PreviousFFBQtyII);

	/** Get FFB Previous Netto-II (kg)	  */
	public BigDecimal getPreviousFFBQtyII();

    /** Column name PreviousKernelQty */
    public static final String COLUMNNAME_PreviousKernelQty = "PreviousKernelQty";

	/** Set Kernel Previous Qty (kg)	  */
	public void setPreviousKernelQty (BigDecimal PreviousKernelQty);

	/** Get Kernel Previous Qty (kg)	  */
	public BigDecimal getPreviousKernelQty();

    /** Column name printProduction */
    public static final String COLUMNNAME_printProduction = "printProduction";

	/** Set Production Report	  */
	public void setprintProduction (String printProduction);

	/** Get Production Report	  */
	public String getprintProduction();

    /** Column name PrintSounding */
    public static final String COLUMNNAME_PrintSounding = "PrintSounding";

	/** Set Print Produksi Harian	  */
	public void setPrintSounding (String PrintSounding);

	/** Get Print Produksi Harian	  */
	public String getPrintSounding();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name ProductionCangkang */
    public static final String COLUMNNAME_ProductionCangkang = "ProductionCangkang";

	/** Set Production Cangkang (kg)	  */
	public void setProductionCangkang (BigDecimal ProductionCangkang);

	/** Get Production Cangkang (kg)	  */
	public BigDecimal getProductionCangkang();

    /** Column name ProductionKernel */
    public static final String COLUMNNAME_ProductionKernel = "ProductionKernel";

	/** Set Production Kernel (kg)	  */
	public void setProductionKernel (BigDecimal ProductionKernel);

	/** Get Production Kernel (kg)	  */
	public BigDecimal getProductionKernel();

    /** Column name Real_KernelSilo1 */
    public static final String COLUMNNAME_Real_KernelSilo1 = "Real_KernelSilo1";

	/** Set Real Kernel Silo 1 (kg)	  */
	public void setReal_KernelSilo1 (BigDecimal Real_KernelSilo1);

	/** Get Real Kernel Silo 1 (kg)	  */
	public BigDecimal getReal_KernelSilo1();

    /** Column name Real_KernelSilo2 */
    public static final String COLUMNNAME_Real_KernelSilo2 = "Real_KernelSilo2";

	/** Set Real Kernel Silo 2 (kg)	  */
	public void setReal_KernelSilo2 (BigDecimal Real_KernelSilo2);

	/** Get Real Kernel Silo 2 (kg)	  */
	public BigDecimal getReal_KernelSilo2();

    /** Column name Real_KernelSilo3 */
    public static final String COLUMNNAME_Real_KernelSilo3 = "Real_KernelSilo3";

	/** Set Real Kernel Silo 3 (kg)	  */
	public void setReal_KernelSilo3 (BigDecimal Real_KernelSilo3);

	/** Get Real Kernel Silo 3 (kg)	  */
	public BigDecimal getReal_KernelSilo3();

    /** Column name Real_KERNettoI */
    public static final String COLUMNNAME_Real_KERNettoI = "Real_KERNettoI";

	/** Set Real KER Netto-I.
	  * Real Kernel Extraction Rate of Netto-I
	  */
	public void setReal_KERNettoI (BigDecimal Real_KERNettoI);

	/** Get Real KER Netto-I.
	  * Real Kernel Extraction Rate of Netto-I
	  */
	public BigDecimal getReal_KERNettoI();

    /** Column name Real_KERNettoII */
    public static final String COLUMNNAME_Real_KERNettoII = "Real_KERNettoII";

	/** Set Real KER Netto-II.
	  * Real Kernel Extraction Rate of Netto-II
	  */
	public void setReal_KERNettoII (BigDecimal Real_KERNettoII);

	/** Get Real KER Netto-II.
	  * Real Kernel Extraction Rate of Netto-II
	  */
	public BigDecimal getReal_KERNettoII();

    /** Column name Real_NutFloor */
    public static final String COLUMNNAME_Real_NutFloor = "Real_NutFloor";

	/** Set Real Nut on Floor (kg)	  */
	public void setReal_NutFloor (BigDecimal Real_NutFloor);

	/** Get Real Nut on Floor (kg)	  */
	public BigDecimal getReal_NutFloor();

    /** Column name Real_NutSilo */
    public static final String COLUMNNAME_Real_NutSilo = "Real_NutSilo";

	/** Set Real Nut Silo (kg)	  */
	public void setReal_NutSilo (BigDecimal Real_NutSilo);

	/** Get Real Nut Silo (kg)	  */
	public BigDecimal getReal_NutSilo();

    /** Column name Real_PreviousKernelQty */
    public static final String COLUMNNAME_Real_PreviousKernelQty = "Real_PreviousKernelQty";

	/** Set Real Kernel Previous Qty (kg)	  */
	public void setReal_PreviousKernelQty (BigDecimal Real_PreviousKernelQty);

	/** Get Real Kernel Previous Qty (kg)	  */
	public BigDecimal getReal_PreviousKernelQty();

    /** Column name Real_ProductionKernel */
    public static final String COLUMNNAME_Real_ProductionKernel = "Real_ProductionKernel";

	/** Set Real Production Kernel (kg)	  */
	public void setReal_ProductionKernel (BigDecimal Real_ProductionKernel);

	/** Get Real Production Kernel (kg)	  */
	public BigDecimal getReal_ProductionKernel();

    /** Column name Real_StockKernelToday */
    public static final String COLUMNNAME_Real_StockKernelToday = "Real_StockKernelToday";

	/** Set Real Stock Kernel Today (kg)	  */
	public void setReal_StockKernelToday (BigDecimal Real_StockKernelToday);

	/** Get Real Stock Kernel Today (kg)	  */
	public BigDecimal getReal_StockKernelToday();

    /** Column name RestantStew */
    public static final String COLUMNNAME_RestantStew = "RestantStew";

	/** Set Restant on Stew (kg)	  */
	public void setRestantStew (BigDecimal RestantStew);

	/** Get Restant on Stew (kg)	  */
	public BigDecimal getRestantStew();

    /** Column name ReturnFibreQty */
    public static final String COLUMNNAME_ReturnFibreQty = "ReturnFibreQty";

	/** Set Fibre Return Qty	  */
	public void setReturnFibreQty (BigDecimal ReturnFibreQty);

	/** Get Fibre Return Qty	  */
	public BigDecimal getReturnFibreQty();

    /** Column name ReturnShellQty */
    public static final String COLUMNNAME_ReturnShellQty = "ReturnShellQty";

	/** Set Shell Return Qty	  */
	public void setReturnShellQty (BigDecimal ReturnShellQty);

	/** Get Shell Return Qty	  */
	public BigDecimal getReturnShellQty();

    /** Column name ReturnSludgeOilQty */
    public static final String COLUMNNAME_ReturnSludgeOilQty = "ReturnSludgeOilQty";

	/** Set Sludge Oil Return Qty	  */
	public void setReturnSludgeOilQty (BigDecimal ReturnSludgeOilQty);

	/** Get Sludge Oil Return Qty	  */
	public BigDecimal getReturnSludgeOilQty();

    /** Column name ReturnSolidDecanterQty */
    public static final String COLUMNNAME_ReturnSolidDecanterQty = "ReturnSolidDecanterQty";

	/** Set Solid Decanter Return Qty	  */
	public void setReturnSolidDecanterQty (BigDecimal ReturnSolidDecanterQty);

	/** Get Solid Decanter Return Qty	  */
	public BigDecimal getReturnSolidDecanterQty();

    /** Column name StartTime */
    public static final String COLUMNNAME_StartTime = "StartTime";

	/** Set Start Time.
	  * Time started
	  */
	public void setStartTime (Timestamp StartTime);

	/** Get Start Time.
	  * Time started
	  */
	public Timestamp getStartTime();

    /** Column name Sterilizer */
    public static final String COLUMNNAME_Sterilizer = "Sterilizer";

	/** Set Sterilizer	  */
	public void setSterilizer (BigDecimal Sterilizer);

	/** Get Sterilizer	  */
	public BigDecimal getSterilizer();

    /** Column name StewCapacity */
    public static final String COLUMNNAME_StewCapacity = "StewCapacity";

	/** Set Stew Capacity (kg)	  */
	public void setStewCapacity (BigDecimal StewCapacity);

	/** Get Stew Capacity (kg)	  */
	public BigDecimal getStewCapacity();

    /** Column name StockFibre */
    public static final String COLUMNNAME_StockFibre = "StockFibre";

	/** Set Stock Fibre (kg).
	  * Stock Fibre/Empty Bunch Today on Kg
	  */
	public void setStockFibre (BigDecimal StockFibre);

	/** Get Stock Fibre (kg).
	  * Stock Fibre/Empty Bunch Today on Kg
	  */
	public BigDecimal getStockFibre();

    /** Column name StockKernelToday */
    public static final String COLUMNNAME_StockKernelToday = "StockKernelToday";

	/** Set Stock Kernel Today (kg)	  */
	public void setStockKernelToday (BigDecimal StockKernelToday);

	/** Get Stock Kernel Today (kg)	  */
	public BigDecimal getStockKernelToday();

    /** Column name StockShell */
    public static final String COLUMNNAME_StockShell = "StockShell";

	/** Set Stock Shell (kg).
	  * Stock Shell Today on Kg
	  */
	public void setStockShell (BigDecimal StockShell);

	/** Get Stock Shell (kg).
	  * Stock Shell Today on Kg
	  */
	public BigDecimal getStockShell();

    /** Column name StockSludgeOil */
    public static final String COLUMNNAME_StockSludgeOil = "StockSludgeOil";

	/** Set Stock Sludge Oil (kg).
	  * Stock Sludge Oil Today on Kg
	  */
	public void setStockSludgeOil (BigDecimal StockSludgeOil);

	/** Get Stock Sludge Oil (kg).
	  * Stock Sludge Oil Today on Kg
	  */
	public BigDecimal getStockSludgeOil();

    /** Column name StockSolidDecanter */
    public static final String COLUMNNAME_StockSolidDecanter = "StockSolidDecanter";

	/** Set Stock Solid Decater (kg).
	  * Stock Solid Decanter Today on Kg
	  */
	public void setStockSolidDecanter (BigDecimal StockSolidDecanter);

	/** Get Stock Solid Decater (kg).
	  * Stock Solid Decanter Today on Kg
	  */
	public BigDecimal getStockSolidDecanter();

    /** Column name TimeInNumber */
    public static final String COLUMNNAME_TimeInNumber = "TimeInNumber";

	/** Set Time Start Number.
	  * Must 4 Number for parsing to date Time In
	  */
	public void setTimeInNumber (String TimeInNumber);

	/** Get Time Start Number.
	  * Must 4 Number for parsing to date Time In
	  */
	public String getTimeInNumber();

    /** Column name TimeOutNumber */
    public static final String COLUMNNAME_TimeOutNumber = "TimeOutNumber";

	/** Set Time End Number.
	  * Must 4 Number for parsing to date Time Out
	  */
	public void setTimeOutNumber (String TimeOutNumber);

	/** Get Time End Number.
	  * Must 4 Number for parsing to date Time Out
	  */
	public String getTimeOutNumber();

    /** Column name UNS_PrevSounding_ID */
    public static final String COLUMNNAME_UNS_PrevSounding_ID = "UNS_PrevSounding_ID";

	/** Set Prev Sounding	  */
	public void setUNS_PrevSounding_ID (int UNS_PrevSounding_ID);

	/** Get Prev Sounding	  */
	public int getUNS_PrevSounding_ID();

	public com.uns.model.I_UNS_Sounding getUNS_PrevSounding() throws RuntimeException;

    /** Column name UNS_Production_ID */
    public static final String COLUMNNAME_UNS_Production_ID = "UNS_Production_ID";

	/** Set Production	  */
	public void setUNS_Production_ID (int UNS_Production_ID);

	/** Get Production	  */
	public int getUNS_Production_ID();

	public com.uns.model.I_UNS_Production getUNS_Production() throws RuntimeException;

    /** Column name UNS_Resource_ID */
    public static final String COLUMNNAME_UNS_Resource_ID = "UNS_Resource_ID";

	/** Set Manufacture Resource	  */
	public void setUNS_Resource_ID (int UNS_Resource_ID);

	/** Get Manufacture Resource	  */
	public int getUNS_Resource_ID();

	public com.uns.model.I_UNS_Resource getUNS_Resource() throws RuntimeException;

    /** Column name UNS_Sounding_ID */
    public static final String COLUMNNAME_UNS_Sounding_ID = "UNS_Sounding_ID";

	/** Set Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID);

	/** Get Sounding	  */
	public int getUNS_Sounding_ID();

    /** Column name UNS_Sounding_UU */
    public static final String COLUMNNAME_UNS_Sounding_UU = "UNS_Sounding_UU";

	/** Set UNS_Sounding_UU	  */
	public void setUNS_Sounding_UU (String UNS_Sounding_UU);

	/** Get UNS_Sounding_UU	  */
	public String getUNS_Sounding_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
