/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Tank
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Tank extends PO implements I_UNS_Tank, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20161217L;

    /** Standard Constructor */
    public X_UNS_Tank (Properties ctx, int UNS_Tank_ID, String trxName)
    {
      super (ctx, UNS_Tank_ID, trxName);
      /** if (UNS_Tank_ID == 0)
        {
			setDiameters (Env.ZERO);
			setHeightBottomTank (Env.ZERO);
			setHeightMeasuringTable (Env.ZERO);
			setHMH (Env.ZERO);
			setHMHOfMeasuringTable (Env.ZERO);
			setInitialLastSounding (Env.ZERO);
// 0
			setMaxHeightNetVolume (Env.ZERO);
			setM_Locator_ID (0);
			setM_Product_ID (0);
			setNetVolume (Env.ZERO);
			setTankHeight (Env.ZERO);
			setTankStatus (null);
// standardform
			setUNS_Tank_ID (0);
			setUNS_Tank_UU (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_Tank (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Tank[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_A_Asset getA_Asset() throws RuntimeException
    {
		return (org.compiere.model.I_A_Asset)MTable.get(getCtx(), org.compiere.model.I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Bottom Volume.
		@param BottomVolume Bottom Volume	  */
	public void setBottomVolume (BigDecimal BottomVolume)
	{
		set_Value (COLUMNNAME_BottomVolume, BottomVolume);
	}

	/** Get Bottom Volume.
		@return Bottom Volume	  */
	public BigDecimal getBottomVolume () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BottomVolume);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Location getC_Location() throws RuntimeException
    {
		return (I_C_Location)MTable.get(getCtx(), I_C_Location.Table_Name)
			.getPO(getC_Location_ID(), get_TrxName());	}

	/** Set Address.
		@param C_Location_ID 
		Location or Address
	  */
	public void setC_Location_ID (int C_Location_ID)
	{
		if (C_Location_ID < 1) 
			set_Value (COLUMNNAME_C_Location_ID, null);
		else 
			set_Value (COLUMNNAME_C_Location_ID, Integer.valueOf(C_Location_ID));
	}

	/** Get Address.
		@return Location or Address
	  */
	public int getC_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Diameters.
		@param Diameters Diameters	  */
	public void setDiameters (BigDecimal Diameters)
	{
		set_Value (COLUMNNAME_Diameters, Diameters);
	}

	/** Get Diameters.
		@return Diameters	  */
	public BigDecimal getDiameters () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Diameters);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Bottom of Tank.
		@param HeightBottomTank Height Bottom of Tank	  */
	public void setHeightBottomTank (BigDecimal HeightBottomTank)
	{
		set_Value (COLUMNNAME_HeightBottomTank, HeightBottomTank);
	}

	/** Get Height Bottom of Tank.
		@return Height Bottom of Tank	  */
	public BigDecimal getHeightBottomTank () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightBottomTank);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Measuring Table.
		@param HeightMeasuringTable Height Measuring Table	  */
	public void setHeightMeasuringTable (BigDecimal HeightMeasuringTable)
	{
		set_Value (COLUMNNAME_HeightMeasuringTable, HeightMeasuringTable);
	}

	/** Get Height Measuring Table.
		@return Height Measuring Table	  */
	public BigDecimal getHeightMeasuringTable () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightMeasuringTable);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Measuring Holes.
		@param HMH Height Measuring Holes	  */
	public void setHMH (BigDecimal HMH)
	{
		set_Value (COLUMNNAME_HMH, HMH);
	}

	/** Get Height Measuring Holes.
		@return Height Measuring Holes	  */
	public BigDecimal getHMH () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HMH);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Measuring Holes Of Measuring Table.
		@param HMHOfMeasuringTable Height Measuring Holes Of Measuring Table	  */
	public void setHMHOfMeasuringTable (BigDecimal HMHOfMeasuringTable)
	{
		set_Value (COLUMNNAME_HMHOfMeasuringTable, HMHOfMeasuringTable);
	}

	/** Get Height Measuring Holes Of Measuring Table.
		@return Height Measuring Holes Of Measuring Table	  */
	public BigDecimal getHMHOfMeasuringTable () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HMHOfMeasuringTable);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Initial Last Sounding.
		@param InitialLastSounding Initial Last Sounding	  */
	public void setInitialLastSounding (BigDecimal InitialLastSounding)
	{
		set_Value (COLUMNNAME_InitialLastSounding, InitialLastSounding);
	}

	/** Get Initial Last Sounding.
		@return Initial Last Sounding	  */
	public BigDecimal getInitialLastSounding () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_InitialLastSounding);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Last Sounding Date.
		@param LastSoundingDate Last Sounding Date	  */
	public void setLastSoundingDate (Timestamp LastSoundingDate)
	{
		set_Value (COLUMNNAME_LastSoundingDate, LastSoundingDate);
	}

	/** Get Last Sounding Date.
		@return Last Sounding Date	  */
	public Timestamp getLastSoundingDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_LastSoundingDate);
	}

	/** Set Last Sounding Quantity.
		@param LastSoundingQty Last Sounding Quantity	  */
	public void setLastSoundingQty (BigDecimal LastSoundingQty)
	{
		set_Value (COLUMNNAME_LastSoundingQty, LastSoundingQty);
	}

	/** Get Last Sounding Quantity.
		@return Last Sounding Quantity	  */
	public BigDecimal getLastSoundingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LastSoundingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Maximum Height from Net Volume.
		@param MaxHeightNetVolume Maximum Height from Net Volume	  */
	public void setMaxHeightNetVolume (BigDecimal MaxHeightNetVolume)
	{
		set_Value (COLUMNNAME_MaxHeightNetVolume, MaxHeightNetVolume);
	}

	/** Get Maximum Height from Net Volume.
		@return Maximum Height from Net Volume	  */
	public BigDecimal getMaxHeightNetVolume () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MaxHeightNetVolume);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Net Volume.
		@param NetVolume Net Volume	  */
	public void setNetVolume (BigDecimal NetVolume)
	{
		set_Value (COLUMNNAME_NetVolume, NetVolume);
	}

	/** Get Net Volume.
		@return Net Volume	  */
	public BigDecimal getNetVolume () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetVolume);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Tank Height.
		@param TankHeight Tank Height	  */
	public void setTankHeight (BigDecimal TankHeight)
	{
		set_Value (COLUMNNAME_TankHeight, TankHeight);
	}

	/** Get Tank Height.
		@return Tank Height	  */
	public BigDecimal getTankHeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TankHeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Tank No.
		@param TankNo Tank No	  */
	public void setTankNo (String TankNo)
	{
		set_Value (COLUMNNAME_TankNo, TankNo);
	}

	/** Get Tank No.
		@return Tank No	  */
	public String getTankNo () 
	{
		return (String)get_Value(COLUMNNAME_TankNo);
	}

	/** Calibrated = calibrated */
	public static final String TANKSTATUS_Calibrated = "calibrated";
	/** Standard Formula = standardform */
	public static final String TANKSTATUS_StandardFormula = "standardform";
	/** Measured Formula = measuredform */
	public static final String TANKSTATUS_MeasuredFormula = "measuredform";
	/** Set Tank Status.
		@param TankStatus Tank Status	  */
	public void setTankStatus (String TankStatus)
	{

		set_Value (COLUMNNAME_TankStatus, TankStatus);
	}

	/** Get Tank Status.
		@return Tank Status	  */
	public String getTankStatus () 
	{
		return (String)get_Value(COLUMNNAME_TankStatus);
	}

	/** Set Type Of Roof.
		@param TypeOfRoof Type Of Roof	  */
	public void setTypeOfRoof (String TypeOfRoof)
	{
		set_Value (COLUMNNAME_TypeOfRoof, TypeOfRoof);
	}

	/** Get Type Of Roof.
		@return Type Of Roof	  */
	public String getTypeOfRoof () 
	{
		return (String)get_Value(COLUMNNAME_TypeOfRoof);
	}

	/** Set Tank.
		@param UNS_Tank_ID Tank	  */
	public void setUNS_Tank_ID (int UNS_Tank_ID)
	{
		if (UNS_Tank_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, Integer.valueOf(UNS_Tank_ID));
	}

	/** Get Tank.
		@return Tank	  */
	public int getUNS_Tank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Tank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Tank_UU.
		@param UNS_Tank_UU UNS_Tank_UU	  */
	public void setUNS_Tank_UU (String UNS_Tank_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Tank_UU, UNS_Tank_UU);
	}

	/** Get UNS_Tank_UU.
		@return UNS_Tank_UU	  */
	public String getUNS_Tank_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Tank_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Volume per cm.
		@param VolumeCm Volume per cm	  */
	public void setVolumeCm (BigDecimal VolumeCm)
	{
		set_Value (COLUMNNAME_VolumeCm, VolumeCm);
	}

	/** Get Volume per cm.
		@return Volume per cm	  */
	public BigDecimal getVolumeCm () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VolumeCm);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}