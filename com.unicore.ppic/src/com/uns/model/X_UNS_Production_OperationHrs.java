/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Production_OperationHrs
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Production_OperationHrs extends PO implements I_UNS_Production_OperationHrs, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170808L;

    /** Standard Constructor */
    public X_UNS_Production_OperationHrs (Properties ctx, int UNS_Production_OperationHrs_ID, String trxName)
    {
      super (ctx, UNS_Production_OperationHrs_ID, trxName);
      /** if (UNS_Production_OperationHrs_ID == 0)
        {
			setEventType (null);
			setLine (0);
			setProcessed (false);
			setTimeEnd (new Timestamp( System.currentTimeMillis() ));
			setTimeStart (new Timestamp( System.currentTimeMillis() ));
			setUNS_Production_OperationHrs_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Production_OperationHrs (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Production_OperationHrs[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Running = RUN */
	public static final String EVENTTYPE_Running = "RUN";
	/** Break = BRK */
	public static final String EVENTTYPE_Break = "BRK";
	/** Preparation = PRE */
	public static final String EVENTTYPE_Preparation = "PRE";
	/** Maintenance = MTN */
	public static final String EVENTTYPE_Maintenance = "MTN";
	/** Finishing = FIN */
	public static final String EVENTTYPE_Finishing = "FIN";
	/** Set Event Type.
		@param EventType 
		Type of Event
	  */
	public void setEventType (String EventType)
	{

		set_Value (COLUMNNAME_EventType, EventType);
	}

	/** Get Event Type.
		@return Type of Event
	  */
	public String getEventType () 
	{
		return (String)get_Value(COLUMNNAME_EventType);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Notes.
		@param Notes Notes	  */
	public void setNotes (String Notes)
	{
		set_Value (COLUMNNAME_Notes, Notes);
	}

	/** Get Notes.
		@return Notes	  */
	public String getNotes () 
	{
		return (String)get_Value(COLUMNNAME_Notes);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Time End.
		@param TimeEnd Time End	  */
	public void setTimeEnd (Timestamp TimeEnd)
	{
		set_Value (COLUMNNAME_TimeEnd, TimeEnd);
	}

	/** Get Time End.
		@return Time End	  */
	public Timestamp getTimeEnd () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TimeEnd);
	}

	/** Set Time Start.
		@param TimeStart Time Start	  */
	public void setTimeStart (Timestamp TimeStart)
	{
		set_Value (COLUMNNAME_TimeStart, TimeStart);
	}

	/** Get Time Start.
		@return Time Start	  */
	public Timestamp getTimeStart () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TimeStart);
	}

	/** Set Total Hours.
		@param TotalHours 
		Total in hours
	  */
	public void setTotalHours (BigDecimal TotalHours)
	{
		set_Value (COLUMNNAME_TotalHours, TotalHours);
	}

	/** Get Total Hours.
		@return Total in hours
	  */
	public BigDecimal getTotalHours () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalHours);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Production getUNS_Production() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Production)MTable.get(getCtx(), com.uns.model.I_UNS_Production.Table_Name)
			.getPO(getUNS_Production_ID(), get_TrxName());	}

	/** Set Production.
		@param UNS_Production_ID Production	  */
	public void setUNS_Production_ID (int UNS_Production_ID)
	{
		if (UNS_Production_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_ID, Integer.valueOf(UNS_Production_ID));
	}

	/** Get Production.
		@return Production	  */
	public int getUNS_Production_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Production Operation Hours.
		@param UNS_Production_OperationHrs_ID Production Operation Hours	  */
	public void setUNS_Production_OperationHrs_ID (int UNS_Production_OperationHrs_ID)
	{
		if (UNS_Production_OperationHrs_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_OperationHrs_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_OperationHrs_ID, Integer.valueOf(UNS_Production_OperationHrs_ID));
	}

	/** Get Production Operation Hours.
		@return Production Operation Hours	  */
	public int getUNS_Production_OperationHrs_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_OperationHrs_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Production_OperationHrs_UU.
		@param UNS_Production_OperationHrs_UU UNS_Production_OperationHrs_UU	  */
	public void setUNS_Production_OperationHrs_UU (String UNS_Production_OperationHrs_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Production_OperationHrs_UU, UNS_Production_OperationHrs_UU);
	}

	/** Get UNS_Production_OperationHrs_UU.
		@return UNS_Production_OperationHrs_UU	  */
	public String getUNS_Production_OperationHrs_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Production_OperationHrs_UU);
	}

	public com.uns.model.I_UNS_Sounding getUNS_Sounding() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Sounding)MTable.get(getCtx(), com.uns.model.I_UNS_Sounding.Table_Name)
			.getPO(getUNS_Sounding_ID(), get_TrxName());	}

	/** Set Sounding.
		@param UNS_Sounding_ID Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID)
	{
		if (UNS_Sounding_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, Integer.valueOf(UNS_Sounding_ID));
	}

	/** Get Sounding.
		@return Sounding	  */
	public int getUNS_Sounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Sounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}