package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;

public class MUNSTankTemperature extends X_UNS_TankTemperature {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3612374738634641056L;

	public MUNSTankTemperature(Properties ctx, int UNS_TankTemperature_ID,
			String trxName) {
		super(ctx, UNS_TankTemperature_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSTankTemperature(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static BigDecimal getDensity(BigDecimal temperature, String trxName){
		BigDecimal density = null;
		BigDecimal maxtemp = DB.getSQLValueBD(trxName, "SELECT MAX(TemperatureValue) FROM UNS_TankTemperature");
		BigDecimal mintemp = DB.getSQLValueBD(trxName, "SELECT MIN(TemperatureValue) FROM UNS_TankTemperature");
		
		if(temperature.compareTo(maxtemp)==1)
			throw new AdempiereException("Suhu Melebihi Dari Batas Suhu Yang Ditetapkan");
		else if(temperature.compareTo(mintemp)<0)
			throw new AdempiereException("Suhu Dibawah Dari Batas Suhu Yang Ditentukan");
		
		String sql = "SELECT Weight FROM UNS_TankTemperature WHERE TemperatureValue = ?";
		density = DB.getSQLValueBD(trxName, sql, temperature);
		return density;
	}

}
