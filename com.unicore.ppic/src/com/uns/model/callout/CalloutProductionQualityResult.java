/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.uns.qad.model.I_UNS_Production_Quality;
import com.uns.qad.model.I_UNS_Quality_Specs;
import com.uns.qad.model.X_UNS_Production_Quality;

/**
 * @author AzHaidar
 *
 */
public class CalloutProductionQualityResult extends CalloutEngine implements
		IColumnCallout {

	/**
	 * 
	 */
	public CalloutProductionQualityResult() {
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) 
	{
		String retVal = "";
		
		if (mTab.getTableName().equals(X_UNS_Production_Quality.Table_Name)
				&& mField.getColumnName().equals(X_UNS_Production_Quality.COLUMNNAME_UNS_Quality_Specs_ID))
		{
			retVal = changeSpecification(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		
		return retVal;
	} //start
	
	
	private String changeSpecification (Properties ctx, int WindowNo, GridTab mTab,
		GridField mField, Object value, Object oldValue)
	{
		if (value == null)
			return null;
		
		//Integer specs_ID = (Integer) value;
		
		I_UNS_Production_Quality qaResult = GridTabWrapper.create(mTab, I_UNS_Production_Quality.class);
		
		I_UNS_Quality_Specs specs = qaResult.getUNS_Quality_Specs();
		
		qaResult.setValue(specs.getValue());
		qaResult.setName(specs.getName());
		qaResult.setM_Product_ID(specs.getM_Product_ID());
		qaResult.setC_UOM_ID(specs.getC_UOM_ID());
		
		return null;
	} //changeSpecification
}
