/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

import com.uns.model.MUNSPriceListConv;

/**
 * @author Burhani Adam
 *
 */
public class CalloutPriceListConv implements IColumnCallout{

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		String retValue = null;
		
		if(mField.getColumnName().equals(MUNSPriceListConv.COLUMNNAME_PriceListFrom_ID))
			retValue = PriceListFrom(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(MUNSPriceListConv.COLUMNNAME_PriceListTo_ID))
			retValue = PriceListTo(ctx, WindowNo, mTab, mField, value);
		
		return retValue;
	}
	
	private String PriceListFrom(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value)
	{
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MUNSPriceListConv.COLUMNNAME_CurrencyFrom_ID, -1);
			return null;
		}
		
		String sql = "SELECT C_Currency_ID FROM M_PriceList WHERE M_PriceList_ID = ?";
		int currency = DB.getSQLValue(null, sql, (Integer) value);
		mTab.setValue(MUNSPriceListConv.COLUMNNAME_CurrencyFrom_ID, currency);
		
		return null;
	}
	
	private String PriceListTo(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value)
	{
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MUNSPriceListConv.COLUMNNAME_CurrencyTo_ID, -1);
			return null;
		}
		
		String sql = "SELECT C_Currency_ID FROM M_PriceList WHERE M_PriceList_ID = ?";
		int currency = DB.getSQLValue(null, sql, (Integer) value);
		mTab.setValue(MUNSPriceListConv.COLUMNNAME_CurrencyTo_ID, currency);
		
		return null;
	}
}