package com.uns.model.callout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;

import com.uns.model.I_UNS_Sounding;
import com.uns.model.MUNSSounding;
import com.uns.model.X_UNS_Sounding;

public class CalloutFFB extends CalloutEngine implements IColumnCallout {
	
	private String CalculateFFBprocessed(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value){
		BigDecimal Qty = (BigDecimal)value;
		if (Qty == null || Qty.compareTo(Env.ZERO) == 0)
				return "";
		
		BigDecimal sterilizer = (BigDecimal) mTab.getValue(MUNSSounding.COLUMNNAME_Sterilizer);
		
		if(sterilizer.compareTo(Env.ZERO) == 0)
			mTab.setValue(X_UNS_Sounding.COLUMNNAME_FFBProcessedNettoI, 0);
		else
			mTab.setValue(X_UNS_Sounding.COLUMNNAME_FFBProcessedNettoI, sterilizer.multiply(Qty));
		
		return "";
	}
	
	private String CaclculateStewCapacity(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value){
		BigDecimal Qty = (BigDecimal)value;
		if (Qty == null || Qty.compareTo(Env.ZERO) == 0)
				return "";
		
		BigDecimal sterilizer = (BigDecimal) mTab.getValue(MUNSSounding.COLUMNNAME_Sterilizer);
		
		mTab.setValue(X_UNS_Sounding.COLUMNNAME_StewCapacity, Qty.divide(sterilizer, 0, RoundingMode.HALF_UP));
		
		return "";
	}
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		String retValue = null;
		
		if(I_UNS_Sounding.COLUMNNAME_StewCapacity.equals(mField.getColumnName()))
			retValue = CalculateFFBprocessed(ctx, WindowNo, mTab, mField, value);
		else if(I_UNS_Sounding.COLUMNNAME_FFBProcessedNettoI.equals(mField.getColumnName()))
			retValue = CaclculateStewCapacity(ctx, WindowNo, mTab, mField, value);
		
		return retValue;
	}

}
