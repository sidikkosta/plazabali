package com.uns.model.callout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.uns.model.I_UNS_Sounding;
import com.uns.model.MUNSSounding;
import com.uns.model.X_UNS_Sounding;

public class CalloutSounding extends CalloutEngine implements IColumnCallout {

	public CalloutSounding() {
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		String retVal=null;
		
		if(mTab.getTableName().equals(MUNSSounding.Table_Name)){
			if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_TimeInNumber)
					|| mField.getColumnName().equals(MUNSSounding.COLUMNNAME_TimeOutNumber))
				retVal = TimeASString(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_DateAcct))
				retVal =  ChangeDateOnTimeInAndTimeOut(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_FFBProcessedNettoI))
				retVal =  CalculateStewCapacity(ctx, WindowNo, mTab, mField, oldValue);
			else if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_StewCapacity) 
					|| mField.getColumnName().equals(MUNSSounding.COLUMNNAME_Sterilizer))
				retVal = CalculateFFBprocessed(ctx, WindowNo, mTab, mField, oldValue);
				
		}

		return retVal;
	}
	
	private String TimeASString(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{		
		String timeString = (String) value;
		
		if(Util.isEmpty(timeString, true))
			return null;
		
		int timeInt = 0;
		int offSet = 0;
		int length = timeString.length();

		if(length == 5)
		{
			String offSetStr = timeString.substring(0, 1);
			if(offSetStr.equals("-"))
				offSet = -1;
			else if(offSetStr.equals("+"))
				offSet = 1;
			else
				throw new AdempiereException("Wrong Symbol. You only can use symbol '+' or '-'");
			
			timeInt = new Integer (timeString.substring(1, 5));
		}
		else if(length == 4)
		{
			timeInt = new Integer (timeString);
		}
		else
		{
			return null;
		}
					
		
		if(timeInt > -1 && timeInt < 2360)
		{
			timeString = String.valueOf(timeInt);
			if(timeString.length()==3)
				timeString = "0"+timeString;
			
			Timestamp dateAcct = TimeUtil.addDays( (Timestamp) mTab.getValue(I_UNS_Sounding.COLUMNNAME_DateAcct) , offSet);
			String date = dateAcct.toString();
			String dateTimeString = date.substring(0, 10) + " " + timeString.substring(0, 2) + ":" + timeString.substring(2) + ":00";
			Timestamp dateTime = null;
			dateTime = Timestamp.valueOf(dateTimeString);
			
			if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_TimeInNumber))
			{
				mTab.setValue(MUNSSounding.COLUMNNAME_StartTime, dateTime);
			}
			else
			{
				mTab.setValue(MUNSSounding.COLUMNNAME_EndTime, dateTime);
			}
		}			
		
		return null;
	}
	
	private String ChangeDateOnTimeInAndTimeOut(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		Timestamp date = (Timestamp) value;
		Timestamp start = (Timestamp) mTab.getValue(I_UNS_Sounding.COLUMNNAME_StartTime);
		Timestamp end = (Timestamp) mTab.getValue(I_UNS_Sounding.COLUMNNAME_EndTime);
		
		//check TimeIn and Time Out
		int inOffSet = 0;
		int outOffSet = 0;
		String timeIn = (String) mTab.getValue(I_UNS_Sounding.COLUMNNAME_TimeInNumber);
		String timeOut = (String) mTab.getValue(I_UNS_Sounding.COLUMNNAME_TimeOutNumber);
		
		if(timeIn != null)
		{
			if(timeIn.length() == 5)
			{
				String offSetStr = timeIn.substring(0, 1);
				if(offSetStr.equals("-"))
					inOffSet = -1;
				else if(offSetStr.equals("+"))
					inOffSet = 1;
				else
					throw new AdempiereException("Wrong Symbol. You only can use symbol '+' or '-'");
			}
		}
		
		if(timeOut != null)
		{
			if(timeOut.length() == 5)
			{
				String offSetStr = timeOut.substring(0, 1);
				if(offSetStr.equals("-"))
					outOffSet = -1;
				else if(offSetStr.equals("+"))
					outOffSet = 1;
				else
					throw new AdempiereException("Wrong Symbol. You only can use symbol '+' or '-'");
			}
		}
		
		String startDateStr = TimeUtil.addDays(date, inOffSet).toString();
		String endDateStr = TimeUtil.addDays(date, outOffSet).toString();
		
		start = Timestamp.valueOf(startDateStr.substring(0,10)+" "+start.toString().substring(11));
		end = Timestamp.valueOf(endDateStr.substring(0,10)+" "+end.toString().substring(11));
		
		mTab.setValue(MUNSSounding.COLUMNNAME_StartTime, start);
		mTab.setValue(MUNSSounding.COLUMNNAME_EndTime, end);
		
		return null;
	}
	
	private String CalculateFFBprocessed(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value){
		BigDecimal Qty = (BigDecimal)value;
		if (Qty == null || Qty.compareTo(Env.ZERO) == 0)
				return "";
		
		BigDecimal columnQty = Env.ZERO;
		
		if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_Sterilizer))
			columnQty = (BigDecimal) mTab.getValue(X_UNS_Sounding.COLUMNNAME_StewCapacity);
		else if(mField.getColumnName().equals(MUNSSounding.COLUMNNAME_StewCapacity))
			columnQty = (BigDecimal) mTab.getValue(X_UNS_Sounding.COLUMNNAME_Sterilizer);
			
		mTab.setValue(X_UNS_Sounding.COLUMNNAME_FFBProcessedNettoI, columnQty.multiply(Qty));
		
		return "";
	}
	
	private String CalculateStewCapacity(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value){
		BigDecimal Qty = (BigDecimal)value;
		if (Qty == null || Qty.compareTo(Env.ZERO) == 0)
				return "";
		
		BigDecimal sterilizer = (BigDecimal) mTab.getValue(MUNSSounding.COLUMNNAME_Sterilizer);
		
		mTab.setValue(X_UNS_Sounding.COLUMNNAME_StewCapacity, Qty.divide(sterilizer, 0, RoundingMode.HALF_UP));
		
		return "";
	}
}
