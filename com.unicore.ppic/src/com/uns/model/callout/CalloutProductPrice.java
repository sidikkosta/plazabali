/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

/**
 * @author Burhani Adam
 *
 */
public class CalloutProductPrice implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutProductPrice() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(mField.getColumnName().equals("M_PriceList_Version_ID"))
		{
			if(value == null || (Integer) value == 0)
				mTab.setValue("AD_Org_ID", -1);
			else
			{
				String sql = "SELECT AD_Org_ID FROM M_PriceList_Version"
						+ " WHERE M_PriceList_Version_ID = ?";
				int org = DB.getSQLValue(null, sql, (Integer) value);
				mTab.setValue("AD_Org_ID", org);
			}
		}
		return null;
	}

}
