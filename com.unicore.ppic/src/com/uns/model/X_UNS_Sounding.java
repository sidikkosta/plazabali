/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Sounding
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Sounding extends PO implements I_UNS_Sounding, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180212L;

    /** Standard Constructor */
    public X_UNS_Sounding (Properties ctx, int UNS_Sounding_ID, String trxName)
    {
      super (ctx, UNS_Sounding_ID, trxName);
      /** if (UNS_Sounding_ID == 0)
        {
			setAcc_Fibre (Env.ZERO);
// 0
			setAcc_Shell (Env.ZERO);
// 0
			setAcc_SludgeOil (Env.ZERO);
// 0
			setAcc_SolidDecanter (Env.ZERO);
// 0
			setBulkingSilo (Env.ZERO);
// 0
			setCaCO3Used_Shift1 (Env.ZERO);
// 0
			setCaCO3Used_Shift2 (Env.ZERO);
// 0
			setCangkangBoiler (Env.ZERO);
// 0
			setCangkangInHopper (Env.ZERO);
// 0
			setCPOReturnQty (Env.ZERO);
// 0
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDeliveredFibreQty (Env.ZERO);
// 0
			setDeliveredQty (Env.ZERO);
			setDeliveredShellQty (Env.ZERO);
// 0
			setDeliveredSludgeOilQty (Env.ZERO);
// 0
			setDeliveredSolidQty (Env.ZERO);
// 0
			setDirtKernel (Env.ZERO);
// 0
			setDirtKernelOnBunker (Env.ZERO);
// 0
			setEndTime (new Timestamp( System.currentTimeMillis() ));
// @SQL=SELECT NOW()
			setFFBProcessedNettoI (Env.ZERO);
// 0
			setFFBProcessedNettoII (Env.ZERO);
// 0
			setFFBReceiptNettoI (Env.ZERO);
// 0
			setFFBReceiptNettoII (Env.ZERO);
// 0
			setFFBRemainingQty (Env.ZERO);
// 0
			setFFBRemainingQtyII (Env.ZERO);
// 0
			setFlowMeterQty (Env.ZERO);
// 0
			setFuelUsed_Shift1 (Env.ZERO);
// 0
			setFuelUsed_Shift2 (Env.ZERO);
// 0
			setisManualSounding (false);
// N
			setKernelDeliveredQty (Env.ZERO);
// 0
			setKernelFloor (Env.ZERO);
// 0
			setKernelFloorBad (Env.ZERO);
// 0
			setKernelReturnQty (Env.ZERO);
// 0
			setKernelSilo1 (Env.ZERO);
// 0
			setKernelSilo2 (Env.ZERO);
// 0
			setKernelSilo3 (Env.ZERO);
// 0
			setKERNettoI (Env.ZERO);
// 0
			setKERNettoII (Env.ZERO);
// 0
			setMoistKernel (Env.ZERO);
// 0
			setMoistKernelOnBunker (Env.ZERO);
// 0
			setName (null);
			setNutFloor (Env.ZERO);
// 0
			setNutPercentage (Env.ZERO);
// 0
			setNutSilo (Env.ZERO);
// 0
			setOERNettoI (Env.ZERO);
// 0
			setOERNettoII (Env.ZERO);
// 0
			setPreviousFFBQty (Env.ZERO);
// 0
			setPreviousFFBQtyII (Env.ZERO);
// 0
			setPreviousKernelQty (Env.ZERO);
// 0
			setProductionCangkang (Env.ZERO);
// 0
			setReal_KernelSilo1 (Env.ZERO);
// 0
			setReal_KernelSilo2 (Env.ZERO);
// 0
			setReal_KernelSilo3 (Env.ZERO);
// 0
			setReal_KERNettoI (Env.ZERO);
// 0
			setReal_KERNettoII (Env.ZERO);
// 0
			setReal_NutFloor (Env.ZERO);
// 0
			setReal_NutSilo (Env.ZERO);
// 0
			setReal_PreviousKernelQty (Env.ZERO);
// 0
			setRestantStew (Env.ZERO);
// 0
			setReturnFibreQty (Env.ZERO);
// 0
			setReturnShellQty (Env.ZERO);
// 0
			setReturnSludgeOilQty (Env.ZERO);
// 0
			setReturnSolidDecanterQty (Env.ZERO);
// 0
			setStartTime (new Timestamp( System.currentTimeMillis() ));
// @SQL=SELECT NOW()
			setSterilizer (Env.ZERO);
// 0
			setStewCapacity (Env.ZERO);
// 0
			setStockFibre (Env.ZERO);
// 0
			setStockShell (Env.ZERO);
// 0
			setStockSludgeOil (Env.ZERO);
// 0
			setStockSolidDecanter (Env.ZERO);
// 0
			setUNS_Resource_ID (0);
// 1000198
			setUNS_Sounding_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Sounding (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Sounding[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Acces Product Fibre.
		@param Acc_Fibre 
		Acces Product Fibre/Empty Bunch
	  */
	public void setAcc_Fibre (BigDecimal Acc_Fibre)
	{
		set_Value (COLUMNNAME_Acc_Fibre, Acc_Fibre);
	}

	/** Get Acces Product Fibre.
		@return Acces Product Fibre/Empty Bunch
	  */
	public BigDecimal getAcc_Fibre () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Acc_Fibre);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Acces Product Shell.
		@param Acc_Shell Acces Product Shell	  */
	public void setAcc_Shell (BigDecimal Acc_Shell)
	{
		set_Value (COLUMNNAME_Acc_Shell, Acc_Shell);
	}

	/** Get Acces Product Shell.
		@return Acces Product Shell	  */
	public BigDecimal getAcc_Shell () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Acc_Shell);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Acces Product Sludge Oil.
		@param Acc_SludgeOil Acces Product Sludge Oil	  */
	public void setAcc_SludgeOil (BigDecimal Acc_SludgeOil)
	{
		set_Value (COLUMNNAME_Acc_SludgeOil, Acc_SludgeOil);
	}

	/** Get Acces Product Sludge Oil.
		@return Acces Product Sludge Oil	  */
	public BigDecimal getAcc_SludgeOil () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Acc_SludgeOil);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Acces Product Solid Decanter.
		@param Acc_SolidDecanter Acces Product Solid Decanter	  */
	public void setAcc_SolidDecanter (BigDecimal Acc_SolidDecanter)
	{
		set_Value (COLUMNNAME_Acc_SolidDecanter, Acc_SolidDecanter);
	}

	/** Get Acces Product Solid Decanter.
		@return Acces Product Solid Decanter	  */
	public BigDecimal getAcc_SolidDecanter () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Acc_SolidDecanter);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Bulking Silo (kg).
		@param BulkingSilo Bulking Silo (kg)	  */
	public void setBulkingSilo (BigDecimal BulkingSilo)
	{
		set_Value (COLUMNNAME_BulkingSilo, BulkingSilo);
	}

	/** Get Bulking Silo (kg).
		@return Bulking Silo (kg)	  */
	public BigDecimal getBulkingSilo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BulkingSilo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CaCO3 Used (Shift1).
		@param CaCO3Used_Shift1 CaCO3 Used (Shift1)	  */
	public void setCaCO3Used_Shift1 (BigDecimal CaCO3Used_Shift1)
	{
		set_Value (COLUMNNAME_CaCO3Used_Shift1, CaCO3Used_Shift1);
	}

	/** Get CaCO3 Used (Shift1).
		@return CaCO3 Used (Shift1)	  */
	public BigDecimal getCaCO3Used_Shift1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CaCO3Used_Shift1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CaCO3 Used (Shift2).
		@param CaCO3Used_Shift2 CaCO3 Used (Shift2)	  */
	public void setCaCO3Used_Shift2 (BigDecimal CaCO3Used_Shift2)
	{
		set_Value (COLUMNNAME_CaCO3Used_Shift2, CaCO3Used_Shift2);
	}

	/** Get CaCO3 Used (Shift2).
		@return CaCO3 Used (Shift2)	  */
	public BigDecimal getCaCO3Used_Shift2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CaCO3Used_Shift2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cangkang Boiler (kg).
		@param CangkangBoiler Cangkang Boiler (kg)	  */
	public void setCangkangBoiler (BigDecimal CangkangBoiler)
	{
		set_Value (COLUMNNAME_CangkangBoiler, CangkangBoiler);
	}

	/** Get Cangkang Boiler (kg).
		@return Cangkang Boiler (kg)	  */
	public BigDecimal getCangkangBoiler () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CangkangBoiler);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cangkang In Hopper (kg).
		@param CangkangInHopper Cangkang In Hopper (kg)	  */
	public void setCangkangInHopper (BigDecimal CangkangInHopper)
	{
		set_Value (COLUMNNAME_CangkangInHopper, CangkangInHopper);
	}

	/** Get Cangkang In Hopper (kg).
		@return Cangkang In Hopper (kg)	  */
	public BigDecimal getCangkangInHopper () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CangkangInHopper);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CPO Current Sounding Qty.
		@param CPOCurrentSounding CPO Current Sounding Qty	  */
	public void setCPOCurrentSounding (BigDecimal CPOCurrentSounding)
	{
		set_Value (COLUMNNAME_CPOCurrentSounding, CPOCurrentSounding);
	}

	/** Get CPO Current Sounding Qty.
		@return CPO Current Sounding Qty	  */
	public BigDecimal getCPOCurrentSounding () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CPOCurrentSounding);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CPO Production Qty.
		@param CPOProductionQty CPO Production Qty	  */
	public void setCPOProductionQty (BigDecimal CPOProductionQty)
	{
		set_Value (COLUMNNAME_CPOProductionQty, CPOProductionQty);
	}

	/** Get CPO Production Qty.
		@return CPO Production Qty	  */
	public BigDecimal getCPOProductionQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CPOProductionQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CPO Return Qty.
		@param CPOReturnQty CPO Return Qty	  */
	public void setCPOReturnQty (BigDecimal CPOReturnQty)
	{
		set_Value (COLUMNNAME_CPOReturnQty, CPOReturnQty);
	}

	/** Get CPO Return Qty.
		@return CPO Return Qty	  */
	public BigDecimal getCPOReturnQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CPOReturnQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_Value (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Fibre Delivered Qty.
		@param DeliveredFibreQty 
		Delivered Product of Fibre / Empty Bunch
	  */
	public void setDeliveredFibreQty (BigDecimal DeliveredFibreQty)
	{
		set_Value (COLUMNNAME_DeliveredFibreQty, DeliveredFibreQty);
	}

	/** Get Fibre Delivered Qty.
		@return Delivered Product of Fibre / Empty Bunch
	  */
	public BigDecimal getDeliveredFibreQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeliveredFibreQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set CPO Delivered Quantity.
		@param DeliveredQty 
		Quantity of Delivered
	  */
	public void setDeliveredQty (BigDecimal DeliveredQty)
	{
		set_Value (COLUMNNAME_DeliveredQty, DeliveredQty);
	}

	/** Get CPO Delivered Quantity.
		@return Quantity of Delivered
	  */
	public BigDecimal getDeliveredQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeliveredQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Shell Delivered Qty.
		@param DeliveredShellQty Shell Delivered Qty	  */
	public void setDeliveredShellQty (BigDecimal DeliveredShellQty)
	{
		set_Value (COLUMNNAME_DeliveredShellQty, DeliveredShellQty);
	}

	/** Get Shell Delivered Qty.
		@return Shell Delivered Qty	  */
	public BigDecimal getDeliveredShellQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeliveredShellQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sludge Oil Delivered Qty.
		@param DeliveredSludgeOilQty 
		Delivered Product of Sludge Oil
	  */
	public void setDeliveredSludgeOilQty (BigDecimal DeliveredSludgeOilQty)
	{
		set_Value (COLUMNNAME_DeliveredSludgeOilQty, DeliveredSludgeOilQty);
	}

	/** Get Sludge Oil Delivered Qty.
		@return Delivered Product of Sludge Oil
	  */
	public BigDecimal getDeliveredSludgeOilQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeliveredSludgeOilQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Solid Decanter Delivered Qty.
		@param DeliveredSolidQty 
		Delivered Product of Solid Decanter
	  */
	public void setDeliveredSolidQty (BigDecimal DeliveredSolidQty)
	{
		set_Value (COLUMNNAME_DeliveredSolidQty, DeliveredSolidQty);
	}

	/** Get Solid Decanter Delivered Qty.
		@return Delivered Product of Solid Decanter
	  */
	public BigDecimal getDeliveredSolidQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeliveredSolidQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Dirt Kernel.
		@param DirtKernel 
		How Much Kernel with Dirt Quality
	  */
	public void setDirtKernel (BigDecimal DirtKernel)
	{
		set_Value (COLUMNNAME_DirtKernel, DirtKernel);
	}

	/** Get Dirt Kernel.
		@return How Much Kernel with Dirt Quality
	  */
	public BigDecimal getDirtKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DirtKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Dirt Kernel On Bunker.
		@param DirtKernelOnBunker 
		How Much Kernel On Bunker with Dirt Quality
	  */
	public void setDirtKernelOnBunker (BigDecimal DirtKernelOnBunker)
	{
		set_Value (COLUMNNAME_DirtKernelOnBunker, DirtKernelOnBunker);
	}

	/** Get Dirt Kernel On Bunker.
		@return How Much Kernel On Bunker with Dirt Quality
	  */
	public BigDecimal getDirtKernelOnBunker () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DirtKernelOnBunker);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set End Time.
		@param EndTime 
		End of the time span
	  */
	public void setEndTime (Timestamp EndTime)
	{
		set_Value (COLUMNNAME_EndTime, EndTime);
	}

	/** Get End Time.
		@return End of the time span
	  */
	public Timestamp getEndTime () 
	{
		return (Timestamp)get_Value(COLUMNNAME_EndTime);
	}

	/** Set FFB Processed Netto-I (kg).
		@param FFBProcessedNettoI FFB Processed Netto-I (kg)	  */
	public void setFFBProcessedNettoI (BigDecimal FFBProcessedNettoI)
	{
		set_Value (COLUMNNAME_FFBProcessedNettoI, FFBProcessedNettoI);
	}

	/** Get FFB Processed Netto-I (kg).
		@return FFB Processed Netto-I (kg)	  */
	public BigDecimal getFFBProcessedNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFBProcessedNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Processed Netto-II (kg).
		@param FFBProcessedNettoII 
		Quantity of FFB Processed Netto-II
	  */
	public void setFFBProcessedNettoII (BigDecimal FFBProcessedNettoII)
	{
		set_Value (COLUMNNAME_FFBProcessedNettoII, FFBProcessedNettoII);
	}

	/** Get FFB Processed Netto-II (kg).
		@return Quantity of FFB Processed Netto-II
	  */
	public BigDecimal getFFBProcessedNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFBProcessedNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Receipt Netto-I(kg).
		@param FFBReceiptNettoI FFB Receipt Netto-I(kg)	  */
	public void setFFBReceiptNettoI (BigDecimal FFBReceiptNettoI)
	{
		set_Value (COLUMNNAME_FFBReceiptNettoI, FFBReceiptNettoI);
	}

	/** Get FFB Receipt Netto-I(kg).
		@return FFB Receipt Netto-I(kg)	  */
	public BigDecimal getFFBReceiptNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFBReceiptNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Receipt Netto-II (kg).
		@param FFBReceiptNettoII 
		Quantity of FFB Receipt Netto-I 
	  */
	public void setFFBReceiptNettoII (BigDecimal FFBReceiptNettoII)
	{
		set_Value (COLUMNNAME_FFBReceiptNettoII, FFBReceiptNettoII);
	}

	/** Get FFB Receipt Netto-II (kg).
		@return Quantity of FFB Receipt Netto-I 
	  */
	public BigDecimal getFFBReceiptNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFBReceiptNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Remaining Netto-I (kg).
		@param FFBRemainingQty FFB Remaining Netto-I (kg)	  */
	public void setFFBRemainingQty (BigDecimal FFBRemainingQty)
	{
		set_Value (COLUMNNAME_FFBRemainingQty, FFBRemainingQty);
	}

	/** Get FFB Remaining Netto-I (kg).
		@return FFB Remaining Netto-I (kg)	  */
	public BigDecimal getFFBRemainingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFBRemainingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Remaining Netto-II (kg).
		@param FFBRemainingQtyII FFB Remaining Netto-II (kg)	  */
	public void setFFBRemainingQtyII (BigDecimal FFBRemainingQtyII)
	{
		set_Value (COLUMNNAME_FFBRemainingQtyII, FFBRemainingQtyII);
	}

	/** Get FFB Remaining Netto-II (kg).
		@return FFB Remaining Netto-II (kg)	  */
	public BigDecimal getFFBRemainingQtyII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFBRemainingQtyII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Flow Meter Qty.
		@param FlowMeterQty 
		Qty Production from Flow Meter
	  */
	public void setFlowMeterQty (BigDecimal FlowMeterQty)
	{
		set_Value (COLUMNNAME_FlowMeterQty, FlowMeterQty);
	}

	/** Get Flow Meter Qty.
		@return Qty Production from Flow Meter
	  */
	public BigDecimal getFlowMeterQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FlowMeterQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Fuel Used (Shift1).
		@param FuelUsed_Shift1 Fuel Used (Shift1)	  */
	public void setFuelUsed_Shift1 (BigDecimal FuelUsed_Shift1)
	{
		set_Value (COLUMNNAME_FuelUsed_Shift1, FuelUsed_Shift1);
	}

	/** Get Fuel Used (Shift1).
		@return Fuel Used (Shift1)	  */
	public BigDecimal getFuelUsed_Shift1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FuelUsed_Shift1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Fuel Used (Shift2).
		@param FuelUsed_Shift2 Fuel Used (Shift2)	  */
	public void setFuelUsed_Shift2 (BigDecimal FuelUsed_Shift2)
	{
		set_Value (COLUMNNAME_FuelUsed_Shift2, FuelUsed_Shift2);
	}

	/** Get Fuel Used (Shift2).
		@return Fuel Used (Shift2)	  */
	public BigDecimal getFuelUsed_Shift2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FuelUsed_Shift2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Get Flow Meter Qty.
		@param GetFlowMeterQty 
		Get Qty Production From Flow Meter
	  */
	public void setGetFlowMeterQty (String GetFlowMeterQty)
	{
		set_Value (COLUMNNAME_GetFlowMeterQty, GetFlowMeterQty);
	}

	/** Get Get Flow Meter Qty.
		@return Get Qty Production From Flow Meter
	  */
	public String getGetFlowMeterQty () 
	{
		return (String)get_Value(COLUMNNAME_GetFlowMeterQty);
	}

	/** Set Height Bulking Silo (cm).
		@param HeightBulkingSilo 
		Height Measured Bulking in Silo
	  */
	public void setHeightBulkingSilo (BigDecimal HeightBulkingSilo)
	{
		set_Value (COLUMNNAME_HeightBulkingSilo, HeightBulkingSilo);
	}

	/** Get Height Bulking Silo (cm).
		@return Height Measured Bulking in Silo
	  */
	public BigDecimal getHeightBulkingSilo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightBulkingSilo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Kernel Silo 1 (cm).
		@param HeightKernelSilo1 
		Height Measured Kernel in Silo 1
	  */
	public void setHeightKernelSilo1 (BigDecimal HeightKernelSilo1)
	{
		set_Value (COLUMNNAME_HeightKernelSilo1, HeightKernelSilo1);
	}

	/** Get Height Kernel Silo 1 (cm).
		@return Height Measured Kernel in Silo 1
	  */
	public BigDecimal getHeightKernelSilo1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightKernelSilo1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Kernel Silo 2 (cm).
		@param HeightKernelSilo2 
		Height Measured Kernel in Silo 2
	  */
	public void setHeightKernelSilo2 (BigDecimal HeightKernelSilo2)
	{
		set_Value (COLUMNNAME_HeightKernelSilo2, HeightKernelSilo2);
	}

	/** Get Height Kernel Silo 2 (cm).
		@return Height Measured Kernel in Silo 2
	  */
	public BigDecimal getHeightKernelSilo2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightKernelSilo2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Kernel Silo 3 (cm).
		@param HeightKernelSilo3 
		Height Measured Kernel in Silo 3
	  */
	public void setHeightKernelSilo3 (BigDecimal HeightKernelSilo3)
	{
		set_Value (COLUMNNAME_HeightKernelSilo3, HeightKernelSilo3);
	}

	/** Get Height Kernel Silo 3 (cm).
		@return Height Measured Kernel in Silo 3
	  */
	public BigDecimal getHeightKernelSilo3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightKernelSilo3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height Nut Silo (cm).
		@param HeightNutSilo 
		Height Measured Nut in Silo
	  */
	public void setHeightNutSilo (BigDecimal HeightNutSilo)
	{
		set_Value (COLUMNNAME_HeightNutSilo, HeightNutSilo);
	}

	/** Get Height Nut Silo (cm).
		@return Height Measured Nut in Silo
	  */
	public BigDecimal getHeightNutSilo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightNutSilo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Manual Sounding.
		@param isManualSounding Manual Sounding	  */
	public void setisManualSounding (boolean isManualSounding)
	{
		set_Value (COLUMNNAME_isManualSounding, Boolean.valueOf(isManualSounding));
	}

	/** Get Manual Sounding.
		@return Manual Sounding	  */
	public boolean isManualSounding () 
	{
		Object oo = get_Value(COLUMNNAME_isManualSounding);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Kernel Delivered Qty (kg).
		@param KernelDeliveredQty Kernel Delivered Qty (kg)	  */
	public void setKernelDeliveredQty (BigDecimal KernelDeliveredQty)
	{
		set_Value (COLUMNNAME_KernelDeliveredQty, KernelDeliveredQty);
	}

	/** Get Kernel Delivered Qty (kg).
		@return Kernel Delivered Qty (kg)	  */
	public BigDecimal getKernelDeliveredQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelDeliveredQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Good Kernel on Floor (kg).
		@param KernelFloor Good Kernel on Floor (kg)	  */
	public void setKernelFloor (BigDecimal KernelFloor)
	{
		set_Value (COLUMNNAME_KernelFloor, KernelFloor);
	}

	/** Get Good Kernel on Floor (kg).
		@return Good Kernel on Floor (kg)	  */
	public BigDecimal getKernelFloor () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelFloor);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Bad Kernel on Floor (kg).
		@param KernelFloorBad Bad Kernel on Floor (kg)	  */
	public void setKernelFloorBad (BigDecimal KernelFloorBad)
	{
		set_Value (COLUMNNAME_KernelFloorBad, KernelFloorBad);
	}

	/** Get Bad Kernel on Floor (kg).
		@return Bad Kernel on Floor (kg)	  */
	public BigDecimal getKernelFloorBad () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelFloorBad);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Kernel Return Qty (kg).
		@param KernelReturnQty Kernel Return Qty (kg)	  */
	public void setKernelReturnQty (BigDecimal KernelReturnQty)
	{
		set_Value (COLUMNNAME_KernelReturnQty, KernelReturnQty);
	}

	/** Get Kernel Return Qty (kg).
		@return Kernel Return Qty (kg)	  */
	public BigDecimal getKernelReturnQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelReturnQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Kernel Silo 1 (kg).
		@param KernelSilo1 Kernel Silo 1 (kg)	  */
	public void setKernelSilo1 (BigDecimal KernelSilo1)
	{
		set_Value (COLUMNNAME_KernelSilo1, KernelSilo1);
	}

	/** Get Kernel Silo 1 (kg).
		@return Kernel Silo 1 (kg)	  */
	public BigDecimal getKernelSilo1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelSilo1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Kernel Silo 2 (kg).
		@param KernelSilo2 Kernel Silo 2 (kg)	  */
	public void setKernelSilo2 (BigDecimal KernelSilo2)
	{
		set_Value (COLUMNNAME_KernelSilo2, KernelSilo2);
	}

	/** Get Kernel Silo 2 (kg).
		@return Kernel Silo 2 (kg)	  */
	public BigDecimal getKernelSilo2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelSilo2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Kernel Silo 3 (kg).
		@param KernelSilo3 Kernel Silo 3 (kg)	  */
	public void setKernelSilo3 (BigDecimal KernelSilo3)
	{
		set_Value (COLUMNNAME_KernelSilo3, KernelSilo3);
	}

	/** Get Kernel Silo 3 (kg).
		@return Kernel Silo 3 (kg)	  */
	public BigDecimal getKernelSilo3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KernelSilo3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set KER Netto-I.
		@param KERNettoI 
		Kernel Extraction Rate of Netto-I
	  */
	public void setKERNettoI (BigDecimal KERNettoI)
	{
		set_Value (COLUMNNAME_KERNettoI, KERNettoI);
	}

	/** Get KER Netto-I.
		@return Kernel Extraction Rate of Netto-I
	  */
	public BigDecimal getKERNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KERNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set KER Netto-II.
		@param KERNettoII 
		Kernel Extraction Rate of Netto-II
	  */
	public void setKERNettoII (BigDecimal KERNettoII)
	{
		set_Value (COLUMNNAME_KERNettoII, KERNettoII);
	}

	/** Get KER Netto-II.
		@return Kernel Extraction Rate of Netto-II
	  */
	public BigDecimal getKERNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_KERNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Last Sounding Quantity.
		@param LastSoundingQty Last Sounding Quantity	  */
	public void setLastSoundingQty (BigDecimal LastSoundingQty)
	{
		set_Value (COLUMNNAME_LastSoundingQty, LastSoundingQty);
	}

	/** Get Last Sounding Quantity.
		@return Last Sounding Quantity	  */
	public BigDecimal getLastSoundingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LastSoundingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Inventory getM_Inventory() throws RuntimeException
    {
		return (org.compiere.model.I_M_Inventory)MTable.get(getCtx(), org.compiere.model.I_M_Inventory.Table_Name)
			.getPO(getM_Inventory_ID(), get_TrxName());	}

	/** Set Phys.Inventory.
		@param M_Inventory_ID 
		Parameters for a Physical Inventory
	  */
	public void setM_Inventory_ID (int M_Inventory_ID)
	{
		if (M_Inventory_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Inventory_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Inventory_ID, Integer.valueOf(M_Inventory_ID));
	}

	/** Get Phys.Inventory.
		@return Parameters for a Physical Inventory
	  */
	public int getM_Inventory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Inventory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Movement getM_Movement() throws RuntimeException
    {
		return (org.compiere.model.I_M_Movement)MTable.get(getCtx(), org.compiere.model.I_M_Movement.Table_Name)
			.getPO(getM_Movement_ID(), get_TrxName());	}

	/** Set Inventory Move.
		@param M_Movement_ID 
		Movement of Inventory
	  */
	public void setM_Movement_ID (int M_Movement_ID)
	{
		if (M_Movement_ID < 1) 
			set_Value (COLUMNNAME_M_Movement_ID, null);
		else 
			set_Value (COLUMNNAME_M_Movement_ID, Integer.valueOf(M_Movement_ID));
	}

	/** Get Inventory Move.
		@return Movement of Inventory
	  */
	public int getM_Movement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Movement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Moist Kernel.
		@param MoistKernel 
		How Much Kernel with Moist Quality
	  */
	public void setMoistKernel (BigDecimal MoistKernel)
	{
		set_Value (COLUMNNAME_MoistKernel, MoistKernel);
	}

	/** Get Moist Kernel.
		@return How Much Kernel with Moist Quality
	  */
	public BigDecimal getMoistKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MoistKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Moist Kernel On Bunker.
		@param MoistKernelOnBunker 
		How Much Kernel On Bunker with Moist Quality
	  */
	public void setMoistKernelOnBunker (BigDecimal MoistKernelOnBunker)
	{
		set_Value (COLUMNNAME_MoistKernelOnBunker, MoistKernelOnBunker);
	}

	/** Get Moist Kernel On Bunker.
		@return How Much Kernel On Bunker with Moist Quality
	  */
	public BigDecimal getMoistKernelOnBunker () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MoistKernelOnBunker);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Nut on Floor (kg).
		@param NutFloor Nut on Floor (kg)	  */
	public void setNutFloor (BigDecimal NutFloor)
	{
		set_Value (COLUMNNAME_NutFloor, NutFloor);
	}

	/** Get Nut on Floor (kg).
		@return Nut on Floor (kg)	  */
	public BigDecimal getNutFloor () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NutFloor);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Nut Percentage (%).
		@param NutPercentage Nut Percentage (%)	  */
	public void setNutPercentage (BigDecimal NutPercentage)
	{
		set_Value (COLUMNNAME_NutPercentage, NutPercentage);
	}

	/** Get Nut Percentage (%).
		@return Nut Percentage (%)	  */
	public BigDecimal getNutPercentage () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NutPercentage);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Nut Silo (kg).
		@param NutSilo Nut Silo (kg)	  */
	public void setNutSilo (BigDecimal NutSilo)
	{
		set_Value (COLUMNNAME_NutSilo, NutSilo);
	}

	/** Get Nut Silo (kg).
		@return Nut Silo (kg)	  */
	public BigDecimal getNutSilo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NutSilo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set OER Netto-I.
		@param OERNettoI 
		Oil Ekstraction Rate of Netto-I
	  */
	public void setOERNettoI (BigDecimal OERNettoI)
	{
		set_Value (COLUMNNAME_OERNettoI, OERNettoI);
	}

	/** Get OER Netto-I.
		@return Oil Ekstraction Rate of Netto-I
	  */
	public BigDecimal getOERNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OERNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set OER Netto-II.
		@param OERNettoII 
		Oil Extraction Rate Netto-II
	  */
	public void setOERNettoII (BigDecimal OERNettoII)
	{
		set_Value (COLUMNNAME_OERNettoII, OERNettoII);
	}

	/** Get OER Netto-II.
		@return Oil Extraction Rate Netto-II
	  */
	public BigDecimal getOERNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OERNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Previous Netto-I (kg).
		@param PreviousFFBQty FFB Previous Netto-I (kg)	  */
	public void setPreviousFFBQty (BigDecimal PreviousFFBQty)
	{
		set_Value (COLUMNNAME_PreviousFFBQty, PreviousFFBQty);
	}

	/** Get FFB Previous Netto-I (kg).
		@return FFB Previous Netto-I (kg)	  */
	public BigDecimal getPreviousFFBQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PreviousFFBQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set FFB Previous Netto-II (kg).
		@param PreviousFFBQtyII FFB Previous Netto-II (kg)	  */
	public void setPreviousFFBQtyII (BigDecimal PreviousFFBQtyII)
	{
		set_Value (COLUMNNAME_PreviousFFBQtyII, PreviousFFBQtyII);
	}

	/** Get FFB Previous Netto-II (kg).
		@return FFB Previous Netto-II (kg)	  */
	public BigDecimal getPreviousFFBQtyII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PreviousFFBQtyII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Kernel Previous Qty (kg).
		@param PreviousKernelQty Kernel Previous Qty (kg)	  */
	public void setPreviousKernelQty (BigDecimal PreviousKernelQty)
	{
		set_Value (COLUMNNAME_PreviousKernelQty, PreviousKernelQty);
	}

	/** Get Kernel Previous Qty (kg).
		@return Kernel Previous Qty (kg)	  */
	public BigDecimal getPreviousKernelQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PreviousKernelQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Production Report.
		@param printProduction Production Report	  */
	public void setprintProduction (String printProduction)
	{
		set_Value (COLUMNNAME_printProduction, printProduction);
	}

	/** Get Production Report.
		@return Production Report	  */
	public String getprintProduction () 
	{
		return (String)get_Value(COLUMNNAME_printProduction);
	}

	/** Set Print Produksi Harian.
		@param PrintSounding Print Produksi Harian	  */
	public void setPrintSounding (String PrintSounding)
	{
		set_Value (COLUMNNAME_PrintSounding, PrintSounding);
	}

	/** Get Print Produksi Harian.
		@return Print Produksi Harian	  */
	public String getPrintSounding () 
	{
		return (String)get_Value(COLUMNNAME_PrintSounding);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Production Cangkang (kg).
		@param ProductionCangkang Production Cangkang (kg)	  */
	public void setProductionCangkang (BigDecimal ProductionCangkang)
	{
		set_Value (COLUMNNAME_ProductionCangkang, ProductionCangkang);
	}

	/** Get Production Cangkang (kg).
		@return Production Cangkang (kg)	  */
	public BigDecimal getProductionCangkang () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProductionCangkang);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Production Kernel (kg).
		@param ProductionKernel Production Kernel (kg)	  */
	public void setProductionKernel (BigDecimal ProductionKernel)
	{
		set_Value (COLUMNNAME_ProductionKernel, ProductionKernel);
	}

	/** Get Production Kernel (kg).
		@return Production Kernel (kg)	  */
	public BigDecimal getProductionKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProductionKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Kernel Silo 1 (kg).
		@param Real_KernelSilo1 Real Kernel Silo 1 (kg)	  */
	public void setReal_KernelSilo1 (BigDecimal Real_KernelSilo1)
	{
		set_Value (COLUMNNAME_Real_KernelSilo1, Real_KernelSilo1);
	}

	/** Get Real Kernel Silo 1 (kg).
		@return Real Kernel Silo 1 (kg)	  */
	public BigDecimal getReal_KernelSilo1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_KernelSilo1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Kernel Silo 2 (kg).
		@param Real_KernelSilo2 Real Kernel Silo 2 (kg)	  */
	public void setReal_KernelSilo2 (BigDecimal Real_KernelSilo2)
	{
		set_Value (COLUMNNAME_Real_KernelSilo2, Real_KernelSilo2);
	}

	/** Get Real Kernel Silo 2 (kg).
		@return Real Kernel Silo 2 (kg)	  */
	public BigDecimal getReal_KernelSilo2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_KernelSilo2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Kernel Silo 3 (kg).
		@param Real_KernelSilo3 Real Kernel Silo 3 (kg)	  */
	public void setReal_KernelSilo3 (BigDecimal Real_KernelSilo3)
	{
		set_Value (COLUMNNAME_Real_KernelSilo3, Real_KernelSilo3);
	}

	/** Get Real Kernel Silo 3 (kg).
		@return Real Kernel Silo 3 (kg)	  */
	public BigDecimal getReal_KernelSilo3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_KernelSilo3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real KER Netto-I.
		@param Real_KERNettoI 
		Real Kernel Extraction Rate of Netto-I
	  */
	public void setReal_KERNettoI (BigDecimal Real_KERNettoI)
	{
		set_Value (COLUMNNAME_Real_KERNettoI, Real_KERNettoI);
	}

	/** Get Real KER Netto-I.
		@return Real Kernel Extraction Rate of Netto-I
	  */
	public BigDecimal getReal_KERNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_KERNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real KER Netto-II.
		@param Real_KERNettoII 
		Real Kernel Extraction Rate of Netto-II
	  */
	public void setReal_KERNettoII (BigDecimal Real_KERNettoII)
	{
		set_Value (COLUMNNAME_Real_KERNettoII, Real_KERNettoII);
	}

	/** Get Real KER Netto-II.
		@return Real Kernel Extraction Rate of Netto-II
	  */
	public BigDecimal getReal_KERNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_KERNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Nut on Floor (kg).
		@param Real_NutFloor Real Nut on Floor (kg)	  */
	public void setReal_NutFloor (BigDecimal Real_NutFloor)
	{
		set_Value (COLUMNNAME_Real_NutFloor, Real_NutFloor);
	}

	/** Get Real Nut on Floor (kg).
		@return Real Nut on Floor (kg)	  */
	public BigDecimal getReal_NutFloor () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_NutFloor);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Nut Silo (kg).
		@param Real_NutSilo Real Nut Silo (kg)	  */
	public void setReal_NutSilo (BigDecimal Real_NutSilo)
	{
		set_Value (COLUMNNAME_Real_NutSilo, Real_NutSilo);
	}

	/** Get Real Nut Silo (kg).
		@return Real Nut Silo (kg)	  */
	public BigDecimal getReal_NutSilo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_NutSilo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Kernel Previous Qty (kg).
		@param Real_PreviousKernelQty Real Kernel Previous Qty (kg)	  */
	public void setReal_PreviousKernelQty (BigDecimal Real_PreviousKernelQty)
	{
		set_Value (COLUMNNAME_Real_PreviousKernelQty, Real_PreviousKernelQty);
	}

	/** Get Real Kernel Previous Qty (kg).
		@return Real Kernel Previous Qty (kg)	  */
	public BigDecimal getReal_PreviousKernelQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_PreviousKernelQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Production Kernel (kg).
		@param Real_ProductionKernel Real Production Kernel (kg)	  */
	public void setReal_ProductionKernel (BigDecimal Real_ProductionKernel)
	{
		set_Value (COLUMNNAME_Real_ProductionKernel, Real_ProductionKernel);
	}

	/** Get Real Production Kernel (kg).
		@return Real Production Kernel (kg)	  */
	public BigDecimal getReal_ProductionKernel () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_ProductionKernel);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Real Stock Kernel Today (kg).
		@param Real_StockKernelToday Real Stock Kernel Today (kg)	  */
	public void setReal_StockKernelToday (BigDecimal Real_StockKernelToday)
	{
		set_Value (COLUMNNAME_Real_StockKernelToday, Real_StockKernelToday);
	}

	/** Get Real Stock Kernel Today (kg).
		@return Real Stock Kernel Today (kg)	  */
	public BigDecimal getReal_StockKernelToday () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Real_StockKernelToday);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Restant on Stew (kg).
		@param RestantStew Restant on Stew (kg)	  */
	public void setRestantStew (BigDecimal RestantStew)
	{
		set_Value (COLUMNNAME_RestantStew, RestantStew);
	}

	/** Get Restant on Stew (kg).
		@return Restant on Stew (kg)	  */
	public BigDecimal getRestantStew () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RestantStew);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Fibre Return Qty.
		@param ReturnFibreQty Fibre Return Qty	  */
	public void setReturnFibreQty (BigDecimal ReturnFibreQty)
	{
		set_Value (COLUMNNAME_ReturnFibreQty, ReturnFibreQty);
	}

	/** Get Fibre Return Qty.
		@return Fibre Return Qty	  */
	public BigDecimal getReturnFibreQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnFibreQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Shell Return Qty.
		@param ReturnShellQty Shell Return Qty	  */
	public void setReturnShellQty (BigDecimal ReturnShellQty)
	{
		set_Value (COLUMNNAME_ReturnShellQty, ReturnShellQty);
	}

	/** Get Shell Return Qty.
		@return Shell Return Qty	  */
	public BigDecimal getReturnShellQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnShellQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sludge Oil Return Qty.
		@param ReturnSludgeOilQty Sludge Oil Return Qty	  */
	public void setReturnSludgeOilQty (BigDecimal ReturnSludgeOilQty)
	{
		set_Value (COLUMNNAME_ReturnSludgeOilQty, ReturnSludgeOilQty);
	}

	/** Get Sludge Oil Return Qty.
		@return Sludge Oil Return Qty	  */
	public BigDecimal getReturnSludgeOilQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnSludgeOilQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Solid Decanter Return Qty.
		@param ReturnSolidDecanterQty Solid Decanter Return Qty	  */
	public void setReturnSolidDecanterQty (BigDecimal ReturnSolidDecanterQty)
	{
		set_Value (COLUMNNAME_ReturnSolidDecanterQty, ReturnSolidDecanterQty);
	}

	/** Get Solid Decanter Return Qty.
		@return Solid Decanter Return Qty	  */
	public BigDecimal getReturnSolidDecanterQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnSolidDecanterQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Start Time.
		@param StartTime 
		Time started
	  */
	public void setStartTime (Timestamp StartTime)
	{
		set_Value (COLUMNNAME_StartTime, StartTime);
	}

	/** Get Start Time.
		@return Time started
	  */
	public Timestamp getStartTime () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartTime);
	}

	/** Set Sterilizer.
		@param Sterilizer Sterilizer	  */
	public void setSterilizer (BigDecimal Sterilizer)
	{
		set_Value (COLUMNNAME_Sterilizer, Sterilizer);
	}

	/** Get Sterilizer.
		@return Sterilizer	  */
	public BigDecimal getSterilizer () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Sterilizer);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Stew Capacity (kg).
		@param StewCapacity Stew Capacity (kg)	  */
	public void setStewCapacity (BigDecimal StewCapacity)
	{
		set_Value (COLUMNNAME_StewCapacity, StewCapacity);
	}

	/** Get Stew Capacity (kg).
		@return Stew Capacity (kg)	  */
	public BigDecimal getStewCapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StewCapacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Stock Fibre (kg).
		@param StockFibre 
		Stock Fibre/Empty Bunch Today on Kg
	  */
	public void setStockFibre (BigDecimal StockFibre)
	{
		set_Value (COLUMNNAME_StockFibre, StockFibre);
	}

	/** Get Stock Fibre (kg).
		@return Stock Fibre/Empty Bunch Today on Kg
	  */
	public BigDecimal getStockFibre () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StockFibre);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Stock Kernel Today (kg).
		@param StockKernelToday Stock Kernel Today (kg)	  */
	public void setStockKernelToday (BigDecimal StockKernelToday)
	{
		set_Value (COLUMNNAME_StockKernelToday, StockKernelToday);
	}

	/** Get Stock Kernel Today (kg).
		@return Stock Kernel Today (kg)	  */
	public BigDecimal getStockKernelToday () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StockKernelToday);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Stock Shell (kg).
		@param StockShell 
		Stock Shell Today on Kg
	  */
	public void setStockShell (BigDecimal StockShell)
	{
		set_Value (COLUMNNAME_StockShell, StockShell);
	}

	/** Get Stock Shell (kg).
		@return Stock Shell Today on Kg
	  */
	public BigDecimal getStockShell () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StockShell);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Stock Sludge Oil (kg).
		@param StockSludgeOil 
		Stock Sludge Oil Today on Kg
	  */
	public void setStockSludgeOil (BigDecimal StockSludgeOil)
	{
		set_Value (COLUMNNAME_StockSludgeOil, StockSludgeOil);
	}

	/** Get Stock Sludge Oil (kg).
		@return Stock Sludge Oil Today on Kg
	  */
	public BigDecimal getStockSludgeOil () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StockSludgeOil);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Stock Solid Decater (kg).
		@param StockSolidDecanter 
		Stock Solid Decanter Today on Kg
	  */
	public void setStockSolidDecanter (BigDecimal StockSolidDecanter)
	{
		set_Value (COLUMNNAME_StockSolidDecanter, StockSolidDecanter);
	}

	/** Get Stock Solid Decater (kg).
		@return Stock Solid Decanter Today on Kg
	  */
	public BigDecimal getStockSolidDecanter () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StockSolidDecanter);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Time Start Number.
		@param TimeInNumber 
		Must 4 Number for parsing to date Time In
	  */
	public void setTimeInNumber (String TimeInNumber)
	{
		set_Value (COLUMNNAME_TimeInNumber, TimeInNumber);
	}

	/** Get Time Start Number.
		@return Must 4 Number for parsing to date Time In
	  */
	public String getTimeInNumber () 
	{
		return (String)get_Value(COLUMNNAME_TimeInNumber);
	}

	/** Set Time End Number.
		@param TimeOutNumber 
		Must 4 Number for parsing to date Time Out
	  */
	public void setTimeOutNumber (String TimeOutNumber)
	{
		set_Value (COLUMNNAME_TimeOutNumber, TimeOutNumber);
	}

	/** Get Time End Number.
		@return Must 4 Number for parsing to date Time Out
	  */
	public String getTimeOutNumber () 
	{
		return (String)get_Value(COLUMNNAME_TimeOutNumber);
	}

	public com.uns.model.I_UNS_Sounding getUNS_PrevSounding() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Sounding)MTable.get(getCtx(), com.uns.model.I_UNS_Sounding.Table_Name)
			.getPO(getUNS_PrevSounding_ID(), get_TrxName());	}

	/** Set Prev Sounding.
		@param UNS_PrevSounding_ID Prev Sounding	  */
	public void setUNS_PrevSounding_ID (int UNS_PrevSounding_ID)
	{
		if (UNS_PrevSounding_ID < 1) 
			set_Value (COLUMNNAME_UNS_PrevSounding_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_PrevSounding_ID, Integer.valueOf(UNS_PrevSounding_ID));
	}

	/** Get Prev Sounding.
		@return Prev Sounding	  */
	public int getUNS_PrevSounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PrevSounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Production getUNS_Production() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Production)MTable.get(getCtx(), com.uns.model.I_UNS_Production.Table_Name)
			.getPO(getUNS_Production_ID(), get_TrxName());	}

	/** Set Production.
		@param UNS_Production_ID Production	  */
	public void setUNS_Production_ID (int UNS_Production_ID)
	{
		if (UNS_Production_ID < 1) 
			set_Value (COLUMNNAME_UNS_Production_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Production_ID, Integer.valueOf(UNS_Production_ID));
	}

	/** Get Production.
		@return Production	  */
	public int getUNS_Production_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Resource getUNS_Resource() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Resource)MTable.get(getCtx(), com.uns.model.I_UNS_Resource.Table_Name)
			.getPO(getUNS_Resource_ID(), get_TrxName());	}

	/** Set Manufacture Resource.
		@param UNS_Resource_ID Manufacture Resource	  */
	public void setUNS_Resource_ID (int UNS_Resource_ID)
	{
		if (UNS_Resource_ID < 1) 
			set_Value (COLUMNNAME_UNS_Resource_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Resource_ID, Integer.valueOf(UNS_Resource_ID));
	}

	/** Get Manufacture Resource.
		@return Manufacture Resource	  */
	public int getUNS_Resource_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Resource_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Sounding.
		@param UNS_Sounding_ID Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID)
	{
		if (UNS_Sounding_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Sounding_ID, Integer.valueOf(UNS_Sounding_ID));
	}

	/** Get Sounding.
		@return Sounding	  */
	public int getUNS_Sounding_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Sounding_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Sounding_UU.
		@param UNS_Sounding_UU UNS_Sounding_UU	  */
	public void setUNS_Sounding_UU (String UNS_Sounding_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Sounding_UU, UNS_Sounding_UU);
	}

	/** Get UNS_Sounding_UU.
		@return UNS_Sounding_UU	  */
	public String getUNS_Sounding_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Sounding_UU);
	}
}