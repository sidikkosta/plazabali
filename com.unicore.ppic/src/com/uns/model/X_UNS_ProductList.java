/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_ProductList
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ProductList extends PO implements I_UNS_ProductList, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190903L;

    /** Standard Constructor */
    public X_UNS_ProductList (Properties ctx, int UNS_ProductList_ID, String trxName)
    {
      super (ctx, UNS_ProductList_ID, trxName);
      /** if (UNS_ProductList_ID == 0)
        {
			setETBBCode (null);
			setIDRAmount (Env.ZERO);
// 0
			setIsManual (true);
// Y
			setPriceLabelType (null);
			setPrintedQty (Env.ZERO);
// 0
			setQty (Env.ZERO);
// 0
			setSKU (null);
			setUNS_ProductList_ID (0);
			setUSDAmount (Env.ZERO);
// 0
        } */
    }

    /** Load Constructor */
    public X_UNS_ProductList (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ProductList[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Category Code.
		@param CategoryCode Category Code	  */
	public void setCategoryCode (String CategoryCode)
	{
		set_Value (COLUMNNAME_CategoryCode, CategoryCode);
	}

	/** Get Category Code.
		@return Category Code	  */
	public String getCategoryCode () 
	{
		return (String)get_Value(COLUMNNAME_CategoryCode);
	}

	/** Set Date Code.
		@param DateCode Date Code	  */
	public void setDateCode (String DateCode)
	{
		set_Value (COLUMNNAME_DateCode, DateCode);
	}

	/** Get Date Code.
		@return Date Code	  */
	public String getDateCode () 
	{
		return (String)get_Value(COLUMNNAME_DateCode);
	}

	/** Set Date printed.
		@param DatePrinted 
		Date the document was printed.
	  */
	public void setDatePrinted (Timestamp DatePrinted)
	{
		set_Value (COLUMNNAME_DatePrinted, DatePrinted);
	}

	/** Get Date printed.
		@return Date the document was printed.
	  */
	public Timestamp getDatePrinted () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DatePrinted);
	}

	/** Set ETBB Code.
		@param ETBBCode ETBB Code	  */
	public void setETBBCode (String ETBBCode)
	{
		set_Value (COLUMNNAME_ETBBCode, ETBBCode);
	}

	/** Get ETBB Code.
		@return ETBB Code	  */
	public String getETBBCode () 
	{
		return (String)get_Value(COLUMNNAME_ETBBCode);
	}

	/** Set IDR Amount.
		@param IDRAmount IDR Amount	  */
	public void setIDRAmount (BigDecimal IDRAmount)
	{
		set_Value (COLUMNNAME_IDRAmount, IDRAmount);
	}

	/** Get IDR Amount.
		@return IDR Amount	  */
	public BigDecimal getIDRAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_IDRAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Manual.
		@param IsManual 
		This is a manual process
	  */
	public void setIsManual (boolean IsManual)
	{
		set_Value (COLUMNNAME_IsManual, Boolean.valueOf(IsManual));
	}

	/** Get Manual.
		@return This is a manual process
	  */
	public boolean isManual () 
	{
		Object oo = get_Value(COLUMNNAME_IsManual);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Sticker = S */
	public static final String PRICELABELTYPE_Sticker = "S";
	/** Hanging = H */
	public static final String PRICELABELTYPE_Hanging = "H";
	/** Set Price Label Type.
		@param PriceLabelType Price Label Type	  */
	public void setPriceLabelType (String PriceLabelType)
	{

		set_Value (COLUMNNAME_PriceLabelType, PriceLabelType);
	}

	/** Get Price Label Type.
		@return Price Label Type	  */
	public String getPriceLabelType () 
	{
		return (String)get_Value(COLUMNNAME_PriceLabelType);
	}

	/** Set Printed Qty.
		@param PrintedQty Printed Qty	  */
	public void setPrintedQty (BigDecimal PrintedQty)
	{
		set_Value (COLUMNNAME_PrintedQty, PrintedQty);
	}

	/** Get Printed Qty.
		@return Printed Qty	  */
	public BigDecimal getPrintedQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PrintedQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Product Name.
		@param ProductName 
		Name of the Product
	  */
	public void setProductName (String ProductName)
	{
		set_Value (COLUMNNAME_ProductName, ProductName);
	}

	/** Get Product Name.
		@return Name of the Product
	  */
	public String getProductName () 
	{
		return (String)get_Value(COLUMNNAME_ProductName);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getProductName());
    }

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set SKU.
		@param SKU 
		Stock Keeping Unit
	  */
	public void setSKU (String SKU)
	{
		set_ValueNoCheck (COLUMNNAME_SKU, SKU);
	}

	/** Get SKU.
		@return Stock Keeping Unit
	  */
	public String getSKU () 
	{
		return (String)get_Value(COLUMNNAME_SKU);
	}

	public com.uns.model.I_UNS_PriceLabelPrint getUNS_PriceLabelPrint() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PriceLabelPrint)MTable.get(getCtx(), com.uns.model.I_UNS_PriceLabelPrint.Table_Name)
			.getPO(getUNS_PriceLabelPrint_ID(), get_TrxName());	}

	/** Set Price Label Printing.
		@param UNS_PriceLabelPrint_ID Price Label Printing	  */
	public void setUNS_PriceLabelPrint_ID (int UNS_PriceLabelPrint_ID)
	{
		if (UNS_PriceLabelPrint_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrint_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrint_ID, Integer.valueOf(UNS_PriceLabelPrint_ID));
	}

	/** Get Price Label Printing.
		@return Price Label Printing	  */
	public int getUNS_PriceLabelPrint_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PriceLabelPrint_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Product List Detail.
		@param UNS_ProductList_ID Product List Detail	  */
	public void setUNS_ProductList_ID (int UNS_ProductList_ID)
	{
		if (UNS_ProductList_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ProductList_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ProductList_ID, Integer.valueOf(UNS_ProductList_ID));
	}

	/** Get Product List Detail.
		@return Product List Detail	  */
	public int getUNS_ProductList_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ProductList_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ProductList_UU.
		@param UNS_ProductList_UU UNS_ProductList_UU	  */
	public void setUNS_ProductList_UU (String UNS_ProductList_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ProductList_UU, UNS_ProductList_UU);
	}

	/** Get UNS_ProductList_UU.
		@return UNS_ProductList_UU	  */
	public String getUNS_ProductList_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ProductList_UU);
	}

	/** Set USD Amount.
		@param USDAmount USD Amount	  */
	public void setUSDAmount (BigDecimal USDAmount)
	{
		set_Value (COLUMNNAME_USDAmount, USDAmount);
	}

	/** Get USD Amount.
		@return USD Amount	  */
	public BigDecimal getUSDAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_USDAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}