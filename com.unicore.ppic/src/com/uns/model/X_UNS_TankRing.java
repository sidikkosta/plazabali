/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_TankRing
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_TankRing extends PO implements I_UNS_TankRing, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20161124L;

    /** Standard Constructor */
    public X_UNS_TankRing (Properties ctx, int UNS_TankRing_ID, String trxName)
    {
      super (ctx, UNS_TankRing_ID, trxName);
      /** if (UNS_TankRing_ID == 0)
        {
			setHeight (Env.ZERO);
			setHeightFrom (Env.ZERO);
			setHeightTo (Env.ZERO);
			setLineNo (0);
			setName (null);
			setThickness (Env.ZERO);
			setUNS_TankRing_ID (0);
			setUNS_TankRing_UU (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_TankRing (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_TankRing[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Height.
		@param Height Height	  */
	public void setHeight (BigDecimal Height)
	{
		set_Value (COLUMNNAME_Height, Height);
	}

	/** Get Height.
		@return Height	  */
	public BigDecimal getHeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Height);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height From.
		@param HeightFrom Height From	  */
	public void setHeightFrom (BigDecimal HeightFrom)
	{
		set_Value (COLUMNNAME_HeightFrom, HeightFrom);
	}

	/** Get Height From.
		@return Height From	  */
	public BigDecimal getHeightFrom () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightFrom);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Height To.
		@param HeightTo Height To	  */
	public void setHeightTo (BigDecimal HeightTo)
	{
		set_Value (COLUMNNAME_HeightTo, HeightTo);
	}

	/** Get Height To.
		@return Height To	  */
	public BigDecimal getHeightTo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightTo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Line.
		@param LineNo 
		Line No
	  */
	public void setLineNo (int LineNo)
	{
		set_Value (COLUMNNAME_LineNo, Integer.valueOf(LineNo));
	}

	/** Get Line.
		@return Line No
	  */
	public int getLineNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_LineNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Thickness.
		@param Thickness Thickness	  */
	public void setThickness (BigDecimal Thickness)
	{
		set_Value (COLUMNNAME_Thickness, Thickness);
	}

	/** Get Thickness.
		@return Thickness	  */
	public BigDecimal getThickness () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Thickness);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Tank getUNS_Tank() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Tank)MTable.get(getCtx(), com.uns.model.I_UNS_Tank.Table_Name)
			.getPO(getUNS_Tank_ID(), get_TrxName());	}

	/** Set Tank.
		@param UNS_Tank_ID Tank	  */
	public void setUNS_Tank_ID (int UNS_Tank_ID)
	{
		if (UNS_Tank_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, Integer.valueOf(UNS_Tank_ID));
	}

	/** Get Tank.
		@return Tank	  */
	public int getUNS_Tank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Tank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tank Ring.
		@param UNS_TankRing_ID Tank Ring	  */
	public void setUNS_TankRing_ID (int UNS_TankRing_ID)
	{
		if (UNS_TankRing_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_TankRing_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_TankRing_ID, Integer.valueOf(UNS_TankRing_ID));
	}

	/** Get Tank Ring.
		@return Tank Ring	  */
	public int getUNS_TankRing_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_TankRing_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_TankRing_UU.
		@param UNS_TankRing_UU UNS_TankRing_UU	  */
	public void setUNS_TankRing_UU (String UNS_TankRing_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_TankRing_UU, UNS_TankRing_UU);
	}

	/** Get UNS_TankRing_UU.
		@return UNS_TankRing_UU	  */
	public String getUNS_TankRing_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_TankRing_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}