/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPrice;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.model.MUNSImportSimpleColumn;
import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;

/**
 * @author Burhani Adam
 *
 */
public class ImportProductPrice extends SvrProcess {

	/**
	 * 
	 */
	public ImportProductPrice() {
		// TODO Auto-generated constructor stub
	}

	private String whereClause = null;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method 
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MPriceListVersion v = new MPriceListVersion(getCtx(), getRecord_ID(), get_TrxName());
		
		if(Util.isEmpty(v.getFile_Directory(), true))
			throw new AdempiereException("To run this process, you must fill the file with the correct directory / path.");
		
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportProductPrice'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with ImportProductPrice name");
		
		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(v.getFile_Directory());
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length > 1)
			throw new AdempiereException("more than one lines in Import Simple XLS " + simple.getName());
		
		MUNSImportSimpleTable table = tables[0];
		MUNSImportSimpleColumn[] columns = table.getLines(true);
		MUNSImportSimpleColumn column = null;
		for(int i=0;i<columns.length;i++)
		{
			if(columns[i].getAD_Column().getColumnName().equals("M_PriceList_Version_ID"))
			{
				column = columns[i];
				break;
			}
		}
		
		if(column == null)
		{
			column = new MUNSImportSimpleColumn(getCtx(), 0, get_TrxName());
			column.setUNS_ImportSimpleTable_ID(table.get_ID());
			column.setName("UNT");
			column.setAD_Column_ID(MColumn.getColumn_ID(MProductPrice.Table_Name, "M_PriceList_Version_ID"));
			column.setColumnNo(6969);
			column.setAD_Reference_ID(11);
			column.setIsEmptyCell(true);
		}
		
		column.setDefaultValue(String.valueOf(v.get_ID()));
		column.saveEx();
		table.getLines(true);
		
		whereClause = "M_Product_ID = @A@";
		table.setWhereClause(whereClause + " AND M_PriceList_Version_ID = " + v.get_ID());
		table.saveEx();
		
		SimpleImportXLS si = new SimpleImportXLS(getCtx(), simple, table, false, 0, 0, 0, getAD_Client_ID(), get_TrxName());;
		String success = null;
		try{
			 success = si.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		
		column.deleteEx(true);
		
		return success;
	}
}