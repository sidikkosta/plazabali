/**
 * 
 */
package com.uns.model.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSPriceLabelPrint;
import com.uns.model.MUNSPriceLabelPrinterConf;
import com.uns.model.MUNSPriceLabelTemplate;
import com.uns.model.MUNSPrintLabelTemplate;
import com.uns.model.MUNSProductList;

/**
 * @author Burhani Adam
 *
 */
public class PrintPriceLabel extends SvrProcess {

	/**
	 * 
	 */
	public PrintPriceLabel() {
		// TODO Auto-generated constructor stub
	}
	
	private	int		m_printerID		=	0;
	private	String	m_shopType		=	null;
	private int		m_productListID =	0;
	private int		m_Qty			=	0;
	private String		m_LabelType		=	null;
	private Hashtable<String, String> mapFile = new Hashtable<>();
	private Properties m_ctx = null;
	private String m_trxName = null;
	private MUNSPriceLabelPrint record = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				continue;
			else if(param.getParameterName().equals("UNS_PriceLabelPrinterConf_ID"))
				m_printerID = param.getParameterAsInt();
			else if(param.getParameterName().equals("ShopType"))
				m_shopType = param.getParameterAsString();
			else if(param.getParameterName().equals("UNS_ProductList_ID"))
				m_productListID = param.getParameterAsInt();
			else if(param.getParameterName().equals("Qty"))
				m_Qty = param.getParameterAsInt();
			else if(param.getParameterName().equals("PriceLabelType"))
				m_LabelType = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
		
		m_ctx = getCtx();
		m_trxName = get_TrxName();
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window Price Label Print.");
		record = new MUNSPriceLabelPrint(m_ctx, getRecord_ID(), m_trxName);
	}
	
	public PrintPriceLabel(MUNSPriceLabelPrint po, int printerID, String shopType, String labelType
			, int productListID, int qtyPrint)
	{
		m_ctx = po.getCtx();
		m_trxName = po.get_TrxName();
		m_LabelType = labelType;
		m_printerID = printerID;
		m_shopType = shopType;
		m_productListID = productListID;
		m_Qty = qtyPrint;
		record = po;
	}
	
	public String run()
	{
		try {
			return doIt();
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		String os = System.getProperty("os.name");
		boolean isWindows = os.startsWith("Windows");
		MUNSPriceLabelPrinterConf conf = new MUNSPriceLabelPrinterConf(m_ctx, m_printerID, m_trxName);
		if(isWindows)
		{
			Runtime.getRuntime().exec("net use lpt1: /d");
			Runtime.getRuntime().exec("net use lpt1: \\\\" + conf.getIPAddress() + "\\" + conf.getPrinterName());
			Thread.sleep(1000);
		}
		MUNSPrintLabelTemplate[] templates = conf.getTemplates();
//		record = new MUNSPriceLabelPrint(m_ctx, getRecord_ID(), m_trxName);
		String labelName = record.getUNS_LabelName().getName();
		for(int i=0;i<templates.length;i++)
		{
			MUNSPriceLabelTemplate template = new MUNSPriceLabelTemplate(m_ctx, templates[i].getUNS_PriceLabelTemplate_ID(),
					m_trxName);
			if(!template.getShopType().equals(m_shopType))
				continue;
			
			if(!template.getPriceLabelType().equals(m_LabelType))
				continue;
			
			MUNSProductList[] list = record.getProducts(template.getPriceLabelType());
			for(int j=0;j<list.length;j++)
			{
				if(m_productListID > 0 && list[j].get_ID() != m_productListID)
					continue;
				boolean isRunRemaining = false;
				if(m_productListID <= 0 || m_Qty == 0)
					m_Qty = list[j].getQty().intValue();
				if(template.getPrinterType().equals(MUNSPriceLabelTemplate.PRINTERTYPE_2_Paper) && (m_Qty % 2) == 1)
				{
					m_Qty = m_Qty - 1;
					isRunRemaining = true;
				}
				Timestamp now = new Timestamp(System.currentTimeMillis());
				String nowString = now.toString().replace("-", "");
				nowString = nowString.substring(0, 8);
				String templateTxt = template.getCompleteTemplate(list[j], m_Qty, labelName);
				try {
					String newFName = "/root/Documents/" + nowString 
							+ "_PrintLabel" + "_" + list[j].getSKU() + "_" + list[j].getETBBCode() + "_1.txt";
					File newFile = new File(newFName);
					
					FileWriter fr = new FileWriter(newFile);

					BufferedWriter bw = new BufferedWriter(fr);
					
					for (String eachLine : templateTxt.split("\n"))
					{
						bw.write(eachLine);
						bw.newLine();
					}
					bw.close();
					
					mapFile.put(newFName, newFName);
					if(isWindows)
						Runtime.getRuntime().exec("cmd /c type " + newFName + " > lpt1:");
					else
						Runtime.getRuntime().exec("lpr -P " + conf.getPrinterName() + " -r " + newFName);
				} catch (IOException e) {
					throw new AdempiereException(e.getMessage());
				}
				
				if(isRunRemaining)
				{
					templateTxt = template.getCompleteTemplate(list[j], 1, labelName);
					try {
						String newFName = "/root/Documents/printLabel" + j + "2.txt";
						File newFile = new File(newFName);
						
						FileWriter fr = new FileWriter(newFile);

						BufferedWriter bw = new BufferedWriter(fr);
						
						for (String eachLine : templateTxt.split("\n"))
						{
							bw.write(eachLine);
							bw.newLine();
						}
						bw.close();
						mapFile.put(newFName, newFName);
						if(isWindows)
							Runtime.getRuntime().exec("cmd /c type " + newFName + " > lpt1:");
						else
							Runtime.getRuntime().exec("lpr -P " + conf.getPrinterName() + " -r " + newFName);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				list[j].setDatePrinted(new Timestamp (System.currentTimeMillis()));
				list[j].saveEx();
			}
		}
		
		if(isWindows)
		{
			Enumeration<String> mapKey = mapFile.keys();
			while (mapKey.hasMoreElements())
			{
				String key = mapKey.nextElement();
				Runtime.getRuntime().exec("cmd /c del " + key);
			}
		}
		
		return null;
	}
}