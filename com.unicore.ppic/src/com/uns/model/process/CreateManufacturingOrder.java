/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MProductBOM;
import org.compiere.model.MStorageOnHand;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.MOrderLine;
import com.uns.model.MUNSManufacturingOrder;
import com.uns.model.MUNSPSSOAllocation;
import com.uns.model.MUNSProductionSchedule;
import com.uns.util.UNSTimeUtil;

/**
 * @author Burhani Adam
 *
 */
public class CreateManufacturingOrder extends SvrProcess {

	private int p_Order_ID = 0;
	private int p_Product_ID = 0;
	private int p_PCategory_ID = 0;
	private int p_OrderLine_ID = 0;
	private int p_Org_ID = 0;
	private boolean p_IgnoreStockOnHand = false;
	private Hashtable<Integer, BigDecimal> m_mapStockOnHand = new Hashtable<>();
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(int i=0;i<params.length;i++)
		{
			String name = params[i].getParameterName();
			if(name.equals("C_Order_ID"))
				p_Order_ID = params[i].getParameterAsInt();
			else if(name.equals("M_Product_ID"))
				p_Product_ID = params[i].getParameterAsInt();
			else if(name.equals("C_OrderLine_ID"))
				p_OrderLine_ID = params[i].getParameterAsInt();
			else if(name.equals("IgnoreStockOnHand"))
				p_IgnoreStockOnHand = params[i].getParameterAsBoolean();
			else if(name.equals("M_Product_Category_ID"))
				p_PCategory_ID = params[i].getParameterAsInt();
			else if(name.equals("AD_Org_ID"))
				p_Org_ID = params[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter : "+name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MOrderLine[] lines = MOrderLine.getUnAllocatedPS(getCtx(), 
				p_Org_ID, p_Order_ID, p_OrderLine_ID, p_Product_ID, p_PCategory_ID, get_TrxName());
		BigDecimal currentStock = Env.ZERO;
		BigDecimal unAllocatedStock = Env.ZERO;
		BigDecimal allocatedStock = Env.ZERO;
		BigDecimal unAllocatedSO = Env.ZERO; 
		
		for(int i=0; i<lines.length; i++)
		{
			unAllocatedSO = lines[i].getQtyOrdered().subtract(lines[i].getQtyDelivered());
			if(!p_IgnoreStockOnHand)
			{
				unAllocatedStock = m_mapStockOnHand.get(lines[i].getM_Product_ID());
				if(allocatedStock == null)
					currentStock = MStorageOnHand.getQtyOnHand(lines[i].getM_Product_ID(), lines[i].getM_Warehouse_ID(),
						0, get_TrxName());
				unAllocatedStock = (allocatedStock == null ? currentStock : allocatedStock).subtract(unAllocatedSO);
				if(unAllocatedStock.compareTo(Env.ONE) == -1)
				{
					unAllocatedSO = unAllocatedStock.abs();
					unAllocatedStock = Env.ZERO;
				}
				else
					continue;
				
				m_mapStockOnHand.put(lines[i].getM_Product_ID(), unAllocatedStock);
			}
				
			for(int j=0; j<1000; j++)
			{
				MUNSProductionSchedule ps = MUNSProductionSchedule.getOfProduct(getCtx(), false, get(lines[i], j), 
						lines[i].getM_Product_ID(), 0, get_TrxName());
				MUNSPSSOAllocation soa = MUNSPSSOAllocation.createSOAllocation(lines[i], ps, getCtx(), get_TrxName());
				if(lines[i].getUnAllocatedQtyOnPS().compareTo(Env.ZERO) <= 0)
				{
					MProductBOM[] boms = getWIPProduct(ps.getUNS_Resource_ID(), ps.getM_Product_ID());
					for(int k=0; k<boms.length; k++)
					{
						MUNSProductionSchedule psWIP = MUNSProductionSchedule.getOfProduct(getCtx(), false, get(lines[i], j), 
								lines[i].getM_Product_ID(), 0, get_TrxName());
						BigDecimal qtyWIP = boms[k].getBOMQty().multiply(soa.getQtyUom());
						psWIP.setQtyOrdered(psWIP.getQtyOrdered().add(qtyWIP));
						psWIP.saveEx();
					}
					break;
				}
			}
		}
		
		return "Success";
	}
	
	private MProductBOM[] getWIPProduct(int UNS_Resource_ID, int M_Product_ID)
	{
		java.util.List<MProductBOM> boms = new ArrayList<>();
		String sql = "SELECT DISTINCT(bom.M_ProductBOM_ID) FROM M_Product p"
				+ " INNER JOIN M_Product_BOM bom ON bom.M_Product_ID = p.M_Product_ID"
				+ " INNER JOIN UNS_Resource_InOut io ON io.M_Product_ID = bom.M_Product_ID"
				+ " INNER JOIN UNS_Resource ws ON ws.UNS_Resource_ID = io.UNS_Resource_ID"
				+ " INNER JOIN UNS_Resource wc ON wc.UNS_Resource_ID = ws.ResourceParent_ID"
				+ " WHERE bom.IsActive = 'Y' AND io.InOutType = 'O' AND ws.ResourceType = 'WS'"
				+ " AND wc.ResourceType = 'WC' AND wc.ResourceParent_ID = ? AND p.M_Product_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, UNS_Resource_ID);
			stmt.setInt(2, M_Product_ID);
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				boms.add(new MProductBOM(getCtx(), rs.getInt(1), get_TrxName()));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		return boms.toArray(new MProductBOM[boms.size()]);
	}
	
	public static void main(String args[])
	{
		Calendar cal = Calendar.getInstance(Locale.UK);
		cal.setTime(java.sql.Timestamp.valueOf("2018-01-01 00:00:00"));
		int a = cal.getWeeksInWeekYear();
		int b = cal.getWeekYear();
		int c = cal.get(Calendar.WEEK_OF_YEAR);
		System.out.println(a + "," + b + "," + c);
	}
	
	private MUNSManufacturingOrder get(MOrderLine line, int next)
	{
		java.sql.Timestamp datePromised = line.getC_Order().getDatePromised();
		if(datePromised.before(new Timestamp (System.currentTimeMillis())))
		{
			datePromised = new Timestamp(System.currentTimeMillis());
		}
		Calendar cal = Calendar.getInstance(Locale.UK);
		cal.setTime(datePromised);
		cal.add(Calendar.WEEK_OF_YEAR, next);
		int weekNo = UNSTimeUtil.getProductionWeekNo(new Timestamp(cal.getTimeInMillis()));
		int year = cal.get(Calendar.YEAR);
//		int weekNo = cal.get(Calendar.WEEK_OF_YEAR);
//		weekNo = weekNo + next;
//		
//		int year = cal.get(Calendar.YEAR);
		
		return MUNSManufacturingOrder.getCreate(getCtx(), line.getAD_Org_ID(), 
				DB.getSQLValue(get_TrxName(), 
						"SELECT C_Year_ID FROM C_Year WHERE FiscalYear=?",
							String.valueOf(year)), 0, String.valueOf(weekNo), get_TrxName());
	}
}