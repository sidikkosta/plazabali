package com.uns.model.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.model.MUNSFlowMeter;
import com.uns.model.MUNSFlowmeterConfiguration;
import com.uns.model.MUNSSounding;
import com.uns.model.MUNSTankSounding;

public class UNSGetValueFlowMeter extends SvrProcess {

	/**
	 * @author Thunder
	 */
	
	private int p_config_id = 0;
	private Timestamp p_startDate = null;
	private Timestamp p_endDate = null;
	private String p_strStartTime = null;
	private String p_strEndTime = null;
	private MUNSSounding m_sounding = null;
	private BigDecimal m_valflowmeter = Env.ZERO;
	
	
	public UNSGetValueFlowMeter() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] param = getParameter();
		
		for(ProcessInfoParameter para : param)
		{
			String name = para.getParameterName();
			
			if(name.equals("UNS_FlowmeterConfiguration_ID"))
				p_config_id = para.getParameterAsInt();
			else if(name.equals("StartDate"))
				p_startDate = para.getParameterAsTimestamp();
			else if(name.equals("StartTime"))
				p_strStartTime = para.getParameterAsString();
			else if(name.equals("EndDate"))
				p_endDate = para.getParameterAsTimestamp();
			else if(name.equals("EndTime"))
				p_strEndTime = para.getParameterAsString();
			else
				throw new AdempiereException("Unknown parameter name :"+name);
		}
		
		if(getRecord_ID() > 0)
		{
			m_sounding = new MUNSSounding(getCtx(), getRecord_ID(), get_TrxName());
			if(m_sounding == null)
				throw new AdempiereException("Cannout found Sounding");
		}
		else
		{
			throw new AdempiereException("Only can access from window");
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		if(p_config_id == 0 || p_startDate == null || p_endDate == null || p_strStartTime == null || p_strEndTime == null)
			throw new AdempiereException("Mandatory all parameter");
		
		if(p_startDate.after(p_endDate))
			throw new AdempiereException("Disallowed Start Time greater then End Time");
		
		if(p_strEndTime.length() != 4 || p_strEndTime.length() != 4)
			throw new AdempiereException("Worng format number on Start Time od End Time. it must 4 number");
		
		convertTime();
		
		//get Value of Flowmeter
		BigDecimal val = getValue();
		
		m_sounding.setFlowMeterQty(val);
		
		if(!m_sounding.save())
			throw new AdempiereException("Error when try to update flowmeter quantity");
		
		//update tank sounding
		MUNSTankSounding[] tanks = m_sounding.getLines();
		for(MUNSTankSounding tank : tanks)
		{
			if(tank.getDifferenceQty().signum() == 0)
			{
				tank.setWeightByFlowMeter(Env.ZERO);
			}
			else
			{
				BigDecimal divider = tank.getDifferenceQty().divide(m_sounding.getCPOProductionQty(), 10, RoundingMode.HALF_UP); 
				tank.setWeightByFlowMeter((divider.multiply(val)).setScale(0, RoundingMode.HALF_UP));
			}
			if(!tank.save())
				throw new AdempiereException("Error when try to update Tank Sounding");
		}
		
		return "Success";
	}
	
	@SuppressWarnings("unused")
	protected BigDecimal getValue() {
		
		BigDecimal value = Env.ZERO;
		
		MUNSFlowmeterConfiguration config = new MUNSFlowmeterConfiguration(getCtx(), p_config_id, get_TrxName());
		if(config == null)
			throw new AdempiereException("Cannot found Flow Meter Configuration");
		
		String calclogic = config.getCalcLogic();
		
		//check Calculation Logic
		String errorLogic = config.validateLogic(calclogic);
		if(!Util.isEmpty(errorLogic, true))
			throw new AdempiereException(errorLogic);
		
		value = doCalc(calclogic);
		
		return value;
	}
	
	public BigDecimal doCalc(String calcLogic) {
		
		BigDecimal value = Env.ZERO;
		
		String componentName = "";
		boolean componentOpenned = false;
		String logic = "";
		
		for(int i=0; i < calcLogic.length(); i++)
		{
			String val = "";
			String current = calcLogic.substring(i, i+1);
			if(current.equals("@") && !componentOpenned)
			{
				componentOpenned = true;
				continue;
			}
			else if(!current.equals("@") && componentOpenned)
			{
				componentName = componentName.concat(current);
				continue;
			}
			else if(current.equals("@") && componentOpenned)
			{
				getQtyFlowMeter(componentName);
				
				val = m_valflowmeter.toString();
				componentName = "";
				componentOpenned = false;
			}
			else
			{
				val = current;
			}
			
			logic = logic.concat(val);
		}
		
		String sql = "SELECT "+logic;
		value = DB.getSQLValueBD(get_TrxName(), sql);
		
		return value;
	}
	
	public void	getQtyFlowMeter(String component) {
		
		MUNSFlowMeter flowmeter = MUNSFlowMeter.get(getCtx(), component, get_TrxName());
		if(flowmeter == null)
			throw new AdempiereException("Cannot found flowmeter");
		
		String sql = "SELECT COALESCE(SUM(TotalCounter),0) FROM UNS_FlowMeterLine WHERE UNS_FlowMeter_ID = ?"
				+ " AND StartTime BETWEEN ? AND ? AND StopTime BETWEEN ? AND ?";
		
		BigDecimal val = DB.getSQLValueBD(get_TrxName(), sql, flowmeter.get_ID(), p_startDate, p_endDate, p_startDate, p_endDate);
		
		m_valflowmeter = val;
		
	}
	
	public void convertTime () {
		
		//Strart Time
		int timeInt = new Integer(p_strStartTime);
		String strDate = p_startDate.toString();
		
		if(timeInt > -1 && timeInt < 2360)
		{
			strDate = strDate.substring(0, 10)+" "+p_strStartTime.substring(0,2)+":"+p_strStartTime.substring(2)+":00";
		
			p_startDate = Timestamp.valueOf(strDate);
		}
		else
		{
			throw new AdempiereException("Wrong format start time");
		}
		
		//End Time
		timeInt = new Integer(p_strStartTime);
		strDate = p_startDate.toString();
		
		if(timeInt > -1 && timeInt < 2360)
		{
			strDate = strDate.substring(0, 10)+" "+p_strEndTime.substring(0,2)+":"+p_strEndTime.substring(2)+":00";
		
			p_startDate = Timestamp.valueOf(strDate);
		}
		else
		{
			throw new AdempiereException("Wrong format end time");
		}
	}
}
