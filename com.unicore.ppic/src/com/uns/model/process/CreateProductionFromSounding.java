/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Util;
import com.uns.model.MUNSSounding;

/**
 * @author Burhani Adam
 *
 */
public class CreateProductionFromSounding extends SvrProcess {

	/**
	 * 
	 */
	public CreateProductionFromSounding() {
		// TODO Auto-generated constructor stub
	}
	
	private Timestamp m_ProductionDate = null;
	private int m_Resource_ID = 0;
	private String m_processMsg = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("ProductionDate"))
				m_ProductionDate = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("UNS_Resource_ID"))
				m_Resource_ID = param.getParameterAsInt();
//			else if(param.getParameterName().equals("MovementQty"))
//				m_MovementQty = param.getParameterAsBigDecimal();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_processMsg = null;
		
		MUNSSounding ts = new MUNSSounding(getCtx(), getRecord_ID(), get_TrxName());
		m_processMsg = ts.createProduction(m_Resource_ID, m_ProductionDate);
		
		if(!Util.isEmpty(m_processMsg, true))
		{
			log.log(Level.WARNING, m_processMsg);
		}
		
		ts.saveEx();
		
		return m_processMsg;
	}
}
