/**
 * 
 */
package com.uns.model.process;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.MUNSMPSHeader;

/**
 * @author AzHaidar
 * @Untasoft 
 */
public class MRPUpdateProcessor extends SvrProcess {
	
	private int C_Year_ID;
	private int AD_Org_ID;
	
	/**
	 * 
	 */
	public MRPUpdateProcessor() {
		
	}
	
	public MRPUpdateProcessor(MUNSMPSHeader mpsHeader)
	{
		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{			
		ProcessInfoParameter[] param = getParameter();
		for (int i=0; i<param.length; i++)
		{
			String paramName = param[i].getParameterName();
			if (paramName.equals("C_Year_ID")) {
				C_Year_ID = param[i].getParameterAsInt();
			}
			else if (paramName.equals("AD_Org_ID")) {
				AD_Org_ID = param[i].getParameterAsInt();
			}
		}
		
		//m_MPSHeader= new MUNSMPSHeader(m_ctx, getRecord_ID(), m_trxName);
		//if(m_MPSHeader.getPrevMPS_ID() > 0)
		//	m_prevMPS = (MUNSMPSHeader)m_MPSHeader.getPrevMPS();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		String msg = null;

		String sql = "SELECT * FROM UpdateMRPLinesBasedOnYear(?, ?)";
		msg = DB.getSQLValueString(get_TrxName(), sql, AD_Org_ID, C_Year_ID);

		return msg;
	}	
}
