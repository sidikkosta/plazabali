/**
 * 
 */
package com.uns.model.process;

import java.util.Properties;
import java.util.logging.Level;

import org.compiere.process.ProcessCall;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.MUNSMOScheduler;
import com.uns.model.MUNSMOSchedulerLines;
import com.uns.model.MUNSProduction;
import com.uns.model.MUNSProductionSchedule;
import com.uns.model.MUNSResource;

/**
 * @author YAKA
 *
 */
public class CreateProduction extends SvrProcess implements ProcessCall {

	private int record_ID;
	private Properties m_Ctx = null;
	private String m_TrxName = null;
	private boolean fromHeader = true;

	/**
	 * 
	 */

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("FromHeader"))
				fromHeader = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		m_Ctx = getCtx();
		m_TrxName = get_TrxName();
		record_ID = getRecord_ID();
	}
	
	public CreateProduction(Properties ctx, int UNS_ProductionSchedule_ID, String trxName)
	{
		this.m_Ctx = ctx;
		this.record_ID = UNS_ProductionSchedule_ID;
		this.m_TrxName = trxName;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if(fromHeader)
		{
			MUNSMOScheduler mos = new MUNSMOScheduler(m_Ctx, record_ID, m_TrxName);
			for(MUNSMOSchedulerLines lines : mos.getLines(true))
			{
				create(lines);
			}
		}
		else
			create(new MUNSMOSchedulerLines(m_Ctx, record_ID, m_TrxName));
	
		return "Production created.";
	}
	
	private void create(MUNSMOSchedulerLines line) throws Exception
	{
		MUNSProductionSchedule ps = new MUNSProductionSchedule(getCtx(), 
				line.getUNS_ProductionSchedule_ID(), get_TrxName());
		
		for(MUNSResource rs : MUNSResource.getChildOf(getCtx(), get_TrxName(), 
				line.getAD_Org_ID(), ps.getM_Product_ID(), line.getUNS_Resource_ID()))
		{
			String sql = "SELECT COALESCE(SUM(MaxCaps),0) FROM UNS_Resource_InOut"
					+ " WHERE M_Product_ID = ? AND UNS_Resource_ID = ?";
			DB.getSQLValueBD(get_TrxName(),
					sql, ps.getM_Product_ID(), rs.getUNS_Resource_ID());
			
			new MUNSProduction(rs);
		}
	}
	
//	private void create(MUNSMOSchedulerLines line) throws Exception
//	{
//		m_unsRscID = line.getUNS_Resource_ID();
//		m_qtyToManufacture = line.getQtyOrdered();
//		if (record_ID < 1)
//			throw new AdempiereException("Record not found");
////		MUNSProductionSchedule ps = new MUNSProductionSchedule(m_Ctx, record_ID, m_TrxName);
//		MUNSProduction p = null;
//		
//		p = new MUNSProduction(ps, m_unsRscID, m_qtyToManufacture);
//		
//		if(!p.save())
//			throw new AdempiereException("Production can't create.");
//		
//		if (p.getOutputType().equals(MUNSProduction.OUTPUTTYPE_Multi))
//		{
//			BigDecimal mainPartBOMQty = MUNSResourceInOut.getMainPartBOMQtyOf(
//					(MProduct) p.getM_Product(), MProductBOM.BOMTYPE_StandardPart);
//			BigDecimal inputQty = ps.getQtyOrdered().multiply(mainPartBOMQty);
//			
//			GenerateOutputPlan.doIt(m_Ctx, p, m_TrxName);
//			
//			for (MUNSProductionOutPlan outputPlan : p.getOutputs())
//			{
//				if (outputPlan.getM_Product_ID() == ps.getM_Product_ID())
//				{
//					outputPlan.setQtyPlan(ps.getQtyOrdered());
//					outputPlan.saveEx();
//					continue;
//				}
//				mainPartBOMQty = MUNSResourceInOut.getMainPartBOMQtyOf(
//						(MProduct) outputPlan.getM_Product(), MProductBOM.BOMTYPE_StandardPart);
//				if (mainPartBOMQty.signum() > 0)
//				{
//					outputPlan.setQtyPlan(inputQty.divide(mainPartBOMQty, 6, BigDecimal.ROUND_HALF_UP));
//					outputPlan.saveEx();
//				}
//			}
//			//for (MUNSProductionOutPlan)
//		}
//		addLog(record_ID, p.getProductionDate(), p.getProductionQty(), p.getDocumentNo(), p.get_Table_ID(), record_ID);
//	}
}
