/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;
import com.uns.util.ErrorMsg;

/**
 * @author YAKA
 *
 */
public class MUNSResourceWorkerLine extends X_UNS_Resource_WorkerLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7389835350424871845L;

	/**
	 * @param ctx
	 * @param UNS_Resource_WorkerLine_ID
	 * @param trxName
	 */
	public MUNSResourceWorkerLine(Properties ctx,
			int UNS_Resource_WorkerLine_ID, String trxName) {
		super(ctx, UNS_Resource_WorkerLine_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSResourceWorkerLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean sucess)
	{
		X_UNS_Employee worker = new X_UNS_Employee(getCtx(), getLabor_ID(), get_TrxName());
		I_UNS_Contract_Recommendation contract = worker.getUNS_Contract_Recommendation();
		
		if((MUNSContractRecommendation.NEXTCONTRACTTYPE_Borongan.equals(contract.getNextContractType())
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganCV.equals(contract.getNextContractType())
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarian.equals(contract.getNextContractType())
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarianCV.equals(contract.getNextContractType()))
					&& !isValidProportion())
		
//		if (EMPLOYMENTSTATUS_Borongan.equals(getEmploymentStatus()) && !isValidProportion())
			return false;
		
		countWorkingAndOffDays();
		
		return true;
	}

	private void countWorkingAndOffDays() {
		String s = "SELECT SE.uns_shopemployee_id"
				+ " FROM uns_shoprecap SR, uns_resource_workerline WL, uns_shopemployee SE, c_period P"
				+ " WHERE SE.uns_employee_id = WL.labor_id"
				+ " AND WL.labor_id = ?"
				+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
				+ " AND WL.validto BETWEEN P.startdate AND P.enddate"
				+ " AND SR.c_period_id = P.c_period_id"
				+ " AND SR.uns_shoprecap_id = SE.uns_shoprecap_id";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DB.prepareStatement(s, get_TrxName());
			ps.setInt(1, getLabor_ID());
			rs = ps.executeQuery();
			if (rs.next()) {
				MUNSShopEmployee employee = new MUNSShopEmployee(getCtx(),
						rs.getInt(1), get_TrxName());
				employee.countWorkingAndOffDays();
				employee.saveEx();
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, s, e);
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
	}

	/**
	 * 
	 * @return
	 */
	private boolean isValidProportion()
	{
		BigDecimal totalPrimePortion = Env.ZERO;
		BigDecimal totalNonPrimePortion = Env.ZERO;
		
		for (MUNSResourceWorkerLine worker : getParent().getWorkerLines(true))
		{
			X_UNS_Employee pekerja = new X_UNS_Employee(getCtx(), worker.getLabor_ID(), get_TrxName());
			I_UNS_Contract_Recommendation contract = pekerja.getUNS_Contract_Recommendation();
			
			if((!MUNSContractRecommendation.NEXTCONTRACTTYPE_Borongan.equals(contract.getNextContractType())
					&& !MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganCV.equals(contract.getNextContractType())
					&& !MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarian.equals(contract.getNextContractType())
					&& !MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarianCV.equals(contract.getNextContractType()))
					|| worker.get_ID() == get_ID())
			
//			if (!EMPLOYMENTSTATUS_Borongan.equals(worker.getEmploymentStatus())
//				|| worker.get_ID() == get_ID())
				continue;
			
			if (worker.isPrimePortion())
				totalPrimePortion = totalPrimePortion.add(worker.getResultProportion());
			else
				totalNonPrimePortion  = totalNonPrimePortion.add(worker.getResultProportion());
		}
		totalPrimePortion = totalPrimePortion.setScale(4, BigDecimal.ROUND_HALF_DOWN);
		if (totalPrimePortion.compareTo(Env.ONEHUNDRED) > 0)
		{
			ErrorMsg.setErrorMsg(getCtx(),
					"SaveError", "Accumulation of Primary Result Proportion cannot greater than 100%");
			return false;
		}
		/*
		if (totalNonPrimePortion.compareTo(Env.ONEHUNDRED) > 0)
		{
			ErrorMsg.setErrorMsg(getCtx(), "SaveError", 
					"Accumulation of Non-Primary result proportion cannot greater than 100%");
			return false;
		}
		*/		
		return true;
	}
	
	@Override
	public MUNSResource getParent()
	{
		return (MUNSResource)getUNS_Resource();
	}
	
	/**
	 * Get Worker Of Departement
	 * @param ctx
	 * @param AD_Org_ID
	 * @param trxName
	 * @return
	 * @deprecated
	 */
	public static List<MUNSResourceWorkerLine>  getOrgWorkers(Properties ctx, int AD_Org_ID, String trxName)
	{
		return Query.get(
				ctx, UNSPPICModelFactory.getExtensionID(), Table_Name
				, COLUMNNAME_AD_Org_ID + " = " + AD_Org_ID, trxName)
				.list();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		checkDuplicateWorker();
		return super.beforeSave(newRecord);
	}
	
	private void checkDuplicateWorker()
	{
		/**
		MUNSResource workerRsc = getParent();
		MUNSResourceWorkerLine[] workers = workerRsc.getWorkerLines(true);
		for(MUNSResourceWorkerLine worker : workers)
		{
			if(worker.get_ID() == get_ID())
				continue;
			if (worker.getLabor_ID() == getLabor_ID()
					&& (getValidFrom() != null && getValidFrom().equals(worker.getValidFrom()))
					&& (getValidTo() != null && getValidTo().equals(worker.getValidTo())))			
				throw new AdempiereUserError("Duplicate Worker on one resource.");
		}
		*/
		
		String sql = "SELECT rs.Name FROM UNS_Resource rs WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_Resource_WorkerLine wl WHERE rs.UNS_Resource_ID = wl.UNS_Resource_ID"
				+ " AND wl.IsActive = 'Y' AND wl.ValidFrom =? AND wl.Labor_ID = ?) AND rs.UNS_Resource_ID <> ?";
		String rsName = DB.getSQLValueString(get_TrxName(), sql, new Object[]{getValidFrom(), getLabor_ID(), getUNS_Resource_ID()});
		
		if(!Util.isEmpty(rsName, true))
			throw new AdempiereException("Duplicate employee on other resource. " + rsName);
		else
		{
			sql = "SELECT COUNT(*) FROM UNS_Resource_WorkerLine wl"
					+ " WHERE ValidFrom = ? AND Labor_ID = ? AND UNS_Resource_WorkerLine_ID <> ?"
					+ " AND IsActive = 'Y'";
			int count = DB.getSQLValue(get_TrxName(), sql, new Object[]{getValidFrom(), getLabor_ID(), get_ID()});
			if(count > 0)
				throw new AdempiereException("Duplicate employee on same resource.");
		}
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_Org_ID
	 * @param employeeID
	 * @param trxName
	 * @return
	 * @deprecated should not get it only by org, as a worker can be presenting multi org in resources.
	 */
	public static MUNSResourceWorkerLine getWorkerOfOrg(Properties ctx, int AD_Org_ID, int employeeID, String trxName)
	{
		for (MUNSResourceWorkerLine worker : getOrgWorkers(ctx, AD_Org_ID, trxName))
		{
			if(employeeID == worker.getLabor_ID())
				return worker;
		}
		return null;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_Org_ID
	 * @param employeeID
	 * @param trxName
	 * @return
	 */
	public static MUNSResourceWorkerLine[] getNonAdditionalWorkerOf(
			Properties ctx, int employeeID, String trxName)
	{
		List<MUNSResourceWorkerLine> rscWorkerList =
				Query.get(ctx, UNSPPICModelFactory.getExtensionID(), MUNSResourceWorkerLine.Table_Name, 
						  "Labor_ID=? AND IsAdditional=?", trxName)
					 .setParameters(employeeID, 'N')
					 .list();
		
		MUNSResourceWorkerLine[] rscWorker = new MUNSResourceWorkerLine[rscWorkerList.size()];
		rscWorkerList.toArray(rscWorker);
		return rscWorker;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_Org_ID
	 * @param employeeID
	 * @param trxName
	 * @return
	 */
	public static MUNSResourceWorkerLine getWorker(
			Properties ctx, int resourceId, int employeeID, String trxName)
	{
		String whereClause = "UNS_Resource_ID=? AND Labor_ID=?";

		MUNSResourceWorkerLine worker =
				Query.get(ctx, UNSPPICModelFactory.getExtensionID(), Table_Name, whereClause, trxName)
				.setParameters(resourceId, employeeID)
				.firstOnly();
		
		return worker;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_Org_ID
	 * @param employeeID
	 * @param ValidFrom
	 * @param trxName
	 * @return
	 */
	public static MUNSResourceWorkerLine getWorker(
			Properties ctx, int resourceId, int employeeID, Timestamp validFrom, String trxName)
	{
		String whereClause = " UNS_Resource_ID=? AND Labor_ID=? AND ValidFrom < ?";

		MUNSResourceWorkerLine worker =
				Query.get(ctx, UNSPPICModelFactory.getExtensionID(), Table_Name, whereClause, trxName)
				.setParameters(resourceId, employeeID, validFrom).setOrderBy("ValidFrom DESC")
				.first();
		
		return worker;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_Org_ID
	 * @param employeeID
	 * @param trxName
	 * @return
	 */
	public static MUNSResourceWorkerLine[] getWorkers(
			Properties ctx, int resourceId, int employeeID, String trxName)
	{
		String whereClause = "UNS_Resource_ID=? AND Labor_ID=?";

		List<MUNSResourceWorkerLine> workers =
				Query.get(ctx, UNSPPICModelFactory.getExtensionID(), Table_Name, whereClause, trxName)
				.setParameters(resourceId, employeeID).setOrderBy("ValidTo DESC, ValidFrom DESC")
				.list();
		
		return workers.toArray(new MUNSResourceWorkerLine[workers.size()]);
	}
	
	public static MUNSResourceWorkerLine getWorkersByValidDate(
			Properties ctx, int resourceId, int employeeID, Timestamp validFrom,
				Timestamp validTo, String trxName)
	{
		String whereClause = "UNS_Resource_ID=? AND Labor_ID=? AND ValidTo = ? AND ValidFrom = ?";

		MUNSResourceWorkerLine workers =
				Query.get(ctx, UNSPPICModelFactory.getExtensionID(), Table_Name, whereClause, trxName)
				.setParameters(resourceId, employeeID, validFrom, validTo).first();
		
		return workers;
	}
	
	/**
	 * 
	 * @return
	 */
	public MUNSResource getWorkCenter()
	{
		return getWorkCenter((MUNSResource) getUNS_Resource());
	}
	
	/**
	 * 
	 * @param rsc
	 * @return
	 */
	private MUNSResource getWorkCenter(MUNSResource rsc)
	{
		if (rsc == null)
			return rsc;
		
		if (rsc.isWorkCenter())
			return rsc;
		
		return getWorkCenter((MUNSResource) rsc.getResourceParent());
	}
	
	public static MUNSResourceWorkerLine get(Properties ctx, int EmployeeID, Timestamp date, String trxName)
	{
		String wc = "UNS_Resource_WorkerLine_ID = (SELECT getresourceworkerline(?,?,?))";
		
		MUNSResourceWorkerLine retVal = Query.get(ctx, UNSPPICModelFactory.getExtensionID(),
				Table_Name, wc, trxName).setParameters(EmployeeID, date, null).setOnlyActiveRecords(true).first();
		
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.PO#afterDelete(boolean)
	 */
	@Override
	protected boolean afterDelete(boolean success) {
		countWorkingAndOffDays();
		return super.afterDelete(success);
	}
}
