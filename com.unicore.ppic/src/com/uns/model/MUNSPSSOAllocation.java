/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MUOM;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.MOrderLine;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;


/**
 * @author YAKA
 *
 */
public class MUNSPSSOAllocation extends X_UNS_PSSOAllocation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5111790168621491240L;

	/**
	 * @param ctx
	 * @param UNS_PSSOAllocation_ID
	 * @param trxName
	 */
	public MUNSPSSOAllocation(Properties ctx, int UNS_PSSOAllocation_ID,
			String trxName) {
		super(ctx, UNS_PSSOAllocation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPSSOAllocation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		if(!success)
			return false;
		
		MUNSProductionSchedule ps = new MUNSProductionSchedule(getCtx(), getUNS_ProductionSchedule_ID(), get_TrxName());
		ps.setQtySO(ps.getQtySO().add(getQtyUom()));
		ps.saveEx();
		
		return true;
	}

	public static BigDecimal getUnAllocationQty(MOrderLine ol, MUNSProductionSchedule ps, Properties ctx, String trxName) {
		BigDecimal unAllocation = ol.getQtyEntered();
		
		for (MUNSPSSOAllocation real : ol.getAllocation()){
			unAllocation = ps.getQtyUom().subtract(real.getQtyUom());
		}
		
		return unAllocation;
	}
	
	public static MUNSPSSOAllocation getAllocation(Properties ctx, MOrderLine line, 
			MUNSProductionSchedule ps, String trxName)
	{
		MUNSPSSOAllocation soa = Query.get(ctx, UNSPPICModelFactory.getExtensionID(),
				Table_Name, COLUMNNAME_C_OrderLine_ID + "=? AND " + COLUMNNAME_UNS_ProductionSchedule_ID + "=?",
				trxName).setParameters(line.get_ID(), ps.get_ID()).first();
		
		return soa;
	}
	
	public static MUNSPSSOAllocation createSOAllocation(MOrderLine m_orderline, MUNSProductionSchedule m_ps, 
			Properties ctx, String trxName)
	{
//		BigDecimal unAllocation = MUNSPSSOAllocation.getUnAllocationQty(m_orderline, m_ps, ctx, trxName);
		BigDecimal unAllocation = m_orderline.getUnAllocatedQtyOnPS();
		if(unAllocation.compareTo(Env.ZERO) <= 0)
		{
			return MUNSPSSOAllocation.getAllocation(ctx, m_orderline, m_ps, trxName);
		}
		MUNSPSSOAllocation new_soallocation = new MUNSPSSOAllocation(ctx, 0, trxName);
//		BigDecimal qty_real = Env.ZERO;
//		int compare = unAllocation.compareTo(m_ps.getQtyUom());
//		if (compare<0)
//			qty_real = unAllocation;
//		else
//			qty_real = m_ps.getQtyUom();
		
		new_soallocation.setQtyUom(MUNSPSSOAllocation.analyzeAvaliableProduction(
				trxName, ctx, m_ps, unAllocation));
		BigDecimal MTProduct = m_ps.getSERLineProduct().getM_Product().getWeight().multiply(BigDecimal.valueOf(0.0001));
		new_soallocation.setQtyMT(new_soallocation.getQtyUom().multiply(MTProduct));
		new_soallocation.setUNS_ProductionSchedule_ID(m_ps.get_ID());
		new_soallocation.setC_OrderLine_ID(m_orderline.get_ID());
		new_soallocation.setClientOrg(m_ps);
		
		if (!new_soallocation.save()){
			throw new AdempiereException("Error when create realization");
		}
		return new_soallocation;
	//TODO SO Allocation
//		MOrderLine setOL = new MOrderLine(ctx, m_orderline.get_ID(), trxName);
//		setOL.setQtyAllocated(setOL.getQtyAllocated().add(qty_real));
//		setOL.saveEx();
		
//		if (compare<0)
//			return "SO Allocation created. SO Line still have unallocation";
//		else if (compare>0)
//			return "SO Allocation created. Manufacturing Order " + m_ps.getDocumentNo()+" still have unallocation";
//		else
//			return "SO Allocation created. ";
	}
	
	private static BigDecimal analyzeAvaliableProduction(String trxName, Properties ctx,
			MUNSProductionSchedule ps, BigDecimal Qty)
	{
		BigDecimal avaliable = Env.ZERO;
		BigDecimal maxCaps = Env.ZERO;
		String sql = "SELECT io.* FROM UNS_Resource_InOut io"
				+ " WHERE io.UNS_Resource_ID = ? AND io.M_Product_ID = ?";
//		maxCaps = DB.getSQLValueBD(trxName, sql, ps.getUNS_Resource_ID(), ps.getM_Product_ID());
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			stmt = DB.prepareStatement(sql, trxName);
			stmt.setInt(1, ps.getUNS_Resource_ID());
			stmt.setInt(2, ps.getM_Product_ID());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				MUNSResourceInOut io = new MUNSResourceInOut(ctx, rs.getInt(1), trxName);
				MUOM uom = MUOM.get(ctx, io.getUOMTime_ID());
				BigDecimal totalHours = io.getDay1ProductionHours().add(io.getDay2ProductionHours())
								.add(io.getDay3ProductionHours()).add(io.getDay4ProductionHours())
								.add(io.getDay5ProductionHours()).add(io.getDay6ProductionHours())
								.add(io.getDay7ProductionHours());
				if(uom.getUOMSymbol().trim().equals("h"))
					maxCaps = io.getMaxCaps().multiply(totalHours);
			}
		}
		catch (Exception e)
		{
			throw new AdempiereException(e.getMessage());
		}
//		if(1==1)
//			throw new AdempiereException("a");
		if(maxCaps == null)
		{
			maxCaps = Env.ZERO;
			return Env.ZERO;
		}
		if(maxCaps.compareTo(ps.getQtyOrdered()) <= 0)
			return avaliable;
		
		avaliable = maxCaps.subtract(ps.getQtyOrdered());
		
		if(avaliable.compareTo(Qty) == 1)
			avaliable = Qty;
		
		return avaliable;
	}
	
	@Override
	protected boolean beforeDelete() {
//		MOrderLine setOL = new MOrderLine(getCtx(), getC_OrderLine_ID(), get_TrxName());
		//TODO SO Allocation
//		setOL.setQtyAllocated(setOL.getQtyAllocated().subtract(getQtyUom()));
//		setOL.saveEx();
		
		return super.beforeDelete();
	}
	
	public MUNSProductionSchedule getParent(){
		return new MUNSProductionSchedule(getCtx(), getUNS_ProductionSchedule_ID(), get_TrxName());
	}
}
