/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;

/**
 * @author AzHaidar
 *
 */
public class MUNSProductionOperationHrs extends X_UNS_Production_OperationHrs {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5626979166655528734L;

	/**
	 * @param ctx
	 * @param UNS_Production_OperationHrs_ID
	 * @param trxName
	 */
	public MUNSProductionOperationHrs(Properties ctx,
			int UNS_Production_OperationHrs_ID, String trxName) {
		super(ctx, UNS_Production_OperationHrs_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSProductionOperationHrs(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
	}

	/**
	 * 
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(getUNS_Production_ID() <= 0 && getUNS_Sounding_ID() <= 0)
			return false;
		
		if(getUNS_Sounding_ID() > 0)
		{
			String sql = "SELECT FFBProcessedNettoI FROM UNS_Sounding WHERE"
					+ " UNS_Sounding_ID = ?";
			BigDecimal ffbprocqty = DB.getSQLValueBD(get_TrxName(), sql, getUNS_Sounding_ID());
			if(ffbprocqty.signum() == 0)
				throw new AdempiereException("No production, cannot save.");
		}
		
		if (getLine() == 0)
		{
			String sql = "SELECT COALESCE(MAX(Line),0)+10 AS DefaultValue FROM UNS_Production_OperationHrs WHERE UNS_Production_ID=?";
			int ii = DB.getSQLValue (get_TrxName(), sql, getUNS_Production_ID());
			setLine (ii);
		}
		
		if (newRecord || is_ValueChanged(COLUMNNAME_TimeStart) || is_ValueChanged(COLUMNNAME_TimeEnd))
		{
			long differentTime = getTimeEnd().getTime() - getTimeStart().getTime();
			double hours = differentTime / (1000.0 * 60.0 * 60.0);
			
			BigDecimal hoursBD = new BigDecimal(hours).setScale(3, BigDecimal.ROUND_HALF_UP);
			setTotalHours(hoursBD);
		}		
		
		return true;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{		
		String sql = "UPDATE UNS_Production SET EffectiveHours=("
				+ "SELECT TotalHours FROM UNS_Production_OperationHrs WHERE UNS_Production_ID=? AND EventType='RUN') "
				+ "	WHERE UNS_Production_ID=?";
		int count = DB.executeUpdateEx(
				sql, new Object[]{getUNS_Production_ID(), getUNS_Production_ID()}, get_TrxName());
		
		if (count < 0)
			throw new AdempiereException("Failed when updating column value of Effective Hours on Production Header.");
		
		return true;
	}
	
}
