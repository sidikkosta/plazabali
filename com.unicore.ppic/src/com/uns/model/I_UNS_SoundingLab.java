/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_SoundingLab
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_SoundingLab 
{

    /** TableName=UNS_SoundingLab */
    public static final String Table_Name = "UNS_SoundingLab";

    /** AD_Table_ID=1000327 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AVGPressFFB */
    public static final String COLUMNNAME_AVGPressFFB = "AVGPressFFB";

	/** Set Average Press FFB	  */
	public void setAVGPressFFB (BigDecimal AVGPressFFB);

	/** Get Average Press FFB	  */
	public BigDecimal getAVGPressFFB();

    /** Column name CondFFB */
    public static final String COLUMNNAME_CondFFB = "CondFFB";

	/** Set CondFFB	  */
	public void setCondFFB (BigDecimal CondFFB);

	/** Get CondFFB	  */
	public BigDecimal getCondFFB();

    /** Column name CondSample */
    public static final String COLUMNNAME_CondSample = "CondSample";

	/** Set CondSample	  */
	public void setCondSample (BigDecimal CondSample);

	/** Get CondSample	  */
	public BigDecimal getCondSample();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DirtKernel */
    public static final String COLUMNNAME_DirtKernel = "DirtKernel";

	/** Set Dirt Kernel.
	  * How Much Kernel with Dirt Quality
	  */
	public void setDirtKernel (BigDecimal DirtKernel);

	/** Get Dirt Kernel.
	  * How Much Kernel with Dirt Quality
	  */
	public BigDecimal getDirtKernel();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name DustCycloneFFB */
    public static final String COLUMNNAME_DustCycloneFFB = "DustCycloneFFB";

	/** Set Dust Cyclone	  */
	public void setDustCycloneFFB (BigDecimal DustCycloneFFB);

	/** Get Dust Cyclone	  */
	public BigDecimal getDustCycloneFFB();

    /** Column name DustCycloneSample */
    public static final String COLUMNNAME_DustCycloneSample = "DustCycloneSample";

	/** Set Dust Cyclone Sample	  */
	public void setDustCycloneSample (BigDecimal DustCycloneSample);

	/** Get Dust Cyclone Sample	  */
	public BigDecimal getDustCycloneSample();

    /** Column name EffEkstraksiCPO */
    public static final String COLUMNNAME_EffEkstraksiCPO = "EffEkstraksiCPO";

	/** Set EffEkstraksiCPO	  */
	public void setEffEkstraksiCPO (BigDecimal EffEkstraksiCPO);

	/** Get EffEkstraksiCPO	  */
	public BigDecimal getEffEkstraksiCPO();

    /** Column name EffEkstraksiKernel */
    public static final String COLUMNNAME_EffEkstraksiKernel = "EffEkstraksiKernel";

	/** Set EffEkstraksiKernel	  */
	public void setEffEkstraksiKernel (BigDecimal EffEkstraksiKernel);

	/** Get EffEkstraksiKernel	  */
	public BigDecimal getEffEkstraksiKernel();

    /** Column name FCycloneFFB */
    public static final String COLUMNNAME_FCycloneFFB = "FCycloneFFB";

	/** Set FCycloneFFB	  */
	public void setFCycloneFFB (BigDecimal FCycloneFFB);

	/** Get FCycloneFFB	  */
	public BigDecimal getFCycloneFFB();

    /** Column name FCycloneFFB_Oil */
    public static final String COLUMNNAME_FCycloneFFB_Oil = "FCycloneFFB_Oil";

	/** Set FCycloneFFB (Oil)	  */
	public void setFCycloneFFB_Oil (BigDecimal FCycloneFFB_Oil);

	/** Get FCycloneFFB (Oil)	  */
	public BigDecimal getFCycloneFFB_Oil();

    /** Column name FCycloneSample */
    public static final String COLUMNNAME_FCycloneSample = "FCycloneSample";

	/** Set FCycloneSample	  */
	public void setFCycloneSample (BigDecimal FCycloneSample);

	/** Get FCycloneSample	  */
	public BigDecimal getFCycloneSample();

    /** Column name FCycloneSample_Oil */
    public static final String COLUMNNAME_FCycloneSample_Oil = "FCycloneSample_Oil";

	/** Set FCycloneSample (Oil)	  */
	public void setFCycloneSample_Oil (BigDecimal FCycloneSample_Oil);

	/** Get FCycloneSample (Oil)	  */
	public BigDecimal getFCycloneSample_Oil();

    /** Column name FruitInEB_FFB */
    public static final String COLUMNNAME_FruitInEB_FFB = "FruitInEB_FFB";

	/** Set Fruit In EB	  */
	public void setFruitInEB_FFB (BigDecimal FruitInEB_FFB);

	/** Get Fruit In EB	  */
	public BigDecimal getFruitInEB_FFB();

    /** Column name FruitInEBSample */
    public static final String COLUMNNAME_FruitInEBSample = "FruitInEBSample";

	/** Set Fruit In EB Sample	  */
	public void setFruitInEBSample (BigDecimal FruitInEBSample);

	/** Get Fruit In EB Sample	  */
	public BigDecimal getFruitInEBSample();

    /** Column name HPhaseFFB */
    public static final String COLUMNNAME_HPhaseFFB = "HPhaseFFB";

	/** Set HPhaseFFB.
	  * HPhaseFFB
	  */
	public void setHPhaseFFB (BigDecimal HPhaseFFB);

	/** Get HPhaseFFB.
	  * HPhaseFFB
	  */
	public BigDecimal getHPhaseFFB();

    /** Column name HPhaseSample */
    public static final String COLUMNNAME_HPhaseSample = "HPhaseSample";

	/** Set HPhaseSample	  */
	public void setHPhaseSample (BigDecimal HPhaseSample);

	/** Get HPhaseSample	  */
	public BigDecimal getHPhaseSample();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name JKFFB */
    public static final String COLUMNNAME_JKFFB = "JKFFB";

	/** Set JKFFB	  */
	public void setJKFFB (BigDecimal JKFFB);

	/** Get JKFFB	  */
	public BigDecimal getJKFFB();

    /** Column name JKSample */
    public static final String COLUMNNAME_JKSample = "JKSample";

	/** Set JKSample	  */
	public void setJKSample (BigDecimal JKSample);

	/** Get JKSample	  */
	public BigDecimal getJKSample();

    /** Column name KERNettoI */
    public static final String COLUMNNAME_KERNettoI = "KERNettoI";

	/** Set KER Netto-I.
	  * Kernel Extraction Rate of Netto-I
	  */
	public void setKERNettoI (BigDecimal KERNettoI);

	/** Get KER Netto-I.
	  * Kernel Extraction Rate of Netto-I
	  */
	public BigDecimal getKERNettoI();

    /** Column name LabSupervisor */
    public static final String COLUMNNAME_LabSupervisor = "LabSupervisor";

	/** Set LabSupervisor	  */
	public void setLabSupervisor (String LabSupervisor);

	/** Get LabSupervisor	  */
	public String getLabSupervisor();

    /** Column name LTDS1FFB */
    public static final String COLUMNNAME_LTDS1FFB = "LTDS1FFB";

	/** Set LTDS1FFB	  */
	public void setLTDS1FFB (BigDecimal LTDS1FFB);

	/** Get LTDS1FFB	  */
	public BigDecimal getLTDS1FFB();

    /** Column name LTDS1Sample */
    public static final String COLUMNNAME_LTDS1Sample = "LTDS1Sample";

	/** Set LTDS1Sample	  */
	public void setLTDS1Sample (BigDecimal LTDS1Sample);

	/** Get LTDS1Sample	  */
	public BigDecimal getLTDS1Sample();

    /** Column name LTDS2FFB */
    public static final String COLUMNNAME_LTDS2FFB = "LTDS2FFB";

	/** Set LTDS2FFB	  */
	public void setLTDS2FFB (BigDecimal LTDS2FFB);

	/** Get LTDS2FFB	  */
	public BigDecimal getLTDS2FFB();

    /** Column name LTDS2Sample */
    public static final String COLUMNNAME_LTDS2Sample = "LTDS2Sample";

	/** Set LTDS2Sample	  */
	public void setLTDS2Sample (BigDecimal LTDS2Sample);

	/** Get LTDS2Sample	  */
	public BigDecimal getLTDS2Sample();

    /** Column name MoistKernel */
    public static final String COLUMNNAME_MoistKernel = "MoistKernel";

	/** Set Moist Kernel.
	  * How Much Kernel with Moist Quality
	  */
	public void setMoistKernel (BigDecimal MoistKernel);

	/** Get Moist Kernel.
	  * How Much Kernel with Moist Quality
	  */
	public BigDecimal getMoistKernel();

    /** Column name NutFFB */
    public static final String COLUMNNAME_NutFFB = "NutFFB";

	/** Set NutFFB	  */
	public void setNutFFB (BigDecimal NutFFB);

	/** Get NutFFB	  */
	public BigDecimal getNutFFB();

    /** Column name NutSample */
    public static final String COLUMNNAME_NutSample = "NutSample";

	/** Set NutSample	  */
	public void setNutSample (BigDecimal NutSample);

	/** Get NutSample	  */
	public BigDecimal getNutSample();

    /** Column name OERNettoI */
    public static final String COLUMNNAME_OERNettoI = "OERNettoI";

	/** Set OER Netto-I.
	  * Oil Ekstraction Rate of Netto-I
	  */
	public void setOERNettoI (BigDecimal OERNettoI);

	/** Get OER Netto-I.
	  * Oil Ekstraction Rate of Netto-I
	  */
	public BigDecimal getOERNettoI();

    /** Column name Press1Sample */
    public static final String COLUMNNAME_Press1Sample = "Press1Sample";

	/** Set Press1Sample	  */
	public void setPress1Sample (BigDecimal Press1Sample);

	/** Get Press1Sample	  */
	public BigDecimal getPress1Sample();

    /** Column name Press2Sample */
    public static final String COLUMNNAME_Press2Sample = "Press2Sample";

	/** Set Press2Sample	  */
	public void setPress2Sample (BigDecimal Press2Sample);

	/** Get Press2Sample	  */
	public BigDecimal getPress2Sample();

    /** Column name Press3Sample */
    public static final String COLUMNNAME_Press3Sample = "Press3Sample";

	/** Set Press3Sample	  */
	public void setPress3Sample (BigDecimal Press3Sample);

	/** Get Press3Sample	  */
	public BigDecimal getPress3Sample();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name RecFFB */
    public static final String COLUMNNAME_RecFFB = "RecFFB";

	/** Set RecFFB	  */
	public void setRecFFB (BigDecimal RecFFB);

	/** Get RecFFB	  */
	public BigDecimal getRecFFB();

    /** Column name RecSample */
    public static final String COLUMNNAME_RecSample = "RecSample";

	/** Set RecSample	  */
	public void setRecSample (BigDecimal RecSample);

	/** Get RecSample	  */
	public BigDecimal getRecSample();

    /** Column name RMill1Sample */
    public static final String COLUMNNAME_RMill1Sample = "RMill1Sample";

	/** Set RMill1Sample	  */
	public void setRMill1Sample (BigDecimal RMill1Sample);

	/** Get RMill1Sample	  */
	public BigDecimal getRMill1Sample();

    /** Column name RMill2Sample */
    public static final String COLUMNNAME_RMill2Sample = "RMill2Sample";

	/** Set RMill2Sample	  */
	public void setRMill2Sample (BigDecimal RMill2Sample);

	/** Get RMill2Sample	  */
	public BigDecimal getRMill2Sample();

    /** Column name RMill3Sample */
    public static final String COLUMNNAME_RMill3Sample = "RMill3Sample";

	/** Set RMill3Sample	  */
	public void setRMill3Sample (BigDecimal RMill3Sample);

	/** Get RMill3Sample	  */
	public BigDecimal getRMill3Sample();

    /** Column name SolidFFB */
    public static final String COLUMNNAME_SolidFFB = "SolidFFB";

	/** Set SolidFFB	  */
	public void setSolidFFB (BigDecimal SolidFFB);

	/** Get SolidFFB	  */
	public BigDecimal getSolidFFB();

    /** Column name SolidSample */
    public static final String COLUMNNAME_SolidSample = "SolidSample";

	/** Set SolidSample	  */
	public void setSolidSample (BigDecimal SolidSample);

	/** Get SolidSample	  */
	public BigDecimal getSolidSample();

    /** Column name TotalCPOLosses */
    public static final String COLUMNNAME_TotalCPOLosses = "TotalCPOLosses";

	/** Set TotalCPOLosses	  */
	public void setTotalCPOLosses (BigDecimal TotalCPOLosses);

	/** Get TotalCPOLosses	  */
	public BigDecimal getTotalCPOLosses();

    /** Column name TotalKernelLosses */
    public static final String COLUMNNAME_TotalKernelLosses = "TotalKernelLosses";

	/** Set TotalKernelLosses	  */
	public void setTotalKernelLosses (BigDecimal TotalKernelLosses);

	/** Get TotalKernelLosses	  */
	public BigDecimal getTotalKernelLosses();

    /** Column name UnderFlowSample */
    public static final String COLUMNNAME_UnderFlowSample = "UnderFlowSample";

	/** Set UnderFlowSample	  */
	public void setUnderFlowSample (BigDecimal UnderFlowSample);

	/** Get UnderFlowSample	  */
	public BigDecimal getUnderFlowSample();

    /** Column name UNS_Sounding_ID */
    public static final String COLUMNNAME_UNS_Sounding_ID = "UNS_Sounding_ID";

	/** Set Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID);

	/** Get Sounding	  */
	public int getUNS_Sounding_ID();

	public com.uns.model.I_UNS_Sounding getUNS_Sounding() throws RuntimeException;

    /** Column name UNS_SoundingLab_ID */
    public static final String COLUMNNAME_UNS_SoundingLab_ID = "UNS_SoundingLab_ID";

	/** Set UNS_SoundingLab_ID	  */
	public void setUNS_SoundingLab_ID (int UNS_SoundingLab_ID);

	/** Get UNS_SoundingLab_ID	  */
	public int getUNS_SoundingLab_ID();

    /** Column name UNS_SoundingLab_UU */
    public static final String COLUMNNAME_UNS_SoundingLab_UU = "UNS_SoundingLab_UU";

	/** Set UNS_SoundingLab_UU	  */
	public void setUNS_SoundingLab_UU (String UNS_SoundingLab_UU);

	/** Get UNS_SoundingLab_UU	  */
	public String getUNS_SoundingLab_UU();

    /** Column name UNS_TankSounding_ID */
    public static final String COLUMNNAME_UNS_TankSounding_ID = "UNS_TankSounding_ID";

	/** Set Tank Sounding	  */
	public void setUNS_TankSounding_ID (int UNS_TankSounding_ID);

	/** Get Tank Sounding	  */
	public int getUNS_TankSounding_ID();

	public com.uns.model.I_UNS_TankSounding getUNS_TankSounding() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name WetSellFFB */
    public static final String COLUMNNAME_WetSellFFB = "WetSellFFB";

	/** Set WetSellFFB	  */
	public void setWetSellFFB (BigDecimal WetSellFFB);

	/** Get WetSellFFB	  */
	public BigDecimal getWetSellFFB();

    /** Column name WetSellSample */
    public static final String COLUMNNAME_WetSellSample = "WetSellSample";

	/** Set WetSellSample	  */
	public void setWetSellSample (BigDecimal WetSellSample);

	/** Get WetSellSample	  */
	public BigDecimal getWetSellSample();
}
