/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_PriceLabelTemplate
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PriceLabelTemplate extends PO implements I_UNS_PriceLabelTemplate, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181002L;

    /** Standard Constructor */
    public X_UNS_PriceLabelTemplate (Properties ctx, int UNS_PriceLabelTemplate_ID, String trxName)
    {
      super (ctx, UNS_PriceLabelTemplate_ID, trxName);
      /** if (UNS_PriceLabelTemplate_ID == 0)
        {
			setPriceLabelType (null);
			setPrinterType (null);
			setShopType (null);
			setUNS_PriceLabelTemplate_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PriceLabelTemplate (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PriceLabelTemplate[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Footer Template.
		@param FooterTemplate Footer Template	  */
	public void setFooterTemplate (String FooterTemplate)
	{
		set_Value (COLUMNNAME_FooterTemplate, FooterTemplate);
	}

	/** Get Footer Template.
		@return Footer Template	  */
	public String getFooterTemplate () 
	{
		return (String)get_Value(COLUMNNAME_FooterTemplate);
	}

	/** Set Header Template.
		@param HeaderTemplate Header Template	  */
	public void setHeaderTemplate (String HeaderTemplate)
	{
		set_Value (COLUMNNAME_HeaderTemplate, HeaderTemplate);
	}

	/** Get Header Template.
		@return Header Template	  */
	public String getHeaderTemplate () 
	{
		return (String)get_Value(COLUMNNAME_HeaderTemplate);
	}

	/** Set Left Template.
		@param LeftTemplate Left Template	  */
	public void setLeftTemplate (String LeftTemplate)
	{
		set_Value (COLUMNNAME_LeftTemplate, LeftTemplate);
	}

	/** Get Left Template.
		@return Left Template	  */
	public String getLeftTemplate () 
	{
		return (String)get_Value(COLUMNNAME_LeftTemplate);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Sticker = S */
	public static final String PRICELABELTYPE_Sticker = "S";
	/** Hanging = H */
	public static final String PRICELABELTYPE_Hanging = "H";
	/** Set Price Label Type.
		@param PriceLabelType Price Label Type	  */
	public void setPriceLabelType (String PriceLabelType)
	{

		set_Value (COLUMNNAME_PriceLabelType, PriceLabelType);
	}

	/** Get Price Label Type.
		@return Price Label Type	  */
	public String getPriceLabelType () 
	{
		return (String)get_Value(COLUMNNAME_PriceLabelType);
	}

	/** 1-Paper = 1P */
	public static final String PRINTERTYPE_1_Paper = "1P";
	/** 2-Paper = 2P */
	public static final String PRINTERTYPE_2_Paper = "2P";
	/** Set Printer Type.
		@param PrinterType Printer Type	  */
	public void setPrinterType (String PrinterType)
	{

		set_Value (COLUMNNAME_PrinterType, PrinterType);
	}

	/** Get Printer Type.
		@return Printer Type	  */
	public String getPrinterType () 
	{
		return (String)get_Value(COLUMNNAME_PrinterType);
	}

	/** Set Right Template.
		@param RightTemplate Right Template	  */
	public void setRightTemplate (String RightTemplate)
	{
		set_Value (COLUMNNAME_RightTemplate, RightTemplate);
	}

	/** Get Right Template.
		@return Right Template	  */
	public String getRightTemplate () 
	{
		return (String)get_Value(COLUMNNAME_RightTemplate);
	}

	/** Bazaar = BZ */
	public static final String SHOPTYPE_Bazaar = "BZ";
	/** Shop = SH */
	public static final String SHOPTYPE_Shop = "SH";
	/** Set Shop Type.
		@param ShopType Shop Type	  */
	public void setShopType (String ShopType)
	{

		set_Value (COLUMNNAME_ShopType, ShopType);
	}

	/** Get Shop Type.
		@return Shop Type	  */
	public String getShopType () 
	{
		return (String)get_Value(COLUMNNAME_ShopType);
	}

	/** Set Price Label Template.
		@param UNS_PriceLabelTemplate_ID Price Label Template	  */
	public void setUNS_PriceLabelTemplate_ID (int UNS_PriceLabelTemplate_ID)
	{
		if (UNS_PriceLabelTemplate_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelTemplate_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelTemplate_ID, Integer.valueOf(UNS_PriceLabelTemplate_ID));
	}

	/** Get Price Label Template.
		@return Price Label Template	  */
	public int getUNS_PriceLabelTemplate_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PriceLabelTemplate_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PriceLabelTemplate_UU.
		@param UNS_PriceLabelTemplate_UU UNS_PriceLabelTemplate_UU	  */
	public void setUNS_PriceLabelTemplate_UU (String UNS_PriceLabelTemplate_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelTemplate_UU, UNS_PriceLabelTemplate_UU);
	}

	/** Get UNS_PriceLabelTemplate_UU.
		@return UNS_PriceLabelTemplate_UU	  */
	public String getUNS_PriceLabelTemplate_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PriceLabelTemplate_UU);
	}
}