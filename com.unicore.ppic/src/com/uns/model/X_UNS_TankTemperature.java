/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_TankTemperature
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_TankTemperature extends PO implements I_UNS_TankTemperature, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20161215L;

    /** Standard Constructor */
    public X_UNS_TankTemperature (Properties ctx, int UNS_TankTemperature_ID, String trxName)
    {
      super (ctx, UNS_TankTemperature_ID, trxName);
      /** if (UNS_TankTemperature_ID == 0)
        {
			setTemperatureValue (Env.ZERO);
			setUNS_TankTemperature_ID (0);
			setUNS_TankTemperature_UU (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_TankTemperature (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_TankTemperature[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set ResultUOM_ID.
		@param ResultUOM_ID ResultUOM_ID	  */
	public void setResultUOM_ID (BigDecimal ResultUOM_ID)
	{
		set_Value (COLUMNNAME_ResultUOM_ID, ResultUOM_ID);
	}

	/** Get ResultUOM_ID.
		@return ResultUOM_ID	  */
	public BigDecimal getResultUOM_ID () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ResultUOM_ID);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TemperatureUOM_ID.
		@param TemperatureUOM_ID TemperatureUOM_ID	  */
	public void setTemperatureUOM_ID (BigDecimal TemperatureUOM_ID)
	{
		set_Value (COLUMNNAME_TemperatureUOM_ID, TemperatureUOM_ID);
	}

	/** Get TemperatureUOM_ID.
		@return TemperatureUOM_ID	  */
	public BigDecimal getTemperatureUOM_ID () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TemperatureUOM_ID);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Temperature Value.
		@param TemperatureValue Temperature Value	  */
	public void setTemperatureValue (BigDecimal TemperatureValue)
	{
		set_Value (COLUMNNAME_TemperatureValue, TemperatureValue);
	}

	/** Get Temperature Value.
		@return Temperature Value	  */
	public BigDecimal getTemperatureValue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TemperatureValue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Tank Temperature.
		@param UNS_TankTemperature_ID Tank Temperature	  */
	public void setUNS_TankTemperature_ID (int UNS_TankTemperature_ID)
	{
		if (UNS_TankTemperature_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_TankTemperature_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_TankTemperature_ID, Integer.valueOf(UNS_TankTemperature_ID));
	}

	/** Get Tank Temperature.
		@return Tank Temperature	  */
	public int getUNS_TankTemperature_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_TankTemperature_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_TankTemperature_UU.
		@param UNS_TankTemperature_UU UNS_TankTemperature_UU	  */
	public void setUNS_TankTemperature_UU (String UNS_TankTemperature_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_TankTemperature_UU, UNS_TankTemperature_UU);
	}

	/** Get UNS_TankTemperature_UU.
		@return UNS_TankTemperature_UU	  */
	public String getUNS_TankTemperature_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_TankTemperature_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Weight.
		@param Weight 
		Weight of a product
	  */
	public void setWeight (BigDecimal Weight)
	{
		set_ValueNoCheck (COLUMNNAME_Weight, Weight);
	}

	/** Get Weight.
		@return Weight of a product
	  */
	public BigDecimal getWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Weight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}