/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Tank
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Tank 
{

    /** TableName=UNS_Tank */
    public static final String Table_Name = "UNS_Tank";

    /** AD_Table_ID=1000317 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_ID */
    public static final String COLUMNNAME_A_Asset_ID = "A_Asset_ID";

	/** Set Asset.
	  * Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID);

	/** Get Asset.
	  * Asset used internally or by customers
	  */
	public int getA_Asset_ID();

	public org.compiere.model.I_A_Asset getA_Asset() throws RuntimeException;

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BottomVolume */
    public static final String COLUMNNAME_BottomVolume = "BottomVolume";

	/** Set Bottom Volume	  */
	public void setBottomVolume (BigDecimal BottomVolume);

	/** Get Bottom Volume	  */
	public BigDecimal getBottomVolume();

    /** Column name C_Location_ID */
    public static final String COLUMNNAME_C_Location_ID = "C_Location_ID";

	/** Set Address.
	  * Location or Address
	  */
	public void setC_Location_ID (int C_Location_ID);

	/** Get Address.
	  * Location or Address
	  */
	public int getC_Location_ID();

	public I_C_Location getC_Location() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name Diameters */
    public static final String COLUMNNAME_Diameters = "Diameters";

	/** Set Diameters	  */
	public void setDiameters (BigDecimal Diameters);

	/** Get Diameters	  */
	public BigDecimal getDiameters();

    /** Column name HeightBottomTank */
    public static final String COLUMNNAME_HeightBottomTank = "HeightBottomTank";

	/** Set Height Bottom of Tank	  */
	public void setHeightBottomTank (BigDecimal HeightBottomTank);

	/** Get Height Bottom of Tank	  */
	public BigDecimal getHeightBottomTank();

    /** Column name HeightMeasuringTable */
    public static final String COLUMNNAME_HeightMeasuringTable = "HeightMeasuringTable";

	/** Set Height Measuring Table	  */
	public void setHeightMeasuringTable (BigDecimal HeightMeasuringTable);

	/** Get Height Measuring Table	  */
	public BigDecimal getHeightMeasuringTable();

    /** Column name HMH */
    public static final String COLUMNNAME_HMH = "HMH";

	/** Set Height Measuring Holes	  */
	public void setHMH (BigDecimal HMH);

	/** Get Height Measuring Holes	  */
	public BigDecimal getHMH();

    /** Column name HMHOfMeasuringTable */
    public static final String COLUMNNAME_HMHOfMeasuringTable = "HMHOfMeasuringTable";

	/** Set Height Measuring Holes Of Measuring Table	  */
	public void setHMHOfMeasuringTable (BigDecimal HMHOfMeasuringTable);

	/** Get Height Measuring Holes Of Measuring Table	  */
	public BigDecimal getHMHOfMeasuringTable();

    /** Column name InitialLastSounding */
    public static final String COLUMNNAME_InitialLastSounding = "InitialLastSounding";

	/** Set Initial Last Sounding	  */
	public void setInitialLastSounding (BigDecimal InitialLastSounding);

	/** Get Initial Last Sounding	  */
	public BigDecimal getInitialLastSounding();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name LastSoundingDate */
    public static final String COLUMNNAME_LastSoundingDate = "LastSoundingDate";

	/** Set Last Sounding Date	  */
	public void setLastSoundingDate (Timestamp LastSoundingDate);

	/** Get Last Sounding Date	  */
	public Timestamp getLastSoundingDate();

    /** Column name LastSoundingQty */
    public static final String COLUMNNAME_LastSoundingQty = "LastSoundingQty";

	/** Set Last Sounding Quantity	  */
	public void setLastSoundingQty (BigDecimal LastSoundingQty);

	/** Get Last Sounding Quantity	  */
	public BigDecimal getLastSoundingQty();

    /** Column name MaxHeightNetVolume */
    public static final String COLUMNNAME_MaxHeightNetVolume = "MaxHeightNetVolume";

	/** Set Maximum Height from Net Volume	  */
	public void setMaxHeightNetVolume (BigDecimal MaxHeightNetVolume);

	/** Get Maximum Height from Net Volume	  */
	public BigDecimal getMaxHeightNetVolume();

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name NetVolume */
    public static final String COLUMNNAME_NetVolume = "NetVolume";

	/** Set Net Volume	  */
	public void setNetVolume (BigDecimal NetVolume);

	/** Get Net Volume	  */
	public BigDecimal getNetVolume();

    /** Column name TankHeight */
    public static final String COLUMNNAME_TankHeight = "TankHeight";

	/** Set Tank Height	  */
	public void setTankHeight (BigDecimal TankHeight);

	/** Get Tank Height	  */
	public BigDecimal getTankHeight();

    /** Column name TankNo */
    public static final String COLUMNNAME_TankNo = "TankNo";

	/** Set Tank No	  */
	public void setTankNo (String TankNo);

	/** Get Tank No	  */
	public String getTankNo();

    /** Column name TankStatus */
    public static final String COLUMNNAME_TankStatus = "TankStatus";

	/** Set Tank Status	  */
	public void setTankStatus (String TankStatus);

	/** Get Tank Status	  */
	public String getTankStatus();

    /** Column name TypeOfRoof */
    public static final String COLUMNNAME_TypeOfRoof = "TypeOfRoof";

	/** Set Type Of Roof	  */
	public void setTypeOfRoof (String TypeOfRoof);

	/** Get Type Of Roof	  */
	public String getTypeOfRoof();

    /** Column name UNS_Tank_ID */
    public static final String COLUMNNAME_UNS_Tank_ID = "UNS_Tank_ID";

	/** Set Tank	  */
	public void setUNS_Tank_ID (int UNS_Tank_ID);

	/** Get Tank	  */
	public int getUNS_Tank_ID();

    /** Column name UNS_Tank_UU */
    public static final String COLUMNNAME_UNS_Tank_UU = "UNS_Tank_UU";

	/** Set UNS_Tank_UU	  */
	public void setUNS_Tank_UU (String UNS_Tank_UU);

	/** Get UNS_Tank_UU	  */
	public String getUNS_Tank_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();

    /** Column name VolumeCm */
    public static final String COLUMNNAME_VolumeCm = "VolumeCm";

	/** Set Volume per cm	  */
	public void setVolumeCm (BigDecimal VolumeCm);

	/** Get Volume per cm	  */
	public BigDecimal getVolumeCm();
}
