/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Properties;

import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPriceLabelTemplate extends X_UNS_PriceLabelTemplate {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5042545469330098006L;

	/**
	 * @param ctx
	 * @param UNS_PriceLabelTemplate_ID
	 * @param trxName
	 */
	public MUNSPriceLabelTemplate(Properties ctx,
			int UNS_PriceLabelTemplate_ID, String trxName) {
		super(ctx, UNS_PriceLabelTemplate_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPriceLabelTemplate(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public String getCompleteTemplate(MUNSProductList list, int QtyInt, String labelName)
	{
		String patternUSD = "###,###.##";
		String patternIDR = "###";
		DecimalFormat decimalFormatUSD = new DecimalFormat(patternUSD);
		DecimalFormat decimalFormatIDR = new DecimalFormat(patternIDR);
		String header = getHeaderTemplate().trim();
		String left = getLeftTemplate().trim();
		String right = getRightTemplate() == null ? null : getRightTemplate().trim();
		String footer = getFooterTemplate().trim();
		
		BigDecimal IDRAmtK = list.getIDRAmount().divide(Env.ONETHOUSAND, 0);
		String IDRString = decimalFormatIDR.format(IDRAmtK);
		String USDString = decimalFormatUSD.format(list.getUSDAmount());
		
		String templateTxt = null;
		if(header != null)
			templateTxt += header;
		if(left != null)
			templateTxt += "\n" + left;
		if(right != null && getPrinterType().equals(MUNSPriceLabelTemplate.PRINTERTYPE_2_Paper) && QtyInt != 1)
			templateTxt += "\n" + right;
		if(footer != null)
			templateTxt += "\n" + footer;
		
		if(getPrinterType().equals(MUNSPriceLabelTemplate.PRINTERTYPE_2_Paper))
		{
			boolean isGanjil = QtyInt % 2 == 1;
			if(isGanjil && QtyInt != 1)
			{
				QtyInt = QtyInt - 1;
			}
			
			if(QtyInt != 1)
				QtyInt = QtyInt / 2;
		}
		
		templateTxt = templateTxt.replaceAll("@SKU@", list.getSKU());
		templateTxt = templateTxt.replaceAll("@PC@", list.getCategoryCode());
		templateTxt = templateTxt.replaceAll("@KODE@", list.getDateCode());
		templateTxt = templateTxt.replaceAll("@ETBB@", list.getETBBCode());
		templateTxt = templateTxt.replaceAll("@LabelName@", labelName);
		templateTxt = templateTxt.replaceAll("@IDR@", IDRString + "K");
		templateTxt = templateTxt.replaceAll("@USD@", USDString);
		templateTxt = templateTxt.replaceAll("@COUNT@", String.valueOf(QtyInt));
		
		return templateTxt;
	}
}