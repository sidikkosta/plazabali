/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;

/**
 * @author menjangan
 *
 */
public class MUNSMOSchedulerLines extends X_UNS_MO_SchedulerLines {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param ctx
	 * @param UNS_MO_SchedulerLines_ID
	 * @param trxName
	 */
	public MUNSMOSchedulerLines(Properties ctx, int UNS_MO_SchedulerLines_ID,
			String trxName) {
		super(ctx, UNS_MO_SchedulerLines_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSMOSchedulerLines(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSMOScheduler getParent()
	{
		return new MUNSMOScheduler(getCtx(), getUNS_MO_Scheduler_ID(), get_TrxName());
	}
	
	public static MUNSMOSchedulerLines getCreate(Properties ctx, MUNSMOScheduler s, int UNS_ProductionSchedule_ID, 
			int UNS_Resource_ID, String trxName)
	{
		MUNSMOSchedulerLines line = Query.get(ctx, UNSPPICModelFactory.getExtensionID(),
				Table_Name, COLUMNNAME_UNS_ProductionSchedule_ID + "=? AND "
				+ COLUMNNAME_UNS_Resource_ID + "=?", trxName)
				.setParameters(UNS_ProductionSchedule_ID, UNS_Resource_ID).first();
		
		if(line == null)
		{
			line = new MUNSMOSchedulerLines(ctx, 0, trxName);
			line.setUNS_MO_Scheduler_ID(s.get_ID());
			line.setClientOrg(s);
			line.setUNS_ProductionSchedule_ID(UNS_ProductionSchedule_ID);
			line.setUNS_Resource_ID(UNS_Resource_ID);
			line.setDescription("Auto generate from parent");
			line.saveEx();
		}
		
		return line;
	}
	
	public MUNSMOSchedulerLines[] getLines()
	{
		List<MUNSMOSchedulerLines> list = new ArrayList<>();
		
		list = Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(), Table_Name, 
				COLUMNNAME_UNS_MO_Scheduler_ID + "=?", get_TrxName()).setParameters(getUNS_MO_Scheduler_ID())
				.list();
		
		return list.toArray(new MUNSMOSchedulerLines[list.size()]);
	}
}
