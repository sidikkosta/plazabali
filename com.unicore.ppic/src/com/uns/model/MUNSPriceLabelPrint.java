/**
 * 
 */
package com.uns.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MCurrency;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInventoryLineMA;
import org.compiere.model.MLocator;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPrice;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.uns.base.model.MInOut;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSPPICModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPriceLabelPrint extends X_UNS_PriceLabelPrint implements DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3031737354740601154L;

	/**
	 * @param ctx
	 * @param UNS_PriceLabelPrint_ID
	 * @param trxName
	 */
	public MUNSPriceLabelPrint(Properties ctx, int UNS_PriceLabelPrint_ID,
			String trxName) {
		super(ctx, UNS_PriceLabelPrint_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPriceLabelPrint(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_PriceLabelPrint_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		if(getDocSourceType().equals(DOCSOURCETYPE_PriceListConversion) && getM_Warehouse_ID() <= 0)
		{
			log.saveError("Error", "Warehouse field is Mandatory.!");
			return false;
		}
		else if(getDocSourceType().equals(DOCSOURCETYPE_MaterialReceipt)
				|| getDocSourceType().equals(DOCSOURCETYPE_SKUConversion))
			setM_Warehouse_ID(-1);
		
		if((getDocSourceType().equals(DOCSOURCETYPE_MaterialReceipt)
				|| getDocSourceType().equals(DOCSOURCETYPE_SKUConversion))
					&& getM_PriceList_Version_ID() <= 0)
		{
			String sql = "SELECT M_PriceList_Version_ID FROM M_PriceList_Version"
					+ " WHERE M_PriceList_ID = ? AND ValidFrom <= ? AND IsActive = 'Y' ORDER BY ValidFrom DESC";
			int plvID = DB.getSQLValue(get_TrxName(), sql, getM_PriceList_ID(), getValidFrom());
			
			if(plvID > 0)
				setM_PriceList_Version_ID(plvID);
		}
		
		return true;
	}	
	
	protected boolean beforeDelete()
	{
		if(!deleteLines())
		{
			log.saveError("Error", "Failed when trying delete list.");
			return false;
		}
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		String msg = null;
		
		if(newRecord)
		{
			msg = generateList();
		}
		else if(!newRecord && (is_ValueChanged(COLUMNNAME_SrcDocumentNo) || is_ValueChanged(COLUMNNAME_DocSourceType)
				|| is_ValueChanged(COLUMNNAME_M_PriceList_Version_ID) || is_ValueChanged(COLUMNNAME_M_Warehouse_ID)))
		{
			if(!deleteLines())
			{
				log.saveError("Error", "Failed when trying delete list.");
				return false;
			}
			
			msg = generateList();
		}
		
		if(null != msg)
		{
			log.saveError("Error", msg);
			return false;
		}
		return true;
	}
	
	private String generateList()
	{
		if(getDocSourceType() == null)
			return "";
		if(getDocSourceType().equals(DOCSOURCETYPE_MaterialReceipt))
			return createFromMReceipt();
		else if(getDocSourceType().equals(DOCSOURCETYPE_PriceListConversion))
			return createFromPLConversion();
		else if(getDocSourceType().equals(DOCSOURCETYPE_PriceListVersion))
			return createFromPLVersion();
		else if(getDocSourceType().equals(DOCSOURCETYPE_SKUConversion))
			return createFromProductConversion();
		
		return null;
	}
	
	private String createFromMReceipt()
	{
		String sql = "SELECT M_InOut_ID FROM M_InOut WHERE IsSOTrx = 'N' AND DocStatus IN ('CO', 'CL')"
				+ " AND DocumentNo = ?";
		int inoutID = DB.getSQLValue(get_TrxName(), sql, getSrcDocumentNo());
		
		if(inoutID <= 0)
			return "Could not find receipt document " + getSrcDocumentNo();
		
		MInOut io = new MInOut(getCtx(), inoutID, get_TrxName());
		
		String code = null;
		if(io.getBCDate() == null)
			code = analyzeDateCode(io.getMovementDate());
		else
			code = analyzeDateCode(io.getBCDate());
		
		MInOutLine[] lines = io.getLines();
		BigDecimal rate = MUNSConversionPriceRate.getRate(MCurrency.get(getCtx(), "USD").get_ID(),
				MCurrency.get(getCtx(), "IDR").get_ID(), io.getMovementDate(), getAD_Org_ID(), get_TrxName());
		if(rate == null)
			return "Rate on conversion price not found.";
		for(int i=0;i<lines.length;i++)
		{
			MProductPrice pp = MProductPrice.get(
					getCtx(), getM_PriceList_Version_ID(), lines[i].getM_Product_ID(), get_TrxName());
			BigDecimal price = Env.ZERO;
			if(pp == null)
			{
				sql = "SELECT PriceList FROM M_ProductPrice pp"
						+ " INNER JOIN M_PriceList_Version plv ON plv.M_PriceList_Version_ID"
						+ " = pp.M_PriceList_Version_ID"
						+ " INNER JOIN M_PriceList p ON p.M_PriceList_ID = plv.M_PriceList_ID"
						+ " WHERE p.IsSOPriceList = 'Y' AND p.C_Currency_ID = 303 AND"
						+ " pp.M_Product_ID = ? AND plv.M_PriceList_ID = ? AND plv.ValidFrom <= ?"
						+ " AND plv.IsActive = 'Y' ORDER BY plv.ValidFrom DESC";
				price = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
					lines[i].getM_Product_ID(), getM_PriceList_ID(), getValidFrom()
					});
				if(price == null)
					return "Product is not on price list. " + lines[i].getM_Product().getName();
			}
			else
				price = pp.getPriceList();
			
			MUNSProductList list = new MUNSProductList(getCtx(), 0, get_TrxName());
			list.setUNS_PriceLabelPrint_ID(get_ID());
			list.setAD_Org_ID(getAD_Org_ID());
			list.setETBBCode(lines[i].getM_AttributeSetInstance().getDescription());
			list.setProductName(lines[i].getProduct().getName());
			list.setQty(lines[i].getMovementQty());
			list.setSKU(lines[i].getProduct().getSKU());
			list.setIDRAmount(price);
			list.setPriceLabelType(lines[i].getProduct().getPriceLabelType());
			String sql1 = "SELECT getproductcategorytopparent(?)";
			int topParentCategoryID = DB.getSQLValue(get_TrxName(), sql1, lines[i].getM_Product_ID());
			MProductCategory category = new MProductCategory(getCtx(), topParentCategoryID, get_TrxName());
			list.setCategoryCode(category.getValue());
			list.setDateCode(code);
			list.setIsManual(false);
			BigDecimal convert = price.divide(rate, 2);
			list.setUSDAmount(convert);
			if(!list.save())
				return CLogger.retrieveErrorString("Failed when trying create list.");
		}
		
		String sql1 = "UPDATE UNS_PriceLabelPrint SET ValidFrom = ?"
				+ " WHERE UNS_PriceLabelPrint_ID = ?";
		DB.executeUpdate(sql1, new Object[]{io.getMovementDate(), get_ID()}, false, get_TrxName());
			
		return null;
	}
	
	private String createFromPLConversion()
	{
		String sql = "SELECT UNS_PriceListConv_ID FROM UNS_PriceListConv WHERE DocStatus IN ('CO', 'CL')"
				+ " AND DocumentNo = ?";
		int plConvID = DB.getSQLValue(get_TrxName(), sql, getSrcDocumentNo());
		
		if(plConvID <= 0)
			return "Could not find price list conversion document " + getSrcDocumentNo();
		
		MUNSPriceListConv conv = new MUNSPriceListConv(getCtx(), plConvID, get_TrxName());
		
		sql = "UPDATE UNS_PriceLabelPrint SET ValidFrom = ?, M_PriceList_ID = ?, M_PriceList_Version_ID =?"
				+ " WHERE UNS_PriceLabelPrint_ID = ?";
		DB.executeUpdate(sql, new Object[]{conv.getValidFrom(), conv.getPriceListTo_ID(), 
				conv.getPL_VersionTo_ID(), get_ID()}, false, get_TrxName());
		MUNSPLConvProduct[] p = conv.getProducts();
		MWarehouse whs = MWarehouse.get(getCtx(), getM_Warehouse_ID());
		MLocator[] locator = whs.getLocators(false);
		String code = analyzeDateCode(conv.getValidFrom());
		for(int x=0;x<p.length;x++)
		{
			MProduct product = MProduct.get(getCtx(), p[x].getM_Product_ID());
			BigDecimal price = (MProductPrice.get(
					getCtx(), conv.getPL_VersionTo_ID(), product.get_ID(), get_TrxName())).getPriceList();
			for(int y=0;y<locator.length;y++)
			{
				MStorageOnHand[] storage = MStorageOnHand.getAll(
						getCtx(), product.get_ID(), locator[y].get_ID(), get_TrxName(), true, 0);
				for(int z=0;z<storage.length;z++)
				{
					if(storage[z].getQtyOnHand().signum() <= 0)
						continue;
					MUNSProductList list = new MUNSProductList(getCtx(), 0, get_TrxName());
					list.setUNS_PriceLabelPrint_ID(get_ID());
					list.setAD_Org_ID(getAD_Org_ID());
					list.setETBBCode(storage[z].getM_AttributeSetInstance().getDescription());
					list.setProductName(product.getName());
					list.setQty(storage[z].getQtyOnHand());
					list.setSKU(product.getSKU());
					list.setIDRAmount(price);
					list.setPriceLabelType(product.getPriceLabelType());
					String sql1 = "SELECT getproductcategorytopparent(?)";
					int topParentCategoryID = DB.getSQLValue(get_TrxName(), sql1, product.get_ID());
					MProductCategory category = new MProductCategory(getCtx(), topParentCategoryID, get_TrxName());
					list.setCategoryCode(category.getValue());
					list.setIsManual(false);
					list.setUSDAmount(p[x].getPriceListSource());
					list.setDateCode(code);
					if(!list.save())
						return "Failed when trying create list.";
				}
			}
		}
		
		String sql1 = "UPDATE UNS_PriceLabelPrint SET ValidFrom = ?"
				+ " WHERE UNS_PriceLabelPrint_ID = ?";
		DB.executeUpdate(sql1, new Object[]{conv.getValidFrom(), get_ID()}, false, get_TrxName());
		
		return null;
	}
	
	private String createFromPLVersion()
	{
		if(getM_PriceList_Version_ID() <= 0)
			return null;
		
		MPriceListVersion v = new MPriceListVersion(getCtx(), getM_PriceList_Version_ID(), get_TrxName());
		MProductPrice[] pp = v.getProductPrice(false);
		MWarehouse whs = MWarehouse.get(getCtx(), getM_Warehouse_ID());
		MLocator[] locator = whs.getLocators(false);
		BigDecimal rate = MUNSConversionPriceRate.getRate(MCurrency.get(getCtx(), "USD").get_ID(),
				MCurrency.get(getCtx(), "IDR").get_ID(), v.getValidFrom(), getAD_Org_ID(), get_TrxName());
		String code = analyzeDateCode(v.getValidFrom());
		for(int x=0;x<pp.length;x++)
		{
			MProduct product = MProduct.get(getCtx(), pp[x].getM_Product_ID());
			BigDecimal price = pp[x].getPriceLimit();
			
			for(int y=0;y<locator.length;y++)
			{
				MStorageOnHand[] storage = MStorageOnHand.getAll(
						getCtx(), product.get_ID(), locator[y].get_ID(), get_TrxName(), true, 0);
				for(int z=0;z<storage.length;z++)
				{
					if(storage[z].getQtyOnHand().signum() <= 0)
						continue;
					MUNSProductList list = new MUNSProductList(getCtx(), 0, get_TrxName());
					list.setUNS_PriceLabelPrint_ID(get_ID());
					list.setAD_Org_ID(getAD_Org_ID());
					list.setETBBCode(storage[z].getM_AttributeSetInstance().getDescription());
					list.setProductName(product.getName());
					list.setQty(storage[z].getQtyOnHand());
					list.setSKU(product.getSKU());
					list.setIDRAmount(price);
					list.setPriceLabelType(product.getPriceLabelType());
					String sql1 = "SELECT getproductcategorytopparent(?)";
					int topParentCategoryID = DB.getSQLValue(get_TrxName(), sql1, product.get_ID());
					MProductCategory category = new MProductCategory(getCtx(), topParentCategoryID, get_TrxName());
					list.setCategoryCode(category.getValue());
					list.setIsManual(false);
					list.setUSDAmount(price.divide(rate, 2));
					list.setDateCode(code);
					if(!list.save())
						return "Failed when trying create list.";
				}
			}	
		}
		
		String sql1 = "UPDATE UNS_PriceLabelPrint SET ValidFrom = ?"
				+ " WHERE UNS_PriceLabelPrint_ID = ?";
		DB.executeUpdate(sql1, new Object[]{v.getValidFrom(), get_ID()}, false, get_TrxName());
		
		return null;
	}
	
	private String createFromProductConversion()
	{
		String sql = "SELECT M_Inventory_ID FROM M_Inventory WHERE DocStatus IN ('CO', 'CL')"
				+ " AND C_DocType_ID IN (SELECT dt.C_DocType_ID FROM C_DocType dt"
				+ " WHERE dt.DocSubTypeInv = 'PC') AND DocumentNo = ?";
		int invID = DB.getSQLValue(get_TrxName(), sql, getSrcDocumentNo());
		if(invID <= 0)
			return "Could not find product convertion document " + getSrcDocumentNo();
		
		MInventory inv = new MInventory(getCtx(), invID, get_TrxName());
		MInventoryLine[] lines = inv.getLines(false);
		
		String code = analyzeDateCode(inv.getMovementDate());
		
		BigDecimal rate = MUNSConversionPriceRate.getRate(MCurrency.get(getCtx(), "USD").get_ID(),
				MCurrency.get(getCtx(), "IDR").get_ID(), inv.getMovementDate(), getAD_Org_ID(), get_TrxName());
		if(rate == null)
			return "Rate on conversion price not found.";
		
		for(int i=0;i<lines.length;i++)
		{
			if(lines[i].getQtyInternalUse().signum() <= 0)
				continue;
			
			MInventoryLineMA[] mas = MInventoryLineMA.get(getCtx(), lines[i].get_ID(), get_TrxName());
			for(int j=0;j<mas.length;j++)
			{
				MProductPrice pp = MProductPrice.get(
						getCtx(), getM_PriceList_Version_ID(), lines[i].getM_Product_ID(), get_TrxName());
				BigDecimal price = Env.ZERO;
				if(pp == null)
				{
					sql = "SELECT PriceList FROM M_ProductPrice pp"
							+ " INNER JOIN M_PriceList_Version plv ON plv.M_PriceList_Version_ID"
							+ " = pp.M_PriceList_Version_ID"
							+ " INNER JOIN M_PriceList p ON p.M_PriceList_ID = plv.M_PriceList_ID = ?"
							+ " WHERE pp.M_Product_ID = ? AND plv.M_PriceList_ID = ? AND plv.ValidFrom <= ?"
							+ " AND plv.IsActive = 'Y' AND p.IsSOPriceList = 'Y'"
							+ " AND p.C_Currency_ID = 303 ORDER BY plv.ValidFrom DESC";
					price = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
						lines[i].getM_Product_ID(), getM_PriceList_ID(), getValidFrom()
						});
					if(price == null)
						return "Product is not on price list. " + lines[i].getM_Product().getName();
				}
				else
					price = pp.getPriceList();
				
				MUNSProductList list = new MUNSProductList(getCtx(), 0, get_TrxName());
				list.setUNS_PriceLabelPrint_ID(get_ID());
				list.setAD_Org_ID(getAD_Org_ID());
				list.setETBBCode(mas[j].getM_AttributeSetInstance().getDescription());
				list.setProductName(lines[i].getProduct().getName());
				list.setQty(mas[j].getMovementQty());
				list.setSKU(lines[i].getProduct().getSKU());
				list.setIDRAmount(price);
				list.setPriceLabelType(lines[i].getProduct().getPriceLabelType());
				String sql1 = "SELECT getproductcategorytopparent(?)";
				int topParentCategoryID = DB.getSQLValue(get_TrxName(), sql1, lines[i].getM_Product_ID());
				MProductCategory category = new MProductCategory(getCtx(), topParentCategoryID, get_TrxName());
				list.setCategoryCode(category.getValue());
				list.setDateCode(code);
				list.setIsManual(false);
				BigDecimal convert = price.divide(rate, 2);
				list.setUSDAmount(convert);
				if(!list.save())
					return CLogger.retrieveErrorString("Failed when trying create list.");
			}
		}
		
		return null;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private boolean deleteLines()
	{
		String sql = "DELETE FROM UNS_ProductList WHERE UNS_PriceLabelPrint_ID = ?";
		return DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0 ? true : false;
	}
	
	public MUNSProductList[] getProducts(String PriceLabelType)
	{
		String whereClause = "UNS_PriceLabelPrint_ID = ?";
		if(null != PriceLabelType)
			whereClause += " AND PriceLabelType = ?";
		Query query = Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(),
				MUNSProductList.Table_Name, whereClause, get_TrxName());
		if(null != PriceLabelType)
			query.setParameters(get_ID(), PriceLabelType);
		else
			query.setParameters(get_ID());
		
		List<MUNSProductList> list = query.list();
		
		return list.toArray(new MUNSProductList[list.size()]);
	}
	
	public static void main (String[] args)
	{
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		Integer code[] = {0,9,7,5,3,1,8,6,4,2};
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, 10);
		
		String year = (new Integer (cal.get(Calendar.YEAR))).toString();
		year = year.substring(3, 4);
		int yearCode = Integer.valueOf(year);
		year = code[yearCode].toString();
		String month1 = null;
		String month2 = null;
		String month = (new Integer (cal.get(Calendar.MONTH))).toString();
		if(month.length() == 1)
		{
			month1 = code[0].toString();
			month2 = code[Integer.valueOf(month)].toString();
		}
		else
		{
			month1 = code[(Integer.valueOf(month.substring(0, 1)))].toString();
			month2 = code[(Integer.valueOf(month.substring(1, 2)))].toString();
		}
		
		month = month1 + "" + month2;
		System.out.println(year);
		System.out.println(month);
		String os = System.getProperty("os.name");
		System.out.println(os);
		
		try {
			Runtime.getRuntime().exec(new String[] {"cmd", "echo abc"});
			System.out.println(Runtime.getRuntime().toString());
		} catch (IOException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		try {
            // Run "netsh" Windows command
            Process process = Runtime.getRuntime().exec("Ping www.google.com");

            // Get input streams
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            // Read command standard output
            String s;
            System.out.println("Standard output: ");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

            // Read command errors
            System.out.println("Standard error: ");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
	}
	
	private String analyzeDateCode(java.sql.Timestamp date)
	{
		Integer code[] = {0,9,7,5,3,1,8,6,4,2};
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		String year = (new Integer (cal.get(Calendar.YEAR))).toString();
		year = year.substring(3, 4);
		int yearCode = Integer.valueOf(year);
		year = code[yearCode].toString();
		String month1 = null;
		String month2 = null;
		String month = (new Integer (cal.get(Calendar.MONTH) + 1)).toString();
		if(month.length() == 1)
		{
			month1 = code[0].toString();
			month2 = code[Integer.valueOf(month)].toString();
		}
		else
		{
			month1 = code[(Integer.valueOf(month.substring(0, 1)))].toString();
			month2 = code[(Integer.valueOf(month.substring(1, 2)))].toString();
		}
		
		month = month1 + "" + month2;
		
		return year + "" + month;
	}
}