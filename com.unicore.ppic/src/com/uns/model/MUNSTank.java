package com.uns.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;


public class MUNSTank extends X_UNS_Tank {
	
	/**
	 * @author Guntur
	 *
	 */
	private static final long serialVersionUID = 2174944713013711989L;
	private static BigDecimal ENV_TWO = new BigDecimal(2);
	private static BigDecimal phi = new BigDecimal(3.14);
	
	public MUNSTank(Properties ctx, int UNS_Tank_ID, String trxName) {
		super(ctx, UNS_Tank_ID, trxName);
		
	}

	public MUNSTank(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}

	public static BigDecimal calculate(Properties ctx, int UNS_Tank_ID, BigDecimal height,
			BigDecimal temperature, String trxName){
		
		BigDecimal weight = null;
		MUNSTank tank = new MUNSTank(ctx, UNS_Tank_ID, trxName);
		
		BigDecimal heighttable = tank.getHeightMeasuringTable();
		BigDecimal grossheight = heighttable.add(height);
		BigDecimal density = MUNSTankTemperature.getDensity(temperature, trxName);
		
		if(TANKSTATUS_Calibrated.equals(tank.getTankStatus())){
			
			BigDecimal fraction = MUNSTankFraction.getFraction(UNS_Tank_ID, grossheight, trxName);
			
			weight = fraction.multiply(density).setScale(0, RoundingMode.HALF_UP);
				
		}
		else if(TANKSTATUS_StandardFormula.equals(tank.getTankStatus()))
		{
			
			BigDecimal radius = tank.getDiameters().divide(ENV_TWO);
			BigDecimal weightfirst = grossheight.multiply(density.multiply(phi.multiply(radius.multiply(radius))));
			weight = weightfirst.divide(BigDecimal.valueOf(1000), 0, RoundingMode.HALF_UP);
			
		}
		else if(TANKSTATUS_MeasuredFormula .equals(tank.getTankStatus()))
		{
			BigDecimal firstcalculate = tank.getBottomVolume().add(height.multiply(tank.getVolumeCm()));
			
			weight = firstcalculate.multiply(density).setScale(0, RoundingMode.HALF_UP);
		}
		
		return weight;
	}
}
