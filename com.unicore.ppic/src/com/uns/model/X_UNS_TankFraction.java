/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_TankFraction
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_TankFraction extends PO implements I_UNS_TankFraction, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20161124L;

    /** Standard Constructor */
    public X_UNS_TankFraction (Properties ctx, int UNS_TankFraction_ID, String trxName)
    {
      super (ctx, UNS_TankFraction_ID, trxName);
      /** if (UNS_TankFraction_ID == 0)
        {
			setHeightValue (Env.ZERO);
			setUNS_TankFraction_ID (0);
			setUNS_TankFraction_UU (null);
			setUNS_Tank_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_TankFraction (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_TankFraction[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Height Value.
		@param HeightValue Height Value	  */
	public void setHeightValue (BigDecimal HeightValue)
	{
		set_Value (COLUMNNAME_HeightValue, HeightValue);
	}

	/** Get Height Value.
		@return Height Value	  */
	public BigDecimal getHeightValue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HeightValue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Tank Fraction.
		@param UNS_TankFraction_ID Tank Fraction	  */
	public void setUNS_TankFraction_ID (int UNS_TankFraction_ID)
	{
		if (UNS_TankFraction_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_TankFraction_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_TankFraction_ID, Integer.valueOf(UNS_TankFraction_ID));
	}

	/** Get Tank Fraction.
		@return Tank Fraction	  */
	public int getUNS_TankFraction_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_TankFraction_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_TankFraction_UU.
		@param UNS_TankFraction_UU UNS_TankFraction_UU	  */
	public void setUNS_TankFraction_UU (String UNS_TankFraction_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_TankFraction_UU, UNS_TankFraction_UU);
	}

	/** Get UNS_TankFraction_UU.
		@return UNS_TankFraction_UU	  */
	public String getUNS_TankFraction_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_TankFraction_UU);
	}

	public com.uns.model.I_UNS_Tank getUNS_Tank() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Tank)MTable.get(getCtx(), com.uns.model.I_UNS_Tank.Table_Name)
			.getPO(getUNS_Tank_ID(), get_TrxName());	}

	/** Set Tank.
		@param UNS_Tank_ID Tank	  */
	public void setUNS_Tank_ID (int UNS_Tank_ID)
	{
		if (UNS_Tank_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Tank_ID, Integer.valueOf(UNS_Tank_ID));
	}

	/** Get Tank.
		@return Tank	  */
	public int getUNS_Tank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Tank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_TankRing getUNS_TankRing() throws RuntimeException
    {
		return (com.uns.model.I_UNS_TankRing)MTable.get(getCtx(), com.uns.model.I_UNS_TankRing.Table_Name)
			.getPO(getUNS_TankRing_ID(), get_TrxName());	}

	/** Set Tank Ring.
		@param UNS_TankRing_ID Tank Ring	  */
	public void setUNS_TankRing_ID (int UNS_TankRing_ID)
	{
		if (UNS_TankRing_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_TankRing_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_TankRing_ID, Integer.valueOf(UNS_TankRing_ID));
	}

	/** Get Tank Ring.
		@return Tank Ring	  */
	public int getUNS_TankRing_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_TankRing_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Volume.
		@param Volume 
		Volume of a product
	  */
	public void setVolume (BigDecimal Volume)
	{
		set_ValueNoCheck (COLUMNNAME_Volume, Volume);
	}

	/** Get Volume.
		@return Volume of a product
	  */
	public BigDecimal getVolume () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Volume);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}