/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPrintLabelTemplate extends X_UNS_PrintLabelTemplate {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7321163105898503338L;

	/**
	 * @param ctx
	 * @param UNS_PrintLabelTemplate_ID
	 * @param trxName
	 */
	public MUNSPrintLabelTemplate(Properties ctx,
			int UNS_PrintLabelTemplate_ID, String trxName) {
		super(ctx, UNS_PrintLabelTemplate_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPrintLabelTemplate(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

}
