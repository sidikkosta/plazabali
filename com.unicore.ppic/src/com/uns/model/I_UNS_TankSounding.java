/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_TankSounding
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_TankSounding 
{

    /** TableName=UNS_TankSounding */
    public static final String Table_Name = "UNS_TankSounding";

    /** AD_Table_ID=1000325 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DeliveredQty */
    public static final String COLUMNNAME_DeliveredQty = "DeliveredQty";

	/** Set Delivered Quantity.
	  * Quantity of Delivered
	  */
	public void setDeliveredQty (BigDecimal DeliveredQty);

	/** Get Delivered Quantity.
	  * Quantity of Delivered
	  */
	public BigDecimal getDeliveredQty();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DifferenceQty */
    public static final String COLUMNNAME_DifferenceQty = "DifferenceQty";

	/** Set Difference.
	  * Difference Quantity
	  */
	public void setDifferenceQty (BigDecimal DifferenceQty);

	/** Get Difference.
	  * Difference Quantity
	  */
	public BigDecimal getDifferenceQty();

    /** Column name DOBI */
    public static final String COLUMNNAME_DOBI = "DOBI";

	/** Set DOBI	  */
	public void setDOBI (BigDecimal DOBI);

	/** Get DOBI	  */
	public BigDecimal getDOBI();

    /** Column name FFA */
    public static final String COLUMNNAME_FFA = "FFA";

	/** Set FFA.
	  * Free Fatty Acid
	  */
	public void setFFA (BigDecimal FFA);

	/** Get FFA.
	  * Free Fatty Acid
	  */
	public BigDecimal getFFA();

    /** Column name HeightMeasured */
    public static final String COLUMNNAME_HeightMeasured = "HeightMeasured";

	/** Set Height Measured (cm)	  */
	public void setHeightMeasured (BigDecimal HeightMeasured);

	/** Get Height Measured (cm)	  */
	public BigDecimal getHeightMeasured();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name isEmpty */
    public static final String COLUMNNAME_isEmpty = "isEmpty";

	/** Set Empty	  */
	public void setisEmpty (boolean isEmpty);

	/** Get Empty	  */
	public boolean isEmpty();

    /** Column name LastSoundingQty */
    public static final String COLUMNNAME_LastSoundingQty = "LastSoundingQty";

	/** Set Last Sounding Quantity	  */
	public void setLastSoundingQty (BigDecimal LastSoundingQty);

	/** Get Last Sounding Quantity	  */
	public BigDecimal getLastSoundingQty();

    /** Column name MI */
    public static final String COLUMNNAME_MI = "MI";

	/** Set MI.
	  * Moisture & Impurity
	  */
	public void setMI (BigDecimal MI);

	/** Get MI.
	  * Moisture & Impurity
	  */
	public BigDecimal getMI();

    /** Column name RecycledQty */
    public static final String COLUMNNAME_RecycledQty = "RecycledQty";

	/** Set Recycled Qty	  */
	public void setRecycledQty (BigDecimal RecycledQty);

	/** Get Recycled Qty	  */
	public BigDecimal getRecycledQty();

    /** Column name SoundingBy */
    public static final String COLUMNNAME_SoundingBy = "SoundingBy";

	/** Set Sounding By	  */
	public void setSoundingBy (String SoundingBy);

	/** Get Sounding By	  */
	public String getSoundingBy();

    /** Column name Temperature */
    public static final String COLUMNNAME_Temperature = "Temperature";

	/** Set Temperature.
	  * The temperature
	  */
	public void setTemperature (BigDecimal Temperature);

	/** Get Temperature.
	  * The temperature
	  */
	public BigDecimal getTemperature();

    /** Column name UNS_SoundingBy_ID */
    public static final String COLUMNNAME_UNS_SoundingBy_ID = "UNS_SoundingBy_ID";

	/** Set Sounding By	  */
	public void setUNS_SoundingBy_ID (int UNS_SoundingBy_ID);

	/** Get Sounding By	  */
	public int getUNS_SoundingBy_ID();

	public org.compiere.model.I_AD_User getUNS_SoundingBy() throws RuntimeException;

    /** Column name UNS_Sounding_ID */
    public static final String COLUMNNAME_UNS_Sounding_ID = "UNS_Sounding_ID";

	/** Set Sounding	  */
	public void setUNS_Sounding_ID (int UNS_Sounding_ID);

	/** Get Sounding	  */
	public int getUNS_Sounding_ID();

	public com.uns.model.I_UNS_Sounding getUNS_Sounding() throws RuntimeException;

    /** Column name UNS_Tank_ID */
    public static final String COLUMNNAME_UNS_Tank_ID = "UNS_Tank_ID";

	/** Set Tank	  */
	public void setUNS_Tank_ID (int UNS_Tank_ID);

	/** Get Tank	  */
	public int getUNS_Tank_ID();

	public com.uns.model.I_UNS_Tank getUNS_Tank() throws RuntimeException;

    /** Column name UNS_TankSounding_ID */
    public static final String COLUMNNAME_UNS_TankSounding_ID = "UNS_TankSounding_ID";

	/** Set Tank Sounding	  */
	public void setUNS_TankSounding_ID (int UNS_TankSounding_ID);

	/** Get Tank Sounding	  */
	public int getUNS_TankSounding_ID();

    /** Column name UNS_TankSounding_UU */
    public static final String COLUMNNAME_UNS_TankSounding_UU = "UNS_TankSounding_UU";

	/** Set UNS_TankSounding_UU	  */
	public void setUNS_TankSounding_UU (String UNS_TankSounding_UU);

	/** Get UNS_TankSounding_UU	  */
	public String getUNS_TankSounding_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Weight */
    public static final String COLUMNNAME_Weight = "Weight";

	/** Set Weight.
	  * Weight of a product
	  */
	public void setWeight (BigDecimal Weight);

	/** Get Weight.
	  * Weight of a product
	  */
	public BigDecimal getWeight();

    /** Column name WeightByFlowMeter */
    public static final String COLUMNNAME_WeightByFlowMeter = "WeightByFlowMeter";

	/** Set Weight By Flow Meter	  */
	public void setWeightByFlowMeter (BigDecimal WeightByFlowMeter);

	/** Get Weight By Flow Meter	  */
	public BigDecimal getWeightByFlowMeter();
}
