/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Production_Specs
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Production_Specs extends PO implements I_UNS_Production_Specs, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150911L;

    /** Standard Constructor */
    public X_UNS_Production_Specs (Properties ctx, int UNS_Production_Specs_ID, String trxName)
    {
      super (ctx, UNS_Production_Specs_ID, trxName);
      /** if (UNS_Production_Specs_ID == 0)
        {
			setProcessed (false);
			setUNS_Production_ID (0);
			setUNS_Production_Specs_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Production_Specs (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Production_Specs[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Notes.
		@param Notes Notes	  */
	public void setNotes (String Notes)
	{
		set_Value (COLUMNNAME_Notes, Notes);
	}

	/** Get Notes.
		@return Notes	  */
	public String getNotes () 
	{
		return (String)get_Value(COLUMNNAME_Notes);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Spec Name.
		@param SpecName 
		The name of the specification
	  */
	public void setSpecName (String SpecName)
	{
		set_Value (COLUMNNAME_SpecName, SpecName);
	}

	/** Get Spec Name.
		@return The name of the specification
	  */
	public String getSpecName () 
	{
		return (String)get_Value(COLUMNNAME_SpecName);
	}

	/** Set Spec Value.
		@param SpecValue 
		The calculation value of the specification
	  */
	public void setSpecValue (BigDecimal SpecValue)
	{
		set_Value (COLUMNNAME_SpecValue, SpecValue);
	}

	/** Get Spec Value.
		@return The calculation value of the specification
	  */
	public BigDecimal getSpecValue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SpecValue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Production getUNS_Production() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Production)MTable.get(getCtx(), com.uns.model.I_UNS_Production.Table_Name)
			.getPO(getUNS_Production_ID(), get_TrxName());	}

	/** Set Production.
		@param UNS_Production_ID Production	  */
	public void setUNS_Production_ID (int UNS_Production_ID)
	{
		if (UNS_Production_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_ID, Integer.valueOf(UNS_Production_ID));
	}

	/** Get Production.
		@return Production	  */
	public int getUNS_Production_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Production Output Specifications.
		@param UNS_Production_Specs_ID Production Output Specifications	  */
	public void setUNS_Production_Specs_ID (int UNS_Production_Specs_ID)
	{
		if (UNS_Production_Specs_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_Specs_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_Specs_ID, Integer.valueOf(UNS_Production_Specs_ID));
	}

	/** Get Production Output Specifications.
		@return Production Output Specifications	  */
	public int getUNS_Production_Specs_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_Specs_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Production_Specs_UU.
		@param UNS_Production_Specs_UU UNS_Production_Specs_UU	  */
	public void setUNS_Production_Specs_UU (String UNS_Production_Specs_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Production_Specs_UU, UNS_Production_Specs_UU);
	}

	/** Get UNS_Production_Specs_UU.
		@return UNS_Production_Specs_UU	  */
	public String getUNS_Production_Specs_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Production_Specs_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}