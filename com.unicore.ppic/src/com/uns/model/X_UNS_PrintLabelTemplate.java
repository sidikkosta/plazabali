/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_PrintLabelTemplate
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PrintLabelTemplate extends PO implements I_UNS_PrintLabelTemplate, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180919L;

    /** Standard Constructor */
    public X_UNS_PrintLabelTemplate (Properties ctx, int UNS_PrintLabelTemplate_ID, String trxName)
    {
      super (ctx, UNS_PrintLabelTemplate_ID, trxName);
      /** if (UNS_PrintLabelTemplate_ID == 0)
        {
			setUNS_PriceLabelTemplate_ID (0);
			setUNS_PrintLabelTemplate_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PrintLabelTemplate (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PrintLabelTemplate[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public com.uns.model.I_UNS_PriceLabelPrinterConf getUNS_PriceLabelPrinterConf() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PriceLabelPrinterConf)MTable.get(getCtx(), com.uns.model.I_UNS_PriceLabelPrinterConf.Table_Name)
			.getPO(getUNS_PriceLabelPrinterConf_ID(), get_TrxName());	}

	/** Set Price Label Printer Configuration.
		@param UNS_PriceLabelPrinterConf_ID Price Label Printer Configuration	  */
	public void setUNS_PriceLabelPrinterConf_ID (int UNS_PriceLabelPrinterConf_ID)
	{
		if (UNS_PriceLabelPrinterConf_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrinterConf_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceLabelPrinterConf_ID, Integer.valueOf(UNS_PriceLabelPrinterConf_ID));
	}

	/** Get Price Label Printer Configuration.
		@return Price Label Printer Configuration	  */
	public int getUNS_PriceLabelPrinterConf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PriceLabelPrinterConf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_PriceLabelTemplate getUNS_PriceLabelTemplate() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PriceLabelTemplate)MTable.get(getCtx(), com.uns.model.I_UNS_PriceLabelTemplate.Table_Name)
			.getPO(getUNS_PriceLabelTemplate_ID(), get_TrxName());	}

	/** Set Price Label Template.
		@param UNS_PriceLabelTemplate_ID Price Label Template	  */
	public void setUNS_PriceLabelTemplate_ID (int UNS_PriceLabelTemplate_ID)
	{
		if (UNS_PriceLabelTemplate_ID < 1) 
			set_Value (COLUMNNAME_UNS_PriceLabelTemplate_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_PriceLabelTemplate_ID, Integer.valueOf(UNS_PriceLabelTemplate_ID));
	}

	/** Get Price Label Template.
		@return Price Label Template	  */
	public int getUNS_PriceLabelTemplate_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PriceLabelTemplate_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Print Label Template.
		@param UNS_PrintLabelTemplate_ID Print Label Template	  */
	public void setUNS_PrintLabelTemplate_ID (int UNS_PrintLabelTemplate_ID)
	{
		if (UNS_PrintLabelTemplate_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PrintLabelTemplate_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PrintLabelTemplate_ID, Integer.valueOf(UNS_PrintLabelTemplate_ID));
	}

	/** Get Print Label Template.
		@return Print Label Template	  */
	public int getUNS_PrintLabelTemplate_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PrintLabelTemplate_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PrintLabelTemplate_UU.
		@param UNS_PrintLabelTemplate_UU UNS_PrintLabelTemplate_UU	  */
	public void setUNS_PrintLabelTemplate_UU (String UNS_PrintLabelTemplate_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PrintLabelTemplate_UU, UNS_PrintLabelTemplate_UU);
	}

	/** Get UNS_PrintLabelTemplate_UU.
		@return UNS_PrintLabelTemplate_UU	  */
	public String getUNS_PrintLabelTemplate_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PrintLabelTemplate_UU);
	}
}