package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Util;

public class MUNSMaintenanceMaterial extends X_UNS_MaintenanceMaterial {

	/**
	 * @author Thunder
	 */
	
	private static final long serialVersionUID = -3809964809404645216L;

	public MUNSMaintenanceMaterial(Properties ctx,
			int UNS_MaintenanceMaterial_ID, String trxName) {
		super(ctx, UNS_MaintenanceMaterial_ID, trxName);
		
	}

	public MUNSMaintenanceMaterial(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(getM_Product_ID() > 0)
		{
			//set UOM
			String sql = "SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?";
			int uom = DB.getSQLValue(get_TrxName(), sql, getM_Product_ID());
			if(uom < 0)
				throw new AdempiereException("Cannot found UOM, please check the product");
			setC_UOM_ID(uom);
			
			//set Product String
			sql = "SELECT Name FROM M_Product WHERE M_Product_ID = ?";
			String product = DB.getSQLValueString(get_TrxName(), sql, getM_Product_ID());
			if(Util.isEmpty(product, true))
				throw new AdempiereException("Cannot found Name Product, please check the product");
			setProduct(product);
		}
		
		return true;
	}
}
