/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_MaintenanceAct
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_MaintenanceAct extends PO implements I_UNS_MaintenanceAct, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180119L;

    /** Standard Constructor */
    public X_UNS_MaintenanceAct (Properties ctx, int UNS_MaintenanceAct_ID, String trxName)
    {
      super (ctx, UNS_MaintenanceAct_ID, trxName);
      /** if (UNS_MaintenanceAct_ID == 0)
        {
			setIsSummary (false);
// N
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_MaintenanceAct (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_MaintenanceAct[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Is Main.
		@param IsSummary Is Main	  */
	public void setIsSummary (boolean IsSummary)
	{
		set_Value (COLUMNNAME_IsSummary, Boolean.valueOf(IsSummary));
	}

	/** Get Is Main.
		@return Is Main	  */
	public boolean isSummary () 
	{
		Object oo = get_Value(COLUMNNAME_IsSummary);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.uns.model.I_UNS_MaintenanceAct getLink_MaintenanceAct() throws RuntimeException
    {
		return (com.uns.model.I_UNS_MaintenanceAct)MTable.get(getCtx(), com.uns.model.I_UNS_MaintenanceAct.Table_Name)
			.getPO(getLink_MaintenanceAct_ID(), get_TrxName());	}

	/** Set Station.
		@param Link_MaintenanceAct_ID Station	  */
	public void setLink_MaintenanceAct_ID (int Link_MaintenanceAct_ID)
	{
		if (Link_MaintenanceAct_ID < 1) 
			set_Value (COLUMNNAME_Link_MaintenanceAct_ID, null);
		else 
			set_Value (COLUMNNAME_Link_MaintenanceAct_ID, Integer.valueOf(Link_MaintenanceAct_ID));
	}

	/** Get Station.
		@return Station	  */
	public int getLink_MaintenanceAct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Link_MaintenanceAct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Station = ST */
	public static final String MAINTENANCETYPE_Station = "ST";
	/** Heavy Equipment = HE */
	public static final String MAINTENANCETYPE_HeavyEquipment = "HE";
	/** Set Maintenance Type.
		@param MaintenanceType Maintenance Type	  */
	public void setMaintenanceType (String MaintenanceType)
	{

		set_Value (COLUMNNAME_MaintenanceType, MaintenanceType);
	}

	/** Get Maintenance Type.
		@return Maintenance Type	  */
	public String getMaintenanceType () 
	{
		return (String)get_Value(COLUMNNAME_MaintenanceType);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set UNS_MaintenanceAct.
		@param UNS_MaintenanceAct_ID UNS_MaintenanceAct	  */
	public void setUNS_MaintenanceAct_ID (int UNS_MaintenanceAct_ID)
	{
		if (UNS_MaintenanceAct_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceAct_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_MaintenanceAct_ID, Integer.valueOf(UNS_MaintenanceAct_ID));
	}

	/** Get UNS_MaintenanceAct.
		@return UNS_MaintenanceAct	  */
	public int getUNS_MaintenanceAct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_MaintenanceAct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_MaintenanceAct_UU.
		@param UNS_MaintenanceAct_UU UNS_MaintenanceAct_UU	  */
	public void setUNS_MaintenanceAct_UU (String UNS_MaintenanceAct_UU)
	{
		set_Value (COLUMNNAME_UNS_MaintenanceAct_UU, UNS_MaintenanceAct_UU);
	}

	/** Get UNS_MaintenanceAct_UU.
		@return UNS_MaintenanceAct_UU	  */
	public String getUNS_MaintenanceAct_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_MaintenanceAct_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}