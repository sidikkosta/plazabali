/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ProductList
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ProductList 
{

    /** TableName=UNS_ProductList */
    public static final String Table_Name = "UNS_ProductList";

    /** AD_Table_ID=1000432 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CategoryCode */
    public static final String COLUMNNAME_CategoryCode = "CategoryCode";

	/** Set Category Code	  */
	public void setCategoryCode (String CategoryCode);

	/** Get Category Code	  */
	public String getCategoryCode();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateCode */
    public static final String COLUMNNAME_DateCode = "DateCode";

	/** Set Date Code	  */
	public void setDateCode (String DateCode);

	/** Get Date Code	  */
	public String getDateCode();

    /** Column name DatePrinted */
    public static final String COLUMNNAME_DatePrinted = "DatePrinted";

	/** Set Date printed.
	  * Date the document was printed.
	  */
	public void setDatePrinted (Timestamp DatePrinted);

	/** Get Date printed.
	  * Date the document was printed.
	  */
	public Timestamp getDatePrinted();

    /** Column name ETBBCode */
    public static final String COLUMNNAME_ETBBCode = "ETBBCode";

	/** Set ETBB Code	  */
	public void setETBBCode (String ETBBCode);

	/** Get ETBB Code	  */
	public String getETBBCode();

    /** Column name IDRAmount */
    public static final String COLUMNNAME_IDRAmount = "IDRAmount";

	/** Set IDR Amount	  */
	public void setIDRAmount (BigDecimal IDRAmount);

	/** Get IDR Amount	  */
	public BigDecimal getIDRAmount();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsManual */
    public static final String COLUMNNAME_IsManual = "IsManual";

	/** Set Manual.
	  * This is a manual process
	  */
	public void setIsManual (boolean IsManual);

	/** Get Manual.
	  * This is a manual process
	  */
	public boolean isManual();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name PriceLabelType */
    public static final String COLUMNNAME_PriceLabelType = "PriceLabelType";

	/** Set Price Label Type	  */
	public void setPriceLabelType (String PriceLabelType);

	/** Get Price Label Type	  */
	public String getPriceLabelType();

    /** Column name PrintedQty */
    public static final String COLUMNNAME_PrintedQty = "PrintedQty";

	/** Set Printed Qty	  */
	public void setPrintedQty (BigDecimal PrintedQty);

	/** Get Printed Qty	  */
	public BigDecimal getPrintedQty();

    /** Column name ProductName */
    public static final String COLUMNNAME_ProductName = "ProductName";

	/** Set Product Name.
	  * Name of the Product
	  */
	public void setProductName (String ProductName);

	/** Get Product Name.
	  * Name of the Product
	  */
	public String getProductName();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name SKU */
    public static final String COLUMNNAME_SKU = "SKU";

	/** Set SKU.
	  * Stock Keeping Unit
	  */
	public void setSKU (String SKU);

	/** Get SKU.
	  * Stock Keeping Unit
	  */
	public String getSKU();

    /** Column name UNS_PriceLabelPrint_ID */
    public static final String COLUMNNAME_UNS_PriceLabelPrint_ID = "UNS_PriceLabelPrint_ID";

	/** Set Price Label Printing	  */
	public void setUNS_PriceLabelPrint_ID (int UNS_PriceLabelPrint_ID);

	/** Get Price Label Printing	  */
	public int getUNS_PriceLabelPrint_ID();

	public com.uns.model.I_UNS_PriceLabelPrint getUNS_PriceLabelPrint() throws RuntimeException;

    /** Column name UNS_ProductList_ID */
    public static final String COLUMNNAME_UNS_ProductList_ID = "UNS_ProductList_ID";

	/** Set Product List Detail	  */
	public void setUNS_ProductList_ID (int UNS_ProductList_ID);

	/** Get Product List Detail	  */
	public int getUNS_ProductList_ID();

    /** Column name UNS_ProductList_UU */
    public static final String COLUMNNAME_UNS_ProductList_UU = "UNS_ProductList_UU";

	/** Set UNS_ProductList_UU	  */
	public void setUNS_ProductList_UU (String UNS_ProductList_UU);

	/** Get UNS_ProductList_UU	  */
	public String getUNS_ProductList_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name USDAmount */
    public static final String COLUMNNAME_USDAmount = "USDAmount";

	/** Set USD Amount	  */
	public void setUSDAmount (BigDecimal USDAmount);

	/** Get USD Amount	  */
	public BigDecimal getUSDAmount();
}
