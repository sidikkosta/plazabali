/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PLConv_Product
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PLConv_Product extends PO implements I_UNS_PLConv_Product, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181005L;

    /** Standard Constructor */
    public X_UNS_PLConv_Product (Properties ctx, int UNS_PLConv_Product_ID, String trxName)
    {
      super (ctx, UNS_PLConv_Product_ID, trxName);
      /** if (UNS_PLConv_Product_ID == 0)
        {
			setPriceLimit (Env.ZERO);
// 0
			setPriceList (Env.ZERO);
// 0
			setPriceListSource (Env.ZERO);
// 0
			setPriceStd (Env.ZERO);
// 0
			setUNS_PLConv_Product_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PLConv_Product (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PLConv_Product[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_M_Product_Category getM_Product_Category() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product_Category)MTable.get(getCtx(), org.compiere.model.I_M_Product_Category.Table_Name)
			.getPO(getM_Product_Category_ID(), get_TrxName());	}

	/** Set Product Category.
		@param M_Product_Category_ID 
		Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID)
	{
		if (M_Product_Category_ID < 1) 
			set_Value (COLUMNNAME_M_Product_Category_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_Category_ID, Integer.valueOf(M_Product_Category_ID));
	}

	/** Get Product Category.
		@return Category of a Product
	  */
	public int getM_Product_Category_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Limit Price.
		@param PriceLimit 
		Lowest price for a product
	  */
	public void setPriceLimit (BigDecimal PriceLimit)
	{
		set_Value (COLUMNNAME_PriceLimit, PriceLimit);
	}

	/** Get Limit Price.
		@return Lowest price for a product
	  */
	public BigDecimal getPriceLimit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceLimit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set List Price.
		@param PriceList 
		List Price
	  */
	public void setPriceList (BigDecimal PriceList)
	{
		set_Value (COLUMNNAME_PriceList, PriceList);
	}

	/** Get List Price.
		@return List Price
	  */
	public BigDecimal getPriceList () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceList);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price List Source.
		@param PriceListSource Price List Source	  */
	public void setPriceListSource (BigDecimal PriceListSource)
	{
		set_Value (COLUMNNAME_PriceListSource, PriceListSource);
	}

	/** Get Price List Source.
		@return Price List Source	  */
	public BigDecimal getPriceListSource () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceListSource);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Standard Price.
		@param PriceStd 
		Standard Price
	  */
	public void setPriceStd (BigDecimal PriceStd)
	{
		set_Value (COLUMNNAME_PriceStd, PriceStd);
	}

	/** Get Standard Price.
		@return Standard Price
	  */
	public BigDecimal getPriceStd () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceStd);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price List Conversion Product.
		@param UNS_PLConv_Product_ID Price List Conversion Product	  */
	public void setUNS_PLConv_Product_ID (int UNS_PLConv_Product_ID)
	{
		if (UNS_PLConv_Product_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PLConv_Product_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PLConv_Product_ID, Integer.valueOf(UNS_PLConv_Product_ID));
	}

	/** Get Price List Conversion Product.
		@return Price List Conversion Product	  */
	public int getUNS_PLConv_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PLConv_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PLConv_Product_UU.
		@param UNS_PLConv_Product_UU UNS_PLConv_Product_UU	  */
	public void setUNS_PLConv_Product_UU (String UNS_PLConv_Product_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PLConv_Product_UU, UNS_PLConv_Product_UU);
	}

	/** Get UNS_PLConv_Product_UU.
		@return UNS_PLConv_Product_UU	  */
	public String getUNS_PLConv_Product_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PLConv_Product_UU);
	}

	public com.uns.model.I_UNS_PriceListConv getUNS_PriceListConv() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PriceListConv)MTable.get(getCtx(), com.uns.model.I_UNS_PriceListConv.Table_Name)
			.getPO(getUNS_PriceListConv_ID(), get_TrxName());	}

	/** Set Price List Conversion.
		@param UNS_PriceListConv_ID Price List Conversion	  */
	public void setUNS_PriceListConv_ID (int UNS_PriceListConv_ID)
	{
		if (UNS_PriceListConv_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceListConv_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PriceListConv_ID, Integer.valueOf(UNS_PriceListConv_ID));
	}

	/** Get Price List Conversion.
		@return Price List Conversion	  */
	public int getUNS_PriceListConv_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PriceListConv_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}