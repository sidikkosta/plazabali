/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTransaction;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;













import com.uns.model.factory.UNSPPICModelFactory;


/**
 * @author Burhani Adam
 *
 */
public class MUNSSounding extends X_UNS_Sounding implements DocAction, DocOptions, IUNSApprovalInfo 
{
	
	public static final String PRODUCT_KERNEL = "KRNL";
	public static final String PRODUCT_FFB = "FFB";
	public static final String PRODUCT_CANGKANG = "CS";
	public static final String PRODUCT_CPO = "CPO";
	
	private static final long serialVersionUID = 6230836135060866954L;
	private BigDecimal m_StockReceiptNettoII = Env.ZERO;
	private BigDecimal m_StockReceiptNettoI = Env.ZERO;
	public MUNSSounding m_PrevSounding = null;
	private BigDecimal m_DeliveredKRNL = null;
	private BigDecimal m_ReturnKRNL = null;
	private BigDecimal m_ReturnCPO = null;
	private MUNSTankSounding[] m_lines = null;

	/**
	 * @param ctx
	 * @param UNS_Sounding_ID
	 * @param trxName
	 */
	public MUNSSounding(Properties ctx, int UNS_Sounding_ID, String trxName) {
		super(ctx, UNS_Sounding_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSSounding(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	@Override
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSSounding[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		StringBuilder info = new StringBuilder();
		
		info.append(getDateDoc());
		
		return info.toString();
//		return Msg.getElement(getCtx(), "UNS_Sounding_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{		
		initCPOReturnQty();
		setCPOReturnQty(m_ReturnCPO);
		
		if(is_ValueChanged(COLUMNNAME_FFBProcessedNettoI) || newRecord)
			calcOthersQty(PRODUCT_FFB, newRecord);
		
		if(newRecord || is_ValueChanged(COLUMNNAME_KernelFloor) || is_ValueChanged(COLUMNNAME_KernelFloorBad) || is_ValueChanged(COLUMNNAME_HeightKernelSilo1) 
				|| is_ValueChanged(COLUMNNAME_HeightKernelSilo2) || is_ValueChanged(COLUMNNAME_HeightKernelSilo3) || is_ValueChanged(COLUMNNAME_HeightBulkingSilo)
				|| is_ValueChanged(COLUMNNAME_NutFloor) || is_ValueChanged(COLUMNNAME_HeightNutSilo) || is_ValueChanged(COLUMNNAME_NutSilo)
				|| is_ValueChanged(COLUMNNAME_KernelSilo1) || is_ValueChanged(COLUMNNAME_KernelSilo2) || is_ValueChanged(COLUMNNAME_KernelSilo3)
				|| is_ValueChanged(COLUMNNAME_BulkingSilo))
			calcOthersQty(PRODUCT_KERNEL, newRecord);
		
		if(newRecord || is_ValueChanged(COLUMNNAME_ProductionCangkang) || is_ValueChanged(COLUMNNAME_CangkangBoiler)
				|| is_ValueChanged(COLUMNNAME_NutPercentage) || is_ValueChanged(COLUMNNAME_FFBProcessedNettoI))
			calcOthersQty(PRODUCT_CANGKANG, newRecord);
		
		return true;	
	}	
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		if(success)
		{

			
		}
		return true;
	}
	protected boolean beforeDelete()
	{
		return true;
	}

	@Override
	protected boolean afterDelete(boolean success) {
		if(success)
		{
			String sql = "DELETE FROM UNS_TankSounding WHERE UNS_Sounding_ID = ?";
			DB.executeUpdate(sql, getUNS_Sounding_ID(), get_TrxName());
		}
		return true;
	}
	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		
		
		setProcessed(true);
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 * @throws SQLException 
	 */
	
	public String completeIt() 
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		if (DOCACTION_Prepare.equals(getDocAction()))
		{
			return DocAction.STATUS_InProgress;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		if (getCPOProductionQty().signum() == 1 && getFFBProcessedNettoI().signum() == 1)
		{
			m_processMsg = createProduction();
			
			if (null != m_processMsg)
			{
				return DOCSTATUS_Invalid;
			}
		}
		
		createMovement();
		
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		if (getCPOProductionQty().signum() != 0 && getFFBProcessedNettoI().signum() == 0
				&& !createCPOPhysical())
		{
			return DOCSTATUS_Invalid;
		}
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}
//
//		if ("Endang".equals("Endang"))
//		{
//			m_processMsg = "Test, biar gak complete";
//			return DOCSTATUS_Invalid;
//		}
		//
		String sql = "SELECT ts.uns_tank_id, ts.weight, so.datedoc, ts.weightbyflowmeter FROM uns_tanksounding ts INNER JOIN uns_sounding so ON "
				+ "ts.uns_sounding_id = so.uns_sounding_id AND ts.uns_sounding_id = "+getUNS_Sounding_ID();
		PreparedStatement prestat = null;
		ResultSet rs = null;
		try {
			prestat = DB.prepareStatement(sql, get_TrxName());
			rs = prestat.executeQuery();
			
			while(rs.next()){
				String sqll = "UPDATE UNS_Tank SET LastSoundingDate = ?::timestamp , LastSoundingQty = ? WHERE UNS_Tank_ID = ?";
				DB.executeUpdateEx(sqll, new Object[]{rs.getTimestamp(3), isManualSounding() ? rs.getBigDecimal(2) : rs.getBigDecimal(4)
						, rs.getInt(1)}, get_TrxName());
			}
		} catch (SQLException e){
			throw new AdempiereException("Error set Last Sounding and Last Date Sounding", e);
		}
		
		MUNSSoundingLab sl = com.uns.base.model.Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(), MUNSSoundingLab.Table_Name,
				"UNS_Sounding_ID=?", get_TrxName()).setParameters(getUNS_Sounding_ID()).first();
		if(sl == null)
		{
			sl = new MUNSSoundingLab(getCtx(), 0, get_TrxName());
			sl.setAD_Org_ID(getAD_Org_ID());
			sl.setUNS_Sounding_ID(getUNS_Sounding_ID());
			sl.setDateDoc(getDateDoc());
			sl.saveEx();
		}
		
		//update Milling
		String sqql = "UPDATE UNS_Production_OperationHrs SET UNS_Production_ID = ?"
				+ " WHERE UNS_Sounding_ID = ?";
		int ok = DB.executeUpdateEx(sqql, new Object[]{getUNS_Production_ID(), getUNS_Sounding_ID()}, get_TrxName());
		if(ok < 0)
			throw new AdempiereException("Error when try to update Milling");
		
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		//check next sounding
		String sql = "SELECT UNS_Sounding_ID FROM UNS_Sounding WHERE DateAcct > ? AND DocStatus NOT IN (?,?)";
		int nextsounding = DB.getSQLValue(get_TrxName(), sql, getDateAcct(), "VO","RE");
		if(nextsounding > 0)
		{
			m_processMsg = "Error. Can't void. Existing document after this one.";
			return false;
		}

		MUNSSoundingLab sl = com.uns.base.model.Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(), MUNSSoundingLab.Table_Name,
				"UNS_Sounding_ID=?", get_TrxName()).setParameters(getUNS_Sounding_ID()).first();
		if(sl != null)
		{
			if(sl.getDocStatus().equals(DOCSTATUS_Completed) || sl.getDocStatus().equals(DOCSTATUS_InProgress) || 
					sl.getDocStatus().equals(DOCSTATUS_Closed) || sl.getDocStatus().equals(DOCSTATUS_Approved))
			{
				m_processMsg = "Please void or reactive Sounding Laboratory first with document no = "+sl.getDocumentNo();
				return false;
			}
			else if(sl.getDocStatus().equals(DOCSTATUS_Drafted))
			{
				try
				{
					boolean ok = sl.processIt(DOCACTION_Void);
					if(!ok)
					{
						m_processMsg = sl.getProcessMsg();
						return false;
					}
					sl.saveEx();
				}
				catch(Exception e)
				{
					m_processMsg = e.getMessage();
					return false;
				}
			}
		}
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		if(getPrevSounding() != null){
			sql = "SELECT ts.uns_tank_id, ts.weight, so.datedoc FROM uns_tanksounding ts INNER JOIN uns_sounding so ON "
					+ "ts.uns_sounding_id = so.uns_sounding_id AND ts.uns_sounding_id = "+getUNS_PrevSounding_ID();
			PreparedStatement prestat = null;
			ResultSet rs = null;
			try {
				prestat = DB.prepareStatement(sql, get_TrxName());
				rs = prestat.executeQuery();
				
				while(rs.next()){
					String sqll = "UPDATE UNS_Tank SET LastSoundingDate = ?::timestamp , LastSoundingQty = ? WHERE UNS_Tank_ID = ?";
					DB.executeUpdateEx(sqll, new Object[]{rs.getTimestamp(3), rs.getBigDecimal(2), rs.getInt(1)}, get_TrxName());
				}
			} catch (SQLException e){
				throw new AdempiereException("Error set Last Sounding and Last Date Sounding", e);
			}
		}else{
			sql = "UPDATE UNS_Tank SET LastSoundingQty = 0, LastSoundingDate = null WHERE UNS_Tank_ID IN ("
					+ "SELECT UNS_Tank_ID FROM UNS_TankSounding WHERE UNS_Sounding_ID = ?)";
			DB.executeUpdate(sql, get_TrxName(), getUNS_Sounding_ID());
		}
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		//check next sounding
		String sql = "SELECT UNS_Sounding_ID FROM UNS_Sounding WHERE DateAcct > ? AND DocStatus IN (?,?)";
		int nextsounding = DB.getSQLValue(get_TrxName(), sql, getDateAcct(), "CO","CL");
		if(nextsounding > 0)
		{
			m_processMsg = "Error. Can't reactive. Existing document after this one in status Complete / Close.";
			return false;
		}
		
		try
		{
			MUNSSoundingLab sl = com.uns.base.model.Query.get(getCtx(), UNSPPICModelFactory.getExtensionID(), MUNSSoundingLab.Table_Name,
					"UNS_Sounding_ID = ?", get_TrxName()).setParameters(getUNS_Sounding_ID()).first();
			if(sl !=null)
			{
				if(sl.getDocStatus().equals(DOCSTATUS_Completed) || sl.getDocStatus().equals(DOCSTATUS_InProgress) ||
						sl.getDocStatus().equals(DOCSTATUS_Closed) || sl.getDocStatus().equals(DOCSTATUS_Approved))
				{
					m_processMsg = "Please void or reactive Sounding Laboratory first, with document no = "+sl.getDocumentNo();
					return false;
				}
			}
			
			if (getM_Inventory_ID() > 0)
			{
				MInventory inv = new MInventory(getCtx(), getM_Inventory_ID(), get_TrxName());
				if (!inv.processIt(DOCACTION_Void))
				{
					m_processMsg = inv.getProcessMsg();
					return false;
				}
				inv.saveEx();
			}
			if (getM_Movement_ID() > 0)
			{
				MMovement move = new MMovement(getCtx(), getM_Movement_ID(), 
						get_TrxName());
				if (!move.processIt(DOCACTION_Void))
				{
					m_processMsg = move.getProcessMsg();
					return false;
				}
				move.saveEx();
			}
			
			if (getUNS_Production_ID() > 0)
			{
				MUNSProduction production = new MUNSProduction(getCtx(), 
						getUNS_Production_ID(), get_TrxName());
				if (!production.processIt(DOCACTION_Void))
				{
					m_processMsg = production.getProcessMsg();
					return false;
				}
				production.saveEx();
			}
			
			setUNS_Production_ID(-1);
			setM_Movement_ID(-1);
			setM_Inventory_ID(-1);
		}
		catch (Exception ex)
		{
			m_processMsg = ex.getMessage();
			return false;
		}
		
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		if(getPrevSounding() != null){
			sql = "SELECT ts.uns_tank_id, ts.weight, so.datedoc FROM uns_tanksounding ts INNER JOIN uns_sounding so ON "
					+ "ts.uns_sounding_id = so.uns_sounding_id AND ts.uns_sounding_id = "+getUNS_PrevSounding_ID();
			PreparedStatement prestat = null;
			ResultSet rs = null;
			try {
				prestat = DB.prepareStatement(sql, get_TrxName());
				rs = prestat.executeQuery();
				
				while(rs.next()){
					String sqll = "UPDATE UNS_Tank SET LastSoundingDate = ?::timestamp , LastSoundingQty = ? WHERE UNS_Tank_ID = ?";
					DB.executeUpdateEx(sqll, new Object[]{rs.getTimestamp(3), rs.getBigDecimal(2), rs.getInt(1)}, get_TrxName());
				}
			} catch (SQLException e){
				throw new AdempiereException("Error set Last Sounding and Last Date Sounding", e);
			}
		}else{
			sql = "UPDATE UNS_Tank SET LastSoundingQty = 0";
			DB.executeUpdate(sql, get_TrxName());
		}
		
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		
		StringBuffer sb = new StringBuffer();
		sb.append(getDocumentNo());
		sb.append("_"+getName());
		sb.append(" :");
		sb.append("Production CPO="+getCPOProductionQty());
		sb.append(" ;OER I="+getOERNettoI()+" ,OER II="+getOERNettoII());
		sb.append(" ,KER I="+getReal_KERNettoI()+" ,KER II"+getReal_KERNettoII());
		
		return sb.toString();
	}

	@Override
	public String getProcessMsg() {
		
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
	
		return 0;
	}

	@Override
	public int getC_Currency_ID() {

		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
	
		return null;
	}

	@Override
	public boolean approveIt() {
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Void;
		}
		else if(docStatus.equals(DOCSTATUS_NotApproved))
		{
			options[index++] = DOCACTION_Complete;
		}
		return index;
	}
	
	private String calcOthersQty(String productName, boolean newRecord)
	{ 
		getPrevSounding();
			
		m_processMsg= null;
		
		//nut silo
		BigDecimal nutsiloheight = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.NUT_SILO_HEIGHT, 300, getAD_Client_ID()));
		BigDecimal nutqtypercm = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.NUT_QTY_PER_CM, 78, getAD_Client_ID()));
		BigDecimal nutvolumecone1 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.NUT_SILO_VOLUME_CONE+"_1", 1660, getAD_Client_ID()));
		BigDecimal nutvolumecone2 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.NUT_SILO_VOLUME_CONE+"_2", 1660, getAD_Client_ID()));
		BigDecimal nutvolumecone3 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.NUT_SILO_VOLUME_CONE+"_3", 1660, getAD_Client_ID()));
		
		//kernel silo
		BigDecimal krnlsiloheight1 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_SILO_HEIGHT+"_1", 530, getAD_Client_ID()));
		BigDecimal krnlsiloheight2 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_SILO_HEIGHT+"_2", 530, getAD_Client_ID()));
		BigDecimal krnlsiloheight3 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_SILO_HEIGHT+"_3", 540, getAD_Client_ID()));
		BigDecimal krnlvolumecone1 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_SILO_VOLUME_CONE+"_1", 2360, getAD_Client_ID()));
		BigDecimal krnlvolumecone2 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_SILO_VOLUME_CONE+"_2", 2360, getAD_Client_ID()));
		BigDecimal krnlvolumecone3 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_SILO_VOLUME_CONE+"_3", 2660, getAD_Client_ID()));
		BigDecimal krnlqtypercm1 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_QTY_PER_CM+"_1", 66, getAD_Client_ID()));
		BigDecimal krnlqtypercm2 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_QTY_PER_CM+"_2", 66, getAD_Client_ID()));
		BigDecimal krnlqtypercm3 = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_QTY_PER_CM+"_3", 87, getAD_Client_ID()));
		
		//bulk silo
		BigDecimal bulksiloheight = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.BULKING_SILO_HEIGHT, 600, getAD_Client_ID()));
		BigDecimal bulkvolumecone = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.BULKING_SILO_VOLUME_CONE, 18827, getAD_Client_ID()));
		BigDecimal bulkqtypercm = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.BULKING_QTY_PER_CM, 260, getAD_Client_ID()));		

		
		//shell
		BigDecimal boilerpercentage = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.SHELL_BOILER_PERCENTAGE, 5.4, getAD_Client_ID()));
		BigDecimal shellprodpercentage = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.SHELL_PRODUCTION_PERCENTAGE, 6, getAD_Client_ID()));
		
		//real Stock Kernel formula percantage
		BigDecimal netKrnlPerc = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.NETTO_KERNEL_PERCENTAGE, 0.85, getAD_Client_ID()));
		BigDecimal krnlOfNutOnBinPerc = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_OF_NUT_PERCANTAGE_ON_BIN, 0.42, getAD_Client_ID()));
		BigDecimal krnlOfNutOnFloorPerc = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.KERNEL_OF_NUT_PERCANTAGE_ON_FLOOR, 0.36, getAD_Client_ID()));
		
		//accesproduct
		BigDecimal accprShell = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.ACCESPRODUCT_SHELL_PERCENTAGE, 0.02, getAD_Client_ID()));
		BigDecimal accprEmptyBunch = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.ACCESPRODUCT_EMPTYBUNCH_PERCENTAGE, 0.23, getAD_Client_ID()));
		BigDecimal accprSolidDec = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.ACCESPRODUCT_SOLIDDECANTER_PERCENTAGE, 0.14, getAD_Client_ID()));
		BigDecimal accprSludgeOil = BigDecimal.valueOf(MSysConfig.getDoubleValue(
				MSysConfig.ACCESPRODUCT_SLUDGEOIL_PERCENTAGE, 0.02, getAD_Client_ID()));
		
		Timestamp datePrevSounding = TimeUtil.addDays(getDateAcct(), -1);
		BigDecimal proportion = Env.ZERO;
		
		if(m_PrevSounding != null)
		{
			setUNS_PrevSounding_ID(m_PrevSounding.getUNS_Sounding_ID());
			
			setPreviousFFBQty(m_PrevSounding.getFFBRemainingQty());
			setPreviousFFBQtyII(m_PrevSounding.getFFBRemainingQtyII());
			setPreviousKernelQty(m_PrevSounding.getStockKernelToday());
			setReal_PreviousKernelQty(m_PrevSounding.getReal_StockKernelToday());
			datePrevSounding = m_PrevSounding.getDateAcct();
		}
		
		if(newRecord || productName.equals(PRODUCT_FFB))
		{			
			String sql = "SELECT COALESCE(SUM(nettoi),0) from UNS_Weighbridgeticket WHERE docstatus  IN ('CO','CL') AND "
					+ "issotrx = 'N' AND datedoc BETWEEN ?::timestamp AND ?::timestamp AND C_DocType_ID = "
					+ "(SELECT MAX(C_DocType_ID) FROM C_DocType WHERE DocBaseType = ?)";
			m_StockReceiptNettoI = DB.getSQLValueBD(get_TrxName(), sql,
					new Object[]{datePrevSounding, TimeUtil.addDays(getDateAcct(), -1), MDocType.DOCBASETYPE_GradedTicket});
			
			sql = "SELECT COALESCE(SUM(nettoi),0) from UNS_Weighbridgeticket WHERE docstatus  IN ('CO','CL') AND "
					+ "issotrx = 'N' AND datedoc BETWEEN ?::timestamp AND ?::timestamp AND C_DocType_ID = "
					+ "(SELECT MAX(C_DocType_ID) FROM C_DocType WHERE DocBaseType = ?)";
			BigDecimal emptyBunch = DB.getSQLValueBD(get_TrxName(), sql,
					new Object[]{datePrevSounding, TimeUtil.addDays(getDateAcct(), -1), MDocType.DOCBASETYPE_EmptyBunchTicket});
			
			m_StockReceiptNettoI = m_StockReceiptNettoI.subtract(emptyBunch);
			
			String sql2 = "SELECT COALESCE(SUM(reflection),0) from UNS_Weighbridgeticket WHERE docstatus  IN ('CO','CL') AND "
					+ "issotrx = 'N' AND datedoc BETWEEN ?::timestamp AND ?::timestamp";
			m_StockReceiptNettoII = DB.getSQLValueBD(get_TrxName(), sql2,
					new Object[]{datePrevSounding, TimeUtil.addDays(getDateAcct(), -1)});
			m_StockReceiptNettoII = m_StockReceiptNettoI.subtract(m_StockReceiptNettoII);
			
			setFFBReceiptNettoI(m_StockReceiptNettoI.setScale(0, RoundingMode.HALF_UP));
			setFFBReceiptNettoII(m_StockReceiptNettoII.setScale(0, RoundingMode.HALF_UP));
			
			if(getFFBProcessedNettoI().signum()==0)
			{
				setFFBProcessedNettoII(Env.ZERO);
				setOERNettoI(Env.ZERO);
				setOERNettoII(Env.ZERO);
				//accesProduct
				setAcc_Shell(Env.ZERO);
				setAcc_Fibre(Env.ZERO);
				setAcc_SolidDecanter(Env.ZERO);
				setAcc_SludgeOil(Env.ZERO);
		
			}
			else
			{
				proportion = (getFFBReceiptNettoII().add(getPreviousFFBQtyII())).divide((getFFBReceiptNettoI().add(getPreviousFFBQty())), 16, RoundingMode.HALF_UP);
				BigDecimal ffbProcessd2 = getFFBProcessedNettoI().multiply(proportion);
				// TODO	ini ane rubah ke DOWN awalnya HALF_DOWN
				setFFBProcessedNettoII(ffbProcessd2.setScale(0, RoundingMode.HALF_UP));
				BigDecimal oerN1 = getCPOProductionQty().divide(getFFBProcessedNettoI(), 5, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED);
				oerN1 = oerN1.setScale(2, RoundingMode.HALF_DOWN);
				BigDecimal oerN2 = getCPOProductionQty().divide(getFFBProcessedNettoII(), 5, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED);
				oerN2 = oerN2.setScale(2, RoundingMode.HALF_DOWN);
				setOERNettoI(oerN1);
				setOERNettoII(oerN2);
				
				//accesProduct
				setAcc_Shell(getFFBProcessedNettoI().multiply(accprShell).setScale(0, RoundingMode.HALF_UP));
				setAcc_Fibre(getFFBProcessedNettoI().multiply(accprEmptyBunch).setScale(0, RoundingMode.HALF_UP));
				setAcc_SolidDecanter(getFFBProcessedNettoI().multiply(accprSolidDec).setScale(0, RoundingMode.HALF_UP));
				setAcc_SludgeOil(getFFBProcessedNettoI().multiply(accprSludgeOil).setScale(0, RoundingMode.HALF_UP));
				
			}
			
			//Stock of Acces Product
			if(m_PrevSounding != null)
			{
				setStockShell(m_PrevSounding.getStockShell().add(getAcc_Shell())
						.subtract(getDeliveredShellQty()));
				setStockFibre(m_PrevSounding.getStockFibre().add(getAcc_Fibre())
						.subtract(getDeliveredFibreQty()));
				setStockSolidDecanter(m_PrevSounding.getStockSolidDecanter().add(getAcc_SolidDecanter())
						.subtract(getDeliveredSolidQty()));
				setStockSludgeOil(m_PrevSounding.getStockSludgeOil().add(getAcc_SludgeOil())
						.subtract(getDeliveredSludgeOilQty()));
			}
			
			setFFBRemainingQty(getFFBReceiptNettoI().add(getRestantStew()).add(getPreviousFFBQty()).subtract(getFFBProcessedNettoI()));
			
			if(getFFBRemainingQty().signum()==0)
				setFFBRemainingQtyII(Env.ZERO);
			else
				setFFBRemainingQtyII(getFFBReceiptNettoII().add((getRestantStew()).multiply(proportion).setScale(0,RoundingMode.HALF_UP))
					.add(getPreviousFFBQtyII()).subtract(getFFBProcessedNettoII()));
			
			if(!newRecord)
			{
				calcOthersQty(PRODUCT_KERNEL, false);
				calcOthersQty(PRODUCT_CANGKANG, false);
			}
		}
		
		if(newRecord || productName.equals(PRODUCT_KERNEL))
		{
			
			initDeliveredKernelQty();
			
			//setEstimate stock
			if (get_Value(COLUMNNAME_HeightNutSilo) == null )
			{
				setNutSilo(Env.ZERO);
			}
			else if(is_ValueChanged(COLUMNNAME_HeightNutSilo) && getHeightNutSilo().compareTo(nutsiloheight) <= 0)
			{
				setNutSilo(((nutsiloheight.subtract(getHeightNutSilo())).multiply(nutqtypercm)).add((nutvolumecone1).add(nutvolumecone2).add(nutvolumecone3))
						.setScale(0, RoundingMode.HALF_UP));
			}
			
			if (get_Value(COLUMNNAME_HeightKernelSilo1) == null )
			{
				setKernelSilo1(Env.ZERO);
			}
			else if(is_ValueChanged(COLUMNNAME_HeightKernelSilo1) && getHeightKernelSilo1().compareTo(krnlsiloheight1) <= 0)
			{
				setKernelSilo1(((krnlsiloheight1.subtract(getHeightKernelSilo1())).multiply(krnlqtypercm1)).add(krnlvolumecone1)
						.setScale(0, RoundingMode.HALF_UP));
			}
			
			if (get_Value(COLUMNNAME_HeightKernelSilo2) == null)
			{
				setKernelSilo2(Env.ZERO);
			}
			else if(is_ValueChanged(COLUMNNAME_HeightKernelSilo2) && getHeightKernelSilo2().compareTo(krnlsiloheight2) <= 0)
			{
				setKernelSilo2(((krnlsiloheight2.subtract(getHeightKernelSilo2())).multiply(krnlqtypercm2)).add(krnlvolumecone2)
						.setScale(0, RoundingMode.HALF_UP));
			}
			
			if (get_Value(COLUMNNAME_HeightKernelSilo3) == null)
			{
				setKernelSilo3(Env.ZERO);
			}
			else if(is_ValueChanged(COLUMNNAME_HeightKernelSilo3) && getHeightKernelSilo3().compareTo(krnlsiloheight3) <= 0)
			{
				setKernelSilo3(((krnlsiloheight3.subtract(getHeightKernelSilo3())).multiply(krnlqtypercm3)).add(krnlvolumecone3)
						.setScale(0, RoundingMode.HALF_UP));
			}
			
			if (get_Value(COLUMNNAME_HeightBulkingSilo) == null)
			{
				setBulkingSilo(Env.ZERO);
			}
			else if(is_ValueChanged(COLUMNNAME_HeightBulkingSilo) && getHeightBulkingSilo().compareTo(bulksiloheight) <= 0)
			{
				setBulkingSilo(((bulksiloheight.subtract(getHeightBulkingSilo())).multiply(bulkqtypercm)).add(bulkvolumecone)
						.setScale(0, RoundingMode.HALF_UP));
			}
		
			setStockKernelToday(getNutSilo().add(getNutFloor()).add(getKernelFloor()).add(getKernelFloorBad()).add(getKernelSilo1())
					.add(getKernelSilo2()).add(getKernelSilo3()).add(getBulkingSilo()));
			
			
			//setRealStock
			BigDecimal realKS1 = getKernelSilo1().multiply(netKrnlPerc);
			BigDecimal realKS2 = getKernelSilo2().multiply(netKrnlPerc);
			BigDecimal realKS3 = getKernelSilo3().multiply(netKrnlPerc);
			BigDecimal realNS = getNutSilo().multiply(krnlOfNutOnBinPerc);
			BigDecimal realNF = getNutFloor().multiply(krnlOfNutOnFloorPerc);
			
			setReal_KernelSilo1(realKS1.setScale(0, RoundingMode.HALF_UP));
			setReal_KernelSilo2(realKS2.setScale(0, RoundingMode.HALF_UP));
			setReal_KernelSilo3(realKS3.setScale(0, RoundingMode.HALF_UP));
			setReal_NutSilo(realNS.setScale(0, RoundingMode.HALF_UP));
			setReal_NutFloor(realNF.setScale(0, RoundingMode.HALF_UP));
			setReal_StockKernelToday(getReal_NutSilo().add(getReal_NutFloor()).add(getKernelFloor()).add(getKernelFloorBad())
					.add(getReal_KernelSilo1()).add(getReal_KernelSilo2()).add(getReal_KernelSilo3()).add(getBulkingSilo())
					.setScale(0, RoundingMode.HALF_UP));
			
			setKernelDeliveredQty(m_DeliveredKRNL);
			setKernelReturnQty(m_ReturnKRNL);
			
			
			if(getFFBProcessedNettoI().signum()==1 || getFFBProcessedNettoII().signum()==1){
				//setEstimate
				setProductionKernel(getStockKernelToday().add(getKernelDeliveredQty().subtract(getPreviousKernelQty())));
				setKERNettoI(getProductionKernel().divide(getFFBProcessedNettoI(), 4, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED));
				setKERNettoII(getProductionKernel().divide(getFFBProcessedNettoII(), 4, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED));
				
				//setReal
				setReal_ProductionKernel(getReal_StockKernelToday().add(getKernelDeliveredQty().subtract(getReal_PreviousKernelQty())));
				setReal_KERNettoI(getReal_ProductionKernel().divide(getFFBProcessedNettoI(), 4, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED));
				setReal_KERNettoII(getReal_ProductionKernel().divide(getFFBProcessedNettoII(), 4, RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED));
				
			} else {
				setProductionKernel(Env.ZERO);
				setKERNettoI(Env.ZERO);
				setKERNettoII(Env.ZERO);
				setReal_KERNettoI(Env.ZERO);
				setReal_KERNettoII(Env.ZERO);
			}
		}
		
		if(newRecord || productName.equals(PRODUCT_CANGKANG))
		{	
			if(getFFBProcessedNettoI().signum()==1 && (newRecord || !is_ValueChanged(COLUMNNAME_ProductionCangkang)))
				{
				setProductionCangkang((shellprodpercentage.multiply(getFFBProcessedNettoI()))
						.divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_UP));
				setCangkangBoiler(boilerpercentage.multiply(getFFBProcessedNettoI()).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_UP));
				}
			else if(getFFBProcessedNettoI().signum()==0)
				{
				setProductionCangkang(Env.ZERO);
				setCangkangBoiler(Env.ZERO);
				}
			
			setCangkangInHopper(getProductionCangkang().subtract(getCangkangBoiler()));
		}
		
		return null;
	}
	
	public MUNSSounding getPrevSounding()
	{
//		if(m_PrevSounding != null)
//		{
//			return m_PrevSounding;
//		}
		
		if (getUNS_PrevSounding_ID() == 0)
			m_PrevSounding = new Query(getCtx(), Table_Name, COLUMNNAME_UNS_Sounding_ID + "<>? AND DocStatus IN (?,?)", get_TrxName())
								.setParameters(get_ID(),"CO","CL").setOrderBy(COLUMNNAME_DateAcct + " DESC").first();
		else 
			m_PrevSounding = new MUNSSounding(getCtx(), getUNS_PrevSounding_ID(), get_TrxName());
		
		return m_PrevSounding;
	}
	
	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable()
	{
		List<Object[]> list = new ArrayList<>();
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{String.class, true});
		
		return list;
	}

	@Override
	public String[] getDetailTableHeader()
	{
		String def[] = new String[]{"Tank", "Sounding Time", "Height", "Temperature", "Last Sounding",
				"Sounding Qty", "Difference", "Description"};
		
		return def;
	}

	@Override
	public List<Object[]> getDetailTableContent()
	{
		List<Object[]> list = new ArrayList<>();
		String sql = "SELECT tank.Name AS Tank, DATE_PART('hours', s.StartTime) || ':' || DATE_PART('minutes', s.StartTime)"
				+ " || '-' || DATE_PART('hours', s.EndTime) || ':' || DATE_PART('minute', s.EndTime) AS Time," //1..StartTime-EndTime
				+ " ts.HeightMeasured AS HM, ts.Temperature AS TMP, ts.LastSoundingQty AS Last,"
				+ " ts.Weight AS Weight, ts.DifferenceQty AS Diff, ts.Description AS Desc"
				+ " FROM UNS_TankSounding ts INNER JOIN UNS_Sounding s ON s.UNS_Sounding_ID = ts.UNS_Sounding_ID"
				+ " INNER JOIN UNS_Tank tank ON tank.UNS_Tank_ID = ts.UNS_Tank_ID"
				+ " WHERE s.UNS_Sounding_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				int count = 0;
				Object[] rowData = new Object[8];
				rowData[count] = rs.getObject("Tank");
				rowData[++count] = rs.getObject("Time");
				rowData[++count] = rs.getObject("HM");
				rowData[++count] = rs.getObject("TMP");
				rowData[++count] = rs.getObject("Last");
				rowData[++count] = rs.getObject("Weight");
				rowData[++count] = rs.getObject("Diff");
				rowData[++count] = rs.getObject("Desc");
				
				list.add(rowData);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		return list;
	}
	
	/**
	 * Create production based on this sounding results.
	 * @return A String of error message if an error occurs, otherwise null if success.
	 */
	public String createProduction()
	{
		return createProduction(getUNS_Resource_ID(), getEndTime());
	} // createProduction
	
	/**
	 * Create production based on the result of sounding calculation.
	 * 
	 * @param UNS_Resource_ID
	 * @param DateProduction
	 * @return
	 */
	public String createProduction(int UNS_Resource_ID, Timestamp DateProduction) {
		
		MUNSResource rsc = new MUNSResource(getCtx(), UNS_Resource_ID, get_TrxName());
		MUNSProduction production = null;
		if (getUNS_Production_ID() > 0) {
			production = new MUNSProduction(getCtx(), getUNS_Production_ID(), 
					get_TrxName());
		} else {
			production = new MUNSProduction(rsc);
		}
		
		try {
			production.setProductionQty(this.getCPOProductionQty());
			production.setPSType(MUNSProduction.PSTYPE_MasterProductionSchedule);
			production.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_Production));
			Timestamp date = TimeUtil.addDays(getDateAcct(), -1);
			production.setMovementDate(date);
			production.setDatePromised(date);
			production.setProductionDate(date);
			production.saveEx();
		} catch (Exception ex) {
			return ex.getMessage();
		}
		
		MUNSProductionDetail[] pdList = production.getLines();
		boolean isFFBInitialized = false;
		boolean isCangkangUsedInitialized = false;
		//boolean is
		
		int output_ID = 0;
		
		for (MUNSProductionDetail pd : pdList)
		{
			BigDecimal productionQty = Env.ZERO;
			switch (MProduct.get(getCtx(), pd.getM_Product_ID()).getValue()){
				case PRODUCT_CPO		: 
					productionQty = isManualSounding() ? getCPOProductionQty() : getFlowMeterQty();
					output_ID = pd.getM_Product_ID();
					break;
				case PRODUCT_FFB		: 
					productionQty = getFFBProcessedNettoI();
					isFFBInitialized = true;
					break;
				case PRODUCT_CANGKANG	:
					if (pd.isEndProduct())
						productionQty = getProductionCangkang();
					else {
						productionQty = getCangkangBoiler();
						isCangkangUsedInitialized = true;
					}
					break;
				case PRODUCT_KERNEL		: 
					productionQty = getReal_ProductionKernel();
					break;
			}
			
			if (pd.isEndProduct())
			{
				pd.setMovementQty(productionQty);
			}
			else { 
				pd.setMovementQty(productionQty.negate());
			}
			
			pd.setQtyUsed(productionQty);
			pd.saveEx();
		}
		
		// Set the FFB as the input production if it's not initialized.
		String sql = "SELECT bom.M_Product_BOM_ID FROM M_Product_BOM bom WHERE M_Product_ID=? "
				+ "AND M_ProductBOM_ID=(SELECT M_Product_ID FROM M_Product WHERE Value=?)";
		
		if (!isFFBInitialized)
		{
			int productBOM_ID = DB.getSQLValueEx(get_TrxName(), sql, output_ID, PRODUCT_FFB);
			
			if (productBOM_ID <= 0) 
			{
				return "Error: Please define Formula (BOM) for CPO with FFB as the input!";
			}
			
			MUNSProductionDetail input = new MUNSProductionDetail(production);
			input.setM_Product_BOM_ID(productBOM_ID);
			input.beforeSaveTabRecord(production.get_ID());
			
			input.setMovementQty(getFFBProcessedNettoI().negate());
			input.setQtyUsed(getFFBProcessedNettoI());
			input.saveEx();
		}
		
		if(!isCangkangUsedInitialized)
		{
			int productBOM_ID = DB.getSQLValueEx(get_TrxName(), sql, output_ID, PRODUCT_CANGKANG);
			
			if (productBOM_ID <= 0)
			{
				return "Error: Please define Formula (BOM) for CPO with Cangkang as the input!";
			}
			
			MUNSProductionDetail input = new MUNSProductionDetail(production);
			input.setM_Product_BOM_ID(productBOM_ID);
			input.beforeSaveTabRecord(production.get_ID());
			
			input.setMovementQty(getCangkangBoiler().negate());
			input.setQtyUsed(getCangkangBoiler());
			input.saveEx();
		}
		
		try {
			if (!production.processIt(DOCACTION_Complete))
			{
				return production.getProcessMsg();
			}
			
			production.saveEx();
			setUNS_Production_ID(production.get_ID());
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}

	/**
	 * Initialized the Kernel delivered.
	 */
	private void initDeliveredKernelQty()
	{
		
		Timestamp startRange = getUNS_PrevSounding_ID() == 0 && this.getPrevSounding() == null? 
				TimeUtil.addDays(getDateAcct(), -1) : this.getPrevSounding().getDateAcct();
		
		String sql = "SELECT COALESCE(SUM(tr.movementqty * -1),0) FROM M_Transaction tr WHERE tr.movementtype = 'T-' "
				+ "AND m_locator_id = ? AND EXISTS (SELECT 1 FROM UNS_WeighbridgeTicket wt WHERE wt.UNS_WeighbridgeTicket_ID ="
				+ "  tr.UNS_WeighbridgeTicket_ID AND wt.M_Product_ID=(SELECT M_Product_ID FROM M_Product WHERE Value = ? ))"
				+ "AND tr.MovementDate BETWEEN ?::timestamp AND ?::timestamp";
		
		String sql2 = "SELECT COALESCE(SUM(tr.movementqty),0) FROM M_Transaction tr WHERE tr.movementtype = 'T-' "
				+ "AND m_locator_id = ? AND EXISTS (SELECT 1 FROM UNS_WeighbridgeTicket wt WHERE wt.UNS_WeighbridgeTicket_ID ="
				+ "  tr.UNS_WeighbridgeTicket_ID AND wt.M_Product_ID=(SELECT M_Product_ID FROM M_Product WHERE Value = ? )"
				+ "	 AND isCancelled = ? )"
				+ "AND tr.MovementDate BETWEEN ?::timestamp AND ?::timestamp";

		m_DeliveredKRNL = DB.getSQLValueBD(get_TrxName(), sql,
				new Object[]{1000325, "KRNL",startRange, TimeUtil.addDays(getDateAcct(), -1)});
		
		m_ReturnKRNL = DB.getSQLValueBD(get_TrxName(), sql2, 
				new Object[]{1000325, "KRNL", "Y",startRange, TimeUtil.addDays(getDateAcct(), -1)});
	}
	
	private void initCPOReturnQty()
	{
		Timestamp startRange = getUNS_PrevSounding_ID() == 0 && this.getPrevSounding() == null? 
				TimeUtil.addDays(getDateAcct(), -1) : this.getPrevSounding().getDateAcct();
		
		String sql = "SELECT COALESCE(SUM(tr.movementqty),0) FROM M_Transaction tr WHERE tr.movementtype = 'T-' AND movementqty > 0"
				+ "AND m_locator_id IN (?,?,?) AND EXISTS (SELECT 1 FROM UNS_WeighbridgeTicket wt WHERE wt.UNS_WeighbridgeTicket_ID ="
				+ "  tr.UNS_WeighbridgeTicket_ID AND wt.M_Product_ID=(SELECT M_Product_ID FROM M_Product WHERE Value = ? )"
				+ "	 AND isCancelled = ? )"
				+ "AND tr.MovementDate BETWEEN ?::timestamp AND ?::timestamp";
		
		m_ReturnCPO = DB.getSQLValueBD(get_TrxName(), sql, 
				new Object[]{1000324,1000451,1000452,"CPO", "Y", startRange, TimeUtil.addDays(getDateAcct(), -1)});
	}
	
	private void createMovement () 
	{
		MMovement move = new MMovement(getCtx(), 0, get_TrxName());
		move.setAD_Org_ID(getAD_Org_ID());
		move.setC_DocType_ID(MDocType.getDocType(
				MDocType.DOCBASETYPE_MaterialMovement));
		move.setDescription("::Auto Generated From Sounding::");
		Timestamp date = TimeUtil.addDays(getDateAcct(), -1);
		move.setMovementDate(date);
		
		//CPO
		List<MovementHelper> cpoToMoves = initialCPOToMove();
		for (int i=0; i<cpoToMoves.size(); i++)
		{			
			for (int j=0; j<cpoToMoves.size(); j++)
			{
				BigDecimal difference = cpoToMoves.get(i).getDifference();
				if (difference.signum() >= 0)
				{
					break;
				}
				
				int locFromID = cpoToMoves.get(i).getM_Locator_ID();
				int locToID = 0;
				BigDecimal movementQty = difference.abs();
				
				BigDecimal jDiff = cpoToMoves.get(j).getDifference();
				if (jDiff.signum() <= 0)
				{
					continue;
				}
				
				if (move.get_ID() == 0 && !move.save())
				{
					m_processMsg = CLogger.retrieveErrorString(
							"Could not save Movement Document");
					return;
				}
				
				if (movementQty.compareTo(jDiff) == 1)
				{
					movementQty = jDiff;
				}
				
				locToID = cpoToMoves.get(j).getM_Locator_ID();
				if (locToID == 0 || locFromID == 0 || locToID == locFromID)
				{
					continue;
				}
				
				MMovementLine line = new MMovementLine(move);
				line.setM_Locator_ID(locFromID);
				line.setM_LocatorTo_ID(locToID);
				line.setM_Product_ID(cpoToMoves.get(i).getM_Product_ID());
				line.setMovementQty(movementQty);
				if (!line.save())
				{
					m_processMsg = CLogger.retrieveErrorString(
							"Could not save Movement Line.");
					return;
				}
				
				cpoToMoves.get(j).allocate(movementQty);
				cpoToMoves.get(i).allocate(movementQty.negate());	
			}
		}
		//End CPO
		if (move.get_ID()> 0)
		{
			if (!move.processIt(DOCACTION_Complete))
			{
				m_processMsg = move.getProcessMsg();
				return;
			}
			move.saveEx();
			
			setM_Movement_ID(move.get_ID());
		}
	}
	
	public MUNSTankSounding[] getLines ()
	{
		return getLines(false, null);
	}
	
	/**
	 * 
	 * @param requery
	 * @param orderBy
	 * @return
	 */
	public MUNSTankSounding[] getLines (boolean requery, String orderBy)
	{
		if (null != m_lines && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSTankSounding> list = com.uns.base.model.Query.get(
				getCtx(), UNSPPICModelFactory.getExtensionID(), 
				MUNSTankSounding.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).
				setOrderBy(orderBy).list();
		
		m_lines = new MUNSTankSounding[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}
	
	public List<MovementHelper> initialCPOToMove ()
	{
		StringBuilder sqlBuild = new StringBuilder("SELECT t.")
		.append(MUNSTank.COLUMNNAME_M_Locator_ID).append(", ts.");
		
		if(isManualSounding())
			sqlBuild.append(MUNSTankSounding.COLUMNNAME_Weight);
		else
			sqlBuild.append(MUNSTankSounding.COLUMNNAME_WeightByFlowMeter);
		
		sqlBuild.append(", ").append("(SELECT COALESCE(SUM(")
		.append(MStorageOnHand.COLUMNNAME_QtyOnHand).append("), 0) FROM ")
		.append(MStorageOnHand.Table_Name).append(" WHERE ")
		.append(MStorageOnHand.COLUMNNAME_M_Locator_ID).append(" = t.")
		.append(MUNSTank.COLUMNNAME_M_Locator_ID).append(" AND ")
		.append(MStorageOnHand.COLUMNNAME_M_Product_ID).append(" = ? ), ")
		.append("(SELECT COALESCE(SUM(")
		.append(MTransaction.COLUMNNAME_MovementQty).append("), 0) FROM ")
		.append(MTransaction.Table_Name).append(" WHERE ")
		.append(MTransaction.COLUMNNAME_M_Product_ID).append(" = ? AND ")
		.append(MTransaction.COLUMNNAME_M_Locator_ID).append(" = t.")
		.append(MUNSTank.COLUMNNAME_M_Locator_ID).append(" AND ")
		.append(MTransaction.COLUMNNAME_MovementDate).append(" >= ? ) ")
		.append(" FROM ").append(MUNSTankSounding.Table_Name)
		.append(" ts INNER JOIN ").append(MUNSTank.Table_Name)
		.append(" t ON t.").append(MUNSTank.COLUMNNAME_UNS_Tank_ID)
		.append(" = ts.").append(MUNSTankSounding.COLUMNNAME_UNS_Tank_ID)
		.append(" WHERE ts.").append(MUNSTankSounding.COLUMNNAME_UNS_Sounding_ID)
		.append(" = ? AND ts.").append(MUNSTankSounding.COLUMNNAME_IsActive)
		.append(" = ? ");

		List<MovementHelper> list = new ArrayList<>();
		String sql = "SELECT M_Product_ID FROM M_Product WHERE Value = ?";
		int cpoID = DB.getSQLValue(get_TrxName(), sql, PRODUCT_CPO);
		sql = sqlBuild.toString();
		PreparedStatement st = DB.prepareStatement(sql, get_TrxName());
		ResultSet rs = null;
		
		try
		{
			int i =0;
			st.setInt(++i, cpoID);
			st.setInt(++i, cpoID);
			st.setTimestamp(++i, getDateAcct());
			st.setInt(++i, getUNS_Sounding_ID());
			st.setString(++i, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal sysStock = rs.getBigDecimal(3);
				BigDecimal trxStock = rs.getBigDecimal(4);
				if (null == trxStock)
				{
					trxStock = Env.ZERO;
				}
				if (null == sysStock)
				{
					sysStock = Env.ZERO;
				}
				sysStock = sysStock.subtract(trxStock);
				MovementHelper helper = new MovementHelper(
						rs.getInt(1), cpoID, rs.getBigDecimal(2), sysStock);
				list.add(helper);
			}
		}
		catch (SQLException ex)
		{
			throw new AdempiereException(ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return list;
	}
	
	private boolean createCPOPhysical ()
	{
		getLines();
		MInventory inventory = null;
		for (int i=0; i<m_lines.length; i++)
		{
			MUNSTankSounding tank = m_lines[i];
			if (tank.getWeight().compareTo(tank.getLastSoundingQty()) == 0)
			{
				continue;
			}
			
			int M_Locator_ID = DB.getSQLValue(
					get_TrxName(), 
					"SELECT M_Locator_ID FROM UNS_Tank WHERE UNS_Tank_ID = ?", 
					tank.getUNS_Tank_ID());
			
			if (null == inventory)
			{
				String sql = "SELECT M_Warehouse_ID FROM M_Locator WHERE M_Locator_ID = ?";
				int whs_ID = DB.getSQLValue(get_TrxName(), sql, M_Locator_ID);
				inventory = new MInventory(getCtx(), 0, get_TrxName());
				inventory.setM_Warehouse_ID(whs_ID);
				inventory.setClientOrg(getAD_Client_ID(), getAD_Org_ID());
				int docType_ID = MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventory);
				inventory.setMovementDate(TimeUtil.addDays(getDateAcct(), -1));
				inventory.setDateAcct(TimeUtil.addDays(getDateAcct(), -1));
				inventory.setC_DocType_ID(docType_ID);
				inventory.setDocumentNo("SND_" + getDocumentNo());
				if (!inventory.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Could not create inventory");
					return false;
				}

				setM_Inventory_ID (inventory.get_ID());
			}
			
			int cpo_ID = DB.getSQLValue(
					get_TrxName(), 
					"SELECT M_Product_ID FROM M_Product WHERE Value = ? ", 
					PRODUCT_CPO);
			String sql = "SELECT COALESCE(SUM(MovementQty), 0) FROM M_Transaction WHERE "
					+ " M_Product_ID = ? AND M_Locator_ID = ? AND MovementDate >= ? ";
			BigDecimal totTransaction = DB.getSQLValueBD(get_TrxName(), sql,cpo_ID, M_Locator_ID, getDateAcct());
			BigDecimal currentQty = tank.getWeight().add(totTransaction);
			MInventoryLine line = new MInventoryLine(
					inventory, M_Locator_ID, cpo_ID, 0, null, 
					currentQty);
			line.setInventoryType(MInventoryLine.INVENTORYTYPE_InventoryDifference);
			if (!line.save())
			{
				m_processMsg = CLogger.retrieveErrorString("Failed when try to save inventory line.");
				return false;
			}
		}
		
		if (null != inventory && !inventory.processIt(DocAction.ACTION_Complete))
		{
			m_processMsg = inventory.getProcessMsg();
			return false;
		}
		
		if (!inventory.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not save inventory");
			return false;
		}
		
		return true;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}

class MovementHelper 
{
	private int m_M_Product_ID = 0;
	private int m_M_Locator_ID = 0;
	private BigDecimal m_realQty = Env.ZERO;
	private BigDecimal m_sysQty = Env.ZERO;
	private BigDecimal m_unallocated = Env.ZERO;
	private BigDecimal m_allocated = Env.ZERO;
	
	public int getM_Product_ID ()
	{
		return m_M_Product_ID;
	}
	
	public void setM_Product_ID (int M_Product_ID)
	{
		m_M_Product_ID = M_Product_ID;
	}
	
	public BigDecimal getDifference ()
	{
		return getUnallocatedQty().subtract(getAllocatedQty());
	}
	
	public BigDecimal getAllocatedQty ()
	{
		return m_allocated;
	}
	
	public BigDecimal getUnallocatedQty ()
	{
		return m_unallocated;
	}
	
	public void allocate (BigDecimal toAllocate)
	{
		m_allocated = m_allocated.add(toAllocate);
	}
	
	public BigDecimal getSysQty ()
	{
		return m_sysQty;
	}
	
	public void setSysQty (BigDecimal sysQty)
	{
		if (null == sysQty)
			return;
		m_sysQty = sysQty;
	}
	
	public BigDecimal getRealQty ()
	{
		return m_realQty;
	}
	
	public void setRealQty (BigDecimal realQty)
	{
		if (null == realQty)
			return;
		m_realQty = realQty;
	}
	
	public int getM_Locator_ID ()
	{
		return m_M_Locator_ID;
	}
	
	public void setM_Locator_ID (int M_Locator_ID)
	{
		m_M_Locator_ID = M_Locator_ID;
	}
	
	public MovementHelper (int M_Locator_ID, int M_Product_ID,  
			BigDecimal realQty, BigDecimal sysQty)
	{
		m_M_Locator_ID = M_Locator_ID;
		m_M_Product_ID = M_Product_ID;
		m_realQty = realQty;
		m_sysQty = sysQty;
		m_unallocated = m_realQty.subtract(m_sysQty);
	}
}