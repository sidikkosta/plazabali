package com.uns.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.Util;

public class MUNSFlowmeterConfiguration extends X_UNS_FlowmeterConfiguration {

	/**
	 * @author Thunder
	 */
	private static final long serialVersionUID = 7428087497373375547L;
	
	private final String[] m_operand = {"*","/","+","-"};
	
	public MUNSFlowmeterConfiguration(Properties ctx,
			int UNS_FlowmeterConfiguration_ID, String trxName) {
		super(ctx, UNS_FlowmeterConfiguration_ID, trxName);
	}

	public MUNSFlowmeterConfiguration(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		String errorLogic = validateLogic(getCalcLogic());
		if(!Util.isEmpty(errorLogic, true))
		{
			throw new AdempiereException(errorLogic);
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param logic
	 * @return
	 */
	public String validateLogic(String logic)
	{
		if (Util.isEmpty(logic, true)) 
		{
			return "Empty logic!!!";
		}
	
		int openBrackets = 0;
		boolean componenopenned = false;
		boolean decimalOpen = false;
		String componentName = "";
		String lastChar = "";
		List<String> components = new ArrayList<String>();
		
		for (int i=0; i<logic.length(); i++)
		{
			
			String current = logic.substring(i, i+1);
			if(i==0)
			{
				lastChar = current;
			}
			if(Util.isEmpty(current, true) && !componenopenned)
			{
				continue;
			}
			
			boolean currentIsComponentInitial = current.equals("@");
			boolean currentIsOpenBrackets = current.equals("(");
			boolean currentIsCloseBrackets = current.equals(")");
			boolean currentIsDigit = Character.isDigit(logic.charAt(i));
			boolean currentIsDot = current.equals(".");
			boolean lastIsComponentInitial = lastChar.equals("@");
			boolean lastIsOpenBrackets = lastChar.equals("(");
			boolean lastIsCloseBrackets = lastChar.equals(")");
			boolean lastIsDot = lastChar.equals("'");
			boolean lastIsDigit = Character.isDigit(lastChar.charAt(0));
			if(current.equals("@") && !componenopenned) 
			{
				componenopenned = true;
			}
			else if(currentIsComponentInitial && componenopenned) 
			{
				componenopenned = false;
				components.add(componentName);
				componentName = "";
			}
			else if(componenopenned)
			{
				componentName = componentName.concat(current);
			}
			else if (!currentIsDigit && !isValidOperand(current) 
					&& !currentIsOpenBrackets && !currentIsCloseBrackets 
					&& !currentIsDot)
			{
				return "Invalid character [" + current + "] at index " + i;
			}
			else if(currentIsDigit && !lastIsDigit && !lastIsOpenBrackets 
					&& lastIsDot&& !isValidOperand(lastChar))
			{
				return "Invalid character [" + current + "] at index " + i;
			}
			else
			{
				if(currentIsOpenBrackets) 
				{
					if(!lastIsOpenBrackets && !isValidOperand(lastChar))
					{
						return "Invalid character [" + current + "] at index " + i;
					}
					
					openBrackets++;
				}
				else if(currentIsCloseBrackets) 
				{
					if(!lastIsDigit && !lastIsCloseBrackets && !lastIsComponentInitial)
					{
						return "Invalid character [" + current + "] at index " + i;
					}
					openBrackets--;
				}
				else if(currentIsDot)
				{
					if(!lastIsDigit)
					{
						return "Invalid character [" + current + "] at index " + i;
					}
					
					decimalOpen = true;
				}
				else if(currentIsDigit)
				{
					decimalOpen = false;
				}
			}
			
			lastChar = current;
		}
		
		if(isValidOperand(lastChar))
		{
			return "Invalid calculation logic at end of logic.";
		}
		else if(decimalOpen)
		{
			return "Invalid Decimal value at end of calculation logic.";
		}
		else if(openBrackets != 0) 
		{
			return "Unbalance brackets";
		}
		else if (componenopenned)
		{
			return "Unclosed component";
		}
		
		for (int j=0; j<components.size(); j++)
		{
			if (getComponent(components.get(j)) == null)
			{
				return "No Component with value " + components.get(j);
			}
		}	
		
		return null;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean isValidOperand(String value)
	{
		for(int i=0; i<m_operand.length; i++) 
		{
			if(value.equals(m_operand[i]))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public MUNSFlowMeter getComponent(String name) {
		
		if(name == null)
			return null;
		
		MUNSFlowMeter flowmeter = MUNSFlowMeter.get(getCtx(), name, get_TrxName());
		
		return flowmeter;
	}
}
