package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

public class MUNSTankRing extends X_UNS_TankRing {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3145129988660054350L;

	public MUNSTankRing(Properties ctx, int UNS_TankRing_ID, String trxName) {
		super(ctx, UNS_TankRing_ID, trxName);
	
	}

	public MUNSTankRing(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	public static int getRingID(BigDecimal grossheight, int tank_ID, String trxName){
		int value = 0;
		
		String sql= "SELECT UNS_TankRing_ID FROM UNS_TankRing WHERE ? BETWEEN HeightFrom AND HeightTo"
				+ " AND UNS_Tank_ID = ? ";
		value = DB.getSQLValueEx(trxName, sql, grossheight, tank_ID);
		
		return value; 
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{	
		String sql = "SELECT COALESCE(MAX(LineNo),0)+1 FROM UNS_TankRing"
				+ " WHERE UNS_TankRing_ID = ? AND isactive = 'Y'";
		int line = DB.getSQLValueEx (get_TrxName(), sql, getUNS_TankRing_ID());
		setLineNo(line);
		
		return super.beforeSave(newRecord);
	}
	
	

}
