/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.qad.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Quality_Specs
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Quality_Specs extends PO implements I_UNS_Quality_Specs, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160109L;

    /** Standard Constructor */
    public X_UNS_Quality_Specs (Properties ctx, int UNS_Quality_Specs_ID, String trxName)
    {
      super (ctx, UNS_Quality_Specs_ID, trxName);
      /** if (UNS_Quality_Specs_ID == 0)
        {
			setExpectedResultFrom (Env.ZERO);
// 0
			setExpectedResultTo (Env.ZERO);
// 0
			setM_Product_ID (0);
			setUNS_Quality_Specs_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Quality_Specs (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Quality_Specs[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Expected Result From.
		@param ExpectedResultFrom Expected Result From	  */
	public void setExpectedResultFrom (BigDecimal ExpectedResultFrom)
	{
		set_Value (COLUMNNAME_ExpectedResultFrom, ExpectedResultFrom);
	}

	/** Get Expected Result From.
		@return Expected Result From	  */
	public BigDecimal getExpectedResultFrom () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ExpectedResultFrom);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Expected Result To.
		@param ExpectedResultTo Expected Result To	  */
	public void setExpectedResultTo (BigDecimal ExpectedResultTo)
	{
		set_Value (COLUMNNAME_ExpectedResultTo, ExpectedResultTo);
	}

	/** Get Expected Result To.
		@return Expected Result To	  */
	public BigDecimal getExpectedResultTo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ExpectedResultTo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Product Quality Specifications.
		@param UNS_Quality_Specs_ID Product Quality Specifications	  */
	public void setUNS_Quality_Specs_ID (int UNS_Quality_Specs_ID)
	{
		if (UNS_Quality_Specs_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Quality_Specs_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Quality_Specs_ID, Integer.valueOf(UNS_Quality_Specs_ID));
	}

	/** Get Product Quality Specifications.
		@return Product Quality Specifications	  */
	public int getUNS_Quality_Specs_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Quality_Specs_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Quality_Specs_UU.
		@param UNS_Quality_Specs_UU UNS_Quality_Specs_UU	  */
	public void setUNS_Quality_Specs_UU (String UNS_Quality_Specs_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Quality_Specs_UU, UNS_Quality_Specs_UU);
	}

	/** Get UNS_Quality_Specs_UU.
		@return UNS_Quality_Specs_UU	  */
	public String getUNS_Quality_Specs_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Quality_Specs_UU);
	}

	/** Set Valid from.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid from.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}

	/** Set Valid to.
		@param ValidTo 
		Valid to including this date (last day)
	  */
	public void setValidTo (Timestamp ValidTo)
	{
		set_Value (COLUMNNAME_ValidTo, ValidTo);
	}

	/** Get Valid to.
		@return Valid to including this date (last day)
	  */
	public Timestamp getValidTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidTo);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}