/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.qad.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Production_Quality
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Production_Quality extends PO implements I_UNS_Production_Quality, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160528L;

    /** Standard Constructor */
    public X_UNS_Production_Quality (Properties ctx, int UNS_Production_Quality_ID, String trxName)
    {
      super (ctx, UNS_Production_Quality_ID, trxName);
      /** if (UNS_Production_Quality_ID == 0)
        {
			setDateTested (new Timestamp( System.currentTimeMillis() ));
			setDocStatus (null);
// DR
			setDocumentNo (null);
			setIsQCPass (false);
// N
			setM_Product_ID (0);
			setProcessed (false);
// N
			setTestResult (Env.ZERO);
			setUNS_Production_Detail_ID (0);
			setUNS_Production_ID (0);
			setUNS_Production_Quality_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Production_Quality (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Production_Quality[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Tested.
		@param DateTested Date Tested	  */
	public void setDateTested (Timestamp DateTested)
	{
		set_Value (COLUMNNAME_DateTested, DateTested);
	}

	/** Get Date Tested.
		@return Date Tested	  */
	public Timestamp getDateTested () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTested);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set QC Pass.
		@param IsQCPass QC Pass	  */
	public void setIsQCPass (boolean IsQCPass)
	{
		set_Value (COLUMNNAME_IsQCPass, Boolean.valueOf(IsQCPass));
	}

	/** Get QC Pass.
		@return QC Pass	  */
	public boolean isQCPass () 
	{
		Object oo = get_Value(COLUMNNAME_IsQCPass);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Notes.
		@param Notes Notes	  */
	public void setNotes (String Notes)
	{
		set_Value (COLUMNNAME_Notes, Notes);
	}

	/** Get Notes.
		@return Notes	  */
	public String getNotes () 
	{
		return (String)get_Value(COLUMNNAME_Notes);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity Manufactured.
		@param QtyManufactured 
		Quantity that already manufactured for the requested/ordered product.
	  */
	public void setQtyManufactured (BigDecimal QtyManufactured)
	{
		set_ValueNoCheck (COLUMNNAME_QtyManufactured, QtyManufactured);
	}

	/** Get Quantity Manufactured.
		@return Quantity that already manufactured for the requested/ordered product.
	  */
	public BigDecimal getQtyManufactured () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyManufactured);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Test Result.
		@param TestResult Test Result	  */
	public void setTestResult (BigDecimal TestResult)
	{
		set_Value (COLUMNNAME_TestResult, TestResult);
	}

	/** Get Test Result.
		@return Test Result	  */
	public BigDecimal getTestResult () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TestResult);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Production_Detail getUNS_Production_Detail() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Production_Detail)MTable.get(getCtx(), com.uns.model.I_UNS_Production_Detail.Table_Name)
			.getPO(getUNS_Production_Detail_ID(), get_TrxName());	}

	/** Set Production Detail.
		@param UNS_Production_Detail_ID Production Detail	  */
	public void setUNS_Production_Detail_ID (int UNS_Production_Detail_ID)
	{
		if (UNS_Production_Detail_ID < 1) 
			set_Value (COLUMNNAME_UNS_Production_Detail_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Production_Detail_ID, Integer.valueOf(UNS_Production_Detail_ID));
	}

	/** Get Production Detail.
		@return Production Detail	  */
	public int getUNS_Production_Detail_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_Detail_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Production getUNS_Production() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Production)MTable.get(getCtx(), com.uns.model.I_UNS_Production.Table_Name)
			.getPO(getUNS_Production_ID(), get_TrxName());	}

	/** Set Production.
		@param UNS_Production_ID Production	  */
	public void setUNS_Production_ID (int UNS_Production_ID)
	{
		if (UNS_Production_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_ID, Integer.valueOf(UNS_Production_ID));
	}

	/** Get Production.
		@return Production	  */
	public int getUNS_Production_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Production QC Result.
		@param UNS_Production_Quality_ID Production QC Result	  */
	public void setUNS_Production_Quality_ID (int UNS_Production_Quality_ID)
	{
		if (UNS_Production_Quality_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_Quality_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Production_Quality_ID, Integer.valueOf(UNS_Production_Quality_ID));
	}

	/** Get Production QC Result.
		@return Production QC Result	  */
	public int getUNS_Production_Quality_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Production_Quality_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Production_Quality_UU.
		@param UNS_Production_Quality_UU UNS_Production_Quality_UU	  */
	public void setUNS_Production_Quality_UU (String UNS_Production_Quality_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Production_Quality_UU, UNS_Production_Quality_UU);
	}

	/** Get UNS_Production_Quality_UU.
		@return UNS_Production_Quality_UU	  */
	public String getUNS_Production_Quality_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Production_Quality_UU);
	}

	/** Set Product Quality Specifications.
		@param UNS_Quality_Specs_ID Product Quality Specifications	  */
	public void setUNS_Quality_Specs_ID (int UNS_Quality_Specs_ID)
	{
		if (UNS_Quality_Specs_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Quality_Specs_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Quality_Specs_ID, Integer.valueOf(UNS_Quality_Specs_ID));
	}

	/** Get Product Quality Specifications.
		@return Product Quality Specifications	  */
	public int getUNS_Quality_Specs_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Quality_Specs_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
	
	public com.uns.qad.model.I_UNS_Quality_Specs getUNS_Quality_Specs() throws RuntimeException
    {
		return (com.uns.qad.model.I_UNS_Quality_Specs)MTable.get(getCtx(), com.uns.qad.model.I_UNS_Quality_Specs.Table_Name)
			.getPO(getUNS_Quality_Specs_ID(), get_TrxName());
    }
}