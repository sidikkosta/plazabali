package com.uns.qad.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.compiere.util.Env;
import com.uns.base.model.Query;

public class MUNSQualitySpecs extends X_UNS_Quality_Specs {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9134293433146935500L;

	public MUNSQualitySpecs(Properties ctx, int UNS_Quality_Specs_ID,
			String trxName) 
	{
		super(ctx, UNS_Quality_Specs_ID, trxName);
	}

	public MUNSQualitySpecs(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	public static MUNSQualitySpecs[] getOfProduct (
			String trxName, int product_ID)
	{
		String whereClause = "M_Product_ID = ? AND IsActive = ? ";
		List<MUNSQualitySpecs> list = Query.get(
				Env.getCtx(), "UNSPPICModelFactory", Table_Name, 
				whereClause, trxName).setParameters(product_ID, "Y").
				list();
		MUNSQualitySpecs[] specs = new MUNSQualitySpecs[list.size()];
		list.toArray(specs);
		
		return specs;
	}
}
