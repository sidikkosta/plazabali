/**
 * 
 */
package com.uns.qad.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.compiere.util.Util;

import com.uns.model.MUNSProduction;
import com.uns.model.MUNSProductionDetail;

/**
 * @author AzHaidar
 * @author	Menjangan@UntaSoft getCreate By production detail and quality specs,
 * 			get By Production
 */
public class MUNSProductionQuality extends X_UNS_Production_Quality implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6417632115483561075L;

	/**
	 * @param ctx
	 * @param UNS_Production_Quality_ID
	 * @param trxName
	 */
	public MUNSProductionQuality(Properties ctx, int UNS_Production_Quality_ID,
			String trxName) {
		super(ctx, UNS_Production_Quality_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSProductionQuality(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		I_UNS_Quality_Specs spec = getUNS_Quality_Specs();
		
		if (Util.isEmpty(getValue(), true)) {
			setValue(spec.getValue());
		}
		
		if (Util.isEmpty(getName(), true)) {
			setName(spec.getName());
		}
		
		if (getM_Product_ID() == 0) {
			setM_Product_ID(spec.getM_Product_ID());
			setC_UOM_ID(spec.getC_UOM_ID());
		}
		
		if (getTestResult().compareTo(spec.getExpectedResultFrom()) >= 0
				&& getTestResult().compareTo(spec.getExpectedResultTo()) <= 0) {
			setIsQCPass(true);
		}
		else {
			setIsQCPass(false);
		}
		
		if (newRecord)
		{
			String sql = "SELECT count(*) FROM UNS_Production_Quality "
					+ "WHERE UNS_Production_ID=? AND UNS_Quality_Specs_ID=? AND UNS_Production_Detail_ID=?";
			boolean exists = DB.getSQLValueEx(
					get_TrxName(), sql, getUNS_Production_ID(), getUNS_Quality_Specs_ID(), getUNS_Production_Detail_ID()) > 0?
							true : false;
					
			if (exists) {
				log.saveError("DuplicatedSpecs", "Cannot insert duplicate Quality Specification.");
				return false;
			}
		}
		
		return true;
	}
	
	public MUNSProductionQuality(MUNSProductionDetail pd)
	{
		this(pd.getCtx(), 0, pd.get_TrxName());
		
		setAD_Org_ID(pd.getAD_Org_ID());
		setUNS_Production_ID(pd.getUNS_Production_ID());
		setUNS_Production_Detail_ID(pd.get_ID());
		setDateTested(pd.getUNS_Production().getProductionDate());
		setM_Product_ID(pd.getM_Product_ID());
	}
	
	/**
	 * 
	 * @param pd
	 * @return {@link MUNSProductionQuality}[]
	 */
	public static MUNSProductionQuality[] getCreates (MUNSProductionDetail pd)
	{
		int[] UNS_Quality_Specs_IDs = PO.getAllIDs(
				MUNSQualitySpecs.Table_Name, "M_Product_ID = " 
						+ pd.getM_Product_ID(), pd.get_TrxName());
		
		List<MUNSProductionQuality> list = 
				new ArrayList<MUNSProductionQuality>();
		
		for (int i=0; i<UNS_Quality_Specs_IDs.length; i++)
		{
			MUNSProductionQuality q = getCreate(pd, UNS_Quality_Specs_IDs[i]);
			list.add(q);
		}
		
		MUNSProductionQuality[] qualities = 
				new MUNSProductionQuality[list.size()];
		list.toArray(qualities);
		
		return qualities;
	}
	
	public static MUNSProductionQuality getCreate (
			MUNSProductionDetail pd, int UNS_Quality_Specs_ID)
	{
		MUNSProductionQuality quality = get(pd, UNS_Quality_Specs_ID);
		
		if (null != quality)
		{
			return quality;
		}
		
		quality = new MUNSProductionQuality(pd);
		quality.setUNS_Quality_Specs_ID(UNS_Quality_Specs_ID);
		quality.saveEx();
		
		return quality;
	}
	
	public static MUNSProductionQuality get(
			MUNSProductionDetail pd, int UNS_Quality_Specs_ID)
	{
		String whereClause = COLUMNNAME_UNS_Production_Detail_ID + " = ?"
				+ " AND IsActive = ? AND " + COLUMNNAME_UNS_Quality_Specs_ID
				+ " = ? ";
		MUNSProductionQuality quality = new Query(
				pd.getCtx(), Table_Name, whereClause, 
				pd.get_TrxName()).setParameters(
						pd.get_ID(), "Y", UNS_Quality_Specs_ID).
				first();
		
		return quality;
	}
	
	public static MUNSProductionQuality[] get (MUNSProduction production)
	{
		String whereClause = COLUMNNAME_UNS_Production_ID + " = ?"
				+ " AND IsActive = ? ";
		List<MUNSProductionQuality> list = new Query(
				production.getCtx(), Table_Name, whereClause, 
				production.get_TrxName()).setParameters(
						production.get_ID(), "Y").
				list();
		MUNSProductionQuality[] q = new MUNSProductionQuality[list.size()];
		list.toArray(q);
		
		return q;
	}
	
	@Deprecated
	public static String getCreate(MUNSProductionDetail pd)
	{
		String retVal = "Please complete production quality - ";
		MUNSProductionQuality pq = new Query(pd.getCtx(), Table_Name, COLUMNNAME_UNS_Production_Detail_ID+"=?",
									pd.get_TrxName()).setParameters(pd.get_ID()).first();
		if(pq != null)
		{
			if(!pq.isProcessed())
				return retVal += pq.getDocumentNo();
			else
				return null;
		}
		
		pq = new MUNSProductionQuality(pd);
		pq.saveEx();
		
		return retVal += pq.getDocumentNo();
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder (getClass().getName())
		.append("[").append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=")
			.append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), getDocumentNo());
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF	
	
	protected boolean afterSave (boolean newRecord, boolean success)
	{	
		
		return true;
	}
	protected boolean beforeDelete()
	{
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessed(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
//		if (DOCACTION_Prepare.equals(getDocAction()))
//		{
//			setProcessed(false);
//			return DocAction.STATUS_InProgress;
//		}
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		MUNSProduction production = new MUNSProduction(
				getCtx(), getUNS_Production_ID(), get_TrxName());
		try
		{
			production.setQAEvent_ID(get_ID());
			boolean ok = production.processIt(DOCACTION_Complete);
			if (!ok)
			{
				m_processMsg = production.getProcessMsg();
				return DOCSTATUS_Invalid;
			}
		}
		catch (Exception ex)
		{
			m_processMsg = ex.getMessage();
			return DOCSTATUS_Invalid;
		}
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}
		
		if (!isApproved())
		{
			approveIt();
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Quantities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	reActivateIt

	@Override
	public String getSummary()
	{		
		StringBuilder sb = new StringBuilder();
		sb.append("#Document No :: " + getDocumentNo());
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0) 
			sb.append(" - ").append(getDescription());
		return sb.toString();
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		setProcessed(false);
		setIsApproved(false);;
		return true;
	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing,
				String orderType, String isSOTrx, int AD_Table_ID,
					String[] docAction, String[] options, int index)
	{
//		if (docStatus.equals(DocAction.STATUS_Drafted))
//		{
//			options[index++] = DocAction.ACTION_Prepare;
//		}
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_ReActivate;
		}	
		return index;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

}