/**
 * 
 */
package com.unta.userwarning;

import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.panel.ADForm;
import org.compiere.util.Env;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Timer;

import com.uns.util.UNSUserWarningContentLoader;
import com.uns.util.UNSUserWarningLoader;

/**
 * @author nurse endang.nurseha@untasoft.com
 * @see www.untasoft.com
 */
public class UNSUserWarningForm extends ADForm implements EventListener<Event>
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4155824809176404695L;
	private Label m_lTitle;
	private Label m_lDescription;
	private Button m_bFirst;
	private Button m_bPrev;
	private Button m_bNext;
	private Button m_bLast;
	private ListModelTable m_model;
	private WListbox m_table;
	private UNSUserWarningContentLoader[] m_loaders;
	private int m_activePageIdx = -1;
	private int m_maxPage = -1;
	private Properties m_ctx;
	private String m_trxName;
	private int m_AD_Client_ID;
	private int m_AD_Role_ID;
	private Label m_lPage;
	
	/**
	 * 
	 */
	public UNSUserWarningForm() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.adempiere.webui.panel.ADForm#initForm()
	 */
	@Override
	protected void initForm() 
	{
		this.m_ctx = Env.getCtx();
		this.m_trxName = null; //create trxName when you need
		this.m_AD_Client_ID = Env.getAD_Client_ID(m_ctx);
		this.m_AD_Role_ID = Env.getAD_Role_ID(m_ctx);
		UNSUserWarningLoader loader = new UNSUserWarningLoader(this.m_ctx, 
				this.m_AD_Client_ID, this.m_AD_Role_ID, this.m_trxName);
		this.m_loaders = loader.load(true);
		this.m_maxPage = m_loaders.length;
		if (this.m_maxPage == 0)
		{
			return;
		}
		
		this.m_lTitle = new Label("Undefined");
		this.m_lDescription = new Label("");
		this.m_lPage = new Label("Page 0 Of 0");
		this.m_bFirst = new Button("First[<<]");
		this.m_bFirst.setName("First");
		this.m_bPrev = new Button("Prev[<]");
		this.m_bPrev.setName("Prev");
		this.m_bNext = new Button("Next[>]");
		this.m_bNext.setName("Next");
		this.m_bLast = new Button("Last[>>]");
		this.m_bLast.setName("Last");
		this.m_table = new WListbox();
		this.m_bPrev.addEventListener(Events.ON_CLICK, this);
		this.m_bFirst.addEventListener(Events.ON_CLICK, this);
		this.m_bNext.addEventListener(Events.ON_CLICK, this);
		this.m_bLast.addEventListener(Events.ON_CLICK, this);
		this.m_bFirst.setWidth("100%");
		this.m_bPrev.setWidth("100%");
		this.m_bNext.setWidth("100%");
		this.m_bLast.setWidth("100%");
		this.m_lTitle.setStyle("font-size:24px");
		this.m_lDescription.setStyle("font-size:14px");
		
		//Create Layout
		Borderlayout layout = new Borderlayout();
		layout.setWidth("100%");
		layout.setHeight("100%");
		layout.setStyle("background-color: transparent; position: absolute;");
		
		//header panel
		North headerPanel = new North();
		headerPanel.setStyle("background-color: transparent");
		headerPanel.setSplittable(false);
		headerPanel.setHeight("100px");
		layout.appendChild(headerPanel);
		
		//content panel
		Center contentPanel = new Center();
		contentPanel.setStyle("background-color: transparent");
		layout.appendChild(contentPanel);
		
		//button panel
		South buttonPanle = new South();
		buttonPanle.setStyle("background-color: transparent");
		buttonPanle.setSplittable(false);
		buttonPanle.setHeight("45px");
		layout.appendChild(buttonPanle);
		
		//content for north
		Grid grid = new Grid();
		grid.setWidth("100%");
        grid.setHeight("100%");
        grid.setStyle("margin:0; padding:0; position: absolute; "
        		+ " align: center; valign: center;");
        grid.makeNoStrip();
        grid.setOddRowSclass("even");
        
        Rows rows = new Rows();
        grid.appendChild(rows);
        
        //Add title into grid container
        Row row = new Row();
        rows.appendChild(row);
        Div div = new Div();
        div.setStyle("text-align:center;");
        row.appendChild(div);
        div.appendChild(this.m_lTitle);
        this.m_lTitle.setWidth("100%");
        // end title
        
        //Add description into grid container
        row = new Row();
        rows.appendChild(row);
        div = new Div();
        div.setStyle("text-align:center");
        row.appendChild(div);
        div.appendChild(this.m_lDescription);
        this.m_lDescription.setWidth("100%");
        //end description
        
        headerPanel.appendChild(grid);
        contentPanel.appendChild(m_table);
        m_table.setHflex("1");
        m_table.setVflex("1");
        
        //button
        grid = new Grid();
		grid.setWidth("100%");
        grid.setHeight("100%");
        grid.setStyle("margin:0; padding:0; position: absolute; "
        		+ " align: center; valign: center;");
        grid.makeNoStrip();
        grid.setOddRowSclass("even");
        
        rows = new Rows();
        grid.appendChild(rows);
        
        //Add button into grid container
        row = new Row();
        rows.appendChild(row);
        
        div = new Div();
        div.setStyle("text-align:center;");
        row.appendCellChild(div);
        div.appendChild(this.m_bFirst);

        div = new Div();
        div.setStyle("text-align:center;");
        row.appendCellChild(div);
        div.appendChild(this.m_bPrev);

        div = new Div();
        div.setStyle("text-align:center;");
        row.appendCellChild(div);
        div.appendChild(this.m_bNext);

        div = new Div();
        div.setStyle("text-align:center;");
        row.appendCellChild(div);
        div.appendChild(this.m_bLast);
        
        buttonPanle.appendChild(grid);
        
		first();
		this.appendChild(layout);
		this.setStyle("height: 100%; width: 100%; position: absolute;");
		Timer timer = new Timer(60000);
		timer.setRepeats(true);
		timer.setDelay(60000);
		timer.addEventListener(Events.ON_TIMER, new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				updateData();
			}
		});
		appendChild(timer);
		timer.start();
	}

	private void next ()
	{
		if (this.m_activePageIdx >= this.m_maxPage-1)
			return;
		this.m_activePageIdx++;
		load();
	}
	
	private void Last ()
	{
		this.m_activePageIdx = this.m_maxPage-1;
		load();
	}
	
	private void first ()
	{
		this.m_activePageIdx = 0;
		load();
	}
	
	private void prev ()
	{
		if (this.m_activePageIdx <= 0)
			return;
		
		this.m_activePageIdx--;
		load();
	}
	
	private boolean isFirst ()
	{
		return this.m_activePageIdx == 0;
	}
	
	private boolean isLast()
	{
		return this.m_activePageIdx == this.m_maxPage-1;
	}
	
	private void load ()
	{
		this.m_table.clear();
		this.m_model = new ListModelTable(this.m_loaders[this.m_activePageIdx].
				getContents(false));
//		this.m_model.setNoColumns(this.m_loaders[this.m_activePageIdx].getContents(false).size());
		WListItemRenderer renderer = new WListItemRenderer(
				this.m_loaders[this.m_activePageIdx].getHeaders(false));
		this.m_table.setModel(m_model);
		this.m_table.setItemRenderer(renderer);
		this.m_table.setSizedByContent(true);
		this.m_table.repaint();
		updateInfo();
	}
	
	private void updateData ()
	{
		this.m_table.clear();
		this.m_model = new ListModelTable(this.m_loaders[this.m_activePageIdx].
				getContents(true));
//		this.m_model.setNoColumns(this.m_loaders[this.m_activePageIdx].getContents(false).get(0).size());
		WListItemRenderer renderer = new WListItemRenderer(
				this.m_loaders[this.m_activePageIdx].getHeaders(false));
		this.m_table.setModel(m_model);
		this.m_table.setItemRenderer(renderer);
		this.m_table.setSizedByContent(true);
		this.m_table.repaint();
		updateInfo();
	}
	
	private void updateInfo ()
	{
		this.m_lTitle.setText(this.m_loaders[m_activePageIdx].getTitle());
		this.m_lDescription.setText(this.m_loaders[m_activePageIdx].getDescription());
		enableButton();
		updatePage();
	}
	
	private void enableButton ()
	{
		this.m_bFirst.setEnabled(true);
		this.m_bLast.setEnabled(true);
		this.m_bNext.setEnabled(true);
		this.m_bPrev.setEnabled(true);
		
		if (isLast())
		{
			this.m_bNext.setEnabled(false);
			this.m_bLast.setEnabled(false);
		}
		else if (isFirst())
		{
			this.m_bFirst.setEnabled(false);
			this.m_bPrev.setEnabled(false);
		}
	}
	
	@Override
	public void dispose ()
	{
		super.dispose();
	}
	
	@Override
	public void onEvent (Event e)
	{
		String eName = e.getName();
		Component comp = e.getTarget();
		if (Events.ON_CLICK.equals(eName))
		{
			if (this.m_bFirst.equals(comp))
				first();
			else if (this.m_bPrev.equals(comp))
				prev();
			else if (this.m_bNext.equals(comp))
				next();
			else if (this.m_bLast.equals(comp))
				Last();
			else
				logger.log(Level.WARNING, "Unknown Component " + comp.toString());
		}
		else
			logger.log(Level.WARNING, "Unhandled event " + eName);
	}
	
	private void updatePage ()
	{
		String pageText = "Page " + Integer.toString(this.m_activePageIdx+1)
				+ " Of " + Integer.toString(this.m_maxPage+1);
		this.m_lPage.setText(pageText);
	}
}
