/**
 * 
 */
package com.uns.pos;

import java.util.Vector;

import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.zkoss.zul.Div;

import com.uns.tablemodel.UNSSalesWatchTableModel;
import com.uns.tablemodel.UNSValuePair;

/**
 * @author nurse
 *
 */
public class UNSSalesWatchTable 
{
	private UNSSalesWatchTableModel m_model;
	private Grid m_grid;
	private final String m_oddColor = "background-color:rgb(233,237,244)";
	private final String m_evenColor = "background-color:rgb(208,216,232)";
	
	/**
	 * 
	 */
	public UNSSalesWatchTable(Grid grid) 
	{
		m_grid = grid;
		m_model = new UNSSalesWatchTableModel();
	}

	public void updateInfo (int orgID)
	{
		m_grid.getChildren().clear();
		m_model.updateRecord(orgID);
		String[] headers = m_model.getHeaders();
		Rows rows = new Rows();
		Row row = new Row();
		String hStyle = "text-align:center;background-color: rgb(79,129,189); padding : 5px 0px 5px 0";
		for (int i=0; i<headers.length; i++)
		{
			Div div = new Div();
			div.setStyle(hStyle);
			Label h = new Label(headers[i]);
			h.setStyle("font-size:12px;color:rgb(255,255,255);");
			div.appendChild(h);
			row.appendCellChild(div);
			rows.appendChild(row);
			m_grid.appendChild(rows);
		}
		
		Vector<Vector<UNSValuePair<Object, Object>>> mrows = m_model.getRows();
		String oddStyle = "padding:0px 5px 0px 5px;" + m_oddColor;
		String evenStyle = "padding:0px 5px 0px 5px; " + m_evenColor;
		
		for (int i=0; i<mrows.size(); i++)
		{
			Vector<UNSValuePair<Object, Object>> mrow = mrows.get(i);
			row = new Row();
			for (int j=0; j<mrow.size(); j++)
			{
				UNSValuePair<Object, Object> col = mrow.get(j);
				Div div = new Div();
				if (i%2 == 0)
				{
					String style = evenStyle;
					if (j==0)
						style += ";text-align:left;";
					else
						style += ";text-align:right;";
					div.setStyle(style);
				}
				else
				{
					String style = oddStyle;
					if (j==0)
						style += ";text-align:left;";
					else
						style += ";text-align:right;";
					div.setStyle(style);
				}
				Label content = new Label(col.toString());
				content.setStyle("font-size:12px");
				content.setMultiline(true);
				div.appendChild(content);
				row.appendCellChild(div);
			}
			
			rows.appendChild(row);
		}
		
		UNSValuePair<Object, Object>[] totals = m_model.getTotals();
		row = new Row();
		
		String color = m_oddColor;
		if (mrows.size() %2 == 0)
			color = m_evenColor;
		color += ";padding : 0px 5px 0px 5px";
		for (int i=0; i<totals.length; i++)
		{
			UNSValuePair<Object, Object> col = totals[i];
			Div div = new Div();
			String style = color;
			if (i==0)
				style += ";text-align:left;";
			else
				style += ";text-align:right;";
			div.setStyle(style);
			Label content = new Label(col.toString());
			content.setStyle("font-size:12px");
			content.setMultiline(true);
			div.appendChild(content);
			row.appendCellChild(div);
		}
		
		rows.appendChild(row);
	}
}
