/**
 * 
 */
package com.uns.pos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpSession;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.panel.ADForm;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.North;
import org.zkoss.zul.Timer;

/**
 * @author nurse
 *
 */
public class UNSSalesWatchFormPanel extends ADForm 
{
	private Label l_title;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5264131577398429218L;
	private int m_originalTimeout = -2;
	private int m_orgID = 0;
	private Listbox f_org;
	private UNSSalesWatchTable m_table;
	private boolean m_onManualUpdate = false;

	/**
	 * 
	 */
	public UNSSalesWatchFormPanel() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.adempiere.webui.panel.ADForm#initForm()
	 */
	@Override
	protected void initForm() 
	{
		l_title = new Label();
		l_title.setStyle("font-size:14px; padding: 0px 10px 0px 0px;");		
		
		f_org = ListboxFactory.newDropdownListbox();
		f_org.setStyle("font-size:14px;margin: 0px 5px 5px 5px;");
		initOrgs();
		f_org.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				m_orgID = f_org.getSelectedItem().toKeyNamePair().getKey();
				m_onManualUpdate = true;
				updateInfo();
				m_onManualUpdate = false;
			}
			
		});
		Borderlayout layout = new Borderlayout();
		layout.setWidth("99%");
		layout.setStyle("background-color: transparent; position: absolute;");
		
		//Title Panel
		North headerPanel = new North();
		headerPanel.setStyle("background-color: transparent");
		headerPanel.setSplittable(false);
		headerPanel.setHeight("30px");
		
		//Title Grid
		Grid grid = new Grid();
		grid.setWidth("100%");
        grid.setHeight("100%");
        grid.setStyle("margin:0; padding:0; position: absolute; "
        		+ " align: center; valign: center;");
        grid.makeNoStrip();
        grid.setOddRowSclass("even");
        
        //hcontent definition
        Rows rows = new Rows();
        Row row = new Row();
        Div div = new Div();
        div.setStyle("text-align:center;");
        div.appendChild(l_title);
        div.appendChild(f_org);
        
        //add to container
        row.appendChild(div);
        rows.appendChild(row);
        grid.appendChild(rows);
        headerPanel.appendChild(grid);
		layout.appendChild(headerPanel);
		
		Center center = new Center();
		center.setStyle("background-color: transparent");
		grid = new Grid();
		grid.setWidth("100%");
        grid.setHeight("100%");
        grid.setStyle("margin:0; padding:0; position: absolute; "
        		+ " align: center; valign: center;overflow:auto; border:none");
        grid.makeNoStrip();
        grid.setOddRowSclass("even");
        center.appendChild(grid);
        m_table = new UNSSalesWatchTable(grid);
        m_table.updateInfo(m_orgID);
		layout.appendChild(center);
		appendChild(layout);
		initTitle();
		Timer timer = new Timer(60000);
		timer.setRepeats(true);
		timer.setDelay(60000);
		timer.addEventListener(Events.ON_TIMER, new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception 
			{
				if (m_onManualUpdate)
					return;
				updateInfo();
			}
		});
		
		appendChild(timer);
		timer.start();
		
		Session currSess = Executions.getCurrent().getDesktop().getSession();
		HttpSession sess = (HttpSession)currSess.getNativeSession();
		if (sess.getMaxInactiveInterval() > 0)
		{
			m_originalTimeout = sess.getMaxInactiveInterval();			
			sess.setMaxInactiveInterval(-1);
		}
	}
	
	private void updateInfo ()
	{
		initTitle();
		m_table.updateInfo(m_orgID);
	}
	
	private void initTitle ()
	{
		StringBuilder sb = new StringBuilder("Sales Watch (Net) ");
		Timestamp current = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm");
		String df = format.format(current);
		sb.append(df);
		String title = sb.toString();
		l_title.setText(title);
	}
	
	@Override
	public void onPageDetached (Page page)
	{
		if (m_originalTimeout <= 0)
			m_originalTimeout = MSysConfig.getIntValue(
					MSysConfig.ZK_SESSION_TIMEOUT_IN_SECONDS, 7200, 
					Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
		if (m_originalTimeout > 0)
		{
			Session currSess = Executions.getCurrent().getDesktop().getSession();
			HttpSession sess = (HttpSession) currSess.getNativeSession();
			sess.setMaxInactiveInterval(m_originalTimeout);
		}
		super.onPageDetached(page);
	}
	
	private void initOrgs ()
	{
		String sql = "SELECT AD_Org_ID, Name FROM AD_Org WHERE IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, null);
			st.setString(1, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				f_org.addItem(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	}
}
