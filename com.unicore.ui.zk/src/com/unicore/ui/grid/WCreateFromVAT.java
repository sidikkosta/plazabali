/**
 * 
 */
package com.unicore.ui.grid;

import static org.compiere.model.SystemIDs.COLUMN_C_INVOICE_C_BPARTNER_ID;
//import static org.compiere.model.SystemIDs.COLUMN_C_BPartner_BP_Group_ID;
import static org.compiere.model.SystemIDs.COLUMN_C_BANKSTATEMENT_C_BANKACCOUNT_ID;
import static org.compiere.model.SystemIDs.COLUMN_M_Product_ID;
import static org.compiere.model.SystemIDs.COLUMN_C_PERIOD_AD_ORG_ID;

import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.model.GridTab;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vlayout;

/**
 * @author Burhani Adam
 *
 */
public class WCreateFromVAT extends CreateFromVAT implements EventListener<Event>, ValueChangeListener{

	private WCreateFromWindow window;
	
	public WCreateFromVAT(GridTab gridTab) {
		super(gridTab);
		log.info(getGridTab().toString());
		
		window = new WCreateFromWindow(this, getGridTab().getWindowNo());
		
		p_WindowNo = getGridTab().getWindowNo();

		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);
	}
	
	/** Window No               */
	private int p_WindowNo;

	/**	Logger			*/
	private CLogger log = CLogger.getCLogger(getClass());
		
	protected Button button = new Button();
	private Label orgLabel = new Label();
	private WEditor orgField;
	private Label bPartnerLabel = new Label();
	private WEditor bPartnerField;
	private Label accountLabel = new Label();
	private WEditor accountField;
	private Label productLabel = new Label();
	private WEditor productField;
	private Checkbox isReturn = new Checkbox();
	private Checkbox isPKP = new Checkbox();
	private Checkbox isCreateReplacement = new Checkbox();
	private boolean isReplacement = false;
	
	protected void zkInit() throws Exception
	{
		orgLabel.setText(Msg.getElement(Env.getCtx(), "AD_Org_ID", false));
        bPartnerLabel.setText(Msg.getElement(Env.getCtx(), "C_BPartner_ID", false));
        accountLabel.setText(Msg.getElement(Env.getCtx(), "C_BankAccount_ID", false));
        productLabel.setText(Msg.getElement(Env.getCtx(), "M_Product_ID", false));
        isPKP.setText(Msg.getElement(Env.getCtx(),"IsPKP", false));
        isReturn.setText("Credit Memo");
        isCreateReplacement.setText("Replacement");
        button.setLabel("Refresh");
    
		Vlayout vlayout = new Vlayout();
		vlayout.setVflex("1");
		vlayout.setWidth("100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		Grid parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
		
		Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
	
		row.appendChild(bPartnerLabel.rightAlign());
		row.appendChild(bPartnerField.getComponent());
		row.appendChild(orgLabel.rightAlign());
		row.appendChild(orgField.getComponent());
		row = rows.newRow();
		row.appendChild(productLabel.rightAlign());
		row.appendChild(productField.getComponent());
		row.appendChild(accountLabel.rightAlign());
		row.appendChild(accountField.getComponent());
		row = rows.newRow();
		row.appendCellChild(isPKP);
		row.appendCellChild(isReturn);
		row.appendCellChild(isCreateReplacement);
		row.appendCellChild(button);
	}
	
	private boolean 	m_actionActive = false;
	
	/**
	 *  Action Listener
	 *  @param e event
	 * @throws Exception 
	 */
	public void onEvent(Event e) throws Exception
	{
		if (m_actionActive)
			return;
		m_actionActive = true;
		
		if(e.getTarget().equals(button))
		{
			isReplacement = isCreateReplacement.isSelected();
			loadInvoices(Env.getContextAsInt(Env.getCtx(), p_WindowNo, "UNS_VAT_ID"),
					bPartnerField.getValue() == null ? 0 : (Integer) bPartnerField.getValue(),
							orgField.getValue() == null ? 0 : (Integer) orgField.getValue(),
								accountField.getValue() == null ? 0 : (Integer) accountField.getValue(),
									isReturn.isSelected(),
										isPKP.isSelected(),
											isReplacement,
												productField.getValue() == null ? 0 : (Integer) productField.getValue());
		}
		
		m_actionActive = false;
	}
		
	/**
	 *  Change Listener
	 *  @param e event
	 */
	public void valueChange (ValueChangeEvent e)
	{
		if (log.isLoggable(Level.CONFIG)) log.config(e.getPropertyName() + "=" + e.getNewValue());

		window.tableChanged(null);
	}   //  vetoableChange
		
	/**
	 *  Load Data - Order
	 *  @param C_Order_ID Order
	 *  @param forInvoice true if for invoice vs. delivery qty
	 *  @param M_Locator_ID
	 */
	protected void loadInvoices(int UNS_VAT_ID, int C_BPartner_ID,
			int C_BPartner_Group_ID, int C_BankAccount_ID, boolean isReturn, boolean isPKP, boolean isReplacement,
			int M_Product_ID)
	{
		loadTableOIS(getInvoiceData(UNS_VAT_ID, C_BPartner_ID, C_BPartner_Group_ID,
				C_BankAccount_ID, isReturn, isPKP, isReplacement, M_Product_ID));
				
	}   //  LoadOrder
		
	/**
	 *  Load Order/Invoice/Shipment data into Table
	 *  @param data data
	 */
	protected void loadTableOIS (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getOISColumnNames(isReplacement));
		//
		
		configureMiniTable(window.getWListbox(), isReplacement);
	}   //  loadOrder
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}

	@Override
	public Object getWindow() {
		return window;
	}

	public boolean dynInit() throws Exception
	{
		button.addActionListener(this);
		log.config("");
		window.setTitle(getTitle());
		initPartner();
//		initPartnerGroup();
		initOrg();
		initAcct();
		initProduct();
		return true;
	}   //  dynInit
	
	protected void initPartner () throws Exception
	{
		//  load BPartner
		int AD_Column_ID = COLUMN_C_INVOICE_C_BPARTNER_ID;        //  C_Invoice.C_BPartner_ID
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.Search);
		bPartnerField = new WSearchEditor("C_BPartner_ID", true, false, true, lookup);
	}
	
//	protected void initPartnerGroup () throws Exception
//	{
//		//  load BPartner
//		int AD_Column_ID = COLUMN_C_BPartner_BP_Group_ID;        //  C_BPartner.C_BP_Group_ID
//		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
//		bpGroupField = new WTableDirEditor("C_BPartner_Group_ID", true, false, true, lookup);
//	}
	
	protected void initOrg () throws Exception
	{
		//  load BPartner
		int AD_Column_ID = COLUMN_C_PERIOD_AD_ORG_ID;        //  C_BankStatement.C_BankAccount
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		orgField= new WTableDirEditor("AD_Org_ID", true, false, true, lookup);
	}
	
	protected void initAcct () throws Exception
	{
		//  load BPartner
		int AD_Column_ID = COLUMN_C_BANKSTATEMENT_C_BANKACCOUNT_ID;        //  C_BankStatement.C_BankAccount
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		accountField = new WTableDirEditor("C_BankAccount_ID", true, false, true, lookup);
	}
	
	protected void initProduct () throws Exception
	{
		//  load BPartner
		int AD_Column_ID = COLUMN_M_Product_ID;        //  C_BankStatement.C_BankAccount
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.Search);
		productField= new WSearchEditor("M_Product_ID", true, false, true, lookup);
	}
}