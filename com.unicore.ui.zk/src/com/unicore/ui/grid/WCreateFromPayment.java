/**
 * 
 */
package com.unicore.ui.grid;

import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.grid.CreateFrom;
import org.compiere.model.GridTab;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vlayout;

/**
 * @author Burhani Adam
 *
 */
public class WCreateFromPayment extends CreateFromPayment implements EventListener<Event>, ValueChangeListener {

	private WCreateFromWindow window;
	
	public WCreateFromPayment(GridTab gridTab) {
		super(gridTab);
		
		log.info(getGridTab().toString());
		
		window = new UNSCreateFromWindow(this, getGridTab().getWindowNo());
		
		p_WindowNo = getGridTab().getWindowNo();

		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);

		// TODO Auto-generated constructor stub
	}
	
	/** Window No               */
	private int p_WindowNo;

	/**	Logger			*/
	private CLogger log = CLogger.getCLogger(getClass());
		
	protected Button button = new Button();
	protected Label paySelectionLabel = new Label();
	protected Listbox paySelectionField = ListboxFactory.newDropdownListbox();
	protected Checkbox onlyDueDateCb = new Checkbox();

	protected void zkInit() throws Exception
	{
		paySelectionLabel.setText(Msg.getElement(Env.getCtx(), "C_PaySelection_ID", false));
        onlyDueDateCb.setText(Msg.getMsg(Env.getCtx(), "OnlyDue", true));
        onlyDueDateCb.setTooltiptext(Msg.getMsg(Env.getCtx(), "OnlyDue", false));
        button.setTooltiptext("Refresh");
        button.setTooltip("Refresh");
        button.setName("Refresh");
        button.setLabel("Refresh");
        button.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				KeyNamePair userKNPair = paySelectionField.getSelectedItem().toKeyNamePair();
				int C_PaySelection_ID = userKNPair == null ? 0 : (Integer) userKNPair.getKey();
				initPaySelectionDetails(C_PaySelection_ID, onlyDueDateCb.isSelected());			
			}
		});
		Vlayout vlayout = new Vlayout();
		vlayout.setVflex("1");
		vlayout.setWidth("100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		Grid parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
		
		Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
	
		row.appendChild(paySelectionLabel.rightAlign());
		row.appendChild(paySelectionField);
		paySelectionField.setHflex("1");
		
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(onlyDueDateCb);
		row.appendCellChild(button);
	}

private boolean 	m_actionActive = false;
	
	/**
	 *  Action Listener
	 *  @param e event
	 * @throws Exception 
	 */
	public void onEvent(Event e) throws Exception
	{
		if (m_actionActive)
			return;
		m_actionActive = true;
		
		//  Order
		if (e.getTarget().equals(paySelectionField))
		{
			KeyNamePair pp = paySelectionField.getSelectedItem().toKeyNamePair();
			if (pp == null || pp.getKey() == 0)
				;
			else
			{
				int C_PaySelection_ID = pp.getKey();
				//  set Invoice and Shipment to Null
				loadOrder(C_PaySelection_ID, onlyDueDateCb.isSelected());
			}
		}
		//sameWarehouseCb
        else if (e.getTarget().equals(onlyDueDateCb))
        {
        	KeyNamePair userKNPair = paySelectionField.getSelectedItem().toKeyNamePair();
			int C_PaySelection_ID = userKNPair == null ? 0 : (Integer) userKNPair.getKey();
			getPaySelectionData(C_PaySelection_ID, onlyDueDateCb.isSelected());					
        }
		
		m_actionActive = false;
	}
		
	/**
	 *  Change Listener
	 *  @param e event
	 */
	public void valueChange (ValueChangeEvent e)
	{
		if (log.isLoggable(Level.CONFIG)) log.config(e.getPropertyName() + "=" + e.getNewValue());

		//  BPartner - load Order/Invoice/Shipment
		if (e.getPropertyName().equals("C_PaySelection_ID"))
		{			
			KeyNamePair userKNPair = paySelectionField.getSelectedItem().getValue();
			int C_PaySelection_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
			initPaySelectionDetails (C_PaySelection_ID, true);
		}
		window.tableChanged(null);
	}   //  vetoableChange
		
	/**
	 *  Load PBartner dependent Order/Invoice/Shipment Field.
	 *  @param C_BPartner_ID BPartner
	 *  @param forInvoice for invoice
	 */
	protected void initPaySelectionDetails (int C_BPartner_ID, boolean isOnlyDueDate)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID=" + C_BPartner_ID);
		KeyNamePair pp = new KeyNamePair(0,"");
		paySelectionField.removeActionListener(this);
		paySelectionField.removeAllItems();
		paySelectionField.addItem(pp);
		
		ArrayList<KeyNamePair> list = loadPaySelectionData(C_BPartner_ID);
		int C_PaySelection_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "C_PaySelection_ID");
		for(KeyNamePair knp : list)
		{
			paySelectionField.addItem(knp);
			if(knp.getKey() == C_PaySelection_ID)
			{
				paySelectionField.setSelectedKeyNamePair(knp);
			}
		}
		if(C_PaySelection_ID > 0)
		{
			loadTableOIS(getPaySelectionData(C_PaySelection_ID, onlyDueDateCb.isSelected()));
		}
		paySelectionField.addActionListener(this);
	}   //  initBPartnerOIS

	/**
	 *  Load Data - Order
	 *  @param C_Order_ID Order
	 *  @param forInvoice true if for invoice vs. delivery qty
	 *  @param M_Locator_ID
	 */
	protected void loadOrder (int C_PaySelection_ID, boolean isOnlyDueDate)
	{
		loadTableOIS(getPaySelectionData(C_PaySelection_ID, isOnlyDueDate));
	}   //  LoadOrder
		
	/**
	 *  Load Order/Invoice/Shipment data into Table
	 *  @param data data
	 */
	protected void loadTableOIS (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getOISColumnNames());
		//
		
		configureMiniTable(window.getWListbox());
	}   //  loadOrder
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}

	@Override
	public Object getWindow() {
		return window;
	}

	public boolean dynInit() throws Exception
	{
		log.config("");
		super.dynInit();
		window.setTitle(getTitle());
		onlyDueDateCb.setSelected(true);
		onlyDueDateCb.addActionListener(this);
		
		int C_BPartner_ID = ((Integer) getGridTab().getValue("C_BPartner_ID")).intValue();
		
		initPaySelectionDetails(C_BPartner_ID, onlyDueDateCb.isSelected());
		
		return true;
	}   //  dynInit
	
//	/**************************************************************************
//	 *  Load User List Field
//	 *  @throws Exception if Lookups cannot be initialized
//	 */
//	protected void initPaySelection() throws Exception
//	{
//		KeyNamePair pp = new KeyNamePair(0,"");
//		paySelectionField.removeActionListener(this);
//		paySelectionField.removeAllItems();
//		paySelectionField.addItem(pp);
//		
//		ArrayList<KeyNamePair> list = new ArrayList<KeyNamePair>();
//		
//		String sql = "SELECT C_PaySelection_ID, DateDoc FROM C_PaySelection";
//
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		try
//		{
//			pstmt = DB.prepareStatement(sql.toString(), null);
//			rs = pstmt.executeQuery();
//			while (rs.next())
//			{
//				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
//			}
//		}
//		catch (SQLException e)
//		{
//			log.log(Level.SEVERE, sql.toString(), e);
//		}
//		finally
//		{
//			DB.close(rs, pstmt);
//			rs = null; pstmt = null;
//		}
//		
//		for(KeyNamePair knp : list)
//		{
//			paySelectionField.addItem(knp);
////			if(knp.getKey() == M_Requisition_ID) {
////				requisitionField.setSelectedKeyNamePair(knp);
////			}
//		}
//		
//		paySelectionField.addActionListener(this);
//	}   //  initBPartner

}


class UNSCreateFromWindow extends WCreateFromWindow
{

	public UNSCreateFromWindow(CreateFrom createFrom, int windowNo) {
		super(createFrom, windowNo);
	}
	
	private boolean justUpdate = false;
	
	@Override
	public void onColumnChange (ListModelTable table,int row, int col)
	{
		if (justUpdate)
		{
			return;
		}
		
		justUpdate = true;
		//Write your code here.
		justUpdate = false;
	}

	private static final long serialVersionUID = -9095123304223114091L;
	
}
