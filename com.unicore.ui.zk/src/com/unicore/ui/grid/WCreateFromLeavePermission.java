/**
 * 
 */
package com.unicore.ui.grid;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MPeriod;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vlayout;

import com.uns.model.MUNSLeavePermissionGroup;
import com.uns.model.MUNSLeavePermissionTrx;

/**
 * @author Burhani Adam
 *
 */
public class WCreateFromLeavePermission extends CreateFromLeavePermission implements EventListener<Event>, ValueChangeListener {

	private WCreateFromWindow window;
	private int p_WindowNo;
	private int m_lengthDay = 0;
	private String m_infoPeriod = "";
	private Checkbox p_Date1 = new Checkbox();
	private Checkbox p_Date2 = new Checkbox();
	private Checkbox p_Date3 = new Checkbox();
	private Checkbox p_Date4 = new Checkbox();
	private Checkbox p_Date5 = new Checkbox();
	private Checkbox p_Date6 = new Checkbox();
	private Checkbox p_Date7 = new Checkbox();
	private Checkbox p_Date8 = new Checkbox();
	private Checkbox p_Date9 = new Checkbox();
	private Checkbox p_Date10 = new Checkbox();
	private Checkbox p_Date11 = new Checkbox();
	private Checkbox p_Date12 = new Checkbox();
	private Checkbox p_Date13 = new Checkbox();
	private Checkbox p_Date14 = new Checkbox();
	private Checkbox p_Date15 = new Checkbox();
	private Checkbox p_Date16 = new Checkbox();
	private Checkbox p_Date17 = new Checkbox();
	private Checkbox p_Date18 = new Checkbox();
	private Checkbox p_Date19 = new Checkbox();
	private Checkbox p_Date20 = new Checkbox();
	private Checkbox p_Date21 = new Checkbox();
	private Checkbox p_Date22 = new Checkbox();
	private Checkbox p_Date23 = new Checkbox();
	private Checkbox p_Date24 = new Checkbox();
	private Checkbox p_Date25 = new Checkbox();
	private Checkbox p_Date26 = new Checkbox();
	private Checkbox p_Date27 = new Checkbox();
	private Checkbox p_Date28 = new Checkbox();
	private Checkbox p_Date29 = new Checkbox();
	private Checkbox p_Date30 = new Checkbox();
	private Checkbox p_Date31 = new Checkbox();
	
	
	public WCreateFromLeavePermission(GridTab gridTab) {
		super(gridTab);
		log.info(getGridTab().toString());
		
		window = new WCreateFromWindow();
		window.setParamaterOnly(true);
		window.init(this, getGridTab().getWindowNo());
		p_WindowNo = getGridTab().getWindowNo();
		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public boolean dynInit() throws Exception
	{
		log.config("");
		window.setTitle("Date Selection");
		
		return true;
	}
	
	protected void zkInit() throws Exception
	{
		initLengthDay();
		window.setHeight("220px");
		window.setWidth("350px");
		window.getConfirmPanel().getOKButton().setEnabled(true);
		initInfoParams();
		Vlayout vlayout = new Vlayout();
		vlayout.setVflex("1");
		vlayout.setWidth("100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		Grid parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
		
		Rows rows = (Rows) parameterStdLayout.newRows();
		
		Row row = rows.newRow();
		row.appendCellChild(p_Date1);
		row.appendCellChild(p_Date2);
		row.appendCellChild(p_Date3);
		row.appendCellChild(p_Date4);
		row.appendCellChild(p_Date5);
		row.appendCellChild(p_Date6);
		row.appendCellChild(p_Date7);
		
		row = rows.newRow();
		row.appendCellChild(p_Date8);
		row.appendCellChild(p_Date9);
		row.appendCellChild(p_Date10);
		row.appendCellChild(p_Date11);
		row.appendCellChild(p_Date12);
		row.appendCellChild(p_Date13);
		row.appendCellChild(p_Date14);
		
		row = rows.newRow();
		row.appendCellChild(p_Date15);
		row.appendCellChild(p_Date16);
		row.appendCellChild(p_Date17);
		row.appendCellChild(p_Date18);
		row.appendCellChild(p_Date19);
		row.appendCellChild(p_Date20);
		row.appendCellChild(p_Date21);
		
		row = rows.newRow();
		row.appendCellChild(p_Date22);
		row.appendCellChild(p_Date23);
		row.appendCellChild(p_Date24);
		row.appendCellChild(p_Date25);
		row.appendCellChild(p_Date26);
		row.appendCellChild(p_Date27);
		row.appendCellChild(p_Date28);
		
		if(m_lengthDay > 28)
		{
			row = rows.newRow();
			row.appendCellChild(p_Date29);
			if(m_lengthDay > 29)
				row.appendCellChild(p_Date30);
			if(m_lengthDay > 30)
				row.appendCellChild(p_Date31);
		}
	}
	
	public Object getWindow() {
		return window;
	}
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}
	
	private void initLengthDay()
	{
		int C_Period_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "C_Period_ID");
		MPeriod p = MPeriod.get(Env.getCtx(), C_Period_ID);
		m_lengthDay = TimeUtil.getDaysBetween(p.getStartDate(), p.getEndDate());
		m_lengthDay++;
		m_infoPeriod = p.getName();
	}
	
	private void initInfoParams()
	{
		p_Date1.setText("01");
		p_Date1.setTooltip("01-" + m_infoPeriod);
		p_Date2.setText("02");
		p_Date2.setTooltip("02-" + m_infoPeriod);
		p_Date3.setText("03");
		p_Date3.setTooltip("03-" + m_infoPeriod);
		p_Date4.setText("04");
		p_Date4.setTooltip("04-" + m_infoPeriod);
		p_Date5.setText("05");
		p_Date5.setTooltip("05-" + m_infoPeriod);
		p_Date6.setText("06");
		p_Date6.setTooltip("06-" + m_infoPeriod);
		p_Date7.setText("07");
		p_Date7.setTooltip("07-" + m_infoPeriod);
		p_Date8.setText("08");
		p_Date8.setTooltip("08-" + m_infoPeriod);
		p_Date9.setText("09");
		p_Date9.setTooltip("09-" + m_infoPeriod);
		p_Date10.setText("10");
		p_Date10.setTooltip("10-" + m_infoPeriod);
		p_Date11.setText("11");
		p_Date11.setTooltip("11-" + m_infoPeriod);
		p_Date12.setText("12");
		p_Date12.setTooltip("12-" + m_infoPeriod);
		p_Date13.setText("13");
		p_Date13.setTooltip("13-" + m_infoPeriod);
		p_Date14.setText("14");
		p_Date14.setTooltip("14-" + m_infoPeriod);
		p_Date15.setText("15");
		p_Date15.setTooltip("15-" + m_infoPeriod);
		p_Date16.setText("16");
		p_Date16.setTooltip("16-" + m_infoPeriod);
		p_Date17.setText("17");
		p_Date17.setTooltip("17-" + m_infoPeriod);
		p_Date18.setText("18");
		p_Date18.setTooltip("18-" + m_infoPeriod);
		p_Date19.setText("19");
		p_Date19.setTooltip("19-" + m_infoPeriod);
		p_Date20.setText("20");
		p_Date20.setTooltip("20-" + m_infoPeriod);
		p_Date21.setText("21");
		p_Date21.setTooltip("21-" + m_infoPeriod);
		p_Date22.setText("22");
		p_Date22.setTooltip("22-" + m_infoPeriod);
		p_Date23.setText("23");
		p_Date23.setTooltip("23-" + m_infoPeriod);
		p_Date24.setText("24");
		p_Date24.setTooltip("24-" + m_infoPeriod);
		p_Date25.setText("25");
		p_Date25.setTooltip("25-" + m_infoPeriod);
		p_Date26.setText("26");
		p_Date26.setTooltip("26-" + m_infoPeriod);
		p_Date27.setText("27");
		p_Date27.setTooltip("27-" + m_infoPeriod);
		p_Date28.setText("28");
		p_Date28.setTooltip("28-" + m_infoPeriod);
		p_Date29.setText("29");
		p_Date29.setTooltip("29-" + m_infoPeriod);
		p_Date30.setText("30");
		p_Date30.setTooltip("30-" + m_infoPeriod);
		p_Date31.setText("31");
		p_Date31.setTooltip("31-" + m_infoPeriod);
	}
	
	@Override
	public boolean save(IMiniTable miniTable, String trxName)
	{
		initValueOnDate();
		int Record_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "UNS_LeavePermissionGroup_ID");
		MUNSLeavePermissionGroup parent = new MUNSLeavePermissionGroup(Env.getCtx(), Record_ID, trxName);
		MPeriod p = MPeriod.get(Env.getCtx(), parent.getC_Period_ID());
		MUNSLeavePermissionTrx trx = null;
		
		if(on1)
		{
			trx = new MUNSLeavePermissionTrx(parent);
			trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "01 00:00:00"));
			trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "01 00:00:00"));
		}
		if(on2)
		{
			if(on1)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "02 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "02 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "02 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on3)
		{
			if(on2)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "03 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "03 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "03 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on4)
		{
			if(on3)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "04 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "04 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "04 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on5)
		{
			if(on4)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "05 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "05 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "05 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on6)
		{
			if(on5)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "06 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "06 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "06 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on7)
		{
			if(on6)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "07 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "07 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "07 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on8)
		{
			if(on7)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "08 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "08 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "08 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on9)
		{
			if(on8)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "09 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "09 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "09 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on10)
		{
			if(on9)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "10 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "10 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "10 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on11)
		{
			if(on10)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "11 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "11 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "11 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on12)
		{
			if(on11)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "12 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "12 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "12 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on13)
		{
			if(on12)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "13 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "13 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "13 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on14)
		{
			if(on13)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "14 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "14 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "14 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on15)
		{
			if(on14)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "15 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "15 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "15 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on16)
		{
			if(on15)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "16 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "16 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "16 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on17)
		{
			if(on16)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "17 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "17 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "17 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on18)
		{
			if(on17)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "18 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "18 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "18 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on19)
		{
			if(on18)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "19 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "19 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "19 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on20)
		{
			if(on19)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "20 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "20 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "20 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on21)
		{
			if(on20)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "21 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "21 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "21 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on22)
		{
			if(on21)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "22 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "22 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "22 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on23)
		{
			if(on22)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "23 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "23 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "23 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on24)
		{
			if(on23)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "24 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "24 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "24 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on25)
		{
			if(on24)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "25 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "25 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "25 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on26)
		{
			if(on25)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "26 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "26 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "26 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on27)
		{
			if(on26)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "27 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "27 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "27 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on28)
		{
			if(on27)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "28 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "28 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "28 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on29)
		{
			if(on28)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "29 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "29 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "29 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on30)
		{
			if(on29)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "30 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "30 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "30 00:00:00"));
			}
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		if(on31)
		{
			if(on30)
			{
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
					+ "-" + p.getPeriodNo() + "-" + "31 00:00:00"));
			}
			else
			{
				trx = new MUNSLeavePermissionTrx(parent);
				trx.setLeaveDateStart(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "31 00:00:00"));
				trx.setLeaveDateEnd(Timestamp.valueOf("" + p.getC_Year().getFiscalYear()
						+ "-" + p.getPeriodNo() + "-" + "31 00:00:00"));
			}
			trx.saveEx();
		}
		else if(trx != null)
		{
			trx.saveEx();
			trx = null;
		}
		
		return true;
	}
	
	boolean on1 = false;
	boolean on2 = false;
	boolean on3 = false;
	boolean on4 = false;
	boolean on5 = false;
	boolean on6 = false;
	boolean on7 = false;
	boolean on8 = false;
	boolean on9 = false;
	boolean on10 = false;
	boolean on11 = false;
	boolean on12 = false;
	boolean on13 = false;
	boolean on14 = false;
	boolean on15 = false;
	boolean on16 = false;
	boolean on17 = false;
	boolean on18 = false;
	boolean on19 = false;
	boolean on20 = false;
	boolean on21 = false;
	boolean on22 = false;
	boolean on23 = false;
	boolean on24 = false;
	boolean on25 = false;
	boolean on26 = false;
	boolean on27 = false;
	boolean on28 = false;
	boolean on29 = false;
	boolean on30 = false;
	boolean on31 = false;
	
	private void initValueOnDate()
	{
		on1 = p_Date1.isSelected();
		on2 = p_Date2.isSelected();
		on3 = p_Date3.isSelected();
		on4 = p_Date4.isSelected();
		on5 = p_Date5.isSelected();
		on6 = p_Date6.isSelected();
		on7 = p_Date7.isSelected();
		on8 = p_Date8.isSelected();
		on9 = p_Date9.isSelected();
		on10 = p_Date10.isSelected();
		on11 = p_Date11.isSelected();
		on12 = p_Date12.isSelected();
		on13 = p_Date13.isSelected();
		on14 = p_Date14.isSelected();
		on15 = p_Date15.isSelected();
		on16 = p_Date16.isSelected();
		on17 = p_Date17.isSelected();
		on18 = p_Date18.isSelected();
		on19 = p_Date19.isSelected();
		on20 = p_Date20.isSelected();
		on21 = p_Date21.isSelected();
		on22 = p_Date22.isSelected();
		on23 = p_Date23.isSelected();
		on24 = p_Date24.isSelected();
		on25 = p_Date25.isSelected();
		on26 = p_Date26.isSelected();
		on27 = p_Date27.isSelected();
		on28 = p_Date28.isSelected();
		on29 = p_Date29.isSelected();
		on30 = p_Date30.isSelected();
		on31 = p_Date31.isSelected();
	}
}