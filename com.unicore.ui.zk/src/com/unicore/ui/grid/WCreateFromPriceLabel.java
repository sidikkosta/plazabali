/**
 * 
 */
package com.unicore.ui.grid;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vlayout;

import com.uns.model.MUNSPriceLabelPrinterConf;

/**
 * @author Burhani Adam
 *
 */
public class WCreateFromPriceLabel extends CreateFromPriceLabel implements EventListener<Event>, ValueChangeListener
{
	private Checkbox p_isShowAll = new Checkbox();
	boolean isShowAll = false;
	private WCreateFromWindow window;
	private Label Printer = new Label();
	private WEditor printerField;
	private Label shopTypeLabel = new Label();
	private Listbox shopTypeField = ListboxFactory.newDropdownListbox();
	private Label labelTypeLabel = new Label();
	private Listbox labelTypeField = ListboxFactory.newDropdownListbox();
	private Label spaceOnly = new Label();
	/** Window No               */
	private int p_WindowNo;
	
	/**
	 * @param gridTab
	 */
	public WCreateFromPriceLabel(GridTab gridTab) {
		super(gridTab);
		log.info(getGridTab().toString());
		
		window = new WCreateFromWindow(this, getGridTab().getWindowNo());
		
		p_WindowNo = getGridTab().getWindowNo();

		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(Event e) throws Exception
	{
		if(e.getTarget().equals(p_isShowAll))
		{
			int UNS_PriceLabelPrint_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "UNS_PriceLabelPrint_ID");
			isShowAll = p_isShowAll.isSelected();
			loadTableOIS(getLinesRecord(UNS_PriceLabelPrint_ID, isShowAll));
		}
	}
	
	protected void zkInit() throws Exception
	{
		Printer.setText("*Printer");
		labelTypeLabel.setText("*Label Type");
		shopTypeLabel.setText("*Shop Type");
		p_isShowAll.setText("Show All");
		p_isShowAll.setTooltip("If no will ignore record has full fill print.");
		spaceOnly.setText("");
    
		Vlayout vlayout = new Vlayout();
		vlayout.setVflex("1");
		vlayout.setWidth("100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		Grid parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
		
		Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
		
		row.appendCellChild(Printer.rightAlign());
		row.appendCellChild(printerField.getComponent(), 2);
		row.appendCellChild(labelTypeLabel.rightAlign());
		row.appendCellChild(labelTypeField);
		
		row = rows.newRow();
		row.appendCellChild(shopTypeLabel.rightAlign());
		row.appendCellChild(shopTypeField);
		row.appendCellChild(spaceOnly);
		row.appendCellChild(spaceOnly);
		row.appendCellChild(p_isShowAll);
	}
	
	public Object getWindow() {
		return window;
	}
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}
	
	@Override
	public boolean save(IMiniTable miniTable, String trxName)
	{
		ValueNamePair pp = (ValueNamePair) shopTypeField.getSelectedItem().getValue();
		if(pp == null)
			throw new AdempiereException("Mandatory Shop Type.!");
		String shopType = pp.getValue();
		setShopType(shopType);
		pp = (ValueNamePair) labelTypeField.getSelectedItem().getValue();
		if(pp == null)
			throw new AdempiereException("Mandatory Label Type.!");
		String labelType = pp.getValue();
		setLabelType(labelType);
		
		int printerID = printerField.getValue() == null ? 0 : (Integer) printerField.getValue();
		if(printerID == 0)
			throw new AdempiereException("Mandatory Printer.!");
		setPrinterID(printerID);
		return super.save(miniTable, trxName);
	}
	
	protected void loadTableOIS (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getOISColumnNames());
		//
		
		configureMiniTable(window.getWListbox());
	}
	
	public boolean dynInit() throws Exception
	{
		p_isShowAll.addActionListener(this);
		log.config("");
		window.setTitle(getTitle());
		
		initPrinter(); initLabelType(); initShopType();;
		int UNS_PriceLabelPrint_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "UNS_PriceLabelPrint_ID");
		loadTableOIS(getLinesRecord(UNS_PriceLabelPrint_ID, false));
		
		return true;
	}   //  dynInit
	
	protected void initPrinter()
	{
		int AD_Column_ID = MColumn.getColumn_ID(MUNSPriceLabelPrinterConf.Table_Name,
				MUNSPriceLabelPrinterConf.COLUMNNAME_UNS_PriceLabelPrinterConf_ID);
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		printerField = new WTableDirEditor(
				MUNSPriceLabelPrinterConf.COLUMNNAME_UNS_PriceLabelPrinterConf_ID, true, false, true, lookup);
	}
	
	protected void initShopType()
	{
		String sql = "SELECT Value, Name FROM AD_Ref_List WHERE AD_Reference_ID = (SELECT AD_Reference_ID"
				+ " FROM AD_Reference WHERE AD_Reference_UU = ?)";
		List<List<Object>> oo = DB.getSQLArrayObjectsEx(null, sql, "e689ccc6-fa79-4962-b222-ee1b5bab4e0d");
		
		if(oo == null)
			return;
		ArrayList<ValueNamePair> data = new ArrayList<ValueNamePair>();
		ValueNamePair vp = null;
		for(int i=0;i<oo.size();i++)
		{
			vp = new ValueNamePair(oo.get(i).get(0).toString(), oo.get(i).get(1).toString());
			data.add(vp);
		}
		
		for(ValueNamePair pp : data)
			shopTypeField.appendItem(pp.getName(), pp);
		shopTypeField.setSelectedIndex(1);
	}
	
	protected void initLabelType()
	{
		String sql = "SELECT Value, Name FROM AD_Ref_List WHERE AD_Reference_ID = (SELECT AD_Reference_ID"
				+ " FROM AD_Reference WHERE AD_Reference_UU = ?)";
		List<List<Object>> oo = DB.getSQLArrayObjectsEx(null, sql, "34b24aa6-8b30-419e-9050-87ba52c7a404");
		
		if(oo == null)
			return;
		ArrayList<ValueNamePair> data = new ArrayList<ValueNamePair>();
		ValueNamePair vp = null;
		for(int i=0;i<oo.size();i++)
		{
			vp = new ValueNamePair(oo.get(i).get(0).toString(), oo.get(i).get(1).toString());
			data.add(vp);
		}
		
		for(ValueNamePair pp : data)
			labelTypeField.appendItem(pp.getName(), pp);
		labelTypeField.setSelectedIndex(1);
	}
}