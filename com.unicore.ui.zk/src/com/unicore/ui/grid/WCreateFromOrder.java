/**
 * 
 */
package com.unicore.ui.grid;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.model.GridTab;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vlayout;

import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MOrder;

/**
 * @author root
 *
 */
public class WCreateFromOrder extends CreateFromPO implements EventListener<Event>, ValueChangeListener{

	private WCreateFromWindow window;
	/**
	 * @param gridTab
	 */
	public WCreateFromOrder(GridTab gridTab) {
		super(gridTab);
		log.info(getGridTab().toString());
		
		window = new WCreateFromWindow(this, getGridTab().getWindowNo());
		
		p_WindowNo = getGridTab().getWindowNo();

		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);
	}

	/** Window No               */
	private int p_WindowNo;

	/**	Logger			*/
	private CLogger log = CLogger.getCLogger(getClass());
		
	protected Label userLabel = new Label();
	protected Listbox userField = ListboxFactory.newDropdownListbox();;
	protected Label bPartnerLabel = new Label();
	protected WEditor bPartnerField;
	protected Button button = new Button();
	protected Label requisitionLabel = new Label();
	protected Listbox requisitionField = ListboxFactory.newDropdownListbox();
	
	protected Checkbox sameWarehouseCb = new Checkbox();
	
	protected void zkInit() throws Exception
	{
    	bPartnerLabel.setText(Msg.getElement(Env.getCtx(), "C_BPartner_ID"));
		requisitionLabel.setText(Msg.getElement(Env.getCtx(), "M_Requisition_ID", false));
		userLabel.setText(Msg.getElement(Env.getCtx(), "AD_User_ID", false));
        sameWarehouseCb.setText(Msg.getMsg(Env.getCtx(), "FromSameWarehouseOnly", true));
        sameWarehouseCb.setTooltiptext(Msg.getMsg(Env.getCtx(), "FromSameWarehouseOnly", false));
        button.setTooltiptext("Refresh");
        button.setTooltip("Refresh");
        button.setName("Refresh");
        button.setLabel("Refresh");
        button.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				int C_BPartner_ID = bPartnerField.getValue() == null ? 0: (Integer) bPartnerField.getValue();
				KeyNamePair userKNPair = userField.getSelectedItem().toKeyNamePair();
				int AD_User_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
				initBPRequisitionDetails(C_BPartner_ID, AD_User_ID, false);			
			}
		});
        
		Vlayout vlayout = new Vlayout();
		vlayout.setVflex("1");
		vlayout.setWidth("100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		Grid parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
		
		Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
		row.appendChild(bPartnerLabel.rightAlign());
		if (bPartnerField != null) {
			row.appendChild(bPartnerField.getComponent());
			bPartnerField.fillHorizontal();
		}
	
		row.appendChild(userLabel.rightAlign());
		row.appendChild(userField);
		userField.setHflex("1");
		
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(sameWarehouseCb);
		
		row.appendChild(requisitionLabel.rightAlign());
		row.appendChild(requisitionField);
		requisitionField.setHflex("1");
		
		
		row.appendCellChild(button);
	}

	private boolean 	m_actionActive = false;
	
	/**
	 *  Action Listener
	 *  @param e event
	 * @throws Exception 
	 */
	public void onEvent(Event e) throws Exception
	{
		if (m_actionActive)
			return;
		m_actionActive = true;
		
		//  Order
		if (e.getTarget().equals(requisitionField))
		{
			KeyNamePair pp = requisitionField.getSelectedItem().toKeyNamePair();
			if (pp == null || pp.getKey() == 0)
				;
			else
			{
				int M_Requisition_ID = pp.getKey();
				//  set Invoice and Shipment to Null
				loadRequisition(M_Requisition_ID);
			}
		}
		//  Invoice
		else if (e.getTarget().equals(userField) || e.getTarget().equals(sameWarehouseCb))
		{
			KeyNamePair pp = userField.getSelectedItem().toKeyNamePair();
			//if (pp == null || pp.getKey() == 0)
			if (pp == null)
				;
			else
			{
				int C_BPartner_ID = bPartnerField.getValue() == null ? 0: (Integer) bPartnerField.getValue();
				KeyNamePair userKNPair = userField.getSelectedItem().toKeyNamePair();
				int AD_User_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
				initBPRequisitionDetails(C_BPartner_ID, AD_User_ID, false);			
			}
		}
		
		m_actionActive = false;
	}
	
	/**
	 *  Change Listener
	 *  @param e event
	 */
	public void valueChange (ValueChangeEvent e)
	{
		if (log.isLoggable(Level.CONFIG)) log.config(e.getPropertyName() + "=" + e.getNewValue());

		//  BPartner - load Order/Invoice/Shipment
		if (e.getPropertyName().equals("C_BPartner_ID"))
		{
			int C_BPartner_ID = 0; 
			if (e.getNewValue() != null){
				C_BPartner_ID = ((Integer)e.getNewValue()).intValue();
			}
			
			KeyNamePair userKNPair = userField.getSelectedItem().getValue();
			int AD_User_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
			initBPRequisitionDetails(C_BPartner_ID, AD_User_ID, false);			
		}
		window.tableChanged(null);
	}   //  vetoableChange
	
	/**************************************************************************
	 *  Load BPartner Field
	 *  @param forInvoice true if Invoices are to be created, false receipts
	 *  @throws Exception if Lookups cannot be initialized
	 */
	protected void initBPartner (boolean forInvoice) throws Exception
	{
		int Column_C_BPartner_ID = MColumn.getColumn_ID(MOrder.Table_Name, MOrder.COLUMNNAME_C_BPartner_ID);
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, Column_C_BPartner_ID, DisplayType.Search);
		bPartnerField = new WSearchEditor (MInvoice.COLUMNNAME_C_BPartner_ID, true, false, true, lookup);
		
		int C_BPartner_ID = 
				Env.getContextAsInt(Env.getCtx(), p_WindowNo, "C_BPartner_ID");//-1;
//		int C_Order_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, MUNSPackingListOrder.COLUMNNAME_C_Order_ID);
//		if(C_Order_ID > 0)
//		{
//			C_BPartner_ID = DB.getSQLValue(null, "SELECT C_BPartner_ID FROM C_Order WHERE C_Order_ID = ?", C_Order_ID);
//		}
		
		bPartnerField.setValue(new Integer(C_BPartner_ID));

		int AD_User_ID = 0;
		if (userField.getSelectedItem() != null) {
			KeyNamePair userKNPair = userField.getSelectedItem().getValue();
			AD_User_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
		}
		
		initBPRequisitionDetails(C_BPartner_ID, AD_User_ID, false);			
	}   //  initBPartner

	/**************************************************************************
	 *  Load User List Field
	 *  @throws Exception if Lookups cannot be initialized
	 */
	protected void initUser() throws Exception
	{
		KeyNamePair pp = new KeyNamePair(0,"");
		userField.removeActionListener(this);
		userField.removeAllItems();
		userField.addItem(pp);
		
		ArrayList<KeyNamePair> list = new ArrayList<KeyNamePair>();
		
		String sql = "SELECT u.AD_User_ID, u.Name FROM AD_User u "
				+ "WHERE u.IsActive='Y' AND "
				+ "		EXISTS (SELECT * FROM C_BPartner bp "
				+ "				WHERE bp.isActive = 'Y' AND u.C_BPartner_ID=bp.C_BPartner_ID "
				+ "				AND (bp.IsEmployee='Y' OR bp.IsSalesRep='Y')) "
				+ "ORDER BY u.Name";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		for(KeyNamePair knp : list)
		{
			userField.addItem(knp);
//			if(knp.getKey() == M_Requisition_ID) {
//				requisitionField.setSelectedKeyNamePair(knp);
//			}
		}
		
		userField.addActionListener(this);
	}   //  initBPartner

	/**
	 *  Load PBartner dependent Order/Invoice/Shipment Field.
	 *  @param C_BPartner_ID BPartner
	 *  @param forInvoice for invoice
	 */
	protected void initBPRequisitionDetails (int C_BPartner_ID, int AD_User_ID, boolean forInvoice)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID=" + C_BPartner_ID);
		KeyNamePair pp = new KeyNamePair(0,"");
		requisitionField.removeActionListener(this);
		requisitionField.removeAllItems();
		requisitionField.addItem(pp);
		
		ArrayList<KeyNamePair> list = loadRequisitionData(C_BPartner_ID, AD_User_ID, sameWarehouseCb.isSelected());
		int M_Requisition_ID = 0;
		for(KeyNamePair knp : list)
		{
			requisitionField.addItem(knp);
//			if(knp.getKey() == M_Requisition_ID) {
//				requisitionField.setSelectedKeyNamePair(knp);
//			}
		}
		
		if(M_Requisition_ID > 0) {
			loadTableOIS(getRequisitionData(M_Requisition_ID));
		}
		
		requisitionField.addActionListener(this);
//		initBPDetails(C_BPartner_ID);
	}   //  initBPartnerOIS
	
	protected ArrayList<KeyNamePair> loadRequisitionData (
			int C_BPartner_ID, int AD_User_ID, boolean sameWarehouseOnly)
	{
		ArrayList<KeyNamePair> list = new ArrayList<KeyNamePair>();

		//	Display
		StringBuffer display = new StringBuffer(" r.DocumentNo ||' - ' ||")
			.append(DB.TO_CHAR("r.DateRequired", DisplayType.Date, Env.getAD_Language(Env.getCtx())))
			.append("||' - '|| u.Name ");
			//.append(DB.TO_CHAR("u.Name", DisplayType.Amount, Env.getAD_Language(Env.getCtx())));
		//
		StringBuffer sql = new StringBuffer("SELECT r.M_Requisition_ID,").append(display)
			.append(" FROM M_Requisition r "
			+ "INNER JOIN AD_User u ON r.AD_User_ID=u.AD_User_ID "
			+ "WHERE r.DocStatus='CO' AND Processed = 'Y'"
			+ " AND r.M_Requisition_ID IN (SELECT M_Requisition_ID FROM M_RequisitionLine rl WHERE "
			+ "	rl.Qty > (SELECT COALESCE(SUM(Qty),0) FROM UNS_HandleRequests WHERE M_RequisitionLine_ID = rl.M_RequisitionLine_ID"
			+ " AND C_OrderLine_ID NOT IN (SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID NOT IN"
			+ " (SELECT C_Order_ID FROM C_Order WHERE DocStatus IN ('VO', 'RE'))))");

		if (C_BPartner_ID > 0) {
			sql = sql.append(" AND rl.C_BPartner_ID=? ");
		}
		
		sql.append(")");
		
		if(sameWarehouseOnly)
		{
			sql = sql.append(" AND r.M_Warehouse_ID=? ");
		}
		
		if (AD_User_ID > 0) {
			sql = sql.append(" AND r.AD_User_ID=? ");
		}
		
		sql = sql.append("ORDER BY r.DateRequired");
		//
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			int index = 1;
			if(C_BPartner_ID > 0) {
				//only active for material receipts
				pstmt.setInt(index, C_BPartner_ID);
				index++;
			}
			if(sameWarehouseOnly) {
				//only active for material receipts
				pstmt.setInt(index, getM_Warehouse_ID());
				index++;
			}
			if(AD_User_ID > 0) {
				//only active for material receipts
				pstmt.setInt(index, AD_User_ID);
			}
			
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		return list;
	}   //  initBPartnerOIS

//	public void initBPDetails(int C_BPartner_ID) 
//	{
//		initBPInvoiceDetails(C_BPartner_ID);
//	}
//
	/**
	 *  Load Data - Order
	 *  @param M_Requisition_ID Order
	 *  @param forInvoice true if for invoice vs. delivery qty
	 *  @param M_Locator_ID
	 */
	protected void loadRequisition (int M_Requisition_ID)
	{
		loadTableOIS(getRequisitionData(M_Requisition_ID));
	}   //  LoadOrder
	
	/**
	 *  Load Order/Invoice/Shipment data into Table
	 *  @param data data
	 */
	protected void loadTableOIS (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getOISColumnNames());
		//
		
		configureMiniTable(window.getWListbox());
	}   //  loadOrder
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}

	@Override
	public Object getWindow() {
		return window;
	}

	public boolean dynInit() throws Exception
	{
		log.config("");
		super.dynInit();
		window.setTitle(getTitle());
		sameWarehouseCb.setSelected(true);
		sameWarehouseCb.addActionListener(this);

		initBPartner(false);
		initUser();

		return true;
	}   //  dynInit
}
