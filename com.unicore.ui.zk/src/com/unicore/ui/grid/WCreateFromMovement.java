/**
 * 
 */
package com.unicore.ui.grid;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.model.GridTab;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vlayout;

/**
 * @author Burhani Adam
 *
 */
public class WCreateFromMovement extends CreateFromMovement implements EventListener<Event>, ValueChangeListener {

private WCreateFromWindow window;
	
	public WCreateFromMovement(GridTab gridTab) {
		super(gridTab);
		
		log.info(getGridTab().toString());
		
		window = new WCreateFromWindow(this, getGridTab().getWindowNo());
		
		p_WindowNo = getGridTab().getWindowNo();

		try
		{
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);

		// TODO Auto-generated constructor stub
	}
	
	/** Window No               */
	private int p_WindowNo;

	/**	Logger			*/
	private CLogger log = CLogger.getCLogger(getClass());
	
	protected Label userLabel = new Label();
	protected Listbox userField = ListboxFactory.newDropdownListbox();;
	protected Button button = new Button();
	protected Label requisitionLabel = new Label();
	protected Listbox requisitionField = ListboxFactory.newDropdownListbox();

	protected void zkInit() throws Exception
	{
		requisitionLabel.setText(Msg.getElement(Env.getCtx(), "M_Requisition_ID", false));
		userLabel.setText(Msg.getElement(Env.getCtx(), "AD_User_ID", false));
        button.setTooltiptext("Refresh");
        button.setTooltip("Refresh");
        button.setName("Refresh");
        button.setLabel("Refresh");
        button.addActionListener(new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				KeyNamePair reqKNPair = requisitionField.getSelectedItem().toKeyNamePair();
				int M_Requisition_ID = reqKNPair == null ? 0 : (Integer) reqKNPair.getKey();
				KeyNamePair userKNPair = userField.getSelectedItem().toKeyNamePair();
				int AD_User_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
				initPaySelectionDetails(M_Requisition_ID, AD_User_ID);
			}
		});
		Vlayout vlayout = new Vlayout();
		vlayout.setVflex("1");
		vlayout.setWidth("100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		Grid parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
		
		Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
	
		row.appendChild(userLabel.rightAlign());
		row.appendChild(userField);
		userField.setHflex("1");
		
		row.appendChild(requisitionLabel.rightAlign());
		row.appendChild(requisitionField);
		requisitionField.setHflex("1");
		
		row.appendCellChild(button);
	}

	private boolean 	m_actionActive = false;
	
	/**
	 *  Action Listener
	 *  @param e event
	 * @throws Exception 
	 */
	public void onEvent(Event e) throws Exception
	{
		if (m_actionActive)
			return;
		m_actionActive = true;
		
		//  Order
		if (e.getTarget().equals(requisitionField))
		{
        	KeyNamePair pp = userField.getSelectedItem().toKeyNamePair();
        	KeyNamePair reqKNPair = requisitionField.getSelectedItem().toKeyNamePair();
        	int M_Requisition_ID = reqKNPair == null ? 0 : (Integer) reqKNPair.getKey();
			if (pp == null || pp.getKey() == 0)
				loadOrder(M_Requisition_ID, 0);
			else
			{
				int AD_User_ID = (Integer) pp.getKey();
				//  set Invoice and Shipment to Null
				loadOrder(M_Requisition_ID, AD_User_ID);
			}
		}
		//sameWarehouseCb
        else if (e.getTarget().equals(userField))
        {
			KeyNamePair pp = requisitionField.getSelectedItem().toKeyNamePair();
        	KeyNamePair userKNPair = userField.getSelectedItem().toKeyNamePair();
        	int AD_User_ID = userKNPair == null ? 0 : (Integer) userKNPair.getKey();
        	if (pp == null || pp.getKey() == 0)
				loadOrder(0, AD_User_ID);
			else
			{
				int M_Requisition_ID = (Integer) pp.getKey();
				//  set Invoice and Shipment to Null
				loadOrder(M_Requisition_ID, AD_User_ID);
			}				
        }
		
		m_actionActive = false;
	}
		
	/**
	 *  Change Listener
	 *  @param e event
	 */
	public void valueChange (ValueChangeEvent e)
	{
		if (log.isLoggable(Level.CONFIG)) log.config(e.getPropertyName() + "=" + e.getNewValue());

		//  BPartner - load Order/Invoice/Shipment
		if (e.getPropertyName().equals("M_Requisition_ID"))
		{			
			KeyNamePair reqKNPair = requisitionField.getSelectedItem().getValue();
			int C_PaySelection_ID = reqKNPair == null? 0 : (Integer) reqKNPair.getKey();
			KeyNamePair userKNPair = requisitionField.getSelectedItem().toKeyNamePair();
			int AD_User_ID = userKNPair == null ? 0 : (Integer) userKNPair.getKey();
			initPaySelectionDetails (C_PaySelection_ID, AD_User_ID);
		}
		window.tableChanged(null);
	}   //  vetoableChange
		
	/**
	 *  Load PBartner dependent Order/Invoice/Shipment Field.
	 *  @param C_BPartner_ID BPartner
	 *  @param forInvoice for invoice
	 */
	protected void initPaySelectionDetails (int M_Requisition_ID, int AD_User_ID)
	{
		KeyNamePair pp = new KeyNamePair(0,"");
		requisitionField.removeActionListener(this);
		requisitionField.removeAllItems();
		requisitionField.addItem(pp);
		
		ArrayList<KeyNamePair> list = loadRequisitionData(AD_User_ID);
		M_Requisition_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, "M_Requisition_ID");
		for(KeyNamePair knp : list)
		{
			requisitionField.addItem(knp);
			if(knp.getKey() == M_Requisition_ID)
			{
				requisitionField.setSelectedKeyNamePair(knp);
			}
		}
		if(M_Requisition_ID > 0)
		{
			loadTableOIS(getRequisitionLine(M_Requisition_ID, AD_User_ID));
		}
		requisitionField.addActionListener(this);
	}   //  initBPartnerOIS

	/**
	 *  Load Data - Order
	 *  @param C_Order_ID Order
	 *  @param forInvoice true if for invoice vs. delivery qty
	 *  @param M_Locator_ID
	 */
	protected void loadOrder (int C_PaySelection_ID, int AD_User_ID)
	{
		loadTableOIS(getRequisitionLine(C_PaySelection_ID, AD_User_ID));
	}   //  LoadOrder
		
	/**
	 *  Load Order/Invoice/Shipment data into Table
	 *  @param data data
	 */
	protected void loadTableOIS (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getOISColumnNames());
		//
		
		configureMiniTable(window.getWListbox());
	}   //  loadOrder
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}

	@Override
	public Object getWindow() {
		return window;
	}

	public boolean dynInit() throws Exception
	{
		log.config("");
		super.dynInit();
		window.setTitle(getTitle());
		
		initUser();
		int M_Requisition_ID = 0;
		if(requisitionField.getSelectedItem() != null)
		{
			KeyNamePair reqKNPair = requisitionField.getSelectedItem().toKeyNamePair();
			M_Requisition_ID = reqKNPair == null ? 0 : (Integer) reqKNPair.getKey();
		}
		int AD_User_ID = 0;
		if(userField.getSelectedItem() != null)
		{
			KeyNamePair userKNPair = userField.getSelectedItem().toKeyNamePair();
			AD_User_ID = userKNPair == null? 0 : (Integer) userKNPair.getKey();
		}
		initPaySelectionDetails(M_Requisition_ID, AD_User_ID);
		return true;
	}   //  dynInit
	
	protected void initUser() throws Exception
	{
		KeyNamePair pp = new KeyNamePair(0,"");
		userField.removeActionListener(this);
		userField.removeAllItems();
		userField.addItem(pp);
		
		ArrayList<KeyNamePair> list = new ArrayList<KeyNamePair>();
		
		String sql = "SELECT u.AD_User_ID, u.Name FROM AD_User u "
				+ "WHERE u.IsActive='Y' AND "
				+ "		EXISTS (SELECT * FROM C_BPartner bp "
				+ "				WHERE bp.isActive = 'Y' AND u.C_BPartner_ID=bp.C_BPartner_ID "
				+ "				AND (bp.IsEmployee='Y' OR bp.IsSalesRep='Y')) "
				+ "ORDER BY u.Name";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				list.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		for(KeyNamePair knp : list)
		{
			userField.addItem(knp);
		}
		
		userField.addActionListener(this);
	}   //  initBPartner
}
