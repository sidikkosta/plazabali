/**
 * 
 */
package com.uns.importer;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.compiere.model.MRefList;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;

import com.uns.base.model.Query;
import com.uns.model.MUNSResourceWorkerLine;
import com.uns.model.MUNSShopEmployee;
import com.uns.model.MUNSShopRecap;
import com.uns.model.MUNSSubregionRecap;
import com.uns.model.X_UNS_Resource;
import com.uns.model.X_UNS_Resource_WorkerLine;
import com.uns.model.X_UNS_ShopRecap;
import com.uns.model.factory.UNSHRMModelFactory;
import com.uns.model.factory.UNSPPICModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class UNSShopEmployeeLineImporter implements ImporterValidation {

	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, PO> m_PORefMap = null;
	private Calendar m_cPeriod;
	private X_UNS_ShopRecap m_shop;
	private MUNSSubregionRecap m_subregionRecap;
	private ValueNamePair[] m_scheduleTypes = null;
	
	/**
	 * 
	 */
	public UNSShopEmployeeLineImporter(Properties ctx, Sheet sheet, String trxName) {
		super();
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
		m_scheduleTypes = MRefList.getList(m_ctx, 1000309, false);
	}
	
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		MUNSShopEmployee employee = (MUNSShopEmployee) po;
		
		if (employee.getUNS_Employee_ID() <= 0)
			return "Employee not found";
		
		m_subregionRecap = new MUNSSubregionRecap(m_ctx,
				Integer.valueOf(m_ctx.get("UNS_SubregionRecap_ID")
						.toString()), m_trxName);
		
		Timestamp period = (Timestamp) freeColVals.get("Periode");
		m_cPeriod = Calendar.getInstance();
		m_cPeriod.setTimeInMillis(period.getTime());
		m_cPeriod.set(Calendar.HOUR, 0);
		m_cPeriod.set(Calendar.MINUTE, 0);
		m_cPeriod.set(Calendar.SECOND, 0);
		m_cPeriod.set(Calendar.MILLISECOND, 0);
		
		if (period.before(m_subregionRecap.getC_Period().getStartDate())
				|| period.after(m_subregionRecap.getC_Period().getEndDate())) {
			return "Wrong period";
		}

		m_shop = Query
				.get(m_ctx,
						UNSHRMModelFactory.EXTENSION_ID,
						X_UNS_ShopRecap.Table_Name,
						"shop_id = ? AND C_Period_ID=(SELECT C_Period_ID FROM C_Period WHERE ? BETWEEN StartDate AND EndDate)",
						m_trxName).setParameters(freeColVals.get("B"), period).first();
		if (m_shop == null) 
		{
			m_shop = new MUNSShopRecap(m_ctx, 0, m_trxName);
			m_shop.setAD_Org_ID(m_subregionRecap.getAD_Org_ID());
			m_shop.setUNS_SubregionRecap_ID(m_subregionRecap.get_ID());
			m_shop.setC_Period_ID(m_subregionRecap.getC_Period_ID());
			
			if (freeColVals.get("B") == null)
				return "Wrong Section of Departement.";
			
			m_shop.setShop_ID((int) freeColVals.get("B"));
			if (!m_shop.save())
				return CLogger.retrieveErrorString("failed when creating Shop-Recap");
		}
		
		int adUserId = new org.compiere.model.Query(m_ctx, MUser.Table_Name,
				MUser.COLUMNNAME_UNS_Employee_ID + " = "
						+ employee.getUNS_Employee_ID(), m_trxName).firstId();
		if (adUserId <= 0) 
		{
			if (m_subregionRecap.getShopType() != null 
				&& !m_subregionRecap.getShopType().equals(MUNSSubregionRecap.SHOPTYPE_ShopFB))
			{
				return "Employee (row " + currentRow + ") with NIK:" 
						+ employee.getUNS_Employee().getValue() 
						+ " does not have a registered user.";
			}
		}
				
		employee.setAD_Org_ID(m_subregionRecap.getAD_Org_ID());
		employee.setAD_User_ID(adUserId);
		employee.setUNS_ShopRecap_ID(m_shop.get_ID());
		employee.setUNS_SubregionRecap_ID(m_shop.getUNS_SubregionRecap_ID());
		//employee.setPositionType();

		String sql = "SELECT Value FROM C_Job "
				+ " WHERE C_Job_ID=(SELECT C_Job_ID FROM UNS_Employee WHERE UNS_Employee_ID=?)";
		String positionKey = DB.getSQLValueStringEx(m_trxName, sql, employee.getUNS_Employee_ID());
		
		if (positionKey == null)
			positionKey = (String) freeColVals.get("E");
		else {
			if (positionKey.equals(MUNSShopEmployee.POSITIONTYPE_Manager)
					|| positionKey.equals(MUNSShopEmployee.POSITIONTYPE_Staff)
					|| positionKey.equals(MUNSShopEmployee.POSITIONTYPE_JuniorManager)
					|| positionKey.equals(MUNSShopEmployee.POSITIONTYPE_ShopSupervisor)
					|| positionKey.equals(MUNSShopEmployee.POSITIONTYPE_SeniorSupervisor)
					|| positionKey.equals(MUNSShopEmployee.POSITIONTYPE_Support))
				;
			else
				return "Job Position of [" + positionKey + "] is not one of: "
				+ MUNSShopEmployee.POSITIONTYPE_Manager + "=(Senior) Manager, "
				+ MUNSShopEmployee.POSITIONTYPE_JuniorManager + "=Junior Manager, "
				+ MUNSShopEmployee.POSITIONTYPE_SeniorSupervisor + "=Senior Supervisor, "
				+ MUNSShopEmployee.POSITIONTYPE_ShopSupervisor + "=Supervisor, "
				+ MUNSShopEmployee.POSITIONTYPE_Staff + "=Staff";
		}
		
		if (positionKey == null)
			return "Employee (row " + currentRow + ") with NIK:" 
					+ employee.getUNS_Employee().getValue() 
					+ " does not have a Position defined,"
					+ " neither in the employee info nor in the excel file.";
		
		employee.setPositionType(positionKey);
		
		// try to get existing resource worker line that is set from contract 
		// and the date range across the period date-range.
		MUNSResourceWorkerLine rwl = Query.get(m_ctx, UNSPPICModelFactory.getExtensionID(), 
				MUNSResourceWorkerLine.Table_Name, 
				"Labor_ID=? AND (  (ValidFrom < ? AND ? BETWEEN ValidFrom AND ValidTo)"
				+ "				OR (ValidFrom <= ? AND ValidTo >= ?)"
				+ "				OR (ValidFrom BETWEEN ? AND ? AND ValidTo > ?))"
				, m_trxName)
				.setParameters(
						employee.getUNS_Employee_ID(), 
						m_subregionRecap.getStartDate(), m_subregionRecap.getEndDate(),
						m_subregionRecap.getStartDate(), m_subregionRecap.getEndDate(),
						m_subregionRecap.getStartDate(), m_subregionRecap.getEndDate(), m_subregionRecap.getEndDate())
				.first();
		
		if (rwl != null) {
			if (!rwl.getValidFrom().before(m_subregionRecap.getStartDate())) {
				if (!rwl.delete(true))
					return "Cannot remove Resource Worker that is created from contract.";
			}
			else { // ValidFrom is before StartDate, so just set the ValidTo to be before StartDate.
				rwl.setValidTo(TimeUtil.addDays(m_subregionRecap.getStartDate(), -1));
				rwl.saveEx();
			}
		}
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MUNSShopEmployee employee = (MUNSShopEmployee) po;
		
		Enumeration<String> keys = freeColVals.keys();
		while (keys.hasMoreElements()) 
		{
			try {
				String key = keys.nextElement();
				int day = Integer.valueOf(key);
				m_cPeriod.set(Calendar.DAY_OF_MONTH, day);
				Timestamp period = new Timestamp(m_cPeriod.getTimeInMillis());
								
				X_UNS_Resource_WorkerLine schedule = Query
						.get(m_ctx,
								UNSPPICModelFactory.getExtensionID(),
								X_UNS_Resource_WorkerLine.Table_Name,
								"labor_id = ? AND validfrom = ? AND validto = ?",
								m_trxName)
						.setParameters(employee.getUNS_Employee_ID(), period, period)
						.first();
				
				if (schedule == null) {
					schedule = new MUNSResourceWorkerLine(m_ctx, 0, m_trxName);
					schedule.setValidFrom(period);
					schedule.setValidTo(period);
				}
				if (freeColVals.get(key) == null || Util.isEmpty((String) freeColVals.get(key), true))
					continue;
					
				String value = (String) freeColVals.get(key);
				X_UNS_Resource resource = Query
						.get(m_ctx,
								UNSPPICModelFactory.getExtensionID(),
								X_UNS_Resource.Table_Name,
								"C_BPartner_ID = ? AND VALUE = ? AND AD_Org_ID=?",
								//"c_bpartner_id = ? AND uns_slottype_id = ("
								//+ "		SELECT uns_slottype_id FROM uns_slottype "
								//+ " 	WHERE value = ? AND AD_Org_ID=?)",
								m_trxName)
						.setParameters(employee.getShop_ID(), value, employee.getAD_Org_ID()).first();
				
				if (resource == null) {
					boolean isValidCuti = false;
					/**
					if (X_UNS_Resource_WorkerLine.DAILYSCHEDULETYPE_CutiHamil.equals(value)
							|| X_UNS_Resource_WorkerLine.DAILYSCHEDULETYPE_CutiPanjang.equals(value)
							|| X_UNS_Resource_WorkerLine.DAILYSCHEDULETYPE_CutiTahunan.equals(value)
							|| X_UNS_Resource_WorkerLine.DAILYSCHEDULETYPE_LiburMingguan.equals(value)
							|| X_UNS_Resource_WorkerLine.DAILYSCHEDULETYPE_LiburNasional.equals(value)) {
					**/
					if (isScheduleTypeExists(value)) {
						isValidCuti = true;
					} else {
						return "Wrong daily schedule type or you have not define a resource for Dept. "
								+ employee.getShop().getValue();
					}
					
					if (isValidCuti) {
						resource = Query
								.get(m_ctx, UNSPPICModelFactory.getExtensionID(),
										X_UNS_Resource.Table_Name,
										"c_bpartner_id = ?", m_trxName)
										.setParameters(freeColVals.get("B")).first();
						schedule.setDailyScheduleType((String) value);
					} 
					
					if (resource == null) {						
						return "No resource found for Dept. " + employee.getShop().getValue();
					}
				}
				
				schedule.setLabor_ID((Integer)freeColVals.get("NIK"));
				schedule.setUNS_Resource_ID(resource.get_ID());
				schedule.saveEx();
			} catch (NumberFormatException e) {
				continue;
			}
		}
		return null;
	}
	
	
	private boolean isScheduleTypeExists(String value)
	{
		boolean exists = false;
		for (int i=0; i<m_scheduleTypes.length; i++) {
			if (m_scheduleTypes[i].getValue().equals(value)) {
				exists = true;
				break;
			}
		}
		return exists;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
		
	}
}