/**
 * 
 */
package com.uns.model.utilities;

import java.sql.Timestamp;

import org.compiere.util.Env;

import com.uns.model.MUNSUserAccess;
import com.uns.model.process.DecryptPayrollEncryption;

/**
 * @author nurse
 *
 */
public class PayrollAccessUserChecker implements Runnable 
{	
	private int p_userID = 0;
	private Timestamp p_date = null;
	private String p_trxName = null;
	private int p_sessionID = 0;
	
	public PayrollAccessUserChecker(int userID, Timestamp date, String trxName) 
	{
		super ();
		p_userID = userID;
		p_date = date;
		p_trxName = trxName;
		if (p_date == null)
			p_date = new Timestamp(System.currentTimeMillis());
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() 
	{
		while (true)
		{
			try 
			{
				Thread.sleep(30000);
			}
			catch (InterruptedException e) 
			{
				break;
			}
			try
			{
				String imUnlimited = Env.getContext(Env.getCtx(), DecryptPayrollEncryption.I_M_UNLIMITED);
				if ("Y".equals(imUnlimited))
					continue;
				int currentSession = Env.getContextAsInt(Env.getCtx(), "#AD_Session_ID");
				if (p_sessionID == 0)
					p_sessionID = currentSession;
				if (p_sessionID != currentSession)
					break;
				if (p_sessionID == 0)
					break;
				MUNSUserAccess.setExpired(p_trxName);
				MUNSUserAccess.registerPrivateKeyForUser(p_userID, p_date, p_trxName);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
}
