/**
 * 
 */
package com.uns.model.utilities;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MNonBusinessDay;
import org.compiere.model.MPeriod;
import org.compiere.util.TimeUtil;

import com.uns.model.MUNSAttConfiguration;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSDailyPresence;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSLeavePermissionTrx;
import com.uns.model.MUNSOTGroupRequest;
import com.uns.model.MUNSOTRequest;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSResource;
import com.uns.model.MUNSResourceWorkerLine;
import com.uns.model.MUNSSlotType;
import com.uns.model.MUNSWorkHoursConfig;
import com.uns.model.MUNSWorkHoursConfigLine;

/**
 * @author Burhani Adam
 *
 */
public class HRMUtils
{
	private MUNSEmployee m_employee;
	private MUNSContractRecommendation m_contract;
	private Timestamp m_date;
	private Timestamp m_presenceDate;
	private MPeriod m_period;
	private Properties m_ctx;
	private String m_trxName;
	private MUNSPayrollConfiguration m_config;
	private MUNSAttConfiguration m_attConfig;
	private MUNSResource m_resource;
	private Timestamp m_start;
	private Timestamp m_end;
	private Timestamp m_timeInRule;
	private Timestamp m_maxLateTimeInRule;
	private Timestamp m_maxEarlierTimeInRule;
	private Timestamp m_timeOutRule;
	private Timestamp m_maxLateTimeOutRule;
	private Timestamp m_maxEarlierTimeOutRule;
	private MUNSSlotType m_slotType;
	private MUNSWorkHoursConfig m_workHoursConfig;
	private int m_defaultHourIn;
	private int m_defaultHourOut;
	private int m_defaultMinuteIn;
	private int m_defaultMinuteOut;
	private double m_defaultBreakTime = 0;
	private double m_breakTimeOnOT = 0;
	private double m_normalWorkHours;
	private double m_overTimeEvtHours;
	private double m_maxWorkHours;
	private double m_addWorkHours;
	private double m_shortTime;
	private double m_adjustTimeRules = 0;
	private Timestamp m_startOT;
	private Timestamp m_endOT;
	private boolean m_isOverTime = false;
	private boolean m_isWorkDay = false;
	private boolean m_isNationalHoliday = false;
	private boolean m_isWeeklyHoliday = false;
	private String m_day = null;

	/**
	 * 
	 */
	public HRMUtils(MUNSEmployee employee, Timestamp date)
	{
		super();
		m_employee = employee;
		m_date = date;
		m_ctx = employee.getCtx();
		m_trxName = employee.get_TrxName();
		m_contract = MUNSContractRecommendation.getEffectiveContract(getCtx(), m_employee, date, get_TrxName());
		if(m_contract == null)
			throw new AdempiereException("No contract found");
		m_config = MUNSPayrollConfiguration.get(
				getCtx(), getDate(), m_contract.getAD_Org_ID(), get_TrxName(), true);
		if(m_attConfig == null)
		{
			m_attConfig = MUNSAttConfiguration.get(
					getCtx(), date, m_contract.getAD_Org_ID(), 
					m_contract.getEmploymentType(), 
					m_contract.getNewSectionOfDept_ID(), 
					m_contract.getNewJob().getC_JobCategory_ID(), 
					get_TrxName());
		}
		m_resource = MUNSResource.getByEmployee(getCtx(), 
				employee.get_ID(), m_date, employee.get_TrxName());
		if(m_resource == null)
			return;
//		initSlotType();
		initWorkHoursConfig();
		initDefaultTime();
	}
	
	//initDay Only
	public HRMUtils(Properties ctx, MUNSEmployee employee, Timestamp date, String trxName)
	{
		super();
		m_employee = employee;
		m_date = date;
		m_ctx = ctx;
		if(m_resource == null)
			m_resource = MUNSResource.getByEmployee(getCtx(), 
					m_employee.get_ID(), date, trxName);
		
		m_contract = MUNSContractRecommendation.getEffectiveContract(
				getCtx(), m_employee, date, get_TrxName());
		if(m_contract == null)
			throw new AdempiereException("No contract found");
		if(m_slotType == null)
			m_slotType = new MUNSSlotType(getCtx(), m_resource.getUNS_SlotType_ID(), get_TrxName());
		MUNSWorkHoursConfig adjustment = MUNSWorkHoursConfig.getByEmployee(getCtx(), 
				m_resource.get_ID(), m_employee.get_ID(), date, get_TrxName());
		MUNSWorkHoursConfigLine adjustLine = null;
		if(adjustment == null)
			adjustLine = MUNSWorkHoursConfigLine.get(getCtx(), m_employee.get_ID(), date, get_TrxName());
		if(adjustment != null && adjustment.getUNS_SlotType_ID() > 0)
			m_slotType = new MUNSSlotType(getCtx(), adjustment.getUNS_SlotType_ID(), get_TrxName());
		else if(adjustLine != null)
		{
			MUNSResource rs = MUNSResource.get(getCtx(), adjustLine.getUNS_Resource_ID());
			m_slotType = new MUNSSlotType(getCtx(), rs.getUNS_SlotType_ID(), get_TrxName());
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		m_isNationalHoliday = MNonBusinessDay.isNonBusinessDay(getCtx(), date, m_contract.getAD_Org_ID(), trxName);
		if(adjustment != null)
		{
			m_isWorkDay = m_slotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
			if(!m_isWorkDay)
				m_isWeeklyHoliday = true;
		}
		else if(adjustLine != null)
		{
			m_isWorkDay = m_slotType.isOperationalShop() ? (adjustLine.getUNS_Resource_ID() > 0) 
					: m_slotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK)); 
			m_isWeeklyHoliday = 
					m_slotType.isOperationalShop() ? 
							(MUNSWorkHoursConfigLine.DAILYSCHEDULETYPE_LiburMingguan.equals(adjustLine.getDailyScheduleType()))
							: !m_isWorkDay;
		}
		else
		{
			MUNSResourceWorkerLine wl = MUNSResourceWorkerLine.get(getCtx(), m_employee.get_ID(), date, get_TrxName());
			if(wl == null)
				throw new AdempiereException("Employee resource configuration not found. " + m_employee.getName());
			if(m_slotType.isOperationalShop())
			{	
				m_isWorkDay = (wl.getDailyScheduleType() == null);
				if(m_isWorkDay && !m_slotType.isOTOnNasionalHoliday())
				{
					m_isNationalHoliday = false;
				}
				if(!m_isWorkDay)
				{	
					if(wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_LiburMingguan))
						m_isWeeklyHoliday = true;
					else if(wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_LiburNasional))
						m_isNationalHoliday = true;
				}	
			}
			else
			{
				m_isWorkDay = m_slotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
				if(!m_isWorkDay && !m_isNationalHoliday)
					m_isWeeklyHoliday = true;
			}
		}
	}
	
	public void initDay (Timestamp date, String trxName)
	{
		if(m_resource == null)
			m_resource = MUNSResource.getByEmployee(getCtx(), 
					m_employee.get_ID(), date, trxName);
		if(m_slotType == null)
			m_slotType = new MUNSSlotType(getCtx(), m_resource.getUNS_SlotType_ID(), get_TrxName());
		MUNSWorkHoursConfig adjustment = MUNSWorkHoursConfig.getByEmployee(getCtx(), 
				m_resource.get_ID(), m_employee.get_ID(), date, get_TrxName());
		MUNSWorkHoursConfigLine adjustLine = null;
		if(adjustment == null)
			adjustLine = MUNSWorkHoursConfigLine.get(getCtx(), m_employee.get_ID(), date, get_TrxName());
		if(adjustment != null && adjustment.getUNS_SlotType_ID() > 0)
			m_slotType = new MUNSSlotType(getCtx(), adjustment.getUNS_SlotType_ID(), get_TrxName());
		else if(adjustLine != null)
		{
			MUNSResource rs = MUNSResource.get(getCtx(), adjustLine.getUNS_Resource_ID());
			m_slotType = new MUNSSlotType(getCtx(), rs.getUNS_SlotType_ID(), get_TrxName());
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		m_isNationalHoliday = MNonBusinessDay.isNonBusinessDay(getCtx(), date, m_employee.getAD_Org_ID(), trxName);
		if(adjustment != null)
		{
			m_isWorkDay = m_slotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
			if(!m_isWorkDay)
				m_isWeeklyHoliday = true;
		}
		else if(adjustLine != null)
		{
			m_isWorkDay = m_slotType.isOperationalShop() ? (adjustLine.getUNS_Resource_ID() > 0) 
					: m_slotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
			m_isWeeklyHoliday = m_slotType.isOperationalShop() ?
					(MUNSWorkHoursConfigLine.DAILYSCHEDULETYPE_LiburMingguan.equals(adjustLine.getDailyScheduleType()))
					: !m_isWorkDay;
		}
		else
		{
			MUNSResourceWorkerLine wl = MUNSResourceWorkerLine.get(getCtx(), m_employee.get_ID(), date, get_TrxName());
			if(wl == null)
				throw new AdempiereException("Employee resource configuration not found. " + m_employee.getName());
			if(m_slotType.isOperationalShop())
			{	
				m_isWorkDay = (wl.getDailyScheduleType() == null);
				if(m_isWorkDay && !m_slotType.isOTOnNasionalHoliday())
				{
					m_isNationalHoliday = false;
				}
				if(!m_isWorkDay)
				{	
					if(wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_LiburMingguan))
						m_isWeeklyHoliday = true;
					else if(wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_LiburNasional))
						m_isNationalHoliday = true;
				}	
			}
			else
			{
				m_isWorkDay = m_slotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
				if(!m_isWorkDay && !m_isNationalHoliday)
					m_isWeeklyHoliday = true;
			}
		}
	}
	
	public HRMUtils(MUNSEmployee employee, Timestamp date, MUNSDailyPresence daily,
			MUNSAttConfiguration attConfig, MUNSLeavePermissionTrx leave)
	{
		m_date = date;
		m_contract = MUNSContractRecommendation.getEffectiveContract(employee.getCtx(), employee, date, employee.get_TrxName());
		if(m_contract == null)
			throw new AdempiereException("No contract found");
		if(attConfig == null && m_attConfig == null)
			attConfig = MUNSAttConfiguration.get(employee.getCtx(), date, m_contract.getAD_Org_ID(),
					m_contract.getEmploymentType(), m_contract.getNewSectionOfDept_ID(), 
					m_contract.getNewJob().getC_JobCategory_ID(), employee.get_TrxName());
		
		m_attConfig = attConfig;	
		m_ctx = employee.getCtx();
		m_trxName = employee.get_TrxName();
		m_employee = employee;
		m_config = getPayConfig();
		Calendar cal = Calendar.getInstance();
		cal.setTime(m_date);
		Calendar calIn = Calendar.getInstance();
		calIn.setTime(daily.getTimeInRules());
		Calendar calOut = Calendar.getInstance();
		calOut.setTime(daily.getTimeOutRules());

		Calendar calDay = Calendar.getInstance();
		calDay.setTime(daily.getPresenceDate());
		int day = calDay.get(Calendar.DAY_OF_WEEK);
		m_day = "" + day;
		initDay(daily.getPresenceDate(), m_trxName);
		
		//try get to new version
		MUNSOTRequest[] OTRequest = MUNSOTGroupRequest.getValidof(
				get_TrxName(), m_date, m_employee.get_ID());
		//if new version == null then use old version
		if(OTRequest.length == 0)
			OTRequest = MUNSOTRequest.getValidof(
					get_TrxName(), m_date, m_employee.get_ID());
		
		walkOnOverTime(OTRequest);
		if(isOverTime())
		{
			long milisecond = m_endOT.getTime() - m_startOT.getTime();
			m_overTimeEvtHours = (double) milisecond /1000/60/60;
		}
		m_timeInRule = daily.getTimeInRules();
		m_timeOutRule = daily.getTimeOutRules();
		
		calIn.add(Calendar.MINUTE, m_attConfig.getMaxLateFSIn());
		if(leave != null)
		{
			long advanceMaxLateFSin = 0;
			if(leave.getLeavePeriodType().equals(MUNSLeavePermissionTrx.LEAVEPERIODTYPE_ShortTime))
			{
				if((leave.getStartTime().before(m_timeInRule) && leave.getEndTime().before(m_timeOutRule))
						|| (leave.getStartTime().equals(m_timeInRule) && leave.getEndTime().before(m_timeOutRule)))
				{
//					Calendar calTimeInRule = Calendar.getInstance();
//					calTimeInRule.setTime(m_timeInRule);
//					Calendar calEndTime = Calendar.getInstance();
//					calEndTime.setTime(leave.getEndTime());
//					calEndTime.set(Calendar.YEAR, calTimeInRule.get(Calendar.YEAR));
//					calEndTime.set(Calendar.MONTH, calTimeInRule.get(Calendar.MONTH));
//					calEndTime.set(Calendar.DATE, calTimeInRule.get(Calendar.DATE));
//					m_shortTime = calEndTime.getTimeInMillis() - m_timeInRule.getTime();
//					m_shortTime = m_shortTime / 60000;
					m_shortTime = leave.getLeaveRequested().intValue();
					calIn.add(Calendar.MINUTE, (int) m_shortTime);
				}
			}
			else if(leave.getLeavePeriodType().equals(MUNSLeavePermissionTrx.LEAVEPERIODTYPE_StartDateIsHalfDay))
			{
				advanceMaxLateFSin = m_timeOutRule.getTime() - m_timeInRule.getTime();
				advanceMaxLateFSin = advanceMaxLateFSin / 60000;
				advanceMaxLateFSin = advanceMaxLateFSin / 2;
				m_adjustTimeRules = advanceMaxLateFSin;
				calIn.add(Calendar.MINUTE, (int) advanceMaxLateFSin);
			}
		}
		m_maxLateTimeInRule = new Timestamp(calIn.getTimeInMillis());
		
		if(leave != null)
		{
			long advanceMaxEarlierFSOut = 0;
			if(leave.getLeavePeriodType().equals(MUNSLeavePermissionTrx.LEAVEPERIODTYPE_ShortTime))
			{
				if((leave.getStartTime().before(m_timeOutRule) && leave.getEndTime().equals(m_timeOutRule))
						|| (leave.getStartTime().before(m_timeOutRule) && leave.getEndTime().after(m_timeOutRule)))
				{
//					Calendar calTimeOutRule = Calendar.getInstance();
//					calTimeOutRule.setTime(m_timeOutRule);
//					Calendar calStartTime = Calendar.getInstance();
//					calStartTime.setTime(leave.getStartTime());
//					calStartTime.set(Calendar.YEAR, calTimeOutRule.get(Calendar.YEAR));
//					calStartTime.set(Calendar.MONTH, calTimeOutRule.get(Calendar.MONTH));
//					calStartTime.set(Calendar.DATE, calTimeOutRule.get(Calendar.DATE));
//					m_shortTime = m_timeOutRule.getTime() - calStartTime.getTimeInMillis();
//					m_shortTime = m_shortTime / 60000;
					m_shortTime = leave.getLeaveRequested().intValue();
					calOut.add(Calendar.MINUTE, -((int) m_shortTime));
				}
			}
			else if(leave.getLeavePeriodType().equals(MUNSLeavePermissionTrx.LEAVEPERIODTYPE_EndDateIsHalfDay))
			{
				advanceMaxEarlierFSOut = m_timeOutRule.getTime() - m_timeInRule.getTime();
				advanceMaxEarlierFSOut = advanceMaxEarlierFSOut / 60000;
				advanceMaxEarlierFSOut = advanceMaxEarlierFSOut / 2;
				m_adjustTimeRules = advanceMaxEarlierFSOut;
				calOut.add(Calendar.MINUTE, -((int) advanceMaxEarlierFSOut));
			}
		}
		
		if(leave != null && leave.getLeavePeriodType().equals(MUNSLeavePermissionTrx.LEAVEPERIODTYPE_ShortTime))
		{
			if(leave.getStartTime().after(m_timeInRule) && leave.getEndTime().before(m_timeOutRule))
			{
				m_shortTime = leave.getLeaveRequested().intValue();
			}
		}
		calOut.add(Calendar.MINUTE, -m_attConfig.getMaxEarLierFSOut());
		m_maxEarlierTimeOutRule  = new Timestamp(calOut.getTimeInMillis());
		
		if((cal.getTimeInMillis() <= m_maxLateTimeInRule.getTime()
				&& cal.getTimeInMillis() >= daily.getMinTimeInRule().getTime())
				|| (cal.getTimeInMillis() <= daily.getMaxTimeOutRule().getTime()
				&& cal.getTimeInMillis() >= m_maxEarlierTimeOutRule.getTime()))
		{
			m_presenceDate = daily.getPresenceDate();
		}
	}
	
	public MUNSAttConfiguration getAttConfig()
	{
		if(m_attConfig != null)
			return m_attConfig;
		
		return null;
	}
	
	public MUNSPayrollConfiguration getPayConfig()
	{
		if(m_config == null)
			m_config = MUNSPayrollConfiguration.get(
					getCtx(), getDate(), 0, get_TrxName(), true);
		
		return m_config;
	}
	
	/**
	 * 
	 * @param employee
	 * @param period
	 */
	public HRMUtils (MUNSEmployee employee, MPeriod period)
	{
		this(employee, period.getStartDate());
		m_period = period;
	}
	
	/**
	 * 
	 * @param employee
	 * @param C_Period_ID
	 */
	public HRMUtils (MUNSEmployee employee, int C_Period_ID)
	{
		this(employee, MPeriod.get(employee.getCtx(), C_Period_ID));
	}
	
	/**
	 * 
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param C_Period_ID
	 * @param trxName
	 */
	public HRMUtils (Properties ctx, int UNS_Employee_ID, 
			int C_Period_ID, String trxName)
	{
		this (new MUNSEmployee(ctx, UNS_Employee_ID, trxName), C_Period_ID);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param date
	 * @param trxName
	 */
	public HRMUtils (Properties ctx, int UNS_Employee_ID, 
			Timestamp date, String trxName)
	{
		this(new MUNSEmployee(ctx, UNS_Employee_ID, trxName), date);
	}
	
	private void initDefaultTime()
	{
		Calendar calIn = Calendar.getInstance();
		Calendar calOut = Calendar.getInstance();
		if(m_workHoursConfig != null && m_workHoursConfig.getUNS_SlotType_ID() <= 0)
		{
			calIn.setTime(m_workHoursConfig.getStartTime());
			calOut.setTime(m_workHoursConfig.getEndTime());
		}
		else if(m_slotType != null)
		{
			calIn.setTime(m_slotType.getTimeSlotStart());
			calOut.setTime(m_slotType.getTimeSlotEnd());
		}
		
		m_defaultHourIn = calIn.get(Calendar.HOUR_OF_DAY);
		m_defaultMinuteIn = calIn.get(Calendar.MINUTE);
		m_defaultHourOut = calOut.get(Calendar.HOUR_OF_DAY);
		m_defaultMinuteOut = calOut.get(Calendar.MINUTE);
	}
	
	/**
	 * 
	 * @return
	 */
	public String init ()
	{
		return init(false);
	}
	
	/** 
	 * @param forPresence
	 * @return
	 */
	public String init (boolean forPresence)
	{
		String msg = null;
		
		if (null == m_config)
		{
			msg = "Can't find payroll configuration.";
		}
		else if (null == m_employee)
		{
			msg = "Missing parameter employee.";
		}
		else if (null == m_date && null == m_period)
		{
			msg = "Required parameter date or period.";
		}
		else if (m_resource == null)
		{
			msg = "Can't find resource for " + m_employee.toString();
		}
		else if (m_slotType == null)
		{
			msg = "Can't find slot type configuration for " + m_employee.toString();
		}
		else if (getC_Period() != null)
		{
			msg = initByPeriod(forPresence);
		}
		else 
		{
			msg = initByDate(forPresence);
		}
		
		if (msg == null) 
		{
			validatePresenceDate();
			boolean isOverTimeOnHoliday = isOverTime() && (isNationalHoliday() || isWeeklyHoliday());
			if(!isOverTimeOnHoliday)
			{
				if(m_workHoursConfig != null && m_workHoursConfig.getUNS_SlotType_ID() <= 0
						&& m_workHoursConfig.isOverwriteBreakTime())
				{
					BigDecimal bd = m_workHoursConfig.getBreakTime();
					m_defaultBreakTime = bd.doubleValue();
				}
				else if (m_slotType != null && m_slotType.isDaySlot())
				{
					BigDecimal bd = m_slotType.getBreakTime(Integer.valueOf(getDay()));
					if (bd != null)
						m_defaultBreakTime = bd.doubleValue();
				}
			}
			initWorkHours();
		}
		
		return msg;
	}
	
	private String initByPeriod (boolean forPresence)
	{
		m_config.initPayrollPeriodOf(getC_Period().get_ID());
		m_start = m_config.getStartDate();
		m_end = m_config.getEndDate();
		
		if (!forPresence)
			return null;
		
		iniTimeSlotComponent();
		
		return null;
	}
	
	/**
	 * 
	 */
	private String initByDate (boolean forPresence)
	{
		m_config.initPayrollPeriodOf(getDate());
		m_period = MPeriod.get(getCtx(), m_config.getC_Period_ID ());
		m_start = m_config.getStartDate();
		m_end = m_config.getEndDate();
		
		if (!forPresence)
			return null;
		
		iniTimeSlotComponent();
		
		return null;
	}
	
	private void initWorkHours ()
	{
		initNormalWorkHours();
		if (m_isOverTime)
		{
			long milisecond = m_endOT.getTime() - m_startOT.getTime();
			m_overTimeEvtHours = (double) milisecond /1000/60/60;
		}
		else
			m_overTimeEvtHours = 0;
		m_maxWorkHours = m_normalWorkHours;
	}
	
	private void initNormalWorkHours()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(m_date);
		int day = cal.get(Calendar.DAY_OF_WEEK);
		m_day = "" + day;
		if(m_day.equals(m_config.getShortDay()))
			m_normalWorkHours =  (double) m_slotType.getShortDayWorkHours().longValue(); // - m_defaultBreakTime;
		else
			m_normalWorkHours =  (double) m_slotType.getNormalDayWorkHours().longValue();// - m_defaultBreakTime;

		initAddWorkHours();
	}
	
	private void initAddWorkHours()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(m_date);
		int day = cal.get(Calendar.DAY_OF_WEEK);
		m_day = "" + day;
		long workHours = m_defaultHourOut - m_defaultHourIn;
//		double workHours = (double) milisecond /1000/60/60;
		if(m_day.equals(m_config.getShortDay()))
			m_addWorkHours = workHours - (double) m_config.getShortDayWorkHours().longValue();
		else
			m_addWorkHours = workHours - (double) m_config.getNormalDayWorkHours().longValue();
	}
	
	/**
	 * Get C_Period model.
	 * @return
	 */
	public MPeriod getC_Period ()
	{
		return m_period;
	}
	
	private void iniTimeSlotComponent()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(m_workHoursConfig != null && m_workHoursConfig.getUNS_SlotType_ID() <= 0 ?
								m_workHoursConfig.getStartTime().getTime() 
									: m_slotType.getTimeSlotStart().getTime());
		cal.setTimeInMillis(m_date.getTime());
		cal.set(Calendar.HOUR_OF_DAY, m_defaultHourIn);
		cal.set(Calendar.MINUTE, m_defaultMinuteIn);
		cal.set(Calendar.SECOND, 0);
		m_timeInRule = new Timestamp(cal.getTimeInMillis());
		
		cal.add(Calendar.MINUTE, m_attConfig.getMaxLateFSIn());
		m_maxLateTimeInRule = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxLateFSIn());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxEarlierFSIn());
		m_maxEarlierTimeInRule = new Timestamp(cal.getTimeInMillis());
		
		cal.setTimeInMillis(m_date.getTime());
		if (m_defaultHourOut < m_defaultHourIn)
		{
			cal.add(Calendar.DATE, 1);
		}
		
		cal.set(Calendar.HOUR_OF_DAY, m_defaultHourOut);
		cal.set(Calendar.MINUTE, m_defaultMinuteOut);
		m_timeOutRule = new Timestamp(cal.getTimeInMillis());

		cal.add(Calendar.MINUTE, -m_attConfig.getMaxEarLierFSOut());
		m_maxEarlierTimeOutRule  = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, m_attConfig.getMaxEarLierFSOut());
		cal.add(Calendar.MINUTE, m_attConfig.getMaxLateFSOut());
		m_maxLateTimeOutRule = new Timestamp(cal.getTimeInMillis());
	}
	
	private void initWorkHoursConfig()
	{
		MUNSWorkHoursConfig config = MUNSWorkHoursConfig.getByEmployee(getCtx(), m_resource.get_ID(), 
													m_employee.get_ID(), m_date, get_TrxName());
		if(config != null)
		{
			if(config.getUNS_SlotType_ID() > 0)
				m_slotType = new MUNSSlotType(getCtx(), config.getUNS_SlotType_ID(), get_TrxName());
			else
			{
				m_workHoursConfig = config;
			}
		}
		if(m_slotType == null)
			initSlotType();
		
		if(m_slotType != null && m_slotType.getUNS_AttConfiguration_ID() > 0)
		{
			m_attConfig = MUNSAttConfiguration.get(getCtx(), 
					m_slotType.getUNS_AttConfiguration_ID(), get_TrxName());
		}
	}
	
	private void initSlotType()
	{
		MUNSSlotType st = MUNSSlotType.getByEmployee(getCtx(), m_employee.get_ID(), m_date, get_TrxName());
		if(st != null)
			m_slotType = st;
	}
	
	/**
	 * Validate presence date by check date, if check date is not between time 
	 * in rule and time out rule the presence date will be set to null.
	 * @return
	 */
	private Timestamp validatePresenceDate ()
	{
		Calendar cal = Calendar.getInstance();
		//try get to new version
		MUNSOTRequest[] OTRequest = MUNSOTGroupRequest.getValidof(
				get_TrxName(), getDate(), m_employee.get_ID());
		//if new version == null then use old version
		if(OTRequest.length == 0)
			OTRequest = MUNSOTRequest.getValidof(
					get_TrxName(), getDate(), m_employee.get_ID());
		
		walkOnOverTime(OTRequest);
		//minimum waktu masuk
		cal.setTimeInMillis(m_maxEarlierTimeInRule.getTime());
		int hourIn = cal.get(Calendar.HOUR_OF_DAY);
		int minuteIn = cal.get(Calendar.MINUTE);
		
		//tanggal dan waktu absen
		cal.setTimeInMillis(m_date.getTime());
		int checkHour = cal.get(Calendar.HOUR_OF_DAY);
		int checkMinute = cal.get(Calendar.MINUTE);
		
		//maximum waktu keluar
		cal.setTimeInMillis(m_maxLateTimeOutRule.getTime());
		int hourOut = cal.get(Calendar.HOUR_OF_DAY);
		int minuteOut = cal.get(Calendar.MINUTE);
	
		if (checkHour < hourIn || (checkHour == hourIn 
				&& checkMinute < minuteIn))
		{
			cal.add(Calendar.DATE, -1);
		}
		else if (checkHour > hourOut || (checkHour == hourOut
				&& checkMinute > minuteOut))
		{
			cal.add(Calendar.DATE, 1);
			hourOut = hourOut + 24;
		}
		
		if ((checkHour > hourIn && checkHour < hourOut)
				|| ((checkHour == hourIn && checkMinute >= minuteIn)
				|| (checkHour == hourOut&& checkHour <= minuteOut)))
		{
			m_presenceDate = TimeUtil.trunc(
					new Timestamp(cal.getTimeInMillis()), 
					TimeUtil.TRUNC_DAY);
		}
		else if (isOverTime())
		{
			if (getDate().equals(getStartOverTime()) 
					|| getDate().equals(getEndOverTime())
					|| (getDate().before(getEndOverTime()) 
							&& getDate().after(getStartOverTime())))
			{
				m_presenceDate = TimeUtil.trunc(m_date, TimeUtil.TRUNC_DAY);
			}
		}
		
		cal.setTime(m_date);
		int day = cal.get(Calendar.DAY_OF_WEEK);
		m_day = "" + day;
//		if (MUNSEmployee.SHIFT_Shift.equals(m_employee.getShift()))
//		{
			m_isWeeklyHoliday = null != m_slotType && 
					!m_slotType.IsWorkDay(day);
//		}
//		else
//		{
//			m_isWeeklyHoliday = !m_employee.isWorkDay(getDay ());
//		}
			
		m_isNationalHoliday = MNonBusinessDay.isNonBusinessDay(
				getCtx(), m_presenceDate == null ? m_date : m_presenceDate, m_employee.getAD_Org_ID(), get_TrxName());
		if (null == getPresenceDate())
		{
			return null;
		}
		
		return m_presenceDate;
	}
	
	private void walkOnOverTime (MUNSOTRequest[] request)
	{
		if (request.length <= 0)
		{
			return;
		}
		
		for(int i=0;i<request.length;i++)
		{
			if(m_startOT == null)
				m_startOT = request[i].getStartTime();
			else if(m_startOT.after(request[i].getStartTime()))
				m_startOT = request[i].getStartTime();
			if(m_endOT == null)
				m_endOT = request[i].getEndTime();
			else if(m_endOT.before(request[i].getEndTime()))
				m_endOT = request[i].getEndTime();
			m_breakTimeOnOT = m_breakTimeOnOT + request[i].getConfirmedBreakTime().doubleValue();
		}
		m_isOverTime = true;
		
		m_OTRequest = request;
	}
	
	public Timestamp getStartOverTime ()
	{
		return m_startOT;
	}
	
	public Timestamp getEndOverTime ()
	{
		return m_endOT;
	}
	
	public boolean isOverTime ()
	{
		return m_isOverTime;
	}
	
	public boolean isNationalHoliday ()
	{
		return m_isNationalHoliday;
	}
	
	public boolean isWeeklyHoliday ()
	{
		return m_isWeeklyHoliday;
	}
	
	public boolean isWorkDay ()
	{
		return m_isWorkDay;
	}
	
	/**
	 * Get presence date after validation
	 * @return
	 */
	public Timestamp getPresenceDate ()
	{
		return m_presenceDate;
	}
	
	public String getDay ()
	{
		return m_day;
	}
	
	public double getMaxWorkHours ()
	{
		return m_maxWorkHours;
	}
	
	/**
	 * Get contexts
	 * @return
	 */
	public Properties getCtx ()
	{
		return m_ctx;
	}
	
	/**
	 * Get transaction name.
	 * @return
	 */
	public String get_TrxName ()
	{
		return m_trxName;
	}
	
	/**
	 * @return
	 */
	public Timestamp getDate ()
	{
		return m_date;
	}
	
	 /**
	 * Get start date of payroll period
	 * @return
	 */
	public Timestamp getStartDate ()
	{
		return m_start;
	}
	
	/**
	 * Get end date of payroll period
	 * @return
	 */
	public Timestamp getEndDate ()
	{
		return m_end;
	}
	
	/**
	 * Get time in rule
	 * @return
	 */
	public Timestamp getTimeInRules ()
	{
		return m_timeInRule;
	}
	
	/**
	 * Get time out rule
	 * @return
	 */
	public Timestamp getTimeOutRules ()
	{
		return m_timeOutRule;
	}
	
	/**
	 * Get maximum earlier allowed time in to finger scan
	 * @return
	 */
	public Timestamp getMaxEarlierTimeInRules ()
	{
		return m_maxEarlierTimeInRule;
	}
	
	/**
	 * Get maximum late allowed time in to finger scan
	 * @return
	 */
	public Timestamp getMaxLateTimeInRules ()
	{
		return m_maxLateTimeInRule;
	}
	
	/**
	 * Get maximum earlier  allowed time out to finger scan
	 * @return
	 */
	public Timestamp getMaxEarlierTimeOutRules ()
	{
		return m_maxEarlierTimeOutRule;
	}
	
	/**
	 * Get maximum late allowed time out t finger scan
	 * @return
	 */
	public Timestamp getMaxLateTimeOutRules ()
	{
		return m_maxLateTimeOutRule;
	}
	
	/**
	 * Get Maximum hours over time
	 * @return
	 */
	public double getMaxOTHours ()
	{
		return m_overTimeEvtHours;
	}
	
	public boolean isAutoSynchronize()
	{
		if(m_slotType != null)
		{
			return m_slotType.isAutoSynchronize();
		}
		
		return true;
	}
	
	public double getAddWorkHours()
	{
		return m_addWorkHours;
	}
	
	public double getNormalWorkHours()
	{
		return m_normalWorkHours;
	}
	
	public double getDefaultBreakTime()
	{
		return m_defaultBreakTime;
	}
	
	public double getDefaultBreakTimeOnMinutes()
	{
		return m_defaultBreakTime * 60;
	}
	
	public MUNSSlotType getSlotType()
	{
		return m_slotType;
	}
	
	public double getBreakTimeOnOT()
	{
		return m_breakTimeOnOT;
	}
	
	private MUNSOTRequest[] m_OTRequest = null;
	
	public MUNSOTRequest[] getOTRequest()
	{
		return m_OTRequest;
	}
	
	private boolean m_OTOnNasionalHoliday = false;
	
	public boolean isOTOnHoliday()
	{
		return m_OTOnNasionalHoliday;
	}
	
	public double getShortTime()
	{
		return m_shortTime;
	}
	
	public double getAdjustTimeRule()
	{
		return m_adjustTimeRules;
	}
	
	public MUNSContractRecommendation getContract()
	{
		return m_contract;
	}
}