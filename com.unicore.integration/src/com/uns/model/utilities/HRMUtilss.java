package com.uns.model.utilities;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;

import org.compiere.model.MNonBusinessDay;
import org.compiere.model.MPeriod;
import org.compiere.model.MSysConfig;
import org.compiere.util.TimeUtil;

import com.uns.model.MUNSAddWorkHours;
import com.uns.model.MUNSAttConfiguration;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSOTGroupRequest;
import com.uns.model.MUNSOTRequest;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSResource;
import com.uns.model.MUNSSlotType;
import com.uns.model.MUNSStationSlotType;

public class HRMUtilss
{
	private MUNSEmployee m_employee;
	private Timestamp m_date;
	private Timestamp m_presenceDate;
	private MPeriod m_period;
	private Properties m_ctx;
	private String m_trxName;
	private MUNSPayrollConfiguration m_config;
	private MUNSAttConfiguration m_attConfig;
	private Timestamp m_start;
	private Timestamp m_end;
	private Timestamp m_timeInRule;
	private Timestamp m_maxLateTimeInRule;
	private Timestamp m_maxEarlierTimeInRule;
	private Timestamp m_timeOutRule;
	private Timestamp m_maxLateTimeOutRule;
	private Timestamp m_maxEarlierTimeOutRule;
	private MUNSSlotType m_slotType;
	private int m_defaultHourIn;
	private int m_defaultHourOut;
	private int m_addWorkHours = 0;
	private int m_defaultMinuteIn;
	private int m_defaultMinuteOut;
	private double m_defaultBreakTime = 0;
	private double m_normalWorkHours;
	private double m_overTimeEvtHours;
	private double m_maxWorkHours;
	private Timestamp m_startOT;
	private Timestamp m_endOT;
	private boolean m_isOverTime = false;
	private boolean m_isNationalHoliday = false;
	private boolean m_isWeeklyHoliday = false;
	private String m_day = null;
	
	/**
	 * 
	 * @param employeeslotType
	 * @param date
	 */
	public HRMUtilss (MUNSEmployee employee, Timestamp date)
	{
		super ();
		m_employee = employee;
		m_date = date;
		m_ctx = employee.getCtx();
		m_trxName = employee.get_TrxName();
		m_config = MUNSPayrollConfiguration.get(
				getCtx(), getDate(), employee.getAD_Org_ID(), get_TrxName(), true);
		m_attConfig = MUNSAttConfiguration.get(
				getCtx(), date, employee.getAD_Org_ID(), 
				employee.getEmploymentType(), 
				employee.getC_BPartner_ID(), 
				employee.getC_Job().getC_JobCategory_ID(), 
				get_TrxName());
		m_defaultHourIn = MSysConfig.getIntValue(
				MSysConfig.DEFAULT_ATT_HOUR_IN, 8, employee.getAD_Client_ID(), 
				employee.getAD_Org_ID());
		m_defaultHourOut = MSysConfig.getIntValue(
				MSysConfig.DEFAULT_ATT_HOUR_OUT, 17, 
				employee.getAD_Client_ID(), employee.getAD_Org_ID());
		m_defaultMinuteIn = MSysConfig.getIntValue(
				MSysConfig.DEFAULT_ATT_MINUTE_IN, 0,
				m_employee.getAD_Client_ID(), m_employee.getAD_Org_ID());
		m_defaultMinuteOut = MSysConfig.getIntValue(
				MSysConfig.DEFAULT_ATT_MINUTE_OUT, 0,
				m_employee.getAD_Client_ID(), m_employee.getAD_Org_ID());
		m_defaultBreakTime = MSysConfig.getDoubleValue("DEFAULT_BREAK_TIME", 1, 
				employee.getAD_Client_ID(), employee.getAD_Org_ID());
		
		initSlotType();
	}
	
	/**
	 * 
	 * @param employee
	 * @param period
	 */
	public HRMUtilss (MUNSEmployee employee, MPeriod period)
	{
		this(employee, period.getStartDate());
		m_period = period;
	}
	
	/**
	 * 
	 * @param employee
	 * @param C_Period_ID
	 */
	public HRMUtilss (MUNSEmployee employee, int C_Period_ID)
	{
		this(employee, MPeriod.get(employee.getCtx(), C_Period_ID));
	}
	
	/**
	 * 
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param C_Period_ID
	 * @param trxName
	 */
	public HRMUtilss (Properties ctx, int UNS_Employee_ID, 
			int C_Period_ID, String trxName)
	{
		this (new MUNSEmployee(ctx, UNS_Employee_ID, trxName), C_Period_ID);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param date
	 * @param trxName
	 */
	public HRMUtilss (Properties ctx, int UNS_Employee_ID, 
			Timestamp date, String trxName)
	{
		this(new MUNSEmployee(ctx, UNS_Employee_ID, trxName), date);
	}
	
	/**
	 * Get contexts
	 * @return
	 */
	public Properties getCtx ()
	{
		return m_ctx;
	}
	
	/**
	 * Get transaction name.
	 * @return
	 */
	public String get_TrxName ()
	{
		return m_trxName;
	}
	
	/**
	 * 
	 * @return
	 */
	public Timestamp getDate ()
	{
		return m_date;
	}
	
	/**
	 * 
	 */
	private String initByDate (boolean forPresence)
	{
		m_config.initPayrollPeriodOf(getDate());
		m_period = MPeriod.get(getCtx(), m_config.getC_Period_ID ());
		m_start = m_config.getStartDate();
		m_end = m_config.getEndDate();
		
		if (!forPresence)
		{
			return null;
		}
		else if (MUNSEmployee.SHIFT_NonShift.equals(m_employee.getShift()))
		{
			initNotTimeSlotShiftComponent();
		}
		else
		{
			if (m_slotType == null)
			{
				return "Can't find slot type configuration for " + m_employee.toString();
			}
			else if (m_slotType.isTimeSlot())
			{
				initTimeSlotShiftComponent();
			}
			else
			{
				initNotTimeSlotShiftComponent ();
			}
		}

		return null;
	}
	
	/**
	 * 
	 */
	private String initByPeriod (boolean forPresence)
	{
		m_config.initPayrollPeriodOf(getC_Period().get_ID());
		m_start = m_config.getStartDate();
		m_end = m_config.getEndDate();
		
		if (!forPresence)
			return null;
		else if (MUNSEmployee.SHIFT_NonShift.equals(m_employee.getShift()))
		{
			initNotTimeSlotShiftComponent();
		}
		else
		{
			if (m_slotType == null)
			{
				return "Can't find slot type configuration for " + m_employee.toString();
			}
			else if (m_slotType.isTimeSlot())
			{
				initTimeSlotShiftComponent();
			}
			else
			{
				initNotTimeSlotShiftComponent ();
			}
		}

		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	public String init ()
	{
		return init(false);
	}
	
	/**
	 * 
	 * @param forPresence
	 * @return
	 */
	public String init (boolean forPresence)
	{
		String msg = null;
		
		if (null == m_config)
		{
			msg = "Can't find payroll configuration.";
		}
		else if (null == m_employee)
		{
			msg = "Missing parameter employee.";
		}
		else if (null == m_date && null == m_period)
		{
			msg = "Required parameter date or period.";
		}
		else if (getC_Period() != null)
		{
			msg = initByPeriod(forPresence);
		}
		else 
		{
			msg = initByDate(forPresence);
		}
		
		if (msg == null) 
		{
			validatePresenceDate();
			if (m_slotType != null && m_slotType.isDaySlot())
			{
				BigDecimal bd = m_slotType.getBreakTime(Integer.valueOf(getDay()));
				if (bd != null)
					m_defaultBreakTime = bd.doubleValue() / 60;
			}
			initWorkHours();
		}
		
		
		return msg;
	}
	
	private void initWorkHours ()
	{
		long milisecond = m_timeOutRule.getTime() - m_timeInRule.getTime();
		m_normalWorkHours = (double) milisecond /1000/60/60;
		if (m_isOverTime)
		{
			milisecond = m_endOT.getTime() - m_startOT.getTime();
			m_overTimeEvtHours = (double) milisecond /1000/60/60;
		}
		else
			m_overTimeEvtHours = 0;
		m_maxWorkHours = m_normalWorkHours + m_overTimeEvtHours + m_addWorkHours - m_defaultBreakTime;
	}
	
	/**
	 * Initialize time slot shift component
	 */
	private void initTimeSlotShiftComponent ()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(m_slotType.getTimeSlotStart().getTime());
		int hourIn = cal.get(Calendar.HOUR_OF_DAY);
		int minuteIn = cal.get(Calendar.MINUTE);
		cal.setTimeInMillis(m_date.getTime());
		cal.set(Calendar.HOUR_OF_DAY, hourIn);
		cal.set(Calendar.MINUTE, minuteIn);
		cal.set(Calendar.SECOND, 0);
		m_timeInRule = new Timestamp(cal.getTimeInMillis());
		
		cal.add(Calendar.MINUTE, m_attConfig.getMaxLateFSIn());
		m_maxLateTimeInRule = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxLateFSIn());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxEarlierFSIn());
		m_maxEarlierTimeInRule = new Timestamp(cal.getTimeInMillis());
		
		cal.setTimeInMillis(m_slotType.getTimeSlotEnd().getTime());
		int hourOut = cal.get(Calendar.HOUR_OF_DAY);
		int minuteOut = cal.get(Calendar.MINUTE);
		cal.setTimeInMillis(m_date.getTime());
		cal.set(Calendar.HOUR_OF_DAY, hourOut);
		cal.set(Calendar.MINUTE, minuteOut);
		cal.set(Calendar.SECOND, 0);
		
		if (hourOut < hourIn)
		{
			cal.add(Calendar.DATE, 1);
		}
		
		m_timeOutRule = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxEarLierFSOut());
		m_maxEarlierTimeOutRule  = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, m_attConfig.getMaxEarLierFSOut());
		cal.add(Calendar.MINUTE, m_attConfig.getMaxLateFSOut());
		cal.add(Calendar.HOUR_OF_DAY, m_addWorkHours);
		m_maxLateTimeOutRule = new Timestamp(cal.getTimeInMillis());
	}
	
	/**
	 * Initialize not time slot shift component
	 */
	private void initNotTimeSlotShiftComponent ()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(m_date.getTime());
		cal.set(Calendar.HOUR_OF_DAY, m_defaultHourIn);
		cal.set(Calendar.MINUTE, m_defaultMinuteIn);
		cal.set(Calendar.SECOND, 0);
		m_timeInRule = new Timestamp(cal.getTimeInMillis());

		cal.add(Calendar.MINUTE, m_attConfig.getMaxLateFSIn());
		m_maxLateTimeInRule = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxLateFSIn());
		cal.add(Calendar.MINUTE, -m_attConfig.getMaxEarlierFSIn());
		m_maxEarlierTimeInRule = new Timestamp(cal.getTimeInMillis());
		
		if (m_defaultHourOut < m_defaultHourIn)
		{
			cal.add(Calendar.DATE, 1);
		}
		
		cal.set(Calendar.HOUR_OF_DAY, m_defaultHourOut);
		cal.set(Calendar.MINUTE, m_defaultMinuteOut);
		m_timeOutRule = new Timestamp(cal.getTimeInMillis());

		cal.add(Calendar.MINUTE, -m_attConfig.getMaxEarLierFSOut());
		m_maxEarlierTimeOutRule  = new Timestamp(cal.getTimeInMillis());
		cal.add(Calendar.MINUTE, m_attConfig.getMaxEarLierFSOut());
		cal.add(Calendar.MINUTE, m_attConfig.getMaxLateFSOut());
		cal.add(Calendar.HOUR_OF_DAY, m_addWorkHours);
		m_maxLateTimeOutRule = new Timestamp(cal.getTimeInMillis());
	}
	
	
	/**
	 * Get C_Period model.
	 * @return
	 */
	public MPeriod getC_Period ()
	{
		return m_period;
	}
	
	/**
	 * Get start date of payroll period
	 * @return
	 */
	public Timestamp getStartDate ()
	{
		return m_start;
	}
	
	/**
	 * Get end date of payroll period
	 * @return
	 */
	public Timestamp getEndDate ()
	{
		return m_end;
	}
	
	/**
	 * Get time in rule
	 * @return
	 */
	public Timestamp getTimeInRules ()
	{
		return m_timeInRule;
	}
	
	/**
	 * Get time out rule
	 * @return
	 */
	public Timestamp getTimeOutRules ()
	{
		return m_timeOutRule;
	}
	
	/**
	 * Get maximum earlier allowed time in to finger scan
	 * @return
	 */
	public Timestamp getMaxEarlierTimeInRules ()
	{
		return m_maxEarlierTimeInRule;
	}
	
	/**
	 * Get maximum late allowed time in to finger scan
	 * @return
	 */
	public Timestamp getMaxLateTimeInRules ()
	{
		return m_maxLateTimeInRule;
	}
	
	/**
	 * Get maximum earlier  allowed time out to finger scan
	 * @return
	 */
	public Timestamp getMaxEarlierTimeOutRules ()
	{
		return m_maxEarlierTimeOutRule;
	}
	
	/**
	 * Get maximum late allowed time out t finger scan
	 * @return
	 */
	public Timestamp getMaxLateTimeOutRules ()
	{
		return m_maxLateTimeOutRule;
	}
	
	/**
	 * Get presence date after validation
	 * @return
	 */
	public Timestamp getPresenceDate ()
	{
		return m_presenceDate;
	}
	
	/**
	 * Initialize slot type of employee
	 * @return
	 */
	private MUNSSlotType initSlotType ()
	{
		if (MUNSEmployee.SHIFT_NonShift.equals(m_employee.getShift()))
			return m_slotType = null;
		if (m_slotType != null)
			return m_slotType;
		int slotType_ID = 0;
		if (m_employee.getEmploymentType().equals("SUB")
				&& !m_employee.isUseGeneralPayroll())
			slotType_ID = MUNSResource.getSlotType_ID(m_employee);
		else
			slotType_ID = MUNSStationSlotType.getSlotTypeID(get_TrxName(), 
					m_employee.get_ID(), m_date);
		
		if (slotType_ID <= 0)
		{
			return null;
		}
		
		m_addWorkHours = MUNSAddWorkHours.getAddWorkHours(get_TrxName(), m_employee.get_ID(), m_date);
		return m_slotType = new MUNSSlotType(getCtx(), slotType_ID, get_TrxName());
	}
	
	/**
	 * Validate presence date by check date, if check date is not between time 
	 * in rule and time out rule the presence date will be set to null.
	 * @return
	 */
	private Timestamp validatePresenceDate ()
	{
		Calendar cal = Calendar.getInstance();
		walkOnOverTime(MUNSOTGroupRequest.getValidof(
				get_TrxName(), getDate(), m_employee.get_ID()));
		//minimum waktu masuk
		cal.setTimeInMillis(m_maxEarlierTimeInRule.getTime());
		int hourIn = cal.get(Calendar.HOUR_OF_DAY);
		int minuteIn = cal.get(Calendar.MINUTE);
		
		//tanggal dan waktu absen
		cal.setTimeInMillis(m_date.getTime());
		int checkHour = cal.get(Calendar.HOUR_OF_DAY);
		int checkMinute = cal.get(Calendar.MINUTE);
		
		//maximum waktu keluar
		cal.setTimeInMillis(m_maxLateTimeOutRule.getTime());
		int hourOut = cal.get(Calendar.HOUR_OF_DAY);
		int minuteOut = cal.get(Calendar.MINUTE);
	
		if (checkHour < hourIn || (checkHour == hourIn 
				&& checkMinute < minuteIn))
		{
			cal.add(Calendar.DATE, -1);
		}
		else if (checkHour > hourOut || (checkHour == hourOut
				&& checkMinute > minuteOut))
		{
			cal.add(Calendar.DATE, 1);
		}
		
		if ((checkHour > hourIn && checkHour < hourOut)
				|| ((checkHour == hourIn && checkMinute >= minuteIn)
				|| (checkHour == hourOut&& checkHour <= minuteOut)))
		{
			m_presenceDate = TimeUtil.trunc(
					new Timestamp(cal.getTimeInMillis()), 
					TimeUtil.TRUNC_DAY);
		}
		else if (isOverTime())
		{
			if (getDate().equals(getStartOverTime()) 
					|| getDate().equals(getEndOverTime())
					|| (getDate().before(getEndOverTime()) 
							&& getDate().after(getStartOverTime())))
			{
				m_presenceDate = TimeUtil.trunc(m_date, TimeUtil.TRUNC_DAY);
			}
		}
		
		m_isNationalHoliday = MNonBusinessDay.isNonBusinessDay(
				getCtx(), m_presenceDate, m_employee.getAD_Org_ID(), get_TrxName());
		if (null == getPresenceDate())
		{
			return null;
		}
		
		cal.setTimeInMillis(getPresenceDate().getTime());
		int day = cal.get(Calendar.DAY_OF_WEEK);
		m_day = "" + day;
		if (MUNSEmployee.SHIFT_Shift.equals(m_employee.getShift()))
		{
			m_isWeeklyHoliday = null != m_slotType && 
					!m_slotType.IsWorkDay(day);
		}
		else
		{
			m_isWeeklyHoliday = !m_employee.isWorkDay(getDay ());
		}
		
		return m_presenceDate;
	}
	
	private void walkOnOverTime (MUNSOTRequest[] request)
	{
		if (request.length <= 0)
		{
			return;
		}
		
		for(int i=0;i<request.length;i++)
		{
			if(m_startOT == null)
				m_startOT = request[i].getStartTime();
			if(m_endOT == null)
				request[i].getEndTime();
			m_isOverTime = true;
		}
	}
	
	public Timestamp getStartOverTime ()
	{
		return m_startOT;
	}
	
	public Timestamp getEndOverTime ()
	{
		return m_endOT;
	}
	
	public boolean isOverTime ()
	{
		return m_isOverTime;
	}
	
	public boolean isNationalHoliday ()
	{
		return m_isNationalHoliday;
	}
	
	public boolean isWeeklyHoliday ()
	{
		return m_isWeeklyHoliday;
	}
	
	public int getAddWorkHourse ()
	{
		return m_addWorkHours;
	}
	
	public String getDay ()
	{
		return m_day;
	}
	
	public double getMaxWorkHours ()
	{
		return m_maxWorkHours;
	}
	
	/**
	 * Get Maximum hours over time
	 * @return
	 */
	public double getMaxOTHours ()
	{
		return m_overTimeEvtHours;
	}
	
	public double getBreakHours ()
	{
		return m_defaultBreakTime;
	}
	
	public double getNormalWorkHourse ()
	{
		return m_normalWorkHours - m_defaultBreakTime;
	}
}