/**
 * 
 */
package com.uns.model.validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MClient;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutConfirm;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.base.model.MMovement;
import com.unicore.base.model.MMovementConfirm;
import com.unicore.base.model.MMovementLine;
import com.unicore.base.model.MRequisition;
import com.unicore.model.MUNSWeighbridgeTicket;

/**
 * @author root
 *
 */
public class UNSMMValidator implements ModelValidator {

	/**
	 * 
	 */
	public UNSMMValidator() {
		super();
	}
	
	private int m_AD_Client_ID = 0;
	private CLogger log = CLogger.getCLogger(getClass());

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#initialize(org.compiere.model.ModelValidationEngine, org.compiere.model.MClient)
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		if(client != null)
		{
			m_AD_Client_ID = client.getAD_Client_ID();
			log.log(Level.INFO, client.toString());
		}
		else
		{
			log.log(Level.INFO, "Initializing global validator -" + this.toString());
			log.log(Level.INFO, "Table affected : ");
		}
	
		log.log(Level.INFO, "- " + MMovement.Table_Name);
		engine.addModelChange(MMovement.Table_Name, this);
		engine.addModelChange(MMovementLine.Table_Name, this);
		engine.addDocValidate(MMovement.Table_Name, this);
		engine.addDocValidate(MUNSWeighbridgeTicket.Table_Name, this);
		engine.addDocValidate(MInOut.Table_Name, this);
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#getAD_Client_ID()
	 */
	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#login(int, int, int)
	 */
	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#modelChange(org.compiere.model.PO, int)
	 */
	@Override
	public String modelChange(PO po, int type) throws Exception {
//		if(po.get_TableName().equals(MMovement.Table_Name))
//		{
//			MMovement move = (MMovement) po;
//			if(!move.isInTransit())
//			{
//				move.setUNS_Shipping_ID(-1);
//				move.setIsAutoCreateShipping(false);
//				return null;
//			}
//			
//			if(move.getUNS_Shipping_ID() == 0 && ! move.isAutoCreateShipping())
//				return "Please define Shipping Document.";
//		}
		
		if(po.get_Table_ID() == MMovementLine.Table_ID)
		{
			if(type == TYPE_BEFORE_DELETE)
			{
				String sql = "DELETE FROM UNS_HandleRequests WHERE M_MovementLine_ID = ?";
				DB.executeUpdate(sql, po.get_ID(), po.get_TrxName());
			}
		}
		
		if(po.get_Table_ID() == MMovement.Table_ID)
		{
			if(type == TYPE_BEFORE_DELETE)
			{
				String sql = "DELETE FROM UNS_HandleRequests rs"
						+ " WHERE rs.M_MovementLine_ID IN ("
						+ "		SELECT l.M_MovementLine_ID FROM M_MovementLine l WHERE l.M_Movement_ID=?)";
				DB.executeUpdate(sql, po.get_ID(), po.get_TrxName());
			}
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#docValidate(org.compiere.model.PO, int)
	 */
	@Override
	public String docValidate(PO po, int timing) {
//		if(!po.get_TableName().equals(MMovement.Table_Name))
//			return null;
		
//		MMovement move = (MMovement) po;
//		if(!move.isInTransit())
//			return null;
//		String msg = null;
//		if(move.getUNS_Shipping_ID() > 0)
//			;
//		else if(move.isAutoCreateShipping())
//			msg = createShipping(move);
//		else
//			msg = "Please define Shipping Document.";
		
//		if(!Util.isEmpty(msg, true))
//			return msg;
		
//		if(timing != TIMING_AFTER_COMPLETE)
//			return null;
//		
//		MUNSShipping shipping = new MUNSShipping(
//				move.getCtx(), move.getUNS_Shipping_ID(), move.get_TrxName());
//		
//		MMovementLine[] lines = move.getLines(false);
//		BigDecimal totalWeight = shipping.getTonase();
//		BigDecimal totalVolume = shipping.getVolume();
//		for(MMovementLine line : lines)
//		{
//			I_M_Product product = line.getM_Product();
//			BigDecimal weight = product.getWeight();
//			BigDecimal volume = product.getVolume();
//			weight = weight.multiply(line.getMovementQty());
//			weight = weight.divide(Env.ONEHUNDRED);
//			volume = volume.multiply(line.getMovementQty());
//			totalWeight = totalWeight.add(weight);
//			totalVolume = totalVolume.add(volume);
//		}
//		
//		shipping.setTonase(totalWeight);
//		shipping.setVolume(totalVolume);
//		shipping.saveEx();
		
		/** POP General Weighbridge Ticket Handle Void */
		if (po.get_TableName().equals("UNS_WeighbridgeTicket"))
		{
			if (TIMING_AFTER_VOID == timing)
			{
				MUNSWeighbridgeTicket ticket = (MUNSWeighbridgeTicket) po;
				if (ticket.getM_Movement_ID() > 0)
				{
					MMovement move = new MMovement(ticket.getCtx(), 
							ticket.getM_Movement_ID(), ticket.get_TrxName());
					if (!"RE".equals(move.getDocStatus()) && !"VO".equals(move.getDocStatus()))
					{
						boolean processOK = move.processIt(DocAction.ACTION_Void);
						if (!processOK)
						{
							return move.getProcessMsg();
						}
						
						move.saveEx();
					}
				}
				if (ticket.getM_MovementConfirm_ID() > 0)
				{
					MMovementConfirm confirm = new MMovementConfirm (ticket.getCtx(), 
							ticket.getM_MovementConfirm_ID(), ticket.get_TrxName());
					if (!"RE".equals(confirm.getDocStatus()) && !"VO".equals(confirm.getDocStatus()))
					{
						boolean processOK = confirm.processIt(DocAction.ACTION_Void);
						if (!processOK)
						{
							return confirm.getProcessMsg();
						}
						
						confirm.saveEx();
					}
				}
			}
		}
		else if(po.get_TableName().equals("M_InOut"))
		{
			if(timing == TIMING_BEFORE_COMPLETE)
			{
				if(MInOut.MOVEMENTTYPE_VendorReceipts.equals(po.get_Value(MInOut.COLUMNNAME_MovementType)))
				{
					if(po.get_ValueAsInt(MInOut.COLUMNNAME_Reversal_ID) <= 0)
						checkUnnoticedMReceipt(po);
				}
			}
			else if(timing == TIMING_AFTER_VOID
					|| timing == TIMING_AFTER_REVERSECORRECT
					|| timing == TIMING_AFTER_REVERSEACCRUAL)
			{
				//unlink m_inoutline on unnoticedMReceipt
				String sql = "UPDATE UNS_UnnoticedMReceiptLine SET M_InOutLine_ID = null"
						+ " WHERE UNS_UnnoticedMReceipt_ID = (SELECT UNS_UnnoticedMReceipt_ID"
						+ " FROM UNS_UnnoticedMReceipt WHERE M_InOut_ID = ? AND"
						+ " DOcStatus NOT IN ('RE','VO'))";
				int ok = DB.executeUpdate(sql, po.get_ID(), po.get_TrxName());
				if(ok < 0)
					throw new AdempiereException("Error when try to unlink unnoticedMreceiptLine");
				
				//unlink m_inout on unnoticedMReceipt
				sql = "UPDATE UNS_UnnoticedMReceipt SET M_InOut_ID = null,"
						+ " NoticeStatus = 'WR' WHERE M_InOut_ID = ? AND"
						+ " DOcStatus NOT IN ('RE','VO')";
				ok = DB.executeUpdate(sql, po.get_ID(), po.get_TrxName());
				if(ok < 0)
					throw new AdempiereException("Error when try to unlink unnoticedMreceipt");
				
			}
		}
		else if(po.get_TableName().equals(MMovement.Table_Name))
		{
			if(timing == TIMING_AFTER_COMPLETE)
			{
				boolean auto = MSysConfig.getBooleanValue(MSysConfig.AUTO_CLOSE_REQUEST_AFTER_MOVE, true);
				if(auto)
				{
					String sql = "SELECT DISTINCT(rl.M_Requisition_ID) FROM UNS_HandleRequests hr"
							+ " INNER JOIN M_MovementLine ml ON ml.M_MovementLine_ID = hr.M_MovementLine_ID"
							+ " INNER JOIN M_RequisitionLine rl ON rl.M_RequisitionLine_ID = hr.M_RequisitionLine_ID"
							+ " WHERE ml.M_Movement_ID = ?";
					PreparedStatement st = null;
					ResultSet rs = null;
					try
					{
						st = DB.prepareStatement(sql, po.get_TrxName());
						st.setInt(1, po.get_ID());
						rs = st.executeQuery();
						while (rs.next())
						{
							MRequisition req = new MRequisition(
									po.getCtx(), rs.getInt(1), po.get_TrxName());
							if(!req.getDocStatus().equals("CL"))
							{
								req.processIt("CL");
								req.saveEx();
							}
						}
					}
					catch (SQLException e)
					{
						throw new AdempiereException(e.getMessage());
					}
				}
			}
			else if(timing == TIMING_BEFORE_PREPARE)
			{
				MMovement move = (MMovement) po;
				if(move.getBCDate() == null ||
						Util.isEmpty(move.getBCCode(), true))
				{
					throw new AdempiereException("Define BC Infomartion for complete this document.");
				}
			}
		}
		return null;
	}
	
	private void checkUnnoticedMReceipt(PO po) {
		
		String sql = "SELECT UNS_UnnoticedMReceipt_ID FROM UNS_UnnoticedMReceipt"
				+ " WHERE M_InOut_ID = ? AND DocStatus = 'CO'";
		int unnoticed_id = DB.getSQLValue(po.get_TrxName(), sql, po.get_ID());
		if(unnoticed_id > 0)
		{
			//check there was unnoticedline not matching with inout line
			String sqql = "SELECT count(*) FROM UNS_UnnoticedMReceiptLine umr"
					+ " FULL OUTER JOIN M_InOutLine il ON il.M_InOutLine_ID = umr.M_InOutLine_ID"
					+ " WHERE (umr.uns_unnoticedmreceipt_id = ? "
					+ " OR il.m_inout_id = ?) AND(umr.M_Product_ID <> il.M_Product_ID OR"
					+ " (umr.M_Product_ID = il.M_Product_ID AND umr.MovementQty <> il.QtyEntered)"
					+ " OR umr.M_Product_ID IS NULL OR umr.M_InOutLine_ID IS NULL)";
			boolean isMatching = DB.getSQLValue(po.get_TrxName(), sqql, po.get_ID(), po.get_ID()) == 0;
			if(isMatching)
			{
				//complete receipt confirm
				sql = "SELECT M_InOutConfirm_ID FROM M_InOutConfirm WHERE"
						+ " M_InOut_ID = ? AND DocStatus = 'DR'";
				int confirm_id = DB.getSQLValue(po.get_TrxName(), sql, po.get_ID());
				if(confirm_id <= 0)
					throw new AdempiereException("Cannot found Document Confirm");
				
				MInOutConfirm confirm = new MInOutConfirm(po.getCtx(), confirm_id, po.get_TrxName());
				
				confirm.setCompleteInOut(false);
				try {
					
					if(!confirm.processIt(MInOutConfirm.DOCACTION_Complete) && !confirm.save())
						throw new AdempiereException("Error when try to complete Receipt Confirm");
					
				} catch (Exception e) {
					e.printStackTrace();
					throw new AdempiereException(confirm.getProcessMsg());
				}
			}
					
		}
		
	}
	
//	private String createShipping(MMovement po)
//	{
//		MUNSShipping shipping = new MUNSShipping(po);
//		try
//		{
//			MAcctSchema[] schems = MAcctSchema.getClientAcctSchema(po.getCtx(), po.getAD_Client_ID(), po.get_TrxName());
//			shipping.setC_Currency_ID(schems[0].getC_Currency_ID());
//			shipping.saveEx();
//			po.setUNS_Shipping_ID(shipping.get_ID());
//			po.saveEx();
//		}
//		catch (Exception e)
//		{
//			return "Failed on create Shipping Document :: " + e.getMessage();
//		}
//		return null;
//	}

}
