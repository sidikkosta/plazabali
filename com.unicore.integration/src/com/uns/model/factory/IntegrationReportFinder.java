/**
 * 
 */
package com.uns.model.factory;

import java.io.InputStream;

import org.adempiere.base.IReportFinder;

/**
 * @author Burhani Adam
 *
 */
public class IntegrationReportFinder implements IReportFinder {

	/**
	 * 
	 */
	public IntegrationReportFinder() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public InputStream getReportAsStream(String name) {
		// TODO Auto-generated method stub
		return getClass().getClassLoader().getResourceAsStream(name);
	}
}
