/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;

/**
 * @author dpitaloka
 *
 */
public class ImportAdditionalStaff extends SvrProcess {

	private String fd;
	//private int recordId;
	//private MUNSSubregionRecap subregion;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() { 
		for ( ProcessInfoParameter para : getParameter()) {			
			if ( para.getParameterName().equals("File_Directory") )
				fd = (String) para.getParameter();
			else 
				log.info("Parameter not found " + para.getParameterName());
		}
		//recordId = getRecord_ID();
		//subregion = new MUNSSubregionRecap(getCtx(), recordId, get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{

		if(Util.isEmpty(fd, true))
			throw new AdempiereException("To run this process, you must fill the file with the correct directory / path.");
		
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportAdditionalStaff'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with ImportAdditionalStaff name");

		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(fd);
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length > 1)
			throw new AdempiereException("more than one lines in Import Simple XLS " + simple.getName());
		
		MUNSImportSimpleTable table = tables[0];
		
		SimpleImportXLS si = new SimpleImportXLS(getCtx(), simple, table, false, 0, 0, 0, getAD_Client_ID(), get_TrxName());
		String success = null;
		try{
			 success = si.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		return success;
	}

}
