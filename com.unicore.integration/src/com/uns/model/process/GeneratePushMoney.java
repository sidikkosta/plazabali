/**
 * 
 */
package com.uns.model.process;

import java.text.NumberFormat;
import java.util.logging.Level;

import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;


/**
 * @author azhaidar
 *
 */
public class GeneratePushMoney extends SvrProcess {
	
	private int 	m_AD_Org_ID;
	private int		m_C_Period_ID;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
				m_AD_Org_ID = params[i].getParameterAsInt();
			else if ("C_Period_ID".equals(name))
				m_C_Period_ID = params[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown parameter " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MPeriod period = MPeriod.get(getCtx(), m_C_Period_ID);
		
		String sql = "update uns_postrxline ptl"
				+ " set pushmoneyamt="
				+ "		(select (getpushmoneyamount(ptl.m_product_id, ptrx.dateacct, div.uns_division_id) * ptl.qtyentered)"
				+ "  	 from uns_postrx ptrx "
				+ "		 inner join uns_pos_session ps on ptrx.uns_pos_session_id=ps.uns_pos_session_id"
				+ "		 inner join uns_posterminal pterm on ps.uns_posterminal_id=pterm.uns_posterminal_id"
				+ "		 inner join uns_division div on div.storetype=pterm.storetype"
				+ "		 where ptrx.uns_postrx_id=ptl.uns_postrx_id) "
				+ " where ptl.uns_postrx_id in ("
				+ "		select pt.uns_postrx_id from uns_postrx pt"
				+ "	 	where pt.dateacct between " + DB.TO_DATE(period.getStartDate()) 
				+ " 		and " + DB.TO_DATE(period.getEndDate()) 
				+ " 		and pt.ad_org_id=" + m_AD_Org_ID
				+ ")";
		int count = DB.executeUpdate(sql, get_TrxName());
		
		return "Push-Money on Period " + period.getName() + " has been generated/updated:# " + NumberFormat.getInstance().format(count);
	}

} //CalculateSalesIncentive