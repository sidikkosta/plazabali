package com.uns.model.process;

import java.io.File;
import java.util.Properties;

import jxl.Sheet;
import jxl.Workbook;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSImportSimpleTable;

public class UNSProcessImportSimple extends SvrProcess {

	private String p_FileDir = null;
	private int p_startLineNo = 0;
	private int p_endLineNo = 0;
	private int p_ImportTable_ID = 0;
	private String m_trxName = null;
	private Properties m_ctx = null;
	
	public UNSProcessImportSimple() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			String name = param.getParameterName();
			if(name.equals("File_Dir"))
				p_FileDir = param.getParameterAsString();
			else if(name.equals("StartNo"))
				p_startLineNo = param.getParameterAsInt();
			else if(name.equals("EndNo"))
				p_endLineNo = param.getParameterAsInt();
			else if(name.equals("UNS_ImportSimpleTable_ID"))
				p_ImportTable_ID = param.getParameterAsInt();
		}
		
		m_trxName = get_TrxName();
		m_ctx = getCtx();

	}
	
	public UNSProcessImportSimple(Properties _ctx, String _FileDir, int _StratNo, int _EndNo,
			int _UNS_ImportSimpleTable_ID, String _trxName) {
		
		m_trxName = _trxName;
		m_ctx = _ctx;
		p_FileDir = _FileDir;
		p_startLineNo = _StratNo;
		p_endLineNo = _EndNo;
		p_ImportTable_ID = _UNS_ImportSimpleTable_ID;
	}

	@Override
	protected String doIt() throws Exception {
		
		if(p_ImportTable_ID <= 0)
			throw new AdempiereException("Mandatory Field Simple Import Table.");
		if(p_FileDir == null)
			throw new AdempiereException("No file");
			
		Workbook book = null;
		try {
			
			File file = new File(p_FileDir);
			if(!file.exists())
				throw new AdempiereException("Could not find file on path "+p_FileDir);
			
			MUNSImportSimpleTable table = new MUNSImportSimpleTable(m_ctx, p_ImportTable_ID, m_trxName);
			
			book = Workbook.getWorkbook(file);
			Sheet sheet = book.getSheet(table.getName());
			if(sheet == null)
			{
				sheet = book.getSheet("Sheet1");
				if(null == sheet)
					throw new AdempiereException("Worksheet doesn't exists");
			}
			
			SimpleImportXLS importer = new SimpleImportXLS(m_ctx, p_startLineNo, p_endLineNo, m_trxName);
			
			boolean success = importer.importSimpleTable(book, table);
			if(!success)
				return "Complete with errors";
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		
		return "Completed";
	}

}
