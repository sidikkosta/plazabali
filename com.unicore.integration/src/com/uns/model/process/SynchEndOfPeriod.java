package com.uns.model.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.Adempiere;
import org.compiere.model.MNonBusinessDay;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.uns.model.MUNSAttConfiguration;
import com.uns.model.MUNSCheckInOut;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSDailyPresence;
import com.uns.model.MUNSEmpStation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSLeavePermissionTrx;
import com.uns.model.MUNSMonthlyPresenceSummary;
import com.uns.model.MUNSMonthlyPresenceVal;
import com.uns.model.MUNSOTGroupRequest;
import com.uns.model.MUNSOTRequest;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPresenceVerification;
import com.uns.model.MUNSResource;
import com.uns.model.MUNSResourceWorkerLine;
import com.uns.model.MUNSSlotType;
import com.uns.model.MUNSWorkHoursConfig;
import com.uns.model.MUNSWorkHoursConfigLine;
import com.uns.model.utilities.HRMUtils;

public class SynchEndOfPeriod extends SvrProcess {
	
	private int p_AD_Org_ID = 0;
	private int p_SectionOfDept_ID = 0;
	private int p_SectionGroup_ID = 0;
	private int p_C_Period_ID = 0;
	private Timestamp p_DateFrom = null;
	private Timestamp p_DateTo = null;
	private boolean p_forceSynch = false;
//	private int p_Resource_ID = 0;
	private int p_Employee_ID = 0;
	private Timestamp m_startDate = null;
	private Timestamp m_endDate = null;
	private MUNSAttConfiguration m_AttConfig = null;
	private MUNSSlotType m_SlotType = null;
	private StringBuilder m_message = new StringBuilder();
	private String logDirectory;
//	private StringBuilder m_logUpdate = new StringBuilder();
	private IProcessUI m_process;
	private String msg = "";

	public SynchEndOfPeriod() 
	{
		super ();
	}

	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
			{
				p_AD_Org_ID = params[i].getParameterAsInt();
			}
			else if ("C_BPartner_ID".equals(name))
			{
				p_SectionOfDept_ID = params[i].getParameterAsInt();
			}
			else if ("C_Period_ID".equals(name))
			{
				p_C_Period_ID = params[i].getParameterAsInt();
			}
			else if("DateFrom".equals(name))
			{
				p_DateFrom = params[i].getParameterAsTimestamp();
			}
			else if("DateTo".equals(name))
			{
				p_DateTo = params[i].getParameterAsTimestamp();
			}
			else if("UNS_Employee_ID".equals(name))
			{
				p_Employee_ID = params[i].getParameterAsInt();
			}
//			else if("UNS_Resource_ID".equals(name))
//			{
//				p_Resource_ID = params[i].getParameterAsInt();
//			}
			else if("C_BP_Group_ID".equals(name))
			{
				p_SectionGroup_ID = params[i].getParameterAsInt();
			}
			else if("ForceSynchronize".equals(name))
			{
				p_forceSynch = params[i].getParameterAsBoolean();
			}
			else
			{
				log.log(Level.SEVERE, "Unknown parameter " + name);
			}
		}
				
	}

	@Override
	protected String doIt() throws Exception 
	{		
		m_process = Env.getProcessUI(getCtx());
		createMonthlyRecord();
		MUNSMonthlyPresenceSummary[] monthly = getMonthlyRecord();
		int length = monthly.length;
		int count = 1;
		for (int i=0; i<monthly.length; i++)
		{
			msg = "Data ke " + count + " dari " + length + " data.";
			m_process.statusUpdate(msg);
			processMonthlyRecord(monthly[i]);
			monthly[i].updateData(true);
			count++;
		}
		
		SynchValidateOffDayOT valOffDayOT = new SynchValidateOffDayOT(
				getCtx(), p_C_Period_ID, p_AD_Org_ID, 0, p_Employee_ID, get_TrxName());
		valOffDayOT.doIt();
		
		if(!m_message.toString().equals(""))
		{
			logDirectory = Adempiere.getAdempiereHome().trim();
			if (!logDirectory.endsWith("/") && !logDirectory.endsWith("\\"))
				logDirectory+= File.separator;
			logDirectory = logDirectory + "log" + File.separator;

			//create packout folder if needed
			File logDirectoryFile = new File(logDirectory);
			if (!logDirectoryFile.exists()) {
				boolean success = logDirectoryFile.mkdirs();
				if (!success) {
					log.warning("Failed to create target directory. " + logDirectory);
				}
			}
			
			Timestamp date = new Timestamp(System.currentTimeMillis());
			String dateProcess = date.toString().replace(":", "-");
			
			File newFile = new File(logDirectory + "SynchronizedEndOfPeriod " + dateProcess + ".txt");
			
			FileWriter fr = new FileWriter(newFile);

			BufferedWriter bw = new BufferedWriter(fr);
			
			for (String eachLine : m_message.toString().split("\n"))
			{
				bw.write(eachLine);
				bw.newLine();
			}
			bw.close();

			if(processUI != null)
				processUI.download(newFile);
			
			return "See log, file " + newFile;
		}
		
		return "Success";
	}
	
	@SuppressWarnings("unused")
	private void createMonthlyRecord()
	{
		analyzeStartEndDate();
		MPeriod p = MPeriod.get(getCtx(), p_C_Period_ID);
		String sql = "SELECT DISTINCT(emp.UNS_Employee_ID) FROM UNS_CheckInOut io"
				+ " INNER JOIN UNS_Employee emp ON emp.AttendanceName = io.AttendanceName"
				+ " INNER JOIN UNS_Contract_Recommendation cr "
				+ " ON cr.UNS_Employee_ID = emp.UNS_Employee_ID"
				+ " INNER JOIN UNS_Resource_WorkerLine rs ON rs.Labor_ID = emp.UNS_Employee_ID"
				+ " LEFT JOIN UNS_MonthlyPresenceSummary mp ON mp.UNS_Employee_ID = emp.UNS_Employee_ID AND EXISTS "
				+ " (SELECT 1 FROM C_Period p WHERE p.C_Period_ID = mp.C_Period_ID AND p.PeriodNo + 1 = ? AND p.C_Year_ID = ?)"
				+ " WHERE (CASE WHEN (CASE WHEN mp.C_Period_ID > 0 THEN mp.EndDate + INTERVAL '1 DAY' ELSE ? END)"
				+ " < cr.DateContractStart THEN ? >= cr.DateContractStart"
				+ " WHEN ? > cr.DateContractEnd THEN (CASE WHEN mp.C_Period_ID > 0 THEN mp.EndDate + INTERVAL '1 DAY' ELSE ? END)"
				+ " <= cr.DateContractEnd ELSE 1=1 END) AND emp.IsActive='Y'"
				+ " AND emp.IsBlacklist='N' AND emp.IsTerminate='N'"
				+ " AND emp.AD_Org_ID = ? AND io.CheckTime BETWEEN "
				+ " (CASE WHEN mp.C_Period_ID > 0 THEN mp.EndDate + INTERVAL '1 DAY' ELSE ? END) AND ?"
				+ " AND NOT EXISTS (SELECT 1 FROM UNS_MonthlyPresenceSummary mps"
				+ " WHERE mps.UNS_Employee_ID = emp.UNS_Employee_ID AND mps.C_Period_ID = ?"
				+ " AND mps.AD_Org_ID = ?)";
		
		if(p_Employee_ID > 0)
			sql += " AND emp.UNS_Employee_ID = " + p_Employee_ID;
		if(p_SectionOfDept_ID > 0)
			sql += " AND emp.C_BPartner_ID = " + p_SectionOfDept_ID;
		if(p_SectionGroup_ID > 0)
			sql += " AND EXISTS (SELECT 1 FROM C_BPartner bp WHERE bp.C_BP_Group_ID = " + p_SectionGroup_ID
				+ " AND bp.C_BPartner_ID = emp.C_BPartner_ID)";
//		if(p_Resource_ID > 0)
//			sql += " AND rs.UNS_Resource_ID = " + p_Resource_ID;
		
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, p.getPeriodNo());
			stmt.setInt(2, p.getC_Year_ID());
			stmt.setTimestamp(3, m_startDate);
			stmt.setTimestamp(4, m_endDate);
			stmt.setTimestamp(5, m_endDate);
			stmt.setTimestamp(6, m_startDate);
			stmt.setInt(7, p_AD_Org_ID);
			stmt.setTimestamp(8, m_startDate);
			stmt.setTimestamp(9, m_endDate);
			stmt.setInt(10, p_C_Period_ID);
			stmt.setInt(11, p_AD_Org_ID);
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				//check addendum contract
				MUNSEmployee employee = new MUNSEmployee(getCtx(), rs.getInt(1), get_TrxName());
				HashMap<Integer, MUNSContractRecommendation> mapContract = employee.getMapContract(true, m_startDate);
				int AddOrg_ID = -1;
				for(int i=0; i<mapContract.size(); i++)
				{
					MUNSContractRecommendation contract = mapContract.get(i);
					
					if(!contract.isSynchronized() || contract.getEffectiveDate().compareTo(m_endDate) > 0)
						continue;
					
					if((contract.getUNS_Contract_Evaluation_ID() > 0 && contract.getDocType().equals(MUNSContractRecommendation.DOCTYPE_Addendum))
							|| contract.getDocType().equals(MUNSContractRecommendation.DOCTYPE_Original))
					{
						if(i > 0)
							AddOrg_ID = mapContract.get(i-1).getAD_Org_ID();
						
						MUNSMonthlyPresenceSummary monthly = MUNSMonthlyPresenceSummary.getCreateByContract(
								getCtx(), contract, p_C_Period_ID, AddOrg_ID, null, null, get_TrxName());
					}
		
					if(contract.getEffectiveDate().compareTo(m_startDate) <= 0)
						break;
				}
				
			}
			
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
	}
	
	private void processMonthlyRecord (MUNSMonthlyPresenceSummary monthly)
	{
		
		Timestamp start = p_DateFrom != null && p_DateFrom.after(monthly.getStartDate())
							&& p_DateFrom.compareTo(monthly.getEndDate()) <= 0 ? p_DateFrom : monthly.getStartDate();
		Timestamp end = p_DateTo != null && p_DateTo.before(monthly.getEndDate())
						&& p_DateTo.compareTo(monthly.getStartDate()) >= 0 ? 
							p_DateTo : monthly.getEndDate();
						
		MUNSEmployee employee = MUNSEmployee.get(getCtx(), monthly.getUNS_Employee_ID());
		
		HashMap<Integer, MUNSContractRecommendation> mapContract = employee.getMapContract(true, start);
		MUNSContractRecommendation contract = null;
		
		while (!(start.after(end)))
		{
			MUNSMonthlyPresenceSummary uMonthly = monthly;
			boolean isDiffLegality = false;
			for(int i=0; i<mapContract.size(); i++)
			{
				Timestamp contractEffDate = mapContract.get(i).getEffectiveDate();
				if(start.compareTo(contractEffDate) >= 0 && mapContract.get(i).isSynchronized())
				{
					contract = mapContract.get(i);
					break;
				}
				else
				{
					String sqll = "SELECT 1 FROM UNS_Contract_Evaluation WHERE UNS_Contract_Evaluation_ID = ?"
							+ " AND Isdifferentlegality = 'Y'";
					isDiffLegality = DB.getSQLValue(
							get_TrxName(), sqll, mapContract.get(i).getUNS_Contract_Evaluation_ID()) != -1;
					continue;
				}
			}
			
			if(contract == null)
				throw new AdempiereException("No contract effective on this date. #"+start + " #Employee " + employee.getName());
			System.out.println(start + " - " + employee.getName());
			if(isDiffLegality)
			{
				uMonthly = MUNSMonthlyPresenceSummary.getCreateByContract(
						getCtx(), contract, p_C_Period_ID, contract.getPrevDept_ID(), null, null, get_TrxName());
			}

			boolean m_forceSynchronize = false;
//			if(!employee.getName().equals("Ria Refita"))
//			{
//				start = TimeUtil.addDays(start, 1); 
//				continue;
//			}
				
//			if(!start.equals(Timestamp.valueOf("2018-01-10 00:00:00.00")))
//			{
//				start = TimeUtil.addDays(start, 1); 
//				continue;
//			}
			
			String addMsg = " " + employee.getName() + " Tanggal " + start.toString().replace(" 00:00:00.0", "");
			m_process.statusUpdate(msg + addMsg);
			
			MUNSResource resource = MUNSResource.getByEmployee(getCtx(), 
					employee.get_ID(), start, employee.get_TrxName());
			if(resource == null)
			{
				if(m_message.toString().equals(""))
					m_message.append("Resource not found for " + employee.getName() + " on " + start.toString().substring(0, 10));
				else
					m_message.append("\nResource not found for " + employee.getName() + " on " + start.toString().substring(0, 10));
				
				start = TimeUtil.addDays(start, 1);
				continue;
			}
			
			m_AttConfig = MUNSAttConfiguration.get(
					getCtx(), start, contract.getAD_Org_ID(), 
					contract.getEmploymentType(), 
					contract.getNewSectionOfDept_ID(), 
					contract.getNewJob().getC_JobCategory_ID(), 
					get_TrxName());
			if(m_AttConfig == null)
				throw new AdempiereException("attendance configuration not found for " + employee.getName());
//			m_SlotType = MUNSSlotType.getByEmployee(getCtx(), employee.get_ID(), start, get_TrxName());
			m_SlotType = new MUNSSlotType(getCtx(), resource.getUNS_SlotType_ID(), get_TrxName());
			MUNSWorkHoursConfig adjustment = MUNSWorkHoursConfig.getByEmployee(getCtx(), 
					resource.get_ID(), employee.get_ID(), start, get_TrxName());
			MUNSWorkHoursConfigLine adjustLine = null;
			if(adjustment == null)
				adjustLine = MUNSWorkHoursConfigLine.get(getCtx(), employee.get_ID(), start, get_TrxName());
			if(adjustment != null && adjustment.getUNS_SlotType_ID() > 0)
				m_SlotType = new MUNSSlotType(getCtx(), adjustment.getUNS_SlotType_ID(), get_TrxName());
			else if(adjustLine != null)
			{
				if(adjustLine.getUNS_Resource_ID() > 0)
				{
					MUNSResource rs = MUNSResource.get(getCtx(), adjustLine.getUNS_Resource_ID());
					m_SlotType = new MUNSSlotType(getCtx(), rs.getUNS_SlotType_ID(), get_TrxName());
				}
			}
			
			MUNSLeavePermissionTrx leave = MUNSLeavePermissionTrx.get(getCtx(), employee.get_ID(), start, get_TrxName());

			//try get to new version
			MUNSOTRequest[] OTRequest = MUNSOTGroupRequest.getValidof(
					get_TrxName(), start, employee.get_ID());
			//if new version == null then use old version
			if(OTRequest.length == 0)
				OTRequest = MUNSOTRequest.getValidof(get_TrxName(), start, employee.get_ID());
//			m_logUpdate.append("\n").append("Day ").append(start);
//			m_process.statusUpdate(m_logUpdate.toString());
			MUNSDailyPresence daily = uMonthly.getDaily(start);
			if(daily == null)
			{
				daily = new MUNSDailyPresence(uMonthly);
				daily.setPresenceDate(start);
				daily.setIsNeedAdjustRule(true);
				daily.setC_BPartner_ID(contract.getNewSectionOfDept_ID());
			}
			else
			{
				daily.setC_BPartner_ID(contract.getNewSectionOfDept_ID());
				if(daily.getUNS_MonthlyPresenceVal_ID() > 0)
				{
					MUNSMonthlyPresenceVal val = new MUNSMonthlyPresenceVal(getCtx(), 
							daily.getUNS_MonthlyPresenceVal_ID(), get_TrxName());
					if(!val.getDocStatus().equals("CO")
							&& !val.getDocStatus().equals("CL"))
					{
//						daily.setFSTimeIn(null);
//						daily.setFSTimeOut(null);
						//NOTHING
					}
					else
					{
						if(OTRequest.length > 0)
							daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Lembur);
						
						if (MUNSDailyPresence.PRESENCESTATUS_Lembur.equals(daily.getPresenceStatus()))
						{
							String desc = daily.getDescription();
							if (desc == null)
								desc = "";
							desc = desc.replace("^_^", "");
							desc += "^_^";
							if(desc.equals(daily.getDescription()))
								desc = desc.replace("^_^", "");
							daily.setDescription(desc);
							if (!daily.save())
								throw new AdempiereException(CLogger.retrieveErrorString("Could not save daily"));
						}
						start = TimeUtil.addDays(start, 1);
						continue;
					}
				}
				else
				{
					MUNSPresenceVerification ver = new MUNSPresenceVerification(getCtx()
							, daily.getUNS_PresenceVerification_ID(), get_TrxName());
					if(ver.getUNS_PresenceVerification_ID() > 0 &&
							(ver.getDocStatus().equals("CO") || ver.getDocStatus().equals("CL")))
					{
						start = TimeUtil.addDays(start, 1);
						continue;
					}
					else
					{
						daily.setFSTimeIn(null);
						daily.setFSTimeOut(null);
					}
				}
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(start);
			boolean isWorkDay = true;
			boolean isNonBusinessDay = false;
			boolean isCuti = false;
			isNonBusinessDay = isNonBusinessDay(start, employee.getAD_Org_ID());

			if(adjustment != null)
				isWorkDay = m_SlotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
			else if(adjustLine != null)
			{
				if(adjustLine.getUNS_Resource_ID() > 0)
				{
					MUNSResource rs = MUNSResource.get(getCtx(), adjustLine.getUNS_Resource_ID());
					m_SlotType = ((MUNSSlotType) rs.getUNS_SlotType());
					isWorkDay = m_SlotType.isOperationalShop() ? true 
							: m_SlotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK)); 
					if(isNonBusinessDay && isWorkDay)
						m_forceSynchronize = true;
				}
				else
					isWorkDay = false;
			}
			else
			{
				MUNSResourceWorkerLine wl = MUNSResourceWorkerLine.get(getCtx(), employee.get_ID(), start, get_TrxName());
				if(wl == null)
					throw new AdempiereException("Employee resource configuration not found. " + employee.getName());
				if(m_SlotType.isOperationalShop())
				{	
					isWorkDay = (wl.getDailyScheduleType() == null);
					if(isWorkDay && !m_SlotType.isOTOnNasionalHoliday())
					{
						isNonBusinessDay = false;
					}
					if(!isWorkDay)
					{	
						if(wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_LiburNasional))
							isNonBusinessDay = true;
						else if(wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_CutiHamil)
								|| wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_CutiPanjang)
								|| wl.getDailyScheduleType().equals(MUNSResourceWorkerLine.DAILYSCHEDULETYPE_CutiTahunan))
							isCuti = true;
					}	
				}
				else
				{
					isWorkDay = m_SlotType.IsWorkDay(cal.get(Calendar.DAY_OF_WEEK));
				}
			}
			
			if(daily.isNeedAdjustRule() || p_forceSynch)
			{
				Calendar calIn = Calendar.getInstance();
				Calendar calOut = Calendar.getInstance();
				calIn.setTime(start);
				calOut.setTime(start);
				if(adjustment != null)
				{
					if(adjustment.getUNS_SlotType_ID() > 0)
					{
						m_SlotType = new MUNSSlotType(getCtx(), adjustment.getUNS_SlotType_ID(), get_TrxName());
						calIn.setTime(m_SlotType.getTimeSlotStart());
						calIn.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
						calOut.setTime(m_SlotType.getTimeSlotEnd());
						calOut.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
					}
					else
					{
						calIn.setTime(adjustment.getStartTime());
						calIn.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
						calOut.setTime(adjustment.getEndTime());
						calOut.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
					}
				}
				else
				{
					calIn.setTime(m_SlotType.getTimeSlotStart());
					calIn.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
					calOut.setTime(m_SlotType.getTimeSlotEnd());
					calOut.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
				}
				
				if(calIn.get(Calendar.HOUR_OF_DAY) >= calOut.get(Calendar.HOUR_OF_DAY))
					calOut.add(Calendar.DATE, 1);

				if(OTRequest.length > 0)
				{
					m_forceSynchronize = true;
					if(isWorkDay && !isNonBusinessDay)
					{
						for(int i=0;i<OTRequest.length;i++)
						{
							if(calIn.getTimeInMillis() >= OTRequest[i].getEndTime().getTime())
								calIn.setTimeInMillis(OTRequest[i].getStartTime().getTime());
							else if(calOut.getTimeInMillis() >= OTRequest[i].getStartTime().getTime())
								calOut.setTimeInMillis(OTRequest[i].getEndTime().getTime());
						}
					}
					else
					{
						for(int i=0;i<OTRequest.length;i++)
						{
							if(i==0)
							{
								calIn.setTimeInMillis(OTRequest[i].getStartTime().getTime());
								calOut.setTimeInMillis(OTRequest[i].getEndTime().getTime());
							}
							else
							{
								if(calIn.getTimeInMillis() >= OTRequest[i].getEndTime().getTime())
									calIn.setTimeInMillis(OTRequest[i].getStartTime().getTime());
								if(calOut.getTimeInMillis() >= OTRequest[i].getStartTime().getTime())
									calOut.setTimeInMillis(OTRequest[i].getEndTime().getTime());
							}
						}
					}
				}
				
				daily.setTimeInRules(new Timestamp(calIn.getTimeInMillis()));
				daily.setTimeOutRules(new Timestamp(calOut.getTimeInMillis()));
				
				int minLateFSIn = m_AttConfig.getMaxEarlierFSIn();
				int maxLateFSOut = m_AttConfig.getMaxLateFSOut();
				
				if(minLateFSIn > 0)
					calIn.add(Calendar.MINUTE, -minLateFSIn);
				if(maxLateFSOut > 0)
					calOut.add(Calendar.MINUTE, maxLateFSOut);
				
				daily.setMinTimeInRule(new Timestamp(calIn.getTimeInMillis()));
				daily.setMaxTimeOutRule(new Timestamp(calOut.getTimeInMillis()));
				daily.setIsNeedAdjustRule(false);
			}
			
			if(isNonBusinessDay && !m_forceSynchronize)
			{
				daily.setFSTimeIn(null);
				daily.setFSTimeOut(null);
				daily.setDayType(MUNSDailyPresence.DAYTYPE_HariLiburNasional);
				daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Libur);
				daily.setRunValidator(false);
				daily.setNeedUpHeader(false);
				daily.saveEx();
				start = TimeUtil.addDays(start, 1);
				continue;
			}
			else if(isWorkDay && isNonBusinessDay && m_SlotType.isOTOnNasionalHoliday() && m_forceSynchronize)
			{
				daily.setDayType(MUNSDailyPresence.DAYTYPE_HariLiburNasional);
				daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Lembur);
			}
			else if(!isWorkDay && m_forceSynchronize && OTRequest != null)
			{
				daily.setDayType(MUNSDailyPresence.DAYTYPE_HariLiburMingguan);
				daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Lembur);
			}
			else if(!isWorkDay && !isCuti)
			{
				daily.setFSTimeIn(null);
				daily.setFSTimeOut(null);
				daily.setDayType(MUNSDailyPresence.DAYTYPE_HariLiburMingguan);
				daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Libur);
				daily.setRunValidator(false);
				daily.setNeedUpHeader(false);
				daily.saveEx();
				start = TimeUtil.addDays(start, 1);
				continue;
			}
			else if(leave == null || m_forceSynchronize)
			{
				daily.setDayType(MUNSDailyPresence.DAYTYPE_HariKerjaBiasa);
				daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Mangkir);
			}
			else if(leave != null)
			{
				daily.setDayType(MUNSDailyPresence.DAYTYPE_HariKerjaBiasa);
				daily.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Izin);
				daily.setPermissionType(leave.getLeaveType());
			}
			
			start = TimeUtil.addDays(start, 1);
			
			if(!isWorkDay && OTRequest == null)
				continue;
				
			if(Util.isEmpty(employee.getAttendanceName(), true))
				continue;
			InOutToDaily(getInOuts(daily.getMinTimeInRule(), daily.getMaxTimeOutRule(),
					employee.getAttendanceName()), daily, employee, leave);
		}
	}
	
	public MUNSSlotType getWorkerShift (MUNSEmployee employee)
	{
		return MUNSResource.getSlotType(employee)[0];
	}

	public MUNSSlotType getShift (MUNSEmployee employee)
	{
		if (employee.getEmploymentType().equals("SUB"))
		{
			return getWorkerShift(employee);
		}
		
		int shift = MUNSEmpStation.getEmployeeSlotType_ID(
				get_TrxName(), employee.get_ID());
		if (shift <= 0)
		{
			return null;
		}
		
		MUNSSlotType slotType = new MUNSSlotType(
				getCtx(), shift, get_TrxName());
		return slotType;
	}
	
	
	private boolean isNonBusinessDay(Timestamp timestamp, int AD_Org_ID) {
		return MNonBusinessDay.isNonBusinessDay(getCtx(), timestamp, AD_Org_ID, get_TrxName());
	}

	private MUNSMonthlyPresenceSummary[] getMonthlyRecord ()
	{
		List<Object> params = new ArrayList<>();
		String whereClause = "C_Period_ID = ? ";
		params.add(p_C_Period_ID);
		if (p_AD_Org_ID > 0)
		{
			whereClause += " AND AD_Org_ID = ?";
			params.add(p_AD_Org_ID);
		}
		if (p_SectionOfDept_ID > 0)
		{
			whereClause += " AND C_BPartner_ID = ?";
			params.add(p_SectionOfDept_ID);
		}
		if(p_SectionGroup_ID > 0)
		{
			whereClause += " AND EXISTS (SELECT 1 FROM C_BPartner bp WHERE bp.C_BP_Group_ID ="
					+ " ? AND bp.C_BPartner_ID = UNS_MonthlyPresenceSummary.C_BPartner_ID)";
			params.add(p_SectionGroup_ID);
		}

		if(p_Employee_ID > 0)
		{
			whereClause += " AND UNS_Employee_ID = ?";
			params.add(p_Employee_ID);
		}
		
		whereClause += " AND DocStatus NOT IN ('VO', 'RE', 'CL', 'CO')";
		
		MUNSMonthlyPresenceSummary[] monthly = MUNSMonthlyPresenceSummary.get(
				getCtx(), whereClause, params, null, get_TrxName());
		
		
		return monthly;
	}

	private MUNSCheckInOut[] getInOuts(Timestamp dateFrom, Timestamp dateTo, String attName)
	{
		List<Object> params = new ArrayList<>();
		dateFrom = TimeUtil.addDays(dateFrom, -1);
		dateTo = TimeUtil.addDays(dateTo, 1);
		params.add(dateFrom);
		params.add(dateTo);
		
		String whereClause = "CheckTime BETWEEN ? AND ?";
		if(!Util.isEmpty(attName, true))
		{
			whereClause += " AND AttendanceName = ?";
			params.add(attName);
		}
		
		MUNSCheckInOut[] ios = MUNSCheckInOut.gets(getCtx(), get_TrxName(), whereClause, params, "CheckTime ASC");
		
		return ios;
	}
	
	private String InOutToDaily(MUNSCheckInOut[] ios, MUNSDailyPresence day, MUNSEmployee emp, MUNSLeavePermissionTrx leave)
	{
		String msg = null;
		
		for(int i = 0; i < ios.length; i++)
		{
			MUNSCheckInOut io = ios[i];
			day.setCheckInOut(io);
			day.setRunValidator(true);
			day.setNeedUpHeader(false);
			day.setAddWorkHours(i + 1);
			day.saveEx();
			io.setNeedRunUNSHRMValidator(false);
			io.setUNS_DailyPresence_ID(day.get_ID());
			io.saveEx();
//			HRMUtils util = new HRMUtils(emp, io.getCheckTime(), day, m_AttConfig, leave);
//			
//			if(util.getPresenceDate() == null)
//				continue;
//			
//			linkedToDaily(emp, util, io, day);
		}
		if(ios.length == 0)
			day.saveEx();
		
		day.load(get_TrxName());
		day.setCheckInOut(null);
		day.setRunValidator(true);
//		day.setNeedUpHeader(true);
		day.setAddWorkHours(999);
		day.saveEx();
		
		return msg;
	}
	
	/**
	 * 
	 * @param presenceDate
	 * @param employee
	 * @param util
	 * @param inOut
	 * @return
	 */
	@SuppressWarnings("unused")
	private String linkedToDaily (MUNSEmployee employee, 
			HRMUtils util, MUNSCheckInOut inOut, MUNSDailyPresence presence)
	{
		Timestamp presenceDate = util.getPresenceDate();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(presenceDate.getTime());
//		String presenceStatus = MUNSDailyPresence.PRESENCESTATUS_FullDay;
//		String dayType = MUNSDailyPresence.DAYTYPE_HariKerjaBiasa;
//		if (util.isNationalHoliday())
//		{	
//			dayType = MUNSDailyPresence.DAYTYPE_HariLiburNasional;
//			presenceStatus = presence.getPresenceStatus();
//		}
//		else if (util.isWeeklyHoliday())
//		{
//			dayType = MUNSDailyPresence.DAYTYPE_HariLiburMingguan;
//			presenceStatus = presence.getPresenceStatus();
//		}
//		if ((util.isOverTime() || util.isOTOnHoliday()) && (util.isWeeklyHoliday() 
//				|| util.isNationalHoliday()))
//		{
//			presenceStatus = MUNSDailyPresence.PRESENCESTATUS_Lembur;
//		}
//		
//		if(dayType.equals(MUNSDailyPresence.DAYTYPE_HariKerjaBiasa)
//				&& presence.getPermissionType() != null)
//		{
//			presenceStatus = presence.getPresenceStatus();
//		}
		
		try
		{	
			Timestamp timeIn = presence.getTimeInRules() == null ? util.getTimeInRules() : presence.getTimeInRules();
			Timestamp maxFSIn = util.getMaxLateTimeInRules();
			Timestamp timeOut = presence.getTimeOutRules() == null ? util.getTimeOutRules() : presence.getTimeOutRules();
			Timestamp minFSOut = util.getMaxEarlierTimeOutRules();
			long maxStart = maxFSIn.getTime();
			long minEnd = minFSOut.getTime();
			long check = util.getDate().getTime();
			
			if (maxStart >= check)
			{
				Timestamp lastIn = presence.getFSTimeIn();
				if (null == lastIn)
				{
					lastIn = util.getDate();
				}
				else if (timeIn.before(lastIn))
				{
					lastIn = util.getDate();
				}
				
				presence.setFSTimeIn(lastIn);
			}
			if (minEnd <= check)
			{
				Timestamp lastOut = presence.getFSTimeOut();
				if (null == lastOut)
				{
					lastOut = util.getDate();
				}
				else if (util.getDate().after(lastOut))
				{
					lastOut = util.getDate();
				}
				
				presence.setFSTimeOut(lastOut);
			}
			
			presence.setTimeInRules(timeIn);
			presence.setTimeOutRules(timeOut);
//			presence.setPresenceStatus(presenceStatus);
//			presence.setDayType(dayType);
			presence.setDay(util.getDay());
			if (inOut.getAD_Org_ID() != presence.getAD_Org_ID())
			{
				inOut.setAD_Org_ID(presence.getAD_Org_ID());
			}
//			presence.setWorkHours(BigDecimal.valueOf(util.getMaxWorkHours()));
//			presence.setAddWorkHours((int) util.getAddWorkHours());
			BigDecimal ot = (BigDecimal.valueOf(util.getMaxOTHours())).subtract(BigDecimal.valueOf(util.getBreakTimeOnOT()));
			presence.setOvertime(ot);
		
//			if(!presenceStatus.equals(MUNSDailyPresence.PRESENCESTATUS_Lembur)
//					&& (util.isWeeklyHoliday() 
//							|| util.isNationalHoliday()))
//			{
//				presence.setFSTimeIn(null);
//				presence.setFSTimeOut(null);
//			}
			
			presence.setRunValidator(true);
			presence.saveEx();
			inOut.setNeedRunUNSHRMValidator(false);
			inOut.setUNS_DailyPresence_ID(presence.get_ID());
			inOut.saveEx();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		
		return null;
	}
	
	private void analyzeStartEndDate()
	{
		MPeriod period = MPeriod.get(getCtx(), p_C_Period_ID);
		Calendar cal = TimeUtil.getCalendar(period.getStartDate());
		
		MUNSPayrollConfiguration config =
				MUNSPayrollConfiguration.get(
						getCtx(), period, 0, get_TrxName(), true);
		
		if (null == config)
		{
			throw new AdempiereException(
					"Can't find payroll configuration.");
		}
		
		int startConfig = config.getPayrollDateStart();
		int endConfig = config.getPayrollDateEnd();
		
		cal.set(Calendar.DATE, startConfig);
		
		int daysOfMonth = cal.getActualMaximum(Calendar.DATE);
		int median = daysOfMonth / 2;
		
		if (startConfig > median)
		{
			cal.add(Calendar.MONTH, -1);
		}
		
		m_startDate = new Timestamp(cal.getTimeInMillis());
		
		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DATE, endConfig);
		
		m_endDate = new Timestamp(cal.getTimeInMillis());
	}
}