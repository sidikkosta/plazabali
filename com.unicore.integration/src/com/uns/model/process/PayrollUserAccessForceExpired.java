/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSUserAccess;

/**
 * @author nurse
 *
 */
public class PayrollUserAccessForceExpired extends SvrProcess 
{

	/**
	 * 
	 */
	public PayrollUserAccessForceExpired() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		//Do nothing
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSUserAccess access = new MUNSUserAccess(getCtx(), getRecord_ID(), get_TrxName());
		access.setIsExpired(true);
		if (!access.save())
			throw new AdempiereException();
		return "Success";
	}

}
