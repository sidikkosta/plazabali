/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSDailyPresence;
import com.uns.model.MUNSMonthlyPresenceSummary;
import com.uns.model.X_UNS_DailyPresence;

/**
 * @author Burhani Adam
 *
 */
public class SynchValidateOffDayOT extends SvrProcess{

	private int p_AD_Org_ID = 0;
	private int p_C_Period_ID = 0;
	private int p_Resource_ID = 0;
	private int p_Employee_ID = 0;
	private Properties m_ctx;
	private String m_trxName;
	
	/**
	 * 
	 */
	public SynchValidateOffDayOT() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
			{
				p_AD_Org_ID = params[i].getParameterAsInt();
			}
			else if ("C_Period_ID".equals(name))
			{
				p_C_Period_ID = params[i].getParameterAsInt();
			}
			else if("UNS_Employee_ID".equals(name))
			{
				p_Employee_ID = params[i].getParameterAsInt();
			}
			else if("UNS_Resource_ID".equals(name))
			{
				p_Resource_ID = params[i].getParameterAsInt();
			}
			else
			{
				log.log(Level.SEVERE, "Unknown parameter " + name);
			}
		}
		m_ctx = getCtx();
		m_trxName = get_TrxName();
	}
	
	public SynchValidateOffDayOT(Properties ctx, int C_Period_ID, int AD_Org_ID,
			int UNS_Resource_ID, int UNS_Employee_ID, String trxName)
	{
		m_ctx = ctx;
		m_trxName = trxName;
		p_C_Period_ID = C_Period_ID;
		p_AD_Org_ID = AD_Org_ID;
		p_Resource_ID = UNS_Resource_ID;
		p_Employee_ID = UNS_Employee_ID;
	}

	@Override
	protected String doIt() throws Exception
	{
		String sql = "SELECT dp.* FROM UNS_DailyPresence dp"
				+ " INNER JOIN UNS_MonthlyPresenceSummary mps ON"
				+ " mps.UNS_MonthlyPresenceSummary_ID = dp.UNS_MonthlyPresenceSummary_ID"
				+ " INNER JOIN UNS_Employee emp ON emp.UNS_Employee_ID = mps.UNS_Employee_ID"
				+ " INNER JOIN UNS_PayrollLevel_Config plc ON plc.UNS_PayrollLevel_Config_ID ="
					+ " getpayrollLevelConf(emp.payrollLevel, emp.payrollTerm, CAST(emp.AD_Org_ID AS integer), 'Y')"
				+ " WHERE mps.DocStatus NOT IN ('VO', 'RE', 'CL', 'CO') AND"
				+ " mps.C_Period_ID = ? AND mps.AD_Org_ID = ? AND dp.DayType = 'HL'"
				+ " AND dp.PresenceStatus = 'LMR' AND plc.ValidateAbnormalOffDayOT = 'Y'";
		
		if(p_Resource_ID > 0)
			sql += " AND EXISTS (SELECT 1 FROM UNS_Resource_WorkerLine wl"
					+ " WHERE wl.IsActive = 'Y' AND wl.UNS_Resource_ID = " + p_Resource_ID
					+ " AND wl.Labor_ID = mps.UNS_Employee_ID)";
		if(p_Employee_ID > 0)
			sql += " AND mps.UNS_Employee_ID = " + p_Employee_ID;
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, m_trxName);
			stmt.setInt(1, p_C_Period_ID);
			stmt.setInt(2, p_AD_Org_ID);
			rs = stmt.executeQuery();
			while(rs.next())
			{
				executeDay(new MUNSDailyPresence(m_ctx, rs, m_trxName));
			}
		}
		catch(SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		upPresenceSummary();
		
		return "Success";
	}
	
	private void executeDay(MUNSDailyPresence daily)
	{		
		boolean setToZERO = true;
		int employee = daily.getUNS_MonthlyPresenceSummary().getUNS_Employee_ID();
		Calendar cal = Calendar.getInstance();
		cal.setTime(daily.getPresenceDate());
		for(int i=1;i<7;i++)
		{
			cal.add(Calendar.DATE, -1);
			MUNSDailyPresence day = MUNSDailyPresence.getByDate(m_ctx, new Timestamp(cal.getTimeInMillis()),
					employee, m_trxName);
			if(day == null)
				continue;
			if(day.getPresenceStatus().equals(X_UNS_DailyPresence.PRESENCESTATUS_Mangkir)
					|| (day.getPermissionType() != null && 
					day.getPermissionType().equals(X_UNS_DailyPresence.PERMISSIONTYPE_PayPermissionIzinPotongGaji)))
			{
				daily.setLD1R(daily.getLD1());
				daily.setLD2R(daily.getLD2());
				daily.setLD3R(daily.getLD3());
				daily.setLD1(Env.ZERO);
				daily.setLD2(Env.ZERO);
				daily.setLD3(Env.ZERO);
				daily.setRunValidator(false);
				daily.setIsManualUpdate(false);
				daily.saveEx();
				setToZERO = false;
				break;
			}
		}
		
		if(setToZERO)
		{
			daily.setLD1R(Env.ZERO);
			daily.setLD2R(Env.ZERO);
			daily.setLD3R(Env.ZERO);
			daily.saveEx();
		}
	}
	
	private void upPresenceSummary() throws Exception
	{
		String sql = "SELECT mps.* FROM UNS_MonthlyPresenceSummary mps"
				+ " INNER JOIN UNS_Employee emp ON emp.UNS_Employee_ID = mps.UNS_Employee_ID"
				+ " INNER JOIN UNS_PayrollLevel_Config plc ON plc.UNS_PayrollLevel_Config_ID ="
					+ " getpayrollLevelConf(emp.payrollLevel, emp.payrollTerm, CAST(emp.AD_Org_ID AS integer), 'Y')"
				+ " WHERE mps.DocStatus NOT IN ('VO', 'RE', 'CL', 'CO') AND"
				+ " mps.C_Period_ID = ? AND mps.AD_Org_ID = ? AND plc.ValidateAbnormalOffDayOT = 'Y'";
		
		if(p_Resource_ID > 0)
			sql += " AND EXISTS (SELECT 1 FROM UNS_Resource_WorkerLine wl"
					+ " WHERE wl.IsActive = 'Y' AND wl.UNS_Resource_ID = " + p_Resource_ID
					+ " AND wl.Labor_ID = mps.UNS_Employee_ID)";
		if(p_Employee_ID > 0)
			sql += " AND mps.UNS_Employee_ID = " + p_Employee_ID;
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, m_trxName);
			stmt.setInt(1, p_C_Period_ID);
			stmt.setInt(2, p_AD_Org_ID);
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSMonthlyPresenceSummary mps = new MUNSMonthlyPresenceSummary(m_ctx, rs, m_trxName);
				mps.setisValidatedOffDayOT(true);
				mps.saveEx();
			}
		}
		catch(SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
	}
}