/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInOutLineMA;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInventoryLineMA;
import org.compiere.model.MMovementLineMA;
import org.compiere.model.MTransaction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MMovementLine;
import com.unicore.base.model.MOrderLine;
import com.unicore.base.model.MRequisitionLine;
import com.unicore.model.MUNSPOSRecapLineMA;
import com.unicore.model.MUNSPOSTrxLineMA;
import com.unicore.model.MUNSRequisitionLineMA;

/**
 * @author Burhani Adam
 *
 */
public class UNSMergeASIByDesc extends SvrProcess {

	private final String[] m_ListTableNeedMerge = {
		MInventoryLineMA.Table_Name,
		MMovementLineMA.Table_Name,
		MUNSPOSRecapLineMA.Table_Name,
		MUNSPOSTrxLineMA.Table_Name,
		MInOutLineMA.Table_Name,
		MUNSRequisitionLineMA.Table_Name,
//		MUNSPLConfirmMA.Table_Name,
//		MUNSProductionDetailMA.Table_Name
	};
	
	private final String[] m_ListTableNeedUpdate = {
			MTransaction.Table_Name,
			MMovementLine.Table_Name,
			MInOutLine.Table_Name,
			MInventoryLine.Table_Name,
			MOrderLine.Table_Name
	};
	/**
	 * 
	 */
	public UNSMergeASIByDesc() {
		// TODO Auto-generated constructor stub
	}
	
	private boolean p_clearStorage = false;
	private IProcessUI m_process;
	private String msg = null;
	private String m_trxName = null;
	private String m_description = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("ClearStorage"))
				p_clearStorage = param.getParameterAsBoolean();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{	
		m_process = Env.getProcessUI(getCtx());
		m_trxName = get_TrxName();
		m_description = "Start :: " + new Timestamp (System.currentTimeMillis());
		if(p_clearStorage)
			reinitStorage();
		else
			updateDateMaterialPolicy();
		
		m_description += ", Finish :: " + new Timestamp (System.currentTimeMillis());
		
		String mark = "UPDATE AD_User SET Description = ? WHERE AD_User_ID = ?";
		DB.executeUpdate(mark, new Object[]{m_description, 1001075}, false, get_TrxName());
		
		return m_description;
	}
	
	@SuppressWarnings("unused")
	private String fixingDuplicateASI()
	{
		String sql = "SELECT COUNT(*) FROM (SELECT Description as Desc FROM M_AttributeSetInstance "
				+ " WHERE IsActive = 'Y'"
				+ " GROUP BY Description HAVING COUNT(*) > 1) AS Master";
		int count = DB.getSQLValue(get_TrxName(), sql);
		sql = "SELECT Description, COUNT(*) FROM M_AttributeSetInstance"
				+ " WHERE IsActive = 'Y'"
				+ " GROUP BY Description"
				+ " HAVING COUNT(*) > 1"
				+ " ORDER BY COUNT(*) ASC";
		List<List<Object>> oo = DB.getSQLArrayObjectsEx(get_TrxName(), sql);
		if(oo == null)
			return "";
		for(int j=0;j<oo.size();j++)
		{
			String descASI = oo.get(j).get(0).toString();
			if(descASI.equals("0"))
				continue;
			int keepASIID = 0;
			String sql1 = "SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance"
					+ " WHERE Description = ? AND DATE_TRUNC('Day', Created) = Created"
					+ " AND IsActive = 'Y'"
					+ " ORDER BY Created ASC";
			keepASIID = DB.getSQLValue(m_trxName, sql1, descASI);
			if(keepASIID <= 0)
			{
				sql1 = "SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance"
						+ " WHERE Description = ?"
						+ " AND IsActive = 'Y'"
						+ " ORDER BY Created ASC";
				keepASIID = DB.getSQLValue(m_trxName, sql1, descASI);
				
				sql1 = "UPDATE M_AttributeSetInstance SET Created = DATE_TRUNC('Day', Created)"
						+ " WHERE M_AttributeSetInstance_ID = ?";
				DB.executeUpdate(sql1, keepASIID, m_trxName);
			}
			
			String[] values = getASI(descASI);
			
			for(int i=0;i<values.length;i++)
			{
				int asiID = new Integer (values[i]);
				msg = count + " data. Data ke-" + (j+1) + " (" + descASI +"). " + Integer.parseInt(oo.get(j).get(1).toString()) 
						+ " duplikat, duplikat ke - " + (i+1) + ".";
				if(asiID == keepASIID)
					continue;
				mergeRecord(asiID, keepASIID);
				updateRecord(asiID, keepASIID);
				MAttributeSetInstance asi = new MAttributeSetInstance(getCtx(), asiID, m_trxName);
				asi.setIsActive(false);
				asi.saveEx();
//				String tmpTxt = asi.get_ID() + ";" + asi.getDescription();
//				try {
//					bw.write(tmpTxt);
//					bw.newLine();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}
		}
//		String newFName = "D:/tmpDeletedASI_" + System.currentTimeMillis() + ".txt";
//		File newFile = new File(newFName);
		
//		FileWriter fr = null;
//		try {
//			fr = new FileWriter(newFile);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//		BufferedWriter bw = new BufferedWriter(fr);

//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try
//		{
//			stmt = DB.prepareStatement(sql, get_TrxName());
//			rs = stmt.executeQuery();
//			while(rs.next())
//			{
//				m_trxName = Trx.createTrxName();
//				if(rs.getString(1).equals("0"))
//					continue;
//				int keepASIID = 0;
//				String sql1 = "SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance"
//						+ " WHERE Description = ? AND DATE_TRUNC('Day', Created) = Created"
//						+ " AND IsActive = 'Y'"
//						+ " ORDER BY Created ASC";
//				keepASIID = DB.getSQLValue(m_trxName, sql1, rs.getString(1));
//				if(keepASIID <= 0)
//				{
//					sql1 = "SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance"
//							+ " WHERE Description = ?"
//							+ " AND IsActive = 'Y'"
//							+ " ORDER BY Created ASC";
//					keepASIID = DB.getSQLValue(m_trxName, sql1, rs.getString(1));
//					
//					sql1 = "UPDATE M_AttributeSetInstance SET Created = DATE_TRUNC('Day', Created)"
//							+ " WHERE M_AttributeSetInstance_ID = ?";
//					DB.executeUpdate(sql1, keepASIID, m_trxName);
//				}
//				
//				String[] values = getASI(rs.getString(1));
//				
//				for(int i=0;i<values.length;i++)
//				{
//					int asiID = new Integer (values[i]);
//					msg = count + " data. Data ke-" + rs.getRow() + " (" + rs.getString(1) +"). " + rs.getInt(2) 
//							+ " duplikat, duplikat ke - " + (i+1) + ".";
//					if(asiID == keepASIID)
//						continue;
//					mergeRecord(asiID, keepASIID);
//					updateRecord(asiID, keepASIID);
//					MAttributeSetInstance asi = new MAttributeSetInstance(getCtx(), asiID, m_trxName);
//					asi.setIsActive(false);
//					asi.saveEx();
//					String tmpTxt = asi.get_ID() + ";" + asi.getDescription();
//					try {
//						bw.write(tmpTxt);
//						bw.newLine();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//				
//				Trx trx = Trx.get(m_trxName, false);
//				trx.commit();
//			}
//		}
//		catch (SQLException e)
//		{
//			throw new AdempiereException(e.getMessage());
//		}
//		finally
//		{
//			DB.close(rs, stmt);
//		}
//		try {
//			bw.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		return "Success";
	}
	
	private String[] getASI(String description)
	{
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(M_AttributeSetInstance_ID),';')"
				+ " FROM M_AttributeSetInstance WHERE Description = ?";
		String value = DB.getSQLValueString(get_TrxName(), sql, description);
		String[] values = value.split(";");
		
		return values;
	}
	
	private void mergeRecord(int fromASIID, int toASIID)
	{
		for(int i=0;i<m_ListTableNeedMerge.length;i++)
		{
			String parentTable = m_ListTableNeedMerge[i].substring(0, m_ListTableNeedMerge[i].length() - 2);
			if(parentTable.endsWith("_"))
				parentTable = parentTable.substring(0, parentTable.length() - 1);
			if(m_ListTableNeedMerge[i].equals(MUNSRequisitionLineMA.Table_Name))
				parentTable = MRequisitionLine.Table_Name;
//			String sql = "SELECT * FROM " + m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?";
			String columnQty = m_ListTableNeedMerge[i].equals("UNS_POSRecapLine_MA") ?
					"Qty" : "MovementQty";
			String sql = "SELECT " + parentTable + "_ID, " + columnQty + " FROM " 
							+ m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?";
			List<List<Object>> oo = DB.getSQLArrayObjectsEx(m_trxName, sql, fromASIID);
			if(oo == null)
				continue;
			for(int j=0;j<oo.size();j++)
			{
				String subMsg = msg + " Merge record " + m_ListTableNeedMerge[i] + ". " + oo.size() + " record, record ke " + (j+1);
				m_process.statusUpdate(subMsg);
				sql = "SELECT COUNT(*) FROM " + m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?"
						+ " AND " + parentTable + "_ID = ?";
				int count = DB.getSQLValue(m_trxName, sql, toASIID, Integer.parseInt(oo.get(j).get(0).toString()));
				if(count > 0)
				{
					BigDecimal qty = (BigDecimal) oo.get(j).get(1); 
					sql = "UPDATE " + m_ListTableNeedMerge[i] + " SET " + columnQty + " = " + columnQty + " + ?"
							+ " WHERE M_AttributeSetInstance_ID = ? AND " + parentTable + "_ID = ?";
					DB.executeUpdate(sql, new Object[]{qty, toASIID, Integer.parseInt(oo.get(j).get(0).toString())}, false, m_trxName);
					sql = "DELETE FROM " + m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?"
							+ " AND " + parentTable + "_ID=?";
					DB.executeUpdate(sql, new Object[]{fromASIID, Integer.parseInt(oo.get(j).get(0).toString())}, false, m_trxName);
				}
				else
				{
					sql = "UPDATE " + m_ListTableNeedMerge[i] + " SET M_AttributeSetInstance_ID = ?"
							+ " WHERE " + parentTable + "_ID=? AND M_AttributeSetInstance_ID = ?";
					DB.executeUpdate(sql, new Object[]{toASIID, Integer.parseInt(oo.get(j).get(0).toString()), fromASIID},
							false, m_trxName);
				}
			}
//			ResultSet rs = null;
//			PreparedStatement stmt = null;
//			try {
//				stmt = DB.prepareStatement(sql, m_trxName);
//				stmt.setInt(1, fromASIID);
//				rs = stmt.executeQuery();
//				while(rs.next())
//				{
//					String subMsg = msg + " Merge record " + m_ListTableNeedMerge[i];
//					m_process.statusUpdate(subMsg);
//					sql = "SELECT " + parentTable + "_ID FROM " + m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?";
//					int count = DB.getSQLValue(m_trxName, sql, toASIID);
//					if(count > 0)
//					{
//						BigDecimal qty = rs.getBigDecimal(columnQty); 
//						sql = "UPDATE " + m_ListTableNeedMerge[i] + " SET " + columnQty + " = " + columnQty + " + ?"
//								+ " WHERE M_AttributeSetInstance_ID = ? AND " + parentTable + "_ID = ?";
//						DB.executeUpdate(sql, new Object[]{qty, toASIID, rs.getInt(parentTable + "_ID")}, false, m_trxName);
//						sql = "DELETE FROM " + m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?"
//								+ " AND " + parentTable + "_ID=?";
//						DB.executeUpdate(sql, new Object[]{fromASIID, rs.getInt(parentTable + "_ID")}, false, m_trxName);
//					}
//					else
//					{
//						sql = "UPDATE " + m_ListTableNeedMerge[i] + " SET M_AttributeSetInstance_ID = ?"
//								+ " WHERE " + parentTable + "_ID=? AND M_AttributeSetInstance_ID = ?";
//						DB.executeUpdate(sql, new Object[]{toASIID, rs.getInt(parentTable + "_ID"), fromASIID},
//								false, m_trxName);
//					}
//				}
//			} catch (SQLException e)
//			{
//				throw new AdempiereException(e.getMessage());
//			}
//			finally
//			{
//				DB.close(rs, stmt);
//			}
		}
	}
	
	private void updateRecord(int fromASIID, int toASIID)
	{
		for(int i=0;i<m_ListTableNeedUpdate.length;i++)
		{
			String subMsg = msg + " Update record " + m_ListTableNeedUpdate[i];
			m_process.statusUpdate(subMsg);
			String sql = "UPDATE " + m_ListTableNeedUpdate[i] + " SET M_AttributeSetInstance_ID = ?"
					+ " WHERE M_AttributeSetInstance_ID = ?";
			DB.executeUpdate(sql, new Object[]{toASIID, fromASIID}, false, m_trxName);
		}
	}
	
	public static void main (String[] args)
	{
		String a = "abcdefghij";
		a = a.substring(0, a.length() - 2);
		System.out.println(a);
	}
	
	private String reinitStorage()
	{
		deleteStorage();
		initializeStorage();
		return "Success";
	}
	
	private void deleteStorage()
	{
		String sql = "DELETE FROM M_StorageOnHand";
		DB.executeUpdate(sql, get_TrxName());
	}
	
	private void initializeStorage()
	{
		String sql = "SELECT COUNT(Master.*) FROM (SELECT COUNT(*) AS A FROM M_Transaction t"
					+ " INNER JOIN M_Product p ON p.M_Product_ID = t.M_Product_ID"
					+ " INNER JOIN M_AttributeSetInstance asi ON asi.M_AttributeSetInstance_ID = t.M_AttributeSetInstance_ID"
					+ " WHERE asi.M_AttributeSetInstance_ID <> 0"
					+ " GROUP BY p.M_Product_ID, t.M_Locator_ID, asi.M_AttributeSetInstance_ID) AS Master";
		
		int count = DB.getSQLValue(get_TrxName(), sql);
		m_process.statusUpdate("Total data " + count + ".");
		
		sql = "SELECT p.M_Product_ID, t.M_Locator_ID, asi.M_AttributeSetInstance_ID,"
				+ " asi.Created, p.C_UOM_ID, SUM(t.MovementQty) FROM M_Transaction t"
				+ " INNER JOIN M_Product p ON p.M_Product_ID = t.M_Product_ID"
				+ " INNER JOIN M_AttributeSetInstance asi ON asi.M_AttributeSetInstance_ID = t.M_AttributeSetInstance_ID"
				+ " GROUP BY p.M_Product_ID, t.M_Locator_ID, asi.M_AttributeSetInstance_ID"
				+ " ORDER BY p.M_Product_ID";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				m_process.statusUpdate("Total data " + count + ". Data ke " + rs.getRow());
				sql = "INSERT INTO M_StorageOnHand (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
						+ " IsActive, M_AttributeSetInstance_ID, M_Locator_ID, M_Product_ID,"
						+ " QtyOnHand, Updated, UpdatedBy, M_StorageOnHand_UU, DateMaterialPolicy,"
						+ " C_UOM_ID, UOMConversionL1_ID, UOMQtyOnHandL1)"
						+ " Values (1000015, 1000054, '1991-01-06 06:01:06', 1001075, 'Y', ?, ?, ?, ?, '1991-01-06 06:01:06', 1001075,"
						+ " uuid_generate_v4(), ?, ?, ?, ?)";
				MAttributeSetInstance asi = new MAttributeSetInstance(getCtx(), rs.getInt(3), get_TrxName());
				DB.executeUpdate(sql, new Object[]{
						asi.get_ID(),
						rs.getInt(2),
						rs.getInt(1),
						rs.getBigDecimal(6),
						asi.getCreated(),
						rs.getInt(5),
						rs.getInt(5),
						rs.getBigDecimal(6)
						}, false, get_TrxName());
			}
		}
		catch(SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
//		finally
//		{
//			DB.close(rs, stmt);
//		}
	}
	
	private void updateDateMaterialPolicy()
	{
		for(int i=0;i<m_ListTableNeedMerge.length;i++)
		{
			String parentTable = m_ListTableNeedMerge[i].substring(0, m_ListTableNeedMerge[i].length() - 2);
			if(parentTable.endsWith("_"))
				parentTable = parentTable.substring(0, parentTable.length() - 1);
			if(m_ListTableNeedMerge[i].equals(MUNSRequisitionLineMA.Table_Name))
				parentTable = MRequisitionLine.Table_Name;
//			String sql = "SELECT * FROM " + m_ListTableNeedMerge[i] + " WHERE M_AttributeSetInstance_ID = ?";
			String columnQty = m_ListTableNeedMerge[i].equals("UNS_POSRecapLine_MA") ?
					"Qty" : "MovementQty";
			
			String sql = "SELECT " + parentTable + "_ID, M_AttributeSetInstance_ID, SUM( " + columnQty + ") FROM " + m_ListTableNeedMerge[i]
					+ " WHERE M_AttributeSetInstance_ID <> 0"
					+ " GROUP BY " + parentTable + "_ID, M_AttributeSetInstance_ID"
					+ " HAVING COUNT(*) > 1";
			List<List<Object>> oo = DB.getSQLArrayObjectsEx(get_TrxName(), sql);
			
			if(oo == null)
				continue;
			for(int j=0;j<oo.size();j++)
			{
				msg = "Update date material policy di " + m_ListTableNeedMerge[i] 
						+ ". Data ke " + (j+1) + " dari " + oo.size() + " data.";
				m_process.statusUpdate(msg);
				sql = "SELECT " + m_ListTableNeedMerge[i] + "_UU FROM " + m_ListTableNeedMerge[i]
						+ " WHERE " + parentTable + "_ID = ? AND M_AttributeSetInstance_ID = ?"
								+ " ORDER BY Created ASC";
				String uu = DB.getSQLValueString(get_TrxName(), sql, Integer.parseInt(oo.get(j).get(0).toString())
						, Integer.parseInt(oo.get(j).get(1).toString()));
				
				sql = "UPDATE " + m_ListTableNeedMerge[i] + " SET " + columnQty + " = ? WHERE " + parentTable + "_ID=? AND "
						+ " M_AttributeSetInstance_ID = ? AND " + m_ListTableNeedMerge[i] + "_UU = ?";
				DB.executeUpdate(sql, new Object[]{(BigDecimal) oo.get(j).get(2), 
						Integer.parseInt(oo.get(j).get(0).toString()), Integer.parseInt(oo.get(j).get(1).toString())
						, uu}, false, get_TrxName());
				sql = "DELETE FROM " + m_ListTableNeedMerge[i] + " WHERE " + parentTable + "_ID=? AND "
						+ " M_AttributeSetInstance_ID = ? AND " + m_ListTableNeedMerge[i] + "_UU <> ?";
				DB.executeUpdate(sql, new Object[]{
						Integer.parseInt(oo.get(j).get(0).toString()), Integer.parseInt(oo.get(j).get(1).toString())
						, uu}, false, get_TrxName());
				
				sql = "UPDATE " + m_ListTableNeedMerge[i] + " SET DateMaterialPolicy = ? WHERE " + parentTable + "_ID=? AND "
						+ " M_AttributeSetInstance_ID = ? AND " + m_ListTableNeedMerge[i] + "_UU = ?";
				MAttributeSetInstance asi = new MAttributeSetInstance(getCtx(), 
						Integer.parseInt(oo.get(j).get(1).toString()), get_TrxName());
				DB.executeUpdate(sql, new Object[]{asi.getCreated(), 
						Integer.parseInt(oo.get(j).get(0).toString()), Integer.parseInt(oo.get(j).get(1).toString())
						, uu}, false, get_TrxName());
			}
		}
	}
}