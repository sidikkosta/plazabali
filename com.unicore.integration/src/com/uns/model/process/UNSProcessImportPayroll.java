package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.model.MUNSImportSimpleColumn;

public class UNSProcessImportPayroll extends SvrProcess {

	private String p_FileDir = null;
	private int p_startLineNo = 0;
	private int p_endLineNo = 0;
	private int p_ImportTable_ID = 0;
	private final String ColName = "UNS_MonthlyPayroll_Employee_ID"; 
	
	public UNSProcessImportPayroll() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			String name = param.getParameterName();
			if(name.equals("File_Directory"))
				p_FileDir = param.getParameterAsString();
			else if(name.equals("StartNo"))
				p_startLineNo = param.getParameterAsInt();
			else if(name.equals("EndNo"))
				p_endLineNo = param.getParameterAsInt();
			else if(name.equals("UNS_ImportSimpleTable_ID"))
				p_ImportTable_ID = param.getParameterAsInt();
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Only process from window");
	}

	@Override
	protected String doIt() throws Exception {
		
		MUNSImportSimpleColumn impSimpCol = null;
		String sql = "SELECT UNS_ImportSimpleColumn_ID FROM UNS_ImportSimpleColumn WHERE UNS_ImportSimpleTable_ID = ?"
				+ " AND Name = ? AND isActive = ?";
		int impSimpCol_ID = DB.getSQLValue(get_TrxName(), sql, p_ImportTable_ID, ColName, "Y");
		
		if(impSimpCol_ID <= 0)
		{
			impSimpCol = new MUNSImportSimpleColumn(getCtx(), 0, get_TrxName());
			impSimpCol.setAD_Org_ID(0);
			impSimpCol.setUNS_ImportSimpleTable_ID(p_ImportTable_ID);
			impSimpCol.setName(ColName);
			impSimpCol.setNameAlias(ColName);
			impSimpCol.setIsEmptyCell(true);
			impSimpCol.setAD_Reference_ID(11);  //Integer
			impSimpCol.setColumnNo(69);
			
		}
		else
		{
			impSimpCol = new MUNSImportSimpleColumn(getCtx(), impSimpCol_ID, get_TrxName());
		}
		impSimpCol.setDefaultValue(String.valueOf(getRecord_ID()));
		impSimpCol.saveEx();
		
		//call process Import
		UNSProcessImportSimple procImpSimp = new UNSProcessImportSimple(
				getCtx(), p_FileDir, p_startLineNo, p_endLineNo, p_ImportTable_ID, get_TrxName());
		
		return procImpSimp.doIt();
	}

}
