/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author AzHaidar
 *
 */
public class ReInitHandleRequests extends SvrProcess {

	/**
	 * 
	 */
	public ReInitHandleRequests() {
		// TODO Auto-generated constructor stub
	}
	
	private Timestamp p_DateStart = null;
	private Timestamp p_EndDate = null;
	private IProcessUI m_process;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			if (param.getParameterName().equals("DateStart")) {
				p_DateStart = param.getParameterAsTimestamp();
			}
			else if(param.getParameterName().equals("EndDate")){
				p_EndDate = param.getParameterAsTimestamp();
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_process = Env.getProcessUI(getCtx());
		m_process.statusUpdate("Start re-init Movement Handle Requests.");

		String sql = "SELECT M_Movement_ID FROM M_Movement mm "
				+ "	WHERE ((POReference IS NOT NULL AND TRIM(POReference)<>''"
				+ "			AND EXISTS (SELECT 1 FROM M_Requisition req "
				+ "						WHERE TRIM(req.DocumentNo)=TRIM(mm.POReference) "
				+ "							OR TRIM(req.ReferenceNo)=TRIM(mm.POReference)))"
				+ "		OR (M_Requisition_ID IS NOT NULL AND M_Requisition_ID>0))"
				+ "		AND MovementDate BETWEEN ? AND ?";
		int[] moveIDList = DB.getIDsEx(get_TrxName(), sql, p_DateStart, p_EndDate);
		
		m_process.statusUpdate("Reinit " + moveIDList.length + " of Movements.");
		
		for (int i=0; i < moveIDList.length; i++)
		{
			int move_ID = moveIDList[i];
			
			m_process.statusUpdate("Initializing " + (i+1) + " of " + moveIDList.length + " Movements.");
			
			sql = "SELECT M_MovementLine_ID, M_Product_ID, MovementQty, Created, CreatedBy, AD_Org_ID "
					+ " FROM M_MovementLine ml WHERE M_Movement_ID=" + move_ID
					+ " AND NOT EXISTS (SELECT 1 FROM UNS_HandleRequests hr "
					+ "			WHERE hr.M_MovementLine_ID=ml.M_MovementLine_ID)";
			
			PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
			ResultSet rs = stmt.executeQuery();
			int k = 0;
			
			while (rs.next())
			{
				int MoveLine_ID = rs.getInt(1);
				int M_Product_ID = rs.getInt(2);
				BigDecimal moveQty = rs.getBigDecimal(3);
				Timestamp created = rs.getTimestamp(4);
				int createdBy = rs.getInt(5);
				int AD_Org_ID = rs.getInt(6);
				
				sql = "SELECT rl.M_RequisitionLine_ID FROM M_RequisitionLine rl "
						+ " INNER JOIN M_Requisition req ON rl.M_Requisition_ID=req.M_Requisition_ID "
						+ " INNER JOIN M_Movement mm ON mm.M_Movement_ID=" + move_ID
						+ " WHERE (req.ReferenceNo=mm.POReference OR req.DocumentNo=mm.POReference"
						+ " 	OR (mm.M_Requisition_ID IS NOT NULL AND mm.M_Requisition_ID=req.M_Requisition_ID))"
						+ " 	AND rl.M_Product_ID=" + M_Product_ID;
				
				int reqLine_ID = DB.getSQLValueEx(get_TrxName(), sql);
				
				if (reqLine_ID <= 0)
					continue;
				
				String insert = "INSERT INTO UNS_HandleRequests (AD_Client_ID, AD_Org_ID, IsActive, "
						+ "	M_MovementLine_ID, M_RequisitionLine_ID, Qty, UNS_HandleRequests_ID, "
						+ "	Created, CreatedBy, Updated, UpdatedBy)"
						+ "VALUES(?,?,'Y',?,?,?, GetNextID('UNS_HandleRequests'),?,?,?,?)";
				Object[] params = new Object[]{getAD_Client_ID(), AD_Org_ID, MoveLine_ID, reqLine_ID, moveQty, 
						created, createdBy, created, createdBy};
				int count = DB.executeUpdateEx(insert, params, get_TrxName());
				
				if (count <= 0)
					throw new AdempiereException("Failed when inserting a requests-moved line.");
				
				k++;
				m_process.statusUpdate("Initializing " + (i+1) + "." + k + " of " + moveIDList.length + " Movements.");
			}
		}
		
		return "Success";
	}	
}
