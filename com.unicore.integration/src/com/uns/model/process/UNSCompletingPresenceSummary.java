package com.uns.model.process;

import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.wf.MWorkflow;

import com.uns.base.model.Query;
import com.uns.model.MUNSMonthlyPresenceSummary;
import com.uns.model.factory.UNSHRMModelFactory;

public class UNSCompletingPresenceSummary extends SvrProcess {

	private int C_PERIOD_ID = 0;
	private int AD_ORG_ID = 0;
	private int C_BPARTNER_ID = 0;
//	private String EMPLOYEE_GRADE = null;
	
	
	public UNSCompletingPresenceSummary() {
		super();
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] param = getParameter();
		
		for(ProcessInfoParameter para : param)
		{
			String name = para.getParameterName();
			if(name.equals("C_Period_ID"))
				C_PERIOD_ID = para.getParameterAsInt();
			else if(name.equals("AD_Org_ID"))
				AD_ORG_ID = para.getParameterAsInt();
//			else if(name.equals("EmployeeGrade"))
//				EMPLOYEE_GRADE = para.getParameterAsString();
			else if(name.equals("C_BPartner_ID"))
				C_BPARTNER_ID = para.getParameterAsInt();
			else
				throw new AdempiereException("Unhandled parameter name " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		MUNSMonthlyPresenceSummary[] monthPresences = getMonthPresences();
		
		int totalSucces = 0;
		int totalDoc = monthPresences.length;
		int totalGagal = 0;
		String empGagalComp = "";
		String msg = null;
		for(MUNSMonthlyPresenceSummary monthPresence : monthPresences)
		{
			try
			{
				monthPresence.set_TrxName(get_TrxName());
				ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(monthPresence, MUNSMonthlyPresenceSummary.ACTION_Complete);
				if(!pi.isError())
				{
					++totalSucces;
				}
				else
				{
					++totalGagal;
					String sql = "SELECT CONCAT(Value,'_',Name) FROM UNS_Employee WHERE UNS_Employee_ID = ?";
					String val = DB.getSQLValueString(get_TrxName(), sql, monthPresence.getUNS_Employee_ID());
					empGagalComp += val+"; ";
				}
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return e.getMessage();
			}
			
		}
		
		msg = "Total Dokumen = "+totalDoc+". \n";
		msg += "Total Sukses = "+totalSucces+". \n";
		msg += "Total Gagal = "+totalGagal+". \n";
		if(totalGagal > 0)
		{
			msg += "#Employee yang gagal : "+empGagalComp;
		}
		
		return msg;
	}
	
	private MUNSMonthlyPresenceSummary[] getMonthPresences() {
		
		String wc = "C_Period_ID = ? AND UNS_Employee_ID IN (SELECT UNS_Employee_ID FROM"
				+ " UNS_Employee WHERE AD_Org_ID = ? ";
//				+ "AND EmployeeGrade = ?";
		
		if(C_BPARTNER_ID > 0)
			wc += " AND C_BPartner_ID = "+C_BPARTNER_ID;
		
		wc += ") AND DocStatus IN (?, ?, ?)";
		
		List<MUNSMonthlyPresenceSummary> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSMonthlyPresenceSummary.Table_Name,
				wc,null)
				.setParameters(C_PERIOD_ID, AD_ORG_ID
//						, EMPLOYEE_GRADE
						, MUNSMonthlyPresenceSummary.DOCSTATUS_Drafted
						, MUNSMonthlyPresenceSummary.DOCSTATUS_InProgress
						, MUNSMonthlyPresenceSummary.DOCSTATUS_Invalid)
				.list();
		
		MUNSMonthlyPresenceSummary[] monthPresences = list.toArray(new MUNSMonthlyPresenceSummary[list.size()]);
		
		return monthPresences;
		
	}
	
}
