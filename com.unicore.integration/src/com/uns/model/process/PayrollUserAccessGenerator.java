/**
 * 
 */
package com.uns.model.process;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MClient;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;
import org.compiere.util.Util;
import com.uns.model.MUNSUserAccess;

/**
 * @author nurse
 *
 */
public class PayrollUserAccessGenerator extends SvrProcess 
{

	/**
	 * 
	 */
	public PayrollUserAccessGenerator() 
	{
		super ();
	}
	
	private String p_fileName = null;
	private String p_password = null;
	private Timestamp p_validFrom = null;
	private Timestamp p_validTo = null;
	private int p_userID = 0;
	private String p_description = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if (params[i].getParameterName().equals("Password"))
				p_password = params[i].getParameterAsString();
			else if (params[i].getParameterName().equals("FileName"))
				p_fileName = params[i].getParameterAsString();
			else if (params[i].getParameterName().equals("ValidFrom"))
				p_validFrom = params[i].getParameterAsTimestamp();
			else if (params[i].getParameterName().equals("ValidTo"))
				p_validTo = params[i].getParameterAsTimestamp();
			else if (params[i].getParameterName().equals("AD_User_ID"))
				p_userID = params[i].getParameterAsInt();
			else if (params[i].getParameterName().equals("Description"))
				p_description = params[i].getParameterAsString();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (p_fileName == null && Util.isEmpty(p_password, true))
			throw new AdempiereUserError("File name or password is mandatory");
		if (p_fileName != null)
		{
			byte[] bytes = Files.readAllBytes(Paths.get(p_fileName));
			p_password = new String(bytes, Charset.forName("UTF-8"));
		}
		
		String sysPassword = MClient.get(getCtx()).getPayrollPassword();
		sysPassword = SecureEngine.decrypt(sysPassword, getAD_Client_ID());
		
		if (!p_password.equals(sysPassword))
			throw new AdempiereUserError("Invalid Password");
		
		long milisStart = p_validFrom.getTime();
		long milisEnd = p_validTo.getTime();
		StringBuilder sb = new StringBuilder(p_password).append("#").append(milisStart).append("#").append(milisEnd);
		String newPwd = sb.toString();
		String encPwd = SecureEngine.encrypt(newPwd, getAD_Client_ID());
		
		MUNSUserAccess access = new MUNSUserAccess(getCtx(), 0, get_TrxName());
		access.setAD_Org_ID(Env.getAD_Org_ID(getCtx()));
		access.setAD_User_ID(p_userID);
		access.setValidFrom(p_validFrom);
		access.setValidTo(p_validTo);
		access.setPassword(encPwd);
		access.setIsExpired(false);
		access.setDescription(p_description);
		
		if (!access.save())
			throw new AdempiereException();
		
		return "success";
	}

}
