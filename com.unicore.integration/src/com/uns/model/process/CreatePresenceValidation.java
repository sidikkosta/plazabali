/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.uns.model.MUNSDailyPresence;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSMonthlyPresenceVal;
import com.uns.model.MUNSOTGroupRequest;
import com.uns.model.MUNSOTRequest;
import com.uns.model.MUNSPayrollConfiguration;

/**
 * @author Burhani Adam
 *
 */
public class CreatePresenceValidation extends SvrProcess {

	private int p_AD_Org_ID = 0;
	private int p_SectionOfDept_ID = 0;
	private int p_C_Period_ID = 0;
	private Timestamp p_DateTo = null;
	private Timestamp p_DateFrom = null;
	private int p_Resource_ID = 0;
	private int p_Employee_ID = 0;
	private Timestamp m_startDate = null;
	private Timestamp m_endDate = null;
	private boolean p_isIncludedMangkir = false;
	private IProcessUI m_processMsg;
	
	/**
	 * 
	 */
	public CreatePresenceValidation() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
			{
				p_AD_Org_ID = params[i].getParameterAsInt();
			}
			else if ("C_BPartner_ID".equals(name))
			{
				p_SectionOfDept_ID = params[i].getParameterAsInt();
			}
			else if ("C_Period_ID".equals(name))
			{
				p_C_Period_ID = params[i].getParameterAsInt();
			}
			else if ("DateFrom".equals(name))
			{
				p_DateFrom = params[i].getParameterAsTimestamp();
			}
			else if("DateTo".equals(name))
			{
				p_DateTo = params[i].getParameterAsTimestamp();
			}
			else if("UNS_Employee_ID".equals(name))
			{
				p_Employee_ID = params[i].getParameterAsInt();
			}
			else if("UNS_Resource_ID".equals(name))
			{
				p_Resource_ID = params[i].getParameterAsInt();
			}
			else if("IsIncludedMangkir".equals(name))
			{
				p_isIncludedMangkir = params[i].getParameterAsBoolean();
			}
			else
			{
				log.log(Level.SEVERE, "Unknown parameter " + name);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_processMsg = Env.getProcessUI(getCtx());
//		analyzeStartEndDate();
		String sql = "SELECT DISTINCT(d.UNS_DailyPresence_ID) FROM UNS_DailyPresence d"
				+ " INNER JOIN UNS_MonthlyPresenceSummary m ON "
				+ " m.UNS_MonthlyPresenceSummary_ID = d.UNS_MonthlyPresenceSummary_ID"
				+ " INNER JOIN UNS_Employee emp ON emp.UNS_Employee_ID = m.UNS_Employee_ID"
				+ " INNER JOIN UNS_Resource_WorkerLine rw ON"
				+ " rw.Labor_ID = emp.UNS_Employee_ID"
				+ " WHERE rw.IsActive = 'Y'"
//				+ "AND d.PresenceDate BETWEEN ? AND ?"
				+ " AND (d.UNS_MonthlyPresenceVal_ID <= 0 OR d.UNS_MonthlyPresenceVal_ID IS NULL)"
				+ " AND (d.UNS_PresenceVerification_ID <= 0 OR d.UNS_PresenceVerification_ID IS NULL)"
				+ " AND m.C_Period_ID = ?";
//				+ " AND ((d.FSTimeIn IS NULL OR d.FSTimeOut IS NULL) AND d.PresenceStatus <> 'IZN' AND d.PresenceStatus <> 'LBR')";
		
//		if(!p_isIncludedMangkir)
//			sql += " AND d.PresenceStatus <> 'MKR'";
		if(p_DateTo != null)
			sql += " AND d.PresenceDate <= '" + p_DateTo + "'";
		if(p_AD_Org_ID > 0)
			sql += " AND emp.AD_Org_ID = " + p_AD_Org_ID;
		if(p_SectionOfDept_ID > 0)
			sql += " AND emp.C_BPartner_ID = " + p_SectionOfDept_ID;
		if(p_Resource_ID > 0)
			sql += " AND rw.UNS_Resource_ID = " + p_Resource_ID;
		if(p_Employee_ID > 0)
			sql += " AND emp.UNS_Employee_ID = " + p_Employee_ID;
			
		List<MUNSDailyPresence> listDay = new ArrayList<>();
//		List<Object> lists = DB.getSQLValueObjectsEx(get_TrxName(), sql, new Object[]{m_startDate, m_endDate});
//		int count = 1;
//		for(int i=0;i<lists.size();i++)
//		{
//			if(count == 1)
//			{
//				m_processMsg.statusUpdate("Waiting..");
//				count++;
//			}
//			else if(count == 2)
//			{
//				m_processMsg.statusUpdate("Waiting...");
//				count++;
//			}
//			else if(count == 3)
//			{
//				m_processMsg.statusUpdate("Waiting....");
//				count = 1;
//			}
//			listDay.add(new MUNSDailyPresence(getCtx(), (Integer) lists.get(0), get_TrxName()));
//		}

		ResultSet rs = null;
		PreparedStatement stmt = null;
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
//			stmt.setTimestamp(1, m_startDate);
//			stmt.setTimestamp(2, m_endDate);
			stmt.setInt(1, p_C_Period_ID);
			rs = stmt.executeQuery();
			int count = 1;
			while(rs.next())
			{
				if(count == 1)
				{
					m_processMsg.statusUpdate("Waiting..");
					count++;
				}
				else if(count == 2)
				{
					m_processMsg.statusUpdate("Waiting...");
					count++;
				}
				else if(count == 3)
				{
					m_processMsg.statusUpdate("Waiting....");
					count = 1;
				}
				listDay.add(new MUNSDailyPresence(getCtx(), rs.getInt(1), get_TrxName()));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		MUNSDailyPresence[] days = listDay.toArray(new MUNSDailyPresence[listDay.size()]);
		
		for(int i=0;i<days.length;i++)
		{
			m_processMsg.statusUpdate("Data ke " + (i+1) + " dari " + days.length + " data.");
			if(days[i].getUNS_MonthlyPresenceVal_ID() > 0)
				continue;
			int empID = days[i].getUNS_MonthlyPresenceSummary().getUNS_Employee_ID();
			boolean isOverTime = false;
			//try get to new version
			isOverTime = MUNSOTGroupRequest.getValidof(
					get_TrxName(), days[i].getPresenceDate(), empID).length > 0 ? true : false;
			//if new version == null then use old version
			if(!isOverTime)
				isOverTime = MUNSOTRequest.getValidof(
						get_TrxName(), days[i].getPresenceDate(), empID).length > 0 ? true : false;
				
			if (
					(days[i].getFSTimeIn() == null || days[i].getFSTimeOut() == null)
					&& !MUNSDailyPresence.PRESENCESTATUS_Izin.
						equals(days[i].getPresenceStatus())
					&& !MUNSDailyPresence.PRESENCESTATUS_Libur.
						equals(days[i].getPresenceStatus())
					&& (p_isIncludedMangkir ? true : !MUNSDailyPresence.PRESENCESTATUS_Mangkir.
						equals(days[i].getPresenceStatus())))
//					((MUNSDailyPresence.DAYTYPE_HariLiburNasional.
//						equals(days[i].getDayType()) ||
//						MUNSDailyPresence.DAYTYPE_HariLiburMingguan.equals(days[i].getDayType()))
//						&& MUNSDailyPresence.PRESENCESTATUS_Lembur.equals(days[i].getPresenceStatus())) ||
//						(isOverTime && days[i].getBelatedDuration() > 0))
			{
				MUNSEmployee emp = MUNSEmployee.get(getCtx(), empID);
//				HRMUtils util = new HRMUtils(getCtx(), emp, days[i].getPresenceDate(), get_TrxName());
//				boolean isWorkDay = util.isWorkDay();
//										 	
//				//check is manual presence verification or not
//				String sql4 = "SELECT 1 FROM UNS_SlotType WHERE UNS_SlotType_ID = ?"
//						+ " AND isManualPresence = ?";
//				boolean isManual = DB.getSQLValue(get_TrxName(), sql4, new Object[]{util.getSlotType().get_ID(), "Y"}) == 1;
//				if(isManual)
//					continue;
//				
//			   	if(isWorkDay || isOverTime)
//		    	{
			   		String sql2 = "SELECT NewSectionOfDept_ID FROM UNS_Contract_Recommendation"
			   				+ " WHERE UNS_Employee_ID = ? AND ? BETWEEN DateContractStart AND DateContractEnd"
			   				+ " AND DocStatus IN ('CO', 'CL') AND IsActive = 'Y'";
			   		int DeptID = DB.getSQLValue(get_TrxName(), sql2, new Object[]{emp.get_ID(), days[i].getPresenceDate()});
			   		if(DeptID <= 0)
			   			DeptID = emp.getC_BPartner_ID();
			   		MUNSMonthlyPresenceVal validation = null;
			    	validation = MUNSMonthlyPresenceVal.createValidation(
								days[i], "PresenceDate", m_startDate, 
								m_endDate, DeptID, 
								p_C_Period_ID);
			    	days[i].setUNS_MonthlyPresenceVal_ID(validation.get_ID());
			    	days[i].setRunValidator(false);
			    	days[i].saveEx();
//		    	}
			}
		}
		
		return "Success";
	}
	
	@SuppressWarnings("unused")
	private void analyzeStartEndDate()
	{
		MPeriod period = MPeriod.get(getCtx(), p_C_Period_ID);
		Calendar cal = TimeUtil.getCalendar(period.getStartDate());
		
		MUNSPayrollConfiguration config =
				MUNSPayrollConfiguration.get(
						getCtx(), period, 0, get_TrxName(), true);
		
		if (null == config)
		{
			throw new AdempiereException(
					"Can't find payroll configuration.");
		}
		
		int startConfig = config.getPayrollDateStart();
		int endConfig = config.getPayrollDateEnd();
		
		cal.set(Calendar.DATE, startConfig);
		
		int daysOfMonth = cal.getActualMaximum(Calendar.DATE);
		int median = daysOfMonth / 2;
		
		if (startConfig > median)
		{
			cal.add(Calendar.MONTH, -1);
		}
		
		m_startDate = new Timestamp(cal.getTimeInMillis());
		
		if(p_DateFrom != null && p_DateFrom.after(m_startDate) && p_DateFrom.before(m_endDate))
			m_startDate = p_DateFrom;
		
		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DATE, endConfig);
		
		m_endDate = new Timestamp(cal.getTimeInMillis());
		
		if(p_DateTo != null && p_DateTo.before(m_endDate) && p_DateTo.after(m_startDate))
			m_endDate = p_DateTo;
	}
}