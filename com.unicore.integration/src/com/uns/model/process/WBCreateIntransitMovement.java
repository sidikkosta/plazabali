/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.unicore.base.model.MMovement;
import com.unicore.base.model.MMovementLine;
import com.unicore.model.MUNSWeighbridgeTicket;

/**
 * @author Menjangan
 * Create the Intransit Movement document based on Weighbridge Ticket.
 */
public class WBCreateIntransitMovement extends SvrProcess 
{

	/**
	 * 
	 */
	public WBCreateIntransitMovement() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			ProcessInfoParameter param = params[i];
			String name = param.getParameterName();
			if ("M_LocatorTo_ID".equals(name))
				p_locatorTo_ID = param.getParameterAsInt();
			else if ("MovementDate".equals(name))
				p_movementDate = param.getParameterAsTimestamp();
			else
				log.log(Level.WARNING, "Unknown parameter " + name);
		}
		
		m_model = new MUNSWeighbridgeTicket(getCtx(), getRecord_ID(), 
				get_TrxName());
	}
	
	private int p_locatorTo_ID = 0;
	private MUNSWeighbridgeTicket m_model = null;
	private Timestamp p_movementDate = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		int movement_ID = m_model.getM_Movement_ID();
		if (movement_ID > 0)
		{
			String sql = "SELECT DocStatus FROM M_Movement WHERE M_Movement_ID = ?";
			String docstatus = DB.getSQLValueString(get_TrxName(), sql, movement_ID);
			if ("CO".equals(docstatus) || "CL".equals(docstatus))
				return "The Movement Document already created before.";
			else if ("DR".equals("DocStatus") || "IP".equals(docstatus) 
					|| "IN".equals(docstatus))
			{
				MMovement move = new MMovement(getCtx(), movement_ID, get_TrxName());	
				move.setMovementDate(p_movementDate);
				move.setIsInTransit(true);
				boolean ok = move.processIt(DocAction.ACTION_Complete);
				if (!ok)
					throw new AdempiereException(move.getProcessMsg());
				
				move.saveEx();
				return "OK";
			}
			
			m_model.setM_Movement_ID(-1);
		}
		
		//get the instansit document type
		String sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType = ? "
				+ " AND IsIntransit = ?";
		int docType_ID = DB.getSQLValue(get_TrxName(), sql, 
				MDocType.DOCBASETYPE_MaterialMovement, "Y");
		
		if (docType_ID <= 0)
			throw new AdempiereException(
					"Could not find document type Material Movement Intransit.");
		
		sql = "SELECT M_Warehouse_ID FROM M_Locator WHERE M_Locator_ID = ?";
		int destinationWhs_ID = DB.getSQLValue(get_TrxName(), sql, p_locatorTo_ID);
		//is cannot be null because the locator to is mandatory in process parameter.
		
		MMovement move = new MMovement(getCtx(), 0, get_TrxName());
		move.setAD_Org_ID(m_model.getAD_Org_ID());
		move.setAD_User_ID(Env.getAD_User_ID(getCtx()));
		move.setC_DocType_ID(docType_ID);
		move.setDestinationWarehouse_ID(destinationWhs_ID);	
		move.setMovementDate(p_movementDate);
		move.setIsInTransit(true);
		
		move.saveEx();
		
		MMovementLine line = new MMovementLine(move);
		line.setM_Locator_ID(m_model.getM_Locator_ID());
		line.setM_LocatorTo_ID(p_locatorTo_ID);
		line.setM_Product_ID(m_model.getM_Product_ID());
		line.setMovementQty(m_model.getNettoII());
		
		line.saveEx();

		m_model.setM_MovementLine_ID(line.get_ID());
		m_model.saveEx();
		
		boolean completed = move.processIt(DocAction.ACTION_Complete);
		if (!completed)
			throw new AdempiereException(move.getProcessMsg());
		move.saveEx();
		
		return "Generated Movement #" + move.getDocumentNo();
	}

}
