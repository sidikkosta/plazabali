package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.MUNSContractRecommendation;

public class SynchAddendumContract extends SvrProcess {

	Properties m_ctx = null;
	String m_trxName = null;
	
	public SynchAddendumContract() {
		
	}

	@Override
	protected void prepare() {
		
		m_ctx = getCtx();
		m_trxName = get_TrxName();
		
	}
	
	public SynchAddendumContract(Properties ctx, String trxName) {
		m_ctx = ctx;
		m_trxName = trxName;
	}

	@Override
	protected String doIt() throws Exception {
		
		String strNow = new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(new Date());
		Timestamp now = Timestamp.valueOf(strNow);
		ArrayList<Integer> contracts = new ArrayList<Integer>();
		
		String sql = "SELECT UNS_Contract_Recommendation_ID FROM UNS_Contract_Recommendation WHERE"
				+ " EffectiveDate <= ? AND DocStatus IN ('CO','CL') AND UNS_Contract_Evaluation_ID > 0"
				+ " AND isSynchronized = 'N' AND DocType = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql, m_trxName);
			st.setTimestamp(1, now);
			st.setString(2, MUNSContractRecommendation.DOCTYPE_Addendum);
			rs = st.executeQuery();
			while(rs.next())
			{
				contracts.add(rs.getInt(1));
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new AdempiereException(ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
		
		
		for(int i=0; i<contracts.size(); i++)
		{
			int count = i+1;
			processUI.statusUpdate("Processing " + count + " from " + contracts.size());
			MUNSContractRecommendation contract = new MUNSContractRecommendation(
					m_ctx, contracts.get(i), m_trxName);
			boolean isDifferentLegality = contract.getUNS_Contract_Evaluation().isDifferentLegality();
			String errMsg = contract.updateData(isDifferentLegality);
			if(errMsg != null)
				throw new AdempiereException(errMsg);
			
			contract.setisSynchronized(true);
			contract.saveEx();
		}
		
		
		return "Success";
	}

}
