/**
 * 
 */
package com.uns.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;

import com.unicore.base.model.MMovementConfirm;
import com.unicore.base.model.MMovementLine;

import org.compiere.model.MMovementLineConfirm;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import com.unicore.base.model.MMovement;
import com.unicore.model.MUNSWeighbridgeTicket;

/**
 * @author Menjangan
 * Sync General Weighbridge Ticket with Destination Warehouse Confirm. 
 * This process maybe create a new Destination Warehouse Confirm when 
 * the document doesn't exists.
 * 
 */
public class WBSyncToDesWhsConfirm extends SvrProcess 
{
	private MUNSWeighbridgeTicket m_model = null;
	private int p_M_Movement_ID = 0;
	
	/**
	 * 
	 */
	public WBSyncToDesWhsConfirm() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			ProcessInfoParameter param = params[i];
			String name = param.getParameterName();
			if ("M_Movement_ID".equals(name))
				p_M_Movement_ID = param.getParameterAsInt();
			else
				log.log(Level.WARNING, "Unknown parameter" + name);
		}
		
		m_model = new MUNSWeighbridgeTicket(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		int confirm_ID = m_model.getM_MovementConfirm_ID();
		if (confirm_ID > 0)
		{
			String sql = "SELECT DocStatus FROM M_MovementConfirm WHERE "
					+ " M_MovementConfirm_ID = ?";
			String status = DB.getSQLValueString(get_TrxName(), sql, confirm_ID);
			if ("CO".equals(status) || "CL".equals(status))
				return "Ticket already sync with confirmation before.";
			else if ("DR".equals(status) || "IP".equals(status) || "IN".equals(status))
			{
				MMovementConfirm confirm = new MMovementConfirm(
						getCtx(), confirm_ID, get_TrxName());
				confirm.setReceiptDate(m_model.getDateDoc());
				confirm.saveEx();
				
				MMovementLineConfirm[] lines = confirm.getLines(false);
				MMovementLineConfirm line = null;
				for (int i=0; i<lines.length; i++)
				{
					if (!lines[i].isActive())
						continue;
					sql = "SELECT M_Product_ID FROM M_MovementLine WHERE "
							+ " M_MovementLine_ID = ?";
					int M_Product_ID = DB.getSQLValue(get_TrxName(), sql, 
							lines[i].getM_MovementLine_ID());
					
					if (M_Product_ID == m_model.getM_Product_ID())
					{
						line = lines[i];
						break;
					}
				}
				
				line.setConfirmedQty(m_model.getNettoII());
				line.saveEx();
				
				boolean ok = confirm.processIt(DocAction.ACTION_Complete);
				if (!ok)
					throw new AdempiereException(confirm.getProcessMsg());
				
				confirm.saveEx();
				return "Sync success, confirmation #" + confirm.getDocumentNo();
			}
			
			m_model.setM_MovementLineConfirm_ID(-1);
		}
		
		MMovement move = new MMovement(getCtx(), p_M_Movement_ID, get_TrxName());
		MMovementConfirm[] confirms = move.getDestinationWarehouseConfirmations(false);
		MMovementConfirm confirm = null;
		for (int i=0; i<confirms.length; i++)
		{
			if (confirms[i].get_ID() == confirm_ID)
				continue;
			
			else if (confirms[i].getDocStatus().equals("DR") 
					|| confirms[i].getDocStatus().equals("IP") 
					|| confirms[i].getDocStatus().equals("IN"))
			{
				confirm = confirms[i];
				break;
			}
		}
		
		if (null == confirm)
			confirm = MMovementConfirm.create(move, false, 
					MMovementConfirm.CONFIRMATIONTYPE_DestinationWarehouseConfirmation);
		
		if (null == confirm)
		{
			String error = "Can't create destination warehouse confirm::";
			error += CLogger.retrieveErrorString("Unknown Error");
			
			throw new AdempiereException(error);
		}
		
		confirm.setReceiptDate(m_model.getDateDoc());
		confirm.saveEx();
		
		MMovementLineConfirm[] lines = confirm.getLines(false);
		MMovementLineConfirm line = null;
		for (int i=0; i<lines.length; i++)
		{
			if (!lines[i].isActive())
				continue;
			String sql = "SELECT M_Product_ID FROM M_MovementLine WHERE "
					+ " M_MovementLine_ID = ?";
			int M_Product_ID = DB.getSQLValue(get_TrxName(), sql, 
					lines[i].getM_MovementLine_ID());
			if (M_Product_ID == m_model.getM_Product_ID())
			{
				line = lines[i];
				break;
			}
		}
		
		if (null == line)
		{
			MMovementLine[] mvLines = move.getLines(false);
			
			for (int i=0; i<mvLines.length; i++)
			{
				if (!mvLines[i].isActive())
					continue;
				else if (mvLines[i].getM_Product_ID() != m_model.getM_Product_ID())
					continue;
				else if (line != null)
					throw new AdempiereException("Unhandle case. More than one row");
				line = new MMovementLineConfirm(confirm);
				line.setMovementLine(mvLines[i]);
				line.saveEx();
			}
		}
		
		line.setConfirmedQty(m_model.getNettoII());
		line.saveEx();

		m_model.setM_MovementLineConfirm_ID(line.get_ID());
		m_model.saveEx();
		
		
		boolean ok = confirm.processIt(DocAction.ACTION_Complete);
		if (!ok)
			throw new AdempiereException(confirm.getProcessMsg());
		
		confirm.saveEx();
		
		return "Sync success. confirmation #" + confirm.getDocumentNo();
	}

}
