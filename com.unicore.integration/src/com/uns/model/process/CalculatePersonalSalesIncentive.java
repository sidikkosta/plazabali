/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MPeriod;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.model.MUNSPushMoneyProduct;
import com.uns.model.I_UNS_ShopRecap;
import com.uns.model.I_UNS_SubregionRecap;
import com.uns.model.MUNSShopEmployee;
import com.uns.model.X_UNS_ShopEmployee;

/**
 * @author dpitaloka
 *
 */
public class CalculatePersonalSalesIncentive extends SvrProcess {

	private BigDecimal m_IndividuPercentage;
	private MUNSShopEmployee m_ShopEmployee;
	private MPeriod m_Period;

	/**
	 * 
	 */
	public CalculatePersonalSalesIncentive() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		countCashierSession();
		setNetSalesRealizationAmount();
		setNotPresentSalesAmount();
		setSalesIncentiveAmt();
		setCashierIncentiveAmt();
		setGroupIncentiveAmt();
		setPushMoneyAmt();
		m_ShopEmployee.save();
		return "Personal Sales Incentive has been calculated.";
	}

	private void setGroupIncentiveAmt() {
		I_UNS_ShopRecap storeSalesRecap = 
				m_ShopEmployee.getUNS_ShopRecap();
		
		I_UNS_SubregionRecap subregionRecap =
				m_ShopEmployee.getUNS_SubregionRecap();
		
		BigDecimal groupIncentiveAmount = BigDecimal.ZERO;
		String positionType = m_ShopEmployee.getPositionType();
		if (MUNSShopEmployee.POSITIONTYPE_Staff.equals(positionType)) {
			BigDecimal points = (storeSalesRecap.getNetSalesRealizationAmt().compareTo(storeSalesRecap
					.getIndividualTargetAmt()) == -1) ? BigDecimal.ZERO
					: m_IndividuPercentage;
			groupIncentiveAmount = storeSalesRecap.getGroupSalesIncentive()
					.multiply(points);
		} else {
			BigDecimal points = (subregionRecap.getNetSalesRealizationAmt()
					.compareTo(subregionRecap.getNetSalesTarget()) == -1) ? BigDecimal.ZERO
					: m_IndividuPercentage;
			groupIncentiveAmount = subregionRecap.getNetSalesIncentiveAmt()
					.multiply(points);
		}
		m_ShopEmployee.setGroupIncentiveAmt(groupIncentiveAmount);		
	}

	private void setCashierIncentiveAmt() {
		I_UNS_ShopRecap storeSalesRecap = 
				m_ShopEmployee.getUNS_ShopRecap();
		BigDecimal totalCashierSession = 
				storeSalesRecap.getTotalCashierSession();
		BigDecimal groupIncentiveForCashier = 
				storeSalesRecap.getGroupCashierIncentive();
		
		BigDecimal cashierCount = m_ShopEmployee.getCashierCount();
		BigDecimal cashierIncentiveAmt = BigDecimal.ZERO;
		if (!cashierCount.equals(BigDecimal.ZERO) && 
				!totalCashierSession.equals(BigDecimal.ZERO)) {
			cashierIncentiveAmt = cashierCount.multiply(
					totalCashierSession).divide(groupIncentiveForCashier);
		}
		m_ShopEmployee.setCashierIncentiveAmt(cashierIncentiveAmt);
	}

	private void setSalesIncentiveAmt() throws SQLException {
		m_ShopEmployee.setSalesIncentiveAmt(m_IndividuPercentage
				.multiply(m_ShopEmployee.getNetSalesRealizationAmt()));
	}

	@Override
	protected void prepare() {
		m_ShopEmployee = new MUNSShopEmployee(getCtx(),
				getRecord_ID(), get_TrxName());
		
		m_Period = getPeriod();

		String sql = "SELECT MAX(c_period_id), individupercent "
				+ "FROM uns_globalincentivesconfig "
				+ "WHERE c_period_id <= ? "
				+ "GROUP BY individupercent";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, get_TrxName());
			pstmt.setInt(1, m_Period.getC_Period_ID());
			rs = pstmt.executeQuery();		
			rs.next();	
			
			m_IndividuPercentage = rs.getBigDecimal(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, pstmt);
			pstmt = null;
			rs = null;
		}
	}
	
	private void setNotPresentSalesAmount() throws SQLException {
		
		String sql = "select SUM(grandtotal) "
				+ "from uns_postrx p "
				+ "where p.salesrep_id = ? "
				+ "and p.datetrx >= ? "
				+ "and p.datetrx =< ? ";
		PreparedStatement ps = DB.prepareStatement(sql, get_TrxName());
		ps.setInt(1, m_ShopEmployee.getAD_User_ID());
		ps.setString(2, DB.TO_DATE(m_Period.getStartDate()));
		ps.setString(3, DB.TO_DATE(m_Period.getEndDate()));
		ResultSet rs = ps.executeQuery(); rs.next();		
		
		m_ShopEmployee.setNotPresentSalesAmt(rs.getBigDecimal(1)
				.subtract(m_ShopEmployee.getNetSalesRealizationAmt()));	
	}

	private void setNetSalesRealizationAmount()
			throws SQLException {
		
		String sql = "select SUM(grandtotal) "
				+ "from uns_dailypresence d, uns_monthlypresencesummary m, uns_postrx p "
				+ "where m.uns_employee_id = p.salesrep_id "
				+ "and d.uns_monthlypresencesummary_id = m.uns_monthlypresencesummary_id"
				+ "and p.salesrep_id = ? "
				+ "and p.datetrx >= ? "
				+ "and p.datetrx =< ? ";
		PreparedStatement ps = DB.prepareStatement(sql, get_TrxName());
		ps.setInt(1, m_ShopEmployee.getAD_User_ID());
		ps.setString(2, DB.TO_DATE(m_Period.getStartDate()));
		ps.setString(3, DB.TO_DATE(m_Period.getEndDate()));
		ResultSet rs = ps.executeQuery(); rs.next();		

//		CalculateSalesIncentive.setNetSalesRealizationAmt(m_ShopEmployee, rs.getBigDecimal(1));
	}

	
	private void setPushMoneyAmt() throws SQLException {
		String sql = "SELECT L.m_product_id, PT.dateacct "
				+ "FROM uns_postrxline L, uns_postrx PT "
				+ "WHERE L.uns_postrx_id = PT.uns_postrx_id "
				+ "AND PT.salesrep_id = ?"
				+ "and PT.datetrx >= ? "
				+ "and PT.datetrx =< ? ";
		
		PreparedStatement ps = DB.prepareStatement(sql, get_TrxName());
		ps.setInt(1, m_ShopEmployee.getAD_User_ID());
		ps.setString(2, DB.TO_DATE(m_Period.getStartDate()));
		ps.setString(3, DB.TO_DATE(m_Period.getEndDate()));
		ResultSet rs = ps.executeQuery();
		BigDecimal pushMoneyAmt = BigDecimal.ZERO;
		while (rs.next()) {
			pushMoneyAmt = MUNSPushMoneyProduct.getAll(getCtx(),
					rs.getInt(1), rs.getTimestamp(2), rs.getTimestamp(2), get_TrxName());
			pushMoneyAmt.add(pushMoneyAmt);
		}
		m_ShopEmployee.setPushMoneyAmt(pushMoneyAmt);			
		
	}

	private MPeriod getPeriod() {
		int cPeriodId;
		if (X_UNS_ShopEmployee.POSITIONTYPE_Staff.equals(m_ShopEmployee.getPositionType())) {
			cPeriodId = m_ShopEmployee.getUNS_ShopRecap().getUNS_SubregionRecap().getC_Period_ID();
		} else {
			cPeriodId = m_ShopEmployee.getUNS_SubregionRecap().getC_Period_ID();
		}
		return MPeriod.get(getCtx(), cPeriodId);
	}

	private void countCashierSession() {
		String s = "SELECT COUNT(*) FROM uns_pos_session PS"
				+ ", uns_shopemployee SE, uns_postrx PT, uns_shoprecap SR"
				+ " WHERE PS.cashier_id = SE.ad_user_id"
				+ " AND SE.ad_user_id = ?"
				+ " AND PT.uns_pos_session_id = PS.uns_pos_session_id"
				+ " AND PT.c_bpartner_id = SR.shop_id"
				+ " AND SE.uns_shoprecap_id = SR.uns_shoprecap_id"
				+ " AND PT.datetrx BETWEEN ? AND ?";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DB.prepareStatement(s, get_TrxName());
			ps.setInt(1, getAD_User_ID());
			ps.setString(2, DB.TO_DATE(m_Period.getStartDate()));
			ps.setString(3, DB.TO_DATE(m_Period.getEndDate()));
			rs = ps.executeQuery();
			if (rs.next()) {
				m_ShopEmployee.setCashierCount(rs.getBigDecimal(1));
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, s, e);
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
	}
}
