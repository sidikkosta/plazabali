/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSGlobalIncentivesConfig;
import com.uns.model.MUNSShopEmployee;
import com.uns.model.MUNSSubregionRecap;
import com.uns.model.X_UNS_GlobalIncentivesConfig;
import com.uns.model.X_UNS_SubregionRecap;

/**
 * @author dpitaloka--azhaidar
 *
 */
public class CalculateSalesIncentive extends SvrProcess {

	private MUNSSubregionRecap m_subregion;
	private X_UNS_GlobalIncentivesConfig m_globalConf;

	/**
	 * 
	 */
	public CalculateSalesIncentive() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		String s = "SELECT COUNT(PCB.uns_periodiccostbenefit_id) "
				+ " FROM uns_periodiccostbenefit PCB, uns_shoprecap SR"
				+ " WHERE PCB.docstatus IN ('CO', 'CL')"
				+ " AND PCB.uns_periodiccostbenefit_id = SR.uns_periodiccostbenefit_id"
				+ " AND SR.uns_subregionrecap_id = " + getRecord_ID();
		
		if (DB.getSQLValue(get_TrxName(), s) > 0)
			throw new AdempiereException("Document has been completed/closed.");
		
		m_subregion = new MUNSSubregionRecap(getCtx(),
				getRecord_ID(), get_TrxName());
		
		m_globalConf = MUNSGlobalIncentivesConfig.get(getCtx(), 
				m_subregion.getAD_Org_ID(), m_subregion.getShopType(), 
				m_subregion.getC_Period_ID(), get_TrxName());
		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (MUNSSubregionRecap.DOCSTATUS_Closed.equals(m_subregion.getDocStatus()))
			return "Cannot recalculate incentives for the closed document.";
			
		if (m_globalConf == null)
			return "Cannot fond appropriate Incentive Configuration for this recapitulation.";
		
		String msg = null;
		
		if (m_subregion.getShopType().equals(MUNSSubregionRecap.SHOPTYPE_ShopFB))
		{
			distributeServiceCharges();
			msg = "Service charge of shops are distributed.";
		}
		else 
		{
			setPushMoneyAmt(m_subregion);
			calculateAndUpdateIncentives();
			msg = "Sales incentives has been calculated.";
		}
		m_subregion.saveEx();
		addLog(msg);
		
		return null;
	}

	//------------------------------------------------------------
	// E M P L O Y E E
	//------------------------------------------------------------
	
	/**
	 * 
	 */
	private void calculateAndUpdateIncentives()
	{
		// --- Update Not Present Sales Amount.
		
		String netSalesAmt = "SELECT COALESCE(SUM(pt.GrandTotal), 0) FROM UNS_POSTrx pt "
				+ " INNER JOIN UNS_ShopRecap sr ON sr.UNS_ShopRecap_ID=se.UNS_ShopRecap_ID"
				+ " INNER JOIN C_Period p ON sr.C_Period_ID=p.C_Period_ID"
				+ " WHERE pt.SalesRep_ID=se.AD_User_ID AND pt.DocStatus IN ('CO', 'CL')"
				+ "		AND pt.DateAcct BETWEEN p.StartDate AND p.EndDate ";
		
		String notPresentSalesAmt = netSalesAmt 
				+ "		AND NOT EXISTS(SELECT 1 FROM UNS_DailyPresence dp "
				+ "				INNER JOIN UNS_MonthlyPresenceSummary mps ON dp.UNS_MonthlyPresenceSummary_ID=mps.UNS_MonthlyPresenceSummary_ID "
				+ "				WHERE mps.UNS_Employee_ID=se.UNS_Employee_ID "
				+ "					AND date_part('month', dp.PresenceDate)=date_part('month', pt.DateAcct) "
				+ "					AND date_part('day', dp.PresenceDate)=date_part('day', pt.DateAcct) "
				+ "					AND dp.PresenceStatus IN ('BLD', 'FLD', 'HLD', 'LMR'))";
		
		String updateShopEmp = "UPDATE UNS_ShopEmployee se SET ";
		String whereClauseUpdateEmpl = " WHERE se.UNS_ShopRecap_ID IN ("
				+ "		SELECT sr.UNS_ShopRecap_ID FROM UNS_ShopRecap sr "
				+ "		WHERE sr.UNS_SubregionRecap_ID=" + m_subregion.get_ID() +")";
		String onlyMasterSchedule = " AND se.IsMasterSchedule='Y'";
		
		String updateNotPresentSalesAmt = 
				updateShopEmp + "NotPresentSalesAmt=(" + notPresentSalesAmt + ")" 
				+ whereClauseUpdateEmpl + onlyMasterSchedule;
		
		int count = DB.executeUpdate(updateNotPresentSalesAmt, get_TrxName());
		log.fine("Updated employee's NotPresentSalesAmt: " + count + " records");
		
		// --- Update Sales Realization Amount
		
		String updateSalesRealization = 
				updateShopEmp + "NetSalesRealizationAmt=(" + netSalesAmt + ")" 
				+ whereClauseUpdateEmpl + onlyMasterSchedule;
		count = DB.executeUpdate(updateSalesRealization, get_TrxName());
		log.fine("Updated employee's SalesRealizationAmt: " + count + " records");
		
		// --- Update Net Sales Realization Amount (Sales Realization Amount - Not Present Sales Amount)
		
		String updateNetSalesRealization = updateShopEmp 
				+ "NetSalesRealizationAmt=(NetSalesRealizationAmt - NotPresentSalesAmt)" 
				+ whereClauseUpdateEmpl + onlyMasterSchedule;
		
		count = DB.executeUpdate(updateNetSalesRealization, get_TrxName());
		log.fine("Updated employee's NetSales Realization Amount: " + count + " records");
		
		// --- Update Cashier's session count.
		
		String cashierSession = "SELECT COUNT(uns_pos_session_id)"
				+ " FROM UNS_POS_Session ps"
				+ " INNER JOIN UNS_Shoprecap sr ON se.UNS_Shoprecap_ID=sr.UNS_ShopRecap_ID"
				+ "	INNER JOIN C_Period p ON sr.C_Period_ID=p.C_Period_ID"
				+ " INNER JOIN UNS_POSTerminal post ON post.UNS_POSTerminal_ID=ps.UNS_POSTerminal_ID"
				+ " WHERE ps.Cashier_ID=se.AD_User_ID AND post.Store_ID=se.Shop_ID"
				+ " 	AND ps.DateAcct BETWEEN p.StartDate AND p.EndDate";
		
		String tandemSession = "SELECT ROUND((COUNT (UNS_CashierTandem_ID) * " 
				+ m_globalConf.getCashierTandemPortion() + " / 100), 2)"
				+ " FROM UNS_CashierTandem ct "
				+ " INNER JOIN UNS_POS_Session ps ON ct.UNS_POS_Session_ID=ps.UNS_POS_Session_ID"
				+ " INNER JOIN UNS_Shoprecap sr ON se.UNS_Shoprecap_ID=sr.UNS_ShopRecap_ID"
				+ "	INNER JOIN C_Period p ON sr.C_Period_ID=p.C_Period_ID"
				+ " INNER JOIN UNS_POSTerminal post ON post.UNS_POSTerminal_ID=ps.UNS_POSTerminal_ID"
				+ " WHERE ps.Cashier_ID=se.AD_User_ID AND post.Store_ID=se.Shop_ID"
				+ " 	AND ct.IsLogin='Y' AND ct.IsMainCashier='N'"
				+ " 	AND ps.DateAcct BETWEEN p.StartDate AND p.EndDate";
		
		String updateCashierSession = 
				updateShopEmp + " CashierCount=(" + cashierSession + "), TandemCount=(" + tandemSession + ") " 
				+ whereClauseUpdateEmpl;
		
		count = DB.executeUpdate(updateCashierSession, get_TrxName());
		log.fine("Updated employee's cashier count: " + count + " records");
		
		// --- update employee's point
		String updatePoints = updateShopEmp 
				+ " Point=(CASE WHEN se.PositionType='" + MUNSShopEmployee.POSITIONTYPE_Staff + "' "
				+ "				THEN " + m_globalConf.getStaffPoint() 
				+ " 		WHEN se.PositionType='" + MUNSShopEmployee.POSITIONTYPE_ShopSupervisor + "' "
				+ "				THEN " + m_globalConf.getSupervisorPortion()
				+ " 		WHEN se.PositionType='" + MUNSShopEmployee.POSITIONTYPE_SeniorSupervisor + "' "
				+ "				THEN " + m_globalConf.getSeniorSupervisorPoint()
				+ " 		WHEN se.PositionType='" + MUNSShopEmployee.POSITIONTYPE_JuniorManager + "' "
				+ "				THEN " + m_globalConf.getJuniorManagerPoint()
				+ " 		WHEN se.PositionType='" + MUNSShopEmployee.POSITIONTYPE_Manager + "' "
				+ "				THEN " + m_globalConf.getManagerPoint()
				+ " 		ELSE 0 "
				+ "		  END)"
				+ whereClauseUpdateEmpl + onlyMasterSchedule;
		count = DB.executeUpdate(updatePoints, get_TrxName());		
		log.fine("Updated employee's point: " + count + " records");
		
		
		/* --------  Update ShopRecap's NetSalesRealizationAmt --------- */
		
		netSalesAmt = "SELECT COALESCE(SUM(pt.GrandTotal), 0) FROM UNS_POSTrx pt "
				+ " INNER JOIN C_Period p ON p.C_Period_ID=sr.C_Period_ID"
				+ " WHERE pt.DateAcct BETWEEN p.StartDate AND p.EndDate "
				+ "		AND pt.C_BPartner_ID=sr.Shop_ID";
		
		String update = "UPDATE UNS_ShopRecap sr SET NetSalesRealizationAmt=(" + netSalesAmt + ")"
				+ " WHERE sr.UNS_SubregionRecap_ID=" + m_subregion.get_ID();
		
		count = DB.executeUpdate(update, get_TrxName());
		log.fine("Updated shop's Net Sales Realization Incentive: " + count + " records");
		
		// --- Update ShopRecap of columns 
		// 	   GroupSalesIncentive, GroupCashierIncentive, TotalPoint, TotalCashierSession.
		update = "UPDATE UNS_ShopRecap sr "
				+ " SET GroupSalesIncentive=(NetSalesRealizationAmt * (?/100) * ((100 - (?+?))/100))," //1,2,3
				+ "		GroupCashierIncentive=(NetSalesRealizationAmt * (?/100) * (?/100))," //4, 5
				+ "     TotalPoint=(SELECT COALESCE(SUM(se.Point), 1) FROM UNS_ShopEmployee se "
				+ "					WHERE se.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID AND se.IsMasterSchedule='Y')"
				//+ "		TotalCashierSession=(SELECT SUM(CashierCount) FROM UNS_ShopEmployee se"
				//+ "							 WHERE se.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID) "
				+ " WHERE sr.UNS_SubregionRecap_ID=" + m_subregion.get_ID();
		
		count = DB.executeUpdateEx(update, 
				new Object[]{m_globalConf.getGroupPercent(), //1
				m_globalConf.getSupervisorPortion(), //2 
				m_globalConf.getCashierPortion(), //3
				m_globalConf.getGroupPercent(), //4
				m_globalConf.getCashierPortion()}, //5 
				get_TrxName());
		log.fine("Updated ShopRecap: " + count + " records");
		
		// -- Update Total Cashier Session on a shop.
		cashierSession = "SELECT COUNT(uns_pos_session_id)"
				+ " FROM UNS_POS_Session ps"
				+ " INNER JOIN UNS_POSTerminal trm ON ps.UNS_POSTerminal_ID=trm.UNS_POSTerminal_ID"
				+ "	INNER JOIN C_Period p ON sr.C_Period_ID=p.C_Period_ID"
				+ " WHERE trm.Store_ID=sr.Shop_ID "
				+ " 	AND ps.DateAcct BETWEEN p.StartDate AND p.EndDate";
		
		tandemSession = "SELECT ROUND((COUNT (UNS_CashierTandem_ID) * " 
				+ m_globalConf.getCashierTandemPortion() + " / 100), 2)"
				+ " FROM UNS_CashierTandem ct "
				+ " INNER JOIN UNS_POS_Session ps ON ct.UNS_POS_Session_ID=ps.UNS_POS_Session_ID"
				+ " INNER JOIN UNS_POSTerminal trm ON ps.UNS_POSTerminal_ID=trm.UNS_POSTerminal_ID"
				+ "	INNER JOIN C_Period p ON sr.C_Period_ID=p.C_Period_ID"
				+ " WHERE trm.Store_ID=sr.Shop_ID "
				+ " 	AND ct.IsLogin='Y' AND ct.IsMainCashier='N'"
				+ " 	AND ps.DateAcct BETWEEN p.StartDate AND p.EndDate";
		
		updateCashierSession = "UPDATE UNS_ShopRecap sr SET " 
				+ " TotalCashierSession=(" + cashierSession + "), TotalTandemCount=(" + tandemSession + ") "
				+ " WHERE UNS_SubregionRecap_ID=" + m_subregion.get_ID();
		
		count = DB.executeUpdate(updateCashierSession, get_TrxName());
		log.fine("Updated shop's Total Cashier Sessions: " + count + " records");
		
		
		
		// --------- UPDATE Employee's Incentives --------------
		
		// -- Update ShopEmployee's incentive for 'staff' positionType.
		String salesIncentive = 
				updateShopEmp + "SalesIncentiveAmt=(COALESCE(NetSalesRealizationAmt, 0) * (?/100)) " //1
				+ ", GroupIncentiveAmt=(CASE "
				+ "		WHEN NetSalesRealizationAmt >= (SELECT sr.MinIdvSalesTargetAmt FROM UNS_ShopRecap sr"
				+ "											 WHERE sr.UNS_ShopRecap_ID=se.UNS_ShopRecap_ID) THEN"
				+ "			((COALESCE(se.Point, 0) "
				+ "			  / (SELECT CASE WHEN sr.TotalPoint = 0 THEN 1 ELSE sr.TotalPoint END "
				+ "				 FROM UNS_ShopRecap sr WHERE sr.UNS_ShopRecap_ID=se.UNS_ShopRecap_ID)) "
				+ "			 * (SELECT sr.GroupSalesIncentive FROM UNS_ShopRecap sr"
				+ "				  WHERE sr.UNS_ShopRecap_ID=se.UNS_ShopRecap_ID))"
				+ "		ELSE 0 END)"
				+ ", CashierIncentiveAmt=((SELECT COALESCE(sr.GroupCashierIncentive, 0) FROM UNS_ShopRecap sr"
				+ "				  			WHERE sr.UNS_ShopRecap_ID=se.UNS_ShopRecap_ID)"
				+ "						  * ((COALESCE(se.CashierCount, 0) + COALESCE(se.TandemCount, 0))"
				+ "						  	 / (SELECT CASE WHEN sr.TotalCashierSession=0 AND sr.TotalTandemCount=0 THEN 1"
				+ "											ELSE (sr.TotalCashierSession + sr.TotalTandemCount) END"
				+ "								FROM UNS_ShopRecap sr"
				+ "				  				WHERE sr.UNS_ShopRecap_ID=se.UNS_ShopRecap_ID))) " 
				+ whereClauseUpdateEmpl + " AND se.PositionType=?"; //3
		
		count = DB.executeUpdateEx(salesIncentive,
							new Object[]{m_globalConf.getIndividuPercent(),
							MUNSShopEmployee.POSITIONTYPE_Staff},
							get_TrxName());
		log.fine("Updated shop-employee's Incentive: " + count + " records");

		
		/** --------- subregion net sales realization -----**/ 
		
		String sql = "SELECT SUM(NetSalesRealizationAmt) FROM UNS_ShopRecap"
				+ " WHERE UNS_SubregionRecap_ID=" + m_subregion.getUNS_SubregionRecap_ID();
		
		BigDecimal subSalesRealization = DB.getSQLValueBD(get_TrxName(), sql);
		m_subregion.setNetSalesRealizationAmt(subSalesRealization);
		
		BigDecimal subNetSalesIncentive = BigDecimal.ZERO;
		
		if(subSalesRealization.compareTo(m_subregion.getMinSubSalesTargetAmt()) >= 0)
			subNetSalesIncentive = subSalesRealization.multiply(
				m_globalConf.getGroupPercent().divide(Env.ONEHUNDRED, 4, RoundingMode.HALF_DOWN))
				.multiply(m_globalConf.getSupervisorPortion().divide(Env.ONEHUNDRED, 4, RoundingMode.HALF_DOWN));
		
		m_subregion.setNetSalesIncentiveAmt(subNetSalesIncentive);
		

		/** ---------- Update the supervisor & manager individual & group sales incentives ------------ **/
		
		String spvIncentive = 
				updateShopEmp + "SalesIncentiveAmt=(se.NetSalesRealizationAmt * (?/100)) " //1
				+ ", GroupIncentiveAmt=(? * (se.Point/(SELECT sr.TotalPoint FROM UNS_ShopRecap sr " //2
				+ "					    WHERE sr.UNS_ShopRecap_ID=se.uns_ShopRecap_ID))) " 
				+ whereClauseUpdateEmpl + " AND se.PositionType IN (?, ?, ?, ?)"; //3, 4, 5, 6
		
		count = DB.executeUpdateEx(spvIncentive, 
							new Object[]{m_globalConf.getIndividuPercent(), //1
							m_subregion.getNetSalesIncentiveAmt(), //2
							MUNSShopEmployee.POSITIONTYPE_ShopSupervisor, //3
							MUNSShopEmployee.POSITIONTYPE_SeniorSupervisor, //4
							MUNSShopEmployee.POSITIONTYPE_JuniorManager, //5
							MUNSShopEmployee.POSITIONTYPE_Manager}, //6
							get_TrxName());
		log.fine("Updated spv-employee's Incentive: " + count);
		
		// Round floor all incentive numbers
		String roundFloor = "UPDATE UNS_ShopEmployee SET "
				+ "  SalesIncentiveAmt=FLOOR(SalesIncentiveAmt)"
				+ ", GroupIncentiveAmt=FLOOR(GroupIncentiveAmt)"
				+ ", CashierIncentiveAmt=FLOOR(CashierIncentiveAmt)"
				+ ", NetSalesRealizationAmt=FLOOR(NetSalesRealizationAmt)"
				+ ", NotPresentSalesAmt=FLOOR(NotPresentSalesAmt)"
				+ "  WHERE UNS_ShopRecap_ID IN ("
				+ "		SELECT UNS_ShopRecap_ID FROM UNS_ShopRecap "
				+ "		WHERE UNS_SubregionRecap_ID=" + m_subregion.getUNS_SubregionRecap_ID() + ")";
		count = DB.executeUpdateEx(roundFloor, get_TrxName());
		
		roundFloor = "UPDATE UNS_ShopRecap SET "
				+ "  NetSalesRealizationAmt=FLOOR(NetSalesRealizationAmt)"
				+ ", GroupSalesIncentive=FLOOR(GroupSalesIncentive)"
				+ ", GroupCashierIncentive=FLOOR(GroupCashierIncentive)"
				+ "	WHERE UNS_SubregionRecap_ID=" + m_subregion.getUNS_SubregionRecap_ID();
		count = DB.executeUpdateEx(roundFloor, get_TrxName());
		
		m_subregion.setNetSalesRealizationAmt(
				m_subregion.getNetSalesRealizationAmt().setScale(0, BigDecimal.ROUND_FLOOR));
		m_subregion.setNetSalesIncentiveAmt(
				m_subregion.getNetSalesIncentiveAmt().setScale(0, BigDecimal.ROUND_FLOOR));
	}
	
	/**
	 * 
	 * @param subregionRecap
	 */
	private void setPushMoneyAmt(X_UNS_SubregionRecap subregionRecap) 
	{
		String sql = "update uns_postrxline ptl"
				+ " set pushmoneyamt="
				+ "		(select (getpushmoneyamount(ptl.m_product_id, ptrx.dateacct, div.uns_division_id) * ptl.qtyentered)"
				+ "  	 from uns_postrx ptrx "
				+ "		 inner join uns_pos_session ps on ptrx.uns_pos_session_id=ps.uns_pos_session_id"
				+ "		 inner join uns_posterminal pterm on ps.uns_posterminal_id=pterm.uns_posterminal_id"
				+ "		 inner join uns_division div on div.storetype=pterm.storetype"
				+ "		 where ptrx.uns_postrx_id=ptl.uns_postrx_id) "
				+ " where ptl.uns_postrx_id in ("
				+ "		select pt.uns_postrx_id from uns_postrx pt"
				+ "	 	where pt.dateacct between " + DB.TO_DATE(subregionRecap.getC_Period().getStartDate()) 
				+ " 		and " + DB.TO_DATE(subregionRecap.getC_Period().getEndDate()) 
				+ " 		and pt.c_bpartner_id in ("
				+ "				select shop.c_bpartner_id from c_bpartner shop "
				+ "				where shop.c_bp_group_id=" + subregionRecap.getSubregion_ID()
				+ "				)"
				+ ")";
		DB.executeUpdate(sql, get_TrxName());
		
		sql = "update uns_shopemployee se"
				+ " set pushmoneyamt = (select FLOOR(sum(ptl.pushmoneyamt))"
				+ " 	from uns_postrxline ptl, uns_postrx pt"
				+ " 	where ptl.uns_postrx_id=pt.uns_postrx_id"
				+ " 		AND pt.salesrep_id=se.ad_user_id"
				+ "			AND pt.dateacct between " + DB.TO_DATE(subregionRecap.getC_Period().getStartDate()) 
				+ " 		AND " + DB.TO_DATE(subregionRecap.getC_Period().getEndDate())
			//	+ "			AND pt.C_BPartner_ID=se.Shop_ID"
				+ "		) "
				+ " where UNS_ShopRecap_ID IN (SELECT UNS_ShopRecap_ID FROM UNS_ShopRecap "
				+ "							WHERE uns_subregionrecap_id = " + subregionRecap.get_ID() + ")";
		DB.executeUpdate(sql, get_TrxName());
	} //setPushMoneyAmt
	
	/**
	 * 
	 */
	private void distributeServiceCharges()
	{
		String sql = "UPDATE UNS_ShopRecap sr SET "
				+ "  TotalPoint=("
				+ "			SELECT COALESCE(SUM(staff.point), 0) FROM UNS_StaffSrvChgRecap staff"
				+ "			WHERE staff.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID "
				+ "				AND staff.PositionType IS NOT NULL AND staff.PositionType<>'SUPT')"
				+ ", SupportPoints=("
				+ "			SELECT COALESCE(SUM(staff.point), 0) FROM UNS_StaffSrvChgRecap staff"
				+ "			WHERE staff.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID "
				+ "				AND (staff.PositionType IS NULL OR staff.PositionType='SUPT'))"
				+ "	WHERE UNS_SubregionRecap_ID=?";
		int count = DB.executeUpdateEx(sql, new Object[]{m_subregion.getUNS_SubregionRecap_ID()}, get_TrxName());
		
		sql = "UPDATE UNS_StaffSrvChgRecap sscr SET ServiceChargeAmt="
				+ "		(SELECT (sscr.Point / CASE WHEN sr.TotalPoint > 0 THEN sr.TotalPoint ELSE 1 END)"
				+ "		  	* sr.EmployeePortion"
				+ "  	 FROM UNS_ShopRecap sr WHERE sr.UNS_ShopRecap_ID=sscr.UNS_ShopRecap_ID)"
				+ " WHERE sscr.UNS_ShopRecap_ID IN ("
				+ "		  SELECT UNS_ShopRecap_ID FROM UNS_ShopRecap WHERE UNS_SubregionRecap_ID=?)"
				+ "		AND (sscr.PositionType IS NOT NULL AND sscr.PositionType<>'SUPT')";
		count = DB.executeUpdate(sql, m_subregion.getUNS_SubregionRecap_ID(), get_TrxName());
		log.fine("Updated employee's ServiceChargeAmt: " + count + " records");
		
		sql = "UPDATE UNS_StaffSrvChgRecap sscr SET ServiceChargeAmt="
				+ "		(SELECT (sscr.Point / CASE WHEN sr.SupportPoints > 0 THEN sr.SupportPoints ELSE 1 END)"
				+ "		  	* sr.SupportPortion"
				+ "  	 FROM UNS_ShopRecap sr WHERE sr.UNS_ShopRecap_ID=sscr.UNS_ShopRecap_ID)"
				+ " WHERE sscr.UNS_ShopRecap_ID IN ("
				+ "		  SELECT UNS_ShopRecap_ID FROM UNS_ShopRecap WHERE UNS_SubregionRecap_ID=?)"
				+ "		AND (sscr.PositionType IS NULL OR sscr.PositionType='SUPT')";
		count = DB.executeUpdate(sql, m_subregion.getUNS_SubregionRecap_ID(), get_TrxName());
		log.fine("Updated employee's ServiceChargeAmt: " + count + " records");
	}
	
} //CalculateSalesIncentive