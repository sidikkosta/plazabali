package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MEXPProcessor;
import org.compiere.model.MEXPProcessorParameter;
import org.compiere.model.MIMPProcessor;
import org.compiere.model.X_IMP_ProcessorParameter;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

public class UNSReplicationConfig extends SvrProcess {

	private static String LOCATION_DB_URL = "LOCATION_DB_URL";
	private static String QueueName = "QUEUENAME";
	private static String TopicName = "TOPICNAME";
	private static String ClientID = "CLIENTID";
	private static String SubscriptionName = "SUBSCRIPTIONNAME";
	private boolean isExp = false;
	private int p_expProcessor_ID = 0;
	private int p_impProcessor_ID = 0;
	private String p_host = null;
	private int p_port = 0;
	private String p_account = null;
	private String p_password = null;
	private String p_queueName = null;
	private String p_clientID = null;
	private String p_locURL = null;
	private MIMPProcessor m_ImpProcessor = null;
	private MEXPProcessor m_EXProcessor = null;
	
	public UNSReplicationConfig() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		
		for(ProcessInfoParameter param : params)
		{
			String name = param.getParameterName();
			if(name.equals("IsExportProcessor"))
				isExp = param.getParameterAsBoolean();
			else if(name.equals("EXP_Processor_ID"))
				p_expProcessor_ID = param.getParameterAsInt();
			else if(name.equals("IMP_Processor_ID"))
				p_impProcessor_ID = param.getParameterAsInt();
			else if(name.equals("Host"))
				p_host = param.getParameterAsString();
			else if(name.equals("Port"))
				p_port = param.getParameterAsInt();
			else if(name.equals("Account"))
				p_account = param.getParameterAsString();
			else if(name.equals("PasswordInfo"))
				p_password = param.getParameterAsString();
			else if(name.equals("QueueName"))
				p_queueName = param.getParameterAsString();
			else if(name.equals("ClientID"))
				p_clientID = param.getParameterAsString();
			else if(name.equals(LOCATION_DB_URL))
				p_locURL = param.getParameterAsString();
			else
				throw new AdempiereException("Unknown parameter name :"+name);
		}
		
	}

	@Override
	protected String doIt() throws Exception {
		
		if(isExp)
		{
			if(p_expProcessor_ID <= 0)
				throw new AdempiereException("Mandatory Export Processor");
			else if(p_locURL == null)
				throw new AdempiereException("Location URL");
		}
		else if(!isExp && p_impProcessor_ID <= 0)
		{
			throw new AdempiereException("Mandatory Import Processor");
		}
		
		if(p_queueName == null || p_queueName.length() == 0)
			throw new AdempiereException("Mandatory field Queue Name");
		
		if(p_clientID == null || p_clientID.length() == 0)
			throw new AdempiereException("Mandatory field Client ID");
		
		if(isExp)
		{
			m_EXProcessor = new MEXPProcessor(getCtx(), p_expProcessor_ID, get_TrxName());
			if(m_EXProcessor == null)
				throw new AdempiereException("Cannot found Export Processor");
			
			m_EXProcessor.setHost(p_host);
			m_EXProcessor.setPort(p_port);
			m_EXProcessor.setAccount(p_account);
			m_EXProcessor.setPasswordInfo(p_password);
			
			if(!m_EXProcessor.isActive())
				m_EXProcessor.setIsActive(true);
			
			m_EXProcessor.saveEx();
			
			MEXPProcessorParameter[] expParams = m_EXProcessor.getParameters();
			for(int i=0; i<expParams.length; i++)
			{
				String value = null;
				if(expParams[i].getValue().toUpperCase().equals(QueueName)
						|| expParams[i].getValue().toUpperCase().equals(TopicName))
					value = p_queueName;
				else if(expParams[i].getValue().toUpperCase().equals(ClientID))
					value = p_clientID;
				else
					continue;
				
				expParams[i].setParameterValue(value);
				expParams[i].saveEx();
			}
			
			//update system configurator of LOCATION_DB_URL
			String sqlUpdate = "UPDATE AD_SysConfig SET Value = ?"
					+ " WHERE Name = ? AND IsActive = 'Y'";
			boolean ok = DB.executeUpdateEx(
					sqlUpdate, new Object[]{p_locURL, LOCATION_DB_URL}, get_TrxName()) != -1;
			if(!ok)
				throw new AdempiereException("Error when try to update Location URL");
		}
		else
		{
			m_ImpProcessor = new MIMPProcessor(getCtx(), p_impProcessor_ID, get_TrxName());
			if(m_ImpProcessor == null)
				throw new AdempiereException("Cannot found Import Processor");
			
			m_ImpProcessor.setHost(p_host);
			m_ImpProcessor.setPort(p_port);
			m_ImpProcessor.setAccount(p_account);
			m_ImpProcessor.setPasswordInfo(p_password);
			
			if(!m_ImpProcessor.isActive())
				m_ImpProcessor.setIsActive(true);
			
			m_ImpProcessor.saveEx();
			
			X_IMP_ProcessorParameter[] impParams = m_ImpProcessor.getIMP_ProcessorParameters(get_TrxName());
			for(int i=0; i<impParams.length ;i++)
			{
				String value = null;
				if(impParams[i].getValue().toUpperCase().equals(QueueName)
						|| impParams[i].getValue().toUpperCase().equals(TopicName))
					value = p_queueName;
				else if(impParams[i].getValue().toUpperCase().equals(ClientID)
						|| impParams[i].getValue().toUpperCase().equals(SubscriptionName))
					value = p_clientID;
				else
					continue;
				
				impParams[i].setParameterValue(value);
				impParams[i].saveEx();
			}
		}
		
		return "Success";
	}

}
