/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import jxl.Sheet;
import jxl.Workbook;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MLocator;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTransaction;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MInOut;
import com.unicore.base.model.MMovement;
import com.unicore.base.model.MMovementConfirm;
import com.uns.util.MessageBox;

/**
 * @author Burhani Adam
 *
 */
public class FixingZeroASIID extends SvrProcess {

	/**
	 * 
	 */
	public FixingZeroASIID() {
		// TODO Auto-generated constructor stub
	}
	
	private String p_File = null;
	private int m_prevASIID = 0;
	private int m_nextASIID = 0;
	private Timestamp m_dmp = null;
	private BigDecimal m_Qty = Env.ZERO;
	Workbook wb = null;
	Sheet sheet = null;
	private boolean m_needSeeStorage = false;
	private Hashtable<String, Integer> movementMap = new Hashtable<>();
	private Hashtable<Integer, String> RePostMap = new Hashtable<>();
	private IProcessUI statusWa;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		p_File = params[0].getParameterAsString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		statusWa = Env.getProcessUI(getCtx());
		try {
			wb = Workbook.getWorkbook(new File(p_File));
		}
		catch (Exception ex) {
			throw new AdempiereException("Error while opening file: " + ex.getMessage());
		}
		sheet = wb.getSheet("Sheet2");
		
		statusWa.statusUpdate("Initialize Transactions...");
		MTransaction[] trx = getTrxs();
		for(int i=0;i<trx.length;i++)
		{
			if(trx[i].getM_MovementLine_ID() > 0 && trx[i].getM_MovementLine().getM_Movement().getDocStatus().equals("RE"))
				continue;
			trx[i].load(get_TrxName());
			System.out.println(trx[i].get_ID());
			boolean isReversal = false;
			statusWa.statusUpdate("Trx ke " + (i+1) + " dari "+ trx.length);
			if(trx[i].getM_MovementLine_ID() > 0)
			{
				int style = movementMap.get(trx[i].getM_MovementLine_ID() + ";" + trx[i].getM_AttributeSetInstance_ID()) == null ?
						0 : movementMap.get(trx[i].getM_MovementLine_ID() + ";" + trx[i].getM_AttributeSetInstance_ID());
				if(style == 69)
					continue;
			}
			m_needSeeStorage = true;
			if((trx[i].getMovementType().contains("-") && trx[i].getMovementQty().signum() > 0)
					|| (trx[i].getMovementType().contains("+") && trx[i].getMovementQty().signum() < 0))
				isReversal = true;
			if(trx[i].getM_MovementLine_ID() > 0)
				RePostMap.put(trx[i].getM_MovementLine().getM_Movement_ID(), "Movement");
			else if(trx[i].getM_InOutLine_ID() > 0)
				RePostMap.put(trx[i].getM_InOutLine().getM_InOut_ID(), "InOut");
			tryToXLSFile(sheet, trx[i], isReversal);
			if(m_needSeeStorage)
				tryToStorage(trx[i], isReversal);
		}
		
		RePostDocs();
		
		return "Success execute " + trx.length + " transactions, and repost " + RePostMap.size() + " documents.";
	}
	
	private MTransaction[] getTrxs()
	{
		List<MTransaction> list = new ArrayList<>();
		
		String sql = "SELECT t.* FROM M_Transaction t WHERE t.M_AttributeSetInstance_ID = 0"
				+ " AND (t.M_InOutLine_ID > 0 OR t.M_MovementLine_ID > 0)"
				+ " AND (CASE WHEN t.M_InOutLine_ID > 0 THEN EXISTS (SELECT 1"
				+ " FROM M_InOutLineMA ma WHERE ma.M_InOutLine_ID = t.M_InOutLine_ID) ELSE 1=1 END)"
				+ " AND (CASE WHEN t.M_MovementLine_ID > 0 THEN EXISTS (SELECT 1"
				+ " FROM M_MovementLineMA ma WHERE ma.M_MovementLine_ID = t.M_MovementLine_ID)"
				+ " AND EXISTS (SELECT 1 FROM M_Movement m WHERE m.M_Movement_ID = (SELECT l.M_Movement_ID"
				+ " FROM M_MovementLine l WHERE l.M_MovementLine_ID = t.M_MovementLine_ID) AND m.DocStatus IN ('CO', 'CL'))"
				+ " ELSE 1=1 END)"
//				+ " AND M_MovementLine_ID = 1027104"
//				+ " AND M_InOutLine_ID = 1028122"
				+ " AND MovementQty <> 0"
				+ " ORDER BY t.MovementDate ASC, t.Created ASC";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				list.add(new MTransaction(getCtx(), rs, get_TrxName()));
			}
		} catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		
		return list.toArray(new MTransaction[list.size()]);
	}
	
	private void tryToXLSFile(Sheet sheet, MTransaction trx, boolean isReversal)
	{
		if(trx.getM_MovementLine_ID() > 0)
		{
			for(int i=1;i<sheet.getRows();i++)
			{
				Object skip = sheet.getCell(16, i).getContents();
				if(skip == null || !skip.equals(""))
					continue;
				Object moveLine = sheet.getCell(1, i).getContents();
				if(moveLine.equals(""))
					break;
				int moveLineID = Integer.parseInt(moveLine.toString());
				if(moveLineID != trx.getM_MovementLine_ID())
					continue;
				Object prevASI = sheet.getCell(9, i).getContents();
				Object nextASI = sheet.getCell(12, i).getContents();
				Object nextQty = sheet.getCell(13, i).getContents();
				m_Qty = BigDecimal.valueOf(new Double(nextQty.toString()));
				if(nextASI != null && !nextASI.equals(""))
					findASI(nextASI.toString(), i);
				else
					continue;
				if((prevASI == null || prevASI.equals("")) && m_nextASIID > 0)
				{
					InsertToMA((org.compiere.model.MMovementLine) trx.getM_MovementLine(), m_nextASIID, m_dmp, m_Qty, isReversal, true);
					CreateOrUpdateTrx((org.compiere.model.MMovementLine) trx.getM_MovementLine(), trx,
							-1, m_nextASIID, m_Qty, true, isReversal);
				}
				else
				{
					m_prevASIID = Integer.parseInt(prevASI.toString());
					UpdateMA((org.compiere.model.MMovementLine) trx.getM_MovementLine(),
							m_prevASIID, m_nextASIID, m_dmp, m_Qty, isReversal, true);
					CreateOrUpdateTrx((org.compiere.model.MMovementLine) trx.getM_MovementLine(), trx,
							m_prevASIID, m_nextASIID, m_Qty, true, isReversal);
				}
				m_needSeeStorage = false;
			}
		}
	}
	
	private void tryToStorage(MTransaction trx, boolean isReversal)
	{
		MStorageOnHand[] soh = getOfLocator(getCtx(), trx.getM_Product_ID(),
				trx.getM_Locator_ID(), get_TrxName(), false);
		BigDecimal qtyToMove = trx.getMovementQty();
		boolean updateOnly = true;
		PO po = null;
		boolean isMovement = false;
		if(trx.getM_MovementLine_ID() > 0)
		{
			po = (org.compiere.model.MMovementLine) trx.getM_MovementLine();
			isMovement = true;
		}
		else if(trx.getM_InOutLine_ID() > 0)
			po = (org.compiere.model.MInOutLine) trx.getM_InOutLine();
		for(int i=0;i<soh.length;i++)
		{
			if(soh[i].getM_AttributeSetInstance_ID() == 0)
				continue;
			soh[i].setQtyOnHand(soh[i].getQtyOnHand().abs());
			if((i+1) == soh.length && soh[i].getQtyOnHand().compareTo(qtyToMove) == -1)
			{
				soh[i].setQtyOnHand(qtyToMove);
			}
			if(soh[i].getQtyOnHand().compareTo(qtyToMove) >= 0)
			{
				CreateOrUpdateTrx(po, trx, updateOnly ? 0 : -1 , soh[i].getM_AttributeSetInstance_ID(), qtyToMove,
						isMovement, isReversal);
				if(updateOnly)
					UpdateMA(po, 0, soh[i].getM_AttributeSetInstance_ID(), soh[i].getM_AttributeSetInstance().getCreated()
							, qtyToMove, isReversal, false);
				else
					InsertToMA(po, soh[i].getM_AttributeSetInstance_ID(), soh[i].getM_AttributeSetInstance().getCreated(),
							qtyToMove, isReversal, false);
					
				qtyToMove = Env.ZERO;
			}
			else
			{
				CreateOrUpdateTrx(po, trx, updateOnly ? 0 : -1 , soh[i].getM_AttributeSetInstance_ID(), soh[i].getQtyOnHand(),
						isMovement, isReversal);
				qtyToMove = qtyToMove.subtract(soh[i].getQtyOnHand().abs());
				if(updateOnly)
					UpdateMA(po, 0, soh[i].getM_AttributeSetInstance_ID(), soh[i].getM_AttributeSetInstance().getCreated()
							, soh[i].getQtyOnHand(), isReversal, false);
				else
					InsertToMA(po, soh[i].getM_AttributeSetInstance_ID(), soh[i].getM_AttributeSetInstance().getCreated(),
							soh[i].getQtyOnHand(), isReversal, false);
			}
			
			if(qtyToMove.compareTo(Env.ZERO) == 0)
				break;
			
			updateOnly = false;
		}
		if(qtyToMove.signum() != 0)
		{
			soh = getOfLocator(getCtx(), trx.getM_Product_ID(),
					trx.getM_Locator_ID(), get_TrxName(), true);
			if(soh.length > 0)
			{
				CreateOrUpdateTrx(po, trx, updateOnly ? 0 : -1 , soh[0].getM_AttributeSetInstance_ID(), qtyToMove,
						isMovement, isReversal);
	
				if(updateOnly)
					UpdateMA(po, 0, soh[0].getM_AttributeSetInstance_ID(), soh[0].getM_AttributeSetInstance().getCreated()
							, qtyToMove, isReversal, false);
				else
					InsertToMA(po, soh[0].getM_AttributeSetInstance_ID(), soh[0].getM_AttributeSetInstance().getCreated(),
							qtyToMove, isReversal, false);
			}
		}
	}
	
	private void InsertToMA(PO po, int M_AttributeSetInstance_ID, Timestamp DateMaterialPolicy, BigDecimal qty, 
			boolean isReversal, boolean isFromExcel)
	{
		String sql = "SELECT COUNT(*) FROM " + po.get_TableName() + "MA"
				+ " WHERE M_AttributeSetInstance_ID = ? AND DateMaterialPolicy = ?"
				+ " AND " + po.get_TableName() + "_ID = ?";
		qty = qty.abs();
		boolean Exists = DB.getSQLValue(get_TrxName(), sql, M_AttributeSetInstance_ID, DateMaterialPolicy, po.get_ID()) > 0;
		if(Exists)
		{
			UpdateMA(po, M_AttributeSetInstance_ID, M_AttributeSetInstance_ID, DateMaterialPolicy, qty, isReversal, isFromExcel);
			return;
		}
		sql = "INSERT INTO " + po.get_TableName() + "MA (AD_Client_ID, AD_Org_ID, Created,"
				+ " CreatedBy, Updated, UpdatedBy, IsActive, M_MovementLine_ID,"
				+ " M_AttributeSetInstance_ID, M_MovementLineMA_UU, DateMaterialPolicy,"
				+ " MovementQty) VALUES (1000015, 1000054, '1991-01-06 06:01:06', 1001075,"
				+ " '1991-01-06 06:01:06', 1001075, 'Y', ?, ?, uuid_generate_v4(), ?, ?)";
		if(DB.executeUpdate(sql, new Object[]{
				po.get_ID(), M_AttributeSetInstance_ID, DateMaterialPolicy, isReversal ? qty.negate() : qty
		}, false, get_TrxName()) < 1)
			throw new AdempiereException("Failed..!!");
	}
	
	private void UpdateMA(PO po, int OLD_AttributeSetInstance_ID, 
			int M_AttributeSetInstance_ID, Timestamp DateMaterialPolicy, BigDecimal qty
			, boolean isReversal, boolean isFromExcel)
	{
		String sql = "SELECT MovementQty FROM " + po.get_TableName() + "MA"
				+ " WHERE M_AttributeSetInstance_ID = ? AND DateMaterialPolicy = ?"
				+ " AND " + po.get_TableName() + "_ID = ?";
		BigDecimal qtyExists = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			M_AttributeSetInstance_ID, DateMaterialPolicy, po.get_ID()
			});
		qty = qty.abs();
		if(qtyExists != null)
		{
			BigDecimal qtyAll = isFromExcel ? qty : qtyExists.add(qty);
			sql = "UPDATE " + po.get_TableName() + "MA SET MovementQty = ?, Created = ?::timestamp WHERE " + po.get_TableName() + "_ID = ?"
							+ " AND M_AttributeSetInstance_ID = ?";
			if(DB.executeUpdate(sql, new Object[]{
					Env.ZERO, "1991-01-06 06:01:06", po.get_ID(),
							OLD_AttributeSetInstance_ID
			}, false, get_TrxName()) < 1)
				throw new AdempiereException(CLogger.retrieveErrorString("Failed..!!"));
			if(DB.executeUpdate(sql, new Object[]{
					isReversal ? 
							qtyAll.negate() : qtyAll, "1991-01-06 06:01:06", po.get_ID(),
							M_AttributeSetInstance_ID
			}, false, get_TrxName()) < 1)
				throw new AdempiereException(CLogger.retrieveErrorString("Failed..!!"));
		}
		else
		{
			sql = "UPDATE " + po.get_TableName() + "MA SET M_AttributeSetInstance_ID = ?,"
					+ " DateMaterialPolicy = ?, MovementQty = ?, Created = ?::timestamp WHERE " + po.get_TableName() + "_ID = ?"
							+ " AND M_AttributeSetInstance_ID = ?";
			if(DB.executeUpdate(sql, new Object[]{
					M_AttributeSetInstance_ID, DateMaterialPolicy, isReversal ? qty.negate() : qty, "1991-01-06 06:01:06",
							po.get_ID(), OLD_AttributeSetInstance_ID
			}, false, get_TrxName()) < 1)
				throw new AdempiereException(CLogger.retrieveErrorString("Failed..!!"));
		}
	}
	
	private void findASI(String Description, int row)
	{
		String sql = "SELECT M_AttributeSetInstance_ID::integer, DATE_TRUNC('Day', Created)"
				+ " FROM M_AttributeSetInstance WHERE Description = ?";
		List<Object> oo = DB.getSQLValueObjectsEx(get_TrxName(), sql, Description);
		if(oo == null)
		{
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), "ETBB Code " + Description
					+ " Not Found. Retry ?", "ETBB Code Not Found.!", MessageBox.YESNO, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_NO)
				return;
			else
			{
				wb.close();
				try {
					wb = Workbook.getWorkbook(new File(p_File));
				} catch (Exception e) {
					e.printStackTrace();
				}
				sheet = wb.getSheet("Sheet2");
				Object asi = sheet.getCell(12, row).getContents();
				findASI(asi.toString(), row);
			}
		}
		else
		{
			m_nextASIID = (int) oo.get(0);
			m_dmp = (Timestamp) oo.get(1);
		}
	}
	
	private void CreateOrUpdateTrx(PO po, MTransaction trx, int OLD_AttributeSetInstance_ID, 
			int M_AttributeSetInstance_ID, BigDecimal qty, boolean isMovement, boolean isReversal)
	{
		String sql = "";
		if(isMovement)
		{
			qty = qty.abs();
			org.compiere.model.MMovementLine line = ((org.compiere.model.MMovementLine) po);
			org.compiere.model.MMovement move = new MMovement(getCtx(), line.getM_Movement_ID(), get_TrxName());
			String sql_ = "SELECT COUNT(*) FROM M_MovementConfirm WHERE M_Movement_ID = ?"
					+ " AND DocStatus IN ('CO', 'CL')";
			boolean isConfirm = DB.getSQLValue(get_TrxName(), sql_, move.get_ID()) > 0;
//			if(confirm.length > 0 && (confirm[0].getDocStatus().equals("CO")
//					|| confirm[0].getDocStatus().equals("CL")))
//			{
//				isConfirm = true;
//			}
			MLocator loc = MWarehouse.getInTransitForWhs(getCtx(), line.getM_LocatorTo().getM_Warehouse_ID());
			if(OLD_AttributeSetInstance_ID < 0)
			{
				//CreateLocatorFrom
				sql = "INSERT INTO M_Transaction (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
						+ " Updated, UpdatedBy, IsActive, M_Transaction_ID, M_Transaction_UU,"
						+ " M_AttributeSetInstance_ID, M_Locator_ID, MovementQty," + po.get_TableName() + "_ID,"
						+ " M_Product_ID, MovementDate, MovementType) VALUES (1000015, 1000054, '1991-01-06 06:01:06',"
						+ " 1001075, '1991-01-06 06:01:06', 1001075, 'Y', getnextid('M_Transaction'),"
						+ " uuid_generate_v4(), ?, ?, ?, ?, ?, ?, ?)";
				if(DB.executeUpdate(sql, new Object[]{
					M_AttributeSetInstance_ID, line.getM_Locator_ID(), isReversal ? qty : qty.negate(), trx.get_Value(po.get_TableName() + "_ID"),
					trx.getM_Product_ID(), trx.getMovementDate(), "M-"
				}, false, get_TrxName()) < 1)
					throw new AdempiereException("Failed.!");
				CreateOrUpdateStorage(M_AttributeSetInstance_ID, trx.getM_Product_ID(),
						line.getM_Locator_ID(), isReversal ? qty : qty.negate());
				
				//CreateLocatorIntransit+
				sql = "INSERT INTO M_Transaction (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
						+ " Updated, UpdatedBy, IsActive, M_Transaction_ID, M_Transaction_UU,"
						+ " M_AttributeSetInstance_ID, M_Locator_ID, MovementQty," + po.get_TableName() + "_ID,"
						+ " M_Product_ID, MovementDate, MovementType) VALUES (1000015, 1000054, '1991-01-06 06:01:06',"
						+ " 1001075, '1991-01-06 06:01:06', 1001075, 'Y', getnextid('M_Transaction'),"
						+ " uuid_generate_v4(), ?, ?, ?, ?, ?, ?, ?)";
				if(DB.executeUpdate(sql, new Object[]{
					M_AttributeSetInstance_ID, loc.getM_Locator_ID(), isReversal ? qty.negate() : qty, trx.get_Value(po.get_TableName() + "_ID"),
					trx.getM_Product_ID(), trx.getMovementDate(), "M+"
				}, false, get_TrxName()) < 1)
					throw new AdempiereException("Failed.!");
				CreateOrUpdateStorage(M_AttributeSetInstance_ID, trx.getM_Product_ID(),
						loc.getM_Locator_ID(), isReversal ? qty.negate() : qty);
				
				if(isConfirm)
				{
					//CreateLocatorIntransit-
					sql = "INSERT INTO M_Transaction (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
							+ " Updated, UpdatedBy, IsActive, M_Transaction_ID, M_Transaction_UU,"
							+ " M_AttributeSetInstance_ID, M_Locator_ID, MovementQty," + po.get_TableName() + "_ID,"
							+ " M_Product_ID, MovementDate, MovementType) VALUES (1000015, 1000054, '1991-01-06 06:01:06',"
							+ " 1001075, '1991-01-06 06:01:06', 1001075, 'Y', getnextid('M_Transaction'),"
							+ " uuid_generate_v4(), ?, ?, ?, ?, ?, ?, ?)";
					if(DB.executeUpdate(sql, new Object[]{
						M_AttributeSetInstance_ID, loc.getM_Locator_ID(), isReversal ? qty : qty.negate(), trx.get_Value(po.get_TableName() + "_ID"),
						trx.getM_Product_ID(), trx.getMovementDate(), "M-"
					}, false, get_TrxName()) < 1)
						throw new AdempiereException("Failed.!");
					CreateOrUpdateStorage(M_AttributeSetInstance_ID, trx.getM_Product_ID(),
							loc.getM_Locator_ID(), isReversal ? qty : qty.negate());
					
					//CreateLocatorTo
					sql = "INSERT INTO M_Transaction (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
							+ " Updated, UpdatedBy, IsActive, M_Transaction_ID, M_Transaction_UU,"
							+ " M_AttributeSetInstance_ID, M_Locator_ID, MovementQty," + po.get_TableName() + "_ID,"
							+ " M_Product_ID, MovementDate, MovementType) VALUES (1000015, 1000054, '1991-01-06 06:01:06',"
							+ " 1001075, '1991-01-06 06:01:06', 1001075, 'Y', getnextid('M_Transaction'),"
							+ " uuid_generate_v4(), ?, ?, ?, ?, ?, ?, ?)";
					if(DB.executeUpdate(sql, new Object[]{
						M_AttributeSetInstance_ID, line.getM_LocatorTo_ID(), isReversal ? qty.negate() : qty, trx.get_Value(po.get_TableName() + "_ID"),
						trx.getM_Product_ID(), trx.getMovementDate(), "M+"
					}, false, get_TrxName()) < 1)
						throw new AdempiereException("Failed.!");
					CreateOrUpdateStorage(M_AttributeSetInstance_ID, trx.getM_Product_ID(),
							line.getM_LocatorTo_ID(), isReversal ? qty.negate() : qty);
				}
			}
			else
			{
				//UpdateLocatorFrom
				sql = "UPDATE M_Transaction SET M_AttributeSetInstance_ID = ?, MovementQty = ?, Created = ?::timestamp"
						+ " WHERE M_Locator_ID = ? AND M_MovementLine_ID = ? AND MovementType = ?"
						+ " AND M_AttributeSetInstance_ID = ?";
				if(DB.executeUpdate(sql, new Object[]{
						M_AttributeSetInstance_ID, isReversal ? qty : qty.negate(), "1991-01-06 06:01:06", line.getM_Locator_ID(),
						line.get_ID(), "M-", OLD_AttributeSetInstance_ID
				}, false, get_TrxName()) < 1)
					throw new AdempiereException("Failed.!");
				
				//UpdateLocatorIntransit+
				sql = "UPDATE M_Transaction SET M_AttributeSetInstance_ID = ?, MovementQty = ?, Created = ?::timestamp"
						+ " WHERE M_Locator_ID = ? AND M_MovementLine_ID = ? AND MovementType = ?"
						+ " AND M_AttributeSetInstance_ID = ?";
				if(DB.executeUpdate(sql, new Object[]{
						M_AttributeSetInstance_ID, isReversal ? qty.negate() : qty, "1991-01-06 06:01:06", loc.getM_Locator_ID(),
						line.get_ID(), "M+", OLD_AttributeSetInstance_ID
				}, false, get_TrxName()) < 1)
					throw new AdempiereException("Failed.!");
				
				if(isConfirm)
				{
					//UpdateLocatorIntransit-
					sql = "UPDATE M_Transaction SET M_AttributeSetInstance_ID = ?, MovementQty = ?, Created = ?::timestamp"
							+ " WHERE M_Locator_ID = ? AND M_MovementLine_ID = ? AND MovementType = ?"
							+ " AND M_AttributeSetInstance_ID = ?";
					if(DB.executeUpdate(sql, new Object[]{
							M_AttributeSetInstance_ID, isReversal ? qty : qty.negate(), "1991-01-06 06:01:06", loc.getM_Locator_ID(),
							line.get_ID(), "M-", OLD_AttributeSetInstance_ID
					}, false, get_TrxName()) < 1)
						throw new AdempiereException("Failed.!");
					
					//UpdateLocatorTo
					sql = "UPDATE M_Transaction SET M_AttributeSetInstance_ID = ?, MovementQty = ?, Created = ?::timestamp"
							+ " WHERE M_Locator_ID = ? AND M_MovementLine_ID = ? AND MovementType = ?"
							+ " AND M_AttributeSetInstance_ID = ?";
					if(DB.executeUpdate(sql, new Object[]{
							M_AttributeSetInstance_ID, isReversal ? qty.negate() : qty, "1991-01-06 06:01:06", line.getM_LocatorTo_ID(),
							line.get_ID(), "M+", OLD_AttributeSetInstance_ID
					}, false, get_TrxName()) < 1)
						throw new AdempiereException("Failed.!");
				}
			}
			movementMap.put(po.get_ID() + ";" + M_AttributeSetInstance_ID, 69);
		}
		else
		{
			if(OLD_AttributeSetInstance_ID < 0)
			{
				sql = "INSERT INTO M_Transaction (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
						+ " Updated, UpdatedBy, IsActive, M_Transaction_ID, M_Transaction_UU,"
						+ " M_AttributeSetInstance_ID, M_Locator_ID, MovementQty," + po.get_TableName() + "_ID,"
						+ " M_Product_ID, MovementDate, MovementType) VALUES (1000015, 1000054, '1991-01-06 06:01:06',"
						+ " 1001075, '1991-01-06 06:01:06', 1001075, 'Y', getnextid('M_Transaction'),"
						+ " uuid_generate_v4(), ?, ?, ?, ?, ?, ?, ?)";
				if(DB.executeUpdate(sql, new Object[]{
					M_AttributeSetInstance_ID, trx.getM_Locator_ID(), qty, trx.get_Value(po.get_TableName() + "_ID"),
					trx.getM_Product_ID(), trx.getMovementDate(), trx.getMovementType()
				}, false, get_TrxName()) < 1)
					throw new AdempiereException("Failed.!");
			}
			else
			{
				sql = "UPDATE M_Transaction SET M_AttributeSetInstance_ID = ?, MovementQty = ?, Created = ?::timestamp"
						+ " WHERE M_Transaction_ID = ?";
				if(DB.executeUpdate(sql, new Object[]{
						M_AttributeSetInstance_ID, qty, "1991-01-06 06:01:06", trx.get_ID()
				}, false, get_TrxName()) < 1)
					throw new AdempiereException("Failed.!");
			}
		}
	}
	
	private void CreateOrUpdateStorage(int M_AttributeSetInstance_ID, int M_Product_ID, int M_Locator_ID, BigDecimal Qty)
	{
		MAttributeSetInstance asi = MAttributeSetInstance.get(getCtx(), M_AttributeSetInstance_ID, M_Product_ID);
		String sql = "SELECT COUNT(*) FROM M_StorageOnHand WHERE M_Product_ID = ? AND M_AttributeSetInstance_ID = ?"
				+ " AND M_Locator_ID = ? AND DateMaterialPolicy = ?";
		boolean exists = DB.getSQLValue(get_TrxName(), sql, new Object[]{
			M_Product_ID, M_AttributeSetInstance_ID, M_Locator_ID, asi.getCreated()
			}) > 0;
		
		if(exists)
		{
			sql = "UPDATE M_StorageOnHand SET QtyOnHand = QtyOnHand + ?, Created = ?::timestamp"
					+ " WHERE M_Product_ID = ? AND M_AttributeSetInstance_ID = ? AND M_Locator_ID = ?"
					+ " AND DateMaterialPolicy = ?";
			if(DB.executeUpdate(sql, new Object[]{
			Qty, "1991-01-06 06:01:06", M_Product_ID, M_AttributeSetInstance_ID, M_Locator_ID, asi.getCreated()
			}, false, get_TrxName()) < 1)
				throw new AdempiereException("Failed.!");
		}
		else
		{
			sql = "INSERT INTO M_StorageOnHand (AD_Client_ID, AD_Org_ID, Created, CreatedBy,"
					+ " Updated, UpdatedBy, IsActive, M_StorageOnHand_UU, M_Product_ID,"
					+ " M_Locator_ID, M_AttributeSetInstance_ID, DateMaterialPolicy,"
					+ " QtyOnHand) VALUES (1000015, 1000054, '1991-01-06 06:01:06',"
					+ " 1001075, '1991-01-06 06:01:06', 1001075, 'Y', uuid_generate_v4(),"
					+ " ?, ?, ?, ?, ?)";
			if(DB.executeUpdate(sql, new Object[]{
					M_Product_ID, M_Locator_ID, M_AttributeSetInstance_ID, asi.getCreated(), Qty
			}, false, get_TrxName()) < 1)
				throw new AdempiereException("Failed.!");
		}
	}
	
	private MStorageOnHand[] getOfLocator (Properties ctx, 
			int M_Product_ID, int M_Locator_ID, String trxName, boolean ZeroOrNot)
	{
		String sqlWhere = "M_Product_ID=? AND M_Locator_ID=? AND M_AttributeSetInstance_ID <> 0";
		if(!ZeroOrNot)
			sqlWhere += "AND QtyOnHand <> 0";
		else
			sqlWhere += "AND QtyOnHand = 0";
		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
								.setParameters(M_Product_ID, M_Locator_ID)
								.setOrderBy("(CASE WHEN QtyOnHand > 0 THEN DateMaterialPolicy END), DateMaterialPolicy")
								.list();
		
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getAll
	
	private void RePostDocs()
	{
		Enumeration<Integer> mapKey = RePostMap.keys();
		int count = 1;
		while(mapKey.hasMoreElements())
		{
			statusWa.statusUpdate("Repost Document ke " + count + " dari " + RePostMap.size());
			Integer key = mapKey.nextElement();
			String docType = RePostMap.get(key);
			MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
			if(docType.equals("Movement"))
			{
				DocManager.postDocument(ass, MMovement.Table_ID, key, true, true, get_TrxName());
				String sql = "SELECT M_MovementConfirm_ID FROM M_MovementConfirm WHERE DocStatus IN ('CO', 'CL', 'RE')"
						+ " AND M_Movement_ID = ?";
				int confirmID = DB.getSQLValue(get_TrxName(), sql, key);
				if(confirmID > 0)
					DocManager.postDocument(ass, MMovementConfirm.Table_ID, confirmID, true, true, get_TrxName());
			}
			else if(docType.equals("InOut"))
				DocManager.postDocument(ass, MInOut.Table_ID, key, true, true, get_TrxName());
			count++;
		}
	}
}