/**
 * 
 */
package com.uns.model.process;

import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.MUNSGlobalIncentivesConfig;
import com.uns.model.MUNSSubregionRecap;
import com.uns.model.X_UNS_GlobalIncentivesConfig;

/**
 * @author azhaidar
 *
 */
public class CalculateServiceCharges extends SvrProcess {

	private MUNSSubregionRecap m_subregion;
	private X_UNS_GlobalIncentivesConfig m_globalConf;

	/**
	 * 
	 */
	public CalculateServiceCharges() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		m_subregion = new MUNSSubregionRecap(getCtx(),
				getRecord_ID(), get_TrxName());
		
		m_globalConf = MUNSGlobalIncentivesConfig.get(getCtx(), 
				m_subregion.getAD_Org_ID(), m_subregion.getShopType(), 
				m_subregion.getC_Period_ID(), get_TrxName());		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (m_globalConf == null)
			return "Cannot fond appropriate Incentive Configuration for this recapitulation.";
		
		String sql = "UPDATE UNS_ShopRecap sr SET TotalServiceChargeAmt=("
				+ " 	SELECT COALESCE(SUM(pt.ServiceCharge), sr.TotalServiceChargeAmt) "
				+ "		FROM UNS_POSTrx pt "
				+ "		INNER JOIN C_Period p ON C_Period_ID=sr.C_Period_ID"
				+ " 	WHERE pt.C_BPartner_ID=sr.Shop_ID "
				+ "			AND pt.DateAcct BETWEEN p.StartDate AND p.EndDate)"
				+ " WHERE sr.UNS_SubregionRecap_ID=" + m_subregion.getUNS_SubregionRecap_ID();
		
		int count = DB.executeUpdateEx(sql, get_TrxName());
		
		sql = "UPDATE UNS_ShopRecap sr SET "
				+ "	 DistributableServiceCharge=(FLOOR(TotalServiceChargeAmt * (?/100)))"
				+ " WHERE sr.TotalServiceChargeAmt IS NOT NULL AND sr.TotalServiceChargeAmt>0"
				+ "		AND sr.UNS_SubregionRecap_ID=" + m_subregion.getUNS_SubregionRecap_ID();
		
		count = DB.executeUpdateEx(sql, new Object[]{m_globalConf.getSCForEmployee()}, get_TrxName());
		
		sql = "UPDATE UNS_ShopRecap sr SET "
				+ "	 EmployeePortion=(FLOOR(DistributableServiceCharge * (1 - (?/100))))"
				+ ", SupportPortion=(FLOOR(DistributableServiceCharge * (?/100)))"
				+ " WHERE sr.TotalServiceChargeAmt IS NOT NULL AND sr.TotalServiceChargeAmt>0"
				+ "		AND sr.UNS_SubregionRecap_ID=" + m_subregion.getUNS_SubregionRecap_ID();
		
		count = DB.executeUpdateEx(sql, new Object[]{m_globalConf.getSupportPortion(), 
													 m_globalConf.getSupportPortion()}, 
									get_TrxName());
		
		sql = "UPDATE UNS_SubregionRecap sub "
				+ " SET TotalServiceChargeAmt=("
				+ "		SELECT SUM(sr.TotalServiceChargeAmt) FROM UNS_ShopRecap sr"
				+ "		WHERE sr.UNS_SubregionRecap_ID=sub.UNS_SubregionRecap_ID)"
				+ ", EmployeePortion=("
				+ "		SELECT SUM(sr.DistributableServiceCharge) FROM UNS_ShopRecap sr"
				+ "		WHERE sr.UNS_SubregionRecap_ID=sub.UNS_SubregionRecap_ID)"
				+ " WHERE sub.UNS_SubregionRecap_ID=" + getRecord_ID();
		
		DB.executeUpdateEx(sql, get_TrxName());
		
		return count + " Shop-recap's service charge amount calculated and updated.";
	}
}
