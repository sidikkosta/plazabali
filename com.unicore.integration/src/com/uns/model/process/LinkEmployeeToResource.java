/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSOTRequest;
import com.uns.model.MUNSResource;
import com.uns.model.MUNSResourceWorkerLine;
import com.uns.model.MUNSWorkHoursConfig;
import com.uns.util.MessageBox;

/**
 * @author Burhani Adam
 *
 */
public class LinkEmployeeToResource extends SvrProcess {

	/**
	 * 
	 */
	public LinkEmployeeToResource() {
		// TODO Auto-generated constructor stub
	}
	
	private int orgID = 0;
	private int employeeID = 0;
	private int resourceID = 0;
	private Timestamp validFrom = null;
	private boolean unLink = false;
	String msg = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
				orgID = params[i].getParameterAsInt();
			else if ("UNS_Employee_ID".equals(name))
				employeeID = params[i].getParameterAsInt();
			else if ("UNS_Resource_ID".equals(name))
				resourceID = params[i].getParameterAsInt();
			else if ("ValidFrom".equals(name))
				validFrom = params[i].getParameterAsTimestamp();
			else if ("isUnLink".equals(name))
				unLink = params[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown parameter " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSResource newResource = MUNSResource.get(getCtx(), resourceID);
		if(employeeID <= 0 || resourceID <= 0 || validFrom == null)
			throw new AdempiereException("@FillMandatory@ Employee / Resource / Valid From");
		
		if(unLink)
		{
			String sql = "DELETE FROM UNS_Resource_WorkerLine WHERE Labor_ID = ? AND UNS_Resource_ID = ?";
			int record = DB.executeUpdate(sql, new Object[]{employeeID, resourceID}, false, get_TrxName());
			if(record == 0)
				return "No Record";
			else
			{
				sql = "SELECT UNS_Resource_WorkerLine_ID FROM UNS_Resource_WorkerLine"
						+ " WHERE Labor_ID = ? ORDER BY ValidFrom DESC";
				int lastID = DB.getSQLValue(get_TrxName(), sql, employeeID);
				
				sql = "UPDATE UNS_Resource_WorkerLine SET IsActive = 'Y', ValidTo = NULL"
						+ " WHERE UNS_Resource_WorkerLine_ID = ?";
				record = DB.executeUpdate(sql, lastID, get_TrxName());
				if(record < 0)
					throw new AdempiereException(CLogger.retrieveErrorString("Error on active old resource"));
				else
					return "UnLink Success";
			}
		}
		
		MUNSResource oldResource = MUNSResource.getByEmployee(getCtx(), employeeID, validFrom, get_TrxName());
		if(oldResource != null)
		{
			if(newResource.get_ID() == oldResource.get_ID())
				return "Old resource same with new resource";
			msg = checkDuplicateAdjusment();
			if(!Util.isEmpty(msg, true))
				throw new AdempiereException(msg);
			msg = checkDuplicateOT();
			if(!Util.isEmpty(msg, true))
				throw new AdempiereException(msg);
			
			int answer = MessageBox.showMsg(getCtx(), getProcessInfo(), "This employee has registered in resource "
							+ oldResource.getName() + ". Are you sure to move " + newResource.getName() + " resource?",
							"Confirmation move resource.", MessageBox.YESNOCANCEL, MessageBox.ICONQUESTION);
			if(answer == MessageBox.RETURN_CANCEL || MessageBox.RETURN_NO == answer || answer == MessageBox.RETURN_NONE)
				throw new AdempiereUserError("Move resource has cancelled.");
			
			MUNSResourceWorkerLine oldWL = MUNSResourceWorkerLine.getWorker(getCtx(), oldResource.get_ID()
					, employeeID, validFrom, get_TrxName());
			Calendar cal = Calendar.getInstance();
			cal.setTime(validFrom);
			cal.add(Calendar.DATE, -1);
			Timestamp newValidTo = new Timestamp(cal.getTimeInMillis());
			if(newValidTo.compareTo(oldWL.getValidTo()) < 0) 
				oldWL.setValidTo(newValidTo);
			
//			oldWL.setIsActive(false);
			oldWL.saveEx();
		}
		
		MUNSEmployee emp = new MUNSEmployee(getCtx(), employeeID, get_TrxName());
		if(emp.getUNS_Contract_Recommendation_ID() <= 0)
			throw new AdempiereException("Employee doesn't have contract.");
		MUNSResourceWorkerLine newWL = new MUNSResourceWorkerLine(getCtx(), 0, get_TrxName());
		newWL.setUNS_Resource_ID(newResource.get_ID());
		newWL.setAD_Org_ID(orgID);
		newWL.setLabor_ID(employeeID);
		newWL.setValidFrom(validFrom);
		newWL.setValidTo(emp.getUNS_Contract_Recommendation().getDateContractEnd());
		newWL.saveEx();
		msg = "Succes link employee to new resource";
		
		return msg;
	}
	
	private String checkDuplicateAdjusment()
	{
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(whc.UNS_WorkHoursConfig_ID), ';') FROM UNS_WorkHoursConfig whc"
				+ " INNER JOIN UNS_WorkHoursConfig_Line whcl ON whcl.UNS_WorkHoursConfig_ID = whc.UNS_WorkHoursConfig_ID"
				+ " WHERE whc.ValidFrom >= ? AND whcl.UNS_Employee_ID = ?";
		String ids = DB.getSQLValueString(get_TrxName(), sql, validFrom, employeeID);
		
		if(Util.isEmpty(ids, true))
			return null;
		
		String[] idString = ids.split(";");
		
		for(int i=0;i<idString.length;i++)
		{
			MUNSWorkHoursConfig whc = new MUNSWorkHoursConfig(getCtx(), new Integer (idString[i]), get_TrxName());
			 sql = "SELECT DocumentNo FROM UNS_WorkHoursConfig whc"
						+ " INNER JOIN UNS_WorkHoursConfig_Line whcl ON whcl.UNS_WorkHoursConfig_ID = whc.UNS_WorkHoursConfig_ID"
						+ " WHERE ((whc.ValidFrom BETWEEN ? AND ?) OR (whc.ValidTo BETWEEN ? AND ?))"
						+ " AND whcl.UNS_Resource_ID = ?";
			 String duplicateDoc = DB.getSQLValueString(get_TrxName(), sql, new Object[]{
				 					whc.getValidFrom(), whc.getValidTo(), 
				 						whc.getValidFrom(), whc.getValidTo(), 
				 							resourceID});
			 if(!Util.isEmpty(duplicateDoc, true))
				 return "Duplicate Adjusment " + whc.getDocumentNo() + " - " + duplicateDoc;
		}
		
		return null;
	}
	
	private String checkDuplicateOT()
	{
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(ot.UNS_OTRequest_ID), ';') FROM UNS_OTRequest ot"
				+ " WHERE ot.DocStatus NOT IN ('VO', 'RE') AND ot.Processed = 'Y' AND ot.DateDoOT >= ? AND"
				+ " (ot.UNS_Employee_ID = ? OR EXISTS (SELECT 1 FROM UNS_OTLine line"
				+ " WHERE line.UNS_OTRequest_ID = ot.UNS_OTRequest_ID"
				+ " AND line.UNS_Employee_ID = ?))";
		String ids = DB.getSQLValueString(get_TrxName(), sql, validFrom, employeeID, employeeID);
		
		if(Util.isEmpty(ids, true))
			return null;
		
		String[] idString = ids.split(";");
		
		for(int i=0;i<idString.length;i++)
		{
			MUNSOTRequest req = new MUNSOTRequest(getCtx(), new Integer (idString[i]), get_TrxName());
			sql = "SELECT ot.DocumentNo FROM UNS_OTRequest ot"
				+ " INNER JOIN UNS_OTLine line ON line.UNS_OTRequest_ID = ot.UNS_OTRequest_ID"
				+ " WHERE ot.DateDoOT = ? AND ot.DocStatus NOT IN ('VO', 'RE') AND ot.Processed = 'Y'"
				+ " AND line.UNS_Resource_ID = ?";
			String duplicateDoc = DB.getSQLValueString(get_TrxName(), sql, req.getDateDoOT(), resourceID);
			if(!Util.isEmpty(duplicateDoc, true))
				 return "Duplicate Overtime " + req.getDocumentNo() + " - " + duplicateDoc;
		}
		
		return null;
	}
}
