/**
 * 
 */
package com.uns.model.process;

import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;
import com.uns.base.model.Query;
import com.uns.model.MUNSSubregionRecap;
import com.uns.model.X_UNS_ShopRecap;
import com.uns.model.X_UNS_SubregionRecap;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author dpitaloka
 *
 */
public class ImportMonthlyShopSchedule extends SvrProcess {

	private boolean DeleteOld = false;

	@Override
	protected void prepare() {
		this.getCtx().setProperty("UNS_SubregionRecap_ID", String.valueOf(this.getRecord_ID()));		

		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOldImported"))
				DeleteOld = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window monthly shop schedule");
	
	}

	@Override
	protected String doIt() throws Exception {
		X_UNS_SubregionRecap subregion = new MUNSSubregionRecap(getCtx(), getRecord_ID(), get_TrxName());
		if(subregion.isProcessed())
			throw new AdempiereException("Document has been processed.");
		
		if(DeleteOld)
		{
			List<X_UNS_ShopRecap> shopRecaps = Query
					.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
							X_UNS_ShopRecap.Table_Name, "uns_subregionrecap_id = ?",
							get_TrxName()).setParameters(getRecord_ID()).list();
			for (X_UNS_ShopRecap shopRecap : shopRecaps) {
				shopRecap.delete(true);
			}
		}
					
		if(Util.isEmpty(subregion.getFile_Directory(), true))
			throw new AdempiereException("To run this process, you must fill the file with the correct directory / path.");
		
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportMonthlyShopSchedule'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with ImportMonthlyShopSchedule name");
		
		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(subregion.getFile_Directory());
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length > 1)
			throw new AdempiereException("more than one lines in Import Simple XLS " + simple.getName());
		
		MUNSImportSimpleTable table = tables[0];
		
		SimpleImportXLS si = new SimpleImportXLS(getCtx(), simple, table, false, 0, 0, 0, getAD_Client_ID(), get_TrxName());
		String success = null;
		try{
			 success = si.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		
		return success;
	}

}
