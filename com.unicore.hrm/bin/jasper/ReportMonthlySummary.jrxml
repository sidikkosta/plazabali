<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportMonthlySummary" language="groovy" pageWidth="986" pageHeight="720" orientation="Landscape" columnWidth="946" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="304be076-a131-4625-8d9a-1b1259897e27">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="154"/>
	<property name="ireport.y" value="96"/>
	<parameter name="C_Period_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[".\\"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	b.now,
	b.last,
	b.leave,
	b.newhires,
	b.Period,
	b.yearID,
	(CASE WHEN b.leave >= b.newhires THEN (b.leave-b.newhires)/((b.now+b.last)/2) ELSE (b.newhires-b.leave)/((b.now+b.last)/2) END) As turnover

FROM (SELECT
	SUM(CASE WHEN a.ContractStart <= a.PeriodEnd AND a.ContractEnd >= a.PeriodEnd THEN a.Count ELSE 0 END) As now,
	SUM(CASE WHEN a.ContractStart <= a.PrevPeriodEnd AND a.ContractEnd >= a.PrevPeriodEnd THEN a.Count ELSE 0 END) As last,
	SUM(CASE WHEN a.ContractStart <= a.PeriodEnd AND a.ContractEnd <= a.PeriodEnd THEN a.Count ELSE 0 END) As leave,
	SUM(CASE WHEN a.ContractStart >= a.PeriodStart AND a.ContractEnd >= a.PeriodEnd THEN a.Count ELSE 0 END) As newhires,
	a.Period,
	a.yearID

FROM (SELECT
	min(cr.DateContractStart) As ContractStart,
	cr.DateContractEnd As ContractEnd,
	p.StartDate As PeriodStart,
	p.EndDate As PeriodEnd,
	pp.StartDate As PrevPeriodStart,
	pp.EndDate As PrevPeriodEnd,
	COUNT (e.UNS_Employee_ID) As Count,
	p.Name As Period,
	y.C_Year_ID as yearID

FROM UNS_Employee e

INNER JOIN UNS_Contract_Recommendation cr ON e.UNS_Employee_ID=cr.UNS_Employee_ID
INNER JOIN AD_Org o ON e.AD_Org_ID=o.AD_Org_ID
INNER JOIN C_Period p ON p.C_Period_ID=$P{C_Period_ID}
INNER JOIN C_Period pp ON pp.EndDate = (p.StartDate - INTERVAL '1 DAY')
INNER JOIN C_Year y ON p.C_Year_ID=y.C_Year_ID

WHERE cr.DocStatus IN ('CO','CL') AND cr.nextcontracttype != 'INT'
GROUP BY o.Name, cr.DateContractStart, cr.DateContractEnd, p.StartDate, p.EndDate, pp.StartDate, pp.EndDate, p.Name, y.C_Year_ID)a
GROUP BY a.Period, a.yearID)b]]>
	</queryString>
	<field name="now" class="java.math.BigDecimal"/>
	<field name="last" class="java.math.BigDecimal"/>
	<field name="leave" class="java.math.BigDecimal"/>
	<field name="newhires" class="java.math.BigDecimal"/>
	<field name="period" class="java.lang.String"/>
	<field name="yearid" class="java.lang.Integer"/>
	<field name="turnover" class="java.math.BigDecimal"/>
	<variable name="turnover_SUM" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{turnover}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="69" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="100" height="20" uuid="2c6cbd50-34a5-41af-8e24-03233786e10e"/>
				<textElement>
					<font isBold="true"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[PLAZA BALI]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="258" height="20" uuid="a06c30c7-9214-46f8-bd99-7d44383d56c1"/>
				<textElement>
					<font isBold="true"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[HUMAN RESOURES DEVELOPMENT DEPARTMENT]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="40" width="100" height="20" uuid="22580691-bbba-43a3-bff2-62e55450abda"/>
				<textElement>
					<font isBold="true"/>
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[MONTHLY REPORT]]></text>
			</staticText>
			<textField>
				<reportElement x="386" y="0" width="100" height="20" uuid="6040b192-08b6-4357-a96d-ce13ad3acf32"/>
				<textFieldExpression><![CDATA["Month: "+$F{period}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="121" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="203" height="21" uuid="4e075db5-2d11-4541-ac72-48d756f2d87a"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[ Total Employee Last Month]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="21" width="203" height="20" uuid="132041b9-d776-411b-901e-5ae747c2ac48"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[ Total Employee This Month]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="41" width="203" height="20" uuid="171b2c1f-98ed-4548-96fd-3a778d56d9c0"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[ Leavers]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="61" width="203" height="20" uuid="540df2f2-cae5-45d5-861f-2336e53d46ff"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[ New Hires]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="81" width="203" height="20" uuid="1033a209-36d4-4269-a3cc-6011c6708cc0"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[ Turn Over Ratio]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="101" width="203" height="20" uuid="d022fe30-c88c-4975-aa0e-3a6b421fe513"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph leftIndent="5"/>
				</textElement>
				<text><![CDATA[ Retention]]></text>
			</staticText>
			<textField>
				<reportElement x="203" y="21" width="101" height="20" uuid="367785a3-84d8-4dca-8b95-5b5e779dc9b0"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{now}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="203" y="41" width="101" height="20" uuid="a902b35d-990f-4554-8703-794f429d0140"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{leave}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="203" y="61" width="101" height="20" uuid="98bc49ef-d716-4581-8d13-60bf6b8e058b"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{newhires}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="203" y="0" width="101" height="21" uuid="2039d14c-2175-4877-b833-89c57683800c"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{last}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00%">
				<reportElement x="203" y="81" width="101" height="20" uuid="a6f2fc79-d7de-4b05-a4ce-b528c84cd6c2"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="5"/>
				</textElement>
				<textFieldExpression><![CDATA[new Double( $F{turnover}.doubleValue() / 100 )]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="45" splitType="Stretch">
			<subreport>
				<reportElement x="0" y="0" width="198" height="45" uuid="3c72e227-fe4d-4979-a0f3-ec903b2ae7ef"/>
				<subreportParameter name="C_Period_ID">
					<subreportParameterExpression><![CDATA[$P{C_Period_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "ReportMonthlySummary_Subreport1.jasper"]]></subreportExpression>
			</subreport>
			<subreport>
				<reportElement x="203" y="0" width="198" height="45" uuid="8465b550-25e7-4d50-9966-f28321cbdb6c"/>
				<subreportParameter name="C_Period_ID">
					<subreportParameterExpression><![CDATA[$P{C_Period_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "ReportMonthlySummary_Subreport3.jasper"]]></subreportExpression>
			</subreport>
			<subreport>
				<reportElement x="412" y="0" width="536" height="45" uuid="baa50b85-8f02-4baf-8460-bf79a2de3e3e"/>
				<subreportParameter name="yearID">
					<subreportParameterExpression><![CDATA[$F{yearid}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "ReportMonthlySummary_Subreport4.jasper"]]></subreportExpression>
			</subreport>
		</band>
		<band height="50">
			<subreport>
				<reportElement x="203" y="0" width="198" height="50" uuid="57fb72a4-c6c8-493f-9f1f-d60a03f8f6e0"/>
				<subreportParameter name="C_Period_ID">
					<subreportParameterExpression><![CDATA[$P{C_Period_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "ReportMonthlySummary_Subreport2.jasper"]]></subreportExpression>
			</subreport>
			<subreport>
				<reportElement x="0" y="0" width="198" height="50" uuid="c6827144-4f29-4c65-b47f-1ad98d0c8aba"/>
				<subreportParameter name="C_Period_ID">
					<subreportParameterExpression><![CDATA[$P{C_Period_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "ReportMonthlySummary_Subreport5.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
