package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Util;

public class MUNSEmpPresenceVerification extends X_UNS_EmpPresenceVerification {

	/**
	 * @author Hamba Allah
	 */
	private static final long serialVersionUID = 8688145821852378589L;
	
	private MUNSPresenceVerification m_parent = null;

	public MUNSEmpPresenceVerification(Properties ctx,
			int UNS_EmpPresenceVerification_ID, String trxName) {
		super(ctx, UNS_EmpPresenceVerification_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSEmpPresenceVerification(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSPresenceVerification getParent() {
		
		if(m_parent != null)
			return m_parent;
		
		m_parent = new MUNSPresenceVerification(getCtx(), getUNS_PresenceVerification_ID(), get_TrxName());
		
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		getParent();
		//check duplicate employee on same day
		String sql = "SELECT pv.DocumentNo FROM UNS_EmpPresenceVerification empv"
				+ " INNER JOIN UNS_PresenceVerification pv ON pv.UNS_PresenceVerification_ID = empv.UNS_PresenceVerification_ID"
				+ " WHERE empv.UNS_EmpPresenceVerification_ID <> ? AND empv.UNS_Employee_ID = ? AND empv.isActive = ?"
				+ " AND pv.DocStatus NOT IN (?,?) AND pv.PresenceDate = ?";
		String docNo = DB.getSQLValueString(get_TrxName(), sql, get_ID(), getUNS_Employee_ID(), "Y", "VO","RE", m_parent.getPresenceDate());
		if(!Util.isEmpty(docNo, true))
		{
			log.saveError("Duplicate", "There was exists employee on Presence Verification with Document No : "+docNo);
			return false;
		}
		
		MUNSDailyPresence day = getDailyPresence();
		if(day == null)
		{
			log.saveError("Error", "Cannot found daily presence. Please syncronize first.");
			return false;
		}	
		if(day.getUNS_MonthlyPresenceVal_ID() > 0)
			throw new AdempiereException("there was exists Monthly Presence Validation on this day. "
					+ "Cannot create Manual Presence Verification");
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		//update Header
		updateHeader();
		
		return super.afterSave(newRecord, success);
	}
	
	@Override
	protected boolean beforeDelete() {
		
		//update Header
		updateHeader();
		
		return super.beforeDelete();
	}
	
	private void updateHeader() {
		
		getParent();
		
		String sql = "SELECT COUNT(*) FROM UNS_EmpPresenceVerification WHERE"
				+ " UNS_PresenceVerification_ID = ? AND isActive = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, getUNS_PresenceVerification_ID(), "Y");
		
		if(m_parent == null)
			throw new AdempiereException("Cannot found Header");
		
		m_parent.setTotalLines(BigDecimal.valueOf(count));
		m_parent.saveEx();
			
	}
	
	public MUNSDailyPresence getDailyPresence() {
		
		getParent();
		
		MUNSDailyPresence day = MUNSDailyPresence.getByDate(
				getCtx(), m_parent.getPresenceDate(), getUNS_Employee_ID(), get_TrxName());
		
		return day;
	}

}
