/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_UpdateContract
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_UpdateContract 
{

    /** TableName=UNS_UpdateContract */
    public static final String Table_Name = "UNS_UpdateContract";

    /** AD_Table_ID=1000396 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AL1Multiplier */
    public static final String COLUMNNAME_AL1Multiplier = "AL1Multiplier";

	/** Set AL1 Multiplier	  */
	public void setAL1Multiplier (BigDecimal AL1Multiplier);

	/** Get AL1 Multiplier	  */
	public BigDecimal getAL1Multiplier();

    /** Column name AL2Multiplier */
    public static final String COLUMNNAME_AL2Multiplier = "AL2Multiplier";

	/** Set AL2 Multiplier	  */
	public void setAL2Multiplier (BigDecimal AL2Multiplier);

	/** Get AL2 Multiplier	  */
	public BigDecimal getAL2Multiplier();

    /** Column name AL3Multiplier */
    public static final String COLUMNNAME_AL3Multiplier = "AL3Multiplier";

	/** Set AL3 Multiplier	  */
	public void setAL3Multiplier (BigDecimal AL3Multiplier);

	/** Get AL3 Multiplier	  */
	public BigDecimal getAL3Multiplier();

    /** Column name ALR1Multiplier */
    public static final String COLUMNNAME_ALR1Multiplier = "ALR1Multiplier";

	/** Set ALR1 Multiplier	  */
	public void setALR1Multiplier (BigDecimal ALR1Multiplier);

	/** Get ALR1 Multiplier	  */
	public BigDecimal getALR1Multiplier();

    /** Column name ALR2Multiplier */
    public static final String COLUMNNAME_ALR2Multiplier = "ALR2Multiplier";

	/** Set ALR2 Multiplier	  */
	public void setALR2Multiplier (BigDecimal ALR2Multiplier);

	/** Get ALR2 Multiplier	  */
	public BigDecimal getALR2Multiplier();

    /** Column name ALR3Multiplier */
    public static final String COLUMNNAME_ALR3Multiplier = "ALR3Multiplier";

	/** Set ALR3 Multiplier	  */
	public void setALR3Multiplier (BigDecimal ALR3Multiplier);

	/** Get ALR3 Multiplier	  */
	public BigDecimal getALR3Multiplier();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name FirstOTMultiplier */
    public static final String COLUMNNAME_FirstOTMultiplier = "FirstOTMultiplier";

	/** Set First OT Multiplier	  */
	public void setFirstOTMultiplier (BigDecimal FirstOTMultiplier);

	/** Get First OT Multiplier	  */
	public BigDecimal getFirstOTMultiplier();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name New_A_L1 */
    public static final String COLUMNNAME_New_A_L1 = "New_A_L1";

	/** Set New A L1	  */
	public void setNew_A_L1 (BigDecimal New_A_L1);

	/** Get New A L1	  */
	public BigDecimal getNew_A_L1();

    /** Column name New_A_L1_R */
    public static final String COLUMNNAME_New_A_L1_R = "New_A_L1_R";

	/** Set New A L1 R	  */
	public void setNew_A_L1_R (BigDecimal New_A_L1_R);

	/** Get New A L1 R	  */
	public BigDecimal getNew_A_L1_R();

    /** Column name New_A_L2 */
    public static final String COLUMNNAME_New_A_L2 = "New_A_L2";

	/** Set New A L2	  */
	public void setNew_A_L2 (BigDecimal New_A_L2);

	/** Get New A L2	  */
	public BigDecimal getNew_A_L2();

    /** Column name New_A_L2_R */
    public static final String COLUMNNAME_New_A_L2_R = "New_A_L2_R";

	/** Set New A L2 R.
	  * Amount New_A_L2_R
	  */
	public void setNew_A_L2_R (BigDecimal New_A_L2_R);

	/** Get New A L2 R.
	  * Amount New_A_L2_R
	  */
	public BigDecimal getNew_A_L2_R();

    /** Column name New_A_L3 */
    public static final String COLUMNNAME_New_A_L3 = "New_A_L3";

	/** Set New A L3	  */
	public void setNew_A_L3 (BigDecimal New_A_L3);

	/** Get New A L3	  */
	public BigDecimal getNew_A_L3();

    /** Column name New_A_L3_R */
    public static final String COLUMNNAME_New_A_L3_R = "New_A_L3_R";

	/** Set New A L3 R	  */
	public void setNew_A_L3_R (BigDecimal New_A_L3_R);

	/** Get New A L3 R	  */
	public BigDecimal getNew_A_L3_R();

    /** Column name NewLeburJamBerikutnya */
    public static final String COLUMNNAME_NewLeburJamBerikutnya = "NewLeburJamBerikutnya";

	/** Set New Lembur Jam Berikutnya	  */
	public void setNewLeburJamBerikutnya (BigDecimal NewLeburJamBerikutnya);

	/** Get New Lembur Jam Berikutnya	  */
	public BigDecimal getNewLeburJamBerikutnya();

    /** Column name NewLeburJamPertama */
    public static final String COLUMNNAME_NewLeburJamPertama = "NewLeburJamPertama";

	/** Set New Lembur Jam Pertama	  */
	public void setNewLeburJamPertama (BigDecimal NewLeburJamPertama);

	/** Get New Lembur Jam Pertama	  */
	public BigDecimal getNewLeburJamPertama();

    /** Column name NextOTMultiplier */
    public static final String COLUMNNAME_NextOTMultiplier = "NextOTMultiplier";

	/** Set Next OT Multiplier	  */
	public void setNextOTMultiplier (BigDecimal NextOTMultiplier);

	/** Get Next OT Multiplier	  */
	public BigDecimal getNextOTMultiplier();

    /** Column name OTBasicAmt */
    public static final String COLUMNNAME_OTBasicAmt = "OTBasicAmt";

	/** Set OT Basic Amount	  */
	public void setOTBasicAmt (BigDecimal OTBasicAmt);

	/** Get OT Basic Amount	  */
	public BigDecimal getOTBasicAmt();

    /** Column name PayrollLevel */
    public static final String COLUMNNAME_PayrollLevel = "PayrollLevel";

	/** Set PayrollLevel	  */
	public void setPayrollLevel (String PayrollLevel);

	/** Get PayrollLevel	  */
	public String getPayrollLevel();

    /** Column name PayrollTerm */
    public static final String COLUMNNAME_PayrollTerm = "PayrollTerm";

	/** Set Payroll Term	  */
	public void setPayrollTerm (String PayrollTerm);

	/** Get Payroll Term	  */
	public String getPayrollTerm();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name UNS_UpdateContract_ID */
    public static final String COLUMNNAME_UNS_UpdateContract_ID = "UNS_UpdateContract_ID";

	/** Set Update Contract	  */
	public void setUNS_UpdateContract_ID (int UNS_UpdateContract_ID);

	/** Get Update Contract	  */
	public int getUNS_UpdateContract_ID();

    /** Column name UNS_UpdateContract_UU */
    public static final String COLUMNNAME_UNS_UpdateContract_UU = "UNS_UpdateContract_UU";

	/** Set UNS_UpdateContract_UU	  */
	public void setUNS_UpdateContract_UU (String UNS_UpdateContract_UU);

	/** Get UNS_UpdateContract_UU	  */
	public String getUNS_UpdateContract_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
