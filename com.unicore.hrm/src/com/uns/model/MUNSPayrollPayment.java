/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MPayment;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author menjangan
 *
 */
public class MUNSPayrollPayment extends X_UNS_PayrollPayment implements
		DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3644571964064367598L;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;

	/**
	 * @param ctx
	 * @param UNS_PayrollPayment_ID
	 * @param trxName
	 */
	public MUNSPayrollPayment(Properties ctx, int UNS_PayrollPayment_ID,
			String trxName) {
		super(ctx, UNS_PayrollPayment_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollPayment(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocOptions#customizeValidActions(java.lang.String, java.lang.Object, java.lang.String, java.lang.String, int, java.lang.String[], java.lang.String[], int)
	 */
	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if (docStatus.equals(DOCSTATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		else if (docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DocAction.ACTION_ReActivate;
			options[index++] = DocAction.ACTION_Void;
		}
		return index;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String action) throws Exception 
	{
		trace(Level.INFO, "Process-It");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() 
	{
		trace(Level.INFO, "Unlock-It");
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() {
		trace(Level.INFO, "Invalidate-It");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		trace(Level.INFO, "Prepare-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		getBAccounts();
		if (m_accounts.length == 0)
		{
			m_processMsg = "@NoLines@";
			return DOCSTATUS_Invalid;
		}
		for (int i=0; i<m_accounts.length; i++) 
		{
			MUNSPayrollPaymentEmp[] emps = m_accounts[i].getLines();
			if (emps.length == 0)
			{
				m_processMsg = "@NoLines@";
				return DOCSTATUS_Invalid;
			}
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		m_justPrepared = true;
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		trace(Level.INFO, "Approve-It");
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() 
	{
		trace(Level.INFO, "Reject-It");
		setIsApproved(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		if (!m_justPrepared)
		{
			String ds  = prepareIt();
			if (!DOCSTATUS_InProgress.equals(ds))
				return ds;
		}
		trace(Level.INFO, "Complete-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		m_processMsg = doCreatePayments();
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		if (!isApproved())
			approveIt();
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		setProcessed(true);	
		
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		trace(Level.INFO, "Void-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		if (!cancelPayment())
			return false;
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		String sql = "UPDATE UNS_PayrollPayment_Emp SET Description = "
				+ " (Description || '***Voided*** Amount = ' || Amount), Amount = 0 "
				+ " WHERE UNS_PayrollPayment_BA_ID IN (SELECT UNS_PayrollPayment_BA_ID "
				+ " FROM UNS_PayrollPayment_BA WHERE UNS_PayrollPayment_ID = ?)";
		int count = DB.executeUpdate(sql, getUNS_PayrollPayment_ID(), false, get_TrxName());
		if (count == -1)
			return false;
		sql = "UPDATE UNS_PayrollPayment_BA SET Description = (Description || '***Voided*** TotalAmt = ' || "
				+ " TotalAmt), TotalAmt = 0 WHERE UNS_PayrollPayment_ID = ?";
		count = DB.executeUpdate(sql, getUNS_PayrollPayment_ID(), false, get_TrxName());
		if (count == -1)
			return false;
		addDescription(Msg.getMsg(getCtx(), "***Voided*** Total Amt = " + getTotalAmt().toString()));
		setTotalAmt(Env.ZERO);
		setDocStatus(DOCSTATUS_Voided);
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt() 
	{
		trace(Level.INFO, "Close-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() 
	{
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt() {
		trace(Level.INFO, "Reactivate-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		if (!cancelPayment())
			return false;
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if(m_processMsg != null)
			return false;
		
		setDocStatus(DOCSTATUS_InProgress);
		setDocAction(DOCACTION_Complete);
		setIsApproved(false);
		setProcessed(false);
		return true;
	}
	
	@Override 
	public String toString ()
	{
		return getDocumentNo();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{	
		return toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() 
	{
		return toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() 
	{	
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() 
	{
		return getTotalAmt();
	}

	private MUNSPayrollPaymentBA[] m_accounts = null;
	
	public MUNSPayrollPaymentBA[] getBAccounts (boolean requery)
	{
		if (m_accounts != null)
		{
			set_TrxName(m_accounts, get_TrxName());
			return m_accounts;
		}
		
		List<MUNSPayrollPaymentBA> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
				MUNSPayrollPaymentBA.Table_Name,Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).list();
		
		m_accounts = new MUNSPayrollPaymentBA[list.size()];
		list.toArray(m_accounts);
		
		return m_accounts;
	}
	
	public MUNSPayrollPaymentBA[] getBAccounts ()
	{
		return  getBAccounts(false);
	}
	
	private String doCreatePayments () 
	{
		MUNSPayrollPaymentBA[] accounts = getBAccounts();
		for (int i=0; i<accounts.length; i++)
		{
			MPayment payment = null;
			if (accounts[i].getC_Payment_ID() > 0) {
				String sql = "SELECT DocStatus FROM C_Payment WHERE C_Payment_ID = ?";
				String dc = DB.getSQLValueString(get_TrxName(), sql, accounts[i].getC_Payment_ID());
				if ("VO".equals(dc) || "RE".equals(dc))
					accounts[i].setC_Payment_ID(-1);
				else if ("CO".equals(dc) || "CL".equals(dc))
					continue;
				else
					payment = new MPayment(getCtx(), accounts[i].getC_Payment_ID(), get_TrxName());
			}
			if (payment == null)
			{
				payment = new MPayment(getCtx(), 0, get_TrxName());
				payment.setAD_Org_ID(getAD_Org_ID());
				payment.setC_BPartner_ID(accounts[i].getC_BPartner_ID());
				payment.setC_Charge_ID(accounts[i].getC_Charge_ID());
				payment.setPayAmt(accounts[i].getTotalAmt());
				payment.setC_DocType_ID(false);
				payment.setC_Currency_ID(DB.getSQLValue(get_TrxName(), 
						"SELECT C_Currency_ID FROM C_BankAccount WHERE C_BankAccount_ID = ?", 
						accounts[i].getC_BankAccount_ID()));
				payment.setBankCash(accounts[i].getC_BankAccount_ID(), false, 
						accounts[i].getTenderType());
				payment.setDateTrx(getDateTrx());
				payment.setDateAcct(getDateAcct());
			}
			if (!payment.save())
				return CLogger.retrieveErrorString("Failed when try to save payment");
			payment.load(get_TrxName());
			if (!payment.processIt(DocAction.ACTION_Complete))
				return payment.getProcessMsg();
			
			payment.load(get_TrxName());
			if (!payment.save())
				return CLogger.retrieveErrorString("Failed when try to save payment");
			accounts[i].setC_Payment_ID(payment.get_ID());
			if (!accounts[i].save())
				return CLogger.retrieveErrorString("Failed when try to update payroll bank account");
		}
		
		return null;
	}
	
	private void trace (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		
		log.log(level, msg);
	}
	
	private void addDescription (String msg)
	{
		String desc = getDescription();
		desc += msg;
		setDescription(desc);
	}
	
	private boolean cancelPayment ()
	{
		MUNSPayrollPaymentBA[] accounts = getBAccounts();
		for (int i=0; i<accounts.length; i++)
		{
			if (accounts[i].getC_Payment_ID() <= 0)
				continue;
			MPayment pay = new MPayment(getCtx(), accounts[i].getC_Payment_ID(), get_TrxName());
			if (DOCSTATUS_Closed.equals(pay.getDocStatus()))
			{
				m_processMsg = "Could not revers / void payment document "
						+ pay.getDocumentNo() + ". The document is already closed.";
				return false;
			}
			if (DOCSTATUS_Voided.equals(pay.getDocStatus())
					|| DOCSTATUS_Reversed.equals(pay.getDocStatus()))
				continue;
			
			if (!pay.processIt(DOCACTION_Void))
			{
				m_processMsg = pay.getProcessMsg();
				return false;
			}
			
			if (!pay.save()) 
			{
				m_processMsg = CLogger.retrieveErrorString(
						"Could not void / reverse payment " + pay.getDocumentNo());
				return false;
			}
			accounts[i].setC_Payment_ID(-1);
			accounts[i].saveEx();
		}
		
		return true;
	}
	
	private boolean deleteLines ()
	{
		String sql = "DELETE FROM UNS_PayrollPayment_Emp WHERE UNS_PayrollPayment_BA_ID IN ("
				+ " SELECT UNS_PayrollPayment_BA_ID FROM UNS_PayrollPayment_BA WHERE "
				+ " UNS_PayrollPayment_ID = ?)";
		int count = DB.executeUpdate(sql, getUNS_PayrollPayment_ID(), get_TrxName());
		if (count == -1)
			return false;
		
		sql = "DELETE FROM UNS_PayrollPayment_BA WHERE UNS_PayrollPayment_ID = ?";
		count = DB.executeUpdate(sql, getUNS_PayrollPayment_ID(), get_TrxName());
		if (count == -1)
			return false;
		
		return true;
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		if (!deleteLines())
		{
			log.log(Level.SEVERE, "Failed when try to delete lines");
			return false;
		}
		
		return super.beforeDelete();
	}
	
	@Override
	public void setProcessed (boolean processed)
	{
		String sql = "UPDATE UNS_PayrollPayment_BA SET Processed = ? WHERE UNS_PayrollPayment_ID = ?";
		int count = DB.executeUpdate(sql, new Object[] {processed, getUNS_PayrollPayment_ID()}, false, get_TrxName());
		if (count == -1)
			return;
		sql = "UPDATE UNS_PayrollPayment_Emp SET Processed = ? WHERE UNS_PayrollPayment_BA_ID IN ( "
				+ " SELECT UNS_PayrollPayment_BA_ID FROM UNS_PayrollPayment_BA WHERE "
				+ " UNS_PayrollPayment_ID = ?)";
		count = DB.executeUpdate(sql, new Object[] {processed, getUNS_PayrollPayment_ID()}, false, get_TrxName());
		if (count == -1)
			return;
		
		super.setProcessed(processed);
	}
}
