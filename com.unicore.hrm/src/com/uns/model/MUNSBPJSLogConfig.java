package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;

public class MUNSBPJSLogConfig extends X_UNS_BPJSLog_Config {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4447573499401209576L;

	public MUNSBPJSLogConfig(Properties ctx, int UNS_BPJSLog_Config_ID,
			String trxName) {
		super(ctx, UNS_BPJSLog_Config_ID, trxName);
		
	}

	public MUNSBPJSLogConfig(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}

	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(!isBasicSalary() && getAD_Column_ID() <= 0)
			throw new AdempiereException("Mandatory Column");
		
		if(isBasicSalary())
		{
			String sql = "SELECT 1 FROM UNS_BPJSLog_Config WHERE"
					+ " BasicSalary = 'Y' AND isActive = 'Y' AND UNS_BPJSLog_Config_ID <> ?";
			boolean exists = DB.getSQLValue(get_TrxName(), sql, getUNS_BPJSLog_Config_ID()) == 1;
			if(exists)
				throw new AdempiereException("Configuration already exists for Basic Salary");
			setAD_Column_ID(-1);
		}
		
		if(getAD_Column_ID() > 0)
			setBasicSalary(false);
		
		return super.beforeSave(newRecord);
	}
}
