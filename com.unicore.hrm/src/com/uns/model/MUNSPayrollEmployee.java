/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;
import org.compiere.util.Util;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;
import com.uns.model.process.PayrollEmployeeGenerator;

/**
 * @author menjangan
 *
 */
public class MUNSPayrollEmployee extends X_UNS_Payroll_Employee implements DocAction, DocOptions {

	public static final String _UNPAID_LEAVE_DEDUCTION = "UNPAID LEAVE DEDUCTION";
	public static final String _TRUANT_LEAVE_DEDUCTION = "TRUANT LEAVE DEDUCTION";
	public static final String _CUTOFF_DAYS_DEDUCTION = "P.DAYS OUT CONTRACT";
	public static final String _BELATED_DEDUCTION = "BELATED DEDUCTION";
	public static final String _SHORTTIME_DEDUCTION = "SHORTTIME DEDUCTION";
	
	private boolean isVoidFromCorrection = false;
	private final String IMPORT_REMARKS = "***Import by System***";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7666909915012518549L;
	
	public static final int SCALE_PRECISION = 0;
	private MUNSMonthlyPayrollEmployee m_parent = null;
	private MUNSPayrollCostBenefit[] m_CostBenefits = null;
	
	public MUNSPayrollCostBenefit[] getCostBenefits (boolean requery)
	{
		if (m_CostBenefits != null && !requery)
		{
			set_TrxName(m_CostBenefits, get_TrxName());
			return m_CostBenefits;
		}
		String wc = "UNS_Payroll_Employee_ID = ?";
		List<MUNSPayrollCostBenefit> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
				MUNSPayrollCostBenefit.Table_Name, wc, get_TrxName()).
				setParameters(getUNS_Payroll_Employee_ID()).list();
		m_CostBenefits = new MUNSPayrollCostBenefit[list.size()];
		list.toArray(m_CostBenefits);
		return m_CostBenefits;
	}

	/**
	 * @param ctx
	 * @param UNS_Payroll_Employee_ID
	 * @param trxName
	 */
	public MUNSPayrollEmployee(Properties ctx, int UNS_Payroll_Employee_ID,
			String trxName) {
		super(ctx, UNS_Payroll_Employee_ID, trxName);
		// 
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollEmployee(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// 
	}
	
	public MUNSPayrollEmployee(MUNSMonthlyPayrollEmployee parent,int i)
	{
		super(parent.getCtx(), 0, parent.get_TrxName());
		setAD_Org_ID(parent.getAD_Org_ID());
		setUNS_MonthlyPayroll_Employee_ID(parent.get_ID());
		setC_Period_ID(parent.getC_Period_ID());
		setDocStatus(parent.getDocStatus());
		setDocAction(parent.getDocAction());
		setGPokok(BigDecimal.ZERO);
		setTakeHomePay(BigDecimal.ZERO);
		setDocumentNo(parent.getDocumentNo()+ "-" + i);
		setA_JHT(Env.ZERO);
		setA_JK(Env.ZERO);
		setA_JKK(Env.ZERO);
		setA_JP(Env.ZERO);
		setA_JPK(Env.ZERO);
		setA_L1(Env.ZERO);
		setA_L1R(Env.ZERO);
		setA_L2(Env.ZERO);
		setA_L2R(Env.ZERO);
		setA_L3(Env.ZERO);
		setA_L3R(Env.ZERO);
		setA_LemburJamBerikutnya(Env.ZERO);
		setA_TotalOverTime(Env.ZERO);
		setA_Rapel(Env.ZERO);
		setA_Premi(Env.ZERO);
		setA_Other(Env.ZERO);
		setA_LemburJamPertama(Env.ZERO);
		setP_JHT(Env.ZERO);
		setP_JK(Env.ZERO);
		setP_JKK(Env.ZERO);
		setP_JPK(Env.ZERO);
		setP_JP(Env.ZERO);
		setP_SPTP(Env.ZERO);
		setPPH21(Env.ZERO);
		setP_Koperasi(Env.ZERO);
		setP_Label(Env.ZERO);
		setP_ListrikAir(Env.ZERO);
		setP_Mangkir(Env.ZERO);
		setP_Obat(Env.ZERO);
		setP_Other(Env.ZERO);
		setP_PinjamanKaryawan(Env.ZERO);
		setCorrectionAmt(Env.ZERO);
		setCorrectionAmtNonPeriodic(Env.ZERO);
		setCorrectionAmtPPh21(Env.ZERO);
		setCorrectionAmtPPh21NonPeriodic(Env.ZERO);
		setTotalOtherAllowances(Env.ZERO);
		setTotalOtherDeductions(Env.ZERO);
		setSourcePPhAmt(Env.ZERO);
		m_parent = parent;
	}
	
	/**
	 * 
	 *
	public void rescaleAllAmounts() {
		int precision = MUOM.getPrecision(getCtx(), getC_UOM_ID());
		GeneralCustomization.setScaleOf(this, NUMBERTYPE_COLUMNS, precision, BigDecimal.ROUND_HALF_UP, false);
	}
	*/
	
	private boolean isDuplicate ()
	{
		String sql = " SELECT DocumentNo FROM UNS_Payroll_Employee WHERE "
				+ " UNS_Employee_ID = ? AND C_Period_ID = ? AND AD_Org_ID = ? "
				+ " AND UNS_Payroll_Employee_ID <> ? AND DocStatus NOT IN (?,?)";
		String docNo = DB.getSQLValueString(
				get_TrxName(), sql, getUNS_Employee_ID(), getC_Period_ID(), 
				getAD_Org_ID(), getUNS_Payroll_Employee_ID(), "RE", "VO");
		if (!Util.isEmpty(docNo, true))
		{
			log.saveError("Duplicate Record Detected", "Data duplication detected - " + docNo);
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean beforeSave(boolean newRecord){
		if (getDocStatus().equals(DOCSTATUS_Drafted)){
			if (getP_PinjamanKaryawan().compareTo(MUNSEmployeeLoan.getLoanToCompany(
					getCtx(), getUNS_Employee_ID(), get_TrxName()))>0)
				throw new AdempiereUserError("Payment of employee loans more than remaining loans, " +
						"Decrease company payment loans");
		
//			if (getP_Koperasi().compareTo(MUNSEmployeeLoan.getLoanToKoperasi(
//					getCtx(), getUNS_Employee_ID(), get_TrxName()))>0)
//				throw new AdempiereUserError("Payment of employee loans more than remaining loans, " +
//						"Decrease koperasi payment loans");
			if (isDuplicate())
				return false;
		}
		
//		for(MDocType cDocType : MDocType.getOfDocBaseType(getCtx(), "PRE")){
//			if (!cDocType.isSOTrx())
//				setC_DocType_ID(cDocType.getC_DocType_ID());
//		}
		
		setTakeHomePay(
				getPayableBruto().subtract(getPayrollDeduction())
				.setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
		
		setStartDate(getParent().getStartDate());
		setEndDate(getParent().getEndDate());
		
		setA_TotalOverTime(getA_LemburJamPertama().add(getA_LemburJamBerikutnya())
				.add(getA_L1()).add(getA_L2()).add(getA_L3())
				.add(getA_L1R()).add(getA_L2R()).add(getA_L3R()));
		
		return super.beforeSave(newRecord);
	}
	
	/**
	 * set all jamsostek
	 * @param payConfig
	 */
	public void setJamSosTek(MUNSPayrollBaseEmployee payrollBase)
	{
		MUNSContractRecommendation recommendation = new MUNSContractRecommendation(
				getCtx(), payrollBase.getUNS_Contract_Recommendation_ID(), get_TrxName());
		
		MUNSBPJSConfig config = MUNSBPJSConfig.getConfig(getCtx(), recommendation, get_TrxName());
		if(config == null)
			throw new AdempiereException("Not found BPJS Configuration for this employee.");
		
		double amountJPKPaidByCom = 0.0;
		double amountJPKPaidByEmp = 0.0;
		double amountJKKPaidByCom = 0.0;
		double amountJKKPaidByEmp = 0.0;
		double amountJKPaidByCom = 0.0;
		double amountJKPaidByEmp = 0.0;
		double amountJHTPaidByCom = 0.0;
		double amountJHTPaidByEmp = 0.0;
		double amountJPPaidByComp = 0.0;
		double amountJPPaidByEmp = 0.0;
		double amtJPK = 0.0;
		double amtJK = 0.0;
		double amtJHT = 0.0;
		double amtJKK = 0.0;
		double amtJP = 0.0;
	
		double jpkKawinPaidByComp = config.getJPKKawinPaidByCompany().doubleValue()/100;
		double jpkKawinPaidByEmp = config.getJPKKawinPaidByEmployee().doubleValue()/100;
		double jpkLajangPaidByComp = config.getJPKLajangPaidByCompany().doubleValue()/100;
		double jpkLajangPaidByEmp = config.getJPKLajangPaidByEmployee().doubleValue()/100;
		double jkPaidByComp = config.getJKPaidByCompany().doubleValue()/100;
		double jkPaidByEmp = config.getJKPaidByEmployee().doubleValue()/100;
		double jkkPaidByComp = config.getJKKPaidByCompany().doubleValue()/100;
		double jkkPaidByEmp = config.getJKKPaidByEmployee().doubleValue()/100;
		double jhtPaidByComp = config.getJHTPaidByCompany().doubleValue()/100;
		double jhtPaidByEmp = config.getJHTPaidByEmployee().doubleValue()/100;
		double jpPaidByComp = config.getJPPaidByCompany().doubleValue()/100;
		double jpPaidByEmp =config.getJPPaidByEmployee().doubleValue()/100;
		
		double maxAmtCalcJPK = config.getMaxAmtToCalcJHT().doubleValue();
		double maxAmtCalcJK = config.getMaxAmtToCalcJK().doubleValue();
		double maxAmtCalcJKK = config.getMaxAmtToCalcJKK().doubleValue();
		double maxAmtCalcJHT = config.getMaxAmtToCalcJHT().doubleValue();
		double maxAmtCalcJP = config.getMaxAmtToCalcJP().doubleValue();
		
		double gajiBruto = payrollBase.getPayableBruto().doubleValue();
		double potongan = payrollBase.getDeduction().doubleValue();
		
		if (payrollBase.isJPKApplyed())
		{
			if(config.getJPKBasicCalculation().equals(
					MUNSPayrollConfiguration.JPKBASICCALCULATION_BaseOnBasicSalary))
			{
				amtJPK = getGPokok().doubleValue();
				if(maxAmtCalcJPK > 0 && amtJPK > maxAmtCalcJPK)
					amtJPK = maxAmtCalcJPK;
			}
			else if (config.getJPKBasicCalculation().equals(
					MUNSPayrollConfiguration.JPKBASICCALCULATION_BaseOnNettoSalary))
			{
				amtJPK = gajiBruto-potongan;
				if(maxAmtCalcJPK > 0 && amtJPK > maxAmtCalcJPK)
					amtJPK = maxAmtCalcJPK;
			}
			else if (config.getJPKBasicCalculation().equals(
					MUNSPayrollConfiguration.JPKBASICCALCULATION_BaseOnBruttoSalary))
			{
				amtJPK = gajiBruto;
				if(maxAmtCalcJPK > 0 && amtJPK > maxAmtCalcJPK)
					amtJPK = maxAmtCalcJPK;
			}
			else if (MUNSPayrollConfiguration.JPKBASICCALCULATION_BaseOnUMK.equals(
					config.getJPKBasicCalculation()))
			{
				amtJPK = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMKLevel_ID()).doubleValue();
				if(maxAmtCalcJPK > 0 && amtJPK > maxAmtCalcJPK)
					amtJPK = maxAmtCalcJPK;
			}
			else if (MUNSPayrollConfiguration.JPKBASICCALCULATION_BaseOnUMP.equals(
					config.getJPKBasicCalculation()))
			{
				amtJPK = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMPLevel_ID()).doubleValue();
				if(maxAmtCalcJPK > 0 && amtJPK > maxAmtCalcJPK)
					amtJPK = maxAmtCalcJPK;
			}
		}
		
		if (payrollBase.isJKApplyed())
		{
			if(config.getJKBasicCalculation().equals(
					MUNSPayrollConfiguration.JKBASICCALCULATION_BaseOnBasicSalary))
			{
				amtJK = getGPokok().doubleValue();
				if(maxAmtCalcJK > 0 && amtJK > maxAmtCalcJK)
					amtJK = maxAmtCalcJK;
			}
			else if (config.getJKBasicCalculation().equals(
					MUNSPayrollConfiguration.JKBASICCALCULATION_BaseOnNettoSalary))
			{
				amtJK = gajiBruto-potongan;
				if(maxAmtCalcJK > 0 && amtJK > maxAmtCalcJK)
					amtJK = maxAmtCalcJK;
			}
			else if (config.getJKBasicCalculation().equals(
					MUNSPayrollConfiguration.JKBASICCALCULATION_BaseOnBruttoSalary))
			{
				amtJK = gajiBruto;
				if(maxAmtCalcJK > 0 && amtJK > maxAmtCalcJK)
					amtJK = maxAmtCalcJK;
			}
			else if (MUNSPayrollConfiguration.JKBASICCALCULATION_BaseOnUMK.equals(
					config.getJKBasicCalculation()))
			{
				amtJK = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMKLevel_ID()).doubleValue();
				if(maxAmtCalcJK > 0 && amtJK > maxAmtCalcJK)
					amtJK = maxAmtCalcJK;
			}
			else if (MUNSPayrollConfiguration.JKBASICCALCULATION_BaseOnUMP.equals(
					config.getJKBasicCalculation()))
			{
				amtJK = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMPLevel_ID()).doubleValue();
				if(maxAmtCalcJK > 0 && amtJK > maxAmtCalcJK)
					amtJK = maxAmtCalcJK;
			}
		}
		
		if (payrollBase.isJKKApplyed())
		{
			if(config.getJKKBasicCalculation().equals(
					MUNSPayrollConfiguration.JKKBASICCALCULATION_BaseOnBasicSalary))
			{
				amtJKK = getGPokok().doubleValue();
				if(maxAmtCalcJKK > 0 && amtJKK > maxAmtCalcJKK)
					amtJKK = maxAmtCalcJKK;
			}
			else if (config.getJKKBasicCalculation().equals(
					MUNSPayrollConfiguration.JKKBASICCALCULATION_BaseOnNettoSalary))
			{
				amtJKK = gajiBruto-potongan;
				if(maxAmtCalcJKK > 0 && amtJKK > maxAmtCalcJKK)
					amtJKK = maxAmtCalcJKK;
			}
			else if (config.getJKKBasicCalculation().equals(
					MUNSPayrollConfiguration.JKKBASICCALCULATION_BaseOnBruttoSalary))
			{
				amtJKK = gajiBruto;
				if(maxAmtCalcJKK > 0 && amtJKK > maxAmtCalcJKK)
					amtJKK = maxAmtCalcJKK;
			}
			else if (MUNSPayrollConfiguration.JKKBASICCALCULATION_BaseOnUMK.equals(
					config.getJKKBasicCalculation()))
			{

				amtJKK = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMKLevel_ID()).doubleValue();
				if(maxAmtCalcJKK > 0 && amtJKK > maxAmtCalcJKK)
					amtJKK = maxAmtCalcJKK;
			}
			else if (MUNSPayrollConfiguration.JKKBASICCALCULATION_BaseOnUMP.equals(
					config.getJKKBasicCalculation()))
			{
				amtJKK = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMPLevel_ID()).doubleValue();
				if(maxAmtCalcJKK > 0 && amtJKK > maxAmtCalcJKK)
					amtJKK = maxAmtCalcJKK;
			}
		}
		
		if (payrollBase.isJHTApplyed())
		{
			if(config.getJHTBasicCalculation().equals(
					MUNSPayrollConfiguration.JHTBASICCALCULATION_BaseOnBasicSalary))
			{
				amtJHT = getGPokok().doubleValue();
				if(maxAmtCalcJHT > 0 && amtJHT > maxAmtCalcJHT)
					amtJHT = maxAmtCalcJHT;
			}
			else if (config.getJHTBasicCalculation().equals(
					MUNSPayrollConfiguration.JHTBASICCALCULATION_BaseOnNettoSalary))
			{
				amtJHT = gajiBruto-potongan;
				if(maxAmtCalcJHT > 0 && amtJHT > maxAmtCalcJHT)
					amtJHT = maxAmtCalcJHT;
			}
			else if (config.getJHTBasicCalculation().equals(
					MUNSPayrollConfiguration.JHTBASICCALCULATION_BaseOnBruttoSalary))
			{
				amtJHT = gajiBruto;
				if(maxAmtCalcJHT > 0 && amtJHT > maxAmtCalcJHT)
					amtJHT = maxAmtCalcJHT;
			}
			else if (MUNSPayrollConfiguration.JHTBASICCALCULATION_BaseOnUMK.equals(
					config.getJHTBasicCalculation()))
			{
				amtJHT = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMKLevel_ID()).doubleValue();
				if(maxAmtCalcJHT > 0 && amtJHT > maxAmtCalcJHT)
					amtJHT = maxAmtCalcJHT;
			}
			else if (MUNSPayrollConfiguration.JHTBASICCALCULATION_BaseOnUMP.equals(
					config.getJHTBasicCalculation()))
			{
				amtJHT = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMPLevel_ID()).doubleValue();
				if(maxAmtCalcJHT > 0 && amtJHT > maxAmtCalcJHT)
					amtJHT = maxAmtCalcJHT;
			}
		}
		
		if (payrollBase.isJPApplied())
		{
			if(config.getJPBasicCalculation().equals(
					MUNSPayrollConfiguration.JPBASICCALCULATION_BaseOnBasicSalary))
			{
				amtJP = getGPokok().doubleValue();
				if(maxAmtCalcJP > 0 && amtJP > maxAmtCalcJP)
					amtJP = maxAmtCalcJP;
			}
			else if (config.getJPBasicCalculation().equals(
					MUNSPayrollConfiguration.JPBASICCALCULATION_BaseOnNettoSalary))
			{
				amtJP = gajiBruto-potongan;
				if(maxAmtCalcJP > 0 && amtJP > maxAmtCalcJP)
					amtJP = maxAmtCalcJP;
			}
			else if (config.getJPBasicCalculation().equals(
					MUNSPayrollConfiguration.JPBASICCALCULATION_BaseOnBruttoSalary))
			{
				amtJP = gajiBruto;
				if(maxAmtCalcJP > 0 && amtJP > maxAmtCalcJP)
					amtJP = maxAmtCalcJP;
			}
			else if (MUNSPayrollConfiguration.JPBASICCALCULATION_BaseOnUMK.equals(
					config.getJPBasicCalculation()))
			{
				amtJP = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMKLevel_ID()).doubleValue();
				if(maxAmtCalcJP > 0 && amtJP > maxAmtCalcJP)
					amtJP = maxAmtCalcJP;
			}
			else if (MUNSPayrollConfiguration.JPBASICCALCULATION_BaseOnUMP.equals(
					config.getJPBasicCalculation()))
			{
				amtJP = MUNSUMKLevel.getAmount(get_TrxName(), payrollBase.getUMPLevel_ID()).doubleValue();
				if(maxAmtCalcJP > 0 && amtJP > maxAmtCalcJP)
					amtJP = maxAmtCalcJP;
			}
		}
		
		MUNSEmployee employee = MUNSEmployee.get(getCtx(), getUNS_Employee_ID());
		
		if(employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_Kawin0Tanggungan)
				||employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_Kawin1Tanggungan)
				||employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_Kawin2Tanggungan)
				||employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_Kawin3Tanggungan))
		{
			amountJPKPaidByCom = amtJPK * jpkKawinPaidByComp;
			amountJPKPaidByEmp = amtJPK * jpkKawinPaidByEmp;
		}
		else if(employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_TidakKawin0Tanggungan)
				||employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_TidakKawin1Tanggungan)
				||employee.getMaritalStatus().equals(MUNSEmployee.MARITALSTATUS_TidakKawin2Tanggungan))
		{
			amountJPKPaidByCom = amtJPK * jpkLajangPaidByComp;
			amountJPKPaidByEmp = amtJPK * jpkLajangPaidByEmp;
		}
		
		amountJKPaidByCom = amtJK * jkPaidByComp;
		amountJKPaidByEmp = amtJK * jkPaidByEmp;
		amountJHTPaidByCom = amtJHT * jhtPaidByComp;
		amountJHTPaidByEmp = amtJHT * jhtPaidByEmp;
		amountJKKPaidByCom = amtJKK * jkkPaidByComp;
		amountJKKPaidByEmp = amtJKK * jkkPaidByEmp;
		amountJPPaidByComp = amtJP * jpPaidByComp;
		amountJPPaidByEmp = amtJP * jpPaidByEmp;
		
		
		if(payrollBase.isJHTApplyed())
		{
			setA_JHT(new BigDecimal(amountJHTPaidByCom).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
			setP_JHT(new BigDecimal(amountJHTPaidByEmp).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
		}
		if (payrollBase.isJKApplyed())
		{
			setA_JK(new BigDecimal(amountJKPaidByCom).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
			setP_JK(new BigDecimal(amountJKPaidByEmp).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
		}
		if (payrollBase.isJKKApplyed())
		{
			setA_JKK(new BigDecimal(amountJKKPaidByCom).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
			setP_JKK(new BigDecimal(amountJKKPaidByEmp).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
		}
		if (payrollBase.isJPKApplyed())
		{
			setA_JPK(new BigDecimal(amountJPKPaidByCom).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
			setP_JPK(new BigDecimal(amountJPKPaidByEmp).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
		}
		
		if (payrollBase.isJPApplied())
		{
			setA_JP(new BigDecimal(amountJPPaidByComp).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
			setP_JP(new BigDecimal(amountJPPaidByEmp).setScale(SCALE_PRECISION, RoundingMode.HALF_UP));
		}
	}
	
	/**
	 * 
	 * @return
	 */
//	public BigDecimal getPotongan()
//	{
//		return getP_Koperasi().add(getP_Label()).add(getP_ListrikAir()).add(getP_Mangkir())
//				.add(getP_Obat()).add(getP_Other()).add(getP_PinjamanKaryawan()).add(getPMakan());
//	}
	
	public BigDecimal getPayableBruto ()
	{
		BigDecimal bruto = getGPokok();
		BigDecimal addAmt = getA_LemburJamPertama().add(getA_LemburJamBerikutnya()).add(getA_L1()).
				add(getA_L2()).add(getA_L3()).add(getTotalOtherAllowances()).add(getCorrectionAmt())
				.add(getCorrectionAmtNonPeriodic()).add(getCorrectionAmtPPh21()).add(getCorrectionAmtPPh21NonPeriodic())
				.add(getA_L1R()).add(getA_L2R()).add(getA_L3R());
		
		bruto = bruto.add(addAmt);
		return bruto;
	}
	
	public BigDecimal getPayrollDeduction ()
	{
		BigDecimal deduction =  getP_JHT().add(getP_JK()).add(getP_JKK()).add(getP_JPK()).add(getP_JP()).add(getTotalOtherDeductions());
		if (!isPPH21PaidByCompany())
			deduction = deduction.add(getPPH21());
		return deduction;
	}
	
	public BigDecimal getPPHBrutoAmt ()
	{
		BigDecimal baseAmt = getGPokok();
		BigDecimal addAmt = //getA_JP().add(getA_JK()).add(getA_JKK()).add(getA_JPK()).
				getA_JK().add(getA_JKK()).add(getA_JPK()).
				add(getA_LemburJamPertama()).add(getA_LemburJamBerikutnya()).add(getA_L1()).
				add(getA_L2()).add(getA_L3()).add(getCorrectionAmtPPh21()).add(getCorrectionAmtPPh21NonPeriodic())
				.add(getA_L1R()).add(getA_L2R()).add(getA_L3R());
		
		MUNSPayrollCostBenefit[] pphComps = getCostBenefits(true);
		for (int i=0; i<pphComps.length; i++) 
		{
			if (!pphComps[i].isPPHComp() || !pphComps[i].isBenefit() || !pphComps[i].isMonthlyPPHComp())
				continue;
			addAmt = addAmt.add(pphComps[i].getAmount());
		}
		
		baseAmt = baseAmt.add(addAmt);
		return baseAmt;
	}
	
	
	public BigDecimal getTotalOtherDeductionPPh21Comp ()
	{
		BigDecimal pphDeducAmt = Env.ZERO;
		MUNSPayrollCostBenefit[] pphComps = getCostBenefits(true);
		for (int i=0; i<pphComps.length; i++) 
		{
			if (!pphComps[i].isPPHComp() || pphComps[i].isBenefit()  || !pphComps[i].isMonthlyPPHComp())
				continue;
			pphDeducAmt = pphDeducAmt.add(pphComps[i].getAmount());
		}
		
		return pphDeducAmt;
	}
	
	public BigDecimal getPPh21Deduc (){
		BigDecimal pphDeducAmt = getP_JP().add(getP_JHT()).add(getP_JK()).add(getP_JKK()).
				add(getP_Mangkir());
		return pphDeducAmt;
	}
	
	public BigDecimal getPPHDeducAmt ()
	{
		BigDecimal pphDeducAmt = getP_JP().add(getP_JHT()).add(getP_JK()).add(getP_JKK()).
				add(getP_Mangkir());
		
		MUNSPayrollCostBenefit[] pphComps = getCostBenefits(true);
		for (int i=0; i<pphComps.length; i++) 
		{
			if (!pphComps[i].isPPHComp() || pphComps[i].isBenefit() || !pphComps[i].isMonthlyPPHComp())
				continue;
			pphDeducAmt = pphDeducAmt.add(pphComps[i].getAmount());
		}
		
		return pphDeducAmt;
	}

	/**
	 * LD1 + LD2 + LD3 + Lembur (Jam 1 + Jam Berikutnya) + A_Other + A_Premi + A_Rapel + GP + T.Jabatan +
	 * T.Kesejahteraan + T.Khusus + T.Lembur.
	 * @return
	 */
//	public BigDecimal getGajiBruto()
//	{
//		return getA_L1().add(getA_L2()).add(getA_L3())
//				.add(getA_LemburJamPertama()).add(getA_LemburJamBerikutnya())
//				.add(getA_Other()).add(getA_Premi()).add(getA_Rapel()).add(getGPokok())
//				.add(getG_T_Jabatan()).add(getG_T_Kesejahteraan()).add(getG_T_Khusus())
//				.add(getG_T_Lembur());
//	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		// 
		if (docStatus.equals(DocumentEngine.STATUS_Drafted)
    			|| docStatus.equals(DocumentEngine.STATUS_Invalid)) {
    		options[index++] = DocumentEngine.ACTION_Prepare;
    	}
    	
    	// If status = Completed, add "Reactivte" in the list
    	if (docStatus.equals(DocumentEngine.STATUS_Completed)) {
    		options[index++] = DocumentEngine.ACTION_ReActivate;
    		options[index++] = DocumentEngine.ACTION_Void;
    	}   	
    		
    	return index;
	}

	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	@Override
	public boolean processIt(String action) throws Exception {
		// 
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		// 
		log.info(toString());
		setProcessed(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		// 
		log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		// 
		log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(!"Y".equals(getGeneratePay()))
		{
			m_processMsg = "This document has not generated. Please generate payroll first.";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
			m_justPrepared = true;
			
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		// 
		log.info(toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		// 
		log.info(toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		// 
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		log.info(toString());
		
//		Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		String remarks = getRemarks();
		if (remarks == null)
			remarks = "";
		if (!remarks.contains(IMPORT_REMARKS))
		{
			getCostBenefits(false);
			for (int i=0; i<m_CostBenefits.length; i++)
			{
				if (m_CostBenefits[i].getAmount().signum() == 0)
					m_CostBenefits[i].deleteEx(true);
				String type = m_CostBenefits[i].getUNS_Payroll_Component_Conf().getCostBenefitType();
				if (type != null && !MUNSPayrollComponentConf.COSTBENEFITTYPE_PinjamanKaryawan.equals(type)) {
					MUNSPeriodicCostBenefitLine.pay(getUNS_Employee_ID(), m_CostBenefits[i].getAmount(), 
							type, get_TrxName());
				}
			}

			createInstallment();
		}
		
		if (!MUNSYearlyPayrollSummary.updateRecord(this, false))
		{
			m_processMsg = "Failed when try to update Yearly Payroll Summary";
			return DOCSTATUS_Invalid;
		}
		
		if (!TPayrollReportProcessor.create(this))
		{
			m_processMsg = CLogger.retrieveErrorString("Could not create Tmp Payroll Report");
			return DOCSTATUS_Invalid;
		}
		
		if (!remarks.contains(IMPORT_REMARKS))
		{
			m_processMsg = updatePayrollCorrection(false, 0);
			if(m_processMsg != null)
				return DOCSTATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
				
		setProcessed(true);	
		
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		if (log.isLoggable(Level.INFO))
			log.log(Level.INFO, "Void-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(!isVoidFromCorrection)
		{
			String sql = "SELECT DocumentNo FROM UNS_PayrollPayment WHERE UNS_PayrollPayment_ID = "
					+ " (SELECT UNS_PayrollPayment_ID FROM UNS_PayrollPayment_BA WHERE UNS_PayrollPayment_BA_ID = "
					+ " (SELECT UNS_PayrollPayment_BA_ID FROM UNS_PayrollPayment_Emp "
					+ " WHERE UNS_Payroll_Employee_ID = ?))";
			String existsDoc = DB.getSQLValueString(get_TrxName(), sql, getUNS_Payroll_Employee_ID());
			if (existsDoc != null)
			{
				m_processMsg = "The payroll is linked to payroll payment document [" + existsDoc + "]";
				return false;
			}
		}
		if (DOCSTATUS_Completed.equals(getDocStatus()))
		{
			getCostBenefits(false);
			for (int i=0; i<m_CostBenefits.length; i++)
			{
				String type = m_CostBenefits[i].getUNS_Payroll_Component_Conf().getCostBenefitType();
				if (type != null && !MUNSPayrollComponentConf.COSTBENEFITTYPE_PinjamanKaryawan.equals(type)) {
					MUNSPeriodicCostBenefitLine.pay(getUNS_Employee_ID(), m_CostBenefits[i].getAmount().negate(), 
							type, get_TrxName());
				}
			}
			
			if (!MUNSYearlyPayrollSummary.updateRecord(this, true))
			{
				m_processMsg = "Failed when try to update Yearly Payroll Summary";
				return false;
			}
			
			MUNSLoanInstallment[] installments = MUNSLoanInstallment.gets(get_TrxName(), get_ID());
			for (int i=0; i<installments.length; i++)
			{
				try {
					if (!installments[i].processIt(DOCACTION_Void))
					{
						m_processMsg = installments[i].getProcessMsg();
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
					m_processMsg = e.getMessage();
					return false;
				}
				if (!installments[i].save())
					return false;
			}
			
			if (!TPayrollReportProcessor.remove(get_TrxName(), getUNS_Employee_ID(), getC_Period_ID(), false))
			{
				m_processMsg = CLogger.retrieveErrorString("Could not remove Tmp Payroll Report");
				return false;
			}
			
			m_processMsg = updatePayrollCorrection(true, 0);
			if(m_processMsg != null)
				return false;
		}		
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		// 
		log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		// 
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		// 
		return false;
	}

	@Override
	public boolean reActivateIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		String sql = "SELECT DocumentNo FROM UNS_PayrollPayment WHERE UNS_PayrollPayment_ID = "
				+ " (SELECT UNS_PayrollPayment_ID FROM UNS_PayrollPayment_BA WHERE UNS_PayrollPayment_BA_ID = "
				+ " (SELECT UNS_PayrollPayment_BA_ID FROM UNS_PayrollPayment_Emp "
				+ " WHERE UNS_Payroll_Employee_ID = ?))";
		String existsDoc = DB.getSQLValueString(get_TrxName(), sql, getUNS_Payroll_Employee_ID());
		if (existsDoc != null)
		{
			m_processMsg = "The payroll is linked to payroll payment document [" + existsDoc + "]";
			return false;
		}
		
		String remarks = getRemarks();
		if (remarks == null)
			remarks = "";
		
		if (!remarks.contains(IMPORT_REMARKS))
		{
			getCostBenefits(false);
			for (int i=0; i<m_CostBenefits.length; i++)
			{
				String type = m_CostBenefits[i].getUNS_Payroll_Component_Conf().getCostBenefitType();
				if (type != null && !MUNSPayrollComponentConf.COSTBENEFITTYPE_PinjamanKaryawan.equals(type)) {
					MUNSPeriodicCostBenefitLine.pay(getUNS_Employee_ID(), m_CostBenefits[i].getAmount().negate(), 
							type, get_TrxName());
				}
			}
		}		
		
		if (!MUNSYearlyPayrollSummary.updateRecord(this, true))
		{
			m_processMsg = "Failed when try to update Yearly Payroll Summary";
			return false;
		}
		
		if (!remarks.contains(IMPORT_REMARKS))
		{
			MUNSLoanInstallment[] installments = MUNSLoanInstallment.gets(get_TrxName(), get_ID());
			for (int i=0; i<installments.length; i++)
			{
				try {
					if (!installments[i].processIt(DOCACTION_Void))
					{
						m_processMsg = installments[i].getProcessMsg();
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
					m_processMsg = e.getMessage();
					return false;
				}
				if (!installments[i].save())
					return false;
			}
		}
		
		if (!TPayrollReportProcessor.remove(get_TrxName(), getUNS_Employee_ID(), getC_Period_ID(), true))
		{
			m_processMsg = CLogger.retrieveErrorString("Could not remove Tmp Payroll Report");
			return false;
		}
		
		if (!remarks.contains(IMPORT_REMARKS))
		{
			m_processMsg = updatePayrollCorrection(true, 0);
			if(m_processMsg != null)
				return false;
		}
				
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		
		return true;
	}

	@Override
	public String getSummary() {
		// 
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO define an info
		return getDocumentNo();
	}

	@Override
	public File createPDF() {
		// 
		return null;
	}

	@Override
	public String getProcessMsg() {
		// 
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// 
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// 
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// 
		return null;
	}
	
	public boolean hasGeneratePay()
	{
		return getGeneratePay().equals("Y");
	}
	
	/**
	 * 
	 * @param monthlyPresence
	 */
	public void createFrom(PO po)
	{
		
		MUNSContractRecommendation contract = null;
		MUNSEmployee employee = null;
		if (po instanceof MUNSEmployee)
		{
			employee = (MUNSEmployee)po;
		}
		else if(po instanceof MUNSContractRecommendation)
		{
			contract = (MUNSContractRecommendation) po;
			employee = MUNSEmployee.get(getCtx(), contract.getUNS_Employee_ID());
		}
		else if(po instanceof MUNSMonthlyPresenceSummary)
		{
			X_UNS_MonthlyPresenceSummary presence = (X_UNS_MonthlyPresenceSummary) po;
			employee = MUNSEmployee.get(getCtx(), presence.getUNS_Employee_ID());
		}
		
		else if(po instanceof MUNSHalfPeriodPresence)
		{
			X_UNS_HalfPeriod_Presence presence = (X_UNS_HalfPeriod_Presence) po;
			employee = MUNSEmployee.get(getCtx(), presence.getUNS_Employee_ID());
		}
		else
		{
			log.log(Level.SEVERE, "Unhandled instance");
			return;
		}
		
		//always use contract exists by effective date
		if(contract == null)
		{
			MUNSMonthlyPayrollEmployee parent = new MUNSMonthlyPayrollEmployee(
					getCtx(), getUNS_MonthlyPayroll_Employee_ID(), get_TrxName());
			contract = MUNSContractRecommendation.getOf(
					getCtx(), employee.get_ID(), parent.getEndDate(), getAD_Org_ID(), get_TrxName());
			if(contract == null)
				throw new AdempiereException("No contract found");
		}
		
		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
				getAD_Org_ID()
				, contract.getNewSectionOfDept_ID()
				,contract.getNextContractType()
				, Env.getContextAsDate(getCtx(), "Date")
				, get_TrxName());
		if(null == payrollTerm)
			payrollTerm = contract.getNextPayrollTerm();
		
		setC_Job_ID(contract.getNewJob_ID());
		setPayrollTerm(payrollTerm);
		setUNS_Employee_ID(employee.getUNS_Employee_ID());
		setShift(contract.getNewShift());
		
		MUNSPayrollBaseEmployee pb = MUNSPayrollBaseEmployee.get(
				getCtx(), contract.get_ID(), get_TrxName());
		
		if (pb == null)
			throw new AdempiereException("Could not find Payroll Base Employee " + employee.toString());
		
		setUNS_Contract_Recommendation_ID(contract.get_ID());
		setUNS_PayrollBase_Employee_ID(pb.get_ID());
	}
	
	/**
	 * 
	 */
	public String calculatePayroll(boolean resetCostBenefit)
	{
		PayrollEmployeeGenerator payGenerator = new PayrollEmployeeGenerator();
		payGenerator.setPayrollEmploye(this);
		payGenerator.setIsResetCostAndBenefit(resetCostBenefit);
		return payGenerator.calculatePay();
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		if(success)
			updateHeader();
		if (getOriginal_ID() > 0 && is_ValueChanged(COLUMNNAME_TakeHomePay)
				&& !MUNSPayrollCorrectionLine.update(get_TrxName(), 
						getUNS_Payroll_Employee_ID(), getOriginal_ID(), getTakeHomePay()))
		{
			return false;
		}
		return super.afterSave(newRecord, success);
	}
	
	private void updateHeader()
	{
		getParent().calculatePaySummary();
	}
	
	@Override
	public MUNSMonthlyPayrollEmployee getParent()
	{
		if (null == m_parent)
		{
			m_parent = new MUNSMonthlyPayrollEmployee(getCtx(), 
					getUNS_MonthlyPayroll_Employee_ID(), get_TrxName());
		}
		
		return m_parent;
	}
	
	private void createInstallment (double cooperativeAmt, double companyAmt)
	{
		if (cooperativeAmt == 0 && companyAmt == 0)
			return;
		
		List<MUNSEmployeeLoan> listOfEmployeeLoan = MUNSEmployeeLoan.gets(
				getCtx(), getUNS_Employee_ID(), get_TrxName());
		
		for (MUNSEmployeeLoan employeeLoan : listOfEmployeeLoan)
		{
			double loanAmtLeft = employeeLoan.getLoanAmtLeft().doubleValue();
			double installment = employeeLoan.getInstallment().doubleValue();
			double paidAmt = 0;
			if (loanAmtLeft > installment)
				loanAmtLeft = installment;
			
			if(MUNSEmployeeLoan.LOANTYPE_Koperasi.equals(employeeLoan.getLoanType()))
			{
				paidAmt = cooperativeAmt;
				if(cooperativeAmt > loanAmtLeft) {
					cooperativeAmt = cooperativeAmt - loanAmtLeft;
					paidAmt = loanAmtLeft;
				} else 
					cooperativeAmt = 0;
			}
			else 
//				if(MUNSEmployeeLoan.LOANTYPE_Company.equals(employeeLoan.getLoanType()))
			{
				paidAmt = companyAmt;
				if(companyAmt > loanAmtLeft) {
					companyAmt = companyAmt - loanAmtLeft;
					paidAmt = loanAmtLeft;
				} else 
					companyAmt = 0;
			}
//			else
//				throw new AdempiereUserError("UNKNOWN LOAN TYPE");
			
			if(paidAmt <= 0)
				continue;

			MUNSLoanInstallment loanInstallment = new MUNSLoanInstallment(getCtx(), 0, get_TrxName());
			
			loanInstallment.setAD_Org_ID(employeeLoan.getAD_Org_ID());
			loanInstallment.setUNS_Employee_Loan_ID(employeeLoan.get_ID());
			loanInstallment.setPaidAmt(new BigDecimal(paidAmt));
			loanInstallment.setPaidDate(getEndDate());
			loanInstallment.setC_DocType_ID(MDocType.getDocType(I_C_DocType.DOCBASETYPE_LoanInstallment));
			loanInstallment.setIsCashPayment(false);
			loanInstallment.setUNS_Payroll_Employee_ID(getUNS_Payroll_Employee_ID());
			loanInstallment.saveEx();
			
			try {
				boolean ok = loanInstallment.processIt(MUNSLoanInstallment.DOCACTION_Complete);
				if (!ok)
				{
					m_processMsg = loanInstallment.getProcessMsg();
					throw new AdempiereException(m_processMsg);
				}
			} 
			catch(Exception ex) {
				throw new AdempiereException(ex.getMessage());
			}
		}
		
		if (listOfEmployeeLoan.size() > 0 && (companyAmt != 0 || cooperativeAmt != 0))
			createInstallment(cooperativeAmt, companyAmt);
	}
	
	private void createInstallment ()
	{
		double leftPaidToCompany = 0.0;
		double leftPaidToKoperasi = 0.0;
		getCostBenefits(false);
		for (int i=0; i<m_CostBenefits.length; i++)
		{
			if (!"PKR".equals(m_CostBenefits[i].getUNS_Payroll_Component_Conf().getCostBenefitType()))
				continue;
			leftPaidToCompany = m_CostBenefits[i].getAmount().doubleValue();
		}
		
		if (leftPaidToCompany == 0 && leftPaidToKoperasi == 0)
		{
			return;
		}
		
		createInstallment(leftPaidToKoperasi, leftPaidToCompany);
	}
	
	public BigDecimal getNonPeriodicBenefitPPh21Amt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "Y");
			st.setString(3, "N");
			st.setString(4, "Y");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getPeriodicBenefitPPh21Amt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "Y");
			st.setString(3, "Y");
			st.setString(4, "Y");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getPeriodicBenefitAmt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "Y");
			st.setString(3, "Y");
			st.setString(4, "N");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getNonPeriodicBenefitAmt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "Y");
			st.setString(3, "N");
			st.setString(4, "N");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getPeriodicDeducPPh21Amt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "N");
			st.setString(3, "Y");
			st.setString(4, "Y");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getNonPeriodicDeducPPh21Amt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "N");
			st.setString(3, "N");
			st.setString(4, "Y");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getPeriodicDeducAmt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "N");
			st.setString(3, "Y");
			st.setString(4, "N");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	public BigDecimal getNonPeriodicDeducAmt () {
		String sql = "SELECT Amount FROM UNS_Payroll_CostBenefit WHERE "
				+ " UNS_Payroll_Employee_ID = ? AND IsBenefit = ? AND IsMonthlyPPHComp = ? "
				+ " AND IsPPhComp = ? AND IsActive = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		BigDecimal amts = Env.ZERO;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Payroll_Employee_ID());
			st.setString(2, "N");
			st.setString(3, "N");
			st.setString(4, "N");
			st.setString(5, "Y");
			rs = st.executeQuery();
			while (rs.next())
			{
				BigDecimal amt = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), getAD_Client_ID());
				if (amt == null)
					amt = Env.ZERO;
				amts = amts.add(amt);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
	
		return amts;
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		if (!TPayrollReportProcessor.remove(get_TrxName(), getUNS_Employee_ID(), getC_Period_ID(), false))
			return false;
		
		String sql = "SELECT pc.DocumentNo FROM UNS_PayrollCorrection pc WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_PayrollCorrectionLine pcl WHERE pcl.UNS_PayrollCorrection_ID"
				+ " = pc.UNS_PayrollCorrection_ID AND pcl.UNS_Payroll_Employee_ID = ?)";
		String correction = DB.getSQLValueString(get_TrxName(), sql, get_ID());
		if(!Util.isEmpty(correction, true))
		{
			log.saveError("Error", "Payroll employee has linked with correction type " + correction + ".");
			return false;
		}
		return super.beforeDelete();
	}
	
	public void initCorrectionAmt ()
	{
		BigDecimal pphAmtPeriodic = MUNSPayrollCorrection.getBalancePPh21(getUNS_Employee_ID(), 0, true, get_TrxName());
		BigDecimal pphAmtNonPeriodic = MUNSPayrollCorrection.getBalancePPh21(getUNS_Employee_ID(), 0, false, get_TrxName());
		BigDecimal amtPeriodic = MUNSPayrollCorrection.getBalance(getUNS_Employee_ID(), 0, true, get_TrxName());
		BigDecimal amtNonPeriodic = MUNSPayrollCorrection.getBalance(getUNS_Employee_ID(), 0, false, get_TrxName());
		setCorrectionAmt(amtPeriodic);
		setCorrectionAmtNonPeriodic(amtNonPeriodic);
		setCorrectionAmtPPh21(pphAmtPeriodic);
		setCorrectionAmtPPh21NonPeriodic(pphAmtNonPeriodic);
	}
	
	public String updatePayrollCorrection(boolean isReversal, int C_Period_ID) {
		
		boolean ok = false;
		
		if(getCorrectionAmt().signum() != 0)
		{
			ok = MUNSPayrollCorrection.pay(getUNS_Employee_ID(), C_Period_ID, getCorrectionAmt(), false, true, isReversal, get_TrxName());
			if(!ok)
				return "Error when try to update Correction Amount Periodic"; 
		}
		
		if(getCorrectionAmtNonPeriodic().signum() != 0)
		{
			ok = MUNSPayrollCorrection.pay(getUNS_Employee_ID(), C_Period_ID, getCorrectionAmtNonPeriodic(), false, false, isReversal , get_TrxName());
			if(!ok)
				return "Error when try to update Correction Amount Non Periodic";
		}
		
		if(getCorrectionAmtPPh21().signum() != 0)
		{
			ok = MUNSPayrollCorrection.pay(
					getUNS_Employee_ID(), C_Period_ID, getCorrectionAmtPPh21(), true, true, isReversal, get_TrxName());
			if(!ok)
				return "Error when try to update Correction Amount (PPH21) Periodic";
		}
		
		if(getCorrectionAmtPPh21NonPeriodic().signum() != 0)
		{
			ok = MUNSPayrollCorrection.pay(
					getUNS_Employee_ID(), C_Period_ID, getCorrectionAmtPPh21NonPeriodic(), true, false, isReversal, get_TrxName());
			if(!ok)
				return "Error when try to update Correction Amount (PPH21) Non Periodic";
		}
		
		return null;
	}
	
	public static MUNSPayrollEmployee getOriginalDoc (
			String trxName, int employeeID, int periodID)
	{
		String clause = "Original_ID IS NULL AND UNS_Employee_ID = ? AND C_Period_ID = ?";
		MUNSPayrollEmployee payroll = Query.get(Env.getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
				Table_Name, clause, trxName).setParameters(employeeID, periodID).first();
		return payroll;
	}
	
	/**
	 * @param voidFromCorrection
	 */
	public void setVoidFromCorrection(boolean voidFromCorrection)
	{
		isVoidFromCorrection = voidFromCorrection;
	}
	
	public static BigDecimal getLastRevenue(Properties ctx, int EmployeeID, boolean GP, String trxName)
	{
		String sql = "SELECT GPokok FROM UNS_Payroll_Employee WHERE UNS_Employee_ID = ?"
				+ " ORDER BY EndDate DESC";
		if(!GP)
			sql.replace("GPokok", "TakeHomePay");
		BigDecimal revenue = DB.getSQLValueBD(trxName, sql, EmployeeID);
		if(revenue == null)
			revenue = Env.ZERO;
		return revenue;
	}
}
