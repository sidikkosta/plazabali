package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MOrg;
import org.compiere.model.MPeriod;
import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSGlobalIncentivesConfig extends X_UNS_GlobalIncentivesConfig {

	private String m_errorMsg = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2605522229879642958L;

	/**
	 * @param ctx
	 * @param UNS_GlobalIncentivesConfig_ID
	 * @param trxName
	 */
	public MUNSGlobalIncentivesConfig(Properties ctx,
			int UNS_GlobalIncentivesConfig_ID, String trxName) {
		super(ctx, UNS_GlobalIncentivesConfig_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSGlobalIncentivesConfig(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
	}

	
	/**
	 * 
	 * @param ctx
	 * @param AD_Org_ID
	 * @param shopType
	 * @param C_Period_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSGlobalIncentivesConfig get(Properties ctx, int AD_Org_ID, 
			String shopType, int C_Period_ID, String trxName)
	{
		String sql = "SELECT UNS_GlobalIncentivesConfig_ID FROM UNS_GlobalIncentivesConfig gic "
				+ " INNER JOIN C_Period p ON gic.C_Period_ID=p.C_Period_ID "
				+ " WHERE gic.AD_Org_ID=? AND gic.ShopType=? "
				+ " 	AND p.StartDate <= (SELECT StartDate FROM C_Period "
				+ "							WHERE C_Period_ID=?)"
				+ " ORDER BY p.StartDate DESC";
		
		int gic_id = DB.getSQLValue(trxName, sql, AD_Org_ID, shopType, C_Period_ID);
		
		if (gic_id <= 0) {
			gic_id = DB.getSQLValue(trxName, sql, 0, shopType, C_Period_ID);
		}
		
		MUNSGlobalIncentivesConfig GIC = null;
		
		if (gic_id > 0) {
			GIC = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, 
					"UNS_GlobalIncentivesConfig_ID=" + gic_id, trxName)
					.firstOnly();
		}
		
		return GIC;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getProcessMsg()
	{
		return m_errorMsg;
	}
	
	@Override
	protected boolean beforeSave(boolean newrecord)
	{
		if (newrecord
				|| is_ValueChanged(COLUMNNAME_ShopType)
				|| is_ValueChanged(COLUMNNAME_C_Period_ID))
		{
			String sql = "SELECT 1 FROM UNS_GlobalIncentivesConfig "
					+ "WHERE AD_Org_ID=? AND ShopType=? AND C_Period_ID=?";
			
			int count = DB.getSQLValue(get_TrxName(), sql, 
					getAD_Org_ID(), getShopType(), getC_Period_ID());
			
			if (count > 0) {
				m_errorMsg = "Duplicate configuration for ShopType \"" 
						+ getShopType() + "\" of Period " 
						+ MPeriod.get(getCtx(), getC_Period_ID()).getName() 
						+ " of organization " + MOrg.get(getCtx(), getAD_Org_ID()).getName();
				log.saveError("", m_errorMsg);
				return false;
			}
		}
		
		return true;
	}
}
