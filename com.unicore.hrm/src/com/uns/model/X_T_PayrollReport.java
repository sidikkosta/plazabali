/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for T_PayrollReport
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_T_PayrollReport extends PO implements I_T_PayrollReport, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180917L;

    /** Standard Constructor */
    public X_T_PayrollReport (Properties ctx, int T_PayrollReport_ID, String trxName)
    {
      super (ctx, T_PayrollReport_ID, trxName);
      /** if (T_PayrollReport_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_T_PayrollReport (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_T_PayrollReport[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException
    {
		return (org.compiere.model.I_C_Year)MTable.get(getCtx(), org.compiere.model.I_C_Year.Table_Name)
			.getPO(getC_Year_ID(), get_TrxName());	}

	/** Set Year.
		@param C_Year_ID 
		Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID)
	{
		if (C_Year_ID < 1) 
			set_Value (COLUMNNAME_C_Year_ID, null);
		else 
			set_Value (COLUMNNAME_C_Year_ID, Integer.valueOf(C_Year_ID));
	}

	/** Get Year.
		@return Calendar Year
	  */
	public int getC_Year_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Year_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set is Periodic?.
		@param isPeriodic is Periodic?	  */
	public void setisPeriodic (boolean isPeriodic)
	{
		set_Value (COLUMNNAME_isPeriodic, Boolean.valueOf(isPeriodic));
	}

	/** Get is Periodic?.
		@return is Periodic?	  */
	public boolean isPeriodic () 
	{
		Object oo = get_Value(COLUMNNAME_isPeriodic);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set M1.
		@param M1 M1	  */
	public void setM1 (BigDecimal M1)
	{
		set_Value (COLUMNNAME_M1, M1);
	}

	/** Get M1.
		@return M1	  */
	public BigDecimal getM1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M10.
		@param M10 M10	  */
	public void setM10 (BigDecimal M10)
	{
		set_Value (COLUMNNAME_M10, M10);
	}

	/** Get M10.
		@return M10	  */
	public BigDecimal getM10 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M10);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M11.
		@param M11 M11	  */
	public void setM11 (BigDecimal M11)
	{
		set_Value (COLUMNNAME_M11, M11);
	}

	/** Get M11.
		@return M11	  */
	public BigDecimal getM11 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M11);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M12.
		@param M12 M12	  */
	public void setM12 (BigDecimal M12)
	{
		set_Value (COLUMNNAME_M12, M12);
	}

	/** Get M12.
		@return M12	  */
	public BigDecimal getM12 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M12);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M2.
		@param M2 M2	  */
	public void setM2 (BigDecimal M2)
	{
		set_Value (COLUMNNAME_M2, M2);
	}

	/** Get M2.
		@return M2	  */
	public BigDecimal getM2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M3.
		@param M3 M3	  */
	public void setM3 (BigDecimal M3)
	{
		set_Value (COLUMNNAME_M3, M3);
	}

	/** Get M3.
		@return M3	  */
	public BigDecimal getM3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M4.
		@param M4 M4	  */
	public void setM4 (BigDecimal M4)
	{
		set_Value (COLUMNNAME_M4, M4);
	}

	/** Get M4.
		@return M4	  */
	public BigDecimal getM4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M5.
		@param M5 M5	  */
	public void setM5 (BigDecimal M5)
	{
		set_Value (COLUMNNAME_M5, M5);
	}

	/** Get M5.
		@return M5	  */
	public BigDecimal getM5 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M5);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M6.
		@param M6 M6	  */
	public void setM6 (BigDecimal M6)
	{
		set_Value (COLUMNNAME_M6, M6);
	}

	/** Get M6.
		@return M6	  */
	public BigDecimal getM6 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M6);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M7.
		@param M7 M7	  */
	public void setM7 (BigDecimal M7)
	{
		set_Value (COLUMNNAME_M7, M7);
	}

	/** Get M7.
		@return M7	  */
	public BigDecimal getM7 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M7);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M8.
		@param M8 M8	  */
	public void setM8 (BigDecimal M8)
	{
		set_Value (COLUMNNAME_M8, M8);
	}

	/** Get M8.
		@return M8	  */
	public BigDecimal getM8 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M8);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set M9.
		@param M9 M9	  */
	public void setM9 (BigDecimal M9)
	{
		set_Value (COLUMNNAME_M9, M9);
	}

	/** Get M9.
		@return M9	  */
	public BigDecimal getM9 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_M9);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** A_JHT = AJHT */
	public static final String PAYROLLCOMPONENT_A_JHT = "AJHT";
	/** P_JHT = PJHT */
	public static final String PAYROLLCOMPONENT_P_JHT = "PJHT";
	/** A_JKK = AJKK */
	public static final String PAYROLLCOMPONENT_A_JKK = "AJKK";
	/** P_JKK = PJKK */
	public static final String PAYROLLCOMPONENT_P_JKK = "PJKK";
	/** A_JK = AJK */
	public static final String PAYROLLCOMPONENT_A_JK = "AJK";
	/** P_JK = PJK */
	public static final String PAYROLLCOMPONENT_P_JK = "PJK";
	/** A_JP = AJP */
	public static final String PAYROLLCOMPONENT_A_JP = "AJP";
	/** P_JP = PJP */
	public static final String PAYROLLCOMPONENT_P_JP = "PJP";
	/** A_JPK = AJPK */
	public static final String PAYROLLCOMPONENT_A_JPK = "AJPK";
	/** P_JPK = PJPK */
	public static final String PAYROLLCOMPONENT_P_JPK = "PJPK";
	/** Gaji Pokok = GP */
	public static final String PAYROLLCOMPONENT_GajiPokok = "GP";
	/** Deduction = POT */
	public static final String PAYROLLCOMPONENT_Deduction = "POT";
	/** Allowance = TJ */
	public static final String PAYROLLCOMPONENT_Allowance = "TJ";
	/** PPh 21 = P21 */
	public static final String PAYROLLCOMPONENT_PPh21 = "P21";
	/** Set PayrollComponent.
		@param PayrollComponent PayrollComponent	  */
	public void setPayrollComponent (String PayrollComponent)
	{

		set_Value (COLUMNNAME_PayrollComponent, PayrollComponent);
	}

	/** Get PayrollComponent.
		@return PayrollComponent	  */
	public String getPayrollComponent () 
	{
		return (String)get_Value(COLUMNNAME_PayrollComponent);
	}

	/** Set Sequence.
		@param Sequence Sequence	  */
	public void setSequence (BigDecimal Sequence)
	{
		set_Value (COLUMNNAME_Sequence, Sequence);
	}

	/** Get Sequence.
		@return Sequence	  */
	public BigDecimal getSequence () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Sequence);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T_PayrollReport.
		@param T_PayrollReport_ID T_PayrollReport	  */
	public void setT_PayrollReport_ID (int T_PayrollReport_ID)
	{
		if (T_PayrollReport_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_T_PayrollReport_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_T_PayrollReport_ID, Integer.valueOf(T_PayrollReport_ID));
	}

	/** Get T_PayrollReport.
		@return T_PayrollReport	  */
	public int getT_PayrollReport_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_T_PayrollReport_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set T_PayrollReport_UU.
		@param T_PayrollReport_UU T_PayrollReport_UU	  */
	public void setT_PayrollReport_UU (String T_PayrollReport_UU)
	{
		set_ValueNoCheck (COLUMNNAME_T_PayrollReport_UU, T_PayrollReport_UU);
	}

	/** Get T_PayrollReport_UU.
		@return T_PayrollReport_UU	  */
	public String getT_PayrollReport_UU () 
	{
		return (String)get_Value(COLUMNNAME_T_PayrollReport_UU);
	}

	/** Set Total Amount.
		@param TotalAmt 
		Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt)
	{
		set_ValueNoCheck (COLUMNNAME_TotalAmt, TotalAmt);
	}

	/** Get Total Amount.
		@return Total Amount
	  */
	public BigDecimal getTotalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}