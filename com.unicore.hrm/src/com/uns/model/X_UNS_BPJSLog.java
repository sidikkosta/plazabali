/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_BPJSLog
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_BPJSLog extends PO implements I_UNS_BPJSLog, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190318L;

    /** Standard Constructor */
    public X_UNS_BPJSLog (Properties ctx, int UNS_BPJSLog_ID, String trxName)
    {
      super (ctx, UNS_BPJSLog_ID, trxName);
      /** if (UNS_BPJSLog_ID == 0)
        {
			setJustInit (false);
// N
			setMutasiType (null);
			setUNS_Employee_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_BPJSLog (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_BPJSLog[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Column getAD_Column() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Column)MTable.get(getCtx(), org.compiere.model.I_AD_Column.Table_Name)
			.getPO(getAD_Column_ID(), get_TrxName());	}

	/** Set Column.
		@param AD_Column_ID 
		Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID)
	{
		if (AD_Column_ID < 1) 
			set_Value (COLUMNNAME_AD_Column_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Column_ID, Integer.valueOf(AD_Column_ID));
	}

	/** Get Column.
		@return Column in the table
	  */
	public int getAD_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set DB Column Name.
		@param ColumnName 
		Name of the column in the database
	  */
	public void setColumnName (String ColumnName)
	{
		set_Value (COLUMNNAME_ColumnName, ColumnName);
	}

	/** Get DB Column Name.
		@return Name of the column in the database
	  */
	public String getColumnName () 
	{
		return (String)get_Value(COLUMNNAME_ColumnName);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Just Init.
		@param JustInit Just Init	  */
	public void setJustInit (boolean JustInit)
	{
		set_Value (COLUMNNAME_JustInit, Boolean.valueOf(JustInit));
	}

	/** Get Just Init.
		@return Just Init	  */
	public boolean isJustInit () 
	{
		Object oo = get_Value(COLUMNNAME_JustInit);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Identitas = 1 */
	public static final String MUTASITYPE_Identitas = "1";
	/** Non Aktif Peserta Akhir Bulan = 10 */
	public static final String MUTASITYPE_NonAktifPesertaAkhirBulan = "10";
	/** Domisili = 2 */
	public static final String MUTASITYPE_Domisili = "2";
	/** Faskes = 3 */
	public static final String MUTASITYPE_Faskes = "3";
	/** Pekerjaan = 4 */
	public static final String MUTASITYPE_Pekerjaan = "4";
	/** Asuransi = 5 */
	public static final String MUTASITYPE_Asuransi = "5";
	/** Passport = 6 */
	public static final String MUTASITYPE_Passport = "6";
	/** Non Aktif Peserta Langsung = 7 */
	public static final String MUTASITYPE_NonAktifPesertaLangsung = "7";
	/** Pindah mutasi dari PPU ke PBPU = 8 */
	public static final String MUTASITYPE_PindahMutasiDariPPUKePBPU = "8";
	/** Pindah mutasi dari PBPU ke PPU = 9 */
	public static final String MUTASITYPE_PindahMutasiDariPBPUKePPU = "9";
	/** Penambahan Anggota Keluarga = 99 */
	public static final String MUTASITYPE_PenambahanAnggotaKeluarga = "99";
	/** Set Mutasi Type.
		@param MutasiType Mutasi Type	  */
	public void setMutasiType (String MutasiType)
	{

		set_Value (COLUMNNAME_MutasiType, MutasiType);
	}

	/** Get Mutasi Type.
		@return Mutasi Type	  */
	public String getMutasiType () 
	{
		return (String)get_Value(COLUMNNAME_MutasiType);
	}

	/** Set New Value.
		@param NewValue 
		New field value
	  */
	public void setNewValue (String NewValue)
	{
		set_Value (COLUMNNAME_NewValue, NewValue);
	}

	/** Get New Value.
		@return New field value
	  */
	public String getNewValue () 
	{
		return (String)get_Value(COLUMNNAME_NewValue);
	}

	/** Set Old Value.
		@param OldValue 
		The old file data
	  */
	public void setOldValue (String OldValue)
	{
		set_Value (COLUMNNAME_OldValue, OldValue);
	}

	/** Get Old Value.
		@return The old file data
	  */
	public String getOldValue () 
	{
		return (String)get_Value(COLUMNNAME_OldValue);
	}

	/** Set BPJS Log.
		@param UNS_BPJSLog_ID BPJS Log	  */
	public void setUNS_BPJSLog_ID (int UNS_BPJSLog_ID)
	{
		if (UNS_BPJSLog_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BPJSLog_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BPJSLog_ID, Integer.valueOf(UNS_BPJSLog_ID));
	}

	/** Get BPJS Log.
		@return BPJS Log	  */
	public int getUNS_BPJSLog_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BPJSLog_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_BPJSLog_UU.
		@param UNS_BPJSLog_UU UNS_BPJSLog_UU	  */
	public void setUNS_BPJSLog_UU (String UNS_BPJSLog_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_BPJSLog_UU, UNS_BPJSLog_UU);
	}

	/** Get UNS_BPJSLog_UU.
		@return UNS_BPJSLog_UU	  */
	public String getUNS_BPJSLog_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_BPJSLog_UU);
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}