/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_DeductionAdjReq_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_DeductionAdjReq_Line 
{

    /** TableName=UNS_DeductionAdjReq_Line */
    public static final String Table_Name = "UNS_DeductionAdjReq_Line";

    /** AD_Table_ID=1000347 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AdjustmentAmt */
    public static final String COLUMNNAME_AdjustmentAmt = "AdjustmentAmt";

	/** Set Adjustment Amount	  */
	public void setAdjustmentAmt (BigDecimal AdjustmentAmt);

	/** Get Adjustment Amount	  */
	public BigDecimal getAdjustmentAmt();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CostBenefitType */
    public static final String COLUMNNAME_CostBenefitType = "CostBenefitType";

	/** Set Cost / Benefit Type	  */
	public void setCostBenefitType (String CostBenefitType);

	/** Get Cost / Benefit Type	  */
	public String getCostBenefitType();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name RemainingAmt */
    public static final String COLUMNNAME_RemainingAmt = "RemainingAmt";

	/** Set Remaining Amt.
	  * Remaining Amount
	  */
	public void setRemainingAmt (BigDecimal RemainingAmt);

	/** Get Remaining Amt.
	  * Remaining Amount
	  */
	public BigDecimal getRemainingAmt();

    /** Column name UNS_DeductionAdjReq_ID */
    public static final String COLUMNNAME_UNS_DeductionAdjReq_ID = "UNS_DeductionAdjReq_ID";

	/** Set Deduction Adjustment Request	  */
	public void setUNS_DeductionAdjReq_ID (int UNS_DeductionAdjReq_ID);

	/** Get Deduction Adjustment Request	  */
	public int getUNS_DeductionAdjReq_ID();

	public com.uns.model.I_UNS_DeductionAdjReq getUNS_DeductionAdjReq() throws RuntimeException;

    /** Column name UNS_DeductionAdjReq_Line_ID */
    public static final String COLUMNNAME_UNS_DeductionAdjReq_Line_ID = "UNS_DeductionAdjReq_Line_ID";

	/** Set Deduction Adjustment Request Line	  */
	public void setUNS_DeductionAdjReq_Line_ID (int UNS_DeductionAdjReq_Line_ID);

	/** Get Deduction Adjustment Request Line	  */
	public int getUNS_DeductionAdjReq_Line_ID();

    /** Column name UNS_DeductionAdjReq_Line_UU */
    public static final String COLUMNNAME_UNS_DeductionAdjReq_Line_UU = "UNS_DeductionAdjReq_Line_UU";

	/** Set UNS_DeductionAdjReq_Line_UU	  */
	public void setUNS_DeductionAdjReq_Line_UU (String UNS_DeductionAdjReq_Line_UU);

	/** Get UNS_DeductionAdjReq_Line_UU	  */
	public String getUNS_DeductionAdjReq_Line_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
