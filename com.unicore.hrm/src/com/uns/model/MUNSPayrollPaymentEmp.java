/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;

import com.unicore.ui.ISortTabRecord;

/**
 * @author menjangan
 *
 */
public class MUNSPayrollPaymentEmp extends X_UNS_PayrollPayment_Emp 
implements ISortTabRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5806007179665400654L;
	private MUNSPayrollPaymentBA m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_PayrollPayment_Emp_ID
	 * @param trxName
	 */
	public MUNSPayrollPaymentEmp(Properties ctx, int UNS_PayrollPayment_Emp_ID,
			String trxName) {
		super(ctx, UNS_PayrollPayment_Emp_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollPaymentEmp(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSPayrollPaymentEmp (MUNSPayrollPaymentBA parent) 
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_PayrollPayment_BA_ID(parent.get_ID());
		m_parent = parent;
	}
	
	public MUNSPayrollPaymentBA getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPayrollPaymentBA(getCtx(), getUNS_PayrollPayment_BA_ID(), get_TrxName());
		return m_parent;
	}
	
	private boolean updateHeader ()
	{
		MUNSPayrollPaymentBA parent = getParent();
		MColumn column = MColumn.get(getCtx(), "UNS_PayrollPayment_Emp", "Amount");
		BigDecimal tAmt = Env.ZERO;
		if (column.isEncrypted())
		{
			String sql = "SELECT Amount FROM UNS_PayrollPayment_Emp WHERE UNS_PayrollPayment_BA_ID = ?";
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, get_TrxName());
				st.setInt(1, getUNS_PayrollPayment_BA_ID());
				rs = st.executeQuery();
				while (rs.next())
				{
					BigDecimal amt = rs.getBigDecimal(1);
					amt = (BigDecimal) SecureEngine.decrypt(amt, getAD_Client_ID());
					if (amt == null)
						amt = Env.ZERO;
					tAmt = tAmt.add(amt);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		else
		{
			String sql = "SELECT COALESCE (SUM (Amount), 0) FROM UNS_PayrollPayment_Emp WHERE UNS_PayrollPayment_BA_ID = ?";
			tAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollPayment_BA_ID());	
		}
		
		parent.setTotalAmt(tAmt);
		return parent.save();
	}
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_Amount))
			if (!updateHeader())
				return false;
		return super.afterSave(newRecord, success);
	}
	
	@Override
	protected boolean afterDelete (boolean success)
	{
		if (!updateHeader())
		{
			log.log(Level.SEVERE, "Error when try to update header");
			return false;
		}
		return super.afterDelete(success);
	}

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) {
		String sql = "SELECT COALESCE (TakeHomePay, 0) FROM UNS_Payroll_Employee WHERE UNS_Payroll_Employee_ID = ?";
		BigDecimal thp = DB.getSQLValueBD(get_TrxName(), sql, getUNS_Payroll_Employee_ID());
		MColumn column = MColumn.get(getCtx(), "UNS_Payroll_Employee", "TakeHomePay");
		if (column.isEncrypted())
			thp = (BigDecimal) SecureEngine.decrypt(thp, getAD_Client_ID());
		
		if (thp == null)
			thp = Env.ZERO;
		setAmount(thp);
		return null;
	}
	
	private MUNSEmployee m_employee = null;
	
	public MUNSEmployee getEmployee ()
	{
		if (m_employee == null)
		{
			String sql = "SELECT UNS_Employee_ID FROM UNS_Payroll_Employee WHERE UNS_Payroll_Employee_ID = ?";
			int empID = DB.getSQLValue(get_TrxName(), sql, getUNS_Payroll_Employee_ID());
			m_employee = new MUNSEmployee(getCtx(), empID, get_TrxName());
		}
		
		return m_employee;
	}

	@Override
	public String beforeRemoveSelection() {
		// TODO Auto-generated method stub
		return null;
	}
}
