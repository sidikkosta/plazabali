/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPayrollCorrectionLine extends X_UNS_PayrollCorrectionLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8289009810080588321L;
	private MUNSPayrollCorrection m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_PayrollCorrectionLine_ID
	 * @param trxName
	 */
	public MUNSPayrollCorrectionLine(Properties ctx,
			int UNS_PayrollCorrectionLine_ID, String trxName) {
		super(ctx, UNS_PayrollCorrectionLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollCorrectionLine(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{		
		if(getCorrectionType().equals(CORRECTIONTYPE_PresenceCorrection))
		{
			String sql = "SELECT COUNT(*) FROM UNS_PayrollCorrectionLine WHERE"
					+ " UNS_PayrollCorrectionLine_ID <> ? AND CorrectionType = ?"
					+ " AND PresenceDate = ?";
			int count = DB.getSQLValue(get_TrxName(), sql, new Object[]{
				get_ID(), CORRECTIONTYPE_PresenceCorrection, getPresenceDate()});
			if(count > 0)
			{
				log.saveError("Error", "Duplicate presence correction on same presence date");
				return false;
			}
		}
		
		if(getCorrectionType().equals(CORRECTIONTYPE_PresenceCorrection))
			setUNS_Payroll_Component_Conf_ID(-1);
		else if(getCorrectionType().equals(CORRECTIONTYPE_AllowanceCorrection) 
				|| getCorrectionType().equals(CORRECTIONTYPE_DeductionCorrection))
			setPresenceDate(null);
		else
		{
			setUNS_Payroll_Component_Conf_ID(-1);
			setPresenceDate(null);
		}
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		return upHeader();
	}
	
	protected boolean beforeDelete()
	{
		if (!cancelPayrollReplacement())
			return false;
		return super.beforeDelete();
	}
	
	@Override
	protected boolean afterDelete (boolean success)
	{
		return upHeader();
	}
	
	private boolean upHeader()
	{
		MUNSPayrollCorrection pc = getParent();
		MColumn column = MColumn.get(getCtx(), Table_Name, COLUMNNAME_Amount);

		BigDecimal pph21AmtPeriodic = Env.ZERO;
		BigDecimal pph21AmtNonPeriodic = Env.ZERO;;
		BigDecimal amtPeriodic = Env.ZERO;;
		BigDecimal amtNonPeriodic = Env.ZERO;;
		if (column.isEncrypted())
		{
			String sql = "SELECT Amount,IsPPhComp,isPeriodic FROM UNS_PayrollCorrectionLine WHERE UNS_PayrollCorrection_ID = ?";
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, get_TrxName());
				st.setInt(1, getUNS_PayrollCorrection_ID());
				rs = st.executeQuery();
				while (rs.next())
				{
					BigDecimal amt = rs.getBigDecimal(1);
					amt = (BigDecimal) SecureEngine.decrypt(amt, getAD_Client_ID());
					if (amt == null)
						amt = Env.ZERO;
					boolean isPPhComp = "Y".equals(rs.getString(2));
					boolean isPeriodic = "Y".equals(rs.getString(3));
					if (isPPhComp && isPeriodic)
						pph21AmtPeriodic = pph21AmtPeriodic.add(amt);
					else if (isPPhComp && !isPeriodic)
						pph21AmtNonPeriodic = pph21AmtNonPeriodic.add(amt);
					else if (!isPPhComp && isPeriodic)
						amtPeriodic = amtPeriodic.add(amt);
					else if (!isPPhComp && !isPeriodic)
						amtNonPeriodic = amtNonPeriodic.add(amt);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		else
		{
			String sql = "SELECT COALESCE (SUM (Amount),0) FROM UNS_PayrollCorrectionLine WHERE UNS_PayrollCorrection_ID = ?"
					+ " AND IsPPhComp = ? AND isPeriodic = ?";
			pph21AmtPeriodic = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollCorrection_ID(), "Y", "Y");
			pph21AmtNonPeriodic = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollCorrection_ID(), "Y", "N");
			amtPeriodic = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollCorrection_ID(), "N", "Y");
			amtNonPeriodic = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollCorrection_ID(), "N", "N");
		}
		
		pc.setAmountPPh21(pph21AmtPeriodic);
		pc.setAmountPPh21NonPeriodic(pph21AmtNonPeriodic);
		pc.setAmount(amtPeriodic);
		pc.setAmountNonPeriodic(amtNonPeriodic);
		
		return pc.save();
	}
	
	public MUNSPayrollCorrection getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPayrollCorrection(getCtx(), getUNS_PayrollCorrection_ID(), get_TrxName());
		
		return m_parent;
	}
	
	public boolean cancelPayrollReplacement ()
	{
		if (getUNS_Payroll_Employee_ID() == 0)
			return true;
		MUNSPayrollEmployee replacement = new MUNSPayrollEmployee(
				getCtx(), getUNS_Payroll_Employee_ID(), get_TrxName());
		if (!"VO".equals(replacement.getDocStatus()) && !"RE".equals(replacement.getDocStatus()))
		{
			try {
				if (!replacement.processIt(DocAction.ACTION_Void)) {
					log.log(Level.SEVERE, replacement.getProcessMsg());
					return false;
				}
				replacement.saveEx();
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
	
	public MUNSPayrollEmployee createPayrollReplacement (boolean recreate)
	{
		MUNSPayrollEmployee replacement = null;
		try
		{
			if (getUNS_Payroll_Employee_ID() > 0)
			{
				replacement = new MUNSPayrollEmployee(getCtx(), getUNS_Payroll_Employee_ID(), get_TrxName());
				if (!recreate)
					return replacement;
				replacement.setVoidFromCorrection(true);
				if (!"VO".equals(replacement.getDocStatus()) && !"RE".equals(replacement.getDocStatus()) 
						&& replacement.isProcessed() && !replacement.processIt(DocAction.ACTION_Void))
				{
					log.log(Level.INFO, replacement.getProcessMsg());
					return null;
				}
				
				replacement.saveEx();
				setUNS_Payroll_Employee_ID(-1);
				replacement = null;
				setAmount(Env.ZERO);
			}
			
			MUNSPayrollEmployee original = MUNSPayrollEmployee.getOriginalDoc(
					get_TrxName(), getParent().getUNS_Employee_ID(), 
					getParent().getC_Period_ID());
			String originalStat = original.getDocStatus();
			original.setVoidFromCorrection(true);
			if (!"CO".equals(originalStat) && !"VO".equals(originalStat) && !"RE".equals(originalStat))
			{
				log.log(Level.SEVERE, "Could not find original document");
				return replacement;
			}
			if (!"VO".equals(originalStat) && !"RE".equals(originalStat)
					&& !original.processIt(DocAction.ACTION_Void))
			{
				log.log(Level.SEVERE, original.getProcessMsg());
				return replacement;
			}
			
			original.saveEx();
			
			MUNSMonthlyPayrollEmployee monthly = original.getParent();
			MUNSMonthlyPayrollEmployee newParent = null;
			if (monthly.isProcessed())
			{
				newParent = MUNSMonthlyPayrollEmployee.getDraft(
						get_TrxName(), monthly.getAD_Org_ID(), monthly.getC_Period_ID(), 
						monthly.getPeriodType());
				if (newParent == null)
				{
					newParent = new MUNSMonthlyPayrollEmployee(getCtx(), 0, get_TrxName());
					PO.copyValues(monthly, newParent, monthly.getAD_Client_ID(), monthly.getAD_Org_ID());
					newParent.saveEx();
				}
			}
			else
				newParent = monthly;
			
			replacement = new MUNSPayrollEmployee(newParent, 0);
			replacement.createFrom((MUNSEmployee)getParent().getUNS_Employee());
			replacement.setC_DocType_ID(monthly.getC_DocType_ID());
			replacement.setIsGenerate(true);
			replacement.setOriginal_ID(original.get_ID());
			replacement.saveEx();
			replacement.calculatePayroll(false);
			replacement.saveEx();
			
			load(get_TrxName());
			BigDecimal correctionAmt = replacement.getTakeHomePay().subtract(original.getTakeHomePay());
			setIsPPHComp(false);
			setisPeriodic(false);
			setAmount(correctionAmt);
			setUNS_Payroll_Employee_ID(replacement.get_ID());
			saveEx();
		}
		catch (Exception Ex)
		{
			log.log(Level.SEVERE, Ex.getMessage());
			return replacement;
		}
		
		return replacement;
	}
	
	public static MUNSPayrollCorrectionLine getCorrectionOfPayroll (String trxName, int payrollID)
	{
		return Query.get(Env.getCtx(), UNSHRMModelFactory.EXTENSION_ID, Table_Name, COLUMNNAME_UNS_Payroll_Employee_ID + " =?", 
				trxName).setParameters(payrollID).first();
	}
	
	public static boolean update (String trxName, int payrollID, int originalID, BigDecimal amount)
	{
		String sql = "SELECT TakeHomePay FROM UNS_Payroll_Employee WHERE UNS_Payroll_Employee_ID = ?";
		BigDecimal originalAmt = DB.getSQLValueBD(trxName, sql, originalID);
		BigDecimal correctionAmt = amount.subtract(originalAmt);
		MUNSPayrollCorrectionLine correction = MUNSPayrollCorrectionLine.getCorrectionOfPayroll(trxName, payrollID);
		if (correction == null)
			return true;
		correction.setAmount(correctionAmt);
		return correction.save();
	}
}
