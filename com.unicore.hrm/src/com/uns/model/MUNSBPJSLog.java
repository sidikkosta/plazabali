package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.DB;

public class MUNSBPJSLog extends X_UNS_BPJSLog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1548803192532144108L;
	public static final String BasicSalary = "Basic Salary"; 

	public MUNSBPJSLog(Properties ctx, int UNS_BPJSLog_ID, String trxName) {
		super(ctx, UNS_BPJSLog_ID, trxName);
		
	}

	public MUNSBPJSLog(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	public static MUNSBPJSLog setLog(
			MUNSEmployee employee, int AD_Column_ID, String ColName, Timestamp dateChanged, String oldVal, String newVal, boolean justInit)
	{
		MUNSBPJSLog bpjsLog = null;
		
		String sql = "SELECT UNS_BPJSLog_ID FROM UNS_BPJSLog WHERE"
				+ " UNS_Employee_ID = ? AND TRUNC(CAST(Created AS Date)) = TRUNC(CAST(? AS Date)) AND JustInit = 'N'";
		if(AD_Column_ID > 0)
			sql += " AND AD_Column_ID = "+AD_Column_ID;
		else
			sql += " AND ColumnName = '"+ColName+"'";
		int bpjsLogID = DB.getSQLValue(employee.get_TrxName(), sql, employee.getUNS_Employee_ID(), dateChanged);
		if(bpjsLogID > 0)
		{
			bpjsLog = new MUNSBPJSLog(employee.getCtx(), bpjsLogID, employee.get_TrxName());
		}
		else
		{
			bpjsLog = new MUNSBPJSLog(employee.getCtx(), 0, employee.get_TrxName());
			bpjsLog.setAD_Client_ID(employee.getAD_Client_ID());
			bpjsLog.setAD_Org_ID(employee.getAD_Org_ID());
			bpjsLog.setUNS_Employee_ID(employee.get_ID());
			bpjsLog.setJustInit(justInit);
			
			if(AD_Column_ID > 0)
			{
				bpjsLog.setAD_Column_ID(AD_Column_ID);
				String sqql = "SELECT ColumnName FROM AD_Column WHERE AD_Column_ID = ?";
				String collName = DB.getSQLValueString(employee.get_TrxName(), sqql, AD_Column_ID);
				bpjsLog.setColumnName(collName);
			}
			else
			{
				bpjsLog.setColumnName(ColName);
			}
			
		}
		
		//getConfig to set mutasi type
		String mutasiType = null;
		if(ColName.equals(BasicSalary))
		{
			String sql3 = "SELECT MutasiType FROM UNS_BPJSLog_Config WHERE"
					+ " BasicSalary = 'Y' AND isActive = 'Y'";
			mutasiType = DB.getSQLValueString(employee.get_TrxName(), sql3);
		}
		else
		{
			String sql3 = "SELECT MutasiType FROM UNS_BPJSLog_Config WHERE"
					+ " AD_Column_ID = ? AND isActive = 'Y'";
			mutasiType = DB.getSQLValueString(employee.get_TrxName(), sql3, AD_Column_ID);
		}
		
		bpjsLog.setMutasiType(mutasiType);
		
		bpjsLog.set_Value(MUNSBPJSLog.COLUMNNAME_OldValue, oldVal);
		bpjsLog.set_Value(MUNSBPJSLog.COLUMNNAME_NewValue, newVal);
		
		//set description mutasi
		String sql4 = "SELECT Description FROM AD_Ref_List WHERE value = ? AND"
				+ " AD_Reference_ID = (SELECT AD_Reference_Value_ID FROM AD_Column WHERE"
					+ " AD_Table_ID = ? AND ColumnName = ?)";
		String desc = DB.getSQLValueString(
				employee.get_TrxName(), sql4, mutasiType, MUNSBPJSLog.Table_ID, MUNSBPJSLog.COLUMNNAME_MutasiType);
		
		bpjsLog.setDescription(desc);
		
		bpjsLog.saveEx();
		
		return bpjsLog;
	}
	
}
