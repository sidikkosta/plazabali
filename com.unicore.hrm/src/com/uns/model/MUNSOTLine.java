/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSOTLine extends X_UNS_OTLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5680316630035875408L;
	
	private MUNSOTRequest m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_OTLine_ID
	 * @param trxName
	 */
	public MUNSOTLine(Properties ctx, int UNS_OTLine_ID, String trxName) {
		super(ctx, UNS_OTLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSOTLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSOTRequest getParent() {
		
		if(m_parent != null)
			return m_parent;
		
		m_parent = new MUNSOTRequest(
				getCtx(), getUNS_OTRequest_ID(), get_TrxName());
		
		return m_parent;
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		getParent();
		//check duplicate
//		String sqql = "SELECT o.DocumentNo FROM UNS_OTLine ol"
//				+ " INNER JOIN UNS_OTRequest o ON o.UNS_OTRequest_ID = ol.UNS_OTRequest_ID"
//				+ " WHERE ((? BETWEEN o.StartTime AND o.EndTime) OR"
//				+ " (? BETWEEN o.StartTime AND o.EndTime) OR (o.StartTime BETWEEN ? AND ? )"
//				+ " OR (o.EndTime BETWEEN ? AND ?)) AND o.DocStatus NOT IN ('VO','RE')"
//				+ " AND UNS_OTLine_ID <> ?";
//		if(getUNS_Resource_ID() > 0)
//		{
//			sqql += " AND ol.UNS_Resource_ID = "+getUNS_Resource_ID();
//		}
//		else if(getUNS_Employee_ID() > 0)
//		{
//			sqql += " AND ol.UNS_Employee_ID = "+getUNS_Employee_ID();
//		}
//		String docNo = DB.getSQLValueString(
//				get_TrxName(), sqql, m_parent.getStartTime(), m_parent.getEndTime(),
//				m_parent.getStartTime(), m_parent.getEndTime(), m_parent.getStartTime(),
//				m_parent.getEndTime(), getUNS_OTLine_ID());
		
		String sql = "SELECT COUNT(*) FROM UNS_OTLine WHERE UNS_OTGroupRequest_ID = ?"
				+ " AND UNS_OTLine_ID <> ? AND UNS_Employee_ID = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, getUNS_OTGroupRequest_ID(), get_ID(), getUNS_Employee_ID());
		if(count > 1)
		{
			log.saveError("Error", "Duplicate employee in on document.");
			return false;
		}
		
		if(newRecord)
		{
			sql = "SELECT COALESCE(MAX(Line),0) FROM UNS_OTLine WHERE UNS_OTRequest_ID = ?"
					+ " AND UNS_OTLine_ID <> ?";
			int line = DB.getSQLValue(get_TrxName(), sql, getUNS_OTRequest_ID(), get_ID());
			setLine(line + 10);
		}
		if(getUNS_Resource_ID() > 0)
			setIsSummary(true);
		else
			setIsSummary(false);
		return true;
	}
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_OTRequest WHERE UNS_OTLine_ID = ?";
		return DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
	}
	
	public MUNSOTRequest[] getActivities()
	{
		List<MUNSOTRequest> list = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSOTRequest.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSOTRequest[list.size()]);
	}
}