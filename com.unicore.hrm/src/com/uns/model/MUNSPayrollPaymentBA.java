/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author menjangan
 *
 */
public class MUNSPayrollPaymentBA extends X_UNS_PayrollPayment_BA {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5848993158233970343L;
	private MUNSPayrollPayment m_parent = null;
	private MUNSPayrollPaymentEmp[] m_lines = null;
	
	/**
	 * @param ctx
	 * @param UNS_PayrollPayment_BA_ID
	 * @param trxName
	 */
	public MUNSPayrollPaymentBA(Properties ctx, int UNS_PayrollPayment_BA_ID,
			String trxName) {
		super(ctx, UNS_PayrollPayment_BA_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollPaymentBA(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSPayrollPayment getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPayrollPayment(getCtx(), getUNS_PayrollPayment_ID(), get_TrxName());
		return m_parent;
	}
	
	public MUNSPayrollPaymentBA (MUNSPayrollPayment parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		m_parent = parent;
	}
	
	private boolean updateHeader ()
	{
		MColumn column = MColumn.get(getCtx(), "UNS_PayrollPayment_BA", "TotalAmt");
		BigDecimal total = Env.ZERO;
		if (column.isEncrypted())
		{
			String sql = "SELECT TotalAmt FROM UNS_PayrollPayment_BA WHERE UNS_PayrollPayment_ID = ?";
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, get_TrxName());
				st.setInt(1, getUNS_PayrollPayment_ID());
				rs = st.executeQuery();
				while (rs.next())
				{
					BigDecimal amt = rs.getBigDecimal(1);
					amt = (BigDecimal) SecureEngine.decrypt(amt, getAD_Client_ID());
					if (amt == null)
						amt = Env.ZERO;
					total = total.add(amt);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		else
		{
			String sql = "SELECT COALESCE (SUM (TotalAmt), 0) FROM UNS_PayrollPayment_BA WHERE UNS_PayrollPayment_ID = ?";
			total = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollPayment_ID());
		}
		
		MUNSPayrollPayment parent = getParent();
		parent.setTotalAmt(total);
		return parent.save();
	}
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_TotalAmt))
			if (!updateHeader())
				return false;
		
		return super.afterSave(newRecord, success);
	}
	
	private boolean deleteLines ()
	{
		String sql = "DELETE FROM UNS_PayrollPayment_Emp WHERE UNS_PayrollPayment_BA_ID = ?";
		int count = DB.executeUpdate(sql, getUNS_PayrollPayment_BA_ID(), get_TrxName());
		if (count == -1)
			return false;
		return true;
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		if (!deleteLines())
		{
			log.log(Level.SEVERE, "Failed when try to delete lines");
			return false;
		}
		return super.beforeDelete();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(!isBenefit())
			setCostBenefitType(null);
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterDelete (boolean success)
	{
		if (!updateHeader())
		{
			log.log(Level.SEVERE, "Error when try to update header");
			return false;
		}
		return super.afterDelete(success);
	}
	
	public MUNSPayrollPaymentEmp[] getLines (boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		List<MUNSPayrollPaymentEmp> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSPayrollPaymentEmp.Table_Name, 
				Table_Name + "_ID = ?", get_TrxName()).setParameters(get_ID()).list();
		m_lines = new MUNSPayrollPaymentEmp[list.size()];
		list.toArray(m_lines);
		return m_lines;
	}
	
	public MUNSPayrollPaymentEmp[] getLines ()
	{
		return getLines(false);
	}
}
