/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSOTGroupRequest extends X_UNS_OTGroupRequest implements DocAction, DocOptions{

	String m_processMsg = null;
	private boolean m_justPrepared = false;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6835342228084715220L;

	/**
	 * @param ctx
	 * @param UNS_OTGroupRequest_ID
	 * @param trxName
	 */
	public MUNSOTGroupRequest(Properties ctx, int UNS_OTGroupRequest_ID,
			String trxName) {
		super(ctx, UNS_OTGroupRequest_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSOTGroupRequest(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSOTRequest[] getValidof (
			String trxName, Timestamp date, int UNS_Employee_ID)
	{
		String wc = " EXISTS (SELECT 1 FROM UNS_OTLine l WHERE"
				+ " l.UNS_OTLine_ID = UNS_OTRequest.UNS_OTLine_ID AND l.UNS_Employee_ID = ?"
				+ " AND EXISTS (SELECT 1 FROM UNS_OTGroupRequest r"
				+ " WHERE r.DocStatus IN ('CO', 'CL') AND r.UNS_OTGroupRequest_ID = l.UNS_OTGroupRequest_ID))"
				+ " AND UNS_OTRequest.DateDoOT = TRUNC (CAST (? AS DATE))";
			
//		String sql = "SELECT wl.UNS_Resource_ID FROM "
//				+ " UNS_Resource_WorkerLine wl WHERE wl.Labor_ID = ? AND "
//				+ " wl.IsActive = 'Y' AND wl.ValidFrom <= ? ORDER BY wl.ValidFrom DESC";
//		int resourceID = DB.getSQLValue(trxName, sql, UNS_Employee_ID, date);
		List<Object> params = new ArrayList<Object>();
		params.add(UNS_Employee_ID);
//		params.add(resourceID);
		params.add(date);
		return get(trxName, wc, params);
	}
	
	public static MUNSOTRequest[] get (String trxName, String whereClause, 
			List<Object> params)
	{
		Query q = (Query) Query.get(Env.getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
				MUNSOTRequest.Table_Name, whereClause, trxName)
				.setOrderBy("StartTime ASC");
		if (null != params)
		{
			q.setParameters(params);
		}
		
		List<MUNSOTRequest> list = q.list();
		MUNSOTRequest[] req = list.toArray(new MUNSOTRequest[list.size()]);
		
		return req;
	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (docStatus.equals(DocAction.STATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_Void;
		}
		
		return index;
	}
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String action) throws Exception 
	{
		doLog(Level.INFO, "Processing Document.");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() 
	{
		doLog(Level.INFO, "Unlock Document");
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		doLog(Level.INFO, "Invalidate Document");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		doLog(Level.INFO, "Prepare Document");
		
		//run UNSHRMValidator
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		deleteEmployeeNotUse();
		
		m_processMsg = duplicateOT();
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		m_processMsg = presenceHasProcessed();
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Complete);
		m_justPrepared = true;
		return DOCSTATUS_InProgress;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		doLog(Level.INFO, "Approve Document");
		setProcessed(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() 
	{
		doLog(Level.INFO, "Reject Document");
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		doLog(Level.INFO, "Complete Document");
		
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
			return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
//		MUNSOTRequest[] activitys = getActivity(false);
//		if(activitys.length == 0)
//		{
//			m_processMsg = "No Activities";
//			return DOCSTATUS_Invalid;
//		}
		
		MUNSOTLine[] lines = getLines(false);
		if(lines.length == 0)
		{
			m_processMsg = "Resource / Employee is mandatory.";
			return DOCSTATUS_Invalid;
		}
		
		MUNSOTGroupConfirm confirm = MUNSOTGroupConfirm.getCreate(this);
		if (null == confirm)
		{
			m_processMsg = "Failed when trying to create confirmation.";
			return DOCSTATUS_Invalid;
		}
		
		if (!confirm.isComplete() && !isForce())
		{
			m_processMsg = "Please complete Over Time Confirmation " 
					+ confirm.getDocumentNo() + " before do next process.";
			return DOCSTATUS_InProgress;
		}
		
		m_processMsg = upAdjustRuleDaily();
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		DB.executeUpdate("UPDATE UNS_OTRequest SET Processed = 'Y', DocStatus = 'CO', DocAction = 'CL'"
				+ " WHERE UNS_OTGroupRequest_ID = ?", get_ID(), get_TrxName());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		doLog(Level.INFO, "Void Document");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
		{
			return false;
		}
		
		MUNSOTGroupConfirm confirm = MUNSOTGroupConfirm.getCreate(this);
		if (confirm != null 
				&& !confirm.getDocStatus().equals(DOCSTATUS_Drafted)
				&& !confirm.getDocStatus().equals(DOCSTATUS_Invalid)
				&& !isForce())
		{
			m_processMsg = "Please void/reverse Confirmation first.";
		}
		else if (confirm != null) {
			confirm.setDocStatus(DOCSTATUS_Voided);
			confirm.setDocAction(DOCACTION_None);
			confirm.setProcessed(true);
			confirm.saveEx();
		}
		
		m_processMsg = upAdjustRuleDaily();
		if (m_processMsg != null)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
		{
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt() 
	{
		doLog(Level.INFO, "Close Document");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
		{
			return false;
		}
		
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
		{
			return false;
		}
		
		setDocAction(DOCACTION_None);
		setDocStatus(DOCSTATUS_Closed);
		setProcessed(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		doLog(Level.INFO, "Reverse Correct-It");
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() 
	{
		doLog(Level.INFO, "Reverse AccrualIt");
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt() 
	{
		m_processMsg = "Disallowed action Reactivate-It";
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder("Over Time Request No ")
		.append(getDocumentNo());
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() 
	{
		return getSummary();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() 
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() 
	{
		return null;
	}
	
	private void doLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
		{
			return;
		}
		
		log.log(level, msg);
	}
	
	private String duplicateOT()
	{
		String msg = null;
//		MUNSOTRequest[] activitys = getActivity(false);
//		
//		for(int x=0;x<activitys.length;x++)
//		{
		MUNSOTLine[] lines = getLines(true);
		String sql = null;
		for(int i=0;i<lines.length;i++)
		{
			sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DISTINCT(gr.DocumentNo)), ';') FROM UNS_OTGroupRequest gr"
					+ " INNER JOIN UNS_OTLine l ON l.UNS_OTGroupRequest_ID = gr.UNS_OTGroupRequest_ID"
					+ " INNER JOIN UNS_OTRequest r ON r.UNS_OTLine_ID = l.UNS_OTLine_ID"
					+ " WHERE ((? BETWEEN r.StartTime AND r.EndTime) OR (? BETWEEN r.StartTime AND r.EndTime))"
					+ " AND l.UNS_Employee_ID = ? AND gr.DocStatus IN ('CO', 'CL')"
					+ " AND gr.UNS_OTGroupRequest_ID <> ?";
			MUNSOTRequest[] activities = lines[i].getActivities();
			for(int j=0;j<activities.length;j++)
			{
				msg = DB.getSQLValueString(get_TrxName(), sql, new Object[]{
					activities[j].getStartTime(), activities[j].getEndTime(),
					lines[i].getUNS_Employee_ID(), get_ID()
					});
				if(!Util.isEmpty(msg, true))
				{
					msg = "Duplicate overtime. #Employee :: " + lines[i].getUNS_Employee_ID() + " #Time :: " +
							activities[j].getStartTime().toString() + " - " +
							activities[j].getEndTime().toString() +" #Document " +
							msg;
					return msg;
				}
			}
		}
//		}
		
		return msg;
	}
	
	public MUNSEmployee[] getEmployees(boolean requery)
	{
		if (m_employee != null && !requery)
		{
			set_TrxName(m_employee, get_TrxName());
			return m_employee;
		}
		
		List<MUNSEmployee> employees = new ArrayList<>();
		String sql = "SELECT DISTINCT(emp.UNS_Employee_ID) FROM UNS_Employee emp"
				+ " WHERE EXISTS (SELECT 1 FROM UNS_OTLine l WHERE"
				+ " l.UNS_OTGroupRequest_ID = ? AND l.UNS_Employee_ID = emp.UNS_Employee_ID)";
//				+ " OR EXISTS (SELECT 1 FROM UNS_Resource_WorkerLine wl"
//				+ " WHERE wl.Labor_ID = emp.UNS_Employee_ID AND ((wl.ValidFrom = ? AND wl.ValidTo = ?)"
//				+ " OR (? BETWEEN wl.ValidFrom AND wl.ValidTo))"
//				+ " AND wl.UNS_Resource_ID = l.UNS_Resource_ID)))";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				employees.add(new MUNSEmployee(getCtx(), rs.getInt(1), get_TrxName()));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		m_employee = employees.toArray(new MUNSEmployee[employees.size()]);
		
		return m_employee;
	}
	
	private MUNSEmployee[] m_employee;
	private MUNSOTRequest[] m_activity;
	private MUNSOTLine[] m_lines;
	
	public MUNSOTRequest[] getActivity(boolean requery)
	{
		if (m_activity != null && !requery)
		{
			set_TrxName(m_activity, get_TrxName());
			return m_activity;
		}
		
		List<MUNSOTRequest> list = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSOTRequest.Table_Name, Table_Name + "_ID=?", get_TrxName()).setParameters(get_ID()).list();
		
		m_activity = list.toArray(new MUNSOTRequest[list.size()]);
		return m_activity;
	}
	
	private String presenceHasProcessed()
	{
		String msg = null;
//		MUNSOTRequest[] activitys = getActivity(false);
//		
//		for(int x=0;x<activitys.length;x++)
//		{
//			MUNSEmployee[] emps = getEmployees(false);
			
		MUNSOTLine[] lines = getLines(false);
		for(int i=0;i<lines.length;i++)
		{
			MUNSMonthlyPresenceSummary monthly = MUNSMonthlyPresenceSummary.get(getCtx(), lines[i].getUNS_Employee_ID(),
					getC_Period_ID(), getAD_Org_ID(), get_TrxName());
			if(monthly != null && (monthly.getDocStatus().equals("CO")
					|| monthly.getDocStatus().equals("CL")))
			{
				msg = "Monthly for employee " + lines[i].getUNS_Employee().getName() 
						+ " has completed, cannot running this action.";
				return msg;
			}
		}
//		}
		
		return msg;
	}
	
	private String upAdjustRuleDaily()
	{
		String msg = null;
//		MUNSOTRequest[] activitys = getActivity(false);
		String sql = "UPDATE UNS_DailyPresence dp SET IsNeedAdjustRule = 'Y'"
				+ " WHERE dp.PresenceDate = ? AND EXISTS (SELECT 1 FROM"
				+ " UNS_MonthlyPresenceSummary mps WHERE mps.UNS_Employee_ID = ?"
				+ " AND mps.UNS_MonthlyPresenceSummary_ID = dp.UNS_MonthlyPresenceSummary_ID)";
//		for(int x=0;x<activitys.length;x++)
//		{
		MUNSOTLine[] lines = getLines(false);
		
		for(int i=0;i<lines.length;i++)
		{
			MUNSOTRequest[] activities = lines[i].getActivities();
			for(int j=0;j<activities.length;j++)
			{
				if(DB.executeUpdate(sql, new Object[]{activities[j].getDateDoOT(), lines[i].getUNS_Employee_ID()}, false, get_TrxName()) < 0)
				{
					msg = "Failed when trying update Need Adjust Rule On Daily Presence " + lines[i].getUNS_Employee().getName()
							+ " - " + activities[j].getDateDoOT() + ". Please contract administrator for fix it.";
					return msg;
				}
			}
		}
//		}
		
		return msg;
	}
	
	public MUNSOTLine[] getLines(boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		List<MUNSOTLine> list = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSOTLine.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		m_lines = list.toArray(new MUNSOTLine[list.size()]);
		return m_lines;
	}
	
	public void setForce(boolean force)
	{
		m_force = force;
	}
	
	public boolean isForce ()
	{
		return m_force;
	}
	
	private boolean m_force = false;
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!newRecord)
			return true;
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(UNS_Employee_ID), ';') FROM UNS_Employee"
				+ " WHERE AD_Org_ID = ? AND IsActive = 'Y' AND IsTerminate = 'N'";
		String retVal = DB.getSQLValueString(get_TrxName(), sql, getAD_Org_ID());
		
		if(retVal == null)
			return true;
		
		String[] retVals = retVal.split(";");
		for(int i=0;i<retVals.length;i++)
		{
			int empID = new Integer (retVals[i]);
			MUNSOTLine line = new MUNSOTLine(getCtx(), 0, get_TrxName());
			line.setAD_Org_ID(getAD_Org_ID());
			line.setUNS_OTGroupRequest_ID(get_ID());
			line.setUNS_Employee_ID(empID);
			line.setIsSummary(false);
			line.saveEx();
		}
		return true;
	}
	
	private void deleteEmployeeNotUse()
	{
		String sql = "DELETE FROM UNS_OTLine l WHERE l.UNS_OTGroupRequest_ID = ?"
				+ " AND NOT EXISTS (SELECT 1 FROM UNS_OTRequest r WHERE r.UNS_OTLine_ID"
				+ " = l.UNS_OTLine_ID)";
		DB.executeUpdate(sql, get_ID(), get_TrxName());
	}
}