/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_YearlyPayrollSummary
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_YearlyPayrollSummary 
{

    /** TableName=UNS_YearlyPayrollSummary */
    public static final String Table_Name = "UNS_YearlyPayrollSummary";

    /** AD_Table_ID=1000358 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name A_JHT */
    public static final String COLUMNNAME_A_JHT = "A_JHT";

	/** Set A.JHT	  */
	public void setA_JHT (BigDecimal A_JHT);

	/** Get A.JHT	  */
	public BigDecimal getA_JHT();

    /** Column name A_JK */
    public static final String COLUMNNAME_A_JK = "A_JK";

	/** Set A.JK	  */
	public void setA_JK (BigDecimal A_JK);

	/** Get A.JK	  */
	public BigDecimal getA_JK();

    /** Column name A_JKK */
    public static final String COLUMNNAME_A_JKK = "A_JKK";

	/** Set A.JKK	  */
	public void setA_JKK (BigDecimal A_JKK);

	/** Get A.JKK	  */
	public BigDecimal getA_JKK();

    /** Column name A_JP */
    public static final String COLUMNNAME_A_JP = "A_JP";

	/** Set A_JP	  */
	public void setA_JP (BigDecimal A_JP);

	/** Get A_JP	  */
	public BigDecimal getA_JP();

    /** Column name A_JPK */
    public static final String COLUMNNAME_A_JPK = "A_JPK";

	/** Set A.JPK	  */
	public void setA_JPK (BigDecimal A_JPK);

	/** Get A.JPK	  */
	public BigDecimal getA_JPK();

    /** Column name A_L1 */
    public static final String COLUMNNAME_A_L1 = "A_L1";

	/** Set Aditional Lembur 1	  */
	public void setA_L1 (BigDecimal A_L1);

	/** Get Aditional Lembur 1	  */
	public BigDecimal getA_L1();

    /** Column name A_L1R */
    public static final String COLUMNNAME_A_L1R = "A_L1R";

	/** Set Aditional Lembur 1 R.
	  * Amount Aditional Lembur 1 R
	  */
	public void setA_L1R (BigDecimal A_L1R);

	/** Get Aditional Lembur 1 R.
	  * Amount Aditional Lembur 1 R
	  */
	public BigDecimal getA_L1R();

    /** Column name A_L2 */
    public static final String COLUMNNAME_A_L2 = "A_L2";

	/** Set Aditional Lembur 2	  */
	public void setA_L2 (BigDecimal A_L2);

	/** Get Aditional Lembur 2	  */
	public BigDecimal getA_L2();

    /** Column name A_L2R */
    public static final String COLUMNNAME_A_L2R = "A_L2R";

	/** Set Aditional Lembur 2 R.
	  * Amount Aditional Lembur 2 R
	  */
	public void setA_L2R (BigDecimal A_L2R);

	/** Get Aditional Lembur 2 R.
	  * Amount Aditional Lembur 2 R
	  */
	public BigDecimal getA_L2R();

    /** Column name A_L3 */
    public static final String COLUMNNAME_A_L3 = "A_L3";

	/** Set Aditional Lembur 3	  */
	public void setA_L3 (BigDecimal A_L3);

	/** Get Aditional Lembur 3	  */
	public BigDecimal getA_L3();

    /** Column name A_L3R */
    public static final String COLUMNNAME_A_L3R = "A_L3R";

	/** Set Aditional Lembur 3 R.
	  * Amount Aditional Lembur 3 R
	  */
	public void setA_L3R (BigDecimal A_L3R);

	/** Get Aditional Lembur 3 R.
	  * Amount Aditional Lembur 3 R
	  */
	public BigDecimal getA_L3R();

    /** Column name A_LemburJamBerikutnya */
    public static final String COLUMNNAME_A_LemburJamBerikutnya = "A_LemburJamBerikutnya";

	/** Set A. Lembur Jam Berikutnya	  */
	public void setA_LemburJamBerikutnya (BigDecimal A_LemburJamBerikutnya);

	/** Get A. Lembur Jam Berikutnya	  */
	public BigDecimal getA_LemburJamBerikutnya();

    /** Column name A_LemburJamPertama */
    public static final String COLUMNNAME_A_LemburJamPertama = "A_LemburJamPertama";

	/** Set Aditional Lembur Jam Pertama	  */
	public void setA_LemburJamPertama (BigDecimal A_LemburJamPertama);

	/** Get Aditional Lembur Jam Pertama	  */
	public BigDecimal getA_LemburJamPertama();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name C_Year_ID */
    public static final String COLUMNNAME_C_Year_ID = "C_Year_ID";

	/** Set Year.
	  * Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID);

	/** Get Year.
	  * Calendar Year
	  */
	public int getC_Year_ID();

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException;

    /** Column name GPokok */
    public static final String COLUMNNAME_GPokok = "GPokok";

	/** Set Gaji Pokok	  */
	public void setGPokok (BigDecimal GPokok);

	/** Get Gaji Pokok	  */
	public BigDecimal getGPokok();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name PeriodStart_ID */
    public static final String COLUMNNAME_PeriodStart_ID = "PeriodStart_ID";

	/** Set Period Start	  */
	public void setPeriodStart_ID (int PeriodStart_ID);

	/** Get Period Start	  */
	public int getPeriodStart_ID();

	public org.compiere.model.I_C_Period getPeriodStart() throws RuntimeException;

    /** Column name P_JHT */
    public static final String COLUMNNAME_P_JHT = "P_JHT";

	/** Set P.JHT	  */
	public void setP_JHT (BigDecimal P_JHT);

	/** Get P.JHT	  */
	public BigDecimal getP_JHT();

    /** Column name P_JK */
    public static final String COLUMNNAME_P_JK = "P_JK";

	/** Set P.JK	  */
	public void setP_JK (BigDecimal P_JK);

	/** Get P.JK	  */
	public BigDecimal getP_JK();

    /** Column name P_JKK */
    public static final String COLUMNNAME_P_JKK = "P_JKK";

	/** Set P.JKK	  */
	public void setP_JKK (BigDecimal P_JKK);

	/** Get P.JKK	  */
	public BigDecimal getP_JKK();

    /** Column name P_JP */
    public static final String COLUMNNAME_P_JP = "P_JP";

	/** Set P_JP	  */
	public void setP_JP (BigDecimal P_JP);

	/** Get P_JP	  */
	public BigDecimal getP_JP();

    /** Column name P_JPK */
    public static final String COLUMNNAME_P_JPK = "P_JPK";

	/** Set P.JPK	  */
	public void setP_JPK (BigDecimal P_JPK);

	/** Get P.JPK	  */
	public BigDecimal getP_JPK();

    /** Column name PPH21 */
    public static final String COLUMNNAME_PPH21 = "PPH21";

	/** Set PPH 21	  */
	public void setPPH21 (BigDecimal PPH21);

	/** Get PPH 21	  */
	public BigDecimal getPPH21();

    /** Column name StartPeriodNo */
    public static final String COLUMNNAME_StartPeriodNo = "StartPeriodNo";

	/** Set Start Period No	  */
	public void setStartPeriodNo (int StartPeriodNo);

	/** Get Start Period No	  */
	public int getStartPeriodNo();

    /** Column name TakeHomePay */
    public static final String COLUMNNAME_TakeHomePay = "TakeHomePay";

	/** Set Take Home Pay.
	  * Take Home Pay
	  */
	public void setTakeHomePay (BigDecimal TakeHomePay);

	/** Get Take Home Pay.
	  * Take Home Pay
	  */
	public BigDecimal getTakeHomePay();

    /** Column name TNonPeriodicBenefit */
    public static final String COLUMNNAME_TNonPeriodicBenefit = "TNonPeriodicBenefit";

	/** Set T Non Periodic Benefit	  */
	public void setTNonPeriodicBenefit (BigDecimal TNonPeriodicBenefit);

	/** Get T Non Periodic Benefit	  */
	public BigDecimal getTNonPeriodicBenefit();

    /** Column name TNonPeriodicBenefitPPh21Comp */
    public static final String COLUMNNAME_TNonPeriodicBenefitPPh21Comp = "TNonPeriodicBenefitPPh21Comp";

	/** Set T Non Periodic Benefit PPh21 Comp	  */
	public void setTNonPeriodicBenefitPPh21Comp (BigDecimal TNonPeriodicBenefitPPh21Comp);

	/** Get T Non Periodic Benefit PPh21 Comp	  */
	public BigDecimal getTNonPeriodicBenefitPPh21Comp();

    /** Column name TNonPeriodicDeducPPh21Comp */
    public static final String COLUMNNAME_TNonPeriodicDeducPPh21Comp = "TNonPeriodicDeducPPh21Comp";

	/** Set T Non Periodic Deduc PPh21 Comp	  */
	public void setTNonPeriodicDeducPPh21Comp (BigDecimal TNonPeriodicDeducPPh21Comp);

	/** Get T Non Periodic Deduc PPh21 Comp	  */
	public BigDecimal getTNonPeriodicDeducPPh21Comp();

    /** Column name TNonPeriodicDeduction */
    public static final String COLUMNNAME_TNonPeriodicDeduction = "TNonPeriodicDeduction";

	/** Set T Non Periodic Deduction	  */
	public void setTNonPeriodicDeduction (BigDecimal TNonPeriodicDeduction);

	/** Get T Non Periodic Deduction	  */
	public BigDecimal getTNonPeriodicDeduction();

    /** Column name TPeriodicBenefit */
    public static final String COLUMNNAME_TPeriodicBenefit = "TPeriodicBenefit";

	/** Set T Periodic Benefit	  */
	public void setTPeriodicBenefit (BigDecimal TPeriodicBenefit);

	/** Get T Periodic Benefit	  */
	public BigDecimal getTPeriodicBenefit();

    /** Column name TPeriodicBenefitPPh21Comp */
    public static final String COLUMNNAME_TPeriodicBenefitPPh21Comp = "TPeriodicBenefitPPh21Comp";

	/** Set T Periodic Benefit PPh21 Comp	  */
	public void setTPeriodicBenefitPPh21Comp (BigDecimal TPeriodicBenefitPPh21Comp);

	/** Get T Periodic Benefit PPh21 Comp	  */
	public BigDecimal getTPeriodicBenefitPPh21Comp();

    /** Column name TPeriodicDeducPPh21Comp */
    public static final String COLUMNNAME_TPeriodicDeducPPh21Comp = "TPeriodicDeducPPh21Comp";

	/** Set T Periodic Deduc PPh21 Comp	  */
	public void setTPeriodicDeducPPh21Comp (BigDecimal TPeriodicDeducPPh21Comp);

	/** Get T Periodic Deduc PPh21 Comp	  */
	public BigDecimal getTPeriodicDeducPPh21Comp();

    /** Column name TPeriodicDeduction */
    public static final String COLUMNNAME_TPeriodicDeduction = "TPeriodicDeduction";

	/** Set T Periodic Deduction	  */
	public void setTPeriodicDeduction (BigDecimal TPeriodicDeduction);

	/** Get T Periodic Deduction	  */
	public BigDecimal getTPeriodicDeduction();

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name UNS_YearlyPayrollSummary_ID */
    public static final String COLUMNNAME_UNS_YearlyPayrollSummary_ID = "UNS_YearlyPayrollSummary_ID";

	/** Set Yearly Payroll Summary	  */
	public void setUNS_YearlyPayrollSummary_ID (int UNS_YearlyPayrollSummary_ID);

	/** Get Yearly Payroll Summary	  */
	public int getUNS_YearlyPayrollSummary_ID();

    /** Column name UNS_YearlyPayrollSummary_UU */
    public static final String COLUMNNAME_UNS_YearlyPayrollSummary_UU = "UNS_YearlyPayrollSummary_UU";

	/** Set UNS_YearlyPayrollSummary_UU	  */
	public void setUNS_YearlyPayrollSummary_UU (String UNS_YearlyPayrollSummary_UU);

	/** Get UNS_YearlyPayrollSummary_UU	  */
	public String getUNS_YearlyPayrollSummary_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
