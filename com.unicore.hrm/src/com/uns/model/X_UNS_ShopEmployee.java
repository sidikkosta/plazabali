/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_ShopEmployee
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ShopEmployee extends PO implements I_UNS_ShopEmployee, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191119L;

    /** Standard Constructor */
    public X_UNS_ShopEmployee (Properties ctx, int UNS_ShopEmployee_ID, String trxName)
    {
      super (ctx, UNS_ShopEmployee_ID, trxName);
      /** if (UNS_ShopEmployee_ID == 0)
        {
			setCashierCount (Env.ZERO);
// 0
			setCashierIncentiveAmt (Env.ZERO);
// 0
			setGroupIncentiveAmt (Env.ZERO);
// 0
			setIsMasterSchedule (true);
// Y
			setNetSalesRealizationAmt (Env.ZERO);
// 0
			setPoint (Env.ZERO);
// 0
			setPositionType (null);
			setSalesIncentiveAmt (Env.ZERO);
// 0
			setUNS_Employee_ID (0);
			setUNS_ShopEmployee_ID (0);
			setUNS_ShopEmployee_UU (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_ShopEmployee (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ShopEmployee[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_Value (COLUMNNAME_AD_User_ID, null);
		else 
			set_Value (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Calculation Class.
		@param CalculationClass 
		Java Class for calculation, implementing Interface Measure
	  */
	public void setCalculationClass (String CalculationClass)
	{
		set_Value (COLUMNNAME_CalculationClass, CalculationClass);
	}

	/** Get Calculation Class.
		@return Java Class for calculation, implementing Interface Measure
	  */
	public String getCalculationClass () 
	{
		return (String)get_Value(COLUMNNAME_CalculationClass);
	}

	/** Set Cashier Count.
		@param CashierCount 
		Total count an employee be a cashier
	  */
	public void setCashierCount (BigDecimal CashierCount)
	{
		set_Value (COLUMNNAME_CashierCount, CashierCount);
	}

	/** Get Cashier Count.
		@return Total count an employee be a cashier
	  */
	public BigDecimal getCashierCount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashierCount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cashier Incentive.
		@param CashierIncentiveAmt Cashier Incentive	  */
	public void setCashierIncentiveAmt (BigDecimal CashierIncentiveAmt)
	{
		set_ValueNoCheck (COLUMNNAME_CashierIncentiveAmt, CashierIncentiveAmt);
	}

	/** Get Cashier Incentive.
		@return Cashier Incentive	  */
	public BigDecimal getCashierIncentiveAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashierIncentiveAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Group Incentive.
		@param GroupIncentiveAmt Group Incentive	  */
	public void setGroupIncentiveAmt (BigDecimal GroupIncentiveAmt)
	{
		set_ValueNoCheck (COLUMNNAME_GroupIncentiveAmt, GroupIncentiveAmt);
	}

	/** Get Group Incentive.
		@return Group Incentive	  */
	public BigDecimal getGroupIncentiveAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GroupIncentiveAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Individual Target.
		@param IndividualTargetAmt 
		Individual Target Amount
	  */
	public void setIndividualTargetAmt (BigDecimal IndividualTargetAmt)
	{
		throw new IllegalArgumentException ("IndividualTargetAmt is virtual column");	}

	/** Get Individual Target.
		@return Individual Target Amount
	  */
	public BigDecimal getIndividualTargetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_IndividualTargetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Master Schedule.
		@param IsMasterSchedule 
		To indicate if it is a master schedule.
	  */
	public void setIsMasterSchedule (boolean IsMasterSchedule)
	{
		set_Value (COLUMNNAME_IsMasterSchedule, Boolean.valueOf(IsMasterSchedule));
	}

	/** Get Master Schedule.
		@return To indicate if it is a master schedule.
	  */
	public boolean isMasterSchedule () 
	{
		Object oo = get_Value(COLUMNNAME_IsMasterSchedule);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Min. Individual Sales Target.
		@param MinIdvSalesTargetAmt 
		Minimum target amount for each sales person
	  */
	public void setMinIdvSalesTargetAmt (BigDecimal MinIdvSalesTargetAmt)
	{
		throw new IllegalArgumentException ("MinIdvSalesTargetAmt is virtual column");	}

	/** Get Min. Individual Sales Target.
		@return Minimum target amount for each sales person
	  */
	public BigDecimal getMinIdvSalesTargetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinIdvSalesTargetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Net Sales Realization.
		@param NetSalesRealizationAmt 
		Total realized net sales within 1 month from all stores
	  */
	public void setNetSalesRealizationAmt (BigDecimal NetSalesRealizationAmt)
	{
		set_Value (COLUMNNAME_NetSalesRealizationAmt, NetSalesRealizationAmt);
	}

	/** Get Net Sales Realization.
		@return Total realized net sales within 1 month from all stores
	  */
	public BigDecimal getNetSalesRealizationAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesRealizationAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Not Present Sales.
		@param NotPresentSalesAmt 
		Amount in a defined currency
	  */
	public void setNotPresentSalesAmt (BigDecimal NotPresentSalesAmt)
	{
		set_Value (COLUMNNAME_NotPresentSalesAmt, NotPresentSalesAmt);
	}

	/** Get Not Present Sales.
		@return Amount in a defined currency
	  */
	public BigDecimal getNotPresentSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NotPresentSalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Point.
		@param Point Point	  */
	public void setPoint (BigDecimal Point)
	{
		set_Value (COLUMNNAME_Point, Point);
	}

	/** Get Point.
		@return Point	  */
	public BigDecimal getPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Point);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Senior Supervisor = SNRS */
	public static final String POSITIONTYPE_SeniorSupervisor = "SNRS";
	/** Shop Supervisor = SSPV */
	public static final String POSITIONTYPE_ShopSupervisor = "SSPV";
	/** Staff = STAF */
	public static final String POSITIONTYPE_Staff = "STAF";
	/** Manager = MNGR */
	public static final String POSITIONTYPE_Manager = "MNGR";
	/** Junior Manager = JMGR */
	public static final String POSITIONTYPE_JuniorManager = "JMGR";
	/** Support = SUPT */
	public static final String POSITIONTYPE_Support = "SUPT";
	/** Set Position Type.
		@param PositionType Position Type	  */
	public void setPositionType (String PositionType)
	{

		set_Value (COLUMNNAME_PositionType, PositionType);
	}

	/** Get Position Type.
		@return Position Type	  */
	public String getPositionType () 
	{
		return (String)get_Value(COLUMNNAME_PositionType);
	}

	public org.compiere.model.I_C_BPartner getPrevShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getPrevShop_ID(), get_TrxName());	}

	/** Set Previous Shop.
		@param PrevShop_ID 
		Identifies a Shop
	  */
	public void setPrevShop_ID (int PrevShop_ID)
	{
		if (PrevShop_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_PrevShop_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_PrevShop_ID, Integer.valueOf(PrevShop_ID));
	}

	/** Get Previous Shop.
		@return Identifies a Shop
	  */
	public int getPrevShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PrevShop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Push Money Amount.
		@param PushMoneyAmt Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt)
	{
		set_Value (COLUMNNAME_PushMoneyAmt, PushMoneyAmt);
	}

	/** Get Push Money Amount.
		@return Push Money Amount	  */
	public BigDecimal getPushMoneyAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PushMoneyAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sales Incentive.
		@param SalesIncentiveAmt 
		Group incentives amount for a sales.
	  */
	public void setSalesIncentiveAmt (BigDecimal SalesIncentiveAmt)
	{
		set_ValueNoCheck (COLUMNNAME_SalesIncentiveAmt, SalesIncentiveAmt);
	}

	/** Get Sales Incentive.
		@return Group incentives amount for a sales.
	  */
	public BigDecimal getSalesIncentiveAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SalesIncentiveAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getShop_ID(), get_TrxName());	}

	/** Set Shop.
		@param Shop_ID 
		Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID)
	{
		if (Shop_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, Integer.valueOf(Shop_ID));
	}

	/** Get Shop.
		@return Identifies a Shop
	  */
	public int getShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Shop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tandem Count.
		@param TandemCount 
		Total count an employee be a tandem of cashier
	  */
	public void setTandemCount (BigDecimal TandemCount)
	{
		set_Value (COLUMNNAME_TandemCount, TandemCount);
	}

	/** Get Tandem Count.
		@return Total count an employee be a tandem of cashier
	  */
	public BigDecimal getTandemCount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TandemCount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TotalCuti.
		@param TotalCuti TotalCuti	  */
	public void setTotalCuti (int TotalCuti)
	{
		set_Value (COLUMNNAME_TotalCuti, Integer.valueOf(TotalCuti));
	}

	/** Get TotalCuti.
		@return TotalCuti	  */
	public int getTotalCuti () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalCuti);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Cuti Hamil.
		@param TotalCuti_CH Total Cuti Hamil	  */
	public void setTotalCuti_CH (int TotalCuti_CH)
	{
		set_Value (COLUMNNAME_TotalCuti_CH, Integer.valueOf(TotalCuti_CH));
	}

	/** Get Total Cuti Hamil.
		@return Total Cuti Hamil	  */
	public int getTotalCuti_CH () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalCuti_CH);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Cuti Panjang.
		@param TotalCuti_CP Total Cuti Panjang	  */
	public void setTotalCuti_CP (int TotalCuti_CP)
	{
		set_Value (COLUMNNAME_TotalCuti_CP, Integer.valueOf(TotalCuti_CP));
	}

	/** Get Total Cuti Panjang.
		@return Total Cuti Panjang	  */
	public int getTotalCuti_CP () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalCuti_CP);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Cuti Tahunan.
		@param TotalCuti_CT Total Cuti Tahunan	  */
	public void setTotalCuti_CT (int TotalCuti_CT)
	{
		set_Value (COLUMNNAME_TotalCuti_CT, Integer.valueOf(TotalCuti_CT));
	}

	/** Get Total Cuti Tahunan.
		@return Total Cuti Tahunan	  */
	public int getTotalCuti_CT () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalCuti_CT);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Cuti Tanpa Bayar.
		@param TotalCuti_CTB Total Cuti Tanpa Bayar	  */
	public void setTotalCuti_CTB (int TotalCuti_CTB)
	{
		set_Value (COLUMNNAME_TotalCuti_CTB, Integer.valueOf(TotalCuti_CTB));
	}

	/** Get Total Cuti Tanpa Bayar.
		@return Total Cuti Tanpa Bayar	  */
	public int getTotalCuti_CTB () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalCuti_CTB);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set TotalLibur.
		@param TotalLibur TotalLibur	  */
	public void setTotalLibur (int TotalLibur)
	{
		set_Value (COLUMNNAME_TotalLibur, Integer.valueOf(TotalLibur));
	}

	/** Get TotalLibur.
		@return TotalLibur	  */
	public int getTotalLibur () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalLibur);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Libur Mingguan.
		@param TotalLibur_L Total Libur Mingguan	  */
	public void setTotalLibur_L (int TotalLibur_L)
	{
		set_Value (COLUMNNAME_TotalLibur_L, Integer.valueOf(TotalLibur_L));
	}

	/** Get Total Libur Mingguan.
		@return Total Libur Mingguan	  */
	public int getTotalLibur_L () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalLibur_L);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Libur Umum/Nasional.
		@param TotalLibur_LU Total Libur Umum/Nasional	  */
	public void setTotalLibur_LU (int TotalLibur_LU)
	{
		set_Value (COLUMNNAME_TotalLibur_LU, Integer.valueOf(TotalLibur_LU));
	}

	/** Get Total Libur Umum/Nasional.
		@return Total Libur Umum/Nasional	  */
	public int getTotalLibur_LU () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalLibur_LU);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set TotalShift1.
		@param TotalShift1 TotalShift1	  */
	public void setTotalShift1 (int TotalShift1)
	{
		set_Value (COLUMNNAME_TotalShift1, Integer.valueOf(TotalShift1));
	}

	/** Get TotalShift1.
		@return TotalShift1	  */
	public int getTotalShift1 () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalShift1);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set TotalShift2.
		@param TotalShift2 TotalShift2	  */
	public void setTotalShift2 (int TotalShift2)
	{
		set_Value (COLUMNNAME_TotalShift2, Integer.valueOf(TotalShift2));
	}

	/** Get TotalShift2.
		@return TotalShift2	  */
	public int getTotalShift2 () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalShift2);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set TotalShift3.
		@param TotalShift3 TotalShift3	  */
	public void setTotalShift3 (int TotalShift3)
	{
		set_Value (COLUMNNAME_TotalShift3, Integer.valueOf(TotalShift3));
	}

	/** Get TotalShift3.
		@return TotalShift3	  */
	public int getTotalShift3 () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalShift3);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Personal Sales Recapitulation.
		@param UNS_ShopEmployee_ID Personal Sales Recapitulation	  */
	public void setUNS_ShopEmployee_ID (int UNS_ShopEmployee_ID)
	{
		if (UNS_ShopEmployee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopEmployee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopEmployee_ID, Integer.valueOf(UNS_ShopEmployee_ID));
	}

	/** Get Personal Sales Recapitulation.
		@return Personal Sales Recapitulation	  */
	public int getUNS_ShopEmployee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShopEmployee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ShopEmployee_UU.
		@param UNS_ShopEmployee_UU UNS_ShopEmployee_UU	  */
	public void setUNS_ShopEmployee_UU (String UNS_ShopEmployee_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ShopEmployee_UU, UNS_ShopEmployee_UU);
	}

	/** Get UNS_ShopEmployee_UU.
		@return UNS_ShopEmployee_UU	  */
	public String getUNS_ShopEmployee_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ShopEmployee_UU);
	}

	public com.uns.model.I_UNS_ShopRecap getUNS_ShopRecap() throws RuntimeException
    {
		return (com.uns.model.I_UNS_ShopRecap)MTable.get(getCtx(), com.uns.model.I_UNS_ShopRecap.Table_Name)
			.getPO(getUNS_ShopRecap_ID(), get_TrxName());	}

	/** Set Shop Sales Recapitulation.
		@param UNS_ShopRecap_ID Shop Sales Recapitulation	  */
	public void setUNS_ShopRecap_ID (int UNS_ShopRecap_ID)
	{
		if (UNS_ShopRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_ID, Integer.valueOf(UNS_ShopRecap_ID));
	}

	/** Get Shop Sales Recapitulation.
		@return Shop Sales Recapitulation	  */
	public int getUNS_ShopRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShopRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_SubregionRecap getUNS_SubregionRecap() throws RuntimeException
    {
		return (com.uns.model.I_UNS_SubregionRecap)MTable.get(getCtx(), com.uns.model.I_UNS_SubregionRecap.Table_Name)
			.getPO(getUNS_SubregionRecap_ID(), get_TrxName());	}

	/** Set Subregion Recapitulation.
		@param UNS_SubregionRecap_ID Subregion Recapitulation	  */
	public void setUNS_SubregionRecap_ID (int UNS_SubregionRecap_ID)
	{
		if (UNS_SubregionRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_ID, Integer.valueOf(UNS_SubregionRecap_ID));
	}

	/** Get Subregion Recapitulation.
		@return Subregion Recapitulation	  */
	public int getUNS_SubregionRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SubregionRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}