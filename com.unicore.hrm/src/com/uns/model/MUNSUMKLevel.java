/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author menjangan
 *
 */
public class MUNSUMKLevel extends X_UNS_UMKLevel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8306001862134429490L;

	/**
	 * @param ctx
	 * @param UNS_UMKLevel_ID
	 * @param trxName
	 */
	public MUNSUMKLevel(Properties ctx, int UNS_UMKLevel_ID, String trxName) {
		super(ctx, UNS_UMKLevel_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUMKLevel(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public static BigDecimal getAmount (String trxName, int UNS_UMKLevel_ID)
	{
		String sql = "SELECT Amount FROM UNS_UMKLevel WHERE UNS_UMKLevel_ID = ? "
				+ " AND IsActive = ? ";
		BigDecimal amt = DB.getSQLValueBD(trxName, sql, UNS_UMKLevel_ID, "Y");
		if (amt == null)
			amt = Env.ZERO;
		return amt;
	}
}
