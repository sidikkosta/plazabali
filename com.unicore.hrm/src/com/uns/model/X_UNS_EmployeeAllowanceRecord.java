/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_EmployeeAllowanceRecord
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EmployeeAllowanceRecord extends PO implements I_UNS_EmployeeAllowanceRecord, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181217L;

    /** Standard Constructor */
    public X_UNS_EmployeeAllowanceRecord (Properties ctx, int UNS_EmployeeAllowanceRecord_ID, String trxName)
    {
      super (ctx, UNS_EmployeeAllowanceRecord_ID, trxName);
      /** if (UNS_EmployeeAllowanceRecord_ID == 0)
        {
			setC_Year_ID (0);
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setLeaveClaimReserved (Env.ZERO);
			setLeavePeriodType (null);
// YL
			setLeaveReservedUsed (Env.ZERO);
// 0
			setMedicalAllowance (Env.ZERO);
			setMedicalAllowanceUsed (Env.ZERO);
			setPeriodDateEnd (new Timestamp( System.currentTimeMillis() ));
			setPeriodDateStart (new Timestamp( System.currentTimeMillis() ));
			setPreviousBalance (Env.ZERO);
// 0
			setProcessed (false);
// N
			setRemainingAmt (Env.ZERO);
			setUNS_Contract_Recommendation_ID (0);
			setUNS_Employee_ID (0);
			setUNS_EmployeeAllowanceRecord_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EmployeeAllowanceRecord (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EmployeeAllowanceRecord[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException
    {
		return (org.compiere.model.I_C_Year)MTable.get(getCtx(), org.compiere.model.I_C_Year.Table_Name)
			.getPO(getC_Year_ID(), get_TrxName());	}

	/** Set Year.
		@param C_Year_ID 
		Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID)
	{
		if (C_Year_ID < 1) 
			set_Value (COLUMNNAME_C_Year_ID, null);
		else 
			set_Value (COLUMNNAME_C_Year_ID, Integer.valueOf(C_Year_ID));
	}

	/** Get Year.
		@return Calendar Year
	  */
	public int getC_Year_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Year_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Total Leave Claim.
		@param LeaveClaimReserved 
		The total of emloyee's leave claim reserved in a period of time
	  */
	public void setLeaveClaimReserved (BigDecimal LeaveClaimReserved)
	{
		set_Value (COLUMNNAME_LeaveClaimReserved, LeaveClaimReserved);
	}

	/** Get Total Leave Claim.
		@return The total of emloyee's leave claim reserved in a period of time
	  */
	public BigDecimal getLeaveClaimReserved () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LeaveClaimReserved);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Long Leave = LL */
	public static final String LEAVEPERIODTYPE_LongLeave = "LL";
	/** Yearly Leave = YL */
	public static final String LEAVEPERIODTYPE_YearlyLeave = "YL";
	/** Set Leave Period Type.
		@param LeavePeriodType 
		Select one of the list based on the requested period.
	  */
	public void setLeavePeriodType (String LeavePeriodType)
	{

		set_Value (COLUMNNAME_LeavePeriodType, LeavePeriodType);
	}

	/** Get Leave Period Type.
		@return Select one of the list based on the requested period.
	  */
	public String getLeavePeriodType () 
	{
		return (String)get_Value(COLUMNNAME_LeavePeriodType);
	}

	/** Set Leave Reserved Used.
		@param LeaveReservedUsed 
		The amount of Leave claim reserved used by employee
	  */
	public void setLeaveReservedUsed (BigDecimal LeaveReservedUsed)
	{
		set_Value (COLUMNNAME_LeaveReservedUsed, LeaveReservedUsed);
	}

	/** Get Leave Reserved Used.
		@return The amount of Leave claim reserved used by employee
	  */
	public BigDecimal getLeaveReservedUsed () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LeaveReservedUsed);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Medical Allowance.
		@param MedicalAllowance 
		The yearly employee's medical allowance amount
	  */
	public void setMedicalAllowance (BigDecimal MedicalAllowance)
	{
		set_Value (COLUMNNAME_MedicalAllowance, MedicalAllowance);
	}

	/** Get Medical Allowance.
		@return The yearly employee's medical allowance amount
	  */
	public BigDecimal getMedicalAllowance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MedicalAllowance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Medical Allowance Used.
		@param MedicalAllowanceUsed Medical Allowance Used	  */
	public void setMedicalAllowanceUsed (BigDecimal MedicalAllowanceUsed)
	{
		set_Value (COLUMNNAME_MedicalAllowanceUsed, MedicalAllowanceUsed);
	}

	/** Get Medical Allowance Used.
		@return Medical Allowance Used	  */
	public BigDecimal getMedicalAllowanceUsed () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MedicalAllowanceUsed);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Period Date End.
		@param PeriodDateEnd Period Date End	  */
	public void setPeriodDateEnd (Timestamp PeriodDateEnd)
	{
		set_Value (COLUMNNAME_PeriodDateEnd, PeriodDateEnd);
	}

	/** Get Period Date End.
		@return Period Date End	  */
	public Timestamp getPeriodDateEnd () 
	{
		return (Timestamp)get_Value(COLUMNNAME_PeriodDateEnd);
	}

	/** Set Period Date Start.
		@param PeriodDateStart Period Date Start	  */
	public void setPeriodDateStart (Timestamp PeriodDateStart)
	{
		set_Value (COLUMNNAME_PeriodDateStart, PeriodDateStart);
	}

	/** Get Period Date Start.
		@return Period Date Start	  */
	public Timestamp getPeriodDateStart () 
	{
		return (Timestamp)get_Value(COLUMNNAME_PeriodDateStart);
	}

	public com.uns.model.I_UNS_EmployeeAllowanceRecord getPrev_Record() throws RuntimeException
    {
		return (com.uns.model.I_UNS_EmployeeAllowanceRecord)MTable.get(getCtx(), com.uns.model.I_UNS_EmployeeAllowanceRecord.Table_Name)
			.getPO(getPrev_Record_ID(), get_TrxName());	}

	/** Set Previous Record.
		@param Prev_Record_ID Previous Record	  */
	public void setPrev_Record_ID (int Prev_Record_ID)
	{
		if (Prev_Record_ID < 1) 
			set_Value (COLUMNNAME_Prev_Record_ID, null);
		else 
			set_Value (COLUMNNAME_Prev_Record_ID, Integer.valueOf(Prev_Record_ID));
	}

	/** Get Previous Record.
		@return Previous Record	  */
	public int getPrev_Record_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Prev_Record_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Previous Balance.
		@param PreviousBalance Previous Balance	  */
	public void setPreviousBalance (BigDecimal PreviousBalance)
	{
		set_Value (COLUMNNAME_PreviousBalance, PreviousBalance);
	}

	/** Get Previous Balance.
		@return Previous Balance	  */
	public BigDecimal getPreviousBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PreviousBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Remaining Amt.
		@param RemainingAmt 
		Remaining Amount
	  */
	public void setRemainingAmt (BigDecimal RemainingAmt)
	{
		set_Value (COLUMNNAME_RemainingAmt, RemainingAmt);
	}

	/** Get Remaining Amt.
		@return Remaining Amount
	  */
	public BigDecimal getRemainingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Remaining Leave Reserved.
		@param RemainingQty 
		The total remaining leave reserved = total leave claim in the period - total leave used
	  */
	public void setRemainingQty (BigDecimal RemainingQty)
	{
		throw new IllegalArgumentException ("RemainingQty is virtual column");	}

	/** Get Remaining Leave Reserved.
		@return The total remaining leave reserved = total leave claim in the period - total leave used
	  */
	public BigDecimal getRemainingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Remarks.
		@param Remarks Remarks	  */
	public void setRemarks (String Remarks)
	{
		set_Value (COLUMNNAME_Remarks, Remarks);
	}

	/** Get Remarks.
		@return Remarks	  */
	public String getRemarks () 
	{
		return (String)get_Value(COLUMNNAME_Remarks);
	}

	public com.uns.model.I_UNS_Contract_Recommendation getUNS_Contract_Recommendation() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Contract_Recommendation)MTable.get(getCtx(), com.uns.model.I_UNS_Contract_Recommendation.Table_Name)
			.getPO(getUNS_Contract_Recommendation_ID(), get_TrxName());	}

	/** Set Contract.
		@param UNS_Contract_Recommendation_ID Contract	  */
	public void setUNS_Contract_Recommendation_ID (int UNS_Contract_Recommendation_ID)
	{
		if (UNS_Contract_Recommendation_ID < 1) 
			set_Value (COLUMNNAME_UNS_Contract_Recommendation_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Contract_Recommendation_ID, Integer.valueOf(UNS_Contract_Recommendation_ID));
	}

	/** Get Contract.
		@return Contract	  */
	public int getUNS_Contract_Recommendation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Contract_Recommendation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Employee Allowance Record.
		@param UNS_EmployeeAllowanceRecord_ID Employee Allowance Record	  */
	public void setUNS_EmployeeAllowanceRecord_ID (int UNS_EmployeeAllowanceRecord_ID)
	{
		if (UNS_EmployeeAllowanceRecord_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EmployeeAllowanceRecord_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EmployeeAllowanceRecord_ID, Integer.valueOf(UNS_EmployeeAllowanceRecord_ID));
	}

	/** Get Employee Allowance Record.
		@return Employee Allowance Record	  */
	public int getUNS_EmployeeAllowanceRecord_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EmployeeAllowanceRecord_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set uns_employeeallowancerecord_uu.
		@param uns_employeeallowancerecord_uu uns_employeeallowancerecord_uu	  */
	public void setuns_employeeallowancerecord_uu (String uns_employeeallowancerecord_uu)
	{
		set_Value (COLUMNNAME_uns_employeeallowancerecord_uu, uns_employeeallowancerecord_uu);
	}

	/** Get uns_employeeallowancerecord_uu.
		@return uns_employeeallowancerecord_uu	  */
	public String getuns_employeeallowancerecord_uu () 
	{
		return (String)get_Value(COLUMNNAME_uns_employeeallowancerecord_uu);
	}
}