/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PayrollCorrectionLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PayrollCorrectionLine extends PO implements I_UNS_PayrollCorrectionLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180630L;

    /** Standard Constructor */
    public X_UNS_PayrollCorrectionLine (Properties ctx, int UNS_PayrollCorrectionLine_ID, String trxName)
    {
      super (ctx, UNS_PayrollCorrectionLine_ID, trxName);
      /** if (UNS_PayrollCorrectionLine_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setCorrectionType (null);
			setDescription (null);
			setIsPPHComp (false);
// N
			setUNS_PayrollCorrection_ID (0);
			setUNS_PayrollCorrectionLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PayrollCorrectionLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PayrollCorrectionLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Allowance Correction = ACR */
	public static final String CORRECTIONTYPE_AllowanceCorrection = "ACR";
	/** Deduction Correction = DCR */
	public static final String CORRECTIONTYPE_DeductionCorrection = "DCR";
	/** Other Correction = OCR */
	public static final String CORRECTIONTYPE_OtherCorrection = "OCR";
	/** Presence Correction = PCR */
	public static final String CORRECTIONTYPE_PresenceCorrection = "PCR";
	/** Payroll Replacement Correction = PRC */
	public static final String CORRECTIONTYPE_PayrollReplacementCorrection = "PRC";
	/** Set Correction Type.
		@param CorrectionType Correction Type	  */
	public void setCorrectionType (String CorrectionType)
	{

		set_Value (COLUMNNAME_CorrectionType, CorrectionType);
	}

	/** Get Correction Type.
		@return Correction Type	  */
	public String getCorrectionType () 
	{
		return (String)get_Value(COLUMNNAME_CorrectionType);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set is Periodic?.
		@param isPeriodic is Periodic?	  */
	public void setisPeriodic (boolean isPeriodic)
	{
		set_Value (COLUMNNAME_isPeriodic, Boolean.valueOf(isPeriodic));
	}

	/** Get is Periodic?.
		@return is Periodic?	  */
	public boolean isPeriodic () 
	{
		Object oo = get_Value(COLUMNNAME_isPeriodic);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set PPH Component ?.
		@param IsPPHComp PPH Component ?	  */
	public void setIsPPHComp (boolean IsPPHComp)
	{
		set_Value (COLUMNNAME_IsPPHComp, Boolean.valueOf(IsPPHComp));
	}

	/** Get PPH Component ?.
		@return PPH Component ?	  */
	public boolean isPPHComp () 
	{
		Object oo = get_Value(COLUMNNAME_IsPPHComp);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Presence Date.
		@param PresenceDate Presence Date	  */
	public void setPresenceDate (Timestamp PresenceDate)
	{
		set_Value (COLUMNNAME_PresenceDate, PresenceDate);
	}

	/** Get Presence Date.
		@return Presence Date	  */
	public Timestamp getPresenceDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_PresenceDate);
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.uns.model.I_UNS_Payroll_Component_Conf getUNS_Payroll_Component_Conf() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Payroll_Component_Conf)MTable.get(getCtx(), com.uns.model.I_UNS_Payroll_Component_Conf.Table_Name)
			.getPO(getUNS_Payroll_Component_Conf_ID(), get_TrxName());	}

	/** Set Payroll Component Configuration.
		@param UNS_Payroll_Component_Conf_ID Payroll Component Configuration	  */
	public void setUNS_Payroll_Component_Conf_ID (int UNS_Payroll_Component_Conf_ID)
	{
		if (UNS_Payroll_Component_Conf_ID < 1) 
			set_Value (COLUMNNAME_UNS_Payroll_Component_Conf_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Payroll_Component_Conf_ID, Integer.valueOf(UNS_Payroll_Component_Conf_ID));
	}

	/** Get Payroll Component Configuration.
		@return Payroll Component Configuration	  */
	public int getUNS_Payroll_Component_Conf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Payroll_Component_Conf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_PayrollCorrection getUNS_PayrollCorrection() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PayrollCorrection)MTable.get(getCtx(), com.uns.model.I_UNS_PayrollCorrection.Table_Name)
			.getPO(getUNS_PayrollCorrection_ID(), get_TrxName());	}

	/** Set Payroll Correction.
		@param UNS_PayrollCorrection_ID Payroll Correction	  */
	public void setUNS_PayrollCorrection_ID (int UNS_PayrollCorrection_ID)
	{
		if (UNS_PayrollCorrection_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrection_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrection_ID, Integer.valueOf(UNS_PayrollCorrection_ID));
	}

	/** Get Payroll Correction.
		@return Payroll Correction	  */
	public int getUNS_PayrollCorrection_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollCorrection_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Correction Line.
		@param UNS_PayrollCorrectionLine_ID Correction Line	  */
	public void setUNS_PayrollCorrectionLine_ID (int UNS_PayrollCorrectionLine_ID)
	{
		if (UNS_PayrollCorrectionLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrectionLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrectionLine_ID, Integer.valueOf(UNS_PayrollCorrectionLine_ID));
	}

	/** Get Correction Line.
		@return Correction Line	  */
	public int getUNS_PayrollCorrectionLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollCorrectionLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PayrollCorrectionLine_UU.
		@param UNS_PayrollCorrectionLine_UU UNS_PayrollCorrectionLine_UU	  */
	public void setUNS_PayrollCorrectionLine_UU (String UNS_PayrollCorrectionLine_UU)
	{
		set_Value (COLUMNNAME_UNS_PayrollCorrectionLine_UU, UNS_PayrollCorrectionLine_UU);
	}

	/** Get UNS_PayrollCorrectionLine_UU.
		@return UNS_PayrollCorrectionLine_UU	  */
	public String getUNS_PayrollCorrectionLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PayrollCorrectionLine_UU);
	}

	public com.uns.model.I_UNS_Payroll_Employee getUNS_Payroll_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Payroll_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Payroll_Employee.Table_Name)
			.getPO(getUNS_Payroll_Employee_ID(), get_TrxName());	}

	/** Set Payroll Employee.
		@param UNS_Payroll_Employee_ID Payroll Employee	  */
	public void setUNS_Payroll_Employee_ID (int UNS_Payroll_Employee_ID)
	{
		if (UNS_Payroll_Employee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Payroll_Employee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Payroll_Employee_ID, Integer.valueOf(UNS_Payroll_Employee_ID));
	}

	/** Get Payroll Employee.
		@return Payroll Employee	  */
	public int getUNS_Payroll_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Payroll_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}