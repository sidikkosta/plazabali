/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_BPJSLog_Config
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_BPJSLog_Config extends PO implements I_UNS_BPJSLog_Config, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190313L;

    /** Standard Constructor */
    public X_UNS_BPJSLog_Config (Properties ctx, int UNS_BPJSLog_Config_ID, String trxName)
    {
      super (ctx, UNS_BPJSLog_Config_ID, trxName);
      /** if (UNS_BPJSLog_Config_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_BPJSLog_Config (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_BPJSLog_Config[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Column getAD_Column() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Column)MTable.get(getCtx(), org.compiere.model.I_AD_Column.Table_Name)
			.getPO(getAD_Column_ID(), get_TrxName());	}

	/** Set Column.
		@param AD_Column_ID 
		Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID)
	{
		if (AD_Column_ID < 1) 
			set_Value (COLUMNNAME_AD_Column_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Column_ID, Integer.valueOf(AD_Column_ID));
	}

	/** Get Column.
		@return Column in the table
	  */
	public int getAD_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Basic Salary.
		@param BasicSalary Basic Salary	  */
	public void setBasicSalary (boolean BasicSalary)
	{
		set_Value (COLUMNNAME_BasicSalary, Boolean.valueOf(BasicSalary));
	}

	/** Get Basic Salary.
		@return Basic Salary	  */
	public boolean isBasicSalary () 
	{
		Object oo = get_Value(COLUMNNAME_BasicSalary);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Identitas = 1 */
	public static final String MUTASITYPE_Identitas = "1";
	/** Non Aktif Peserta Akhir Bulan = 10 */
	public static final String MUTASITYPE_NonAktifPesertaAkhirBulan = "10";
	/** Domisili = 2 */
	public static final String MUTASITYPE_Domisili = "2";
	/** Faskes = 3 */
	public static final String MUTASITYPE_Faskes = "3";
	/** Pekerjaan = 4 */
	public static final String MUTASITYPE_Pekerjaan = "4";
	/** Asuransi = 5 */
	public static final String MUTASITYPE_Asuransi = "5";
	/** Passport = 6 */
	public static final String MUTASITYPE_Passport = "6";
	/** Non Aktif Peserta Langsung = 7 */
	public static final String MUTASITYPE_NonAktifPesertaLangsung = "7";
	/** Pindah mutasi dari PPU ke PBPU = 8 */
	public static final String MUTASITYPE_PindahMutasiDariPPUKePBPU = "8";
	/** Pindah mutasi dari PBPU ke PPU = 9 */
	public static final String MUTASITYPE_PindahMutasiDariPBPUKePPU = "9";
	/** Penambahan Anggota Keluarga = 99 */
	public static final String MUTASITYPE_PenambahanAnggotaKeluarga = "99";
	/** Set Mutasi Type.
		@param MutasiType Mutasi Type	  */
	public void setMutasiType (String MutasiType)
	{

		set_Value (COLUMNNAME_MutasiType, MutasiType);
	}

	/** Get Mutasi Type.
		@return Mutasi Type	  */
	public String getMutasiType () 
	{
		return (String)get_Value(COLUMNNAME_MutasiType);
	}

	/** Set BPJS Log Config.
		@param UNS_BPJSLog_Config_ID BPJS Log Config	  */
	public void setUNS_BPJSLog_Config_ID (int UNS_BPJSLog_Config_ID)
	{
		if (UNS_BPJSLog_Config_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BPJSLog_Config_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BPJSLog_Config_ID, Integer.valueOf(UNS_BPJSLog_Config_ID));
	}

	/** Get BPJS Log Config.
		@return BPJS Log Config	  */
	public int getUNS_BPJSLog_Config_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BPJSLog_Config_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_BPJSLog_Config_UU.
		@param UNS_BPJSLog_Config_UU UNS_BPJSLog_Config_UU	  */
	public void setUNS_BPJSLog_Config_UU (String UNS_BPJSLog_Config_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_BPJSLog_Config_UU, UNS_BPJSLog_Config_UU);
	}

	/** Get UNS_BPJSLog_Config_UU.
		@return UNS_BPJSLog_Config_UU	  */
	public String getUNS_BPJSLog_Config_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_BPJSLog_Config_UU);
	}
}