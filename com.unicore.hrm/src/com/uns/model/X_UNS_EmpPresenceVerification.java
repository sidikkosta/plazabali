/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_EmpPresenceVerification
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EmpPresenceVerification extends PO implements I_UNS_EmpPresenceVerification, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181008L;

    /** Standard Constructor */
    public X_UNS_EmpPresenceVerification (Properties ctx, int UNS_EmpPresenceVerification_ID, String trxName)
    {
      super (ctx, UNS_EmpPresenceVerification_ID, trxName);
      /** if (UNS_EmpPresenceVerification_ID == 0)
        {
			setUNS_Employee_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EmpPresenceVerification (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EmpPresenceVerification[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Is Absence.
		@param IsAbsence Is Absence	  */
	public void setIsAbsence (boolean IsAbsence)
	{
		set_Value (COLUMNNAME_IsAbsence, Boolean.valueOf(IsAbsence));
	}

	/** Get Is Absence.
		@return Is Absence	  */
	public boolean isAbsence () 
	{
		Object oo = get_Value(COLUMNNAME_IsAbsence);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Employee Presence Verification.
		@param UNS_EmpPresenceVerification_ID Employee Presence Verification	  */
	public void setUNS_EmpPresenceVerification_ID (int UNS_EmpPresenceVerification_ID)
	{
		if (UNS_EmpPresenceVerification_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EmpPresenceVerification_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EmpPresenceVerification_ID, Integer.valueOf(UNS_EmpPresenceVerification_ID));
	}

	/** Get Employee Presence Verification.
		@return Employee Presence Verification	  */
	public int getUNS_EmpPresenceVerification_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EmpPresenceVerification_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EmpPresenceVerification_UU.
		@param UNS_EmpPresenceVerification_UU UNS_EmpPresenceVerification_UU	  */
	public void setUNS_EmpPresenceVerification_UU (String UNS_EmpPresenceVerification_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EmpPresenceVerification_UU, UNS_EmpPresenceVerification_UU);
	}

	/** Get UNS_EmpPresenceVerification_UU.
		@return UNS_EmpPresenceVerification_UU	  */
	public String getUNS_EmpPresenceVerification_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EmpPresenceVerification_UU);
	}

	public com.uns.model.I_UNS_PresenceVerification getUNS_PresenceVerification() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PresenceVerification)MTable.get(getCtx(), com.uns.model.I_UNS_PresenceVerification.Table_Name)
			.getPO(getUNS_PresenceVerification_ID(), get_TrxName());	}

	/** Set Presence Verification.
		@param UNS_PresenceVerification_ID Presence Verification	  */
	public void setUNS_PresenceVerification_ID (int UNS_PresenceVerification_ID)
	{
		if (UNS_PresenceVerification_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PresenceVerification_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PresenceVerification_ID, Integer.valueOf(UNS_PresenceVerification_ID));
	}

	/** Get Presence Verification.
		@return Presence Verification	  */
	public int getUNS_PresenceVerification_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PresenceVerification_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}