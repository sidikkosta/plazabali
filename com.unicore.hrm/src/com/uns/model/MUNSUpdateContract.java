/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MColumn;
import org.compiere.model.MTable;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSUpdateContract extends X_UNS_UpdateContract implements DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5658181495091670006L;

	/**
	 * @param ctx
	 * @param UNS_UpdateContract_ID
	 * @param trxName
	 */
	public MUNSUpdateContract(Properties ctx, int UNS_UpdateContract_ID,
			String trxName) {
		super(ctx, UNS_UpdateContract_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUpdateContract(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT COUNT(*) FROM UNS_UpdateContract WHERE"
				+ " PayrollLevel = ? AND PayrollTerm = ? AND UNS_UpdateContract_ID <> ?"
				+ " AND DocStatus NOT IN ('VO', 'RE', 'CL', 'CO')";
		int count = DB.getSQLValue(get_TrxName(), sql, getPayrollLevel(), getPayrollTerm(), get_ID());
		if(count > 0)
		{
			log.saveError("Error", "Duplicate record unprocess.");
			return false;
		}
		
		MUNSPayrollConfiguration payrollConf = MUNSPayrollConfiguration.get(
				getCtx(), getDateDoc(), getAD_Org_ID(), get_TrxName(), true);
		if(payrollConf == null)
		{
			log.saveError("Error", "Cannnot foound Payroll Configuration");
			return false;
		}
		
		if(payrollConf.isMultiplicationOTCalc() && (getNewLeburJamPertama().signum() > 0 || getNewLeburJamBerikutnya().signum() > 0 || getNew_A_L1().signum() > 0
				|| getNew_A_L2().signum() > 0 || getNew_A_L3().signum() > 0 || getNew_A_L1_R().signum() > 0
				|| getNew_A_L2_R().signum() > 0 || getNew_A_L3_R().signum() > 0))
		{
			log.saveError("Error", "Disallowed update overtime amount component. Payroll Configuration - IsMultiplicaionOTCalc is True");
			return false;
		}
		else if(!payrollConf.isMultiplicationOTCalc() && (getFirstOTMultiplier().signum() > 0 || getNextOTMultiplier().signum() > 0 || getAL1Multiplier().signum() > 0
				|| getAL2Multiplier().signum() > 0 || getAL3Multiplier().signum() > 0 || getALR1Multiplier().signum() > 0
				|| getALR2Multiplier().signum() > 0 || getALR3Multiplier().signum() > 0))
		{
			log.saveError("Error", "Disallowed update overtime multiplier component. Payroll Configuration - IsMultiplicaionOTCalc is False");
			return false;
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_UpdateContractHistory WHERE"
				+ " UNS_UpdateContract_ID = ?";
		return DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSUpdateContract[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_UpdateContract_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		MUNSEmployee[] emps = getEmployees();
		if(emps.length == 0)
		{
			m_processMsg = "@NoLines@";
			return DocAction.STATUS_Invalid;
		}
		
		for(int i=0;i<emps.length;i++)
		{
			MTable table = MTable.get(getCtx(), Table_Name);
			MColumn[] columns = table.getColumns(false);
			MUNSContractRecommendation cr = new MUNSContractRecommendation(getCtx(), emps[i].getUNS_Contract_Recommendation_ID(),
					get_TrxName());
			
			for(int j=0;j<columns.length;j++)
			{
				if(!columns[j].isAllowCopy())
					continue;
				
				String columnName = columns[j].getColumnName();
				if(get_Value(columnName) == null || ((BigDecimal) get_Value(columnName)).signum() < 0)
					continue;
				cr.set_ValueOfColumn(columnName, get_Value(columnName));
				if(!cr.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Error when trying update contract or payroll base employee.");
					return DocAction.STATUS_Invalid;
				}
				else
				{
					m_processMsg = cr.generatePayrollBaseEmployee();
					if(m_processMsg != null)
						return DocAction.STATUS_Invalid;
				}
			}
		}
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(getDocStatus().equals("CO") && !reverseCorrectIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
	
		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return true;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return Env.ZERO;
	}

	@Override
	public boolean approveIt() {
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public MUNSEmployee[] getEmployees()
	{
		String whereClause = "UNS_Employee_ID IN (SELECT uch.UNS_Employee_ID"
				+ " FROM UNS_UpdateContractHistory uch WHERE uch.UNS_UpdateContract_ID = ?)";
		List<MUNSEmployee> list = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSEmployee.Table_Name, whereClause, get_TrxName()).setParameters(get_ID()).list();
		
		return list.toArray(new MUNSEmployee[list.size()]);
	}
}