/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_LeaveReservedConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_LeaveReservedConfig 
{

    /** TableName=UNS_LeaveReservedConfig */
    public static final String Table_Name = "UNS_LeaveReservedConfig";

    /** AD_Table_ID=1000100 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AbsencesCount */
    public static final String COLUMNNAME_AbsencesCount = "AbsencesCount";

	/** Set Absences Count	  */
	public void setAbsencesCount (int AbsencesCount);

	/** Get Absences Count	  */
	public int getAbsencesCount();

    /** Column name AbsencesCutLeave */
    public static final String COLUMNNAME_AbsencesCutLeave = "AbsencesCutLeave";

	/** Set Absences Cut Leave	  */
	public void setAbsencesCutLeave (boolean AbsencesCutLeave);

	/** Get Absences Cut Leave	  */
	public boolean isAbsencesCutLeave();

    /** Column name AccumulatedLeavePeriod */
    public static final String COLUMNNAME_AccumulatedLeavePeriod = "AccumulatedLeavePeriod";

	/** Set Accumulated Leave Period	  */
	public void setAccumulatedLeavePeriod (String AccumulatedLeavePeriod);

	/** Get Accumulated Leave Period	  */
	public String getAccumulatedLeavePeriod();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AutoCreateLeaveONUnpaidLeave */
    public static final String COLUMNNAME_AutoCreateLeaveONUnpaidLeave = "AutoCreateLeaveONUnpaidLeave";

	/** Set Auto Create Leave ON Unpaid Leave	  */
	public void setAutoCreateLeaveONUnpaidLeave (boolean AutoCreateLeaveONUnpaidLeave);

	/** Get Auto Create Leave ON Unpaid Leave	  */
	public boolean isAutoCreateLeaveONUnpaidLeave();

    /** Column name C_JobCategory_ID */
    public static final String COLUMNNAME_C_JobCategory_ID = "C_JobCategory_ID";

	/** Set Position Category.
	  * Job Position Category
	  */
	public void setC_JobCategory_ID (int C_JobCategory_ID);

	/** Get Position Category.
	  * Job Position Category
	  */
	public int getC_JobCategory_ID();

	public org.compiere.model.I_C_JobCategory getC_JobCategory() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DisallowNegativeLeave */
    public static final String COLUMNNAME_DisallowNegativeLeave = "DisallowNegativeLeave";

	/** Set Disallow Negative Leave	  */
	public void setDisallowNegativeLeave (boolean DisallowNegativeLeave);

	/** Get Disallow Negative Leave	  */
	public boolean isDisallowNegativeLeave();

    /** Column name GracePeriod */
    public static final String COLUMNNAME_GracePeriod = "GracePeriod";

	/** Set Grace Period	  */
	public void setGracePeriod (int GracePeriod);

	/** Get Grace Period	  */
	public int getGracePeriod();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name LeaveCanUseOnStartWork */
    public static final String COLUMNNAME_LeaveCanUseOnStartWork = "LeaveCanUseOnStartWork";

	/** Set Leave can use on start work	  */
	public void setLeaveCanUseOnStartWork (boolean LeaveCanUseOnStartWork);

	/** Get Leave can use on start work	  */
	public boolean isLeaveCanUseOnStartWork();

    /** Column name LeaveClaimReserved */
    public static final String COLUMNNAME_LeaveClaimReserved = "LeaveClaimReserved";

	/** Set Total Leave Claim.
	  * The total of emloyee's leave claim reserved in a period of time
	  */
	public void setLeaveClaimReserved (BigDecimal LeaveClaimReserved);

	/** Get Total Leave Claim.
	  * The total of emloyee's leave claim reserved in a period of time
	  */
	public BigDecimal getLeaveClaimReserved();

    /** Column name LeaveType */
    public static final String COLUMNNAME_LeaveType = "LeaveType";

	/** Set LeaveType	  */
	public void setLeaveType (String LeaveType);

	/** Get LeaveType	  */
	public String getLeaveType();

    /** Column name LongLeaveRecurringYear */
    public static final String COLUMNNAME_LongLeaveRecurringYear = "LongLeaveRecurringYear";

	/** Set Long Leave Recurring Year	  */
	public void setLongLeaveRecurringYear (int LongLeaveRecurringYear);

	/** Get Long Leave Recurring Year	  */
	public int getLongLeaveRecurringYear();

    /** Column name MaternityClaimReserved */
    public static final String COLUMNNAME_MaternityClaimReserved = "MaternityClaimReserved";

	/** Set Maternity Claim Reserved	  */
	public void setMaternityClaimReserved (int MaternityClaimReserved);

	/** Get Maternity Claim Reserved	  */
	public int getMaternityClaimReserved();

    /** Column name MaximumAdvanceLeave */
    public static final String COLUMNNAME_MaximumAdvanceLeave = "MaximumAdvanceLeave";

	/** Set Maximum Advance Leave	  */
	public void setMaximumAdvanceLeave (int MaximumAdvanceLeave);

	/** Get Maximum Advance Leave	  */
	public int getMaximumAdvanceLeave();

    /** Column name MaxMaternity */
    public static final String COLUMNNAME_MaxMaternity = "MaxMaternity";

	/** Set Max. Maternity	  */
	public void setMaxMaternity (int MaxMaternity);

	/** Get Max. Maternity	  */
	public int getMaxMaternity();

    /** Column name Nationality */
    public static final String COLUMNNAME_Nationality = "Nationality";

	/** Set Nationality	  */
	public void setNationality (String Nationality);

	/** Get Nationality	  */
	public String getNationality();

    /** Column name PeriodAbsences */
    public static final String COLUMNNAME_PeriodAbsences = "PeriodAbsences";

	/** Set Period Absences	  */
	public void setPeriodAbsences (String PeriodAbsences);

	/** Get Period Absences	  */
	public String getPeriodAbsences();

    /** Column name ResetBasedOn */
    public static final String COLUMNNAME_ResetBasedOn = "ResetBasedOn";

	/** Set Reset Based On	  */
	public void setResetBasedOn (String ResetBasedOn);

	/** Get Reset Based On	  */
	public String getResetBasedOn();

    /** Column name UNS_LeaveReservedConfig_ID */
    public static final String COLUMNNAME_UNS_LeaveReservedConfig_ID = "UNS_LeaveReservedConfig_ID";

	/** Set UNS_LeaveReservedConfig	  */
	public void setUNS_LeaveReservedConfig_ID (int UNS_LeaveReservedConfig_ID);

	/** Get UNS_LeaveReservedConfig	  */
	public int getUNS_LeaveReservedConfig_ID();

    /** Column name UNS_LeaveReservedConfig_UU */
    public static final String COLUMNNAME_UNS_LeaveReservedConfig_UU = "UNS_LeaveReservedConfig_UU";

	/** Set UNS_LeaveReservedConfig_UU	  */
	public void setUNS_LeaveReservedConfig_UU (String UNS_LeaveReservedConfig_UU);

	/** Get UNS_LeaveReservedConfig_UU	  */
	public String getUNS_LeaveReservedConfig_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name UseDateCalendar */
    public static final String COLUMNNAME_UseDateCalendar = "UseDateCalendar";

	/** Set Use Date Calendar	  */
	public void setUseDateCalendar (boolean UseDateCalendar);

	/** Get Use Date Calendar	  */
	public boolean isUseDateCalendar();
}
