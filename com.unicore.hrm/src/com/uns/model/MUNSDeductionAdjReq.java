package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSDeductionAdjReq extends X_UNS_DeductionAdjReq implements DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3850490690780288357L;
	public String m_processMsg = null;
	public boolean m_justPrepared = false;
	
	public MUNSDeductionAdjReqLine[] m_lines = null;

	public MUNSDeductionAdjReq(Properties ctx, int UNS_DeductionAdjReq_ID,
			String trxName) {
		super(ctx, UNS_DeductionAdjReq_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSDeductionAdjReq(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		String sql = "SELECT 1 FROM UNS_DeductionAdjReq WHERE UNS_Employee_ID = ?"
				+ " AND C_Period_ID = ? AND DOcStatus NOT IN ('VO','RE')"
				+ " AND UNS_DeductionAdjReq_ID <> ?";
		boolean exists = DB.getSQLValue(
				get_TrxName(), sql, getUNS_Employee_ID(), getC_Period_ID(), getUNS_DeductionAdjReq_ID()) > 0;
		
		if(exists)
			throw new AdempiereException("Disallowed duplicate data");
		
		return super.beforeSave(newRecord);
	}
	
	public static BigDecimal AdjustmentAmt(Properties ctx,
			int UNS_Employee_ID, int C_Period_ID, String costBenType, String trxName) {
		
		BigDecimal amount = null;
		
		String whereClause = "(SELECT UNS_DeductionAdjReq_ID FROM UNS_DeductionAdjReq WHERE"
				+ " UNS_Employee_ID = ? AND C_Period_ID = ? AND DocStatus IN ('CO','CL'))";
		
		List<MUNSDeductionAdjReqLine> listLine = Query.get(
				ctx, UNSHRMModelFactory.EXTENSION_ID, MUNSDeductionAdjReqLine.Table_Name,
				"UNS_DeductionAdjReq_ID = "+whereClause, trxName)
				.setParameters(UNS_Employee_ID,C_Period_ID)
				.list();
		
		for(MUNSDeductionAdjReqLine line : listLine)
		{
			if(line.getCostBenefitType().equals(costBenType))
			{
				amount = line.getAdjustmentAmt();
				break;
			}
		}
		
		return amount;
	}
	
	public MUNSDeductionAdjReqLine[] getLines() {
		
		if(m_lines != null)
			return m_lines;
		
		List<MUNSDeductionAdjReqLine> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSDeductionAdjReqLine.Table_Name,
				"UNS_DeductionAdjReq_ID = ?", get_TrxName()).setParameters(getUNS_DeductionAdjReq_ID())
				.list();
		m_lines = new MUNSDeductionAdjReqLine[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}

	@Override
	public boolean processIt(String action) throws Exception {
		
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}

	@Override
	public boolean invalidateIt() {

		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}

	@Override
	public String prepareIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("PrepareIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;	
		
		setProcessed(true);
		m_justPrepared = true;
		
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		setIsApproved(true);
		setProcessed(true);
		return true;
	}

	@Override
	public boolean rejectIt() {

		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	@Override
	public String completeIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;	
		if (!isApproved())
			approveIt();
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("VoidIt - " + toString());
		// Before void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;	
		
		setProcessed(true);
		return true;
	}

	@Override
	public boolean closeIt() {
		
		return false;
	}

	@Override
	public boolean reverseCorrectIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;	
		
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;	

		return false;
	}

	@Override
	public boolean reActivateIt() {
		
		if (log.isLoggable(Level.INFO)) log.info("ReActiveIt - " + toString());
		// Before reactive
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		//check & update Payroll
		String sql = "SELECT UNS_Payroll_Employee_ID FROM UNS_Payroll_Employee WHERE C_Period_ID = ?"
				+ " AND UNS_Employee_ID = ? AND DocStatus NOT IN ('VO','RE')";
		int payroll_id = DB.getSQLValue(get_TrxName(), sql, getC_Period_ID(), getUNS_Employee_ID());
		if(payroll_id > 0)
		{
			MUNSPayrollEmployee	payroll = new MUNSPayrollEmployee(getCtx(), payroll_id, get_TrxName());
			if(payroll.getDocStatus().equals(MUNSPayrollEmployee.DOCSTATUS_Completed)
					|| payroll.getDocStatus().equals(MUNSPayrollEmployee.DOCSTATUS_Closed))
			{
				m_processMsg = "Payroll has been complete, disallowed re-Active.";
				return false;
			}
			else if (payroll.getDocStatus().equals(MUNSPayrollEmployee.DOCSTATUS_InProgress))
			{
				m_processMsg = "Payroll is in progress, disallowed re-Active.";
				return false;
			}
			else
			{
				payroll.setIsGenerate(false);
				payroll.saveEx();
			}
		}		
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;	
		
		setProcessed(false);
		return true;
	}

	@Override
	public String getSummary() {
		
		return null;
	}

	@Override
	public String getDocumentNo() {
		
		return null;
	}

	@Override
	public String getDocumentInfo() {
		
		return null;
	}

	@Override
	public File createPDF() {
		
		return null;
	}

	@Override
	public String getProcessMsg() {
		
		return null;
	}

	@Override
	public int getDoc_User_ID() {
		
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		
		return null;
	}
}
