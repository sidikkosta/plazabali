/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author eko
 *
 */
public class MUNSLeaveReservedConfig extends X_UNS_LeaveReservedConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param ctx
	 * @param UNS_LeaveReservedConfig_ID
	 * @param trxName
	 */
	public MUNSLeaveReservedConfig(Properties ctx,
			int UNS_LeaveReservedConfig_ID, String trxName) {
		super(ctx, UNS_LeaveReservedConfig_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSLeaveReservedConfig(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 * @param ctx
	 * @param hrdLevel
	 * @param trxName
	 * @return {@link MUNSLeaveReservedConfig}
	 */
	public static MUNSLeaveReservedConfig get(Properties ctx, int positionCategory, String nationality,
			String leaveType, int AD_Org_ID, boolean checkAllOrg, String trxName)
	{
		MUNSLeaveReservedConfig config = null;
		if (nationality==null)
			nationality="WNI";
		config = Query.get(
				ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, COLUMNNAME_C_JobCategory_ID + 
				"=? AND IsActive='Y' AND Nationality=? AND LeaveType = ? AND AD_Org_ID = ?", trxName)
				.setParameters(positionCategory,nationality,leaveType, AD_Org_ID).firstOnly();
		
		if(config == null && checkAllOrg)
			config = MUNSLeaveReservedConfig.get(ctx, positionCategory, nationality, leaveType, 0, false, trxName);
		
		return config;
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT COUNT(*) FROM UNS_LeaveReservedConfig"
				+ " WHERE C_JobCategory_ID = ? AND IsActive = 'Y'"
				+ " AND Nationality=? AND LeaveType = ? AND UNS_LeaveReservedConfig_ID <> ?"
				+ " AND AD_Org_ID = ?";
		int count = DB.getSQLValue(get_TrxName(), sql,
						new Object[]{getC_JobCategory_ID(), getNationality(), getLeaveType(), get_ID(), getAD_Org_ID()});
		
		if(count > 0)
		{
			log.saveError("Error", "Duplicate configuration.");
			return false;
		}
		
		return true;
	}
	
	public static MUNSLeaveReservedConfig[] get(Properties ctx, String leaveType, String trxName)
	{
		List<MUNSLeaveReservedConfig> list =  Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name,
				COLUMNNAME_LeaveType + "=?", trxName).setOnlyActiveRecords(true).setParameters(leaveType)
				.list();
		
		return list.toArray(new MUNSLeaveReservedConfig[list.size()]);
	}
}