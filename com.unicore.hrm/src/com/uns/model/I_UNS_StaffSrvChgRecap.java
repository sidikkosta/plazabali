/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_StaffSrvChgRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_StaffSrvChgRecap 
{

    /** TableName=UNS_StaffSrvChgRecap */
    public static final String Table_Name = "UNS_StaffSrvChgRecap";

    /** AD_Table_ID=1000476 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Point */
    public static final String COLUMNNAME_Point = "Point";

	/** Set Point	  */
	public void setPoint (BigDecimal Point);

	/** Get Point	  */
	public BigDecimal getPoint();

    /** Column name PositionType */
    public static final String COLUMNNAME_PositionType = "PositionType";

	/** Set Position Type	  */
	public void setPositionType (String PositionType);

	/** Get Position Type	  */
	public String getPositionType();

    /** Column name ServiceChargeAmt */
    public static final String COLUMNNAME_ServiceChargeAmt = "ServiceChargeAmt";

	/** Set Service Charge Amount	  */
	public void setServiceChargeAmt (BigDecimal ServiceChargeAmt);

	/** Get Service Charge Amount	  */
	public BigDecimal getServiceChargeAmt();

    /** Column name Shop_ID */
    public static final String COLUMNNAME_Shop_ID = "Shop_ID";

	/** Set Shop.
	  * Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID);

	/** Get Shop.
	  * Identifies a Shop
	  */
	public int getShop_ID();

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException;

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name UNS_ShopRecap_ID */
    public static final String COLUMNNAME_UNS_ShopRecap_ID = "UNS_ShopRecap_ID";

	/** Set Shop Sales Recapitulation	  */
	public void setUNS_ShopRecap_ID (int UNS_ShopRecap_ID);

	/** Get Shop Sales Recapitulation	  */
	public int getUNS_ShopRecap_ID();

	public com.uns.model.I_UNS_ShopRecap getUNS_ShopRecap() throws RuntimeException;

    /** Column name UNS_StaffSrvChgRecap_ID */
    public static final String COLUMNNAME_UNS_StaffSrvChgRecap_ID = "UNS_StaffSrvChgRecap_ID";

	/** Set Staff Service Charge Recap	  */
	public void setUNS_StaffSrvChgRecap_ID (int UNS_StaffSrvChgRecap_ID);

	/** Get Staff Service Charge Recap	  */
	public int getUNS_StaffSrvChgRecap_ID();

    /** Column name UNS_StaffSrvChgRecap_UU */
    public static final String COLUMNNAME_UNS_StaffSrvChgRecap_UU = "UNS_StaffSrvChgRecap_UU";

	/** Set Staff Service Charge Recap UU	  */
	public void setUNS_StaffSrvChgRecap_UU (String UNS_StaffSrvChgRecap_UU);

	/** Get Staff Service Charge Recap UU	  */
	public String getUNS_StaffSrvChgRecap_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
