/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PayrollPayBenefit
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PayrollPayBenefit extends PO implements I_UNS_PayrollPayBenefit, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180601L;

    /** Standard Constructor */
    public X_UNS_PayrollPayBenefit (Properties ctx, int UNS_PayrollPayBenefit_ID, String trxName)
    {
      super (ctx, UNS_PayrollPayBenefit_ID, trxName);
      /** if (UNS_PayrollPayBenefit_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_PayrollPayBenefit (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PayrollPayBenefit[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.uns.model.I_UNS_Payroll_Employee getUNS_Payroll_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Payroll_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Payroll_Employee.Table_Name)
			.getPO(getUNS_Payroll_Employee_ID(), get_TrxName());	}

	/** Set Payroll Employee.
		@param UNS_Payroll_Employee_ID Payroll Employee	  */
	public void setUNS_Payroll_Employee_ID (int UNS_Payroll_Employee_ID)
	{
		if (UNS_Payroll_Employee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Payroll_Employee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Payroll_Employee_ID, Integer.valueOf(UNS_Payroll_Employee_ID));
	}

	/** Get Payroll Employee.
		@return Payroll Employee	  */
	public int getUNS_Payroll_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Payroll_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Payroll Benefit.
		@param UNS_PayrollPayBenefit_ID Payroll Benefit	  */
	public void setUNS_PayrollPayBenefit_ID (int UNS_PayrollPayBenefit_ID)
	{
		if (UNS_PayrollPayBenefit_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayBenefit_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayBenefit_ID, Integer.valueOf(UNS_PayrollPayBenefit_ID));
	}

	/** Get Payroll Benefit.
		@return Payroll Benefit	  */
	public int getUNS_PayrollPayBenefit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollPayBenefit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PayrollPayBenefit_UU.
		@param UNS_PayrollPayBenefit_UU UNS_PayrollPayBenefit_UU	  */
	public void setUNS_PayrollPayBenefit_UU (String UNS_PayrollPayBenefit_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayBenefit_UU, UNS_PayrollPayBenefit_UU);
	}

	/** Get UNS_PayrollPayBenefit_UU.
		@return UNS_PayrollPayBenefit_UU	  */
	public String getUNS_PayrollPayBenefit_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PayrollPayBenefit_UU);
	}

	public com.uns.model.I_UNS_PayrollPayment_BA getUNS_PayrollPayment_BA() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PayrollPayment_BA)MTable.get(getCtx(), com.uns.model.I_UNS_PayrollPayment_BA.Table_Name)
			.getPO(getUNS_PayrollPayment_BA_ID(), get_TrxName());	}

	/** Set Payroll Bank Account.
		@param UNS_PayrollPayment_BA_ID Payroll Bank Account	  */
	public void setUNS_PayrollPayment_BA_ID (int UNS_PayrollPayment_BA_ID)
	{
		if (UNS_PayrollPayment_BA_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_BA_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_BA_ID, Integer.valueOf(UNS_PayrollPayment_BA_ID));
	}

	/** Get Payroll Bank Account.
		@return Payroll Bank Account	  */
	public int getUNS_PayrollPayment_BA_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollPayment_BA_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}