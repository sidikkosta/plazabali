/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MPeriod;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSOTGroupConfirm extends X_UNS_OTGroupConfirm implements DocOptions, DocAction{

	String m_processMsg = null;
	private boolean m_justPrepared = false;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8408037096789989823L;

	/**
	 * @param ctx
	 * @param UNS_OTGroupConfirm_ID
	 * @param trxName
	 */
	public MUNSOTGroupConfirm(Properties ctx, int UNS_OTGroupConfirm_ID,
			String trxName) {
		super(ctx, UNS_OTGroupConfirm_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSOTGroupConfirm(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSOTGroupConfirm(MUNSOTGroupRequest request)
	{
		this(request.getCtx(), 0, request.get_TrxName());
		setAD_Org_ID(request.getAD_Org_ID());
		setDateDoc(request.getDateDoc());
		setUNS_OTGroupRequest_ID(request.get_ID());
		setC_Period_ID(request.getC_Period_ID());
	}
	
	public static MUNSOTGroupConfirm getCreate(MUNSOTGroupRequest request)
	{
		MUNSOTGroupConfirm confirm = Query.get(request.getCtx(),
				UNSHRMModelFactory.EXTENSION_ID, Table_Name, COLUMNNAME_UNS_OTGroupRequest_ID + "=?",
					request.get_TrxName()).setParameters(request.get_ID()).firstOnly();
		
		if(confirm != null)
			return confirm;
		
		confirm = new MUNSOTGroupConfirm(request);
		confirm.saveEx();
		
		MUNSPayrollConfiguration config = MUNSPayrollConfiguration.get(request.getCtx(),
				(MPeriod) request.getC_Period(), request.getAD_Org_ID(), request.get_TrxName(), true);
		if(!config.isNeedConfirmOnOT())
		{
			confirm.setDocStatus("CO");
			confirm.setDocAction("CL");
			confirm.setProcessed(true);
			confirm.saveEx();
			request.setForce(true);
		}
		
		return confirm;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(newRecord && getUNS_OTGroupRequest_ID() > 0)
		{
			if(!generateLines())
			{
				log.saveError("Error", CLogger.retrieveErrorString("Failed when trying create Activity OR Resource/Employee."));
				return false;
			}
		}
		
		return true;
	}
	
	private boolean generateLines()
	{
		MUNSOTGroupRequest req = new MUNSOTGroupRequest(getCtx(), getUNS_OTGroupRequest_ID(), get_TrxName());
		MUNSOTRequest[] activitys = req.getActivity(false);
		for(int x=0;x<activitys.length;x++)
		{
			MUNSOTConfirmation confirm = new MUNSOTConfirmation (activitys[x]);
			confirm.setUNS_OTGroupConfirm_ID(get_ID());
			confirm.setDateDoc(activitys[x].getRequestDate());
			confirm.setStartTime(activitys[x].getStartTime());
			confirm.setEndTime(activitys[x].getEndTime());
			confirm.setConfirmedHours(activitys[x].getRequestedHours());
			confirm.setConfirmedBreakTime(activitys[x].getBreakTime());
			confirm.setUNS_OTLine_ID(activitys[x].getUNS_OTLine_ID());
			confirm.setUNS_OTRequest_ID(activitys[x].get_ID());
			if(!confirm.save())
				return false;
		}
		
		String sql = "UPDATE UNS_OTLine SET UNS_OTGroupConfirm_ID = ? WHERE"
				+ " UNS_OTGroupRequest_ID = ?";
		if(DB.executeUpdate(sql, new Object[]{get_ID(), req.get_ID()}, false, get_TrxName()) <= 0)
			return false;
		
		return true;
	}
	
	public boolean processIt(String action) throws Exception 
	{
		doLog(Level.INFO, "Processing Document.");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() 
	{
		doLog(Level.INFO, "Unlock Document");
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		doLog(Level.INFO, "Invalidate Document");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		doLog(Level.INFO, "Prepare Document");
		
		//run UNSHRMValidator
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Complete);
		return DOCSTATUS_InProgress;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		doLog(Level.INFO, "Approve Document");
		setProcessed(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() 
	{
		doLog(Level.INFO, "Reject Document");
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		doLog(Level.INFO, "Complete Document");
		
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
			return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		try {
			m_processMsg = processRequest(ACTION_Complete);
		} catch (Exception e) {
			m_processMsg = e.getMessage();
		}
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
		{
			return DOCSTATUS_Invalid;
		}
		
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		doLog(Level.INFO, "Void Document");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
		{
			return false;
		}
		
		try {
			m_processMsg = processRequest(ACTION_Void);
		} catch (Exception e) {
			m_processMsg = e.getMessage();
		}
		if (m_processMsg != null)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
		{
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt() 
	{
		doLog(Level.INFO, "Close Document");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
		{
			return false;
		}
		
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
		{
			return false;
		}
		
		setDocAction(DOCACTION_None);
		setDocStatus(DOCSTATUS_Closed);
		setProcessed(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		doLog(Level.INFO, "Reverse Correct-It");
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() 
	{
		doLog(Level.INFO, "Reverse AccrualIt");
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt() 
	{
		m_processMsg = "Disallowed action Reactivate-It";
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder("Over Time Request No ")
		.append(getDocumentNo());
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() 
	{
		return getSummary();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() 
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() 
	{
		return null;
	}
	
	private void doLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
		{
			return;
		}
		
		log.log(level, msg);
	}
	
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (docStatus.equals(DocAction.STATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_Void;
		}
		
		return index;
	}
	
	private String processRequest (String action) throws Exception
	{
		//TODO setForced to request before run action.
		MUNSOTGroupRequest request = new MUNSOTGroupRequest(getCtx(), getUNS_OTGroupRequest_ID(), get_TrxName());
		
		if (request.getDocStatus().equals(action)) {
			// do nothing.
			return null;
		}
		
		if (DOCACTION_Complete.equals(action))
		{
			MUNSOTConfirmation[] activityConfirm = getActivityConfirm();
			String sql = "UPDATE UNS_OTRequest SET ConfirmedHours = ?, ConfirmedBreakTime = ?"
					+ " WHERE UNS_OTRequest_ID = ?";
			for(int i=0;i<activityConfirm.length;i++)
			{
				if(activityConfirm[i].getUNS_OTRequest_ID() > 0)
				{
					DB.executeUpdate(sql, new Object[]{
							activityConfirm[i].getConfirmedHours(), activityConfirm[i].getConfirmedBreakTime(),
							activityConfirm[i].getUNS_OTRequest_ID()
					}, false, get_TrxName());
				}
			}
		}
		
		request.setForce(true);
		boolean ok = request.processIt(action);
		
		if (!ok)
		{
			return request.getProcessMsg();
		}
		
		request.saveEx();
		
		return null;
	}
	
	public MUNSOTConfirmation[] getActivityConfirm()
	{
		List<MUNSOTConfirmation> list = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSOTConfirmation.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSOTConfirmation[list.size()]);
	}
	
	public boolean isComplete ()
	{
		boolean complete = getDocStatus().equals(DOCSTATUS_Completed)
				|| getDocStatus().equals(DOCSTATUS_Voided)
				|| getDocStatus().equals(DOCSTATUS_Closed)
				|| getDocStatus().equals(DOCSTATUS_Reversed);
		
		return complete;
	}
}