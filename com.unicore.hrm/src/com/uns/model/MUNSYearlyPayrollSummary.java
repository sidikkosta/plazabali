/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author menjangan
 *
 */
public class MUNSYearlyPayrollSummary extends X_UNS_YearlyPayrollSummary {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6986990977786850098L;

	/**
	 * @param ctx
	 * @param UNS_YearlyPayrollSummary_ID
	 * @param trxName
	 */
	public MUNSYearlyPayrollSummary(Properties ctx,
			int UNS_YearlyPayrollSummary_ID, String trxName) {
		super(ctx, UNS_YearlyPayrollSummary_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSYearlyPayrollSummary(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	
	public static MUNSYearlyPayrollSummary get (String trxName, int employeeID, int yearID) {
		return Query.get(Env.getCtx(), UNSHRMModelFactory.EXTENSION_ID, Table_Name, 
				"UNS_Employee_ID = ? AND C_Year_ID = ?", trxName).
				setParameters(employeeID, yearID).setOrderBy("IsActive Desc").first();
	}
	
	public static boolean updateRecord (MUNSPayrollEmployee payroll, boolean isVoidProcess) {
		MUNSYearlyPayrollSummary summary = get(payroll.get_TrxName(), 
				payroll.getUNS_Employee_ID(), payroll.getC_Period().getC_Year_ID());
		BigDecimal multiplicand = Env.ONE;
		if (isVoidProcess)
			multiplicand = multiplicand.negate();
		
		if (summary == null)
			summary = new MUNSYearlyPayrollSummary(payroll);
		BigDecimal AJHT = summary.getA_JHT();
		BigDecimal AJK = summary.getA_JK();
		BigDecimal AJKK = summary.getA_JKK();
		BigDecimal AJP = summary.getA_JP();
		BigDecimal AJPK = summary.getA_JPK();
		BigDecimal AL1 = summary.getA_L1();
		BigDecimal AL2 = summary.getA_L2();
		BigDecimal AL3 = summary.getA_L3();
		BigDecimal AL1R = summary.getA_L1R();
		BigDecimal AL2R = summary.getA_L2R();
		BigDecimal AL3R = summary.getA_L3R();
		BigDecimal firstHoursOT = summary.getA_LemburJamPertama();
		BigDecimal nextHoursOT = summary.getA_LemburJamBerikutnya();
		BigDecimal baseSalary = summary.getGPokok();
		BigDecimal PJHT = summary.getP_JHT();
		BigDecimal PJK = summary.getP_JK();
		BigDecimal PJKK = summary.getP_JKK();
		BigDecimal PJP = summary.getP_JP();
		BigDecimal PJPK = summary.getP_JPK();
		BigDecimal PPH21 = summary.getPPH21();
		BigDecimal takeHomePay = summary.getTakeHomePay();
		
		//Benefit inside or outside payroll and pph or not pph
		BigDecimal tBenefitPPhCompInsidePayroll = summary.getTPeriodicBenefitPPh21Comp();
		BigDecimal tBenefitPPhCompOutsidePayroll = summary.getTNonPeriodicBenefitPPh21Comp();
		BigDecimal tBenefitInsidePayroll = summary.getTPeriodicBenefit();
		BigDecimal tBenefitOutsidePayroll = summary.getTNonPeriodicBenefit();

		//Deduction inside or outside payroll and pph or not pph
		BigDecimal tDeducPPhCompInsidePayroll = summary.getTPeriodicDeducPPh21Comp();
		BigDecimal tDeducPPhCompOutsidePayroll = summary.getTNonPeriodicDeducPPh21Comp();
		BigDecimal tDeducInsidePayroll = summary.getTPeriodicDeduction();
		BigDecimal tDeducOutsidePayroll = summary.getTNonPeriodicDeduction();
		
		BigDecimal correctionpph21 = payroll.getCorrectionAmtPPh21();
		BigDecimal correctionpph21NonPeriodic = payroll.getCorrectionAmtPPh21NonPeriodic();
		BigDecimal correction = payroll.getCorrectionAmt();
		BigDecimal correctionNonPeriodic = payroll.getCorrectionAmtNonPeriodic();
		
		if (correctionpph21.signum() == 1)
			tBenefitPPhCompInsidePayroll = tBenefitPPhCompInsidePayroll.add(correctionpph21.multiply(multiplicand));
		else
			tDeducPPhCompInsidePayroll = tDeducPPhCompInsidePayroll.add(correctionpph21.abs().multiply(multiplicand));
		if (correction.signum() == 1)
			tBenefitInsidePayroll = tBenefitInsidePayroll.add(correction.multiply(multiplicand));
		else
			tDeducInsidePayroll = tDeducInsidePayroll.add(correction.abs().multiply(multiplicand));
		
		if(correctionpph21NonPeriodic.signum() == 1)
			tBenefitPPhCompOutsidePayroll = tBenefitPPhCompOutsidePayroll.add(correctionpph21NonPeriodic.multiply(multiplicand));
		else
			tDeducPPhCompOutsidePayroll = tDeducPPhCompOutsidePayroll.add(correctionpph21NonPeriodic.abs().multiply(multiplicand));
		if(correctionNonPeriodic.signum() == 1)
			tBenefitOutsidePayroll = tBenefitOutsidePayroll.add(correctionNonPeriodic.multiply(multiplicand));
		else
			tDeducOutsidePayroll = tDeducOutsidePayroll.add(correctionNonPeriodic.multiply(multiplicand));
		
		AJHT = AJHT.add(payroll.getA_JHT().multiply(multiplicand));
		AJK = AJK.add(payroll.getA_JK().multiply(multiplicand));
		AJKK = AJKK.add(payroll.getA_JKK().multiply(multiplicand));
		AJP = AJP.add(payroll.getA_JP().multiply(multiplicand));
		AJPK = AJPK.add(payroll.getA_JPK().multiply(multiplicand));
		AL1 = AL1.add(payroll.getA_L1().multiply(multiplicand));
		AL2 = AL2.add(payroll.getA_L2().multiply(multiplicand));
		AL3 = AL3.add(payroll.getA_L3().multiply(multiplicand));
		
		AL1R = AL1R.add(payroll.getA_L1R().multiply(multiplicand));
		AL2R = AL2R.add(payroll.getA_L2R().multiply(multiplicand));
		AL3R = AL3R.add(payroll.getA_L3R().multiply(multiplicand));
		
		firstHoursOT = firstHoursOT.add(payroll.getA_LemburJamPertama().multiply(multiplicand));
		nextHoursOT = nextHoursOT.add(payroll.getA_LemburJamBerikutnya().multiply(multiplicand));
		baseSalary = baseSalary.add(payroll.getGPokok().multiply(multiplicand));
		PJHT = PJHT.add(payroll.getP_JHT().multiply(multiplicand));
		PJK = PJK.add(payroll.getP_JK().multiply(multiplicand));
		PJKK = PJKK.add(payroll.getP_JKK().multiply(multiplicand));
		PJP = PJP.add(payroll.getP_JP().multiply(multiplicand));
		PJPK = PJPK.add(payroll.getP_JPK().multiply(multiplicand));
		PPH21 = PPH21.add(payroll.getSourcePPhAmt().multiply(multiplicand));
		takeHomePay = takeHomePay.add(payroll.getTakeHomePay().multiply(multiplicand));
		
		tBenefitPPhCompInsidePayroll = tBenefitPPhCompInsidePayroll.add(payroll.getPeriodicBenefitPPh21Amt().multiply(multiplicand));
		tBenefitPPhCompOutsidePayroll = tBenefitPPhCompOutsidePayroll.add(payroll.getNonPeriodicBenefitPPh21Amt().multiply(multiplicand));
		tBenefitInsidePayroll = tBenefitInsidePayroll.add(payroll.getPeriodicBenefitAmt().multiply(multiplicand));
		tBenefitOutsidePayroll = tBenefitOutsidePayroll.add(payroll.getNonPeriodicBenefitAmt().multiply(multiplicand));
		tDeducPPhCompInsidePayroll = tDeducPPhCompInsidePayroll.add(payroll.getPeriodicDeducPPh21Amt().multiply(multiplicand));
		tDeducPPhCompOutsidePayroll = tDeducPPhCompOutsidePayroll.add(payroll.getNonPeriodicDeducPPh21Amt().multiply(multiplicand));
		tDeducInsidePayroll = tDeducInsidePayroll.add(payroll.getPeriodicDeducAmt().multiply(multiplicand));
		tDeducOutsidePayroll = tDeducOutsidePayroll.add(payroll.getNonPeriodicDeducAmt().multiply(multiplicand));
		
		summary.setA_JHT(AJHT);
		summary.setA_JK(AJK);
		summary.setA_JKK(AJKK);
		summary.setA_JP(AJP);
		summary.setA_JPK(AJPK);
		summary.setA_L1(AL1);
		summary.setA_L2(AL2);
		summary.setA_L3(AL3);
		summary.setA_L1R(AL1R);
		summary.setA_L2R(AL2R);
		summary.setA_L3R(AL3R);
		summary.setA_LemburJamPertama(firstHoursOT);
		summary.setA_LemburJamBerikutnya(nextHoursOT);
		summary.setGPokok(baseSalary);
		summary.setP_JHT(PJHT);
		summary.setP_JK(PJK);
		summary.setP_JKK(PJKK);
		summary.setP_JP(PJP);
		summary.setP_JPK(PJPK);
		summary.setPPH21(PPH21);
		summary.setTakeHomePay(takeHomePay);

		//set TBenefit And Deduc
		summary.setTPeriodicBenefitPPh21Comp(tBenefitPPhCompInsidePayroll);
		summary.setTNonPeriodicBenefitPPh21Comp(tBenefitPPhCompOutsidePayroll);
		summary.setTPeriodicBenefit(tBenefitInsidePayroll);
		summary.setTNonPeriodicBenefit(tBenefitOutsidePayroll);
		
		summary.setTPeriodicDeducPPh21Comp(tDeducPPhCompInsidePayroll);
		summary.setTNonPeriodicDeducPPh21Comp(tDeducPPhCompOutsidePayroll);
		summary.setTPeriodicDeduction(tDeducInsidePayroll);
		summary.setTNonPeriodicDeduction(tDeducOutsidePayroll);
		return summary.save();
	}
	
	public BigDecimal getPPHBrutoAmt ()
	{
		BigDecimal baseAmt = getGPokok();
		BigDecimal addAmt = // getA_JP().add(getA_JK()).add(getA_JKK()).add(getA_JPK()).
				getA_JK().add(getA_JKK()).add(getA_JPK()).
				add(getA_LemburJamPertama()).add(getA_LemburJamBerikutnya()).add(getA_L1()).
				add(getA_L2()).add(getA_L3()).add(getTPeriodicBenefitPPh21Comp())
				.add(getTNonPeriodicBenefitPPh21Comp()).add(getA_L1R()).
				add(getA_L2R()).add(getA_L3R());
		baseAmt = baseAmt.add(addAmt);
		return baseAmt;
	}
	
	public BigDecimal getPPHDeducAmt ()
	{
		BigDecimal pphDeducAmt = getP_JP().add(getP_JK()).add(getP_JKK()).add(getP_JHT()).
				add(getTPeriodicDeducPPh21Comp()).add(getTNonPeriodicDeducPPh21Comp());		
		return pphDeducAmt;
	}
	
	public BigDecimal getPPh21NetAmt () {
		return getPPHBrutoAmt().subtract(getPPHDeducAmt());
	}
	
	public MUNSYearlyPayrollSummary (MUNSPayrollEmployee payroll)
	{
		this (payroll.getCtx(), 0, payroll.get_TrxName());
		setClientOrg(payroll);
		setUNS_Employee_ID(payroll.getUNS_Employee_ID());
		setC_Year_ID(payroll.getC_Period().getC_Year_ID());
		setPeriodStart_ID(payroll.getC_Period_ID());
		setStartPeriodNo(payroll.getC_Period().getPeriodNo());
	}
}
