/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_SubregionRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_SubregionRecap 
{

    /** TableName=UNS_SubregionRecap */
    public static final String Table_Name = "UNS_SubregionRecap";

    /** AD_Table_ID=1000452 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CalculateServiceCharge */
    public static final String COLUMNNAME_CalculateServiceCharge = "CalculateServiceCharge";

	/** Set Calculate Service Charges.
	  * Calculate & Load all service charges of (a) shop in a subregion
	  */
	public void setCalculateServiceCharge (String CalculateServiceCharge);

	/** Get Calculate Service Charges.
	  * Calculate & Load all service charges of (a) shop in a subregion
	  */
	public String getCalculateServiceCharge();

    /** Column name CalculationClass */
    public static final String COLUMNNAME_CalculationClass = "CalculationClass";

	/** Set Calculation Class.
	  * Java Class for calculation, implementing Interface Measure
	  */
	public void setCalculationClass (String CalculationClass);

	/** Get Calculation Class.
	  * Java Class for calculation, implementing Interface Measure
	  */
	public String getCalculationClass();

    /** Column name C_Period_ID */
    public static final String COLUMNNAME_C_Period_ID = "C_Period_ID";

	/** Set Period.
	  * Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID);

	/** Get Period.
	  * Period of the Calendar
	  */
	public int getC_Period_ID();

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EmployeePortion */
    public static final String COLUMNNAME_EmployeePortion = "EmployeePortion";

	/** Set Employee Portion	  */
	public void setEmployeePortion (int EmployeePortion);

	/** Get Employee Portion	  */
	public int getEmployeePortion();

    /** Column name EndDate */
    public static final String COLUMNNAME_EndDate = "EndDate";

	/** Set End Date.
	  * Last effective date (inclusive)
	  */
	public void setEndDate (Timestamp EndDate);

	/** Get End Date.
	  * Last effective date (inclusive)
	  */
	public Timestamp getEndDate();

    /** Column name File_Directory */
    public static final String COLUMNNAME_File_Directory = "File_Directory";

	/** Set File_Directory	  */
	public void setFile_Directory (String File_Directory);

	/** Get File_Directory	  */
	public String getFile_Directory();

    /** Column name ImportAdditionalStaff */
    public static final String COLUMNNAME_ImportAdditionalStaff = "ImportAdditionalStaff";

	/** Set Import Additional Staff	  */
	public void setImportAdditionalStaff (String ImportAdditionalStaff);

	/** Get Import Additional Staff	  */
	public String getImportAdditionalStaff();

    /** Column name ImportMonthlyShopSchedule */
    public static final String COLUMNNAME_ImportMonthlyShopSchedule = "ImportMonthlyShopSchedule";

	/** Set Import Monthly Shop Schedule	  */
	public void setImportMonthlyShopSchedule (String ImportMonthlyShopSchedule);

	/** Get Import Monthly Shop Schedule	  */
	public String getImportMonthlyShopSchedule();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name LoadShopStaff */
    public static final String COLUMNNAME_LoadShopStaff = "LoadShopStaff";

	/** Set Load Shop Staff	  */
	public void setLoadShopStaff (String LoadShopStaff);

	/** Get Load Shop Staff	  */
	public String getLoadShopStaff();

    /** Column name MinSubSalesTargetAmt */
    public static final String COLUMNNAME_MinSubSalesTargetAmt = "MinSubSalesTargetAmt";

	/** Set Min. Subregion Sales Target.
	  * Minimum target amount for shop
	  */
	public void setMinSubSalesTargetAmt (BigDecimal MinSubSalesTargetAmt);

	/** Get Min. Subregion Sales Target.
	  * Minimum target amount for shop
	  */
	public BigDecimal getMinSubSalesTargetAmt();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name NetSalesIncentiveAmt */
    public static final String COLUMNNAME_NetSalesIncentiveAmt = "NetSalesIncentiveAmt";

	/** Set Net Sales Incentive.
	  * Amount in a defined currency
	  */
	public void setNetSalesIncentiveAmt (BigDecimal NetSalesIncentiveAmt);

	/** Get Net Sales Incentive.
	  * Amount in a defined currency
	  */
	public BigDecimal getNetSalesIncentiveAmt();

    /** Column name NetSalesRealizationAmt */
    public static final String COLUMNNAME_NetSalesRealizationAmt = "NetSalesRealizationAmt";

	/** Set Net Sales Realization.
	  * Total realized net sales within 1 month from all stores
	  */
	public void setNetSalesRealizationAmt (BigDecimal NetSalesRealizationAmt);

	/** Get Net Sales Realization.
	  * Total realized net sales within 1 month from all stores
	  */
	public BigDecimal getNetSalesRealizationAmt();

    /** Column name NetSalesTarget */
    public static final String COLUMNNAME_NetSalesTarget = "NetSalesTarget";

	/** Set Net Sales Target.
	  * Net sales target from all stores in a certain subregion
	  */
	public void setNetSalesTarget (BigDecimal NetSalesTarget);

	/** Get Net Sales Target.
	  * Net sales target from all stores in a certain subregion
	  */
	public BigDecimal getNetSalesTarget();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ShopType */
    public static final String COLUMNNAME_ShopType = "ShopType";

	/** Set Shop Type	  */
	public void setShopType (String ShopType);

	/** Get Shop Type	  */
	public String getShopType();

    /** Column name StartDate */
    public static final String COLUMNNAME_StartDate = "StartDate";

	/** Set Start Date.
	  * First effective day (inclusive)
	  */
	public void setStartDate (Timestamp StartDate);

	/** Get Start Date.
	  * First effective day (inclusive)
	  */
	public Timestamp getStartDate();

    /** Column name Subregion2_ID */
    public static final String COLUMNNAME_Subregion2_ID = "Subregion2_ID";

	/** Set Subregion 2.
	  * Subregion 2
	  */
	public void setSubregion2_ID (int Subregion2_ID);

	/** Get Subregion 2.
	  * Subregion 2
	  */
	public int getSubregion2_ID();

	public org.compiere.model.I_C_BP_Group getSubregion2() throws RuntimeException;

    /** Column name Subregion_ID */
    public static final String COLUMNNAME_Subregion_ID = "Subregion_ID";

	/** Set Subregion.
	  * Subregion
	  */
	public void setSubregion_ID (int Subregion_ID);

	/** Get Subregion.
	  * Subregion
	  */
	public int getSubregion_ID();

	public org.compiere.model.I_C_BP_Group getSubregion() throws RuntimeException;

    /** Column name TotalServiceChargeAmt */
    public static final String COLUMNNAME_TotalServiceChargeAmt = "TotalServiceChargeAmt";

	/** Set Total Service Charge	  */
	public void setTotalServiceChargeAmt (BigDecimal TotalServiceChargeAmt);

	/** Get Total Service Charge	  */
	public BigDecimal getTotalServiceChargeAmt();

    /** Column name UNS_SubregionRecap_ID */
    public static final String COLUMNNAME_UNS_SubregionRecap_ID = "UNS_SubregionRecap_ID";

	/** Set Subregion Recapitulation	  */
	public void setUNS_SubregionRecap_ID (int UNS_SubregionRecap_ID);

	/** Get Subregion Recapitulation	  */
	public int getUNS_SubregionRecap_ID();

    /** Column name UNS_SubregionRecap_UU */
    public static final String COLUMNNAME_UNS_SubregionRecap_UU = "UNS_SubregionRecap_UU";

	/** Set UNS_SubregionRecap_UU	  */
	public void setUNS_SubregionRecap_UU (String UNS_SubregionRecap_UU);

	/** Get UNS_SubregionRecap_UU	  */
	public String getUNS_SubregionRecap_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
