/**
 * 
 */
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.compiere.acct.Doc;
import org.compiere.acct.DocLine;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.model.MPeriod;

import com.uns.model.I_UNS_Contract_Recommendation;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollCostBenefit;
import com.uns.model.MUNSPayrollEmployee;
import com.uns.util.UNSApps;

/**
 * @author menjangan
 * Unused. post moved to {@link DOC_UNSMonthlyPayroll}
 */
public class Doc_UNSPayrollEmployee extends Doc {

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_UNSPayrollEmployee(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
	}
	
	public Doc_UNSPayrollEmployee(MAcctSchema as,ResultSet rs,String trxName)
	{
		super(as,MUNSPayrollEmployee.class,rs,null,trxName);
	}

	
	private MUNSPayrollEmployee m_PayEmployee = null;
	
	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails() {
		m_PayEmployee = (MUNSPayrollEmployee)getPO();
		setDateDoc(m_PayEmployee.getUpdated());
		setDateAcct(m_PayEmployee.getUpdated());
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() {
		return BigDecimal.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) {
		// TODO Auto-generated method stub
		ArrayList<Fact> facts = new ArrayList<Fact>();
		//not used
		if (m_PayEmployee.get_TableName().equals(MUNSPayrollEmployee.Table_Name))
			return facts;
		if ("VO".equals(m_PayEmployee.getDocStatus()))
			return facts;
		MUNSEmployee employee = (MUNSEmployee)m_PayEmployee.getUNS_Employee();
		
		MUNSPayrollConfiguration payConfig =
				MUNSPayrollConfiguration.get(
						getCtx(), (MPeriod)m_PayEmployee.getC_Period(), employee.getAD_Org_ID(), getTrxName(), true);
		
		
		/** Account */
		int BiayaGajiAcct = payConfig.getBiayaGajiBulananAcct_ID();
		int HutangGajiAcct = payConfig.getHutangGajiBulananAcct_ID();
		
		I_UNS_Contract_Recommendation contract = employee.getUNS_Contract_Recommendation();
		
		if(MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarian.equals(
				contract.getNextContractType())
				|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarianCV.equals(
						contract.getNextContractType()))
		{
			BiayaGajiAcct = payConfig.getBiayaGajiHarianAcct_ID();
			HutangGajiAcct = payConfig.getHutangGajiHarianAcct_ID();
		}
		
		int pph21Acct = payConfig.getPPH21_Acct_ID();
		int B_JHTAcct = payConfig.getB_JHTAcct_ID();
		int B_JPKAcct = payConfig.getB_JPKAcct_ID();
		int B_JKKAcct = payConfig.getB_JKKAcct_ID();
		int B_JKAcct = payConfig.getB_JKAcct_ID();
		int H_JHTAcct = payConfig.getH_JHT_Acct_ID();
		int H_JPKAcct = payConfig.getH_JPK_Acct_ID();
		int H_JKKAcct = payConfig.getH_JKK_Acct_ID();
		int H_JKAcct = payConfig.getH_JK_Acct_ID();
		int A_LemburAcct = payConfig.getA_Lembur_Acct_ID();
		int A_L1Acct = payConfig.getA_L1_Acct_ID();
		int A_L2Acct = payConfig.getA_L2_Acct_ID();
		int A_L3Acct = payConfig.getA_L3_Acct_ID();
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		setC_Currency_ID (as.getC_Currency_ID());
		FactLine dr = null;
		FactLine cr = null;
		
		BigDecimal biayaGaji = m_PayEmployee.getGPokok();
		BigDecimal hutangGaji = m_PayEmployee.getPayableBruto();
		
		MAccount accountDebit = null;
		MAccount accountCredit = null;
		if (biayaGaji != null && biayaGaji.signum() != 0)
		{
			DocLine dlBGaji = new DocLine(m_PayEmployee, this);
			dlBGaji.setAmount(biayaGaji);
			accountDebit =  MAccount.get(getCtx(), BiayaGajiAcct);
			dr = fact.createLine(dlBGaji, accountDebit,
					as.getC_Currency_ID(), dlBGaji.getAmtSource(), null);
			//
			if (dr == null)
			{
				p_Error = "DR not created: " + dlBGaji;
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());	
		}
		

		if (m_PayEmployee.isPPH21PaidByCompany() && m_PayEmployee.getPPH21().signum() == 1)
		{
			MAccount biayaPPh21Acct = MCharge.getAccount(
					UNSApps.getRefAsInt(UNSApps.CHRG_BiayaPPH21), as);
			DocLine  dlBiayaPPh21 = new DocLine(m_PayEmployee, this);
			dlBiayaPPh21.setAmount(m_PayEmployee.getPPH21());
			dr = fact.createLine(dlBiayaPPh21, biayaPPh21Acct,
					as.getC_Currency_ID(), dlBiayaPPh21.getAmtSource(), null);
				//
			if (dr == null)
			{
				p_Error = "DR not created: " + dlBiayaPPh21;
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
			
		if (hutangGaji != null && hutangGaji.signum() != 0)
		{
			DocLine dlHGaji = new DocLine(m_PayEmployee, this);
			dlHGaji.setAmount(hutangGaji);
			accountCredit = MAccount.get(getCtx(), HutangGajiAcct);
					
			cr = fact.createLine(dlHGaji, accountCredit,
					as.getC_Currency_ID(), null, dlHGaji.getAmtSource());
				//
			if (cr == null)
			{
				p_Error = "DR not created: " + dlHGaji;
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());

		}
		
		if (m_PayEmployee.getA_L1().signum() != 0)
		{
			accountDebit = MAccount.get(getCtx(), A_L1Acct);	
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_L1(), null);
		//
			if (dr == null)
			{
				p_Error = "DR Biaya AL1 is not created ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getA_L2().signum() != 0)
		{
			accountDebit = MAccount.get(getCtx(), A_L2Acct);	
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_L2(), null);
			//
			if (dr == null)
			{
				p_Error = "DR Biaya AL2 is not created ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getA_L3().signum() != 0)
		{
			accountDebit = MAccount.get(getCtx(), A_L3Acct);	
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_L3(), null);
			//
			if (dr == null)
			{
				p_Error = "DR Biaya AL3 is not created ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		BigDecimal lemburAmt = m_PayEmployee.getA_LemburJamPertama().add(m_PayEmployee.getA_LemburJamBerikutnya());
		if (lemburAmt.signum() != 0)
		{
			accountDebit = MAccount.get(getCtx(), A_LemburAcct);	
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), lemburAmt, null);
			//
			if (dr == null)
			{
				p_Error = "DR Biaya Lembur is not created ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		BigDecimal hGajiDrAmt = m_PayEmployee.getPayrollDeduction();
		if (hGajiDrAmt.signum() != 0)
		{
			accountDebit = MAccount.get(getCtx(), HutangGajiAcct);
			
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), hGajiDrAmt, null);
			//
			if (dr == null)
			{
				p_Error = "DR Hutang Gaji is not created ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getP_JHT().signum() != 0)
		{
			accountCredit = MAccount.get(getCtx(), H_JHTAcct);
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null,  m_PayEmployee.getP_JHT());
					//
			if (cr == null)
			{
				p_Error = "CR JHT paid by employee not created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getA_JHT().signum() != 0)
		{
			accountDebit = MAccount.get(getCtx(), B_JHTAcct);
			
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_JHT(), null);
				//
			if (dr == null)
			{
				p_Error = "DR A JHT not created ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
			
			accountCredit = MAccount.get(getCtx(), H_JHTAcct);
			
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getA_JHT());
					//
			if (cr == null)
			{
				p_Error = "CR JHT not created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getP_JK().signum() != 0)
		{
			accountCredit = MAccount.get(getCtx(), H_JKAcct);
			
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getP_JK());
					//
			if (cr == null)
			{
				p_Error = "CR JK not created";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getA_JK().signum() != 0)
		{
			accountDebit = MAccount.get(
					getCtx(), B_JKAcct);
				
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_JK(), null);
			//
			if (dr == null)
			{
				p_Error = "DR A JK created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
				
				
			accountCredit = MAccount.get(
						getCtx(), H_JKAcct);
				
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getA_JK());
					//
			if (cr == null)
			{
				p_Error = "CR A JK not created";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}

		if (m_PayEmployee.getA_JKK().signum()!= 0)
		{
			accountDebit = MAccount.get(
					getCtx(), B_JKKAcct);
			
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_JKK(), null);
			//
			if (dr == null)
			{
				p_Error = "DR A JKK not created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
				
				
			accountCredit = MAccount.get(
						getCtx(), H_JKKAcct);
				
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getA_JKK());
					//
			if (cr == null)
			{
				p_Error = "DR A JKK not created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
				
		}
		
		if (m_PayEmployee.getP_JKK().signum() != 0)
		{
			accountCredit = MAccount.get(
					getCtx(), H_JKKAcct);
			
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(),null,  m_PayEmployee.getP_JKK());
				//
			if (cr == null)
			{
				p_Error = "DR P _JKK not created";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getP_JPK().signum() != 0)
		{
			accountCredit = MAccount.get(
					getCtx(), H_JPKAcct);
				
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getP_JPK());
					//
			if (cr == null)
			{
				p_Error = "DR P JPK not created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
				
		if (m_PayEmployee.getA_JPK().signum() != 0)
		{
			accountDebit = MAccount.get(
					getCtx(), B_JPKAcct);
			
			dr = fact.createLine(null, accountDebit,
					as.getC_Currency_ID(), m_PayEmployee.getA_JPK(), null);
				//
			if (dr == null)
			{
				p_Error = "DR A JPK not created: ";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			dr.setC_BPartner_ID(employee.getC_BPartner_ID());
			
			accountCredit = MAccount.get(
					getCtx(), H_JPKAcct);
					
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getA_JPK());
					//
			if (cr == null)
			{
				p_Error = "CR A JPK not created";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		if (m_PayEmployee.getPPH21().signum() != 0)
		{
			accountCredit = MAccount.get(
					getCtx(),pph21Acct);
				
			cr = fact.createLine(null, accountCredit,
					as.getC_Currency_ID(), null, m_PayEmployee.getPPH21());
				//
			if (cr == null)
			{
				p_Error = "CR PPH21 not created";
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
	
		MUNSPayrollCostBenefit[] costBens = m_PayEmployee.getCostBenefits(false);
		for (int i=0; i<costBens.length; i++) 
		{
			if (costBens[i].getAmount().signum() == 0)
				continue;
			DocLine dl = new DocLine(costBens[i], this);
			dl.setAmount(costBens[i].getAmount());
			MAccount dlAcct = MAccount.get(getCtx(), costBens[i].getCostBenefit_Acct());
			if (costBens[i].isBenefit()) {
				cr = fact.createLine(dl, dlAcct, getC_Currency_ID(), dl.getAmtSource(), null);
			} else
				cr = fact.createLine(dl, dlAcct, getC_Currency_ID(), null, dl.getAmtSource());
			if (cr == null)
			{
				p_Error = "DR not created: " + dl;
				log.log(Level.WARNING, p_Error);
				return null;
			}
			cr.setC_BPartner_ID(employee.getC_BPartner_ID());
		}
		
		facts.add(fact);
		return facts;
	}

}
