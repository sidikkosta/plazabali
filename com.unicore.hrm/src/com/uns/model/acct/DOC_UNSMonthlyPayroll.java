/**
 * 
 */
package com.uns.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.model.MPeriod;
import org.compiere.process.DocAction;
import org.compiere.util.Env;
import com.uns.model.I_UNS_Contract_Recommendation;
import com.uns.model.I_UNS_Employee;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSMonthlyPayrollEmployee;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollCostBenefit;
import com.uns.model.MUNSPayrollEmployee;
import com.uns.util.UNSApps;

/**
 * @author nurse
 *
 */
public class DOC_UNSMonthlyPayroll extends Doc 
{

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public DOC_UNSMonthlyPayroll(MAcctSchema as, Class<?> clazz, ResultSet rs,
			String defaultDocumentType, String trxName) 
	{
		super(as, clazz, rs, defaultDocumentType, trxName);
	}
	
	public DOC_UNSMonthlyPayroll (MAcctSchema as,ResultSet rs,String trxName)
	{
		this (as, MUNSMonthlyPayrollEmployee.class, rs, null, trxName);
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails() 
	{
		MUNSMonthlyPayrollEmployee monthly = (MUNSMonthlyPayrollEmployee) getPO();
		setDateAcct(monthly.getEndDate());
		setDateDoc(monthly.getEndDate());
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() 
	{
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) 
	{
		ArrayList<Fact> facts = new ArrayList<>();
		MUNSMonthlyPayrollEmployee monthly = (MUNSMonthlyPayrollEmployee) getPO();
		if (DocAction.STATUS_Voided.equals(monthly.getDocStatus()))
			return facts;
		
		MPeriod period = MPeriod.get(getCtx(), monthly.getC_Period_ID());
		MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
				getCtx(), period, monthly.getAD_Org_ID(), getTrxName(), true);
		MUNSPayrollEmployee[] payrolls = monthly.getLines(false);
		ArrayList<LineModel> models = new ArrayList<>();
		
		int pph21Acct = payConfig.getPPH21_Acct_ID();
		int B_JHTAcct = payConfig.getB_JHTAcct_ID();
		int B_JPKAcct = payConfig.getB_JPKAcct_ID();
		int B_JKKAcct = payConfig.getB_JKKAcct_ID();
		int B_JKAcct = payConfig.getB_JKAcct_ID();
		int H_JHTAcct = payConfig.getH_JHT_Acct_ID();
		int H_JPKAcct = payConfig.getH_JPK_Acct_ID();
		int H_JKKAcct = payConfig.getH_JKK_Acct_ID();
		int H_JKAcct = payConfig.getH_JK_Acct_ID();
		int A_LemburAcct = payConfig.getA_Lembur_Acct_ID();
		int A_L1Acct = payConfig.getA_L1_Acct_ID();
		int A_L2Acct = payConfig.getA_L2_Acct_ID();
		int A_L3Acct = payConfig.getA_L3_Acct_ID();
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		setC_Currency_ID (as.getC_Currency_ID());
		
		for (int i=0; i<payrolls.length; i++)
		{
			MUNSPayrollEmployee payroll = payrolls[i];
			I_UNS_Employee employee = payroll.getUNS_Employee();
			I_UNS_Contract_Recommendation contract = employee.getUNS_Contract_Recommendation();
			int BiayaGajiAcct = payConfig.getBiayaGajiBulananAcct_ID();
			int HutangGajiAcct = payConfig.getHutangGajiBulananAcct_ID();
			
			if(MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarian.equals(
					contract.getNextContractType())
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarianCV.equals(
							contract.getNextContractType()))
			{
				BiayaGajiAcct = payConfig.getBiayaGajiHarianAcct_ID();
				HutangGajiAcct = payConfig.getHutangGajiHarianAcct_ID();
			}
			
			BigDecimal biayaGaji = payroll.getGPokok().add(payroll.getCorrectionAmt()).
					add(payroll.getCorrectionAmtNonPeriodic()).add(payroll.getCorrectionAmtPPh21()).
					add(payroll.getCorrectionAmtPPh21NonPeriodic());
			
			BigDecimal hutangGaji = Env.ZERO;
			int sectionOfDeptID = employee.getC_BPartner_ID();
			
			if (biayaGaji.signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, BiayaGajiAcct, true);
				model.addAmount(biayaGaji);
				hutangGaji = hutangGaji.add(biayaGaji);
			}
			
			if (payroll.isPPH21PaidByCompany() && payroll.getPPH21().signum() != 0)
			{
				MAccount account = MCharge.getAccount(
						UNSApps.getRefAsInt(UNSApps.CHRG_BiayaPPH21), as);
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, account, true);
				model.addAmount(payroll.getPPH21());
			}
			
			if (payroll.getA_L1().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, A_L1Acct, true);
				model.addAmount(payroll.getA_L1());
				hutangGaji = hutangGaji.add(payroll.getA_L1());
			}
			
			if (payroll.getA_L2().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, A_L2Acct, true);
				model.addAmount(payroll.getA_L2());
				hutangGaji = hutangGaji.add(payroll.getA_L2());
			}
			
			if (payroll.getA_L3().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, A_L3Acct, true);
				model.addAmount(payroll.getA_L3());
				hutangGaji = hutangGaji.add(payroll.getA_L3());
			}
			
			BigDecimal _OTAmt = payroll.getA_LemburJamPertama().add(payroll.getA_LemburJamBerikutnya());
			if (_OTAmt.signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, A_LemburAcct, true);
				model.addAmount(_OTAmt);
				hutangGaji = hutangGaji.add(_OTAmt);
			}
			
			MUNSPayrollCostBenefit[] costBens = payroll.getCostBenefits(false);
			for (int j=0; j<costBens.length; j++) 
			{
				if (!costBens[j].isBenefit())
					continue;
				
				BigDecimal amount = costBens[j].getAmount();
				if (amount.signum() == 0)
					continue;

				MAccount account = MAccount.get(getCtx(), costBens[j].getCostBenefit_Acct());
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, account, true);
				model.addAmount(amount);
				
				model = extractEqualOrCreate(models, sectionOfDeptID, HutangGajiAcct, false);
				model.addAmount(amount);
			}
			
			
			if (payroll.getP_JHT().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, H_JHTAcct, false);
				model.addAmount(payroll.getP_JHT());
				hutangGaji = hutangGaji.subtract(payroll.getP_JHT());
			}
			
			if (payroll.getA_JHT().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, B_JHTAcct, true);
				model.addAmount(payroll.getA_JHT());
				
				model = extractEqualOrCreate(models, sectionOfDeptID, H_JHTAcct, false);
				model.addAmount(payroll.getA_JHT());
			}
			
			if (payroll.getP_JK().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, H_JKAcct, false);
				model.addAmount(payroll.getP_JK());
				hutangGaji = hutangGaji.subtract(payroll.getP_JK());
			}
			
			if (payroll.getA_JK().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, B_JKAcct, true);
				model.addAmount(payroll.getA_JHT());
				
				model = extractEqualOrCreate(models, sectionOfDeptID, H_JKAcct, false);
				model.addAmount(payroll.getA_JK());
			}
			
			if (payroll.getP_JKK().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, H_JKKAcct, false);
				model.addAmount(payroll.getP_JKK());
				hutangGaji = hutangGaji.subtract(payroll.getP_JKK());
			}
			
			if (payroll.getA_JKK().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, B_JKKAcct, true);
				model.addAmount(payroll.getA_JKK());
				
				model = extractEqualOrCreate(models, sectionOfDeptID, H_JKKAcct, false);
				model.addAmount(payroll.getA_JKK());
			}
			
			if (payroll.getP_JPK().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, H_JPKAcct, false);
				model.addAmount(payroll.getP_JPK());
				hutangGaji = hutangGaji.subtract(payroll.getP_JPK());
			}
			
			if (payroll.getA_JPK().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, B_JPKAcct, true);
				model.addAmount(payroll.getA_JPK());
				
				model = extractEqualOrCreate(models, sectionOfDeptID, H_JPKAcct, false);
				model.addAmount(payroll.getA_JPK());
			}
			
			if (payroll.getPPH21().signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, pph21Acct, false);
				model.addAmount(payroll.getPPH21());
				if (!payroll.isPPH21PaidByCompany())
					hutangGaji = hutangGaji.subtract(payroll.getPPH21());
			}
			
			
			costBens = payroll.getCostBenefits(false);
			for (int j=0; j<costBens.length; j++) 
			{
				if (costBens[j].isBenefit())
					continue;
				
				BigDecimal amount = costBens[j].getAmount();
				if (amount.signum() == 0)
					continue;

				MAccount account = MAccount.get(getCtx(), costBens[j].getCostBenefit_Acct());
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, account, false);
				model.addAmount(amount);
				
				model = extractEqualOrCreate(models, sectionOfDeptID, HutangGajiAcct, true);
				model.addAmount(amount);
			}
			
			if (hutangGaji.signum() != 0)
			{
				LineModel model = extractEqualOrCreate(models, sectionOfDeptID, HutangGajiAcct, false);
				model.addAmount(hutangGaji);
			}
		}
		
		for (int i=0; i<models.size(); i++)
		{
			BigDecimal amount = models.get(i)._amount;
			if (!models.get(i)._isDebit)
				amount = amount.negate();
			FactLine fl = fact.createLine(
					null, models.get(i)._account, getC_Currency_ID(), 
					amount);
			if (fl == null)
			{
				p_Error = "Fact Line not created " + models.get(i).toString();
				return null;
			}
			
			fl.setC_BPartner_ID(models.get(i)._sectionOfDeptID);
		}
		
		facts.add(fact);
		return facts;
	}
	
	private LineModel extractEqualOrCreate (
			ArrayList<LineModel> models, int sectionOfDept, MAccount account, 
			boolean isDebit)
	{
		for (int i=0; i<models.size(); i++)
			if (models.get(i)._account.get_ID() == account.get_ID()
			&& models.get(i)._sectionOfDeptID == sectionOfDept
			&& isDebit == models.get(i)._isDebit)
				return models.get(i);
		LineModel model = new LineModel(sectionOfDept, account, Env.ZERO, isDebit);
		models.add(model);
		return model;
	}
	
	private LineModel extractEqualOrCreate (
			ArrayList<LineModel> models, int sectionOfDept, int accountID, 
			boolean isDebit)
	{
		for (int i=0; i<models.size(); i++)
			if (models.get(i)._account.get_ID() == accountID 
			&& models.get(i)._sectionOfDeptID == sectionOfDept
			&& isDebit == models.get(i)._isDebit)
				return models.get(i);
		LineModel model = new LineModel(sectionOfDept, accountID, Env.ZERO, isDebit);
		models.add(model);
		return model;
	}
	
	private class LineModel 
	{
		int _sectionOfDeptID;
		MAccount _account;
		BigDecimal _amount;
		boolean _isDebit;
		
		LineModel (int sectionOfDeptID, int accountID, BigDecimal amount, boolean isDebit)
		{
			_sectionOfDeptID = sectionOfDeptID;
			_account = MAccount.get(getCtx(), accountID);
			_amount = amount;
			_isDebit = isDebit;
		}
		
		LineModel (int sectionOfDeptID, MAccount account, BigDecimal amount, boolean isDebit)
		{
			_sectionOfDeptID = sectionOfDeptID;
			_account = account;
			_amount = amount;
			_isDebit = isDebit;
		}
		
		void addAmount (BigDecimal amount)
		{
			_amount = _amount.add(amount);
		}
		
		@Override
		public String toString ()
		{
			StringBuilder sb = new StringBuilder("");
			sb.append(_sectionOfDeptID).append(" : ").append(_account.toString())
			.append(_amount.toString()).append(_isDebit ? "Debit" : "Credit");
			String toString = sb.toString();
			return toString;
		}
	}

}

