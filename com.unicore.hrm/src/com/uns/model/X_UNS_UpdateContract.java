/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_UpdateContract
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_UpdateContract extends PO implements I_UNS_UpdateContract, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180725L;

    /** Standard Constructor */
    public X_UNS_UpdateContract (Properties ctx, int UNS_UpdateContract_ID, String trxName)
    {
      super (ctx, UNS_UpdateContract_ID, trxName);
      /** if (UNS_UpdateContract_ID == 0)
        {
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setPayrollLevel (null);
			setPayrollTerm (null);
			setProcessed (false);
// N
			setUNS_UpdateContract_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UpdateContract (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UpdateContract[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set AL1 Multiplier.
		@param AL1Multiplier AL1 Multiplier	  */
	public void setAL1Multiplier (BigDecimal AL1Multiplier)
	{
		set_Value (COLUMNNAME_AL1Multiplier, AL1Multiplier);
	}

	/** Get AL1 Multiplier.
		@return AL1 Multiplier	  */
	public BigDecimal getAL1Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AL1Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set AL2 Multiplier.
		@param AL2Multiplier AL2 Multiplier	  */
	public void setAL2Multiplier (BigDecimal AL2Multiplier)
	{
		set_Value (COLUMNNAME_AL2Multiplier, AL2Multiplier);
	}

	/** Get AL2 Multiplier.
		@return AL2 Multiplier	  */
	public BigDecimal getAL2Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AL2Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set AL3 Multiplier.
		@param AL3Multiplier AL3 Multiplier	  */
	public void setAL3Multiplier (BigDecimal AL3Multiplier)
	{
		set_Value (COLUMNNAME_AL3Multiplier, AL3Multiplier);
	}

	/** Get AL3 Multiplier.
		@return AL3 Multiplier	  */
	public BigDecimal getAL3Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AL3Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ALR1 Multiplier.
		@param ALR1Multiplier ALR1 Multiplier	  */
	public void setALR1Multiplier (BigDecimal ALR1Multiplier)
	{
		set_Value (COLUMNNAME_ALR1Multiplier, ALR1Multiplier);
	}

	/** Get ALR1 Multiplier.
		@return ALR1 Multiplier	  */
	public BigDecimal getALR1Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ALR1Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ALR2 Multiplier.
		@param ALR2Multiplier ALR2 Multiplier	  */
	public void setALR2Multiplier (BigDecimal ALR2Multiplier)
	{
		set_Value (COLUMNNAME_ALR2Multiplier, ALR2Multiplier);
	}

	/** Get ALR2 Multiplier.
		@return ALR2 Multiplier	  */
	public BigDecimal getALR2Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ALR2Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ALR3 Multiplier.
		@param ALR3Multiplier ALR3 Multiplier	  */
	public void setALR3Multiplier (BigDecimal ALR3Multiplier)
	{
		set_Value (COLUMNNAME_ALR3Multiplier, ALR3Multiplier);
	}

	/** Get ALR3 Multiplier.
		@return ALR3 Multiplier	  */
	public BigDecimal getALR3Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ALR3Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getDocumentNo());
    }

	/** Set First OT Multiplier.
		@param FirstOTMultiplier First OT Multiplier	  */
	public void setFirstOTMultiplier (BigDecimal FirstOTMultiplier)
	{
		set_Value (COLUMNNAME_FirstOTMultiplier, FirstOTMultiplier);
	}

	/** Get First OT Multiplier.
		@return First OT Multiplier	  */
	public BigDecimal getFirstOTMultiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FirstOTMultiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set New A L1.
		@param New_A_L1 New A L1	  */
	public void setNew_A_L1 (BigDecimal New_A_L1)
	{
		set_Value (COLUMNNAME_New_A_L1, New_A_L1);
	}

	/** Get New A L1.
		@return New A L1	  */
	public BigDecimal getNew_A_L1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L1 R.
		@param New_A_L1_R New A L1 R	  */
	public void setNew_A_L1_R (BigDecimal New_A_L1_R)
	{
		set_Value (COLUMNNAME_New_A_L1_R, New_A_L1_R);
	}

	/** Get New A L1 R.
		@return New A L1 R	  */
	public BigDecimal getNew_A_L1_R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L1_R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L2.
		@param New_A_L2 New A L2	  */
	public void setNew_A_L2 (BigDecimal New_A_L2)
	{
		set_Value (COLUMNNAME_New_A_L2, New_A_L2);
	}

	/** Get New A L2.
		@return New A L2	  */
	public BigDecimal getNew_A_L2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L2 R.
		@param New_A_L2_R 
		Amount New_A_L2_R
	  */
	public void setNew_A_L2_R (BigDecimal New_A_L2_R)
	{
		set_Value (COLUMNNAME_New_A_L2_R, New_A_L2_R);
	}

	/** Get New A L2 R.
		@return Amount New_A_L2_R
	  */
	public BigDecimal getNew_A_L2_R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L2_R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L3.
		@param New_A_L3 New A L3	  */
	public void setNew_A_L3 (BigDecimal New_A_L3)
	{
		set_Value (COLUMNNAME_New_A_L3, New_A_L3);
	}

	/** Get New A L3.
		@return New A L3	  */
	public BigDecimal getNew_A_L3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L3 R.
		@param New_A_L3_R New A L3 R	  */
	public void setNew_A_L3_R (BigDecimal New_A_L3_R)
	{
		set_Value (COLUMNNAME_New_A_L3_R, New_A_L3_R);
	}

	/** Get New A L3 R.
		@return New A L3 R	  */
	public BigDecimal getNew_A_L3_R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L3_R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New Lembur Jam Berikutnya.
		@param NewLeburJamBerikutnya New Lembur Jam Berikutnya	  */
	public void setNewLeburJamBerikutnya (BigDecimal NewLeburJamBerikutnya)
	{
		set_Value (COLUMNNAME_NewLeburJamBerikutnya, NewLeburJamBerikutnya);
	}

	/** Get New Lembur Jam Berikutnya.
		@return New Lembur Jam Berikutnya	  */
	public BigDecimal getNewLeburJamBerikutnya () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NewLeburJamBerikutnya);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New Lembur Jam Pertama.
		@param NewLeburJamPertama New Lembur Jam Pertama	  */
	public void setNewLeburJamPertama (BigDecimal NewLeburJamPertama)
	{
		set_Value (COLUMNNAME_NewLeburJamPertama, NewLeburJamPertama);
	}

	/** Get New Lembur Jam Pertama.
		@return New Lembur Jam Pertama	  */
	public BigDecimal getNewLeburJamPertama () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NewLeburJamPertama);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Next OT Multiplier.
		@param NextOTMultiplier Next OT Multiplier	  */
	public void setNextOTMultiplier (BigDecimal NextOTMultiplier)
	{
		set_Value (COLUMNNAME_NextOTMultiplier, NextOTMultiplier);
	}

	/** Get Next OT Multiplier.
		@return Next OT Multiplier	  */
	public BigDecimal getNextOTMultiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NextOTMultiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set OT Basic Amount.
		@param OTBasicAmt OT Basic Amount	  */
	public void setOTBasicAmt (BigDecimal OTBasicAmt)
	{
		set_Value (COLUMNNAME_OTBasicAmt, OTBasicAmt);
	}

	/** Get OT Basic Amount.
		@return OT Basic Amount	  */
	public BigDecimal getOTBasicAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OTBasicAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Level 1 = 1 */
	public static final String PAYROLLLEVEL_Level01 = "1";
	/** Level 2 = 2 */
	public static final String PAYROLLLEVEL_Level02 = "2";
	/** Level 3 = 3 */
	public static final String PAYROLLLEVEL_Level03 = "3";
	/** Level 4 = 4 */
	public static final String PAYROLLLEVEL_Level04 = "4";
	/** Level 5 = 5 */
	public static final String PAYROLLLEVEL_Level05 = "5";
	/** Level 6 = 6 */
	public static final String PAYROLLLEVEL_Level06 = "6";
	/** Not Defined = 0 */
	public static final String PAYROLLLEVEL_NotDefined = "0";
	/** Level 7 = 7 */
	public static final String PAYROLLLEVEL_Level07 = "7";
	/** Level 8 = 8 */
	public static final String PAYROLLLEVEL_Level08 = "8";
	/** Level 9 = 9 */
	public static final String PAYROLLLEVEL_Level09 = "9";
	/** Level 10 = 10 */
	public static final String PAYROLLLEVEL_Level10 = "10";
	/** Level 11 = 11 */
	public static final String PAYROLLLEVEL_Level11 = "11";
	/** Level 12 = 12 */
	public static final String PAYROLLLEVEL_Level12 = "12";
	/** Level 13 = 13 */
	public static final String PAYROLLLEVEL_Level13 = "13";
	/** Level 14 = 14 */
	public static final String PAYROLLLEVEL_Level14 = "14";
	/** Level 15 = 15 */
	public static final String PAYROLLLEVEL_Level15 = "15";
	/** Level 16 = 16 */
	public static final String PAYROLLLEVEL_Level16 = "16";
	/** Level 17 = 17 */
	public static final String PAYROLLLEVEL_Level17 = "17";
	/** Set PayrollLevel.
		@param PayrollLevel PayrollLevel	  */
	public void setPayrollLevel (String PayrollLevel)
	{

		set_ValueNoCheck (COLUMNNAME_PayrollLevel, PayrollLevel);
	}

	/** Get PayrollLevel.
		@return PayrollLevel	  */
	public String getPayrollLevel () 
	{
		return (String)get_Value(COLUMNNAME_PayrollLevel);
	}

	/** Monthly = 01 */
	public static final String PAYROLLTERM_Monthly = "01";
	/** Weekly = 02 */
	public static final String PAYROLLTERM_Weekly = "02";
	/** 2 Weekly = 03 */
	public static final String PAYROLLTERM_2Weekly = "03";
	/** Harian Bulanan = 04 */
	public static final String PAYROLLTERM_HarianBulanan = "04";
	/** Set Payroll Term.
		@param PayrollTerm Payroll Term	  */
	public void setPayrollTerm (String PayrollTerm)
	{

		set_ValueNoCheck (COLUMNNAME_PayrollTerm, PayrollTerm);
	}

	/** Get Payroll Term.
		@return Payroll Term	  */
	public String getPayrollTerm () 
	{
		return (String)get_Value(COLUMNNAME_PayrollTerm);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Update Contract.
		@param UNS_UpdateContract_ID Update Contract	  */
	public void setUNS_UpdateContract_ID (int UNS_UpdateContract_ID)
	{
		if (UNS_UpdateContract_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UpdateContract_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UpdateContract_ID, Integer.valueOf(UNS_UpdateContract_ID));
	}

	/** Get Update Contract.
		@return Update Contract	  */
	public int getUNS_UpdateContract_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UpdateContract_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UpdateContract_UU.
		@param UNS_UpdateContract_UU UNS_UpdateContract_UU	  */
	public void setUNS_UpdateContract_UU (String UNS_UpdateContract_UU)
	{
		set_Value (COLUMNNAME_UNS_UpdateContract_UU, UNS_UpdateContract_UU);
	}

	/** Get UNS_UpdateContract_UU.
		@return UNS_UpdateContract_UU	  */
	public String getUNS_UpdateContract_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UpdateContract_UU);
	}
}