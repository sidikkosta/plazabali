/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_OTRequest
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_OTRequest 
{

    /** TableName=UNS_OTRequest */
    public static final String Table_Name = "UNS_OTRequest";

    /** AD_Table_ID=1000308 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BreakTime */
    public static final String COLUMNNAME_BreakTime = "BreakTime";

	/** Set Break Time	  */
	public void setBreakTime (BigDecimal BreakTime);

	/** Get Break Time	  */
	public BigDecimal getBreakTime();

    /** Column name ConfirmedBreakTime */
    public static final String COLUMNNAME_ConfirmedBreakTime = "ConfirmedBreakTime";

	/** Set Confirmed Break Time	  */
	public void setConfirmedBreakTime (BigDecimal ConfirmedBreakTime);

	/** Get Confirmed Break Time	  */
	public BigDecimal getConfirmedBreakTime();

    /** Column name ConfirmedHours */
    public static final String COLUMNNAME_ConfirmedHours = "ConfirmedHours";

	/** Set ConfirmedHours	  */
	public void setConfirmedHours (BigDecimal ConfirmedHours);

	/** Get ConfirmedHours	  */
	public BigDecimal getConfirmedHours();

    /** Column name ConvertibleToBeLeave */
    public static final String COLUMNNAME_ConvertibleToBeLeave = "ConvertibleToBeLeave";

	/** Set Convertible To Be Leave	  */
	public void setConvertibleToBeLeave (boolean ConvertibleToBeLeave);

	/** Get Convertible To Be Leave	  */
	public boolean isConvertibleToBeLeave();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoOT */
    public static final String COLUMNNAME_DateDoOT = "DateDoOT";

	/** Set DateDoOT	  */
	public void setDateDoOT (Timestamp DateDoOT);

	/** Get DateDoOT	  */
	public Timestamp getDateDoOT();

    /** Column name Day */
    public static final String COLUMNNAME_Day = "Day";

	/** Set Day	  */
	public void setDay (String Day);

	/** Get Day	  */
	public String getDay();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EndTime */
    public static final String COLUMNNAME_EndTime = "EndTime";

	/** Set End Time.
	  * End of the time span
	  */
	public void setEndTime (Timestamp EndTime);

	/** Get End Time.
	  * End of the time span
	  */
	public Timestamp getEndTime();

    /** Column name EndTimeTxt */
    public static final String COLUMNNAME_EndTimeTxt = "EndTimeTxt";

	/** Set End Time Text	  */
	public void setEndTimeTxt (String EndTimeTxt);

	/** Get End Time Text	  */
	public String getEndTimeTxt();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name PrintSPL */
    public static final String COLUMNNAME_PrintSPL = "PrintSPL";

	/** Set Print SPL	  */
	public void setPrintSPL (String PrintSPL);

	/** Get Print SPL	  */
	public String getPrintSPL();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name RequestDate */
    public static final String COLUMNNAME_RequestDate = "RequestDate";

	/** Set Request Date	  */
	public void setRequestDate (Timestamp RequestDate);

	/** Get Request Date	  */
	public Timestamp getRequestDate();

    /** Column name RequestedHours */
    public static final String COLUMNNAME_RequestedHours = "RequestedHours";

	/** Set RequestedHours	  */
	public void setRequestedHours (BigDecimal RequestedHours);

	/** Get RequestedHours	  */
	public BigDecimal getRequestedHours();

    /** Column name StartTime */
    public static final String COLUMNNAME_StartTime = "StartTime";

	/** Set Start Time.
	  * Time started
	  */
	public void setStartTime (Timestamp StartTime);

	/** Get Start Time.
	  * Time started
	  */
	public Timestamp getStartTime();

    /** Column name StartTimeTxt */
    public static final String COLUMNNAME_StartTimeTxt = "StartTimeTxt";

	/** Set Start Time Text	  */
	public void setStartTimeTxt (String StartTimeTxt);

	/** Get Start Time Text	  */
	public String getStartTimeTxt();

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name UNS_OTGroupRequest_ID */
    public static final String COLUMNNAME_UNS_OTGroupRequest_ID = "UNS_OTGroupRequest_ID";

	/** Set Overtime Group Request	  */
	public void setUNS_OTGroupRequest_ID (int UNS_OTGroupRequest_ID);

	/** Get Overtime Group Request	  */
	public int getUNS_OTGroupRequest_ID();

	public com.uns.model.I_UNS_OTGroupRequest getUNS_OTGroupRequest() throws RuntimeException;

    /** Column name UNS_OTLine_ID */
    public static final String COLUMNNAME_UNS_OTLine_ID = "UNS_OTLine_ID";

	/** Set Over Time Line	  */
	public void setUNS_OTLine_ID (int UNS_OTLine_ID);

	/** Get Over Time Line	  */
	public int getUNS_OTLine_ID();

	public com.uns.model.I_UNS_OTLine getUNS_OTLine() throws RuntimeException;

    /** Column name UNS_OTRequest_ID */
    public static final String COLUMNNAME_UNS_OTRequest_ID = "UNS_OTRequest_ID";

	/** Set UNS_OTRequest_ID	  */
	public void setUNS_OTRequest_ID (int UNS_OTRequest_ID);

	/** Get UNS_OTRequest_ID	  */
	public int getUNS_OTRequest_ID();

    /** Column name UNS_OTRequest_UU */
    public static final String COLUMNNAME_UNS_OTRequest_UU = "UNS_OTRequest_UU";

	/** Set UNS_OTRequest_UU	  */
	public void setUNS_OTRequest_UU (String UNS_OTRequest_UU);

	/** Get UNS_OTRequest_UU	  */
	public String getUNS_OTRequest_UU();

    /** Column name UNS_SlotType_ID */
    public static final String COLUMNNAME_UNS_SlotType_ID = "UNS_SlotType_ID";

	/** Set Slot Type	  */
	public void setUNS_SlotType_ID (int UNS_SlotType_ID);

	/** Get Slot Type	  */
	public int getUNS_SlotType_ID();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
