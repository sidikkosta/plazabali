/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_SubregionRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_SubregionRecap extends PO implements I_UNS_SubregionRecap, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191119L;

    /** Standard Constructor */
    public X_UNS_SubregionRecap (Properties ctx, int UNS_SubregionRecap_ID, String trxName)
    {
      super (ctx, UNS_SubregionRecap_ID, trxName);
      /** if (UNS_SubregionRecap_ID == 0)
        {
			setC_Period_ID (0);
			setIsApproved (false);
// N
			setMinSubSalesTargetAmt (Env.ZERO);
// 0
			setSubregion_ID (0);
			setUNS_SubregionRecap_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_SubregionRecap (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_SubregionRecap[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Calculate Service Charges.
		@param CalculateServiceCharge 
		Calculate & Load all service charges of (a) shop in a subregion
	  */
	public void setCalculateServiceCharge (String CalculateServiceCharge)
	{
		set_Value (COLUMNNAME_CalculateServiceCharge, CalculateServiceCharge);
	}

	/** Get Calculate Service Charges.
		@return Calculate & Load all service charges of (a) shop in a subregion
	  */
	public String getCalculateServiceCharge () 
	{
		return (String)get_Value(COLUMNNAME_CalculateServiceCharge);
	}

	/** Set Calculation Class.
		@param CalculationClass 
		Java Class for calculation, implementing Interface Measure
	  */
	public void setCalculationClass (String CalculationClass)
	{
		set_Value (COLUMNNAME_CalculationClass, CalculationClass);
	}

	/** Get Calculation Class.
		@return Java Class for calculation, implementing Interface Measure
	  */
	public String getCalculationClass () 
	{
		return (String)get_Value(COLUMNNAME_CalculationClass);
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getC_Period_ID()));
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Employee Portion.
		@param EmployeePortion Employee Portion	  */
	public void setEmployeePortion (int EmployeePortion)
	{
		set_Value (COLUMNNAME_EmployeePortion, Integer.valueOf(EmployeePortion));
	}

	/** Get Employee Portion.
		@return Employee Portion	  */
	public int getEmployeePortion () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_EmployeePortion);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set End Date.
		@param EndDate 
		Last effective date (inclusive)
	  */
	public void setEndDate (Timestamp EndDate)
	{
		set_Value (COLUMNNAME_EndDate, EndDate);
	}

	/** Get End Date.
		@return Last effective date (inclusive)
	  */
	public Timestamp getEndDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_EndDate);
	}

	/** Set File_Directory.
		@param File_Directory File_Directory	  */
	public void setFile_Directory (String File_Directory)
	{
		set_Value (COLUMNNAME_File_Directory, File_Directory);
	}

	/** Get File_Directory.
		@return File_Directory	  */
	public String getFile_Directory () 
	{
		return (String)get_Value(COLUMNNAME_File_Directory);
	}

	/** Set Import Additional Staff.
		@param ImportAdditionalStaff Import Additional Staff	  */
	public void setImportAdditionalStaff (String ImportAdditionalStaff)
	{
		set_Value (COLUMNNAME_ImportAdditionalStaff, ImportAdditionalStaff);
	}

	/** Get Import Additional Staff.
		@return Import Additional Staff	  */
	public String getImportAdditionalStaff () 
	{
		return (String)get_Value(COLUMNNAME_ImportAdditionalStaff);
	}

	/** Set Import Monthly Shop Schedule.
		@param ImportMonthlyShopSchedule Import Monthly Shop Schedule	  */
	public void setImportMonthlyShopSchedule (String ImportMonthlyShopSchedule)
	{
		set_Value (COLUMNNAME_ImportMonthlyShopSchedule, ImportMonthlyShopSchedule);
	}

	/** Get Import Monthly Shop Schedule.
		@return Import Monthly Shop Schedule	  */
	public String getImportMonthlyShopSchedule () 
	{
		return (String)get_Value(COLUMNNAME_ImportMonthlyShopSchedule);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Load Shop Staff.
		@param LoadShopStaff Load Shop Staff	  */
	public void setLoadShopStaff (String LoadShopStaff)
	{
		set_Value (COLUMNNAME_LoadShopStaff, LoadShopStaff);
	}

	/** Get Load Shop Staff.
		@return Load Shop Staff	  */
	public String getLoadShopStaff () 
	{
		return (String)get_Value(COLUMNNAME_LoadShopStaff);
	}

	/** Set Min. Subregion Sales Target.
		@param MinSubSalesTargetAmt 
		Minimum target amount for shop
	  */
	public void setMinSubSalesTargetAmt (BigDecimal MinSubSalesTargetAmt)
	{
		set_Value (COLUMNNAME_MinSubSalesTargetAmt, MinSubSalesTargetAmt);
	}

	/** Get Min. Subregion Sales Target.
		@return Minimum target amount for shop
	  */
	public BigDecimal getMinSubSalesTargetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinSubSalesTargetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Net Sales Incentive.
		@param NetSalesIncentiveAmt 
		Amount in a defined currency
	  */
	public void setNetSalesIncentiveAmt (BigDecimal NetSalesIncentiveAmt)
	{
		set_Value (COLUMNNAME_NetSalesIncentiveAmt, NetSalesIncentiveAmt);
	}

	/** Get Net Sales Incentive.
		@return Amount in a defined currency
	  */
	public BigDecimal getNetSalesIncentiveAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesIncentiveAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Net Sales Realization.
		@param NetSalesRealizationAmt 
		Total realized net sales within 1 month from all stores
	  */
	public void setNetSalesRealizationAmt (BigDecimal NetSalesRealizationAmt)
	{
		set_Value (COLUMNNAME_NetSalesRealizationAmt, NetSalesRealizationAmt);
	}

	/** Get Net Sales Realization.
		@return Total realized net sales within 1 month from all stores
	  */
	public BigDecimal getNetSalesRealizationAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesRealizationAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Net Sales Target.
		@param NetSalesTarget 
		Net sales target from all stores in a certain subregion
	  */
	public void setNetSalesTarget (BigDecimal NetSalesTarget)
	{
		set_Value (COLUMNNAME_NetSalesTarget, NetSalesTarget);
	}

	/** Get Net Sales Target.
		@return Net sales target from all stores in a certain subregion
	  */
	public BigDecimal getNetSalesTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Bazaar = BZ */
	public static final String SHOPTYPE_Bazaar = "BZ";
	/** Shop Retail Duty Free = DF */
	public static final String SHOPTYPE_ShopRetailDutyFree = "DF";
	/** Shop Retail Duty Paid = DP */
	public static final String SHOPTYPE_ShopRetailDutyPaid = "DP";
	/** Shop F&B = FB */
	public static final String SHOPTYPE_ShopFB = "FB";
	/** Online = OL */
	public static final String SHOPTYPE_Online = "OL";
	/** Set Shop Type.
		@param ShopType Shop Type	  */
	public void setShopType (String ShopType)
	{

		set_Value (COLUMNNAME_ShopType, ShopType);
	}

	/** Get Shop Type.
		@return Shop Type	  */
	public String getShopType () 
	{
		return (String)get_Value(COLUMNNAME_ShopType);
	}

	/** Set Start Date.
		@param StartDate 
		First effective day (inclusive)
	  */
	public void setStartDate (Timestamp StartDate)
	{
		set_Value (COLUMNNAME_StartDate, StartDate);
	}

	/** Get Start Date.
		@return First effective day (inclusive)
	  */
	public Timestamp getStartDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartDate);
	}

	public org.compiere.model.I_C_BP_Group getSubregion2() throws RuntimeException
    {
		return (org.compiere.model.I_C_BP_Group)MTable.get(getCtx(), org.compiere.model.I_C_BP_Group.Table_Name)
			.getPO(getSubregion2_ID(), get_TrxName());	}

	/** Set Subregion 2.
		@param Subregion2_ID 
		Subregion 2
	  */
	public void setSubregion2_ID (int Subregion2_ID)
	{
		if (Subregion2_ID < 1) 
			set_Value (COLUMNNAME_Subregion2_ID, null);
		else 
			set_Value (COLUMNNAME_Subregion2_ID, Integer.valueOf(Subregion2_ID));
	}

	/** Get Subregion 2.
		@return Subregion 2
	  */
	public int getSubregion2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Subregion2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BP_Group getSubregion() throws RuntimeException
    {
		return (org.compiere.model.I_C_BP_Group)MTable.get(getCtx(), org.compiere.model.I_C_BP_Group.Table_Name)
			.getPO(getSubregion_ID(), get_TrxName());	}

	/** Set Subregion.
		@param Subregion_ID 
		Subregion
	  */
	public void setSubregion_ID (int Subregion_ID)
	{
		if (Subregion_ID < 1) 
			set_Value (COLUMNNAME_Subregion_ID, null);
		else 
			set_Value (COLUMNNAME_Subregion_ID, Integer.valueOf(Subregion_ID));
	}

	/** Get Subregion.
		@return Subregion
	  */
	public int getSubregion_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Subregion_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Service Charge.
		@param TotalServiceChargeAmt Total Service Charge	  */
	public void setTotalServiceChargeAmt (BigDecimal TotalServiceChargeAmt)
	{
		set_Value (COLUMNNAME_TotalServiceChargeAmt, TotalServiceChargeAmt);
	}

	/** Get Total Service Charge.
		@return Total Service Charge	  */
	public BigDecimal getTotalServiceChargeAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalServiceChargeAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Subregion Recapitulation.
		@param UNS_SubregionRecap_ID Subregion Recapitulation	  */
	public void setUNS_SubregionRecap_ID (int UNS_SubregionRecap_ID)
	{
		if (UNS_SubregionRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_ID, Integer.valueOf(UNS_SubregionRecap_ID));
	}

	/** Get Subregion Recapitulation.
		@return Subregion Recapitulation	  */
	public int getUNS_SubregionRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SubregionRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_SubregionRecap_UU.
		@param UNS_SubregionRecap_UU UNS_SubregionRecap_UU	  */
	public void setUNS_SubregionRecap_UU (String UNS_SubregionRecap_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_UU, UNS_SubregionRecap_UU);
	}

	/** Get UNS_SubregionRecap_UU.
		@return UNS_SubregionRecap_UU	  */
	public String getUNS_SubregionRecap_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_SubregionRecap_UU);
	}
}