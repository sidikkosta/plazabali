package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

//import org.compiere.util.CLogger;
//import org.compiere.util.DB;
//import org.compiere.util.Env;

public class MUNSPayrollCostBenefit extends X_UNS_Payroll_CostBenefit {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1815639362495765898L;
	public boolean isManul = true;

	public MUNSPayrollCostBenefit(Properties ctx,
			int UNS_Payroll_CostBenefit_ID, String trxName) {
		super(ctx, UNS_Payroll_CostBenefit_ID, trxName);
	}

	public MUNSPayrollCostBenefit(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public BigDecimal getTrxAmount () {
		BigDecimal amt = getAmount();
		if (!isBenefit())
			amt = amt.negate();
		return amt;
	}
	
//	public static MUNSPayrollCostBenefit get (int pEmployee_ID, int Component_ID, String trxName) {
//		MUNSPayrollCostBenefit costBenefit = Query.get(
//				Env.getCtx(), UNSHRMModelFactory.EXTENSION_ID, Table_Name, 
//				"UNS_Payroll_Employee_ID = ? AND UNS_Payroll_Component_Conf_ID = ?", 
//				trxName).setParameters(pEmployee_ID, Component_ID).firstOnly();
//		return costBenefit;
//	}
	
	public MUNSPayrollCostBenefit (MUNSPayrollEmployee payroll, MUNSPayrollComponentConf conf) {
		this (payroll.getCtx(), 0, payroll.get_TrxName());
		setClientOrg(payroll);
		if (conf != null) {
			setIsBenefit(conf.isBenefit());
			setName(conf.getName());
			setUNS_Payroll_Component_Conf_ID(conf.get_ID());
			setCostBenefit_Acct(conf.getCostBenefit_Acct());
			setAmount(conf.getAmount());
			setSeqNo(conf.getSeqNo());
			setIsPPHComp(conf.isPPHComp());
			setIsAllowOverride(conf.isAllowOverride());
			setIsPaidOutsidePayroll(conf.isPaidOutsidePayroll());
			setIsPrinted(conf.isPrinted());
			setIsMonthlyPPHComp(conf.get_ValueAsBoolean(COLUMNNAME_IsMonthlyPPHComp)); //TODO change it when the x is generated
		}
		setUNS_Payroll_Employee_ID(payroll.get_ID());
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord) 
	{
		if (is_ValueChanged(COLUMNNAME_Amount) && isManul) {
			getPayrollEmployee().setGeneratePay("N");
			if (!getPayrollEmployee().save())
			{
				return false;
			}
		}
		return super.beforeSave(newRecord);
	}
	
//	private boolean updateHeader ()
//	{
//		String sql = "SELECT COALESCE (SUM(Amount), 0) FROM "
//				+ " UNS_Payroll_CostBenefit WHERE UNS_Payroll_Employee_ID = ? AND IsBenefit = ?";
//		BigDecimal tCost = DB.getSQLValueBD(get_TrxName(), sql, getUNS_Payroll_Employee_ID(), "N");
//		if (tCost == null)
//			tCost = Env.ZERO;
//		sql = "SELECT COALESCE (SUM(Amount), 0) FROM "
//				+ " UNS_Payroll_CostBenefit WHERE UNS_Payroll_Employee_ID = ? AND IsBenefit = ?";
//		BigDecimal tBens = DB.getSQLValueBD(get_TrxName(), sql, getUNS_Payroll_Employee_ID(), "Y");
//		if (tBens == null)
//			tBens = Env.ZERO;
//		MUNSPayrollEmployee payEmp = getPayrollEmployee();
//		payEmp.setTotalOtherAllowances(tBens);
//		payEmp.setTotalOtherDeductions(tCost);
//		return payEmp.save();
//	}
	
	protected boolean afterSave (boolean newRecord, boolean success)
	{	
//		if (!updateHeader()) {
//			log.saveError("Error when update header", 
//					CLogger.retrieveErrorString("Failed when try to update header"));
//			return false;
//		}
		return super.afterSave(newRecord, success);
	}
	
	private MUNSPayrollEmployee m_payrollEmployee = null;
	
	public MUNSPayrollEmployee getPayrollEmployee ()
	{
		if (m_payrollEmployee == null)
			m_payrollEmployee = new MUNSPayrollEmployee(getCtx(), getUNS_Payroll_Employee_ID(), get_TrxName());
		return m_payrollEmployee;
	}
}
