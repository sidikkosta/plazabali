/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;
import com.uns.util.UNSTimeUtil;

/**
 * @author eko
 *
 */
public class MUNSPayrollLevelConfig extends X_UNS_PayrollLevel_Config {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MUNSPayrollConfiguration m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_PayrollLevel_Config_ID
	 * @param trxName
	 */
	public MUNSPayrollLevelConfig(Properties ctx,
			int UNS_PayrollLevel_Config_ID, String trxName) {
		super(ctx, UNS_PayrollLevel_Config_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollLevelConfig(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param payrollLevel
	 * @param payrollTerm
	 * @param trxName
	 * @return
	 */
	public static MUNSPayrollLevelConfig get(Properties ctx, String payrollLevel, String payrollTerm, int AD_Org_ID
			, boolean checkAllOrg , String trxName)
	{
		MUNSPayrollLevelConfig payLevel = null;
		String whereClause =   "PayrollLevel =? AND PayrollTerm = ? AND UNS_PayrollConfiguration_ID = " +
				"	(SELECT pc.UNS_PayrollConfiguration_ID FROM UNS_PayrollConfiguration pc " +
				"	 WHERE Now() BETWEEN pc.ValidFrom AND pc.ValidTo AND pc.IsActive='Y' AND AD_Org_ID = ? )";

		payLevel = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, whereClause, trxName)
				.setParameters(payrollLevel, payrollTerm, AD_Org_ID)
				.first();
		
		if(payLevel == null && checkAllOrg)
		{
			payLevel = MUNSPayrollLevelConfig.get(ctx, payrollLevel, payrollTerm, 0, false, trxName);
		}
		
		return payLevel;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param employee
	 * @param trxName
	 * @return
	 *
	public static BigDecimal getMedicalAllowanceOf (Properties ctx, I_UNS_Employee employee, String trxName)
	{
		String sql =   "SELECT COALESCE (MedicalAllowance, 0) FROM UNS_AllowanceConfig " +
				" WHERE C_JobCategory_ID=? AND UNS_PayrollConfiguration_ID = " +
				"	(SELECT pc.UNS_PayrollConfiguration_ID FROM UNS_PayrollConfiguration pc " +
				"	 WHERE Now() BETWEEN pc.ValidFrom AND pc.ValidTo AND pc.IsActive='Y')";
		BigDecimal medicalAllowance = 
				DB.getSQLValueBD(trxName, sql, employee.getC_Job().getC_JobCategory_ID());
		if (medicalAllowance == null)
			medicalAllowance = Env.ZERO;
		return medicalAllowance;
	}
	*/
	
	/**
	 * 
	 * @param ctx
	 * @param employee
	 * @param trxName
	 * @return
	 */
	public static BigDecimal getMedicalAllowanceOf (Properties ctx, I_UNS_Employee employee, String trxName)
	{
		BigDecimal medicalAllowance = BigDecimal.ZERO;
		
		String sql =   "SELECT UNS_AllowanceConfig_ID FROM UNS_AllowanceConfig " +
				" WHERE C_JobCategory_ID=? AND ContractType=? AND Status = ? AND UNS_PayrollConfiguration_ID = " +
				"	(SELECT pc.UNS_PayrollConfiguration_ID FROM UNS_PayrollConfiguration pc " +
				"	 WHERE Now() BETWEEN pc.ValidFrom AND pc.ValidTo AND pc.IsActive='Y')"
				+ " AND PayrollLevel = ?";
		
		int record_ID = DB.getSQLValue(trxName, sql, employee.getC_Job().getC_JobCategory_ID()
				, employee.getUNS_Contract_Recommendation().getNextContractType(), employee.getStatus()
				, employee.getUNS_Contract_Recommendation().getNewPayrollLevel());
		if(record_ID <= 0)
			return medicalAllowance;
		
		X_UNS_AllowanceConfig allowanceConfig = new X_UNS_AllowanceConfig(ctx, record_ID, trxName);
		if(allowanceConfig.isAllowanceBaseOnSallary())
		{
			MUNSPayrollBaseEmployee payrollBase = MUNSPayrollBaseEmployee.get(
					ctx, employee.getUNS_Contract_Recommendation_ID(), trxName);
			medicalAllowance = payrollBase.getGPokok();
			if(allowanceConfig.getAllowanceMultiplier().compareTo(Env.ZERO) > 0)
				medicalAllowance = medicalAllowance.multiply(
						allowanceConfig.getAllowanceMultiplier());
		}
		else
		{
			medicalAllowance = allowanceConfig.getMedicalAllowance();
		}
		
		if (medicalAllowance == null)
			medicalAllowance = Env.ZERO;
		return medicalAllowance;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param contract
	 * @param trxName
	 * @return
	 */
	public static BigDecimal getMedicalAllowanceOfContract (Properties ctx, MUNSContractRecommendation contract, String trxName)
	{
		BigDecimal medicalAllowance = BigDecimal.ZERO;
		
		String sql =   "SELECT UNS_AllowanceConfig_ID FROM UNS_AllowanceConfig " +
				" WHERE C_JobCategory_ID=? AND ContractType=? AND Status = ? AND UNS_PayrollConfiguration_ID = " +
				"	(SELECT pc.UNS_PayrollConfiguration_ID FROM UNS_PayrollConfiguration pc " +
				"	 WHERE Now() BETWEEN pc.ValidFrom AND pc.ValidTo AND pc.IsActive='Y')"
				+ " AND PayrollLevel = ?";
		
		int record_ID = DB.getSQLValue(trxName, sql, contract.getNewJob().getC_JobCategory_ID()
				, contract.getNextContractType(), contract.getUNS_Employee().getStatus()
				, contract.getNewPayrollLevel());
		if(record_ID <= 0)
			return medicalAllowance;
		
		X_UNS_AllowanceConfig allowanceConfig = new X_UNS_AllowanceConfig(ctx, record_ID, trxName);
		if(allowanceConfig.isAllowanceBaseOnSallary())
		{
			MUNSPayrollBaseEmployee payrollBase = MUNSPayrollBaseEmployee.get(
					ctx, contract.get_ID(), trxName);
			medicalAllowance = payrollBase.getGPokok();
			if(allowanceConfig.getAllowanceMultiplier().compareTo(Env.ZERO) > 0)
				medicalAllowance = medicalAllowance.multiply(
						allowanceConfig.getAllowanceMultiplier());
		}
		else
		{
			medicalAllowance = allowanceConfig.getMedicalAllowance();
		}
		
		if (medicalAllowance == null)
			medicalAllowance = Env.ZERO;
		return medicalAllowance;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param employee
	 * @param trxName
	 * @return
	 */
	public static BigDecimal getPrevMedicalAllowanceOf (Properties ctx, I_UNS_Employee employee, String trxName)
	{
		BigDecimal medicalAllowance = BigDecimal.ZERO;
		
		String sql =   "SELECT UNS_AllowanceConfig_ID FROM UNS_AllowanceConfig " +
				" WHERE C_JobCategory_ID=? AND ContractType=? AND Status = ? AND UNS_PayrollConfiguration_ID = " +
				"	(SELECT pc.UNS_PayrollConfiguration_ID FROM UNS_PayrollConfiguration pc " +
				"	 WHERE Now() BETWEEN pc.ValidFrom AND pc.ValidTo AND pc.IsActive='Y')"
				+ " AND PayrollLevel = ?";
		
		int record_ID = DB.getSQLValue(trxName, sql, employee.getUNS_Contract_Recommendation().getPrevJob().getC_JobCategory_ID()
				, employee.getUNS_Contract_Recommendation().getPrevContractType(), employee.getStatus()
				, employee.getUNS_Contract_Recommendation().getPrevPayrollLevel());
		if(record_ID <= 0)
			return medicalAllowance;
		
		X_UNS_AllowanceConfig allowanceConfig = new X_UNS_AllowanceConfig(ctx, record_ID, trxName);
		if(allowanceConfig.isAllowanceBaseOnSallary())
		{
			MUNSPayrollBaseEmployee payrollBase = MUNSPayrollBaseEmployee.get(
					ctx, employee.getUNS_Contract_Recommendation_ID(), trxName);
			medicalAllowance = payrollBase.getGPokok();
			if(allowanceConfig.getAllowanceMultiplier().compareTo(Env.ZERO) > 0)
				medicalAllowance = medicalAllowance.multiply(
						allowanceConfig.getAllowanceMultiplier());
		}
		else
		{
			medicalAllowance = allowanceConfig.getMedicalAllowance();
		}
		
		if (medicalAllowance == null)
			medicalAllowance = Env.ZERO;
		return medicalAllowance;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param contract
	 * @param trxName
	 * @return
	 */
	public static BigDecimal getPrevMedicalAllowanceOfContract (Properties ctx, MUNSContractRecommendation contract, String trxName)
	{
		BigDecimal medicalAllowance = BigDecimal.ZERO;
		
		String sql =   "SELECT UNS_AllowanceConfig_ID FROM UNS_AllowanceConfig " +
				" WHERE C_JobCategory_ID=? AND ContractType=? AND Status = ? AND UNS_PayrollConfiguration_ID = " +
				"	(SELECT pc.UNS_PayrollConfiguration_ID FROM UNS_PayrollConfiguration pc " +
				"	 WHERE Now() BETWEEN pc.ValidFrom AND pc.ValidTo AND pc.IsActive='Y')"
				+ " AND PayrollLevel = ?";
		
		int record_ID = DB.getSQLValue(trxName, sql, contract.getPrevJob().getC_JobCategory_ID()
				, contract.getPrevContractType(), contract.getUNS_Employee().getStatus()
				, contract.getPrevPayrollLevel());
		if(record_ID <= 0)
			return medicalAllowance;
		
		X_UNS_AllowanceConfig allowanceConfig = new X_UNS_AllowanceConfig(ctx, record_ID, trxName);
		if(allowanceConfig.isAllowanceBaseOnSallary())
		{
			MUNSPayrollBaseEmployee payrollBase = MUNSPayrollBaseEmployee.get(
					ctx, contract.get_ID(), trxName);
			medicalAllowance = payrollBase.getGPokok();
			if(allowanceConfig.getAllowanceMultiplier().compareTo(Env.ZERO) > 0)
				medicalAllowance = medicalAllowance.multiply(
						allowanceConfig.getAllowanceMultiplier());
		}
		else
		{
			medicalAllowance = allowanceConfig.getMedicalAllowance();
		}
		
		if (medicalAllowance == null)
			medicalAllowance = Env.ZERO;
		return medicalAllowance;
	}
	
	public MUNSPayrollConfiguration getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPayrollConfiguration(getCtx(), getUNS_PayrollConfiguration_ID(), get_TrxName());
		
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		//check duplicate
		String sql = "SELECT DocumentNo FROM UNS_PayrollConfiguration pc"
				+ " INNER JOIN UNS_PayrollLevel_Config plc ON plc.UNS_PayrollConfiguration_ID = pc.UNS_PayrollConfiguration_ID"
				+ " WHERE plc.UNS_PayrollLevel_Config_ID <> ? AND plc.PayrollTerm = ? AND plc.PayrollLevel = ?"
				+ " AND plc.AD_Org_ID = ?";
		String docNo = DB.getSQLValueString(
				get_TrxName(), sql, get_ID(), getPayrollTerm(), getPayrollLevel(), getAD_Org_ID());
		
		if(docNo != null)
			throw new AdempiereException("There was exists payroll level configuration on Payroll Configuration No. "+docNo);
		
		if (getParent().isMultiplicationOTCalc())
		{
			BigDecimal basicAmt = getOTBasicAmt();
			BigDecimal al1 = basicAmt.multiply(getAL1Multiplier());
			BigDecimal al2 = basicAmt.multiply(getAL2Multiplier());
			BigDecimal al3 = basicAmt.multiply(getAL3Multiplier());
			BigDecimal alr1 = basicAmt.multiply(getALR1Multiplier());
			BigDecimal alr2 = basicAmt.multiply(getALR2Multiplier());
			BigDecimal alr3 = basicAmt.multiply(getALR3Multiplier());
			BigDecimal firstOTAmt = basicAmt.multiply(getFirstOTMultiplier());
			BigDecimal nextOTAmt = basicAmt.multiply(getNextOTMultiplier());
			setMin_A_L1(al1);
			setMin_A_L2(al2);
			setMin_A_L3(al3);
			setMin_A_L1_R(alr1);
			setMin_A_L2_R(alr2);
			setMin_A_L3_R(alr3);
			setMin_A_LemburJamPertama(firstOTAmt);
			setMin_A_Lembur(nextOTAmt);
		}
		
		return super.beforeSave(newRecord);
	}
	
	public BigDecimal calculateTHR (MUNSEmployee emp, Timestamp date)
	{
		MUNSContractRecommendation contract = (MUNSContractRecommendation) emp.getUNS_Contract_Recommendation();
		if (contract.get_ID() == 0)
			return Env.ZERO;
		
		String basicCalc = getTHRBasicCalculation();
		BigDecimal amountBased = contract.getNew_G_Pokok();
		BigDecimal addAmt = Env.ZERO;
		if (THRBASICCALCULATION_BrutoSalary.equals(basicCalc))
		{
			String sql = "SELECT SUM (Amount) FROM UNS_Payroll_Component_Conf WHERE "
					+ " UNS_Contract_Recommendation_ID = ? AND IsActive = ? AND isBenefit = ?";
			addAmt = DB.getSQLValueBD(get_TrxName(), sql, contract.get_ID(), "Y", "Y");
		}
		else if (THRBASICCALCULATION_NetSalary.equals(basicCalc))
		{
			String sql = "SELECT SUM (Amount) FROM UNS_Payroll_Component_Conf WHERE "
					+ " UNS_Contract_Recommendation_ID = ? AND IsActive = ? ";
			addAmt = DB.getSQLValueBD(get_TrxName(), sql, contract.get_ID(), "Y");
		}
		
		amountBased.add(addAmt);
		BigDecimal thrAmt = amountBased.multiply(getTHRMultiplication());
		Timestamp entryDate = emp.getEffectiveEntryDate();
		if (entryDate == null)
			return Env.ZERO;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(entryDate.getTime());
		int start = cal.get(Calendar.DATE);
		cal.setTimeInMillis(date.getTime());
		int end = cal.get(Calendar.DATE);
		int monthBetween = UNSTimeUtil.getMonthsBetween(entryDate, date);
		if (monthBetween <= 0)
			return Env.ZERO;
		else if (monthBetween == 1 && end < start)
			return Env.ZERO;
		if (monthBetween < 12)
		{
			BigDecimal monthlyTHRAmt = thrAmt.divide(new BigDecimal(12), 5, RoundingMode.HALF_DOWN);
			thrAmt = monthlyTHRAmt.multiply(new BigDecimal(monthBetween));
		}
		
		return thrAmt;
	}
}
