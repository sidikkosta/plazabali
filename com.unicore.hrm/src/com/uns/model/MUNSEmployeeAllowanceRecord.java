/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.I_C_Year;
import org.compiere.model.MPeriod;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Haryadi, Burhani
 *
 */
public class MUNSEmployeeAllowanceRecord extends X_UNS_EmployeeAllowanceRecord implements DocAction, DocOptions{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3641561496083493614L;

	/**
	 * @param ctx
	 * @param UNS_MedicalAllowanceRecord_ID
	 * @param trxName
	 */
	public MUNSEmployeeAllowanceRecord(Properties ctx,
			int UNS_MedicalAllowanceRecord_ID, String trxName) {
		super(ctx, UNS_MedicalAllowanceRecord_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEmployeeAllowanceRecord(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
	}
	
	private boolean hasCalculated = false;
	
	public void setHasCalculated(boolean hasCalculated)
	{
		this.hasCalculated = hasCalculated;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSEmployeeAllowanceRecord[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_PointSchema_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
		
	protected boolean beforeDelete()
	{
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
			
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		if(getLeaveClaimReserved().signum() != 0)
		{
			m_processMsg = "Leave has been used, cannot void.";
			return false;
		}
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		if(getLeavePeriodType().equals(LEAVEPERIODTYPE_YearlyLeave))
		{
			m_processMsg = "Year leave type not implemented reactivate action.";
			return false;
		}
		else
		{
			if(getLeaveClaimReserved().signum() != 0)
			{
				m_processMsg = "Leave has been used, cannot reactivate.";
				return false;
			}
		}
		setProcessed(false);

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	//@Guntur_2019-04-12 penambahan parameter AD_Org_ID karena efek dari adendum
	/**
	 * Get medical allowance record of employee which is valid for date.
	 * 
	 * @param ctx
	 * @param Employee_ID
	 * @param date
	 * @param trxName
	 * @param AD_Org_ID
	 * @return
	 */
	public static MUNSEmployeeAllowanceRecord get(Properties ctx, int Employee_ID, Timestamp date, String type, int AD_Org_ID, String trxName)
	{
		MUNSEmployeeAllowanceRecord record = null;
		
		String whereClause = "UNS_Employee_ID=? AND ? BETWEEN PeriodDateStart AND PeriodDateEnd AND AD_Org_ID = ?"
				+ " AND LeavePeriodType = ?";
		
		record = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, whereClause, trxName)
				.setParameters(Employee_ID, date,AD_Org_ID, type)
				.first();
		
		return record;
	}
	
	public static MUNSEmployeeAllowanceRecord getOfYear(Properties ctx, int Employee_ID, String year,
			String type, int OrgID, String trxName)
	{
		MUNSEmployeeAllowanceRecord record = null;
		
		String whereClause = "UNS_Employee_ID=? AND ?::numeric = DATE_PART('Year', PeriodDateStart)"
				+ " AND LeavePeriodType = ? AND AD_Org_ID = ?";
		
		record = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, whereClause, trxName)
				.setParameters(Employee_ID, year, type, OrgID)
				.first();
		
		return record;
	}
	
	/**
	 * Get medical allowance record of employee which is valid for date. If not exist, then create it based on
	 * employee's contract.
	 * 
	 * @param ctx
	 * @param employee
	 * @param date
	 * @param trxName
	 * @return
	 */
//	public static MUNSEmployeeAllowanceRecord getCreateMedical(
//			Properties ctx, MUNSEmployee employee, Timestamp date, String trxName)
//	{
//		BigDecimal employeeAllowance = MUNSPayrollLevelConfig.getMedicalAllowanceOf(ctx, employee, trxName);
//		
//		if (employeeAllowance == null)
//			return null;
//		
//		MUNSEmployeeAllowanceRecord medAllowanceRec = getCreate(ctx, employee, date, employeeAllowance, -1, trxName);
//		
//		return medAllowanceRec;
//	}
	
//	/**
//	 * Get Leave Reserved allowance record of employee which is valid for date. If not exist, then create it based on
//	 * employee's contract.
//	 * 
//	 * @param ctx
//	 * @param employee
//	 * @param date
//	 * @param trxName
//	 * @return
//	 */
//	public static MUNSEmployeeAllowanceRecord getCreateLeaveReserved(
//			Properties ctx, MUNSEmployee employee, Timestamp date, String trxName)
//	{
//		MUNSLeaveReservedConfig lrc = 
//				MUNSLeaveReservedConfig.get(ctx,
//											employee.getC_Job().getC_JobCategory_ID(), 
//											employee.getNationality(), 
//											trxName);
//		if (lrc == null)
//			return null;
//		
//		int leaveAllowance = lrc.getLeaveClaimReserved();
//		
//		MUNSEmployeeAllowanceRecord medAllowanceRec = 
//				getCreate(ctx, employee, date, new BigDecimal(-1), leaveAllowance, trxName);
//		
//		return medAllowanceRec;
//	}
	
//	public static MUNSEmployeeAllowanceRecord getCreate(
//			Properties ctx, MUNSEmployee employee, Timestamp date, String trxName)
//	{
//		MUNSLeaveReservedConfig lrc = 
//				MUNSLeaveReservedConfig.get(ctx,
//											employee.getC_Job().getC_JobCategory_ID(), 
//											employee.getNationality(), 
//											trxName);
//		if (lrc == null)
//			return null;
//		
//		BigDecimal employeeAllowance = MUNSPayrollLevelConfig.getMedicalAllowanceOf(ctx, employee, trxName);
//		
//		if (employeeAllowance == null)
//			return null;
//		
//		int leaveAllowance = lrc.getLeaveClaimReserved();
//		
//		MUNSEmployeeAllowanceRecord medAllowanceRec = 
//				getCreate(ctx, employee, date, employeeAllowance, leaveAllowance, trxName);
//		
//		return medAllowanceRec;
//	}
	
//	/**
//	 * Get medical allowance record of employee which is valid for date. If not exist, then create it based on
//	 * employee's contract.
//	 * 
//	 * @param ctx
//	 * @param employee
//	 * @param date
//	 * @param yearlyAllowanceAmt
//	 * @param trxName
//	 * @return
//	 */
//	public static MUNSEmployeeAllowanceRecord getCreate(
//			Properties ctx, MUNSEmployee employee, Timestamp date, 
//			BigDecimal yearlyAllowanceAmt, int yearlyLeaveReserved, String trxName)
//	{
//		MUNSEmployeeAllowanceRecord record = get(ctx, employee.get_ID(), date, trxName);
//
//		MUNSContractRecommendation contract = null;
//		
//		if (record == null)
//		{
//			// try to find the contract based on the date.
//			contract = MUNSContractRecommendation.getOf(ctx, employee.get_ID(), date, trxName);
//			
//			if (contract == null)
//				return null;
//		}
//		else
//			return record;
//		
//		if (yearlyAllowanceAmt.signum() < 0)
//			yearlyAllowanceAmt = MUNSPayrollLevelConfig.getMedicalAllowanceOf(ctx, employee, trxName);
//		
//		MUNSLeaveReservedConfig lrc = 
//				MUNSLeaveReservedConfig.get(ctx,
//											employee.getC_Job().getC_JobCategory_ID(), 
//											employee.getNationality(), 
//											trxName);
//		
//		if(lrc == null)
//			return null;
//		
//		if (yearlyLeaveReserved == -1)
//			yearlyLeaveReserved = lrc.getLeaveClaimReserved();
//		
//		if (yearlyAllowanceAmt.signum() <=0  && yearlyLeaveReserved <= 0)
//			return null;
//		
//		Timestamp tglStart = contract.getDateContractStart();
//		
//		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
//				employee.getAD_Org_ID()
//				, contract.getNewSectionOfDept_ID()
//				, contract.getNextContractType()
//				, Env.getContextAsDate(ctx, "Date")
//				, trxName);
//		if(null == payrollTerm)
//			payrollTerm = employee.getPayrollTerm();
//		
//		if (MUNSEmployee.PAYROLLTERM_Monthly.equals(payrollTerm))
//		{			
//			Calendar cal = Calendar.getInstance();
//			cal.set(2018, 0, 1);
//			
//			if (tglStart.before(cal.getTime())) 
//			{ // ini untuk yang sudah karyawan tetap sebelum tanggal 1/1/2017.
//				cal.setTime(date);
//				cal.set(Calendar.DATE, 1);
//				cal.set(Calendar.MONTH, 0);
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//			else { // ini untuk yang sudah karyawan tetap setelah tanggal 1/1/2017.
//				cal.setTime(contract.getDateContractStart());
//				
//				int contractMonth = cal.get(Calendar.MONTH);
//				int contractDate = cal.get(Calendar.DATE);
//				
//				Calendar theDateCal = Calendar.getInstance();
//				theDateCal.setTime(date);
//				int theDateMonth = theDateCal.get(Calendar.MONTH);
//				int theDateDate = theDateCal.get(Calendar.DATE);
//				
//				cal.set(Calendar.YEAR, theDateCal.get(Calendar.YEAR));
//				
//				if (theDateMonth <= contractMonth && theDateDate < contractDate)
//				{
//					cal.add(Calendar.YEAR, -1);
//				}
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//		}
//		else {
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(contract.getDateContractStart());
//			
//			Calendar theDateCal = Calendar.getInstance();
//			theDateCal.setTime(date);
//			int theDateYear = theDateCal.get(Calendar.YEAR);
//			
//			cal.set(Calendar.YEAR, theDateYear);
//			
//			if (cal.before(theDateCal))
//				/*
//				 * Cth: 1. tgl contract start = 1 Feb 2017, dan date = 1 Jun 2017, 
//				 *         mk stlh 'cal' di pastikan mjd th 2017 set tglStart = 1 Feb 2017.
//				 */
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			else {
//				/*
//				 * Contoh: 1. tgl contract start = 1 Jun 2013, dan date = 1 mei 2014, 
//				 *            maka kurangi tahun 'cal' mundur 1 th.
//				 * 2. tgl contract start = 1 Des 2013, dan date = 1 Feb 2014, 
//				 *    mk stlh 'cal' di jadikan tahun 1 Des 2014, kurangi lagi 1 th mjd 1 Des 2013.
//				 */
//				cal.add(Calendar.YEAR, -1);
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			}
//		}
//		
//		// Pertama test dg 1 hari sebelum tglStart di tahun berikutnya.
//		// misalkan tglStart 1 Feb 2014, maka test tglEnd dg 31 Jan 2015. Jika ternyata DateContractEnd sebelum
//		// tglEnd, maka gunakan DateContractEnd sebagai periode akhir allowance.
//		Calendar calEndAllowancePeriod = Calendar.getInstance();
//		calEndAllowancePeriod.setTime(tglStart);
//		calEndAllowancePeriod.add(Calendar.DATE, -1);
//		calEndAllowancePeriod.add(Calendar.YEAR, 1);
//		
//		Timestamp tglEnd = new Timestamp(calEndAllowancePeriod.getTimeInMillis());
//		
//		float totalDaysInPeriod = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		if (tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
//		
//		float periodDurationInDays = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		float currentContractProportion = periodDurationInDays/totalDaysInPeriod;
//		
//		BigDecimal currentAllowanceAmt = Env.ZERO;
//		if (yearlyAllowanceAmt != null && yearlyAllowanceAmt.signum() > 0)
//		{
//			currentAllowanceAmt = 
//					yearlyAllowanceAmt.multiply(BigDecimal.valueOf(currentContractProportion))
//					.setScale(0, BigDecimal.ROUND_HALF_DOWN);
//		
//			//if (currentAllowanceAmt.signum() == 0)
//			//	return record;
//		}
//		
//		float currentLeaveReserved = 0;
//		if (yearlyLeaveReserved > 0) 
//		{
//			currentLeaveReserved = yearlyLeaveReserved * currentContractProportion;
//			/*
//			MUNSEmployeeAllowanceRecord prevRecord = 
//					Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, "UNS_Employee_ID=?", trxName)
//					.setParameters(employee.get_ID())
//					.setOrderBy(COLUMNNAME_PeriodDateEnd + " DESC")
//					.first();
//			if (prevRecord != null)
//			{
//				BigDecimal lastLeaveRemaining = 
//						prevRecord.getLeaveClaimReserved().subtract(prevRecord.getLeaveReservedUsed());
//				
//				currentLeaveReserved = currentLeaveReserved + lastLeaveRemaining.floatValue();
//			}
//			*/
//		}
//			
//		record = new MUNSEmployeeAllowanceRecord(ctx, 0, trxName);
//		
//		//TODO handle period closed (not found period)
//       /*
//		I_C_Year year = MPeriod.get(ctx, tglStart, employee.getAD_Org_ID()).getC_Year();
//        */
//		MPeriod period = MPeriod.get(
//				ctx, tglStart, employee.getAD_Org_ID(), trxName);
//		if (period == null)
//		{
//			return null;
//		}
//		
//        I_C_Year year = period.getC_Year();
//		record.setC_Year_ID(year.getC_Year_ID());
//		record.setUNS_Employee_ID(employee.get_ID());
//		record.setUNS_Contract_Recommendation_ID(contract.getUNS_Contract_Recommendation_ID());
//		record.setAD_Org_ID(employee.getAD_Org_ID());
//		record.setName("Medical allowance of year " + year.getFiscalYear());
//		record.setRemarks("Allowance initialized based on contract " + contract.getDocumentNo() + " at " + 
//				 		  TimeUtil.getDay(Calendar.getInstance().getTimeInMillis()));
//		
//		record.setPeriodDateStart(tglStart);
//		record.setPeriodDateEnd(tglEnd);
//		
//		record.setMedicalAllowance(currentAllowanceAmt);
//		record.setMedicalAllowanceUsed(Env.ZERO);
//		record.setRemainingAmt(currentAllowanceAmt);
//		record.setLeaveClaimReserved(BigDecimal.valueOf(currentLeaveReserved).setScale(1, RoundingMode.HALF_UP));
//		record.setLeaveReservedUsed(Env.ZERO);
//
//		record.saveEx();
//		
//		return record;
//	}
	
	public static MUNSEmployeeAllowanceRecord getCreate_old(
			Properties ctx, MUNSEmployee employee, Timestamp date, 
			BigDecimal yearlyAllowanceAmt, String trxName)
	{
		MUNSEmployeeAllowanceRecord record = get(ctx, employee.get_ID(), date, null, employee.getAD_Org_ID(), trxName);

		MUNSContractRecommendation contract = null;
		
		if (record == null)
		{
			// try to find the contract based on the date.
			contract = MUNSContractRecommendation.getOf(ctx, employee.get_ID(), date, employee.getAD_Org_ID(), trxName);
			
			if (contract == null)
				return null;
		}
		else if (record != null &&
			record.getUNS_Contract_Recommendation_ID() == employee.getUNS_Contract_Recommendation_ID()) {
			return record;
		}
		
		if (employee.getUNS_Contract_Recommendation_ID() == 0)
			throw new AdempiereException("Cannot find contract of employee " + employee.getName());
		
		@SuppressWarnings("deprecation")
	    MPeriod period = MPeriod.get(ctx, date, employee.getAD_Org_ID());
		
		int fiscalYear = Integer.valueOf(period.getC_Year().getFiscalYear());
		
		if (null == contract)
			contract = (MUNSContractRecommendation) employee.getUNS_Contract_Recommendation();
		
		Calendar periodCal = Calendar.getInstance();
		periodCal.set(fiscalYear, 0, 1);
		
		Timestamp tglStart = new Timestamp(periodCal.getTimeInMillis());
		
		if (tglStart.before(contract.getDateContractStart())) 
			tglStart = contract.getDateContractStart();
		
		periodCal.set(fiscalYear, 11, 31);
		
		Timestamp tglEnd = new Timestamp(periodCal.getTimeInMillis());
		
		if (tglEnd.after(contract.getDateContractEnd()))
			tglEnd = contract.getDateContractEnd();
		
		float periodDurationInDays = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
		float totalFiscalDays = periodCal.getActualMaximum(Calendar.DAY_OF_YEAR);
		
		float currentContractProportion = periodDurationInDays/totalFiscalDays;
		
		BigDecimal currentAllowanceAmt = 
				yearlyAllowanceAmt.multiply(BigDecimal.valueOf(currentContractProportion))
				.setScale(0, BigDecimal.ROUND_HALF_DOWN);
		
		if (currentAllowanceAmt.signum() == 0)
			return record;
		
		if (record == null) 
		{
			record = new MUNSEmployeeAllowanceRecord(ctx, 0, trxName);
			
			record.setC_Year_ID(period.getC_Year_ID());
			record.setUNS_Employee_ID(employee.get_ID());
			record.setUNS_Contract_Recommendation_ID(contract.getUNS_Contract_Recommendation_ID());
			record.setAD_Org_ID(employee.getAD_Org_ID());
			record.setName("Medical allowance of year " + fiscalYear);
			record.setRemarks("- Init allowance based on contract " + contract.getDocumentNo() + " at " + 
					 		  TimeUtil.getDay(Calendar.getInstance().getTimeInMillis()));
			
			record.setPeriodDateStart(tglStart);
			record.setPeriodDateEnd(tglEnd);
			
			record.setMedicalAllowance(currentAllowanceAmt);
			record.setMedicalAllowanceUsed(Env.ZERO);
			record.setRemainingAmt(currentAllowanceAmt);
		}		
		else {
			record.setPeriodDateEnd(tglEnd);
			record.setUNS_Contract_Recommendation_ID(contract.getUNS_Contract_Recommendation_ID());
			record.setRemarks(record.getRemarks() + " \n" + 
							 "- Update allowance based on contract " + contract.getDocumentNo() + " at " + 
							 TimeUtil.getDay(Calendar.getInstance().getTimeInMillis()));
			record.setMedicalAllowance(record.getMedicalAllowance().add(currentAllowanceAmt));
		}
		record.saveEx();
		
		return record;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		if(getPeriodDateStart().compareTo(Timestamp.valueOf("2019-01-01 00:00:00")) < 0)
			throw new AdempiereException("Wrong date. Disallowed under 2019");
		
		setRemainingAmt(getMedicalAllowance().subtract(getMedicalAllowanceUsed()));
		
		if (getRemainingAmt().signum() < 0)
			throw new AdempiereException("Cannot set remaining amount to negative value.");
		
//		if(getLeavePeriodType().equals(LEAVEPERIODTYPE_LongLeave))
//		{
		String sql = "SELECT r.DocumentNo FROM UNS_EmployeeAllowanceRecord r"
				+ " WHERE (? BETWEEN r.PeriodDateStart AND r.PeriodDateEnd"
				+ " OR ? BETWEEN r.PeriodDateStart AND r.PeriodDateEnd)"
				+ " AND r.DocStatus NOT IN ('VO', 'RE') AND r.UNS_Employee_ID = ?"
				+ " AND r.UNS_EmployeeAllowanceRecord_ID <> ? AND r.LeavePeriodType = ?";
		String docNo = DB.getSQLValueString(get_TrxName(), sql,
				new Object[]{getPeriodDateStart(), getPeriodDateEnd(), getUNS_Employee_ID(), get_ID(), getLeavePeriodType()});
		
		if(!Util.isEmpty(docNo, true))
		{
			log.saveError("Error", "Duplicate record. " + docNo);
			return false;
		}
//		}
		
		BigDecimal remainingLeave = getRemainingLeave();
		if(!newRecord && is_ValueChanged(COLUMNNAME_LeaveReservedUsed) && remainingLeave.compareTo(Env.ZERO) == -1)
		{
			if(getLeavePeriodType().equals(LEAVEPERIODTYPE_LongLeave))
			{
				log.saveError("Error", "Over claim leave.");
				return false;
			}
			remainingLeave = remainingLeave.abs();
			MUNSLeaveReservedConfig config = MUNSLeaveReservedConfig.get(getCtx(),
					getUNS_Employee().getC_Job().getC_JobCategory_ID(), getUNS_Employee().getNationality(),
					getLeavePeriodType(), getAD_Org_ID(), true, get_TrxName());
			if(config.isDisallowNegativeLeave() || remainingLeave.compareTo(BigDecimal.valueOf(config.getMaximumAdvanceLeave()))
					== 1)
			{
				log.saveError("Error", "Over claim leave.");
				return false;
			}
		}
		
		return true;
	}
	
	public static MUNSEmployeeAllowanceRecord getCreate(Properties ctx, MUNSEmployee employee, Timestamp date,
			String type, String trxName)
	{
		if(type == null)
			type = LEAVEPERIODTYPE_YearlyLeave;
		boolean isYearLeave = type.equals(LEAVEPERIODTYPE_YearlyLeave);
		
		BigDecimal yearlyAllowanceAmt = Env.ZERO;
		BigDecimal leaveReserved = Env.ZERO;

		Calendar cal = Calendar.getInstance();
		cal.set(2019, 0, 1);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		if(date.getTime() < cal.getTimeInMillis())
		{
			date = Timestamp.valueOf("2019-01-01 00:00:00");
		}
		
//		if (record == null)
//		{
		// try to find the contract based on the date.
		MUNSContractRecommendation contract = MUNSContractRecommendation.
				getOf(ctx, employee.get_ID(), date, employee.getAD_Org_ID(), trxName);
		
		if (contract == null)
			return null;
//		}
		
		boolean isDiffLegality = contract.getUNS_Contract_Evaluation().isDifferentLegality();
		
		MUNSEmployeeAllowanceRecord record = get(ctx, employee.get_ID(), date, type, contract.getAD_Org_ID(), trxName);
		
		if(record == null && !isDiffLegality)
			record = get(ctx, employee.get_ID(), date, type, contract.getPrevDept_ID(), trxName);
		
		Calendar calNow = Calendar.getInstance();
		calNow.setTime(date);
		
		if(record != null)
		{
			if(record.getPeriodDateEnd().compareTo(contract.getDateContractEnd()) == -1)
			{
				Calendar calContract = Calendar.getInstance();
				calContract.setTime(contract.getDateContractEnd());
				if(calContract.get(Calendar.YEAR) > calNow.get(Calendar.YEAR))
				{
					calContract.set(Calendar.MONTH, 11);
					calContract.set(Calendar.DAY_OF_MONTH, 31);
					calContract.set(Calendar.YEAR, calNow.get(Calendar.YEAR));
				}
				record.setPeriodDateEnd(new Timestamp(calContract.getTimeInMillis()));
				record.saveEx();
			}
			
			return record;
		}
		else if(isYearLeave && record == null)
		{
			String sql = "SELECT COUNT(*) FROM UNS_Contract_Recommendation "
					+ " WHERE DATE_PART('Year', DateContractStart) = ? OR DATE_PART('Year', DateContractEnd) = ?"
					+ " AND DocStatus IN ('CO', 'CL') AND AD_Org_ID = ? AND UNS_Employee_ID = ?";
			int count = DB.getSQLValue(trxName, sql, new Object[]{
					calNow.get(Calendar.YEAR), calNow.get(Calendar.YEAR), contract.getAD_Org_ID(), contract.getUNS_Employee_ID()});
			if(count > 0)
			{
				record = MUNSEmployeeAllowanceRecord.getOfYear(ctx, contract.getUNS_Employee_ID(),
						String.valueOf(calNow.get(Calendar.YEAR)), type, contract.getNewDept_ID(), trxName);
				if(record != null)
				{
					calNow.set(Calendar.MONTH, 0);
					calNow.set(Calendar.DAY_OF_MONTH, 1);
					record.setPeriodDateStart(TimeUtil.trunc(new Timestamp(calNow.getTimeInMillis()), TimeUtil.TRUNC_DAY));
					record.saveEx();
					return record;
				}
			}
			return null;
		}
		
		if(!isYearLeave)
			return null;
		
		yearlyAllowanceAmt = MUNSPayrollLevelConfig.getMedicalAllowanceOf(ctx, employee, trxName);
		
		MUNSLeaveReservedConfig lrc = 
				MUNSLeaveReservedConfig.get(ctx,
											employee.getC_Job().getC_JobCategory_ID(), 
											employee.getNationality(), type, employee.getAD_Org_ID(),
											true, trxName);
		
		if(lrc == null)
			return null;
		
		Timestamp tglStart = contract.getDateContractStart();
		if(contract.getDateContractEnd().getTime() < cal.getTimeInMillis())
		{
			return null;
		}
		
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(tglStart);
		Timestamp tglEnd = null;
		if(calNow.getTimeInMillis() >= calStart.getTimeInMillis())
		{
			if(calStart.getTime().before(Timestamp.valueOf(calNow.get(Calendar.YEAR) + "-01-01 00:00:00.00")))
				calNow.setTime(Timestamp.valueOf(calNow.get(Calendar.YEAR) + "-01-01 00:00:00.00"));
			else
				calNow.setTime(tglStart);
		}
		MUNSPayrollConfiguration conf = MUNSPayrollConfiguration.get(ctx, date,
				employee.getAD_Org_ID(), trxName, true);
		MPeriod periods = MPeriod.get(ctx, date, employee.getAD_Org_ID(), trxName);
		Calendar calPeriod = Calendar.getInstance();
		cal.setTime(periods.getStartDate());
		int periodStart = lrc.isUseDateCalendar() ? calPeriod.get(Calendar.DATE) : conf.getPayrollDateStart();
		int contractMonth = calNow.get(Calendar.MONTH);
		int balanceMonth = 12 - contractMonth;
		if(lrc.isLeaveCanUseOnStartWork())
		{
			if(lrc.getAccumulatedLeavePeriod().equals(MUNSLeaveReservedConfig.ACCUMULATEDLEAVEPERIOD_Yearly))
			{
				if(cal.get(Calendar.YEAR) > calStart.get(Calendar.YEAR))
				{
//					calStart.add(Calendar.YEAR, (cal.get(Calendar.YEAR) - calStart.get(Calendar.YEAR)));
					tglStart = new Timestamp (cal.getTimeInMillis());
				}
				if(tglStart.after(cal.getTime()))
				{
					int contractDay = calNow.get(Calendar.DAY_OF_MONTH);
					if(lrc.getResetBasedOn().equals(MUNSLeaveReservedConfig.RESETBASEDON_EndOfYear))
					{
						if(contractDay > periodStart)
							balanceMonth = balanceMonth - 1;
						
						leaveReserved = (lrc.getLeaveClaimReserved().divide(BigDecimal.valueOf(12))).
								multiply(BigDecimal.valueOf(balanceMonth));
					}
					else
						leaveReserved = lrc.getLeaveClaimReserved();
				}
				else
				{
					leaveReserved = lrc.getLeaveClaimReserved();
				}
			}
			else if(lrc.getAccumulatedLeavePeriod().equals(MUNSLeaveReservedConfig.ACCUMULATEDLEAVEPERIOD_Monthly))
			{
				leaveReserved = Env.ZERO;
			}
		}
		
		if(tglStart.before(cal.getTime()))
		{
			tglStart = new Timestamp (cal.getTimeInMillis());
		}
		
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		String sql = "SELECT COUNT(*) FROM UNS_Contract_Recommendation cr"
				+ " WHERE cr.DocStatus IN ('CO', 'CL') AND cr.AD_Org_ID = ?"
				+ " AND cr.UNS_Contract_Recommendation_ID <> ?"
				+ " AND cr.DateContractStart <= ('" + now.get(Calendar.YEAR) + "-01-01')::timestamp"
				+ " AND EXISTS (SELECT 1 FROM UNS_Contract_Evaluation e"
				+ " WHERE e.UNS_Contract_Evaluation_ID = ?"
				+ " AND e.UNS_Contract_Recommendation_ID = cr.UNS_Contract_Recommendation_ID)";
		int count = DB.getSQLValue(trxName, sql, contract.getAD_Org_ID(),
				contract.get_ID(), contract.getUNS_Contract_Evaluation_ID());
		if(calStart.get(Calendar.YEAR) < now.get(Calendar.YEAR) || count > 0
				)
		{
			calStart.set(Calendar.YEAR, now.get(Calendar.YEAR));
			calStart.set(Calendar.MONTH, 0);
			calStart.set(Calendar.DATE, 1);
			tglStart = new Timestamp(calStart.getTimeInMillis());
		}
		
//		if(lrc.getResetBasedOn().equals(MUNSLeaveReservedConfig.RESETBASEDON_EndOfYear))
//		{
			cal.setTimeInMillis(calStart.getTimeInMillis());
			calNow.set(Calendar.MONTH, 11);
			calNow.set(Calendar.DAY_OF_MONTH, 31);
//		}
//		else
//			calNow.add(Calendar.YEAR, 1);
		tglEnd = new Timestamp(calNow.getTimeInMillis());
//		if(tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
		
//		if (yearlyAllowanceAmt.signum() <=0  && leaveReserved.signum() <= 0)
//			return null;
//		
//		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
//				employee.getAD_Org_ID()
//				, contract.getNewSectionOfDept_ID()
//				, contract.getNextContractType()
//				, Env.getContextAsDate(ctx, "Date")
//				, trxName);
//		if(null == payrollTerm)
//			payrollTerm = employee.getPayrollTerm();
//		
//		if (MUNSEmployee.PAYROLLTERM_Monthly.equals(payrollTerm))
//		{			
//			Calendar cal = Calendar.getInstance();
//			cal.set(2018, 0, 1);
//			
//			if (tglStart.before(cal.getTime())) 
//			{ // ini untuk yang sudah karyawan tetap sebelum tanggal 1/1/2017.
//				cal.setTime(date);
//				cal.set(Calendar.DATE, 1);
//				cal.set(Calendar.MONTH, 0);
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//			else { // ini untuk yang sudah karyawan tetap setelah tanggal 1/1/2017.
//				cal.setTime(contract.getDateContractStart());
//				
//				int contractMonth = cal.get(Calendar.MONTH);
//				int contractDate = cal.get(Calendar.DATE);
//				
//				Calendar theDateCal = Calendar.getInstance();
//				theDateCal.setTime(date);
//				int theDateMonth = theDateCal.get(Calendar.MONTH);
//				int theDateDate = theDateCal.get(Calendar.DATE);
//				
//				cal.set(Calendar.YEAR, theDateCal.get(Calendar.YEAR));
//				
//				if (theDateMonth <= contractMonth && theDateDate < contractDate)
//				{
//					cal.add(Calendar.YEAR, -1);
//				}
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//		}
//		else {
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(contract.getDateContractStart());
//			
//			Calendar theDateCal = Calendar.getInstance();
//			theDateCal.setTime(date);
//			int theDateYear = theDateCal.get(Calendar.YEAR);
//			
//			cal.set(Calendar.YEAR, theDateYear);
//			
//			if (cal.before(theDateCal))
//				/*
//				 * Cth: 1. tgl contract start = 1 Feb 2017, dan date = 1 Jun 2017, 
//				 *         mk stlh 'cal' di pastikan mjd th 2017 set tglStart = 1 Feb 2017.
//				 */
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			else {
//				/*
//				 * Contoh: 1. tgl contract start = 1 Jun 2013, dan date = 1 mei 2014, 
//				 *            maka kurangi tahun 'cal' mundur 1 th.
//				 * 2. tgl contract start = 1 Des 2013, dan date = 1 Feb 2014, 
//				 *    mk stlh 'cal' di jadikan tahun 1 Des 2014, kurangi lagi 1 th mjd 1 Des 2013.
//				 */
//				cal.add(Calendar.YEAR, -1);
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			}
//		}
//		
//		// Pertama test dg 1 hari sebelum tglStart di tahun berikutnya.
//		// misalkan tglStart 1 Feb 2014, maka test tglEnd dg 31 Jan 2015. Jika ternyata DateContractEnd sebelum
//		// tglEnd, maka gunakan DateContractEnd sebagai periode akhir allowance.
//		Calendar calEndAllowancePeriod = Calendar.getInstance();
//		calEndAllowancePeriod.setTime(tglStart);
//		calEndAllowancePeriod.add(Calendar.DATE, -1);
//		calEndAllowancePeriod.add(Calendar.YEAR, 1);
//		
//		Timestamp tglEnd = new Timestamp(calEndAllowancePeriod.getTimeInMillis());
//		
//		float totalDaysInPeriod = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		if (tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
//		
//		float periodDurationInDays = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		float currentContractProportion = periodDurationInDays/totalDaysInPeriod;
//		
		BigDecimal currentAllowanceAmt = Env.ZERO;
		if (yearlyAllowanceAmt != null && yearlyAllowanceAmt.signum() > 0)
		{
			currentAllowanceAmt = yearlyAllowanceAmt;
//					(yearlyAllowanceAmt.divide((BigDecimal.valueOf(12))
//						.multiply(BigDecimal.valueOf(balanceMonth))))
//							.setScale(0, BigDecimal.ROUND_HALF_DOWN);
		
			//if (currentAllowanceAmt.signum() == 0)
			//	return record;
		}
//		
//		BigDecimal currentLeaveReserved = Env.ZERO;
//		if (leaveReserved.compareTo(Env.ZERO) > 0) 
//		{
//			currentLeaveReserved = leaveReserved.multiply(BigDecimal.valueOf(currentContractProportion));
//			/*
//			MUNSEmployeeAllowanceRecord prevRecord = 
//					Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, "UNS_Employee_ID=?", trxName)
//					.setParameters(employee.get_ID())
//					.setOrderBy(COLUMNNAME_PeriodDateEnd + " DESC")
//					.first();
//			if (prevRecord != null)
//			{
//				BigDecimal lastLeaveRemaining = 
//						prevRecord.getLeaveClaimReserved().subtract(prevRecord.getLeaveReservedUsed());
//				
//				currentLeaveReserved = currentLeaveReserved + lastLeaveRemaining.floatValue();
//			}
//			*/
//		}
			
		record = new MUNSEmployeeAllowanceRecord(ctx, 0, trxName);
		
		//TODO handle period closed (not found period)
       /*
		I_C_Year year = MPeriod.get(ctx, tglStart, employee.getAD_Org_ID()).getC_Year();
        */
		MPeriod period = MPeriod.get(
				ctx, tglStart, employee.getAD_Org_ID(), trxName);
		if (period == null)
		{
			return null;
		}
		
        I_C_Year year = period.getC_Year();
		record.setC_Year_ID(year.getC_Year_ID());
		record.setUNS_Employee_ID(employee.get_ID());
		record.setUNS_Contract_Recommendation_ID(contract.getUNS_Contract_Recommendation_ID());
		record.setAD_Org_ID(employee.getAD_Org_ID());
		record.setName("Medical allowance of year " + year.getFiscalYear());
		record.setRemarks("Allowance initialized based on contract " + contract.getDocumentNo() + " at " + 
				 		  TimeUtil.getDay(Calendar.getInstance().getTimeInMillis()));
		
		record.setPeriodDateStart(tglStart);
		record.setPeriodDateEnd(tglEnd);
		
		record.setMedicalAllowance(currentAllowanceAmt);
		record.setMedicalAllowanceUsed(Env.ZERO);
		record.setRemainingAmt(currentAllowanceAmt);
		record.setLeaveClaimReserved(leaveReserved.setScale(1, RoundingMode.HALF_UP));
		record.setLeaveReservedUsed(Env.ZERO);
		record.setLeavePeriodType(LEAVEPERIODTYPE_YearlyLeave);
		
		MUNSEmployeeAllowanceRecord prevRecord = MUNSEmployeeAllowanceRecord.getPreviousRecord(record);
		if(prevRecord != null)
		{
			record.setPrev_Record_ID(prevRecord.get_ID());
			record.setPreviousBalance(prevRecord.getRemainingLeave());
		}
		
		record.setDocStatus("CO");
		record.setDocAction("CL");
		record.setProcessed(true);
		record.saveEx();
		
		record.setHasCalculated(true);
		
		return record;
	}
	
	//add contract for parameter
	public static MUNSEmployeeAllowanceRecord getCreate(Properties ctx, MUNSEmployee employee, Timestamp date,
			String type, MUNSContractRecommendation contract, String trxName)
	{
		if(type == null)
			type = LEAVEPERIODTYPE_YearlyLeave;
		boolean isYearLeave = type.equals(LEAVEPERIODTYPE_YearlyLeave);
		
		BigDecimal yearlyAllowanceAmt = Env.ZERO;
		BigDecimal leaveReserved = Env.ZERO;

		Calendar cal = Calendar.getInstance();
		cal.set(2019, 0, 1);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		if(date.getTime() < cal.getTimeInMillis())
		{
			date = Timestamp.valueOf("2019-01-01 00:00:00");
		}
		
//		if (record == null)
//		{
		// try to find the contract based on the date.
		if(contract == null)
			contract = MUNSContractRecommendation.
					getOf(ctx, employee.get_ID(), date, employee.getAD_Org_ID(), trxName);
		
		if (contract == null)
			return null;
//		}
		
		boolean isDiffLegality = contract.getUNS_Contract_Evaluation().isDifferentLegality();
		
		MUNSEmployeeAllowanceRecord record = get(ctx, employee.get_ID(), date, type, contract.getAD_Org_ID(), trxName);
		
		if(record == null && !isDiffLegality)
			record = get(ctx, employee.get_ID(), date, type, contract.getPrevDept_ID(), trxName);
		
		Calendar calNow = Calendar.getInstance();
		calNow.setTime(date);
		if(record != null)
		{
			if(record.getPeriodDateEnd().compareTo(contract.getDateContractEnd()) == -1)
			{
				Calendar calContract = Calendar.getInstance();
				calContract.setTime(contract.getDateContractEnd());
				if(calContract.get(Calendar.YEAR) > calNow.get(Calendar.YEAR))
				{
					calContract.set(Calendar.MONTH, 11);
					calContract.set(Calendar.DAY_OF_MONTH, 31);
					calContract.set(Calendar.YEAR, calNow.get(Calendar.YEAR));
				}
				record.setPeriodDateEnd(new Timestamp(calContract.getTimeInMillis()));
				record.saveEx();
			}
			return record;
		}
		else if(!isYearLeave)
			return null;
		
		yearlyAllowanceAmt = MUNSPayrollLevelConfig.getMedicalAllowanceOf(ctx, employee, trxName);
		
		MUNSLeaveReservedConfig lrc = 
				MUNSLeaveReservedConfig.get(ctx,
											employee.getC_Job().getC_JobCategory_ID(), 
											employee.getNationality(), type, employee.getAD_Org_ID(),
											true, trxName);
		
		if(lrc == null)
			return null;
		
		Timestamp tglStart = contract.getDateContractStart();
		if(contract.getDateContractEnd().getTime() < cal.getTimeInMillis())
		{
			return null;
		}
		
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(tglStart);
		Timestamp tglEnd = null;
		if(calNow.getTimeInMillis() >= calStart.getTimeInMillis())
		{
			if(calStart.getTime().before(Timestamp.valueOf(calNow.get(Calendar.YEAR) + "-01-01 00:00:00.00")))
				calNow.setTime(Timestamp.valueOf(calNow.get(Calendar.YEAR) + "-01-01 00:00:00.00"));
			else
				calNow.setTime(tglStart);
		}
		MUNSPayrollConfiguration conf = MUNSPayrollConfiguration.get(ctx, date,
				employee.getAD_Org_ID(), trxName, true);
		MPeriod periods = MPeriod.get(ctx, date, employee.getAD_Org_ID(), trxName);
		Calendar calPeriod = Calendar.getInstance();
		cal.setTime(periods.getStartDate());
		int periodStart = lrc.isUseDateCalendar() ? calPeriod.get(Calendar.DATE) : conf.getPayrollDateStart();
		int contractMonth = calNow.get(Calendar.MONTH);
		int balanceMonth = 12 - contractMonth;
		if(lrc.isLeaveCanUseOnStartWork())
		{
			if(lrc.getAccumulatedLeavePeriod().equals(MUNSLeaveReservedConfig.ACCUMULATEDLEAVEPERIOD_Yearly))
			{
				if(cal.get(Calendar.YEAR) > calStart.get(Calendar.YEAR))
				{
//					calStart.add(Calendar.YEAR, (cal.get(Calendar.YEAR) - calStart.get(Calendar.YEAR)));
					tglStart = new Timestamp (cal.getTimeInMillis());
				}
//				if(tglStart.after(cal.getTime()))
//				{
					int contractDay = calNow.get(Calendar.DAY_OF_MONTH);
//					if(lrc.getResetBasedOn().equals(MUNSLeaveReservedConfig.RESETBASEDON_EndOfYear))
//					{
						if(contractDay > periodStart)
						{
							balanceMonth = balanceMonth - 1;
						
						leaveReserved = (lrc.getLeaveClaimReserved().divide(BigDecimal.valueOf(12))).
								multiply(BigDecimal.valueOf(balanceMonth));
						}
//					}
//				}
				else
				{
					leaveReserved = BigDecimal.valueOf(balanceMonth);
				}
			}
			else if(lrc.getAccumulatedLeavePeriod().equals(MUNSLeaveReservedConfig.ACCUMULATEDLEAVEPERIOD_Monthly))
			{
				leaveReserved = Env.ZERO;
			}
		}
		
		if(tglStart.before(cal.getTime()))
		{
			tglStart = new Timestamp (cal.getTimeInMillis());
		}
		
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		String sql = "SELECT COUNT(*) FROM UNS_Contract_Recommendation cr"
				+ " WHERE cr.DocStatus IN ('CO', 'CL') AND cr.AD_Org_ID = ?"
				+ " AND cr.UNS_Contract_Recommendation_ID <> ?"
				+ " AND cr.DateContractStart <= ('" + now.get(Calendar.YEAR) + "-01-01')::timestamp"
				+ " AND EXISTS (SELECT 1 FROM UNS_Contract_Evaluation e"
				+ " WHERE e.UNS_Contract_Evaluation_ID = ?"
				+ " AND e.UNS_Contract_Recommendation_ID = cr.UNS_Contract_Recommendation_ID)";
		int count = DB.getSQLValue(trxName, sql, contract.getAD_Org_ID(),
				contract.get_ID(), contract.getUNS_Contract_Evaluation_ID());
		if(calStart.get(Calendar.YEAR) < now.get(Calendar.YEAR) || count > 0)
		{
			calStart.set(Calendar.YEAR, now.get(Calendar.YEAR));
			calStart.set(Calendar.MONTH, 0);
			calStart.set(Calendar.DATE, 1);
			tglStart = new Timestamp(calStart.getTimeInMillis());
		}
		
//		if(lrc.getResetBasedOn().equals(MUNSLeaveReservedConfig.RESETBASEDON_EndOfYear))
//		{
			cal.setTimeInMillis(calStart.getTimeInMillis());
			calNow.set(Calendar.MONTH, 11);
			calNow.set(Calendar.DAY_OF_MONTH, 31);
//		}
//		else
//			calNow.add(Calendar.YEAR, 1);
		tglEnd = new Timestamp(calNow.getTimeInMillis());
//		if(tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
		
//		if (yearlyAllowanceAmt.signum() <=0  && leaveReserved.signum() <= 0)
//			return null;
//		
//		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
//				employee.getAD_Org_ID()
//				, contract.getNewSectionOfDept_ID()
//				, contract.getNextContractType()
//				, Env.getContextAsDate(ctx, "Date")
//				, trxName);
//		if(null == payrollTerm)
//			payrollTerm = employee.getPayrollTerm();
//		
//		if (MUNSEmployee.PAYROLLTERM_Monthly.equals(payrollTerm))
//		{			
//			Calendar cal = Calendar.getInstance();
//			cal.set(2018, 0, 1);
//			
//			if (tglStart.before(cal.getTime())) 
//			{ // ini untuk yang sudah karyawan tetap sebelum tanggal 1/1/2017.
//				cal.setTime(date);
//				cal.set(Calendar.DATE, 1);
//				cal.set(Calendar.MONTH, 0);
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//			else { // ini untuk yang sudah karyawan tetap setelah tanggal 1/1/2017.
//				cal.setTime(contract.getDateContractStart());
//				
//				int contractMonth = cal.get(Calendar.MONTH);
//				int contractDate = cal.get(Calendar.DATE);
//				
//				Calendar theDateCal = Calendar.getInstance();
//				theDateCal.setTime(date);
//				int theDateMonth = theDateCal.get(Calendar.MONTH);
//				int theDateDate = theDateCal.get(Calendar.DATE);
//				
//				cal.set(Calendar.YEAR, theDateCal.get(Calendar.YEAR));
//				
//				if (theDateMonth <= contractMonth && theDateDate < contractDate)
//				{
//					cal.add(Calendar.YEAR, -1);
//				}
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//		}
//		else {
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(contract.getDateContractStart());
//			
//			Calendar theDateCal = Calendar.getInstance();
//			theDateCal.setTime(date);
//			int theDateYear = theDateCal.get(Calendar.YEAR);
//			
//			cal.set(Calendar.YEAR, theDateYear);
//			
//			if (cal.before(theDateCal))
//				/*
//				 * Cth: 1. tgl contract start = 1 Feb 2017, dan date = 1 Jun 2017, 
//				 *         mk stlh 'cal' di pastikan mjd th 2017 set tglStart = 1 Feb 2017.
//				 */
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			else {
//				/*
//				 * Contoh: 1. tgl contract start = 1 Jun 2013, dan date = 1 mei 2014, 
//				 *            maka kurangi tahun 'cal' mundur 1 th.
//				 * 2. tgl contract start = 1 Des 2013, dan date = 1 Feb 2014, 
//				 *    mk stlh 'cal' di jadikan tahun 1 Des 2014, kurangi lagi 1 th mjd 1 Des 2013.
//				 */
//				cal.add(Calendar.YEAR, -1);
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			}
//		}
//		
//		// Pertama test dg 1 hari sebelum tglStart di tahun berikutnya.
//		// misalkan tglStart 1 Feb 2014, maka test tglEnd dg 31 Jan 2015. Jika ternyata DateContractEnd sebelum
//		// tglEnd, maka gunakan DateContractEnd sebagai periode akhir allowance.
//		Calendar calEndAllowancePeriod = Calendar.getInstance();
//		calEndAllowancePeriod.setTime(tglStart);
//		calEndAllowancePeriod.add(Calendar.DATE, -1);
//		calEndAllowancePeriod.add(Calendar.YEAR, 1);
//		
//		Timestamp tglEnd = new Timestamp(calEndAllowancePeriod.getTimeInMillis());
//		
//		float totalDaysInPeriod = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		if (tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
//		
//		float periodDurationInDays = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		float currentContractProportion = periodDurationInDays/totalDaysInPeriod;
//		
		BigDecimal currentAllowanceAmt = Env.ZERO;
		if (yearlyAllowanceAmt != null && yearlyAllowanceAmt.signum() > 0)
		{
			currentAllowanceAmt = yearlyAllowanceAmt;
//					(yearlyAllowanceAmt.divide((BigDecimal.valueOf(12))
//						.multiply(BigDecimal.valueOf(balanceMonth))))
//							.setScale(0, BigDecimal.ROUND_HALF_DOWN);
		
			//if (currentAllowanceAmt.signum() == 0)
			//	return record;
		}
//		
//		BigDecimal currentLeaveReserved = Env.ZERO;
//		if (leaveReserved.compareTo(Env.ZERO) > 0) 
//		{
//			currentLeaveReserved = leaveReserved.multiply(BigDecimal.valueOf(currentContractProportion));
//			/*
//			MUNSEmployeeAllowanceRecord prevRecord = 
//					Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, "UNS_Employee_ID=?", trxName)
//					.setParameters(employee.get_ID())
//					.setOrderBy(COLUMNNAME_PeriodDateEnd + " DESC")
//					.first();
//			if (prevRecord != null)
//			{
//				BigDecimal lastLeaveRemaining = 
//						prevRecord.getLeaveClaimReserved().subtract(prevRecord.getLeaveReservedUsed());
//				
//				currentLeaveReserved = currentLeaveReserved + lastLeaveRemaining.floatValue();
//			}
//			*/
//		}
			
		record = new MUNSEmployeeAllowanceRecord(ctx, 0, trxName);
		
		//TODO handle period closed (not found period)
       /*
		I_C_Year year = MPeriod.get(ctx, tglStart, employee.getAD_Org_ID()).getC_Year();
        */
		MPeriod period = MPeriod.get(
				ctx, tglStart, employee.getAD_Org_ID(), trxName);
		if (period == null)
		{
			return null;
		}
		
        I_C_Year year = period.getC_Year();
		record.setC_Year_ID(year.getC_Year_ID());
		record.setUNS_Employee_ID(employee.get_ID());
		record.setUNS_Contract_Recommendation_ID(contract.getUNS_Contract_Recommendation_ID());
		record.setAD_Org_ID(employee.getAD_Org_ID());
		record.setName("Medical allowance of year " + year.getFiscalYear());
		record.setRemarks("Allowance initialized based on contract " + contract.getDocumentNo() + " at " + 
				 		  TimeUtil.getDay(Calendar.getInstance().getTimeInMillis()));
		
		record.setPeriodDateStart(tglStart);
		record.setPeriodDateEnd(tglEnd);
		
		record.setMedicalAllowance(currentAllowanceAmt);
		record.setMedicalAllowanceUsed(Env.ZERO);
		record.setRemainingAmt(currentAllowanceAmt);
		record.setLeaveClaimReserved(leaveReserved.setScale(1, RoundingMode.HALF_UP));
		record.setLeaveReservedUsed(Env.ZERO);
		record.setLeavePeriodType(LEAVEPERIODTYPE_YearlyLeave);
		
		MUNSEmployeeAllowanceRecord prevRecord = MUNSEmployeeAllowanceRecord.getPreviousRecord(record);
		if(prevRecord != null)
		{
			record.setPrev_Record_ID(prevRecord.get_ID());
			record.setPreviousBalance(prevRecord.getRemainingLeave());
		}
		
		record.setDocStatus("CO");
		record.setDocAction("CL");
		record.setProcessed(true);
		record.saveEx();
		
		record.setHasCalculated(true);
		
		return record;
	}
	
	public String reCalculateRecord(Timestamp date)
	{
		
		if(hasCalculated)
			return null;

		MUNSEmployee employee = (MUNSEmployee) getUNS_Employee();
		String type = getLeavePeriodType();
		if(!type.equals(LEAVEPERIODTYPE_YearlyLeave))
			return null;
		
		Calendar cal = Calendar.getInstance();
		cal.set(2019, 0, 1);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		if(date.getTime() < cal.getTimeInMillis())
		{
			date = Timestamp.valueOf("2019-01-01 00:00:00");
		}
		
		//getContract by date
		MUNSContractRecommendation contract = MUNSContractRecommendation.getOf(
				getCtx(), employee.get_ID(), date, employee.getAD_Org_ID(), get_TrxName());
		
		if (contract == null)
			return null;
		
		BigDecimal yearlyAllowanceAmt = Env.ZERO;
		BigDecimal leaveReserved = Env.ZERO;
		
		if(contract.getDateContractEnd().getTime() < cal.getTimeInMillis())
		{
			return null;
		}
		
	 	yearlyAllowanceAmt = MUNSPayrollLevelConfig.getMedicalAllowanceOf(
	 			getCtx(), employee, get_TrxName());
		
		MUNSLeaveReservedConfig lrc = 
				MUNSLeaveReservedConfig.get(getCtx(),
											employee.getC_Job().getC_JobCategory_ID(), 
											employee.getNationality(), type, employee.getAD_Org_ID(),
											true, get_TrxName());
		
		if(lrc == null)
			return null;
		
		Calendar calNow = Calendar.getInstance();
		Timestamp tglStart = contract.getDateContractStart();
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(tglStart);
		Timestamp tglEnd = null;
		calNow.setTime(date);
		if(calNow.getTimeInMillis() >= calStart.getTimeInMillis())
		{
			if(calStart.getTime().before(Timestamp.valueOf(calNow.get(Calendar.YEAR) + "-01-01 00:00:00.00")))
				calNow.setTime(Timestamp.valueOf(calNow.get(Calendar.YEAR) + "-01-01 00:00:00.00"));
			else
				calNow.setTime(tglStart);
		}
		MUNSPayrollConfiguration conf = MUNSPayrollConfiguration.get(getCtx(), date,
				employee.getAD_Org_ID(), get_TrxName(), true);
		MPeriod periods = MPeriod.get(getCtx(), date, employee.getAD_Org_ID(), get_TrxName());
		Calendar calPeriod = Calendar.getInstance();
		cal.setTime(periods.getStartDate());
		int periodStart = lrc.isUseDateCalendar() ? calPeriod.get(Calendar.DATE) : conf.getPayrollDateStart();
		int contractMonth = calNow.get(Calendar.MONTH);
		int balanceMonth = 12 - contractMonth;
		if(lrc.isLeaveCanUseOnStartWork())
		{
			if(lrc.getAccumulatedLeavePeriod().equals(MUNSLeaveReservedConfig.ACCUMULATEDLEAVEPERIOD_Yearly))
			{
				if(cal.get(Calendar.YEAR) > calStart.get(Calendar.YEAR))
				{
					calStart.add(Calendar.YEAR, (cal.get(Calendar.YEAR) - calStart.get(Calendar.YEAR)));
					tglStart = new Timestamp (cal.getTimeInMillis());
				}
//				if(tglStart.after(cal.getTime()))
//				{
					int contractDay = calNow.get(Calendar.DAY_OF_MONTH);
//					if(lrc.getResetBasedOn().equals(MUNSLeaveReservedConfig.RESETBASEDON_EndOfYear))
//					{
						if(contractDay > periodStart)
						{
							balanceMonth = balanceMonth - 1;
						
						leaveReserved = (lrc.getLeaveClaimReserved().divide(BigDecimal.valueOf(12))).
								multiply(BigDecimal.valueOf(balanceMonth));
						}
//					}
//				}
				else
				{
					leaveReserved = BigDecimal.valueOf(balanceMonth);
				}
			}
			else if(lrc.getAccumulatedLeavePeriod().equals(MUNSLeaveReservedConfig.ACCUMULATEDLEAVEPERIOD_Monthly))
			{
				leaveReserved = Env.ZERO;
			}
		}
		
		if(tglStart.before(cal.getTime()))
		{
			tglStart = new Timestamp (cal.getTimeInMillis());
		}
		
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		if(calStart.get(Calendar.YEAR) < now.get(Calendar.YEAR))
		{
			calStart.set(Calendar.YEAR, now.get(Calendar.YEAR));
			calStart.set(Calendar.MONTH, 0);
			calStart.set(Calendar.DATE, 1);
			tglStart = new Timestamp(calStart.getTimeInMillis());
		}
		
//		if(lrc.getResetBasedOn().equals(MUNSLeaveReservedConfig.RESETBASEDON_EndOfYear))
//		{
			cal.setTimeInMillis(calStart.getTimeInMillis());
			calNow.set(Calendar.MONTH, 11);
			calNow.set(Calendar.DAY_OF_MONTH, 31);
//		}
//		else
//			calNow.add(Calendar.YEAR, 1);
		tglEnd = new Timestamp(calNow.getTimeInMillis());
//		if(tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
		
//		if (yearlyAllowanceAmt.signum() <=0  && leaveReserved.signum() <= 0)
//			return null;
//		
//		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
//				employee.getAD_Org_ID()
//				, contract.getNewSectionOfDept_ID()
//				, contract.getNextContractType()
//				, Env.getContextAsDate(ctx, "Date")
//				, trxName);
//		if(null == payrollTerm)
//			payrollTerm = employee.getPayrollTerm();
//		
//		if (MUNSEmployee.PAYROLLTERM_Monthly.equals(payrollTerm))
//		{			
//			Calendar cal = Calendar.getInstance();
//			cal.set(2018, 0, 1);
//			
//			if (tglStart.before(cal.getTime())) 
//			{ // ini untuk yang sudah karyawan tetap sebelum tanggal 1/1/2017.
//				cal.setTime(date);
//				cal.set(Calendar.DATE, 1);
//				cal.set(Calendar.MONTH, 0);
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//			else { // ini untuk yang sudah karyawan tetap setelah tanggal 1/1/2017.
//				cal.setTime(contract.getDateContractStart());
//				
//				int contractMonth = cal.get(Calendar.MONTH);
//				int contractDate = cal.get(Calendar.DATE);
//				
//				Calendar theDateCal = Calendar.getInstance();
//				theDateCal.setTime(date);
//				int theDateMonth = theDateCal.get(Calendar.MONTH);
//				int theDateDate = theDateCal.get(Calendar.DATE);
//				
//				cal.set(Calendar.YEAR, theDateCal.get(Calendar.YEAR));
//				
//				if (theDateMonth <= contractMonth && theDateDate < contractDate)
//				{
//					cal.add(Calendar.YEAR, -1);
//				}
//				tglStart = new Timestamp(cal.getTimeInMillis());
//			}
//		}
//		else {
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(contract.getDateContractStart());
//			
//			Calendar theDateCal = Calendar.getInstance();
//			theDateCal.setTime(date);
//			int theDateYear = theDateCal.get(Calendar.YEAR);
//			
//			cal.set(Calendar.YEAR, theDateYear);
//			
//			if (cal.before(theDateCal))
//				/*
//				 * Cth: 1. tgl contract start = 1 Feb 2017, dan date = 1 Jun 2017, 
//				 *         mk stlh 'cal' di pastikan mjd th 2017 set tglStart = 1 Feb 2017.
//				 */
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			else {
//				/*
//				 * Contoh: 1. tgl contract start = 1 Jun 2013, dan date = 1 mei 2014, 
//				 *            maka kurangi tahun 'cal' mundur 1 th.
//				 * 2. tgl contract start = 1 Des 2013, dan date = 1 Feb 2014, 
//				 *    mk stlh 'cal' di jadikan tahun 1 Des 2014, kurangi lagi 1 th mjd 1 Des 2013.
//				 */
//				cal.add(Calendar.YEAR, -1);
//				tglStart = new Timestamp (cal.getTimeInMillis());
//			}
//		}
//		
//		// Pertama test dg 1 hari sebelum tglStart di tahun berikutnya.
//		// misalkan tglStart 1 Feb 2014, maka test tglEnd dg 31 Jan 2015. Jika ternyata DateContractEnd sebelum
//		// tglEnd, maka gunakan DateContractEnd sebagai periode akhir allowance.
//		Calendar calEndAllowancePeriod = Calendar.getInstance();
//		calEndAllowancePeriod.setTime(tglStart);
//		calEndAllowancePeriod.add(Calendar.DATE, -1);
//		calEndAllowancePeriod.add(Calendar.YEAR, 1);
//		
//		Timestamp tglEnd = new Timestamp(calEndAllowancePeriod.getTimeInMillis());
//		
//		float totalDaysInPeriod = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		if (tglEnd.after(contract.getDateContractEnd()))
//			tglEnd = contract.getDateContractEnd();
//		
//		float periodDurationInDays = TimeUtil.getDaysBetween(tglStart, tglEnd) + 1;
//		
//		float currentContractProportion = periodDurationInDays/totalDaysInPeriod;
//		
		BigDecimal currentAllowanceAmt = Env.ZERO;
		if (yearlyAllowanceAmt != null && yearlyAllowanceAmt.signum() > 0)
		{
			currentAllowanceAmt = yearlyAllowanceAmt;
//					(yearlyAllowanceAmt.divide((BigDecimal.valueOf(12))
//						.multiply(BigDecimal.valueOf(balanceMonth))))
//							.setScale(0, BigDecimal.ROUND_HALF_DOWN);
		
			//if (currentAllowanceAmt.signum() == 0)
			//	return record;
		}
//		
//		BigDecimal currentLeaveReserved = Env.ZERO;
//		if (leaveReserved.compareTo(Env.ZERO) > 0) 
//		{
//			currentLeaveReserved = leaveReserved.multiply(BigDecimal.valueOf(currentContractProportion));
//			/*
//			MUNSEmployeeAllowanceRecord prevRecord = 
//					Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, "UNS_Employee_ID=?", trxName)
//					.setParameters(employee.get_ID())
//					.setOrderBy(COLUMNNAME_PeriodDateEnd + " DESC")
//					.first();
//			if (prevRecord != null)
//			{
//				BigDecimal lastLeaveRemaining = 
//						prevRecord.getLeaveClaimReserved().subtract(prevRecord.getLeaveReservedUsed());
//				
//				currentLeaveReserved = currentLeaveReserved + lastLeaveRemaining.floatValue();
//			}
//			*/
//		}
		//TODO handle period closed (not found period)
	       /*
		I_C_Year year = MPeriod.get(ctx, tglStart, employee.getAD_Org_ID()).getC_Year();
        */
		MPeriod period = MPeriod.get(
				getCtx(), tglStart, employee.getAD_Org_ID(), get_TrxName());
		if (period == null)
		{
			return null;
		}
		
		I_C_Year year = period.getC_Year();
		setC_Year_ID(year.getC_Year_ID());
		setName("Medical allowance of year " + year.getFiscalYear());
		setRemarks("Allowance initialized based on contract " + contract.getDocumentNo() + " at " + 
				 		  TimeUtil.getDay(Calendar.getInstance().getTimeInMillis()));
		setPeriodDateStart(tglStart);
		setPeriodDateEnd(tglEnd);
		setMedicalAllowance(currentAllowanceAmt);
		setLeaveClaimReserved(leaveReserved.setScale(1, RoundingMode.HALF_UP));
		
		MUNSEmployeeAllowanceRecord prevRecord = MUNSEmployeeAllowanceRecord.getPreviousRecord(this);
		if(prevRecord != null)
		{
			setPrev_Record_ID(prevRecord.get_ID());
			setPreviousBalance(prevRecord.getRemainingLeave());
		}
		
		saveEx();
		
		return null;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(docStatus.equals(STATUS_Completed))
		{
			options[index++] = DOCACTION_Re_Activate;
		}
		return index;
	}
	
	public static MUNSEmployeeAllowanceRecord[] getByContract(Properties ctx, int contractID, String trxName)
	{
		List<MUNSEmployeeAllowanceRecord> record = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID,
				Table_Name, COLUMNNAME_UNS_Contract_Recommendation_ID + "=?", trxName)
					.setParameters(contractID).list();
		
		return record.toArray(new MUNSEmployeeAllowanceRecord[record.size()]);
	}
	
	public BigDecimal getRemainingLeave()
	{
		return (getLeaveClaimReserved().subtract(getLeaveReservedUsed())).add(getPreviousBalance());
	}
	
	public static MUNSEmployeeAllowanceRecord getPreviousRecord (MUNSEmployeeAllowanceRecord currRecord)
	{
		String sql = "SELECT UNS_EmployeeAllowanceRecord_ID FROM UNS_EmployeeAllowanceRecord"
				+ " WHERE UNS_Employee_ID = ? AND LeavePeriodType = ?"
				+ " AND UNS_EmployeeAllowanceRecord_ID <> ? AND PeriodDateEnd < ?"
				+ " ORDER BY PeriodDateEnd DESC";
		int prevID = DB.getSQLValue(currRecord.get_TrxName(), sql, 
						new Object[]{currRecord.getUNS_Employee_ID(), 
							currRecord.getLeavePeriodType(), currRecord.get_ID(),
								currRecord.getPeriodDateStart()});
		
		if(prevID <= 0)
			return null;
		else
			return new MUNSEmployeeAllowanceRecord(currRecord.getCtx(), prevID, currRecord.get_TrxName());
	}
	
	public void reCalcMedicalAllowance(MUNSContractRecommendation contract, boolean updateNextRecord)
	{
		BigDecimal prevYearlyAllowanceAmt = MUNSPayrollLevelConfig.getPrevMedicalAllowanceOfContract(
	 			getCtx(), contract, get_TrxName());
		BigDecimal yearlyAllowanceAmt = MUNSPayrollLevelConfig.getMedicalAllowanceOfContract(
	 			getCtx(), contract, get_TrxName());
		if(prevYearlyAllowanceAmt.compareTo(yearlyAllowanceAmt) == 0)
			return;
		if(getPeriodDateStart().after(contract.getEffectiveDate()))
		{
			setMedicalAllowance(yearlyAllowanceAmt);
			return;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(contract.getEffectiveDate());
		int month = cal.get(Calendar.MONTH);
		int balanceMonth = 12 - month;
		BigDecimal adjustAmt = Env.ZERO;
		if(balanceMonth != 12)
		{
			adjustAmt = (prevYearlyAllowanceAmt.divide(BigDecimal.valueOf(12),2)).multiply(
					BigDecimal.valueOf(balanceMonth));
			adjustAmt = adjustAmt.negate();
			adjustAmt = adjustAmt.add((yearlyAllowanceAmt.divide(BigDecimal.valueOf(12),2)).multiply(
					BigDecimal.valueOf(balanceMonth)));
			setMedicalAllowance(getMedicalAllowance().add(adjustAmt));
		}
		else
		{
			setMedicalAllowance(yearlyAllowanceAmt); 
		}
		
		if(updateNextRecord)
		{
			String sql = "SELECT r.* FROM UNS_EmployeeAllowanceRecord r"
					+ " INNER JOIN C_Year y ON y.C_Year_ID = r.C_Year_ID"
					+ " WHERE r.UNS_Employee_ID = ? AND y.FiscalYear::numeric > DATE_PART('Year', ?::timestamp)";
			PreparedStatement st = null;
			ResultSet rs = null;
			
			try {
				st = DB.prepareStatement(sql, contract.get_TrxName());
				st.setInt(1, contract.getUNS_Employee_ID());
				st.setTimestamp(2, getPeriodDateEnd());
				rs = st.executeQuery();
				while(rs.next())
				{
					MUNSEmployeeAllowanceRecord nextRecord = new MUNSEmployeeAllowanceRecord(contract.getCtx(), rs, contract.get_TrxName());
					nextRecord.reCalcMedicalAllowance(contract, false);
					nextRecord.saveEx();
				}
			} catch (SQLException e) {
				throw new AdempiereException(e.getMessage());
			}
		}
	}
	
	public static void main (String args[])
	{
		String a = "ABCD;EFGHI-K";
		int b = a.indexOf(";");
		int c = a.indexOf("-");
		System.out.println(a.substring(b+1, c));
	}
}