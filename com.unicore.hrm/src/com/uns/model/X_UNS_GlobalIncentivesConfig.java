/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_GlobalIncentivesConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_GlobalIncentivesConfig extends PO implements I_UNS_GlobalIncentivesConfig, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190104L;

    /** Standard Constructor */
    public X_UNS_GlobalIncentivesConfig (Properties ctx, int UNS_GlobalIncentivesConfig_ID, String trxName)
    {
      super (ctx, UNS_GlobalIncentivesConfig_ID, trxName);
      /** if (UNS_GlobalIncentivesConfig_ID == 0)
        {
			setC_Period_ID (0);
			setCashierPortion (Env.ZERO);
			setCashierTandemPortion (Env.ZERO);
// 0
			setGroupPercent (Env.ZERO);
			setIndividuPercent (Env.ZERO);
			setJuniorManagerPoint (Env.ZERO);
// 0
			setManagerPoint (Env.ZERO);
// 0
			setMinIndividualTarget (Env.ZERO);
// 100
			setMinSubregionTarget (Env.ZERO);
// 100
			setSCForEmployee (Env.ZERO);
// 0
			setSeniorSupervisorPoint (Env.ZERO);
			setShopType (null);
			setStaffPoint (Env.ZERO);
			setStoreSupervisorPoint (Env.ZERO);
			setSupervisorPortion (Env.ZERO);
			setSupportPortion (Env.ZERO);
// 0
			setUNS_GlobalIncentivesConfig_ID (0);
			setUNS_GlobalIncentivesConfig_UU (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_GlobalIncentivesConfig (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_GlobalIncentivesConfig[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cashier Portion (%).
		@param CashierPortion 
		Cashier Percentage from Group Percentage/Incentives Amount
	  */
	public void setCashierPortion (BigDecimal CashierPortion)
	{
		set_Value (COLUMNNAME_CashierPortion, CashierPortion);
	}

	/** Get Cashier Portion (%).
		@return Cashier Percentage from Group Percentage/Incentives Amount
	  */
	public BigDecimal getCashierPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashierPortion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cashier Tandem Portion (%).
		@param CashierTandemPortion 
		The POS Session Count portion in percentage for staff being a tandem to help main cashier in a session.
	  */
	public void setCashierTandemPortion (BigDecimal CashierTandemPortion)
	{
		set_Value (COLUMNNAME_CashierTandemPortion, CashierTandemPortion);
	}

	/** Get Cashier Tandem Portion (%).
		@return The POS Session Count portion in percentage for staff being a tandem to help main cashier in a session.
	  */
	public BigDecimal getCashierTandemPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashierTandemPortion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Group (%).
		@param GroupPercent 
		Group Incentives Percentage from Net-Sales
	  */
	public void setGroupPercent (BigDecimal GroupPercent)
	{
		set_Value (COLUMNNAME_GroupPercent, GroupPercent);
	}

	/** Get Group (%).
		@return Group Incentives Percentage from Net-Sales
	  */
	public BigDecimal getGroupPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GroupPercent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Individu (%).
		@param IndividuPercent 
		Incentives Percentage from Net-Sales for certain person
	  */
	public void setIndividuPercent (BigDecimal IndividuPercent)
	{
		set_Value (COLUMNNAME_IndividuPercent, IndividuPercent);
	}

	/** Get Individu (%).
		@return Incentives Percentage from Net-Sales for certain person
	  */
	public BigDecimal getIndividuPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_IndividuPercent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Junior Manager Point.
		@param JuniorManagerPoint 
		The point entitled for Junior Manager position
	  */
	public void setJuniorManagerPoint (BigDecimal JuniorManagerPoint)
	{
		set_Value (COLUMNNAME_JuniorManagerPoint, JuniorManagerPoint);
	}

	/** Get Junior Manager Point.
		@return The point entitled for Junior Manager position
	  */
	public BigDecimal getJuniorManagerPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JuniorManagerPoint);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Manager Point.
		@param ManagerPoint 
		The point entitled for Manager position
	  */
	public void setManagerPoint (BigDecimal ManagerPoint)
	{
		set_Value (COLUMNNAME_ManagerPoint, ManagerPoint);
	}

	/** Get Manager Point.
		@return The point entitled for Manager position
	  */
	public BigDecimal getManagerPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ManagerPoint);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Min. Individual Target (%).
		@param MinIndividualTarget Min. Individual Target (%)	  */
	public void setMinIndividualTarget (BigDecimal MinIndividualTarget)
	{
		set_Value (COLUMNNAME_MinIndividualTarget, MinIndividualTarget);
	}

	/** Get Min. Individual Target (%).
		@return Min. Individual Target (%)	  */
	public BigDecimal getMinIndividualTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinIndividualTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Min. Subregion Target (%).
		@param MinSubregionTarget Min. Subregion Target (%)	  */
	public void setMinSubregionTarget (BigDecimal MinSubregionTarget)
	{
		set_Value (COLUMNNAME_MinSubregionTarget, MinSubregionTarget);
	}

	/** Get Min. Subregion Target (%).
		@return Min. Subregion Target (%)	  */
	public BigDecimal getMinSubregionTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinSubregionTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Service Charge For Employee (%).
		@param SCForEmployee 
		Percentage of Service Charge for an employee
	  */
	public void setSCForEmployee (BigDecimal SCForEmployee)
	{
		set_Value (COLUMNNAME_SCForEmployee, SCForEmployee);
	}

	/** Get Service Charge For Employee (%).
		@return Percentage of Service Charge for an employee
	  */
	public BigDecimal getSCForEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SCForEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Senior Supervisor Point.
		@param SeniorSupervisorPoint 
		The point entitled for Senior Supervisor position
	  */
	public void setSeniorSupervisorPoint (BigDecimal SeniorSupervisorPoint)
	{
		set_Value (COLUMNNAME_SeniorSupervisorPoint, SeniorSupervisorPoint);
	}

	/** Get Senior Supervisor Point.
		@return The point entitled for Senior Supervisor position
	  */
	public BigDecimal getSeniorSupervisorPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SeniorSupervisorPoint);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Bazaar = BZ */
	public static final String SHOPTYPE_Bazaar = "BZ";
	/** Shop Retail Duty Free = DF */
	public static final String SHOPTYPE_ShopRetailDutyFree = "DF";
	/** Shop Retail Duty Paid = DP */
	public static final String SHOPTYPE_ShopRetailDutyPaid = "DP";
	/** Shop F&B = FB */
	public static final String SHOPTYPE_ShopFB = "FB";
	/** Set Shop Type.
		@param ShopType Shop Type	  */
	public void setShopType (String ShopType)
	{

		set_ValueNoCheck (COLUMNNAME_ShopType, ShopType);
	}

	/** Get Shop Type.
		@return Shop Type	  */
	public String getShopType () 
	{
		return (String)get_Value(COLUMNNAME_ShopType);
	}

	/** Set Staff Point.
		@param StaffPoint 
		The point entitled for Staff position
	  */
	public void setStaffPoint (BigDecimal StaffPoint)
	{
		set_Value (COLUMNNAME_StaffPoint, StaffPoint);
	}

	/** Get Staff Point.
		@return The point entitled for Staff position
	  */
	public BigDecimal getStaffPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StaffPoint);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Supervisor Point.
		@param StoreSupervisorPoint 
		The point entitled for Supervisor position
	  */
	public void setStoreSupervisorPoint (BigDecimal StoreSupervisorPoint)
	{
		set_Value (COLUMNNAME_StoreSupervisorPoint, StoreSupervisorPoint);
	}

	/** Get Supervisor Point.
		@return The point entitled for Supervisor position
	  */
	public BigDecimal getStoreSupervisorPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StoreSupervisorPoint);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Supervisor Portion (%).
		@param SupervisorPortion 
		(Store & Senior) Supervisor Percentage from Group Incentives Amount/Percentage
	  */
	public void setSupervisorPortion (BigDecimal SupervisorPortion)
	{
		set_Value (COLUMNNAME_SupervisorPortion, SupervisorPortion);
	}

	/** Get Supervisor Portion (%).
		@return (Store & Senior) Supervisor Percentage from Group Incentives Amount/Percentage
	  */
	public BigDecimal getSupervisorPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SupervisorPortion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Support Portion (%).
		@param SupportPortion 
		Percentage portion for the support employee (support dept)
	  */
	public void setSupportPortion (BigDecimal SupportPortion)
	{
		set_Value (COLUMNNAME_SupportPortion, SupportPortion);
	}

	/** Get Support Portion (%).
		@return Percentage portion for the support employee (support dept)
	  */
	public BigDecimal getSupportPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SupportPortion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Global Incentives Configuration.
		@param UNS_GlobalIncentivesConfig_ID Global Incentives Configuration	  */
	public void setUNS_GlobalIncentivesConfig_ID (int UNS_GlobalIncentivesConfig_ID)
	{
		if (UNS_GlobalIncentivesConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_GlobalIncentivesConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_GlobalIncentivesConfig_ID, Integer.valueOf(UNS_GlobalIncentivesConfig_ID));
	}

	/** Get Global Incentives Configuration.
		@return Global Incentives Configuration	  */
	public int getUNS_GlobalIncentivesConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_GlobalIncentivesConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Global Incentives Configuration.
		@param UNS_GlobalIncentivesConfig_UU Global Incentives Configuration	  */
	public void setUNS_GlobalIncentivesConfig_UU (String UNS_GlobalIncentivesConfig_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_GlobalIncentivesConfig_UU, UNS_GlobalIncentivesConfig_UU);
	}

	/** Get Global Incentives Configuration.
		@return Global Incentives Configuration	  */
	public String getUNS_GlobalIncentivesConfig_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_GlobalIncentivesConfig_UU);
	}
}