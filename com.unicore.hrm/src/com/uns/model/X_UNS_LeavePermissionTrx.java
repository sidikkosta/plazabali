/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_LeavePermissionTrx
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_LeavePermissionTrx extends PO implements I_UNS_LeavePermissionTrx, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191105L;

    /** Standard Constructor */
    public X_UNS_LeavePermissionTrx (Properties ctx, int UNS_LeavePermissionTrx_ID, String trxName)
    {
      super (ctx, UNS_LeavePermissionTrx_ID, trxName);
      /** if (UNS_LeavePermissionTrx_ID == 0)
        {
			setBreakTime (Env.ZERO);
// 0
			setC_Period_ID (0);
			setC_Year_ID (0);
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setEmploymentType (null);
			setIsManual (true);
// Y
			setLastLeaveUsed (Env.ZERO);
// 0
			setLeaveDateEnd (new Timestamp( System.currentTimeMillis() ));
			setLeaveDateStart (new Timestamp( System.currentTimeMillis() ));
			setLeaveRequested (Env.ZERO);
// 1
			setLeaveType (null);
			setProcessed (false);
// N
			setType (null);
// YL
			setUNS_Employee_ID (0);
			setUNS_LeavePermissionTrx_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_LeavePermissionTrx (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_LeavePermissionTrx[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set BackFromLeaveDate.
		@param BackFromLeaveDate BackFromLeaveDate	  */
	public void setBackFromLeaveDate (Timestamp BackFromLeaveDate)
	{
		set_Value (COLUMNNAME_BackFromLeaveDate, BackFromLeaveDate);
	}

	/** Get BackFromLeaveDate.
		@return BackFromLeaveDate	  */
	public Timestamp getBackFromLeaveDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_BackFromLeaveDate);
	}

	/** Set Break Time.
		@param BreakTime Break Time	  */
	public void setBreakTime (BigDecimal BreakTime)
	{
		set_Value (COLUMNNAME_BreakTime, BreakTime);
	}

	/** Get Break Time.
		@return Break Time	  */
	public BigDecimal getBreakTime () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BreakTime);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException
    {
		return (org.compiere.model.I_C_Year)MTable.get(getCtx(), org.compiere.model.I_C_Year.Table_Name)
			.getPO(getC_Year_ID(), get_TrxName());	}

	/** Set Year.
		@param C_Year_ID 
		Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID)
	{
		if (C_Year_ID < 1) 
			set_Value (COLUMNNAME_C_Year_ID, null);
		else 
			set_Value (COLUMNNAME_C_Year_ID, Integer.valueOf(C_Year_ID));
	}

	/** Get Year.
		@return Calendar Year
	  */
	public int getC_Year_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Year_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Company = COM */
	public static final String EMPLOYMENTTYPE_Company = "COM";
	/** Sub Contract = SUB */
	public static final String EMPLOYMENTTYPE_SubContract = "SUB";
	/** Set Employment Type.
		@param EmploymentType Employment Type	  */
	public void setEmploymentType (String EmploymentType)
	{

		set_Value (COLUMNNAME_EmploymentType, EmploymentType);
	}

	/** Get Employment Type.
		@return Employment Type	  */
	public String getEmploymentType () 
	{
		return (String)get_Value(COLUMNNAME_EmploymentType);
	}

	/** Set End Time.
		@param EndTime 
		End of the time span
	  */
	public void setEndTime (Timestamp EndTime)
	{
		set_Value (COLUMNNAME_EndTime, EndTime);
	}

	/** Get End Time.
		@return End of the time span
	  */
	public Timestamp getEndTime () 
	{
		return (Timestamp)get_Value(COLUMNNAME_EndTime);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Manual.
		@param IsManual 
		This is a manual process
	  */
	public void setIsManual (boolean IsManual)
	{
		set_Value (COLUMNNAME_IsManual, Boolean.valueOf(IsManual));
	}

	/** Get Manual.
		@return This is a manual process
	  */
	public boolean isManual () 
	{
		Object oo = get_Value(COLUMNNAME_IsManual);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.uns.model.I_UNS_Employee getJobCareTaker() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getJobCareTaker_ID(), get_TrxName());	}

	/** Set Job Care Taker.
		@param JobCareTaker_ID Job Care Taker	  */
	public void setJobCareTaker_ID (int JobCareTaker_ID)
	{
		if (JobCareTaker_ID < 1) 
			set_Value (COLUMNNAME_JobCareTaker_ID, null);
		else 
			set_Value (COLUMNNAME_JobCareTaker_ID, Integer.valueOf(JobCareTaker_ID));
	}

	/** Get Job Care Taker.
		@return Job Care Taker	  */
	public int getJobCareTaker_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_JobCareTaker_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Last Leave Used.
		@param LastLeaveUsed Last Leave Used	  */
	public void setLastLeaveUsed (BigDecimal LastLeaveUsed)
	{
		set_Value (COLUMNNAME_LastLeaveUsed, LastLeaveUsed);
	}

	/** Get Last Leave Used.
		@return Last Leave Used	  */
	public BigDecimal getLastLeaveUsed () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LastLeaveUsed);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Leave Date End.
		@param LeaveDateEnd Leave Date End	  */
	public void setLeaveDateEnd (Timestamp LeaveDateEnd)
	{
		set_Value (COLUMNNAME_LeaveDateEnd, LeaveDateEnd);
	}

	/** Get Leave Date End.
		@return Leave Date End	  */
	public Timestamp getLeaveDateEnd () 
	{
		return (Timestamp)get_Value(COLUMNNAME_LeaveDateEnd);
	}

	/** Set Leave Date Start.
		@param LeaveDateStart Leave Date Start	  */
	public void setLeaveDateStart (Timestamp LeaveDateStart)
	{
		set_Value (COLUMNNAME_LeaveDateStart, LeaveDateStart);
	}

	/** Get Leave Date Start.
		@return Leave Date Start	  */
	public Timestamp getLeaveDateStart () 
	{
		return (Timestamp)get_Value(COLUMNNAME_LeaveDateStart);
	}

	/** Set Leave Destination.
		@param LeaveDestination Leave Destination	  */
	public void setLeaveDestination (String LeaveDestination)
	{
		set_Value (COLUMNNAME_LeaveDestination, LeaveDestination);
	}

	/** Get Leave Destination.
		@return Leave Destination	  */
	public String getLeaveDestination () 
	{
		return (String)get_Value(COLUMNNAME_LeaveDestination);
	}

	/** Full Day = AFD */
	public static final String LEAVEPERIODTYPE_FullDay = "AFD";
	/** End Date is Half Day = EHD */
	public static final String LEAVEPERIODTYPE_EndDateIsHalfDay = "EHD";
	/** Start and End Date is Half Day = SEHD */
	public static final String LEAVEPERIODTYPE_StartAndEndDateIsHalfDay = "SEHD";
	/** Start Date is Half Day = SHD */
	public static final String LEAVEPERIODTYPE_StartDateIsHalfDay = "SHD";
	/** Short Time = SHT */
	public static final String LEAVEPERIODTYPE_ShortTime = "SHT";
	/** Set Leave Period Type.
		@param LeavePeriodType 
		Select one of the list based on the requested period.
	  */
	public void setLeavePeriodType (String LeavePeriodType)
	{

		set_Value (COLUMNNAME_LeavePeriodType, LeavePeriodType);
	}

	/** Get Leave Period Type.
		@return Select one of the list based on the requested period.
	  */
	public String getLeavePeriodType () 
	{
		return (String)get_Value(COLUMNNAME_LeavePeriodType);
	}

	/** Set Leave Requested.
		@param LeaveRequested Leave Requested	  */
	public void setLeaveRequested (BigDecimal LeaveRequested)
	{
		set_Value (COLUMNNAME_LeaveRequested, LeaveRequested);
	}

	/** Get Leave Requested.
		@return Leave Requested	  */
	public BigDecimal getLeaveRequested () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LeaveRequested);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Leave / Cuti = LCTI */
	public static final String LEAVETYPE_LeaveCuti = "LCTI";
	/** Permission (Dispensation) / Izin Dibayar = PMDB */
	public static final String LEAVETYPE_PermissionDispensationIzinDibayar = "PMDB";
	/** Permission (Dinas) = PMDN */
	public static final String LEAVETYPE_PermissionDinas = "PMDN";
	/** Pay Permission / Izin Potong Gaji = PPAY */
	public static final String LEAVETYPE_PayPermissionIzinPotongGaji = "PPAY";
	/** Maternity / Hamil+Melahirkan = MLHR */
	public static final String LEAVETYPE_MaternityHamilPlusMelahirkan = "MLHR";
	/** Surat Keterangan Istirahat = SKI */
	public static final String LEAVETYPE_SuratKeteranganIstirahat = "SKI";
	/** Surat Keterangan Istirahat Kecelakaan Kerja = SKIKK */
	public static final String LEAVETYPE_SuratKeteranganIstirahatKecelakaanKerja = "SKIKK";
	/** Other = OTHR */
	public static final String LEAVETYPE_Other = "OTHR";
	/** Set Leave Type.
		@param LeaveType Leave Type	  */
	public void setLeaveType (String LeaveType)
	{

		set_Value (COLUMNNAME_LeaveType, LeaveType);
	}

	/** Get Leave Type.
		@return Leave Type	  */
	public String getLeaveType () 
	{
		return (String)get_Value(COLUMNNAME_LeaveType);
	}

	/** Set Other Remarks.
		@param OtherRemarks Other Remarks	  */
	public void setOtherRemarks (String OtherRemarks)
	{
		set_Value (COLUMNNAME_OtherRemarks, OtherRemarks);
	}

	/** Get Other Remarks.
		@return Other Remarks	  */
	public String getOtherRemarks () 
	{
		return (String)get_Value(COLUMNNAME_OtherRemarks);
	}

	/** Set Print Leave Permission.
		@param PrintLeavepermission Print Leave Permission	  */
	public void setPrintLeavepermission (String PrintLeavepermission)
	{
		set_Value (COLUMNNAME_PrintLeavepermission, PrintLeavepermission);
	}

	/** Get Print Leave Permission.
		@return Print Leave Permission	  */
	public String getPrintLeavepermission () 
	{
		return (String)get_Value(COLUMNNAME_PrintLeavepermission);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Remarks.
		@param Remarks Remarks	  */
	public void setRemarks (String Remarks)
	{
		set_Value (COLUMNNAME_Remarks, Remarks);
	}

	/** Get Remarks.
		@return Remarks	  */
	public String getRemarks () 
	{
		return (String)get_Value(COLUMNNAME_Remarks);
	}

	/** Set Start Time.
		@param StartTime 
		Time started
	  */
	public void setStartTime (Timestamp StartTime)
	{
		set_Value (COLUMNNAME_StartTime, StartTime);
	}

	/** Get Start Time.
		@return Time started
	  */
	public Timestamp getStartTime () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartTime);
	}

	/** Set Total Leave Claim Reserved.
		@param TotalLeaveClaimReserved Total Leave Claim Reserved	  */
	public void setTotalLeaveClaimReserved (BigDecimal TotalLeaveClaimReserved)
	{
		set_ValueNoCheck (COLUMNNAME_TotalLeaveClaimReserved, TotalLeaveClaimReserved);
	}

	/** Get Total Leave Claim Reserved.
		@return Total Leave Claim Reserved	  */
	public BigDecimal getTotalLeaveClaimReserved () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLeaveClaimReserved);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Long Leave = LL */
	public static final String TYPE_LongLeave = "LL";
	/** Yearly Leave = YL */
	public static final String TYPE_YearlyLeave = "YL";
	/** Set Type.
		@param Type 
		Type of Validation (SQL, Java Script, Java Language)
	  */
	public void setType (String Type)
	{

		set_Value (COLUMNNAME_Type, Type);
	}

	/** Get Type.
		@return Type of Validation (SQL, Java Script, Java Language)
	  */
	public String getType () 
	{
		return (String)get_Value(COLUMNNAME_Type);
	}

	public com.uns.model.I_UNS_DispensationConfig getUNS_DispensationConfig() throws RuntimeException
    {
		return (com.uns.model.I_UNS_DispensationConfig)MTable.get(getCtx(), com.uns.model.I_UNS_DispensationConfig.Table_Name)
			.getPO(getUNS_DispensationConfig_ID(), get_TrxName());	}

	/** Set Dispensation.
		@param UNS_DispensationConfig_ID Dispensation	  */
	public void setUNS_DispensationConfig_ID (int UNS_DispensationConfig_ID)
	{
		if (UNS_DispensationConfig_ID < 1) 
			set_Value (COLUMNNAME_UNS_DispensationConfig_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_DispensationConfig_ID, Integer.valueOf(UNS_DispensationConfig_ID));
	}

	/** Get Dispensation.
		@return Dispensation	  */
	public int getUNS_DispensationConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DispensationConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_LeavePermissionGroup getUNS_LeavePermissionGroup() throws RuntimeException
    {
		return (com.uns.model.I_UNS_LeavePermissionGroup)MTable.get(getCtx(), com.uns.model.I_UNS_LeavePermissionGroup.Table_Name)
			.getPO(getUNS_LeavePermissionGroup_ID(), get_TrxName());	}

	/** Set Leave Permission Group.
		@param UNS_LeavePermissionGroup_ID Leave Permission Group	  */
	public void setUNS_LeavePermissionGroup_ID (int UNS_LeavePermissionGroup_ID)
	{
		if (UNS_LeavePermissionGroup_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_LeavePermissionGroup_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_LeavePermissionGroup_ID, Integer.valueOf(UNS_LeavePermissionGroup_ID));
	}

	/** Get Leave Permission Group.
		@return Leave Permission Group	  */
	public int getUNS_LeavePermissionGroup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_LeavePermissionGroup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_LeavePermissionTrx.
		@param UNS_LeavePermissionTrx_ID UNS_LeavePermissionTrx	  */
	public void setUNS_LeavePermissionTrx_ID (int UNS_LeavePermissionTrx_ID)
	{
		if (UNS_LeavePermissionTrx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_LeavePermissionTrx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_LeavePermissionTrx_ID, Integer.valueOf(UNS_LeavePermissionTrx_ID));
	}

	/** Get UNS_LeavePermissionTrx.
		@return UNS_LeavePermissionTrx	  */
	public int getUNS_LeavePermissionTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_LeavePermissionTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_LeavePermissionTrx_UU.
		@param UNS_LeavePermissionTrx_UU UNS_LeavePermissionTrx_UU	  */
	public void setUNS_LeavePermissionTrx_UU (String UNS_LeavePermissionTrx_UU)
	{
		set_Value (COLUMNNAME_UNS_LeavePermissionTrx_UU, UNS_LeavePermissionTrx_UU);
	}

	/** Get UNS_LeavePermissionTrx_UU.
		@return UNS_LeavePermissionTrx_UU	  */
	public String getUNS_LeavePermissionTrx_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_LeavePermissionTrx_UU);
	}

	public com.uns.model.I_UNS_YearlyPresenceSummary getUNS_YearlyPresenceSummary() throws RuntimeException
    {
		return (com.uns.model.I_UNS_YearlyPresenceSummary)MTable.get(getCtx(), com.uns.model.I_UNS_YearlyPresenceSummary.Table_Name)
			.getPO(getUNS_YearlyPresenceSummary_ID(), get_TrxName());	}

	/** Set Yearly Presence Summary.
		@param UNS_YearlyPresenceSummary_ID Yearly Presence Summary	  */
	public void setUNS_YearlyPresenceSummary_ID (int UNS_YearlyPresenceSummary_ID)
	{
		if (UNS_YearlyPresenceSummary_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_YearlyPresenceSummary_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_YearlyPresenceSummary_ID, Integer.valueOf(UNS_YearlyPresenceSummary_ID));
	}

	/** Get Yearly Presence Summary.
		@return Yearly Presence Summary	  */
	public int getUNS_YearlyPresenceSummary_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_YearlyPresenceSummary_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}