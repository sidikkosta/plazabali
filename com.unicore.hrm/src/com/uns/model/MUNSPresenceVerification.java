package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSPresenceVerification extends X_UNS_PresenceVerification 
						implements DocAction, DocOptions {

	/**
	 * @author Hamba Allah
	 */
	
	private static final long serialVersionUID = 7773012516332205102L;
	
	private MUNSEmpPresenceVerification[] m_lines= null;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;

	public MUNSPresenceVerification(Properties ctx,
			int UNS_PresenceVerification_ID, String trxName) {
		super(ctx, UNS_PresenceVerification_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSPresenceVerification(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSEmpPresenceVerification[] getLines() {
		
		List<MUNSEmpPresenceVerification> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSEmpPresenceVerification.Table_Name,
				"UNS_PresenceVerification_ID = ? AND isActive = ?", get_TrxName())
				.setParameters(getUNS_PresenceVerification_ID(), "Y")
				.list();
		
		m_lines = list.toArray(new MUNSEmpPresenceVerification[list.size()]);
		
		return m_lines;
	}

	@Override
	public boolean processIt(String processAction) throws Exception {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if ((newRecord || is_ValueChanged(COLUMNNAME_C_BPartner_ID) 
				|| is_ValueChanged(COLUMNNAME_PresenceDate) 
				|| is_ValueChanged(COLUMNNAME_AD_Org_ID)))
		{
			if(isDuplicate())
			{
				log.saveError("Error",  
						"Duplicate Monthly Validation Record");
				return false;
			}
			if(getLines().length > 0)
			{
				log.saveError("Error",  
						"Document has lines, cannot change it");
				return false;
			}
		}
		
		return super.beforeSave(newRecord);
	}

	@Override
	public String prepareIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(null == getLines())
		{
			m_processMsg = "No Lines";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String completeIt() {

		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//		Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//update daily presence
		for(MUNSEmpPresenceVerification line : m_lines)
		{
			MUNSDailyPresence day = line.getDailyPresence();
			if(day == null)
				continue;
			
			if(!line.isAbsence())
			{
				day.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_FullDay);
				if(day.getFSTimeIn() == null)
					day.setFSTimeIn(day.getTimeInRules());
				
				if(day.getFSTimeOut() == null)
					day.setFSTimeOut(day.getTimeOutRules());
			}
			else
			{
				day.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Mangkir);
				day.setFSTimeIn(null);
				day.setFSTimeOut(null);
			}
			
			day.setUNS_PresenceVerification_ID(get_ID());
			day.saveEx();
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		//update daily presence
//		for(MUNSEmpPresenceVerification line : m_lines)
//		{
//			MUNSDailyPresence day = line.getDailyPresence();
//			if(day == null)
//				continue;
//			
//			day.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Mangkir);
//			
//			if(day.getFSTimeIn() == null)
//				day.setFSTimeIn(day.getTimeInRules());
//			
//			if(day.getFSTimeOut() == null)
//				day.setFSTimeOut(day.getTimeOutRules());
//			
//			day.setUNS_PresenceVerification_ID(get_ID());
//			day.saveEx();
//		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = "Under Development";
		return false;
	}

	@Override
	public boolean closeIt() {
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		
		m_processMsg = "Disallowed Reverse Correct It";
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		
		m_processMsg = "Disallowed Reverse Accrual It";
		return false;
	}

	@Override
	public boolean reActivateIt() {
		
		m_processMsg = "Under Development";
		
		return false;
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean isDuplicate ()
	{
		StringBuilder sb = new StringBuilder("SELECT COUNT (*) FROM ")
		.append(Table_Name).append(" WHERE ").append(COLUMNNAME_DocStatus)
		.append(" NOT IN (?,?,?,?) AND ").append(Table_Name).append("_ID <> ? ")
		.append(" AND ").append(COLUMNNAME_IsActive).append(" = ? ")
		.append(" AND ").append(COLUMNNAME_C_BPartner_ID).append(" = ? AND ")
		.append(COLUMNNAME_PresenceDate).append(" = ? AND ")
		.append(COLUMNNAME_AD_Org_ID).append(" =?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(DOCSTATUS_Completed);
		params.add(DOCSTATUS_Closed);
		params.add(DOCSTATUS_Reversed);
		params.add(DOCSTATUS_Voided);
		params.add(get_ID());
		params.add("Y");
		params.add(getC_BPartner_ID());
		params.add(getPresenceDate());
		params.add(getAD_Org_ID());
		String sql = sb.toString();
		int record = DB.getSQLValue(get_TrxName(), sql, params);
		return record > 0;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		
		if(docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Void;
			options[index++] = DOCACTION_Re_Activate;
		}
		
		return index;
	}
	
}
