package com.uns.model.callout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.I_UNS_DeductionAdjReq_Line;
import com.uns.model.MUNSDeductionAdjReq;
import com.uns.model.MUNSDeductionAdjReqLine;
import com.uns.model.MUNSEmployeeLoan;
import com.uns.model.MUNSPeriodicCostBenefit;

public class CalloutDeductionAdj implements IColumnCallout {

	public CalloutDeductionAdj() {
		
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		String retVal = null;
		
		if(mTab.getTableName().equals(MUNSDeductionAdjReqLine.Table_Name))
		{
			if(mField.getColumnName().equals(I_UNS_DeductionAdjReq_Line.COLUMNNAME_CostBenefitType))
				retVal = updateAmt(ctx, WindowNo, mTab, mField, value);
		}
		
		
		return retVal;
	}
	
	protected String updateAmt(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value) {
		
		String type = value.toString();
		
		if(type == null)
			return null;
	
		int parentID = (Integer) mTab.getValue(MUNSDeductionAdjReqLine.COLUMNNAME_UNS_DeductionAdjReq_ID);
		MUNSDeductionAdjReq parent = new MUNSDeductionAdjReq(ctx, parentID, null);
		
		BigDecimal remainingAmt = remainingAmt(type, parent);
		mTab.setValue(MUNSDeductionAdjReqLine.COLUMNNAME_RemainingAmt, remainingAmt);
		
		BigDecimal amount = Env.ZERO;
		if(type.equals(MUNSDeductionAdjReqLine.COSTBENEFITTYPE_PinjamanKaryawan))
			amount = installment(ctx, parent);
		else
			amount = remainingAmt;
		
		mTab.setValue(MUNSDeductionAdjReqLine.COLUMNNAME_AdjustmentAmt, amount);
		
		return null;
	}
	
	private BigDecimal remainingAmt(String type, MUNSDeductionAdjReq parent) {
		
		BigDecimal remainingAmt = Env.ZERO;
		
		if(type.equals(MUNSDeductionAdjReqLine.COSTBENEFITTYPE_PinjamanKaryawan))
		{
			String sql = "SELECT COALESCE(SUM(LoanAmtLeft),0) FROM UNS_Employee_Loan"
					+ " WHERE UNS_Employee_ID = ? AND DocStatus IN ('CO','CL')";
			remainingAmt = DB.getSQLValueBD(null, sql, parent.getUNS_Employee_ID());
		}
		else
		{
			String costBenType = null;
			if(type.equals(MUNSDeductionAdjReqLine.COSTBENEFITTYPE_Canteen))
				costBenType = MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Canteen;
			else if(type.equals(MUNSDeductionAdjReqLine.COSTBENEFITTYPE_Cooperative))
				costBenType = MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Cooperative;
			else
				return Env.ZERO;
			
			String sql = "SELECT COALESCE(RemainingAmount,0) FROM UNS_PeriodicCostBenefitLine pl"
					+ " INNER JOIN UNS_PeriodicCostBenefit p ON p.UNS_PeriodicCostBenefit_ID = pl.UNS_PeriodicCostBenefit_ID"
					+ " WHERE pl.isActive = 'Y' AND pl.UNS_Employee_ID = ? AND p.C_Period_ID = ?"
					+ " AND p.CostBenefitType = ? AND isBenefit = 'N' AND DocStatus IN ('CO','CL')";
			remainingAmt = DB.getSQLValueBD(
					null, sql, parent.getUNS_Employee_ID(), parent.getC_Period_ID()
					, costBenType);
		}
		
		return remainingAmt;
	}
	
	private BigDecimal installment(Properties ctx, MUNSDeductionAdjReq parent) {
		
		BigDecimal amount = Env.ZERO;
		double totalInstallment = 0.0;
		
		List<MUNSEmployeeLoan> loans = MUNSEmployeeLoan.gets(ctx, parent.getUNS_Employee_ID(), null);
		
		for (MUNSEmployeeLoan loan : loans)
		{
			if (loan.getLoanAmtLeft().compareTo(BigDecimal.ZERO) <= 0)
				continue;
			if (loan.getLoanAmtLeft().compareTo(loan.getInstallment()) >= 0)
			{
//				if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Koperasi))
//					totalInstallmentKoprasi += employeeLoan.getInstallment().doubleValue();
//				else //if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Company))
				totalInstallment += loan.getInstallment().doubleValue();
			} 
			else 
			{
//				if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Koperasi))
//					totalInstallmentKoprasi += employeeLoan.getLoanAmtLeft().doubleValue();
//				else //if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Company))
				totalInstallment += loan.getLoanAmtLeft().doubleValue();
			}
		}
		amount = new BigDecimal(totalInstallment).setScale(0, RoundingMode.HALF_UP);
		
		return amount;
	}
}
