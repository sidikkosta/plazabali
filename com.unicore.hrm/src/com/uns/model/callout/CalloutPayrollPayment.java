/**
 * 
 */
package com.uns.model.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MColumn;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;

import com.uns.model.MUNSPayrollEmployee;
import com.uns.model.MUNSPayrollPaymentEmp;

/**
 * @author menjangan
 *
 */
public class CalloutPayrollPayment implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutPayrollPayment() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		if (MUNSPayrollPaymentEmp.COLUMNNAME_UNS_Payroll_Employee_ID.equals(mField.getColumnName()))
			return onPayrollSelected(ctx, WindowNo, mTab, mField, value, oldValue);
		return null;
	}

	private String onPayrollSelected (Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (value == null)
		{
			mTab.setValue(MUNSPayrollPaymentEmp.COLUMNNAME_Amount, Env.ZERO);
			return null;
		}
		
		GridTab bankAcct = mTab.getParentTab();
		if (bankAcct == null)
			return null;
		GridTab header = bankAcct.getParentTab();
		if (header == null)
			return null;
		String sql = "SELECT COALESCE (TakeHomePay, 0) FROM UNS_Payroll_Employee WHERE UNS_Payroll_Employee_ID = ?";
		BigDecimal thp = DB.getSQLValueBD(null, sql, value);
		MColumn column = MColumn.get(ctx, MUNSPayrollEmployee.Table_Name, MUNSPayrollEmployee.COLUMNNAME_TakeHomePay);
		if (column.isEncrypted())
			thp = (BigDecimal) SecureEngine.decrypt(thp, Env.getAD_Client_ID(ctx));
		if (thp == null)
			thp = Env.ZERO;
		mTab.setValue(MUNSPayrollPaymentEmp.COLUMNNAME_Amount, thp);
		
		return null;
	}
}
