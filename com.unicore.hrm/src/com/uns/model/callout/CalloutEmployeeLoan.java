/**
 * 
 */
package com.uns.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.uns.model.I_UNS_Employee_Loan;
import com.uns.model.MUNSEmployeeLoan;
import com.uns.model.MUNSEmployeeLoanGroup;

/**
 * @author Burhani Adam
 *
 */
public class CalloutEmployeeLoan implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutEmployeeLoan() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		if(mTab.getTableName().equals(MUNSEmployeeLoan.Table_Name))
		{
			if(mField.getColumnName().equals(MUNSEmployeeLoan.COLUMNNAME_UNS_Employee_Loan_Group_ID))
				return parent(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		
		return null;
	}
	
	private String parent(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(value == null || (Integer) value == 0)
			return null;
		
		MUNSEmployeeLoanGroup group = new MUNSEmployeeLoanGroup(ctx, (Integer) value, null);
		I_UNS_Employee_Loan loan = GridTabWrapper.create(mTab, I_UNS_Employee_Loan.class);
		loan.setAD_Org_ID(group.getAD_Org_ID());
		loan.setC_DocType_ID(group.getC_DocType_ID());
		loan.setTrxDate(group.getTrxDate());
		loan.setC_Charge_ID(group.getC_Charge_ID());
		loan.setLoanType(group.getLoanType());
		
		return null;
	}
}