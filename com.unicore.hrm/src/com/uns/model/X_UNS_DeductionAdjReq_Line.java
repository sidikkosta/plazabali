/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_DeductionAdjReq_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_DeductionAdjReq_Line extends PO implements I_UNS_DeductionAdjReq_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180330L;

    /** Standard Constructor */
    public X_UNS_DeductionAdjReq_Line (Properties ctx, int UNS_DeductionAdjReq_Line_ID, String trxName)
    {
      super (ctx, UNS_DeductionAdjReq_Line_ID, trxName);
      /** if (UNS_DeductionAdjReq_Line_ID == 0)
        {
			setAdjustmentAmt (Env.ZERO);
// 0
        } */
    }

    /** Load Constructor */
    public X_UNS_DeductionAdjReq_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_DeductionAdjReq_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Adjustment Amount.
		@param AdjustmentAmt Adjustment Amount	  */
	public void setAdjustmentAmt (BigDecimal AdjustmentAmt)
	{
		set_Value (COLUMNNAME_AdjustmentAmt, AdjustmentAmt);
	}

	/** Get Adjustment Amount.
		@return Adjustment Amount	  */
	public BigDecimal getAdjustmentAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AdjustmentAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Canteen = CNT */
	public static final String COSTBENEFITTYPE_Canteen = "CNT";
	/** Cooperative = CPS */
	public static final String COSTBENEFITTYPE_Cooperative = "CPS";
	/** Pinjaman Karyawan = PKR */
	public static final String COSTBENEFITTYPE_PinjamanKaryawan = "PKR";
	/** Bonuses = BNS */
	public static final String COSTBENEFITTYPE_Bonuses = "BNS";
	/** Set Cost / Benefit Type.
		@param CostBenefitType Cost / Benefit Type	  */
	public void setCostBenefitType (String CostBenefitType)
	{

		set_Value (COLUMNNAME_CostBenefitType, CostBenefitType);
	}

	/** Get Cost / Benefit Type.
		@return Cost / Benefit Type	  */
	public String getCostBenefitType () 
	{
		return (String)get_Value(COLUMNNAME_CostBenefitType);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Remaining Amt.
		@param RemainingAmt 
		Remaining Amount
	  */
	public void setRemainingAmt (BigDecimal RemainingAmt)
	{
		set_ValueNoCheck (COLUMNNAME_RemainingAmt, RemainingAmt);
	}

	/** Get Remaining Amt.
		@return Remaining Amount
	  */
	public BigDecimal getRemainingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_DeductionAdjReq getUNS_DeductionAdjReq() throws RuntimeException
    {
		return (com.uns.model.I_UNS_DeductionAdjReq)MTable.get(getCtx(), com.uns.model.I_UNS_DeductionAdjReq.Table_Name)
			.getPO(getUNS_DeductionAdjReq_ID(), get_TrxName());	}

	/** Set Deduction Adjustment Request.
		@param UNS_DeductionAdjReq_ID Deduction Adjustment Request	  */
	public void setUNS_DeductionAdjReq_ID (int UNS_DeductionAdjReq_ID)
	{
		if (UNS_DeductionAdjReq_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DeductionAdjReq_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DeductionAdjReq_ID, Integer.valueOf(UNS_DeductionAdjReq_ID));
	}

	/** Get Deduction Adjustment Request.
		@return Deduction Adjustment Request	  */
	public int getUNS_DeductionAdjReq_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DeductionAdjReq_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Deduction Adjustment Request Line.
		@param UNS_DeductionAdjReq_Line_ID Deduction Adjustment Request Line	  */
	public void setUNS_DeductionAdjReq_Line_ID (int UNS_DeductionAdjReq_Line_ID)
	{
		if (UNS_DeductionAdjReq_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_DeductionAdjReq_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_DeductionAdjReq_Line_ID, Integer.valueOf(UNS_DeductionAdjReq_Line_ID));
	}

	/** Get Deduction Adjustment Request Line.
		@return Deduction Adjustment Request Line	  */
	public int getUNS_DeductionAdjReq_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DeductionAdjReq_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_DeductionAdjReq_Line_UU.
		@param UNS_DeductionAdjReq_Line_UU UNS_DeductionAdjReq_Line_UU	  */
	public void setUNS_DeductionAdjReq_Line_UU (String UNS_DeductionAdjReq_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_DeductionAdjReq_Line_UU, UNS_DeductionAdjReq_Line_UU);
	}

	/** Get UNS_DeductionAdjReq_Line_UU.
		@return UNS_DeductionAdjReq_Line_UU	  */
	public String getUNS_DeductionAdjReq_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_DeductionAdjReq_Line_UU);
	}
}