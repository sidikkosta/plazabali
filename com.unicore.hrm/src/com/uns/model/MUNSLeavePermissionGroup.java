/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSLeavePermissionGroup extends X_UNS_LeavePermissionGroup implements DocAction, DocOptions {
	
	private boolean m_justPrepared = false;

	/**
	 * 
	 */
	private static final long serialVersionUID = -7788357462758358125L;

	/**
	 * @param ctx
	 * @param UNS_LeavePermissionGroup_ID
	 * @param trxName
	 */
	public MUNSLeavePermissionGroup(Properties ctx,
			int UNS_LeavePermissionGroup_ID, String trxName) {
		super(ctx, UNS_LeavePermissionGroup_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSLeavePermissionGroup(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSLeavePermissionTrx[] getLines()
	{
		List<MUNSLeavePermissionTrx> list = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSLeavePermissionTrx.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSLeavePermissionTrx[list.size()]);
	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		// 
		if (docStatus.equals(DocumentEngine.STATUS_Drafted)
    			|| docStatus.equals(DocumentEngine.STATUS_Invalid)) {
    		options[index++] = DocumentEngine.ACTION_Prepare;
    	}
    	
    	// If status = Completed, add "Reactivte" in the list
    	if (docStatus.equals(DocumentEngine.STATUS_Completed)) {
    		options[index++] = DocumentEngine.ACTION_Void;
    	}   	
    		
    	return index;
	}
	
	private String m_processMsg = null;
	
	@Override
	public boolean processIt(String action) throws Exception {
		// 
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		// 
		log.info(toString());
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() {
		// 
		log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() {
		// 
		log.info(toString());
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() {
		// 
		log.info(toString());
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		// 
		log.info(toString());
		
		//if (getJobCareTaker_ID()==0)
		//	throw new FillMandatoryException(COLUMNNAME_JobCareTaker_ID);
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
	
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() {
		// 
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		log.info(toString());
		
//		Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		MUNSLeavePermissionTrx[] lines = getLines();
		if(lines.length == 0)
		{
			m_processMsg = "@NoLines@";
			return DocAction.STATUS_Invalid;
		}
		
		for(int i=0;i<lines.length;i++)
		{
			try {
				if(!lines[i].processIt(ACTION_Complete) || !lines[i].save())
				{
					m_processMsg = lines[i].getProcessMsg();
					return DocAction.STATUS_Invalid;
				}
			} catch (Exception e) {
				m_processMsg = e.getMessage();
			}
		}
		
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);	
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		MUNSLeavePermissionTrx[] lines = getLines();
		
		for(int i=0;i<lines.length;i++)
		{
			try {
				if(!lines[i].processIt(ACTION_Void) || !lines[i].save())
				{
					m_processMsg = lines[i].getProcessMsg();
					return false;
				}
			} catch (Exception e) {
				m_processMsg = e.getMessage();
			}
		}
		
		if (m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_None);
		setDocStatus(STATUS_Voided);
		setProcessed(true);
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt() {
		// 
		log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() {
		// 
		return false;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() {
		// 
		return false;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt() {
		// 
		log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return false;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() {
		
		StringBuffer sb = new StringBuffer();
		
		String sql = "SELECT Name FROM AD_Org WHERE AD_Org_ID = ?";
		String orgName = DB.getSQLValueString(get_TrxName(), sql, getAD_Org_ID());
		sb.append("#Organization : "+orgName);
		
		sql = "SELECT Name FROM UNS_Employee WHERE UNS_Employee_ID = ?";
		String empName = DB.getSQLValueString(get_TrxName(), sql, getUNS_Employee_ID());
		sb.append(" #Employee :"+ empName);
		
		return sb.toString();
	} 
	
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentNo()
	 */
	@Override
	public String getDocumentNo() {
		// 
		return null;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() {
		// 
		return null;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() {
		// 
		return null;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() {
		// 
		return m_processMsg;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() {
		// 
		return 0;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		// 
		return 0;
	}

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() {
		// 
		return null;
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord || is_ValueChanged(COLUMNNAME_UNS_Employee_ID)
				|| is_ValueChanged(COLUMNNAME_C_Period_ID))
		{
			int lenght = TimeUtil.getDaysBetween(getC_Period().getStartDate(), getC_Period().getEndDate());
			Timestamp start = getC_Period().getStartDate();
			MUNSEmployeeAllowanceRecord record = null;
			for(int i=0;i<=lenght;i++)
			{
				start = TimeUtil.addDays(start, i);
				record = MUNSEmployeeAllowanceRecord.getCreate(
						getCtx(), (MUNSEmployee) getUNS_Employee(), start, getType(), get_TrxName());
				if(record != null)
					break;
			}
			if(record == null)
			{
				log.saveError("Error", "Employee doesn't have allowance record.");
				return false;
			}
			setTotalLeaveClaimReserved(record.getLeaveClaimReserved().add(record.getPreviousBalance()));
			setRemainingQty(getTotalLeaveClaimReserved().subtract(record.getLeaveReservedUsed()));
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_LeavePermissionTrx WHERE UNS_LeavePermissionGroup_ID = ?";
		
		return DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0;
	}
}