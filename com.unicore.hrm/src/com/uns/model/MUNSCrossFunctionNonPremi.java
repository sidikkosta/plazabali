package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSCrossFunctionNonPremi extends X_UNS_CrossFunction_NonPremi
		implements DocAction {

	/**
	 * @author Hamba Allah
	 */
	private static final long serialVersionUID = 4302893903528602449L;
	
	public String m_processMsg = null;
	public boolean m_justPrepared = false; 
	public MUNSEmpCFNonPremi[] m_lines = null;

	public MUNSCrossFunctionNonPremi(Properties ctx,
			int UNS_CrossFunction_NonPremi_ID, String trxName) {
		super(ctx, UNS_CrossFunction_NonPremi_ID, trxName);
		
	}

	public MUNSCrossFunctionNonPremi(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		
	}

	@Override
	public boolean processIt(String action) throws Exception {
		
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}
	
	public static MUNSCrossFunctionNonPremi getByDocNo(Properties ctx, String _docNo, String trxName) {
		
		return Query.get(
				ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name
				, COLUMNNAME_DocumentNo + " = '" + _docNo+"' ",
				trxName)
				.firstOnly();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		String sql = "SELECT 1 FROM UNS_CrossFunction_NonPremi WHERE DocumentNo = ?"
				+ " AND UNS_CrossFunction_NonPremi_ID <> ?";
		boolean exists = DB.getSQLValue(get_TrxName(), sql, getDocumentNo(), get_ID()) != -1;
		if(exists)
		{
			log.saveError("Save Error", "Document no was exists");
		}
		
		sql = "SELECT DocumentNo FROM UNS_CrossFunction_NonPremi WHERE UNS_CrossFunction_NonPremi_ID <> ?"
				+ " AND C_Period_ID = ? AND DocStatus IN ('DR','IP','IN') AND AD_OrgTrx_ID = ?";
		String docNo = DB.getSQLValueString(get_TrxName(), sql, get_ID(), getC_Period_ID() , getAD_OrgTrx_ID());
		if(docNo != null)
		{
			log.saveError("Duplicate", "There was exists document still open.  Document No : "+docNo);
			return false;
		}
		
		return super.beforeSave(newRecord);
	}
	
	public MUNSEmpCFNonPremi[] getLines(boolean requery) {
		
		if (m_lines != null && !requery) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSEmpCFNonPremi> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSEmpCFNonPremi.Table_Name,
				MUNSCrossFunctionNonPremi.Table_Name+"_ID = ? AND IsActive = ?", get_TrxName())
				.setParameters(get_ID(), "Y")
				.list();
		
		m_lines = new MUNSEmpCFNonPremi[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}

	@Override
	public String prepareIt() {
		if(log.isLoggable(Level.INFO))
			log.info(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		getLines(true);
		
		if(m_lines.length <= 0)
		{
			m_processMsg = "No Lines.";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	@Override
	public String completeIt() {
		
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		if (log.isLoggable(Level.INFO))
			log.config(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		getLines(true);
		
		StringBuilder msgBuilder = null;
		
		for(MUNSEmpCFNonPremi line : m_lines)
		{
			if(line.isDisbursed())
			{
				String sqql = "SELECT CONCAT(Value,'_', Name) FROM UNS_Employee WHERE UNS_Employee_ID = ?";
				String nameEmp = DB.getSQLValueString(get_TrxName(), sqql, line.getUNS_Employee_ID());
				
				if(msgBuilder == null)
					msgBuilder = new StringBuilder(nameEmp);
				else
					msgBuilder = msgBuilder.append(", ").append(nameEmp);
			}
		}
		
		String msg = msgBuilder.toString();
		if(!Util.isEmpty(msg, true))
		{
			m_processMsg = "Cannot Void it. There was employee has been disbursed. Please Contact Administrator."
					+ " #Employee : "+msg;
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public boolean closeIt() {
		if (log.isLoggable(Level.INFO))
			log.config(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		
		m_processMsg = "Disallowed Reverse Correct";
		
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		
		m_processMsg = "Disallowed Reverse Accrual";
		
		return false;
	}

	@Override
	public boolean reActivateIt() {
		if(log.isLoggable(Level.INFO))
			log.config(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if(m_processMsg != null)
			return false;
		
		getLines(true);
		
		StringBuilder msgBuilder = null;
		
		for(MUNSEmpCFNonPremi line : m_lines)
		{
			if(line.isDisbursed())
			{
				String sqql = "SELECT CONCAT(Value,'_', Name) FROM UNS_Employee WHERE UNS_Employee_ID = ?";
				String nameEmp = DB.getSQLValueString(get_TrxName(), sqql, line.getUNS_Employee_ID());
				
				if(msgBuilder == null)
					msgBuilder = new StringBuilder(nameEmp);
				else
					msgBuilder = msgBuilder.append(", ").append(nameEmp);
			}
		}
		
		String msg = msgBuilder.toString();
		if(!Util.isEmpty(msg, true))
		{
			m_processMsg = "Cannot Reactive it. There was employee has been disbursed. Please Contact Administrator."
					+ " #Employee : "+msg;
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public String getSummary() {
		
		return null;
	}

	@Override
	public String getDocumentInfo() {
		StringBuilder info = new StringBuilder();
		
		info.append(getCreated());
		
		return info.toString();
	}

	@Override
	public File createPDF() {
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}
	
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF

	@Override
	public String getProcessMsg() {
		
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		
		return null;
	}

}
