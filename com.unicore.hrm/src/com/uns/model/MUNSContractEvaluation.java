/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.wf.MWorkflow;

import com.uns.model.process.CreateNewContract;

/**
 * @author eko
 *
 */
public class MUNSContractEvaluation extends X_UNS_Contract_Evaluation
					implements DocAction, DocOptions, IUNSApprovalInfo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param ctx
	 * @param UNS_Contract_Evaluation_ID
	 * @param trxName
	 */
	public MUNSContractEvaluation(Properties ctx,
			int UNS_Contract_Evaluation_ID, String trxName) {
		super(ctx, UNS_Contract_Evaluation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSContractEvaluation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Set inactive for all preveouse contract recomendation.
	 * @return
	 */
	public boolean inActivePrevContracRecommendation()
	{
		/*
		List<MUNSContractRecommendation> listOfContractRecommendation = MUNSContractRecommendation.get(
												getCtx(), getUNS_Contract_Evaluation_ID(), get_TrxName());
		for (MUNSContractRecommendation cr : listOfContractRecommendation)
		{
			cr.setIsActive(false);
			if (!cr.save())
				return false;
		}
		*/
		return true;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		
		if (docStatus.equals(DocumentEngine.STATUS_Drafted)
    			|| docStatus.equals(DocumentEngine.STATUS_Invalid)) {
    		options[index++] = DocumentEngine.ACTION_Prepare;
    	}
    	
    	// If status = Completed, add "Reactivte" in the list
    	if (docStatus.equals(DocumentEngine.STATUS_Completed)) {
    		options[index++] = DocumentEngine.ACTION_Close;
    		options[index++] = DocumentEngine.ACTION_Void;
    	}  
    	
    	if(docStatus.equals(DocumentEngine.STATUS_NotApproved)) {
    		options[index++] = DocumentEngine.ACTION_Complete;
    	}
    		
    	return index;
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(getUNS_Contract_Recommendation_ID() > 0 && getRecommendation().equals(RECOMMENDATION_ContractTermination))
		{
			if(getLastEndContractDate().after(getUNS_Contract_Recommendation().getDateContractEnd()))
			{
				log.saveError("Error", "Date contract end can't bigger than current contract.");
				return false;
			}
		}
		
		return true;
	}

	private String m_processMsg = null;
	@Override
	public boolean processIt(String action) throws Exception {
		// TODO Auto-generated method stub
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		setProcessed(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	private boolean m_justPrepared = false;
	
	@Override
	public String prepareIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;

		// pindah ke completeit() karena ada approval
//		if (!isCreatedContract() && !RECOMMENDATION_ContractTermination.equals(getRecommendation())) {
//			try {
//				CreateNewContract create = new CreateNewContract(getCtx(), get_TrxName());
//				create.processIt(this);
//			} catch (Exception e) {
//				
//				throw new AdempiereUserError("Error while trying create new contract.\n"+ e.getMessage());
//			}
//		}
		
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		// TODO Auto-generated method stub
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		log.info(toString());
		
//		Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
		{
			setProcessed(false);
			return DocAction.STATUS_Invalid;
		}
		
		if (!isCreatedContract() && !RECOMMENDATION_ContractTermination.equals(getRecommendation())
				&& !RECOMMENDATION_No_Recommendation.equals(getRecommendation())) {
			try {
				CreateNewContract create = new CreateNewContract(getCtx(), get_TrxName());
				create.processIt(this);
			} catch (Exception e) {
				setProcessed(false);
				throw new AdempiereUserError("Error while trying create new contract.\n"+ e.getMessage());
			}
		}
		
		//if contract termination inActiveEmployee.
		if (null == getRecommendation())
		{
			m_processMsg = "Null value of recommendation";
			setProcessed(false);
			return DocAction.STATUS_Invalid;
		}
		
		if (getRecommendation().equals(RECOMMENDATION_ContractTermination))
		{
			MUNSEmployee employee = (MUNSEmployee)getUNS_Employee();
			MUNSContractRecommendation contractRecommend = 
					new MUNSContractRecommendation(getCtx(), getUNS_Contract_Recommendation_ID(), get_TrxName());
//			contractRecommend.setIsActive(false);
			contractRecommend.setDateContractEnd(getLastEndContractDate());
			contractRecommend.saveEx();
//			MUNSPayrollBaseEmployee pbEmployee = 
//					MUNSPayrollBaseEmployee.getPrev(getCtx(), getUNS_Employee_ID(), get_TrxName());
//			if(null != pbEmployee)
//			{
//				pbEmployee.setIsActive(false);
//				pbEmployee.save();
//			}
			employee.setIsTerminate(true);
			if(!employee.save())
				throw new AdempiereException("Failed to update employee");
		} else if(isCreatedContract()) {
			MUNSContractRecommendation contractRecommend = 
					MUNSContractRecommendation.get(getCtx(), getUNS_Contract_Evaluation_ID(), get_TrxName());
			if(contractRecommend==null)
				return DocAction.STATUS_InProgress;
			else if (!contractRecommend.isApproved()){
				m_processMsg = "Please Complete Contract Recommendation [" 
												+ contractRecommend.getDocumentNo() + "]";
				setProcessed(true);
				setDocAction(DOCACTION_Complete);
				return DocAction.STATUS_InProgress;
			}
		} else
			throw new AdempiereUserError("Create new contract before complete."); 
	

		setProcessed(true);	
		//m_processMsg = info.toString();
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		log.info(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (getRecommendation().equals(RECOMMENDATION_ContractTermination))
		{
			MUNSEmployee employee = (MUNSEmployee)getUNS_Employee();
			MUNSContractRecommendation contractRecommend = 
					new MUNSContractRecommendation(getCtx(), getUNS_Contract_Recommendation_ID(), get_TrxName());
			contractRecommend.setIsActive(true);
			contractRecommend.save();

			MUNSPayrollBaseEmployee pbEmployee = MUNSPayrollBaseEmployee.get(getCtx(), contractRecommend.get_ID(), get_TrxName());
			if(null != pbEmployee)
			{
				pbEmployee.setIsActive(true);
				pbEmployee.saveEx();
			}
			employee.setIsTerminate(false);
			if(!employee.save())
				throw new AdempiereException("Failed to update employee");
		}
		else if(isCreatedContract()) {
			MUNSContractRecommendation contractRecommend = 
					MUNSContractRecommendation.get(getCtx(), getUNS_Contract_Evaluation_ID(), get_TrxName());
			if(contractRecommend != null)
			{
				try
				{
					ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(contractRecommend, DocAction.ACTION_Void);
					if(pi.isError())
					{
						m_processMsg = pi.getSummary();
						return false;
					}
				}
				catch (Exception e)
				{
					m_processMsg = e.getLocalizedMessage();
					return false;
				}
			}
			
		} else
			throw new AdempiereUserError("Create new contract before complete."); 
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reActivateIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSummary() {
		
		StringBuffer sb = new StringBuffer();
		
		String sql = "SELECT Name FROM AD_Org WHERE AD_Org_ID = ?";
		String orgName = DB.getSQLValueString(get_TrxName(), sql, getAD_Org_ID());
		sb.append("#Organization : "+orgName);
		
		return sb.toString();
	}

	@Override
	public String getDocumentNo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isCreatedContract()
	{
		return "Y".equals(getCreateNewSalary());
	}

	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		
		List<Object[]> list = new ArrayList<>();
		list.add(new Object[]{String.class, true});		//employee
		list.add(new Object[]{String.class, true});		//Last Start Contract Date
		list.add(new Object[]{String.class, true});		//Last End Contract Date
		list.add(new Object[]{String.class, true});		//Recommendation
		list.add(new Object[]{String.class, true});		//Description
		
		return list;
	}

	@Override
	public String[] getDetailTableHeader() {
		
		String def[] = new String[]{"Employee", "Last Start Contract Date","Last End Contract Date"
				,"Recommendation","Description"}; 
		
		return def;
	}

	@Override
	public List<Object[]> getDetailTableContent() {
		
		List<Object[]> list = new ArrayList<>();
		
		String sql = "SELECT CONCAT(e.Value,'_',e.Name) AS Employee,"						//1.. employee
				+ " ce.LastContractDate,"													//2.. Last Contract Date
				+ " ce.LastEndContractDate,"												//3.. Last End COntract Date
				+ " ce.Recommendation,"														//4.. Recommendation
				+ " ce.Description"															//5.. Description
				+ " FROM UNS_Contract_Evaluation ce"
				+ " INNER JOIN UNS_Employee e ON e.UNS_Employee_ID = ce.UNS_Employee_ID"
				+ " WHERE ce.UNS_Contract_Evaluation_ID = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, get_ID());
			rs = st.executeQuery();
			
			while(rs.next())
			{
				String startDate = new SimpleDateFormat("dd-MMM-YYYY").format(rs.getObject(2));
				String endDate = new SimpleDateFormat("dd-MMM-YYYY").format(rs.getObject(3));
				
				int count = 0;
				Object[] rowData = new Object[5];
				rowData[count] = rs.getObject(1);											//employee
				rowData[++count] = startDate;												//Last Start Contract Date
				rowData[++count] = endDate;													//Last End Contract Date
				rowData[++count] = getRecommendationName(rs.getObject(4).toString());		//Recommendation
				rowData[++count] = rs.getObject(5);											//Description
				
				list.add(rowData);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			DB.close(rs, st);
		}
		return list;
	}
	
	private String getRecommendationName(String _recommendation) {
		
		String retVal = null;
		
		switch (_recommendation) {
		case MUNSContractEvaluation.RECOMMENDATION_Contract2:
			retVal = "Contract 2";
			break;
		case MUNSContractEvaluation.RECOMMENDATION_ContractTermination:
			retVal = "Contract Termination";
			break;
		case MUNSContractEvaluation.RECOMMENDATION_DemotedToPosition:
			retVal = "Demote To Position";
			break;
		case MUNSContractEvaluation.RECOMMENDATION_PromotedToPosition:
			retVal = "Promote to Position";
			break;
		case MUNSContractEvaluation.RECOMMENDATION_Re_Contract:
			retVal = "Re-Contract";
			break;
		case MUNSContractEvaluation.RECOMMENDATION_SequenceContract:
			retVal = "Sequence Contract";
			break;
		case MUNSContractEvaluation.RECOMMENDATION_ToPermanenStatus:
			retVal = "to Permanent Status";
			break;
		}
		
		return retVal;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}
