/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_ShopTargetConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ShopTargetConfig extends PO implements I_UNS_ShopTargetConfig, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181007L;

    /** Standard Constructor */
    public X_UNS_ShopTargetConfig (Properties ctx, int UNS_ShopTargetConfig_ID, String trxName)
    {
      super (ctx, UNS_ShopTargetConfig_ID, trxName);
      /** if (UNS_ShopTargetConfig_ID == 0)
        {
			setNetSalesTarget (Env.ZERO);
			setShop_ID (0);
			setUNS_ShopTargetConfig_ID (0);
			setUNS_ShopTargetConfig_UU (null);
			setUNS_SubregionTargetConfig_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ShopTargetConfig (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ShopTargetConfig[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Net Sales Target.
		@param NetSalesTarget 
		Net sales target from all stores in a certain subregion
	  */
	public void setNetSalesTarget (BigDecimal NetSalesTarget)
	{
		set_Value (COLUMNNAME_NetSalesTarget, NetSalesTarget);
	}

	/** Get Net Sales Target.
		@return Net sales target from all stores in a certain subregion
	  */
	public BigDecimal getNetSalesTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getShop_ID(), get_TrxName());	}

	/** Set Shop_ID.
		@param Shop_ID 
		Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID)
	{
		if (Shop_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, Integer.valueOf(Shop_ID));
	}

	/** Get Shop_ID.
		@return Identifies a Shop
	  */
	public int getShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Shop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ShopTargetConfig_ID.
		@param UNS_ShopTargetConfig_ID UNS_ShopTargetConfig_ID	  */
	public void setUNS_ShopTargetConfig_ID (int UNS_ShopTargetConfig_ID)
	{
		if (UNS_ShopTargetConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopTargetConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopTargetConfig_ID, Integer.valueOf(UNS_ShopTargetConfig_ID));
	}

	/** Get UNS_ShopTargetConfig_ID.
		@return UNS_ShopTargetConfig_ID	  */
	public int getUNS_ShopTargetConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShopTargetConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ShopTargetConfig_UU.
		@param UNS_ShopTargetConfig_UU UNS_ShopTargetConfig_UU	  */
	public void setUNS_ShopTargetConfig_UU (String UNS_ShopTargetConfig_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ShopTargetConfig_UU, UNS_ShopTargetConfig_UU);
	}

	/** Get UNS_ShopTargetConfig_UU.
		@return UNS_ShopTargetConfig_UU	  */
	public String getUNS_ShopTargetConfig_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ShopTargetConfig_UU);
	}

	/** Set UNS_SubregionTargetConfig_ID.
		@param UNS_SubregionTargetConfig_ID UNS_SubregionTargetConfig_ID	  */
	public void setUNS_SubregionTargetConfig_ID (int UNS_SubregionTargetConfig_ID)
	{
		if (UNS_SubregionTargetConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionTargetConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionTargetConfig_ID, Integer.valueOf(UNS_SubregionTargetConfig_ID));
	}

	/** Get UNS_SubregionTargetConfig_ID.
		@return UNS_SubregionTargetConfig_ID	  */
	public int getUNS_SubregionTargetConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SubregionTargetConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}