/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_LeavePermissionGroup
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_LeavePermissionGroup extends PO implements I_UNS_LeavePermissionGroup, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191122L;

    /** Standard Constructor */
    public X_UNS_LeavePermissionGroup (Properties ctx, int UNS_LeavePermissionGroup_ID, String trxName)
    {
      super (ctx, UNS_LeavePermissionGroup_ID, trxName);
      /** if (UNS_LeavePermissionGroup_ID == 0)
        {
			setC_Period_ID (0);
			setC_Year_ID (0);
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setEmploymentType (null);
			setLeavePeriodType (null);
// AFD
			setLeaveRequested (Env.ZERO);
// 0
			setLeaveType (null);
			setRemainingQty (Env.ZERO);
// 0
			setTotalLeaveClaimReserved (Env.ZERO);
// 0
			setType (null);
			setUNS_Employee_ID (0);
			setUNS_LeavePermissionGroup_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_LeavePermissionGroup (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_LeavePermissionGroup[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create lines from.
		@param CreateFrom 
		Process which will generate a new document lines based on an existing document
	  */
	public void setCreateFrom (String CreateFrom)
	{
		set_Value (COLUMNNAME_CreateFrom, CreateFrom);
	}

	/** Get Create lines from.
		@return Process which will generate a new document lines based on an existing document
	  */
	public String getCreateFrom () 
	{
		return (String)get_Value(COLUMNNAME_CreateFrom);
	}

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException
    {
		return (org.compiere.model.I_C_Year)MTable.get(getCtx(), org.compiere.model.I_C_Year.Table_Name)
			.getPO(getC_Year_ID(), get_TrxName());	}

	/** Set Year.
		@param C_Year_ID 
		Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID)
	{
		if (C_Year_ID < 1) 
			set_Value (COLUMNNAME_C_Year_ID, null);
		else 
			set_Value (COLUMNNAME_C_Year_ID, Integer.valueOf(C_Year_ID));
	}

	/** Get Year.
		@return Calendar Year
	  */
	public int getC_Year_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Year_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Company = COM */
	public static final String EMPLOYMENTTYPE_Company = "COM";
	/** Sub Contract = SUB */
	public static final String EMPLOYMENTTYPE_SubContract = "SUB";
	/** Set Employment Type.
		@param EmploymentType Employment Type	  */
	public void setEmploymentType (String EmploymentType)
	{

		set_Value (COLUMNNAME_EmploymentType, EmploymentType);
	}

	/** Get Employment Type.
		@return Employment Type	  */
	public String getEmploymentType () 
	{
		return (String)get_Value(COLUMNNAME_EmploymentType);
	}

	/** Full Day = AFD */
	public static final String LEAVEPERIODTYPE_FullDay = "AFD";
	/** End Date is Half Day = EHD */
	public static final String LEAVEPERIODTYPE_EndDateIsHalfDay = "EHD";
	/** Start and End Date is Half Day = SEHD */
	public static final String LEAVEPERIODTYPE_StartAndEndDateIsHalfDay = "SEHD";
	/** Start Date is Half Day = SHD */
	public static final String LEAVEPERIODTYPE_StartDateIsHalfDay = "SHD";
	/** Short Time = SHT */
	public static final String LEAVEPERIODTYPE_ShortTime = "SHT";
	/** Set Leave Period Type.
		@param LeavePeriodType 
		Select one of the list based on the requested period.
	  */
	public void setLeavePeriodType (String LeavePeriodType)
	{

		set_Value (COLUMNNAME_LeavePeriodType, LeavePeriodType);
	}

	/** Get Leave Period Type.
		@return Select one of the list based on the requested period.
	  */
	public String getLeavePeriodType () 
	{
		return (String)get_Value(COLUMNNAME_LeavePeriodType);
	}

	/** Set Leave Requested.
		@param LeaveRequested Leave Requested	  */
	public void setLeaveRequested (BigDecimal LeaveRequested)
	{
		set_Value (COLUMNNAME_LeaveRequested, LeaveRequested);
	}

	/** Get Leave Requested.
		@return Leave Requested	  */
	public BigDecimal getLeaveRequested () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LeaveRequested);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Leave / Cuti = LCTI */
	public static final String LEAVETYPE_LeaveCuti = "LCTI";
	/** Ijin Resmi = PMDB */
	public static final String LEAVETYPE_IjinResmi = "PMDB";
	/** Business Trip = PMDN */
	public static final String LEAVETYPE_BusinessTrip = "PMDN";
	/** Pay Permission / Izin Potong Gaji = PPAY */
	public static final String LEAVETYPE_PayPermissionIzinPotongGaji = "PPAY";
	/** Maternity / Hamil+Melahirkan = MLHR */
	public static final String LEAVETYPE_MaternityHamilPlusMelahirkan = "MLHR";
	/** Surat Keterangan Sakit = SKI */
	public static final String LEAVETYPE_SuratKeteranganSakit = "SKI";
	/** Surat Keterangan Istirahat Kecelakaan Kerja = SKIKK */
	public static final String LEAVETYPE_SuratKeteranganIstirahatKecelakaanKerja = "SKIKK";
	/** Other = OTHR */
	public static final String LEAVETYPE_Other = "OTHR";
	/** Set LeaveType.
		@param LeaveType LeaveType	  */
	public void setLeaveType (String LeaveType)
	{

		set_Value (COLUMNNAME_LeaveType, LeaveType);
	}

	/** Get LeaveType.
		@return LeaveType	  */
	public String getLeaveType () 
	{
		return (String)get_Value(COLUMNNAME_LeaveType);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Remaining Qty.
		@param RemainingQty 
		The total remaining quantity
	  */
	public void setRemainingQty (BigDecimal RemainingQty)
	{
		set_Value (COLUMNNAME_RemainingQty, RemainingQty);
	}

	/** Get Remaining Qty.
		@return The total remaining quantity
	  */
	public BigDecimal getRemainingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Leave Claim Reserved.
		@param TotalLeaveClaimReserved Total Leave Claim Reserved	  */
	public void setTotalLeaveClaimReserved (BigDecimal TotalLeaveClaimReserved)
	{
		set_Value (COLUMNNAME_TotalLeaveClaimReserved, TotalLeaveClaimReserved);
	}

	/** Get Total Leave Claim Reserved.
		@return Total Leave Claim Reserved	  */
	public BigDecimal getTotalLeaveClaimReserved () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLeaveClaimReserved);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Long Leave = LL */
	public static final String TYPE_LongLeave = "LL";
	/** Yearly Leave = YL */
	public static final String TYPE_YearlyLeave = "YL";
	/** Set Type.
		@param Type Type	  */
	public void setType (String Type)
	{

		set_Value (COLUMNNAME_Type, Type);
	}

	/** Get Type.
		@return Type	  */
	public String getType () 
	{
		return (String)get_Value(COLUMNNAME_Type);
	}

	public com.uns.model.I_UNS_DispensationConfig getUNS_DispensationConfig() throws RuntimeException
    {
		return (com.uns.model.I_UNS_DispensationConfig)MTable.get(getCtx(), com.uns.model.I_UNS_DispensationConfig.Table_Name)
			.getPO(getUNS_DispensationConfig_ID(), get_TrxName());	}

	/** Set Dispensation Configuration.
		@param UNS_DispensationConfig_ID Dispensation Configuration	  */
	public void setUNS_DispensationConfig_ID (int UNS_DispensationConfig_ID)
	{
		if (UNS_DispensationConfig_ID < 1) 
			set_Value (COLUMNNAME_UNS_DispensationConfig_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_DispensationConfig_ID, Integer.valueOf(UNS_DispensationConfig_ID));
	}

	/** Get Dispensation Configuration.
		@return Dispensation Configuration	  */
	public int getUNS_DispensationConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DispensationConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getUNS_Employee_ID()));
    }

	/** Set Leave Permission Group.
		@param UNS_LeavePermissionGroup_ID Leave Permission Group	  */
	public void setUNS_LeavePermissionGroup_ID (int UNS_LeavePermissionGroup_ID)
	{
		if (UNS_LeavePermissionGroup_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_LeavePermissionGroup_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_LeavePermissionGroup_ID, Integer.valueOf(UNS_LeavePermissionGroup_ID));
	}

	/** Get Leave Permission Group.
		@return Leave Permission Group	  */
	public int getUNS_LeavePermissionGroup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_LeavePermissionGroup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}