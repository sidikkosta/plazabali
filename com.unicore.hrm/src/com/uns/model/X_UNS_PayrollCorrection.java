/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PayrollCorrection
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PayrollCorrection extends PO implements I_UNS_PayrollCorrection, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180628L;

    /** Standard Constructor */
    public X_UNS_PayrollCorrection (Properties ctx, int UNS_PayrollCorrection_ID, String trxName)
    {
      super (ctx, UNS_PayrollCorrection_ID, trxName);
      /** if (UNS_PayrollCorrection_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setBalance (Env.ZERO);
// 0
			setC_Period_ID (0);
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setPaidAmt (Env.ZERO);
// 0
			setUNS_Employee_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PayrollCorrection (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PayrollCorrection[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount Periodic.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount Periodic.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount Non Periodic.
		@param AmountNonPeriodic Amount Non Periodic	  */
	public void setAmountNonPeriodic (BigDecimal AmountNonPeriodic)
	{
		set_Value (COLUMNNAME_AmountNonPeriodic, AmountNonPeriodic);
	}

	/** Get Amount Non Periodic.
		@return Amount Non Periodic	  */
	public BigDecimal getAmountNonPeriodic () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmountNonPeriodic);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount (PPh 21) Periodic.
		@param AmountPPh21 Amount (PPh 21) Periodic	  */
	public void setAmountPPh21 (BigDecimal AmountPPh21)
	{
		set_Value (COLUMNNAME_AmountPPh21, AmountPPh21);
	}

	/** Get Amount (PPh 21) Periodic.
		@return Amount (PPh 21) Periodic	  */
	public BigDecimal getAmountPPh21 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmountPPh21);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount (PPh 21) Non Periodic.
		@param AmountPPh21NonPeriodic Amount (PPh 21) Non Periodic	  */
	public void setAmountPPh21NonPeriodic (BigDecimal AmountPPh21NonPeriodic)
	{
		set_Value (COLUMNNAME_AmountPPh21NonPeriodic, AmountPPh21NonPeriodic);
	}

	/** Get Amount (PPh 21) Non Periodic.
		@return Amount (PPh 21) Non Periodic	  */
	public BigDecimal getAmountPPh21NonPeriodic () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmountPPh21NonPeriodic);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Attendance Name.
		@param AttendanceName Attendance Name	  */
	public void setAttendanceName (String AttendanceName)
	{
		set_ValueNoCheck (COLUMNNAME_AttendanceName, AttendanceName);
	}

	/** Get Attendance Name.
		@return Attendance Name	  */
	public String getAttendanceName () 
	{
		return (String)get_Value(COLUMNNAME_AttendanceName);
	}

	/** Set Balance Periodic.
		@param Balance Balance Periodic	  */
	public void setBalance (BigDecimal Balance)
	{
		set_Value (COLUMNNAME_Balance, Balance);
	}

	/** Get Balance Periodic.
		@return Balance Periodic	  */
	public BigDecimal getBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Balance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Balance Non Periodic.
		@param BalanceNonPeriodic Balance Non Periodic	  */
	public void setBalanceNonPeriodic (BigDecimal BalanceNonPeriodic)
	{
		set_Value (COLUMNNAME_BalanceNonPeriodic, BalanceNonPeriodic);
	}

	/** Get Balance Non Periodic.
		@return Balance Non Periodic	  */
	public BigDecimal getBalanceNonPeriodic () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BalanceNonPeriodic);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Balance (PPh 21) Periodic.
		@param BalancePPh21 Balance (PPh 21) Periodic	  */
	public void setBalancePPh21 (BigDecimal BalancePPh21)
	{
		set_Value (COLUMNNAME_BalancePPh21, BalancePPh21);
	}

	/** Get Balance (PPh 21) Periodic.
		@return Balance (PPh 21) Periodic	  */
	public BigDecimal getBalancePPh21 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BalancePPh21);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Balance (PPh 21) Non Periodic.
		@param BalancePPh21NonPeriodic Balance (PPh 21) Non Periodic	  */
	public void setBalancePPh21NonPeriodic (BigDecimal BalancePPh21NonPeriodic)
	{
		set_Value (COLUMNNAME_BalancePPh21NonPeriodic, BalancePPh21NonPeriodic);
	}

	/** Get Balance (PPh 21) Non Periodic.
		@return Balance (PPh 21) Non Periodic	  */
	public BigDecimal getBalancePPh21NonPeriodic () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BalancePPh21NonPeriodic);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Paid Amt Periodic.
		@param PaidAmt Paid Amt Periodic	  */
	public void setPaidAmt (BigDecimal PaidAmt)
	{
		set_Value (COLUMNNAME_PaidAmt, PaidAmt);
	}

	/** Get Paid Amt Periodic.
		@return Paid Amt Periodic	  */
	public BigDecimal getPaidAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PaidAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Paid Amt Non Periodic.
		@param PaidAmtNonPeriodic Paid Amt Non Periodic	  */
	public void setPaidAmtNonPeriodic (BigDecimal PaidAmtNonPeriodic)
	{
		set_Value (COLUMNNAME_PaidAmtNonPeriodic, PaidAmtNonPeriodic);
	}

	/** Get Paid Amt Non Periodic.
		@return Paid Amt Non Periodic	  */
	public BigDecimal getPaidAmtNonPeriodic () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PaidAmtNonPeriodic);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Paid Amt (PPh21) Periodic.
		@param PaidAmtPPh21 Paid Amt (PPh21) Periodic	  */
	public void setPaidAmtPPh21 (BigDecimal PaidAmtPPh21)
	{
		set_Value (COLUMNNAME_PaidAmtPPh21, PaidAmtPPh21);
	}

	/** Get Paid Amt (PPh21) Periodic.
		@return Paid Amt (PPh21) Periodic	  */
	public BigDecimal getPaidAmtPPh21 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PaidAmtPPh21);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Paid Amount (PPh21) Non Periodic.
		@param PaidAmtPPh21NonPeriodic Paid Amount (PPh21) Non Periodic	  */
	public void setPaidAmtPPh21NonPeriodic (BigDecimal PaidAmtPPh21NonPeriodic)
	{
		set_Value (COLUMNNAME_PaidAmtPPh21NonPeriodic, PaidAmtPPh21NonPeriodic);
	}

	/** Get Paid Amount (PPh21) Non Periodic.
		@return Paid Amount (PPh21) Non Periodic	  */
	public BigDecimal getPaidAmtPPh21NonPeriodic () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PaidAmtPPh21NonPeriodic);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Payroll Correction.
		@param UNS_PayrollCorrection_ID Payroll Correction	  */
	public void setUNS_PayrollCorrection_ID (int UNS_PayrollCorrection_ID)
	{
		if (UNS_PayrollCorrection_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrection_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrection_ID, Integer.valueOf(UNS_PayrollCorrection_ID));
	}

	/** Get Payroll Correction.
		@return Payroll Correction	  */
	public int getUNS_PayrollCorrection_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollCorrection_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PayrollCorrection_UU.
		@param UNS_PayrollCorrection_UU UNS_PayrollCorrection_UU	  */
	public void setUNS_PayrollCorrection_UU (String UNS_PayrollCorrection_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PayrollCorrection_UU, UNS_PayrollCorrection_UU);
	}

	/** Get UNS_PayrollCorrection_UU.
		@return UNS_PayrollCorrection_UU	  */
	public String getUNS_PayrollCorrection_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PayrollCorrection_UU);
	}
}