/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_LeaveReservedConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_LeaveReservedConfig extends PO implements I_UNS_LeaveReservedConfig, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190206L;

    /** Standard Constructor */
    public X_UNS_LeaveReservedConfig (Properties ctx, int UNS_LeaveReservedConfig_ID, String trxName)
    {
      super (ctx, UNS_LeaveReservedConfig_ID, trxName);
      /** if (UNS_LeaveReservedConfig_ID == 0)
        {
			setAbsencesCount (0);
// 0
			setAbsencesCutLeave (false);
// N
			setAccumulatedLeavePeriod (null);
			setAutoCreateLeaveONUnpaidLeave (false);
// N
			setC_JobCategory_ID (0);
			setDisallowNegativeLeave (true);
// Y
			setGracePeriod (0);
// 0
			setLeaveCanUseOnStartWork (true);
// Y
			setLeaveClaimReserved (Env.ZERO);
// 0
			setLeaveType (null);
// YL
			setLongLeaveRecurringYear (0);
// 0
			setMaximumAdvanceLeave (0);
// 0
			setMaxMaternity (0);
// 2
			setNationality (null);
// WNI
			setUNS_LeaveReservedConfig_ID (0);
			setUseDateCalendar (false);
// N
        } */
    }

    /** Load Constructor */
    public X_UNS_LeaveReservedConfig (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_LeaveReservedConfig[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Absences Count.
		@param AbsencesCount Absences Count	  */
	public void setAbsencesCount (int AbsencesCount)
	{
		set_Value (COLUMNNAME_AbsencesCount, Integer.valueOf(AbsencesCount));
	}

	/** Get Absences Count.
		@return Absences Count	  */
	public int getAbsencesCount () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AbsencesCount);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Absences Cut Leave.
		@param AbsencesCutLeave Absences Cut Leave	  */
	public void setAbsencesCutLeave (boolean AbsencesCutLeave)
	{
		set_Value (COLUMNNAME_AbsencesCutLeave, Boolean.valueOf(AbsencesCutLeave));
	}

	/** Get Absences Cut Leave.
		@return Absences Cut Leave	  */
	public boolean isAbsencesCutLeave () 
	{
		Object oo = get_Value(COLUMNNAME_AbsencesCutLeave);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** 2 Weekly = 2W */
	public static final String ACCUMULATEDLEAVEPERIOD_2Weekly = "2W";
	/** Daily = DA */
	public static final String ACCUMULATEDLEAVEPERIOD_Daily = "DA";
	/** Monthly = MO */
	public static final String ACCUMULATEDLEAVEPERIOD_Monthly = "MO";
	/** Weekly = WE */
	public static final String ACCUMULATEDLEAVEPERIOD_Weekly = "WE";
	/** Yearly = YE */
	public static final String ACCUMULATEDLEAVEPERIOD_Yearly = "YE";
	/** Set Accumulated Leave Period.
		@param AccumulatedLeavePeriod Accumulated Leave Period	  */
	public void setAccumulatedLeavePeriod (String AccumulatedLeavePeriod)
	{

		set_Value (COLUMNNAME_AccumulatedLeavePeriod, AccumulatedLeavePeriod);
	}

	/** Get Accumulated Leave Period.
		@return Accumulated Leave Period	  */
	public String getAccumulatedLeavePeriod () 
	{
		return (String)get_Value(COLUMNNAME_AccumulatedLeavePeriod);
	}

	/** Set Auto Create Leave ON Unpaid Leave.
		@param AutoCreateLeaveONUnpaidLeave Auto Create Leave ON Unpaid Leave	  */
	public void setAutoCreateLeaveONUnpaidLeave (boolean AutoCreateLeaveONUnpaidLeave)
	{
		set_Value (COLUMNNAME_AutoCreateLeaveONUnpaidLeave, Boolean.valueOf(AutoCreateLeaveONUnpaidLeave));
	}

	/** Get Auto Create Leave ON Unpaid Leave.
		@return Auto Create Leave ON Unpaid Leave	  */
	public boolean isAutoCreateLeaveONUnpaidLeave () 
	{
		Object oo = get_Value(COLUMNNAME_AutoCreateLeaveONUnpaidLeave);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_C_JobCategory getC_JobCategory() throws RuntimeException
    {
		return (org.compiere.model.I_C_JobCategory)MTable.get(getCtx(), org.compiere.model.I_C_JobCategory.Table_Name)
			.getPO(getC_JobCategory_ID(), get_TrxName());	}

	/** Set Position Category.
		@param C_JobCategory_ID 
		Job Position Category
	  */
	public void setC_JobCategory_ID (int C_JobCategory_ID)
	{
		if (C_JobCategory_ID < 1) 
			set_Value (COLUMNNAME_C_JobCategory_ID, null);
		else 
			set_Value (COLUMNNAME_C_JobCategory_ID, Integer.valueOf(C_JobCategory_ID));
	}

	/** Get Position Category.
		@return Job Position Category
	  */
	public int getC_JobCategory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_JobCategory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Disallow Negative Leave.
		@param DisallowNegativeLeave Disallow Negative Leave	  */
	public void setDisallowNegativeLeave (boolean DisallowNegativeLeave)
	{
		set_Value (COLUMNNAME_DisallowNegativeLeave, Boolean.valueOf(DisallowNegativeLeave));
	}

	/** Get Disallow Negative Leave.
		@return Disallow Negative Leave	  */
	public boolean isDisallowNegativeLeave () 
	{
		Object oo = get_Value(COLUMNNAME_DisallowNegativeLeave);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Grace Period.
		@param GracePeriod Grace Period	  */
	public void setGracePeriod (int GracePeriod)
	{
		set_Value (COLUMNNAME_GracePeriod, Integer.valueOf(GracePeriod));
	}

	/** Get Grace Period.
		@return Grace Period	  */
	public int getGracePeriod () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GracePeriod);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Leave can use on start work.
		@param LeaveCanUseOnStartWork Leave can use on start work	  */
	public void setLeaveCanUseOnStartWork (boolean LeaveCanUseOnStartWork)
	{
		set_Value (COLUMNNAME_LeaveCanUseOnStartWork, Boolean.valueOf(LeaveCanUseOnStartWork));
	}

	/** Get Leave can use on start work.
		@return Leave can use on start work	  */
	public boolean isLeaveCanUseOnStartWork () 
	{
		Object oo = get_Value(COLUMNNAME_LeaveCanUseOnStartWork);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Total Leave Claim.
		@param LeaveClaimReserved 
		The total of emloyee's leave claim reserved in a period of time
	  */
	public void setLeaveClaimReserved (BigDecimal LeaveClaimReserved)
	{
		set_Value (COLUMNNAME_LeaveClaimReserved, LeaveClaimReserved);
	}

	/** Get Total Leave Claim.
		@return The total of emloyee's leave claim reserved in a period of time
	  */
	public BigDecimal getLeaveClaimReserved () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LeaveClaimReserved);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Long Leave = LL */
	public static final String LEAVETYPE_LongLeave = "LL";
	/** Yearly Leave = YL */
	public static final String LEAVETYPE_YearlyLeave = "YL";
	/** Set LeaveType.
		@param LeaveType LeaveType	  */
	public void setLeaveType (String LeaveType)
	{

		set_Value (COLUMNNAME_LeaveType, LeaveType);
	}

	/** Get LeaveType.
		@return LeaveType	  */
	public String getLeaveType () 
	{
		return (String)get_Value(COLUMNNAME_LeaveType);
	}

	/** Set Long Leave Recurring Year.
		@param LongLeaveRecurringYear Long Leave Recurring Year	  */
	public void setLongLeaveRecurringYear (int LongLeaveRecurringYear)
	{
		set_Value (COLUMNNAME_LongLeaveRecurringYear, Integer.valueOf(LongLeaveRecurringYear));
	}

	/** Get Long Leave Recurring Year.
		@return Long Leave Recurring Year	  */
	public int getLongLeaveRecurringYear () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_LongLeaveRecurringYear);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Maternity Claim Reserved.
		@param MaternityClaimReserved Maternity Claim Reserved	  */
	public void setMaternityClaimReserved (int MaternityClaimReserved)
	{
		set_Value (COLUMNNAME_MaternityClaimReserved, Integer.valueOf(MaternityClaimReserved));
	}

	/** Get Maternity Claim Reserved.
		@return Maternity Claim Reserved	  */
	public int getMaternityClaimReserved () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MaternityClaimReserved);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Maximum Advance Leave.
		@param MaximumAdvanceLeave Maximum Advance Leave	  */
	public void setMaximumAdvanceLeave (int MaximumAdvanceLeave)
	{
		set_Value (COLUMNNAME_MaximumAdvanceLeave, Integer.valueOf(MaximumAdvanceLeave));
	}

	/** Get Maximum Advance Leave.
		@return Maximum Advance Leave	  */
	public int getMaximumAdvanceLeave () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MaximumAdvanceLeave);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Max. Maternity.
		@param MaxMaternity Max. Maternity	  */
	public void setMaxMaternity (int MaxMaternity)
	{
		set_Value (COLUMNNAME_MaxMaternity, Integer.valueOf(MaxMaternity));
	}

	/** Get Max. Maternity.
		@return Max. Maternity	  */
	public int getMaxMaternity () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MaxMaternity);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Warga Negara Asing = WNA */
	public static final String NATIONALITY_WargaNegaraAsing = "WNA";
	/** Warga Negara Indonesia = WNI */
	public static final String NATIONALITY_WargaNegaraIndonesia = "WNI";
	/** Set Nationality.
		@param Nationality Nationality	  */
	public void setNationality (String Nationality)
	{

		set_Value (COLUMNNAME_Nationality, Nationality);
	}

	/** Get Nationality.
		@return Nationality	  */
	public String getNationality () 
	{
		return (String)get_Value(COLUMNNAME_Nationality);
	}

	/** 2 Weekly = 2W */
	public static final String PERIODABSENCES_2Weekly = "2W";
	/** Daily = DA */
	public static final String PERIODABSENCES_Daily = "DA";
	/** Monthly = MO */
	public static final String PERIODABSENCES_Monthly = "MO";
	/** Weekly = WE */
	public static final String PERIODABSENCES_Weekly = "WE";
	/** Yearly = YE */
	public static final String PERIODABSENCES_Yearly = "YE";
	/** Set Period Absences.
		@param PeriodAbsences Period Absences	  */
	public void setPeriodAbsences (String PeriodAbsences)
	{

		set_Value (COLUMNNAME_PeriodAbsences, PeriodAbsences);
	}

	/** Get Period Absences.
		@return Period Absences	  */
	public String getPeriodAbsences () 
	{
		return (String)get_Value(COLUMNNAME_PeriodAbsences);
	}

	/** Contract = CON */
	public static final String RESETBASEDON_Contract = "CON";
	/** End Of Year = EOY */
	public static final String RESETBASEDON_EndOfYear = "EOY";
	/** Set Reset Based On.
		@param ResetBasedOn Reset Based On	  */
	public void setResetBasedOn (String ResetBasedOn)
	{

		set_Value (COLUMNNAME_ResetBasedOn, ResetBasedOn);
	}

	/** Get Reset Based On.
		@return Reset Based On	  */
	public String getResetBasedOn () 
	{
		return (String)get_Value(COLUMNNAME_ResetBasedOn);
	}

	/** Set UNS_LeaveReservedConfig.
		@param UNS_LeaveReservedConfig_ID UNS_LeaveReservedConfig	  */
	public void setUNS_LeaveReservedConfig_ID (int UNS_LeaveReservedConfig_ID)
	{
		if (UNS_LeaveReservedConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_LeaveReservedConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_LeaveReservedConfig_ID, Integer.valueOf(UNS_LeaveReservedConfig_ID));
	}

	/** Get UNS_LeaveReservedConfig.
		@return UNS_LeaveReservedConfig	  */
	public int getUNS_LeaveReservedConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_LeaveReservedConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_LeaveReservedConfig_UU.
		@param UNS_LeaveReservedConfig_UU UNS_LeaveReservedConfig_UU	  */
	public void setUNS_LeaveReservedConfig_UU (String UNS_LeaveReservedConfig_UU)
	{
		set_Value (COLUMNNAME_UNS_LeaveReservedConfig_UU, UNS_LeaveReservedConfig_UU);
	}

	/** Get UNS_LeaveReservedConfig_UU.
		@return UNS_LeaveReservedConfig_UU	  */
	public String getUNS_LeaveReservedConfig_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_LeaveReservedConfig_UU);
	}

	/** Set Use Date Calendar.
		@param UseDateCalendar Use Date Calendar	  */
	public void setUseDateCalendar (boolean UseDateCalendar)
	{
		set_Value (COLUMNNAME_UseDateCalendar, Boolean.valueOf(UseDateCalendar));
	}

	/** Get Use Date Calendar.
		@return Use Date Calendar	  */
	public boolean isUseDateCalendar () 
	{
		Object oo = get_Value(COLUMNNAME_UseDateCalendar);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}
}