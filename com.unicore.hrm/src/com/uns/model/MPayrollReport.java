package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MPayrollReport extends X_T_PayrollReport {

	/**
	 * @author Hamba Allah
	 */
	private static final long serialVersionUID = 4010960621389850604L;

	public MPayrollReport(Properties ctx, int T_PayrollReport_ID,
			String trxName) {
		super(ctx, T_PayrollReport_ID, trxName);
		
	}

	public MPayrollReport(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		setTotalAmt(getM1().add(getM2()).add(getM3()).add(getM4()).add(getM5()).add(getM6())
				.add(getM7()).add(getM8()).add(getM9()).add(getM10()).add(getM11()).add(getM12()));
		
		return super.beforeSave(newRecord);
	}

}
