/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.model.MSysConfig;
import org.compiere.model.MUser;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;



import org.compiere.util.Util;

//import org.compiere.util.Env;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author menjangan
 *
 */
public class MUNSContractRecommendation extends X_UNS_Contract_Recommendation implements DocAction, DocOptions {

	
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private MUNSPayrollComponentConf[] m_components = null;
	
	public MUNSPayrollComponentConf[] getComponents (boolean requery)
	{
		if (m_components != null)
		{
			set_TrxName(m_components, get_TrxName());
			return m_components;
		}
		String wc = Table_Name + "_ID = ?";
		List<MUNSPayrollComponentConf> comps = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
				MUNSPayrollComponentConf.Table_Name, wc, get_TrxName()).setParameters(get_ID()).list();
		m_components = new MUNSPayrollComponentConf[comps.size()];
		comps.toArray(m_components);
		
		return m_components;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2950121656417505222L;

	/**
	 * @param ctx
	 * @param UNS_Contract_Recommendation_ID
	 * @param trxName
	 */
	public MUNSContractRecommendation(Properties ctx,
			int UNS_Contract_Recommendation_ID, String trxName) {
		super(ctx, UNS_Contract_Recommendation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSContractRecommendation(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSContractRecommendation(MUNSEmployee employee)
	{
		super(employee.getCtx(), 0, employee.get_TrxName());
		setAD_Org_ID(employee.getAD_Org_ID());
		setUNS_Employee_ID(employee.getUNS_Employee_ID());
	}
	
	/**
	 * 
	 * @param ctx
	 * @param UNS_Contract_Evaluation_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSContractRecommendation get(
			Properties ctx, int UNS_Contract_Evaluation_ID, String trxName)
	{
		return Query.get(
				ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name
				, COLUMNNAME_UNS_Contract_Evaluation_ID + " = " + UNS_Contract_Evaluation_ID
				+ " AND " + COLUMNNAME_IsActive + " = 'Y' ", 
				trxName)
				.firstOnly();
	}
	
	public static MUNSContractRecommendation getEffectiveContract(Properties ctx, MUNSEmployee employee, Timestamp date ,String trxName)
	{
		HashMap<Integer, MUNSContractRecommendation> mapContract = employee.getMapContract(true, date);
		MUNSContractRecommendation contract = null;
		for(int i=0; i<mapContract.size(); i++)
		{
			contract = mapContract.get(i);
			if(!contract.isSynchronized())
				continue;
			
			if(contract.getEffectiveDate().compareTo(date) <= 0)
				break;
		}
		
		return contract;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		// TODO Auto-generated method stub

		if (docStatus.equals(DocumentEngine.STATUS_Drafted)
    			|| docStatus.equals(DocumentEngine.STATUS_Invalid)) {
    		options[index++] = DocumentEngine.ACTION_Prepare;
    	}
    	
    	// If status = Completed, add "Reactivte" in the list
    	if (docStatus.equals(DocumentEngine.STATUS_Completed)) {
    		options[index++] = DocumentEngine.ACTION_Reverse_Correct;
    		options[index++] = DocumentEngine.ACTION_Void;
    		options[index++] = DocumentEngine.ACTION_ReActivate;
    	}   	
    		
    	return index;
		
	}

	@Override
	public boolean processIt(String action) throws Exception {
		// TODO Auto-generated method stub
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		setProcessed(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
	
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
			
		I_UNS_Employee employee = getUNS_Employee();
		if(employee.isBlacklist())
		{
			m_processMsg = "Can't create Contract for blacklisted emloyeee";
			return DocAction.STATUS_Drafted;
		}
		if (null == getNewPayrollLevel() || getNewPayrollLevel().equalsIgnoreCase(NEWPAYROLLLEVEL_NotDefined))
		{
			m_processMsg = "Employee must have payroll level, please define payroll level";
			return DocAction.STATUS_Drafted;
		}

//		if (EMPLOYMENTTYPE_Company.equals(getEmploymentType()) && getNew_G_Pokok().equals(BigDecimal.ZERO))
//		{
//			m_processMsg = "Employee basic payroll: please define Basic Salary of the contract.";
//			return DocAction.STATUS_Drafted;
//		}

		if(getDateContractEnd().before(getDateContractStart()))
		{
			m_processMsg = "Date contract end must be greater than date contract start";
			return DocAction.STATUS_Drafted;
		}
		
		MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
				getCtx(), getDateContractStart(), getNewDept_ID(), get_TrxName(), true);
		if(null == payConfig)
			throw new AdempiereUserError("Not found payroll configuration");
		
		MUNSPayrollLevelConfig payLevelConfig = payConfig.getPayrollLevel(getNewPayrollLevel()
				, getNextPayrollTerm(), getNewDept_ID(), true);
		if (null == payLevelConfig)
			throw new AdempiereUserError("Not found Payroll Level Configuration with level " + getNewPayrollLevel());

			
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		log.info(toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		log.info(toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		log.info(toString());
		
//		Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		boolean differentLegality = getUNS_Contract_Evaluation_ID() > 0 ? getUNS_Contract_Evaluation().isDifferentLegality() : false;
		
		if((isMoveTo() || isMoveToDept()) && getUNS_Contract_Evaluation_ID() > 0)
		{
			setAD_Org_ID(getNewDept_ID());
			
			String strNow = new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(new Date());
			Timestamp now = Timestamp.valueOf(strNow);
			if(now.compareTo(getEffectiveDate()) >= 0)
			{
				m_processMsg = updateData(differentLegality);
				if(m_processMsg != null)
					return DocAction.STATUS_Invalid;
				setisSynchronized(true);
			}
			
		}
		else
		{
			setisSynchronized(true);
		}
		
		MUNSEmployee employee = new MUNSEmployee(getCtx(), getUNS_Employee_ID(), get_TrxName());
		if (MUNSEmployee.EMPLOYMENTTYPE_Company.equals(employee.getEmploymentType())
				|| isUseGeneralPayroll())
		{
//			MUNSPayrollBaseEmployee prevPayrollBase = MUNSPayrollBaseEmployee.getPrev(
//					getCtx(), getUNS_Employee_ID(), get_TrxName());
//			if(null != prevPayrollBase)
//			{
//				prevPayrollBase.setIsActive(false);
//				prevPayrollBase.save();				
//			}
			
			m_processMsg = generatePayrollBaseEmployee();
			if(m_processMsg != null)
				return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = employee.updateEmployeeData(this);
		if (null != m_processMsg)
			return DocAction.STATUS_Invalid;
		
		try
		{
			employee.saveEx();
		}
		catch (Exception ex)
		{
			m_processMsg = ex.getMessage();
			return DocAction.STATUS_Invalid;
		}
		
		// Try to create allowance record for the company's employee.
		if (employee.getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_Company))
		{
			MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.getCreate(
					getCtx(), (MUNSEmployee) employee, getEffectiveDate(),
					MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave,
					this, get_TrxName());
			
			if(record != null)
			{
				record.reCalcMedicalAllowance(this, true);
				record.saveEx();
			}
			
			setProcessed(true);
//			if(record != null)
//				record.reCalculateRecord(getEffectiveDate());
		}
		setProcessed(true);	
		setIsApproved(true);
		setDocAction(DOCACTION_Close);
		saveEx();
		
		if (getUNS_Contract_Evaluation_ID() != 0){
			MUNSContractEvaluation contractEvalutaion = (MUNSContractEvaluation) getUNS_Contract_Evaluation();
			try {
				if(!contractEvalutaion.processIt(DOCACTION_Complete)) 
				{
					m_processMsg = "Error while auto completing Contract Evaluation of " 
							+ contractEvalutaion.getDocumentNo() + ". Error: " 
							+ CLogger.retrieveErrorString("No detail error.");
				}
			} catch(Exception e){
				throw new AdempiereUserError(e.getMessage());
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
		{
			setProcessed(false);
			return DocAction.STATUS_Invalid;
		}
		
		//create user login
		if(isAutoCreateUser())
		{
			MUser user = null;
			//check user login
			String sql = "SELECT AD_User_ID FROM AD_User WHERE UNS_Employee_ID = ?"
					+ " AND isActive = 'Y'";
			int ad_user_id = DB.getSQLValue(get_TrxName(), sql, getUNS_Employee_ID());
			if(ad_user_id > 0)
				user = new MUser(getCtx(), ad_user_id, get_TrxName());
			
			if(user != null)
			{
				if(getNewNIK().equals(user.getName()))
				{
					user.setName(getNewNIK());
					user.setValue(getNewNIK());
					if(!user.save())
						throw new AdempiereException("Error when try to update user login.");
				}
			}
			else
			{
				MUNSEmployee emp = new MUNSEmployee(getCtx(), getUNS_Employee_ID(), get_TrxName());
				user = new MUser(getCtx(), 0, get_TrxName());
				user.setAD_Org_ID(0);
				user.setUNS_Employee_ID(emp.get_ID());
				user.setName(getNewNIK());
				user.setValue(getNewNIK());
				user.setRealName(emp.getName());
				user.setEMail(emp.getEMail());
				user.setUserAgent(false);
				user.setAccessOwnOrgData(true);
				user.setPassword(getNewNIK());
				if(!user.save())
					throw new AdempiereException("Error when try to create user login");
			}
		}
		
		//update BPJS Change Log
		updateBPJSLog();
		
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		// Before Void
		if(getDocStatus().equals(DOCSTATUS_Closed))
		{
			m_processMsg = "Document has closed.";
			return false;
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (DOCSTATUS_Closed.equals(getDocStatus())
			|| DOCSTATUS_Reversed.equals(getDocStatus())
			|| DOCSTATUS_Voided.equals(getDocStatus()))
		{
			m_processMsg = "Document Closed: " + getDocStatus();
			return false;
		}
		
		if (getUNS_Contract_Evaluation_ID() > 0)
		{
			MUNSContractRecommendation prevContract = 
					MUNSContractRecommendation.get(getCtx(), 
							getUNS_Contract_Evaluation_ID(), get_TrxName());
			
			if (null == prevContract)
			{
				m_processMsg = "could not find prev contract of " + toString();
				return false;
			}
			
			String sql = " UPDATE " + MUNSPayrollBaseEmployee.Table_Name + " SET "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_IsActive + " = ? WHERE "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_UNS_Contract_Recommendation_ID
					+ " = ? ";
			if (DB.executeUpdate(sql, new Object[]{"Y", prevContract.get_ID()},
					false, get_TrxName())== -1)
			{
				m_processMsg = "can't update payroll base.";
				return false;
			}
			else if (DB.executeUpdate(sql, new Object[]{"N", get_ID()},
					false, get_TrxName())== -1)
			{
				m_processMsg = "can't update payroll base.";
				return false;
			}
			
			MUNSEmployee employe = (MUNSEmployee) getUNS_Employee();
			m_processMsg = employe.updateEmployeeData(prevContract);
			if (null != m_processMsg)
			{
				return false;
			}
			
			try
			{
				employe.save();
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return false;
			}
		}
		else
		{
			MUNSEmployee employe = (MUNSEmployee) getUNS_Employee();
			m_processMsg = employe.updateEmployeeData(this);
			String sql = " UPDATE " + MUNSPayrollBaseEmployee.Table_Name + " SET "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_IsActive + " = ? WHERE "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_UNS_Contract_Recommendation_ID
					+ " = ? ";
			if (DB.executeUpdate(sql, new Object[]{"N", get_ID()},
					false, get_TrxName())== -1)
			{
				m_processMsg = "can't update payroll base.";
				return false;
			}
			
			if (null != m_processMsg)
			{
				return false;
			}
			
			try
			{
				employe.save();
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return false;
			}
		}
		
		m_processMsg = checkAllowanceRecord();
		if(m_processMsg != null)
			return false;

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
	
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		return false;
	}

	@Override
	public boolean reActivateIt() {
		// TODO Auto-generated method stub
		log.info(toString());
		//ga boleh reaktif @BurhaniAdam
		if(!isFromAnotherProcess())
		{
			m_processMsg = "Reactivate disallowed for this document.!";
			if(m_processMsg != null)
				return false;
		}
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		if (getUNS_Contract_Evaluation_ID() > 0)
		{	
			MUNSContractRecommendation prevContract = Query.get(
					getCtx(), UNSHRMModelFactory.EXTENSION_ID, Table_Name, Table_Name+"_ID = "
							+ "(SELECT UNS_Contract_Recommendation_ID FROM UNS_Contract_Evaluation"
							+ " WHERE UNS_Contract_Evaluation_ID = ?)", get_TrxName())
						.setParameters(getUNS_Contract_Evaluation_ID())
						.first();
			
			if (null == prevContract)
			{
				m_processMsg = "could not find prev contract of " + toString();
				return false;
			}
			
			String sql = " UPDATE " + MUNSPayrollBaseEmployee.Table_Name + " SET "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_IsActive + " = ? WHERE "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_UNS_Contract_Recommendation_ID
					+ " = ? ";
			if (DB.executeUpdate(sql, new Object[]{"Y", prevContract.get_ID()},
					false, get_TrxName())== -1)
			{
				m_processMsg = "can't update payroll base.";
				return false;
			}
			else if (DB.executeUpdate(sql, new Object[]{"N", get_ID()},
					false, get_TrxName())== -1)
			{
				m_processMsg = "can't update payroll base.";
				return false;
			}
			
			MUNSEmployee employe = (MUNSEmployee) getUNS_Employee();
			m_processMsg = employe.updateEmployeeData(this);
			if (null != m_processMsg)
			{
				return false;
			}
			
			try
			{
				employe.save();
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return false;
			}
		}
		else
		{
			MUNSEmployee employe = (MUNSEmployee) getUNS_Employee();
			m_processMsg = employe.updateEmployeeData(this);
			String sql = " UPDATE " + MUNSPayrollBaseEmployee.Table_Name + " SET "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_IsActive + " = ? WHERE "
					+ MUNSPayrollBaseEmployee.COLUMNNAME_UNS_Contract_Recommendation_ID
					+ " = ? ";
			if (DB.executeUpdate(sql, new Object[]{"N", get_ID()},
					false, get_TrxName())== -1)
			{
				m_processMsg = "can't update payroll base.";
				return false;
			}
			
			if (null != m_processMsg)
			{
				return false;
			}
			
			try
			{
				employe.save();
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return false;
			}
		}
		
		m_processMsg = checkAllowanceRecord();
		if(m_processMsg != null)
			return false;
		
		if(!removePrevResource())
		{
			m_processMsg = "Failed when trying delete previous resource.";
			return false;
		}
		
		setProcessed(false);
		setIsApproved(false);
		setDocAction(DOCACTION_Complete);
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		return true;
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	
	protected String generatePayrollBaseEmployee()
	{
		String msg = null;
		MUNSPayrollBaseEmployee pbEmployee = MUNSPayrollBaseEmployee.Create(this);
		if (!pbEmployee.save())
			msg = " Failed to create new Payroll Base Employee";
		return msg;
	}
	@Override
	protected boolean beforeDelete(){
		if (getUNS_Contract_Evaluation_ID()>0)
			return false;
		if (isProcessed())
			return false;
		
		return true;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		//IsImportData
		if (newRecord && getNewNIK()== null){
			setNewNIK(getUNS_Employee().getValue());
			
			String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
					getAD_Org_ID()
					, getNewSectionOfDept_ID()
					, getNextContractType()
					, Env.getContextAsDate(getCtx(), "Date")
					, get_TrxName());
			if(null == payrollTerm)
				payrollTerm = getUNS_Employee().getPayrollTerm();
			
			setNextPayrollTerm(payrollTerm);
			setNewPayrollLevel(getUNS_Employee().getPayrollLevel());
//			setNewGender(getUNS_Employee().getGender());
			setNewShift(getUNS_Employee().getShift());
			setNewAgent_ID(getUNS_Employee().getVendor_ID());
		}
				
		if(newRecord)
		{
			if(getUNS_Employee().getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_SubContract))
				setNextContractNumber(getPrevContractNumber()+1);
		}
		
		if(getUNS_Contract_Evaluation_ID() <= 0)
			setEffectiveDate(getDateContractStart());
		
		//Direct Additional ITD-Andy 29/07/13
		if (getDateContractEnd().before(getDateContractStart()))
			throw new AdempiereUserError("Date contact end must bigger than date contact start");
		
		if(getUNS_Employee().isBlacklist())
			throw new AdempiereUserError("Can't create contract for blacklisted employee or worker");
		if(null == getNextContractType())
			throw new AdempiereException("Field Mandatory Contract");

		if(getEmploymentType().equals(EMPLOYMENTTYPE_Company)
				&& getNextContractType().equals(NEXTCONTRACTTYPE_SquenceContract))
		{
			throw new AdempiereUserError("Contract sequence are only for workers");
		}
		else if(getEmploymentType().equals(EMPLOYMENTTYPE_SubContract)
				&& !getNextContractType().equals(NEXTCONTRACTTYPE_SquenceContract))
		{
			throw new AdempiereUserError("Please choose sequence contract for Worker");
		}
		
		//calculate OT BasicAmt
		String sql = "SELECT isCalculateOTBySalary FROM UNS_PayrollLevel_Config WHERE UNS_PayrollLevel_Config_ID ="
				+ " getPayrollLevelConf(?,?,?,?)";
		
		boolean isCalculate = DB.getSQLValueString(get_TrxName(), sql, new Object[]{
			getNewPayrollLevel(),getNextPayrollTerm(),getAD_Org_ID(),"Y"}).equals("Y");
		
		if(isCalculate)
		{
			setOTBasicAmt(calculateOTBasicAmt().setScale(2, BigDecimal.ROUND_HALF_DOWN));
		}
		
		if(!newRecord && (is_ValueChanged(COLUMNNAME_OTBasicAmt) || is_ValueChanged(COLUMNNAME_AL1Multiplier)
				|| is_ValueChanged(COLUMNNAME_AL2Multiplier) || is_ValueChanged(COLUMNNAME_AL3Multiplier)
				|| is_ValueChanged(COLUMNNAME_ALR1Multiplier) || is_ValueChanged(COLUMNNAME_ALR2Multiplier)
				|| is_ValueChanged(COLUMNNAME_ALR3Multiplier) || is_ValueChanged(COLUMNNAME_FirstOTMultiplier) 
				|| is_ValueChanged(COLUMNNAME_NextOTMultiplier) || is_ValueChanged(COLUMNNAME_New_A_L1)
				|| is_ValueChanged(COLUMNNAME_New_A_L2) || is_ValueChanged(COLUMNNAME_New_A_L3)
				|| is_ValueChanged(COLUMNNAME_New_A_L1_R) || is_ValueChanged(COLUMNNAME_New_A_L2_R)
				|| is_ValueChanged(COLUMNNAME_New_A_L3_R) || is_ValueChanged(COLUMNNAME_NewLeburJamPertama)
				|| is_ValueChanged(COLUMNNAME_NewLeburJamBerikutnya)
				))
		{
			updateOvertimeComp();
		}
		
		return super.beforeSave(newRecord);
	}
	
	public MUNSContractRecommendation getPrev ()
	{
		if (getUNS_Contract_Evaluation_ID() == 0)
			return null;
		
		MUNSContractEvaluation evaluation = new MUNSContractEvaluation(
				getCtx(), getUNS_Contract_Evaluation_ID(), get_TrxName());
		if (evaluation.getUNS_Contract_Recommendation_ID() == 0)
			return null;
		
		return new MUNSContractRecommendation(getCtx(), evaluation.getUNS_Contract_Recommendation_ID(), 
				get_TrxName());
	}
	
	/**
	 * @deprecated
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSContractRecommendation getPrev(
			Properties ctx, int UNS_Employee_ID, String trxName)
	{
		MUNSContractRecommendation prev = null;
		String sql = "SELECT * FROM " + Table_Name
				+ " WHERE " + COLUMNNAME_DateContractEnd + " = " +
						"(SELECT MAX(" + COLUMNNAME_DateContractEnd + ") FROM " + Table_Name + " "
								+ " WHERE " + COLUMNNAME_UNS_Employee_ID
								+ " = " + UNS_Employee_ID + " AND " + COLUMNNAME_IsActive + " ='Y')" 
								+ " AND " + COLUMNNAME_IsActive + " = 'Y' AND " 
								+ COLUMNNAME_UNS_Employee_ID
								+ " = " + UNS_Employee_ID;
		
		PreparedStatement stm = null;
		ResultSet rs = null;
		try
		{
			stm = DB.prepareStatement(sql,trxName);
			rs = stm.executeQuery();
			if (rs.next())
				prev = new MUNSContractRecommendation(ctx, rs, trxName);
		}catch (Exception e)
		{
			e.printStackTrace();
		}finally
		{
			DB.close(rs, stm);
		}
		
		return prev;
	}
	
	public boolean isGenerateNIK()
	{
		return getGenerateNIK().equalsIgnoreCase("Y");
	}

	/**
	 * 
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param trxName
	 * @return
	 */
	public static List<MUNSContractRecommendation> getContractsOf(
			Properties ctx, int UNS_Employee_ID, String trxName)
	{
		return Query.get(
				ctx, UNSHRMModelFactory.getExtensionID(), Table_Name, 
				COLUMNNAME_UNS_Employee_ID + " = " + UNS_Employee_ID, trxName)
				.setOrderBy(COLUMNNAME_DateContractEnd + " ASC").list();
	}
	
	/**
	 * Get employee contract which is valid on the date of dateToSearch.
	 * 
	 * @param ctx
	 * @param UNS_Employee_ID
	 * @param date
	 * @param trxName
	 * @return
	 */
	public static MUNSContractRecommendation getOf(
			Properties ctx, int UNS_Employee_ID, Timestamp dateToSearch, int AD_Org_ID , String trxName)
	{
//		return Query.get(
//				ctx, UNSHRMModelFactory.getExtensionID(), Table_Name, 
//				"UNS_Employee_ID = ? AND ? BETWEEN DateContractStart AND DateContractEnd AND AD_Org_ID = ?"
//				+ " AND ? >= EffectiveDate AND isSynchronized = 'Y' AND DocStatus IN ('CO','CL')", trxName)
//				.setParameters(UNS_Employee_ID, dateToSearch, AD_Org_ID, dateToSearch).setOrderBy("EffectiveDate DESC")
//				.first();
		//temporary
		return Query.get(
				ctx, UNSHRMModelFactory.getExtensionID(), Table_Name, 
				"UNS_Employee_ID = ? AND ? BETWEEN DateContractStart AND DateContractEnd AND AD_Org_ID = ?"
				+ " AND ? >= EffectiveDate AND isSynchronized = 'Y' AND Processed = 'Y'", trxName)
				.setParameters(UNS_Employee_ID, dateToSearch, AD_Org_ID, dateToSearch).setOrderBy("EffectiveDate DESC")
				.first();
	}
	
	public static MUNSContractRecommendation getOf(
			Properties ctx, int UNS_Employee_ID, String trxName)
	{
		return Query.get(
				ctx, UNSHRMModelFactory.getExtensionID(), Table_Name, 
				Table_Name + "_ID = (SELECT " + Table_Name + "_ID FROM UNS_Employee "
						+ " WHERE UNS_Employee_ID = ?)", trxName)
				.setParameters(UNS_Employee_ID)
				.firstOnly();
	}
	
	public void setPotonganToZero(){
		setNew_P_Label(BigDecimal.ZERO);
		setNew_P_Lain2(BigDecimal.ZERO);
		setNew_P_Mangkir(BigDecimal.ZERO);
		setNew_P_SPTP(BigDecimal.ZERO);
	}
	
	public void generateNIK()
	{
		
	}	
	
	private MUNSEmployee m_employee = null;
	
	@Override
	public MUNSEmployee getUNS_Employee ()
	{
		if (null != m_employee)
		{
			return m_employee;
		}
		
		m_employee = (MUNSEmployee)super.getUNS_Employee();
		return m_employee;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param trxName
	 * @param AD_Org_ID
	 * @param date
	 * @return
	 */
	public static MUNSContractRecommendation[] getUsedGeneralPayroll (
			Properties ctx, String trxName, int AD_Org_ID, Timestamp startDate, Timestamp endDate)
	{
		String whereClause = "EffectiveDate = (SELECT MAX(EffectiveDate) FROM"
				+ " UNS_ContractRecommendation c WHERE EffectiveDate <= ?"
				+ " IsActive = ? AND IsUseGeneralPayroll = ? AND DocStatus IN (?,?) AND"
				+ " UNS_Employee_ID = UNS_Contract_Recommendation.UNS_Employee_ID ) AND"
				+ " EXISTS(SELECT 1 FROM UNS_Employee WHERE UNS_Employee_ID = UNS_Contract_Recommendation.UNS_Employee_ID"
				+ " AND IsActive = ?) AND IsActive = ? AND IsUseGeneralPayroll = ? AND ? BETWEEN DateContractStart AND DateContractEnd";
		
		List<Object> parameters = new ArrayList<>();
		parameters.add(endDate);
		parameters.add("Y");
		parameters.add("Y");
		parameters.add("CO");
		parameters.add("CL");
		parameters.add("Y");
		parameters.add("Y");
		parameters.add(startDate);
		
		ArrayList<MUNSContractRecommendation> list = new ArrayList<MUNSContractRecommendation>();

		MUNSContractRecommendation[] contracts = gets(ctx, trxName, whereClause, parameters, "UNS_Employee_ID ASC");
		for(int i=0; i<contracts.length; i++)
		{
			if(contracts[i].getAD_Org_ID() == AD_Org_ID)
				list.add(contracts[i]);
			
			if(!contracts[i].getDocType().equals(MUNSContractRecommendation.DOCTYPE_Addendum)
					|| contracts[i].getEffectiveDate().before(startDate))
			{	
				break;
			}
			
			//check other contract
			MUNSContractRecommendation contract = contracts[i];
			
			for(int x=0; x<10; x++)
			{
//				if(contract.getAD_Org_ID() == AD_Org_ID)
//					list.add(contract);
				
				boolean isDiffLegality = contract.getUNS_Contract_Evaluation().isDifferentLegality();
				
				contract = new MUNSContractRecommendation(
						ctx, contract.getUNS_Contract_Evaluation().getUNS_Contract_Recommendation_ID(), trxName);
				
				if(contract.getEffectiveDate().before(startDate))
					break;
				
				if(contract.getAD_Org_ID() != AD_Org_ID)
					continue;
				
				if(isDiffLegality)
					list.add(contract);
			}
		}
		
		contracts = new MUNSContractRecommendation[list.size()];
		list.toArray(contracts);
		
		return contracts;
	}
	
	/**
	 * 
	 * @param ctx
	 * @param trxName
	 * @param whereClause
	 * @param parameters
	 * @param orderBy
	 * @return
	 */
	public static MUNSContractRecommendation[] gets (Properties ctx, 
			String trxName, String whereClause, List<Object> parameters, 
			String orderBy)
	{
		Query q = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, 
				whereClause, trxName);
		if (parameters != null)
		{
			q.setParameters(parameters);
		}
		if (orderBy != null)
		{
			q.setOrderBy(orderBy);
		}
		
		List<MUNSContractRecommendation> list = q.list();
		MUNSContractRecommendation[] contracts = 
				new MUNSContractRecommendation[list.size()];
		list.toArray(contracts);
		
		return contracts;
	}
	
	public BigDecimal getOTBaseAmt() {
		
		BigDecimal amt = Env.ZERO;
		BigDecimal multiplier = Env.ZERO;
		BigDecimal oTAmt = Env.ZERO;
		
		MUNSPayrollLevelConfig conf = MUNSPayrollLevelConfig.get(
				getCtx(), getNewPayrollLevel(), getNextPayrollTerm(), getAD_Org_ID(), true, get_TrxName());
		
		if(conf != null)
		{
			if(conf.getAL1Multiplier().signum() == 1)
			{
				multiplier = conf.getAL1Multiplier();
				oTAmt = getNew_A_L1();
			}
			else if(conf.getAL2Multiplier().signum() == 1)
			{
				multiplier = conf.getAL2Multiplier();
				oTAmt = getNew_A_L2();
			}
			else if(conf.getAL3Multiplier().signum() == 1)
			{
				multiplier = conf.getAL3Multiplier();
				oTAmt = getNew_A_L3();
			}
			else if(conf.getALR1Multiplier().signum() == 1)
			{
				multiplier = conf.getALR1Multiplier();
				oTAmt = getNew_A_L1_R();
			}
			else if(conf.getALR2Multiplier().signum() == 1)
			{
				multiplier = conf.getALR2Multiplier();
				oTAmt = getNew_A_L2_R();
			}
			else if(conf.getALR3Multiplier().signum() == 1)
			{
				multiplier = conf.getALR3Multiplier();
				oTAmt = getNew_A_L3_R();
			}
			else if(conf.getFirstOTMultiplier().signum() == 1)
			{
				multiplier = conf.getFirstOTMultiplier();
				oTAmt = getNewLeburJamPertama();
			}
			else if(conf.getNextOTMultiplier().signum() == 1)
			{
				multiplier = conf.getNextOTMultiplier();
				oTAmt = getNewLeburJamBerikutnya();
			}
			
			if(oTAmt.signum() == 1 && multiplier.signum() == 1)
			{
				amt = oTAmt.divide(multiplier, 2, RoundingMode.HALF_UP);
			}
		}
		
		return amt;
	}
	
	public void updateOvertimeComp() {
		
		BigDecimal amt = getOTBasicAmt();
		
		if(amt.signum() == 0)
			amt = getOTBaseAmt();
		
		if(isMultiplicationOTCalc())
		{
			setNew_A_L1(getAL1Multiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNew_A_L2(getAL2Multiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNew_A_L3(getAL3Multiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNew_A_L1_R(getALR1Multiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNew_A_L2_R(getALR2Multiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNew_A_L3_R(getALR3Multiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNewLeburJamPertama(getFirstOTMultiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
			setNewLeburJamBerikutnya(getNextOTMultiplier().multiply(amt).setScale(0, RoundingMode.HALF_UP));
		}
		else if(amt.signum() > 0)
		{
			setAL1Multiplier(getNew_A_L1().divide(amt, 2, RoundingMode.HALF_UP));
			setAL2Multiplier(getNew_A_L2().divide(amt, 2, RoundingMode.HALF_UP));
			setAL3Multiplier(getNew_A_L3().divide(amt, 2, RoundingMode.HALF_UP));
			setALR1Multiplier(getNew_A_L1_R().divide(amt, 2, RoundingMode.HALF_UP));
			setALR2Multiplier(getNew_A_L2_R().divide(amt, 2, RoundingMode.HALF_UP));
			setALR3Multiplier(getNew_A_L3_R().divide(amt, 2, RoundingMode.HALF_UP));
			setFirstOTMultiplier(getNewLeburJamPertama().divide(amt, 2, RoundingMode.HALF_UP));
			setNextOTMultiplier(getNewLeburJamBerikutnya().divide(amt, 2, RoundingMode.HALF_UP));
		}
		else
		{
			setAL1Multiplier(Env.ZERO);
			setAL2Multiplier(Env.ZERO);
			setAL3Multiplier(Env.ZERO);
			setALR1Multiplier(Env.ZERO);
			setALR2Multiplier(Env.ZERO);
			setALR3Multiplier(Env.ZERO);
			setFirstOTMultiplier(Env.ZERO);
			setNextOTMultiplier(Env.ZERO);
		}
	}
	
	private String checkAllowanceRecord()
	{
		MUNSEmployeeAllowanceRecord[] record = MUNSEmployeeAllowanceRecord.getByContract(getCtx(), get_ID(), get_TrxName());
		
		for(int i=0;i<record.length;i++)
		{
			//@Guntur 2019-04-04
			if(record[i].getMedicalAllowanceUsed().signum() != 0
					|| record[i].getLeaveReservedUsed().signum() != 0)
			{
				continue;
//				return "Allowance record has used, no " + record[i].getDocumentNo();
			}
			
			DB.executeUpdate("DELETE FROM UNS_EmployeeAllowanceRecord WHERE UNS_EmployeeAllowanceRecord_ID = ?",
					record[i].get_ID(), get_TrxName());
		}
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private String updateExistsPresence()
	{
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(getDateContractStart());
		 MUNSPayrollConfiguration config = MUNSPayrollConfiguration.get(
				 getCtx(), getDateContractStart(), getAD_Org_ID(), get_TrxName(), true);
		 MPeriod p = MPeriod.get(getCtx(), getDateContractStart(), getAD_Org_ID(), get_TrxName());
		 
		 if(cal.get(Calendar.DATE) > config.getPayrollDateStart())
		 {
			 cal.add(Calendar.MONTH, 1);
			 p = MPeriod.get(getCtx(), new Timestamp(cal.getTimeInMillis()), getAD_Org_ID(), get_TrxName());
		 }
		 
		 MUNSMonthlyPresenceSummary mps = MUNSMonthlyPresenceSummary.get(getCtx(), getUNS_Employee_ID(), p.get_ID(),
				 getAD_Org_ID(), get_TrxName());
		 if(mps != null)
		 {
			 if(mps.isProcessed())
				 return "Monthly Presence on " + p.getName() + " has completed.";
			 
			 cal.setTime(mps.getEndDate());
			 if(cal.get(Calendar.DATE) < config.getPayrollDateEnd())
			 {
				 
			 }
		 }
		 
		 return null;
	}
	
	public BigDecimal calculateOTBasicAmt(){
		
		double retVal = 0;
		
		double totGPTJ = getNew_G_Pokok().doubleValue();
		m_components = getComponents(true);
		for(int i=0; i<m_components.length; i++)
		{
			if(!m_components[i].isBenefit() || m_components[i].getCostBenefitType() != null)
				continue;
			
			totGPTJ += m_components[i].getAmount().doubleValue();
		}
		
		double totHours = MSysConfig.getDoubleValue("TotalJamKerja_SatuBulan", 173, getAD_Client_ID());
		
		
		retVal = totGPTJ / totHours;
		
		return new BigDecimal(retVal);
	}
	
	public void updateBPJSLog()
	{
		BigDecimal oldVal = Env.ZERO;
		String sql = "SELECT NewValue FROM UNS_BPJSLog WHERE UNS_Employee_ID = ?"
				+ " AND ColumnName = ? AND isActive = 'Y'";
		String oldValStr = DB.getSQLValueString(get_TrxName(), sql, getUNS_Employee_ID(), MUNSBPJSLog.BasicSalary);
		if(!Util.isEmpty(oldValStr, true))
			oldVal = new BigDecimal(oldValStr);
		
		if(oldVal.compareTo(getNew_G_Pokok()) != 0)
		{
			//set BPJS Change Log
			boolean justInit = true;
			if(oldVal.signum() != 0)
				justInit = false;
			MUNSBPJSLog bpjslog = MUNSBPJSLog.setLog(
					getUNS_Employee(), 0 , MUNSBPJSLog.BasicSalary, new Timestamp(System.currentTimeMillis())
					, oldValStr, getNew_G_Pokok().toString(), justInit);
			if(bpjslog == null)
			{
				throw new AdempiereException("Error when try to save BPJS Change Log");
			}		
		}
	}
	
	private boolean removePrevResource()
	{
		if(getUNS_Resource_ID() <=0)
			return true;
		
		String sql = "DELETE FROM UNS_Resource_WorkerLine WHERE ValidFrom = ? AND ValidTo = ? AND Labor_ID = ?"
				+ " AND UNS_Resource_ID = ?";
		return DB.executeUpdate(sql, new Object[]{getDateContractStart(),
				getDateContractEnd(), getUNS_Employee_ID(), getUNS_Resource_ID()}, false, get_TrxName()) >= 0;
	}
	
	@SuppressWarnings("resource")
	public String updateData(boolean differentLegality) {
	
		if(!getDocType().equals(MUNSContractRecommendation.DOCTYPE_Addendum))
			return null;
		
		MUNSEmployee emp = new MUNSEmployee(getCtx(), getUNS_Employee_ID(), get_TrxName());
		emp.setAD_Org_ID(getNewDept_ID());
		emp.setC_BPartner_ID(getNewSectionOfDept_ID());
		if(!emp.save())
			return "Error when try to update Employee";
	
		String sql = "SELECT UNS_YearlyPresenceSummary_ID AS year, UNS_MonthlyPresenceSummary_ID AS month FROM UNS_MonthlyPresenceSummary mps"
				+ " WHERE UNS_employee_ID = ? AND AD_Org_ID = ? AND enddate >= ? AND DocStatus NOT IN ('VO','RE')";
		
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_Employee_ID());
			st.setInt(2, getPrevDept_ID());
			st.setTimestamp(3, getEffectiveDate());
			rs = st.executeQuery();
			while(rs.next())
			{
				int yearlyID = rs.getInt("year");
				int monthlyID = rs.getInt("month");
				
				if(differentLegality)
				{
					String sqlCheck = "SELECT 1 FROM UNS_MonthlyPresenceSummary WHERE UNS_MonthlyPresenceSummary_ID = ?"
							+ " AND StartDate = ? ";
					boolean match = DB.getSQLValue(get_TrxName(), sqlCheck, monthlyID, getEffectiveDate()) == 1;
					if(match)
					{
						String sqlUpdate = "UPDATE UNS_MonthlyPresenceSummary SET AD_Org_ID = ? , C_BPartner_ID = ?"
								+ " WHERE UNS_MonthlyPresenceSummary_ID = ?";
						boolean ok = DB.executeUpdateEx(
								sqlUpdate, new Object[]{getAD_Org_ID(), getNewSectionOfDept_ID(), monthlyID}
								, get_TrxName()) != -1;
						if(!ok)
						{
							return "Error when try to update monthly presence";
						}
						updateDailyPresence(monthlyID, false);
						
						MUNSYearlyPresenceSummary yearly = new MUNSYearlyPresenceSummary(getCtx(), yearlyID, get_TrxName());
						if(!yearly.updateData())
							return "Error when try to update yearly presence summary";
						
						
						//create new yearly
						MUNSYearlyPresenceSummary newYearly = new MUNSYearlyPresenceSummary(getCtx(), emp, 
								yearly.getC_Year_ID(), get_TrxName());
						sqlUpdate = "UPDATE UNS_MonthlyPresenceSummary SET UNS_YearlyPresenceSummary_ID = ?"
								+ " WHERE UNS_MonthlyPresenceSummary_ID = ?";
						ok = DB.executeUpdateEx(
								sqlUpdate, new Object[]{newYearly.get_ID(), monthlyID}, get_TrxName()) != -1;
						
						if(!newYearly.updateData())
							return "Error when try to update new yearly presence summary";
					}
					else
					{
						updateDailyPresence(monthlyID, true);
					}
				}
				else
				{
					String sqlUpdate = "UPDATE UNS_MonthlyPresenceSummary SET AD_Org_ID = ? , C_BPartner_ID = ?"
							+ " WHERE UNS_MonthlyPresenceSummary_ID = ?";
					boolean ok = DB.executeUpdateEx(
							sqlUpdate, new Object[]{getAD_Org_ID(), getNewSectionOfDept_ID(), monthlyID}
							, get_TrxName()) != -1;
					if(!ok)
					{
						return "Error when try to update monthly presence";
					}
					
					sqlUpdate = "UPDATE UNS_YearlyPresenceSummary SET AD_Org_ID = ? , C_BPartner_ID = ?"
							+ " WHERE UNS_YearlyPresenceSummary_ID = ?";
					ok = DB.executeUpdateEx(
							sqlUpdate, new Object[]{getAD_Org_ID(), getNewSectionOfDept_ID(), yearlyID}, get_TrxName()) != -1;
					if(!ok)
						return "Error when try to update yearly presence";
					
					//update daily
					updateDailyPresence(monthlyID, false);
				}
				
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new AdempiereException(ex.getMessage());
		}
		finally{
			DB.close(rs, st);
		}
		
		if(emp.getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_Company))
		{
			MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.getCreate(
					getCtx(), emp, getEffectiveDate(), MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave
					, this, get_TrxName());
			if(!differentLegality)
			{
				if(record.getAD_Org_ID() != getAD_Org_ID())
				{
					record.setAD_Org_ID(getAD_Org_ID());
					record.saveEx();
				}
			}
			
			if(record != null)
				record.reCalculateRecord(getEffectiveDate());
		}
		
		return null;
	}
	
	@SuppressWarnings("resource")
	public void updateDailyPresence(int _MonthlyPresenceID , boolean _isDelete)
	{
		String sql = "SELECT UNS_DailyPresence_ID FROM UNS_DailyPresence WHERE UNS_MonthlyPresenceSummary_ID = ?"
				+ " AND PresenceDate >= ?";
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, _MonthlyPresenceID);
			st.setTimestamp(2, getEffectiveDate());
			rs = st.executeQuery();
			
			while(rs.next())
			{
				MUNSDailyPresence daily = new MUNSDailyPresence(getCtx(), rs.getInt(1), get_TrxName());
				if(_isDelete)
				{
					if(!daily.delete(true, get_TrxName()))
						throw new AdempiereException("Error when try to delete daily presence");
				}
				else
				{
					daily.setAD_Org_ID(getAD_Org_ID());
					daily.setC_BPartner_ID(getNewSectionOfDept_ID());
					if(!daily.save())
						throw new AdempiereException("Error when try to update daily presence");
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
	}
	
	private boolean m_IsFromAnotherProcess = false;
	
	private boolean isFromAnotherProcess()
	{
		return m_IsFromAnotherProcess;
	}
	
	public void setFromAnotherProcess(boolean isFromAnotherProcess)
	{
		m_IsFromAnotherProcess = isFromAnotherProcess;
	}
}