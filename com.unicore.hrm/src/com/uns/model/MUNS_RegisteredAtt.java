/**
 * 
 */
package com.uns.model;

import java.util.Properties;

/**
 * @author MuhAmin
 *
 */
public class MUNS_RegisteredAtt extends X_UNS_RegisteredAtt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3312094363770077401L;

	public MUNS_RegisteredAtt(Properties ctx, int UNS_RegisteredAtt_ID,
			String trxName) {
		super(ctx, UNS_RegisteredAtt_ID, trxName);
	}

}
