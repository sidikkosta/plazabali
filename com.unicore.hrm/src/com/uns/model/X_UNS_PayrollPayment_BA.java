/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PayrollPayment_BA
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PayrollPayment_BA extends PO implements I_UNS_PayrollPayment_BA, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180601L;

    /** Standard Constructor */
    public X_UNS_PayrollPayment_BA (Properties ctx, int UNS_PayrollPayment_BA_ID, String trxName)
    {
      super (ctx, UNS_PayrollPayment_BA_ID, trxName);
      /** if (UNS_PayrollPayment_BA_ID == 0)
        {
			setC_BankAccount_ID (0);
			setC_BPartner_ID (0);
			setC_Charge_ID (0);
			setProcessed (false);
// N
			setTenderType (null);
// X
			setUNS_PayrollPayment_BA_ID (0);
			setUNS_PayrollPayment_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PayrollPayment_BA (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PayrollPayment_BA[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankAccount)MTable.get(getCtx(), org.compiere.model.I_C_BankAccount.Table_Name)
			.getPO(getC_BankAccount_ID(), get_TrxName());	}

	/** Set Cash / Bank Account.
		@param C_BankAccount_ID 
		Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID)
	{
		if (C_BankAccount_ID < 1) 
			set_Value (COLUMNNAME_C_BankAccount_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankAccount_ID, Integer.valueOf(C_BankAccount_ID));
	}

	/** Get Cash / Bank Account.
		@return Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Charge_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Canteen = CNT */
	public static final String COSTBENEFITTYPE_Canteen = "CNT";
	/** Cooperative = CPS */
	public static final String COSTBENEFITTYPE_Cooperative = "CPS";
	/** Pinjaman Karyawan = PKR */
	public static final String COSTBENEFITTYPE_PinjamanKaryawan = "PKR";
	/** Bonuses = BNS */
	public static final String COSTBENEFITTYPE_Bonuses = "BNS";
	/** Tunjangan Hari Raya = THR */
	public static final String COSTBENEFITTYPE_TunjanganHariRaya = "THR";
	/** Set Cost / Benefit Type.
		@param CostBenefitType Cost / Benefit Type	  */
	public void setCostBenefitType (String CostBenefitType)
	{

		set_Value (COLUMNNAME_CostBenefitType, CostBenefitType);
	}

	/** Get Cost / Benefit Type.
		@return Cost / Benefit Type	  */
	public String getCostBenefitType () 
	{
		return (String)get_Value(COLUMNNAME_CostBenefitType);
	}

	public org.compiere.model.I_C_Payment getC_Payment() throws RuntimeException
    {
		return (org.compiere.model.I_C_Payment)MTable.get(getCtx(), org.compiere.model.I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Payment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create lines from.
		@param CreateFrom 
		Process which will generate a new document lines based on an existing document
	  */
	public void setCreateFrom (String CreateFrom)
	{
		set_Value (COLUMNNAME_CreateFrom, CreateFrom);
	}

	/** Get Create lines from.
		@return Process which will generate a new document lines based on an existing document
	  */
	public String getCreateFrom () 
	{
		return (String)get_Value(COLUMNNAME_CreateFrom);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Is Benefit?.
		@param IsBenefit 
		If checked it is a benefit and will be added to employee's payroll, otherwise it is a cost and will be deducted.
	  */
	public void setIsBenefit (boolean IsBenefit)
	{
		set_Value (COLUMNNAME_IsBenefit, Boolean.valueOf(IsBenefit));
	}

	/** Get Is Benefit?.
		@return If checked it is a benefit and will be added to employee's payroll, otherwise it is a cost and will be deducted.
	  */
	public boolean isBenefit () 
	{
		Object oo = get_Value(COLUMNNAME_IsBenefit);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** TenderType AD_Reference_ID=214 */
	public static final int TENDERTYPE_AD_Reference_ID=214;
	/** Credit Card = C */
	public static final String TENDERTYPE_CreditCard = "C";
	/** Check = K */
	public static final String TENDERTYPE_Check = "K";
	/** Direct Deposit = A */
	public static final String TENDERTYPE_DirectDeposit = "A";
	/** Direct Debit = D */
	public static final String TENDERTYPE_DirectDebit = "D";
	/** Account = T */
	public static final String TENDERTYPE_Account = "T";
	/** Cash = X */
	public static final String TENDERTYPE_Cash = "X";
	/** Set Tender type.
		@param TenderType 
		Method of Payment
	  */
	public void setTenderType (String TenderType)
	{

		set_ValueNoCheck (COLUMNNAME_TenderType, TenderType);
	}

	/** Get Tender type.
		@return Method of Payment
	  */
	public String getTenderType () 
	{
		return (String)get_Value(COLUMNNAME_TenderType);
	}

	/** Set Total Amount.
		@param TotalAmt 
		Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt)
	{
		set_ValueNoCheck (COLUMNNAME_TotalAmt, TotalAmt);
	}

	/** Get Total Amount.
		@return Total Amount
	  */
	public BigDecimal getTotalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payroll Bank Account.
		@param UNS_PayrollPayment_BA_ID Payroll Bank Account	  */
	public void setUNS_PayrollPayment_BA_ID (int UNS_PayrollPayment_BA_ID)
	{
		if (UNS_PayrollPayment_BA_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_BA_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_BA_ID, Integer.valueOf(UNS_PayrollPayment_BA_ID));
	}

	/** Get Payroll Bank Account.
		@return Payroll Bank Account	  */
	public int getUNS_PayrollPayment_BA_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollPayment_BA_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PayrollPayment_BA_UU.
		@param UNS_PayrollPayment_BA_UU UNS_PayrollPayment_BA_UU	  */
	public void setUNS_PayrollPayment_BA_UU (String UNS_PayrollPayment_BA_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_BA_UU, UNS_PayrollPayment_BA_UU);
	}

	/** Get UNS_PayrollPayment_BA_UU.
		@return UNS_PayrollPayment_BA_UU	  */
	public String getUNS_PayrollPayment_BA_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PayrollPayment_BA_UU);
	}

	public com.uns.model.I_UNS_PayrollPayment getUNS_PayrollPayment() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PayrollPayment)MTable.get(getCtx(), com.uns.model.I_UNS_PayrollPayment.Table_Name)
			.getPO(getUNS_PayrollPayment_ID(), get_TrxName());	}

	/** Set Payroll Payment.
		@param UNS_PayrollPayment_ID Payroll Payment	  */
	public void setUNS_PayrollPayment_ID (int UNS_PayrollPayment_ID)
	{
		if (UNS_PayrollPayment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollPayment_ID, Integer.valueOf(UNS_PayrollPayment_ID));
	}

	/** Get Payroll Payment.
		@return Payroll Payment	  */
	public int getUNS_PayrollPayment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollPayment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}