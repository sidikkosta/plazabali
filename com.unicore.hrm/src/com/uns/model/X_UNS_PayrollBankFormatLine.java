/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_PayrollBankFormatLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PayrollBankFormatLine extends PO implements I_UNS_PayrollBankFormatLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181214L;

    /** Standard Constructor */
    public X_UNS_PayrollBankFormatLine (Properties ctx, int UNS_PayrollBankFormatLine_ID, String trxName)
    {
      super (ctx, UNS_PayrollBankFormatLine_ID, trxName);
      /** if (UNS_PayrollBankFormatLine_ID == 0)
        {
			setName (null);
			setSourceValue (null);
			setUNS_PayrollBankFormat_ID (0);
			setUNS_PayrollBankFormatLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PayrollBankFormatLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PayrollBankFormatLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** NIK = 01 */
	public static final String SOURCEVALUE_NIK = "01";
	/** Holder Name = 02 */
	public static final String SOURCEVALUE_HolderName = "02";
	/** Account No = 03 */
	public static final String SOURCEVALUE_AccountNo = "03";
	/** Take Home Pay = 04 */
	public static final String SOURCEVALUE_TakeHomePay = "04";
	/** KCP = 05 */
	public static final String SOURCEVALUE_KCP = "05";
	/** Set Source Value.
		@param SourceValue Source Value	  */
	public void setSourceValue (String SourceValue)
	{

		set_Value (COLUMNNAME_SourceValue, SourceValue);
	}

	/** Get Source Value.
		@return Source Value	  */
	public String getSourceValue () 
	{
		return (String)get_Value(COLUMNNAME_SourceValue);
	}

	public com.uns.model.I_UNS_PayrollBankFormat getUNS_PayrollBankFormat() throws RuntimeException
    {
		return (com.uns.model.I_UNS_PayrollBankFormat)MTable.get(getCtx(), com.uns.model.I_UNS_PayrollBankFormat.Table_Name)
			.getPO(getUNS_PayrollBankFormat_ID(), get_TrxName());	}

	/** Set Payroll Bank Format.
		@param UNS_PayrollBankFormat_ID Payroll Bank Format	  */
	public void setUNS_PayrollBankFormat_ID (int UNS_PayrollBankFormat_ID)
	{
		if (UNS_PayrollBankFormat_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollBankFormat_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollBankFormat_ID, Integer.valueOf(UNS_PayrollBankFormat_ID));
	}

	/** Get Payroll Bank Format.
		@return Payroll Bank Format	  */
	public int getUNS_PayrollBankFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollBankFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Payroll Bank Format Line.
		@param UNS_PayrollBankFormatLine_ID Payroll Bank Format Line	  */
	public void setUNS_PayrollBankFormatLine_ID (int UNS_PayrollBankFormatLine_ID)
	{
		if (UNS_PayrollBankFormatLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollBankFormatLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PayrollBankFormatLine_ID, Integer.valueOf(UNS_PayrollBankFormatLine_ID));
	}

	/** Get Payroll Bank Format Line.
		@return Payroll Bank Format Line	  */
	public int getUNS_PayrollBankFormatLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PayrollBankFormatLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PayrollBankFormatLine_UU.
		@param UNS_PayrollBankFormatLine_UU UNS_PayrollBankFormatLine_UU	  */
	public void setUNS_PayrollBankFormatLine_UU (String UNS_PayrollBankFormatLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PayrollBankFormatLine_UU, UNS_PayrollBankFormatLine_UU);
	}

	/** Get UNS_PayrollBankFormatLine_UU.
		@return UNS_PayrollBankFormatLine_UU	  */
	public String getUNS_PayrollBankFormatLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PayrollBankFormatLine_UU);
	}
}