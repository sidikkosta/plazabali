/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_ShopRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ShopRecap extends PO implements I_UNS_ShopRecap, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191119L;

    /** Standard Constructor */
    public X_UNS_ShopRecap (Properties ctx, int UNS_ShopRecap_ID, String trxName)
    {
      super (ctx, UNS_ShopRecap_ID, trxName);
      /** if (UNS_ShopRecap_ID == 0)
        {
			setDistributableServiceCharge (Env.ZERO);
// 0
			setEmployeePortion (Env.ZERO);
// 0
			setGroupCashierIncentive (Env.ZERO);
// 0
			setGroupSalesIncentive (Env.ZERO);
// 0
			setIndividualTargetAmt (Env.ZERO);
// 0
			setMinIdvSalesTargetAmt (Env.ZERO);
// 0
			setMinSubSalesTargetAmt (Env.ZERO);
// 0
			setNetSalesRealizationAmt (Env.ZERO);
// 0
			setNetSalesTarget (Env.ZERO);
// 0
			setNotPresentSalesAmt (Env.ZERO);
// 0
			setProcessed (false);
// N
			setSalesCount (0);
// 0
			setSupportPoints (Env.ZERO);
// 0
			setSupportPortion (Env.ZERO);
// 0
			setTotalCashierSession (Env.ZERO);
// 0
			setTotalPoint (Env.ZERO);
// 0
			setUNS_ShopRecap_ID (0);
			setUNS_ShopRecap_UU (null);
			setUNS_SubregionRecap_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ShopRecap (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ShopRecap[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Calculation Class.
		@param CalculationClass 
		Java Class for calculation, implementing Interface Measure
	  */
	public void setCalculationClass (String CalculationClass)
	{
		set_Value (COLUMNNAME_CalculationClass, CalculationClass);
	}

	/** Get Calculation Class.
		@return Java Class for calculation, implementing Interface Measure
	  */
	public String getCalculationClass () 
	{
		return (String)get_Value(COLUMNNAME_CalculationClass);
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Distributable Service Charge.
		@param DistributableServiceCharge 
		The amount of service charge will be distributed to all staff in the shop proportinally
	  */
	public void setDistributableServiceCharge (BigDecimal DistributableServiceCharge)
	{
		set_Value (COLUMNNAME_DistributableServiceCharge, DistributableServiceCharge);
	}

	/** Get Distributable Service Charge.
		@return The amount of service charge will be distributed to all staff in the shop proportinally
	  */
	public BigDecimal getDistributableServiceCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DistributableServiceCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Employee Portion.
		@param EmployeePortion Employee Portion	  */
	public void setEmployeePortion (BigDecimal EmployeePortion)
	{
		set_Value (COLUMNNAME_EmployeePortion, EmployeePortion);
	}

	/** Get Employee Portion.
		@return Employee Portion	  */
	public BigDecimal getEmployeePortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EmployeePortion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Group Cashier Incentive.
		@param GroupCashierIncentive 
		Percentage for cashier from group incentives.
	  */
	public void setGroupCashierIncentive (BigDecimal GroupCashierIncentive)
	{
		set_Value (COLUMNNAME_GroupCashierIncentive, GroupCashierIncentive);
	}

	/** Get Group Cashier Incentive.
		@return Percentage for cashier from group incentives.
	  */
	public BigDecimal getGroupCashierIncentive () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GroupCashierIncentive);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Group Sales Incentive.
		@param GroupSalesIncentive 
		Percentage for sales person from group incentives.
	  */
	public void setGroupSalesIncentive (BigDecimal GroupSalesIncentive)
	{
		set_Value (COLUMNNAME_GroupSalesIncentive, GroupSalesIncentive);
	}

	/** Get Group Sales Incentive.
		@return Percentage for sales person from group incentives.
	  */
	public BigDecimal getGroupSalesIncentive () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GroupSalesIncentive);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Individual Target.
		@param IndividualTargetAmt 
		Individual Target Amount
	  */
	public void setIndividualTargetAmt (BigDecimal IndividualTargetAmt)
	{
		set_Value (COLUMNNAME_IndividualTargetAmt, IndividualTargetAmt);
	}

	/** Get Individual Target.
		@return Individual Target Amount
	  */
	public BigDecimal getIndividualTargetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_IndividualTargetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Load Shop Staff.
		@param LoadShopStaff Load Shop Staff	  */
	public void setLoadShopStaff (String LoadShopStaff)
	{
		set_Value (COLUMNNAME_LoadShopStaff, LoadShopStaff);
	}

	/** Get Load Shop Staff.
		@return Load Shop Staff	  */
	public String getLoadShopStaff () 
	{
		return (String)get_Value(COLUMNNAME_LoadShopStaff);
	}

	/** Set Min. Individual Sales Target.
		@param MinIdvSalesTargetAmt 
		Minimum target amount for each sales person
	  */
	public void setMinIdvSalesTargetAmt (BigDecimal MinIdvSalesTargetAmt)
	{
		set_Value (COLUMNNAME_MinIdvSalesTargetAmt, MinIdvSalesTargetAmt);
	}

	/** Get Min. Individual Sales Target.
		@return Minimum target amount for each sales person
	  */
	public BigDecimal getMinIdvSalesTargetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinIdvSalesTargetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Min. Subregion Sales Target.
		@param MinSubSalesTargetAmt 
		Minimum target amount for shop
	  */
	public void setMinSubSalesTargetAmt (BigDecimal MinSubSalesTargetAmt)
	{
		set_Value (COLUMNNAME_MinSubSalesTargetAmt, MinSubSalesTargetAmt);
	}

	/** Get Min. Subregion Sales Target.
		@return Minimum target amount for shop
	  */
	public BigDecimal getMinSubSalesTargetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinSubSalesTargetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Net Sales Realization.
		@param NetSalesRealizationAmt 
		Total realized net sales within 1 month from all stores
	  */
	public void setNetSalesRealizationAmt (BigDecimal NetSalesRealizationAmt)
	{
		set_Value (COLUMNNAME_NetSalesRealizationAmt, NetSalesRealizationAmt);
	}

	/** Get Net Sales Realization.
		@return Total realized net sales within 1 month from all stores
	  */
	public BigDecimal getNetSalesRealizationAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesRealizationAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Net Sales Target.
		@param NetSalesTarget 
		Net sales target from all stores in a certain subregion
	  */
	public void setNetSalesTarget (BigDecimal NetSalesTarget)
	{
		set_Value (COLUMNNAME_NetSalesTarget, NetSalesTarget);
	}

	/** Get Net Sales Target.
		@return Net sales target from all stores in a certain subregion
	  */
	public BigDecimal getNetSalesTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NetSalesTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Not Present Sales.
		@param NotPresentSalesAmt 
		Amount in a defined currency
	  */
	public void setNotPresentSalesAmt (BigDecimal NotPresentSalesAmt)
	{
		set_Value (COLUMNNAME_NotPresentSalesAmt, NotPresentSalesAmt);
	}

	/** Get Not Present Sales.
		@return Amount in a defined currency
	  */
	public BigDecimal getNotPresentSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NotPresentSalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Sales Count.
		@param SalesCount 
		Number of employees in a store excluding supervisor
	  */
	public void setSalesCount (int SalesCount)
	{
		set_Value (COLUMNNAME_SalesCount, Integer.valueOf(SalesCount));
	}

	/** Get Sales Count.
		@return Number of employees in a store excluding supervisor
	  */
	public int getSalesCount () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SalesCount);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Set Service Charge.
		@param setServiceCharge Set Service Charge	  */
	public void setsetServiceCharge (String setServiceCharge)
	{
		set_Value (COLUMNNAME_setServiceCharge, setServiceCharge);
	}

	/** Get Set Service Charge.
		@return Set Service Charge	  */
	public String getsetServiceCharge () 
	{
		return (String)get_Value(COLUMNNAME_setServiceCharge);
	}

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getShop_ID(), get_TrxName());	}

	/** Set Shop.
		@param Shop_ID 
		Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID)
	{
		if (Shop_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, Integer.valueOf(Shop_ID));
	}

	/** Get Shop.
		@return Identifies a Shop
	  */
	public int getShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Shop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Support Points.
		@param SupportPoints 
		Total point of support staffs
	  */
	public void setSupportPoints (BigDecimal SupportPoints)
	{
		set_Value (COLUMNNAME_SupportPoints, SupportPoints);
	}

	/** Get Support Points.
		@return Total point of support staffs
	  */
	public BigDecimal getSupportPoints () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SupportPoints);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Support Portion (%).
		@param SupportPortion 
		Percentage portion for the support employee (support dept)
	  */
	public void setSupportPortion (BigDecimal SupportPortion)
	{
		set_Value (COLUMNNAME_SupportPortion, SupportPortion);
	}

	/** Get Support Portion (%).
		@return Percentage portion for the support employee (support dept)
	  */
	public BigDecimal getSupportPortion () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SupportPortion);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Cashier Session.
		@param TotalCashierSession 
		Total Cashier Session
	  */
	public void setTotalCashierSession (BigDecimal TotalCashierSession)
	{
		set_Value (COLUMNNAME_TotalCashierSession, TotalCashierSession);
	}

	/** Get Total Cashier Session.
		@return Total Cashier Session
	  */
	public BigDecimal getTotalCashierSession () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalCashierSession);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Points.
		@param TotalPoint 
		Sum of staff points (ex. supervisor)
	  */
	public void setTotalPoint (BigDecimal TotalPoint)
	{
		set_ValueNoCheck (COLUMNNAME_TotalPoint, TotalPoint);
	}

	/** Get Total Points.
		@return Sum of staff points (ex. supervisor)
	  */
	public BigDecimal getTotalPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalPoint);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Service Charge.
		@param TotalServiceChargeAmt Total Service Charge	  */
	public void setTotalServiceChargeAmt (BigDecimal TotalServiceChargeAmt)
	{
		set_Value (COLUMNNAME_TotalServiceChargeAmt, TotalServiceChargeAmt);
	}

	/** Get Total Service Charge.
		@return Total Service Charge	  */
	public BigDecimal getTotalServiceChargeAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalServiceChargeAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Tandem Count.
		@param TotalTandemCount 
		Total cashier tandem portion in 1 month
	  */
	public void setTotalTandemCount (BigDecimal TotalTandemCount)
	{
		set_Value (COLUMNNAME_TotalTandemCount, TotalTandemCount);
	}

	/** Get Total Tandem Count.
		@return Total cashier tandem portion in 1 month
	  */
	public BigDecimal getTotalTandemCount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalTandemCount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Periodic Cost And Benefit.
		@param UNS_PeriodicCostBenefit_ID Periodic Cost And Benefit	  */
	public void setUNS_PeriodicCostBenefit_ID (int UNS_PeriodicCostBenefit_ID)
	{
		if (UNS_PeriodicCostBenefit_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PeriodicCostBenefit_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PeriodicCostBenefit_ID, Integer.valueOf(UNS_PeriodicCostBenefit_ID));
	}

	/** Get Periodic Cost And Benefit.
		@return Periodic Cost And Benefit	  */
	public int getUNS_PeriodicCostBenefit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PeriodicCostBenefit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Shop Sales Recapitulation.
		@param UNS_ShopRecap_ID Shop Sales Recapitulation	  */
	public void setUNS_ShopRecap_ID (int UNS_ShopRecap_ID)
	{
		if (UNS_ShopRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_ID, Integer.valueOf(UNS_ShopRecap_ID));
	}

	/** Get Shop Sales Recapitulation.
		@return Shop Sales Recapitulation	  */
	public int getUNS_ShopRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShopRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ShopRecap_UU.
		@param UNS_ShopRecap_UU UNS_ShopRecap_UU	  */
	public void setUNS_ShopRecap_UU (String UNS_ShopRecap_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_UU, UNS_ShopRecap_UU);
	}

	/** Get UNS_ShopRecap_UU.
		@return UNS_ShopRecap_UU	  */
	public String getUNS_ShopRecap_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ShopRecap_UU);
	}

	public com.uns.model.I_UNS_SubregionRecap getUNS_SubregionRecap() throws RuntimeException
    {
		return (com.uns.model.I_UNS_SubregionRecap)MTable.get(getCtx(), com.uns.model.I_UNS_SubregionRecap.Table_Name)
			.getPO(getUNS_SubregionRecap_ID(), get_TrxName());	}

	/** Set Subregion Recapitulation.
		@param UNS_SubregionRecap_ID Subregion Recapitulation	  */
	public void setUNS_SubregionRecap_ID (int UNS_SubregionRecap_ID)
	{
		if (UNS_SubregionRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SubregionRecap_ID, Integer.valueOf(UNS_SubregionRecap_ID));
	}

	/** Get Subregion Recapitulation.
		@return Subregion Recapitulation	  */
	public int getUNS_SubregionRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SubregionRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}