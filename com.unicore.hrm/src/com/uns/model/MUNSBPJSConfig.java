package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSBPJSConfig extends X_UNS_BPJSConfig {

	/**
	 * @author Hamba Allah
	 */
	private static final long serialVersionUID = -3958135480916739152L;

	public MUNSBPJSConfig(Properties ctx, int UNS_BPJSConfig_ID, String trxName) {
		super(ctx, UNS_BPJSConfig_ID, trxName);
		
	}

	public MUNSBPJSConfig(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		//check duplicate
		String duplicateDoc = getDuplicateDoc();
		if(duplicateDoc != null)
		{
			log.saveError("Error save", "Duplicate configuration with document no: "+duplicateDoc);
			return false;
		}
		
		return true;
	}
	
	public static MUNSBPJSConfig getConfig(Properties ctx, MUNSContractRecommendation recommendation, String trxName)
	{
		if(recommendation == null)
			return null;
		
		//find position, section, org
		MUNSBPJSConfig config = MUNSBPJSConfig.getConfig(
				ctx, recommendation.getAD_Org_ID(), recommendation.getNewJob_ID(), recommendation.getNewSectionOfDept_ID(), trxName);
		
		if(config == null)
		{
			//find position, org
			config = MUNSBPJSConfig.getConfig(
					ctx, recommendation.getAD_Org_ID(), recommendation.getNewJob_ID(), 0, trxName);
		}
		
		if(config == null)
		{
			//find section, org
			config = MUNSBPJSConfig.getConfig(
					ctx, recommendation.getAD_Org_ID(), 0, recommendation.getNewSectionOfDept_ID(), trxName);
		}
		
		if(config == null)
		{
			//find org only
			config = MUNSBPJSConfig.getConfig(ctx, 0, 0, 0, trxName);
		}
		
		return config;
	}
	
	public static MUNSBPJSConfig getConfig(Properties ctx, int AD_Org_ID, int Position_ID, int Section_ID, String trxName)
	{
		StringBuilder wc = new StringBuilder("IsActive = 'Y'");
		
		if(AD_Org_ID > 0)
			wc.append("AND AD_Org_ID IN ("+AD_Org_ID+" ,0)");
		else
			wc.append("AND AD_Org_ID = "+AD_Org_ID);
		
		if(Position_ID > 0)
			wc.append("AND C_Job_ID = "+Position_ID);
		else
			wc.append("AND (C_Job_ID IS NULL OR C_Job_ID = 0)");
		
		if(Section_ID > 0)
			wc.append("AND C_BPartner_ID = "+Section_ID);
		else
			wc.append("AND (C_BPartner_ID IS NULL OR C_BPartner_ID = 0)");
			
		MUNSBPJSConfig config = Query.get(
				ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name, wc.toString(), trxName)
				.setOrderBy("AD_Org_ID DESC")
				.firstOnly();
		
		return config;
	}
	
	public String getDuplicateDoc()
	{
		StringBuilder sql = new StringBuilder("SELECT DocumentNo FROM UNS_BPJSConfig WHERE"
				+ " AD_Org_ID = ? AND IsActive = ? AND UNS_BPJSConfig_ID <> ?");
		
		if(getC_Job_ID() > 0)
			sql.append(" AND C_Job_ID = "+getC_Job_ID());
		else
			sql.append(" AND (C_Job_ID IS NULL OR C_Job_ID = 0)");
		
		if(getC_BPartner_ID() > 0)
			sql.append(" AND C_BPartner_ID = "+getC_BPartner_ID());
		else
			sql.append(" AND (C_BPartner_ID IS NULL OR C_BPartner_ID = 0)");
		
		String retVal = DB.getSQLValueString(get_TrxName(), sql.toString(), getAD_Org_ID(),"Y", getUNS_BPJSConfig_ID());
		
		return retVal;
	}
}
