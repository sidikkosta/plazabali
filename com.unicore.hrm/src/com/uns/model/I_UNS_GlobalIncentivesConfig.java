/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_GlobalIncentivesConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_GlobalIncentivesConfig 
{

    /** TableName=UNS_GlobalIncentivesConfig */
    public static final String Table_Name = "UNS_GlobalIncentivesConfig";

    /** AD_Table_ID=1000453 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Period_ID */
    public static final String COLUMNNAME_C_Period_ID = "C_Period_ID";

	/** Set Period.
	  * Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID);

	/** Get Period.
	  * Period of the Calendar
	  */
	public int getC_Period_ID();

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException;

    /** Column name CashierPortion */
    public static final String COLUMNNAME_CashierPortion = "CashierPortion";

	/** Set Cashier Portion (%).
	  * Cashier Percentage from Group Percentage/Incentives Amount
	  */
	public void setCashierPortion (BigDecimal CashierPortion);

	/** Get Cashier Portion (%).
	  * Cashier Percentage from Group Percentage/Incentives Amount
	  */
	public BigDecimal getCashierPortion();

    /** Column name CashierTandemPortion */
    public static final String COLUMNNAME_CashierTandemPortion = "CashierTandemPortion";

	/** Set Cashier Tandem Portion (%).
	  * The POS Session Count portion in percentage for staff being a tandem to help main cashier in a session.
	  */
	public void setCashierTandemPortion (BigDecimal CashierTandemPortion);

	/** Get Cashier Tandem Portion (%).
	  * The POS Session Count portion in percentage for staff being a tandem to help main cashier in a session.
	  */
	public BigDecimal getCashierTandemPortion();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name GroupPercent */
    public static final String COLUMNNAME_GroupPercent = "GroupPercent";

	/** Set Group (%).
	  * Group Incentives Percentage from Net-Sales
	  */
	public void setGroupPercent (BigDecimal GroupPercent);

	/** Get Group (%).
	  * Group Incentives Percentage from Net-Sales
	  */
	public BigDecimal getGroupPercent();

    /** Column name IndividuPercent */
    public static final String COLUMNNAME_IndividuPercent = "IndividuPercent";

	/** Set Individu (%).
	  * Incentives Percentage from Net-Sales for certain person
	  */
	public void setIndividuPercent (BigDecimal IndividuPercent);

	/** Get Individu (%).
	  * Incentives Percentage from Net-Sales for certain person
	  */
	public BigDecimal getIndividuPercent();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name JuniorManagerPoint */
    public static final String COLUMNNAME_JuniorManagerPoint = "JuniorManagerPoint";

	/** Set Junior Manager Point.
	  * The point entitled for Junior Manager position
	  */
	public void setJuniorManagerPoint (BigDecimal JuniorManagerPoint);

	/** Get Junior Manager Point.
	  * The point entitled for Junior Manager position
	  */
	public BigDecimal getJuniorManagerPoint();

    /** Column name ManagerPoint */
    public static final String COLUMNNAME_ManagerPoint = "ManagerPoint";

	/** Set Manager Point.
	  * The point entitled for Manager position
	  */
	public void setManagerPoint (BigDecimal ManagerPoint);

	/** Get Manager Point.
	  * The point entitled for Manager position
	  */
	public BigDecimal getManagerPoint();

    /** Column name MinIndividualTarget */
    public static final String COLUMNNAME_MinIndividualTarget = "MinIndividualTarget";

	/** Set Min. Individual Target (%)	  */
	public void setMinIndividualTarget (BigDecimal MinIndividualTarget);

	/** Get Min. Individual Target (%)	  */
	public BigDecimal getMinIndividualTarget();

    /** Column name MinSubregionTarget */
    public static final String COLUMNNAME_MinSubregionTarget = "MinSubregionTarget";

	/** Set Min. Subregion Target (%)	  */
	public void setMinSubregionTarget (BigDecimal MinSubregionTarget);

	/** Get Min. Subregion Target (%)	  */
	public BigDecimal getMinSubregionTarget();

    /** Column name SCForEmployee */
    public static final String COLUMNNAME_SCForEmployee = "SCForEmployee";

	/** Set Service Charge For Employee (%).
	  * Percentage of Service Charge for an employee
	  */
	public void setSCForEmployee (BigDecimal SCForEmployee);

	/** Get Service Charge For Employee (%).
	  * Percentage of Service Charge for an employee
	  */
	public BigDecimal getSCForEmployee();

    /** Column name SeniorSupervisorPoint */
    public static final String COLUMNNAME_SeniorSupervisorPoint = "SeniorSupervisorPoint";

	/** Set Senior Supervisor Point.
	  * The point entitled for Senior Supervisor position
	  */
	public void setSeniorSupervisorPoint (BigDecimal SeniorSupervisorPoint);

	/** Get Senior Supervisor Point.
	  * The point entitled for Senior Supervisor position
	  */
	public BigDecimal getSeniorSupervisorPoint();

    /** Column name ShopType */
    public static final String COLUMNNAME_ShopType = "ShopType";

	/** Set Shop Type	  */
	public void setShopType (String ShopType);

	/** Get Shop Type	  */
	public String getShopType();

    /** Column name StaffPoint */
    public static final String COLUMNNAME_StaffPoint = "StaffPoint";

	/** Set Staff Point.
	  * The point entitled for Staff position
	  */
	public void setStaffPoint (BigDecimal StaffPoint);

	/** Get Staff Point.
	  * The point entitled for Staff position
	  */
	public BigDecimal getStaffPoint();

    /** Column name StoreSupervisorPoint */
    public static final String COLUMNNAME_StoreSupervisorPoint = "StoreSupervisorPoint";

	/** Set Supervisor Point.
	  * The point entitled for Supervisor position
	  */
	public void setStoreSupervisorPoint (BigDecimal StoreSupervisorPoint);

	/** Get Supervisor Point.
	  * The point entitled for Supervisor position
	  */
	public BigDecimal getStoreSupervisorPoint();

    /** Column name SupervisorPortion */
    public static final String COLUMNNAME_SupervisorPortion = "SupervisorPortion";

	/** Set Supervisor Portion (%).
	  * (Store & Senior) Supervisor Percentage from Group Incentives Amount/Percentage
	  */
	public void setSupervisorPortion (BigDecimal SupervisorPortion);

	/** Get Supervisor Portion (%).
	  * (Store & Senior) Supervisor Percentage from Group Incentives Amount/Percentage
	  */
	public BigDecimal getSupervisorPortion();

    /** Column name SupportPortion */
    public static final String COLUMNNAME_SupportPortion = "SupportPortion";

	/** Set Support Portion (%).
	  * Percentage portion for the support employee (support dept)
	  */
	public void setSupportPortion (BigDecimal SupportPortion);

	/** Get Support Portion (%).
	  * Percentage portion for the support employee (support dept)
	  */
	public BigDecimal getSupportPortion();

    /** Column name UNS_GlobalIncentivesConfig_ID */
    public static final String COLUMNNAME_UNS_GlobalIncentivesConfig_ID = "UNS_GlobalIncentivesConfig_ID";

	/** Set Global Incentives Configuration	  */
	public void setUNS_GlobalIncentivesConfig_ID (int UNS_GlobalIncentivesConfig_ID);

	/** Get Global Incentives Configuration	  */
	public int getUNS_GlobalIncentivesConfig_ID();

    /** Column name UNS_GlobalIncentivesConfig_UU */
    public static final String COLUMNNAME_UNS_GlobalIncentivesConfig_UU = "UNS_GlobalIncentivesConfig_UU";

	/** Set Global Incentives Configuration	  */
	public void setUNS_GlobalIncentivesConfig_UU (String UNS_GlobalIncentivesConfig_UU);

	/** Get Global Incentives Configuration	  */
	public String getUNS_GlobalIncentivesConfig_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
