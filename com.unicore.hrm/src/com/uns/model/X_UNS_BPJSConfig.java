/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_BPJSConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_BPJSConfig extends PO implements I_UNS_BPJSConfig, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181206L;

    /** Standard Constructor */
    public X_UNS_BPJSConfig (Properties ctx, int UNS_BPJSConfig_ID, String trxName)
    {
      super (ctx, UNS_BPJSConfig_ID, trxName);
      /** if (UNS_BPJSConfig_ID == 0)
        {
			setJHTBasicCalculation (null);
// BGP
			setJHTPaidByCompany (Env.ZERO);
// 0
			setJHTPaidByEmployee (Env.ZERO);
// 0
			setJKBasicCalculation (null);
// BGP
			setJKKBasicCalculation (null);
// BGP
			setJKKPaidByCompany (Env.ZERO);
// 0
			setJKKPaidByEmployee (Env.ZERO);
// 0
			setJKPaidByCompany (Env.ZERO);
// 0
			setJKPaidByEmployee (Env.ZERO);
// 0
			setJPBasicCalculation (null);
// BGP
			setJPKBasicCalculation (null);
// BGP
			setJPKKawinPaidByCompany (Env.ZERO);
// 0
			setJPKKawinPaidByEmployee (Env.ZERO);
// 0
			setJPKLajangPaidByCompany (Env.ZERO);
// 0
			setJPKLajangPaidByEmployee (Env.ZERO);
// 0
			setJPPaidByCompany (Env.ZERO);
// 0
			setJPPaidByEmployee (Env.ZERO);
// 0
			setMaxAmtToCalcJHT (Env.ZERO);
// 0
			setMaxAmtToCalcJK (Env.ZERO);
// 0
			setMaxAmtToCalcJKK (Env.ZERO);
// 0
			setMaxAmtToCalcJP (Env.ZERO);
// 0
			setMaxAmtToCalcJPK (Env.ZERO);
// 0
        } */
    }

    /** Load Constructor */
    public X_UNS_BPJSConfig (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_BPJSConfig[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Job getC_Job() throws RuntimeException
    {
		return (org.compiere.model.I_C_Job)MTable.get(getCtx(), org.compiere.model.I_C_Job.Table_Name)
			.getPO(getC_Job_ID(), get_TrxName());	}

	/** Set Position.
		@param C_Job_ID 
		Job Position
	  */
	public void setC_Job_ID (int C_Job_ID)
	{
		if (C_Job_ID < 1) 
			set_Value (COLUMNNAME_C_Job_ID, null);
		else 
			set_Value (COLUMNNAME_C_Job_ID, Integer.valueOf(C_Job_ID));
	}

	/** Get Position.
		@return Job Position
	  */
	public int getC_Job_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Job_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Base On Brutto Salary = BGB */
	public static final String JHTBASICCALCULATION_BaseOnBruttoSalary = "BGB";
	/** Base On Netto Salary = BGN */
	public static final String JHTBASICCALCULATION_BaseOnNettoSalary = "BGN";
	/** Base On Basic Salary = BGP */
	public static final String JHTBASICCALCULATION_BaseOnBasicSalary = "BGP";
	/** Base On UMK = BOU */
	public static final String JHTBASICCALCULATION_BaseOnUMK = "BOU";
	/** Base On UMP = BOP */
	public static final String JHTBASICCALCULATION_BaseOnUMP = "BOP";
	/** Set JHT Basic Calculation.
		@param JHTBasicCalculation JHT Basic Calculation	  */
	public void setJHTBasicCalculation (String JHTBasicCalculation)
	{

		set_Value (COLUMNNAME_JHTBasicCalculation, JHTBasicCalculation);
	}

	/** Get JHT Basic Calculation.
		@return JHT Basic Calculation	  */
	public String getJHTBasicCalculation () 
	{
		return (String)get_Value(COLUMNNAME_JHTBasicCalculation);
	}

	/** Set JHT Paid By Comp(%).
		@param JHTPaidByCompany 
		JHT Paid By Company in percent
	  */
	public void setJHTPaidByCompany (BigDecimal JHTPaidByCompany)
	{
		set_Value (COLUMNNAME_JHTPaidByCompany, JHTPaidByCompany);
	}

	/** Get JHT Paid By Comp(%).
		@return JHT Paid By Company in percent
	  */
	public BigDecimal getJHTPaidByCompany () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JHTPaidByCompany);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JHT Paid By Employee (%).
		@param JHTPaidByEmployee 
		JHT Paid By Employee In Percent
	  */
	public void setJHTPaidByEmployee (BigDecimal JHTPaidByEmployee)
	{
		set_Value (COLUMNNAME_JHTPaidByEmployee, JHTPaidByEmployee);
	}

	/** Get JHT Paid By Employee (%).
		@return JHT Paid By Employee In Percent
	  */
	public BigDecimal getJHTPaidByEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JHTPaidByEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Base On Brutto Salary = BGB */
	public static final String JKBASICCALCULATION_BaseOnBruttoSalary = "BGB";
	/** Base On Netto Salary = BGN */
	public static final String JKBASICCALCULATION_BaseOnNettoSalary = "BGN";
	/** Base On Basic Salary = BGP */
	public static final String JKBASICCALCULATION_BaseOnBasicSalary = "BGP";
	/** Base On UMK = BOU */
	public static final String JKBASICCALCULATION_BaseOnUMK = "BOU";
	/** Base On UMP = BOP */
	public static final String JKBASICCALCULATION_BaseOnUMP = "BOP";
	/** Set JK Basic Calculation.
		@param JKBasicCalculation JK Basic Calculation	  */
	public void setJKBasicCalculation (String JKBasicCalculation)
	{

		set_Value (COLUMNNAME_JKBasicCalculation, JKBasicCalculation);
	}

	/** Get JK Basic Calculation.
		@return JK Basic Calculation	  */
	public String getJKBasicCalculation () 
	{
		return (String)get_Value(COLUMNNAME_JKBasicCalculation);
	}

	/** Base On Brutto Salary = BGB */
	public static final String JKKBASICCALCULATION_BaseOnBruttoSalary = "BGB";
	/** Base On Netto Salary = BGN */
	public static final String JKKBASICCALCULATION_BaseOnNettoSalary = "BGN";
	/** Base On Basic Salary = BGP */
	public static final String JKKBASICCALCULATION_BaseOnBasicSalary = "BGP";
	/** Base On UMK = BOU */
	public static final String JKKBASICCALCULATION_BaseOnUMK = "BOU";
	/** Base On UMP = BOP */
	public static final String JKKBASICCALCULATION_BaseOnUMP = "BOP";
	/** Set JKK Basic Calculation.
		@param JKKBasicCalculation JKK Basic Calculation	  */
	public void setJKKBasicCalculation (String JKKBasicCalculation)
	{

		set_Value (COLUMNNAME_JKKBasicCalculation, JKKBasicCalculation);
	}

	/** Get JKK Basic Calculation.
		@return JKK Basic Calculation	  */
	public String getJKKBasicCalculation () 
	{
		return (String)get_Value(COLUMNNAME_JKKBasicCalculation);
	}

	/** Set JKK Paid By Comp(%).
		@param JKKPaidByCompany 
		JKK Paid By Company in percent
	  */
	public void setJKKPaidByCompany (BigDecimal JKKPaidByCompany)
	{
		set_Value (COLUMNNAME_JKKPaidByCompany, JKKPaidByCompany);
	}

	/** Get JKK Paid By Comp(%).
		@return JKK Paid By Company in percent
	  */
	public BigDecimal getJKKPaidByCompany () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JKKPaidByCompany);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JKK Paid By Employee (%).
		@param JKKPaidByEmployee 
		JKK Paid By Employee In Percent
	  */
	public void setJKKPaidByEmployee (BigDecimal JKKPaidByEmployee)
	{
		set_Value (COLUMNNAME_JKKPaidByEmployee, JKKPaidByEmployee);
	}

	/** Get JKK Paid By Employee (%).
		@return JKK Paid By Employee In Percent
	  */
	public BigDecimal getJKKPaidByEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JKKPaidByEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JK Paid By Comp(%).
		@param JKPaidByCompany 
		JK Paid By Company in percent
	  */
	public void setJKPaidByCompany (BigDecimal JKPaidByCompany)
	{
		set_Value (COLUMNNAME_JKPaidByCompany, JKPaidByCompany);
	}

	/** Get JK Paid By Comp(%).
		@return JK Paid By Company in percent
	  */
	public BigDecimal getJKPaidByCompany () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JKPaidByCompany);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JK Paid By Employee (%).
		@param JKPaidByEmployee 
		JK Paid By Employee In Percent
	  */
	public void setJKPaidByEmployee (BigDecimal JKPaidByEmployee)
	{
		set_Value (COLUMNNAME_JKPaidByEmployee, JKPaidByEmployee);
	}

	/** Get JK Paid By Employee (%).
		@return JK Paid By Employee In Percent
	  */
	public BigDecimal getJKPaidByEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JKPaidByEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Base On Brutto Salary = BGB */
	public static final String JPBASICCALCULATION_BaseOnBruttoSalary = "BGB";
	/** Base On Netto Salary = BGN */
	public static final String JPBASICCALCULATION_BaseOnNettoSalary = "BGN";
	/** Base On Basic Salary = BGP */
	public static final String JPBASICCALCULATION_BaseOnBasicSalary = "BGP";
	/** Base On UMK = BOU */
	public static final String JPBASICCALCULATION_BaseOnUMK = "BOU";
	/** Base On UMP = BOP */
	public static final String JPBASICCALCULATION_BaseOnUMP = "BOP";
	/** Set JP Basic Calculation.
		@param JPBasicCalculation JP Basic Calculation	  */
	public void setJPBasicCalculation (String JPBasicCalculation)
	{

		set_Value (COLUMNNAME_JPBasicCalculation, JPBasicCalculation);
	}

	/** Get JP Basic Calculation.
		@return JP Basic Calculation	  */
	public String getJPBasicCalculation () 
	{
		return (String)get_Value(COLUMNNAME_JPBasicCalculation);
	}

	/** Base On Brutto Salary = BGB */
	public static final String JPKBASICCALCULATION_BaseOnBruttoSalary = "BGB";
	/** Base On Netto Salary = BGN */
	public static final String JPKBASICCALCULATION_BaseOnNettoSalary = "BGN";
	/** Base On Basic Salary = BGP */
	public static final String JPKBASICCALCULATION_BaseOnBasicSalary = "BGP";
	/** Base On UMK = BOU */
	public static final String JPKBASICCALCULATION_BaseOnUMK = "BOU";
	/** Base On UMP = BOP */
	public static final String JPKBASICCALCULATION_BaseOnUMP = "BOP";
	/** Set JPK Basic Calculation.
		@param JPKBasicCalculation JPK Basic Calculation	  */
	public void setJPKBasicCalculation (String JPKBasicCalculation)
	{

		set_Value (COLUMNNAME_JPKBasicCalculation, JPKBasicCalculation);
	}

	/** Get JPK Basic Calculation.
		@return JPK Basic Calculation	  */
	public String getJPKBasicCalculation () 
	{
		return (String)get_Value(COLUMNNAME_JPKBasicCalculation);
	}

	/** Set JPK Kawin Paid By Comp(%).
		@param JPKKawinPaidByCompany 
		JPK Kawin Paid By Conpany in percent
	  */
	public void setJPKKawinPaidByCompany (BigDecimal JPKKawinPaidByCompany)
	{
		set_Value (COLUMNNAME_JPKKawinPaidByCompany, JPKKawinPaidByCompany);
	}

	/** Get JPK Kawin Paid By Comp(%).
		@return JPK Kawin Paid By Conpany in percent
	  */
	public BigDecimal getJPKKawinPaidByCompany () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JPKKawinPaidByCompany);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JPK Kawin Paid By Emp(%).
		@param JPKKawinPaidByEmployee 
		JPK Kawin Paid By Employee in percent
	  */
	public void setJPKKawinPaidByEmployee (BigDecimal JPKKawinPaidByEmployee)
	{
		set_Value (COLUMNNAME_JPKKawinPaidByEmployee, JPKKawinPaidByEmployee);
	}

	/** Get JPK Kawin Paid By Emp(%).
		@return JPK Kawin Paid By Employee in percent
	  */
	public BigDecimal getJPKKawinPaidByEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JPKKawinPaidByEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JPK Lajang Paid By Comp(%).
		@param JPKLajangPaidByCompany 
		JPK Lajang Paid By Company in percent
	  */
	public void setJPKLajangPaidByCompany (BigDecimal JPKLajangPaidByCompany)
	{
		set_Value (COLUMNNAME_JPKLajangPaidByCompany, JPKLajangPaidByCompany);
	}

	/** Get JPK Lajang Paid By Comp(%).
		@return JPK Lajang Paid By Company in percent
	  */
	public BigDecimal getJPKLajangPaidByCompany () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JPKLajangPaidByCompany);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JPK Lajang Paid By Emp(%).
		@param JPKLajangPaidByEmployee 
		JPK Lajang Paid By Company in percent
	  */
	public void setJPKLajangPaidByEmployee (BigDecimal JPKLajangPaidByEmployee)
	{
		set_Value (COLUMNNAME_JPKLajangPaidByEmployee, JPKLajangPaidByEmployee);
	}

	/** Get JPK Lajang Paid By Emp(%).
		@return JPK Lajang Paid By Company in percent
	  */
	public BigDecimal getJPKLajangPaidByEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JPKLajangPaidByEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JP Paid By Company (%).
		@param JPPaidByCompany JP Paid By Company (%)	  */
	public void setJPPaidByCompany (BigDecimal JPPaidByCompany)
	{
		set_Value (COLUMNNAME_JPPaidByCompany, JPPaidByCompany);
	}

	/** Get JP Paid By Company (%).
		@return JP Paid By Company (%)	  */
	public BigDecimal getJPPaidByCompany () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JPPaidByCompany);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set JP Paid By Employee (%).
		@param JPPaidByEmployee JP Paid By Employee (%)	  */
	public void setJPPaidByEmployee (BigDecimal JPPaidByEmployee)
	{
		set_Value (COLUMNNAME_JPPaidByEmployee, JPPaidByEmployee);
	}

	/** Get JP Paid By Employee (%).
		@return JP Paid By Employee (%)	  */
	public BigDecimal getJPPaidByEmployee () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_JPPaidByEmployee);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Max Amount to Calc JHT.
		@param MaxAmtToCalcJHT Max Amount to Calc JHT	  */
	public void setMaxAmtToCalcJHT (BigDecimal MaxAmtToCalcJHT)
	{
		set_Value (COLUMNNAME_MaxAmtToCalcJHT, MaxAmtToCalcJHT);
	}

	/** Get Max Amount to Calc JHT.
		@return Max Amount to Calc JHT	  */
	public BigDecimal getMaxAmtToCalcJHT () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MaxAmtToCalcJHT);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Max Amount to Calc JK.
		@param MaxAmtToCalcJK Max Amount to Calc JK	  */
	public void setMaxAmtToCalcJK (BigDecimal MaxAmtToCalcJK)
	{
		set_Value (COLUMNNAME_MaxAmtToCalcJK, MaxAmtToCalcJK);
	}

	/** Get Max Amount to Calc JK.
		@return Max Amount to Calc JK	  */
	public BigDecimal getMaxAmtToCalcJK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MaxAmtToCalcJK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Max Amount to Calc JKK.
		@param MaxAmtToCalcJKK Max Amount to Calc JKK	  */
	public void setMaxAmtToCalcJKK (BigDecimal MaxAmtToCalcJKK)
	{
		set_Value (COLUMNNAME_MaxAmtToCalcJKK, MaxAmtToCalcJKK);
	}

	/** Get Max Amount to Calc JKK.
		@return Max Amount to Calc JKK	  */
	public BigDecimal getMaxAmtToCalcJKK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MaxAmtToCalcJKK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Max Amount to Calc JP.
		@param MaxAmtToCalcJP Max Amount to Calc JP	  */
	public void setMaxAmtToCalcJP (BigDecimal MaxAmtToCalcJP)
	{
		set_Value (COLUMNNAME_MaxAmtToCalcJP, MaxAmtToCalcJP);
	}

	/** Get Max Amount to Calc JP.
		@return Max Amount to Calc JP	  */
	public BigDecimal getMaxAmtToCalcJP () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MaxAmtToCalcJP);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Max Amount to Calc JPK.
		@param MaxAmtToCalcJPK Max Amount to Calc JPK	  */
	public void setMaxAmtToCalcJPK (BigDecimal MaxAmtToCalcJPK)
	{
		set_Value (COLUMNNAME_MaxAmtToCalcJPK, MaxAmtToCalcJPK);
	}

	/** Get Max Amount to Calc JPK.
		@return Max Amount to Calc JPK	  */
	public BigDecimal getMaxAmtToCalcJPK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MaxAmtToCalcJPK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set BPJS Configuration.
		@param UNS_BPJSConfig_ID BPJS Configuration	  */
	public void setUNS_BPJSConfig_ID (int UNS_BPJSConfig_ID)
	{
		if (UNS_BPJSConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BPJSConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BPJSConfig_ID, Integer.valueOf(UNS_BPJSConfig_ID));
	}

	/** Get BPJS Configuration.
		@return BPJS Configuration	  */
	public int getUNS_BPJSConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BPJSConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_BPJSConfig_UU.
		@param UNS_BPJSConfig_UU UNS_BPJSConfig_UU	  */
	public void setUNS_BPJSConfig_UU (String UNS_BPJSConfig_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_BPJSConfig_UU, UNS_BPJSConfig_UU);
	}

	/** Get UNS_BPJSConfig_UU.
		@return UNS_BPJSConfig_UU	  */
	public String getUNS_BPJSConfig_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_BPJSConfig_UU);
	}
}