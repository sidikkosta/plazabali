/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author menjangan
 *
 */
public class MUNSPayrollBaseEmployee extends X_UNS_PayrollBase_Employee {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1188035197642766104L;

	/**
	 * @param ctx
	 * @param UNS_PayrollBase_Employee_ID
	 * @param trxName
	 */
	public MUNSPayrollBaseEmployee(Properties ctx,
			int UNS_PayrollBase_Employee_ID, String trxName) {
		super(ctx, UNS_PayrollBase_Employee_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollBaseEmployee(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSPayrollBaseEmployee Create (MUNSContractRecommendation cr)
	{
		MUNSPayrollBaseEmployee pb = MUNSPayrollBaseEmployee.get(cr.getCtx(), cr.get_ID(), cr.get_TrxName());
		if (pb == null)
		{
			pb = new MUNSPayrollBaseEmployee(cr);
		}
		pb.setA_L1(cr.getNew_A_L1());
		pb.setA_L2(cr.getNew_A_L2());
		pb.setA_L3(cr.getNew_A_L3());
		pb.setA_L1R(cr.getNew_A_L1_R());
		pb.setA_L2R(cr.getNew_A_L2_R());
		pb.setA_L3R(cr.getNew_A_L3_R());
		pb.setA_LemburJamPertama(cr.getNewLeburJamPertama());
		pb.setA_LemburJamBerikutnya(cr.getNewLeburJamBerikutnya());
		pb.setA_Other(cr.getNew_A_Lain2());
		pb.setA_Premi(cr.getNew_A_Premi());
		pb.setA_Rapel(BigDecimal.ZERO);
		pb.setG_T_Jabatan(cr.getNew_T_Jabatan());
		pb.setG_T_Kesejahteraan(cr.getNew_T_Kesejahteraan());
		pb.setG_T_Khusus(BigDecimal.ZERO);
		pb.setG_T_Lembur(cr.getNew_T_Lembur());
		pb.setGPokok(cr.getNew_G_Pokok());
		pb.setP_Koperasi(BigDecimal.ZERO);
		pb.setP_Label(cr.getNew_P_Label());
		pb.setP_ListrikAir(BigDecimal.ZERO);
		pb.setP_Mangkir(cr.getNew_P_Mangkir());
		pb.setP_Obat(BigDecimal.ZERO);
		pb.setP_Other(cr.getNew_P_Lain2());
		pb.setP_PinjamanKaryawan(BigDecimal.ZERO);
		pb.setP_SPTP(cr.getNew_P_SPTP());
		pb.setValidFrom(cr.getDateContractStart());
		pb.setPayrollLevel(cr.getNewPayrollLevel());
		pb.setValidTo(cr.getDateContractEnd());
		pb.setIsJHTApplyed(cr.isJHTApplyed());
		pb.setIsJKApplyed(cr.isJKApplyed());
		pb.setIsJKKApplyed(cr.isJKKApplyed());
		pb.setIsJPApplied(cr.isJPApplied());
		pb.setIsJPKApplyed(cr.isJPKApplyed());
		pb.setUMKLevel_ID(cr.getNewUMKLevel_ID());
		pb.setUMPLevel_ID(cr.getNewUMPLevel_ID());
		pb.setIsActive(true);
		pb.setA_JHT(Env.ZERO);
		pb.setA_JK(Env.ZERO);
		pb.setA_JKK(Env.ZERO);
		pb.setA_JPK(Env.ZERO);
		pb.setP_JHT(Env.ZERO);
		pb.setP_JK(Env.ZERO);
		pb.setP_JKK(Env.ZERO);
		pb.setP_JPK(Env.ZERO);
		return pb;
	}
	
	public MUNSPayrollBaseEmployee(MUNSContractRecommendation cr)
	{
		super(cr.getCtx(), 0, cr.get_TrxName());
		setUNS_Employee_ID(cr.getUNS_Employee_ID());
		setUNS_Contract_Recommendation_ID(cr.get_ID());
		setClientOrg(cr);
	}
	
	public static MUNSPayrollBaseEmployee get(
			Properties ctx, int UNS_Contract_Recommendation_ID, String trxName)
	{
		return Query.get(
				ctx, 
				UNSHRMModelFactory.getExtensionID(), 
				Table_Name, 
				COLUMNNAME_UNS_Contract_Recommendation_ID + " = " 
				+ UNS_Contract_Recommendation_ID, trxName).setOrderBy("IsActive DESC").first();
	}
	
	/**
	 * 
	 * @param ctx
	 * @param payrollLevel
	 * @param date
	 * @param UNS_Employee_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSPayrollBaseEmployee getPrev(
			Properties ctx, int UNS_Employee_ID, String trxName)
	{
		MUNSPayrollBaseEmployee pbEmploye = null;
		String sql = "SELECT * FROM " + Table_Name
				+ " WHERE " + COLUMNNAME_ValidTo + " = " +
						"(SELECT MAX(" + COLUMNNAME_ValidTo + ") FROM " + Table_Name + " "
								+ " WHERE " + COLUMNNAME_UNS_Employee_ID
								+ " = " + UNS_Employee_ID + " AND " + COLUMNNAME_IsActive + " ='Y')" 
								+ " AND " + COLUMNNAME_IsActive + " = 'Y' AND " 
								+ COLUMNNAME_UNS_Employee_ID
								+ " = " + UNS_Employee_ID;
		
		PreparedStatement stm = null;
		ResultSet rs = null;
		try
		{
			stm = DB.prepareStatement(sql,trxName);
			rs = stm.executeQuery();
			if (rs.next())
				pbEmploye = new MUNSPayrollBaseEmployee(ctx, rs, trxName);
		}catch (Exception e)
		{
			e.printStackTrace();
		}finally
		{
			DB.close(rs, stm);
		}
		
		return pbEmploye;
	}
	
	/** modified by ITD-Andy, get DocType of Payroll */
	public static MDocType getDocType(Properties ctx, boolean isEmployee) {
		try {
			for (MDocType doc : MDocType.getOfDocBaseType(ctx, "PRE")) {
				if(doc.isSOTrx()!= isEmployee)
					return doc;
				else
					return doc;
			}
		} catch (Exception e) {
			throw new AdempiereException(e);
		}
		return null;
	}
	
	public BigDecimal getPayableBruto()
	{
		BigDecimal bruto = getGPokok();
		BigDecimal addAmt = Env.ZERO;
		MUNSPayrollComponentConf[] comp = MUNSPayrollComponentConf.get(
				new MUNSContractRecommendation(getCtx(), getUNS_Contract_Recommendation_ID(), get_TrxName()));
		for(int i=0; i<comp.length; i++)
		{
			if(comp[i].isBenefit() && comp[i].getCostBenefitType() == null)
				addAmt = addAmt.add(comp[i].getAmount());

		}
		
		bruto = bruto.add(addAmt);
		
		return bruto;
	}
	
	public BigDecimal getDeduction()
	{
		BigDecimal deductionAmt = Env.ZERO;
		MUNSPayrollComponentConf[] comp = MUNSPayrollComponentConf.get(
				new MUNSContractRecommendation(getCtx(), getUNS_Contract_Recommendation_ID(), get_TrxName()));
		for(int i=0; i<comp.length; i++)
		{
			if(!comp[i].isBenefit() && comp[i].getCostBenefitType() == null)
				deductionAmt = deductionAmt.add(comp[i].getAmount());
		}
		
		return deductionAmt;
	}
}
