/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Msg;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSAdjustCooperativeDebt extends X_UNS_AdjustCooperativeDebt implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3090407314350183406L;

	/**
	 * @param ctx
	 * @param UNS_AdjustCooperativeDebt_ID
	 * @param trxName
	 */
	public MUNSAdjustCooperativeDebt(Properties ctx,
			int UNS_AdjustCooperativeDebt_ID, String trxName) {
		super(ctx, UNS_AdjustCooperativeDebt_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAdjustCooperativeDebt(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord || is_ValueChanged(COLUMNNAME_UNS_StoreCustomer_ID))
		{
			String sql = "SELECT Supervisor_ID FROM UNS_StoreCustomer WHERE"
					+ " UNS_StoreCustomer_ID = ?";
			int spvID = DB.getSQLValue(get_TrxName(), sql, getUNS_StoreCustomer_ID());
			if(spvID <= 0)
			{
				log.saveError("Error", "Please supervisor on store customer.");
				return false;
			}
			else
				setUNS_Employee_ID(spvID);
		}
		if(getTotalDebt().compareTo(getAllocatedAmt()) == -1)
		{
			log.saveError("Error", "Allocated amount can not greater than total debt.");
			return false;
		}
		
		if(newRecord || is_ValueChanged(COLUMNNAME_UNS_StoreBilling_ID))
		{
			String sql = "SELECT SUM(bc.TotalDebt), SUM(bc.InstallmentAmt) FROM UNS_StoreBillingCustomer bc"
					+ " INNER JOIN UNS_StoreBillingLine bl ON bl.UNS_StoreBillingLine_ID = bc.UNS_StoreBillingLine_ID"
					+ " WHERE bl.UNS_StoreBilling_ID = ? AND bc.UNS_StoreCustomer_ID = ?";
			List<List<Object>> columns = DB.getSQLArrayObjectsEx(
					get_TrxName(), sql, getUNS_StoreBilling_ID(), getUNS_StoreCustomer_ID());
			if(columns.get(0) == null)
			{
				log.saveError("Error", "Not found this customer on recaps.");
				return false;
			}
			else
			{
				BigDecimal totalDebt = (BigDecimal) columns.get(0).get(0);
				BigDecimal installmentAmt = (BigDecimal) columns.get(0).get(1);
				setInstallmentAmt(installmentAmt);
				setLogisticAmt(totalDebt);
				setTotalDebt(installmentAmt.add(totalDebt));
			}	
		}
		
		return true;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSAdjustCooperativeDebt[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "DocumentNo") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(getAllocatedAmt().signum() <= 0)
		{
			m_processMsg = "Allocated amount can not zero or negate.";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		m_processMsg = updatePeriodicCostBenefit(getUNS_Employee_ID(), true);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		String sql = "SELECT UNS_Employee_ID FROM UNS_StoreCustomer"
				+ " WHERE UNS_StoreCustomer_ID = ?";
		int empID = DB.getSQLValue(get_TrxName(), sql, getUNS_StoreCustomer_ID());
		if(empID > 0)
		{
			m_processMsg = updatePeriodicCostBenefit(empID, false);
			if(m_processMsg != null)
				return DocAction.STATUS_Invalid;
		}
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(!reverseCorrectIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return true;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}
	
	public void setProcessMsg(String processMsg)
	{
		m_processMsg = processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return getAllocatedAmt();
	}

	@Override
	public boolean approveIt() {

		return true;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_ReActivate;
			options[index++] = DocAction.ACTION_Void;
		}
		return index;
	}
	
	public static MUNSAdjustCooperativeDebt get(Properties ctx, int empID, String trxName)
	{
		String whereClause = "UNS_StoreCustomer_ID = (SELECT UNS_StoreCustomer_ID FROM"
				+ " UNS_StoreCustomer WHERE UNS_Employee_ID = ?)";
		
		MUNSAdjustCooperativeDebt debt = Query.get(ctx, UNSHRMModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(empID).firstOnly();
		
		return debt;
	}
	
	private String updatePeriodicCostBenefit(int empID, boolean isSPV)
	{
		MUNSPeriodicCostBenefitLine line = MUNSPeriodicCostBenefitLine.get(getCtx(),
				getC_Period_ID(), MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Cooperative,
				empID, get_TrxName());
		MUNSPeriodicCostBenefit costBen = null;
		if(line == null)
		{
			costBen = MUNSPeriodicCostBenefit.getOnPeriod(getAD_Org_ID(), getC_Period().getC_Year_ID(),
					getC_Period_ID(), MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Cooperative, get_TrxName());
			if(costBen == null || costBen.isProcessed())
			{
				costBen = new MUNSPeriodicCostBenefit(getCtx(), 0, get_TrxName());
				costBen.setAD_Org_ID(getAD_Org_ID());
				costBen.setC_Year_ID(getC_Period().getC_Year_ID());
				costBen.setC_Period_ID(getC_Period_ID());
				costBen.saveEx();
			}

			line = new MUNSPeriodicCostBenefitLine(costBen);
			line.setUNS_Employee_ID(empID);
		}
		
		if(costBen == null)
			costBen = new MUNSPeriodicCostBenefit(getCtx(), line.getUNS_PeriodicCostBenefit_ID(),
				get_TrxName());
		boolean isComplete = false;
		if(costBen.getDocStatus().equals("CO"))
		{
			try {
				isComplete = true;
				costBen.setHasChange_ID(empID);
				costBen.processIt(DocAction.ACTION_ReActivate);
				costBen.saveEx();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		line.setAdjustCoopDebtAmt(line.getAdjustCoopDebtAmt().add(
				isSPV ? getAllocatedAmt() : getAllocatedAmt().negate()));
		if(!isSPV)
		{
			line.setInstallmentAmt(line.getInstallmentAmt().add(getInstallmentAmt()));
			line.setLogisticAmt(line.getLogisticAmt().add(getLogisticAmt()));
		}
		line.setAmount(line.getPrevAmount()
				.add(line.getLogisticAmt()).add(line.getInstallmentAmt()).add(line.getAdjustCoopDebtAmt()));
		
		String sql = "SELECT UNS_Employee_ID FROM UNS_StoreCustomer"
				+ " WHERE UNS_StoreCustomer_ID = ?";
		int subordinateID = DB.getSQLValue(get_TrxName(), sql, getUNS_StoreCustomer_ID());
		
		String empName = "SELECT Name FROM UNS_Employee WHERE UNS_Employee_ID = ?";
		String name = DB.getSQLValueString(get_TrxName(), empName, isSPV ? subordinateID : getUNS_Employee_ID());
		String prefix = isSPV ? "F:" : "T:";
		line.setDescription((line.getDescription() == null ? "" : line.getDescription()).concat(prefix).concat(name));
		if(!line.save())
			return "Could not update periodic cost benefit line.";
		
		if(isComplete)
		{
			try {
				isComplete = true;
				costBen.processIt(DocAction.ACTION_Complete);
				costBen.saveEx();
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		
		return null;
	}
}