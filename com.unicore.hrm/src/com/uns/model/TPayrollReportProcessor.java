/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author menjangan
 *
 */
public class TPayrollReportProcessor {
	
	public static String COLUMNNAME_IsPeriodic = "isPeriodic";
	public static String COMP_GajiBruto = "GB";
	public static String COMP_PTKP = "PTKP";
	public static String COMP_BiayaJabatanAtasGaji = "BJAG"; //biaya jabatan gaji + tunjangan
	public static String COMP_BiayaJabatanAtasBonus = "PJAB"; //biaya jabatan atas bonus / THR
	public static String COMP_GajiNetto = "GN";
	public static String COMP_PPHTerhutang = "PPH1Thun";
	public static String COMP_PPHTerhutangTanpaBonuss = "PPHTB"; //1tahun
	
	private static final String[] COLUMN_NAMES = new String[]{
			MUNSPayrollEmployee.COLUMNNAME_A_JHT,
			MUNSPayrollEmployee.COLUMNNAME_P_JHT,
			MUNSPayrollEmployee.COLUMNNAME_A_JK,
			MUNSPayrollEmployee.COLUMNNAME_P_JK,
			MUNSPayrollEmployee.COLUMNNAME_A_JKK,
			MUNSPayrollEmployee.COLUMNNAME_P_JKK,
			MUNSPayrollEmployee.COLUMNNAME_A_JPK,
			MUNSPayrollEmployee.COLUMNNAME_P_JPK,
			MUNSPayrollEmployee.COLUMNNAME_A_JP,
			MUNSPayrollEmployee.COLUMNNAME_P_JP,
			MUNSPayrollEmployee.COLUMNNAME_SourcePPhAmt,
			MUNSPayrollEmployee.COLUMNNAME_GPokok,
			MUNSPayrollEmployee.COLUMNNAME_A_TotalOverTime
	};
	
	private static final String[] COMPONENTS = new String[] {
		"AJHT",
		"PJHT",
		"AJK",
		"PJK",
		"AJKK",
		"PJKK",
		"AJPK",
		"PJPK",
		"AJP",
		"PJP",
		"P21",
		"GP",
		"OT"
	};
	
	public static final boolean create (MUNSPayrollEmployee payroll)
	{
		if (COMPONENTS.length != COLUMN_NAMES.length)
			return false;
		for (int i=0; i<COMPONENTS.length; i++)
		{
			if (!TPayrollReportProcessor.create(payroll.get_TrxName(), COMPONENTS[i],
					payroll.getUNS_Employee_ID(), payroll.getC_Period_ID(), 
					(BigDecimal)payroll.get_Value(COLUMN_NAMES[i]), true, payroll.getCtx()))
				return false;
		}
		BigDecimal tPeriodicBenefit = Env.ZERO;
		BigDecimal tNonPeriodicBenefit = Env.ZERO;
		BigDecimal tPeriodicDeduction = Env.ZERO;
		BigDecimal tNonPeriodicDeduction = Env.ZERO;
		MUNSPayrollCostBenefit[] costBens = payroll.getCostBenefits(false);
		for (int i=0; i<costBens.length; i++)
		{
			if (!costBens[i].isPPHComp())
				continue;
			else if (costBens[i].isBenefit())
			{
				if (costBens[i].isMonthlyPPHComp())
					tPeriodicBenefit= tPeriodicBenefit.add(costBens[i].getAmount());
				else
					tNonPeriodicBenefit= tNonPeriodicBenefit.add(costBens[i].getAmount());
			}
			else
			{
				if (costBens[i].isMonthlyPPHComp())
					tPeriodicDeduction= tPeriodicDeduction.add(costBens[i].getAmount());
				else
					tNonPeriodicDeduction = tNonPeriodicDeduction.add(costBens[i].getAmount());
			}
		}
		
		if (!TPayrollReportProcessor.create(payroll.get_TrxName(), "TJ",
				payroll.getUNS_Employee_ID(), payroll.getC_Period_ID(), tPeriodicBenefit, true, payroll.getCtx()))
			return false;
		if (!TPayrollReportProcessor.create(payroll.get_TrxName(), "TJ",
				payroll.getUNS_Employee_ID(), payroll.getC_Period_ID(), tNonPeriodicBenefit, false, payroll.getCtx()))
			return false;
		if (!TPayrollReportProcessor.create(payroll.get_TrxName(), "POT",
				payroll.getUNS_Employee_ID(), payroll.getC_Period_ID(), tPeriodicDeduction, true, payroll.getCtx()))
			return false;
		if (!TPayrollReportProcessor.create(payroll.get_TrxName(), "POT",
				payroll.getUNS_Employee_ID(), payroll.getC_Period_ID(), tNonPeriodicDeduction, false, payroll.getCtx()))
			return false;
		
		
		return true;
	}

	public static boolean create (String trxName, String payrollComponent, int UNS_Employee_ID, 
			int C_Period_ID, BigDecimal amount, boolean isPeriodicComp, Properties ctx)
	{
		int C_Year_ID = DB.getSQLValue(trxName, "SELECT C_Year_ID FROM C_Period WHERE C_Period_ID = ?", C_Period_ID);
		BigDecimal[] m = new BigDecimal[12];
		BigDecimal tAmt = Env.ZERO;
		String sql = "SELECT PeriodNo FROM C_Period WHERE C_Period_ID = ? AND C_Year_ID = ?";
		int periodNo = DB.getSQLValue(trxName, sql, C_Period_ID, C_Year_ID);
		if (periodNo > 1)
		{
			int prevPeriodNo = periodNo - 1;
			sql = "SELECT C_Period_ID FROM C_Period WHERE C_Year_ID = ? AND PeriodNo = ? ";
			int prevPeriodID = DB.getSQLValue(trxName, sql, C_Year_ID, prevPeriodNo);
			sql = "SELECT * FROM T_PayrollReport WHERE C_Period_ID = ? AND C_Year_ID = ? AND UNS_Employee_ID= ? "
					+ " AND PayrollComponent = ? AND " + COLUMNNAME_IsPeriodic + " = ?";
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, trxName);
				st.setInt(1, prevPeriodID);
				st.setInt(2, C_Year_ID);
				st.setInt(3, UNS_Employee_ID);
				st.setString(4, payrollComponent);
				st.setString(5, isPeriodicComp ? "Y" : "N");
				rs = st.executeQuery();
				if (rs.next())
				{
					for (int i=0; i<prevPeriodNo; i++)
					{
						String column = "m" + (i+1);
						m[i] = rs.getBigDecimal(column);
					}
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
				return false;
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		
		sql = "SELECT T_PayrollReport_ID FROM T_PayrollReport WHERE UNS_Employee_ID = ?"
				+ " AND C_Period_ID = ? AND C_Year_ID = ? AND PayrollComponent = ? AND IsPeriodic = ? ";
		int tPayroll_ID = DB.getSQLValue(trxName, sql,
				new Object[]{UNS_Employee_ID, C_Period_ID, C_Year_ID, payrollComponent, isPeriodicComp});
		
		MPayrollReport tPayroll = new MPayrollReport(ctx, tPayroll_ID, trxName);
		
//		sql = "DELETE FROM T_PayrollReport WHERE UNS_Employee_ID = ? AND C_Period_ID = ? AND C_Year_ID = ? AND PayrollComponent = ? AND IsPeriodic = ? ";
//		int ok = DB.executeUpdate(sql, new Object[]{UNS_Employee_ID, C_Period_ID, C_Year_ID, payrollComponent, isPeriodicComp}, false, trxName);
//		if (ok== -1)
//			return false;
		
		for (int i=0; i<m.length; i++)
		{
			if (isPeriodicComp) {
				if (i >= (periodNo-1))
					m[i] = amount;
			} else {
				if (i== periodNo-1) {
					m[i] = amount;
					tAmt = amount;
				}
			}
			if (m[i] == null)
				m[i] = Env.ZERO;
			
			if (!COMP_BiayaJabatanAtasBonus.equals(payrollComponent) && !COMP_BiayaJabatanAtasGaji.equals(payrollComponent)
					&& !COMP_GajiBruto.equals(payrollComponent) && !COMP_GajiNetto.equals(payrollComponent)
					&& !COMP_PPHTerhutang.equals(payrollComponent) && !COMP_PPHTerhutangTanpaBonuss.equals(payrollComponent)
					&& !COMP_PTKP.equals(payrollComponent))
				tAmt = tAmt.add(m[i]);
		}
		
		sql = "SELECT MAX(Sequence) FROM T_PayrollReport WHERE UNS_Employee_ID = ? ";
		int seqNo = DB.getSQLValue(trxName, sql, UNS_Employee_ID);
		seqNo = seqNo + 10;
		
//		sql = "INSERT INTO t_payrollreport (payrollcomponent, uns_employee_id, c_year_id, "
//				+ " c_period_id, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, "
//				+ " m12, totalamt, sequence, isperiodic) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
//				+ " ?, ?, ?, ?)";
//		int x = 0;
//		Object[] params = new Object[19];
//		params[x++] = payrollComponent;
//		params[x++] = UNS_Employee_ID;
//		params[x++] = C_Year_ID;
//		params[x++] = C_Period_ID;
//		for (int i=0; i<m.length; i++)
//			params[x++] = m[i];
//		params[x++] = tAmt;
//		params[x++] = seqNo;
//		params[x++] = isPeriodicComp ? "Y" : "N";
//		
//		int result = DB.executeUpdate(sql, params, false, trxName);
		
		//get AD Org Employee
		sql = "SELECT AD_Org_ID FROM UNS_Employee WHERE UNS_Employee_ID = ?";
		int orgID = DB.getSQLValue(trxName, sql, UNS_Employee_ID);
		
		if(tPayroll.get_ID() == 0)
		{
			tPayroll = new MPayrollReport(ctx, 0, trxName);
			tPayroll.setAD_Org_ID(orgID);
			tPayroll.setUNS_Employee_ID(UNS_Employee_ID);
			tPayroll.setC_Year_ID(C_Year_ID);
			tPayroll.setC_Period_ID(C_Period_ID);
			tPayroll.setSequence(BigDecimal.valueOf(seqNo));
			tPayroll.setisPeriodic(isPeriodicComp);
		}

		for(int i=0;i<m.length; i++)
		{
			int j = i+1;
			String columnName = "M"+j;
			tPayroll.set_ValueOfColumn(columnName, m[i]);
		}
		tPayroll.setPayrollComponent(payrollComponent);
		
		if(!tPayroll.save())
			return false;
		
		return true;
	}

	public static boolean remove (String trxName, int UNS_Employee_ID, int C_Period_ID, boolean IsExcludeSpecialComp) {
		String sql = "DELETE FROM T_PayrollReport WHERE C_Period_ID = ? AND UNS_Employee_ID = ?";
		List<Object> params = new ArrayList<>();
		
		params.add(C_Period_ID);
		params.add(UNS_Employee_ID);
		
		if (IsExcludeSpecialComp) {
			sql += "AND PayrollComponent NOT IN (?,?,?,?,?,?, ?)";
			params.add(COMP_BiayaJabatanAtasBonus);
			params.add(COMP_BiayaJabatanAtasGaji);
			params.add(COMP_GajiBruto);
			params.add(COMP_GajiNetto);
			params.add(COMP_PPHTerhutang);
			params.add(COMP_PPHTerhutangTanpaBonuss);
			params.add(COMP_PTKP);
		}
		
		int result = DB.executeUpdate(sql, params.toArray(), false, trxName);
		return result != -1;
	}
}
