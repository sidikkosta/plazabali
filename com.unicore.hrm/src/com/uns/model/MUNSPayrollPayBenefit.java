package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.ui.ISortTabRecord;

public class MUNSPayrollPayBenefit extends X_UNS_PayrollPayBenefit 
	implements ISortTabRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3939457965790119086L;
	
	private MUNSPayrollPaymentBA m_parent = null;

	public MUNSPayrollPayBenefit(Properties ctx, int UNS_PayrollPayBenefit_ID,
			String trxName) {
		super(ctx, UNS_PayrollPayBenefit_ID, trxName);
		
	}

	public MUNSPayrollPayBenefit(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	public MUNSPayrollPaymentBA getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPayrollPaymentBA(getCtx(), getUNS_PayrollPayment_BA_ID(), get_TrxName());
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		String sql = "SELECT pp.DocumentNo FROM UNS_PayrollPayBenefit ppb"
				+ " INNER JOIN UNS_PayrollPayment_BA ppa ON ppa.UNS_PayrollPayment_BA_ID = "
				+ " ppb.UNS_PayrollPayment_BA_ID "
				+ "	INNER JOIN UNS_PayrollPayment pp ON pp.UNS_PayrollPayment_ID = "
				+ " ppb.UNS_PayrollPayment_ID"
				+ " WHERE ppa.CostBenefitType = ? AND pp.DocStatus NOT IN ('VO','RE')"
				+ " AND ppb.UNS_Payroll_Employee_ID <> ?";
		String docNo = DB.getSQLValueString(get_TrxName(),
				sql, getParent().getCostBenefitType(), getUNS_Payroll_Employee_ID());
		
		if(!Util.isEmpty(docNo, true))
			throw new AdempiereException("Duplicate Payroll Employee. Exists in payroll payment with no "+docNo);
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		if(newRecord || is_ValueChanged(COLUMNNAME_Amount))
		{
			if (!updateHeader())
				return false;
		}
		
		return super.afterSave(newRecord, success);
	}
	
	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) {
		
		String sql = "SELECT CostBenefitType FROM UNS_PayrollPayment_BA WHERE"
				+ " UNS_PayrollPayment_BA_ID = ? ";
		String type = DB.getSQLValueString(get_TrxName(), sql, parentRecord_ID);
		if(type == null)
			throw new AdempiereException("Cannot found benefit type");
		
		BigDecimal amount = MUNSPeriodicCostBenefit.getMyCostOrBenefit(
			getUNS_Payroll_Employee().getUNS_Employee_ID(),
			getUNS_Payroll_Employee().getAD_Org_ID(), type, get_TrxName());
		
		setAmount(amount);
		
		return null;
	}
	
	@Override
	public String beforeRemoveSelection() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean updateHeader ()
	{
		MUNSPayrollPaymentBA parent = getParent();
		String sql = "SELECT COALESCE (SUM (Amount), 0) FROM UNS_PayrollPayBenefit WHERE UNS_PayrollPayment_BA_ID = ?";
		BigDecimal tAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_PayrollPayment_BA_ID());
		parent.setTotalAmt(tAmt);
		return parent.save();
	}
	
	@Override
	protected boolean afterDelete (boolean success)
	{
		if (!updateHeader())
		{
			log.log(Level.SEVERE, "Error when try to update header");
			return false;
		}
		return super.afterDelete(success);
	}
}
