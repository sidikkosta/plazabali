/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPayrollCorrection extends X_UNS_PayrollCorrection implements DocAction, DocOptions, IUNSApprovalInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8493973881787039217L;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	
	/**
	 * @param ctx
	 * @param UNS_PayrollCorrection_ID
	 * @param trxName
	 */
	public MUNSPayrollCorrection(Properties ctx, int UNS_PayrollCorrection_ID,
			String trxName) {
		super(ctx, UNS_PayrollCorrection_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollCorrection(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord ||is_ValueChanged(COLUMNNAME_UNS_Employee_ID))
		{
			MUNSEmployee emp = MUNSEmployee.get(getCtx(), getUNS_Employee_ID());
			setC_BPartner_ID(emp.getC_BPartner_ID());
			setAttendanceName(emp.getAttendanceName());
		}
		
		setBalance(getAmount().subtract(getPaidAmt()));
		setBalancePPh21(getAmountPPh21().subtract(getPaidAmtPPh21()));
		setBalanceNonPeriodic(getAmountNonPeriodic().subtract(getPaidAmtNonPeriodic()));
		setBalancePPh21NonPeriodic(getAmountPPh21NonPeriodic().subtract(getPaidAmtPPh21NonPeriodic()));
		
		return true;
	}
	
	public MUNSPayrollCorrectionLine[] getLines()
	{
		List<MUNSPayrollCorrectionLine> lines = Query.get(getCtx(), 
			UNSHRMModelFactory.EXTENSION_ID, MUNSPayrollCorrectionLine.Table_Name,
				COLUMNNAME_UNS_PayrollCorrection_ID + "=?", get_TrxName()).
					setParameters(get_ID()).setOnlyActiveRecords(true).list();
		
		return lines.toArray(new MUNSPayrollCorrectionLine[lines.size()]);
	}
	
	private boolean hasPaid()
	{
		return getPaidAmt().signum() != 0;
	}
	
	public static boolean pay (int employeeID, int periodID, BigDecimal amount, boolean isPPh21Comp 
			, boolean isPeriodic, boolean isReversal, String trxName) 
	{	
		
		String orderBy = "p.StartDate";
		if(isReversal)
			orderBy.concat(" DESC");
		else
			orderBy.concat(" ASC");
		
		MUNSPayrollCorrection[] recs = get(Env.getCtx(), employeeID, periodID, orderBy, trxName);
		for (int i=0; i<recs.length && amount.signum() != 0; i++) 
		{	
			BigDecimal tobePay = amount;
			BigDecimal diff = Env.ZERO;
			String colName = null;
			if (isPPh21Comp) 
			{
				if(isPeriodic)
				{
					diff = isReversal ? recs[i].getPaidAmtPPh21() : recs[i].getBalancePPh21();
					colName = COLUMNNAME_PaidAmtPPh21;
				}
				else
				{
					diff = isReversal ? recs[i].getPaidAmtPPh21NonPeriodic() : recs[i].getBalancePPh21NonPeriodic();
					colName = COLUMNNAME_PaidAmtPPh21NonPeriodic;
				}
			}
			else
			{
				if(isPeriodic)
				{
					diff = isReversal ? recs[i].getPaidAmt() : recs[i].getBalance();
					colName = COLUMNNAME_PaidAmt;
				}
				else
				{
					diff = isReversal ? recs[i].getPaidAmtNonPeriodic() : recs[i].getBalanceNonPeriodic();
					colName = COLUMNNAME_PaidAmtNonPeriodic;
				}
				
			}
			
			if (diff.abs().compareTo(tobePay.abs()) == -1)
			{
				if(isReversal)
					tobePay = Env.ZERO;
				else
					tobePay = diff;
			}
			
			if(isReversal && tobePay.signum() != 0)
				tobePay = diff.subtract(tobePay);
			
			recs[i].set_Value(colName, tobePay);
			if (!recs[i].save())
				return false;
			amount = amount.subtract(tobePay);
		}
		return true;
	}
	
//	public static boolean ReversalPay(int employeeID, int periodID, BigDecimal amount, boolean isPPh21Comp 
//			, boolean isPeriodic, String trxName) {
//		
//		MUNSPayrollCorrection[] recs = get(Env.getCtx(), employeeID, periodID, "C_Period_ID DESC", trxName);
//		for(int i = 0; i < recs.length; i++)
//		{
//			if(amount.signum() == 0)
//				return true;
//			
//			BigDecimal tobeBalance = amount;
//			BigDecimal paidAmt = Env.ZERO;
//			String colName = null;
//			if (isPPh21Comp) 
//			{
//				if(isPeriodic)
//				{
//					paidAmt = recs[i].getPaidAmtPPh21();
//					colName = COLUMNNAME_BalancePPh21;
//				}
//				else
//				{
//					paidAmt = recs[i].getPaidAmtPPh21NonPeriodic();
//					colName = COLUMNNAME_BalancePPh21NonPeriodic;
//				}
//			}
//			else
//			{
//				if(isPeriodic)
//				{
//					paidAmt = recs[i].getPaidAmt();
//					colName = COLUMNNAME_Balance;
//				}
//				else
//				{
//					paidAmt = recs[i].getPaidAmtNonPeriodic();
//					colName = COLUMNNAME_BalanceNonPeriodic;
//				}
//				
//			}
//			
//			if(paidAmt.abs().compareTo(tobeBalance.abs()) == -1)
//			{
//				tobeBalance = paidAmt;
//			}
//			
//			recs[i].set_Value(colName, tobeBalance);
//			if (!recs[i].save())
//				return false;
//			amount = amount.subtract(tobePay);
//			
//		}
//		
//		return true;
//	}
	
	public static MUNSPayrollCorrection[] get(
			Properties ctx, int UNS_Employee_ID, int C_Period_ID, String OrderBy, String trxName)
	{
		
		List<MUNSPayrollCorrection> corrections = new ArrayList<>();
		
		String sql = "SELECT pc.* FROM UNS_PayrollCorrection pc"
				+ " INNER JOIN C_Period p ON p.C_Period_ID = pc.C_Period_ID"
				+ " WHERE pc.UNS_Employee_ID = ? AND pc.DocStatus IN ('CO', 'CL')";
		if(C_Period_ID > 0)
			sql += " AND pc.C_Period_ID = "+C_Period_ID;
		
		if(OrderBy != null)
			sql += " ORDER BY "+OrderBy;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = DB.prepareStatement(sql, trxName);
			ps.setInt(1, UNS_Employee_ID);
			rs = ps.executeQuery();
			while(rs.next()) {
				MUNSPayrollCorrection corr = new MUNSPayrollCorrection(ctx, rs, trxName);
				corrections.add(corr);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		finally{
			DB.close(rs, ps);
		}

//		String jc = "INNER JOIN C_Period_ID p ON p.C_Period_ID = UNS_PayrollCorrection.C_Period_ID";
//		
//		if(OrderBy == null)
//		{	
//			
//			corrections = Query.get(
//					ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name,
//						COLUMNNAME_UNS_Employee_ID + "=? AND " + COLUMNNAME_C_Period_ID + "=?"
//							+ " AND DocStatus IN ('CO', 'CL')", trxName).addJoinClause(jc)
//								.setParameters(UNS_Employee_ID, C_Period_ID).list();
//		}
//		else
//		{
//			corrections = Query.get(
//					ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name,
//						COLUMNNAME_UNS_Employee_ID + "=? AND " + COLUMNNAME_C_Period_ID + "=?"
//							+ " AND DocStatus IN ('CO', 'CL')", trxName).addJoinClause(jc)
//								.setParameters(UNS_Employee_ID, C_Period_ID)
//								.setOrderBy(OrderBy).list();
//		}
			
		return corrections.toArray(new MUNSPayrollCorrection[corrections.size()]);
	}
	
	/**
	 * @param ctx
	 * @param UNS_Employee_ID (Mandatory)
	 * @param C_Period_ID (can 0)
	 * @param trxName
	 * @return balance
	 */
	public static BigDecimal getBalance (int UNS_Employee_ID, int C_Period_ID, boolean isPeriodic, String trxName)
	{
		BigDecimal balance = Env.ZERO;
		String columnName = null;
		
		if(isPeriodic)
			columnName = "Balance";
		else
			columnName = "BalanceNonPeriodic";
		MColumn column = MColumn.get(Env.getCtx(), Table_Name, columnName);
		if (column.isEncrypted())
		{
			String sql = "SELECT "+columnName+
					" FROM UNS_PayrollCorrection WHERE UNS_Employee_ID = ?"
					+ " AND DocStatus IN ('CO','CL')";
			if(C_Period_ID > 0)
				sql += " AND C_Period_ID = " + C_Period_ID;
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, trxName);
				st.setInt(1, UNS_Employee_ID);
				rs = st.executeQuery();
				while (rs.next())
				{
					BigDecimal bal = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), Env.getAD_Client_ID(Env.getCtx()));
					balance = balance.add(bal);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		else
		{
			String sql = "SELECT SUM("+columnName+
					") FROM UNS_PayrollCorrection WHERE UNS_Employee_ID = ?"
					+ " AND DocStatus IN ('CO','CL')";
			if(C_Period_ID > 0)
				sql += " AND C_Period_ID = " + C_Period_ID;
			balance = DB.getSQLValueBD(trxName, sql, UNS_Employee_ID);
			if(balance == null)
				balance = Env.ZERO;
		}
		
		return balance;
	}
	
	/**
	 * 
	 * @param employeeID
	 * @param C_Period_ID
	 * @param trxName
	 * @return
	 */
	public static BigDecimal getBalancePPh21 (int employeeID, int C_Period_ID, boolean isPeriodic, String trxName)
	{
		BigDecimal balance = Env.ZERO;
		String columnName = null;
		
		if(isPeriodic)
			columnName = "BalancePPh21";
		else
			columnName = "BalancePPh21NonPeriodic";
		
		MColumn column = MColumn.get(Env.getCtx(), Table_Name, columnName);
		if (column.isEncrypted())
		{
			String sql = "SELECT "+columnName+
					" FROM UNS_PayrollCorrection WHERE UNS_Employee_ID = ?"
					+ " AND DocStatus IN ('CO','CL')";
			if(C_Period_ID > 0)
				sql += " AND C_Period_ID = " + C_Period_ID;
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, trxName);
				st.setInt(1, employeeID);
				rs = st.executeQuery();
				while (rs.next())
				{
					BigDecimal bal = (BigDecimal) SecureEngine.decrypt(rs.getBigDecimal(1), Env.getAD_Client_ID(Env.getCtx()));
					balance = balance.add(bal);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		else
		{
			String sql = "SELECT SUM("+columnName+
					") FROM UNS_PayrollCorrection WHERE UNS_Employee_ID = ?"
					+ " AND DocStatus IN ('CO','CL')";
			if(C_Period_ID > 0)
				sql += " AND C_Period_ID = " + C_Period_ID;
			balance = DB.getSQLValueBD(trxName, sql, employeeID);
			if(balance == null)
				balance = Env.ZERO;
		}
		
		return balance;
	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (docStatus.equals(DOCSTATUS_InProgress))
		{
			options[index++] = DOCACTION_Void;
		}
		
		if(docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Void;
			options[index++] = DOCACTION_Re_Activate;
		}
		
		return index;
	}
	
	private void doAddLog (Level level, String msg)
	{
		if (log.isLoggable(level)) log.log(level, msg);
	}
	
	@Override
	public boolean processIt(String action) throws Exception 
	{
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() 
	{
		doAddLog(Level.INFO, "Unlock it");
		setProcessed(false);
		return true;
	}

	@Override
	public boolean invalidateIt() 
	{
		doAddLog(Level.INFO, "Invalidate It");
		return false;
	}

	@Override
	public String prepareIt() 
	{
		doAddLog(Level.INFO, "Prepare Document");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		MUNSPayrollCorrectionLine[] lines = getLines();
		if (lines.length == 0)
		{
			m_processMsg = "@No Lines@";
			return DOCSTATUS_Invalid;
		}
		for (int i=0; i<lines.length; i++)
		{
			if(lines[i].getAmount().signum() == 0)
			{
				m_processMsg = "Some line containts zero amount";
				return DOCSTATUS_Invalid;
			}
			if (!MUNSPayrollCorrectionLine.CORRECTIONTYPE_PayrollReplacementCorrection.
					equals(lines[i].getCorrectionType()))
				continue;
			if (lines[i].getUNS_Payroll_Employee_ID() == 0)
			{
				m_processMsg = "Replacement of payroll tobe corrected is not generated";
				return DOCSTATUS_Invalid;
			}
			
			String sql = "SELECT Docstatus FROM UNS_Payroll_Employee WHERE UNS_Payroll_Employee_ID = ?";
			String stat = DB.getSQLValueString(get_TrxName(), sql, lines[i].getUNS_Payroll_Employee_ID());
			if (!"CO".equals(stat) && !"CL".equals(stat)) {
				m_processMsg = "Please complete linked payroll of correction first.";
				return DOCSTATUS_Invalid;
			}
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_justPrepared = true;
		setProcessed(true);
		setDocStatus(DOCSTATUS_InProgress);
		
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() 
	{
		doAddLog(Level.INFO, "Approve it");
		setIsApproved(true);
		setProcessed(true);
		
		return true;
	}

	@Override
	public boolean rejectIt() 
	{
		doAddLog(Level.INFO, "Reject it");
		setProcessed(false);
		setIsApproved(false);

		return true;
	}

	@Override
	public String completeIt() 
	{
		doAddLog(Level.INFO, "Complete It");
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (status != DOCSTATUS_InProgress)
			{
				return DOCSTATUS_Invalid;
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
			
		if (!isApproved())
		{
			approveIt();
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		setDocStatus(DOCSTATUS_Completed);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() 
	{
		doAddLog(Level.INFO, "Void it");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		if(hasPaid())
		{
			m_processMsg = "This correction has been paid";
			return false;
		}
		
		MUNSPayrollCorrectionLine[] lines = getLines();
		for (int i=0; i<lines.length; i++) {
			if (!lines[i].cancelPayrollReplacement()) {
				m_processMsg = CLogger.retrieveErrorString("Could not cancel payroll replacement");
				return false;
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setDocStatus(DOCSTATUS_Voided);
		return true;
	}

	@Override
	public boolean closeIt() 
	{
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setDocStatus(DOCSTATUS_Closed);
		setDocAction(DOCACTION_None);
		
		return true;
	}

	@Override
	public boolean reverseCorrectIt() 
	{
		m_processMsg = "Invalid action Reverse.";
		return false;
	}

	@Override
	public boolean reverseAccrualIt() 
	{
		m_processMsg = "Invalid action Reverse.";
		return false;
	}

	@Override
	public boolean reActivateIt() 
	{
		doAddLog(Level.INFO, "reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		if(hasPaid())
		{
			m_processMsg = "This correction has been paid";
			return false;
		}
		MUNSPayrollCorrectionLine[] lines = getLines();
		for (int i=0; i<lines.length; i++) {
			if (!lines[i].cancelPayrollReplacement()) {
				m_processMsg = CLogger.retrieveErrorString("Could not cancel payroll replacement");
				return false;
			}
			lines[i].setAmount(Env.ZERO);
			lines[i].setUNS_Payroll_Employee_ID(-1);
			if (!lines[i].save())
			{
				m_processMsg = CLogger.retrieveErrorString("Unknown Error");
				return false;
			}
		}
		setProcessed(false);
		setIsApproved(false);
		setDocAction(DOCACTION_Prepare);
		setDocStatus(DOCSTATUS_Drafted);
		saveEx();
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public String getSummary() 
	{
		return getDocumentInfo();
	}

	@Override
	public String getDocumentInfo() 
	{
		StringBuilder sb = new StringBuilder("[").append(get_ID())
				.append(", ").append(getDocumentNo()).append("]")
				.append(" - ").append(getDateDoc()).append(" - ")
				.append(getUNS_Employee().getName());
		String docInfo = sb.toString();
		
		return docInfo;
	}

	@Override
	public File createPDF() 
	{
		return null;
	}

	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	@Override
	public int getC_Currency_ID() 
	{
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() 
	{
		return getAmount();
	}

	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		List<Object[]> list = new ArrayList<>();	
		list.add(new Object[]{String.class, true}); 			
		list.add(new Object[]{String.class, true}); 			
		list.add(new Object[]{BigDecimal.class, true}); 							
		return list;
	}

	@Override
	public String[] getDetailTableHeader() {
		String def[] = new String[]{"Correction Type", "Source", "Amount", "Description"};
		return def;
	}

	@Override
	public List<Object[]> getDetailTableContent()
	{	
		List<Object[]> list = new ArrayList<>();
		String sql = "SELECT pcl.CorrectionType, COALESCE(pcc.Name, CAST(pcl.PresenceDate AS Char)),"
				+ " pcl.Amount, pcl.Description FROM UNS_PayrollCorrectionLine pcl"
				+ " LEFT JOIN UNS_Payroll_Component_Conf pcc ON pcc.UNS_Payroll_Component_Conf_ID"
				+ " = pcl.UNS_Payroll_Component_Conf_ID"
				+ " WHERE pcl.UNS_PayrollCorrection_ID = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, get_ID());
			rs = st.executeQuery();
			
			while (rs.next())
			{
				int count = 0;
			
				Object[] rowData = new Object[4];
				rowData[count] = rs.getObject(1);
				rowData[++count] = rs.getObject(2);
				rowData[++count] = rs.getObject(3);
				rowData[++count] = rs.getObject(4);
				
				list.add(rowData);
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return list;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}
