/**
 * 
 */
package com.uns.model.process;

import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.SvrProcess;

import com.uns.base.model.Query;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author nurse
 *
 */
public class CompleteImportedContract extends SvrProcess {

	/**
	 * 
	 */
	public CompleteImportedContract() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		//
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		List<MUNSContractRecommendation> recommendations = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
				MUNSContractRecommendation.Table_Name, 
				MUNSContractRecommendation.COLUMNNAME_UNS_Contract_Evaluation_ID 
				+ " IN (SELECT UNS_Contract_Evaluation_ID FROM UNS_Contract_Evaluation WHERE Description= ?)"
				+ " AND IsActive = 'Y' AND DocStatus NOT IN ('CO', 'CL', 'RE', 'VO')", 
				get_TrxName()).setParameters("###Auto Generated From Import Process###").list();
		for (int i=0; i<recommendations.size(); i++)
		{
			String status = recommendations.get(i).getDocStatus();
			if (!"CO".equals(status) && !"CL".equals(status) && !"RE".equals(status) && !"VO".equals(status)
					&& !recommendations.get(i).processIt(DocAction.ACTION_Complete))
			{
				throw new AdempiereException(recommendations.get(i).getProcessMsg());
			}
			recommendations.get(i).saveEx();
		}
		return recommendations.size() + " document processed";
	}

}
