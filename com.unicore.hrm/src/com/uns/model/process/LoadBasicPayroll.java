/**
 * 
 */
package com.uns.model.process;

import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSPayrollComponentConf;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollLevelConfig;

/**
 * @author menjangan
 *
 */
public class LoadBasicPayroll extends SvrProcess {

	MUNSContractRecommendation m_ContractRecommend = null;
	
	private Properties m_ctx = null;
	private String m_trxName = null;
	private boolean onlyComp = false;
	private boolean usePreviousPayroll = false;
	private MUNSContractRecommendation m_prevContractRecommend = null;
	

	/**
	 * 
	 */
	public LoadBasicPayroll() {
	}
	
	public LoadBasicPayroll(Properties ctx, int UNS_ContractRecommendation_ID, String trxName)
	{
		this.m_ctx = ctx;
		this.m_trxName = trxName;
		this.m_ContractRecommend = new MUNSContractRecommendation(
				m_ctx, UNS_ContractRecommendation_ID, m_trxName);
	}
	
	public LoadBasicPayroll(MUNSContractRecommendation contract)
	{
		this.m_ctx = contract.getCtx();
		this.m_trxName = contract.get_TrxName();
		this.m_ContractRecommend = contract;
	}
	
	public String load()
	{
		try {
			doIt();
		}catch (Exception ex)
		{
			return ex.getMessage();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		this.m_trxName = get_TrxName();
		this.m_ctx = getCtx();
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter para : params)
		{
			String name = para.getParameterName();
			if(name.equals("UsePreviousPayroll"))
				usePreviousPayroll = para.getParameterAsBoolean();
			else
				throw new AdempiereException("Unknown Parameter name : "+name);
		}
		
		m_ContractRecommend = new MUNSContractRecommendation(m_ctx, getRecord_ID(), m_trxName);
	}

	public void setOnlyComp(boolean onlyComp) {
		this.onlyComp = onlyComp;
	}
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	public String doIt() throws Exception {
		// worker is not generate payroll
		if (MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganCV.equals(
				m_ContractRecommend.getNextContractType()))
			return null;
		else if (!m_ContractRecommend.getDocStatus().equals("DR")
				&& !m_ContractRecommend.getDocStatus().equals("IP")) {
			throw new AdempiereException("Could not process processed document.");
		}
		MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
				m_ctx, m_ContractRecommend.getDateContractEnd(), m_ContractRecommend.getNewDept_ID(), m_trxName, true);
		if (null == payConfig)
			throw new AdempiereUserError("Not Found Payroll Configuration");
	
		MUNSPayrollLevelConfig payrollLevelConfig = payConfig.getPayrollLevel(
			m_ContractRecommend.getNewPayrollLevel(), m_ContractRecommend.getNextPayrollTerm()
			, m_ContractRecommend.getAD_Org_ID(), true);
		if (null == payrollLevelConfig)
			throw new AdempiereUserError(
				"Not Found Payroll Level Config for level " + m_ContractRecommend.getNewPayrollLevel());
		MUNSPayrollComponentConf[] comps = null;
		if(usePreviousPayroll)
		{
			if(m_ContractRecommend.getUNS_Contract_Evaluation_ID() > 0)
			{
				String sql = "SELECT UNS_Contract_Recommendation_ID FROM UNS_Contract_Evaluation"
						+ " WHERE UNS_Contract_Evaluation_ID = ?";
				int prevContractID = DB.getSQLValue(get_TrxName(), sql, m_ContractRecommend.getUNS_Contract_Evaluation_ID());
				m_prevContractRecommend = new MUNSContractRecommendation(getCtx(), prevContractID, get_TrxName());
				if(null == m_prevContractRecommend)
					throw new AdempiereUserError("Cannot found previous contract.");
				comps = MUNSPayrollComponentConf.get(m_prevContractRecommend);
			}
			else
			{
				throw new AdempiereException("Contract doesn't have previous contract");
			}
		}
		else
			comps = MUNSPayrollComponentConf.get(payrollLevelConfig);
		
		if(!onlyComp)
		{
			if (usePreviousPayroll)
			{	
				m_ContractRecommend.setNew_G_Pokok(m_prevContractRecommend.getNew_G_Pokok());
				m_ContractRecommend.setNew_T_Kesejahteraan(m_prevContractRecommend.getNew_T_Kesejahteraan());
				m_ContractRecommend.setNew_T_Jabatan(m_prevContractRecommend.getNew_T_Jabatan());
				m_ContractRecommend.setNew_T_Lembur(m_prevContractRecommend.getNew_T_Lembur());
				m_ContractRecommend.setNew_A_L1(m_prevContractRecommend.getNew_A_L1());
				m_ContractRecommend.setNew_A_L2(m_prevContractRecommend.getNew_A_L2());
				m_ContractRecommend.setNew_A_L3(m_prevContractRecommend.getNew_A_L3());
				m_ContractRecommend.setNew_A_L1_R(m_prevContractRecommend.getNew_A_L1());
				m_ContractRecommend.setNew_A_L2_R(m_prevContractRecommend.getNew_A_L2_R());
				m_ContractRecommend.setNew_A_L3_R(m_prevContractRecommend.getNew_A_L3_R());
				m_ContractRecommend.setNew_A_Premi(m_prevContractRecommend.getNew_A_Premi());
				m_ContractRecommend.setNew_A_Lain2(m_prevContractRecommend.getNew_A_Lain2());
				m_ContractRecommend.setNew_P_Mangkir(m_prevContractRecommend.getNew_P_Mangkir());
				m_ContractRecommend.setNew_P_SPTP(m_prevContractRecommend.getNew_P_SPTP());
				m_ContractRecommend.setNew_P_Lain2(m_prevContractRecommend.getNew_P_Lain2());
				m_ContractRecommend.setNewLeburJamPertama(m_prevContractRecommend.getNewLeburJamPertama());
				m_ContractRecommend.setNewLeburJamBerikutnya(m_prevContractRecommend.getNewLeburJamBerikutnya());
				m_ContractRecommend.setIsJHTApplyed(m_prevContractRecommend.isJHTApplyed());
				m_ContractRecommend.setIsJKApplyed(m_prevContractRecommend.isJKApplyed());
				m_ContractRecommend.setIsJKKApplyed(m_prevContractRecommend.isJKKApplyed());
				m_ContractRecommend.setIsJPApplied(m_prevContractRecommend.isJPApplied());
				m_ContractRecommend.setIsJPKApplyed(m_prevContractRecommend.isJPKApplyed());
				m_ContractRecommend.setIsMultiplicationOTCalc(m_prevContractRecommend.isMultiplicationOTCalc());
				m_ContractRecommend.setAL1Multiplier(m_prevContractRecommend.getAL1Multiplier());
				m_ContractRecommend.setAL2Multiplier(m_prevContractRecommend.getAL2Multiplier());
				m_ContractRecommend.setAL3Multiplier(m_prevContractRecommend.getAL3Multiplier());
				m_ContractRecommend.setALR1Multiplier(m_prevContractRecommend.getALR1Multiplier());
				m_ContractRecommend.setALR2Multiplier(m_prevContractRecommend.getALR2Multiplier());
				m_ContractRecommend.setALR3Multiplier(m_prevContractRecommend.getALR3Multiplier());
				m_ContractRecommend.setFirstOTMultiplier(m_prevContractRecommend.getFirstOTMultiplier());
				m_ContractRecommend.setNextOTMultiplier(m_prevContractRecommend.getNextOTMultiplier());
				m_ContractRecommend.setOTBasicAmt(m_prevContractRecommend.getOTBasicAmt());
				m_ContractRecommend.setPPH21PaidByCompany(m_prevContractRecommend.isPPH21PaidByCompany());
			}
			else 
//			if (m_ContractRecommend.getNewPayrollLevel().equals(m_ContractRecommend.getPrevPayrollLevel())){
//				
//				m_ContractRecommend.setNew_G_Pokok(m_ContractRecommend.getPrev_G_Pokok());
//				m_ContractRecommend.setNew_T_Kesejahteraan(m_ContractRecommend.getPrev_T_Kesejahteraan());
//				m_ContractRecommend.setNew_T_Jabatan(m_ContractRecommend.getPrev_T_Jabatan());
//				m_ContractRecommend.setNew_T_Lembur(m_ContractRecommend.getPrev_T_Lembur());
//				m_ContractRecommend.setNew_A_L1(m_ContractRecommend.getPrev_A_L1());
//				m_ContractRecommend.setNew_A_L2(m_ContractRecommend.getPrev_A_L2());
//				m_ContractRecommend.setNew_A_L3(m_ContractRecommend.getPrev_A_L3());
//				m_ContractRecommend.setNew_A_L1_R(m_ContractRecommend.getPrev_A_L1_R());
//				m_ContractRecommend.setNew_A_L2_R(m_ContractRecommend.getPrev_A_L2_R());
//				m_ContractRecommend.setNew_A_L3_R(m_ContractRecommend.getPrev_A_L3_R());
//				m_ContractRecommend.setNew_A_Premi(m_ContractRecommend.getPrev_A_Premi());
//				m_ContractRecommend.setNew_A_Lain2(m_ContractRecommend.getPrev_A_Lain2());
//				m_ContractRecommend.setNew_P_Mangkir(m_ContractRecommend.getPrev_P_Mangkir());
//				m_ContractRecommend.setNew_P_SPTP(m_ContractRecommend.getPrev_P_SPTP());
//				m_ContractRecommend.setNew_P_Lain2(m_ContractRecommend.getPrev_P_Lain2());
//				m_ContractRecommend.setNewLeburJamPertama(m_ContractRecommend.getPrevLeburJamPertama());
//				m_ContractRecommend.setNewLeburJamBerikutnya(m_ContractRecommend.getPrevLeburJamBerikutnya());
//			} else
			{		
				m_ContractRecommend.setNew_G_Pokok(payrollLevelConfig.getMin_G_Pokok());
				m_ContractRecommend.setNew_T_Kesejahteraan(payrollLevelConfig.getMin_G_T_Kesejahteraan());
				m_ContractRecommend.setNew_T_Jabatan(payrollLevelConfig.getMin_G_T_Jabatan());
				m_ContractRecommend.setNew_T_Lembur(payrollLevelConfig.getMin_G_T_Lembur());
				m_ContractRecommend.setNew_A_L1(payrollLevelConfig.getMin_A_L1());
				m_ContractRecommend.setNew_A_L2(payrollLevelConfig.getMin_A_L2());
				m_ContractRecommend.setNew_A_L3(payrollLevelConfig.getMin_A_L3());
				m_ContractRecommend.setNew_A_L1_R(payrollLevelConfig.getMin_A_L1_R());
				m_ContractRecommend.setNew_A_L2_R(payrollLevelConfig.getMin_A_L2_R());
				m_ContractRecommend.setNew_A_L3_R(payrollLevelConfig.getMin_A_L3_R());
				m_ContractRecommend.setNew_A_Premi(payrollLevelConfig.getMin_A_Premi());
				m_ContractRecommend.setNew_A_Lain2(payrollLevelConfig.getMin_A_Lain2());
				m_ContractRecommend.setNew_P_Mangkir(payrollLevelConfig.getP_Mangkir());
				m_ContractRecommend.setNew_P_SPTP(payrollLevelConfig.getP_SPTP());
				m_ContractRecommend.setNew_P_Lain2(payrollLevelConfig.getP_Lain2());
				m_ContractRecommend.setNewLeburJamPertama(payrollLevelConfig.getMin_A_LemburJamPertama());
				m_ContractRecommend.setNewLeburJamBerikutnya(payrollLevelConfig.getMin_A_Lembur());
				m_ContractRecommend.setIsJHTApplyed(payConfig.isJHTApplyed());
				m_ContractRecommend.setIsJKApplyed(payConfig.isJKApplyed());
				m_ContractRecommend.setIsJKKApplyed(payConfig.isJKKApplyed());
				m_ContractRecommend.setIsJPApplied(payConfig.isJPApplied());
				m_ContractRecommend.setIsJPKApplyed(payConfig.isJPKApplyed());
				m_ContractRecommend.setIsMultiplicationOTCalc(payConfig.isMultiplicationOTCalc());
				m_ContractRecommend.setAL1Multiplier(payrollLevelConfig.getAL1Multiplier());
				m_ContractRecommend.setAL2Multiplier(payrollLevelConfig.getAL2Multiplier());
				m_ContractRecommend.setAL3Multiplier(payrollLevelConfig.getAL3Multiplier());
				m_ContractRecommend.setALR1Multiplier(payrollLevelConfig.getALR1Multiplier());
				m_ContractRecommend.setALR2Multiplier(payrollLevelConfig.getALR2Multiplier());
				m_ContractRecommend.setALR3Multiplier(payrollLevelConfig.getALR3Multiplier());
				m_ContractRecommend.setFirstOTMultiplier(payrollLevelConfig.getFirstOTMultiplier());
				m_ContractRecommend.setNextOTMultiplier(payrollLevelConfig.getNextOTMultiplier());
				m_ContractRecommend.setOTBasicAmt(payrollLevelConfig.getOTBasicAmt());
				m_ContractRecommend.setPPH21PaidByCompany(payConfig.isPPH21PaidByCompany());
			}
			m_ContractRecommend.setPotonganToZero();
		}
		
		MUNSPayrollComponentConf[] exists = MUNSPayrollComponentConf.get(m_ContractRecommend); 
		for (int i=0; i<comps.length; i++) 
		{
			boolean isExists = false;
			for (int j=0; j<exists.length; j++)
			{
				if (exists[j].getName().equals(comps[i].getName())) {
					isExists = true;
					exists[j].setIsPPHComp(comps[i].isPPHComp());
					exists[j].setIsBase(comps[i].isBase());
					exists[j].setIsPaidOutsidePayroll(comps[i].isPaidOutsidePayroll());
					exists[j].setIsAllowOverride(comps[i].isAllowOverride());
					exists[j].setIsBaseonPresence(comps[i].isBaseonPresence());
					exists[j].setIsBenefit(comps[i].isBenefit());
					exists[j].setisMonthly(exists[j].isMonthly());
					exists[j].setIsMonthlyPPHComp(exists[j].isMonthlyPPHComp());
					exists[j].setIsPrinted(comps[i].isPrinted());
					exists[j].saveEx();
					break;
				}
			}
			if (isExists)
				continue;
			MUNSPayrollComponentConf comp = new MUNSPayrollComponentConf(m_ContractRecommend, comps[i]);
			comp.setIsPPHComp(comps[i].isPPHComp());
			comp.setIsBase(comps[i].isBase());
			comp.setIsPaidOutsidePayroll(comps[i].isPaidOutsidePayroll());
			comp.setIsAllowOverride(comps[i].isAllowOverride());
			comp.setIsBaseonPresence(comps[i].isBaseonPresence());
			comp.setIsBenefit(comps[i].isBenefit());
			comp.setisMonthly(comps[i].isMonthly());
			comp.setIsMonthlyPPHComp(comps[i].isMonthlyPPHComp());
			comp.setIsPrinted(comps[i].isPrinted());
			comp.saveEx();
		}
		if (!m_ContractRecommend.save(m_trxName))
			throw new AdempiereSystemError("Can't Load Basic Payroll");
		return null;
	}

}
