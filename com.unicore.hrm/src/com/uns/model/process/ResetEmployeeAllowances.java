/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MOrg;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import com.uns.base.model.Query;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSEmployeeAllowanceRecord;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author Haryadi
 *
 */
public class ResetEmployeeAllowances extends SvrProcess 
{
	private int m_Org_ID = 0;
	private Timestamp m_Date = null;
	private int m_EmployeeID = 0;
	private Properties m_ctx;
	private String m_trxName;
	
	/**
	 * 
	 */
	public ResetEmployeeAllowances() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			String paramName = param.getParameterName();
			if (paramName.equals("AD_Org_ID"))
				m_Org_ID = param.getParameterAsInt();
			else if(paramName.equals("Date"))
				m_Date = param.getParameterAsTimestamp();
			else if(paramName.equals("UNS_Employee_ID"))
				m_EmployeeID = param.getParameterAsInt();
		}
		
		m_ctx = getCtx();
		m_trxName = get_TrxName();
	}
	
	public ResetEmployeeAllowances(Properties ctx, int orgID, int employeeID, Timestamp date, String trxName)
	{
		m_ctx =  ctx;
		m_trxName = trxName;
		m_Org_ID = orgID;
		m_EmployeeID = employeeID;
		m_Date = date;
		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	public String doIt() throws Exception 
	{
		String whereClause = "EmploymentType='COM' AND IsActive='Y' " +
				"AND IsBlacklist='N' AND IsTerminate='N' AND UNS_Employee.UNS_Contract_Recommendation_ID > 0 " +
				"AND ? < (SELECT ctr.DateContractEnd FROM UNS_Contract_Recommendation ctr" +
				"		  WHERE ctr.UNS_Contract_Recommendation_ID = UNS_Employee.UNS_Contract_Recommendation_ID)";
		if (m_Org_ID > 0)
			whereClause += " AND AD_Org_ID=" + m_Org_ID;
		if (m_EmployeeID > 0)
			whereClause += " AND UNS_Employee_ID = " + m_EmployeeID;
		
		Timestamp currDate = m_Date == null ? new Timestamp(System.currentTimeMillis()) : m_Date;
		
		List<MUNSEmployee> employeeList = 
				Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
						  MUNSEmployee.Table_Name, whereClause, m_trxName)
					 .setParameters(currDate)
					 .setOrderBy(MUNSEmployee.COLUMNNAME_Value)
					 .list();
		
		if (employeeList.isEmpty())
			return "No Employee at department of " + MOrg.get(m_ctx, m_Org_ID).getName();
		
		String msg = "";
		//addLog ("Allowance records created for employee of :");
		
		for (MUNSEmployee employee : employeeList)
		{
			MUNSEmployeeAllowanceRecord empAllowance =
					MUNSEmployeeAllowanceRecord.getCreate(m_ctx, employee, currDate,
							MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave, m_trxName);
			
			if (empAllowance != null)
			{
				String createdMsg = "[" + employee.getValue() + "-" + employee.getName() + "]" + 
						(msg.isEmpty()? "," : "");
				
				addLog(createdMsg);
				msg += createdMsg;
			}
		}
		
		if (!msg.isEmpty())
			msg = "Allowance records created for employee of :\n";
		
		return msg;
	}

}
