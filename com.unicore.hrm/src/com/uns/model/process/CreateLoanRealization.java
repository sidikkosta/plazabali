/**
 * 
 */
package com.uns.model.process;

import org.compiere.process.SvrProcess;

/**
 * @author menjangan
 *
 */
public class CreateLoanRealization extends SvrProcess {

	/**
	 * 
	 */
	public CreateLoanRealization() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		//Do nothing
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		/*
		if(getRecord_ID() == 0)
			return "No Record ID !!!";
		
		MUNSEmployeeLoan loan = new MUNSEmployeeLoan(
				getCtx(), getRecord_ID(), get_TrxName());
		if(loan.isCreatedPayment())
		{
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo()
					, "Payment has ben created for this document. " +
							"Do you want to remove it and then re-create new payment?"
					, "Delete Previous Confirmation"
					, MessageBox.YESNO
					, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_NO)
				return "Process Aborted";
			
			MPayment pay = new MPayment(getCtx(), loan.getC_Payment_ID(), get_TrxName());
			if(!pay.getDocStatus().equals(MPayment.DOCSTATUS_Drafted))
				return "Process aborted, the payment has ben processed.";
			
			if(!pay.delete(true))
				return "Process aborted, can't delete previous payment";
		}
			
		return loan.createPay();
		*/
		return null;
	}

}
