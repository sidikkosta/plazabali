/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.uns.model.MUNSMedicalRecord;
import com.uns.model.MUNSReimbursement;

/**
 * @author Burhani Adam
 *
 */
public class LoadUnPaidMedicalAllowance extends SvrProcess {
	
	private int p_AD_Org_ID = 0;
	private Timestamp p_RequestDate = null;
	private int p_PeriodID = 0;

	/**
	 * 
	 */
	public LoadUnPaidMedicalAllowance() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
				p_AD_Org_ID = params[i].getParameterAsInt();
			else if("RequestDate".equals(name))
				p_RequestDate = params[i].getParameterAsTimestamp();
			else if("C_Period_ID".equals(name))
				p_PeriodID = params[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown parameter " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSMedicalRecord[] records = MUNSMedicalRecord.getUnPaid(getCtx(), p_AD_Org_ID, p_PeriodID, get_TrxName());
		if(records.length == 0)
			return "There is no data to be processed";
		
		BigDecimal reimburseAmt = Env.ZERO;
		
		MUNSReimbursement reimburse = MUNSReimbursement.getOpen(getCtx(), p_AD_Org_ID, p_RequestDate, get_TrxName());
		int count = 0;
		for(int i=0;i<records.length;i++)
		{
			reimburseAmt = reimburseAmt.add(records[i].getMedicalCosts());
			records[i].setUNS_Reimbursement_ID(reimburse.get_ID());
			records[i].saveEx();
			count++;
		}
		reimburse.setTotalRecord(reimburse.getTotalRecord() + records.length);
		reimburse.setSubjectCosts(reimburse.getSubjectCosts().add(reimburseAmt));
		reimburse.saveEx();
		
		return "Success " + count + " medical record has create reimbursement.";
	}
}