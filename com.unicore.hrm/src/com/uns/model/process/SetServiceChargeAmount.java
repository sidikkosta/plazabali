/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSGlobalIncentivesConfig;
import com.uns.model.MUNSShopRecap;
import com.uns.model.MUNSSubregionRecap;
import com.uns.model.X_UNS_GlobalIncentivesConfig;

/**
 * @author dpitaloka
 *
 */
public class SetServiceChargeAmount extends SvrProcess {
	
	private BigDecimal m_amount;
	private X_UNS_GlobalIncentivesConfig m_globalConf;
	private MUNSShopRecap m_shopRecap;
	
	/**
	 * 
	 */
	public SetServiceChargeAmount() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// Each Report & Process parameter name is set by the field DB Column Name
		for ( ProcessInfoParameter para : getParameter())
		{
			if ( para.getParameterName().equals("ServiceChargeAmt") )
				m_amount = (BigDecimal) para.getParameter();
			else 
				log.info("Parameter not found " + para.getParameterName());
		}
		
		m_shopRecap = new MUNSShopRecap(getCtx(), getRecord_ID(), get_TrxName());
		MUNSSubregionRecap subregion = (MUNSSubregionRecap) m_shopRecap.getUNS_SubregionRecap();
		
		m_globalConf = MUNSGlobalIncentivesConfig.get(getCtx(), 
				subregion.getAD_Org_ID(), subregion.getShopType(), 
				subregion.getC_Period_ID(), get_TrxName());		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (m_globalConf == null)
			return "Cannot fond appropriate Incentive Configuration for this recapitulation.";
		/**
		String sql = "UPDATE uns_shoprecap SET "
				+ "totalservicechargeamt = "+ amount
				+ " WHERE uns_shoprecap_id = " + getRecord_ID();
		DB.executeUpdateEx(sql, get_TrxName());
		**/
		BigDecimal employeePercent = Env.ONEHUNDRED.subtract(m_globalConf.getSupportPortion());
		
		BigDecimal distributableAmt = m_amount.multiply(m_globalConf.getSCForEmployee().divide(
				Env.ONEHUNDRED, 4, BigDecimal.ROUND_DOWN));
		BigDecimal employeePortion = distributableAmt.multiply(
				employeePercent.divide(Env.ONEHUNDRED, 4, BigDecimal.ROUND_DOWN));
		BigDecimal supportPortion = distributableAmt.multiply(
				m_globalConf.getSupportPortion().divide(Env.ONEHUNDRED, 4, BigDecimal.ROUND_DOWN));
		
		m_shopRecap.setTotalServiceChargeAmt(m_amount);		
		m_shopRecap.setDistributableServiceCharge(distributableAmt);
		m_shopRecap.setEmployeePortion(employeePortion);
		m_shopRecap.setSupportPortion(supportPortion);
		m_shopRecap.saveEx();
		
		String sql = "UPDATE UNS_SubregionRecap sub "
				+ " SET TotalServiceChargeAmt=("
				+ "		SELECT SUM(sr.TotalServiceChargeAmt) FROM UNS_ShopRecap sr"
				+ "		WHERE sr.UNS_SubregionRecap_ID=sub.UNS_SubregionRecap_ID)"
				+ ", EmployeePortion=("
				+ "		SELECT SUM(sr.DistributableServiceCharge) FROM UNS_ShopRecap sr"
				+ "		WHERE sr.UNS_SubregionRecap_ID=sub.UNS_SubregionRecap_ID)"
				+ " WHERE sub.UNS_SubregionRecap_ID=" + getRecord_ID();
		
		DB.executeUpdateEx(sql, get_TrxName());
		
		return "Total service charge amount has been changed.";
	}

}
