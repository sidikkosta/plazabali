/**
 * 
 */
package com.uns.model.process;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSMonthlyPresenceSummary;

/**
 * @author menjangan
 *
 */
public class GeneratePeriodicCanteenActivity extends SvrProcess {

	private int p_orgID = -1;
	private int p_periodID = -1;
	private int p_secDeptID = -1;
	private int p_employeeID = -1;
	
	/**
	 * 
	 */
	public GeneratePeriodicCanteenActivity() {
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] ps = getParameter();
		for (int i=0; i<ps.length; i++) {
			if (ps[i].getParameterName().equals("AD_Org_ID"))
				p_orgID = ps[i].getParameterAsInt();
			else if (ps[i].getParameterName().equals("C_Period_ID"))
				p_periodID = ps[i].getParameterAsInt();
			else if (ps[i].getParameterName().equals("SectionDept_ID"))
				p_secDeptID = ps[i].getParameterAsInt();
			else if (ps[i].getParameterName().equals("UNS_Employee_ID"))
				p_employeeID = ps[i].getParameterAsInt();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		if (p_periodID == -1)
			throw new AdempiereException("Mandatory parameter Period");
		StringBuilder sb = new StringBuilder(" C_Period_ID = ? ");
		List<Object> params = new ArrayList<>();
		params.add(p_periodID);
		if (p_orgID > 0) {
			sb.append(" AND AD_Org_ID = ?");
			params.add(p_orgID);
		}
		if (p_secDeptID > 0) {
			sb.append(" AND C_BPartner_ID = ? ");
			params.add(p_secDeptID);
		}
		if (p_employeeID > 0) {
			sb.append(" AND UNS_Employee_ID = ? ");
			params.add(p_employeeID);
		}
		
		sb.append(" AND NOT EXISTS (SELECT UNS_CanteenActivity_ID "
				+ " FROM UNS_CanteenActivity WHERE UNS_MonthlyPresenceSummary_ID = "
				+ " UNS_MonthlyPresenceSummary.UNS_MonthlyPresenceSummary_ID)");
		MUNSMonthlyPresenceSummary[] attendances = MUNSMonthlyPresenceSummary.get(
				getCtx(), sb.toString(), params, null, get_TrxName());
		String msg = null;
		for (int i=0; i<attendances.length; i++) {
			if ((msg = attendances[i].generateEmployeeCanteenActivity(false, false, false)) != null)
				throw new AdempiereException(msg);
		}
		return null;
	}
}
