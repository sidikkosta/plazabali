/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.uns.base.model.Query;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSMonthlyPresenceSummary;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author nurse
 *
 */
public class ChangeStartEndPeriodOfPresence extends SvrProcess 
{
	private int p_periodID = 0;
	private int p_orgID = 0;
	private int p_sectOfDeptID = 0;
	private Timestamp p_startDate = null;
	private Timestamp p_endDate = null;
	private Timestamp m_nextStart = null;
	private int m_nextPeriodID = 0;
	private IProcessUI m_message;
	
	/**
	 * 
	 */
	public ChangeStartEndPeriodOfPresence() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("C_Period_ID".equals(params[i].getParameterName()))
				p_periodID = params[i].getParameterAsInt();
			else if ("AD_Org_ID".equals(params[i].getParameterName()))
				p_orgID = params[i].getParameterAsInt();
			else if ("SectionOfDept_ID".equals(params[i].getParameterName()))
				p_sectOfDeptID = params[i].getParameterAsInt();
			else if ("StartDate".equals(params[i].getParameterName()))
				p_startDate = params[i].getParameterAsTimestamp();
			else if ("EndDate".equals(params[i].getParameterName()))
				p_endDate = params[i].getParameterAsTimestamp();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		m_message = Env.getProcessUI(getCtx());
		m_nextStart = TimeUtil.addDays(p_endDate, 1);
		MPeriod period = MPeriod.get(getCtx(), p_periodID);
		int nextPeriodNo = period.getPeriodNo();
		int nexYearID = period.getC_Year_ID();
		if (nextPeriodNo == 12)
		{
			nextPeriodNo = 0;
			String fcYear = DB.getSQLValueString(
					get_TrxName(), "SELECT FiscalYear FROM C_Year WHERE C_Year_ID = ?", 
					period.getC_Year_ID());
			int yearNo = Integer.valueOf(fcYear);
			yearNo++;
			nexYearID = DB.getSQLValue(get_TrxName(), "SELECT C_Year_ID FROM C_Year WHERE FiscalYear = ?", Integer.toString(yearNo));
			if (nexYearID <= 0)
				throw new AdempiereException("could not find year " + yearNo);
		}
		
		nextPeriodNo++;	
		m_nextPeriodID = DB.getSQLValue(get_TrxName(), 
				"SELECT C_Period_ID FROM C_Period WHERE C_Year_ID = ? AND PeriodNo = ?", 
				nexYearID, nextPeriodNo);
		
		StringBuilder sb = new StringBuilder(MUNSMonthlyPresenceSummary.COLUMNNAME_C_Period_ID).append(" = ?");	
		List<Object> params = new ArrayList<>();
		params.add(p_periodID);
		
		if (p_orgID != 0)
		{
			sb.append(" AND ").append(MUNSMonthlyPresenceSummary.COLUMNNAME_AD_Org_ID).append(" = ? ");
			params.add(p_orgID);
		}
		if (p_sectOfDeptID != 0)
		{
			sb.append(" AND ").append(MUNSMonthlyPresenceSummary.COLUMNNAME_C_BPartner_ID).append(" = ? ");
			params.add(p_sectOfDeptID);
		}
		
		sb.append(" AND Processed = 'N'");
		String wc = sb.toString();
		
		List<MUNSMonthlyPresenceSummary> months = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSMonthlyPresenceSummary.Table_Name, 
				wc, get_TrxName()).setParameters(params).list();
		for (int i=0; i<months.size(); i++)
		{
			m_message.statusUpdate(months.size() + " record. Processed record :: " + i);
			if (!updateMonthly(months.get(i)))
				throw new AdempiereException();
		}
		
		return months.size() + " record updated";
	}

	private boolean updateMonthly (MUNSMonthlyPresenceSummary month)
	{
		MUNSEmployee emp = MUNSEmployee.get(getCtx(), month.getUNS_Employee_ID());
		HashMap<Integer,MUNSContractRecommendation> list = emp.getMapContract(true, p_startDate);
		Timestamp start = p_startDate;
		MUNSContractRecommendation contract = null;
		while(!start.after(p_endDate))
		{
			boolean founded = false;
			for(int i=0; i<list.size(); i++)
			{
				contract = list.get(i);
				if(!contract.isSynchronized())
					continue;
				
				if(contract.getEffectiveDate().compareTo(start) <= 0)
				{
					founded = true;
					break;
				}
			}
			if(founded)
				break;
			start = TimeUtil.addDays(start, 1);
		}
		
		Timestamp end = p_endDate;
		while(!end.before(p_startDate))
		{
			boolean founded = false;
			for(int i=0; i<list.size(); i++)
			{
				contract = list.get(i);
				if(!contract.isSynchronized())
					continue;
				
				if(contract.getDateContractEnd().compareTo(end) >= 0)
				{
					founded = true;
					break;
				}
			}
			if(founded)
				break;
			end = TimeUtil.addDays(end, -1);
		}

		month.setStartDate(start);
		month.setEndDate(end);
		if (!month.save())
			return false;
		if (!deleteDaily(month))
			return false;
		return createUpdateNextPeriod(month);
	}
	
	private boolean deleteDaily (MUNSMonthlyPresenceSummary month)
	{
		String sql = "DELETE FROM UNS_DailyPresence WHERE UNS_MonthlyPresenceSummary_ID = ? "
				+ " AND (PresenceDate < ? OR PresenceDate > ?)";
		int result = DB.executeUpdate(sql, new Object[]{month.get_ID(), month.getStartDate(), month.getEndDate()},  
				false, get_TrxName());
		return result != -1;
	}
	
	private boolean createUpdateNextPeriod (MUNSMonthlyPresenceSummary current)
	{
		MUNSMonthlyPresenceSummary nextMonth = MUNSMonthlyPresenceSummary.get(
				getCtx(), current.getUNS_Employee_ID(), m_nextPeriodID, current.getAD_Org_ID(), get_TrxName());
		if (nextMonth == null)
			return true;
		if (nextMonth.isProcessed())
			return false;
		nextMonth.setStartDate(m_nextStart);
		if (!deleteDaily(nextMonth))
			return false;
		return nextMonth.save();
	}
}
