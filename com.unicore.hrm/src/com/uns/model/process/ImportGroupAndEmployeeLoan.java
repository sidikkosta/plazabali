/**
 * 
 */
package com.uns.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;

/**
 * @author AzHaidar
 *
 */
public class ImportGroupAndEmployeeLoan extends SvrProcess {

	private String m_File_Directory;

	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("File_Directory"))
				m_File_Directory = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}		
	}

	@Override
	protected String doIt() throws Exception 
	{		
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportGroupAndEmployeeLoan'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with name ImportGroupAndEmployeeLoan.");
		
		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(m_File_Directory);
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length < 1)
			throw new AdempiereException("No Import Simple XLS Table found " + simple.getName());
		
		SimpleImportXLS groupRec = new SimpleImportXLS(getCtx(), simple, tables[0], false, 0, 0, 0, getAD_Client_ID(), get_TrxName());
		
		SimpleImportXLS empRec = new SimpleImportXLS(getCtx(), simple, tables[1], false, 0, 0, 0, getAD_Client_ID(), get_TrxName());
		
		String success1 = null;
		String success2 = null;
		try{
			 success1 = groupRec.doIt();
			 success2 = empRec.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		
		return success1 + "; " + success2;
	}

}
