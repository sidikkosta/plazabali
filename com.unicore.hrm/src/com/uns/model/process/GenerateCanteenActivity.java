/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSMonthlyPresenceSummary;

/**
 * @author menjangan
 *
 */
public class GenerateCanteenActivity extends SvrProcess {

	MUNSMonthlyPresenceSummary m_model;
	/**
	 * 
	 */
	public GenerateCanteenActivity() {
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		m_model = new MUNSMonthlyPresenceSummary(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		String msg;
		if ((msg = m_model.generateEmployeeCanteenActivity(false, false, false)) != null)
			throw new AdempiereException(msg);
		return "success";
	}

}
