package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import com.uns.model.MUNSMonthlyPresenceSummary;

public class UNSActionPresenceSummary extends SvrProcess {

	public String DOC_ACTION = "CO";
	public int p_Org_ID = -1;
	public int p_Period_ID = 0;
	
	
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			String name = param.getParameterName();
			if(name.equals("DocAction"))
				DOC_ACTION = param.getParameterAsString();
			else if(name.equals("AD_Org_ID"))
				p_Org_ID = param.getParameterAsInt();
			else if(name.equals("C_Period_ID"))
				p_Period_ID = param.getParameterAsInt();
			else
				throw new AdempiereException("Unknown Parameter Name : "+name);
		}
	
		
	}

	@Override
	protected String doIt() throws Exception {
		
		String whereClause = null;
		
		if(p_Org_ID > 0)
			whereClause = "AD_Org_ID = "+p_Org_ID;
		
		if(p_Period_ID > 0)
		{
			if(whereClause != null)
				whereClause += " AND ";
			
			whereClause += "C_Period_ID = "+p_Period_ID;
		}
		
		UNSCompletingIncompleteDocument process = new UNSCompletingIncompleteDocument(
				get_TrxName(), MUNSMonthlyPresenceSummary.Table_ID, DOC_ACTION, whereClause, getCtx());
		process.setNeedAsk(false);
		
		return process.doIt();
	}

}
