/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.SecureEngine;
import com.uns.model.MUNSPayrollEmployee;
import com.uns.model.MUNSPayrollPaymentBA;
import com.uns.model.MUNSPayrollPaymentEmp;

/**
 * @author nurse
 *
 */
public class GeneratePayrollPayment extends SvrProcess 
{

	private boolean p_allBank = false;
	/**
	 * 
	 */
	public GeneratePayrollPayment() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("IsAllBank".equals(params[i].getParameterName()))
				p_allBank = params[i].getParameterAsBoolean();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSPayrollPaymentBA model = new MUNSPayrollPaymentBA(getCtx(), getRecord_ID(), get_TrxName());
		StringBuilder b = new StringBuilder("SELECT UNS_Payroll_Employee_ID, TakeHomePay ");
		b.append(" FROM UNS_Payroll_Employee WHERE DocStatus IN ('CO','CL') AND C_Period_ID = ? ")
		.append(" AND AD_Org_ID = ? AND NOT EXISTS (SELECT UNS_Payroll_Employee_ID FROM ")
		.append(" UNS_PayrollPayment_Emp WHERE IsActive = 'Y' AND UNS_Payroll_Employee_ID = ")
		.append(" UNS_Payroll_Employee.UNS_Payroll_Employee_ID AND EXISTS (SELECT * FROM ")
		.append(" UNS_PayrollPayment_BA WHERE IsActive = 'Y' AND UNS_PayrollPayment_BA_ID = ")
		.append(" UNS_PayrollPayment_Emp.UNS_PayrollPayment_BA_ID AND EXISTS (SELECT * FROM ")
		.append(" UNS_PayrollPayment WHERE UNS_PayrollPayment_ID = UNS_PayrollPayment_BA.")
		.append("UNS_PayrollPayment_ID AND DocStatus NOT IN ('RE','VO')))) ");
		if (!p_allBank)
			b.append(" AND (SELECT C_Bank_ID FROM UNS_Employee WHERE UNS_Employee_ID = UNS_Payroll_Employee.UNS_Employee_ID) = ")
			.append(model.getC_BankAccount().getC_Bank_ID());
		String sql = b.toString();
		PreparedStatement st = null;
		ResultSet rs = null;
		int count = 0;
		MColumn column = MColumn.get(getCtx(), MUNSPayrollEmployee.Table_Name, MUNSPayrollEmployee.COLUMNNAME_TakeHomePay); 
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, model.getParent().getC_Period_ID());
			st.setInt(2, model.getAD_Org_ID());
			rs = st.executeQuery();
			while (rs.next())
			{
				MUNSPayrollPaymentEmp emp = new MUNSPayrollPaymentEmp(model);
				emp.setUNS_Payroll_Employee_ID(rs.getInt(1));
				BigDecimal amount = rs.getBigDecimal(2);
				if (column.isEncrypted())
					amount = (BigDecimal) SecureEngine.decrypt(amount, getAD_Client_ID());
				emp.setAmount(amount);
				emp.saveEx();
			}
		}
		catch (SQLException ex)
		{
			throw new AdempiereException(ex.getMessage());
		}
		finally 
		{
			DB.close(rs, st);
		}
		
		return count + " record generated";
	}

}
