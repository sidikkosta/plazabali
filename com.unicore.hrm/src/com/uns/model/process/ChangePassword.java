/**
 * 
 */
package com.uns.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MUser;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

/**
 * @author ALBURHANY
 *
 */
public class ChangePassword extends SvrProcess{
	
	private String oldPassword = null;
	private String newPassword = null;
//	private String newPasswordConfirm = null; TODO comment, unused
	
	@Override
	protected void prepare()
	{		        
		ProcessInfoParameter[] params = getParameter();
        for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("OldPassword"))
				oldPassword = param.getParameterAsString();
			else if(param.getParameterName().equals("NewPassword"))
				newPassword = param.getParameterAsString();
			else if(param.getParameterName().equals("NewPasswordConfirm"));
//				newPasswordConfirm= param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
        		
	}

	@Override
	protected String doIt() throws Exception {
		
		MUser user = new MUser(getCtx(), getProcessInfo().getAD_User_ID(), get_TrxName());
		
//		String a = oldPassword;
//		String b = newPassword;
//		String c = newPasswordConfirm;
//		String d = user.getPassword();
		if(user.getPassword() != oldPassword)
			throw new AdempiereException("Password do not macth");
		else if(oldPassword != newPassword)
			throw new AdempiereException("new password do not macth");
		else
			user.setPassword(newPassword);
			user.saveEx();
		return null;
	}

}
