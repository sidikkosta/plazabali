/**
 * 
 */
package com.uns.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.NIKGenerator;
import com.uns.util.MessageBox;

/**
 * @author menjangan
 *
 */
public class GenerateNIK extends SvrProcess {

	private MUNSContractRecommendation m_ContractRecomendation = null;
	private String m_reff = "";
	private boolean m_updateAttName = false;
	private String m_prefixNIK = "";
	/**
	 * 
	 */
	public GenerateNIK() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++){
			String name = para[i].getParameterName();
			log.fine("prepare - " + para[i]);
			if ("value".equalsIgnoreCase(name))
				m_reff = (String) para[i].getParameter();
			else if ("UpdateAttName".equals(name))
				m_updateAttName = para[i].getParameterAsBoolean();
			else if ("PrefixNIK".equals(name))
				m_prefixNIK = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);		
		}
		m_ContractRecomendation = new MUNSContractRecommendation(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {		
		if (m_ContractRecomendation.isProcessed()) {
			throw new AdempiereException("Could not process processed document");
		}
		MUNSEmployee employee = MUNSEmployee.get(getCtx(), m_ContractRecomendation.getUNS_Employee_ID());
		String payrollTerm = m_ContractRecomendation.getNextPayrollTerm();
//		String employeType = employee.getEmploymentType();
//		if(m_ContractRecomendation.isGenerateNIK())
//			return null;
		
		if (m_reff!=null && m_reff.length()>0 && !m_reff.equalsIgnoreCase("000000")){
			
			//check dupilcate nik
			String sql = "SELECT name FROM UNS_Employee WHERE UNS_Employee_ID <> ?"
					+ " AND Value = ? AND isTerminate = ? ORDER BY isActive DESC";
			String empName = DB.getSQLValueString(get_TrxName(), sql, employee.get_ID(), m_reff, "N");
			if(!Util.isEmpty(empName, true))
				throw new AdempiereException("Duplicate NIK with active employee. #Employee Name : "+empName);
			
			empName = DB.getSQLValueString(get_TrxName(), sql, employee.get_ID(), m_reff, "Y");
			if(!Util.isEmpty(empName, true))
			{
				int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), 
						"There was employee has been terminated use this NIK. #Name : "+empName
						+ ".\n Do you want reused this NIK?",
						"Duplicate NIK ", 
						MessageBox.YESNO, MessageBox.ICONQUESTION);
				if(retVal == MessageBox.RETURN_NO)
				{
					return null;
				}
			}
			
			m_ContractRecomendation.setNewNIK(m_reff);
			employee.setValue(m_reff);
		}
		else
		{
			NIKGenerator nikGenerator = new NIKGenerator(m_ContractRecomendation,
					NIKGenerator.TYPE_CUSTOMIZATIONNIK, m_prefixNIK, get_TrxName(), getProcessInfo());
			m_ContractRecomendation.setNewNIK(nikGenerator.getNewNIK());
			employee.setValue(nikGenerator.getNewNIK());
			if(m_updateAttName)
				employee.setAttendanceName(nikGenerator.getNewNIK());
		}
		/**
		else if (employeType.equals(MUNSEmployee.EMPLOYMENTTYPE_Company))
		{			
			if (m_ContractRecomendation.getNextContractType().equals(
					MUNSContractRecommendation.NEXTCONTRACTTYPE_Permanen)
					|| payrollTerm.equals(MUNSEmployee.PAYROLLTERM_Monthly))
			{
				NIKGenerator nikGenerator = new NIKGenerator(m_ContractRecomendation, NIKGenerator.TYPE_PERMANEN, get_TrxName());
				m_ContractRecomendation.setNewNIK(nikGenerator.getNewNIK());
				employee.setValue(nikGenerator.getNewNIK());
				//employee.setEmploymentStatus(MUNSEmployee.EMPLOYMENTSTATUS_Bulanan);
			}
			
			else if (payrollTerm.equals(MUNSEmployee.PAYROLLTERM_2Weekly)
					|| payrollTerm.equals(MUNSEmployee.PAYROLLTERM_Weekly))
			{
				NIKGenerator nikGenerator = new NIKGenerator(m_ContractRecomendation, NIKGenerator.TYPE_HARIAN, get_TrxName());
				m_ContractRecomendation.setNewNIK(nikGenerator.getNewNIK());
				employee.setValue(nikGenerator.getNewNIK());
			}
		}
		else if(employee.getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_SubContract))
		{
			NIKGenerator nikGenerator = new NIKGenerator(m_ContractRecomendation, NIKGenerator.TYPE_PEMBORONG, get_TrxName());
			m_ContractRecomendation.setNewNIK(nikGenerator.getNewNIK());
			employee.setValue(nikGenerator.getNewNIK());
		}
		*/
		
		m_ContractRecomendation.setGenerateNIK("Y");
		m_ContractRecomendation.saveEx();
		
		employee.setPayrollTerm(payrollTerm);
		employee.saveEx();
		return null;
	}

}
