/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.compiere.model.MOrg;
import org.compiere.model.MPeriod;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.MUNSPeriodicCostBenefit;
import com.uns.model.MUNSPeriodicCostBenefitLine;
import com.uns.model.X_UNS_PeriodicCostBenefit;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author dpitaloka -- AzHaidar
 *
 */
public class LoadToEmployeePayrollBenefit extends SvrProcess {

	private int m_Org_ID = 0;
	private MPeriod m_period;
	
	private Hashtable<String, MUNSPeriodicCostBenefit> m_pcbMap = 
			new Hashtable<String, MUNSPeriodicCostBenefit>();

	/**
	 * 
	 */
	public LoadToEmployeePayrollBenefit() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		for ( ProcessInfoParameter para : getParameter()) 
		{			
			//if ( para.getParameterName().equals("UNS_SubregionRecap_ID") ) {
			if ( para.getParameterName().equals("AD_Org_ID") ) {
				m_Org_ID = para.getParameterAsInt();
			} else if ( para.getParameterName().equals("C_Period_ID") ) {
				m_period = new MPeriod(getCtx(), para.getParameterAsInt(), get_TrxName());
			} else
				log.info("Parameter not found " + para.getParameterName());
		}
		
		// Delete PeriodicCostBenefit-Line.
		String sql = "DELETE FROM UNS_PeriodicCostBenefitLine WHERE UNS_PeriodicCostBenefit_ID IN ("
				+ "		SELECT UNS_PeriodicCostBenefit_ID FROM UNS_PeriodicCostBenefit "
				+ "		WHERE DocStatus NOT IN ('CO', 'CL') AND C_Period_ID=" + m_period.getC_Period_ID();
		if (m_Org_ID > 0)
			sql += " AND AD_Org_ID=" + m_Org_ID;
		sql += ")";
		
		int count = DB.executeUpdate(sql, get_TrxName());
		log.info("Detail PeriodicCostBenefit deleted: " + count + " records.");
		addLog("Detail PeriodicCostBenefit deleted: " + count + " records.");
		
		// Delete PeriodicCostBenefit.
		sql = "DELETE FROM UNS_PeriodicCostBenefit "
				+ "WHERE DocStatus NOT IN ('CO', 'CL') AND C_Period_ID=" + m_period.get_ID();
		if (m_Org_ID > 0)
			sql += " AND AD_Org_ID=" + m_Org_ID;
		
		count = DB.executeUpdate(sql, get_TrxName());
		log.info("PeriodicCostBenefit deleted: " + count + " records.");
		addLog("PeriodicCostBenefit deleted: " + count + " records.");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		String sql = "SELECT sub.AD_Org_ID, se.UNS_Employee_ID, "
				+ "	COALESCE(SUM(se.SalesIncentiveAmt), 0), COALESCE(SUM(se.CashierIncentiveAmt), 0),"
				+ "	COALESCE(SUM(se.GroupIncentiveAmt), 0), COALESCE(SUM(se.PushMoneyAmt), 0) "
				+ " FROM UNS_ShopEmployee se"
				+ "		INNER JOIN UNS_ShopRecap sr ON se.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID"
				+ "		INNER JOIN UNS_SubregionRecap sub ON sub.UNS_SubregionRecap_ID=sr.UNS_SubregionRecap_ID "
				+ "	WHERE sub.C_Period_ID=" + m_period.get_ID() + " AND sub.DocStatus IN ('CO', 'CL') ";
		
		String sql2 = "SELECT sub.AD_Org_ID, sscr.UNS_Employee_ID, "
				+ " COALESCE(SUM(sscr.ServiceChargeAmt), 0) "
				+ " FROM UNS_StaffSrvChgRecap sscr"
				+ "		INNER JOIN UNS_ShopRecap sr ON sscr.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID"
				+ "		INNER JOIN UNS_SubregionRecap sub ON sub.UNS_SubregionRecap_ID=sr.UNS_SubregionRecap_ID "
				+ "	WHERE sub.C_Period_ID=" + m_period.get_ID() + " AND sub.DocStatus IN ('CO', 'CL') ";
		
		if (m_Org_ID > 0) {
			sql += " AND sub.AD_Org_ID=" + m_Org_ID;
			sql2 += " AND sub.AD_Org_ID=" + m_Org_ID;
		}
		
		sql += " GROUP BY sub.AD_Org_ID, se.UNS_Employee_ID ";
		sql2 += " GROUP BY sub.AD_Org_ID, sscr.UNS_Employee_ID ";
		
		
		Hashtable<String, String> msgs = new Hashtable<String, String>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = DB.prepareStatement(sql, get_TrxName());
			rs = ps.executeQuery();
			
			while (rs.next())
			{
				int AD_Org_ID = rs.getInt(1);
				int UNS_Employee_ID = rs.getInt(2);
				
				String key = AD_Org_ID + "_" + "IIN";
				String msg = resetPeriodicCostBenefitLine(
						AD_Org_ID, "IIN", "Sales Incentive", UNS_Employee_ID, rs.getBigDecimal(3));
				if (msg != null && msgs.get(key) == null)
					msgs.put(key, msg);
				
				key = AD_Org_ID + "_" + "CIN";
				msg = resetPeriodicCostBenefitLine(
						AD_Org_ID, "CIN", "Cashier Incentive", UNS_Employee_ID, rs.getBigDecimal(4));
				if (msg != null && msgs.get(key) == null)
					msgs.put(key, msg);

				key = AD_Org_ID + "_" + "GIN";
				msg = resetPeriodicCostBenefitLine(
						AD_Org_ID, "GIN", "Group Incentive", UNS_Employee_ID, rs.getBigDecimal(5));
				if (msg != null && msgs.get(key) == null)
					msgs.put(key, msg);

				key = AD_Org_ID + "_" + "PUM";
				msg = resetPeriodicCostBenefitLine(
						AD_Org_ID, "PUM", "Push Money Incentive", UNS_Employee_ID, rs.getBigDecimal(6));
				if (msg != null && msgs.get(key) == null)
					msgs.put(key, msg);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
		
		try {
			ps = DB.prepareStatement(sql2, get_TrxName());
			rs = ps.executeQuery();
			
			while (rs.next())
			{
				int AD_Org_ID = rs.getInt(1);
				int UNS_Employee_ID = rs.getInt(2);
				
				String key = AD_Org_ID + "_" + "SVC";
				String msg = resetPeriodicCostBenefitLine(
						AD_Org_ID, "SVC", "Service Charge Incentive ", UNS_Employee_ID, rs.getBigDecimal(3));
				if (msg != null && msgs.get(key) == null)
					msgs.put(key, msg);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
		
		for (String msg : msgs.values())
			addLog(msg);

		return null;
	}
	
	private MUNSPeriodicCostBenefit getCreatePeriodicCostBenefit(int AD_Org_ID, String costBenefitType)
	{
		String key = AD_Org_ID + "_" + costBenefitType; 
		MUNSPeriodicCostBenefit costBenefit = m_pcbMap.get(key);
		if (costBenefit == null) {
			costBenefit = Query
				.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
						X_UNS_PeriodicCostBenefit.Table_Name,
						"costbenefittype=? AND c_period_id = ? AND AD_Org_ID=?", get_TrxName())
						.setParameters(costBenefitType, m_period.get_ID(), AD_Org_ID)
						.first();
			if (costBenefit == null) 
			{
				costBenefit = new MUNSPeriodicCostBenefit(getCtx(), 0, get_TrxName());
				costBenefit.setAD_Org_ID(AD_Org_ID);
				costBenefit.setCostBenefitType(costBenefitType);
				costBenefit.setC_Period_ID(m_period.getC_Period_ID());
				costBenefit.setC_Year_ID(m_period.getC_Year_ID());
				costBenefit.setDateFrom(m_period.getStartDate());
				costBenefit.setDateTo(m_period.getEndDate());
				costBenefit.setIsActive(true);
				costBenefit.saveEx();
			}
			
			m_pcbMap.put(key, costBenefit);			
		}
		
		return costBenefit;
	}
	
	private String resetPeriodicCostBenefitLine(int AD_Org_ID, String costBenefitType, String subject,
			int UNS_Employee_ID, BigDecimal amount)
	{
		if (amount == null || amount.signum() == 0)
			return null;
		
		MUNSPeriodicCostBenefit PeriodicBenefit = getCreatePeriodicCostBenefit(AD_Org_ID, costBenefitType);
		String msg = null;
		
		if(PeriodicBenefit.getDocStatus().equals(DocAction.STATUS_Completed)
				|| PeriodicBenefit.getDocStatus().equals(DocAction.STATUS_Closed)) {
				msg = subject + " of " + MOrg.get(getCtx(), AD_Org_ID).getValue() 
						+ " has been completed. It can't be reset.";
		}
		else {
			msg = subject + " of " + MOrg.get(getCtx(), AD_Org_ID).getValue() 
						+ " created.";
			MUNSPeriodicCostBenefitLine benefitLine = new MUNSPeriodicCostBenefitLine(PeriodicBenefit);
			benefitLine.setUNS_Employee_ID(UNS_Employee_ID);
			benefitLine.setAmount(amount);
			benefitLine.save();					
		}
		
		return msg;
	}
}
