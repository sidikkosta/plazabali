package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MUser;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.MUNSEmployee;

public class UNSCreateUserFromEmployee extends SvrProcess {

	private MUNSEmployee emp = null;
	
	public UNSCreateUserFromEmployee() {
		
	}

	@Override
	protected void prepare() {
		
		if(getRecord_ID() > 0)
			emp = new MUNSEmployee(getCtx(), getRecord_ID(), get_TrxName());
		else
			throw new AdempiereException("Only can process from window");
		
	}

	@Override
	protected String doIt() throws Exception {
		
		if(emp.getNickName() == null)
			throw new AdempiereException("Null column nickname. Please fill it first to continue the process.");
		
		//check user first
		String sql = "SELECT AD_User_ID FROM AD_User WHERE UNS_Employee_ID = ?"
				+ " AND IsActive = 'Y'";
		int ad_user_id = DB.getSQLValue(get_TrxName(), sql, emp.get_ID());
		if(ad_user_id > 0)
			return "This employee already has User Login.";
		
		MUser user = new MUser(getCtx(), 0, get_TrxName());
		user.setAD_Org_ID(0);
		user.setUNS_Employee_ID(emp.get_ID());
		user.setName(emp.getNickName());
		user.setValue(emp.getNickName());
		user.setRealName(emp.getName());
		user.setEMail(emp.getEMail());
		user.setUserAgent(false);
		user.setAccessOwnOrgData(true);
		user.saveEx();
		
		return "success";
	}

}
