/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.compiere.process.SvrProcess;
import org.compiere.util.CPreparedStatement;
import org.compiere.util.DB;
import com.uns.model.X_UNS_ShopRecap;

/**
 * @author dpitaloka
 *
 */
public class CalculateStoreSalesIncentive extends SvrProcess {

	private BigDecimal m_supervisorPortion = BigDecimal.ZERO;
	private BigDecimal m_cashierPortion = BigDecimal.ZERO;
	private BigDecimal m_groupPercent = BigDecimal.ZERO;
	private X_UNS_ShopRecap m_storeSalesRecap;

	/**
	 * 
	 */
	public CalculateStoreSalesIncentive() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		String sql = "SELECT SUM(grandtotal)"
				+ " FROM uns_postrx PT, uns_shoprecap SR, c_period P"
				+ " WHERE SR.uns_shoprecap = ?"
				+ " AND PT.c_bpartner_id = SR.shop_id"
				+ " AND P.c_period_id = SR.c_period_id"
				+ " AND PT.dateacct BETWEEN P.startdate AND P.enddate";
		
		CPreparedStatement pstmt = DB.prepareStatement(sql, get_TrxName());
		pstmt.setInt(1, getRecord_ID());
		ResultSet rs = pstmt.executeQuery();
		BigDecimal netSalesRealizationAmt = BigDecimal.ZERO;
		if (rs.next()) {
			netSalesRealizationAmt = rs.getBigDecimal(1);
		}
		
		BigDecimal groupIncentiveForSales = BigDecimal.ZERO;		
		BigDecimal temp1, temp2, temp3;
		temp1 = netSalesRealizationAmt.multiply(m_groupPercent);
		temp2 = m_groupPercent.multiply(m_cashierPortion.add(m_supervisorPortion));
		temp3 = netSalesRealizationAmt.multiply(temp2);
		groupIncentiveForSales = temp1.subtract(temp3);
		m_storeSalesRecap.setGroupSalesIncentive(groupIncentiveForSales);

		BigDecimal groupIncentiveForCashier = BigDecimal.ZERO;
		groupIncentiveForCashier = netSalesRealizationAmt
				.multiply(m_cashierPortion);
		
		m_storeSalesRecap.setGroupCashierIncentive(groupIncentiveForCashier);

		m_storeSalesRecap.save();
		return "Store Sales Incentive has been calculated";
	}

	@Override
	protected void prepare() {
		m_storeSalesRecap = new X_UNS_ShopRecap(getCtx(),
				getRecord_ID(), get_TrxName());

		String sqlConfig = "SELECT MAX(c_period_id), supervisorportion, cashierportion, grouppercent "
				+ "FROM uns_globalincentivesconfig "
				+ "WHERE c_period_id <= ? "
				+ "GROUP BY supervisorportion, cashierportion, grouppercent";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sqlConfig, get_TrxName());
			pstmt.setInt(1, (m_storeSalesRecap.getUNS_SubregionRecap().getC_Period_ID()));
			rs = pstmt.executeQuery();
			
			if (rs.next()) {				
				m_supervisorPortion = rs.getBigDecimal(2);
				m_cashierPortion = rs.getBigDecimal(3);
				m_groupPercent = rs.getBigDecimal(4);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

//	private BigDecimal calculateNotPresentSalesAmt() {
//		BigDecimal notpresentsalesamt = Query.get(getCtx(),
//				UNSHRMModelFactory.EXTENSION_ID, I_UNS_ShopEmployee.Table_Name,
//				"uns_shoprecap_id =  ?", get_TrxName()).aggregate(
//				"notpresentsalesamt", Query.AGGREGATE_SUM);
//		return notpresentsalesamt;		
//	}
}
