/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollEmployee;

/**
 * @author menjangan
 *
 */
public class GeneratePPh21 extends SvrProcess {

	private MUNSPayrollEmployee m_payroll = null;
	/**
	 * 
	 */
	public GeneratePPh21() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		m_payroll = new MUNSPayrollEmployee(getCtx(), getRecord_ID(), get_TrxName());

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
		if(m_payroll.isProcessed())
			return "Document already processed";
		
		if (!"Y".equals(m_payroll.getGeneratePay()))
			throw new AdempiereException("Please generate payroll first.");
				MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
				getCtx(), (MPeriod)m_payroll.getC_Period(), m_payroll.getAD_Org_ID(),
				get_TrxName(), true);
		m_payroll.setPPH21(payConfig.getPPH(m_payroll, false));
		m_payroll.saveEx();
		return null;
	}

}
