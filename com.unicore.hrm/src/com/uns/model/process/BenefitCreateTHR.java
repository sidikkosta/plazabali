/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import com.uns.model.MUNSPeriodicCostBenefit;

/**
 * @author nurse
 *
 */
public class BenefitCreateTHR extends SvrProcess 
{

	private MUNSPeriodicCostBenefit m_benefit = null;
	private String p_religion = null;
	private Timestamp p_dateCalc = null;
	
	/**
	 * 
	 */
	public BenefitCreateTHR() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("DateCount".equals(params[i].getParameterName()))
				p_dateCalc = params[i].getParameterAsTimestamp();
			else if ("Religion".equals(params[i].getParameterName()))
				p_religion = params[i].getParameterAsString();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
		m_benefit = new MUNSPeriodicCostBenefit(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		GenerateTHR turbin = new GenerateTHR(m_benefit, p_religion, p_dateCalc);
		turbin.setProcessUI(processUI);
		return turbin.doIt();
	}

}
