/**
 * 
 */
package com.uns.model.process;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSDailyPresence;

/**
 * @author Burhani Adam
 *
 */
public class UNSUnlinkPresenceValidation extends SvrProcess {
	
	private int p_DailyPresenceID = 0;
	/**
	 * 
	 */
	public UNSUnlinkPresenceValidation() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("UNS_DailyPresence_ID"))
				p_DailyPresenceID = param.getParameterAsInt();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSDailyPresence dp = new MUNSDailyPresence(getCtx(), p_DailyPresenceID, get_TrxName());
		dp.setUNS_MonthlyPresenceVal_ID(-1);
		dp.setFSTimeIn(null);
		dp.setFSTimeOut(null);
		dp.setRunValidator(false);
		dp.setPresenceStatus(MUNSDailyPresence.PRESENCESTATUS_Mangkir);
		dp.saveEx();
		return "Success unlink presence validation.";
	}
}