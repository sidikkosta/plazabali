/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.MUNSGlobalIncentivesConfig;
import com.uns.model.MUNSShopEmployee;
import com.uns.model.MUNSShopRecap;
import com.uns.model.MUNSStaffSrvChgRecap;
import com.uns.model.MUNSSubregionRecap;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author dpitaloka
 *
 */
public class LoadShopStaff extends SvrProcess 
{

	private List<MUNSShopRecap> m_shopList = null;
	private MUNSSubregionRecap m_subRecap = null;
	
	/**
	 * 
	 */
	public LoadShopStaff() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		if (MUNSSubregionRecap.Table_ID == getTable_ID())
		{
			m_shopList = Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSShopRecap.Table_Name, 
					"UNS_SubregionRecap_ID=?", get_TrxName())
					.setParameters(getRecord_ID())
					.list();
			m_subRecap = new MUNSSubregionRecap(getCtx(), getRecord_ID(), get_TrxName());
		}
		else 
		{
			MUNSShopRecap shopRecap = new MUNSShopRecap(getCtx(), getRecord_ID(), get_TrxName());
			m_shopList = new ArrayList<MUNSShopRecap>();
			m_shopList.add(shopRecap);
			m_subRecap = (MUNSSubregionRecap) shopRecap.getUNS_SubregionRecap();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSGlobalIncentivesConfig gic = MUNSGlobalIncentivesConfig.get(
				getCtx(), m_subRecap.getAD_Org_ID(), m_subRecap.getShopType(), 
				m_subRecap.getC_Period_ID(), get_TrxName());
		
		if (gic == null)
			return "Failed: cannot find any Global Incentive Configuration for F&B ShopType in the expected period.";
				
		for (MUNSShopRecap shopRecap : m_shopList)
		{
			String sql = "SELECT uns_employee_id, positionType FROM UNS_ShopEmployee "
					+ " WHERE UNS_ShopRecap_ID=" + shopRecap.get_ID();
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = DB.prepareStatement(sql, get_TrxName());
				//ps.setInt(1, getRecord_ID());
				rs = ps.executeQuery(); 
				while (rs.next()) 
				{
					int employee_ID = rs.getInt(1);
					
					MUNSStaffSrvChgRecap srvChgRecap = 
							Query.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSStaffSrvChgRecap.Table_Name, 
									"UNS_Employee_ID=? AND UNS_ShopRecap_ID=?", get_TrxName())
							.setParameters(employee_ID, shopRecap.getUNS_ShopRecap_ID())
							.firstOnly();
					
					if (srvChgRecap == null) {
						srvChgRecap = new MUNSStaffSrvChgRecap(getCtx(), 0, get_TrxName());
					
						srvChgRecap.setUNS_Employee_ID(employee_ID);
						srvChgRecap.setUNS_ShopRecap_ID(shopRecap.get_ID());
					}
					srvChgRecap.setShop_ID(shopRecap.getShop_ID());
					
					String shopType = rs.getString(2);
					srvChgRecap.setPositionType(shopType);
					
					switch (shopType) {
					case MUNSShopEmployee.POSITIONTYPE_Staff:
						srvChgRecap.setPoint(gic.getStaffPoint());
						break;

					case MUNSShopEmployee.POSITIONTYPE_ShopSupervisor:
						srvChgRecap.setPoint(gic.getStoreSupervisorPoint());
						break;

					case MUNSShopEmployee.POSITIONTYPE_SeniorSupervisor:
						srvChgRecap.setPoint(gic.getSeniorSupervisorPoint());
						break;

					case MUNSShopEmployee.POSITIONTYPE_JuniorManager:
						srvChgRecap.setPoint(gic.getJuniorManagerPoint());
						break;

					case MUNSShopEmployee.POSITIONTYPE_Manager:
						srvChgRecap.setPoint(gic.getManagerPoint());
						break;

					default:
						srvChgRecap.setPoint(BigDecimal.ONE);
						break;
					}
					srvChgRecap.saveEx();
				}
			} catch (SQLException e) {
				log.log(Level.SEVERE, sql, e);
			} finally {
				DB.close(rs, ps);
				rs = null;
				ps = null;
			}
		}
		return "Shop staffs was loaded.";
	}

}
