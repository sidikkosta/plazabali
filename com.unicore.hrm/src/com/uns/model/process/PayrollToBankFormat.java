/**
 * 
 */
package com.uns.model.process;

import java.io.File;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Ini;

import com.uns.model.MUNSPayrollBankFormat;
import com.uns.model.MUNSPayrollPaymentBA;

/**
 * @author nurse
 *
 */
public class PayrollToBankFormat extends SvrProcess 
{

	private MUNSPayrollPaymentBA m_model = null;
	private MUNSPayrollBankFormat p_bankFormat = null;
	
	/**
	 * 
	 */
	public PayrollToBankFormat() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("UNS_PayrollBankFormat_ID".equals(params[i].getParameterName()))
				p_bankFormat = new MUNSPayrollBankFormat(getCtx(), params[i].getParameterAsInt(), get_TrxName());
		}
		m_model = new MUNSPayrollPaymentBA(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (p_bankFormat == null)
			throw new AdempiereUserError("Mandatory parameter Bank Format!!!");
		File file = p_bankFormat.generateBankFormat(m_model);
		if (file == null)
			throw new AdempiereException("Error when try to create file");
		processUI.download(file);
		String fileName = "";
		if (Ini.isClient())
			fileName = file.getAbsolutePath();
		return "File exported " + fileName;
	}

}
