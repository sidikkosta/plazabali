/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;

import org.compiere.model.X_C_Job;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSEmployeeAllowanceRecord;
import com.uns.model.MUNSLeaveReservedConfig;

/**
 * @author Burhani Adam
 *
 */
public class CreateLongLeaveRecord extends SvrProcess {

	/**
	 * 
	 */
	public CreateLongLeaveRecord() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_AD_Org_ID = 0;
	private int p_C_BPartner_ID = 0;
	private int p_UNS_Employee_ID = 0;
	private Timestamp p_ValidFrom = null;
	private int p_C_Job_ID = 0;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
				p_AD_Org_ID = params[i].getParameterAsInt();
			else if ("C_BPartner_ID".equals(name))
				p_C_BPartner_ID = params[i].getParameterAsInt();
			else if ("UNS_Employee_ID".equals(name))
				p_UNS_Employee_ID = params[i].getParameterAsInt();
			else if ("C_Job_ID".equals(name))
				p_C_Job_ID = params[i].getParameterAsInt();
			else if ("ValidFrom".equals(name))
				p_ValidFrom = params[i].getParameterAsTimestamp();
			else
				log.log(Level.SEVERE, "Unknown parameter " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSLeaveReservedConfig[] configs = MUNSLeaveReservedConfig.get(getCtx(),
				MUNSLeaveReservedConfig.LEAVETYPE_LongLeave, get_TrxName());
		
		for(int i=0;i<configs.length;i++)
		{
			if(p_C_Job_ID > 0)
			{
				X_C_Job job = new X_C_Job(getCtx(), p_C_Job_ID, get_TrxName());
				if(job.getC_JobCategory_ID() != configs[i].getC_JobCategory_ID())
					continue;
			}
			
			String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(UNS_Employee_ID),';') FROM UNS_Employee emp WHERE"
					+ " EffectiveEntryDate IS NOT NULL AND (UNS_Contract_Recommendation_ID IS NOT NULL"
					+ " OR UNS_Contract_Recommendation_ID > 0)"
					+ " AND AD_Org_ID = ?";
			if(p_C_Job_ID > 0)
				sql += " AND C_Job_ID = " + p_C_Job_ID;
			if(p_C_BPartner_ID > 0)
				sql += " AND C_BPartner_ID = " + p_C_BPartner_ID;
			if(p_UNS_Employee_ID > 0)
				sql += " AND UNS_Employee_ID = " + p_UNS_Employee_ID;
			
			String empIDString = DB.getSQLValueString(get_TrxName(), sql, p_AD_Org_ID);
			if(empIDString == null)
				return "No Data";
			String[] empIDArray = empIDString.split(";");
			
			for(int j=0;j<empIDArray.length;j++)
			{
				Integer empID = new Integer (empIDArray[j]);
				MUNSEmployee emp = new MUNSEmployee(getCtx(), empID, get_TrxName());
				if(!emp.getNationality().equals(configs[i].getNationality()))
					continue;
				MUNSContractRecommendation contract = new MUNSContractRecommendation(getCtx(),
						emp.getUNS_Contract_Recommendation_ID(), get_TrxName());
				Timestamp last = emp.getLastLongLeaveDate() == null ? emp.getEffectiveEntryDate() : emp.getLastLongLeaveDate();
				Calendar calLast = Calendar.getInstance();
				calLast.setTime(last);
			    calLast.add(Calendar.YEAR, configs[i].getLongLeaveRecurringYear());
			    Timestamp minDate = new Timestamp (calLast.getTimeInMillis());
			    if(contract.getDateContractEnd().before(minDate))
			    	continue;
			    Timestamp now = new Timestamp(System.currentTimeMillis());
			    if(minDate.after(now))
			    	continue;
			    
			    MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.get(getCtx(), emp.get_ID(), minDate,
			    		MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_LongLeave, emp.getAD_Org_ID(), get_TrxName());
			    if(record != null)
			    	continue;
			    
			    record = new MUNSEmployeeAllowanceRecord(getCtx(), 0, get_TrxName());
			    record.setAD_Org_ID(emp.getAD_Org_ID());
			    record.setName("Recurring Long Leave.");
			    record.setUNS_Employee_ID(emp.get_ID());
			    record.setUNS_Contract_Recommendation_ID(contract.get_ID());
			    String sql2 = "SELECT C_Year_ID FROM C_Year WHERE FiscalYear = ?";
			    int C_Year_ID = DB.getSQLValue(get_TrxName(), sql2, String.valueOf(calLast.get(Calendar.YEAR)));
			    record.setC_Year_ID(C_Year_ID);
			    record.setLeavePeriodType(MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_LongLeave);
			    record.setPeriodDateStart(p_ValidFrom);
			    Calendar calValidTo = Calendar.getInstance();
			    calValidTo.add(Calendar.MONTH, configs[i].getGracePeriod());
			    record.setPeriodDateEnd(new Timestamp (calValidTo.getTimeInMillis()));
			    record.setRemarks("Long leave initialized based on contract " + contract.getDocumentNo() + ".");
			    record.setMedicalAllowance(Env.ZERO);
			    record.setMedicalAllowanceUsed(Env.ZERO);
			    record.setLeaveClaimReserved(configs[i].getLeaveClaimReserved());
			    record.saveEx();
			    
			    record.processIt("CO");
			    record.saveEx();
			    
			    emp.setLastLongLeaveDate(minDate);
			    emp.saveEx();
			}
		}
		
		return "Success";
	}
}