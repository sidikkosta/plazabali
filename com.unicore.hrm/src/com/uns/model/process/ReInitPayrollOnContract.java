/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.model.MUNSContractRecommendation;

/**
 * @author Burhani Adam
 *
 */
public class ReInitPayrollOnContract extends SvrProcess {
	
	private IProcessUI processUI;
	private int p_OrgID = 0;
	private String p_PayrollLevel = null;
	private String p_PayrollTerm = null;
	private int p_EmpID = 0;
	private boolean p_OnlyComp = false;
	
	/**
	 * 
	 */
	public ReInitPayrollOnContract() {
		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("AD_Org_ID"))
				p_OrgID = param.getParameterAsInt();
			else if(param.getParameterName().equals("PayrollLevel"))
				p_PayrollLevel = param.getParameterAsString();
			else if(param.getParameterName().equals("PayrollTerm"))
				p_PayrollTerm = param.getParameterAsString();
			else if(param.getParameterName().equals("UNS_Employee_ID"))
				p_EmpID = param.getParameterAsInt();
			else if(param.getParameterName().equals("OnlyComp"))
				p_OnlyComp = param.getParameterAsBoolean();
		}
		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		processUI = Env.getProcessUI(getCtx());
		
		processUI.statusUpdate("Initialize Employee");
		
		MUNSContractRecommendation[] contracts = getContracts();
		
		for(int i=0;i<contracts.length;i++)
		{
			processUI.statusUpdate("Process " + contracts.length + " contract on Employee :: " 
					+ (i+1) + ". " + contracts[i].getUNS_Employee().getName());
			contracts[i].setFromAnotherProcess(true);
			contractAction(contracts[i], "RE");
			LoadBasicPayroll load = new LoadBasicPayroll(contracts[i]);
			load.setOnlyComp(p_OnlyComp);
			load.load();
			contractAction(contracts[i], "CO");
		}

		return "Success " + contracts.length;
	}
	
	private void contractAction(MUNSContractRecommendation cr, String doAction) throws Exception
	{
		try {
			if(!cr.processIt(doAction))
				throw new AdempiereException(cr.getProcessMsg() + " - " + cr.getUNS_Employee().getName());
			cr.saveEx();
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage() + " - " + cr.getUNS_Employee().getName());
		}
	}
	
	private MUNSContractRecommendation[] getContracts()
	{
		String sql = "SELECT cr.* FROM UNS_Contract_Recommendation cr"
				+ " INNER JOIN UNS_Employee emp ON emp.UNS_Contract_Recommendation_ID = cr.UNS_Contract_Recommendation_ID"
				+ " WHERE emp.IsTerminate = 'N' AND emp.IsActive = 'Y'"
				+ " AND emp.AD_Org_ID = ?";
		if(!Util.isEmpty(p_PayrollLevel, true))
			sql += " AND emp.PayrollLevel = '" + p_PayrollLevel + "'";
		if(!Util.isEmpty(p_PayrollTerm, true))
			sql += " AND emp.PayrollTerm = '" + p_PayrollTerm + "'";
		if(p_EmpID > 0)
			sql += " AND emp.UNS_Employee_ID = " + p_EmpID;
		List<MUNSContractRecommendation> list = new ArrayList<>();
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, p_OrgID);
			rs = st.executeQuery();
			while(rs.next())
			{
				list.add(new MUNSContractRecommendation(getCtx(), rs, get_TrxName()));
			}
		}
		catch(SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return list.toArray(new MUNSContractRecommendation[list.size()]);
	}
}