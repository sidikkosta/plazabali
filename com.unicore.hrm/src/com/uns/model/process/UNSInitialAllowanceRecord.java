/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSEmployeeAllowanceRecord;

/**
 * @author Burhani Adam
 *
 */
public class UNSInitialAllowanceRecord extends SvrProcess
{

	/**
	 * 
	 */
	public UNSInitialAllowanceRecord() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_OrgID = 0;
	private int p_YearID = 0;
	private int p_BPartnerID = 0;
	private int p_EmployeeID = 0;
	private IProcessUI m_process;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("AD_Org_ID"))
				p_OrgID = param.getParameterAsInt();
			else if(param.getParameterName().equals("C_Year_ID"))
				p_YearID = param.getParameterAsInt();
			else if(param.getParameterName().equals("C_BPartner_ID"))
				p_BPartnerID = param.getParameterAsInt();
			else if(param.getParameterName().equals("UNS_Employee_ID"))
				p_EmployeeID = param.getParameterAsInt();
		}

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_process = Env.getProcessUI(getCtx());
		MUNSContractRecommendation[] contracts = getContracts();
		int count = 1;
		for(MUNSContractRecommendation contract : contracts)
		{
			MUNSEmployee emp = MUNSEmployee.get(getCtx(), contract.getUNS_Employee_ID());
			m_process.statusUpdate(contracts.length + " record. Record-" + count + " for " + emp.getName());
			String sql = "SELECT (CASE WHEN DATE_PART('Year', ?::timestamp) = y.FiscalYear::numeric THEN ?::timestamp"
					+ " WHEN DATE_PART('Year', ?::timestamp) = y.FiscalYear::numeric THEN ?::timestamp"
					+ " ELSE (y.FiscalYear || '-01-01')::timestamp"
					+ " END) FROM C_Year y WHERE y.C_Year_ID = ?";
			Timestamp date = DB.getSQLValueTS(get_TrxName(), sql, new Object[]{
				contract.getDateContractStart(), contract.getDateContractStart(),
				contract.getDateContractEnd(), contract.getDateContractEnd(),
				p_YearID});
			MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.getCreate(getCtx(), emp, date, 
					MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave, get_TrxName());
			if(record == null)
				throw new AdempiereException("Failed when trying create allowance record for employee " + emp.getName() + ". "
						+ CLogger.retrieveErrorString(""));
			count++;
		}
		
		return "Success create/update " + contracts.length + " employess";
	}
	
	private MUNSContractRecommendation[] getContracts()
	{
		List<MUNSContractRecommendation> list = new ArrayList<>();
		String sql = "SELECT cr.* FROM UNS_Employee e"
				+ " INNER JOIN UNS_Contract_Recommendation cr ON cr.UNS_Employee_ID = e.UNS_Employee_ID"
				+ " INNER JOIN C_Year y ON y.C_Year_ID = ?"
				+ " WHERE cr.DocStatus IN ('CO', 'CL') AND cr.IsActive = 'Y' AND "
				+ " DATE_PART('Year', cr.DateContractStart) <= y.FiscalYear::numeric"
				+ " AND DATE_PART('Year', cr.DateContractEnd) >= y.FiscalYear::numeric"
				+ " AND e.AD_Org_ID = ?";
		if(p_BPartnerID > 0)
			sql += " AND e.C_BPartner_ID = " + p_BPartnerID;
		if(p_EmployeeID > 0)
			sql += " AND e.UNS_Employee_ID = " + p_EmployeeID;
		
		sql += " ORDER BY e.Name, cr.DateContractStart";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, p_YearID);
			stmt.setInt(2, p_OrgID);
			rs = stmt.executeQuery();
			while(rs.next())
			{
				list.add(new MUNSContractRecommendation(getCtx(), rs, get_TrxName()));
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		return list.toArray(new MUNSContractRecommendation[list.size()]);
	}
}