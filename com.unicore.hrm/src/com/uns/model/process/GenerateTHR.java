/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollLevelConfig;
import com.uns.model.MUNSPeriodicCostBenefit;
import com.uns.model.MUNSPeriodicCostBenefitLine;

/**
 * @author nurse
 *
 */
public class GenerateTHR extends SvrProcess 
{

	private int p_orgID = 0;
	private int p_sectionOfDeptID = 0;
	private String p_religion = null;
	private MPeriod m_period = null;
	private Hashtable<String, MUNSPeriodicCostBenefit> m_mapBenefit;
	private Timestamp p_dateCalc = null;
	private Properties m_ctx = null;
	private String m_trxName = null;
	private int m_ignored = 0;
	
	@Override
	public String get_TrxName ()
	{
		if (m_trxName == null)
			m_trxName = super.get_TrxName();
		return m_trxName;
	}
	
	@Override
	public Properties getCtx ()
	{
		if (m_ctx == null)
			m_ctx = super.getCtx();
		
		return m_ctx;
	}
	
	public void setProcessUI (IProcessUI process)
	{
		processUI = process;
	}
	
	public GenerateTHR (MUNSPeriodicCostBenefit benefit, String religion, Timestamp dateCalc)
	{
		this ();
		if (benefit.isProcessed())
			throw new AdempiereUserError("Benefit is processed before");
		if (!MUNSPeriodicCostBenefit.COSTBENEFITTYPE_TunjanganHariRaya.equals(benefit.getCostBenefitType()))
			throw new AdempiereException("Invalid Benefit Type");
		m_mapBenefit.put(buildKey(benefit.getAD_Org_ID(), benefit.getC_Year_ID(), benefit.getC_Period_ID()), benefit);
		p_orgID = benefit.getAD_Org_ID();
		p_religion = religion;
		p_dateCalc = dateCalc;
		m_ctx = benefit.getCtx();
		m_trxName = benefit.get_TrxName();
		m_period = (MPeriod) benefit.getC_Period();
	}
	
	/**
	 * 
	 */
	public GenerateTHR() 
	{
		super ();
		m_mapBenefit = new Hashtable<>();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("AD_Org_ID".equals(params[i].getParameterName()))
				p_orgID = params[i].getParameterAsInt();
			else if ("SectionOfDept_ID".equals(params[i].getParameterName()))
				p_sectionOfDeptID = params[i].getParameterAsInt();
			else if ("Religion".equals(params[i].getParameterName()))
				p_religion = params[i].getParameterAsString();
			else if ("C_Period_ID".equals(params[i].getParameterName()))
				m_period = MPeriod.get(getCtx(), params[i].getParameterAsInt());
			else if ("DateCount".equals(params[i].getParameterName()))
				p_dateCalc = params[i].getParameterAsTimestamp();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	public String doIt() throws Exception 
	{
		List<Object> params = new ArrayList<>();
		params.add(p_religion);
		params.add("Y");
		params.add("N");
		params.add("N");
		StringBuilder sb = new StringBuilder("Religion = ? AND IsActive = ? AND IsTerminate = ? AND IsBlacklist = ?");
		if (p_orgID > 0)
		{
			sb.append(" AND AD_Org_ID = ? ");
			params.add(p_orgID);
		}
		if (p_sectionOfDeptID > 0)
		{
			sb.append(" AND C_BPartner_ID = ? ");
			params.add(p_sectionOfDeptID);
		}
		String where = sb.toString();
		List<MUNSEmployee> employees = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSEmployee.Table_Name, 
				where, get_TrxName()).setParameters(params).setOrderBy("Value").
				list();
		String msg;
		for (int i=0; i<employees.size(); i++)
		{
			if ((msg = doGenerate(employees.get(i))) != null)
				throw new AdempiereException(msg);
		}
		
		return employees.size() - m_ignored + " record generated";
	}
	
	private String docalc (MUNSPeriodicCostBenefitLine costBen, MUNSEmployee emp)
	{
		MUNSPayrollLevelConfig config = MUNSPayrollLevelConfig.get(
				getCtx(), emp.getPayrollLevel(), emp.getPayrollTerm(), emp.getAD_Org_ID(), true, get_TrxName());
		if (config == null)
		{
			m_ignored++;
			return null;
		}
		BigDecimal amount = config.calculateTHR(emp, p_dateCalc);
		if (amount == null || amount.signum() == 0)
		{
			m_ignored++;
			return null;
		}
		costBen.setAmount(amount);
		if (!costBen.save())
			return CLogger.retrieveErrorString("Could not create benefit");
		return null;
	}

	private String doGenerate (MUNSEmployee emp)
	{
		MUNSPeriodicCostBenefitLine line = MUNSPeriodicCostBenefitLine.get(
				getCtx(), m_period.get_ID(), MUNSPeriodicCostBenefit.COSTBENEFITTYPE_TunjanganHariRaya, 
				emp.get_ID(), get_TrxName());
		if (line != null)
		{
			if (line.isProcessed())
			{
				m_ignored ++;
				return null;
			}
			if (line.getAD_Org_ID() != emp.getAD_Org_ID())
				line.setAD_Org_ID(emp.getAD_Org_ID());
			return docalc(line, emp);
		}
		
		String key = buildKey(emp.getAD_Org_ID(), m_period.getC_Year_ID(), m_period.get_ID());
		MUNSPeriodicCostBenefit benefit = m_mapBenefit.get(key);
		if (benefit == null)
		{
			benefit = MUNSPeriodicCostBenefit.getDraftOnPeriod(
					emp.getAD_Org_ID(), m_period.getC_Year_ID(), 
					m_period.get_ID(),MUNSPeriodicCostBenefit.COSTBENEFITTYPE_TunjanganHariRaya, 
					get_TrxName());
			
			if (benefit == null)
			{
				benefit = new MUNSPeriodicCostBenefit(getCtx(), 0, get_TrxName());
				benefit.setAD_Org_ID(emp.getAD_Org_ID());
				benefit.setC_Period_ID(m_period.get_ID());
				benefit.setC_Year_ID(m_period.getC_Year_ID());
				benefit.setCostBenefitType(MUNSPeriodicCostBenefit.COSTBENEFITTYPE_TunjanganHariRaya);
				MUNSPayrollConfiguration payrollConfig = MUNSPayrollConfiguration.get(
						getCtx(), m_period, emp.getAD_Org_ID(), get_TrxName(), true);
				if(payrollConfig == null)
				{
					benefit.setDateFrom(m_period.getStartDate());
					benefit.setDateTo(m_period.getEndDate());
				}
				else
				{
					benefit.setDateFrom(payrollConfig.getStartDate());
					benefit.setDateTo(payrollConfig.getEndDate());
				}
				if (!benefit.save())
					return CLogger.retrieveErrorString("Could not save benefit");
			}
			
			m_mapBenefit.put(key, benefit);
		}
		
		line = new MUNSPeriodicCostBenefitLine(benefit);
		line.setUNS_Employee_ID(emp.get_ID());
		return docalc(line, emp);
	}
	
	private String buildKey (int orgID, int yearID, int periodID)
	{
		StringBuilder sb = new StringBuilder(orgID);
		sb.append("-").append(yearID).append("-").append(periodID);
		String key = sb.toString();
		return key;
	}
}
