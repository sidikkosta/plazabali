/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.process.DocAction;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.uns.base.model.Query;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollCostBenefit;
import com.uns.model.MUNSPayrollEmployee;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author nurse
 *
 */
public class PayrollImportRecovery extends SvrProcess 
{
	
	public PayrollImportRecovery() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		//no parameter
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		trace ("Reactivate all payroll documents");
		String sql = "UPDATE UNS_Payroll_Employee SET "
				+ " DocStatus = 'DR', DocAction = 'CO', Processed = 'N'";
		int ok = DB.executeUpdate(sql, get_TrxName());
		if (ok == -1)
			throw new AdempiereException(
					CLogger.retrieveErrorString(
							"Failed when try to reactivate payroll"));
		
		trace("Load all payroll documents");
		MUNSPayrollEmployee[] payrolls = getPayRolls();
		if (!deleteTPayrollReport())
			throw new AdempiereException(
					CLogger.retrieveErrorString(
							"Failed when try to delete T Payroll Report"));
		
		for (int i=0; i<payrolls.length; i++)
		{
			trace("Processing payroll ", 
					payrolls[i].getDocumentInfo(), 
					" [", (i+1), "/", payrolls.length, "]");
			MUNSPayrollCostBenefit[] costBenefit = payrolls[i].getCostBenefits(false);
			BigDecimal benefit = Env.ZERO;
			BigDecimal deduction = Env.ZERO;
			for (int j=0; j<costBenefit.length; j++)
			{
				if (costBenefit[j].isPaidOutsidePayroll())
					continue;
				
				if (costBenefit[j].isBenefit())
					benefit = benefit.add(costBenefit[j].getAmount());
				else 
					deduction = deduction.add(costBenefit[j].getAmount());
			}
			payrolls[i].setTotalOtherAllowances(benefit);
			payrolls[i].setTotalOtherDeductions(deduction);
			payrolls[i].setGeneratePay("Y");
			payrolls[i].saveEx();
			
			MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
					getCtx(), (MPeriod)payrolls[i].getC_Period(), 
					payrolls[i].getAD_Org_ID(),
					get_TrxName(), true);
			payrolls[i].setPPH21(payConfig.getPPH(payrolls[i], false));
			payrolls[i].saveEx();
			
			boolean success = payrolls[i].processIt(DocAction.ACTION_Complete);
			if (!success)
				throw new AdempiereException(payrolls[i].getProcessMsg());
		}
		
		return payrolls.length + " record processed";
	}
	
	private boolean deleteTPayrollReport ()
	{
		String sql = "DELETE FROM T_PayrollReport";
		int ok = DB.executeUpdate(sql, get_TrxName());
		if (ok == -1)
			return false;
		sql = "DELETE FROM UNS_YearlyPayrollSummary";
		ok = DB.executeUpdate(sql, get_TrxName());
		
		return ok != -1;
	}
	
	private MUNSPayrollEmployee[] getPayRolls ()
	{
		String where = "DocStatus NOT IN (?,?)";
		List<MUNSPayrollEmployee> list = Query.get(
				getCtx(), UNSHRMModelFactory.EXTENSION_ID, 
				MUNSPayrollEmployee.Table_Name, where, 
				get_TrxName()).
				setParameters("RE","VO").
				setOrderBy(MUNSPayrollEmployee.COLUMNNAME_StartDate).
				list();
		MUNSPayrollEmployee[] pays = new MUNSPayrollEmployee[list.size()];
		list.toArray(pays);
		return pays;
	}

	private void trace (Object...args)
	{
		if (args == null)
			return;
		StringBuilder msgBuilder = new StringBuilder("");
		for (int i=0; i<args.length; i++)
		{
			if (args[i] == null)
				continue;
			msgBuilder.append(args[i].toString());
			msgBuilder.append(" ");
		}
		
		String msg = msgBuilder.toString();
		if (processUI != null)
			processUI.statusUpdate(msg);
		info(msg);
	}
	
	private void info (String msg)
	{
		if (!log.isLoggable(Level.INFO))
			return;
		log.info(msg);
	}
}
