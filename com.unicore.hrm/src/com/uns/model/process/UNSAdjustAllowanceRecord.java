/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSEmployeeAllowanceRecord;

/**
 * @author Burhani Adam
 *
 */
public class UNSAdjustAllowanceRecord extends SvrProcess {

	private int p_EmpID = 0;
	private BigDecimal p_Adjust = Env.ZERO;
	private boolean p_isMedicalAllowance = true;
	private Timestamp m_DateLastAdjust = null;
	/**
	 * 
	 */
	public UNSAdjustAllowanceRecord() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("UNS_Employee_ID"))
				p_EmpID = param.getParameterAsInt();
			else if(param.getParameterName().equals("AdjustTo"))
				p_Adjust = param.getParameterAsBigDecimal();
			else if(param.getParameterName().equals("isMedicalAllowance"))
				p_isMedicalAllowance = param.getParameterAsBoolean();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSEmployee emp = MUNSEmployee.get(getCtx(), p_EmpID);
		m_DateLastAdjust = new Timestamp (System.currentTimeMillis());
		MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.get(
				getCtx(), emp.get_ID(), new Timestamp (System.currentTimeMillis()),
					MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave, emp.getAD_Org_ID(),
					get_TrxName());
		if(record == null)
			return "Employee doesn't have record.";
		if(p_isMedicalAllowance)
		{
			record.setRemainingAmt(p_Adjust);
			record.setMedicalAllowance(record.getMedicalAllowanceUsed().add(p_Adjust));
			emp.setLastAdjustMedical(m_DateLastAdjust);
		}
		else
		{
			record.setLeaveClaimReserved((p_Adjust.add(record.getLeaveReservedUsed())).subtract(record.getPreviousBalance()));
			emp.setLastAdjustLeave(m_DateLastAdjust);
		}
		record.saveEx();
		emp.saveEx();
		return "Success";
	}
}