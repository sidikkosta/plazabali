/**
 * 
 */
package com.uns.model.process;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import com.uns.base.model.Query;
import com.uns.model.MUNSContractEvaluation;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSPayrollComponentConf;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author nurse
 *
 */
public class ImportContractFromXls extends SvrProcess 
{

	/**
	 * 
	 */
	public ImportContractFromXls() 
	{
		super ();
	}
	
	private String p_fileName = null;
	private String p_notes = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("Notes".equals(params[i].getParameterName()))
				p_notes = params[i].getParameterAsString();
			else
				log.warning("Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		File file = new File(p_fileName);
		if (!file.exists() || file.isDirectory())
			throw new AdempiereUserError("Could not find file " + p_fileName);
		Workbook book = Workbook.getWorkbook(file);
		Sheet[] sheets = book.getSheets();
		for (int i=0; i<sheets.length; i++)
		{
			importRecord(sheets[i]);
		}
		
		return "success";
	}
	
	private void importRecord (Sheet sheet)
	{
		int rows = sheet.getRows();
		int cols = sheet.getColumns();
		for (int i=2; i<rows; i++)
		{
			//ContractID
			Cell cell = sheet.getCell(1, i);
			if (cell == null)
				continue;
			String content = cell.getContents();
			if (Util.isEmpty(content, true))
				continue;
			int contractID = new Integer(content);
			
			MUNSContractRecommendation contract = new MUNSContractRecommendation(
					getCtx(), contractID, get_TrxName());
			
			//Org
			cell = sheet.getCell(4, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			int orgID = -1;
			if (!Util.isEmpty(content, true))
				orgID = DB.getSQLValue(get_TrxName(), "SELECT AD_Org_ID FROM AD_Org WHERE Value = ?", content);
			if (orgID == -1)
				orgID = contract.getAD_Org_ID ();
			
			//NIK
			cell = sheet.getCell(6, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getNewNIK();
			String nik = content;
			
			//Section Of Dept
			cell = sheet.getCell(9, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			int sectionOfDeptID = -1;
			if (!Util.isEmpty(content, true))
				sectionOfDeptID = DB.getSQLValue(get_TrxName(), "SELECT C_BPartner_ID FROM C_BPartner WHERE Value = ?", content);
			if (sectionOfDeptID == -1)
				sectionOfDeptID = contract.getNewSectionOfDept_ID();
			
			//Contract Type
			cell = sheet.getCell(11, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getNextContractType();
			String contractType = content;
			
			//Payroll Term
			cell = sheet.getCell(13, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getNextPayrollTerm();
			String payrollTerm = content;
			
			//Position
			cell = sheet.getCell(15, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			int jobID = -1;
			if (!Util.isEmpty(content, true))
				jobID = DB.getSQLValue(get_TrxName(), "SELECT C_Job_ID FROM C_Job WHERE Name = ?", content);
			if (jobID == -1)
				jobID = contract.getNewJob_ID();
			
			//payroll level
			cell = sheet.getCell(17, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getNewPayrollLevel();
			String payrollLevel = content;
			
			//shiftType
			cell = sheet.getCell(19, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getNewShift();
			String shiftType = content;
			
			//DateContractStart
			cell = sheet.getCell(22, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getDateContractStart().toString();
			Timestamp dateContractStart = Timestamp.valueOf(content);
			
			//DateCOntractEnd
			cell = sheet.getCell(24, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getDateContractEnd().toString();
			Timestamp dateContractEnd = Timestamp.valueOf(content);
			
			//PPh21 PaidByCompany
			cell = sheet.getCell(26, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.isPPH21PaidByCompany() ? "Y" : "N";
			boolean pph21PaidByCompany = "Y".equals(content);	
			
			//basic salary
			cell = sheet.getCell(28, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getNew_G_Pokok().toString();
			BigDecimal basicSalary = new BigDecimal(content);
			
			//OT Basic Amount
			cell = sheet.getCell(30, i);
			if (cell == null)
				continue;
			content = cell.getContents();
			if (Util.isEmpty(content, true))
				content = contract.getOTBasicAmt().toString();
			BigDecimal otBasicAmt = new BigDecimal(content);
			
			String recommendation = MUNSContractEvaluation.RECOMMENDATION_No_Recommendation;
			if (MUNSContractRecommendation.NEXTCONTRACTTYPE_Borongan.equals(contractType)
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganCV.equals(contractType)
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarian.equals(contractType)
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_BoronganHarianCV.equals(contractType)
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_SquenceContract.equals(contractType))
				recommendation = MUNSContractEvaluation.RECOMMENDATION_SequenceContract;
			else if (MUNSContractRecommendation.NEXTCONTRACTTYPE_Contract1.equals(contractType))
				recommendation = MUNSContractEvaluation.RECOMMENDATION_Contract1;
			else if (MUNSContractRecommendation.NEXTCONTRACTTYPE_Contract2.equals(contractType))
				recommendation = MUNSContractEvaluation.RECOMMENDATION_Contract2;
			else if (MUNSContractRecommendation.NEXTCONTRACTTYPE_Interlude.equals(contractType))
				recommendation = MUNSContractEvaluation.RECOMMENDATION_InterludeContract;
			else if (MUNSContractRecommendation.NEXTCONTRACTTYPE_Permanen.equals(contractType))
				recommendation = MUNSContractEvaluation.RECOMMENDATION_ToPermanenStatus;
			else if (MUNSContractRecommendation.NEXTCONTRACTTYPE_Recontract1.equals(contractType)
					|| MUNSContractRecommendation.NEXTCONTRACTTYPE_Recontract2.equals(contractType))
				recommendation = MUNSContractEvaluation.RECOMMENDATION_Re_Contract;
			
			MUNSContractEvaluation evaluation = Query.get(
					getCtx(), UNSHRMModelFactory.EXTENSION_ID, MUNSContractEvaluation.Table_Name, 
					MUNSContractEvaluation.COLUMNNAME_UNS_Contract_Recommendation_ID + "= ?", 
					get_TrxName()).setParameters (contractID).first();
			if (evaluation == null)
			{
				evaluation = createEvalueation(contract);
				if (evaluation == null)
					throw new AdempiereException();
			}
			try
			{
				evaluation.setRecommendation(recommendation);
				evaluation.saveEx();
				evaluation.load(get_TrxName());
				if (contract.getAD_Org_ID() != orgID)
				{
					evaluation.setIsMoveToOrg(true);
					evaluation.setRecommendation(MUNSContractEvaluation.RECOMMENDATION_No_Recommendation);
					evaluation.saveEx();
				}
				String status = evaluation.getDocStatus();
				boolean needProcess = !"CO".equals(status) && !"RE".equals(status) 
						&& !"VO".equals(status) && !"CL".equals(status);
				if (needProcess && !evaluation.processIt(DocAction.ACTION_Complete))
					throw new AdempiereException(evaluation.getProcessMsg());
				evaluation.saveEx();
			}
			catch (Exception ex)
			{
				throw new AdempiereException(ex.getMessage());
			}
			
			MUNSContractRecommendation newContract = MUNSContractRecommendation.get(getCtx(), evaluation.get_ID(), get_TrxName());
			if (newContract == null)
				continue;
			if (newContract.isProcessed())
				continue;
			
			LoadBasicPayroll basicpayrollLoader = new LoadBasicPayroll(newContract);
			basicpayrollLoader.load();
			
			newContract.setAD_Org_ID(orgID);
			if (contract.getAD_Org_ID() != orgID)
			{
				newContract.setIsMoveTo(true);
				newContract.setNewDept_ID(orgID);
			}
			newContract.setNewNIK(nik);
			newContract.setNewSectionOfDept_ID(sectionOfDeptID);
			newContract.setNextContractType(contractType);
			newContract.setNextPayrollTerm(payrollTerm);
			newContract.setNewJob_ID(jobID);
			newContract.setNewPayrollLevel(payrollLevel);
			newContract.setNewShift(shiftType);
			newContract.setDateContractStart(dateContractStart);
			newContract.setDateContractEnd(dateContractEnd);
			newContract.setPPH21PaidByCompany(pph21PaidByCompany);
			newContract.setNew_G_Pokok(basicSalary);
			newContract.setOTBasicAmt(otBasicAmt);
			
			if (!Util.isEmpty(p_notes))
				newContract.setNotes(p_notes);
			
			newContract.saveEx();
			String name = null;
			for (int j=31; j<cols;j++)
			{
				BigDecimal amount = Env.ZERO;
				if (j%2 != 0)
				{
					cell = sheet.getCell(j, 0);
					name = cell.getContents();
					if (name == null)
						name = "#######";
					continue;
				}
				else
				{
					cell = sheet.getCell(j, i);
					content = cell.getContents();
					if ("Date Approval".equals(name))
					{	
						if (Util.isEmpty(content, true))
							continue;
						newContract.set_ValueOfColumn(
								"DateApproval", Timestamp.valueOf(content));
						continue;
					}
					else if ("Effective Date".equals(name))
					{
						if (Util.isEmpty(content, true))
							continue;
						newContract.setEffectiveDate(Timestamp.valueOf(content));
						continue;
					}
					else
					{
						if (Util.isEmpty(content, true))
							content = "0";
						amount = new BigDecimal(content);
					}
				}
				
				MUNSPayrollComponentConf[] confs = newContract.getComponents(true);
				for (int k=0; k<confs.length; k++)
				{
					if (confs[k].getName().equals(name))
					{
						confs[k].setAmount(amount);
						confs[k].saveEx();
						break;
					}
				}
				name = null;
			}
			
			newContract.load(get_TrxName());
			newContract.saveEx();
		}
	}
	
	public MUNSContractEvaluation createEvalueation (MUNSContractRecommendation contract)
	{
		MUNSContractEvaluation evaluation = new MUNSContractEvaluation(getCtx(), 0, get_TrxName());
		evaluation.setAD_Org_ID(contract.getAD_Org_ID());
		evaluation.setAD_OrgTrx_ID(contract.getAD_Org_ID());
		evaluation.setDescription("###Auto Generated From Import Process###");
		evaluation.setEvalDetailRefNo("#");
		evaluation.setTotalGrade(new BigDecimal(75));
		evaluation.setGrade(MUNSContractEvaluation.GRADE_B);
		evaluation.setLastContractDate(contract.getDateContractStart());
		evaluation.setLastEndContractDate(contract.getDateContractEnd());
		evaluation.setRecommendation(MUNSContractEvaluation.RECOMMENDATION_PromotedToPosition);
		evaluation.setRemarks("#");
		evaluation.setUNS_Contract_Recommendation_ID(contract.get_ID());
		evaluation.setUNS_Employee_ID(contract.getUNS_Employee_ID());
		if (!evaluation.save())
			return null;
		
		return evaluation;
	}
}
