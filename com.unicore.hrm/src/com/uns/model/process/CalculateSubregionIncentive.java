package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.X_UNS_SubregionRecap;

public class CalculateSubregionIncentive extends SvrProcess {

	public CalculateSubregionIncentive() {
		super();
	}

	@Override
	protected String doIt() throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String sql = "SELECT SUM(netsalestarget), SUM(netsalesrealizationamt) "
				+ "FROM uns_shoprecap "
				+ "WHERE uns_subregionrecap_id = ?";
		
		pstmt = DB.prepareStatement(sql, get_TrxName());
		pstmt.setInt(1, getRecord_ID());
		rs = pstmt.executeQuery();
		rs.next();
		BigDecimal netSalesTarget = rs.getBigDecimal(1);
		BigDecimal netSalesRealizationAmount = rs.getBigDecimal(2);
		
		X_UNS_SubregionRecap recap = new X_UNS_SubregionRecap(getCtx(),
				getRecord_ID(), get_TrxName());
		recap.setNetSalesTarget(netSalesTarget);
		recap.setNetSalesRealizationAmt(netSalesRealizationAmount);
		BigDecimal netSalesIncentiveAmt = BigDecimal.ZERO;
		if (netSalesRealizationAmount.compareTo(netSalesTarget) > 0) {
			sql = "SELECT MAX(c_period_id), supervisorportion "
					+ "FROM uns_globalincentivesconfig "
					+ "WHERE c_period_id <= ? GROUP BY supervisorportion;";
			pstmt = DB.prepareStatement(sql, get_TrxName());
			pstmt.setInt(1, recap.getC_Period_ID());
			rs = pstmt.executeQuery();

			rs.next();
			BigDecimal spvPortion = rs.getBigDecimal(2);
			netSalesIncentiveAmt = spvPortion.multiply(netSalesRealizationAmount);
			recap.setNetSalesIncentiveAmt(netSalesIncentiveAmt);
		}
		
		recap.save();

		return "Subregion Incentive has been calculated";
	}

	@Override
	protected void prepare() {
		
	}

}
