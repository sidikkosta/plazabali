/**
 * 
 */
package com.uns.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.model.MDocType;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.uns.model.MUNSEmployeeLoan;

/**
 * @author Burhani Adam
 *
 */
public class ShortCashierToLoan extends SvrProcess{

	/**
	 * 
	 */
	public ShortCashierToLoan() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_AD_Org_ID = 0;
	private int p_C_BP_Group_ID = 0;
	private int p_C_BPartner_ID = 0;
	private int p_UNS_Employee_ID = 0;
	private int p_Period = 0;
	private boolean p_AutoComplete = false;
	private Timestamp p_From = null;
	private Timestamp p_To = null;
	private String p_Description = null;
	private Hashtable<Integer, MUNSEmployeeLoan> mapLoan = new Hashtable<>();

	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
				p_AD_Org_ID = params[i].getParameterAsInt();
			else if ("C_BP_Group_ID".equals(name))
				p_C_BP_Group_ID = params[i].getParameterAsInt();
			else if ("C_BPartner_ID".equals(name))
				p_C_BPartner_ID = params[i].getParameterAsInt();
			else if ("UNS_Employee_ID".equals(name))
				p_UNS_Employee_ID = params[i].getParameterAsInt();
			else if ("InstallmentPeriod".equals(name))
				p_Period = params[i].getParameterAsInt();
			else if ("AutoComplete".equals(name))
				p_AutoComplete = params[i].getParameterAsBoolean();
			else if ("DateFrom".equals(name))
				p_From = params[i].getParameterAsTimestamp();
			else if ("DateTo".equals(name))
				p_To = params[i].getParameterAsTimestamp();
			else if ("Description".equals(name))
				p_Description = params[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown parameter " + name);
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		if(p_Period <= 0)
			throw new AdempiereException("Installment period must be greater than 0");
		
		MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
		MAcctSchema as = null;
		for(int i=0;i<ass.length;i++)
		{
			if(ass[i].getC_Currency_ID() == 303)
			{
				as = ass[i];
				break;
			}
		}
		String shortCashierAcctSQL = "SELECT UNS_ShortCashier_Acct FROM C_AcctSchema_Default "
				+ " WHERE C_AcctSchema_ID=" + as.get_ID();
		
		int shortCashierAcct = DB.getSQLValueEx(get_TrxName(), shortCashierAcctSQL);
				
		int chargeID = MCharge.getOfAccount(shortCashierAcct, as);
		
		if(chargeID <= 0)
			throw new AdempiereException("Charge on payment allocate not set.");
		
		int count = 0;
		String sql = "SELECT emp.UNS_Employee_ID, sc.UNS_SessionCashAccount_ID, sc.ShortCashierAmtBase1 FROM UNS_SessionCashAccount sc"
				+ " INNER JOIN UNS_POS_Session ps ON ps.UNS_POS_Session_ID = sc.UNS_POS_Session_ID"
				+ " INNER JOIN AD_User us ON us.AD_User_ID = ps.Cashier_ID"
				+ " INNER JOIN UNS_Employee emp ON emp.UNS_Employee_ID = us.UNS_Employee_ID"
				+ " INNER JOIN UNS_POSTerminal pt ON pt.UNS_POSTerminal_ID = ps.UNS_POSTerminal_ID"
				+ " WHERE ps.TotalShortCashierAmt > 0 AND sc.ShortCashierAmtBase1 <> 0 AND sc.IsCreatedEmployeeLoan = 'N'"
				+ " AND ps.DocStatus IN ('CO', 'CL') AND ps.AD_Org_ID = ? AND (ps.DateAcct BETWEEN ? AND ?)"
				+ " AND sc.UNS_Employee_Loan_ID IS NULL AND NOT EXISTS (SELECT 1 FROM UNS_ShortCashierCorr_Line"
				+ " cl WHERE cl.UNS_POS_Session_ID = ps.UNS_POS_Session_ID AND cl.C_Currency_ID = sc.C_Currency_ID"
				+ " AND NOT EXISTS (SELECT 1 FROM UNS_ShortCashierCorrection cc WHERE cc.UNS_ShortCashierCorrection_ID"
				+ " = cl.UNS_ShortCashierCorrection_ID))";
		
		if(p_C_BP_Group_ID > 0)
			sql += " AND EXISTS (SELECT 1 FROM C_BPartner bp WHERE bp.C_BPartner_ID = pt.Store_ID"
					+ " AND bp.C_BP_Group_ID = " + p_C_BP_Group_ID +")";
		if(p_C_BPartner_ID > 0)
			sql += " AND pt.Store_ID = " + p_C_BPartner_ID;
		if(p_UNS_Employee_ID > 0)
			sql += " AND emp.UNS_Employee_ID = " + p_UNS_Employee_ID;

		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, p_AD_Org_ID);
			stmt.setTimestamp(2, p_From);
			stmt.setTimestamp(3, p_To);
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSEmployeeLoan loan = mapLoan.get(rs.getInt(1));
				if(loan == null)
				{
					loan = new MUNSEmployeeLoan(getCtx(), 0, get_TrxName());
					loan.setAD_Org_ID(p_AD_Org_ID);
					loan.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_LoanInstallment));
					loan.setLoanType("SHC");
					loan.setUNS_Employee_ID(rs.getInt(1));
					loan.setRequestDate(new Timestamp(System.currentTimeMillis()));
					loan.setTrxDate(new Timestamp(System.currentTimeMillis()));
					loan.setReason("Auto generate from short cashier to loan process.");
					loan.setLoanAmt(rs.getBigDecimal(3));
					loan.setInstallmentPeriod(p_Period);
					loan.setC_Charge_ID(chargeID);
					loan.setReason(p_Description);
					loan.saveEx();
					mapLoan.put(rs.getInt(1), loan);
				}
				else
				{
					loan.setLoanAmt(loan.getLoanAmt().add(rs.getBigDecimal(3)));
					loan.saveEx();
				}
				
				String sql2 = "UPDATE UNS_SessionCashAccount SET UNS_Employee_Loan_ID = ?, IsCreatedEmployeeLoan ='Y'"
						+ " WHERE UNS_SessionCashAccount_ID = ?";
				int count2 = DB.executeUpdate(sql2, new Object[]{loan.get_ID(), rs.getInt(2)}, false, get_TrxName());
				if(count2 < 0)
					throw new AdempiereException("Failed when trying update session cash account.");
				
				count++;
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		sql = "SELECT emp.UNS_Employee_ID, cl.UNS_ShortCashierCorr_Line_ID, cl.CorrectionAmtB1"
				+ " FROM UNS_ShortCashierCorr_Line cl"
				+ " INNER JOIN UNS_Employee emp ON emp.UNS_Employee_ID = cl.UNS_Employee_ID"
				+ " INNER JOIN UNS_ShortCashierCorrection sc ON sc.UNS_ShortCashierCorrection_ID = cl.UNS_ShortCashierCorrection_ID"
				+ " WHERE sc.DocStatus IN ('CO', 'CL') AND cl.UNS_Employee_Loan_ID IS NULL"
				+ " AND sc.AD_Org_ID = ? AND DateDoc BETWEEN ? AND ? AND cl.CorrectionAmtB1 > 0";
		
		if(p_C_BP_Group_ID > 0)
			sql += " AND sc.C_BP_Group_ID = " + p_C_BP_Group_ID;
		if(p_C_BPartner_ID > 0)
			sql += " AND cl.Store_ID = " + p_C_BPartner_ID;
		if(p_UNS_Employee_ID > 0)
			sql += " AND emp.UNS_Employee_ID = " + p_UNS_Employee_ID;
		
		PreparedStatement stmts = null;
		ResultSet rss = null;
		
		try {
			stmts = DB.prepareStatement(sql, get_TrxName());
			stmts.setInt(1, p_AD_Org_ID);
			stmts.setTimestamp(2, p_From);
			stmts.setTimestamp(3, p_To);
			rss = stmts.executeQuery();
			while(rss.next())
			{
				MUNSEmployeeLoan loan = mapLoan.get(rss.getInt(1));
				if(loan == null)
				{
					loan = new MUNSEmployeeLoan(getCtx(), 0, get_TrxName());
					loan.setAD_Org_ID(p_AD_Org_ID);
					loan.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_LoanInstallment));
					loan.setLoanType("SHC");
					loan.setUNS_Employee_ID(rss.getInt(1));
					loan.setRequestDate(new Timestamp(System.currentTimeMillis()));
					loan.setTrxDate(new Timestamp(System.currentTimeMillis()));
					loan.setReason("Auto generate from short cashier to loan process.");
					loan.setLoanAmt(rss.getBigDecimal(3));
					loan.setInstallmentPeriod(p_Period);
					loan.setC_Charge_ID(chargeID);
					loan.setReason(p_Description);
					loan.saveEx();
					mapLoan.put(rss.getInt(1), loan);
				}
				else
				{
					loan.setLoanAmt(loan.getLoanAmt().add(rss.getBigDecimal(3)));
					loan.saveEx();
				}
				
				String sql2 = "UPDATE UNS_SessionCashAccount SET UNS_Employee_Loan_ID = ?"
						+ " WHERE UNS_SessionCashAccount_ID = ?";
				int count2 = DB.executeUpdate(sql2, new Object[]{loan.get_ID(), rss.getInt(2)}, false, get_TrxName());
				if(count2 < 0)
					throw new AdempiereException("Failed when trying update session cash account.");
				
				count++;
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		if(p_AutoComplete && count > 0)
		{
			Enumeration<Integer> loopLoan = mapLoan.keys();
			
			while (loopLoan.hasMoreElements())
			{
				Integer idEmp = (Integer) loopLoan.nextElement();
				MUNSEmployeeLoan loan = mapLoan.get(idEmp);
				if(!loan.processIt("CO") || !loan.save())
					throw new AdempiereException(loan.getProcessMsg());
			}
		}
		
		return "Employee loan has created. " + count + " records.";
	}
}