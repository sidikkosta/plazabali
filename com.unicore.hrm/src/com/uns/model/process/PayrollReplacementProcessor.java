/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;

import com.uns.model.MUNSPayrollCorrectionLine;
import com.uns.model.MUNSPayrollEmployee;

/**
 * @author nurse
 *
 */
public class PayrollReplacementProcessor extends SvrProcess {

	/**
	 * 
	 */
	public PayrollReplacementProcessor() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("Recreate".equals(params[i].getParameterName()))
				m_recreate = params[i].getParameterAsBoolean();
		}
	}
	
	private boolean m_recreate = false;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSPayrollCorrectionLine model = new MUNSPayrollCorrectionLine(
				getCtx(), getRecord_ID(), get_TrxName());
		if (!MUNSPayrollCorrectionLine.CORRECTIONTYPE_PayrollReplacementCorrection.
				equals(model.getCorrectionType()))
			return null;
		MUNSPayrollEmployee payroll = model.createPayrollReplacement(m_recreate);
		if (payroll == null)
			throw new AdempiereException(CLogger.retrieveErrorString("Unknown Error"));
		
		return "Success payroll info [" + payroll.getDocumentInfo() +"]";
	}

}
