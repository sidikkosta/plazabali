/**
 * 
 */
package com.uns.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;
import com.uns.model.MUNSPeriodicCostBenefit;

/**
 * @author menjangan
 *
 */
public class ProcessPeriodCostBenefit extends SvrProcess {

	private MUNSPeriodicCostBenefit m_model;
	/**
	 * 
	 */
	public ProcessPeriodCostBenefit() {
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		m_model = new MUNSPeriodicCostBenefit(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		String msg = null;
		if ((msg = m_model.processRecord()) != null) {
			throw new AdempiereException(msg);
		}
		m_model.saveEx();
		return "Success";
	}

}
