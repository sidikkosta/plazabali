/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSEmployeeAllowanceRecord;
import com.uns.model.MUNSMonthlyPresenceSummary;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollLevelConfig;

/**
 * @author Burhani Adam
 *
 */
public class ConvertOvertimeToLeave extends SvrProcess {

	/**
	 * 
	 */
	public ConvertOvertimeToLeave() {
		// TODO Auto-generated constructor stub
	}
	
	private int m_converted = 0;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("ConvertedOvertime"))
				m_converted = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{		
		MUNSMonthlyPresenceSummary mps = new MUNSMonthlyPresenceSummary(getCtx(), getRecord_ID(), get_TrxName());
		MUNSEmployee emp = MUNSEmployee.get(getCtx(), mps.getUNS_Employee_ID());
		MUNSPayrollLevelConfig level = MUNSPayrollLevelConfig.get(getCtx(), emp.getPayrollLevel(),
				emp.getPayrollTerm(), emp.getAD_Org_ID(), true, get_TrxName());
		BigDecimal totalOT = 
				(mps.getTotalLD1().multiply(level.getAL1Multiplier()))
			.add(mps.getTotalLD2().multiply(level.getAL2Multiplier()))
			.add(mps.getTotalLD3().multiply(level.getAL3Multiplier()))
			.add(mps.getTotalLD1R().multiply(level.getALR1Multiplier()))
			.add(mps.getTotalLD2R().multiply(level.getALR2Multiplier()))
			.add(mps.getTotalLD3R().multiply(level.getALR3Multiplier()))
			.add(mps.getTotalOvertime1stHour().multiply(level.getFirstOTMultiplier()))
			.add(mps.getTotalOvertimeNextHour().multiply(level.getNextOTMultiplier()));
		
		if(totalOT.compareTo(BigDecimal.valueOf(m_converted)) == -1)
			throw new AdempiereException("Converted overtime cannot bigger than total overtime on this monthly.");
		MUNSPayrollConfiguration config = MUNSPayrollConfiguration.get(getCtx(), (MPeriod) mps.getC_Period(),
				mps.getAD_Org_ID(), get_TrxName(), true);
		boolean isNegate = false;
		if(config == null)
			throw new AdempiereException("Payroll Configuration not found..!!");
		
		//ini untuk ngecek kalau misalkan sudah pernah di konversi tapi nilainya salah atau mau di nol kan (ga jadi konversi)
		int prevConverted = 0;
		if(mps.getConvertedOvertime() > 0)
		{
			if(m_converted > 0)
				throw new AdempiereException("Please reset converted overtime with parameter filled zero,"
						+ " next re run this process again");
			else
			{
				m_converted = mps.getConvertedOvertime();
				isNegate = true;
			}
		}
		
		int converted = config.convertOTToLeave(m_converted);
		converted = converted - prevConverted;
		if(isNegate)
			converted = converted * -1;
		
		if(converted <= 0)
			mps.setConvertedOvertime(0);
		else
			mps.setConvertedOvertime(converted * config.getConvertibleOTToLeave());
		mps.saveEx();
		
		MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.get(getCtx(), mps.getUNS_Employee_ID(),
				mps.getEndDate(), MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave, mps.getAD_Org_ID(), get_TrxName());
		record.setLeaveClaimReserved(record.getLeaveClaimReserved().add(BigDecimal.valueOf(converted)));
		record.saveEx();
		
		return "Overtime has been successfully converted to " + converted +  (converted > 1 ? " days" : " day");
	}
}
