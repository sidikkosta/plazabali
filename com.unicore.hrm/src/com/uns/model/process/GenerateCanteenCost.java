package com.uns.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSCanteenActivity;
import com.uns.model.MUNSCanteenPrice;
import com.uns.model.MUNSPeriodicCostBenefit;
import com.uns.model.MUNSPeriodicCostBenefitLine;

public class GenerateCanteenCost extends SvrProcess {

	public GenerateCanteenCost() {
				
	}
	
	private MUNSPeriodicCostBenefit m_costBen;
	
	@Override
	protected void prepare() {
		
		if(getRecord_ID() > 0)
			m_costBen = new MUNSPeriodicCostBenefit(getCtx(), getRecord_ID(), get_TrxName());
		else
			throw new AdempiereException("Only can access from window");
		
		if(!m_costBen.getCostBenefitType().equals(MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Canteen))
			throw new AdempiereException("Cannot process. The type must Canteen");

	}

	@Override
	protected String doIt() throws Exception {
		
		String sql = "SELECT DISTINCT(UNS_Employee_ID) FROM UNS_CanteenActivity"
				+ " WHERE AD_Org_ID = ? AND DateTrx BETWEEN ? AND ?";
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, m_costBen.getAD_Org_ID());
			st.setTimestamp(2, m_costBen.getDateFrom());
			st.setTimestamp(3, m_costBen.getDateTo());
			rs = st.executeQuery();
			while(rs.next())
			{
				MUNSCanteenActivity[] activities = m_costBen.getActivities(rs.getInt(1), 
						m_costBen.getDateFrom(), m_costBen.getDateTo(), m_costBen.getAD_Org_ID(), false);
				BigDecimal amount = Env.ZERO;
				for(int i=0; i < activities.length; i++)
				{
					MUNSCanteenPrice price = MUNSCanteenPrice.getByDate(m_costBen.getAD_Org_ID(), activities[i].getDateTrx(), get_TrxName());
					if (activities[i].isBreakfast())
						amount = amount.add(price.getBreakfast());
					if (activities[i].isLunch())
						amount = amount.add(price.getLunch());
					if (activities[i].isDinner())
						amount = amount.add(price.getDinner());
				}
				if (amount.signum() == 0)
					continue;
				
				createUpdateBenefitLine(rs.getInt(1), amount);
			}
			
		} catch (SQLException e) {
			return e.getMessage();
		}
		finally {
			DB.close(rs, st);
		}
		
		return null;
	}
	
	public void createUpdateBenefitLine(int employeeID, BigDecimal amount) {
		
		MUNSPeriodicCostBenefitLine line;
		
		String sql = "SELECT UNS_PeriodicCostBenefitLine_ID FROM UNS_PeriodicCostBenefitLine"
				+ " WHERE UNS_PeriodicCostBenefit_ID = ? AND UNS_Employee_ID = ? ";
		int benefitLine_ID = DB.getSQLValue(get_TrxName(), sql, m_costBen.get_ID(), employeeID);
		if(benefitLine_ID <= 0)
		{
			line = new MUNSPeriodicCostBenefitLine(m_costBen);
			line.setUNS_Employee_ID(employeeID);
		}
		else
		{
			 line = new MUNSPeriodicCostBenefitLine(getCtx(), benefitLine_ID, get_TrxName());
		}
		
		line.setAmount(amount);
		line.setRemainingAmount(Env.ZERO);
		line.setPaidAmt(Env.ZERO);
		if (!line.save()) {
			throw new AdempiereException("Could not save Periodic Cost Benefit Line");
		}
	}
}
