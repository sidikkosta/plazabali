package com.uns.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;

import com.uns.model.MUNSEmpStation;
import com.uns.model.MUNSStationSlotType;

public class AddOrChangeStationSlot extends SvrProcess {

	private int p_slotTypeID = 0;
	private Timestamp p_ValidFrom = null;
	private String p_desc = null;
	
	private MUNSEmpStation m_empStation = null;
	
	
	public AddOrChangeStationSlot() {
		
	}

	@Override
	protected void prepare() {
		
		for(ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if(name.equals("UNS_SlotType_ID"))
				p_slotTypeID = para.getParameterAsInt();
			else if(name.equals("ValidFrom"))
				p_ValidFrom = para.getParameterAsTimestamp();
			else if(name.equals("Description"))
				p_desc = para.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown parameter "+name);
		}
		
		m_empStation = new MUNSEmpStation(getCtx(), getRecord_ID(), get_TrxName());

	}

	@Override
	protected String doIt() throws Exception {
		
		if(p_slotTypeID == 0 || p_ValidFrom == null)
			throw new AdempiereException("Mandatory all parameter");
		
//		String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//		Timestamp now = Timestamp.valueOf(dateStr+" 00:00:00");
		Timestamp validTo = null;
		
//		if(now.equals(p_ValidFrom) || now.after(p_ValidFrom))
//			throw new AdempiereException("Cannot process. You should fill ValidFrom with date before todate");
		
		MUNSStationSlotType prevSlotStation = null;
		MUNSStationSlotType nextSlotStation = null;
		String sql = "SELECT UNS_StationSlotType_ID FROM UNS_StationSlotType WHERE UNS_EmpStation_ID = ?"
				+ " AND ValidFrom <= ? ORDER By ValidFrom DESC";
		int befSlotID = DB.getSQLValue(get_TrxName(), sql, m_empStation.getUNS_EmpStation_ID(), p_ValidFrom);
		if(befSlotID > 0)
		{
			prevSlotStation = new MUNSStationSlotType(getCtx(), befSlotID, get_TrxName());
			if(prevSlotStation.getValidTo() != null)
			{
				Timestamp befValidTo = prevSlotStation.getValidTo();
				
				String sqql = "SELECT UNS_StationSlotType_ID FROM UNS_StationSlotType WHERE UNS_EmpStation_ID = ?"
						+ " AND ValidFrom > ? ORDER By ValidFrom ASC";
				int afSlotID = DB.getSQLValue(get_TrxName(), sqql, m_empStation.getUNS_EmpStation_ID(), befValidTo);
				if(afSlotID > 0)
					nextSlotStation = new MUNSStationSlotType(getCtx(), afSlotID, get_TrxName());
			}
			
			if(prevSlotStation.getValidFrom().equals(p_ValidFrom))
				throw new AdempiereException("Valid from is same with Valid from of other Station slot type");
			else if(p_ValidFrom.equals(prevSlotStation.getValidTo()))
				throw new AdempiereException("Valid from is same with Valid to of other Station slot type");
			
			if(nextSlotStation != null)
			{
				validTo = TimeUtil.addDays(nextSlotStation.getValidFrom(), -1);
			}	
			
		}
		
		MUNSStationSlotType slotStation = createSlot();
		if(slotStation == null)
			throw new AdempiereException("Error when create slot station");
		
		slotStation.setValidTo(validTo);
		slotStation.saveEx();
		
		if(prevSlotStation != null)
		{
			prevSlotStation.setValidTo(TimeUtil.addDays(p_ValidFrom, -1));
			prevSlotStation.saveEx();
		}
		
		return "success";
	}
	
	private MUNSStationSlotType createSlot() {
		
		MUNSStationSlotType slot = new MUNSStationSlotType(getCtx(), 0, get_TrxName());
		slot.setAD_Org_ID(m_empStation.getAD_Org_ID());
		slot.setUNS_EmpStation_ID(m_empStation.getUNS_EmpStation_ID());
		slot.setUNS_SlotType_ID(p_slotTypeID);
		slot.setValidFrom(p_ValidFrom);
		slot.setDescription(p_desc);
		
		return slot;
	}
}
