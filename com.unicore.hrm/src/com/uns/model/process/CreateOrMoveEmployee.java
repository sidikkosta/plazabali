package com.uns.model.process;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;

import com.uns.model.MUNSEmpStation;
import com.uns.model.MUNSEmpStationLine;

public class CreateOrMoveEmployee extends SvrProcess {

	private int p_employeeID = 0;
	private Timestamp p_validFrom = null;
	private int p_stationTo = 0;
	private boolean p_isMove = false;
	
	private MUNSEmpStation m_empStation = null;
	
	public CreateOrMoveEmployee() {

	}

	@Override
	protected void prepare() {
		
		for(ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if(name.equals("UNS_Employee_ID"))
				p_employeeID = para.getParameterAsInt();
			else if(name.equals("ValidFrom"))
				p_validFrom = para.getParameterAsTimestamp();
			else if(name.equals("EmpStationTo_ID"))
				p_stationTo = para.getParameterAsInt();
			else if(name.equals("isMove"))
				p_isMove = para.getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter "+name);
		}
		
		if(getRecord_ID() > 0)
			m_empStation = new MUNSEmpStation(getCtx(), getRecord_ID(), get_TrxName());
		else
			{
			m_empStation = MUNSEmpStation.getbyDate(getCtx(), p_employeeID, p_validFrom, get_TrxName());
			if(m_empStation == null)
				throw new AdempiereException("No current station for this employee");
			}
	}

	@Override
	protected String doIt() throws Exception {
		
		if(p_employeeID <= 0 || p_validFrom == null || (p_isMove && p_stationTo == 0))
			throw new AdempiereException("Mandatory all parameter");
		
		if(m_empStation.getUNS_EmpStation_ID() == p_stationTo)
			throw new AdempiereException("You cannot move employee to station which the same as before");
		
		if(dateNow().equals(p_validFrom))
			throw new AdempiereException("Cannot process. You should fill ValidFrom with date before todate");
		
		if(getRecord_ID() > 0)
		{
			MUNSEmpStation currentStation = MUNSEmpStation.getbyDate(
					getCtx(), p_employeeID, dateNow() , get_TrxName());
			if(currentStation != null)
			{
				if(currentStation.getUNS_EmpStation_ID() != m_empStation.getUNS_EmpStation_ID())
					throw new AdempiereException("You should move on current station");
			}
		}
		
		if(!p_isMove)
		{
			String sql = "SELECT 1 FROM UNS_EmpStationLine WHERE UNS_EmpStation_ID = ? AND UNS_Employee_ID = ? AND isActive = ?";
			boolean exists = DB.getSQLValue(get_TrxName(), sql, m_empStation.getUNS_EmpStation_ID(), p_employeeID, "Y") > 0;
			if(exists)
				throw new AdempiereException("Cannot Add, the employee has added. Please move from his/her current station.");
		}
		
		Timestamp validTo = null;
		
		MUNSEmpStationLine prevLine = null;
		
		//check previous Line and get ValidTo	
		String sql = "SELECT UNS_EmpStationLine_ID FROM UNS_EmpStationLine WHERE "
				+ " UNS_Employee_ID = ? AND ValidFrom <= ? AND isActive = ?  ORDER BY ValidFrom DESC";
		int id = DB.getSQLValue(
				get_TrxName(), sql.toString(), p_employeeID, p_validFrom, "Y");
		if(id > 0)
		{
			prevLine = new MUNSEmpStationLine(getCtx(), id, get_TrxName());
			if(prevLine.getValidFrom().equals(p_validFrom))
				throw new AdempiereException("Valid from is same with Valid from of other employee station");
			
			if(prevLine.getValidTo() != null)
			{
				if(prevLine.getValidTo().equals(p_validFrom))
					throw new AdempiereException("Valid from is same with Valid to of other employee station");
				
				sql = "SELECT ValidFrom From UNS_EmpStationLine WHERE"
					+ " UNS_Employee_ID = ? AND ValidFrom > ? AND isActive = ? ORDER BY ValidFrom ASC ";
				Timestamp nextValidFrom = DB.getSQLValueTS(
						get_TrxName(), sql, p_employeeID, prevLine.getValidTo(), "Y");
				if(nextValidFrom != null)
					validTo = TimeUtil.addDays(nextValidFrom, -1);
			}
			
		}
		
		MUNSEmpStationLine toLine = new MUNSEmpStationLine(getCtx(), 0, get_TrxName());
		
		toLine.setAD_Org_ID(m_empStation.getAD_Org_ID());
		toLine.setUNS_EmpStation_ID(p_stationTo);
		toLine.setUNS_Employee_ID(p_employeeID);
		toLine.setValidFrom(p_validFrom);
		toLine.setValidTo(validTo);
		toLine.saveEx();
		
		if(prevLine != null)
		{
			prevLine.setValidTo(TimeUtil.addDays(p_validFrom, -1));
			prevLine.saveEx();
		}
		
		return "Success";
	}
	
	public Timestamp dateNow() {
		
		String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();
		
		return Timestamp.valueOf(dateStr+" 00:00:00");
	}
}
