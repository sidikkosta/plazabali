/**
 * 
 */
package com.uns.model.process;

import java.math.BigDecimal;

import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.uns.model.MUNSDailyPresence;
import com.uns.model.MUNSMonthlyPresenceSummary;

/**
 * @author Burhani Adam
 *
 */
public class AdjustCalculationOT extends SvrProcess {

	/**
	 * 
	 */
	public AdjustCalculationOT() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSDailyPresence day = new MUNSDailyPresence(getCtx(), getRecord_ID(), get_TrxName());
		MUNSMonthlyPresenceSummary mps = new MUNSMonthlyPresenceSummary(getCtx(), 
				day.getUNS_MonthlyPresenceSummary_ID(), get_TrxName());
		
		if(mps.isProcessed())
			return "Process cancelled, monthly has processed.";
		
		BigDecimal nol = Env.ZERO;
		if(day.getLD1().signum() != 0)
		{
			day.setLD1R(day.getLD1());
			day.setLD2R(day.getLD2());
			day.setLD3R(day.getLD3());
			day.setLD1(nol);
			day.setLD2(nol);
			day.setLD3(nol);
			day.setIsManualUpdate(true);
			day.setRunValidator(false);
			day.saveEx();
			MUNSMonthlyPresenceSummary.updateSummary(mps, false);
		}
		else if(day.getLD1R().signum() != 0)
		{
			day.setLD1(day.getLD1R());
			day.setLD2(day.getLD2R());
			day.setLD3(day.getLD3R());
			day.setLD1R(nol);
			day.setLD2R(nol);
			day.setLD3R(nol);
			day.setIsManualUpdate(true);
			day.setRunValidator(false);
			day.saveEx();
			MUNSMonthlyPresenceSummary.updateSummary(mps, false);
		}
		return "Success";
	}
}