/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartner;
import org.compiere.model.MBankAccount;
import org.compiere.model.MCharge;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.uns.model.MUNSMedicalRecord;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSReimbursement;

/**
 * @author Burhani Adam
 *
 */
public class ReimbursementCreatePayment extends SvrProcess {

	/**
	 * 
	 */
	public ReimbursementCreatePayment() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_OrgID = 0;
	private int p_BankAccountID = 0;
	private int p_ReimbursementID = 0;
	private Timestamp p_dateTrx = null;
	private boolean p_AutoComplete = false;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("AD_Org_ID".equals(name))
			{
				p_OrgID = params[i].getParameterAsInt();
			}
			else if ("UNS_Reimbursement_ID".equals(name))
			{
				p_ReimbursementID = params[i].getParameterAsInt();
			}
			else if ("C_BankAccount_ID".equals(name))
			{
				p_BankAccountID = params[i].getParameterAsInt();
			}
			else if ("DateAcct".equals(name))
			{
				p_dateTrx = params[i].getParameterAsTimestamp();
			}
			else if ("AutoComplete".equals(name))
			{
				p_AutoComplete = params[i].getParameterAsBoolean();
			}
			else
			{
				log.log(Level.SEVERE, "Unknown parameter " + name);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MBankAccount acct = MBankAccount.get(getCtx(), p_BankAccountID);
		MUNSReimbursement reimburs = new MUNSReimbursement(getCtx(), p_ReimbursementID, get_TrxName());
		
		if(acct.getAD_Org_ID() != reimburs.getAD_Org_ID())
			throw new AdempiereException("Organization (Account <> Reimbursement).");
		
		if(reimburs.getC_Payment_ID() > 0)
			return "Payment has created. " + reimburs.getC_Payment().getDocumentNo();
		
		MUNSPayrollConfiguration config = MUNSPayrollConfiguration.get(getCtx(), p_dateTrx, p_OrgID, get_TrxName(), true);
		MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
		MAcctSchema as = null;
		for(int i=0;i<ass.length;i++)
		{
			if(ass[i].getC_Currency_ID() == 303)
			{
				as = ass[i];
				break;
			}
		}
		int chargeID = MCharge.getOfAccount(config.getE_Medical_Expense_Acct(), as);
		if(chargeID <= 0)
			throw new AdempiereException("Charge on payment allocate not set.");
		
		MPayment pay = new MPayment(getCtx(), 0, get_TrxName());
		pay.setAD_Org_ID(p_OrgID);
		pay.setC_BankAccount_ID(acct.get_ID());
		pay.setDateAcct(p_dateTrx);
		pay.setDateTrx(p_dateTrx);
		pay.setC_BPartner_ID(MBPartner.get(getCtx(), "HRD").get_ID());
		pay.setC_DocType_ID(false);
		pay.setTenderType(MPayment.TENDERTYPE_Cash);
		pay.setDescription("Medical reimbursement no " + reimburs.getDocumentNo());
		pay.setC_Currency_ID(303);
		pay.setPayAmt(reimburs.getSubjectCosts());
		pay.setReferenceNo(reimburs.getDocumentNo());
		pay.saveEx();
		
		MUNSMedicalRecord[] records = reimburs.getLines();
		
		for(int i=0;i<records.length;i++)
		{
			MPaymentAllocate allocate = MPaymentAllocate.getOfPartner(getCtx(), pay.get_ID(),
					records[i].getC_BPartner_ID(), get_TrxName());
			
			if(allocate == null)
			{
				allocate = new MPaymentAllocate(getCtx(), 0, get_TrxName());
				allocate.setC_Payment_ID(pay.get_ID());
				allocate.setAD_Org_ID(pay.getAD_Org_ID());
				allocate.setC_BPartner_ID(records[i].getC_BPartner_ID());
//				allocate.setInvoiceAmt(records[i].getMedicalCosts());
				allocate.setPayToOverUnderAmount(records[i].getMedicalCosts());
				allocate.setAmount(records[i].getMedicalCosts());
				allocate.setC_Charge_ID(chargeID);
			}
			else
				allocate.setAmount(allocate.getAmount().add(records[i].getMedicalCosts()));
			
			allocate.saveEx();
		}
		
		if(p_AutoComplete)
		{
			if(!pay.processIt("CO") || !pay.save())
				throw new AdempiereException("Failed when trying complete payment. " + pay.getProcessMsg());
		}
		
		reimburs.setC_Payment_ID(pay.get_ID());
		reimburs.saveEx();
		
		return "Payment has created. " + pay.getDocumentNo();
	}
}