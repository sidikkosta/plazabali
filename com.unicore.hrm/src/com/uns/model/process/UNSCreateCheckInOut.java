/**
 * 
 */
package com.uns.model.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Util;

import com.uns.model.MUNSCheckInOut;
import com.uns.model.MUNSEmployee;

/**
 * @author Burhani Adam
 *
 */
public class UNSCreateCheckInOut extends SvrProcess {

	/**
	 * 
	 */
	public UNSCreateCheckInOut() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_EmpID = 0;
	private Timestamp p_CheckTime = null;
	private String p_Description = "";

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		{
			for(ProcessInfoParameter param : params)
			{
				if(param.getParameterName().equals("UNS_Employee_ID"))
					p_EmpID = param.getParameterAsInt();
				else if(param.getParameterName().equals("CheckTime"))
					p_CheckTime = param.getParameterAsTimestamp();
				else if(param.getParameterName().equals("Description"))
					p_Description = param.getParameterAsString();
			}
		}
		if(p_EmpID < 0 || p_CheckTime == null)
		{
			throw new AdempiereException("Mandatory Employee / Check Time.");
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSEmployee emp = MUNSEmployee.get(getCtx(), p_EmpID);
		String whereClause = "AttendanceName = ? AND CheckTime = ?";
		List<Object> params = new ArrayList<>();
		params.add(emp.getAttendanceName());
		params.add(p_CheckTime);
		MUNSCheckInOut[] io = MUNSCheckInOut.gets(getCtx(), get_TrxName(), whereClause, params, null);
		if(io.length > 0)
			return "Success";
		else
		{
			MUNSCheckInOut newIO = new MUNSCheckInOut(emp, p_CheckTime);
			if(!Util.isEmpty(p_Description, true))
				newIO.setMemoInfo(p_Description);
			newIO.setNeedRunUNSHRMValidator(false);
			newIO.saveEx();
		}
		
		return "Success.";
	}
}