/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_PayrollCorrection
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_PayrollCorrection 
{

    /** TableName=UNS_PayrollCorrection */
    public static final String Table_Name = "UNS_PayrollCorrection";

    /** AD_Table_ID=1000365 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Amount */
    public static final String COLUMNNAME_Amount = "Amount";

	/** Set Amount Periodic.
	  * Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount);

	/** Get Amount Periodic.
	  * Amount in a defined currency
	  */
	public BigDecimal getAmount();

    /** Column name AmountNonPeriodic */
    public static final String COLUMNNAME_AmountNonPeriodic = "AmountNonPeriodic";

	/** Set Amount Non Periodic	  */
	public void setAmountNonPeriodic (BigDecimal AmountNonPeriodic);

	/** Get Amount Non Periodic	  */
	public BigDecimal getAmountNonPeriodic();

    /** Column name AmountPPh21 */
    public static final String COLUMNNAME_AmountPPh21 = "AmountPPh21";

	/** Set Amount (PPh 21) Periodic	  */
	public void setAmountPPh21 (BigDecimal AmountPPh21);

	/** Get Amount (PPh 21) Periodic	  */
	public BigDecimal getAmountPPh21();

    /** Column name AmountPPh21NonPeriodic */
    public static final String COLUMNNAME_AmountPPh21NonPeriodic = "AmountPPh21NonPeriodic";

	/** Set Amount (PPh 21) Non Periodic	  */
	public void setAmountPPh21NonPeriodic (BigDecimal AmountPPh21NonPeriodic);

	/** Get Amount (PPh 21) Non Periodic	  */
	public BigDecimal getAmountPPh21NonPeriodic();

    /** Column name AttendanceName */
    public static final String COLUMNNAME_AttendanceName = "AttendanceName";

	/** Set Attendance Name	  */
	public void setAttendanceName (String AttendanceName);

	/** Get Attendance Name	  */
	public String getAttendanceName();

    /** Column name Balance */
    public static final String COLUMNNAME_Balance = "Balance";

	/** Set Balance Periodic	  */
	public void setBalance (BigDecimal Balance);

	/** Get Balance Periodic	  */
	public BigDecimal getBalance();

    /** Column name BalanceNonPeriodic */
    public static final String COLUMNNAME_BalanceNonPeriodic = "BalanceNonPeriodic";

	/** Set Balance Non Periodic	  */
	public void setBalanceNonPeriodic (BigDecimal BalanceNonPeriodic);

	/** Get Balance Non Periodic	  */
	public BigDecimal getBalanceNonPeriodic();

    /** Column name BalancePPh21 */
    public static final String COLUMNNAME_BalancePPh21 = "BalancePPh21";

	/** Set Balance (PPh 21) Periodic	  */
	public void setBalancePPh21 (BigDecimal BalancePPh21);

	/** Get Balance (PPh 21) Periodic	  */
	public BigDecimal getBalancePPh21();

    /** Column name BalancePPh21NonPeriodic */
    public static final String COLUMNNAME_BalancePPh21NonPeriodic = "BalancePPh21NonPeriodic";

	/** Set Balance (PPh 21) Non Periodic	  */
	public void setBalancePPh21NonPeriodic (BigDecimal BalancePPh21NonPeriodic);

	/** Get Balance (PPh 21) Non Periodic	  */
	public BigDecimal getBalancePPh21NonPeriodic();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Period_ID */
    public static final String COLUMNNAME_C_Period_ID = "C_Period_ID";

	/** Set Period.
	  * Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID);

	/** Get Period.
	  * Period of the Calendar
	  */
	public int getC_Period_ID();

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name PaidAmt */
    public static final String COLUMNNAME_PaidAmt = "PaidAmt";

	/** Set Paid Amt Periodic	  */
	public void setPaidAmt (BigDecimal PaidAmt);

	/** Get Paid Amt Periodic	  */
	public BigDecimal getPaidAmt();

    /** Column name PaidAmtNonPeriodic */
    public static final String COLUMNNAME_PaidAmtNonPeriodic = "PaidAmtNonPeriodic";

	/** Set Paid Amt Non Periodic	  */
	public void setPaidAmtNonPeriodic (BigDecimal PaidAmtNonPeriodic);

	/** Get Paid Amt Non Periodic	  */
	public BigDecimal getPaidAmtNonPeriodic();

    /** Column name PaidAmtPPh21 */
    public static final String COLUMNNAME_PaidAmtPPh21 = "PaidAmtPPh21";

	/** Set Paid Amt (PPh21) Periodic	  */
	public void setPaidAmtPPh21 (BigDecimal PaidAmtPPh21);

	/** Get Paid Amt (PPh21) Periodic	  */
	public BigDecimal getPaidAmtPPh21();

    /** Column name PaidAmtPPh21NonPeriodic */
    public static final String COLUMNNAME_PaidAmtPPh21NonPeriodic = "PaidAmtPPh21NonPeriodic";

	/** Set Paid Amount (PPh21) Non Periodic	  */
	public void setPaidAmtPPh21NonPeriodic (BigDecimal PaidAmtPPh21NonPeriodic);

	/** Get Paid Amount (PPh21) Non Periodic	  */
	public BigDecimal getPaidAmtPPh21NonPeriodic();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name UNS_PayrollCorrection_ID */
    public static final String COLUMNNAME_UNS_PayrollCorrection_ID = "UNS_PayrollCorrection_ID";

	/** Set Payroll Correction	  */
	public void setUNS_PayrollCorrection_ID (int UNS_PayrollCorrection_ID);

	/** Get Payroll Correction	  */
	public int getUNS_PayrollCorrection_ID();

    /** Column name UNS_PayrollCorrection_UU */
    public static final String COLUMNNAME_UNS_PayrollCorrection_UU = "UNS_PayrollCorrection_UU";

	/** Set UNS_PayrollCorrection_UU	  */
	public void setUNS_PayrollCorrection_UU (String UNS_PayrollCorrection_UU);

	/** Get UNS_PayrollCorrection_UU	  */
	public String getUNS_PayrollCorrection_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
