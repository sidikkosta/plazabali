/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_OTLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_OTLine 
{

    /** TableName=UNS_OTLine */
    public static final String Table_Name = "UNS_OTLine";

    /** AD_Table_ID=1000472 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsSummary */
    public static final String COLUMNNAME_IsSummary = "IsSummary";

	/** Set Summary Level.
	  * This is a summary entity
	  */
	public void setIsSummary (boolean IsSummary);

	/** Get Summary Level.
	  * This is a summary entity
	  */
	public boolean isSummary();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name UNS_OTConfirmation_ID */
    public static final String COLUMNNAME_UNS_OTConfirmation_ID = "UNS_OTConfirmation_ID";

	/** Set UNS_OTConfirmation_ID	  */
	public void setUNS_OTConfirmation_ID (int UNS_OTConfirmation_ID);

	/** Get UNS_OTConfirmation_ID	  */
	public int getUNS_OTConfirmation_ID();

	public com.uns.model.I_UNS_OTConfirmation getUNS_OTConfirmation() throws RuntimeException;

    /** Column name UNS_OTGroupConfirm_ID */
    public static final String COLUMNNAME_UNS_OTGroupConfirm_ID = "UNS_OTGroupConfirm_ID";

	/** Set Overtime Group Confirmation	  */
	public void setUNS_OTGroupConfirm_ID (int UNS_OTGroupConfirm_ID);

	/** Get Overtime Group Confirmation	  */
	public int getUNS_OTGroupConfirm_ID();

	public com.uns.model.I_UNS_OTGroupConfirm getUNS_OTGroupConfirm() throws RuntimeException;

    /** Column name UNS_OTGroupRequest_ID */
    public static final String COLUMNNAME_UNS_OTGroupRequest_ID = "UNS_OTGroupRequest_ID";

	/** Set Overtime Group Request	  */
	public void setUNS_OTGroupRequest_ID (int UNS_OTGroupRequest_ID);

	/** Get Overtime Group Request	  */
	public int getUNS_OTGroupRequest_ID();

	public com.uns.model.I_UNS_OTGroupRequest getUNS_OTGroupRequest() throws RuntimeException;

    /** Column name UNS_OTLine_ID */
    public static final String COLUMNNAME_UNS_OTLine_ID = "UNS_OTLine_ID";

	/** Set Over Time Line	  */
	public void setUNS_OTLine_ID (int UNS_OTLine_ID);

	/** Get Over Time Line	  */
	public int getUNS_OTLine_ID();

    /** Column name UNS_OTLine_UU */
    public static final String COLUMNNAME_UNS_OTLine_UU = "UNS_OTLine_UU";

	/** Set UNS_OTLine_UU	  */
	public void setUNS_OTLine_UU (String UNS_OTLine_UU);

	/** Get UNS_OTLine_UU	  */
	public String getUNS_OTLine_UU();

    /** Column name UNS_OTRequest_ID */
    public static final String COLUMNNAME_UNS_OTRequest_ID = "UNS_OTRequest_ID";

	/** Set UNS_OTRequest_ID	  */
	public void setUNS_OTRequest_ID (int UNS_OTRequest_ID);

	/** Get UNS_OTRequest_ID	  */
	public int getUNS_OTRequest_ID();

	public com.uns.model.I_UNS_OTRequest getUNS_OTRequest() throws RuntimeException;

    /** Column name UNS_Resource_ID */
    public static final String COLUMNNAME_UNS_Resource_ID = "UNS_Resource_ID";

	/** Set Manufacture Resource	  */
	public void setUNS_Resource_ID (int UNS_Resource_ID);

	/** Get Manufacture Resource	  */
	public int getUNS_Resource_ID();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
