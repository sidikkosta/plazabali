/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_UpdateContractHistory
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_UpdateContractHistory extends PO implements I_UNS_UpdateContractHistory, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180725L;

    /** Standard Constructor */
    public X_UNS_UpdateContractHistory (Properties ctx, int UNS_UpdateContractHistory_ID, String trxName)
    {
      super (ctx, UNS_UpdateContractHistory_ID, trxName);
      /** if (UNS_UpdateContractHistory_ID == 0)
        {
			setUNS_Contract_Recommendation_ID (0);
			setUNS_Employee_ID (0);
			setUNS_UpdateContractHistory_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UpdateContractHistory (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UpdateContractHistory[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set AL1 Multiplier.
		@param AL1Multiplier AL1 Multiplier	  */
	public void setAL1Multiplier (BigDecimal AL1Multiplier)
	{
		set_Value (COLUMNNAME_AL1Multiplier, AL1Multiplier);
	}

	/** Get AL1 Multiplier.
		@return AL1 Multiplier	  */
	public BigDecimal getAL1Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AL1Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set AL2 Multiplier.
		@param AL2Multiplier AL2 Multiplier	  */
	public void setAL2Multiplier (BigDecimal AL2Multiplier)
	{
		set_Value (COLUMNNAME_AL2Multiplier, AL2Multiplier);
	}

	/** Get AL2 Multiplier.
		@return AL2 Multiplier	  */
	public BigDecimal getAL2Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AL2Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set AL3 Multiplier.
		@param AL3Multiplier AL3 Multiplier	  */
	public void setAL3Multiplier (BigDecimal AL3Multiplier)
	{
		set_Value (COLUMNNAME_AL3Multiplier, AL3Multiplier);
	}

	/** Get AL3 Multiplier.
		@return AL3 Multiplier	  */
	public BigDecimal getAL3Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AL3Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ALR1 Multiplier.
		@param ALR1Multiplier ALR1 Multiplier	  */
	public void setALR1Multiplier (BigDecimal ALR1Multiplier)
	{
		set_Value (COLUMNNAME_ALR1Multiplier, ALR1Multiplier);
	}

	/** Get ALR1 Multiplier.
		@return ALR1 Multiplier	  */
	public BigDecimal getALR1Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ALR1Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ALR2 Multiplier.
		@param ALR2Multiplier ALR2 Multiplier	  */
	public void setALR2Multiplier (BigDecimal ALR2Multiplier)
	{
		set_Value (COLUMNNAME_ALR2Multiplier, ALR2Multiplier);
	}

	/** Get ALR2 Multiplier.
		@return ALR2 Multiplier	  */
	public BigDecimal getALR2Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ALR2Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ALR3 Multiplier.
		@param ALR3Multiplier ALR3 Multiplier	  */
	public void setALR3Multiplier (BigDecimal ALR3Multiplier)
	{
		set_Value (COLUMNNAME_ALR3Multiplier, ALR3Multiplier);
	}

	/** Get ALR3 Multiplier.
		@return ALR3 Multiplier	  */
	public BigDecimal getALR3Multiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ALR3Multiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set First OT Multiplier.
		@param FirstOTMultiplier First OT Multiplier	  */
	public void setFirstOTMultiplier (BigDecimal FirstOTMultiplier)
	{
		set_Value (COLUMNNAME_FirstOTMultiplier, FirstOTMultiplier);
	}

	/** Get First OT Multiplier.
		@return First OT Multiplier	  */
	public BigDecimal getFirstOTMultiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FirstOTMultiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L1.
		@param New_A_L1 New A L1	  */
	public void setNew_A_L1 (BigDecimal New_A_L1)
	{
		set_Value (COLUMNNAME_New_A_L1, New_A_L1);
	}

	/** Get New A L1.
		@return New A L1	  */
	public BigDecimal getNew_A_L1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L1 R.
		@param New_A_L1_R New A L1 R	  */
	public void setNew_A_L1_R (BigDecimal New_A_L1_R)
	{
		set_Value (COLUMNNAME_New_A_L1_R, New_A_L1_R);
	}

	/** Get New A L1 R.
		@return New A L1 R	  */
	public BigDecimal getNew_A_L1_R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L1_R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L2.
		@param New_A_L2 New A L2	  */
	public void setNew_A_L2 (BigDecimal New_A_L2)
	{
		set_Value (COLUMNNAME_New_A_L2, New_A_L2);
	}

	/** Get New A L2.
		@return New A L2	  */
	public BigDecimal getNew_A_L2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L2 R.
		@param New_A_L2_R 
		Amount New_A_L2_R
	  */
	public void setNew_A_L2_R (BigDecimal New_A_L2_R)
	{
		set_Value (COLUMNNAME_New_A_L2_R, New_A_L2_R);
	}

	/** Get New A L2 R.
		@return Amount New_A_L2_R
	  */
	public BigDecimal getNew_A_L2_R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L2_R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L3.
		@param New_A_L3 New A L3	  */
	public void setNew_A_L3 (BigDecimal New_A_L3)
	{
		set_Value (COLUMNNAME_New_A_L3, New_A_L3);
	}

	/** Get New A L3.
		@return New A L3	  */
	public BigDecimal getNew_A_L3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New A L3 R.
		@param New_A_L3_R New A L3 R	  */
	public void setNew_A_L3_R (BigDecimal New_A_L3_R)
	{
		set_Value (COLUMNNAME_New_A_L3_R, New_A_L3_R);
	}

	/** Get New A L3 R.
		@return New A L3 R	  */
	public BigDecimal getNew_A_L3_R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_New_A_L3_R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New Lembur Jam Berikutnya.
		@param NewLeburJamBerikutnya New Lembur Jam Berikutnya	  */
	public void setNewLeburJamBerikutnya (BigDecimal NewLeburJamBerikutnya)
	{
		set_Value (COLUMNNAME_NewLeburJamBerikutnya, NewLeburJamBerikutnya);
	}

	/** Get New Lembur Jam Berikutnya.
		@return New Lembur Jam Berikutnya	  */
	public BigDecimal getNewLeburJamBerikutnya () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NewLeburJamBerikutnya);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set New Lembur Jam Pertama.
		@param NewLeburJamPertama New Lembur Jam Pertama	  */
	public void setNewLeburJamPertama (BigDecimal NewLeburJamPertama)
	{
		set_Value (COLUMNNAME_NewLeburJamPertama, NewLeburJamPertama);
	}

	/** Get New Lembur Jam Pertama.
		@return New Lembur Jam Pertama	  */
	public BigDecimal getNewLeburJamPertama () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NewLeburJamPertama);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Next OT Multiplier.
		@param NextOTMultiplier Next OT Multiplier	  */
	public void setNextOTMultiplier (BigDecimal NextOTMultiplier)
	{
		set_Value (COLUMNNAME_NextOTMultiplier, NextOTMultiplier);
	}

	/** Get Next OT Multiplier.
		@return Next OT Multiplier	  */
	public BigDecimal getNextOTMultiplier () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NextOTMultiplier);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set OT Basic Amount.
		@param OTBasicAmt OT Basic Amount	  */
	public void setOTBasicAmt (BigDecimal OTBasicAmt)
	{
		set_Value (COLUMNNAME_OTBasicAmt, OTBasicAmt);
	}

	/** Get OT Basic Amount.
		@return OT Basic Amount	  */
	public BigDecimal getOTBasicAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OTBasicAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_UNS_Contract_Recommendation getUNS_Contract_Recommendation() throws RuntimeException
    {
		return (I_UNS_Contract_Recommendation)MTable.get(getCtx(), I_UNS_Contract_Recommendation.Table_Name)
			.getPO(getUNS_Contract_Recommendation_ID(), get_TrxName());	}

	/** Set Contract.
		@param UNS_Contract_Recommendation_ID Contract	  */
	public void setUNS_Contract_Recommendation_ID (int UNS_Contract_Recommendation_ID)
	{
		if (UNS_Contract_Recommendation_ID < 1) 
			set_Value (COLUMNNAME_UNS_Contract_Recommendation_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Contract_Recommendation_ID, Integer.valueOf(UNS_Contract_Recommendation_ID));
	}

	/** Get Contract.
		@return Contract	  */
	public int getUNS_Contract_Recommendation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Contract_Recommendation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (I_UNS_Employee)MTable.get(getCtx(), I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_UNS_UpdateContract getUNS_UpdateContract() throws RuntimeException
    {
		return (I_UNS_UpdateContract)MTable.get(getCtx(), I_UNS_UpdateContract.Table_Name)
			.getPO(getUNS_UpdateContract_ID(), get_TrxName());	}

	/** Set Update Contract.
		@param UNS_UpdateContract_ID Update Contract	  */
	public void setUNS_UpdateContract_ID (int UNS_UpdateContract_ID)
	{
		if (UNS_UpdateContract_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UpdateContract_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UpdateContract_ID, Integer.valueOf(UNS_UpdateContract_ID));
	}

	/** Get Update Contract.
		@return Update Contract	  */
	public int getUNS_UpdateContract_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UpdateContract_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Update Contract History.
		@param UNS_UpdateContractHistory_ID Update Contract History	  */
	public void setUNS_UpdateContractHistory_ID (int UNS_UpdateContractHistory_ID)
	{
		if (UNS_UpdateContractHistory_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UpdateContractHistory_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UpdateContractHistory_ID, Integer.valueOf(UNS_UpdateContractHistory_ID));
	}

	/** Get Update Contract History.
		@return Update Contract History	  */
	public int getUNS_UpdateContractHistory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UpdateContractHistory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UpdateContractHistory_UU.
		@param UNS_UpdateContractHistory_UU UNS_UpdateContractHistory_UU	  */
	public void setUNS_UpdateContractHistory_UU (String UNS_UpdateContractHistory_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UpdateContractHistory_UU, UNS_UpdateContractHistory_UU);
	}

	/** Get UNS_UpdateContractHistory_UU.
		@return UNS_UpdateContractHistory_UU	  */
	public String getUNS_UpdateContractHistory_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UpdateContractHistory_UU);
	}
}