/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author dpitaloka
 *
 */
public class MUNSSubregionRecap extends X_UNS_SubregionRecap implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 227343225400229676L;

	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	
	/**
	 * @param ctx
	 * @param UNS_SubregionRecap_ID
	 * @param trxName
	 */
	public MUNSSubregionRecap(Properties ctx, int UNS_SubregionRecap_ID,
			String trxName) {
		super(ctx, UNS_SubregionRecap_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSSubregionRecap(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (newRecord && getShopType() != null) 
		{
			String sql = "SELECT c_bpartner_id FROM c_bpartner "
					+ "WHERE c_bp_group_id  = ?  "
					+ "AND isemployee = 'Y'";
			MUNSShopRecap storeSalesRecap = null;
			try 
			{
				pstmt = DB.prepareStatement(sql, get_TrxName());
				pstmt.setInt(1, getSubregion_ID());
				rs = pstmt.executeQuery();
				while (rs.next()) {
					storeSalesRecap = new MUNSShopRecap(getCtx(), 0,
							get_TrxName());
					storeSalesRecap.setUNS_SubregionRecap_ID(this.get_ID());
					storeSalesRecap.setShop_ID(rs.getInt(1));
					storeSalesRecap.setC_Period_ID(this.getC_Period_ID());
					storeSalesRecap.save();
				}
			} catch (SQLException e) {
				log.log(Level.SEVERE, sql, e);
				return false;
			} finally {
				DB.close(rs, pstmt);
				rs = null;
				pstmt = null;
			}
		}
		
		return super.afterSave(newRecord, success);
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.PO#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		this.setStartDate(this.getC_Period().getStartDate());
		this.setEndDate(this.getC_Period().getEndDate());
		if (!newRecord) 
		{
			if (is_ValueChanged(COLUMNNAME_Subregion_ID)) 
			{
				int shopCount = Query
						.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
								I_UNS_ShopRecap.Table_Name,
								"uns_subregionrecap_id = ?", get_TrxName())
						.setParameters(get_ID())
						.aggregate(null, Query.AGGREGATE_COUNT).intValue();
				if (shopCount > 0) 
					throw new AdempiereException("Unable to change subregion.");
			}
			if (is_ValueChanged(COLUMNNAME_C_Period_ID)) 
			{
				String s = "SELECT COUNT(*) FROM uns_resource_workerline WL"
						+ ", uns_shopemployee SE, uns_shoprecap ShR, uns_subregionrecap SuR, C_Period P"
						+ " WHERE SuR.uns_subregionrecap_id = ?"
						+ " AND SuR.uns_subregionrecap_id = ShR.uns_subregionrecap_id"
						+ " AND SE.uns_shoprecap_id = ShR.uns_shoprecap_id"
						+ " AND P.c_period_id = SuR.c_period_id"
						+ " AND SE.uns_employee_id = WL.labor_id"
						+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
						+ " AND WL.validto BETWEEN P.startdate AND P.enddate";
				int count = DB.getSQLValue(get_TrxName(), s, getUNS_SubregionRecap_ID());
				
				if (count > 0)
					throw new AdempiereException("Unable to change period.");
			}
		}
		return super.beforeSave(newRecord);
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		if (docStatus.equals(DocAction.STATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_ReActivate;
		}
		
		return index;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String action) throws Exception 
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		log.info("unlockIt - " + toString());
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		log.info(toString());
		setDocAction(DocAction.ACTION_Invalidate);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() {
		log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		setTotalCashierSession();
		
		m_justPrepared = true;
		if (!DocAction.ACTION_Complete.equals(getDocAction()))
			setDocAction(DocAction.ACTION_Complete);
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		log.info(toString());
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() {
		log.info(toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		log.info(toString());

		// Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		Iterable<X_UNS_ShopRecap> shops = Query
				.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
						I_UNS_ShopRecap.Table_Name,
						"uns_subregionrecap_id = ?", get_TrxName())
				.setParameters(get_ID()).list();
		for (X_UNS_ShopRecap shop : shops) {
			Iterable<X_UNS_ShopEmployee> shopEmployees = Query
					.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
							I_UNS_ShopEmployee.Table_Name,
							"uns_shoprecap_id = ?", get_TrxName())
					.setParameters(shop.get_ID()).list();
			for (X_UNS_ShopEmployee shopEmployee : shopEmployees) {
				MUNSEmployee employee = new MUNSEmployee(getCtx(),
						shopEmployee.getUNS_Employee_ID(), get_TrxName());
				employee.setC_BPartner_ID(shop.getShop_ID());
				employee.saveEx();
			}
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		//setDateDoc(new Timestamp(Calendar.getInstance().getTimeInMillis())); // Reset the request timestamp to today.

		setProcessed(true);
		setDocAction(DocAction.ACTION_Close);
		m_processMsg = "Completed.";
		return DocAction.STATUS_Completed;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DocAction.ACTION_None);

		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt() {
		log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		setDocAction(DocAction.ACTION_Close);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() 
	{
		log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reActivateIt() {

		log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setProcessed(false);
		
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() {
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		sb.append(":")
			.append(getSubregion().getName()).append(" ").append(getC_Period().getName());
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() 
	{
		StringBuilder msgreturn = new StringBuilder().append(getDocumentNo());
		return msgreturn.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() {
		try
		{
			StringBuilder msgfile = new StringBuilder().append(get_TableName()).append(get_ID()).append("_");
			File temp = File.createTempFile(msgfile.toString(), ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
		ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.MANUFACTURING_ORDER, get_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if(format.getJasperProcess_ID() > 0)	
		{
			ProcessInfo pi = new ProcessInfo ("", format.getJasperProcess_ID());
			pi.setRecord_ID ( get_ID() );
			pi.setIsBatch(true);
			
			ServerProcessCtl.process(pi, null);
			
			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	}	//	createPDF

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() {
		return getCreatedBy();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		return Env.getContextAsInt(getCtx(),"$C_Currency_ID");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() {
		return Env.ZERO;
	}

	@Override
	public String getDocumentNo() {
		return null;
	}
	
	private void setTotalCashierSession() {
		String s = "UPDATE uns_shoprecap SR"
				+ " SET totalcashiersession = ("
				+ "SELECT COUNT(uns_pos_session_id)"
				+ " FROM uns_pos_session PS, uns_shopemployee SE, c_period P"
				+ " WHERE PS.cashier_id = SE.ad_user_id"
				+ " AND SE.uns_shoprecap_id = SR.uns_shoprecap_id"
				+ " AND P.c_period_id = SR.c_period_id"
				+ " AND PS.dateacct BETWEEN P.startdate AND P.enddate"
				+ ") WHERE uns_subregionrecap_id = ?";
		PreparedStatement p = DB.prepareStatement(s, get_TrxName());
		ResultSet r = null;
		try {
			p.setInt(1, get_ID());
			p.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(r, p);
			r = null;
			p = null;
		}		
	}
}
