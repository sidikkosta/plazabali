/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ShopRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ShopRecap 
{

    /** TableName=UNS_ShopRecap */
    public static final String Table_Name = "UNS_ShopRecap";

    /** AD_Table_ID=1000451 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CalculationClass */
    public static final String COLUMNNAME_CalculationClass = "CalculationClass";

	/** Set Calculation Class.
	  * Java Class for calculation, implementing Interface Measure
	  */
	public void setCalculationClass (String CalculationClass);

	/** Get Calculation Class.
	  * Java Class for calculation, implementing Interface Measure
	  */
	public String getCalculationClass();

    /** Column name C_Period_ID */
    public static final String COLUMNNAME_C_Period_ID = "C_Period_ID";

	/** Set Period.
	  * Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID);

	/** Get Period.
	  * Period of the Calendar
	  */
	public int getC_Period_ID();

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DistributableServiceCharge */
    public static final String COLUMNNAME_DistributableServiceCharge = "DistributableServiceCharge";

	/** Set Distributable Service Charge.
	  * The amount of service charge will be distributed to all staff in the shop proportinally
	  */
	public void setDistributableServiceCharge (BigDecimal DistributableServiceCharge);

	/** Get Distributable Service Charge.
	  * The amount of service charge will be distributed to all staff in the shop proportinally
	  */
	public BigDecimal getDistributableServiceCharge();

    /** Column name EmployeePortion */
    public static final String COLUMNNAME_EmployeePortion = "EmployeePortion";

	/** Set Employee Portion	  */
	public void setEmployeePortion (BigDecimal EmployeePortion);

	/** Get Employee Portion	  */
	public BigDecimal getEmployeePortion();

    /** Column name GroupCashierIncentive */
    public static final String COLUMNNAME_GroupCashierIncentive = "GroupCashierIncentive";

	/** Set Group Cashier Incentive.
	  * Percentage for cashier from group incentives.
	  */
	public void setGroupCashierIncentive (BigDecimal GroupCashierIncentive);

	/** Get Group Cashier Incentive.
	  * Percentage for cashier from group incentives.
	  */
	public BigDecimal getGroupCashierIncentive();

    /** Column name GroupSalesIncentive */
    public static final String COLUMNNAME_GroupSalesIncentive = "GroupSalesIncentive";

	/** Set Group Sales Incentive.
	  * Percentage for sales person from group incentives.
	  */
	public void setGroupSalesIncentive (BigDecimal GroupSalesIncentive);

	/** Get Group Sales Incentive.
	  * Percentage for sales person from group incentives.
	  */
	public BigDecimal getGroupSalesIncentive();

    /** Column name IndividualTargetAmt */
    public static final String COLUMNNAME_IndividualTargetAmt = "IndividualTargetAmt";

	/** Set Individual Target.
	  * Individual Target Amount
	  */
	public void setIndividualTargetAmt (BigDecimal IndividualTargetAmt);

	/** Get Individual Target.
	  * Individual Target Amount
	  */
	public BigDecimal getIndividualTargetAmt();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name LoadShopStaff */
    public static final String COLUMNNAME_LoadShopStaff = "LoadShopStaff";

	/** Set Load Shop Staff	  */
	public void setLoadShopStaff (String LoadShopStaff);

	/** Get Load Shop Staff	  */
	public String getLoadShopStaff();

    /** Column name MinIdvSalesTargetAmt */
    public static final String COLUMNNAME_MinIdvSalesTargetAmt = "MinIdvSalesTargetAmt";

	/** Set Min. Individual Sales Target.
	  * Minimum target amount for each sales person
	  */
	public void setMinIdvSalesTargetAmt (BigDecimal MinIdvSalesTargetAmt);

	/** Get Min. Individual Sales Target.
	  * Minimum target amount for each sales person
	  */
	public BigDecimal getMinIdvSalesTargetAmt();

    /** Column name MinSubSalesTargetAmt */
    public static final String COLUMNNAME_MinSubSalesTargetAmt = "MinSubSalesTargetAmt";

	/** Set Min. Subregion Sales Target.
	  * Minimum target amount for shop
	  */
	public void setMinSubSalesTargetAmt (BigDecimal MinSubSalesTargetAmt);

	/** Get Min. Subregion Sales Target.
	  * Minimum target amount for shop
	  */
	public BigDecimal getMinSubSalesTargetAmt();

    /** Column name NetSalesRealizationAmt */
    public static final String COLUMNNAME_NetSalesRealizationAmt = "NetSalesRealizationAmt";

	/** Set Net Sales Realization.
	  * Total realized net sales within 1 month from all stores
	  */
	public void setNetSalesRealizationAmt (BigDecimal NetSalesRealizationAmt);

	/** Get Net Sales Realization.
	  * Total realized net sales within 1 month from all stores
	  */
	public BigDecimal getNetSalesRealizationAmt();

    /** Column name NetSalesTarget */
    public static final String COLUMNNAME_NetSalesTarget = "NetSalesTarget";

	/** Set Net Sales Target.
	  * Net sales target from all stores in a certain subregion
	  */
	public void setNetSalesTarget (BigDecimal NetSalesTarget);

	/** Get Net Sales Target.
	  * Net sales target from all stores in a certain subregion
	  */
	public BigDecimal getNetSalesTarget();

    /** Column name NotPresentSalesAmt */
    public static final String COLUMNNAME_NotPresentSalesAmt = "NotPresentSalesAmt";

	/** Set Not Present Sales.
	  * Amount in a defined currency
	  */
	public void setNotPresentSalesAmt (BigDecimal NotPresentSalesAmt);

	/** Get Not Present Sales.
	  * Amount in a defined currency
	  */
	public BigDecimal getNotPresentSalesAmt();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name SalesCount */
    public static final String COLUMNNAME_SalesCount = "SalesCount";

	/** Set Sales Count.
	  * Number of employees in a store excluding supervisor
	  */
	public void setSalesCount (int SalesCount);

	/** Get Sales Count.
	  * Number of employees in a store excluding supervisor
	  */
	public int getSalesCount();

    /** Column name setServiceCharge */
    public static final String COLUMNNAME_setServiceCharge = "setServiceCharge";

	/** Set Set Service Charge	  */
	public void setsetServiceCharge (String setServiceCharge);

	/** Get Set Service Charge	  */
	public String getsetServiceCharge();

    /** Column name Shop_ID */
    public static final String COLUMNNAME_Shop_ID = "Shop_ID";

	/** Set Shop.
	  * Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID);

	/** Get Shop.
	  * Identifies a Shop
	  */
	public int getShop_ID();

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException;

    /** Column name SupportPoints */
    public static final String COLUMNNAME_SupportPoints = "SupportPoints";

	/** Set Support Points.
	  * Total point of support staffs
	  */
	public void setSupportPoints (BigDecimal SupportPoints);

	/** Get Support Points.
	  * Total point of support staffs
	  */
	public BigDecimal getSupportPoints();

    /** Column name SupportPortion */
    public static final String COLUMNNAME_SupportPortion = "SupportPortion";

	/** Set Support Portion (%).
	  * Percentage portion for the support employee (support dept)
	  */
	public void setSupportPortion (BigDecimal SupportPortion);

	/** Get Support Portion (%).
	  * Percentage portion for the support employee (support dept)
	  */
	public BigDecimal getSupportPortion();

    /** Column name TotalCashierSession */
    public static final String COLUMNNAME_TotalCashierSession = "TotalCashierSession";

	/** Set Total Cashier Session.
	  * Total Cashier Session
	  */
	public void setTotalCashierSession (BigDecimal TotalCashierSession);

	/** Get Total Cashier Session.
	  * Total Cashier Session
	  */
	public BigDecimal getTotalCashierSession();

    /** Column name TotalPoint */
    public static final String COLUMNNAME_TotalPoint = "TotalPoint";

	/** Set Total Points.
	  * Sum of staff points (ex. supervisor)
	  */
	public void setTotalPoint (BigDecimal TotalPoint);

	/** Get Total Points.
	  * Sum of staff points (ex. supervisor)
	  */
	public BigDecimal getTotalPoint();

    /** Column name TotalServiceChargeAmt */
    public static final String COLUMNNAME_TotalServiceChargeAmt = "TotalServiceChargeAmt";

	/** Set Total Service Charge	  */
	public void setTotalServiceChargeAmt (BigDecimal TotalServiceChargeAmt);

	/** Get Total Service Charge	  */
	public BigDecimal getTotalServiceChargeAmt();

    /** Column name TotalTandemCount */
    public static final String COLUMNNAME_TotalTandemCount = "TotalTandemCount";

	/** Set Total Tandem Count.
	  * Total cashier tandem portion in 1 month
	  */
	public void setTotalTandemCount (BigDecimal TotalTandemCount);

	/** Get Total Tandem Count.
	  * Total cashier tandem portion in 1 month
	  */
	public BigDecimal getTotalTandemCount();

    /** Column name UNS_PeriodicCostBenefit_ID */
    public static final String COLUMNNAME_UNS_PeriodicCostBenefit_ID = "UNS_PeriodicCostBenefit_ID";

	/** Set Periodic Cost And Benefit	  */
	public void setUNS_PeriodicCostBenefit_ID (int UNS_PeriodicCostBenefit_ID);

	/** Get Periodic Cost And Benefit	  */
	public int getUNS_PeriodicCostBenefit_ID();

    /** Column name UNS_ShopRecap_ID */
    public static final String COLUMNNAME_UNS_ShopRecap_ID = "UNS_ShopRecap_ID";

	/** Set Shop Sales Recapitulation	  */
	public void setUNS_ShopRecap_ID (int UNS_ShopRecap_ID);

	/** Get Shop Sales Recapitulation	  */
	public int getUNS_ShopRecap_ID();

    /** Column name UNS_ShopRecap_UU */
    public static final String COLUMNNAME_UNS_ShopRecap_UU = "UNS_ShopRecap_UU";

	/** Set UNS_ShopRecap_UU	  */
	public void setUNS_ShopRecap_UU (String UNS_ShopRecap_UU);

	/** Get UNS_ShopRecap_UU	  */
	public String getUNS_ShopRecap_UU();

    /** Column name UNS_SubregionRecap_ID */
    public static final String COLUMNNAME_UNS_SubregionRecap_ID = "UNS_SubregionRecap_ID";

	/** Set Subregion Recapitulation	  */
	public void setUNS_SubregionRecap_ID (int UNS_SubregionRecap_ID);

	/** Get Subregion Recapitulation	  */
	public int getUNS_SubregionRecap_ID();

	public com.uns.model.I_UNS_SubregionRecap getUNS_SubregionRecap() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
