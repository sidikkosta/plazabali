/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_AttConfiguration
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_AttConfiguration 
{

    /** TableName=UNS_AttConfiguration */
    public static final String Table_Name = "UNS_AttConfiguration";

    /** AD_Table_ID=1000311 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BelatedTolerance */
    public static final String COLUMNNAME_BelatedTolerance = "BelatedTolerance";

	/** Set Belated Tolerance	  */
	public void setBelatedTolerance (int BelatedTolerance);

	/** Get Belated Tolerance	  */
	public int getBelatedTolerance();

    /** Column name C_JobCategory_ID */
    public static final String COLUMNNAME_C_JobCategory_ID = "C_JobCategory_ID";

	/** Set Position Category.
	  * Job Position Category
	  */
	public void setC_JobCategory_ID (int C_JobCategory_ID);

	/** Get Position Category.
	  * Job Position Category
	  */
	public int getC_JobCategory_ID();

	public org.compiere.model.I_C_JobCategory getC_JobCategory() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name EarlierFSOutPresence */
    public static final String COLUMNNAME_EarlierFSOutPresence = "EarlierFSOutPresence";

	/** Set EarlierFSOutPresence	  */
	public void setEarlierFSOutPresence (String EarlierFSOutPresence);

	/** Get EarlierFSOutPresence	  */
	public String getEarlierFSOutPresence();

    /** Column name EmploymentType */
    public static final String COLUMNNAME_EmploymentType = "EmploymentType";

	/** Set Employment Type	  */
	public void setEmploymentType (String EmploymentType);

	/** Get Employment Type	  */
	public String getEmploymentType();

    /** Column name FullRange */
    public static final String COLUMNNAME_FullRange = "FullRange";

	/** Set Full Range	  */
	public void setFullRange (int FullRange);

	/** Get Full Range	  */
	public int getFullRange();

    /** Column name HalfRange */
    public static final String COLUMNNAME_HalfRange = "HalfRange";

	/** Set Half Range	  */
	public void setHalfRange (int HalfRange);

	/** Get Half Range	  */
	public int getHalfRange();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name LateFSInPresence */
    public static final String COLUMNNAME_LateFSInPresence = "LateFSInPresence";

	/** Set LateFSInPresence	  */
	public void setLateFSInPresence (String LateFSInPresence);

	/** Get LateFSInPresence	  */
	public String getLateFSInPresence();

    /** Column name MaxEarlierFSIn */
    public static final String COLUMNNAME_MaxEarlierFSIn = "MaxEarlierFSIn";

	/** Set MaxEarlierFSIn	  */
	public void setMaxEarlierFSIn (int MaxEarlierFSIn);

	/** Get MaxEarlierFSIn	  */
	public int getMaxEarlierFSIn();

    /** Column name MaxEarLierFSOut */
    public static final String COLUMNNAME_MaxEarLierFSOut = "MaxEarLierFSOut";

	/** Set MaxEarLierFSOut	  */
	public void setMaxEarLierFSOut (int MaxEarLierFSOut);

	/** Get MaxEarLierFSOut	  */
	public int getMaxEarLierFSOut();

    /** Column name MaxLateFSIn */
    public static final String COLUMNNAME_MaxLateFSIn = "MaxLateFSIn";

	/** Set MaxLateFSIn	  */
	public void setMaxLateFSIn (int MaxLateFSIn);

	/** Get MaxLateFSIn	  */
	public int getMaxLateFSIn();

    /** Column name MaxLateFSOut */
    public static final String COLUMNNAME_MaxLateFSOut = "MaxLateFSOut";

	/** Set MaxLateFSOut	  */
	public void setMaxLateFSOut (int MaxLateFSOut);

	/** Get MaxLateFSOut	  */
	public int getMaxLateFSOut();

    /** Column name Multiple */
    public static final String COLUMNNAME_Multiple = "Multiple";

	/** Set Multiple	  */
	public void setMultiple (int Multiple);

	/** Get Multiple	  */
	public int getMultiple();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name OverTimeRoundType */
    public static final String COLUMNNAME_OverTimeRoundType = "OverTimeRoundType";

	/** Set OT Round Type	  */
	public void setOverTimeRoundType (String OverTimeRoundType);

	/** Get OT Round Type	  */
	public String getOverTimeRoundType();

    /** Column name RoundingMode */
    public static final String COLUMNNAME_RoundingMode = "RoundingMode";

	/** Set Rounding Mode	  */
	public void setRoundingMode (String RoundingMode);

	/** Get Rounding Mode	  */
	public String getRoundingMode();

    /** Column name SectionOfDept_ID */
    public static final String COLUMNNAME_SectionOfDept_ID = "SectionOfDept_ID";

	/** Set SectionOfDept_ID	  */
	public void setSectionOfDept_ID (int SectionOfDept_ID);

	/** Get SectionOfDept_ID	  */
	public int getSectionOfDept_ID();

	public org.compiere.model.I_C_BPartner getSectionOfDept() throws RuntimeException;

    /** Column name SingleFSPresence */
    public static final String COLUMNNAME_SingleFSPresence = "SingleFSPresence";

	/** Set SingleFSPresence	  */
	public void setSingleFSPresence (String SingleFSPresence);

	/** Get SingleFSPresence	  */
	public String getSingleFSPresence();

    /** Column name UNS_AttConfiguration_ID */
    public static final String COLUMNNAME_UNS_AttConfiguration_ID = "UNS_AttConfiguration_ID";

	/** Set UNS_AttConfiguration_ID	  */
	public void setUNS_AttConfiguration_ID (int UNS_AttConfiguration_ID);

	/** Get UNS_AttConfiguration_ID	  */
	public int getUNS_AttConfiguration_ID();

    /** Column name UNS_AttConfiguration_UU */
    public static final String COLUMNNAME_UNS_AttConfiguration_UU = "UNS_AttConfiguration_UU";

	/** Set UNS_AttConfiguration_UU	  */
	public void setUNS_AttConfiguration_UU (String UNS_AttConfiguration_UU);

	/** Get UNS_AttConfiguration_UU	  */
	public String getUNS_AttConfiguration_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name ValidFrom */
    public static final String COLUMNNAME_ValidFrom = "ValidFrom";

	/** Set Valid from.
	  * Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom);

	/** Get Valid from.
	  * Valid from including this date (first day)
	  */
	public Timestamp getValidFrom();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}
