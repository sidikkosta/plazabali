/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_StaffSrvChgRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_StaffSrvChgRecap extends PO implements I_UNS_StaffSrvChgRecap, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181217L;

    /** Standard Constructor */
    public X_UNS_StaffSrvChgRecap (Properties ctx, int UNS_StaffSrvChgRecap_ID, String trxName)
    {
      super (ctx, UNS_StaffSrvChgRecap_ID, trxName);
      /** if (UNS_StaffSrvChgRecap_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_StaffSrvChgRecap (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_StaffSrvChgRecap[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Point.
		@param Point Point	  */
	public void setPoint (BigDecimal Point)
	{
		set_Value (COLUMNNAME_Point, Point);
	}

	/** Get Point.
		@return Point	  */
	public BigDecimal getPoint () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Point);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Senior Supervisor = SNRS */
	public static final String POSITIONTYPE_SeniorSupervisor = "SNRS";
	/** Shop Supervisor = SSPV */
	public static final String POSITIONTYPE_ShopSupervisor = "SSPV";
	/** Staff = STAF */
	public static final String POSITIONTYPE_Staff = "STAF";
	/** Manager = MNGR */
	public static final String POSITIONTYPE_Manager = "MNGR";
	/** Junior Manager = JMGR */
	public static final String POSITIONTYPE_JuniorManager = "JMGR";
	/** Set Position Type.
		@param PositionType Position Type	  */
	public void setPositionType (String PositionType)
	{

		set_Value (COLUMNNAME_PositionType, PositionType);
	}

	/** Get Position Type.
		@return Position Type	  */
	public String getPositionType () 
	{
		return (String)get_Value(COLUMNNAME_PositionType);
	}

	/** Set Service Charge Amount.
		@param ServiceChargeAmt Service Charge Amount	  */
	public void setServiceChargeAmt (BigDecimal ServiceChargeAmt)
	{
		set_Value (COLUMNNAME_ServiceChargeAmt, ServiceChargeAmt);
	}

	/** Get Service Charge Amount.
		@return Service Charge Amount	  */
	public BigDecimal getServiceChargeAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ServiceChargeAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getShop_ID(), get_TrxName());	}

	/** Set Shop.
		@param Shop_ID 
		Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID)
	{
		if (Shop_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Shop_ID, Integer.valueOf(Shop_ID));
	}

	/** Get Shop.
		@return Identifies a Shop
	  */
	public int getShop_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Shop_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.uns.model.I_UNS_ShopRecap getUNS_ShopRecap() throws RuntimeException
    {
		return (com.uns.model.I_UNS_ShopRecap)MTable.get(getCtx(), com.uns.model.I_UNS_ShopRecap.Table_Name)
			.getPO(getUNS_ShopRecap_ID(), get_TrxName());	}

	/** Set Shop Sales Recapitulation.
		@param UNS_ShopRecap_ID Shop Sales Recapitulation	  */
	public void setUNS_ShopRecap_ID (int UNS_ShopRecap_ID)
	{
		if (UNS_ShopRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShopRecap_ID, Integer.valueOf(UNS_ShopRecap_ID));
	}

	/** Get Shop Sales Recapitulation.
		@return Shop Sales Recapitulation	  */
	public int getUNS_ShopRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShopRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Staff Service Charge Recap.
		@param UNS_StaffSrvChgRecap_ID Staff Service Charge Recap	  */
	public void setUNS_StaffSrvChgRecap_ID (int UNS_StaffSrvChgRecap_ID)
	{
		if (UNS_StaffSrvChgRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StaffSrvChgRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StaffSrvChgRecap_ID, Integer.valueOf(UNS_StaffSrvChgRecap_ID));
	}

	/** Get Staff Service Charge Recap.
		@return Staff Service Charge Recap	  */
	public int getUNS_StaffSrvChgRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StaffSrvChgRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Staff Service Charge Recap UU.
		@param UNS_StaffSrvChgRecap_UU Staff Service Charge Recap UU	  */
	public void setUNS_StaffSrvChgRecap_UU (String UNS_StaffSrvChgRecap_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_StaffSrvChgRecap_UU, UNS_StaffSrvChgRecap_UU);
	}

	/** Get Staff Service Charge Recap UU.
		@return Staff Service Charge Recap UU	  */
	public String getUNS_StaffSrvChgRecap_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_StaffSrvChgRecap_UU);
	}
}