/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author dpitaloka
 *
 */
public class MUNSShopEmployee extends X_UNS_ShopEmployee {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4777195552443919676L;
	private boolean m_isImport = false;
	/**
	 * @param ctx
	 * @param UNS_PersonalSalesRecap_ID
	 * @param trxName
	 */
	public MUNSShopEmployee(Properties ctx,
			int UNS_PersonalSalesRecap_ID, String trxName) {
		super(ctx, UNS_PersonalSalesRecap_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSShopEmployee(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if (newRecord) {
			setPrevShop_ID(getShop_ID());
		} else {
			setPrevShop_ID(get_ValueOldAsInt(COLUMNNAME_Shop_ID));
		}
		
		String sql = "SELECT  ad_user_id FROM ad_user "
				+ "WHERE salesrep_id = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DB.prepareStatement(sql, get_TrxName());
			ps.setInt(1, getUNS_Employee_ID());
			rs = ps.executeQuery(); 
			if (rs.next()) setAD_User_ID(rs.getInt(1));
		} catch (SQLException e) {
			log.log(Level.SEVERE, sql, e);
			return false;
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
		
		if (!isMasterSchedule())
		{
			sql = "SELECT COUNT(*) FROM UNS_ShopEmployee se "
					+ " INNER JOIN UNS_ShopRecap sr ON se.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID"
					+ " WHERE sr.C_Period_ID=(SELECT C_Period_ID FROM UNS_ShopRecap WHERE UNS_ShopRecap_ID=?)"
					+ "		AND se.UNS_Employee_ID=? AND se.IsMasterSchedule='Y'";
			int count = DB.getSQLValue(get_TrxName(), sql, getUNS_ShopRecap_ID(), getUNS_Employee_ID());
			
			if(count == 0)
				setIsMasterSchedule(true);
		}
		else if (is_ValueChanged(COLUMNNAME_IsMasterSchedule)) { // IsMasterSchedule
			String update = "UPDATE UNS_ShopEmployee SET IsMasterSchedule='N' "
					+ " WHERE UNS_ShopEmployee_ID IN ("
					+ "		SELECT UNS_ShopEmployee_ID FROM UNS_ShopEmployee se "
					+ " 	INNER JOIN UNS_ShopRecap sr ON se.UNS_ShopRecap_ID=sr.UNS_ShopRecap_ID"
					+ " 	WHERE sr.C_Period_ID=(SELECT C_Period_ID FROM UNS_ShopRecap WHERE UNS_ShopRecap_ID=?)"
					+ "			AND se.UNS_Employee_ID=? AND se.UNS_ShopEmployee_ID<>?)";
			
			int count = DB.getSQLValue(get_TrxName(), update, 
					getUNS_ShopRecap_ID(), getUNS_Employee_ID(), getUNS_ShopEmployee_ID());
			log.info("Updated ShopEmployee to set it as not master-schedule: " + count);
		}		

		return true;
	}

	public void countWorkingAndOffDays() {
		resetCount();
		countLibur();
		countCuti();
		countTotalShift1();
		countTotalShift2();
		countTotalShift3();
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.PO#afterSave(boolean, boolean)
	 */
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{	
		BigDecimal count = Query
				.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID, Table_Name,
						"uns_shoprecap_id = ? AND isMasterSchedule='Y'", get_TrxName())
				.setParameters(getUNS_ShopRecap_ID())
				.aggregate("*", Query.AGGREGATE_COUNT);
		X_UNS_ShopRecap shopRecap = (X_UNS_ShopRecap) getUNS_ShopRecap();
		shopRecap.setSalesCount(count.intValue());
		shopRecap.save();
		
		return super.afterSave(newRecord, success);
	}


	public void setImport(boolean isImport)
	{
		m_isImport = isImport;
	}

	public boolean isImport()
	{
		return m_isImport;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.PO#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete() 
	{
		String s = "DELETE FROM uns_resource_workerline"
				+ " WHERE uns_resource_workerline_id IN ("
				+ "SELECT uns_resource_workerline_id FROM uns_resource_workerline WL"
				+ ", uns_resource rsc, uns_shopemployee SE, uns_shoprecap SR, c_period P"
				+ " WHERE WL.labor_id = SE.uns_employee_id"
				+ " AND rsc.UNS_Resource_ID=WL.UNS_Resource_ID"
				+ " AND rsc.C_BPartner_ID=SE.Shop_ID"
				+ " AND SR.uns_shoprecap_id = SE.uns_shoprecap_id"
				+ " AND SR.c_period_id = P.c_period_id"
				+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
				+ " AND WL.validto BETWEEN P.startdate AND P.enddate"
				+ " AND SE.uns_shopemployee_id = ?"
				+ ")";
		
		PreparedStatement pstmt = null;
		try {
			pstmt = DB.prepareStatement(s, get_TrxName());
			pstmt.setInt(1, get_ID());
			pstmt.execute();
		} catch (SQLException e) {
			log.log(Level.SEVERE, s, e);
			return false;
		} finally {
			DB.close(pstmt);
			pstmt = null;
		}
		
		return super.beforeDelete();
	}

	/**
	 * Get number of schedule type for this employee in a shop.
	 * 
	 * @param schType
	 * @return
	 */
	private int countScheduleType(String schType) {
		String s = "SELECT COUNT(*) FROM uns_resource_workerline WL"
				+ ", uns_shoprecap SR, uns_shopemployee SE, c_period P"
				+ " WHERE WL.dailyscheduletype=?"
				+ " AND WL.labor_id = ?"
				+ " AND SE.uns_shoprecap_id = SR.uns_shoprecap_id"
				+ " AND SR.c_period_id = P.c_period_id"
				+ " AND WL.labor_id = SE.uns_employee_id" 
				+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
				+ " AND WL.validto BETWEEN P.startdate AND P.enddate"
				+ " AND WL.UNS_Resource_ID IN ("
				+ "		SELECT UNS_Resource_ID FROM UNS_Resource WHERE C_BPartner_ID=?)";
		
		int count = DB.getSQLValueEx(get_TrxName(), s, 
				schType, getUNS_Employee_ID(), getShop_ID());
		return count;
	}

	private void countLibur() {
		int totL 	= countScheduleType("L");
		int totLU 	= countScheduleType("LU");
		int totalLibur = totL + totLU;
		setTotalLibur_L(totL);
		setTotalLibur_LU(totLU);
		setTotalLibur(totalLibur);
	}

	private void countCuti() {
		int tot_CT 	= countScheduleType("CT");
		int tot_CP 	= countScheduleType("CP");
		int tot_CH 	= countScheduleType("CH");
		int tot_CTB = countScheduleType("CTB");
		int totalCuti = tot_CT + tot_CP + tot_CH + tot_CTB;
		setTotalCuti(totalCuti);
		setTotalCuti_CT(tot_CT);
		setTotalCuti_CP(tot_CP);
		setTotalCuti_CH(tot_CH);
		setTotalCuti_CTB(tot_CTB);
	}

	private void countTotalShift1() {
		String s = "SELECT COUNT(*) FROM uns_resource_workerline WL"
				+ ", uns_shoprecap SR, uns_shopemployee SE, c_period P, uns_resource R, uns_slottype ST"
				+ " WHERE WL.labor_id = ?"
				+ " AND WL.uns_resource_id = R.uns_resource_id"
				+ " AND "
				+ " (WL.dailyscheduletype IS NULL"
				+ " OR WL.dailyscheduletype = '')"
				+ " AND ST.value IN ('P1', 'P2', 'P3', 'P4', 'P5', 'P6')"
				+ " AND R.uns_slottype_id = ST.uns_slottype_id"
				+ " AND SE.uns_shoprecap_id = SR.uns_shoprecap_id"
				+ " AND SR.c_period_id = P.c_period_id"
				+ " AND WL.labor_id = SE.uns_employee_id" 
				+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
				+ " AND WL.validto BETWEEN P.startdate AND P.enddate"
				+ " AND WL.UNS_Resource_ID IN ("
				+ "		SELECT UNS_Resource_ID FROM UNS_Resource WHERE C_BPartner_ID=?)";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DB.prepareStatement(s, get_TrxName());
			ps.setInt(1, getUNS_Employee_ID());
			ps.setInt(2, getShop_ID());
			rs = ps.executeQuery();
			if (rs.next()) {
				setTotalShift1(rs.getInt(1));
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, s, e);
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
	}

	private void countTotalShift2() {
		String s = "SELECT COUNT(*) FROM uns_resource_workerline WL"
				+ ", uns_shoprecap SR, uns_shopemployee SE, c_period P, uns_resource R, uns_slottype ST"
				+ " WHERE WL.labor_id = ?"
				+ " AND WL.uns_resource_id = R.uns_resource_id"
				+ " AND "
				+ " (WL.dailyscheduletype IS NULL"
				+ " OR WL.dailyscheduletype = '')"
				+ " AND ST.value IN ('S1', 'S2', 'S3', 'S4', 'S5', 'S6')"
				+ " AND R.uns_slottype_id = ST.uns_slottype_id"
				+ " AND SE.uns_shoprecap_id = SR.uns_shoprecap_id"
				+ " AND SR.c_period_id = P.c_period_id"
				+ " AND WL.labor_id = SE.uns_employee_id" 
				+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
				+ " AND WL.validto BETWEEN P.startdate AND P.enddate"
				+ " AND WL.UNS_Resource_ID IN ("
				+ "		SELECT UNS_Resource_ID FROM UNS_Resource WHERE C_BPartner_ID=?)";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DB.prepareStatement(s, get_TrxName());
			ps.setInt(1, getUNS_Employee_ID());
			ps.setInt(2, getShop_ID());
			rs = ps.executeQuery();
			if (rs.next()) {
				setTotalShift2(rs.getInt(1));
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, s, e);
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
	}

	private void countTotalShift3() {
		String s = "SELECT COUNT(*) FROM uns_resource_workerline WL"
				+ ", uns_shoprecap SR, uns_shopemployee SE, c_period P, uns_resource R, uns_slottype ST"
				+ " WHERE WL.labor_id = ?"
				+ " AND WL.uns_resource_id = R.uns_resource_id"
				+ " AND "
				+ " (WL.dailyscheduletype IS NULL"
				+ " OR WL.dailyscheduletype = '')"
				+ " AND ST.value IN ('M1', 'M2', 'M3', 'M4', 'M5', 'M6')"
				+ " AND R.uns_slottype_id = ST.uns_slottype_id"
				+ " AND SE.uns_shoprecap_id = SR.uns_shoprecap_id"
				+ " AND SR.c_period_id = P.c_period_id"
				+ " AND WL.labor_id = SE.uns_employee_id" 
				+ " AND WL.validfrom BETWEEN P.startdate AND P.enddate"
				+ " AND WL.validto BETWEEN P.startdate AND P.enddate"
				+ " AND WL.UNS_Resource_ID IN ("
				+ "		SELECT UNS_Resource_ID FROM UNS_Resource WHERE C_BPartner_ID=?)";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DB.prepareStatement(s, get_TrxName());
			ps.setInt(1, getUNS_Employee_ID());
			ps.setInt(2, getShop_ID());
			rs = ps.executeQuery();
			if (rs.next()) {
				setTotalShift3(rs.getInt(1));
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, s, e);
		} finally {
			DB.close(rs, ps);
			rs = null;
			ps = null;
		}
	}

	private void resetCount() {
		setTotalLibur(0);
		setTotalCuti(0);
		setTotalShift1(0);
		setTotalShift2(0);
		setTotalShift3(0);
	}
}
