/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for T_PayrollReport
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_T_PayrollReport 
{

    /** TableName=T_PayrollReport */
    public static final String Table_Name = "T_PayrollReport";

    /** AD_Table_ID=1000366 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Period_ID */
    public static final String COLUMNNAME_C_Period_ID = "C_Period_ID";

	/** Set Period.
	  * Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID);

	/** Get Period.
	  * Period of the Calendar
	  */
	public int getC_Period_ID();

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException;

    /** Column name C_Year_ID */
    public static final String COLUMNNAME_C_Year_ID = "C_Year_ID";

	/** Set Year.
	  * Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID);

	/** Get Year.
	  * Calendar Year
	  */
	public int getC_Year_ID();

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name isPeriodic */
    public static final String COLUMNNAME_isPeriodic = "isPeriodic";

	/** Set is Periodic?	  */
	public void setisPeriodic (boolean isPeriodic);

	/** Get is Periodic?	  */
	public boolean isPeriodic();

    /** Column name M1 */
    public static final String COLUMNNAME_M1 = "M1";

	/** Set M1	  */
	public void setM1 (BigDecimal M1);

	/** Get M1	  */
	public BigDecimal getM1();

    /** Column name M10 */
    public static final String COLUMNNAME_M10 = "M10";

	/** Set M10	  */
	public void setM10 (BigDecimal M10);

	/** Get M10	  */
	public BigDecimal getM10();

    /** Column name M11 */
    public static final String COLUMNNAME_M11 = "M11";

	/** Set M11	  */
	public void setM11 (BigDecimal M11);

	/** Get M11	  */
	public BigDecimal getM11();

    /** Column name M12 */
    public static final String COLUMNNAME_M12 = "M12";

	/** Set M12	  */
	public void setM12 (BigDecimal M12);

	/** Get M12	  */
	public BigDecimal getM12();

    /** Column name M2 */
    public static final String COLUMNNAME_M2 = "M2";

	/** Set M2	  */
	public void setM2 (BigDecimal M2);

	/** Get M2	  */
	public BigDecimal getM2();

    /** Column name M3 */
    public static final String COLUMNNAME_M3 = "M3";

	/** Set M3	  */
	public void setM3 (BigDecimal M3);

	/** Get M3	  */
	public BigDecimal getM3();

    /** Column name M4 */
    public static final String COLUMNNAME_M4 = "M4";

	/** Set M4	  */
	public void setM4 (BigDecimal M4);

	/** Get M4	  */
	public BigDecimal getM4();

    /** Column name M5 */
    public static final String COLUMNNAME_M5 = "M5";

	/** Set M5	  */
	public void setM5 (BigDecimal M5);

	/** Get M5	  */
	public BigDecimal getM5();

    /** Column name M6 */
    public static final String COLUMNNAME_M6 = "M6";

	/** Set M6	  */
	public void setM6 (BigDecimal M6);

	/** Get M6	  */
	public BigDecimal getM6();

    /** Column name M7 */
    public static final String COLUMNNAME_M7 = "M7";

	/** Set M7	  */
	public void setM7 (BigDecimal M7);

	/** Get M7	  */
	public BigDecimal getM7();

    /** Column name M8 */
    public static final String COLUMNNAME_M8 = "M8";

	/** Set M8	  */
	public void setM8 (BigDecimal M8);

	/** Get M8	  */
	public BigDecimal getM8();

    /** Column name M9 */
    public static final String COLUMNNAME_M9 = "M9";

	/** Set M9	  */
	public void setM9 (BigDecimal M9);

	/** Get M9	  */
	public BigDecimal getM9();

    /** Column name PayrollComponent */
    public static final String COLUMNNAME_PayrollComponent = "PayrollComponent";

	/** Set PayrollComponent	  */
	public void setPayrollComponent (String PayrollComponent);

	/** Get PayrollComponent	  */
	public String getPayrollComponent();

    /** Column name Sequence */
    public static final String COLUMNNAME_Sequence = "Sequence";

	/** Set Sequence	  */
	public void setSequence (BigDecimal Sequence);

	/** Get Sequence	  */
	public BigDecimal getSequence();

    /** Column name T_PayrollReport_ID */
    public static final String COLUMNNAME_T_PayrollReport_ID = "T_PayrollReport_ID";

	/** Set T_PayrollReport	  */
	public void setT_PayrollReport_ID (int T_PayrollReport_ID);

	/** Get T_PayrollReport	  */
	public int getT_PayrollReport_ID();

    /** Column name T_PayrollReport_UU */
    public static final String COLUMNNAME_T_PayrollReport_UU = "T_PayrollReport_UU";

	/** Set T_PayrollReport_UU	  */
	public void setT_PayrollReport_UU (String T_PayrollReport_UU);

	/** Get T_PayrollReport_UU	  */
	public String getT_PayrollReport_UU();

    /** Column name TotalAmt */
    public static final String COLUMNNAME_TotalAmt = "TotalAmt";

	/** Set Total Amount.
	  * Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt);

	/** Get Total Amount.
	  * Total Amount
	  */
	public BigDecimal getTotalAmt();

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
