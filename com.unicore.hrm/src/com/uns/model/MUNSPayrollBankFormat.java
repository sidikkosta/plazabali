/**
 * 
 */
package com.uns.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.Adempiere;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author nurse
 *
 */
public class MUNSPayrollBankFormat extends X_UNS_PayrollBankFormat {

	private static final long serialVersionUID = -5787709197148137948L;
	private MUNSPayrollBankFormatLine[] m_lines = null;
	
	public MUNSPayrollBankFormatLine[] getLines ()
	{
		return getLines(false);
	}
	
	public MUNSPayrollBankFormatLine[] getLines (boolean refresh)
	{
		if (m_lines != null && !refresh)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSPayrollBankFormatLine> list = Query.get(
				getCtx(), 
				UNSHRMModelFactory.EXTENSION_ID, 
				MUNSPayrollBankFormatLine.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).
				setOrderBy("SeqNo").setOnlyActiveRecords(true).
				list();
		m_lines = new MUNSPayrollBankFormatLine[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}

	/**
	 * @param ctx
	 * @param UNS_PayrollBankFormat_ID
	 * @param trxName
	 */
	public MUNSPayrollBankFormat(Properties ctx, int UNS_PayrollBankFormat_ID,
			String trxName) 
	{
		super(ctx, UNS_PayrollBankFormat_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollBankFormat(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public File generateBankFormat (MUNSPayrollPaymentBA bankAcct) throws IOException
	{
		String filePath = Adempiere.getAdempiereHome().trim();
		if (!filePath.endsWith("/") && !filePath.endsWith("\\"))
			filePath += File.separator;
		long milis = System.currentTimeMillis();
		filePath += getName();
		filePath += milis;
		filePath += ".csv";
		File file = new File(filePath);
		FileOutputStream os = new FileOutputStream(file);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		MUNSPayrollPaymentEmp[] emps = bankAcct.getLines(true);
		writer.write(initHeader());
		for (int i=0; i<emps.length; i++)
		{
			writer.newLine();
			writer.write(getValuesFromPaymentEmp(emps[i]));
		}
		writer.close();
		os.close();
		return file;
	}
	
	private String initHeader ()
	{
		getLines();
		StringBuilder valBuilder = new StringBuilder();
		for (int i=0; i<m_lines.length; i++)
		{
			valBuilder.append(m_lines[i].getName());
			if (i<m_lines.length-1)
				valBuilder.append(getDelimiter());
		}
		String retVal = valBuilder.toString();
		return retVal;
	}
	
	private String getValuesFromPaymentEmp (MUNSPayrollPaymentEmp emp)
	{
		MUNSPayrollBankFormatLine[] lines = getLines();
		StringBuilder valBuilder = new StringBuilder();
		for (int i=0; i<lines.length; i++)
		{
			valBuilder.append(m_lines[i].initValue(emp));
			if (i<m_lines.length-1)
				valBuilder.append(getDelimiter());
		}
		String retVal = valBuilder.toString();
		return retVal;
	}
}
