/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_YearlyPayrollSummary
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_YearlyPayrollSummary extends PO implements I_UNS_YearlyPayrollSummary, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180604L;

    /** Standard Constructor */
    public X_UNS_YearlyPayrollSummary (Properties ctx, int UNS_YearlyPayrollSummary_ID, String trxName)
    {
      super (ctx, UNS_YearlyPayrollSummary_ID, trxName);
      /** if (UNS_YearlyPayrollSummary_ID == 0)
        {
			setA_JHT (Env.ZERO);
// 0
			setA_JK (Env.ZERO);
// 0
			setA_JKK (Env.ZERO);
// 0
			setA_JPK (Env.ZERO);
// 0
			setA_L1 (Env.ZERO);
// 0
			setA_L1R (Env.ZERO);
// 0
			setA_L2 (Env.ZERO);
// 0
			setA_L2R (Env.ZERO);
			setA_L3 (Env.ZERO);
// 0
			setA_L3R (Env.ZERO);
			setA_LemburJamBerikutnya (Env.ZERO);
// 0
			setA_LemburJamPertama (Env.ZERO);
// 0
			setGPokok (Env.ZERO);
			setP_JHT (Env.ZERO);
// 0
			setP_JK (Env.ZERO);
// 0
			setP_JKK (Env.ZERO);
// 0
			setP_JP (Env.ZERO);
// 0
			setP_JPK (Env.ZERO);
// 0
			setPPH21 (Env.ZERO);
// 0
			setStartPeriodNo (0);
// 0
			setTakeHomePay (Env.ZERO);
			setTNonPeriodicBenefit (Env.ZERO);
// 0
			setTNonPeriodicBenefitPPh21Comp (Env.ZERO);
// 0
			setTNonPeriodicDeducPPh21Comp (Env.ZERO);
// 0
			setTNonPeriodicDeduction (Env.ZERO);
// 0
			setTPeriodicBenefit (Env.ZERO);
// 0
			setTPeriodicBenefitPPh21Comp (Env.ZERO);
// 0
			setTPeriodicDeducPPh21Comp (Env.ZERO);
// 0
			setTPeriodicDeduction (Env.ZERO);
// 0
			setUNS_Employee_ID (0);
			setUNS_YearlyPayrollSummary_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_YearlyPayrollSummary (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_YearlyPayrollSummary[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set A.JHT.
		@param A_JHT A.JHT	  */
	public void setA_JHT (BigDecimal A_JHT)
	{
		set_Value (COLUMNNAME_A_JHT, A_JHT);
	}

	/** Get A.JHT.
		@return A.JHT	  */
	public BigDecimal getA_JHT () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_JHT);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set A.JK.
		@param A_JK A.JK	  */
	public void setA_JK (BigDecimal A_JK)
	{
		set_Value (COLUMNNAME_A_JK, A_JK);
	}

	/** Get A.JK.
		@return A.JK	  */
	public BigDecimal getA_JK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_JK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set A.JKK.
		@param A_JKK A.JKK	  */
	public void setA_JKK (BigDecimal A_JKK)
	{
		set_Value (COLUMNNAME_A_JKK, A_JKK);
	}

	/** Get A.JKK.
		@return A.JKK	  */
	public BigDecimal getA_JKK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_JKK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set A_JP.
		@param A_JP A_JP	  */
	public void setA_JP (BigDecimal A_JP)
	{
		set_Value (COLUMNNAME_A_JP, A_JP);
	}

	/** Get A_JP.
		@return A_JP	  */
	public BigDecimal getA_JP () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_JP);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set A.JPK.
		@param A_JPK A.JPK	  */
	public void setA_JPK (BigDecimal A_JPK)
	{
		set_Value (COLUMNNAME_A_JPK, A_JPK);
	}

	/** Get A.JPK.
		@return A.JPK	  */
	public BigDecimal getA_JPK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_JPK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur 1.
		@param A_L1 Aditional Lembur 1	  */
	public void setA_L1 (BigDecimal A_L1)
	{
		set_Value (COLUMNNAME_A_L1, A_L1);
	}

	/** Get Aditional Lembur 1.
		@return Aditional Lembur 1	  */
	public BigDecimal getA_L1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_L1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur 1 R.
		@param A_L1R 
		Amount Aditional Lembur 1 R
	  */
	public void setA_L1R (BigDecimal A_L1R)
	{
		set_Value (COLUMNNAME_A_L1R, A_L1R);
	}

	/** Get Aditional Lembur 1 R.
		@return Amount Aditional Lembur 1 R
	  */
	public BigDecimal getA_L1R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_L1R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur 2.
		@param A_L2 Aditional Lembur 2	  */
	public void setA_L2 (BigDecimal A_L2)
	{
		set_Value (COLUMNNAME_A_L2, A_L2);
	}

	/** Get Aditional Lembur 2.
		@return Aditional Lembur 2	  */
	public BigDecimal getA_L2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_L2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur 2 R.
		@param A_L2R 
		Amount Aditional Lembur 2 R
	  */
	public void setA_L2R (BigDecimal A_L2R)
	{
		set_Value (COLUMNNAME_A_L2R, A_L2R);
	}

	/** Get Aditional Lembur 2 R.
		@return Amount Aditional Lembur 2 R
	  */
	public BigDecimal getA_L2R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_L2R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur 3.
		@param A_L3 Aditional Lembur 3	  */
	public void setA_L3 (BigDecimal A_L3)
	{
		set_Value (COLUMNNAME_A_L3, A_L3);
	}

	/** Get Aditional Lembur 3.
		@return Aditional Lembur 3	  */
	public BigDecimal getA_L3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_L3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur 3 R.
		@param A_L3R 
		Amount Aditional Lembur 3 R
	  */
	public void setA_L3R (BigDecimal A_L3R)
	{
		set_Value (COLUMNNAME_A_L3R, A_L3R);
	}

	/** Get Aditional Lembur 3 R.
		@return Amount Aditional Lembur 3 R
	  */
	public BigDecimal getA_L3R () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_L3R);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set A. Lembur Jam Berikutnya.
		@param A_LemburJamBerikutnya A. Lembur Jam Berikutnya	  */
	public void setA_LemburJamBerikutnya (BigDecimal A_LemburJamBerikutnya)
	{
		set_Value (COLUMNNAME_A_LemburJamBerikutnya, A_LemburJamBerikutnya);
	}

	/** Get A. Lembur Jam Berikutnya.
		@return A. Lembur Jam Berikutnya	  */
	public BigDecimal getA_LemburJamBerikutnya () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_LemburJamBerikutnya);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Aditional Lembur Jam Pertama.
		@param A_LemburJamPertama Aditional Lembur Jam Pertama	  */
	public void setA_LemburJamPertama (BigDecimal A_LemburJamPertama)
	{
		set_Value (COLUMNNAME_A_LemburJamPertama, A_LemburJamPertama);
	}

	/** Get Aditional Lembur Jam Pertama.
		@return Aditional Lembur Jam Pertama	  */
	public BigDecimal getA_LemburJamPertama () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_LemburJamPertama);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException
    {
		return (org.compiere.model.I_C_Year)MTable.get(getCtx(), org.compiere.model.I_C_Year.Table_Name)
			.getPO(getC_Year_ID(), get_TrxName());	}

	/** Set Year.
		@param C_Year_ID 
		Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID)
	{
		if (C_Year_ID < 1) 
			set_Value (COLUMNNAME_C_Year_ID, null);
		else 
			set_Value (COLUMNNAME_C_Year_ID, Integer.valueOf(C_Year_ID));
	}

	/** Get Year.
		@return Calendar Year
	  */
	public int getC_Year_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Year_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Gaji Pokok.
		@param GPokok Gaji Pokok	  */
	public void setGPokok (BigDecimal GPokok)
	{
		set_Value (COLUMNNAME_GPokok, GPokok);
	}

	/** Get Gaji Pokok.
		@return Gaji Pokok	  */
	public BigDecimal getGPokok () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GPokok);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Period getPeriodStart() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getPeriodStart_ID(), get_TrxName());	}

	/** Set Period Start.
		@param PeriodStart_ID Period Start	  */
	public void setPeriodStart_ID (int PeriodStart_ID)
	{
		if (PeriodStart_ID < 1) 
			set_Value (COLUMNNAME_PeriodStart_ID, null);
		else 
			set_Value (COLUMNNAME_PeriodStart_ID, Integer.valueOf(PeriodStart_ID));
	}

	/** Get Period Start.
		@return Period Start	  */
	public int getPeriodStart_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PeriodStart_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set P.JHT.
		@param P_JHT P.JHT	  */
	public void setP_JHT (BigDecimal P_JHT)
	{
		set_Value (COLUMNNAME_P_JHT, P_JHT);
	}

	/** Get P.JHT.
		@return P.JHT	  */
	public BigDecimal getP_JHT () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_P_JHT);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set P.JK.
		@param P_JK P.JK	  */
	public void setP_JK (BigDecimal P_JK)
	{
		set_Value (COLUMNNAME_P_JK, P_JK);
	}

	/** Get P.JK.
		@return P.JK	  */
	public BigDecimal getP_JK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_P_JK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set P.JKK.
		@param P_JKK P.JKK	  */
	public void setP_JKK (BigDecimal P_JKK)
	{
		set_Value (COLUMNNAME_P_JKK, P_JKK);
	}

	/** Get P.JKK.
		@return P.JKK	  */
	public BigDecimal getP_JKK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_P_JKK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set P_JP.
		@param P_JP P_JP	  */
	public void setP_JP (BigDecimal P_JP)
	{
		set_Value (COLUMNNAME_P_JP, P_JP);
	}

	/** Get P_JP.
		@return P_JP	  */
	public BigDecimal getP_JP () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_P_JP);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set P.JPK.
		@param P_JPK P.JPK	  */
	public void setP_JPK (BigDecimal P_JPK)
	{
		set_Value (COLUMNNAME_P_JPK, P_JPK);
	}

	/** Get P.JPK.
		@return P.JPK	  */
	public BigDecimal getP_JPK () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_P_JPK);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set PPH 21.
		@param PPH21 PPH 21	  */
	public void setPPH21 (BigDecimal PPH21)
	{
		set_Value (COLUMNNAME_PPH21, PPH21);
	}

	/** Get PPH 21.
		@return PPH 21	  */
	public BigDecimal getPPH21 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PPH21);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Start Period No.
		@param StartPeriodNo Start Period No	  */
	public void setStartPeriodNo (int StartPeriodNo)
	{
		set_Value (COLUMNNAME_StartPeriodNo, Integer.valueOf(StartPeriodNo));
	}

	/** Get Start Period No.
		@return Start Period No	  */
	public int getStartPeriodNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_StartPeriodNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Take Home Pay.
		@param TakeHomePay 
		Take Home Pay
	  */
	public void setTakeHomePay (BigDecimal TakeHomePay)
	{
		set_Value (COLUMNNAME_TakeHomePay, TakeHomePay);
	}

	/** Get Take Home Pay.
		@return Take Home Pay
	  */
	public BigDecimal getTakeHomePay () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TakeHomePay);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Non Periodic Benefit.
		@param TNonPeriodicBenefit T Non Periodic Benefit	  */
	public void setTNonPeriodicBenefit (BigDecimal TNonPeriodicBenefit)
	{
		set_Value (COLUMNNAME_TNonPeriodicBenefit, TNonPeriodicBenefit);
	}

	/** Get T Non Periodic Benefit.
		@return T Non Periodic Benefit	  */
	public BigDecimal getTNonPeriodicBenefit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TNonPeriodicBenefit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Non Periodic Benefit PPh21 Comp.
		@param TNonPeriodicBenefitPPh21Comp T Non Periodic Benefit PPh21 Comp	  */
	public void setTNonPeriodicBenefitPPh21Comp (BigDecimal TNonPeriodicBenefitPPh21Comp)
	{
		set_Value (COLUMNNAME_TNonPeriodicBenefitPPh21Comp, TNonPeriodicBenefitPPh21Comp);
	}

	/** Get T Non Periodic Benefit PPh21 Comp.
		@return T Non Periodic Benefit PPh21 Comp	  */
	public BigDecimal getTNonPeriodicBenefitPPh21Comp () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TNonPeriodicBenefitPPh21Comp);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Non Periodic Deduc PPh21 Comp.
		@param TNonPeriodicDeducPPh21Comp T Non Periodic Deduc PPh21 Comp	  */
	public void setTNonPeriodicDeducPPh21Comp (BigDecimal TNonPeriodicDeducPPh21Comp)
	{
		set_Value (COLUMNNAME_TNonPeriodicDeducPPh21Comp, TNonPeriodicDeducPPh21Comp);
	}

	/** Get T Non Periodic Deduc PPh21 Comp.
		@return T Non Periodic Deduc PPh21 Comp	  */
	public BigDecimal getTNonPeriodicDeducPPh21Comp () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TNonPeriodicDeducPPh21Comp);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Non Periodic Deduction.
		@param TNonPeriodicDeduction T Non Periodic Deduction	  */
	public void setTNonPeriodicDeduction (BigDecimal TNonPeriodicDeduction)
	{
		set_Value (COLUMNNAME_TNonPeriodicDeduction, TNonPeriodicDeduction);
	}

	/** Get T Non Periodic Deduction.
		@return T Non Periodic Deduction	  */
	public BigDecimal getTNonPeriodicDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TNonPeriodicDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Periodic Benefit.
		@param TPeriodicBenefit T Periodic Benefit	  */
	public void setTPeriodicBenefit (BigDecimal TPeriodicBenefit)
	{
		set_Value (COLUMNNAME_TPeriodicBenefit, TPeriodicBenefit);
	}

	/** Get T Periodic Benefit.
		@return T Periodic Benefit	  */
	public BigDecimal getTPeriodicBenefit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TPeriodicBenefit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Periodic Benefit PPh21 Comp.
		@param TPeriodicBenefitPPh21Comp T Periodic Benefit PPh21 Comp	  */
	public void setTPeriodicBenefitPPh21Comp (BigDecimal TPeriodicBenefitPPh21Comp)
	{
		set_Value (COLUMNNAME_TPeriodicBenefitPPh21Comp, TPeriodicBenefitPPh21Comp);
	}

	/** Get T Periodic Benefit PPh21 Comp.
		@return T Periodic Benefit PPh21 Comp	  */
	public BigDecimal getTPeriodicBenefitPPh21Comp () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TPeriodicBenefitPPh21Comp);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Periodic Deduc PPh21 Comp.
		@param TPeriodicDeducPPh21Comp T Periodic Deduc PPh21 Comp	  */
	public void setTPeriodicDeducPPh21Comp (BigDecimal TPeriodicDeducPPh21Comp)
	{
		set_Value (COLUMNNAME_TPeriodicDeducPPh21Comp, TPeriodicDeducPPh21Comp);
	}

	/** Get T Periodic Deduc PPh21 Comp.
		@return T Periodic Deduc PPh21 Comp	  */
	public BigDecimal getTPeriodicDeducPPh21Comp () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TPeriodicDeducPPh21Comp);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set T Periodic Deduction.
		@param TPeriodicDeduction T Periodic Deduction	  */
	public void setTPeriodicDeduction (BigDecimal TPeriodicDeduction)
	{
		set_Value (COLUMNNAME_TPeriodicDeduction, TPeriodicDeduction);
	}

	/** Get T Periodic Deduction.
		@return T Periodic Deduction	  */
	public BigDecimal getTPeriodicDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TPeriodicDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_Employee)MTable.get(getCtx(), com.uns.model.I_UNS_Employee.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getUNS_Employee_ID()));
    }

	/** Set Yearly Payroll Summary.
		@param UNS_YearlyPayrollSummary_ID Yearly Payroll Summary	  */
	public void setUNS_YearlyPayrollSummary_ID (int UNS_YearlyPayrollSummary_ID)
	{
		if (UNS_YearlyPayrollSummary_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_YearlyPayrollSummary_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_YearlyPayrollSummary_ID, Integer.valueOf(UNS_YearlyPayrollSummary_ID));
	}

	/** Get Yearly Payroll Summary.
		@return Yearly Payroll Summary	  */
	public int getUNS_YearlyPayrollSummary_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_YearlyPayrollSummary_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_YearlyPayrollSummary_UU.
		@param UNS_YearlyPayrollSummary_UU UNS_YearlyPayrollSummary_UU	  */
	public void setUNS_YearlyPayrollSummary_UU (String UNS_YearlyPayrollSummary_UU)
	{
		set_Value (COLUMNNAME_UNS_YearlyPayrollSummary_UU, UNS_YearlyPayrollSummary_UU);
	}

	/** Get UNS_YearlyPayrollSummary_UU.
		@return UNS_YearlyPayrollSummary_UU	  */
	public String getUNS_YearlyPayrollSummary_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_YearlyPayrollSummary_UU);
	}
}