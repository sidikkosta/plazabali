/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_UMKLevel
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_UMKLevel extends PO implements I_UNS_UMKLevel, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180416L;

    /** Standard Constructor */
    public X_UNS_UMKLevel (Properties ctx, int UNS_UMKLevel_ID, String trxName)
    {
      super (ctx, UNS_UMKLevel_ID, trxName);
      /** if (UNS_UMKLevel_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setName (null);
			setUNS_UMKLevel_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UMKLevel (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UMKLevel[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set UMK Level.
		@param UNS_UMKLevel_ID UMK Level	  */
	public void setUNS_UMKLevel_ID (int UNS_UMKLevel_ID)
	{
		if (UNS_UMKLevel_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UMKLevel_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UMKLevel_ID, Integer.valueOf(UNS_UMKLevel_ID));
	}

	/** Get UMK Level.
		@return UMK Level	  */
	public int getUNS_UMKLevel_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UMKLevel_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UMKLevel_UU.
		@param UNS_UMKLevel_UU UNS_UMKLevel_UU	  */
	public void setUNS_UMKLevel_UU (String UNS_UMKLevel_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UMKLevel_UU, UNS_UMKLevel_UU);
	}

	/** Get UNS_UMKLevel_UU.
		@return UNS_UMKLevel_UU	  */
	public String getUNS_UMKLevel_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UMKLevel_UU);
	}
}