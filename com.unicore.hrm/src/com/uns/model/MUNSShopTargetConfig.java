/**
 * 
 */
package com.uns.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.util.DB;

/**
 * @author dpitaloka
 *
 */
public class MUNSShopTargetConfig extends X_UNS_ShopTargetConfig implements
		DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7504208097422825433L;

	/**
	 * @param ctx
	 * @param UNS_ShopTargetConfig_ID
	 * @param trxName
	 */
	public MUNSShopTargetConfig(Properties ctx, int UNS_ShopTargetConfig_ID,
			String trxName) {
		super(ctx, UNS_ShopTargetConfig_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSShopTargetConfig(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{
		if (!success)
			return false;
		
		// update subregion's target.
		String sql = "UPDATE UNS_SubregionTargetConfig subc "
				+ " SET NetSalesTarget = ("
				+ "		SELECT SUM(shopc.NetSalesTarget) "
				+ " 	FROM UNS_ShopTargetConfig shopc"
				+ " 	WHERE shopc.UNS_SubregionTargetConfig_ID=subc.UNS_SubregionTargetConfig_ID)"
				+ " WHERE UNS_SubregionTargetConfig_ID=" + getUNS_SubregionTargetConfig_ID();
		
		int count = DB.executeUpdate(sql, get_TrxName());
		
		if (count <= 0)
			throw new AdempiereException("Failed updating (header) Subregion-target config.");
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#setDocStatus(java.lang.String)
	 */
	@Override
	public void setDocStatus(String newStatus) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocStatus()
	 */
	@Override
	public String getDocStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String action) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		MUNSShopRecap shop = new MUNSShopRecap(getCtx(), getShop_ID(), get_TrxName());
		
		if (shop != null) 
		{
			shop.setNetSalesTarget(getNetSalesTarget());
			String s = "SELECT SUM(netsalestarget) FROM uns_shoprecap WHERE uns_subregionrecap_id = ?";
			PreparedStatement ps = DB.prepareStatement(s, get_TrxName());
			ResultSet rs = null;
			try {
				ps.setInt(1, shop.getUNS_SubregionRecap_ID());
				rs = ps.executeQuery();
				if (rs.next()) {
					MUNSSubregionRecap subregion = new MUNSSubregionRecap(
							getCtx(), shop.getUNS_SubregionRecap_ID(),
							get_TrxName());
					subregion.setNetSalesTarget(rs.getBigDecimal(1));
				}
			} catch (SQLException e) {
				log.log(Level.SEVERE, s, e);
			} finally {
				DB.close(rs, ps);
				rs = null;
				ps= null;
			}
		
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentNo()
	 */
	@Override
	public String getDocumentNo() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getProcessMsg()
	 */
	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocAction()
	 */
	@Override
	public String getDocAction() {
		// TODO Auto-generated method stub
		return null;
	}

}
