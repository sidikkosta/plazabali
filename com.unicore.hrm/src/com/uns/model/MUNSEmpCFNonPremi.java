package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSEmpCFNonPremi extends X_UNS_EmpCF_NonPremi {

	/**
	 * @author Hamba Allah
	 */
	private static final long serialVersionUID = -8561133461895973040L;
	
	protected MUNSCrossFunctionNonPremi m_parent = null;

	public MUNSEmpCFNonPremi(Properties ctx, int UNS_EmpCF_NonPremi_ID,
			String trxName) {
		super(ctx, UNS_EmpCF_NonPremi_ID, trxName);
		
	}

	public MUNSEmpCFNonPremi(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	public static MUNSEmpCFNonPremi getByBenefitLine(Properties ctx, int UNS_PeriodicCostBenefitLine_ID , String trxName ) {
		
		return Query.get(
				ctx, UNSHRMModelFactory.EXTENSION_ID, Table_Name,
				COLUMNNAME_UNS_PeriodicCostBenefitLine_ID + " = "+UNS_PeriodicCostBenefitLine_ID,
				trxName).firstOnly();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		getParent();
		
		String sql = "SELECT cf.DocumentNo FROM UNS_EmpCF_NonPremi e"
				+ " INNER JOIN UNS_CrossFunction_NonPremi cf ON cf.UNS_CrossFunction_NonPremi_ID = e.UNS_CrossFunction_NonPremi_ID"
				+ " WHERE e.UNS_EmpCF_NonPremi_ID <> ? AND cf.DocStatus NOT IN ('VO','RE') AND cf.C_Period_ID = ?"
				+ " AND cf.AD_OrgTrx_ID = ?";
		String docNo = DB.getSQLValueString(get_TrxName(), sql, get_ID(), m_parent.getC_Period_ID(), m_parent.getAD_OrgTrx_ID());
		if(docNo != null)
		{
			log.saveError("Duplicate", "There was exists document with this employee. Document No "+docNo);
			return false;
		}
		
		return super.beforeSave(newRecord);
	}
	
	public MUNSCrossFunctionNonPremi getParent() {
		
		if(m_parent != null)
			return m_parent;
		
		m_parent = new MUNSCrossFunctionNonPremi(getCtx(), getUNS_CrossFunction_NonPremi_ID(), get_TrxName());
		
		return m_parent;
		
	}

}
