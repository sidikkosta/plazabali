/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MColumn;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

/**
 * @author nurse
 *
 */
public class MUNSPayrollBankFormatLine extends X_UNS_PayrollBankFormatLine 
{

	private static final long serialVersionUID = 3093563513823058426L;
	private MUNSPayrollBankFormat m_parent = null;
	
	public MUNSPayrollBankFormat getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPayrollBankFormat(getCtx(), getUNS_PayrollBankFormat_ID(), get_TrxName());
		return m_parent;
	}

	/**
	 * @param ctx
	 * @param UNS_PayrollBankFormatLine_ID
	 * @param trxName
	 */
	public MUNSPayrollBankFormatLine(Properties ctx,
			int UNS_PayrollBankFormatLine_ID, String trxName) 
	{
		super(ctx, UNS_PayrollBankFormatLine_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPayrollBankFormatLine(Properties ctx, ResultSet rs,
			String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (getSeqNo() == 0)
		{
			String sql = "SELECT (COALESCE (MAX (SeqNo), 0)+10) FROM UNS_PayrollBankFormatLine WHERE UNS_PayrollBankFormat_ID = ?";
			int max = DB.getSQLValue(get_TrxName(), sql, getUNS_PayrollBankFormat_ID());
			setSeqNo(max);
		}
		return super.beforeSave(newRecord);
	}
	
	public String initValue (MUNSPayrollPaymentEmp emp)
	{
		String sourceVal = getSourceValue();
		switch (sourceVal)
		{
			case SOURCEVALUE_AccountNo : 
				return emp.getEmployee().getAccountNo();
			case SOURCEVALUE_HolderName : 
				return emp.getEmployee().getName();
			case SOURCEVALUE_KCP : 
				return emp.getEmployee().getKCPBank();
			case SOURCEVALUE_NIK : 
				return emp.getEmployee().getValue();
			case SOURCEVALUE_TakeHomePay : 
				MColumn column = MColumn.get(emp.getCtx(), MUNSPayrollPaymentEmp.Table_Name, MUNSPayrollPaymentEmp.COLUMNNAME_Amount);
				if (column.isEncrypted() && Util.isEmpty(Env.getContext(Env.getCtx(), MSysConfig.PAYROLL_PASSWORD), true))
					return "********";
				else
					return emp.getAmount().toString();
			default :
				return null;
		}
	}
	
}
