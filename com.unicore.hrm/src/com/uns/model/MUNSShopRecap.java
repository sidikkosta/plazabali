package com.uns.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

public class MUNSShopRecap extends X_UNS_ShopRecap {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 5617080117345961446L;

	protected boolean m_updateSalesTarget = false;

	public MUNSShopRecap(Properties ctx, int UNS_StoreSalesRecap_ID,
			String trxName) {
		super(ctx, UNS_StoreSalesRecap_ID, trxName);
	}

	public MUNSShopRecap(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.PO#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete() {
		Iterator<X_UNS_ShopEmployee> employees = Query
				.get(getCtx(), UNSHRMModelFactory.EXTENSION_ID,
						X_UNS_ShopEmployee.Table_Name, "uns_shoprecap_id = ?",
						get_TrxName()).setParameters(get_ID()).iterate();
		while(employees.hasNext()) {
			employees.next().deleteEx(true);
		}
		return super.beforeDelete();
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.PO#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		MUNSSubregionRecap subregionRecap = (MUNSSubregionRecap) getUNS_SubregionRecap();
		
		if (getShop().getC_BP_Group_ID() != subregionRecap.getSubregion_ID()
				&&
				(subregionRecap.getSubregion2_ID() > 0 &&
				 getShop().getC_BP_Group_ID() != subregionRecap.getSubregion2_ID())) {
			throw new AdempiereException("Shop is not in the same subregion: "
					+ getShop().getName());
		}
		
		if(subregionRecap.getShopType() != null 
				&& !subregionRecap.getShopType().equals(MUNSSubregionRecap.SHOPTYPE_ShopFB))
		{
			if(newRecord)
			{
				String sql = "SELECT shopC.NetSalesTarget FROM UNS_ShopTargetConfig shopC "
						+ " INNER JOIN UNS_SubregionTargetConfig subC "
						+ "		ON shopC.UNS_SubregionTargetConfig_ID=subC.UNS_SubregionTargetConfig_ID"
						+ " INNER JOIN C_Period p ON p.C_Period_ID=subC.C_Period_ID"
						+ " INNER JOIN UNS_SubregionRecap subr ON subr.UNS_SubregionRecap_ID=?"
						+ " WHERE shopC.Shop_ID=? AND p.StartDate <= subr.startDate"
						+ " ORDER BY p.StartDate DESC";
				BigDecimal netSalesTarget = DB.getSQLValueBD(get_TrxName(), sql, 
						getUNS_SubregionRecap_ID(), getShop_ID());
				if (netSalesTarget == null)
					netSalesTarget = BigDecimal.ZERO;
				setNetSalesTarget(netSalesTarget);
				m_updateSalesTarget = true;
			}
			
			if(m_updateSalesTarget || is_ValueChanged(COLUMNNAME_NetSalesTarget)) 
			{
				m_updateSalesTarget = true;
				
				MUNSGlobalIncentivesConfig gic = 
						MUNSGlobalIncentivesConfig.get(getCtx(), subregionRecap.getAD_Org_ID(), 
								subregionRecap.getShopType(), subregionRecap.getC_Period_ID(), 
								get_TrxName());
				if (gic == null)
					throw new AdempiereUserError("Cannot update sales target. "
							+ "Please be sure, you have defined a Global Incentive Configuration "
							+ "for the desired ShopType.");
				BigDecimal individualSalesTarget = getNetSalesTarget();
				
				if(getSalesCount() > 0)
					individualSalesTarget = getNetSalesTarget()
							.divide(BigDecimal.valueOf(getSalesCount()), 2, RoundingMode.HALF_UP);
					
				BigDecimal minSubSalesTarget = getNetSalesTarget()
						.multiply(gic.getMinSubregionTarget().divide(Env.ONEHUNDRED, 4, RoundingMode.HALF_UP));
				BigDecimal minIdvSalesTarget = individualSalesTarget
						.multiply(gic.getMinIndividualTarget().divide(Env.ONEHUNDRED, 4, RoundingMode.HALF_UP));
				
				setIndividualTargetAmt(individualSalesTarget);
				setMinSubSalesTargetAmt(minSubSalesTarget);
				setMinIdvSalesTargetAmt(minIdvSalesTarget);
			}
		}
		
		return super.beforeSave(newRecord);
	} //beforeSave
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{
		if (!success)
			return false;
		
		if(m_updateSalesTarget)
		{
			String sql = "UPDATE UNS_SubregionRecap subr "
					+ " SET NetSalesTarget=(SELECT SUM(shopr.NetSalesTarget) FROM UNS_ShopRecap shopr"
					+ "			WHERE shopr.UNS_SubregionRecap_ID=subr.UNS_SubregionRecap_ID),"
					+ "		MinSubSalesTargetAmt=(SELECT SUM(shopr.MinSubSalesTargetAmt) FROM UNS_ShopRecap shopr"
					+ "			WHERE shopr.UNS_SubregionRecap_ID=subr.UNS_SubregionRecap_ID)"
					+ "  WHERE subr.UNS_SubregionRecap_ID=" + getUNS_SubregionRecap_ID();
			
			int count = DB.executeUpdate(sql, get_TrxName());
			
			if (count <=0) {
				log.saveError("Error", "Failed when updating subregion-recap's sales target.");
				return false;
			}
		}
		
		return true;
	} //afterSave
}