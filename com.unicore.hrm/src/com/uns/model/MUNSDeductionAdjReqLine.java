package com.uns.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class MUNSDeductionAdjReqLine extends X_UNS_DeductionAdjReq_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5235752256430197953L;

	MUNSDeductionAdjReq m_parent = null;
	
	public MUNSDeductionAdjReqLine(Properties ctx,
			int UNS_DeductionAdjReq_Line_ID, String trxName) {
		super(ctx, UNS_DeductionAdjReq_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSDeductionAdjReqLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSDeductionAdjReq getParent() {
		
		if(m_parent == null)
			m_parent = new MUNSDeductionAdjReq(getCtx(), getUNS_DeductionAdjReq_ID(), get_TrxName()); 
		
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		setRemainingAmt(remainingAmt());
		return super.beforeSave(newRecord);
	}
	
	protected BigDecimal remainingAmt() {
		
		BigDecimal remainingAmt = Env.ZERO;
		
		if(getCostBenefitType().equals(COSTBENEFITTYPE_PinjamanKaryawan))
		{
			String sql = "SELECT COALESCE(SUM(LoanAmtLeft),0) FROM UNS_Employee_Loan"
					+ " WHERE UNS_Employee_ID = ? AND DocStatus IN ('CO','CL')";
			remainingAmt = DB.getSQLValueBD(get_TrxName(), sql, getParent().getUNS_Employee_ID());
		}
		else
		{
			String type = null;
			if(getCostBenefitType().equals(COSTBENEFITTYPE_Cooperative))
			{
				type = MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Canteen;
			}
			else if(getCostBenefitType().equals(COSTBENEFITTYPE_Cooperative))
			{
				type = MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Cooperative;
			}
			
			String sql = "SELECT COALESCE(RemainingAmount,0) FROM UNS_PeriodicCostBenefitLine pl"
					+ " INNER JOIN UNS_PeriodicCostBenefit p ON p.UNS_PeriodicCostBenefit_ID = pl.UNS_PeriodicCostBenefit_ID"
					+ " WHERE pl.isActive = 'Y' AND pl.UNS_Employee_ID = ? AND p.C_Period_ID = ?"
					+ " AND p.CostBenefitType = ? AND isBenefit = 'N' AND DocStatus IN ('CO','CL')";
			remainingAmt = DB.getSQLValueBD(
					get_TrxName(), sql, getParent().getUNS_Employee_ID(), getParent().getC_Period_ID()
					, type);
		}
		
		
		return remainingAmt;
	}
	
	public BigDecimal installment() {
		
		BigDecimal amount = Env.ZERO;
		double totalInstallment = 0.0;
		
		List<MUNSEmployeeLoan> loans = MUNSEmployeeLoan.gets(getCtx(), getParent().getUNS_Employee_ID(), get_TrxName());
		
		for (MUNSEmployeeLoan loan : loans)
		{
			if (loan.getLoanAmtLeft().compareTo(BigDecimal.ZERO) <= 0)
				continue;
			if (loan.getLoanAmtLeft().compareTo(loan.getInstallment()) >= 0)
			{
//				if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Koperasi))
//					totalInstallmentKoprasi += employeeLoan.getInstallment().doubleValue();
//				else //if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Company))
				totalInstallment += loan.getInstallment().doubleValue();
			} 
			else 
			{
//				if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Koperasi))
//					totalInstallmentKoprasi += employeeLoan.getLoanAmtLeft().doubleValue();
//				else //if (employeeLoan.getLoanType().equals(MUNSEmployeeLoan.LOANTYPE_Company))
				totalInstallment += loan.getLoanAmtLeft().doubleValue();
			}
		}
		amount = new BigDecimal(totalInstallment).setScale(0, RoundingMode.HALF_UP);
		
		return amount;
		
	}
	
}
