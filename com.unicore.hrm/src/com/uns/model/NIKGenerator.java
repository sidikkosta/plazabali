/**
 * 
 */
package com.uns.model;

import java.math.BigDecimal;
import java.util.Calendar;

import org.compiere.model.MOrg;
import org.compiere.model.MOrgInfo;
import org.compiere.process.ProcessInfo;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.util.MessageBox;

/**
 * @author menjangan
 *
 */
public class NIKGenerator {

	public static final String TYPE_PERMANEN ="N";
	public static final String TYPE_PEMBORONG = "P";
	public static final String TYPE_HARIAN = "H";
	public static final String TYPE_CUSTOMIZATIONNIK = "C";
	private String m_NewNIK = null;

	/**
	 * 
	 */
	public NIKGenerator(MUNSContractRecommendation record, String type, String prefixNIK, String trxName, final ProcessInfo pi) {
		// TODO Auto-generated constructor stub
		if (type.equals(TYPE_PERMANEN))
			generateNIKPermanen(trxName);
		else if (type.equals(TYPE_HARIAN))
			generateNIKHarianBualan(trxName);
		else if (type.equals(TYPE_PEMBORONG))
			generateNIKPemborong(trxName);
		else if (type.equals(TYPE_CUSTOMIZATIONNIK))
			generateCustomizationNIK(record, prefixNIK, trxName, pi);
		else
			throw new AdempiereUserError("UNKNOW TYPE");
	}
	
	/**
	 * Generate NIK For Employee With Contract Permanen
	 * @param ctx
	 * @param trxName
	 */
	private void generateNIKPermanen(String trxName)
	{
		String sql = "SELECT MAX(" 
				+ MUNSEmployee.COLUMNNAME_Value + ") FROM " 
				+ MUNSEmployee.Table_Name + " WHERE SUBSTRING(" 
				+ MUNSEmployee.COLUMNNAME_Value + " FROM 1 FOR 1) <> '" 
				+ TYPE_PEMBORONG + "' AND " + "SUBSTRING(" 
				+ MUNSEmployee.COLUMNNAME_Value + " FROM 1 FOR 1) <> '" + TYPE_HARIAN + "'";
		
		String nik = DB.getSQLValueString(trxName, sql);
		if (null == nik)
			m_NewNIK = "00000";
		else
		{
			Integer nikAsInt = new Integer(nik);
			nikAsInt += 1;
			String numberAsString = nikAsInt.toString();
			for(int i=0; i<5; i++)
			{
				if (numberAsString.length() < 6)
					numberAsString = "0" + numberAsString;
			}
			m_NewNIK = numberAsString;
		}
	}
	
	/**
	 * 
	 * @param trxName
	 */
	private void generateNIKHarianBualan(String trxName)
	{
		String sql =  "SELECT MAX(" 
				+ MUNSEmployee.COLUMNNAME_Value + ") FROM " 
				+ MUNSEmployee.Table_Name + " WHERE SUBSTRING(" 
				+ MUNSEmployee.COLUMNNAME_Value + " FROM 1 FOR 1) = '" 
				+ TYPE_HARIAN + "'";
		
		String nik = DB.getSQLValueString(trxName, sql);
		if (null == nik)
			m_NewNIK = "H000001";
		else
		{
			nik = nik.substring(1, 6);
			Integer nikAsInt = new Integer(nik);
			nikAsInt += 1;
			String numberAsString = nikAsInt.toString();
			for(int i=0; i<5; i++)
			{
				if (numberAsString.length() < 6)
					numberAsString = "0" + numberAsString;
			}
			m_NewNIK = TYPE_HARIAN + numberAsString;
		}
	}
	
	/**
	 * 
	 * @param trxName
	 */
	private void generateNIKPemborong(String trxName)
	{
		String sql =  "SELECT MAX(" 
				+ MUNSEmployee.COLUMNNAME_Value + ") FROM " 
				+ MUNSEmployee.Table_Name + " WHERE SUBSTRING(" 
				+ MUNSEmployee.COLUMNNAME_Value + " FROM 1 FOR 1) = '" 
				+ TYPE_PEMBORONG + "'";
		
		String nik = DB.getSQLValueString(trxName, sql);
		if (null == nik)
			m_NewNIK = "P000001";
		else
		{
			nik = nik.substring(1, 6);
			Integer nikAsInt = new Integer(nik);
			nikAsInt += 1;
			String numberAsString = nikAsInt.toString();
			for(int i=0; i<5; i++)
			{
				if (numberAsString.length() < 6)
					numberAsString = "0" + numberAsString;
			}
			m_NewNIK = TYPE_PEMBORONG + numberAsString;
		}
	}
	
	private void generateCustomizationNIK(MUNSContractRecommendation record, String prefixNIK, String trxName, ProcessInfo pi)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(record.getUNS_Employee().getEntryDate() == null ? record.getDateContractStart()
				: record.getUNS_Employee().getEntryDate());
		String year = String.valueOf(cal.get(Calendar.YEAR));
		year = year.substring(2);
		String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
		if(month.length() == 1)
			month = "0" + month;
		MOrg org = MOrg.get(record.getCtx(), record.getNewDept_ID());
		String orgCode = (MOrgInfo.get(record.getCtx(), org.get_ID(), record.get_TrxName())).getNIKCode();
		if(record.getNewSectionOfDept().getC_BP_Group().getValue().equals("HO"))
			orgCode = "8";
		if(record.getEmploymentType().equals(MUNSContractRecommendation.EMPLOYMENTTYPE_SubContract))
			orgCode = "9";
		String regionCode = org.getC_SalesRegion().getNIKCode();
		String prefix = !Util.isEmpty(prefixNIK, true) ? prefixNIK : orgCode + regionCode;
		String uniqueKey = prefix + year + month;
		if(record.getUNS_Employee().getValue() != null && record.getUNS_Employee().getValue().contains(uniqueKey))
		{
			int answer = MessageBox.showMsg(record.getCtx(), pi, "Previous NIK has have pattern correctly."
					+ " Are you sure to re-generate ?", "Re-Generate Confirm", MessageBox.YESNO, MessageBox.ICONQUESTION);
			if(answer == MessageBox.RETURN_NO || MessageBox.RETURN_CANCEL == answer)
			{
				m_NewNIK = record.getNewNIK();
				return;
			}
		}
		String sql = "SELECT MAX(Value) FROM UNS_Employee WHERE Value Like '" + uniqueKey + "%'";
		String max = DB.getSQLValueString(trxName, sql);
		if(max != null && max.equals(record.getNewNIK()))
		{
			m_NewNIK = record.getNewNIK();
			return;
		}
		String value = null;
		if(!Util.isEmpty(max, true))
		{
			BigDecimal nextCode = Env.ONE.add(new BigDecimal(max));
			nextCode = nextCode.setScale(0);
			value = String.valueOf(nextCode);
		}
		else
			value = uniqueKey + "0001";
		
		m_NewNIK = value;
	}
	
	/**
	 * 
	 * @return String Of NIK
	 */
	public String getNewNIK()
	{
		return m_NewNIK;
	}
}