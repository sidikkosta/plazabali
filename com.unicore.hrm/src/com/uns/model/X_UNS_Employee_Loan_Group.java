/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_Employee_Loan_Group
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Employee_Loan_Group extends PO implements I_UNS_Employee_Loan_Group, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191128L;

    /** Standard Constructor */
    public X_UNS_Employee_Loan_Group (Properties ctx, int UNS_Employee_Loan_Group_ID, String trxName)
    {
      super (ctx, UNS_Employee_Loan_Group_ID, trxName);
      /** if (UNS_Employee_Loan_Group_ID == 0)
        {
			setC_Charge_ID (0);
			setC_DocType_ID (0);
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setLoanType (null);
// CMP
			setProcessed (false);
// N
			setTotalLoanAmt (Env.ZERO);
// 0
			setTotalRecord (0);
// 0
			setTrxDate (new Timestamp( System.currentTimeMillis() ));
			setUNS_Employee_Loan_Group_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Employee_Loan_Group (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Employee_Loan_Group[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Company = CMP */
	public static final String LOANTYPE_Company = "CMP";
	/** Koperasi = KPR */
	public static final String LOANTYPE_Koperasi = "KPR";
	/** Medical = MED */
	public static final String LOANTYPE_Medical = "MED";
	/** Short Cashier = SHC */
	public static final String LOANTYPE_ShortCashier = "SHC";
	/** Incident Report = ICR */
	public static final String LOANTYPE_IncidentReport = "ICR";
	/** Set Loan Type.
		@param LoanType Loan Type	  */
	public void setLoanType (String LoanType)
	{

		set_Value (COLUMNNAME_LoanType, LoanType);
	}

	/** Get Loan Type.
		@return Loan Type	  */
	public String getLoanType () 
	{
		return (String)get_Value(COLUMNNAME_LoanType);
	}

	/** Set Note.
		@param Note 
		Optional additional user defined information
	  */
	public void setNote (String Note)
	{
		set_Value (COLUMNNAME_Note, Note);
	}

	/** Get Note.
		@return Optional additional user defined information
	  */
	public String getNote () 
	{
		return (String)get_Value(COLUMNNAME_Note);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Total Loan Amt.
		@param TotalLoanAmt Total Loan Amt	  */
	public void setTotalLoanAmt (BigDecimal TotalLoanAmt)
	{
		set_Value (COLUMNNAME_TotalLoanAmt, TotalLoanAmt);
	}

	/** Get Total Loan Amt.
		@return Total Loan Amt	  */
	public BigDecimal getTotalLoanAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLoanAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Record.
		@param TotalRecord Total Record	  */
	public void setTotalRecord (int TotalRecord)
	{
		set_Value (COLUMNNAME_TotalRecord, Integer.valueOf(TotalRecord));
	}

	/** Get Total Record.
		@return Total Record	  */
	public int getTotalRecord () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalRecord);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Trx Date.
		@param TrxDate Trx Date	  */
	public void setTrxDate (Timestamp TrxDate)
	{
		set_Value (COLUMNNAME_TrxDate, TrxDate);
	}

	/** Get Trx Date.
		@return Trx Date	  */
	public Timestamp getTrxDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TrxDate);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getTrxDate()));
    }

	/** Set Employee Loan Group.
		@param UNS_Employee_Loan_Group_ID Employee Loan Group	  */
	public void setUNS_Employee_Loan_Group_ID (int UNS_Employee_Loan_Group_ID)
	{
		if (UNS_Employee_Loan_Group_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_Loan_Group_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Employee_Loan_Group_ID, Integer.valueOf(UNS_Employee_Loan_Group_ID));
	}

	/** Get Employee Loan Group.
		@return Employee Loan Group	  */
	public int getUNS_Employee_Loan_Group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_Loan_Group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Employee_Loan_Group_UU.
		@param UNS_Employee_Loan_Group_UU UNS_Employee_Loan_Group_UU	  */
	public void setUNS_Employee_Loan_Group_UU (String UNS_Employee_Loan_Group_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Employee_Loan_Group_UU, UNS_Employee_Loan_Group_UU);
	}

	/** Get UNS_Employee_Loan_Group_UU.
		@return UNS_Employee_Loan_Group_UU	  */
	public String getUNS_Employee_Loan_Group_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Employee_Loan_Group_UU);
	}
}