/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;
import org.compiere.model.MClient;
import org.compiere.model.MSysConfig;
import org.compiere.util.CCache;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.SecureEngine;
import com.uns.base.model.Query;
import com.uns.model.factory.UNSHRMModelFactory;

/**
 * @author nurse
 *
 */
public class MUNSUserAccess extends X_UNS_UserAccess 
{
	private static CCache<Integer, MUNSUserAccess> m_cache = new CCache<>(Table_Name, 20);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1696353949292509000L;

	/**
	 * @param ctx
	 * @param UNS_UserAccess_ID
	 * @param trxName
	 */
	public MUNSUserAccess(Properties ctx, int UNS_UserAccess_ID, String trxName) 
	{
		super(ctx, UNS_UserAccess_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUserAccess(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	
	public static synchronized MUNSUserAccess get (int userID, Timestamp date, String trxName)
	{
		MUNSUserAccess access = m_cache.get(userID);
		if (access == null)
		{
			String where = "AD_User_ID = ? AND ? BETWEEN ValidFrom AND ValidTo AND IsExpired = ?";
			access = Query.get(
					Env.getCtx(), UNSHRMModelFactory.EXTENSION_ID, Table_Name,where , trxName).
					setParameters(userID, date, "N").firstOnly();
		}
		return access;
	}
	
	public static synchronized void registerPrivateKeyForUser (int userID, Timestamp date, String trxName)
	{
		if (userID == 0 || date == null)
		{
			Env.getCtx().remove(MSysConfig.PAYROLL_PASSWORD);
			return;
		}
		
		MUNSUserAccess access = get(userID, date, trxName);
		if (access == null)
		{
			Env.getCtx().remove(MSysConfig.PAYROLL_PASSWORD);
			return;
		}
		int clientID = Env.getAD_Client_ID(Env.getCtx());
		String encPasswd = access.getPassword();
		String decPasswd = SecureEngine.decrypt(encPasswd, clientID);
		String[] pwdMilis = decPasswd.split("#");
		if (pwdMilis == null || pwdMilis.length != 3)
		{
			Env.getCtx().remove(MSysConfig.PAYROLL_PASSWORD);
			access.setIsExpired(true);
			access.save();
			return;
		}
		String password = pwdMilis[0];
		String strMilisStart = pwdMilis[1];
		String strMilisEnd = pwdMilis[2];
		long longMilisStart = Long.valueOf(strMilisStart);
		long longMilisEnd = Long.valueOf(strMilisEnd);
		Timestamp validFrom = new Timestamp(longMilisStart);
		Timestamp validTo = new Timestamp(longMilisEnd);
		if (!validFrom.equals(access.getValidFrom()) || !validTo.equals(access.getValidTo()))
		{
			Env.getCtx().remove(MSysConfig.PAYROLL_PASSWORD);
			access.setIsExpired(true);
			access.save();
			return;
		}
	

		String sysPassword = MClient.get(Env.getCtx()).getPayrollPassword();
		sysPassword = SecureEngine.decrypt(sysPassword, clientID);
		
		if (!sysPassword.equals(password))
		{
			Env.getCtx().remove(MSysConfig.PAYROLL_PASSWORD);
			access.setIsExpired(true);
			access.save();
			return;
		}
		
		Env.setContext(Env.getCtx(), MSysConfig.PAYROLL_PASSWORD, password);
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (getValidTo().before(getValidFrom()))
		{
			log.log(Level.SEVERE, "Valid To Before Valid From");
			return false;
		}
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (!isExpired())
		{
			String sql = "UPDATE UNS_UserAccess Set IsExpired = 'Y' WHERE IsExpired = 'N' AND UNS_UserAccess_ID <> ? AND AD_User_ID = ?";
			int ok = DB.executeUpdate(sql, new Object[]{getUNS_UserAccess_ID(),getAD_User_ID()}, false, get_TrxName());
			if (ok == -1)
			{
				log.log(Level.SEVERE, "Failed when try to update existing user access");
				return false;
			}
		}
		return super.afterSave(newRecord, success);
	}
	
	public static boolean setExpired (String trxName)
	{
		String sql = "UPDATE UNS_UserAccess SET IsExpired = 'Y' WHERE IsExpired = 'N' AND ValidTo < NOW()";
		int result = DB.executeUpdate(sql, false, trxName);
		return result != -1;
	}
}