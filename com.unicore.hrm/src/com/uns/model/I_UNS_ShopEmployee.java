/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ShopEmployee
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ShopEmployee 
{

    /** TableName=UNS_ShopEmployee */
    public static final String Table_Name = "UNS_ShopEmployee";

    /** AD_Table_ID=1000450 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name CalculationClass */
    public static final String COLUMNNAME_CalculationClass = "CalculationClass";

	/** Set Calculation Class.
	  * Java Class for calculation, implementing Interface Measure
	  */
	public void setCalculationClass (String CalculationClass);

	/** Get Calculation Class.
	  * Java Class for calculation, implementing Interface Measure
	  */
	public String getCalculationClass();

    /** Column name CashierCount */
    public static final String COLUMNNAME_CashierCount = "CashierCount";

	/** Set Cashier Count.
	  * Total count an employee be a cashier
	  */
	public void setCashierCount (BigDecimal CashierCount);

	/** Get Cashier Count.
	  * Total count an employee be a cashier
	  */
	public BigDecimal getCashierCount();

    /** Column name CashierIncentiveAmt */
    public static final String COLUMNNAME_CashierIncentiveAmt = "CashierIncentiveAmt";

	/** Set Cashier Incentive	  */
	public void setCashierIncentiveAmt (BigDecimal CashierIncentiveAmt);

	/** Get Cashier Incentive	  */
	public BigDecimal getCashierIncentiveAmt();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name GroupIncentiveAmt */
    public static final String COLUMNNAME_GroupIncentiveAmt = "GroupIncentiveAmt";

	/** Set Group Incentive	  */
	public void setGroupIncentiveAmt (BigDecimal GroupIncentiveAmt);

	/** Get Group Incentive	  */
	public BigDecimal getGroupIncentiveAmt();

    /** Column name IndividualTargetAmt */
    public static final String COLUMNNAME_IndividualTargetAmt = "IndividualTargetAmt";

	/** Set Individual Target.
	  * Individual Target Amount
	  */
	public void setIndividualTargetAmt (BigDecimal IndividualTargetAmt);

	/** Get Individual Target.
	  * Individual Target Amount
	  */
	public BigDecimal getIndividualTargetAmt();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsMasterSchedule */
    public static final String COLUMNNAME_IsMasterSchedule = "IsMasterSchedule";

	/** Set Master Schedule.
	  * To indicate if it is a master schedule.
	  */
	public void setIsMasterSchedule (boolean IsMasterSchedule);

	/** Get Master Schedule.
	  * To indicate if it is a master schedule.
	  */
	public boolean isMasterSchedule();

    /** Column name MinIdvSalesTargetAmt */
    public static final String COLUMNNAME_MinIdvSalesTargetAmt = "MinIdvSalesTargetAmt";

	/** Set Min. Individual Sales Target.
	  * Minimum target amount for each sales person
	  */
	public void setMinIdvSalesTargetAmt (BigDecimal MinIdvSalesTargetAmt);

	/** Get Min. Individual Sales Target.
	  * Minimum target amount for each sales person
	  */
	public BigDecimal getMinIdvSalesTargetAmt();

    /** Column name NetSalesRealizationAmt */
    public static final String COLUMNNAME_NetSalesRealizationAmt = "NetSalesRealizationAmt";

	/** Set Net Sales Realization.
	  * Total realized net sales within 1 month from all stores
	  */
	public void setNetSalesRealizationAmt (BigDecimal NetSalesRealizationAmt);

	/** Get Net Sales Realization.
	  * Total realized net sales within 1 month from all stores
	  */
	public BigDecimal getNetSalesRealizationAmt();

    /** Column name NotPresentSalesAmt */
    public static final String COLUMNNAME_NotPresentSalesAmt = "NotPresentSalesAmt";

	/** Set Not Present Sales.
	  * Amount in a defined currency
	  */
	public void setNotPresentSalesAmt (BigDecimal NotPresentSalesAmt);

	/** Get Not Present Sales.
	  * Amount in a defined currency
	  */
	public BigDecimal getNotPresentSalesAmt();

    /** Column name Point */
    public static final String COLUMNNAME_Point = "Point";

	/** Set Point	  */
	public void setPoint (BigDecimal Point);

	/** Get Point	  */
	public BigDecimal getPoint();

    /** Column name PositionType */
    public static final String COLUMNNAME_PositionType = "PositionType";

	/** Set Position Type	  */
	public void setPositionType (String PositionType);

	/** Get Position Type	  */
	public String getPositionType();

    /** Column name PrevShop_ID */
    public static final String COLUMNNAME_PrevShop_ID = "PrevShop_ID";

	/** Set Previous Shop.
	  * Identifies a Shop
	  */
	public void setPrevShop_ID (int PrevShop_ID);

	/** Get Previous Shop.
	  * Identifies a Shop
	  */
	public int getPrevShop_ID();

	public org.compiere.model.I_C_BPartner getPrevShop() throws RuntimeException;

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name PushMoneyAmt */
    public static final String COLUMNNAME_PushMoneyAmt = "PushMoneyAmt";

	/** Set Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt);

	/** Get Push Money Amount	  */
	public BigDecimal getPushMoneyAmt();

    /** Column name SalesIncentiveAmt */
    public static final String COLUMNNAME_SalesIncentiveAmt = "SalesIncentiveAmt";

	/** Set Sales Incentive.
	  * Group incentives amount for a sales.
	  */
	public void setSalesIncentiveAmt (BigDecimal SalesIncentiveAmt);

	/** Get Sales Incentive.
	  * Group incentives amount for a sales.
	  */
	public BigDecimal getSalesIncentiveAmt();

    /** Column name Shop_ID */
    public static final String COLUMNNAME_Shop_ID = "Shop_ID";

	/** Set Shop.
	  * Identifies a Shop
	  */
	public void setShop_ID (int Shop_ID);

	/** Get Shop.
	  * Identifies a Shop
	  */
	public int getShop_ID();

	public org.compiere.model.I_C_BPartner getShop() throws RuntimeException;

    /** Column name TandemCount */
    public static final String COLUMNNAME_TandemCount = "TandemCount";

	/** Set Tandem Count.
	  * Total count an employee be a tandem of cashier
	  */
	public void setTandemCount (BigDecimal TandemCount);

	/** Get Tandem Count.
	  * Total count an employee be a tandem of cashier
	  */
	public BigDecimal getTandemCount();

    /** Column name TotalCuti */
    public static final String COLUMNNAME_TotalCuti = "TotalCuti";

	/** Set TotalCuti	  */
	public void setTotalCuti (int TotalCuti);

	/** Get TotalCuti	  */
	public int getTotalCuti();

    /** Column name TotalCuti_CH */
    public static final String COLUMNNAME_TotalCuti_CH = "TotalCuti_CH";

	/** Set Total Cuti Hamil	  */
	public void setTotalCuti_CH (int TotalCuti_CH);

	/** Get Total Cuti Hamil	  */
	public int getTotalCuti_CH();

    /** Column name TotalCuti_CP */
    public static final String COLUMNNAME_TotalCuti_CP = "TotalCuti_CP";

	/** Set Total Cuti Panjang	  */
	public void setTotalCuti_CP (int TotalCuti_CP);

	/** Get Total Cuti Panjang	  */
	public int getTotalCuti_CP();

    /** Column name TotalCuti_CT */
    public static final String COLUMNNAME_TotalCuti_CT = "TotalCuti_CT";

	/** Set Total Cuti Tahunan	  */
	public void setTotalCuti_CT (int TotalCuti_CT);

	/** Get Total Cuti Tahunan	  */
	public int getTotalCuti_CT();

    /** Column name TotalCuti_CTB */
    public static final String COLUMNNAME_TotalCuti_CTB = "TotalCuti_CTB";

	/** Set Total Cuti Tanpa Bayar	  */
	public void setTotalCuti_CTB (int TotalCuti_CTB);

	/** Get Total Cuti Tanpa Bayar	  */
	public int getTotalCuti_CTB();

    /** Column name TotalLibur */
    public static final String COLUMNNAME_TotalLibur = "TotalLibur";

	/** Set TotalLibur	  */
	public void setTotalLibur (int TotalLibur);

	/** Get TotalLibur	  */
	public int getTotalLibur();

    /** Column name TotalLibur_L */
    public static final String COLUMNNAME_TotalLibur_L = "TotalLibur_L";

	/** Set Total Libur Mingguan	  */
	public void setTotalLibur_L (int TotalLibur_L);

	/** Get Total Libur Mingguan	  */
	public int getTotalLibur_L();

    /** Column name TotalLibur_LU */
    public static final String COLUMNNAME_TotalLibur_LU = "TotalLibur_LU";

	/** Set Total Libur Umum/Nasional	  */
	public void setTotalLibur_LU (int TotalLibur_LU);

	/** Get Total Libur Umum/Nasional	  */
	public int getTotalLibur_LU();

    /** Column name TotalShift1 */
    public static final String COLUMNNAME_TotalShift1 = "TotalShift1";

	/** Set TotalShift1	  */
	public void setTotalShift1 (int TotalShift1);

	/** Get TotalShift1	  */
	public int getTotalShift1();

    /** Column name TotalShift2 */
    public static final String COLUMNNAME_TotalShift2 = "TotalShift2";

	/** Set TotalShift2	  */
	public void setTotalShift2 (int TotalShift2);

	/** Get TotalShift2	  */
	public int getTotalShift2();

    /** Column name TotalShift3 */
    public static final String COLUMNNAME_TotalShift3 = "TotalShift3";

	/** Set TotalShift3	  */
	public void setTotalShift3 (int TotalShift3);

	/** Get TotalShift3	  */
	public int getTotalShift3();

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

	public com.uns.model.I_UNS_Employee getUNS_Employee() throws RuntimeException;

    /** Column name UNS_ShopEmployee_ID */
    public static final String COLUMNNAME_UNS_ShopEmployee_ID = "UNS_ShopEmployee_ID";

	/** Set Personal Sales Recapitulation	  */
	public void setUNS_ShopEmployee_ID (int UNS_ShopEmployee_ID);

	/** Get Personal Sales Recapitulation	  */
	public int getUNS_ShopEmployee_ID();

    /** Column name UNS_ShopEmployee_UU */
    public static final String COLUMNNAME_UNS_ShopEmployee_UU = "UNS_ShopEmployee_UU";

	/** Set UNS_ShopEmployee_UU	  */
	public void setUNS_ShopEmployee_UU (String UNS_ShopEmployee_UU);

	/** Get UNS_ShopEmployee_UU	  */
	public String getUNS_ShopEmployee_UU();

    /** Column name UNS_ShopRecap_ID */
    public static final String COLUMNNAME_UNS_ShopRecap_ID = "UNS_ShopRecap_ID";

	/** Set Shop Sales Recapitulation	  */
	public void setUNS_ShopRecap_ID (int UNS_ShopRecap_ID);

	/** Get Shop Sales Recapitulation	  */
	public int getUNS_ShopRecap_ID();

	public com.uns.model.I_UNS_ShopRecap getUNS_ShopRecap() throws RuntimeException;

    /** Column name UNS_SubregionRecap_ID */
    public static final String COLUMNNAME_UNS_SubregionRecap_ID = "UNS_SubregionRecap_ID";

	/** Set Subregion Recapitulation	  */
	public void setUNS_SubregionRecap_ID (int UNS_SubregionRecap_ID);

	/** Get Subregion Recapitulation	  */
	public int getUNS_SubregionRecap_ID();

	public com.uns.model.I_UNS_SubregionRecap getUNS_SubregionRecap() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
