/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

/**
 * @author dpitaloka
 *
 */
public class MUNSStaffSrvChgRecap extends X_UNS_StaffSrvChgRecap {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5959713103049710930L;

	public MUNSStaffSrvChgRecap(Properties ctx, int UNS_StaffSrvChgRecap_ID,
			String trxName) {
		super(ctx, UNS_StaffSrvChgRecap_ID, trxName);
	}

	public MUNSStaffSrvChgRecap(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean afterSave(boolean newRecord, boolean success) 
	{
		if (!success)
			return false;
		
		String sql = "UPDATE UNS_ShopRecap shop SET "
				+ "  TotalPoint=("
				+ "			SELECT COALESCE(SUM(staff.point), 0) FROM UNS_StaffSrvChgRecap staff"
				+ "			WHERE staff.UNS_ShopRecap_ID=shop.UNS_ShopRecap_ID "
				+ "				AND staff.PositionType IS NOT NULL AND staff.PositionType<>'SUPT')"
				+ ", SupportPoints=("
				+ "			SELECT COALESCE(SUM(staff.point), 0) FROM UNS_StaffSrvChgRecap staff"
				+ "			WHERE staff.UNS_ShopRecap_ID=shop.UNS_ShopRecap_ID "
				+ "				AND (staff.PositionType IS NULL OR staff.PositionType='SUPT'))"
				+ "  WHERE shop.UNS_ShopRecap_ID=" + getUNS_ShopRecap_ID();
		
		int count = DB.executeUpdate(sql, get_TrxName());
		
		if (count <=0) {
			log.saveError("Error", "Failed when updating shop-recap's total points.");
			return false;
		}
		
		return true;
	} //afterSave
}
