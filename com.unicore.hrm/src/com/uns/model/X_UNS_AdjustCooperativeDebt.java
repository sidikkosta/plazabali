/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_AdjustCooperativeDebt
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_AdjustCooperativeDebt extends PO implements I_UNS_AdjustCooperativeDebt, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180718L;

    /** Standard Constructor */
    public X_UNS_AdjustCooperativeDebt (Properties ctx, int UNS_AdjustCooperativeDebt_ID, String trxName)
    {
      super (ctx, UNS_AdjustCooperativeDebt_ID, trxName);
      /** if (UNS_AdjustCooperativeDebt_ID == 0)
        {
			setAllocatedAmt (Env.ZERO);
// 0
			setC_Period_ID (0);
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setInstallmentAmt (Env.ZERO);
// 0
			setLogisticAmt (Env.ZERO);
// 0
			setProcessed (false);
			setTotalDebt (Env.ZERO);
// 0
			setUNS_AdjustCooperativeDebt_ID (0);
			setUNS_StoreBilling_ID (0);
			setUNS_StoreCustomer_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_AdjustCooperativeDebt (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_AdjustCooperativeDebt[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Allocated Amountt.
		@param AllocatedAmt 
		Amount allocated to this document
	  */
	public void setAllocatedAmt (BigDecimal AllocatedAmt)
	{
		set_Value (COLUMNNAME_AllocatedAmt, AllocatedAmt);
	}

	/** Get Allocated Amountt.
		@return Amount allocated to this document
	  */
	public BigDecimal getAllocatedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AllocatedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Installment Amount.
		@param InstallmentAmt Installment Amount	  */
	public void setInstallmentAmt (BigDecimal InstallmentAmt)
	{
		set_Value (COLUMNNAME_InstallmentAmt, InstallmentAmt);
	}

	/** Get Installment Amount.
		@return Installment Amount	  */
	public BigDecimal getInstallmentAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_InstallmentAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Logistic Amount.
		@param LogisticAmt Logistic Amount	  */
	public void setLogisticAmt (BigDecimal LogisticAmt)
	{
		set_Value (COLUMNNAME_LogisticAmt, LogisticAmt);
	}

	/** Get Logistic Amount.
		@return Logistic Amount	  */
	public BigDecimal getLogisticAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LogisticAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Total Debt.
		@param TotalDebt Total Debt	  */
	public void setTotalDebt (BigDecimal TotalDebt)
	{
		set_Value (COLUMNNAME_TotalDebt, TotalDebt);
	}

	/** Get Total Debt.
		@return Total Debt	  */
	public BigDecimal getTotalDebt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalDebt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Adjust Cooperative Debt.
		@param UNS_AdjustCooperativeDebt_ID Adjust Cooperative Debt	  */
	public void setUNS_AdjustCooperativeDebt_ID (int UNS_AdjustCooperativeDebt_ID)
	{
		if (UNS_AdjustCooperativeDebt_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AdjustCooperativeDebt_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AdjustCooperativeDebt_ID, Integer.valueOf(UNS_AdjustCooperativeDebt_ID));
	}

	/** Get Adjust Cooperative Debt.
		@return Adjust Cooperative Debt	  */
	public int getUNS_AdjustCooperativeDebt_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AdjustCooperativeDebt_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_AdjustCooperativeDebt_UU.
		@param UNS_AdjustCooperativeDebt_UU UNS_AdjustCooperativeDebt_UU	  */
	public void setUNS_AdjustCooperativeDebt_UU (String UNS_AdjustCooperativeDebt_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_AdjustCooperativeDebt_UU, UNS_AdjustCooperativeDebt_UU);
	}

	/** Get UNS_AdjustCooperativeDebt_UU.
		@return UNS_AdjustCooperativeDebt_UU	  */
	public String getUNS_AdjustCooperativeDebt_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_AdjustCooperativeDebt_UU);
	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Store Billing's.
		@param UNS_StoreBilling_ID Store Billing's	  */
	public void setUNS_StoreBilling_ID (int UNS_StoreBilling_ID)
	{
		if (UNS_StoreBilling_ID < 1) 
			set_Value (COLUMNNAME_UNS_StoreBilling_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_StoreBilling_ID, Integer.valueOf(UNS_StoreBilling_ID));
	}

	/** Get Store Billing's.
		@return Store Billing's	  */
	public int getUNS_StoreBilling_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StoreBilling_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Store Customer.
		@param UNS_StoreCustomer_ID Store Customer	  */
	public void setUNS_StoreCustomer_ID (int UNS_StoreCustomer_ID)
	{
		if (UNS_StoreCustomer_ID < 1) 
			set_Value (COLUMNNAME_UNS_StoreCustomer_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_StoreCustomer_ID, Integer.valueOf(UNS_StoreCustomer_ID));
	}

	/** Get Store Customer.
		@return Store Customer	  */
	public int getUNS_StoreCustomer_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StoreCustomer_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}