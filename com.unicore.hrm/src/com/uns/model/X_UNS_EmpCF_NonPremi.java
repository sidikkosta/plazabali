/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.uns.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_EmpCF_NonPremi
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EmpCF_NonPremi extends PO implements I_UNS_EmpCF_NonPremi, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180927L;

    /** Standard Constructor */
    public X_UNS_EmpCF_NonPremi (Properties ctx, int UNS_EmpCF_NonPremi_ID, String trxName)
    {
      super (ctx, UNS_EmpCF_NonPremi_ID, trxName);
      /** if (UNS_EmpCF_NonPremi_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_EmpCF_NonPremi (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EmpCF_NonPremi[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Disbursed.
		@param isDisbursed Disbursed	  */
	public void setisDisbursed (boolean isDisbursed)
	{
		set_Value (COLUMNNAME_isDisbursed, Boolean.valueOf(isDisbursed));
	}

	/** Get Disbursed.
		@return Disbursed	  */
	public boolean isDisbursed () 
	{
		Object oo = get_Value(COLUMNNAME_isDisbursed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Job Description.
		@param JobDescription Job Description	  */
	public void setJobDescription (String JobDescription)
	{
		set_Value (COLUMNNAME_JobDescription, JobDescription);
	}

	/** Get Job Description.
		@return Job Description	  */
	public String getJobDescription () 
	{
		return (String)get_Value(COLUMNNAME_JobDescription);
	}

	public com.uns.model.I_UNS_CrossFunction_NonPremi getUNS_CrossFunction_NonPremi() throws RuntimeException
    {
		return (com.uns.model.I_UNS_CrossFunction_NonPremi)MTable.get(getCtx(), com.uns.model.I_UNS_CrossFunction_NonPremi.Table_Name)
			.getPO(getUNS_CrossFunction_NonPremi_ID(), get_TrxName());	}

	/** Set Cross Function Non Premi.
		@param UNS_CrossFunction_NonPremi_ID Cross Function Non Premi	  */
	public void setUNS_CrossFunction_NonPremi_ID (int UNS_CrossFunction_NonPremi_ID)
	{
		if (UNS_CrossFunction_NonPremi_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CrossFunction_NonPremi_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CrossFunction_NonPremi_ID, Integer.valueOf(UNS_CrossFunction_NonPremi_ID));
	}

	/** Get Cross Function Non Premi.
		@return Cross Function Non Premi	  */
	public int getUNS_CrossFunction_NonPremi_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CrossFunction_NonPremi_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cross Employee.
		@param UNS_EmpCF_NonPremi_ID Cross Employee	  */
	public void setUNS_EmpCF_NonPremi_ID (int UNS_EmpCF_NonPremi_ID)
	{
		if (UNS_EmpCF_NonPremi_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EmpCF_NonPremi_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EmpCF_NonPremi_ID, Integer.valueOf(UNS_EmpCF_NonPremi_ID));
	}

	/** Get Cross Employee.
		@return Cross Employee	  */
	public int getUNS_EmpCF_NonPremi_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EmpCF_NonPremi_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EmpCF_NonPremi_UU.
		@param UNS_EmpCF_NonPremi_UU UNS_EmpCF_NonPremi_UU	  */
	public void setUNS_EmpCF_NonPremi_UU (String UNS_EmpCF_NonPremi_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EmpCF_NonPremi_UU, UNS_EmpCF_NonPremi_UU);
	}

	/** Get UNS_EmpCF_NonPremi_UU.
		@return UNS_EmpCF_NonPremi_UU	  */
	public String getUNS_EmpCF_NonPremi_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EmpCF_NonPremi_UU);
	}

	public com.uns.model.I_UNS_CrossFunction_NonPremi getUNS_Employee() throws RuntimeException
    {
		return (com.uns.model.I_UNS_CrossFunction_NonPremi)MTable.get(getCtx(), com.uns.model.I_UNS_CrossFunction_NonPremi.Table_Name)
			.getPO(getUNS_Employee_ID(), get_TrxName());	}

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Detail.
		@param UNS_PeriodicCostBenefitLine_ID Detail	  */
	public void setUNS_PeriodicCostBenefitLine_ID (int UNS_PeriodicCostBenefitLine_ID)
	{
		if (UNS_PeriodicCostBenefitLine_ID < 1) 
			set_Value (COLUMNNAME_UNS_PeriodicCostBenefitLine_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_PeriodicCostBenefitLine_ID, Integer.valueOf(UNS_PeriodicCostBenefitLine_ID));
	}

	/** Get Detail.
		@return Detail	  */
	public int getUNS_PeriodicCostBenefitLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PeriodicCostBenefitLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}