/**
 * 
 */
package com.uns.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MColumn;
import org.compiere.model.MTable;

/**
 * @author Burhani Adam
 *
 */
public class MUNSUpdateContractHistory extends X_UNS_UpdateContractHistory {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4516553793181982128L;

	/**
	 * @param ctx
	 * @param UNS_UpdateContractHistory_ID
	 * @param trxName
	 */
	public MUNSUpdateContractHistory(Properties ctx,
			int UNS_UpdateContractHistory_ID, String trxName) {
		super(ctx, UNS_UpdateContractHistory_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUpdateContractHistory(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if((newRecord || is_ValueChanged(COLUMNNAME_UNS_Employee_ID)) && getUNS_Employee_ID() > 0)
		{
			MTable table = MTable.get(getCtx(), Table_Name);
			MColumn[] columns = table.getColumns(false);
			MUNSContractRecommendation cr = MUNSContractRecommendation.getOf(getCtx(), getUNS_Employee_ID(), get_TrxName());
			
			for(int i=0;i<columns.length;i++)
			{
				if(!columns[i].isAllowCopy())
					continue;
				
				String columnName = columns[i].getColumnName();
				set_ValueOfColumn(columnName, cr.get_Value(columnName));
			}
		}
		
		return true;
	}
}