/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.uns.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_BPJSConfig
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_BPJSConfig 
{

    /** TableName=UNS_BPJSConfig */
    public static final String Table_Name = "UNS_BPJSConfig";

    /** AD_Table_ID=1000482 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Job_ID */
    public static final String COLUMNNAME_C_Job_ID = "C_Job_ID";

	/** Set Position.
	  * Job Position
	  */
	public void setC_Job_ID (int C_Job_ID);

	/** Get Position.
	  * Job Position
	  */
	public int getC_Job_ID();

	public org.compiere.model.I_C_Job getC_Job() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name JHTBasicCalculation */
    public static final String COLUMNNAME_JHTBasicCalculation = "JHTBasicCalculation";

	/** Set JHT Basic Calculation	  */
	public void setJHTBasicCalculation (String JHTBasicCalculation);

	/** Get JHT Basic Calculation	  */
	public String getJHTBasicCalculation();

    /** Column name JHTPaidByCompany */
    public static final String COLUMNNAME_JHTPaidByCompany = "JHTPaidByCompany";

	/** Set JHT Paid By Comp(%).
	  * JHT Paid By Company in percent
	  */
	public void setJHTPaidByCompany (BigDecimal JHTPaidByCompany);

	/** Get JHT Paid By Comp(%).
	  * JHT Paid By Company in percent
	  */
	public BigDecimal getJHTPaidByCompany();

    /** Column name JHTPaidByEmployee */
    public static final String COLUMNNAME_JHTPaidByEmployee = "JHTPaidByEmployee";

	/** Set JHT Paid By Employee (%).
	  * JHT Paid By Employee In Percent
	  */
	public void setJHTPaidByEmployee (BigDecimal JHTPaidByEmployee);

	/** Get JHT Paid By Employee (%).
	  * JHT Paid By Employee In Percent
	  */
	public BigDecimal getJHTPaidByEmployee();

    /** Column name JKBasicCalculation */
    public static final String COLUMNNAME_JKBasicCalculation = "JKBasicCalculation";

	/** Set JK Basic Calculation	  */
	public void setJKBasicCalculation (String JKBasicCalculation);

	/** Get JK Basic Calculation	  */
	public String getJKBasicCalculation();

    /** Column name JKKBasicCalculation */
    public static final String COLUMNNAME_JKKBasicCalculation = "JKKBasicCalculation";

	/** Set JKK Basic Calculation	  */
	public void setJKKBasicCalculation (String JKKBasicCalculation);

	/** Get JKK Basic Calculation	  */
	public String getJKKBasicCalculation();

    /** Column name JKKPaidByCompany */
    public static final String COLUMNNAME_JKKPaidByCompany = "JKKPaidByCompany";

	/** Set JKK Paid By Comp(%).
	  * JKK Paid By Company in percent
	  */
	public void setJKKPaidByCompany (BigDecimal JKKPaidByCompany);

	/** Get JKK Paid By Comp(%).
	  * JKK Paid By Company in percent
	  */
	public BigDecimal getJKKPaidByCompany();

    /** Column name JKKPaidByEmployee */
    public static final String COLUMNNAME_JKKPaidByEmployee = "JKKPaidByEmployee";

	/** Set JKK Paid By Employee (%).
	  * JKK Paid By Employee In Percent
	  */
	public void setJKKPaidByEmployee (BigDecimal JKKPaidByEmployee);

	/** Get JKK Paid By Employee (%).
	  * JKK Paid By Employee In Percent
	  */
	public BigDecimal getJKKPaidByEmployee();

    /** Column name JKPaidByCompany */
    public static final String COLUMNNAME_JKPaidByCompany = "JKPaidByCompany";

	/** Set JK Paid By Comp(%).
	  * JK Paid By Company in percent
	  */
	public void setJKPaidByCompany (BigDecimal JKPaidByCompany);

	/** Get JK Paid By Comp(%).
	  * JK Paid By Company in percent
	  */
	public BigDecimal getJKPaidByCompany();

    /** Column name JKPaidByEmployee */
    public static final String COLUMNNAME_JKPaidByEmployee = "JKPaidByEmployee";

	/** Set JK Paid By Employee (%).
	  * JK Paid By Employee In Percent
	  */
	public void setJKPaidByEmployee (BigDecimal JKPaidByEmployee);

	/** Get JK Paid By Employee (%).
	  * JK Paid By Employee In Percent
	  */
	public BigDecimal getJKPaidByEmployee();

    /** Column name JPBasicCalculation */
    public static final String COLUMNNAME_JPBasicCalculation = "JPBasicCalculation";

	/** Set JP Basic Calculation	  */
	public void setJPBasicCalculation (String JPBasicCalculation);

	/** Get JP Basic Calculation	  */
	public String getJPBasicCalculation();

    /** Column name JPKBasicCalculation */
    public static final String COLUMNNAME_JPKBasicCalculation = "JPKBasicCalculation";

	/** Set JPK Basic Calculation	  */
	public void setJPKBasicCalculation (String JPKBasicCalculation);

	/** Get JPK Basic Calculation	  */
	public String getJPKBasicCalculation();

    /** Column name JPKKawinPaidByCompany */
    public static final String COLUMNNAME_JPKKawinPaidByCompany = "JPKKawinPaidByCompany";

	/** Set JPK Kawin Paid By Comp(%).
	  * JPK Kawin Paid By Conpany in percent
	  */
	public void setJPKKawinPaidByCompany (BigDecimal JPKKawinPaidByCompany);

	/** Get JPK Kawin Paid By Comp(%).
	  * JPK Kawin Paid By Conpany in percent
	  */
	public BigDecimal getJPKKawinPaidByCompany();

    /** Column name JPKKawinPaidByEmployee */
    public static final String COLUMNNAME_JPKKawinPaidByEmployee = "JPKKawinPaidByEmployee";

	/** Set JPK Kawin Paid By Emp(%).
	  * JPK Kawin Paid By Employee in percent
	  */
	public void setJPKKawinPaidByEmployee (BigDecimal JPKKawinPaidByEmployee);

	/** Get JPK Kawin Paid By Emp(%).
	  * JPK Kawin Paid By Employee in percent
	  */
	public BigDecimal getJPKKawinPaidByEmployee();

    /** Column name JPKLajangPaidByCompany */
    public static final String COLUMNNAME_JPKLajangPaidByCompany = "JPKLajangPaidByCompany";

	/** Set JPK Lajang Paid By Comp(%).
	  * JPK Lajang Paid By Company in percent
	  */
	public void setJPKLajangPaidByCompany (BigDecimal JPKLajangPaidByCompany);

	/** Get JPK Lajang Paid By Comp(%).
	  * JPK Lajang Paid By Company in percent
	  */
	public BigDecimal getJPKLajangPaidByCompany();

    /** Column name JPKLajangPaidByEmployee */
    public static final String COLUMNNAME_JPKLajangPaidByEmployee = "JPKLajangPaidByEmployee";

	/** Set JPK Lajang Paid By Emp(%).
	  * JPK Lajang Paid By Company in percent
	  */
	public void setJPKLajangPaidByEmployee (BigDecimal JPKLajangPaidByEmployee);

	/** Get JPK Lajang Paid By Emp(%).
	  * JPK Lajang Paid By Company in percent
	  */
	public BigDecimal getJPKLajangPaidByEmployee();

    /** Column name JPPaidByCompany */
    public static final String COLUMNNAME_JPPaidByCompany = "JPPaidByCompany";

	/** Set JP Paid By Company (%)	  */
	public void setJPPaidByCompany (BigDecimal JPPaidByCompany);

	/** Get JP Paid By Company (%)	  */
	public BigDecimal getJPPaidByCompany();

    /** Column name JPPaidByEmployee */
    public static final String COLUMNNAME_JPPaidByEmployee = "JPPaidByEmployee";

	/** Set JP Paid By Employee (%)	  */
	public void setJPPaidByEmployee (BigDecimal JPPaidByEmployee);

	/** Get JP Paid By Employee (%)	  */
	public BigDecimal getJPPaidByEmployee();

    /** Column name MaxAmtToCalcJHT */
    public static final String COLUMNNAME_MaxAmtToCalcJHT = "MaxAmtToCalcJHT";

	/** Set Max Amount to Calc JHT	  */
	public void setMaxAmtToCalcJHT (BigDecimal MaxAmtToCalcJHT);

	/** Get Max Amount to Calc JHT	  */
	public BigDecimal getMaxAmtToCalcJHT();

    /** Column name MaxAmtToCalcJK */
    public static final String COLUMNNAME_MaxAmtToCalcJK = "MaxAmtToCalcJK";

	/** Set Max Amount to Calc JK	  */
	public void setMaxAmtToCalcJK (BigDecimal MaxAmtToCalcJK);

	/** Get Max Amount to Calc JK	  */
	public BigDecimal getMaxAmtToCalcJK();

    /** Column name MaxAmtToCalcJKK */
    public static final String COLUMNNAME_MaxAmtToCalcJKK = "MaxAmtToCalcJKK";

	/** Set Max Amount to Calc JKK	  */
	public void setMaxAmtToCalcJKK (BigDecimal MaxAmtToCalcJKK);

	/** Get Max Amount to Calc JKK	  */
	public BigDecimal getMaxAmtToCalcJKK();

    /** Column name MaxAmtToCalcJP */
    public static final String COLUMNNAME_MaxAmtToCalcJP = "MaxAmtToCalcJP";

	/** Set Max Amount to Calc JP	  */
	public void setMaxAmtToCalcJP (BigDecimal MaxAmtToCalcJP);

	/** Get Max Amount to Calc JP	  */
	public BigDecimal getMaxAmtToCalcJP();

    /** Column name MaxAmtToCalcJPK */
    public static final String COLUMNNAME_MaxAmtToCalcJPK = "MaxAmtToCalcJPK";

	/** Set Max Amount to Calc JPK	  */
	public void setMaxAmtToCalcJPK (BigDecimal MaxAmtToCalcJPK);

	/** Get Max Amount to Calc JPK	  */
	public BigDecimal getMaxAmtToCalcJPK();

    /** Column name UNS_BPJSConfig_ID */
    public static final String COLUMNNAME_UNS_BPJSConfig_ID = "UNS_BPJSConfig_ID";

	/** Set BPJS Configuration	  */
	public void setUNS_BPJSConfig_ID (int UNS_BPJSConfig_ID);

	/** Get BPJS Configuration	  */
	public int getUNS_BPJSConfig_ID();

    /** Column name UNS_BPJSConfig_UU */
    public static final String COLUMNNAME_UNS_BPJSConfig_UU = "UNS_BPJSConfig_UU";

	/** Set UNS_BPJSConfig_UU	  */
	public void setUNS_BPJSConfig_UU (String UNS_BPJSConfig_UU);

	/** Get UNS_BPJSConfig_UU	  */
	public String getUNS_BPJSConfig_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
