package com.uns.importer;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSMonthlyPayrollEmployee;
import com.uns.model.MUNSPayrollBaseEmployee;
import com.uns.model.MUNSPayrollComponentConf;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollCostBenefit;
import com.uns.model.MUNSPayrollEmployee;
import com.uns.model.MUNSPayrollTermConfig;
import com.uns.model.MUNSPeriodicCostBenefit;

/**
 * @author Hamba Allah
 */

public class UNSImportPayrollKHL implements ImporterValidation {

	protected Properties m_ctx = null;
	protected String m_trxName = null;
	protected Sheet m_sheet = null;
	
	private MUNSMonthlyPayrollEmployee m_parent = null;
	private static final String COL_NO = "A";
	private static final String COL_NAMA = "C";
	private static final String COL_GAJI_POKOK = "D";
	private static final String COL_SUB_MAKAN = "E";
	private static final String COL_SUB_LISTRIK = "F";
	private static final String COL_POT_KOPERASI = "G";
	private static final String COL_POT_KANTIN = "H";
	
	private BigDecimal m_gajiPokok = Env.ZERO;
	private BigDecimal m_subListrik = Env.ZERO;
	private BigDecimal m_subMakan = Env.ZERO;
	private BigDecimal m_potKoperasi = Env.ZERO;
	private BigDecimal m_potKantin = Env.ZERO;
	
	public UNSImportPayrollKHL(Properties ctx, Sheet sheet, String trxName) {
		
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}
	
	protected void initAmt(Hashtable<String, Object> freeColVals) {
		m_gajiPokok = (BigDecimal) freeColVals.get(COL_GAJI_POKOK);
		m_subListrik = (BigDecimal) freeColVals.get(COL_SUB_LISTRIK);
		m_subMakan = (BigDecimal) freeColVals.get(COL_SUB_MAKAN);
		m_potKoperasi = (BigDecimal) freeColVals.get(COL_POT_KOPERASI);
		m_potKantin = (BigDecimal) freeColVals.get(COL_POT_KANTIN);
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		int noLine = (Integer) freeColVals.get(COL_NO);
		String name = freeColVals.get(COL_NAMA).toString();
		int employeeID = po.get_ValueAsInt(MUNSPayrollEmployee.COLUMNNAME_UNS_Employee_ID);
		int monthlyPayrollID = po.get_ValueAsInt(MUNSPayrollEmployee.COLUMNNAME_UNS_MonthlyPayroll_Employee_ID);
		initAmt(freeColVals);
		
		if(employeeID <= 0)
			throw new AdempiereException("No Employee Data on System. Check NIK. #"+name);
		
		if(monthlyPayrollID <= 0)
			throw new AdempiereException("Mandatory monthly payroll employee");
		
		m_parent = new MUNSMonthlyPayrollEmployee(m_ctx, monthlyPayrollID, m_trxName);
		
		if(m_parent == null || m_parent.get_ID() <= 0)
			throw new AdempiereException("Cannot found Monthly Payroll Employee");
		
		if(m_parent.isProcessed())
			throw new AdempiereException("Monthly Payroll Employee has been processed. Cannot Import it.");
		
		String sqql = "SELECT CONCAT(value,'_',name) FROM UNS_Employee WHERE UNS_Employee_ID = ?";
		String empName = DB.getSQLValueString(m_trxName, sqql, employeeID);
		
		if(Util.isEmpty(empName,true))
			throw new AdempiereException("No employee Date on line No "+noLine);
		
		//check employee exists on payroll employee
		String sql = "SELECT m.DocumentNo FROM UNS_Payroll_Employee p"
				+ " INNER JOIN UNS_MonthlyPayroll_Employee m ON m.UNS_MonthlyPayroll_Employee_ID = p.UNS_MonthlyPayroll_Employee_ID"
				+ " WHERE p.UNS_Employee_ID = ? AND p.C_Period_ID = ? AND p.DocStatus NOT IN (?,?)";
		String docNo = DB.getSQLValueString(m_trxName, sql, employeeID, m_parent.getC_Period_ID(), "VO","RE");
		if(!Util.isEmpty(docNo, true))
			throw new AdempiereException("There was exists payroll employee of this employee ( "+empName
					+ " ) on Monthly Payroll Employee with No : "+docNo);
		
		MUNSPayrollEmployee payrollEmp = (MUNSPayrollEmployee) po;
		payrollEmp.setAD_Org_ID(m_parent.getAD_Org_ID());
		payrollEmp.setC_Period_ID(m_parent.getC_Period_ID());
		payrollEmp.setDocStatus(m_parent.getDocStatus());
		payrollEmp.setDocAction(m_parent.getDocAction());
		payrollEmp.setGPokok(m_gajiPokok);
		payrollEmp.setTakeHomePay(BigDecimal.ZERO);
		payrollEmp.setDocumentNo(m_parent.getDocumentNo()+ "-" + noLine);
		payrollEmp.setIsAllIn(true);
		payrollEmp.setRemarks("Import by System");
		
		MUNSEmployee employee = new MUNSEmployee(m_ctx, employeeID, m_trxName);
		
		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
				payrollEmp.getAD_Org_ID()
				, employee.getC_BPartner_ID()
				, employee.getUNS_Contract_Recommendation().getNextContractType()
				, Env.getContextAsDate(m_ctx, "Date")
				, null);
		
		if(null == payrollTerm)
			payrollTerm = employee.getPayrollTerm();
		
		payrollEmp.setC_Job_ID(employee.getC_Job_ID());
		payrollEmp.setShift(employee.getShift());
		payrollEmp.setPayrollTerm(payrollTerm);
		
		String sqll = "SELECT UNS_PayrollBase_Employee_ID FROM UNS_PayrollBase_Employee WHERE"
				+ " UNS_Contract_Recommendation_ID = ? AND IsActive = ? ORDER BY IsActive DESC";
		int payrollBaseEmpID = DB.getSQLValue(m_trxName, sqll, employee.getUNS_Contract_Recommendation_ID(), "Y");
		if(payrollBaseEmpID <= 0)
			throw new AdempiereException("Cannot found Payroll Base Employee. #"+empName);
		
		payrollEmp.setUNS_Contract_Recommendation_ID(employee.getUNS_Contract_Recommendation_ID());
		payrollEmp.setUNS_PayrollBase_Employee_ID(payrollBaseEmpID);
		if(employee.getVendor_ID()==0)
			payrollEmp.setC_DocType_ID(MUNSPayrollBaseEmployee.getDocType(m_ctx, false).get_ID());
		else 
			payrollEmp.setC_DocType_ID(MUNSPayrollBaseEmployee.getDocType(m_ctx, true).get_ID());
		
		if(!payrollEmp.save())
			throw new AdempiereException("Error when try to create payroll employee. #"+empName);
		
		String errorMsg = null;
		
		errorMsg = createPayrollCostBenefit(payrollEmp);
		if(errorMsg != null)
			throw new AdempiereException(errorMsg);
		
		MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
				m_ctx, (MPeriod) payrollEmp.getC_Period(),
				payrollEmp.getAD_Org_ID(), m_trxName, true);
		
		//generate PPH
		BigDecimal pph21 = payConfig.getPPH(payrollEmp, false);
		payrollEmp.setPPH21(pph21);
		payrollEmp.setGeneratePay("Y");
		payrollEmp.saveEx();
		
		return null;
	}
	
	protected String createPayrollCostBenefit(MUNSPayrollEmployee payrollEmp) {
		
		MUNSContractRecommendation contract = MUNSContractRecommendation.getOf(m_ctx, payrollEmp.getUNS_Employee_ID(), m_trxName);
		BigDecimal totalDeduction = Env.ZERO;
		BigDecimal totalAllowance = Env.ZERO;
		if(contract == null)
			return "Cannot found contract";
		
		MUNSPayrollComponentConf[] conf = MUNSPayrollComponentConf.get(contract);
		
		for(int i=0; i < conf.length;i++)
		{
			String type = conf[i].getCostBenefitType();
			BigDecimal paidAmt = Env.ZERO;
			
			MUNSPayrollCostBenefit costBenefit = null;
			MUNSPayrollCostBenefit[] costBenefits = payrollEmp.getCostBenefits(false);
			for(int j=0; j < costBenefits.length; j++)
			{
				if (costBenefits[j].getUNS_Payroll_Component_Conf_ID() == conf[i].get_ID())
				{
					costBenefit = costBenefits[j];
					break;
				}
			}
			
			if(costBenefit == null)
				costBenefit = new MUNSPayrollCostBenefit(payrollEmp, conf[i]);
			
			if(type != null && !MUNSPayrollComponentConf.COSTBENEFITTYPE_PinjamanKaryawan.equals(type))
			{

				if(type.equals(MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Canteen))
				{
					paidAmt = m_potKantin;
				}
				else if(type.equals(MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Cooperative))
				{
					paidAmt = m_potKoperasi;
				}
//				else if(type.equals(MUNSPeriodicCostBenefit.COSTBENEFITTYPE_Premi))
//				{
//					paidAmt = MUNSPeriodicCostBenefit.getMyCostOrBenefit(
//							payrollEmp.getUNS_Employee_ID(), 
//							type, m_trxName);
//				}
				
			}
			else
			{
				String compName = costBenefit.getName().toUpperCase();
				//subsidi listrik
				if(compName.contains("PLN") && costBenefit.isBenefit())
				{
					paidAmt = m_subListrik;
				}
				//subsidi makan
				else if(compName.contains("MAKAN")&& costBenefit.isBenefit())
				{
					paidAmt = m_subMakan;
				}
			}
			
			costBenefit.setAmount(paidAmt);
			if(!costBenefit.save())
				return "Error when try to create/update Payroll Cost Benefit";
			
			if(costBenefit.isBenefit())
				totalAllowance = totalAllowance.add(costBenefit.getAmount());
			else
				totalDeduction = totalDeduction.add(costBenefit.getAmount());
			
		}
		
		payrollEmp.setTotalOtherAllowances(payrollEmp.getTotalOtherAllowances().add(totalAllowance));
		payrollEmp.setTotalOtherDeductions(payrollEmp.getTotalOtherDeductions().add(totalDeduction));
		payrollEmp.saveEx();
		
		return null;
		
	}
	
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		//complete payroll employee
		MUNSPayrollEmployee payEmp = (MUNSPayrollEmployee) po;
		try {
			
			if(payEmp.getDocStatus().equals(MUNSPayrollEmployee.DOCSTATUS_Drafted)
					|| payEmp.getDocStatus().equals(MUNSPayrollEmployee.DOCSTATUS_InProgress))
			{
				if(!payEmp.processIt(MUNSPayrollEmployee.DOCACTION_Complete))
					throw new AdempiereException("Cannot complete Payroll Employee. #"+payEmp.getUNS_Employee().getName()
							+" "+payEmp.getProcessMsg());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return payEmp.getProcessMsg();
		}
		
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		
		m_trxName = trxName;

	}

}
