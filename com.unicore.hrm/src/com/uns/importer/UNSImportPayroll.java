package com.uns.importer;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSPayrollBaseEmployee;
import com.uns.model.MUNSPayrollComponentConf;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollCostBenefit;
import com.uns.model.MUNSPayrollEmployee;
import com.uns.model.MUNSPayrollTermConfig;

public class UNSImportPayroll implements ImporterValidation {

//	private Hashtable<Integer, PO> m_poRefMap = null;
	
	protected Properties m_ctx = null;
	protected String m_trxName = null;
	protected Sheet m_sheet = null;
	public UNSImportPayroll(Properties ctx, Sheet sheet, String trxName)
	{
		super();
		m_ctx = ctx;
		m_sheet = sheet;
		m_trxName = trxName;
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		MUNSPayrollEmployee payroll = (MUNSPayrollEmployee) po;
		if(payroll.getUNS_Employee_ID() <= 0)
			throw new AdempiereException("Not found employee on row " + currentRow);
		
		BigDecimal indvIncentive = (BigDecimal) freeColVals.get("Q");
		BigDecimal csrIncentive = (BigDecimal) freeColVals.get("R");
		BigDecimal grIncentive = (BigDecimal) freeColVals.get("S");
		BigDecimal pushMoney = (BigDecimal) freeColVals.get("T");
		BigDecimal potMangkir = (BigDecimal) freeColVals.get("U");
		BigDecimal potTelat = (BigDecimal) freeColVals.get("V");
		BigDecimal potShortTime = (BigDecimal) freeColVals.get("W");
		BigDecimal potCutiUnpaid = (BigDecimal) freeColVals.get("X");
		BigDecimal shortCashier = (BigDecimal) freeColVals.get("Y");
		BigDecimal incidentReport = (BigDecimal) freeColVals.get("Z");
		BigDecimal PPh = (BigDecimal) freeColVals.get("AA");
		BigDecimal UTUM = (BigDecimal) freeColVals.get("AC");
		BigDecimal THR = (BigDecimal) freeColVals.get("AD");
		BigDecimal BONUS = (BigDecimal) freeColVals.get("AE");
		BigDecimal BestSeller = (BigDecimal) freeColVals.get("AF");
		BigDecimal ServiceCharge = (BigDecimal) freeColVals.get("AG");
		BigDecimal OtherAllowance = (BigDecimal) freeColVals.get("AH");
		BigDecimal OtherDeduction = (BigDecimal) freeColVals.get("AI");
		BigDecimal CutOfSalary = (BigDecimal) freeColVals.get("AJ");
		int UNS_MonthlyPayroll_Employee_ID = (Integer) freeColVals.get("UNS_MonthlyPayroll_Employee_ID");
		
		payroll.setUNS_MonthlyPayroll_Employee_ID(UNS_MonthlyPayroll_Employee_ID);
		
		MUNSEmployee employee = new MUNSEmployee(m_ctx, payroll.getUNS_Employee_ID(), m_trxName);
		
		String payrollTerm = MUNSPayrollTermConfig.getPayrollTermOf(
				payroll.getAD_Org_ID()
				, employee.getC_BPartner_ID()
				, employee.getUNS_Contract_Recommendation().getNextContractType()
				, Env.getContextAsDate(m_ctx, "Date")
				, null);
		
		if(null == payrollTerm)
			payrollTerm = employee.getPayrollTerm();
		
		payroll.setC_Job_ID(employee.getC_Job_ID());
		payroll.setShift(employee.getShift());
		payroll.setPayrollTerm(payrollTerm);
		payroll.setC_Period_ID(payroll.getParent().getC_Period_ID());
	
		MUNSPayrollBaseEmployee pbEmployee = 
				MUNSPayrollBaseEmployee.get(m_ctx, employee.getUNS_Contract_Recommendation_ID(), null);
		payroll.setUNS_Contract_Recommendation_ID(employee.getUNS_Contract_Recommendation_ID());
		payroll.setUNS_PayrollBase_Employee_ID(pbEmployee.get_ID());
		payroll.setPPH21PaidByCompany(employee.getUNS_Contract_Recommendation().isPPH21PaidByCompany());
		if(employee.getVendor_ID()==0)
			payroll.setC_DocType_ID(MUNSPayrollBaseEmployee.getDocType(m_ctx, false).get_ID());
		else 
			payroll.setC_DocType_ID(MUNSPayrollBaseEmployee.getDocType(m_ctx, true).get_ID()); 
		
		payroll.setPPH21(PPh);
		payroll.setRemarks("***Import by System***  PPh on excel = "+PPh);
		payroll.setIsGenerate(true);
		payroll.setDocStatus("DR");
		if(!payroll.save())
			throw new AdempiereException("Error when try to save payroll employee");
		
		MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.get(
				payroll.getCtx(), (MPeriod) payroll.getC_Period(), payroll.getAD_Org_ID(),
				payroll.get_TrxName(), true);
				
		if(null==payConfig)
			throw new AdempiereException("Not found payroll configuration");
		
		MUNSContractRecommendation contract = (MUNSContractRecommendation) employee.getUNS_Contract_Recommendation();
		BigDecimal tCost = Env.ZERO;
		BigDecimal tBenefit = Env.ZERO;
		
		//generate cost/benefit
			//clear all payroll cost/benefit
		String sql = "DELETE FROM UNS_Payroll_CostBenefit WHERE"
				+ " UNS_Payroll_Employee_ID = ?";
		boolean ok = DB.executeUpdate(sql, payroll.get_ID(), m_trxName) != -1;
		if(!ok)
			throw new AdempiereException("Error when try to clear payroll cost benefit");
		
		MUNSPayrollComponentConf[] conf = MUNSPayrollComponentConf.get(contract);
		for(int i=0; i<conf.length; i++)
		{
			MUNSPayrollCostBenefit costBen = new MUNSPayrollCostBenefit(payroll, conf[i]);
			
			BigDecimal amount = conf[i].getAmount();
			
			//overwrite amount 
			if(conf[i].getCostBenefitType() != null)
			{
				if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_IndividualIncentives))
					amount = indvIncentive;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_CashierIncentives))
					amount = csrIncentive;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_GroupIncentives))
					amount = grIncentive;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_Push_Money))
					amount = pushMoney;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_PinjamanKaryawan))
					amount = shortCashier.add(incidentReport);
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_TunjanganHariRaya))
					amount = THR;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_Bonuses))
					amount = BONUS;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_BestSeller))
					amount = BestSeller;
				else if(conf[i].getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_ServiceCharges))
					amount = ServiceCharge;
				else
					amount = Env.ZERO;
			}
			else if(conf[i].getCostBenefitType() == null)
			{
				if(("Tunjangan transport&makan").equals(conf[i].getName()))
					amount = UTUM;
				else if(("Other Allowance").equals(conf[i].getName()))
					amount = OtherAllowance;
				else if(("Other Deduction").equals(conf[i].getName()))
					amount = OtherDeduction;
				else if(("Cut Of Salary").equals(conf[i].getName()))
					amount = CutOfSalary;
			}
			else
				amount = Env.ZERO;
			
			if(amount== null)
				amount = Env.ZERO;
			costBen.setAmount(amount);
			costBen.saveEx();
			
			if(costBen.isBenefit())
				tBenefit = tBenefit.add(amount);
			else
				tCost = tCost.add(amount);
			
		}
		
		if(potMangkir.signum() > 0)
		{
			MUNSPayrollCostBenefit costBen = new MUNSPayrollCostBenefit(payroll, null);
			costBen.setIsBenefit(false);
			costBen.setName(MUNSPayrollEmployee._TRUANT_LEAVE_DEDUCTION);
			costBen.setUNS_Payroll_Component_Conf_ID(-1);
			costBen.setCostBenefit_Acct(payConfig.getBiayaGajiBulananAcct_ID());
			costBen.setIsPPHComp(true);
			costBen.setIsPaidOutsidePayroll(false);
			costBen.setIsMonthlyPPHComp(true);
			costBen.setSeqNo(0);
			costBen.setAmount(potMangkir);
			costBen.isManul = false;
			costBen.saveEx();
			
			tCost = tCost.add(potMangkir);
		}
		
		if(potTelat.signum() > 0)
		{
			MUNSPayrollCostBenefit costBen = new MUNSPayrollCostBenefit(payroll, null);
			costBen.setIsBenefit(false);
			costBen.setName(MUNSPayrollEmployee._BELATED_DEDUCTION);
			costBen.setUNS_Payroll_Component_Conf_ID(-1);
			costBen.setCostBenefit_Acct(payConfig.getBiayaGajiBulananAcct_ID());
			costBen.setIsPPHComp(true);
			costBen.setIsPaidOutsidePayroll(false);
			costBen.setIsMonthlyPPHComp(true);
			costBen.setSeqNo(0);
			costBen.setAmount(potTelat);
			costBen.isManul = false;
			costBen.saveEx();
			
			tCost = tCost.add(potTelat);
		}
		
		if(potShortTime.signum() > 0)
		{
			MUNSPayrollCostBenefit costBen = new MUNSPayrollCostBenefit(payroll, null);
			costBen.setIsBenefit(false);
			costBen.setName(MUNSPayrollEmployee._SHORTTIME_DEDUCTION);
			costBen.setUNS_Payroll_Component_Conf_ID(-1);
			costBen.setCostBenefit_Acct(payConfig.getBiayaGajiBulananAcct_ID());
			costBen.setIsPPHComp(true);
			costBen.setIsPaidOutsidePayroll(false);
			costBen.setIsMonthlyPPHComp(true);
			costBen.setSeqNo(0);
			costBen.setAmount(potShortTime);
			costBen.isManul = false;
			costBen.saveEx();
			
			tCost = tCost.add(potShortTime);
		}
		
		if(potCutiUnpaid.signum() > 0)
		{
			MUNSPayrollCostBenefit costBen = new MUNSPayrollCostBenefit(payroll, null);
			costBen.setIsBenefit(false);
			costBen.setName(MUNSPayrollEmployee._UNPAID_LEAVE_DEDUCTION);
			costBen.setUNS_Payroll_Component_Conf_ID(-1);
			costBen.setCostBenefit_Acct(payConfig.getBiayaGajiBulananAcct_ID());
			costBen.setIsPPHComp(true);
			costBen.setIsPaidOutsidePayroll(false);
			costBen.setIsMonthlyPPHComp(true);
			costBen.setSeqNo(0);
			costBen.setAmount(potCutiUnpaid);
			costBen.isManul = false;
			costBen.saveEx();
			
			tCost = tCost.add(potCutiUnpaid);
		}
		
		payroll.setTotalOtherAllowances(tBenefit);
		payroll.setTotalOtherDeductions(tCost);
		payroll.setIsGenerate(true);
		payroll.setPPH21(payConfig.getPPH(payroll, false));
		payroll.saveEx();
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		
//		for(PO po : pos)
//		{
//			MUNSPayrollEmployee payroll = (MUNSPayrollEmployee) po;
//			
//			try {
//				if(!payroll.processIt(MUNSPayrollEmployee.ACTION_Complete))
//					throw new AdempiereException(payroll.getProcessMsg());
//				
//				payroll.saveEx();
//			} catch (Exception e) {
//				e.printStackTrace();
//				throw new AdempiereException(payroll.getProcessMsg());
//			}
//		}
		
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub

	}

}
