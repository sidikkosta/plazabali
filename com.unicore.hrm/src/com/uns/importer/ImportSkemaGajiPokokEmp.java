package com.uns.importer;

import java.math.BigDecimal;
import java.util.Hashtable;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSPayrollComponentConf;
import com.uns.model.process.LoadBasicPayroll;
import com.uns.model.process.SimpleImportXLS;

public class ImportSkemaGajiPokokEmp implements ImporterValidation {

	public ImportSkemaGajiPokokEmp() {
		
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		MUNSContractRecommendation tempContract = (MUNSContractRecommendation) po;
		
		MUNSContractRecommendation contract = MUNSContractRecommendation.getOf(
				po.getCtx(), tempContract.getUNS_Employee_ID(), po.get_TrxName());
		if(contract == null)
			throw new AdempiereException("This employee does not have contract."
					+ " Please create contract first. #"+tempContract.getUNS_Employee().getValue());
		
		if(contract.getDocStatus().equals(MUNSContractRecommendation.DOCSTATUS_Completed))
		{
			try {
				contract.processIt(MUNSContractRecommendation.ACTION_ReActivate);
				contract.saveEx();
			} catch (Exception e) {
				e.printStackTrace();
				throw new AdempiereException(e.getMessage());
			}
		}
		else if(contract.getDocStatus().equals(MUNSContractRecommendation.DOCSTATUS_Closed))
			throw new AdempiereException("Contract has been closed.Cannot continue process.");
		
		LoadBasicPayroll load = new LoadBasicPayroll(
				contract.getCtx(), contract.getUNS_Contract_Recommendation_ID(), contract.get_TrxName());
		try {
			String processMsg = load.doIt();
			if(processMsg != null)
				throw new AdempiereException(processMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		contract.setNew_G_Pokok(tempContract.getNew_G_Pokok());
		contract.setIsJPKApplyed(tempContract.isJPKApplyed());
		contract.setIsJPApplied(tempContract.isJPApplied());
		contract.setIsJKApplyed(tempContract.isJKApplyed());
		contract.setIsJKKApplyed(tempContract.isJKKApplyed());
		contract.setIsJHTApplyed(tempContract.isJHTApplyed());
		
		if(!contract.save())
			throw new AdempiereException("Error when try to update contract.");
		
		//update component payroll
		MUNSPayrollComponentConf[] comps = contract.getComponents(true); 
		for(MUNSPayrollComponentConf comp : comps)
		{
			if(comp.getCostBenefitType() == null)
			{
				if(comp.getName().toUpperCase().contains("TRANSPORT"))
				{
					boolean used = (Boolean) freeColVals.get("E");
					if(used)
						comp.setAmount((BigDecimal) freeColVals.get("F"));
					else
						comp.deleteEx(true);
					
					continue;
				}
			}
			else
			{
				if(comp.getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_PinjamanKaryawan))
					continue;
				
				String column = null;
				boolean used = false;
				if(comp.getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_IndividualIncentives))
					column = "L";
				else if(comp.getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_CashierIncentives))
					column = "M";
				else if(comp.getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_GroupIncentives))
					column = "N";
				else if(comp.getCostBenefitType().equals(MUNSPayrollComponentConf.COSTBENEFITTYPE_Push_Money))
					column = "O";
				
				if(column != null)
					used = (Boolean) freeColVals.get(column);
				
				if(!used)
					comp.deleteEx(true);
				else
				{
					comp.setIsActive(true);
					comp.saveEx();
				}
				
				continue;
			}
		}
		
		if(contract.getDocStatus().equals(MUNSContractRecommendation.DOCSTATUS_InProgress)
				|| contract.getDocStatus().equals(MUNSContractRecommendation.DOCSTATUS_Invalid)
				|| contract.getDocStatus().equals(MUNSContractRecommendation.DOCSTATUS_Drafted))
		{
			try {
				contract.processIt(MUNSContractRecommendation.ACTION_Complete);
				contract.saveEx();
			} catch (Exception e) {
				e.printStackTrace();
				throw new AdempiereException(e.getMessage());
			}
		}
		return SimpleImportXLS.DONT_SAVE;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		return SimpleImportXLS.DONT_SAVE;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		

	}

}
