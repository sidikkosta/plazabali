/**
 * 
 */
package com.uns.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import jxl.Sheet;

import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Trx;

import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSEmployeeAllowanceRecord;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Burhani Adam
 *
 */
public class UNSAllowanceRecordImporter implements ImporterValidation
{
	
	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, PO> m_PORefMap = null;
	
	static final String COL_NIK = "NIKBaru";
	static final String	COL_Cuti	= "Cuti";

	/**
	 * 
	 */
	public UNSAllowanceRecordImporter() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 */
	public UNSAllowanceRecordImporter(Properties ctx, Sheet sheet, String trxName) {
		
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		int empID = (Integer) freeColVals.get(COL_NIK);
		MUNSEmployee emp = MUNSEmployee.get(m_ctx, empID);
//		Timestamp dateExcel = new Timestamp(System.currentTimeMillis());
		
		String sql = "DELETE FROM UNS_EmployeeAllowanceRecord WHERE UNS_Employee_ID = ?";
		DB.executeUpdate(sql, emp.get_ID(), m_trxName);
		Trx trx = Trx.get(m_trxName, false);
		trx.commit();
		
		List<MUNSContractRecommendation> list = MUNSContractRecommendation.getContractsOf(m_ctx, emp.get_ID(), m_trxName);
		MUNSContractRecommendation[] contract = list.toArray(new MUNSContractRecommendation[list.size()]);
		BigDecimal remainingLeave = (BigDecimal) freeColVals.get(COL_Cuti);
		for(int i=0;i<contract.length;i++)
		{
//			Timestamp date = contract[i].getDateContractStart();
			if(!contract[i].getDocStatus().equals("CO")
					&& !contract[i].getDocStatus().equals("CL"))
				continue;
			if(contract[i].getDateContractEnd().before(Timestamp.valueOf("2019-01-01 00:00:00.00")))
				continue;
			
			MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.getCreate(
					m_ctx, emp, Timestamp.valueOf("2019-01-01 00:00:00.00"),
					MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave,
						m_trxName);
			
			if(record == null)
				return SimpleImportXLS.DONT_SAVE;
			record.setPreviousBalance(remainingLeave);
			record.setLeaveClaimReserved(BigDecimal.valueOf(12));
//			record.setRemainingQty(record.getPreviousBalance().add(record.getLeaveClaimReserved()));
			record.saveEx();
			
			break;
		}
			
//			if(contract[i].getDateContractStart().before(Timestamp.valueOf("2019-01-01 00:00:00.00")))
//				date = Timestamp.valueOf("2019-01-01 00:00:00.00"); 
//			
//			MUNSEmployeeAllowanceRecord record = MUNSEmployeeAllowanceRecord.getCreate(
//					m_ctx, emp, date, MUNSEmployeeAllowanceRecord.LEAVEPERIODTYPE_YearlyLeave,
//						m_trxName);
//			if((i+1) != contract.length)
//			{
//				record.setLeaveReservedUsed(record.getLeaveClaimReserved());
//				record.saveEx();
//			}
//			trx.commit();
//			if(contract[i].getDateContractEnd().after(Timestamp.valueOf("2019-12-31 00:00:00.00")))
//			{
//				Calendar endYear = Calendar.getInstance();
//				endYear.setTime(contract[i].getDateContractEnd());
//				endYear.add(Calendar.YEAR, 2019);
//				endYear.add(Calendar.MONTH, 11);
//				endYear.add(Calendar.DATE, 31);
//				Calendar endContract = Calendar.getInstance();
//				endContract.setTime(contract[i].getDateContractEnd());
//				int monthOnNextYear = endContract.get(Calendar.MONTH) + 1;
//				remainingLeave = remainingLeave.subtract(BigDecimal.valueOf(monthOnNextYear));
//			}
//		}
//		
//		MUNSEmployeeAllowanceRecord[] records = emp.getAllowanceRecord();
//		for(int i=0;i<records.length;i++)
//		{
//			if((i+1)==records.length)
//			{
//				BigDecimal used = remainingLeave.subtract(records[i].getLeaveClaimReserved());
//				used = used.abs();
//				records[i].setLeaveReservedUsed(used);
//				records[i].saveEx();
//				trx.commit();
//			}
//		}
		
		return SimpleImportXLS.DONT_SAVE;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
		
	}
}