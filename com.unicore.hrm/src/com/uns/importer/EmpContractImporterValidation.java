/**
 * 
 */
package com.uns.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.base.model.Query;
import com.uns.model.MUNSContractRecommendation;
import com.uns.model.MUNSEmployee;
import com.uns.model.MUNSPayrollConfiguration;
import com.uns.model.MUNSPayrollLevelConfig;
import com.uns.model.MUNSUMKLevel;
import com.uns.model.X_UNS_Contract_Recommendation;
import com.uns.model.factory.UNSHRMModelFactory;
import com.uns.model.process.LoadBasicPayroll;

/**
 * @author Haryadi
 *
 */
public class EmpContractImporterValidation implements ImporterValidation {

	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, PO> m_PORefMap = null;
	private	  String m_employmentType = MUNSContractRecommendation.EMPLOYMENTTYPE_Company;
	private final String COL_MANUAL = "isManual";
	private final String COL_GP = "New_G_Pokok";
	@SuppressWarnings("unused")
	private final String COL_Lembur = "Lembur";
	private final String COL_OverwriteEntryDate = "OverwriteEntryDate";
	//is manual process contract, if true then call process action COntract Recomendation
	private boolean isManual = false;
	private boolean isOverwriteEntryDate = false;
	
	
	/**
	 * 
	 */
	public EmpContractImporterValidation(Properties ctx, Sheet sheet, String trxName) {
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}

	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
		
	}
	
	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po, Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MUNSContractRecommendation newCtr = (MUNSContractRecommendation)po;
		String attendanceName = (String) freeColVals.get("C");
		if(newCtr.getUNS_Employee_ID() > 0)
		{
			MUNSEmployee emp = new MUNSEmployee(po.getCtx(), newCtr.getUNS_Employee_ID(), po.get_TrxName());
			if(emp.getAttendanceName().equals(attendanceName))
			{
				isManual = (boolean) freeColVals.get(COL_MANUAL);
				isOverwriteEntryDate = (boolean) freeColVals.get(COL_OverwriteEntryDate);
				if (poRefMap == null)
					poRefMap = new Hashtable<String, PO>();
				m_PORefMap = poRefMap;
				m_PORefMap.put(newCtr.getNewNIK(), emp);
				return null;
			}
		}
		setEmploymentType(newCtr);
		
		MUNSPayrollConfiguration payConfig = MUNSPayrollConfiguration.getCurrentActive(m_ctx, m_trxName);
		MUNSPayrollLevelConfig payrollLevel = 
				payConfig.getPayrollLevel(newCtr.getNewPayrollLevel(), newCtr.getNextPayrollTerm(), newCtr.getAD_Org_ID(), true);
		
		if(payrollLevel == null)
			throw new AdempiereException("Cannout found payroll level configuration");
		isManual = (boolean) freeColVals.get(COL_MANUAL);
		isOverwriteEntryDate = (boolean) freeColVals.get(COL_OverwriteEntryDate);
		
		// Set all necessary values from the payroll configuration.
		newCtr.setEmploymentType(getEmploymentType());
		if(!newCtr.isMoveTo())
			newCtr.setNewDept_ID(newCtr.getAD_Org_ID());
		else
			//TODO
			
		if(!isManual)
		{
			newCtr.setDocStatus(MUNSContractRecommendation.DOCSTATUS_Completed);
			newCtr.setDocAction(MUNSContractRecommendation.DOCACTION_Close);
			newCtr.setProcessed(true);
		}
		
		String oriNotes = newCtr.getNotes();
		//newCtr.setNewNIK(oriNotes);
		newCtr.setNotes("***Imported on " + new java.util.Date() + "***");
		
		if (payrollLevel != null && payrollLevel.getUNS_PayrollLevel_Config_ID() > 1)
		{
			if(newCtr.getNew_G_Pokok().signum() <= 0)
				newCtr.setNew_G_Pokok(payrollLevel.getMin_G_Pokok());
			if(newCtr.getNew_T_Jabatan().signum() <= 0)
				newCtr.setNew_T_Jabatan(payrollLevel.getMin_G_T_Jabatan());
			if(newCtr.getNew_T_Kesejahteraan().signum() <= 0)
				newCtr.setNew_T_Kesejahteraan(payrollLevel.getMin_G_T_Kesejahteraan());
			if(newCtr.getNew_T_Lembur().signum() <= 0)
				newCtr.setNew_T_Lembur(payrollLevel.getMin_G_T_Lembur());
			
			if (newCtr.getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_SubContract)
				|| !newCtr.getNextPayrollTerm().equals(
						MUNSContractRecommendation.NEXTPAYROLLTERM_Monthly))
				newCtr.setNew_G_Pokok(MUNSUMKLevel.getAmount(m_trxName, payrollLevel.getUMKLevel_ID()));
			
			newCtr.setNew_A_L1(payrollLevel.getMin_A_L1());
			newCtr.setNew_A_L2(payrollLevel.getMin_A_L2());
			newCtr.setNew_A_L3(payrollLevel.getMin_A_L3());
		}
		MUNSEmployee employee = null;
		
		if (newCtr.getUNS_Employee_ID() == 0)
		{
			String name = null;
			Timestamp ttlDate = null;
			
			if(oriNotes != null && oriNotes.length() > 0)
			{
				name = oriNotes.substring(0, oriNotes.indexOf("__"));
				String ttl = oriNotes.substring(oriNotes.indexOf("__") + 2);
				System.out.println("TTL : " + ttl);
				ttlDate = Timestamp.valueOf(ttl);
			}
			else
			{
				name = freeColVals.get("I").toString();
				ttlDate = (Timestamp) freeColVals.get("AL");
			}
			
			String whereClause = "Name=? AND DateOfBirth=?";
			employee = 
					Query.get(m_ctx, UNSHRMModelFactory.EXTENSION_ID, MUNSEmployee.Table_Name, whereClause, m_trxName)
					.setParameters(name, ttlDate)
					.first();
			
			if (employee == null)
			{
				
				String NickName = (String) freeColVals.get("J");
				String marga = (String) freeColVals.get("K");
				Timestamp entryDate = (Timestamp) freeColVals.get("L");
				String gender = (String) freeColVals.get("P");
				String KTP = (String) freeColVals.get("Q");
				String KK = (String) freeColVals.get("R");
				String passport = (String) freeColVals.get("S");
				String SIM = (String) freeColVals.get("T");
				String NPWP = (String) freeColVals.get("U");
				String BPJSKerja = (String) freeColVals.get("V");
				String BPJSKesehatan = (String) freeColVals.get("W");
				String klsRawat = (String) freeColVals.get("X");
				String FaskesCode = (String) freeColVals.get("Y");
				String FaskesName = (String) freeColVals.get("Z");
				String DentistCode = (String) freeColVals.get("AA");
				String DentistName = (String) freeColVals.get("AB");
				String email = (String) freeColVals.get("AC");
				String acctNo = (String) freeColVals.get("AD");
				int BankID = (Integer) freeColVals.get("AE");
				String KCPBank = (String) freeColVals.get("AF");
				String mobilePhone = (String) freeColVals.get("AG");
				String statusOnBPJS = (String) freeColVals.get("AH");
				String maritalStatus = (String) freeColVals.get("AI");
				String nationality = (String) freeColVals.get("AJ");
				String placeOfBirth = (String) freeColVals.get("AK");
				String religion = (String) freeColVals.get("AM");
				int numberOfChild = freeColVals.get("AN") == null ? 0 : (Integer) freeColVals.get("AN");
				String motherName = (String) freeColVals.get("AO");
				String fatherName = (String) freeColVals.get("AP");
				String mateName = (String) freeColVals.get("AQ");
				String insured1 = (String) freeColVals.get("AR");
				String insured2 = (String) freeColVals.get("AT");
				String insured3 = (String) freeColVals.get("AV");
				String insured4 = (String) freeColVals.get("AX");
				String lastEduc = (String) freeColVals.get("AZ");
				String address =  (String) freeColVals.get("BA");
				int cityID = freeColVals.get("BB") == null ? -1 : (Integer) freeColVals.get("BB");
				int DistrictID = freeColVals.get("BC") == null ? -1 : (Integer) freeColVals.get("BC");
				int KelurahanID = freeColVals.get("BD") == null ? -1 : (Integer) freeColVals.get("BD");
				String RT = (String) freeColVals.get("BE");
				String RW = (String) freeColVals.get("BF");
				String zipCode = (String) freeColVals.get("BG");
				
				employee = new MUNSEmployee(m_ctx, 0, m_trxName);
				employee.setAD_Org_ID(newCtr.getNewDept_ID());
				employee.setDateOfBirth(ttlDate);
				employee.setEmploymentType(getEmploymentType());
				employee.setName(name);
				employee.setFamilyName(marga == null ? "-" : marga);
				employee.setValue(newCtr.getNewNIK());
				employee.setPayrollTerm(newCtr.getNextPayrollTerm());
				employee.setC_Job_ID(newCtr.getNewJob_ID());
				employee.setC_BPartner_ID(newCtr.getNewSectionOfDept_ID());
				employee.setPayrollLevel(newCtr.getNewPayrollLevel());
//				employee.setGender(newCtr.getNewGender());
				employee.setAttendanceName(attendanceName);
				employee.setNickName(NickName);
				if(entryDate != null)
					employee.setEffectiveEntryDate(entryDate);
				employee.setGender(gender);
				if(KTP != null)
				{
					employee.setIDType(MUNSEmployee.IDTYPE_KTP);
					employee.setIDNo(KTP);
				}
				if(SIM != null)
				{
					employee.setIDType2(MUNSEmployee.IDTYPE2_SIM);
					employee.setIDNo2(SIM);
				}
				if(passport != null)
				{
					employee.setIDType3(MUNSEmployee.IDTYPE3_Pasport);
					employee.setIDNo3(passport);
				}
				employee.setKartuKeluarga(KK);
				employee.setNPWP(NPWP);
				employee.setBPJSKetenagakerjaan(BPJSKerja);
				employee.setBPJSKesehatan(BPJSKesehatan);
				employee.setKelasRawat(klsRawat);
				employee.setHealthFacilityCode1(FaskesCode);
				employee.setHealthFacilityName1(FaskesName);
				employee.setDentistHealthFacilityCode(DentistCode);
				employee.setDentistHealthFacilityName(DentistName);
				employee.setEMail(email);
				employee.setAccountNo(acctNo);
				employee.setC_Bank_ID(BankID);
				employee.setKCPBank(KCPBank);
				employee.setMobilePhone(mobilePhone);
				employee.setStatus(statusOnBPJS);
//				if (employee.getGender().equals(MUNSEmployee.GENDER_Female)) {
//					if (ttlDate.before(Date.valueOf("1988-01-01")))
//						employee.setMaritalStatus(MUNSEmployee.MARITALSTATUS_Kawin0Tanggungan);
//					else
//						employee.setMaritalStatus(MUNSEmployee.MARITALSTATUS_TidakKawin0Tanggungan);
//				}
//				else {
//					if (ttlDate.before(Date.valueOf("1985-01-01")))
//						employee.setMaritalStatus(MUNSEmployee.MARITALSTATUS_Kawin0Tanggungan);
//					else
//						employee.setMaritalStatus(MUNSEmployee.MARITALSTATUS_TidakKawin0Tanggungan);
//				}
				employee.setMaritalStatus(maritalStatus);
				employee.setNationality(nationality);
				employee.setPlaceOfBirth(placeOfBirth);
				employee.setReligion(religion);
				employee.setNumberOfChild(numberOfChild);
				employee.setMotherName(motherName == null ? "-" : motherName);
				employee.setFatherName(fatherName);
				employee.setHWname(mateName);
				employee.setEmployeeInsured1(insured1);
				employee.setEmployeeInsured2(insured2);
				employee.setEmployeeInsured3(insured3);
				employee.setEmployeeInsured4(insured4);
				employee.setHighestEdBackround(lastEduc);
				employee.setAddress(address);
				employee.setC_City_ID(cityID);
				employee.setUNS_District_ID(DistrictID);
				employee.setUNS_KelurahanDesa_ID(KelurahanID);
				employee.setRT(RT);
				employee.setRW(RW);
				employee.setZipCode(zipCode);
				
//				if (getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_Company))
//					//&& (newCtr.getNextEmploymentStatus())
//				{
//					;
//				}
//				else if (getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_SubContract))
//				{
//					employee.setVendor_ID(newCtr.getNewAgent_ID());
//					employee.setContractNumber(newCtr.getNextContractNumber());
//				}
				employee.saveEx();
			}
			newCtr.setUNS_Employee_ID(employee.get_ID());
			
			if (poRefMap == null)
				poRefMap = new Hashtable<String, PO>();
			m_PORefMap = poRefMap;
		}
		else {
			employee = new MUNSEmployee(m_ctx, newCtr.getUNS_Employee_ID(), m_trxName);
		}
		
		Object gpObject = freeColVals.get(COL_GP);
		BigDecimal gp = gpObject == null ? Env.ZERO : (BigDecimal) gpObject;
		newCtr.setNew_G_Pokok(gp);
			
		//set zero all lemburcomponent
		newCtr.setOTBasicAmt(Env.ZERO);
		newCtr.setAL1Multiplier(Env.ZERO);
		newCtr.setAL2Multiplier(Env.ZERO);
		newCtr.setAL3Multiplier(Env.ZERO);
		newCtr.setALR1Multiplier(Env.ZERO);
		newCtr.setALR2Multiplier(Env.ZERO);
		newCtr.setALR3Multiplier(Env.ZERO);
		newCtr.setNew_A_L1(Env.ZERO);
		newCtr.setNew_A_L2(Env.ZERO);
		newCtr.setNew_A_L3(Env.ZERO);
		newCtr.setNew_A_L1_R(Env.ZERO);
		newCtr.setNew_A_L2_R(Env.ZERO);
		newCtr.setNew_A_L3_R(Env.ZERO);
		newCtr.setFirstOTMultiplier(Env.ZERO);
		newCtr.setNextOTMultiplier(Env.ZERO);
		
		//employee.setValue(oriNotes);
		if (m_PORefMap == null)
			m_PORefMap = poRefMap;
		if (m_PORefMap.get(newCtr.getNewNIK()) == null)
			m_PORefMap.put(newCtr.getNewNIK(), employee);
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po, Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MUNSEmployee employee = (MUNSEmployee)
				poRefMap.get((String) po.get_Value(MUNSContractRecommendation.COLUMNNAME_NewNIK));
		employee.setUNS_Contract_Recommendation_ID(po.get_ID());
		MUNSContractRecommendation theCtr = (MUNSContractRecommendation) po;
		
		employee.setAD_Org_ID(theCtr.getNewDept_ID());
		employee.setValue(theCtr.getNewNIK());
		employee.setPayrollLevel(theCtr.getNewPayrollLevel());
		employee.setEmploymentType(getEmploymentType());
		employee.setPayrollTerm(theCtr.getNextPayrollTerm());
		employee.setAD_Org_ID(theCtr.getNewDept_ID());
		employee.setC_BPartner_ID(theCtr.getNewSectionOfDept_ID());
		employee.setShift(theCtr.getNewShift());
		employee.setC_Job_ID(theCtr.getNewJob_ID());
		if (getEmploymentType().equals(MUNSEmployee.EMPLOYMENTTYPE_SubContract)) {
			employee.setContractNumber(theCtr.getNextContractNumber());
			employee.setUNS_SlotType_ID(theCtr.getNewSlotType_ID());
			employee.setVendor_ID(theCtr.getNewAgent_ID());
		}
		employee.saveEx();
		
		LoadBasicPayroll load = new LoadBasicPayroll(m_ctx, theCtr.getUNS_Contract_Recommendation_ID(), m_trxName);
		try {
			load.setOnlyComp(true);
			String processMsg = load.doIt();
			if(processMsg != null)
				throw new AdempiereException(processMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		
		if(isManual)
		{
			for(PO po : pos)
			{
				MUNSContractRecommendation contract = (MUNSContractRecommendation) po;
				try {
					Timestamp entryDate = contract.getUNS_Employee().getEffectiveEntryDate();
					//complete contract
					if(!contract.processIt(MUNSContractRecommendation.ACTION_Complete))
						throw new AdempiereException(contract.getProcessMsg());
					
					contract.saveEx();
					
					if(isOverwriteEntryDate)
					{
						String sqlUpdate = "UPDATE UNS_Employee SET EffectiveEntryDate = ?"
								+ " WHERE UNS_Employee_ID = ?";
						DB.executeUpdateEx(sqlUpdate, new Object[]{ entryDate, contract.getUNS_Employee_ID()}, po.get_TrxName());
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	protected String getEmploymentType()
	{
		return m_employmentType;
	}

	/**
	 * 
	 * @param ctr
	 */
	protected void setEmploymentType(X_UNS_Contract_Recommendation ctr)
	{
		String newNIK = ctr.getNewNIK();
		
		if (newNIK.charAt(0) == 'P')
			m_employmentType = MUNSContractRecommendation.EMPLOYMENTTYPE_SubContract;
		else
			m_employmentType = MUNSContractRecommendation.EMPLOYMENTTYPE_Company;
	}
}
