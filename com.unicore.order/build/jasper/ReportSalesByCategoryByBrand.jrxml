<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportItemSoldReportByBrand" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="a5632b90-8236-4a72-a5a1-053161d36678">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="M_Product_Category_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="UNS_Brand_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="IsTradingDate" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="UNS_ProductType" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Location" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="M_Product_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT

pd.SKU AS sku,
pd.Name AS Description,
pd.articleno AS artno,
COALESCE(br.Name,'-') AS brand,
COALESCE(pcpr.value,'-') AS subcat,
pcpr.name AS namesubcat,
sum(ptl.qtyordered) AS qty,
now(),
bp.Value AS vbpartner,
bp.Name AS nbpartner,
uom.uomsymbol as uom,
SUM(ptl.LineAmt-(ptl.LineAmt*pt.discount/100)) AS NetAmt


FROM UNS_POSTrx pt
INNER JOIN UNS_POSTrxLine ptl ON ptl.UNS_POSTrx_ID=pt.UNS_POSTrx_ID
INNER JOIN M_Product pd ON pd.M_Product_ID=ptl.M_Product_ID
INNER JOIN UNS_POS_Session sesi ON sesi.UNS_POS_Session_ID=pt.UNS_POS_Session_ID
LEFT JOIN M_Product_Category pcpr ON pcpr.M_Product_Category_ID= (SELECT getproductcategorytopparent(pd.M_product_ID))
INNER JOIN C_UOM UOM ON UOM.C_UOM_ID=pd.C_UOM_ID
LEFT JOIN M_Product_PO bpp ON bpp.M_Product_ID=pd.M_Product_ID AND isCurrentVendor='Y'
LEFT JOIN C_BPartner bp ON bp.C_BPartner_ID=bpp.C_BPartner_ID
INNER JOIN UNS_Brand br ON br.UNS_Brand_ID=pd.UNS_Brand_ID
INNER JOIN C_BPartner store ON store.C_BPartner_ID=pt.C_BPartner_ID


WHERE (CASE WHEN pt.UNS_POSTrx_ID > 0 THEN sesi.isTrialMode='N' AND pt.IsPaid = 'Y' AND pt.DocStatus IN ('CO', 'CL') AND
(CASE WHEN $P{IsTradingDate} = 'Y' THEN pt.dateacct BETWEEN $P{DateFrom} AND  $P{DateTo} ELSE CAST(pt.datetrx AS Date) BETWEEN $P{DateFrom} AND  $P{DateTo} END) ELSE 1=1 END)
 AND
(CASE WHEN $P{M_Product_Category_ID} IS NOT NULL THEN pcpr.M_Product_Category_ID=$P{M_Product_Category_ID} ELSE 1=1 END)
AND
(CASE WHEN $P{UNS_Brand_ID} IS NOT NULL THEN pd.UNS_Brand_ID=$P{UNS_Brand_ID} ELSE 1=1 END)
 AND
(CASE WHEN $P{UNS_ProductType}='I' THEN isImport='Y'
WHEN $P{UNS_ProductType}='L' THEN isImport='N' ELSE 1=1 END)
AND
(CASE WHEN $P{UNS_Brand_ID} IS NOT NULL THEN pd.UNS_Brand_ID=$P{UNS_Brand_ID} ELSE 1=1 END)
AND
(CASE WHEN $P{M_Product_ID} IS NOT NULL THEN pd.M_Product_ID=$P{M_Product_ID} ELSE 1=1 END) AND
CASE WHEN $P{Location} IS NOT NULL THEN store.value LIKE '%$P!{Location}%' ELSE 1=1 END

GROUP BY pd.sku, pd.Name, br.Name, pcpr.value, bp.value, bp.name, uom.uomsymbol, pd.m_product_id, ptl,priceactual, pcpr.name

ORDER BY PCPR.VALUE, brand, pd.sku]]>
	</queryString>
	<field name="sku" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="artno" class="java.lang.String"/>
	<field name="brand" class="java.lang.String"/>
	<field name="subcat" class="java.lang.String"/>
	<field name="namesubcat" class="java.lang.String"/>
	<field name="qty" class="java.math.BigDecimal"/>
	<field name="now" class="java.sql.Timestamp"/>
	<field name="vbpartner" class="java.lang.String"/>
	<field name="nbpartner" class="java.lang.String"/>
	<field name="uom" class="java.lang.String"/>
	<field name="netamt" class="java.math.BigDecimal"/>
	<variable name="NetSales" class="java.math.BigDecimal" resetType="Group" resetGroup="product" incrementType="Group" incrementGroup="Brand">
		<variableExpression><![CDATA[]]></variableExpression>
	</variable>
	<variable name="SumQty" class="java.math.BigDecimal" resetType="Group" resetGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
	</variable>
	<variable name="TotalSumQty" class="java.math.BigDecimal" resetType="Group" resetGroup="Brand" incrementType="Group" incrementGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$V{SumQty}]]></variableExpression>
	</variable>
	<variable name="TotalSumQtyCat" class="java.math.BigDecimal" resetType="Group" resetGroup="CATEGORY" incrementType="Group" incrementGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$V{SumQty}]]></variableExpression>
	</variable>
	<variable name="TotalSumNetSales" class="java.math.BigDecimal" resetType="Group" resetGroup="Brand" incrementType="Group" incrementGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$F{netamt}]]></variableExpression>
	</variable>
	<variable name="TotalSumNetSalesCat" class="java.math.BigDecimal" resetType="Group" resetGroup="CATEGORY" incrementType="Group" incrementGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$F{netamt}]]></variableExpression>
	</variable>
	<variable name="netamt_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{netamt}]]></variableExpression>
	</variable>
	<variable name="netamt_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Brand" calculation="Sum">
		<variableExpression><![CDATA[$F{netamt}]]></variableExpression>
	</variable>
	<variable name="netamt_3" class="java.math.BigDecimal" resetType="Group" resetGroup="CATEGORY" calculation="Sum">
		<variableExpression><![CDATA[$F{netamt}]]></variableExpression>
	</variable>
	<variable name="netamt_4" class="java.math.BigDecimal" resetType="Group" resetGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$F{netamt}]]></variableExpression>
	</variable>
	<variable name="qty_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
	</variable>
	<group name="CATEGORY" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{subcat}]]></groupExpression>
		<groupHeader>
			<band height="50">
				<staticText>
					<reportElement x="0" y="30" width="261" height="20" uuid="3cc30263-2f3e-427e-a45a-44c1ffec3bfb"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5" lineStyle="Double"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Brand]]></text>
				</staticText>
				<staticText>
					<reportElement x="261" y="30" width="75" height="20" uuid="076cc56c-e35e-4d33-861e-1ae62b0e9785"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5" lineStyle="Double"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Quantity]]></text>
				</staticText>
				<staticText>
					<reportElement x="336" y="30" width="120" height="20" uuid="0ae04956-1baf-4fde-933e-f3f5b5d7e6ab"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5" lineStyle="Double"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Net Sales (Rp)]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="0" width="90" height="17" uuid="ce96a278-da88-475f-9620-b9a44242addb"/>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Category]]></text>
				</staticText>
				<textField>
					<reportElement x="90" y="0" width="171" height="17" uuid="28884bde-8b08-4920-a999-76f263337c47"/>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[": "+$F{subcat}+" - "+$F{namesubcat}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="456" y="30" width="99" height="20" uuid="4f1175bf-a84e-427f-8317-ee62679ce7c7"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5" lineStyle="Double"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[%]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="17">
				<textField pattern="###0">
					<reportElement x="261" y="0" width="75" height="17" uuid="fd100233-c243-4ea8-ac5e-ffe2c099530f"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalSumQtyCat}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="336" y="0" width="120" height="17" uuid="d0f483ea-f8bf-439a-b6d5-78c2b0fe9e47"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
						<paragraph rightIndent="4"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{netamt_3}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="0" y="0" width="261" height="17" uuid="cffde09f-b528-44f9-b612-9861393eb590"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA['TOTAL CATEGORY  : '+$F{subcat}+' '+$F{namesubcat}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="456" y="0" width="99" height="17" uuid="fe344f6a-13d5-4bbd-a337-e24a31689bed"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[100 %]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="Brand" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{brand}]]></groupExpression>
		<groupFooter>
			<band height="17">
				<textField pattern="###0">
					<reportElement x="261" y="0" width="75" height="17" uuid="7a6dd3ff-1560-46b1-9889-ed740e19b35e"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid"/>
						<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalSumQty}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="336" y="0" width="120" height="17" uuid="7a8c6870-632c-4155-be5b-6d1da5c6b57e"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid"/>
						<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="false"/>
						<paragraph rightIndent="4"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{netamt_2}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="0" width="261" height="17" uuid="c7205cc4-3a66-4c9e-a468-ef13e5380832"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid"/>
						<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="false" isItalic="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{brand}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Auto" pattern="#,##0 %">
					<reportElement x="456" y="0" width="99" height="17" uuid="1c29995a-2939-4f32-8782-ab9d56ca65da"/>
					<textElement textAlignment="Center"/>
					<textFieldExpression><![CDATA[new Double ($V{netamt_2}.doubleValue() / $V{netamt_3}.doubleValue())]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="product">
		<groupExpression><![CDATA[$F{sku}]]></groupExpression>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="75" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="336" height="24" uuid="f884434c-ffc4-4c0d-968b-fccbd631c322"/>
				<textElement verticalAlignment="Middle">
					<font size="12" isBold="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[SALES BY CATEGORY BY BRAND]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="24" width="90" height="17" uuid="3af8ba7b-4d1d-43c2-87f1-c43fa5a7c8b8"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[For the period]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="41" width="90" height="17" uuid="aa143420-c8db-4033-81ff-ef1421a9e781"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Location]]></text>
			</staticText>
			<textField>
				<reportElement x="336" y="41" width="219" height="17" uuid="6cf447db-0c2b-4f02-9228-db2d59d75f74"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$P{IsTradingDate}=="Y" ? "Base On Trading Date" : "Base On Actual Date"]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy h.mm a">
				<reportElement x="336" y="24" width="219" height="17" uuid="527165fc-ed79-435d-afe0-9bfc90b2bb46"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Printed "+new SimpleDateFormat("dd MMM yyyy hh:mm a").format($F{now})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="90" y="24" width="246" height="17" uuid="edd32a34-118b-4e85-9f86-9547034d180e"/>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+
new SimpleDateFormat("dd/MM/yyyy").format($P{DateFrom})+" to "+
new SimpleDateFormat("dd/MM/yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="90" y="41" width="171" height="17" uuid="b5fb65bc-8a51-43a5-a745-786f0b59be41"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{Location} == null || $P{Location} == "" ? ": All Location" : ": "+$P{Location}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="336" y="0" width="219" height="24" uuid="ffb90ceb-997c-4a28-a5df-e1f7a41c71fe"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="58" width="90" height="17" uuid="3748699a-58d3-4913-b9de-cd42316fc5bc"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Division]]></text>
			</staticText>
			<textField>
				<reportElement x="90" y="58" width="171" height="17" uuid="c4c4ada2-8795-479d-ac46-37b7dbbe18ab"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{UNS_ProductType} == null || $P{UNS_ProductType} == "" ? ": All Division" : ($P{UNS_ProductType} == "L" ? ": Local Only" : ": Duty Free Only")]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<summary>
		<band height="20">
			<textField pattern="#,##0.00">
				<reportElement x="336" y="0" width="120" height="20" uuid="bae76a7d-fa51-451e-9da2-a9606e7fd360"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph rightIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{netamt_1}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="0" width="261" height="20" uuid="ada9e69a-a0e5-4472-b60a-e7b0068b3af5"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[GRAND TOTAL]]></text>
			</staticText>
			<staticText>
				<reportElement x="456" y="0" width="99" height="20" uuid="80cd36dd-65c3-4736-9528-77f6550baf8b"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="###0">
				<reportElement x="261" y="0" width="75" height="20" uuid="ac5b3c4b-5c7f-49aa-a89d-0361ec57fd3c"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{qty_1}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
