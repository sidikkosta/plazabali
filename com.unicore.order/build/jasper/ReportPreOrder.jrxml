<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportPreOrder" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="9de937ef-3393-4bb4-8d53-0e673115fd43">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="StatusOrder" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="StartDate" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="EndDate" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="UNS_Shipping_Track_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT po.DocumentNo AS NoPreOrder,
po.Created AS Created,
bp.Name AS NamaToko,
bpl.Name AS AlamatToko,
COALESCE(r.Name, 'Rayon Not Defined') AS Rayon,
au.Name AS User,
po.AttName AS NamaPelanggan,
po.AttAddress AS AlamatPelanggan,
po.description AS ket,
o.documentno AS NoSO,
(CASE WHEN po.docstatus IN ('DR', 'CO') THEN 'SO Not Created'
      WHEN o.c_order_id IS NULL AND po.docstatus='CL' THEN 'PO Voided'
      WHEN o.docstatus='VO' THEN 'SO Voided'
      WHEN o.DocStatus NOT IN ('VO') THEN 'SO Created'
      ELSE 'All Status'
      END) AS statusorder,
(CASE WHEN pl.docstatus IS NULL THEN 'PL Not Created'
      WHEN pl.docstatus = 'DR' THEN 'Draft'
      WHEN pl.docstatus = 'IP' THEN 'In Progress'
      WHEN pl.docstatus = 'IN' THEN 'Invalid'
      WHEN pl.docstatus = 'CO' THEN 'Completed'
      WHEN pl.docstatus = 'CL' THEN 'Closed'
      WHEN pl.docstatus IN ('VO', 'RE') THEN 'Voided'
      ELSE 'Packing List Belum Dibuat'
END) AS statuspacking,
(CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN bp.Name
      ELSE 'All BP' END) AS paramtoko,
p.Name AS Product,
pol.Qty AS Qty,
COALESCE(ol.QtyOrdered, ol.QtyEntered, 0) AS QtyOrdered,
COALESCE(ol.QtyPacked,0) AS QtyPacked,
COALESCE(ol.QtyDelivered,0) AS QtyDelivered,
uom.Name AS UOM,
(CASE WHEN po.UNS_Shipping_Track_ID IS NOT NULL THEN st.name
      ELSE 'Shipping Track Belum Terdefinisi'
 END) AS ShippingTrack,
(CASE WHEN po.isNeedAppointment = 'Y' THEN 'Perlu Buat Janji Lokasi Kiriman'
      ELSE '' END) AS SJLokasi

FROM UNS_PreOrder po
INNER JOIN UNS_PreOrder_Line pol ON pol.UNS_PreOrder_ID = po.UNS_PreOrder_ID
LEFT OUTER JOIN C_Order o ON o.C_Order_ID = po.C_Order_ID
LEFT OUTER JOIN C_OrderLine ol ON pol.C_Orderline_ID=ol.C_Orderline_ID
LEFT OUTER JOIN uns_packinglist_order plo ON plo.c_order_id=o.c_order_id
LEFT OUTER JOIN uns_packinglist pl ON pl.uns_packinglist_id=plo.uns_packinglist_id
LEFT OUTER JOIN uns_pl_confirm plc on plc.uns_packinglist_id=pl.uns_packinglist_id
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = po.C_BPartner_ID
INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_ID = bp.C_BPartner_ID
LEFT OUTER JOIN UNS_Rayon r ON r.UNS_Rayon_ID = bpl.UNS_Rayon_ID
INNER JOIN M_Product p ON p.M_Product_ID = pol.M_Product_ID
INNER JOIN C_UOM uom ON uom.C_UOM_ID = pol.C_UOM_ID
INNER JOIN AD_User au ON au.AD_User_ID = po.AD_User_ID
LEFT OUTER JOIN UNS_Shipping_Track st ON st.UNS_Shipping_Track_ID=po.UNS_Shipping_Track_ID

WHERE po.DocStatus NOT IN ('DR') AND
po.Created BETWEEN $P{StartDate} AND $P{EndDate} AND
(CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN bp.C_BPartner_ID = $P{C_BPartner_ID}
      ELSE 1=1
 END)
AND
(CASE WHEN $P{StatusOrder}='SOC' THEN o.C_Order_ID IS NOT NULL AND o.DocStatus NOT IN ('VO')
      WHEN $P{StatusOrder}='SONC' THEN o.C_Order_ID IS NULL AND po.DocStatus='CO'
      WHEN $P{StatusOrder}='SOV' THEN o.C_Order_ID IS NOT NULL AND o.DocStatus='VO'
      WHEN $P{StatusOrder}='POV' THEN o.C_Order_ID IS NULL AND po.DocStatus='CL'
      WHEN $P{StatusOrder}='PLNC' THEN pl.UNS_PackingList_id IS NULL OR pl.DocStatus IN ('VO', 'RE')
      WHEN $P{StatusOrder}='PLC' THEN pl.uns_packinglist_id IS NOT NULL AND pl.DocStatus IN ('DR', 'IP', 'IN', 'CO', 'CL')
      ELSE 1=1
END) AND
(CASE WHEN $P{UNS_Shipping_Track_ID} IS NOT NULL THEN po.UNS_Shipping_Track_ID = $P{UNS_Shipping_Track_ID}
      ELSE 1=1 END)

ORDER BY po.uns_shipping_track_id, po.datepromised]]>
	</queryString>
	<field name="nopreorder" class="java.lang.String"/>
	<field name="created" class="java.sql.Timestamp"/>
	<field name="namatoko" class="java.lang.String"/>
	<field name="alamattoko" class="java.lang.String"/>
	<field name="rayon" class="java.lang.String"/>
	<field name="user" class="java.lang.String"/>
	<field name="namapelanggan" class="java.lang.String"/>
	<field name="alamatpelanggan" class="java.lang.String"/>
	<field name="ket" class="java.lang.String"/>
	<field name="noso" class="java.lang.String"/>
	<field name="statusorder" class="java.lang.String"/>
	<field name="statuspacking" class="java.lang.String"/>
	<field name="paramtoko" class="java.lang.String"/>
	<field name="product" class="java.lang.String"/>
	<field name="qty" class="java.math.BigDecimal"/>
	<field name="qtyordered" class="java.math.BigDecimal"/>
	<field name="qtypacked" class="java.math.BigDecimal"/>
	<field name="qtydelivered" class="java.math.BigDecimal"/>
	<field name="uom" class="java.lang.String"/>
	<field name="shippingtrack" class="java.lang.String"/>
	<field name="sjlokasi" class="java.lang.String"/>
	<variable name="Number" class="java.lang.Number" calculation="DistinctCount">
		<variableExpression><![CDATA[$V{Number} == null ? 0 : $V{Number}+1]]></variableExpression>
	</variable>
	<group name="ShippingTrack" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{shippingtrack}]]></groupExpression>
		<groupHeader>
			<band height="21">
				<textField>
					<reportElement isPrintRepeatedValues="false" x="1" y="0" width="316" height="20" uuid="18fc31c6-6cd7-479a-8857-c439f80ae4c9"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{shippingtrack}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="5"/>
		</groupFooter>
	</group>
	<group name="PreOrder">
		<groupExpression><![CDATA[$F{nopreorder}]]></groupExpression>
		<groupHeader>
			<band height="87">
				<staticText>
					<reportElement x="331" y="67" width="59" height="20" uuid="be7e1bd3-8d15-426e-b284-2aa1c33f4d0c"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Qty]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement x="18" y="50" width="537" height="17" isRemoveLineWhenBlank="true" uuid="311c83be-f7cf-4a4a-a12e-72b17cd0f616">
						<printWhenExpression><![CDATA[$F{namapelanggan} != null]]></printWhenExpression>
					</reportElement>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="false" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA["#Lokasi : " + $F{namapelanggan} + " - " + $F{alamatpelanggan}+" ( " +$F{sjlokasi}+ " ) "]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="18" y="1" width="537" height="17" uuid="759d1411-c600-400b-a777-d53dcf218fd2"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA["#No Pre Order : " + $F{nopreorder} + " #User : " + $F{user} + "(" + new SimpleDateFormat("dd-MM-yyyy H:m").format($F{created}) + ")" +"  #No SO : " +  ($F{noso} == null ? 'Belum Dibuat' :$F{noso})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="502" y="67" width="53" height="20" uuid="d4175de0-57d1-4408-ba6b-482b3935aae8"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[UOM]]></text>
				</staticText>
				<staticText>
					<reportElement x="18" y="67" width="313" height="20" uuid="b60f90b5-3465-48b8-80e7-7001812aaec8"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Product]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="18" y="17" width="537" height="17" uuid="ce9d3a99-5f12-4515-85ef-05ca5236eeb0"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{namatoko} + " - " + $F{alamattoko} + " (" + ($F{ket} == null ? ' ' : $F{ket})+ ")"]]></textFieldExpression>
				</textField>
				<textField pattern="###0" isBlankWhenNull="true">
					<reportElement x="0" y="1" width="18" height="20" isRemoveLineWhenBlank="true" uuid="9ef4dab8-7bdd-4950-bb64-a298f9b208f3"/>
					<box>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{Number}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="18" y="34" width="537" height="17" uuid="f281cb77-143e-4439-80d2-b62cd39002a4"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA["#Status Order : " +$F{statusorder} + "         #Packing List : "+$F{statuspacking}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="390" y="67" width="56" height="20" uuid="7ca7ac8c-3468-4452-b523-91a02efbc31d"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[QtyPck]]></text>
				</staticText>
				<staticText>
					<reportElement x="446" y="67" width="56" height="20" uuid="d14dd52a-4727-4b41-b9cf-c9dcac13ba98"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[QtyDel]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="15">
				<line>
					<reportElement x="19" y="-1" width="536" height="1" uuid="b619abf0-2ed2-42f1-a791-767f2786849d"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="19" splitType="Stretch">
			<textField>
				<reportElement x="0" y="-1" width="555" height="20" uuid="eca2aa89-9331-4130-9d8d-95f1e8855072"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Parameter #Business Partner : " + $F{paramtoko} + " #Date : " + new SimpleDateFormat("dd-MM-yyy H:m").format($P{StartDate}) + " - " + new SimpleDateFormat("dd-MM-yyyy H:m").format($P{EndDate})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<detail>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement x="18" y="0" width="313" height="20" uuid="17005184-4043-472f-af87-3b597b8f3caa"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{product}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="331" y="0" width="59" height="20" uuid="40b7f22d-64f7-4df2-9ba8-520feff7a6d5"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="502" y="0" width="53" height="20" uuid="1758f0aa-195a-437e-88e5-59ffd3982262"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{uom}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="390" y="0" width="56" height="20" uuid="ddca11b0-0271-4de7-b1e6-382b67a84e42"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qtypacked}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="446" y="0" width="56" height="20" uuid="731ec09a-ac01-48c6-b086-68efb52cc7d1"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qtydelivered}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
