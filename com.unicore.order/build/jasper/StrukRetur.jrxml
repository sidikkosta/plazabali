<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="StrukPenjualan" language="groovy" pageWidth="209" pageHeight="396" columnWidth="189" leftMargin="10" rightMargin="10" topMargin="18" bottomMargin="7" uuid="0c803c06-40d8-4d3f-971e-93d2fd0589cd">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="AD_User_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="UNS_POSTrx_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["./"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT distinct POS.DocumentNo AS DocNo,
PDK.Name AS PDK,
PDK.Value AS KDPDK,
BP.Name AS BP,
POSL.QtyEntered AS Qty,
POSL.PriceActual AS Harga,
POSL.PriceList AS Daftar,
POSL.QtyEntered * POSL.PriceList AS tot,
COALESCE (POSL.DiscountAmt,0) AS Disc,
POS.DiscountAmt AS DiscDoc,
NOW(),
AU.Name AS Nama,
POS.C_DocType_ID AS type

FROM UNS_POSTrx POS
INNER JOIN UNS_POSTrxLine POSL ON POSL.UNS_POSTrx_ID=POS.UNS_POSTrx_ID
INNER JOIN C_BPartner BP ON BP.C_BPartner_ID=POS.C_BPartner_ID
INNER JOIN M_Product PDK ON PDK.M_Product_ID=POSL.M_Product_ID
INNER JOIN AD_User AU ON AU.AD_User_ID=$P{AD_User_ID}

WHERE POS.UNS_POSTrx_ID=$P{UNS_POSTrx_ID} AND POS.DocStatus IN ('CO', 'CL', 'RE')

GROUP BY DocNo, PDK, KDPDK, BP, Qty, Harga, Daftar, tot, Nama, Disc, DiscDoc, type]]>
	</queryString>
	<field name="docno" class="java.lang.String"/>
	<field name="pdk" class="java.lang.String"/>
	<field name="kdpdk" class="java.lang.String"/>
	<field name="bp" class="java.lang.String"/>
	<field name="qty" class="java.math.BigDecimal"/>
	<field name="harga" class="java.math.BigDecimal"/>
	<field name="daftar" class="java.math.BigDecimal"/>
	<field name="tot" class="java.math.BigDecimal"/>
	<field name="disc" class="java.math.BigDecimal"/>
	<field name="discdoc" class="java.math.BigDecimal"/>
	<field name="now" class="java.sql.Timestamp"/>
	<field name="nama" class="java.lang.String"/>
	<field name="type" class="java.math.BigDecimal"/>
	<variable name="jumlah" class="java.lang.String">
		<variableExpression><![CDATA[$F{qty}*$F{harga}]]></variableExpression>
	</variable>
	<variable name="tot_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{tot}]]></variableExpression>
	</variable>
	<variable name="tot_2" class="java.math.BigDecimal" resetType="Group" resetGroup="DOC" calculation="Sum">
		<variableExpression><![CDATA[$F{tot}]]></variableExpression>
	</variable>
	<variable name="disc_1" class="java.math.BigDecimal" resetType="Group" resetGroup="DOC" calculation="Sum">
		<variableExpression><![CDATA[$F{disc}]]></variableExpression>
	</variable>
	<group name="DOC">
		<groupExpression><![CDATA[$F{docno}]]></groupExpression>
		<groupHeader>
			<band height="2">
				<line>
					<reportElement x="11" y="0" width="172" height="1" uuid="ad20cd85-fffc-4824-889b-39a2ef83cfab"/>
					<graphicElement>
						<pen lineStyle="Double"/>
					</graphicElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="94">
				<staticText>
					<reportElement x="9" y="5" width="44" height="12" uuid="eb6b3423-298c-427d-978c-4fd3c6ec655f"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[Total Item]]></text>
				</staticText>
				<line>
					<reportElement x="11" y="1" width="172" height="1" uuid="fde6c949-e74f-41f1-83f3-1f1346ec3590"/>
					<graphicElement>
						<pen lineStyle="Double"/>
					</graphicElement>
				</line>
				<textField>
					<reportElement x="53" y="5" width="13" height="12" uuid="6a57cd61-138a-426a-94b8-c774daee83ff"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA["#"+$V{COLUMN_COUNT}]]></textFieldExpression>
				</textField>
				<subreport>
					<reportElement x="6" y="39" width="174" height="55" uuid="7caa0d9d-acd5-4273-a7c3-da81e9d47e22"/>
					<subreportParameter name="UNS_POSTrx_ID">
						<subreportParameterExpression><![CDATA[$P{UNS_POSTrx_ID}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "StrukRetur_Subreport1.jasper"]]></subreportExpression>
				</subreport>
				<textField pattern="#,##0">
					<reportElement x="139" y="5" width="45" height="12" uuid="c80c5eb0-8420-4816-812c-3afc8dbae0da"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA['-' + $V{tot_2}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="9" y="16" width="44" height="12" uuid="8a1edba2-abc1-4d79-ac84-95200dc37581"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[Total Disc]]></text>
				</staticText>
				<textField pattern="#,##0">
					<reportElement x="139" y="16" width="45" height="12" uuid="4d362744-eff5-496f-a355-5f84b604b5ba"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{disc_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="128" y="27" width="56" height="12" uuid="8c195d8e-5305-4b44-ad5f-0631a043accc"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{discdoc}==null ? 0 : $F{discdoc}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="9" y="27" width="44" height="12" uuid="1cfed3d5-57ef-48a2-90e0-62f3f4d4bfa3"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[Disc Doc.]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="51" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="1" width="189" height="20" uuid="0b80693a-c8ab-4bcf-b540-e7e57d877bb8"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[KOPERASI BEA CUKAI]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy HH:mm:ss">
				<reportElement x="9" y="39" width="44" height="12" uuid="a753169f-4e91-4472-922d-1c9cef9228d2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{now}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="132" y="39" width="52" height="12" uuid="88e32208-21dd-4ec0-af75-d95f22913386"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["#"+$F{docno}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="73" y="39" width="59" height="12" uuid="cc1a0aee-8e39-4e86-bb9a-c9c29092e372"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["#" +$F{nama}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement mode="Transparent" x="0" y="19" width="189" height="20" forecolor="#000000" backcolor="#FFFFFF" uuid="8394f46c-6c31-4cee-b8a8-8fa14b36e3b9"/>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="SansSerif" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["JL. A. YANI TELP. 021-4890308 EXT 184/884 021-4712868"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="145" y="2" width="44" height="10" uuid="2a1d37c8-df99-4439-95a3-e86a2a34ff00"/>
				<textElement textAlignment="Right">
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{type}==1000549 ? "#RETUR#" : '']]></textFieldExpression>
			</textField>
		</band>
	</title>
	<detail>
		<band height="13" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement x="9" y="0" width="72" height="12" uuid="deca4ef1-9e3b-4afc-81c4-763481139a3a"/>
				<textElement verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pdk}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0">
				<reportElement x="83" y="0" width="56" height="12" uuid="f0c205db-21d9-40a6-ae84-d9550ad24d8f"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[NumberFormat.getInstance().format($F{qty})+" X " + NumberFormat.getInstance().format($F{daftar})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0">
				<reportElement x="145" y="0" width="39" height="12" uuid="de803248-65ad-4fa5-8cd6-2959a8525ec0"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA['-' + $F{tot}]]></textFieldExpression>
			</textField>
		</band>
		<band height="13">
			<printWhenExpression><![CDATA[$F{disc} != 0]]></printWhenExpression>
			<textField pattern="#,##0">
				<reportElement x="45" y="0" width="35" height="12" uuid="23940e42-9289-4a15-9d80-a72cb2fd6600"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{disc}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="9" y="0" width="23" height="12" uuid="9678493e-b1e7-47fa-8a4e-eb03a4631196">
					<printWhenExpression><![CDATA[$F{disc} != 0]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Disc.]]></text>
			</staticText>
		</band>
	</detail>
</jasperReport>
