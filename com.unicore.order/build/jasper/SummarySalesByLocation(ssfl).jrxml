<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="SummarySalesByLocation" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="ed323643-04f3-48b5-90a2-9c0ca04c452b">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="IsTradingDate" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Location" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
ps.dateacct,
pt.DateTrx :: date AS DateTrx,
bp.value as kdshop,
bp.name as shop,
sum(ptl.LineAmt) AS LineAmt,
SUM(ptl.DiscountAmt) + pt.DiscountAmt AS Diskon,
sum(ptl.LineNetAmt) AS Net,
now(),
pt.GrandTotal AS gtotal,
pt.totalamt AS total,
pt.uns_postrx_id

FROM UNS_POSTrx pt

INNER JOIN UNS_POSTrxLine ptl ON ptl.UNS_POSTrx_ID=pt.UNS_POSTrx_ID
INNER JOIN UNS_POS_Session ps ON ps.UNS_POS_Session_ID=pt.UNS_POS_Session_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID=pt.C_BPartner_ID

WHERE  pt.DocStatus IN ('CO','CL')
AND pt.IsPaid='Y' and ps.IsTrialMode='N'
AND CASE WHEN $P{Location} IS NOT NULL THEN bp.value LIKE '%$P!{Location}%' ELSE 1=1 END AND

(CASE WHEN $P{IsTradingDate} = 'Y' THEN (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN ps.dateacct >= $P{DateFrom} AND ps.dateacct  <= $P{DateTo} WHEN $P{DateFrom}::timestamp IS NOT NULL and $P{DateTo}::timestamp IS NULL THEN ps.dateacct >= $P{DateFrom} WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN ps.dateacct  <= $P{DateTo} ELSE ps.dateacct  <= now() END)

ELSE

(CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN pt.DateTrx >= $P{DateFrom} AND pt.DateTrx  <= $P{DateTo} WHEN $P{DateFrom}::timestamp IS NOT NULL and $P{DateTo}::timestamp IS NULL THEN pt.DateTrx >= $P{DateFrom} WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN pt.DateTrx  <= $P{DateTo} ELSE pt.DateTrx  <= now() END) END)

GROUP BY ps.dateacct, pt.DateTrx,bp.name, pt.GrandTotal, pt.totalamt, pt.uns_postrx_id, bp.value
ORDER BY ps.DateAcct,pt.DateTrx]]>
	</queryString>
	<field name="dateacct" class="java.sql.Timestamp"/>
	<field name="datetrx" class="java.sql.Date"/>
	<field name="kdshop" class="java.lang.String"/>
	<field name="shop" class="java.lang.String"/>
	<field name="lineamt" class="java.math.BigDecimal"/>
	<field name="diskon" class="java.math.BigDecimal"/>
	<field name="net" class="java.math.BigDecimal"/>
	<field name="now" class="java.sql.Timestamp"/>
	<field name="gtotal" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="uns_postrx_id" class="java.math.BigDecimal"/>
	<variable name="SumTrx" class="java.math.BigDecimal" resetType="Group" resetGroup="DateAcct" calculation="Count">
		<variableExpression><![CDATA[$F{uns_postrx_id}]]></variableExpression>
	</variable>
	<variable name="SumLineAmt" class="java.math.BigDecimal" resetType="Group" resetGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$F{gtotal}+$F{diskon}]]></variableExpression>
	</variable>
	<variable name="SumDisc" class="java.math.BigDecimal" resetType="Group" resetGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$F{diskon}]]></variableExpression>
	</variable>
	<variable name="SumNetAmt" class="java.math.BigDecimal" resetType="Group" resetGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$F{gtotal}]]></variableExpression>
	</variable>
	<variable name="NetSales/Trans" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{SumNetAmt}/$V{SumTrx}]]></variableExpression>
	</variable>
	<variable name="ShopSumTrx" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$V{SumTrx}]]></variableExpression>
	</variable>
	<variable name="ShopSumLineAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$V{SumLineAmt}]]></variableExpression>
	</variable>
	<variable name="ShopDiscAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$V{SumDisc}]]></variableExpression>
	</variable>
	<variable name="ShopSumNetAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$V{SumNetAmt}]]></variableExpression>
	</variable>
	<variable name="ShopSumNetSales/Trans" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Sum">
		<variableExpression><![CDATA[$V{NetSales/Trans}]]></variableExpression>
	</variable>
	<variable name="AvgSumLineAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Average">
		<variableExpression><![CDATA[$V{SumLineAmt}]]></variableExpression>
	</variable>
	<variable name="AvgDiscAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Average">
		<variableExpression><![CDATA[$V{SumDisc}]]></variableExpression>
	</variable>
	<variable name="AvgSumNetAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Average">
		<variableExpression><![CDATA[$V{SumNetAmt}]]></variableExpression>
	</variable>
	<variable name="AvgSumNetSales/Trans" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Average">
		<variableExpression><![CDATA[$V{NetSales/Trans}]]></variableExpression>
	</variable>
	<variable name="AvgSumTrx" class="java.math.BigDecimal" incrementType="Group" incrementGroup="DateAcct" calculation="Average">
		<variableExpression><![CDATA[$V{SumTrx}]]></variableExpression>
	</variable>
	<variable name="CountDate" class="java.lang.Integer" incrementType="Group" incrementGroup="DateAcct" calculation="Count">
		<variableExpression><![CDATA[$F{dateacct}]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<group name="DateAcct">
		<groupExpression><![CDATA[$P{IsTradingDate} == 'Y' ? $F{dateacct} : $F{datetrx}]]></groupExpression>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0">
					<reportElement x="400" y="0" width="47" height="20" uuid="2c6be3c5-f680-4234-b1d8-e4ac746ea872"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{SumTrx}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="100" y="0" width="100" height="20" uuid="2938c43b-b1a8-4575-9a83-1e89b759c603"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{SumLineAmt}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="200" y="0" width="100" height="20" uuid="28a3eb37-ea67-47c9-851a-dcec47114e64"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{SumDisc}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="300" y="0" width="100" height="20" uuid="a395bb98-a7e5-4d32-a677-9f0353fe86b3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{SumNetAmt}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="447" y="0" width="108" height="20" uuid="9d85f832-5031-4205-8300-bb276bf7bcfa"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{NetSales/Trans}]]></textFieldExpression>
				</textField>
				<textField pattern="dd MMM yyyy">
					<reportElement x="0" y="0" width="100" height="20" uuid="c6a2edcb-58ab-4a61-9053-62ebaa0e1b6a"/>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$P{IsTradingDate} == 'Y' ? $F{dateacct} :$F{datetrx}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="76" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="555" height="29" uuid="24511d2b-ea51-45c3-9a43-fcf17cb4fe47"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true" isUnderline="true"/>
				</textElement>
				<text><![CDATA[SUMMARY SALES BY LOCATION]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="29" width="200" height="15" uuid="634c34ea-927b-4639-9ae9-c5a4cfeffc94"/>
				<textFieldExpression><![CDATA[$P{IsTradingDate} == 'Y' ? 'Base On Trading Date' : 'Base On Actual Date']]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="44" width="100" height="17" uuid="7e7944db-b856-4e6b-9e17-dcb97da8a2d8"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Location]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="61" width="100" height="15" uuid="306738fc-cd55-4dc1-b129-1893421dc0a3"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Period ]]></text>
			</staticText>
			<textField>
				<reportElement x="100" y="44" width="200" height="17" uuid="ccd55c55-a71c-47db-8720-7ffb6e64ec13"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{Location} == null ? ": All Location" : ": "+$P{Location}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy h.mm a">
				<reportElement x="447" y="29" width="108" height="15" uuid="aebc0a60-ab21-4a3d-a703-a0e74b576d3e"/>
				<textFieldExpression><![CDATA[$F{now}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="447" y="44" width="108" height="17" uuid="15527fdd-a3f9-4a23-99b2-fb21e84efb18"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy">
				<reportElement x="100" y="61" width="200" height="15" uuid="6fa26ba9-0027-426b-aa16-667f93e2e44b"/>
				<textFieldExpression><![CDATA[": "+
new SimpleDateFormat("dd/MM/yyyy").format($P{DateFrom})+" to "+
new SimpleDateFormat("dd/MM/yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="29">
			<staticText>
				<reportElement x="0" y="0" width="100" height="29" uuid="8db7da02-dfa7-4770-b658-2bf3421012cf"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[DATE]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="0" width="100" height="29" uuid="c8f9fbad-cfa5-4ff4-b65f-31cd32e98d96"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[GROSS SALES]]></text>
			</staticText>
			<staticText>
				<reportElement x="200" y="0" width="100" height="29" uuid="6833b945-633c-4fc9-87d8-700a1b09cee3"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[DISCOUNT]]></text>
			</staticText>
			<staticText>
				<reportElement x="300" y="0" width="100" height="29" uuid="69edbb33-5236-4c72-b630-867041ea51a0"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[NET SALES]]></text>
			</staticText>
			<staticText>
				<reportElement x="400" y="0" width="47" height="29" uuid="ffba350e-d35a-42b0-9617-763de633f1e8"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[# OF TRANS]]></text>
			</staticText>
			<staticText>
				<reportElement x="447" y="0" width="108" height="29" uuid="9a9ed27a-9300-4462-b930-8b8d9e5135a3"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[NET SALES / TRANS]]></text>
			</staticText>
		</band>
	</columnHeader>
	<summary>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="20" width="100" height="20" uuid="0dbfef3c-6869-4455-8dff-a3fc1febac57"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[AVERAGE]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="100" y="20" width="100" height="20" uuid="99d35a79-a980-48de-8319-fa92091e571e"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{AvgSumLineAmt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="20" width="100" height="20" uuid="1b219ad8-e637-4584-a887-ddb59cbd7eb1"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{AvgDiscAmt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="300" y="20" width="100" height="20" uuid="cd2ceec4-4075-4fdb-b69b-9e4cfca1352a"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{AvgSumNetAmt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="447" y="20" width="108" height="20" uuid="958624b0-9584-468b-ac56-e48b4af33b9e"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{AvgSumNetSales/Trans}]]></textFieldExpression>
			</textField>
			<textField pattern="###0">
				<reportElement x="400" y="20" width="47" height="20" uuid="6c96e210-c2fd-4b76-8515-99d3e1ede346"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{AvgSumTrx}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="100" y="0" width="100" height="20" uuid="91a4501f-892e-431c-95ef-bf2ec6a2092a"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ShopSumLineAmt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="0" width="100" height="20" uuid="7cab1d85-c678-4fcc-8ed0-52038f407245"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ShopDiscAmt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="300" y="0" width="100" height="20" uuid="abb8249e-43c5-43a7-a554-baee84f3b0cd"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ShopSumNetAmt}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="400" y="0" width="47" height="20" uuid="a25098ba-3e19-42be-80b9-d077fda985cd"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ShopSumTrx}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="447" y="0" width="108" height="20" uuid="a586f456-deba-490b-8940-416e13b5a940"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ShopSumNetSales/Trans}]]></textFieldExpression>
			</textField>
			<textField pattern="###0">
				<reportElement x="0" y="0" width="100" height="20" uuid="efead7fe-79bf-422c-a9f7-be17b2dda76f"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA['TOTAL '+$V{CountDate}+' days']]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
