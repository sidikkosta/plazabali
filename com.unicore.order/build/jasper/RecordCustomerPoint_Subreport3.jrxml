<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RecordCustomerPoint_Subreport3" language="groovy" pageWidth="595" pageHeight="842" columnWidth="595" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="67ed0cc8-377d-4d63-980a-b49370747c32">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="isDetailTransaction" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT BPartner, Location, SUM(PointValid) AS PV, SUM(PointNValid) AS PNYV, Date, Reedem, (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN BPartner ELSE 'All Business Partner' END) AS ParamBPartner FROM
(
SELECT bp.Name AS BPartner, bpl.Name AS Location, iv.dateinvoiced AS Date,
COALESCE(SUM(cplv.Point),0) AS PointValid, 0 AS PointNValid, cp.AccumulatedRedeemed AS Reedem

FROM UNS_CustomerPoint cp

INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = cp.C_BPartner_ID
INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_ID = bp.C_BPartner_ID
INNER JOIN UNS_CustomerPoint_Line cplv ON cplv.UNS_CustomerPoint_ID = cp.UNS_CustomerPoint_ID
INNER JOIN C_InvoiceLine ilv ON ilv.C_InvoiceLine_ID = cplv.C_InvoiceLine_ID
INNER JOIN C_Invoice iv ON iv.C_Invoice_ID = ilv.C_Invoice_ID AND invoiceopen(iv.C_Invoice_ID, 0) <= 0

WHERE (CASE WHEN $P{DateFrom}::Timestamp IS NOT NULL THEN iv.DateInvoiced > $P{DateFrom}::Timestamp
WHEN $P{DateTo}::Timestamp IS NOT NULL THEN iv.DateInvoiced < $P{DateTo}::Timestamp
WHEN $P{DateFrom}::Timestamp IS NOT NULL AND $P{DateTo}::Timestamp IS NOT NULL THEN iv.DateInvoiced BETWEEN $P{DateFrom}::Timestamp AND $P{DateTo}::Timestamp ELSE 1=1 END)
AND (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN bp.C_BPartner_ID = $P{C_BPartner_ID} ELSE 1=1 END)

GROUP BY BPartner, Location, Date, Reedem

UNION

SELECT bp.Name AS BPartner, bpl.Name AS Location, inv.dateinvoiced AS Date,
0, COALESCE(SUM(cplnv.Point),0) AS PointNValid, cp.AccumulatedRedeemed AS Reedem

FROM UNS_CustomerPoint cp

INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = cp.C_BPartner_ID
INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_ID = bp.C_BPartner_ID
INNER JOIN UNS_CustomerPoint_Line cplnv ON cplnv.UNS_CustomerPoint_ID = cp.UNS_CustomerPoint_ID
INNER JOIN C_InvoiceLine ilnv ON ilnv.C_InvoiceLine_ID = cplnv.C_InvoiceLine_ID
INNER JOIN C_Invoice inv ON inv.C_Invoice_ID = ilnv.C_Invoice_ID AND invoiceopen(inv.C_Invoice_ID, 0) > 0

WHERE (CASE WHEN $P{DateFrom}::Timestamp IS NOT NULL THEN inv.DateInvoiced > $P{DateFrom}::Timestamp
WHEN $P{DateTo}::Timestamp IS NOT NULL THEN inv.DateInvoiced < $P{DateTo}::Timestamp
WHEN $P{DateFrom}::Timestamp IS NOT NULL AND $P{DateTo}::Timestamp IS NOT NULL THEN inv.DateInvoiced BETWEEN $P{DateFrom}::Timestamp AND $P{DateTo} ELSE 1=1 END) AND 'SRK'=$P{isDetailTransaction}
AND (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN bp.C_BPartner_ID = $P{C_BPartner_ID} ELSE 1=1 END)

GROUP BY BPartner, Location, Date, Reedem

) AS UnionPoint

GROUP BY BPartner, Location, Date, Reedem

ORDER BY BPartner ASC]]>
	</queryString>
	<field name="bpartner" class="java.lang.String"/>
	<field name="location" class="java.lang.String"/>
	<field name="pv" class="java.math.BigDecimal"/>
	<field name="pnyv" class="java.math.BigDecimal"/>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="reedem" class="java.math.BigDecimal"/>
	<field name="parambpartner" class="java.lang.String"/>
	<variable name="SUMPV" class="java.math.BigDecimal" resetType="Group" resetGroup="BPartner" calculation="Sum">
		<variableExpression><![CDATA[$F{pv}]]></variableExpression>
	</variable>
	<variable name="SUMPNV" class="java.math.BigDecimal" resetType="Group" resetGroup="BPartner" calculation="Sum">
		<variableExpression><![CDATA[$F{pnyv}]]></variableExpression>
	</variable>
	<variable name="currentpoint" class="java.math.BigDecimal" resetType="Group" resetGroup="BPartner">
		<variableExpression><![CDATA[$V{SUMPV}.subtract($F{reedem})]]></variableExpression>
	</variable>
	<group name="BPartner">
		<groupExpression><![CDATA[$F{bpartner}]]></groupExpression>
		<groupHeader>
			<band height="30">
				<textField>
					<reportElement x="0" y="0" width="555" height="15" isRemoveLineWhenBlank="true" uuid="601cb2e7-2c32-4754-9cba-8ccd401c6c7b">
						<printWhenExpression><![CDATA[$P{C_BPartner_ID} == null]]></printWhenExpression>
					</reportElement>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" isBold="true" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{bpartner} + " - " + $F{location}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="15" width="185" height="15" uuid="04c62009-1930-4b3e-a436-8e589214736d"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="SansSerif" isBold="true"/>
					</textElement>
					<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement x="185" y="15" width="185" height="15" uuid="203c70ea-cdf5-41ed-9ffa-1959a297da52"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="SansSerif" isBold="true"/>
					</textElement>
					<text><![CDATA[Valid Point]]></text>
				</staticText>
				<staticText>
					<reportElement x="370" y="15" width="185" height="15" uuid="6fc406d9-9560-4032-81ef-7df22f72cdf1"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="SansSerif" isBold="true"/>
					</textElement>
					<text><![CDATA[Not Yet Valid Point]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="30">
				<textField pattern="#,##0.00;(#,##0.00)">
					<reportElement x="0" y="0" width="185" height="15" uuid="426268dd-2f0e-4161-8304-cea443116402"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif"/>
					</textElement>
					<textFieldExpression><![CDATA["Redeemed Point :: " + $F{reedem}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;(-#,##0.00)">
					<reportElement x="185" y="0" width="185" height="15" uuid="6360a1b1-0bb3-4f68-b4cd-e4637232bf62"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif"/>
					</textElement>
					<textFieldExpression><![CDATA["Summary Valid Point :: " + $V{SUMPV}.setScale(2, 3)]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00;(#,##0.00)">
					<reportElement x="370" y="0" width="185" height="15" uuid="3f5789d2-94a6-4160-b47c-9c3ea5be9824"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif"/>
					</textElement>
					<textFieldExpression><![CDATA["Summary Not Valid Point :: " + $V{SUMPNV}.setScale(2,3)]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="0" y="15" width="555" height="15" uuid="2e1256d5-5124-4a6f-aa76-1bbe057514c3"/>
					<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="SansSerif" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Avaliable Point for Redeemed :: " + $V{currentpoint}.setScale(2,3)]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="75" splitType="Stretch">
			<textField>
				<reportElement x="1" y="52" width="555" height="20" uuid="f98b5558-bb78-4240-90fc-5d4d20a7a007"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Parameter #Business Partner :: " + $F{parambpartner}+" # Periode :: "+  new SimpleDateFormat("dd-MM-yyyy").format($P{DateFrom}) +" s/d "+ new SimpleDateFormat("dd-MM-yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
			<image>
				<reportElement x="1" y="0" width="556" height="51" uuid="2a12dfd4-4a91-4e7a-9ef7-11e66998d578"/>
				<imageExpression><![CDATA["/root/work/dev/UntaCore_TMG/com.unicore.order/src/jasper/images/TMGPoint.png"]]></imageExpression>
			</image>
			<staticText>
				<reportElement x="138" y="17" width="397" height="46" uuid="7f856ef7-7d20-4992-81e1-3524e66a8f56"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="17" isBold="true" isItalic="true"/>
				</textElement>
				<text><![CDATA[CUSTOMER POINT]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="15" splitType="Stretch">
			<textField pattern="dd/MM/yyyy">
				<reportElement x="0" y="0" width="185" height="15" uuid="977f85a7-42ee-4235-ae97-065e9f7c4dca"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{date}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;(-#,##0.00)">
				<reportElement x="185" y="0" width="185" height="15" uuid="5f11fa15-bc55-4b8e-a1c8-98fbfdff4014"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pv}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;(#,##0.00)">
				<reportElement x="370" y="0" width="185" height="15" uuid="05f13bdf-b162-4f23-99b5-ee291ef379dc"/>
				<box topPadding="1" leftPadding="5" bottomPadding="1" rightPadding="5">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pnyv}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
