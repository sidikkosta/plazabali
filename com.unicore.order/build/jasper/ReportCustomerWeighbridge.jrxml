<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportCustomerWeighbridge" language="groovy" pageWidth="792" pageHeight="612" orientation="Landscape" columnWidth="752" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="b01c6892-68bf-4583-bcd7-5ec7d7c4cc92">
	<property name="ireport.zoom" value="1.000000000000004"/>
	<property name="ireport.x" value="7"/>
	<property name="ireport.y" value="0"/>
	<style name="style1">
		<conditionalStyle>
			<conditionExpression><![CDATA[$F{split} == "Y"]]></conditionExpression>
			<style mode="Transparent" forecolor="#FF0000">
				<box>
					<pen lineColor="#000000"/>
					<topPen lineColor="#000000"/>
					<leftPen lineColor="#000000"/>
					<bottomPen lineWidth="0.25" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineColor="#000000"/>
				</box>
			</style>
		</conditionalStyle>
	</style>
	<parameter name="C_Order_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["D:\\dev\\tmg\\project\\com.unicore.order\\src\\jasper\\"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	co.documentno,
	co.poreference AS contractno,
	co.ReferenceNo AS DOno,
	bp.Name AS buyer,
	wtc.timein AS TimeInCustomer,
	wtc.timeout AS TimeOutCustomer,
	wtc.vehicleno AS PlatNo,
	wt.nettoii AS DOQty,
	wt.DocumentNo AS ticket,
	wt.TimeOut AS TimeOutOwn,
	wtc.nettoii AS BuyerQty,
	coln.qtyentered AS qty,
	p.Name AS product,
	ad.Name AS own,
	wt.issplitorder AS split,
	wt.SplittedTicket_ID AS splitticket

FROM UNS_WBTicket_Confirm wtc

INNER JOIN
	uns_weighbridgeticket wt ON wt.uns_weighbridgeticket_id=wtc.uns_weighbridgeticket_id
INNER JOIN
	c_order co ON co.c_order_id=wtc.c_order_id
INNER JOIN
	C_OrderLine coln ON coln.c_order_id=co.c_order_id
INNER JOIN
	C_BPartner bp ON bp.C_BPartner_ID=co.C_BPartner_ID
INNER JOIN
	M_Product p ON p.M_Product_ID=coln.M_Product_ID
INNER JOIN
	AD_Client ad ON ad.AD_Client_ID=wt.AD_Client_ID

WHERE
co.C_Order_ID = $P{C_Order_ID}
AND
wt.DocStatus IN ('CO','CL')
ORDER BY wt.TimeOut]]>
	</queryString>
	<field name="documentno" class="java.lang.String"/>
	<field name="contractno" class="java.lang.String"/>
	<field name="dono" class="java.lang.String"/>
	<field name="buyer" class="java.lang.String"/>
	<field name="timeincustomer" class="java.sql.Timestamp"/>
	<field name="timeoutcustomer" class="java.sql.Timestamp"/>
	<field name="platno" class="java.lang.String"/>
	<field name="doqty" class="java.math.BigDecimal"/>
	<field name="ticket" class="java.lang.String"/>
	<field name="timeoutown" class="java.sql.Timestamp"/>
	<field name="buyerqty" class="java.math.BigDecimal"/>
	<field name="qty" class="java.math.BigDecimal"/>
	<field name="product" class="java.lang.String"/>
	<field name="own" class="java.lang.String"/>
	<field name="split" class="java.lang.String"/>
	<field name="splitticket" class="java.math.BigDecimal"/>
	<variable name="selisih" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{doqty}.subtract($F{buyerqty})]]></variableExpression>
	</variable>
	<variable name="total_DO" class="java.math.BigDecimal" resetType="Group" resetGroup="C_Order" calculation="Sum">
		<variableExpression><![CDATA[$F{doqty}]]></variableExpression>
	</variable>
	<variable name="Total_Buyer" class="java.math.BigDecimal" resetType="Group" resetGroup="C_Order" calculation="Sum">
		<variableExpression><![CDATA[$F{buyerqty}]]></variableExpression>
	</variable>
	<variable name="total_selisih" class="java.math.BigDecimal" resetType="Group" resetGroup="C_Order" calculation="Sum">
		<variableExpression><![CDATA[$V{selisih}]]></variableExpression>
	</variable>
	<variable name="No" class="java.lang.Integer" resetType="Group" resetGroup="C_Order">
		<variableExpression><![CDATA[$V{No} == null ? 1 : $V{No}+1]]></variableExpression>
	</variable>
	<variable name="sisaSutopo" class="java.lang.Integer" resetType="Group" resetGroup="C_Order">
		<variableExpression><![CDATA[$V{sisaSutopo} == null ? $F{qty}-$F{doqty} : $V{sisaSutopo}-$F{doqty}]]></variableExpression>
	</variable>
	<variable name="sisabuyer" class="java.lang.Integer" resetType="Group" resetGroup="C_Order">
		<variableExpression><![CDATA[$V{sisabuyer} == null ? $F{qty} - $F{buyerqty} : $V{sisabuyer} - $F{buyerqty}]]></variableExpression>
	</variable>
	<group name="C_Order">
		<groupExpression><![CDATA[$F{documentno}]]></groupExpression>
		<groupHeader>
			<band height="142">
				<rectangle>
					<reportElement x="483" y="2" width="269" height="89" uuid="b533acf6-b5f0-4b7b-8588-681162174a7d"/>
				</rectangle>
				<staticText>
					<reportElement x="80" y="96" width="104" height="46" uuid="dde5dc93-f4df-48e7-bed9-b4d75079497c"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Waktu Kirim]]></text>
				</staticText>
				<staticText>
					<reportElement x="184" y="96" width="106" height="46" uuid="5296403c-4db3-4337-be56-f13838ec40af"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Waktu Terima]]></text>
				</staticText>
				<staticText>
					<reportElement x="397" y="96" width="67" height="46" uuid="661003cd-0917-405a-bba3-62d75ae867a6"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[No Polisi]]></text>
				</staticText>
				<staticText>
					<reportElement x="525" y="123" width="61" height="19" uuid="cb882dc2-fbec-4784-b4ca-da460bbe6331"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Pembeli]]></text>
				</staticText>
				<staticText>
					<reportElement x="464" y="123" width="61" height="19" uuid="5000ce15-a541-4c42-8457-64a1c2b39d49"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Sutopo]]></text>
				</staticText>
				<staticText>
					<reportElement x="586" y="96" width="50" height="46" uuid="2dd1fe51-2d9f-4747-a1bf-4c5c138c901e"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Selisih]]></text>
				</staticText>
				<staticText>
					<reportElement x="290" y="96" width="107" height="46" uuid="593b0722-9bd4-48bc-8ea5-10a244738320"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Waktu Selesai Bongkar]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="96" width="18" height="46" uuid="ff91f857-16ae-434e-9eeb-b05eb9244782"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[No]]></text>
				</staticText>
				<staticText>
					<reportElement x="636" y="96" width="116" height="27" uuid="02f043bf-17a2-4423-a8af-4cfbdc983d4b"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Sisa Kontrak Bedasarkan]]></text>
				</staticText>
				<staticText>
					<reportElement x="636" y="123" width="58" height="19" uuid="40bf161b-1f08-49bf-94e0-24ee1ca91033"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Sutopo]]></text>
				</staticText>
				<staticText>
					<reportElement x="694" y="123" width="58" height="19" uuid="394acfd6-4406-4491-a10b-4dbe27de5015"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Pembeli]]></text>
				</staticText>
				<textField>
					<reportElement x="552" y="5" width="197" height="17" uuid="520b4626-3d7a-4c63-b680-5bd1290c0fbd"/>
					<textFieldExpression><![CDATA[$F{buyer}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="487" y="5" width="59" height="17" uuid="1d02d58a-26ea-4558-a3df-a103d0658d19"/>
					<text><![CDATA[Pembeli]]></text>
				</staticText>
				<staticText>
					<reportElement x="487" y="22" width="59" height="17" uuid="b9e15e0a-833e-483e-a238-c4bc64dbf389"/>
					<text><![CDATA[No Kontrak]]></text>
				</staticText>
				<staticText>
					<reportElement x="487" y="37" width="59" height="17" uuid="075e247f-7333-4283-9d9a-89a0bd6c88aa"/>
					<text><![CDATA[No DO]]></text>
				</staticText>
				<staticText>
					<reportElement x="487" y="54" width="59" height="17" uuid="cb3a82a2-0181-4856-8883-e4ed92342364"/>
					<text><![CDATA[Partay]]></text>
				</staticText>
				<staticText>
					<reportElement x="487" y="71" width="59" height="17" uuid="32d2cf8c-1048-4512-9dcf-e3d5dd1a10a7"/>
					<text><![CDATA[Produk]]></text>
				</staticText>
				<textField>
					<reportElement x="552" y="22" width="197" height="17" uuid="7207d71c-ed69-4369-a9e2-d9605fbdfaa0"/>
					<textFieldExpression><![CDATA[$F{contractno}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="552" y="37" width="197" height="17" uuid="adae5c8d-a786-4967-8e49-3dd9d58cdb7c"/>
					<textFieldExpression><![CDATA[$F{dono}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="552" y="54" width="197" height="17" uuid="aa8c34bf-56a9-4feb-af7c-97f212dc9059"/>
					<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="552" y="71" width="197" height="17" uuid="8c4561ef-469f-41c4-8bb7-d2e0e75348af"/>
					<textFieldExpression><![CDATA[$F{product}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="546" y="5" width="6" height="17" uuid="d790671e-63f0-40af-9798-45aa258d2900"/>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="546" y="22" width="6" height="17" uuid="cf6d92ea-cd4f-4b50-b13c-6b63dfd73817"/>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="546" y="37" width="6" height="17" uuid="8a379068-d6a3-4b14-8452-10488886bb6d"/>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="546" y="54" width="6" height="17" uuid="e7318219-a314-44a8-98d8-806b34231367"/>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="546" y="71" width="6" height="17" uuid="dc05b94d-8a34-4b5b-99d3-acc57496c44c"/>
					<text><![CDATA[:]]></text>
				</staticText>
				<image>
					<reportElement x="0" y="0" width="53" height="53" uuid="00b50670-2436-4163-a463-998f7f2fa769"/>
					<imageExpression><![CDATA["/images/Sutopo.png"]]></imageExpression>
				</image>
				<textField>
					<reportElement x="0" y="53" width="250" height="20" forecolor="#FF6633" uuid="1873817d-34ca-4bcf-86cb-e726de6303bd"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{own}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="18" y="96" width="62" height="46" uuid="e8b37cf4-2ed5-40dc-90da-b7b15d543634"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[No Tiket]]></text>
				</staticText>
				<staticText>
					<reportElement x="464" y="96" width="122" height="27" uuid="9c04c16f-3a62-4775-b0a5-eaba466f1e87"/>
					<box>
						<pen lineWidth="0.5"/>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Berat Bersih]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="15">
				<textField pattern="#,##0">
					<reportElement x="464" y="0" width="61" height="15" uuid="5c8e4cee-37ab-46e5-80d4-6f4d8cbbe784"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_DO}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="525" y="0" width="61" height="15" uuid="8fa4170f-836c-4f3b-aaba-e2249ea4847e"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Total_Buyer}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="586" y="0" width="50" height="15" uuid="e9e6aa89-6fd2-4c65-ad2e-272f7c6f59b5"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_selisih}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="0" width="464" height="15" uuid="e0ee9f92-802a-4e6b-9511-e52b17bdb6c1"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement x="636" y="0" width="116" height="15" uuid="87923e2e-514f-4215-8f7b-95361ae0aa98"/>
					<box>
						<topPen lineWidth="0.5"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="39" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="752" height="39" uuid="75bbabfc-61a9-4167-bf28-a8a692658d9e"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[LAPORAN TIKET PENJUALAN (KONFIRMASI PEMBELI)]]></text>
			</staticText>
			<staticText>
				<reportElement x="652" y="0" width="100" height="20" uuid="e0e146d5-4490-448b-8686-a7de3781f3bd"/>
				<textElement textAlignment="Right">
					<font size="8" isItalic="true"/>
				</textElement>
				<text><![CDATA[UntaCoreERP]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="15" splitType="Stretch">
			<textField>
				<reportElement style="style1" x="397" y="0" width="67" height="15" uuid="a1e06283-e8f1-443f-b35f-302b67135218"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{platno}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement style="style1" x="586" y="0" width="50" height="15" uuid="1f7d6fbe-801c-463e-bbc6-7da796788096"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{selisih}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement style="style1" x="464" y="0" width="61" height="15" uuid="73f2a9da-7dab-4edd-a86e-ff6fa52070f7"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{doqty}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement style="style1" x="525" y="0" width="61" height="15" uuid="e0046dfe-75d0-4a01-8704-4003abb4d93b"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{buyerqty}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MM-yyyy HH:mm">
				<reportElement style="style1" x="80" y="0" width="104" height="15" uuid="2f4bf77b-76c5-409e-a627-b5634f60d575"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{timeoutown}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MM-yyyy HH:mm">
				<reportElement style="style1" x="184" y="0" width="106" height="15" uuid="26a92db8-9a23-4305-8d96-c730105fa379"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{timeincustomer}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MM-yyyy HH:mm">
				<reportElement style="style1" x="290" y="0" width="107" height="15" uuid="38e48923-2265-4175-988f-4e25aa4faf48"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{timeoutcustomer}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="style1" x="0" y="0" width="18" height="15" uuid="a111a061-a0c7-43d3-b49b-d1a072bbe201"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="1"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{No}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="style1" x="18" y="0" width="62" height="15" uuid="3c3ec273-7e67-4007-97cd-311cd4227adc"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ticket}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement style="style1" x="636" y="0" width="58" height="15" uuid="a37c4652-6334-41b4-89d2-961be10ae321"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sisaSutopo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement style="style1" x="694" y="0" width="58" height="15" uuid="678e5348-5259-400e-a156-48db883f66fe"/>
				<box>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sisabuyer}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="40" splitType="Stretch">
			<subreport>
				<reportElement x="0" y="6" width="752" height="34" uuid="3b3385ba-c3ed-43b9-a4da-5b6c693778c7"/>
				<subreportParameter name="C_Order_ID">
					<subreportParameterExpression><![CDATA[$P{C_Order_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "ReportCustomerWeighbridge_Subreport1.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
