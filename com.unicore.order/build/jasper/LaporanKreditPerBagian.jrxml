<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="LaporanKreditPerBagian" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="4f2fce01-1efd-4543-9ee2-2eed0424ad66">
	<property name="ireport.zoom" value="0.75"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="Bendahara" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Manager" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	SUM(ID) AS ID,
	SUM(paid) AS Paid,
	SUM(InstallmentItem) AS InstallmentItem,
	SUM(InstallmentMotor) AS InstallmentMotor,
	unitkerja,
	now() AS Skrng,
	$P{Bendahara} AS Bendahara,
	$P{Manager} AS Manager

FROM

(SELECT
	COUNT(DISTINCT(bpt.C_BPartner_ID)) AS ID,
	0 AS Paid,
	uk.name AS UnitKerja,
	SUM(CASE WHEN ca.ProductType = 'I' THEN ca.MonthlyInstallmentAmt ELSE 0 END) AS InstallmentItem,
	SUM(CASE WHEN ca.ProductType = 'V' THEN ca.MonthlyInstallmentAmt ELSE 0 END) AS InstallmentMotor,
	ca.datedoc AS date

	FROM UNS_CreditAgreement ca

	INNER JOIN
		C_BPartner bpt ON bpt.C_BPartner_ID=ca.C_BPartner_ID
	LEFT JOIN
		uns_bp_memberinfo bminfo ON bminfo.c_bpartner_id=bpt.c_bpartner_id
	LEFT JOIN
		uns_unitkerja uk ON uk.uns_unitkerja_id=bminfo.uns_unitkerja_id
	INNER JOIN
		UNS_CreditPaySchedule ps ON ps.UNS_CreditAgreement_ID=ca.UNS_CreditAgreement_ID
	INNER JOIN
		C_Invoice ci ON ci.C_Invoice_ID=ps.C_Invoice_ID


GROUP BY uk.name,date


UNION ALL

SELECT
		COUNT(DISTINCT(bpt.C_BPartner_ID)) AS ID,
		SUM(paidamt) AS Paid,
		uk.name AS UnitKerja,
		0 AS installmenti,
		0 AS installmentii,
		py.datedoc AS date

	FROM uns_pospayment py

		INNER JOIN
			C_BPartner bpt ON bpt.C_BPartner_ID=py.C_BPartner_ID AND bpt.Value NOT IN ('#NA')
		LEFT JOIN
			uns_bp_memberinfo bminfo ON bminfo.c_bpartner_id=bpt.c_bpartner_id
		LEFT JOIN
			uns_unitkerja uk ON uk.uns_unitkerja_id=bminfo.uns_unitkerja_id
		LEFT JOIN
			c_invoice ci ON ci.c_invoice_id=py.c_invoice_id

	WHERE
		paymentmethod='1' AND
		py.docstatus in ('CO','CL') AND
		ci.c_invoice_id is not null

GROUP BY uk.name,date

) AS main

WHERE	(CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL
	THEN date BETWEEN $P{DateFrom} AND $P{DateTo}::timestamp
	WHEN $P{DateFrom}::timestamp IS NOT NULL THEN date >= $P{DateFrom}::timestamp
	WHEN $P{DateTo}::timestamp IS NOT NULL THEN date <= $P{DateTo}::timestamp ELSE 1=1 END)


GROUP BY Main.UnitKerja]]>
	</queryString>
	<field name="id" class="java.math.BigDecimal"/>
	<field name="paid" class="java.math.BigDecimal"/>
	<field name="installmentitem" class="java.math.BigDecimal"/>
	<field name="installmentmotor" class="java.math.BigDecimal"/>
	<field name="unitkerja" class="java.lang.String"/>
	<field name="skrng" class="java.sql.Timestamp"/>
	<field name="bendahara" class="java.lang.String"/>
	<field name="manager" class="java.lang.String"/>
	<variable name="totalbarang" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{installmentitem}]]></variableExpression>
	</variable>
	<variable name="totalmotor" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{installmentmotor}]]></variableExpression>
	</variable>
	<variable name="number" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{number} == null ? 1 : $V{number} + 1]]></variableExpression>
	</variable>
	<variable name="TotalOrang" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{id}]]></variableExpression>
	</variable>
	<variable name="jumlah" class="java.math.BigDecimal" resetType="Column">
		<variableExpression><![CDATA[$F{paid}.add($F{installmentitem}).add($F{installmentmotor})]]></variableExpression>
	</variable>
	<variable name="jumlah_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$V{jumlah}]]></variableExpression>
	</variable>
	<variable name="totalkonsumsi" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{paid}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="88" splitType="Stretch">
			<staticText>
				<reportElement uuid="cd5e5d59-193e-4079-8057-fc18aa9f8a2c" x="262" y="27" width="289" height="37"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[DAFTAR PEGAWAI DENGAN TRANSAKSI KREDIT PADA TOKO KOPERASI D.J.B.C]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="0bfe3ae5-9385-48e7-b804-d99354ad47cd" x="0" y="68" width="83" height="20"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Tanggal Cetak]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7943554e-94fa-4e5c-8448-475044e18a38" x="83" y="68" width="17" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="74cca753-3509-4006-b8cc-c077a62a035a" x="715" y="0" width="87" height="20"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isItalic="true"/>
				</textElement>
				<text><![CDATA[UntaCoreReport]]></text>
			</staticText>
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="b85414ab-c0bd-48b6-a8bf-6a8ae012075e" x="100" y="68" width="143" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{skrng}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="36" splitType="Stretch">
			<staticText>
				<reportElement uuid="8776016f-e640-4323-be17-caa5cfd7b268" x="0" y="0" width="130" height="20"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[REKAPITULASI KREDIT]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement uuid="6e8e906c-e294-4104-8cd1-909b61a8890f" x="47" y="0" width="106" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[JUMLAH ORANG]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c46fcb5c-b464-4541-afbc-f71b7ce3141f" x="153" y="0" width="155" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[BAGIAN]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c26552ad-59f2-4ef5-a540-6a184d23c721" x="308" y="0" width="126" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[KONSUMSI]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b969eb4e-231c-4a8a-981c-e862f48dc10f" x="564" y="0" width="118" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[ANGSURAN MOTOR]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="a70267ee-add6-4a1b-a84c-4227132b0241" x="434" y="0" width="130" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[ANGSURAN BARANG]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="967c57ca-085b-43cc-a5e8-e1ba41410d80" x="682" y="0" width="120" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[JUMLAH]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d3cee507-0a83-4e48-8d62-bd15475d4f77" x="0" y="0" width="47" height="30"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[No.]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement uuid="f3fba203-e511-4b7d-9cea-d1d5a353b3be" x="0" y="0" width="47" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{number}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0;-#,##0">
				<reportElement uuid="cb269e4b-d8c9-4196-9bdd-b3bce290e6fb" x="682" y="0" width="120" height="20"/>
				<box rightPadding="4">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{jumlah}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="7b587a36-0d17-4ba3-8d4e-239867b60a5a" x="68" y="0" width="85" height="20"/>
				<box leftPadding="8">
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[ORANG]]></text>
			</staticText>
			<textField>
				<reportElement uuid="c26360f0-d13c-4aef-866c-182fafd56d3d" x="47" y="0" width="21" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="5409f22c-3ba3-4f1f-a93d-b575ad3a6bbc" x="308" y="0" width="126" height="20"/>
				<box rightPadding="4">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paid} == 0 ? "-" : $F{paid}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="9609ef76-33ce-4e91-ac62-b322150e751b" x="153" y="0" width="155" height="20"/>
				<box leftPadding="3">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{unitkerja}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="e0cffd39-c283-4ace-90e3-82a96c9c308c" x="434" y="0" width="130" height="20"/>
				<box rightPadding="4">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{installmentitem} == 0 ? "-" : $F{installmentitem}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="3147af55-e5b1-4a69-8d26-5aad81054738" x="564" y="0" width="118" height="20"/>
				<box rightPadding="4">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{installmentmotor} == 0 ? "-" : $F{installmentmotor}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="29" splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="141" splitType="Stretch">
			<textField pattern="dd MMMMM yyyy">
				<reportElement uuid="fa73c213-c9dc-468d-bda6-94f02644cff9" x="663" y="15" width="95" height="20"/>
				<box leftPadding="0"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{skrng}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="e59753c1-87a6-4596-8541-4a132ad2a9ac" x="615" y="35" width="143" height="30"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[A.N. PENGURUS KOPESAT MANAGER KEUANGAN]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="0c72675a-9e9a-4db5-9749-fe3341f33309" x="33" y="45" width="175" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[BENDAHARA GAJI KANTOR PUSAT]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f9b6d17d-35cf-4d95-b1c8-82df10cfde1f" x="33" y="25" width="175" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[MENGETAHUI]]></text>
			</staticText>
			<textField>
				<reportElement uuid="70152afb-e6ad-473c-b5ca-79800e35810b" x="33" y="121" width="175" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" isUnderline="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{bendahara}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="e12483cd-a5b0-4fcd-b458-4c4b120a5949" x="615" y="121" width="143" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" isUnderline="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{manager}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="cdc4416b-29c7-4ed4-9dda-c8c564ef7c53" x="615" y="15" width="48" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[Jakarta,]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band height="24" splitType="Stretch">
			<textField pattern="#,##0">
				<reportElement uuid="8cd94d0f-3be5-488a-bf49-14fd84db72ff" x="434" y="0" width="130" height="24"/>
				<box rightPadding="4">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalbarang}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="d3a464d8-3473-4821-aa75-a730b09bae6f" x="564" y="0" width="118" height="24"/>
				<box rightPadding="4">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalmotor}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="c9acbb13-ff0c-4b02-b007-7f1d496d5ad5" x="168" y="0" width="140" height="24"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<textField>
				<reportElement uuid="72893b31-1b2b-4d99-aea0-c68684e79316" x="47" y="0" width="36" height="24"/>
				<box leftPadding="8">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalOrang}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="7a988c34-4a40-4ab2-a37b-8b2d7ae6733b" x="0" y="0" width="47" height="24"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement uuid="e226e76a-0566-4eda-9beb-1846c3b8a716" x="682" y="0" width="120" height="24"/>
				<box rightPadding="4">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{jumlah_2}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="3a172c6d-ebbe-4e7b-8aaf-6a0641dbb0b9" x="83" y="0" width="85" height="24"/>
				<box leftPadding="0">
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[ORANG]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement uuid="fa9b2ebe-15b8-4a95-ba4f-30e34398fba2" x="308" y="0" width="126" height="24"/>
				<box rightPadding="4">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalkonsumsi}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
