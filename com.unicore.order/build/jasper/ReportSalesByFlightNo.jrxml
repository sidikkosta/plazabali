<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportSalesByFlightNo" language="groovy" pageWidth="360" pageHeight="648" columnWidth="320" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="cf798906-ef98-42f6-900a-11d7c74debdd">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="IsTradingDate" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Location" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="UNS_ProductType" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
COALESCE(FlightNo,'-') AS FlightNo,
bpl.Name AS Location,
bp.Name AS Toko,
SUBSTRING(FlightNo,1,2) AS FlightCode,
SUM(ptl.lineamt-((ptl.lineamt*pt.discount)/100)) AS NetSale,
Count(FlightNo) AS Transaction


FROM UNS_POSTrx pt
INNER JOIN UNS_POSTrxLine ptl ON ptl.UNS_POSTrx_ID=pt.UNS_POSTrx_ID
INNER JOIN M_Product pr ON pr.M_Product_ID=ptl.M_Product_ID
INNER JOIN UNS_POS_Session ps ON ps.UNS_POS_Session_ID=pt.UNS_POS_Session_ID
INNER JOIN C_BPartner_Location bpl ON pt.C_BPartner_Location_ID = bpl.C_BPartner_Location_ID
INNER JOIN C_BPartner bp ON pt.C_BPartner_ID = bp.C_BPartner_ID
LEFT JOIN UNS_CustomerInfo ci ON ci.UNS_CustomerInfo_ID=pt.UNS_CustomerInfo_ID

WHERE
pt.DocStatus IN('CO','CL') AND pt.IsPaid='Y' AND ps.IsTrialMode='N'
AND (CASE WHEN $P{IsTradingDate} = 'Y' THEN (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN pt.DateAcct >= $P{DateFrom}::timestamp AND pt.DateAcct  <= $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NULL THEN pt.DateAcct >= $P{DateFrom}::timestamp WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN pt.DateAcct  <= $P{DateTo}::timestamp ELSE pt.DateAcct  <= now() END)

ELSE

(CASE WHEN $P{DateFrom}::date IS NOT NULL AND $P{DateTo}::date IS NOT NULL THEN pt.DateTrx::date >= $P{DateFrom}::date AND pt.DateTrx::date  <= $P{DateTo}::date WHEN $P{DateFrom}::date IS NOT NULL AND $P{DateTo}::date IS NULL THEN pt.DateTrx::date >= $P{DateFrom}::date WHEN $P{DateFrom}::date IS NULL AND $P{DateTo}::date IS NOT NULL THEN pt.DateTrx::date  <= $P{DateTo}::date ELSE pt.DateTrx::date  <= now() END)END)
AND (CASE WHEN $P{Location} IS NOT NULL THEN bp.value LIKE '%$P!{Location}%'  ELSE 1=1 END)
AND (CASE WHEN $P{UNS_ProductType} = 'I' THEN pr.IsImport='Y' WHEN $P{UNS_ProductType} = 'L' THEN pr.IsImport='N' ELSE 1=1 END)

GROUP BY Toko,FlightNo,bpl.name
ORDER BY FlightNo ASC]]>
	</queryString>
	<field name="flightno" class="java.lang.String"/>
	<field name="location" class="java.lang.String"/>
	<field name="toko" class="java.lang.String"/>
	<field name="flightcode" class="java.lang.String"/>
	<field name="netsale" class="java.math.BigDecimal"/>
	<field name="transaction" class="java.lang.Long"/>
	<variable name="transaction_1" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{transaction}]]></variableExpression>
	</variable>
	<variable name="netsale_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{netsale}]]></variableExpression>
	</variable>
	<group name="FlightNo">
		<groupExpression><![CDATA[$F{flightno}]]></groupExpression>
		<groupFooter>
			<band height="21">
				<textField>
					<reportElement x="0" y="0" width="100" height="21" uuid="31a4bc0f-50e0-47bd-b386-b664fde7bbad"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial Narrow"/>
						<paragraph leftIndent="15"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{flightno}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="200" y="0" width="120" height="21" uuid="50fd6b1d-dea8-4525-a995-5e683b4710b0"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial Narrow"/>
						<paragraph leftIndent="5" rightIndent="20"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{netsale}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0">
					<reportElement x="100" y="0" width="100" height="21" uuid="92e59bf4-97ca-42e3-a2e9-667a1ab73b62"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial Narrow"/>
						<paragraph leftIndent="5" rightIndent="40"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{transaction}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="29" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="14" width="320" height="15" uuid="9ce0b0af-79e0-4891-b68e-3b164a2368f0"/>
				<textElement>
					<font fontName="Arial Narrow" size="13" isBold="false"/>
				</textElement>
				<text><![CDATA[SALES BY FLIGHT NO]]></text>
			</staticText>
			<textField>
				<reportElement x="169" y="0" width="123" height="20" uuid="25c4ea1f-3edd-4c25-be0b-4f0e88e4a445"/>
				<textElement textAlignment="Right">
					<font fontName="Arial Narrow" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " +$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="48" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="15" width="320" height="18" uuid="987e1f1e-bdd0-4e73-8d7c-650c7c6ba5a9">
					<printWhenExpression><![CDATA[$P{IsTradingDate} == 'Y']]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Arial Narrow"/>
				</textElement>
				<text><![CDATA[Base On Trading Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="34" height="15" uuid="3a21a389-2009-4ebd-b3f6-a1f3498dbf69"/>
				<textElement>
					<font fontName="Arial Narrow"/>
				</textElement>
				<text><![CDATA[From]]></text>
			</staticText>
			<textField pattern="dd MMMMM yyyy">
				<reportElement x="21" y="0" width="232" height="15" uuid="7aa14b24-89a4-4821-941b-0f2bf6d0170c"/>
				<textElement textAlignment="Right">
					<font fontName="Arial Narrow"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd MMM yyyy").format($P{DateFrom})+"   To   "+new SimpleDateFormat("dd MMM yyyy").format($P{DateTo}) + "      Based on Actual Date"]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy h.mm a">
				<reportElement x="200" y="33" width="120" height="15" uuid="93ec349a-9671-4887-810f-59e5a7835d80"/>
				<textElement textAlignment="Right">
					<font fontName="Arial Narrow"/>
				</textElement>
				<textFieldExpression><![CDATA["Printed "+new SimpleDateFormat("dd MMM yyyy hh:mm aaa").format(new java.util.Date())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="33" width="62" height="15" uuid="987e1f1e-bdd0-4e73-8d7c-650c7c6ba5a9"/>
				<textElement>
					<font fontName="Arial Narrow"/>
				</textElement>
				<text><![CDATA[Location Match]]></text>
			</staticText>
			<textField>
				<reportElement x="62" y="33" width="138" height="15" uuid="92007e8d-d8f6-4532-a814-1e9e85396eba"/>
				<textElement>
					<font fontName="Arial Narrow"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Location} == "" ? "All Shop" : $P{Location}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="15" width="320" height="18" uuid="7ea716f5-8cff-4ec7-9e09-12a8ffb671f9">
					<printWhenExpression><![CDATA[$P{IsTradingDate} == 'N']]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Arial Narrow"/>
				</textElement>
				<text><![CDATA[Base On Actual Date]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<line>
				<reportElement x="0" y="17" width="320" height="1" uuid="4f552a35-69d3-414b-ad14-63064a928396"/>
				<graphicElement>
					<pen lineWidth="0.5" lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="0" y="19" width="320" height="1" uuid="c2ffcf71-f800-45d3-a3f1-92ca86ffb321"/>
				<graphicElement>
					<pen lineWidth="0.5" lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="0" y="1" width="100" height="18" uuid="08037bc2-11be-4cc2-bc2e-f728a0e8c864"/>
				<box>
					<pen lineStyle="Dashed"/>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<leftPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					<rightPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Flight No.]]></text>
			</staticText>
			<staticText>
				<reportElement x="200" y="1" width="120" height="18" uuid="b984483e-54e5-4add-bf22-a77907079256"/>
				<box>
					<pen lineStyle="Dashed"/>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<leftPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					<rightPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Net Sales]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="1" width="100" height="18" uuid="222107e3-6662-4daf-aca9-03e716de354c"/>
				<box>
					<pen lineStyle="Dashed"/>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<leftPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					<rightPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[#Transaction]]></text>
			</staticText>
		</band>
	</columnHeader>
	<summary>
		<band height="23" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="100" height="20" uuid="479adcf8-a5fc-441b-985f-015261c7542c"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Narrow" size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement x="100" y="0" width="100" height="20" uuid="d4e51a4a-6f59-4aae-8b01-1ce1f96fa4f1"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Narrow"/>
					<paragraph rightIndent="40"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{transaction_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="0" width="120" height="20" uuid="6d9110b8-a876-4224-ba61-6a092552d7b9"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Narrow"/>
					<paragraph rightIndent="20"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{netsale_1}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="19" width="320" height="1" uuid="c2ffcf71-f800-45d3-a3f1-92ca86ffb321"/>
				<graphicElement>
					<pen lineWidth="1.0" lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="0" y="22" width="320" height="1" uuid="c2ffcf71-f800-45d3-a3f1-92ca86ffb321"/>
				<graphicElement>
					<pen lineWidth="1.0" lineStyle="Dashed"/>
				</graphicElement>
			</line>
		</band>
	</summary>
</jasperReport>
