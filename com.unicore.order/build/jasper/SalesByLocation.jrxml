<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="SalesByLocation" language="groovy" pageWidth="504" pageHeight="612" columnWidth="464" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="268a1111-3c9f-4d69-a87c-85978d991ce4">
	<property name="ireport.zoom" value="3.0"/>
	<property name="ireport.x" value="313"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="C_BP_Group_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="IsTradingDate" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
pt.dateacct,
pt.DateTrx,
bp.value as kdshop,
bp.name as shop,
bpg.name as location,
SUM(ptl.DiscountAmt)+pt.DiscountAmt AS TotalDisc,
now(),
pt.GrandTotal AS gtotal

FROM UNS_POSTrx pt
INNER JOIN UNS_POSTrxLine ptl ON ptl.UNS_POSTrx_ID=pt.UNS_POSTrx_ID
INNER JOIN UNS_POS_Session ps ON ps.UNS_POS_Session_ID=pt.UNS_POS_Session_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID=pt.C_BPartner_ID
INNER JOIN C_BP_Group bpg ON bpg.C_BP_Group_ID=bp.C_BP_Group_ID

WHERE pt.DocStatus IN ('CO','CL') and  pt.IsPaid='Y' AND ps.isTrialMode='N' AND

(CASE WHEN $P{C_BP_Group_ID} IS NOT NULL THEN bp.C_BP_Group_ID=$P{C_BP_Group_ID} ELSE 1=1 END) AND

(CASE WHEN $P{IsTradingDate} = 'Y' THEN pt.DateAcct BETWEEN $P{DateFrom}::timestamp AND $P{DateTo}::timestamp
ELSE
CAST(pt.datetrx AS Date) BETWEEN $P{DateFrom}::timestamp AND $P{DateTo}::timestamp END)

GROUP BY bpg.C_BP_Group_ID, bp.C_BPartner_ID, pt.UNS_POSTrx_ID

ORDER BY bp.value]]>
	</queryString>
	<field name="dateacct" class="java.sql.Timestamp"/>
	<field name="datetrx" class="java.sql.Timestamp"/>
	<field name="kdshop" class="java.lang.String"/>
	<field name="shop" class="java.lang.String"/>
	<field name="location" class="java.lang.String"/>
	<field name="totaldisc" class="java.math.BigDecimal"/>
	<field name="now" class="java.sql.Timestamp"/>
	<field name="gtotal" class="java.math.BigDecimal"/>
	<variable name="ShopLineAmt" class="java.math.BigDecimal" resetType="Group" resetGroup="SHOP" calculation="Sum">
		<variableExpression><![CDATA[$F{gtotal}+$F{totaldisc}]]></variableExpression>
	</variable>
	<variable name="ShopDiscAmt" class="java.math.BigDecimal" resetType="Group" resetGroup="SHOP" calculation="Sum">
		<variableExpression><![CDATA[$F{totaldisc}]]></variableExpression>
	</variable>
	<variable name="ShopNetAmt" class="java.math.BigDecimal" resetType="Group" resetGroup="SHOP" calculation="Sum">
		<variableExpression><![CDATA[$F{gtotal}]]></variableExpression>
	</variable>
	<variable name="lineamt_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$F{gtotal}+$F{totaldisc}]]></variableExpression>
	</variable>
	<variable name="diskon_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$F{totaldisc}]]></variableExpression>
	</variable>
	<variable name="net_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$F{gtotal}]]></variableExpression>
	</variable>
	<variable name="SumLineNetAmt" class="java.math.BigDecimal" incrementType="Group" incrementGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$V{lineamt_1}]]></variableExpression>
	</variable>
	<variable name="SumDiskon" class="java.math.BigDecimal" incrementType="Group" incrementGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$V{diskon_1}]]></variableExpression>
	</variable>
	<variable name="SumNet" class="java.math.BigDecimal" incrementType="Group" incrementGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$V{net_1}]]></variableExpression>
	</variable>
	<group name="Location">
		<groupExpression><![CDATA[$F{location}]]></groupExpression>
		<groupHeader>
			<band height="17">
				<staticText>
					<reportElement x="1" y="0" width="153" height="17" uuid="67bd2f78-4df5-4eea-ab05-acccafe0d868"/>
					<box>
						<pen lineStyle="Double"/>
						<topPen lineStyle="Double"/>
						<leftPen lineStyle="Double"/>
						<bottomPen lineWidth="1.0" lineStyle="Double"/>
						<rightPen lineStyle="Double"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Location]]></text>
				</staticText>
				<staticText>
					<reportElement x="359" y="0" width="105" height="17" uuid="180bf8f2-8641-4f86-8fc0-dbd3287571d2"/>
					<box>
						<pen lineStyle="Double"/>
						<topPen lineStyle="Double"/>
						<leftPen lineStyle="Double"/>
						<bottomPen lineWidth="1.0" lineStyle="Double"/>
						<rightPen lineStyle="Double"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<text><![CDATA[Net Sales]]></text>
				</staticText>
				<staticText>
					<reportElement x="154" y="0" width="105" height="17" uuid="9da82ac9-87d4-4a9c-b0c8-e266fb4989d2"/>
					<box>
						<pen lineStyle="Double"/>
						<topPen lineStyle="Double"/>
						<leftPen lineStyle="Double"/>
						<bottomPen lineWidth="1.0" lineStyle="Double"/>
						<rightPen lineStyle="Double"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<text><![CDATA[Gross Sales]]></text>
				</staticText>
				<staticText>
					<reportElement x="259" y="0" width="100" height="17" uuid="3e93e706-574b-425a-b5fb-64ee8d3f810f"/>
					<box>
						<pen lineStyle="Double"/>
						<topPen lineStyle="Double"/>
						<leftPen lineStyle="Double"/>
						<bottomPen lineWidth="1.0" lineStyle="Double"/>
						<rightPen lineStyle="Double"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<text><![CDATA[Discount]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="21">
				<textField pattern="#,##0.00">
					<reportElement x="359" y="0" width="105" height="20" uuid="117d83c7-9d0d-4bf7-898c-5b287be2fcf0"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{net_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="154" y="0" width="105" height="20" uuid="8c26a517-2bd2-433e-b44e-619373b43eeb"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{lineamt_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="1" y="0" width="153" height="20" uuid="ba3afc67-89c0-448a-9bfe-e5db191803e3"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[SUB TOTAL]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="259" y="0" width="100" height="20" uuid="365d8e0f-3c35-4df8-aec0-f7023e6eaef2"/>
					<box>
						<topPen lineWidth="0.5" lineStyle="Solid"/>
						<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{diskon_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="SHOP">
		<groupExpression><![CDATA[$F{shop}]]></groupExpression>
		<groupFooter>
			<band height="17">
				<textField pattern="#,##0.00">
					<reportElement x="154" y="0" width="105" height="17" uuid="90877b14-8893-43d3-b83f-9e5cb039beee"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{ShopLineAmt}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="359" y="0" width="105" height="17" uuid="fe3cb67b-ee3d-4db8-8a11-c8aa0c94c25b"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{ShopNetAmt}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="1" y="0" width="153" height="17" uuid="a9db22ba-db65-42de-b73e-bee11638d5a3"/>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{kdshop}+' - '+$F{shop}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="259" y="0" width="100" height="17" uuid="d269565a-7f09-48c4-ba71-27c7a4de23b3"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{ShopDiscAmt}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="67" splitType="Stretch">
			<staticText>
				<reportElement x="1" y="0" width="258" height="20" uuid="32b4d536-2cc4-4ad8-bcc9-6f89ca338f7c"/>
				<textElement verticalAlignment="Middle">
					<font size="12" isBold="true" isUnderline="true"/>
				</textElement>
				<text><![CDATA[SALES BY LOCATION]]></text>
			</staticText>
			<textField pattern="dd-MMM-yy">
				<reportElement x="1" y="20" width="258" height="15" uuid="7c1e4d50-770e-4ddd-82b8-541c27c88b47"/>
				<textElement>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA["Period : "+new SimpleDateFormat("dd MMM yyyy").format($P{DateFrom})+" To "+new SimpleDateFormat("dd MMM yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1" y="35" width="258" height="15" uuid="0e10fe3a-816f-46fa-a9c8-ffe994c80ee4"/>
				<textFieldExpression><![CDATA[$P{IsTradingDate} == 'Y' ? 'Base On Trading Date' : 'Base On Actual Date']]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy h.mm a">
				<reportElement x="359" y="20" width="105" height="15" uuid="4418891b-c35d-4478-a8af-880be3063875"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd MMM yyyy hh:mm a").format($F{now})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1" y="50" width="258" height="17" uuid="97ca0592-238f-4d82-bf6c-96b0cbb245ee"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{C_BP_Group_ID}== null ? 'All Location' : $F{location}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="359" y="0" width="105" height="20" uuid="3845227e-bd14-494c-86ad-0786329a29c5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<summary>
		<band height="20">
			<textField pattern="#,##0.00">
				<reportElement x="154" y="0" width="105" height="20" uuid="cbb46594-b36e-4b04-aa12-e6c285c2e48f"/>
				<box>
					<bottomPen lineWidth="1.0" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{SumLineNetAmt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="359" y="0" width="105" height="20" uuid="15a448f5-cb81-449d-9e24-529a25ca23c3"/>
				<box>
					<bottomPen lineWidth="1.0" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{SumNet}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="1" y="0" width="153" height="20" uuid="81532ff2-299d-4400-8cb0-588002df0100"/>
				<box>
					<bottomPen lineWidth="1.0" lineStyle="Double"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="259" y="0" width="100" height="20" uuid="ffd87336-0693-4f51-972a-9695b707b841"/>
				<box>
					<bottomPen lineWidth="1.0" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{SumDiskon}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
