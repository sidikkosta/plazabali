<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RekapPenjualan" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="79aee00e-7368-4ece-a9b0-784897c7bf36">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="PaymentMethod" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SalesRep_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
POS.trxno AS TrxNo,
BP.Value AS NIP,
BP.Name AS BP,
pmt.posamount AS tot,
NOW(),
pmt.datedoc AS date,
REF.name AS JenisPembayaran,
AU.realname AS nama

FROM UNS_POSTrx POS

INNER JOIN C_BPartner BP ON BP.C_BPartner_ID=POS.C_BPartner_ID
INNER JOIN UNS_POSPayment pmt ON pmt.UNS_POSTrx_ID=POS.UNS_POSTrx_ID
INNER JOIN AD_Ref_List REF ON REF.value=pmt.PaymentMethod AND AD_Reference_ID=1000203
INNER JOIN AD_User AU ON AU.AD_User_ID=POS.salesrep_id

WHERE POS.docstatus in ('CO','CL') AND
(CASE WHEN $P{SalesRep_ID} IS NOT NULL THEN POS.salesrep_id=$P{SalesRep_ID} ELSE 1=1 END) AND
(CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN POS.datedoc BETWEEN $P{DateFrom}::timestamp AND $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NOT NULL THEN POS.datedoc >= $P{DateFrom}::timestamp WHEN $P{DateTo}::timestamp IS NOT NULL THEN POS.datedoc <= $P{DateTo}::timestamp ELSE 1=1 END) AND
(CASE WHEN $P{PaymentMethod} <>'' THEN pmt.PaymentMethod=$P{PaymentMethod} ELSE 1=1 END)

ORDER BY date]]>
	</queryString>
	<field name="trxno" class="java.lang.String"/>
	<field name="nip" class="java.lang.String"/>
	<field name="bp" class="java.lang.String"/>
	<field name="tot" class="java.math.BigDecimal"/>
	<field name="now" class="java.sql.Timestamp"/>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="jenispembayaran" class="java.lang.String"/>
	<field name="nama" class="java.lang.String"/>
	<variable name="subtotal" class="java.math.BigDecimal" resetType="Group" resetGroup="Date" calculation="Sum">
		<variableExpression><![CDATA[$F{tot}]]></variableExpression>
	</variable>
	<variable name="subtotal_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Date" calculation="Sum">
		<variableExpression><![CDATA[$V{subtotal}]]></variableExpression>
	</variable>
	<variable name="number" class="java.lang.Number" resetType="Group" resetGroup="Date">
		<variableExpression><![CDATA[$V{number} == null ? 1 : $V{number} + 1]]></variableExpression>
	</variable>
	<group name="Date">
		<groupExpression><![CDATA[$F{date}]]></groupExpression>
		<groupHeader>
			<band height="40">
				<staticText>
					<reportElement x="22" y="20" width="99" height="20" uuid="08ff2f04-f74a-4a79-b702-ac4278bf5517"/>
					<box leftPadding="6" rightPadding="0">
						<topPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[No Transaksi]]></text>
				</staticText>
				<staticText>
					<reportElement x="121" y="20" width="127" height="20" uuid="3b57f579-b83c-44e5-8fce-a35fa14cd80b"/>
					<box>
						<topPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[NIP]]></text>
				</staticText>
				<staticText>
					<reportElement x="461" y="20" width="94" height="20" uuid="bd8c2355-91a2-4027-b72f-590b6c41b46d"/>
					<box rightPadding="3">
						<topPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<text><![CDATA[Total Belanja]]></text>
				</staticText>
				<staticText>
					<reportElement x="248" y="20" width="149" height="20" uuid="eb3dd02d-f50e-4c36-84f0-ea2a8c9a484f"/>
					<box>
						<topPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Nama]]></text>
				</staticText>
				<staticText>
					<reportElement x="11" y="0" width="55" height="20" uuid="479bf5a0-bcd5-4fee-8083-fe9e08995758"/>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Tanggal]]></text>
				</staticText>
				<textField pattern="dd/MM/yyyy">
					<reportElement x="76" y="0" width="100" height="20" uuid="48bdc20d-76fe-4541-b758-271e255e1140"/>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{date}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="66" y="0" width="10" height="20" uuid="eb3dd02d-f50e-4c36-84f0-ea2a8c9a484f"/>
					<box leftPadding="4">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="397" y="20" width="64" height="20" uuid="e46cd128-dd4b-4f39-a044-8d2369ca1566"/>
					<box>
						<topPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[Pembayaran]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="20" width="22" height="20" uuid="6d71bc03-6917-477b-9520-7e17ae6c9e44"/>
					<box>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<text><![CDATA[No.]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<textField pattern="#,##0">
					<reportElement x="488" y="0" width="67" height="20" uuid="85cb67c8-6158-4522-a5b8-e1855345b321"/>
					<box rightPadding="3">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="1" y="0" width="460" height="20" uuid="eb3dd02d-f50e-4c36-84f0-ea2a8c9a484f"/>
					<box>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Total]]></text>
				</staticText>
				<staticText>
					<reportElement x="461" y="0" width="27" height="20" uuid="eb3dd02d-f50e-4c36-84f0-ea2a8c9a484f"/>
					<box>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<text><![CDATA[Rp.]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="56" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="16" width="555" height="20" uuid="069de753-6bb1-4aa9-819e-f451fee71a6d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true" isUnderline="true"/>
				</textElement>
				<text><![CDATA[REKAP PENJUALAN TOKO]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="36" width="555" height="20" uuid="a13ab3c6-7f30-411f-bec1-51d4c911ddae"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Nama Kasir : "+$F{nama}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="20" splitType="Stretch"/>
	</pageHeader>
	<detail>
		<band height="13" splitType="Stretch">
			<textField>
				<reportElement x="22" y="0" width="99" height="13" uuid="bc8ad5a8-52ca-4459-8bfa-2567f7ccaeed"/>
				<box leftPadding="6"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trxno}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement x="488" y="0" width="67" height="13" uuid="cf2bfb3d-5bfa-4738-a943-b48faceeafe8"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tot}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="461" y="0" width="27" height="13" uuid="eb3dd02d-f50e-4c36-84f0-ea2a8c9a484f"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<text><![CDATA[Rp.]]></text>
			</staticText>
			<textField>
				<reportElement x="397" y="0" width="64" height="13" uuid="15776be8-e0c8-4639-b5a8-f2ab3d3727a1"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{jenispembayaran}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1" y="0" width="21" height="13" uuid="65ecdd9b-0007-4036-8554-4a83a0356f33"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="121" y="0" width="127" height="13" uuid="d716eb65-624c-4f42-a029-9e859677f31c"/>
				<box leftPadding="3"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nip}.equals( "#NA" ) ? " " : $F{nip}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="248" y="0" width="149" height="13" uuid="17aa0026-6fa0-41da-88fb-e1a5ba37cf9c"/>
				<box leftPadding="3"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{bp}.equals( "Non Member" ) ? " " : $F{bp}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="49" splitType="Stretch">
			<textField>
				<reportElement x="435" y="29" width="80" height="20" uuid="fa55fb13-0604-410b-9f95-5829f850c118"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="515" y="29" width="40" height="20" uuid="06cd02d0-4e28-49f4-af6d-ff0ef56ee8ae"/>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="42" splitType="Stretch"/>
	</summary>
</jasperReport>
