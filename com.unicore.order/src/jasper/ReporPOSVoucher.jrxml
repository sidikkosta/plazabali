<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReporPOSVoucher" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="ef91799c-4093-4081-8368-eb312af8b8f6">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SalesRep_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	cb.name AS Organization,
	acl.name AS Client,
	ptx.trxno AS NoPembayaran,
	py.accountno AS NoVoucher,
	cb.name AS Customer,
	cb.value AS NIP,
	py.payamt AS totalvoucher,
	py.paidamt AS bayar,
	REF.name AS JenisPembayaran,
	au.realname AS user,
	py.datedoc AS tglpy,
	now() AS Skrng

FROM UNS_POSPayment py

INNER JOIN AD_Client acl ON acl.AD_Client_ID=py.AD_Client_ID
INNER JOIN C_BPartner cb ON cb.C_BPartner_ID=py.C_BPartner_ID
INNER JOIN UNS_POSTrx ptx ON ptx.UNS_POSTrx_ID=py.UNS_POSTrx_ID
INNER JOIN AD_Ref_List REF ON REF.value=py.PaymentMethod AND AD_Reference_ID=1000203
INNER JOIN AD_User AU ON AU.AD_User_ID=py.salesrep_id

WHERE
	paymentmethod in('4','5','7') AND
	py.docstatus in('CO','CL') AND
	(CASE WHEN $P{SalesRep_ID} IS NOT NULL THEN py.salesrep_id=$P{SalesRep_ID} ELSE 1=1 END) AND
	(CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN py.datedoc BETWEEN
	$P{DateFrom} AND $P{DateTo}::timestamp WHEN
	$P{DateFrom}::timestamp IS NOT NULL THEN py.datedoc >= $P{DateFrom}::timestamp WHEN
	$P{DateTo}::timestamp IS NOT NULL THEN py.datedoc <= $P{DateTo}::timestamp ELSE 1=1 END)]]>
	</queryString>
	<field name="organization" class="java.lang.String"/>
	<field name="client" class="java.lang.String"/>
	<field name="nopembayaran" class="java.lang.String"/>
	<field name="novoucher" class="java.lang.String"/>
	<field name="customer" class="java.lang.String"/>
	<field name="nip" class="java.lang.String"/>
	<field name="totalvoucher" class="java.math.BigDecimal"/>
	<field name="bayar" class="java.math.BigDecimal"/>
	<field name="jenispembayaran" class="java.lang.String"/>
	<field name="user" class="java.lang.String"/>
	<field name="tglpy" class="java.sql.Timestamp"/>
	<field name="skrng" class="java.sql.Timestamp"/>
	<variable name="number" class="java.lang.Number">
		<variableExpression><![CDATA[$V{number} == null ? 1 : $V{number} + 1]]></variableExpression>
	</variable>
	<variable name="sisavoucher" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{totalvoucher}.subtract($F{bayar})]]></variableExpression>
	</variable>
	<variable name="totalvoucher" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{totalvoucher}]]></variableExpression>
	</variable>
	<variable name="totalbayar" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{bayar}]]></variableExpression>
	</variable>
	<variable name="totalsisavoucher" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$V{sisavoucher}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="83" splitType="Stretch">
			<textField>
				<reportElement uuid="db542d26-346e-41b5-a034-6eb15d09c68c" x="0" y="20" width="188" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{client}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="808b0300-1044-43be-ab34-07a28167c006" x="339" y="33" width="73" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Nama kasir]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="97750173-824d-4f76-a48f-4ba538cc965c" x="412" y="33" width="10" height="15"/>
				<box leftPadding="0">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="10c68041-7ccd-422c-aefe-b23f34f3b33e" x="412" y="48" width="10" height="15"/>
				<box leftPadding="0">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7efb9e52-8292-4ebf-8b97-175bec736b14" x="0" y="40" width="188" height="23"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="8"/>
				</textElement>
				<text><![CDATA["JL. A. YANI TELP. 021-4890308 EXT 184/884 021-4712868"]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7916721c-c2f5-418a-a7af-79efbd9a7fa0" x="0" y="0" width="555" height="20"/>
				<box rightPadding="8"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[LAPORAN TRANSAKSI VOUCHER]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="2595c422-6267-423f-81ee-da0270580cef" x="339" y="48" width="73" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Tanggal Transaksi]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement uuid="c3593021-fc2e-4f6e-be2e-984cd31628c7" x="422" y="33" width="133" height="15"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{SalesRep_ID} == null ? "Semua Kasir" : $F{user}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="2d991a20-6a26-40d3-8039-e75fb0e21ae1" x="0" y="81" width="555" height="1"/>
				<graphicElement>
					<pen lineStyle="Double"/>
				</graphicElement>
			</line>
			<textField>
				<reportElement uuid="96889aae-90e1-450e-9ec6-5ba445394cd0" x="422" y="48" width="133" height="14"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[""+new SimpleDateFormat("dd/MM/yyyy").format($P{DateFrom})+" - "+new SimpleDateFormat("dd/MM/yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="d385af45-48cf-4114-88a9-75d6123c5d9f" x="112" y="62" width="133" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{skrng}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="b6cb1a34-9c1e-4c43-a5b9-999c84c7d8fb" x="0" y="62" width="100" height="15"/>
				<box leftPadding="3">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Tanggal Cetak]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c2fb1cb1-ecd4-4b2b-a7a8-3206cb725943" x="100" y="62" width="12" height="15"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band height="9" splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="22" splitType="Stretch">
			<staticText>
				<reportElement uuid="d9fe7802-577b-42f4-a7f1-1b7cc409c9b2" x="24" y="0" width="58" height="22"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[No.Transaksi]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d44f97fc-eb61-454a-84cd-b9cc603121e4" x="140" y="0" width="94" height="22"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[NIP]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="4febdeea-9af7-49e9-bdc2-4d52f16203e1" x="234" y="0" width="115" height="22"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Customer]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="780373e0-d27e-4461-8e41-d5200a074c1e" x="349" y="0" width="63" height="22"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Pembayaran]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b5ede4cc-b56b-4e14-8129-366c6a12dce7" x="412" y="0" width="55" height="22"/>
				<box leftPadding="0" rightPadding="0">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Total Voucher]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="18482aa5-6ced-45de-8c81-fc20676969f3" x="0" y="0" width="24" height="22"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[No.]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="3f174fc6-0d66-45c0-ae8f-23eb79bece7a" x="82" y="0" width="58" height="22"/>
				<box>
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[No.Voucher]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b5ede4cc-b56b-4e14-8129-366c6a12dce7" x="467" y="0" width="45" height="22"/>
				<box leftPadding="0" rightPadding="0">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Bayar]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b5ede4cc-b56b-4e14-8129-366c6a12dce7" x="512" y="0" width="43" height="22"/>
				<box leftPadding="0" rightPadding="0">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Sisa Voucher]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement uuid="ec597c72-f490-46dc-8952-3924773cdabc" x="24" y="0" width="58" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nopembayaran}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="28bc77f9-c56b-4a48-8b02-9d366d444080" x="140" y="0" width="94" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nip}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="372f8ef2-b75f-4b7c-a396-db9a3001123a" x="234" y="0" width="115" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="144266c8-6610-42fa-8bad-6ab71d9e4763" x="349" y="0" width="63" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{jenispembayaran}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="f87a624b-6f3b-45aa-8291-7a2b49cea142" x="412" y="0" width="55" height="20"/>
				<box leftPadding="0" rightPadding="5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{totalvoucher}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="a92c0dea-6bd3-4c73-96b2-1ab39db484cc" x="0" y="0" width="24" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="929d4bfd-340b-4fd5-a027-40d171ffc63b" x="82" y="0" width="58" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{novoucher}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="f87a624b-6f3b-45aa-8291-7a2b49cea142" x="467" y="0" width="45" height="20"/>
				<box leftPadding="0" rightPadding="5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{bayar}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="33f4e2cf-155e-4f1c-a33a-6658536c7766" x="512" y="0" width="43" height="20"/>
				<box rightPadding="5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sisavoucher} == 0 ? "-" : $V{sisavoucher}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="20" splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement uuid="24ffbc29-d00c-4628-8c23-9eaa8cb0126f" x="435" y="0" width="80" height="20"/>
				<textElement textAlignment="Right">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA["Halaman "+$V{PAGE_NUMBER}+" Sampai"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="8ac65631-45e0-4a09-b2e8-9ce7c60b02e9" x="515" y="0" width="40" height="20"/>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="77" splitType="Stretch">
			<line>
				<reportElement uuid="76aa0d34-1246-44f1-aeca-5bb6805fe1ba" x="0" y="0" width="555" height="1"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="27a185cd-dd4a-4fe4-a383-573a272c8a0a" x="373" y="17" width="97" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total Voucher]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="27a185cd-dd4a-4fe4-a383-573a272c8a0a" x="373" y="37" width="97" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total bayar]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="27a185cd-dd4a-4fe4-a383-573a272c8a0a" x="373" y="57" width="97" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total Sisa Voucher]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement uuid="e2081491-64c5-4959-9800-712e964e258f" x="493" y="57" width="62" height="20"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalsisavoucher}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="b91f66af-5ecf-434d-9cc0-e27ccf633e16" x="493" y="17" width="62" height="20"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalvoucher}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="3ac1ddd2-5739-4509-924d-6942bf97d5b4" x="493" y="37" width="62" height="20"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalbayar}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="27a185cd-dd4a-4fe4-a383-573a272c8a0a" x="470" y="17" width="23" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rp.]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="27a185cd-dd4a-4fe4-a383-573a272c8a0a" x="470" y="37" width="23" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rp.]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="27a185cd-dd4a-4fe4-a383-573a272c8a0a" x="470" y="57" width="23" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rp.]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
