<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DaftarTransaksiPelanggan" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="3fc53972-d9e6-49c4-a131-b698d3b9c2eb">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="C_BPartner_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SalesRep_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="AD_Org_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="isPaid" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
     cus.name AS namatoko,
     pdk.name AS nmproduk,
     sm.documentno AS noship,
     invoiceopen(fk.c_invoice_id, 0),
     fk.grandtotal as gtot,
     fk.documentno AS nofaktur,
     fk.dateinvoiced AS tglfaktur,
     fkl.pricelist AS harga,
     fkl.discountamt AS disc,
     fkl.linenetamt AS jumlah,
     sm.movementdate AS tglkirim,
     fkl.qtyinvoiced AS qty,
     sat.name AS satuan,
     now() AS tanggalskrg,
     au.name AS sales,
     sm.POReference AS SJ,
     po.attname AS namalokasi,
     po.attaddress AS tujunlokasi,
     fk.ReferenceNo AS FK,
     fk.ispaid AS paid

FROM
     c_invoice fk
     INNER JOIN c_invoiceline fkl ON fkl.c_invoice_id=fk.c_invoice_id
     INNER JOIN m_product pdk ON pdk.m_product_id=fkl.m_product_id
     INNER JOIN c_uom sat ON sat.c_uom_id=fkl.uomconversionl1_id
     LEFT JOIN m_inout sm ON sm.c_invoice_id=fk.c_invoice_id
     INNER JOIN c_bpartner cus ON cus.c_bpartner_id=fk.c_bpartner_id
     INNER JOIN ad_user au ON au.ad_user_id=fk.salesrep_id
     LEFT JOIN c_order o ON o.c_order_id=fk.c_order_id
     LEFT JOIN uns_preorder po ON po.c_order_id=o.c_order_id

WHERE
     fk.isSOTrx = 'Y' AND
     fk.docstatus IN ('CO', 'CL') AND
     (CASE WHEN $P{AD_Org_ID} IS NOT NULL THEN fk.AD_Org_ID = $P{AD_Org_ID} ELSE 1=1 END) AND
     (CASE WHEN $P{SalesRep_ID} IS NOT NULL THEN au.AD_User_ID = $P{SalesRep_ID} ELSE 1=1 END) AND
     (CASE WHEN $P{C_BPartner_ID} IS NOT NULL THEN cus.C_BPartner_ID = $P{C_BPartner_ID} ELSE 1=1 END) AND
     (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN fk.dateinvoiced >= $P{DateFrom}::timestamp AND fk.dateinvoiced <= $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NOT NULL and $P{DateTo}::timestamp IS NULL THEN fk.dateinvoiced >= $P{DateFrom}::timestamp WHEN   $P{DateFrom}::timestamp IS NULL and $P{DateTo}::timestamp IS NOT NULL THEN fk.dateinvoiced <= $P{DateTo}::timestamp ELSE 1=1 END)

ORDER BY sales, namatoko, nofaktur, pdk.versionno]]>
	</queryString>
	<field name="namatoko" class="java.lang.String"/>
	<field name="nmproduk" class="java.lang.String"/>
	<field name="noship" class="java.lang.String"/>
	<field name="invoiceopen" class="java.math.BigDecimal"/>
	<field name="gtot" class="java.math.BigDecimal"/>
	<field name="nofaktur" class="java.lang.String"/>
	<field name="tglfaktur" class="java.sql.Timestamp"/>
	<field name="harga" class="java.math.BigDecimal"/>
	<field name="disc" class="java.math.BigDecimal"/>
	<field name="jumlah" class="java.math.BigDecimal"/>
	<field name="tglkirim" class="java.sql.Timestamp"/>
	<field name="qty" class="java.math.BigDecimal"/>
	<field name="satuan" class="java.lang.String"/>
	<field name="tanggalskrg" class="java.sql.Timestamp"/>
	<field name="sales" class="java.lang.String"/>
	<field name="sj" class="java.lang.String"/>
	<field name="namalokasi" class="java.lang.String"/>
	<field name="tujunlokasi" class="java.lang.String"/>
	<field name="fk" class="java.lang.String"/>
	<field name="paid" class="java.lang.String"/>
	<variable name="invoiceopen_1" class="java.math.BigDecimal" incrementType="Group" incrementGroup="Faktur" calculation="Sum">
		<variableExpression><![CDATA[$F{invoiceopen}]]></variableExpression>
	</variable>
	<variable name="gtot_1" class="java.math.BigDecimal" incrementType="Group" incrementGroup="Faktur" calculation="Sum">
		<variableExpression><![CDATA[$F{gtot}]]></variableExpression>
	</variable>
	<group name="Sales" minHeightToStartNewPage="2">
		<groupExpression><![CDATA[$F{sales}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement x="5" y="1" width="123" height="18" uuid="199651e8-6e21-4f3b-b10e-ca1a395ae465"/>
					<textElement>
						<font size="11" isBold="true" isUnderline="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{sales}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="12">
				<line>
					<reportElement x="1" y="10" width="555" height="1" uuid="04a28ab3-c703-402d-8a94-6d9044a98775"/>
					<graphicElement>
						<pen lineStyle="Dashed"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="Faktur">
		<groupExpression><![CDATA[$F{nofaktur}]]></groupExpression>
		<groupHeader>
			<band height="126">
				<staticText>
					<reportElement x="87" y="87" width="5" height="15" uuid="21bee47a-dba1-4801-848c-0e85bcac7d69"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<textField>
					<reportElement isPrintRepeatedValues="false" x="1" y="3" width="555" height="20" isRemoveLineWhenBlank="true" uuid="cc6dfb94-ecfa-421f-ac83-2cf648d173c9"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{namatoko}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="16" y="87" width="72" height="15" uuid="79aa44f2-61c1-44d4-a260-d9bfa7982916"/>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Jumlah Faktur]]></text>
				</staticText>
				<textField pattern="dd MMMMM yyyy" isBlankWhenNull="true">
					<reportElement x="413" y="73" width="100" height="15" uuid="323ede4e-e18e-4381-89a6-078b5bdd383c"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{tglkirim}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="16" y="60" width="72" height="15" uuid="a0c246b3-0d4c-449e-851e-f6321d5be0b4"/>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Tanggal Faktur]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="413" y="59" width="100" height="15" uuid="c4e21586-ec49-454f-b257-7c487e76ea4b"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{sj}==null ? $F{noship} :$F{sj}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="409" y="59" width="5" height="15" uuid="e33a1cf7-3710-40c5-b219-868b9dfd5a38"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="343" y="87" width="67" height="15" uuid="52b90346-075e-4d61-8ab3-56ca4e288331">
						<printWhenExpression><![CDATA[$F{paid}=='N']]></printWhenExpression>
					</reportElement>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Sisa Hutang]]></text>
				</staticText>
				<textField pattern="#,##0.-">
					<reportElement x="91" y="87" width="100" height="15" uuid="5e2820a0-677d-4465-90da-a8ed6ec70970"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{gtot}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="343" y="59" width="67" height="15" uuid="e840a9ae-8476-4667-a061-6c73a938435a"/>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[No SJ]]></text>
				</staticText>
				<staticText>
					<reportElement x="87" y="60" width="5" height="15" uuid="e735f00f-b46f-40a4-81e7-544ec557a5f8"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="87" y="46" width="5" height="15" uuid="013a9fdd-6255-4327-af81-42e4716b9c65"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement x="343" y="73" width="67" height="15" uuid="962ad479-96b0-4284-b194-26edf3bd7680"/>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Tanggal Kirim]]></text>
				</staticText>
				<staticText>
					<reportElement x="409" y="87" width="5" height="15" uuid="bdc62af3-080f-4189-85c0-0566c293a3cd">
						<printWhenExpression><![CDATA[$F{paid}=='N']]></printWhenExpression>
					</reportElement>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<textField pattern="#,##0.-">
					<reportElement x="413" y="87" width="100" height="15" uuid="8ba8a450-b736-40b9-af52-d2e9e13be744">
						<printWhenExpression><![CDATA[$F{paid}=='N']]></printWhenExpression>
					</reportElement>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{invoiceopen}]]></textFieldExpression>
				</textField>
				<textField pattern="dd MMMMM yyyy">
					<reportElement x="91" y="60" width="100" height="15" uuid="3a5f0078-d91b-4272-94b6-72eff9d6bb96"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{tglfaktur}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="16" y="46" width="72" height="15" uuid="e672be3f-2725-4e16-b8ae-164c47d32c67"/>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[No. Faktur]]></text>
				</staticText>
				<staticText>
					<reportElement x="409" y="73" width="5" height="15" uuid="51102aa8-13d9-4db3-b2eb-56a22701cdf4"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
				<textField>
					<reportElement x="91" y="46" width="100" height="15" uuid="2f5882b4-51a5-444c-b37b-eb96aee925d5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{fk}==null ? $F{nofaktur}:$F{fk}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement x="-1" y="105" width="555" height="20" uuid="e428380a-2f4a-4b2f-9326-423da598697f"/>
				</rectangle>
				<staticText>
					<reportElement x="4" y="108" width="243" height="15" uuid="40947714-099e-4c39-a4f8-7e59a07ec1a6"/>
					<box leftPadding="5" rightPadding="5">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Nama Barang]]></text>
				</staticText>
				<staticText>
					<reportElement x="249" y="108" width="61" height="15" uuid="bd0cf31b-b9cd-4048-a21e-b523aa213243"/>
					<box leftPadding="5" rightPadding="5">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Jml Barang]]></text>
				</staticText>
				<staticText>
					<reportElement x="478" y="108" width="75" height="15" uuid="0f5c714e-a763-4bad-9e44-cca47a24a669"/>
					<box leftPadding="5" rightPadding="5">
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Sub Total]]></text>
				</staticText>
				<staticText>
					<reportElement x="310" y="108" width="74" height="15" uuid="d8aba62f-8678-47b1-84ac-715e6c8f1f81"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Harga @]]></text>
				</staticText>
				<staticText>
					<reportElement x="407" y="108" width="64" height="15" uuid="41dcbb6b-7904-4fc1-899d-c182577896eb"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Disc]]></text>
				</staticText>
				<textField>
					<reportElement x="1" y="23" width="555" height="20" isRemoveLineWhenBlank="true" uuid="aaa64133-768f-4a68-aea4-798a7ca25cf8">
						<printWhenExpression><![CDATA[$F{namalokasi}!=null]]></printWhenExpression>
					</reportElement>
					<textFieldExpression><![CDATA["Lokasi :"+$F{namalokasi}+" #Alamat: " +$F{tujunlokasi}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement x="203" y="52" width="128" height="28" forecolor="#FF0000" uuid="a59e0009-bc32-4d71-89a9-19a649ad847e">
						<printWhenExpression><![CDATA[$F{paid}=='Y']]></printWhenExpression>
					</reportElement>
				</rectangle>
				<staticText>
					<reportElement x="203" y="53" width="128" height="27" forecolor="#FF0000" uuid="3b47eb30-9f66-48f0-bbd0-1937e9ef6eba">
						<printWhenExpression><![CDATA[$F{paid}=='Y']]></printWhenExpression>
					</reportElement>
					<box>
						<pen lineColor="#FF0033"/>
						<topPen lineColor="#FF0033"/>
						<leftPen lineColor="#FF0033"/>
						<bottomPen lineColor="#FF0033"/>
						<rightPen lineColor="#FF0033"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="18" isBold="true"/>
					</textElement>
					<text><![CDATA[L  U  N  A  S]]></text>
				</staticText>
				<staticText>
					<reportElement x="16" y="75" width="71" height="13" uuid="18b8d595-9a04-4919-b834-b8fbbec9d4fa"/>
					<box leftPadding="5" rightPadding="5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[Umur Faktur]]></text>
				</staticText>
				<textField pattern="dd MMMMM yyyy">
					<reportElement x="91" y="75" width="100" height="13" uuid="4c2065cd-88aa-44f1-ab6c-ba60c15a80a3"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{tanggalskrg}-$F{tglfaktur} +" Hari"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="87" y="75" width="5" height="13" uuid="8ec948ac-ea6a-4114-ae7e-f4f19446490a"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Tahoma" size="8"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="4">
				<line>
					<reportElement x="1" y="0" width="553" height="1" uuid="f22d60f6-4039-450e-957d-702776b5d1d6"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="36" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="1" width="550" height="15" uuid="76e315f1-58dc-43c6-8aa0-bc0f459d4426"/>
				<textElement textAlignment="Center">
					<font fontName="Tahoma" size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[DAFTAR RINCIAN TRANSAKSI PELANGGAN]]></text>
			</staticText>
			<textField pattern="EEEEE, dd-MMM-yyyy HH:mm:ss">
				<reportElement x="420" y="1" width="130" height="15" uuid="b6ab5df9-ecfd-49b2-bcdb-0461a01271de"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("EEEEE, dd-MM-yyyy HH:mm:ss").format($F{tanggalskrg})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1" y="16" width="553" height="20" uuid="2758c78b-14cc-4d92-87ff-80898b7ed8e0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none">
					<font isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Periode : " + new SimpleDateFormat("dd-MM-yyyy").format($P{DateFrom}) + " s/d " + new SimpleDateFormat("dd-MM-yyyy").format($P{DateTo})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<detail>
		<band height="16" splitType="Stretch">
			<textField>
				<reportElement x="5" y="1" width="243" height="15" uuid="4c3b30b0-2355-4e05-b380-a46654b579ce"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nmproduk}]]></textFieldExpression>
			</textField>
			<textField pattern="###0">
				<reportElement x="248" y="1" width="22" height="15" uuid="19296a52-a81b-4cc1-9774-cb5caacc37d1"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="273" y="1" width="38" height="15" uuid="0575f7bb-2adf-448f-b8d9-f1a9010da85c"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{satuan}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.-">
				<reportElement x="311" y="1" width="74" height="15" uuid="3222238d-d673-4c7a-8b27-c2be2040fd14"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{harga}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.-">
				<reportElement x="465" y="1" width="84" height="15" uuid="5466eec9-9c5d-4647-b94d-14f483872a58"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{jumlah}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.-">
				<reportElement x="392" y="1" width="70" height="15" uuid="3f919c6d-8822-4e6d-93d0-2d95e5847e4b"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Tahoma" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{disc}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="61" splitType="Stretch">
			<textField pattern="#,##0.-">
				<reportElement x="450" y="4" width="100" height="15" uuid="2621d69a-75ee-4447-b596-5e013a7f69d3"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$V{gtot_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.-">
				<reportElement x="449" y="32" width="100" height="15" uuid="c92a91ba-5158-4d2e-9d05-237cecaca83d"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Tahoma" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{invoiceopen_1}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="284" y="32" width="101" height="15" uuid="e14c3f2a-67e8-4af6-8053-86a67b66aa02"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Tahoma" size="10"/>
				</textElement>
				<text><![CDATA[Total Sisa Hutang]]></text>
			</staticText>
			<staticText>
				<reportElement x="284" y="4" width="101" height="15" uuid="c20a6952-fe9a-4299-989b-3b219d32176d"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Tahoma" size="10"/>
				</textElement>
				<text><![CDATA[Total Invoice]]></text>
			</staticText>
			<staticText>
				<reportElement x="284" y="18" width="101" height="15" uuid="f9e81240-800e-4a55-ba93-002a6cdc4a61"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Tahoma" size="10"/>
				</textElement>
				<text><![CDATA[Total Penerimaan]]></text>
			</staticText>
			<textField pattern="#,##0.-">
				<reportElement x="449" y="18" width="100" height="15" uuid="d61c99c7-4e0b-4352-a672-bdd3d2976ca0"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$V{gtot_1}-$V{invoiceopen_1}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
