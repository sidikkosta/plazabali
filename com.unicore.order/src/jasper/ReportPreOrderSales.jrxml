<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportPreOrderSales" language="groovy" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="aa7882cc-9191-4743-a5be-fff5cae08c51">
	<property name="ireport.zoom" value="1.2100000000000002"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="AD_Org_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DocList" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SalesRep_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="isProcessed" class="java.lang.String"/>
	<parameter name="AD_User_ID" class="java.lang.Integer"/>
	<queryString>
		<![CDATA[SELECT
bp.Name AS Customer,
bpl.Name AS Address,
po.DocumentNo AS NoPO,
po.DateDoc AS DatePO,
rlpo.Name AS StatusPO,
co.DocumentNo AS NoOrder,
co.DateOrdered AS DateORder,
rlco.Name AS StatusOrder,
ci.DocumentNo AS NoInv,
ci.DateInvoiced AS DateInv,
rlci.Name AS StatusInv,
CONCAT(COALESCE(s.realname , s.name), ' (', ao.Name, ')') AS Sales,
spo.Name AS SalesPO,
sco.Name AS SalesSO,
sin.Name AS SalesInv,
co.grandtotal as amountorder,
ci.grandtotal as amountinv

FROM UNS_PreOrder po

INNER JOIN AD_Org ao ON ao.AD_Org_ID = po.AD_Org_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = po.C_BPartner_ID
INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_ID = bp.C_BPartner_ID
LEFT JOIN C_Order co ON co.C_Order_ID = po.C_Order_ID
LEFT JOIN C_Invoice ci ON ci.C_Order_ID = co.C_Order_ID
LEFT JOIN AD_User s ON s.AD_User_ID=(CASE WHEN $P{SalesRep_ID} IS NOT NULL THEN $P{SalesRep_ID} WHEN $P{DocList}='IN' THEN ci.salesrep_id WHEN $P{DocList}='CO' THEN co.salesrep_id ELSE po.createdby END)
LEFT JOIN AD_User us ON us.AD_User_ID = $P{AD_User_ID}
LEFT JOIN AD_User_OrgAccess uo ON uo.AD_User_ID = us.AD_User_ID
LEFT JOIN AD_Reference rf ON rf.Name = '_Document Status'
LEFT JOIN AD_Ref_List rlpo ON rlpo.Value = po.DocStatus AND rlpo.AD_Reference_ID = rf.AD_Reference_ID
LEFT JOIN AD_Ref_List rlco ON rlco.Value = co.DocStatus AND rlco.AD_Reference_ID = rf.AD_Reference_ID
LEFT JOIN AD_Ref_List rlci ON rlci.Value = ci.DocStatus AND rlci.AD_Reference_ID = rf.AD_Reference_ID

LEFT JOIN AD_User spo ON spo.AD_User_ID = po.CreatedBy
LEFT JOIN AD_User sco ON sco.AD_User_ID = co.SalesRep_ID
LEFT JOIN AD_User sin ON sin.AD_User_ID = ci.SalesRep_ID

WHERE (CASE WHEN $P{AD_Org_ID} IS NOT NULL THEN ao.AD_Org_ID = $P{AD_Org_ID} WHEN us.UserAgent = 'N' THEN ao.AD_Org_ID = us.AD_Org_ID ELSE 1=1 END) AND CASE WHEN $P{SalesRep_ID} IS NOT NULL THEN (CASE WHEN $P{DocList}='IN' THEN ci.salesrep_id=$P{SalesRep_ID} WHEN $P{DocList} ='PO' THEN po.createdby=$P{SalesRep_ID} WHEN $P{DocList}='CO' THEN co.salesrep_id=$P{SalesRep_ID} ELSE 1=1 END) ELSE 1=1 END AND

(CASE WHEN $P{DocList} = 'IN' THEN (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN ci.DateInvoiced >= $P{DateFrom}::timestamp AND ci.DateInvoiced <= $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN ci.DateInvoiced <= $P{DateTo}::timestamp ELSE ci.DateInvoiced >= $P{DateFrom}::timestamp END)
WHEN $P{DocList} = 'CO' THEN (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN co.DateOrdered >= $P{DateFrom}::timestamp AND co.DateOrdered <= $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN co.DateOrdered <= $P{DateTo}::timestamp ELSE co.DateOrdered >= $P{DateFrom}::timestamp END)
ELSE (CASE WHEN $P{DateFrom}::timestamp IS NOT NULL AND $P{DateTo}::timestamp IS NOT NULL THEN po.DateDoc >= $P{DateFrom}::timestamp AND po.DateDoc <= $P{DateTo}::timestamp WHEN $P{DateFrom}::timestamp IS NULL AND $P{DateTo}::timestamp IS NOT NULL THEN po.DateDoc <= $P{DateTo}::timestamp ELSE po.DateDoc >= $P{DateFrom}::timestamp END) END) AND CASE WHEN $P{isProcessed}='Y' THEN co.Processed='Y' AND ci.Processed='Y' AND po.Processed='Y' ELSE 1=1 END AND ci.DocStatus NOT IN ('RE', 'VO')

ORDER BY Sales, Customer]]>
	</queryString>
	<field name="customer" class="java.lang.String"/>
	<field name="address" class="java.lang.String"/>
	<field name="nopo" class="java.lang.String"/>
	<field name="datepo" class="java.sql.Timestamp"/>
	<field name="statuspo" class="java.lang.String"/>
	<field name="noorder" class="java.lang.String"/>
	<field name="dateorder" class="java.sql.Timestamp"/>
	<field name="statusorder" class="java.lang.String"/>
	<field name="noinv" class="java.lang.String"/>
	<field name="dateinv" class="java.sql.Timestamp"/>
	<field name="statusinv" class="java.lang.String"/>
	<field name="sales" class="java.lang.String"/>
	<field name="salespo" class="java.lang.String"/>
	<field name="salesso" class="java.lang.String"/>
	<field name="salesinv" class="java.lang.String"/>
	<field name="amountorder" class="java.math.BigDecimal"/>
	<field name="amountinv" class="java.math.BigDecimal"/>
	<variable name="total_order" class="java.math.BigDecimal" resetType="Group" resetGroup="Sales" calculation="Sum">
		<variableExpression><![CDATA[$F{amountorder}]]></variableExpression>
	</variable>
	<variable name="total_inv" class="java.math.BigDecimal" resetType="Group" resetGroup="Sales" calculation="Sum">
		<variableExpression><![CDATA[$F{amountinv}]]></variableExpression>
	</variable>
	<group name="Sales" isStartNewPage="true">
		<groupExpression><![CDATA[$F{sales}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement uuid="5f5b7103-0124-4919-ba8b-f85d30e9b05d" x="71" y="0" width="248" height="20"/>
					<box leftPadding="3"/>
					<textElement textAlignment="Justified" verticalAlignment="Middle">
						<font size="14" isBold="true" isItalic="false" isUnderline="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{sales}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="80e4572f-94d8-433e-b086-f5207ed5f84c" x="2" y="0" width="57" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="14"/>
					</textElement>
					<text><![CDATA[User]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="bb05f280-0022-4b93-b4fe-86dd33ef0c6a" x="59" y="0" width="12" height="20"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="14"/>
					</textElement>
					<text><![CDATA[:]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="50">
				<textField pattern="#,##0.00">
					<reportElement uuid="b3268b25-ec05-467e-b329-9dcb105ecc5a" x="204" y="0" width="110" height="16"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_order}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement uuid="0c636a6c-e43d-4bf9-988b-5b005aa5c922" x="314" y="0" width="100" height="16"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_inv}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement uuid="a925f74d-0416-4256-a34a-87ea7adb1000" x="0" y="16" width="555" height="1"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="Customer">
		<groupExpression><![CDATA[$F{customer}]]></groupExpression>
		<groupHeader>
			<band height="37">
				<textField>
					<reportElement uuid="5f5b7103-0124-4919-ba8b-f85d30e9b05d" x="0" y="0" width="555" height="17"/>
					<box leftPadding="3">
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="0.5"/>
						<bottomPen lineWidth="0.5"/>
						<rightPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Justified" verticalAlignment="Top">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA["#Customer :: " + $F{customer} + " #Address :: " + $F{address}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="599d93d6-204d-47c0-b8f8-4599fff1ffd6" x="204" y="17" width="112" height="20"/>
					<box leftPadding="4">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Sales Order]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="2de3f985-e00e-4416-b35f-5fcdc4843291" x="99" y="17" width="106" height="20"/>
					<box leftPadding="4">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Pre Order]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="c7dce352-2926-46e4-9132-057218a97a2c" x="315" y="17" width="110" height="20"/>
					<box leftPadding="4">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Invoice]]></text>
				</staticText>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="31" splitType="Stretch">
			<staticText>
				<reportElement uuid="1af80934-a248-4e64-bf0c-8fa0a62ef530" x="0" y="0" width="555" height="31"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Report Pre Order Sales]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="86" splitType="Stretch">
			<textField>
				<reportElement uuid="96bf19cf-f825-44de-ac29-1fac750de875" x="204" y="48" width="112" height="16">
					<printWhenExpression><![CDATA[$F{noorder} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{statusorder}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="ee15a85b-69ef-4b1a-b94b-e67685f65376" x="315" y="48" width="110" height="16">
					<printWhenExpression><![CDATA[$F{noinv} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{statusinv}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="5f169be0-de05-4a7f-94a4-f9a17e78b27b" x="99" y="48" width="106" height="16"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{statuspo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="a87434e6-978c-4ebb-b74f-fd95bd79abc7" x="204" y="16" width="112" height="16">
					<printWhenExpression><![CDATA[$F{noorder} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{noorder}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="a5b94f26-8a65-4852-8603-e25fc20279b0" x="204" y="32" width="112" height="16">
					<printWhenExpression><![CDATA[$F{noorder} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dateorder}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="3b6de768-e3be-4123-a618-750d48a63ee3" x="315" y="32" width="110" height="16">
					<printWhenExpression><![CDATA[$F{noinv} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dateinv}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="d0f12cd3-4892-4978-ba56-a6a017de0542" x="1" y="48" width="84" height="16"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Status]]></text>
			</staticText>
			<textField>
				<reportElement uuid="f42446a5-5971-4444-8d13-f9f461939246" x="315" y="16" width="110" height="16">
					<printWhenExpression><![CDATA[$F{noinv} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{noinv}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="f1d822d0-a53e-4bbb-998c-b37e52808c42" x="99" y="16" width="106" height="16"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nopo}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="937c69ae-49cf-4e03-bb99-c7f0089286fd" x="99" y="32" width="106" height="16"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{datepo}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="2a66ebb3-cd03-4113-ab45-32be53ee09b4" x="1" y="16" width="84" height="16"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Nomor]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="8cb46f2e-0e01-4170-b16a-696924ad144f" x="1" y="32" width="84" height="16"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Tanggal]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="2a66ebb3-cd03-4113-ab45-32be53ee09b4" x="1" y="0" width="84" height="16"/>
				<box>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Sales]]></text>
			</staticText>
			<textField>
				<reportElement uuid="f1d822d0-a53e-4bbb-998c-b37e52808c42" x="99" y="0" width="106" height="16"/>
				<box leftPadding="3">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{salespo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="f1d822d0-a53e-4bbb-998c-b37e52808c42" x="204" y="0" width="112" height="16">
					<printWhenExpression><![CDATA[$F{noorder} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{salesso}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="f1d822d0-a53e-4bbb-998c-b37e52808c42" x="315" y="0" width="110" height="16">
					<printWhenExpression><![CDATA[$F{noinv} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{salesinv}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="6c14f9f6-4e40-4463-8973-23e771f96846" x="85" y="0" width="14" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e55183e8-b3bf-4cde-bc01-82ed932eeee0" x="85" y="16" width="14" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="ece26268-6404-480c-9f6e-c79c8b976153" x="85" y="32" width="14" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="21b77163-6ddb-4ecd-8b88-aa9360c9ae2c" x="85" y="48" width="14" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[:]]></text>
			</staticText>
			<line>
				<reportElement uuid="a925f74d-0416-4256-a34a-87ea7adb1000" x="0" y="82" width="555" height="1"/>
			</line>
			<textField pattern="#,##0.00">
				<reportElement uuid="ee15a85b-69ef-4b1a-b94b-e67685f65376" x="315" y="64" width="110" height="16">
					<printWhenExpression><![CDATA[$F{noinv} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{amountinv}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="d0f12cd3-4892-4978-ba56-a6a017de0542" x="1" y="64" width="84" height="16"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Amount]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="21b77163-6ddb-4ecd-8b88-aa9360c9ae2c" x="85" y="64" width="14" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[:]]></text>
			</staticText>
			<textField>
				<reportElement uuid="5f169be0-de05-4a7f-94a4-f9a17e78b27b" x="99" y="64" width="106" height="16"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["--"]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement uuid="96bf19cf-f825-44de-ac29-1fac750de875" x="204" y="64" width="112" height="16">
					<printWhenExpression><![CDATA[$F{noorder} != null]]></printWhenExpression>
				</reportElement>
				<box leftPadding="3"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{amountorder}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
