<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="PrintTTTagihan" language="groovy" pageWidth="459" pageHeight="606" columnWidth="419" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="0" uuid="b5922b5e-fcc6-422f-a46e-6864aa6b2b08">
	<property name="ireport.zoom" value="1.6500000000000008"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="IncludedOldBill" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="UNS_BillingGroup_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
distinct
bp.name AS toko,
bpl.name AS lokasi,
b.documentno AS nodoc,
bg.datedoc AS date,
fk.dateinvoiced AS tglfk,
case when bl.referenceno is not null then bl.referenceno else fk.documentno end AS nofk,
bl.openamt AS nilaifk,
case when sm.poreference is not null then sm.poreference else sm.documentno end AS nosj,
now() AS tglskrg,
bl.description AS des,
b.description AS namatk

FROM
uns_billinggroup bg
INNER JOIN uns_billing b ON b.uns_billinggroup_id=bg.uns_billinggroup_id
INNER JOIN uns_billingline bl ON bl.uns_billing_id=b.uns_billing_id
INNER JOIN c_bpartner bp ON bp.c_bpartner_id=b.c_bpartner_id
INNER JOIN c_bpartner_location bpl ON bpl.c_bpartner_id=bp.c_bpartner_id
INNER JOIN c_invoice fk ON fk.c_invoice_id=bl.c_invoice_id
LEFT JOIN m_inout sm ON sm.c_invoice_id=fk.c_invoice_id

WHERE
bg.UNS_BillingGroup_ID = $P{UNS_BillingGroup_ID} AND (CASE WHEN $P{IncludedOldBill} = 'N' THEN (b.OldBilling_ID <= 0 OR b.OldBilling_ID IS NULL) ELSE 1=1 END)

ORDER BY bp.name]]>
	</queryString>
	<field name="toko" class="java.lang.String"/>
	<field name="lokasi" class="java.lang.String"/>
	<field name="nodoc" class="java.lang.String"/>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="tglfk" class="java.sql.Timestamp"/>
	<field name="nofk" class="java.lang.String"/>
	<field name="nilaifk" class="java.math.BigDecimal"/>
	<field name="nosj" class="java.lang.String"/>
	<field name="tglskrg" class="java.sql.Timestamp"/>
	<field name="des" class="java.lang.String"/>
	<field name="namatk" class="java.lang.String"/>
	<variable name="nilaifk_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[$F{nilaifk}]]></variableExpression>
	</variable>
	<group name="Customer" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{nodoc}]]></groupExpression>
		<groupHeader>
			<band height="163">
				<textField>
					<reportElement x="288" y="-5" width="130" height="20" uuid="3e4211ac-e778-47b7-b7f5-5f0a358945e0"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{namatk} == null ? $F{toko} : $F{namatk}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="288" y="18" width="130" height="45" uuid="9bf57fc6-0fa0-4a98-a15a-1808a4f18fb5"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{lokasi}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="169" y="79" width="113" height="20" uuid="d2aad93d-29f0-4953-9df2-c6335afcf830"/>
					<textElement verticalAlignment="Middle">
						<font size="14"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{nodoc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="24" splitType="Stretch">
			<textField pattern="dd/MM/yyyy">
				<reportElement x="26" y="0" width="72" height="24" uuid="1307e6e3-80fc-498e-9852-8c2a79718616"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tglfk}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="155" y="0" width="65" height="24" uuid="ff2f8778-ebc0-4682-b592-56b20fbb9818"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nofk}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.-">
				<reportElement x="216" y="0" width="94" height="24" uuid="2122fdac-a5cf-4fef-afa5-2650e45c8498"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nilaifk}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="98" y="0" width="62" height="24" uuid="e1529f42-d0bc-4200-a39d-c64bd5703d16"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nosj}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="56">
			<textField pattern="#,##0.-">
				<reportElement x="216" y="-1" width="94" height="20" uuid="ee6df0a0-b0e4-4bbe-a47a-5987c70e32d3"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{nilaifk_1}]]></textFieldExpression>
			</textField>
			<textField pattern="dd MMMMM yyyy">
				<reportElement x="285" y="36" width="133" height="20" uuid="595244d4-5993-4b06-8649-3400ed233f0e"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
