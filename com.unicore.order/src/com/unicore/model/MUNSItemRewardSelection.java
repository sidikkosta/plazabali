/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author ALBURHANY
 *
 */
public class MUNSItemRewardSelection extends X_UNS_ItemReward_Selection {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3949760906778973008L;

	/**
	 * @param ctx
	 * @param UNS_ItemReward_Selection_ID
	 * @param trxName
	 */
	public MUNSItemRewardSelection(Properties ctx,
			int UNS_ItemReward_Selection_ID, String trxName) {
		super(ctx, UNS_ItemReward_Selection_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSItemRewardSelection(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT 1 FROM UNS_ItemReward_Selection WHERE IsActive = 'Y' AND M_Product_ID = " + getM_Product_ID()
						+ " AND UNS_PeriodRedeem_ID = " + getUNS_PeriodRedeem_ID() 
						+ " AND UNS_ItemReward_Selection_ID <> " + get_ID();
		int duplicate = DB.getSQLValue(get_TrxName(), sql);
		
		if(duplicate >= 1)
			throw new AdempiereUserError("Duplicate product in one Period Redeem");
		
		if(getValuePoint().compareTo(Env.ZERO) <= 0)
			throw new AdempiereException("Value Point disallowed negate or zero");
			
		return true;
	}
	
	public boolean beforeDelete()
	{
//		String sql = "SELECT 1 FROM UNS_CustomerRedeem"; TODO comment, unused @Menjangan
		return true;
	}
}