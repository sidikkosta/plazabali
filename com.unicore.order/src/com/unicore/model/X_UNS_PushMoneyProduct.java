/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PushMoneyProduct
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PushMoneyProduct extends PO implements I_UNS_PushMoneyProduct, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181004L;

    /** Standard Constructor */
    public X_UNS_PushMoneyProduct (Properties ctx, int UNS_PushMoneyProduct_ID, String trxName)
    {
      super (ctx, UNS_PushMoneyProduct_ID, trxName);
      /** if (UNS_PushMoneyProduct_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setM_Product_ID (0);
			setUNS_PushMoneyProduct_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PushMoneyProduct (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PushMoneyProduct[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_PushMoney getUNS_PushMoney() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PushMoney)MTable.get(getCtx(), com.unicore.model.I_UNS_PushMoney.Table_Name)
			.getPO(getUNS_PushMoney_ID(), get_TrxName());	}

	/** Set Push Money.
		@param UNS_PushMoney_ID Push Money	  */
	public void setUNS_PushMoney_ID (int UNS_PushMoney_ID)
	{
		if (UNS_PushMoney_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PushMoney_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PushMoney_ID, Integer.valueOf(UNS_PushMoney_ID));
	}

	/** Get Push Money.
		@return Push Money	  */
	public int getUNS_PushMoney_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PushMoney_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Push Money Product.
		@param UNS_PushMoneyProduct_ID Push Money Product	  */
	public void setUNS_PushMoneyProduct_ID (int UNS_PushMoneyProduct_ID)
	{
		if (UNS_PushMoneyProduct_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PushMoneyProduct_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PushMoneyProduct_ID, Integer.valueOf(UNS_PushMoneyProduct_ID));
	}

	/** Get Push Money Product.
		@return Push Money Product	  */
	public int getUNS_PushMoneyProduct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PushMoneyProduct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PushMoneyProduct_UU.
		@param UNS_PushMoneyProduct_UU UNS_PushMoneyProduct_UU	  */
	public void setUNS_PushMoneyProduct_UU (String UNS_PushMoneyProduct_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PushMoneyProduct_UU, UNS_PushMoneyProduct_UU);
	}

	/** Get UNS_PushMoneyProduct_UU.
		@return UNS_PushMoneyProduct_UU	  */
	public String getUNS_PushMoneyProduct_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PushMoneyProduct_UU);
	}
}