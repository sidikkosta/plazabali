/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CCache;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSStoreConfiguration extends X_UNS_StoreConfiguration 
{
	private static CCache<Integer, MUNSStoreConfiguration> m_cache = new CCache<>(Table_Name, 10);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6427771327698651524L;

	/**
	 * @param ctx
	 * @param UNS_StoreConfiguration_ID
	 * @param trxName
	 */
	public MUNSStoreConfiguration(Properties ctx,
			int UNS_StoreConfiguration_ID, String trxName) {
		super(ctx, UNS_StoreConfiguration_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSStoreConfiguration(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSStoreConfiguration get(Properties ctx, int StoreID, String trxName)
	{
		MUNSStoreConfiguration config = m_cache.get(StoreID);
		if (config != null)
		{
			config.set_TrxName(trxName);
			return config;
		}
		
		config = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, COLUMNNAME_Store_ID + "=?", trxName).setParameters(StoreID)
					.firstOnly();
		
		return config;
	}
}