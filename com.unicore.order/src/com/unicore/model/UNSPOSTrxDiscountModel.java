package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MBPartner;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class UNSPOSTrxDiscountModel implements I_DiscountModel {

	private MUNSPOSTrx m_model = null;
	private List<I_DiscountModelLine> m_lines = null;
	private MBPartner m_BPartner = null;
	private MBPartner m_SalesRep = null;
	private boolean m_isBirthday = false;
	private String m_customerType = null;
	
	public UNSPOSTrxDiscountModel (MUNSPOSTrx model)
	{
		super();
		setModel(model);
	}
	
	@Override
	public void setModel(PO po) 
	{
		this.m_model = (MUNSPOSTrx) po;
		initialRequirements();
	}

	@Override
	public MUNSPOSTrx getModel() 
	{
		return this.m_model;
	}

	@Override
	public List<I_DiscountModelLine> getLines(boolean requery) 
	{
		if (null != m_lines && !requery)
		{
			return this.m_lines;
		}
		
		MUNSPOSTrxLine[] lines = getModel().getLines(requery);
		m_lines = new ArrayList<>();
		
		for (int i=0; i<lines.length; i++)
		{
			m_lines.add(new UNSPOSTrxLineDiscountModel(lines[i]));
		}
		
		return m_lines;
	}

	@Override
//	public I_DiscountModelLine getByProduct(int M_Product_ID, int refLine_ID)
	public I_DiscountModelLine getByProduct(int M_Product_ID, int refLine_ID,
			boolean isDiscountedToBonuses, boolean createNew) 
	{	
		I_DiscountModelLine line = null;
		List<I_DiscountModelLine> list = getLines(true);
		for(int i=0; i< list.size(); i++)
		{
			if(list.get(i).getProduct() == null)
				continue;
			
			if(list.get(i).getProduct().get_ID() != M_Product_ID)
				continue;
			
//			if(refLine_ID != -1 && list.get(i).getRefLine_ID() != refLine_ID)
//				continue;
			
			line =  list.get(i);
			break;
		}
		
		if(line == null && createNew)
		{
			MUNSPOSTrxLine tLine = new MUNSPOSTrxLine(m_model);
			tLine.setM_Product_ID(M_Product_ID);
			tLine.setisProductBonuses(true);
			tLine.setQtyEntered(Env.ZERO);
			tLine.setQtyBonuses(Env.ZERO);
			getModel().addLines(tLine);
			line = new UNSPOSTrxLineDiscountModel(tLine);
		}
		if (line != null)
			line.setHeaderInfo(m_model);
		
		return line;
	}

	@Override
	public void setDiscount(BigDecimal discount) {
		getModel().setDiscount(discount);
	}

	@Override
	public BigDecimal getDiscount() {
		return getModel().getDiscount();
	}

	@Override
	public void setDiscountAmt(BigDecimal discount) {
		getModel().setDiscountAmt(discount);
	}

	@Override
	public BigDecimal getDiscountAmt() {
		return getModel().getDiscountAmt();
	}

	@Override
	public void setGrandTotal(BigDecimal GrandTotal) {
		getModel().setGrandTotal(GrandTotal);
	}

	@Override
	public BigDecimal getGrandTotal() {
		return getModel().getGrandTotal();
	}

	@Override
	public void setTotalLines(BigDecimal TotalLines) {
		getModel().setTotalAmt(TotalLines);
	}

	@Override
	public BigDecimal getTotalLines() {
		return getModel().getTotalAmt();
	}

	@Override
	public void setBpartner(MBPartner Bpartner) {
		this.m_BPartner = Bpartner;
	}

	@Override
	public MBPartner getBPartner() {
		return this.m_BPartner;
	}

	@Override
	public void setSalesRep(MBPartner partner) {
		this.m_SalesRep = partner;
	}

	@Override
	public MBPartner getSalesRep() {
		return this.m_SalesRep;
	}

	@Override
	public boolean isBirthday() {
		return this.m_isBirthday;
	}

	@Override
	public void setIsBirthDay(boolean isBirthday) {
		this.m_isBirthday = isBirthday;
	}

	@Override
	public String get_TrxName() {
		return getModel().get_TrxName();
	}

	@Override
	public Properties getCtx() {
		return getModel().getCtx();
	}

	@Override
	public Timestamp getDateDiscounted() {
		return getModel().getDateAcct();
	}

	@Override
	public void setDateDiscounted(Timestamp dateDiscounted) {
		getModel().setDateAcct(dateDiscounted);
	}

	@Override
	public boolean isSOTrx() {
		return true;
	}

	@Override
	public void initialRequirements() 
	{
		MBPartner partner = new MBPartner(getCtx(), getModel().getC_BPartner_ID(), get_TrxName());
		setBpartner(partner);
		if(getModel().getSalesRep_ID() > 0)
		{
			MUser user = new MUser(getCtx(), getModel().getSalesRep_ID(), get_TrxName());
			MBPartner salesRep = (MBPartner) user.getC_BPartner();
			setSalesRep(salesRep);
		}
		if (getModel().getUNS_CustomerInfo_ID() > 0)
		{
			String customerType = DB.getSQLValueString(
					get_TrxName(), 
					"SELECT CustomerType FROM UNS_CustomerInfo WHERE UNS_CustomerInfo_ID = ?", 
					getModel().getUNS_CustomerInfo_ID());
			setCustomerType(customerType);
		}
		initializeBirthday();
	}

	@Override
	public boolean isPickup() {
		return false;
	}

	@Override
	public boolean isCash() {
		return false;
	}
	
	private void initializeBirthday ()
	{
		Timestamp datePromised = getModel().getDateTrx();
		Timestamp dateOfBirth = getBPartner().getBirthday();
		
		if(null == dateOfBirth)
		{
			setIsBirthDay(false);
			return;
		}
		
		Calendar calPromised = Calendar.getInstance();
		Calendar calBirthday = Calendar.getInstance();
		
		calPromised.setTimeInMillis(datePromised.getTime());
		calBirthday.setTimeInMillis(dateOfBirth.getTime());
		
		int datePromInt = calPromised.get(Calendar.DATE);
		int dateBirthInt = calBirthday.get(Calendar.DATE);
		int monthPromised = calPromised.get(Calendar.MONTH);
		int monthBirthday = calBirthday.get(Calendar.MONTH);
		
		boolean isSameDate = datePromInt == dateBirthInt;
		boolean isSameMonth = monthPromised == monthBirthday;
		
		if(isSameDate && isSameMonth)
			setIsBirthDay(true);
		else
			setIsBirthDay(false);
	}

	@Override
	public String getCustomerType() {
		// TODO Auto-generated method stub
		return m_customerType;
	}

	@Override
	public void setCustomerType(String customerType) {
		m_customerType = customerType;
	}

	private boolean m_isTrialMode = false;
	
	@Override
	public void setIsTrialMode(boolean isTrialmode) 
	{
		m_isTrialMode = isTrialmode;
	}

	@Override
	public boolean isTrialMode() 
	{
		return m_isTrialMode;
	}
}
