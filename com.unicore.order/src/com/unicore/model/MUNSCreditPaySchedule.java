/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.process.DocAction;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.UNSApps;

/**
 * @author Menjangan
 *
 */
public class MUNSCreditPaySchedule extends X_UNS_CreditPaySchedule 
implements Comparable<MUNSCreditPaySchedule>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1945032250959083553L;
	private String m_errorMsg = null;

	/**
	 * @param ctx
	 * @param UNS_CreditPaySchedule_ID
	 * @param trxName
	 */
	public MUNSCreditPaySchedule(Properties ctx, int UNS_CreditPaySchedule_ID,
			String trxName) {
		super(ctx, UNS_CreditPaySchedule_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCreditPaySchedule(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	private MUNSCreditAgreement m_parent = null;
	
	public MUNSCreditPaySchedule (MUNSCreditAgreement parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_CreditAgreement_ID(parent.get_ID());
		this.m_parent = parent;
	}
	
	public MUNSCreditAgreement getParent ()
	{
		if (null == m_parent)
		{
			m_parent = new MUNSCreditAgreement(getCtx(), getUNS_CreditAgreement_ID(), 
					get_TrxName());
		}
		
		return this.m_parent;
	}
	
	public MInvoice createInvoice (int C_PaymentTerm_ID)
	{
		MInvoice invoice = new MInvoice(getCtx(), 0, get_TrxName());
		invoice.setAD_Org_ID(getParent().getAD_Org_ID());
		invoice.setC_BPartner_ID(getParent().getC_BPartner_ID());
		invoice.setC_BPartner_Location_ID(getParent().getC_BPartner_Location_ID());
		invoice.setC_Currency_ID(303); //TODO
		invoice.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_ARInvoice));
		invoice.setIsSOTrx(true);
		invoice.setC_DocTypeTarget_ID();
		invoice.setDateAcct(getParent().getDateAcct());
		invoice.setDateInvoiced(getParent().getDateDoc());
		invoice.setDateOrdered(getParent().getDateDoc());
		invoice.setM_PriceList_ID(getParent().getM_PriceList_ID());
		invoice.setPaymentRule(MInvoice.PAYMENTRULE_OnCredit);
		invoice.setSalesRep_ID(getParent().getAD_User_ID());
		
		if (C_PaymentTerm_ID > 0)
			invoice.setC_PaymentTerm_ID(C_PaymentTerm_ID);
		
		String sql = "SELECT name FROM M_Product WHERE M_Product_ID = ?";
		String product = DB.getSQLValueString(get_TrxName(), sql, getParent().getM_Product_ID());
		
		if(getParent().getProductType().equals(X_UNS_CreditAgreement.PRODUCTTYPE_Item))
		{
			invoice.setDescription("::Kredit Barang. "+product+" ::");
		}
		else if(getParent().getProductType().equals(X_UNS_CreditAgreement.PRODUCTTYPE_Vehicle))
		{
			invoice.setDescription("::Kredit Kendaraan. "+product+" ::");
		}
	
		try
		{
			invoice.saveEx();
			
			MInvoiceLine line = new MInvoiceLine(invoice);
			line.setQty(Env.ONE);
			line.setPriceEntered(getParent().getMonthlyInstallmentAmt()); 
			line.setPriceActual(line.getPriceEntered());
			line.setPriceList(line.getPriceEntered());
			int C_Tax_ID = DB.getSQLValueEx(get_TrxName(), "SELECT C_Tax_ID FROM C_Tax WHERE Name = ?",
					"Standard");
			
			if (C_Tax_ID <= 0)
			{
				throw new AdempiereUserError("Could not find tax with name Standard");
			}
			
			line.setC_Tax_ID(C_Tax_ID);
			line.setC_Charge_ID(UNSApps.getRefAsInt(UNSApps.CHRG_PendptnPnjualn));
			line.saveEx();
			
			if (!invoice.processIt(DocAction.STATUS_Completed))
			{
				this.m_errorMsg = invoice.getProcessMsg();
				return null;
			}
			
			invoice.saveEx();
		}
		catch (Exception ex)
		{
			this.m_errorMsg = ex.getMessage();
			return null;
		}
		
		return invoice;
	}
	
	public boolean cancelIt ()
	{
		if (getC_Invoice_ID() == 0)
		{
			addDescription("***Voided***");
			return true;
		}
		
		MInvoice invoice = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
		if (!"RE".equals(invoice.getDocStatus()) && !"VO".equals(invoice.getDocStatus()))
		{
			try
			{
				boolean ok = invoice.processIt(DocAction.ACTION_Void);
				if (!ok)
				{
					m_errorMsg = invoice.getProcessMsg();
					return false;
				}
				
				addDescription("***Voided | Paid Amt " + getPaidAmt());
				addDescription(" | Invoice ");
				addDescription(invoice.getDocumentNo());
				
				invoice.saveEx();
			}
			catch (Exception ex)
			{
				m_errorMsg = ex.getMessage();
				return false;
			}

		}
				
		return true;
	}
	
	public String getErrorMsg ()
	{
		return m_errorMsg;
	}
	
	public void addDescription (String description)
	{
		String oldDesc = getDescription();
		StringBuilder buildDesc = new StringBuilder();
		
		if (!Util.isEmpty(oldDesc, true))
		{
			buildDesc.append(oldDesc);
			buildDesc.append(" | ");
		}
		
		buildDesc.append(description);
		String newDescription = buildDesc.toString();
		setDescription(newDescription);
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		if (getC_Invoice_ID() > 0)
		{
			MInvoice invoice = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
			if (!invoice.isProcessed() && !invoice.delete(true))					
				return false;
			else if (!"RE".equals(invoice.getDocStatus()) && !"VO".equals(invoice.getDocStatus()))
			{
				boolean ok = invoice.processIt(DocAction.ACTION_Void);
				if (!ok)
				{
					log.log(Level.SEVERE, invoice.getProcessMsg());
					return false;
				}
				
				if (!invoice.save())
					return false;
			}
		}
		
		return super.beforeDelete();
	}
	
	public static MUNSCreditPaySchedule[] get (String trxName, 
			int UNS_CreditAgreement_ID)
	{
		String whereClause = "UNS_CreditAgreement_ID = ? ";
		List<MUNSCreditPaySchedule> list = Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				Table_Name, whereClause, trxName).
				setParameters(UNS_CreditAgreement_ID).
				setOrderBy(COLUMNNAME_Line).
				setOnlyActiveRecords(false).
				list();
		MUNSCreditPaySchedule[] schedules = new MUNSCreditPaySchedule[list.size()];
		list.toArray(schedules);
		
		return schedules;
	}

	@Override
	public int compareTo(MUNSCreditPaySchedule arg0) 
	{
		return getLine() - arg0.getLine();
	}
}