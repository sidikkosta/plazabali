/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSCashierTandem extends X_UNS_CashierTandem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -506298728682249790L;
	
	private MUNSPOSSession m_session = null;
	
	public MUNSPOSSession getParent ()
	{
		if (m_session == null)
			m_session = new MUNSPOSSession(getCtx(), getUNS_POS_Session_ID(), get_TrxName());
		
		return m_session;
	}
	
	public MUNSCashierTandem (MUNSPOSSession session)
	{
		this (session.getCtx(), 0, session.get_TrxName());
		setClientOrg(session);
		m_session = session;
		setUNS_POS_Session_ID(session.get_ID());
	}
	
	public static MUNSCashierTandem get (String trxName, int sessionID, int cashierID)
	{
		String where = "UNS_POS_Session_ID = ? AND Cashier_ID = ?";
		MUNSCashierTandem tandem = Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				where, trxName).setParameters(sessionID, cashierID).
				firstOnly();
		return tandem;
	}

	/**
	 * @param ctx
	 * @param UNS_CashierTandem_ID
	 * @param trxName
	 */
	public MUNSCashierTandem(Properties ctx, int UNS_CashierTandem_ID,
			String trxName) 
	{
		super(ctx, UNS_CashierTandem_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCashierTandem(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if(isReplication())
			return true;
		String parentDesc = getParent().getDescription();
		if (parentDesc == null)
			parentDesc = "";
		if (!parentDesc.contains("**") && !parentDesc.contains("Import "))
		{
			String sql = "SELECT DocumentNo FROM UNS_POS_Session WHERE (Cashier_ID = ? OR EXISTS (SELECT * FROM UNS_CashierTandem WHERE Cashier_ID = ? AND "
					+ " UNS_POS_Session_ID = UNS_POS_Session.UNS_POS_SEssion_ID)) AND "
					+ " UNS_POS_Session_ID <> ? AND DocStatus IN (?,?)";
			String exsistsDocNo = DB.getSQLValueString(get_TrxName(), sql, getCashier_ID(), getCashier_ID(), getUNS_POS_Session_ID(), "DR", "IP");
			if (!Util.isEmpty(exsistsDocNo))
			{
				log.log(Level.SEVERE, "Session for user already exists " + exsistsDocNo);
				return false;
			}
		}
		return super.beforeSave(newRecord);
	}
}