/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author root
 *
 */
public class MUNSAchievedIncentiveLine extends X_UNS_AchievedIncentive_Line {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MUNSAchievedIncentive m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_AchievedIncentive_Line_ID
	 * @param trxName
	 */
	public MUNSAchievedIncentiveLine(Properties ctx,
			int UNS_AchievedIncentive_Line_ID, String trxName) {
		super(ctx, UNS_AchievedIncentive_Line_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAchievedIncentiveLine(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
	}
	
	/**
	 * 
	 */
	public MUNSAchievedIncentive getParent()
	{
		if(null != m_parent)
			return m_parent;
		
		return (MUNSAchievedIncentive) getUNS_AchievedIncentive();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		updateHeaderPeriod();
		return super.afterSave(newRecord, success);
	}
	
	/**
	 * 
	 * @param parentPeriod
	 */
	public MUNSAchievedIncentiveLine(MUNSAcvIncentiveByPeriod parentPeriod)
	{
		super(parentPeriod.getCtx(), 0, parentPeriod.get_TrxName());
		setUNS_AcvIncentiveByPeriod_ID(parentPeriod.get_ID());
		setUNS_AchievedIncentive_ID(parentPeriod.getUNS_AchievedIncentive_ID());
		setAD_Org_ID(parentPeriod.getAD_Org_ID());
		m_acvIncentiveByPeriod = parentPeriod;
	}

	
	/**
	 * 
	 */
	private void updateHeaderPeriod()
	{
		MUNSAcvIncentiveByPeriod acvPeriod = getAcvIncentivePeriod();
		String sql = "SELECT COALESCE(SUM(Amount), 0) FROM UNS_AchievedIncentive_Line "
				+ " WHERE UNS_AcvIncentiveByPeriod_ID = ?";
		BigDecimal totalAmt = DB.getSQLValueBD(get_TrxName(), sql, acvPeriod.get_ID());
		acvPeriod.setAmount(totalAmt);
		acvPeriod.saveEx();
	}
	
	private MUNSAcvIncentiveByPeriod m_acvIncentiveByPeriod = null;
	
	/**
	 * 
	 * @return
	 */
	public MUNSAcvIncentiveByPeriod getAcvIncentivePeriod()
	{
		if(null != m_acvIncentiveByPeriod)
			return m_acvIncentiveByPeriod;
		
		m_acvIncentiveByPeriod = new MUNSAcvIncentiveByPeriod(
				getCtx(), getUNS_AcvIncentiveByPeriod_ID(), get_TrxName());
		
		return m_acvIncentiveByPeriod;
	}

	/**
	 * 
	 * @param parent
	 * @param reference
	 * @param trxName
	 * @return
	 */
	public static MUNSAchievedIncentiveLine get(PO parent, PO reference, String trxName)
	{
		if(null == parent || null == reference)
			throw new AdempiereException(
					"Error on get Achieved Incentive Line no parent and no reference parameter");
		
		StringBuilder sql = new StringBuilder("SELECT * FROM ").append(Table_Name)
				.append(" WHERE ").append(parent.get_TableName()).append("_ID = ?")
				.append(" AND ").append(reference.get_TableName()).append("_ID = ?");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = DB.prepareStatement(sql.toString(), trxName);
			st.setInt(1, parent.get_ValueAsInt(parent.get_TableName().concat("_ID")));
			st.setInt(2, reference.get_ValueAsInt(reference.get_TableName().concat("_ID")));
			rs = st.executeQuery();
			if(rs.next())
			{
				return new MUNSAchievedIncentiveLine(Env.getCtx(), rs, trxName);
			}
		} catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param reference
	 * @param trxName
	 * @return
	 */
	public static MUNSAchievedIncentiveLine get(PO reference, String trxName)
	{
		if(null == reference)
			throw new AdempiereException(
					"Error on get Achieved Incentive Line no reference parameter");
		
		StringBuilder sql = new StringBuilder("SELECT * FROM ").append(Table_Name)
				.append(" WHERE ").append(reference.get_TableName()).append("_ID = ?");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = DB.prepareStatement(sql.toString(), trxName);
			st.setInt(1, reference.get_ValueAsInt(reference.get_TableName().concat("_ID")));
			rs = st.executeQuery();
			if(rs.next())
			{
				return new MUNSAchievedIncentiveLine(Env.getCtx(), rs, trxName);
			}
		} catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param C_InvoiceLine_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSAchievedIncentiveLine get(int C_InvoiceLine_ID, String trxName)
	{
		if(C_InvoiceLine_ID <= 0)
			throw new AdempiereException(
					"Error on get Achieved Incentive Line no reference parameter");
		
		StringBuilder sql = new StringBuilder("SELECT * FROM ").append(Table_Name)
				.append(" WHERE C_InvoiceLine_ID = ?");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = DB.prepareStatement(sql.toString(), trxName);
			st.setInt(1, C_InvoiceLine_ID);
			rs = st.executeQuery();
			if(rs.next())
			{
				return new MUNSAchievedIncentiveLine(Env.getCtx(), rs, trxName);
			}
		} catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public boolean isSalesIncentive()
	{
		return getC_InvoiceLine_ID() > 0;
	}
	
	public boolean isBillingIncentive()
	{
		return getC_PaymentAllocate_ID() > 0;
	}
	
}
