/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.MessageBox;

/**
 * @author AzHaidar
 *
 */
public class MUNSGradingSheetAll extends X_UNS_Grading_Sheet_All implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4642448568266737686L;

	/**
	 * @param ctx
	 * @param UNS_Grading_Sheet_All_ID
	 * @param trxName
	 */
	public MUNSGradingSheetAll(Properties ctx, int UNS_Grading_Sheet_All_ID,
			String trxName) {
		super(ctx, UNS_Grading_Sheet_All_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSGradingSheetAll(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param UNS_WeighbridgeTicket_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSGradingSheetAll get(Properties ctx, int UNS_WeighbridgeTicket_ID, String trxName) 
	{
		MUNSGradingSheetAll grading = 
				Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, "UNS_WeighbridgeTicket_ID=?", trxName)
				.setParameters(UNS_WeighbridgeTicket_ID)
				.first();
		
		return grading;
	}

	@Override
    protected boolean beforeSave(boolean newRecord) 
    {
		boolean needChecking = !newRecord && !getDocStatus().equals(DOCSTATUS_Voided) && !getDocStatus().equals(DOCSTATUS_Reversed);
		// Void dan Reversed tidak akan newRecord, krn hanya mekanisme void yang dipakai, 
		// dan sudah pasti no need to checking.
		
		if ((!newRecord && !needChecking) || m_isResettingReflection)
			return true;
		
		if (newRecord)
		{
			String sql = "SELECT 1 FROM UNS_Grading_Sheet_All WHERE UNS_WeighbridgeTicket_ID=?";
			int count = DB.getSQLValueEx(get_TrxName(), sql, getUNS_WeighbridgeTicket_ID());
			
			if (count > 0) {
				log.saveError("GradingSheetExists", "1 Ticket must have only one grading sheet.");
				return false;
			}
			if (getFFBReceiptCondition() == null)
				setFFBReceiptCondition(FFBRECEIPTCONDITION_DominantlyBacklog);			
		}
		else {
			if (getFFBReceiptCondition() != null && 
					getFFBReceiptCondition().equals(FFBRECEIPTCONDITION_GraderOverwritten))
				return true;					
		}
		
		MUNSWeighbridgeTicket wbt = 
				new MUNSWeighbridgeTicket(getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
		
		MUNSOrderContract ctr = 
				Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
						MUNSOrderContract.Table_Name, "C_Order_ID = ?", get_TrxName())
					.setParameters(wbt.getC_Order_ID())
					.first();
		
		if (ctr == null)
		{
			log.saveError("ErrorTicketNoContract", "Weighbridge Ticket document does not have order/contract.");
			return false;
		}
		
		boolean defaultCalculation = true;
		
		if (!newRecord && 
				(is_ValueChanged(COLUMNNAME_FFBReceiptCondition) || 
					(is_ValueChanged(COLUMNNAME_IsQtyPercentage) && !getFFBReceiptCondition().equals(FFBRECEIPTCONDITION_GraderOverwritten)))) 
		{
			String msg = "The IsQtyPercentage and or FFB condition receipt has been changed, "
					+ "do you want to recalculate the compulsory "
					+ "and the old-bunch deduction value to default calculation?";
			String title = "Recalculate compulsory deduction?";
			
			int answer = MessageBox.showMsg(
					this, getCtx(), msg, title, MessageBox.YESNO, MessageBox.ICONQUESTION);
			if (answer == MessageBox.RETURN_NO) 
			{
				defaultCalculation = false;
				set_Value(COLUMNNAME_FFBReceiptCondition, get_ValueOld(COLUMNNAME_FFBReceiptCondition));
			}

			if (isQtyPercentage() && !getFFBReceiptCondition().equals(FFBRECEIPTCONDITION_GraderOverwritten))
			{
				msg = "Do you also want to reset all the value on the Netto Status Group?";
				title = "Resetting all value on Netto Status?";
				answer = MessageBox.showMsg(
						this, getCtx(), msg, title, MessageBox.YESNO, MessageBox.ICONQUESTION);
				if (answer == MessageBox.RETURN_YES) 
				{
					setCompulsoryDeduction(Env.ZERO);
					setSmallFruit(Env.ZERO);
					setUnripe(Env.ZERO);
					setOverRipe(Env.ZERO);
					setDirt(Env.ZERO);
					setLongStalk(Env.ZERO);
					setDura(Env.ZERO);
					setRottenBunch(Env.ZERO);
					setDripWet(Env.ZERO);
					
					setUnderdone(Env.ZERO);
					setEmptyBunch(Env.ZERO);
					setDirtyBunch(Env.ZERO);
					setOldBunch(Env.ZERO);
					setBunchFailure(Env.ZERO);
					setAdditional_Qty(Env.ZERO);
				}
			}			
		}
		
		if ((newRecord || is_ValueChanged(COLUMNNAME_FFBReceiptCondition) || is_ValueChanged(COLUMNNAME_IsQtyPercentage))
				&& (!is_ValueChanged(COLUMNNAME_CompulsoryDeduction) || !is_ValueChanged(COLUMNNAME_OldBunch)
					|| !is_ValueChanged(COLUMNNAME_Percent_CompulsoryDeduction) || !is_ValueChanged(COLUMNNAME_Percent_OldBunch)
					|| !is_ValueChanged(COLUMNNAME_Est_CompulsoryDeduction) || !is_ValueChanged(COLUMNNAME_Est_OldBunch))) 
		{
			if (defaultCalculation)
			{
				BigDecimal compulsoryPercentage = ctr.getFreshCompulsoryDeduction();
				BigDecimal oldBunchPercentage = Env.ZERO;
				
				if (getFFBReceiptCondition() != null
						&& getFFBReceiptCondition().equals(FFBRECEIPTCONDITION_DominantlyBacklog) 
						&& ctr.getBacklogCompulsoryDeduction().compareTo(ctr.getFreshCompulsoryDeduction()) > 0)
				{
					//compulsoryPercentage = ctr.getBacklogCompulsoryDeduction();
					oldBunchPercentage = ctr.getBacklogCompulsoryDeduction().subtract(compulsoryPercentage);
				}
				
				
				if (isQtyPercentage())
				{
					if (compulsoryPercentage.signum() > 0)
					{
						BigDecimal compulsoryDeduction = 
								wbt.getGrossWeight().subtract(getDefaultTare()).multiply(compulsoryPercentage)
								.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_HALF_UP);
						setEst_CompulsoryDeduction(compulsoryDeduction);
						setPercent_CompulsoryDeduction(compulsoryPercentage);
						setCompulsoryDeduction(Env.ZERO); // To make it calculated when ticket's tare defined.
					}
					
					if (oldBunchPercentage.signum() > 0)
					{
						BigDecimal oldBunchDeduction = 
								wbt.getGrossWeight().subtract(getDefaultTare()).multiply(oldBunchPercentage)
								.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_HALF_UP);
						setEst_OldBunch(oldBunchDeduction);
						setPercent_OldBunch(oldBunchPercentage);
						setOldBunch(Env.ZERO); // To make it calculated when ticket's tare defined.
					}
				}
				else if (wbt.getTare().signum() > 0)
				{
					if (compulsoryPercentage.signum() > 0)
					{
						BigDecimal compulsoryDeduction = 
								wbt.getNettoI().multiply(compulsoryPercentage)
								.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_HALF_UP);
						setCompulsoryDeduction(compulsoryDeduction);
					}
					
					if (oldBunchPercentage.signum() > 0)
					{
						BigDecimal oldBunchDeduction = wbt.getNettoI().multiply(oldBunchPercentage)
								.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_HALF_UP);
						setOldBunch(oldBunchDeduction);
					}
				}
			}
		}
		else if(needChecking)
			defaultCalculation = false;
		
		if (!defaultCalculation && 
				(is_ValueChanged(COLUMNNAME_CompulsoryDeduction) || is_ValueChanged(COLUMNNAME_OldBunch)
				|| is_ValueChanged(COLUMNNAME_Percent_CompulsoryDeduction) || is_ValueChanged(COLUMNNAME_Percent_OldBunch)
				|| is_ValueChanged(COLUMNNAME_Est_CompulsoryDeduction) || is_ValueChanged(COLUMNNAME_Est_OldBunch)))
		{
			setFFBReceiptCondition(FFBRECEIPTCONDITION_GraderOverwritten);
		}
		
		boolean hasCompulsoryClause = (ctr.getFreshCompulsoryDeduction().signum() > 0 
				|| ctr.getBacklogCompulsoryDeduction().signum() > 0);
		
		if (!newRecord && hasCompulsoryClause && (getCompulsoryDeduction().signum() <= 0 && !isQtyPercentage()))
		{
			log.saveError("ErrorCompulsoryMandatory", 
						  "You have to fill compulsory deduction amount, it is based on contract.");
			return false;
		}
		
		return true;
    } // beforeSave
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		boolean needChecking = !getDocStatus().equals(DOCSTATUS_Voided) && !getDocStatus().equals(DOCSTATUS_Reversed);
		if(needChecking)
		{
			BigDecimal totalReflection = getTotalReflection();
					
			if (!m_isResettingReflection && totalReflection.signum() > 0)
			{
				String sql = "UPDATE UNS_WeighbridgeTicket SET reflection=?, OriginalReflection=?, NettoII=(NettoI-?) "
						+ " WHERE UNS_WeighbridgeTicket_ID=?";
				
				int count = DB.executeUpdateEx(sql, 
						new Object[]{totalReflection, totalReflection, totalReflection, getUNS_WeighbridgeTicket_ID()}, 
						get_TrxName());
				
				if (count <= 0)
				{
					log.saveError("CannotUpdateReflection", "Error: cannot update Reflection and NettoII amount.");
					return false;
				}
			}
		}
				
		return true;
	} // afterSave
	
	
	public BigDecimal getTotalReflection()
	{
		BigDecimal totalReflection = getCompulsoryDeduction().add(getBunchFailure()).add(getDirtyBunch())
				.add(getDripWet()).add(getDura()).add(getEmptyBunch()).add(getLongStalk())
				.add(getOldBunch()).add(getRottenBunch()).add(getUnderdone()).add(getUnripe().add(getAdditional_Qty())
				.add(getOverRipe()).add(getDirt()).add(getSmallFruit()));
		return totalReflection;
	} // getTotalReflection
	
	
	public BigDecimal resetReflection (MUNSWeighbridgeTicket wbt)
	{
		m_isResettingReflection = true;
		
		if (!isQtyPercentage())
		{
			if (!getFFBReceiptCondition().equals(FFBRECEIPTCONDITION_GraderOverwritten))
			{ 
				// Jika bukan grader overwritten, maka kaluklasikan ulang compulsory dan old-bunch 
				// sesuai dg tare yang ada di tiket.
				BigDecimal nettoI = wbt.getGrossWeight().subtract(wbt.getTare());
				MUNSOrderContract ctr = 
						Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
								MUNSOrderContract.Table_Name, "C_Order_ID = ?", get_TrxName())
							.setParameters(wbt.getC_Order_ID())
							.first();
				
				BigDecimal compulsory =
						ctr.getFreshCompulsoryDeduction().multiply(nettoI)
						.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_UNNECESSARY);
				setCompulsoryDeduction(compulsory);
				
				if (getFFBReceiptCondition().equals(FFBRECEIPTCONDITION_DominantlyBacklog)
						&& (ctr.getFreshCompulsoryDeduction().compareTo(ctr.getBacklogCompulsoryDeduction()) < 0))
				{
					BigDecimal oldBunch = ctr.getBacklogCompulsoryDeduction().subtract(ctr.getFreshCompulsoryDeduction())
							.multiply(nettoI).divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_UNNECESSARY);
					setOldBunch(oldBunch);
				}
			}
			
			this.saveEx();
			
			m_isResettingReflection = false;
			
			return getTotalReflection();
		}
		
		BigDecimal totalReflection = Env.ZERO;
		BigDecimal nettoI = wbt.getGrossWeight().subtract(wbt.getTare());
		double dNettoI = nettoI.doubleValue();
		
		if (getCompulsoryDeduction().signum() == 0 || 
				getDocStatus().equals(DOCSTATUS_Completed) || getDocStatus().equals(DOCSTATUS_Closed)) 
		{
			BigDecimal compulsory =
					getPercent_CompulsoryDeduction().multiply(nettoI)
					.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_UNNECESSARY);
					//calculateRoundedQty(getPercent_CompulsoryDeduction().doubleValue(), dNettoI);
			setCompulsoryDeduction(compulsory);
		}
		if (getBunchFailure().signum() == 0) {
			BigDecimal bunchFailure = 
					calculateRoundedQty(getPercent_BunchFailure().doubleValue(), dNettoI);
			setBunchFailure(bunchFailure);
		}
		if (getAdditional_Qty().signum() == 0) {
			BigDecimal addDeduct = 
					calculateRoundedQty(getPercent_AddDeduct().doubleValue(), dNettoI);
			setAdditional_Qty(addDeduct);
		}
		if (getDirtyBunch().signum() == 0) {
			BigDecimal dirtyBunch = 
					calculateRoundedQty(getPercent_DirtyBunch().doubleValue(), dNettoI);
			setDirtyBunch(dirtyBunch);
		}
		if (getDripWet().signum() == 0) {
			BigDecimal dripWet = 
					calculateRoundedQty(getPercent_DripWet().doubleValue(), dNettoI);
			setDripWet(dripWet);
		}
		if (getDura().signum() == 0) {
			BigDecimal dura = 
					calculateRoundedQty(getPercent_Dura().doubleValue(), dNettoI);
			setDura(dura);
		}
		if (getEmptyBunch().signum() == 0) {
			BigDecimal emptyBunch = 
					calculateRoundedQty(getPercent_EmptyBunch().doubleValue(), dNettoI);
			setEmptyBunch(emptyBunch);
		}
		if (getLongStalk().signum() == 0) {
			BigDecimal longStalk = 
					calculateRoundedQty(getPercent_LongStalk().doubleValue(), dNettoI);
			setLongStalk(longStalk);
		}
		if (getOldBunch().signum() == 0 ||
				getDocStatus().equals(DOCSTATUS_Completed) || getDocStatus().equals(DOCSTATUS_Closed))
		{
			BigDecimal oldBunch = getPercent_OldBunch().multiply(nettoI)
					.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_UNNECESSARY);
					//calculateRoundedQty(getPercent_OldBunch().doubleValue(), dNettoI);
			setOldBunch(oldBunch);
		}
		if (getRottenBunch().signum() == 0) {
			BigDecimal rottenBunch = 
					calculateRoundedQty(getPercent_RottenBunch().doubleValue(), dNettoI);
			setRottenBunch(rottenBunch);
		}
		if (getUnderdone().signum() == 0) {
			BigDecimal underdone = 
					calculateRoundedQty(getPercent_Underdone().doubleValue(), dNettoI);
			setUnderdone(underdone);
		}
		if (getUnripe().signum() == 0) {
			BigDecimal unrip = 
					calculateRoundedQty(getPercent_Unripe().doubleValue(), dNettoI);		
			setUnripe(unrip);
		}
		if (getSmallFruit().signum() == 0) {
			BigDecimal smallfruit = 
					calculateRoundedQty(getPercent_SmallFruit().doubleValue(), dNettoI);
			setSmallFruit(smallfruit);
		}
		if (getOverRipe().signum() == 0) {
			BigDecimal overrip =
					calculateRoundedQty(getPercent_OverRipe().doubleValue(), dNettoI);
			setOverRipe(overrip);
		}
		if (getDirt().signum() == 0) {
			BigDecimal dirt = 
					calculateRoundedQty(getPercent_Dirt().doubleValue(), dNettoI);
			setDirt(dirt);
		}
		
		saveEx();
		
		m_isResettingReflection = false;
		
		totalReflection = getTotalReflection();
		
		return totalReflection;
	}
	
	
	private BigDecimal calculateRoundedQty(double percentage, double dNettoI)
	{
		BigDecimal deduction = Env.ZERO;
		if (getRoundingScale().equals(ROUNDINGSCALE_NoScale))
		{
			deduction = BigDecimal.valueOf((percentage/100) * dNettoI).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		else {
			double roundingScale = Double.valueOf(getRoundingScale());
			
			double portion = percentage/100;
			double deducted = portion * dNettoI;
			
			double rounded = deducted / roundingScale;
			
			BigDecimal rounding = BigDecimal.valueOf(roundingScale);
			
			deduction = BigDecimal.valueOf(rounded).setScale(0, BigDecimal.ROUND_HALF_UP).multiply(rounding);
		}
		
		return deduction;
	}
	
	
	public BigDecimal recalculateDeductions(MUNSWeighbridgeTicket wbt)
	{
		BigDecimal totalDeduction = Env.ZERO;
		
		
		
		return totalDeduction;
	}
	
	
	String m_processMsg = null;
	private boolean m_justPrepared = false;
	private boolean m_isResettingReflection = false;

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String processAction) throws Exception 
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		//setProcessing(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;


		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	} // prepareIt
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		
		MUNSWeighbridgeTicket ticket = new MUNSWeighbridgeTicket(
				getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
		if (ticket.getTare().signum() > 0) {
			BigDecimal newReflection = resetReflection(ticket);
			ticket.setReflection(newReflection);
			ticket.setOriginalReflection(newReflection);
			
			BigDecimal nettoII = ticket.getNettoI().subtract(newReflection);
			
			ticket.setNettoII(nettoII);
		}
		
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		setProcessed(true);	
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		MUNSWeighbridgeTicket ticket = (MUNSWeighbridgeTicket) getUNS_WeighbridgeTicket();
		
		if (ticket.isProcessed())
		{
			m_processMsg = "Cannot reactivate Grading Sheet for the completed Ticket.";
			return false;
		}

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocStatus(DOCSTATUS_Drafted);
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		//	: Grand Total = 123.00 (#1)
		BigDecimal totalDeduction = 
				getCompulsoryDeduction().add(getBunchFailure()).add(getDirtyBunch()).add(getDripWet())
				.add(getDura()).add(getEmptyBunch()).add(getLongStalk()).add(getOldBunch())
				.add(getRottenBunch()).add(getUnderdone()).add(getUnripe()).add(getOverRipe()).add(getSmallFruit()).add(getDirt());
		sb.append(": ").
			append("Total").append("=").append(totalDeduction);
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo()
	{
		return getDocumentNo();
	}	//	getDocumentInfo

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
		ReportEngine re = ReportEngine.get (
				getCtx(), ReportEngine.SHIPMENT, getUNS_Grading_Sheet_All_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if(format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo ("", format.getJasperProcess_ID());
			pi.setRecord_ID ( getUNS_Grading_Sheet_All_ID() );
			pi.setIsBatch(true);
			
			ServerProcessCtl.process(pi, null);
			
			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	}	//	createPDF
	
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{	
		return getUpdatedBy();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt()
	{
		return Env.ZERO;
	}	//	getApprovalAmt

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(DOCSTATUS_Completed.equals(getDocStatus()))
		{
			options[index++] = DocumentEngine.ACTION_ReActivate;
		}
		
		return index;
	}

} // MUNSGradingSheetAll