/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
//import org.compiere.util.Env;
import org.compiere.util.Env;

/**
 * @author ALBURHANY
 *
 */
public class MUNSAccessoriesLine extends X_UNS_AccessoriesLine implements ISortTabRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9035519669228783060L;
	private static final int HEADOFFICE_ID = 1000039;
	private static final int FIBREGLASS_ID = 1000041;
	private static final int PIPEFACTORY_ID = 1000042;
	private static final int BEKASI_ID = 1000047;
	
	public MUNSAccessoriesLine(Properties ctx, int UNS_AccessoriesLine_ID,
			String trxName) {
		super(ctx, UNS_AccessoriesLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAccessoriesLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static String CreateAccessories(Properties ctx, int M_Product_ID, BigDecimal QtyParent, int Parent_ID,
			String TableName, String trxName, boolean newRecord)
	{
		StringBuilder msg = new StringBuilder();
		if(null == TableName)
			return msg.toString();
		
		//NOW READY IN HEADOFFICE ONLY
		boolean justHeadOffice = MSysConfig.getBooleanValue(MSysConfig.IMPLEMENTED_JUST_HEADOFFICE, false);
		String sqlAD_Org_ID = "SELECT AD_Org_ID FROM " + TableName + " WHERE " + TableName + "_ID = ?";
		int Org_ID = DB.getSQLValue(trxName, sqlAD_Org_ID, Parent_ID);
		if(HEADOFFICE_ID != Org_ID && FIBREGLASS_ID != Org_ID && PIPEFACTORY_ID != Org_ID && BEKASI_ID != Org_ID
				&& justHeadOffice)
			return msg.toString();
		//
		
		if(M_Product_ID > 0)
		{
			if(MProduct.isWithAccessories(ctx, M_Product_ID)) 
			{
				for(MUNSProductAccessories pa : MUNSProductAccessories.getAccessoriesLines(ctx, M_Product_ID, trxName))
				{
					MUNSAccessoriesLine line = null;
					boolean LoadAllAccessories = MSysConfig.getBooleanValue(MSysConfig.LOAD_ALL_ACCESSORIES, false);
					if(pa.getAccessoriesType().equals(X_UNS_ProductAccessories.ACCESSORIESTYPE_Optional)
							&& newRecord && !LoadAllAccessories)
						continue;
					if(newRecord)
					{
						String sql = "SELECT COUNT(*) FROM UNS_AccessoriesLine WHERE UNS_ProductAccessories_ID=?"
								+ " AND " + TableName + "_ID=?";
						boolean exists = DB.getSQLValue(trxName, sql, new Object[]{pa.get_ID(), Parent_ID}) > 0 ?
								true : false;
						if(exists)
							continue;
					}
					line = get(ctx, pa.get_ID(), TableName, Parent_ID, trxName);
					if(null == line)
					{
						line = new MUNSAccessoriesLine(ctx, 0, trxName);
						line.setUNS_ProductAccessories_ID(pa.get_ID());
						line.set_ValueOfColumn(TableName +"_ID", Parent_ID);
					}
					line.setQty(QtyParent.multiply(pa.getQty()));
					if(!line.save())
						msg.append("Failed when trying adding OR update Product Accessories");
				}
			}
		}
		
		return msg.toString();
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		int AD_Org_ID = 0;
		if(getUNS_ProductAccessories_ID() < 0)
			return true;
		if(newRecord)
		{
			BigDecimal QtyParent = Env.ZERO;
			if(getUNS_PreOrder_Line_ID() > 0)
			{
				MUNSPreOrderLine ol = new MUNSPreOrderLine(getCtx(), getUNS_PreOrder_Line_ID(), get_TrxName());
//				QtyParent = DB.getSQLValueBD(get_TrxName(), "SELECT Qty FROM UNS_PreOrder_Line WHERE UNS_PreOrder_Line_ID=?",
//						getUNS_PreOrder_Line_ID());
				QtyParent = ol.getQty();
				AD_Org_ID = ol.getAD_Org_ID();
			}
			else if(getC_OrderLine_ID() > 0)
			{	
				QtyParent = getC_OrderLine().getQtyEntered();
				AD_Org_ID = getC_OrderLine().getAD_Org_ID();
			}
			else if(getC_InvoiceLine_ID() > 0)
			{
				QtyParent = getC_InvoiceLine().getQtyEntered();
				AD_Org_ID = getC_InvoiceLine().getAD_Org_ID();
			}
			
			MUNSProductAccessories pa = new MUNSProductAccessories(getCtx(), getUNS_ProductAccessories_ID(), get_TrxName());
			
			setAD_Org_ID(AD_Org_ID);
			setUNS_ProductAccessories_ID(pa.get_ID());
			setProductAccessories_ID(pa.getProductAccessories_ID());
			if(getQty().compareTo(Env.ZERO) == 0)
				setQty(QtyParent.multiply(pa.getQty()));
			setC_UOM_ID(pa.getC_UOM_ID());
			setAccessoriesType(pa.getAccessoriesType());
		}
		
		return true;
	}
	
	public boolean beforeDelete()
	{
		if(getAccessoriesType().equals(ACCESSORIESTYPE_Mandatory))
			throw new AdempiereUserError("Can't remove accessories with mandatory type");
		
		return true;
	}

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public MUNSAccessoriesLine copyFrom (MUNSAccessoriesLine from, String TableName, int Parent_ID)
	{
		MUNSAccessoriesLine to = new MUNSAccessoriesLine(from.getCtx(), 0, from.get_TrxName());
		PO.copyValues (from, to, from.getAD_Client_ID(), from.getAD_Org_ID());
		to.setC_InvoiceLine_ID(0);
		to.setC_OrderLine_ID(0);
		to.setUNS_PreOrder_Line_ID(0);
		to.setQty(from.getQty());
		to.set_ValueOfColumn(TableName+"_ID", Parent_ID);
		//		
		to.saveEx();
		
		return to;
	}
	
	public static MUNSAccessoriesLine get (Properties ctx, int UNS_ProductAccessories_ID, String TableName, int Parent_ID, String trxName)
	{
		MUNSAccessoriesLine line = null;
		String whereClause = "UNS_ProductAccessories_ID = ? AND " + TableName + "_ID=?";
		line = new Query(ctx, Table_Name, whereClause, trxName).setParameters(UNS_ProductAccessories_ID, Parent_ID)
				.first();
		
		return line;
	}
	
	public static String plTOInvoice(Properties ctx, int UNS_PackingList_Line_ID, int M_Product_ID, String trxName)
	{
		String m_Message = null;
		
		for(MUNSPackingListLine list : MUNSPackingListLine.getLines(ctx, UNS_PackingList_Line_ID, trxName))
		{
			MUNSAccessoriesLine line = new MUNSAccessoriesLine(list.getCtx(), 0, list.get_TrxName());
//			MUNSProductAccessories accessories = MUNSProductAccessories.getOfProduct(list.getCtx(), M_Product_ID,
//					list.getM_Product_ID(), list.get_TrxName());
			String sql = "SELECT UNS_ProductAccessories_ID FROM UNS_ProductAccessories WHERE M_Product_ID=?"
					+ " AND ProductAccessories_ID = ?";
			int idProductAccessories = DB.getSQLValue(trxName, sql, new Object[]{M_Product_ID, list.getM_Product_ID()});
			line.setAD_Org_ID(list.getAD_Org_ID());
			line.setUNS_ProductAccessories_ID(idProductAccessories);
			line.setProductAccessories_ID(list.getM_Product_ID());
			line.setQty(list.getQtyShipping());
			line.setC_InvoiceLine_ID(list.getC_InvoiceLine_ID());
			if(!line.save())
				m_Message = "Failed when create product accessories for invoice line";
		}
		
		return m_Message;
	}
	
	public static MUNSAccessoriesLine[] getLinesByParent(Properties ctx, String TableName, int Parent_ID, String trxName)
	{
		ArrayList<MUNSAccessoriesLine> list = new ArrayList<MUNSAccessoriesLine>();
		
		String sql = "SELECT UNS_AccessoriesLine_ID FROM UNS_AccessoriesLine "
				+ "WHERE " + TableName + "_ID=?";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, Parent_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add(new MUNSAccessoriesLine(ctx, rs.getInt(1),
						trxName));
			rs.close();
			pstmt.close();
			pstmt = null;
		} catch (SQLException ex) {
			throw new AdempiereException("Unable to load production details",
					ex);
		} finally {
			DB.close(rs, pstmt);
		}

		MUNSAccessoriesLine[] retValue = new MUNSAccessoriesLine[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	private boolean m_reversal = false;
	
	public boolean isReversal()
	{
		return m_reversal;
	}
	
	public void setReversal (boolean reverse)
	{
		m_reversal = reverse;
	}
}

//SUBEK00597900516