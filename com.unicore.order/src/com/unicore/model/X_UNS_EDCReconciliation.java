/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_EDCReconciliation
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EDCReconciliation extends PO implements I_UNS_EDCReconciliation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180907L;

    /** Standard Constructor */
    public X_UNS_EDCReconciliation (Properties ctx, int UNS_EDCReconciliation_ID, String trxName)
    {
      super (ctx, UNS_EDCReconciliation_ID, trxName);
      /** if (UNS_EDCReconciliation_ID == 0)
        {
			setPayableRefundAmt (Env.ZERO);
// 0
			setUNS_EDCReconciliation_ID (0);
			setUNS_SalesReconciliation_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EDCReconciliation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EDCReconciliation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Payable Refund Amount.
		@param PayableRefundAmt 
		The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt)
	{
		set_Value (COLUMNNAME_PayableRefundAmt, PayableRefundAmt);
	}

	/** Get Payable Refund Amount.
		@return The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Amount.
		@param TotalAmt 
		Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt)
	{
		set_Value (COLUMNNAME_TotalAmt, TotalAmt);
	}

	/** Get Total Amount.
		@return Total Amount
	  */
	public BigDecimal getTotalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Card Type.
		@param UNS_CardType_ID Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID)
	{
		if (UNS_CardType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, Integer.valueOf(UNS_CardType_ID));
	}

	/** Get Card Type.
		@return Card Type	  */
	public int getUNS_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDC Reconciliation.
		@param UNS_EDCReconciliation_ID EDC Reconciliation	  */
	public void setUNS_EDCReconciliation_ID (int UNS_EDCReconciliation_ID)
	{
		if (UNS_EDCReconciliation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCReconciliation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDCReconciliation_ID, Integer.valueOf(UNS_EDCReconciliation_ID));
	}

	/** Get EDC Reconciliation.
		@return EDC Reconciliation	  */
	public int getUNS_EDCReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EDCReconciliation_UU.
		@param UNS_EDCReconciliation_UU UNS_EDCReconciliation_UU	  */
	public void setUNS_EDCReconciliation_UU (String UNS_EDCReconciliation_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EDCReconciliation_UU, UNS_EDCReconciliation_UU);
	}

	/** Get UNS_EDCReconciliation_UU.
		@return UNS_EDCReconciliation_UU	  */
	public String getUNS_EDCReconciliation_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EDCReconciliation_UU);
	}

	public I_UNS_SalesReconciliation getUNS_SalesReconciliation() throws RuntimeException
    {
		return (I_UNS_SalesReconciliation)MTable.get(getCtx(), I_UNS_SalesReconciliation.Table_Name)
			.getPO(getUNS_SalesReconciliation_ID(), get_TrxName());	}

	/** Set Sales Reconciliation.
		@param UNS_SalesReconciliation_ID Sales Reconciliation	  */
	public void setUNS_SalesReconciliation_ID (int UNS_SalesReconciliation_ID)
	{
		if (UNS_SalesReconciliation_ID < 1) 
			set_Value (COLUMNNAME_UNS_SalesReconciliation_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_SalesReconciliation_ID, Integer.valueOf(UNS_SalesReconciliation_ID));
	}

	/** Get Sales Reconciliation.
		@return Sales Reconciliation	  */
	public int getUNS_SalesReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SalesReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}