/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MConversionRate;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSPaymentTrx extends X_UNS_PaymentTrx
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -263870242410043414L;
	private MUNSPOSPayment m_parent = null;
	
	public static final int REFERENCE_TRXTYPE_ID = 1000267;
	private MUNSPOSSession m_session = null;
	private MUNSCardTrxDetail[] m_lines = null;
	
	public MUNSPOSSession getSession ()
	{
		if (m_session == null)
			m_session = new MUNSPOSSession(getCtx(), getUNS_POS_Session_ID(), get_TrxName());
		return m_session;
	}
	
	public MUNSPOSPayment getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPOSPayment(getCtx(), getUNS_POSPayment_ID(), get_TrxName());
		
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if(getParent().isReplication() || isReplication())
			return true;
		
		BigDecimal trxAmt = getAmount();
		if (!isReceipt())
			trxAmt = trxAmt.negate();
		setTrxAmt(trxAmt);
		if (!validateRecord(false))
			return false;
		if (!PAYMENTMETHOD_Card.equals(getPaymentMethod()) && !PAYMENTMETHOD_Wechat.equals(getPaymentMethod()))
		{
			setIsReconciled(true);
		}
		if (!getTrxType().startsWith("E") && !getTrxType().startsWith("M") && !isProcessed())
		{
			Timestamp dateTrx = getParent().getDateTrx();
			if (getParent().isCreditMemo()&& getUNS_POS_Session_ID() == 
					getParent().getPOSTransaction().getOriginalTrx().getUNS_POS_Session_ID())
			{
//				if (PAYMENTMETHOD_Cash.equals(getPaymentMethod()))
//				{
//					String sql1 = "SELECT SUM(trxAmt) FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID = ("
//							+ " SELECT UNS_POSPayment_ID FROM UNS_POSPayment WHERE UNS_POSTrx_ID = ?) "
//							+ " AND PaymentMethod = '0' AND C_Currency_ID = ? AND IsReceipt = 'Y'";
//					String sql2 = "SELECT SUM (trxAmt) FROM UNS_PaymentTrx "
//							+ " WHERE UNS_POSPayment_ID IN (SELECT UNS_POSPayment_ID FROM UNS_POSPayment "
//							+ " WHERE UNS_POSTrx_ID IN (SELECT UNS_POSTrx_ID FROM UNS_POSTrx "
//							+ " WHERE Reference_ID = ?)) AND UNS_PaymentTrx_ID <> ? AND "
//							+ " PaymentMethod = '0' AND C_Currency_ID = ? AND IsReceipt = 'N'";
//					BigDecimal refundAble = DB.getSQLValueBD(
//							get_TrxName(), sql1, getParent().getPOSTransaction().getReference_ID(),
//							getC_Currency_ID());
//					if (refundAble == null)
//						refundAble = Env.ZERO;
//					BigDecimal paid = DB.getSQLValueBD(
//							get_TrxName(), sql2, getParent().getPOSTransaction().getReference_ID(), 
//							getUNS_PaymentTrx_ID(), getC_Currency_ID());
//					if (paid == null)
//						paid = Env.ZERO;
//					refundAble = refundAble.subtract(paid);
//					if (getTrxAmt().abs().compareTo(refundAble.abs()) == 1)
//					{
//						log.log(Level.SEVERE, "Trx amount greater than original trx amount");
//						return false;
//					}
//				}
				String sql = "SELECT DateTrx FROM UNS_POSPayment WHERE UNS_POSPayment_ID = ("
						+ " SELECT UNS_POSPayment_ID FROM UNS_POSPayment WHERE UNS_POSTrx_ID = ?)";
				dateTrx = DB.getSQLValueTS(get_TrxName(), sql, getParent().getPOSTransaction().getReference_ID());
				if  (dateTrx == null)
					dateTrx = getParent().getDateTrx();
			}
			
			BigDecimal convertedAmt = MConversionRate.convert(
					getCtx(), trxAmt, getC_Currency_ID(), 
					getSession().getC_Currency_ID(), dateTrx, 0, getAD_Client_ID(), 
					getAD_Org_ID());
			setConvertedAmt(convertedAmt);
		}
		
		if(getTrxType().equals(TRXTYPE_CapitalIn))
		{
			Timestamp dateTrx = getCreated();
			
			BigDecimal convertedAmt = MConversionRate.convert(
					getCtx(), trxAmt, getC_Currency_ID(), 
					getSession().getC_Currency_ID(), dateTrx, 0, getAD_Client_ID(), 
					getAD_Org_ID());
			setConvertedAmt(convertedAmt);
		}
		if ((TRXTYPE_Refund.equals(getTrxType()) && !getParent().getDateAcct().equals( 
				getParent().getPOSTransaction().getOriginalTrx().getDateAcct()))
				|| TRXTYPE_Change.equals(getTrxType()))
		{
			BigDecimal minAmt = MUNSPOSConfigurationCurr.getMinimumTrxAmt(
					getAD_Org_ID(), getC_Currency_ID(), get_TrxName());
			String desc = getParent().getDescription();
			if(desc == null)
				desc = "";
			if (minAmt.signum() == 1 && getAmount().abs().compareTo(minAmt) == -1
					&& !desc.contains("**") && !desc.contains("Import"))
			{
				log.log(Level.SEVERE, "Trx amount below than minimum trx amount");
				return false;
			}
		}
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (!success)
			return false;
		if(getParent().isReplication() || isReplication())
			return true;
		
		if (!updateHeader())
			return false;
		
		return super.afterSave(newRecord, success);
	}
	
	private boolean updateHeader ()
	{
		if (getTrxType().startsWith("E") || getTrxType().startsWith("M"))
			return true;
		
		MUNSPOSPayment parent = getParent();
		if(parent.get_ID() > 0)
		{
			String sql = "SELECT COALESCE (SUM (ConvertedAmt), 0) "
					+ " FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID = ?";
			BigDecimal tConvertedAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSPayment_ID());
			parent.setPaidAmt(tConvertedAmt);
			return parent.save();
		}
		return true;
	}
	
	/**
	 * @param ctx
	 * @param UNS_PaymentTrx_ID
	 * @param trxName
	 */
	public MUNSPaymentTrx(Properties ctx, int UNS_PaymentTrx_ID, String trxName) {
		super(ctx, UNS_PaymentTrx_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPaymentTrx(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	private boolean validateCashVoucher ()
	{
		if (isProcessed())
			return true;
		
//		MUNSVoucherCode vCode = MUNSVoucherCode.getCode(getCtx(), getTrxNo(), getC_Currency_ID(), get_TrxName());
//		if(vCode == null)
//		{
//			log.saveError("Invalid Voucher", "Invalid voucher code");
//			return false;
//		}
//		else if(!vCode.isValidVoucher(getParent().getDateTrx()))
//		{
//			log.saveError("Invalid Voucher", "Invalid voucher code");
//			return false;
//		}
//		
//		if(vCode.getUsedAmt().signum() > 0 &&
//				vCode.getUNS_VoucherBook().getType().equals(MUNSVoucherBook.TYPE_ValidForOneTransaction))
//		{
//			log.saveError("Error", "This voucher has been used.");
//			return false;
//		}
		
		return true;
	}
	
	
	
	public String processTransaction ()
	{
		if (PAYMENTMETHOD_Voucher.equals(getPaymentMethod()))
		{
			String msg = updateVoucherCode();
			if (msg != null)
				return msg;
		}
		return null;
	}
	
	
	private String updateVoucherCode()
	{	
		MUNSVoucherCode vCode = MUNSVoucherCode.getCode(getCtx(),getParent().getDateAcct(),
				getTrxNo(), getC_Currency_ID(), get_TrxName());
		if(vCode == null)
			return "Could not find voucher " + getTrxNo() + "-" + getC_Currency().getCurSymbol();
		
		if(vCode.getUnusedAmt().compareTo(getTrxAmt()) == -1) 
			return "Unused Amount should not be less than paid amount";
		
		if (!vCode.updateVoucherCode(false, getTrxAmt()))
			return "Failed when try to update balance of voucher";
		
		return null;
	}
	
	private boolean validateRecord (boolean onDocAction)
	{
		if (getTrxType().startsWith("E") || getTrxType().startsWith("M"))
			return true;
		
		String paymentMethod = getPaymentMethod();
		 
		if (PAYMENTMETHOD_Voucher.equals(paymentMethod))
		{
			if (Util.isEmpty(getTrxNo(), true))
			{
				log.saveError("SaveError", "Mandatory field Account No");
				return false;
			}
			
			return validateCashVoucher();
		}
		
		if (onDocAction && (PAYMENTMETHOD_Card.equals(paymentMethod)
				|| PAYMENTMETHOD_Wechat.equals(paymentMethod)))
		{
			MUNSCardTrxDetail trxDetail = MUNSCardTrxDetail.get(this);
			if (trxDetail == null)
			{
				log.saveError("SaveError", "Mandatory field EDC");
				return false;
			}
		}
		
		String sql = "SELECT DateTrx FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
		Timestamp trxDate = DB.getSQLValueTS(get_TrxName(), sql, getParent().getUNS_POSTrx_ID());
		if (getParent().getDateTrx().before(trxDate))
		{
			log.saveError("SaveError", "Payment date before transaction date");
			return false;
		}
		
		return true;
	}
	
	public boolean isTrxCash()
	{
		String type = getTrxType();
		
		return type != null && (type.equals(TRXTYPE_CashIn)
				|| type.equals(TRXTYPE_Change) || type.equals(TRXTYPE_ExchangeIn)
				|| type.equals(TRXTYPE_ExchangeOut));
	}
	
	public boolean isTrxEDC()
	{
		String type = getTrxType();
		
		return type != null && type.equals(TRXTYPE_EDCIn);
	}
	
	public MUNSPaymentTrx (MUNSPOSPayment parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		m_parent = parent;
		setClientOrg(parent);
		setUNS_POSPayment_ID(parent.get_ID());
	}
	
	public static MUNSPaymentTrx get(String trxName, int id)
	{
		return Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPaymentTrx.Table_Name, 
				MUNSPaymentTrx.Table_Name + "_ID = ?", trxName).setParameters(id).firstOnly();
	}
	
	@Override
	protected boolean afterDelete (boolean success)
	{
		if (!success)
			return false;
		if (!updateHeader())
			return false;
		return super.afterDelete(success);
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		m_lines = getLines(true);
		for(int i=0;i<m_lines.length; i++)
		{
			if(!m_lines[i].delete(true))
				return false;
		}
		
		return super.beforeDelete();
	}
	
	public boolean processExchange ()
	{
		
		return MUNSSessionCashAccount.update(
				get_TrxName(), getUNS_POS_Session_ID(), 
				getC_Currency_ID(), getTrxType(), 
				getTrxAmt(), true);
	}
	
	public MUNSCardTrxDetail[] getLines(boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSCardTrxDetail> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSCardTrxDetail.Table_Name,
				Table_Name+"_ID = ?", get_TrxName())
				.setParameters(get_ID())
				.list();
		m_lines = new MUNSCardTrxDetail[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}
}