/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_SessionEDCSummary
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_SessionEDCSummary 
{

    /** TableName=UNS_SessionEDCSummary */
    public static final String Table_Name = "UNS_SessionEDCSummary";

    /** AD_Table_ID=1000397 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name PayableRefundAmt */
    public static final String COLUMNNAME_PayableRefundAmt = "PayableRefundAmt";

	/** Set Payable Refund Amount.
	  * The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt);

	/** Get Payable Refund Amount.
	  * The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt();

    /** Column name PayableRefundAmtB1 */
    public static final String COLUMNNAME_PayableRefundAmtB1 = "PayableRefundAmtB1";

	/** Set Payable Refund Amount B1.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB1 (BigDecimal PayableRefundAmtB1);

	/** Get Payable Refund Amount B1.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB1();

    /** Column name PayableRefundAmtB2 */
    public static final String COLUMNNAME_PayableRefundAmtB2 = "PayableRefundAmtB2";

	/** Set Payable Refund Amount B2.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB2 (BigDecimal PayableRefundAmtB2);

	/** Get Payable Refund Amount B2.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB2();

    /** Column name TotalAmt */
    public static final String COLUMNNAME_TotalAmt = "TotalAmt";

	/** Set Total Amount.
	  * Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt);

	/** Get Total Amount.
	  * Total Amount
	  */
	public BigDecimal getTotalAmt();

    /** Column name TotalAmtB1 */
    public static final String COLUMNNAME_TotalAmtB1 = "TotalAmtB1";

	/** Set Total Amount B1	  */
	public void setTotalAmtB1 (BigDecimal TotalAmtB1);

	/** Get Total Amount B1	  */
	public BigDecimal getTotalAmtB1();

    /** Column name TotalAmtB2 */
    public static final String COLUMNNAME_TotalAmtB2 = "TotalAmtB2";

	/** Set Total Amount B2	  */
	public void setTotalAmtB2 (BigDecimal TotalAmtB2);

	/** Get Total Amount B2	  */
	public BigDecimal getTotalAmtB2();

    /** Column name UNS_CardType_ID */
    public static final String COLUMNNAME_UNS_CardType_ID = "UNS_CardType_ID";

	/** Set Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID);

	/** Get Card Type	  */
	public int getUNS_CardType_ID();

	public com.unicore.model.I_UNS_CardType getUNS_CardType() throws RuntimeException;

    /** Column name UNS_EDC_ID */
    public static final String COLUMNNAME_UNS_EDC_ID = "UNS_EDC_ID";

	/** Set EDC	  */
	public void setUNS_EDC_ID (int UNS_EDC_ID);

	/** Get EDC	  */
	public int getUNS_EDC_ID();

	public com.unicore.model.I_UNS_EDC getUNS_EDC() throws RuntimeException;

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException;

    /** Column name UNS_SessionEDCSummary_ID */
    public static final String COLUMNNAME_UNS_SessionEDCSummary_ID = "UNS_SessionEDCSummary_ID";

	/** Set EDC Session Summary	  */
	public void setUNS_SessionEDCSummary_ID (int UNS_SessionEDCSummary_ID);

	/** Get EDC Session Summary	  */
	public int getUNS_SessionEDCSummary_ID();

    /** Column name UNS_SessionEDCSummary_UU */
    public static final String COLUMNNAME_UNS_SessionEDCSummary_UU = "UNS_SessionEDCSummary_UU";

	/** Set UNS_SessionEDCSummary_UU	  */
	public void setUNS_SessionEDCSummary_UU (String UNS_SessionEDCSummary_UU);

	/** Get UNS_SessionEDCSummary_UU	  */
	public String getUNS_SessionEDCSummary_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
