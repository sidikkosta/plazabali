/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POSConfiguration_Curr
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSConfiguration_Curr extends PO implements I_UNS_POSConfiguration_Curr, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190422L;

    /** Standard Constructor */
    public X_UNS_POSConfiguration_Curr (Properties ctx, int UNS_POSConfiguration_Curr_ID, String trxName)
    {
      super (ctx, UNS_POSConfiguration_Curr_ID, trxName);
      /** if (UNS_POSConfiguration_Curr_ID == 0)
        {
			setC_Currency_ID (0);
			setIsUsedForChange (false);
// N
			setMinimumAmt (Env.ZERO);
// 0
			setUNS_POSConfiguration_Curr_ID (0);
			setUNS_POSConfiguration_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSConfiguration_Curr (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSConfiguration_Curr[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_ValidCombination getCash_Acct() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getCash_Acct_ID(), get_TrxName());	}

	/** Set Cash Account.
		@param Cash_Acct_ID Cash Account	  */
	public void setCash_Acct_ID (int Cash_Acct_ID)
	{
		if (Cash_Acct_ID < 1) 
			set_Value (COLUMNNAME_Cash_Acct_ID, null);
		else 
			set_Value (COLUMNNAME_Cash_Acct_ID, Integer.valueOf(Cash_Acct_ID));
	}

	/** Get Cash Account.
		@return Cash Account	  */
	public int getCash_Acct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Cash_Acct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankAccount)MTable.get(getCtx(), org.compiere.model.I_C_BankAccount.Table_Name)
			.getPO(getC_BankAccount_ID(), get_TrxName());	}

	/** Set Cash / Bank Account.
		@param C_BankAccount_ID 
		Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID)
	{
		if (C_BankAccount_ID < 1) 
			set_Value (COLUMNNAME_C_BankAccount_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankAccount_ID, Integer.valueOf(C_BankAccount_ID));
	}

	/** Get Cash / Bank Account.
		@return Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Used For Change ?.
		@param IsUsedForChange Used For Change ?	  */
	public void setIsUsedForChange (boolean IsUsedForChange)
	{
		set_Value (COLUMNNAME_IsUsedForChange, Boolean.valueOf(IsUsedForChange));
	}

	/** Get Used For Change ?.
		@return Used For Change ?	  */
	public boolean isUsedForChange () 
	{
		Object oo = get_Value(COLUMNNAME_IsUsedForChange);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Minimum Amt.
		@param MinimumAmt 
		Minimum Amount in Document Currency
	  */
	public void setMinimumAmt (BigDecimal MinimumAmt)
	{
		set_Value (COLUMNNAME_MinimumAmt, MinimumAmt);
	}

	/** Get Minimum Amt.
		@return Minimum Amount in Document Currency
	  */
	public BigDecimal getMinimumAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinimumAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Currency Configuration.
		@param UNS_POSConfiguration_Curr_ID Currency Configuration	  */
	public void setUNS_POSConfiguration_Curr_ID (int UNS_POSConfiguration_Curr_ID)
	{
		if (UNS_POSConfiguration_Curr_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_Curr_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_Curr_ID, Integer.valueOf(UNS_POSConfiguration_Curr_ID));
	}

	/** Get Currency Configuration.
		@return Currency Configuration	  */
	public int getUNS_POSConfiguration_Curr_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSConfiguration_Curr_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSConfiguration_Curr_UU.
		@param UNS_POSConfiguration_Curr_UU UNS_POSConfiguration_Curr_UU	  */
	public void setUNS_POSConfiguration_Curr_UU (String UNS_POSConfiguration_Curr_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_Curr_UU, UNS_POSConfiguration_Curr_UU);
	}

	/** Get UNS_POSConfiguration_Curr_UU.
		@return UNS_POSConfiguration_Curr_UU	  */
	public String getUNS_POSConfiguration_Curr_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSConfiguration_Curr_UU);
	}

	public com.unicore.model.I_UNS_POSConfiguration getUNS_POSConfiguration() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSConfiguration)MTable.get(getCtx(), com.unicore.model.I_UNS_POSConfiguration.Table_Name)
			.getPO(getUNS_POSConfiguration_ID(), get_TrxName());	}

	/** Set Global POS Configuration.
		@param UNS_POSConfiguration_ID Global POS Configuration	  */
	public void setUNS_POSConfiguration_ID (int UNS_POSConfiguration_ID)
	{
		if (UNS_POSConfiguration_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_ID, Integer.valueOf(UNS_POSConfiguration_ID));
	}

	/** Get Global POS Configuration.
		@return Global POS Configuration	  */
	public int getUNS_POSConfiguration_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSConfiguration_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}