/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POSTrxLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSTrxLine extends PO implements I_UNS_POSTrxLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181024L;

    /** Standard Constructor */
    public X_UNS_POSTrxLine (Properties ctx, int UNS_POSTrxLine_ID, String trxName)
    {
      super (ctx, UNS_POSTrxLine_ID, trxName);
      /** if (UNS_POSTrxLine_ID == 0)
        {
			setC_UOM_ID (0);
// 0
			setIsBOM (false);
// N
			setisProductBonuses (false);
// N
			setLineAmt (Env.ZERO);
// 0
			setLineNetAmt (Env.ZERO);
// 0
			setM_Product_ID (0);
			setPriceActual (Env.ZERO);
// 0
			setPriceLimit (Env.ZERO);
// 0
			setPriceList (Env.ZERO);
// 0
			setProcessed (false);
			setQtyBonuses (Env.ZERO);
// 0
			setQtyEntered (Env.ZERO);
			setQtyOrdered (Env.ZERO);
// 0
			setServiceCharge (Env.ZERO);
// 0
			setUNS_POSTrx_ID (0);
			setUNS_POSTrxLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSTrxLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSTrxLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Barcode.
		@param Barcode Barcode	  */
	public void setBarcode (String Barcode)
	{
		set_Value (COLUMNNAME_Barcode, Barcode);
	}

	/** Get Barcode.
		@return Barcode	  */
	public String getBarcode () 
	{
		return (String)get_Value(COLUMNNAME_Barcode);
	}

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException
    {
		return (org.compiere.model.I_C_Tax)MTable.get(getCtx(), org.compiere.model.I_C_Tax.Table_Name)
			.getPO(getC_Tax_ID(), get_TrxName());	}

	/** Set Tax.
		@param C_Tax_ID 
		Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID)
	{
		if (C_Tax_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Tax_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Tax_ID, Integer.valueOf(C_Tax_ID));
	}

	/** Get Tax.
		@return Tax identifier
	  */
	public int getC_Tax_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Tax_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Discount Note.
		@param DiscNote Discount Note	  */
	public void setDiscNote (String DiscNote)
	{
		set_Value (COLUMNNAME_DiscNote, DiscNote);
	}

	/** Get Discount Note.
		@return Discount Note	  */
	public String getDiscNote () 
	{
		return (String)get_Value(COLUMNNAME_DiscNote);
	}

	/** Set Discount %.
		@param Discount 
		Discount in percent
	  */
	public void setDiscount (BigDecimal Discount)
	{
		set_Value (COLUMNNAME_Discount, Discount);
	}

	/** Get Discount %.
		@return Discount in percent
	  */
	public BigDecimal getDiscount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Discount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Amount.
		@param DiscountAmt 
		Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt)
	{
		set_Value (COLUMNNAME_DiscountAmt, DiscountAmt);
	}

	/** Get Discount Amount.
		@return Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DiscountAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Refference.
		@param DiscReff Discount Refference	  */
	public void setDiscReff (String DiscReff)
	{
		set_Value (COLUMNNAME_DiscReff, DiscReff);
	}

	/** Get Discount Refference.
		@return Discount Refference	  */
	public String getDiscReff () 
	{
		return (String)get_Value(COLUMNNAME_DiscReff);
	}

	/** Set ETBB Code.
		@param ETBBCode ETBB Code	  */
	public void setETBBCode (String ETBBCode)
	{
		set_Value (COLUMNNAME_ETBBCode, ETBBCode);
	}

	/** Get ETBB Code.
		@return ETBB Code	  */
	public String getETBBCode () 
	{
		return (String)get_Value(COLUMNNAME_ETBBCode);
	}

	/** Set Bill of Materials.
		@param IsBOM 
		Bill of Materials
	  */
	public void setIsBOM (boolean IsBOM)
	{
		set_ValueNoCheck (COLUMNNAME_IsBOM, Boolean.valueOf(IsBOM));
	}

	/** Get Bill of Materials.
		@return Bill of Materials
	  */
	public boolean isBOM () 
	{
		Object oo = get_Value(COLUMNNAME_IsBOM);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Product Bonuses.
		@param isProductBonuses 
		Product Bonuses
	  */
	public void setisProductBonuses (boolean isProductBonuses)
	{
		set_Value (COLUMNNAME_isProductBonuses, Boolean.valueOf(isProductBonuses));
	}

	/** Get Product Bonuses.
		@return Product Bonuses
	  */
	public boolean isProductBonuses () 
	{
		Object oo = get_Value(COLUMNNAME_isProductBonuses);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line Amount.
		@param LineAmt 
		The amount for this line, generally it is after discount amount.
	  */
	public void setLineAmt (BigDecimal LineAmt)
	{
		set_Value (COLUMNNAME_LineAmt, LineAmt);
	}

	/** Get Line Amount.
		@return The amount for this line, generally it is after discount amount.
	  */
	public BigDecimal getLineAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LineAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Line Amount.
		@param LineNetAmt 
		Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public void setLineNetAmt (BigDecimal LineNetAmt)
	{
		set_Value (COLUMNNAME_LineNetAmt, LineNetAmt);
	}

	/** Get Line Amount.
		@return Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public BigDecimal getLineNetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LineNetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Unit Price.
		@param PriceActual 
		Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual)
	{
		set_ValueNoCheck (COLUMNNAME_PriceActual, PriceActual);
	}

	/** Get Unit Price.
		@return Actual Price
	  */
	public BigDecimal getPriceActual () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceActual);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Limit Price.
		@param PriceLimit 
		Lowest price for a product
	  */
	public void setPriceLimit (BigDecimal PriceLimit)
	{
		set_Value (COLUMNNAME_PriceLimit, PriceLimit);
	}

	/** Get Limit Price.
		@return Lowest price for a product
	  */
	public BigDecimal getPriceLimit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceLimit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set List Price.
		@param PriceList 
		List Price
	  */
	public void setPriceList (BigDecimal PriceList)
	{
		set_Value (COLUMNNAME_PriceList, PriceList);
	}

	/** Get List Price.
		@return List Price
	  */
	public BigDecimal getPriceList () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceList);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Bonuses Qty.
		@param QtyBonuses 
		Bonuses Qty
	  */
	public void setQtyBonuses (BigDecimal QtyBonuses)
	{
		set_Value (COLUMNNAME_QtyBonuses, QtyBonuses);
	}

	/** Get Bonuses Qty.
		@return Bonuses Qty
	  */
	public BigDecimal getQtyBonuses () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyBonuses);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param QtyEntered 
		The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered)
	{
		set_ValueNoCheck (COLUMNNAME_QtyEntered, QtyEntered);
	}

	/** Get Quantity.
		@return The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ordered Quantity.
		@param QtyOrdered 
		Ordered Quantity
	  */
	public void setQtyOrdered (BigDecimal QtyOrdered)
	{
		set_ValueNoCheck (COLUMNNAME_QtyOrdered, QtyOrdered);
	}

	/** Get Ordered Quantity.
		@return Ordered Quantity
	  */
	public BigDecimal getQtyOrdered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyOrdered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_POSTrxLine getReference() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTrxLine)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTrxLine.Table_Name)
			.getPO(getReference_ID(), get_TrxName());	}

	/** Set Refrerence Record.
		@param Reference_ID Refrerence Record	  */
	public void setReference_ID (int Reference_ID)
	{
		if (Reference_ID < 1) 
			set_Value (COLUMNNAME_Reference_ID, null);
		else 
			set_Value (COLUMNNAME_Reference_ID, Integer.valueOf(Reference_ID));
	}

	/** Get Refrerence Record.
		@return Refrerence Record	  */
	public int getReference_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Reference_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Service Charge.
		@param ServiceCharge Service Charge	  */
	public void setServiceCharge (BigDecimal ServiceCharge)
	{
		set_Value (COLUMNNAME_ServiceCharge, ServiceCharge);
	}

	/** Get Service Charge.
		@return Service Charge	  */
	public BigDecimal getServiceCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ServiceCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set SKU.
		@param SKU 
		Stock Keeping Unit
	  */
	public void setSKU (String SKU)
	{
		set_ValueNoCheck (COLUMNNAME_SKU, SKU);
	}

	/** Get SKU.
		@return Stock Keeping Unit
	  */
	public String getSKU () 
	{
		return (String)get_Value(COLUMNNAME_SKU);
	}

	/** Set Tax Amount.
		@param TaxAmt 
		Tax Amount for a document
	  */
	public void setTaxAmt (BigDecimal TaxAmt)
	{
		set_ValueNoCheck (COLUMNNAME_TaxAmt, TaxAmt);
	}

	/** Get Tax Amount.
		@return Tax Amount for a document
	  */
	public BigDecimal getTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set POS Recapitulation Line.
		@param UNS_POSRecapLine_ID POS Recapitulation Line	  */
	public void setUNS_POSRecapLine_ID (int UNS_POSRecapLine_ID)
	{
		if (UNS_POSRecapLine_ID < 1) 
			set_Value (COLUMNNAME_UNS_POSRecapLine_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_POSRecapLine_ID, Integer.valueOf(UNS_POSRecapLine_ID));
	}

	/** Get POS Recapitulation Line.
		@return POS Recapitulation Line	  */
	public int getUNS_POSRecapLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSRecapLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POSTrx getUNS_POSTrx() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTrx)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTrx.Table_Name)
			.getPO(getUNS_POSTrx_ID(), get_TrxName());	}

	/** Set POS Transactions.
		@param UNS_POSTrx_ID POS Transactions	  */
	public void setUNS_POSTrx_ID (int UNS_POSTrx_ID)
	{
		if (UNS_POSTrx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_ID, Integer.valueOf(UNS_POSTrx_ID));
	}

	/** Get POS Transactions.
		@return POS Transactions	  */
	public int getUNS_POSTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set POS Trx Line.
		@param UNS_POSTrxLine_ID POS Trx Line	  */
	public void setUNS_POSTrxLine_ID (int UNS_POSTrxLine_ID)
	{
		if (UNS_POSTrxLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrxLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrxLine_ID, Integer.valueOf(UNS_POSTrxLine_ID));
	}

	/** Get POS Trx Line.
		@return POS Trx Line	  */
	public int getUNS_POSTrxLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTrxLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSTrxLine_UU.
		@param UNS_POSTrxLine_UU UNS_POSTrxLine_UU	  */
	public void setUNS_POSTrxLine_UU (String UNS_POSTrxLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSTrxLine_UU, UNS_POSTrxLine_UU);
	}

	/** Get UNS_POSTrxLine_UU.
		@return UNS_POSTrxLine_UU	  */
	public String getUNS_POSTrxLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSTrxLine_UU);
	}

	public com.unicore.model.I_UNS_POSTrxLine getVoidLine() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTrxLine)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTrxLine.Table_Name)
			.getPO(getVoidLine_ID(), get_TrxName());	}

	/** Set Voided Line.
		@param VoidLine_ID 
		Voided Line
	  */
	public void setVoidLine_ID (int VoidLine_ID)
	{
		if (VoidLine_ID < 1) 
			set_Value (COLUMNNAME_VoidLine_ID, null);
		else 
			set_Value (COLUMNNAME_VoidLine_ID, Integer.valueOf(VoidLine_ID));
	}

	/** Get Voided Line.
		@return Voided Line
	  */
	public int getVoidLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_VoidLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}