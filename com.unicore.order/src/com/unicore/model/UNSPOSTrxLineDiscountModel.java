package com.unicore.model;

import java.math.BigDecimal;
import org.compiere.model.PO;
import com.uns.model.MProduct;

public class UNSPOSTrxLineDiscountModel implements I_DiscountModelLine {
	
	private MUNSPOSTrxLine m_model = null;
	private MProduct m_product = null;

	public UNSPOSTrxLineDiscountModel (MUNSPOSTrxLine po)
	{
		setModel(po);
	}

	@Override
	public void setModel(PO po) {
		m_model = (MUNSPOSTrxLine) po;
		setProduct(new MProduct(getModel().getCtx(), getModel().getM_Product_ID()
				, getModel().get_TrxName()));
	}

	@Override
	public MUNSPOSTrxLine getModel() {
		return this.m_model;
	}

	@Override
	public MProduct getProduct() {
		return this.m_product;
	}

	@Override
	public void setProduct(MProduct proruct) {
		this.m_product = proruct;
	}

	@Override
	public void setDiscountAmt(BigDecimal discountAmt) {
		getModel().setDiscountAmt(discountAmt);
	}

	@Override
	public BigDecimal getDiscountAmt() {
		return getModel().getDiscountAmt();
	}

	@Override
	public void setDiscount(BigDecimal discount) {
		getModel().setDiscount(discount);
	}

	@Override
	public BigDecimal getDiscount() {
		return getModel().getDiscount();
	}

	@Override
	public void setPrice(BigDecimal price) {
		getModel().setPriceActual(price);
	}

	@Override
	public void setPrice() {
		getModel().setPrice();
	}

	@Override
	public void setHeaderInfo(PO header) {
		getModel().setHeaderInfo((MUNSPOSTrx)header);
	}

	@Override
	public void setRefLine_ID(int ref_ID) {
		getModel().set_ValueOfColumn("Reference_ID", ref_ID);
	}

	@Override
	public boolean isProductBonuses() {
		return getModel().isProductBonuses();
	}

	@Override
	public void setIsProductBonus(boolean isDiscountBonus) {
		getModel().setisProductBonuses(isDiscountBonus);
	}

	@Override
	public BigDecimal getPriceList() {
		return getModel().getPriceList();
	}

	@Override
	public BigDecimal getPriceEntered() {
		return getModel().getPriceActual();
	}

	@Override
	public BigDecimal getPriceActual() {
		return getModel().getPriceActual();
	}

	@Override
	public void setPriceEntered(BigDecimal priceEntered) {
		getModel().setPriceActual(priceEntered);
	}

	@Override
	public void setPriceActual(BigDecimal priceActual) {
		getModel().setPriceActual(priceActual);
	}

	@Override
	public void setQtyMerge(BigDecimal qtyMerge) {
		getModel().setQtyOrdered(qtyMerge);
	}

	@Override
	public BigDecimal getQtyMerge() {
		return getModel().getQtyOrdered();
	}

	@Override
	public BigDecimal getQtyEntered() {
		return getModel().getQtyEntered();
	}

	@Override
	public void setQtyEntered(BigDecimal qtyEntered) {
		getModel().setQtyEntered(qtyEntered);
	}

	@Override
	public BigDecimal getQtyBonuses() {
		return getModel().getQtyBonuses();
	}

	@Override
	public void setQtyBonuses(BigDecimal qtyBonuses) {
		getModel().setQtyBonuses(qtyBonuses);
	}

	@Override
	public void setLineNetAmt(BigDecimal lineNetAmt) {
		getModel().setLineNetAmt(lineNetAmt);
	}

	@Override
	public BigDecimal getLineNetAmt() {
		return getModel().getLineNetAmt();
	}

	@Override
	public int getPrecision() {
		return 2;
	}

	@Override
	public void setQty(BigDecimal qty) {
		getModel().setQtyEntered(qty);
	}

	@Override
	public void setPriceList(BigDecimal priceList) {
		getModel().setPriceList(priceList);

	}

	@Override
	public void setPriceLimit(BigDecimal priceLimit) {
		getModel().setPriceLimit(priceLimit);
	}

	@Override
	public BigDecimal getPriceLimit() {
		return getModel().getPriceLimit();
	}

	@Override
	public void setLineNetAmt() {
		getModel().setLineNetAmt();
	}

	@Override
	public void setPriceCost(BigDecimal priceCOst) {
		getModel().setPriceActual(priceCOst);
	}

	@Override
	public BigDecimal getPriceCost() {
		return getModel().getPriceActual();
	}

	@Override
	public int getRefLine_ID() {
		return getModel().get_ValueAsInt("Reference_ID");
	}

	@Override
	public int getLinkedLine_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

}
