package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MConversionRate;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.ErrorMsg;
import com.uns.util.MessageBox;
import com.uns.util.UNSApps;

/**
 * 
 * @author Guntur
 *
 */
public class MUNSPOSSession extends X_UNS_POS_Session implements DocAction,
		DocOptions {

	/**
	 * 
	 */
	
	private String m_processMsg = null;
//	private boolean m_justPrepared = false;
	private static final long serialVersionUID = 4730731491593825981L;
	private MUNSPOSTrx[] m_lines = null;
	private MUNSSessionCashAccount[] m_accounts = null;
	private MUNSSessionEDCSummary[] m_edcs = null;
	private MUNSCashierTandem[] m_tandems = null;
	private MUNSPOSTerminal m_terminal = null;
	
	public MUNSSessionCashAccount[] getCashAcounts (boolean refresh)
	{
		if (m_accounts != null && !refresh)
		{
			set_TrxName(m_accounts, get_TrxName());
			return m_accounts;
		}
		
		List<MUNSSessionCashAccount> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSSessionCashAccount.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).list();
		m_accounts = new MUNSSessionCashAccount[list.size()];
		list.toArray(m_accounts);
		
		return m_accounts;
	}
	
	public MUNSSessionEDCSummary[] getEDCSummary (boolean refresh)
	{
		if(m_edcs != null && !refresh)
		{
			set_TrxName(m_edcs, get_TrxName());
			return m_edcs;
		}
		
		List<MUNSSessionEDCSummary> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSSessionEDCSummary.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).list();
		m_edcs = new MUNSSessionEDCSummary[list.size()];
		list.toArray(m_edcs);
		
		return m_edcs;
	}
	
	public MUNSPOSTrx[] getLines (boolean requery)
	{
		if (null != m_lines && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSPOSTrx> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSPOSTrx.Table_Name, "UNS_POS_Session_ID = ?", 
				get_TrxName()).setParameters(get_ID()).list();
		
		m_lines = new MUNSPOSTrx[list.size()];
		list.toArray(m_lines);
		return m_lines;
	}

	public MUNSPOSSession(Properties ctx, int UNS_POS_Session_ID, String trxName) {
		super(ctx, UNS_POS_Session_ID, trxName);
	}

	public MUNSPOSSession(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
	
		if (DOCSTATUS_Drafted.equals(docStatus))
		{
			options[0] = DOCACTION_Prepare;
			index = 1;
		}
		if(DOCSTATUS_Invalid.equals(docStatus))
		{
			options[index++] = DOCACTION_Prepare;
		}
		
		return index;
	}

	
	@Override
	public boolean processIt(String action) throws Exception 
	{
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() 
	{
		addLog(Level.INFO, "Unlock it");
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		addLog(Level.INFO, "Invalidate it");
		setDocAction(DOCACTION_Prepare);
		return true;
	}
	
	@Override
	public String prepareIt() {
		addLog(Level.INFO, "Prepare It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		if(!isReplication())
		{
			MUNSCashierTandem tandem = MUNSCashierTandem.get(get_TrxName(), get_ID(), getCashier_ID());
			if (tandem == null)
			{
				tandem = new MUNSCashierTandem(this);
				tandem.setCashier_ID(getCashier_ID());
				tandem.saveEx();
			}
		}
		if(getDateOpened() == null)
			setDateOpened(new Timestamp (System.currentTimeMillis()));
		
//		m_justPrepared = true;
		setProcessed(true);		
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		addLog(Level.INFO, "Approve It");
		setIsApproved (true);
		setProcessed(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		addLog(Level.INFO, "Reject It");
		setIsApproved (false);
		setProcessed(false);
		return true;
	}
	
	private String returnCapital ()
	{
		String sql = "SELECT C_Currency_ID, SUM (TrxAmt) FROM UNS_PaymentTrx WHERE TrxType = ? "
				+ " AND PaymentMethod = ? AND UNS_POS_Session_ID = ? GROUP BY C_Currency_ID";
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setString(1, MUNSPaymentTrx.TRXTYPE_CapitalIn);
			st.setString(2, MUNSPaymentTrx.PAYMENTMETHOD_Cash);
			st.setInt(3, getUNS_POS_Session_ID());
			rs = st.executeQuery();
			while (rs.next())
			{
				int currencyID = rs.getInt(1);
				BigDecimal amt = rs.getBigDecimal(2);
				if (amt == null || amt.signum() == 0)
					continue;
				amt = amt.negate();
				MUNSPaymentTrx trx = new MUNSPaymentTrx(getCtx(), 0, get_TrxName());
				trx.setAD_Org_ID(getAD_Org_ID());
				trx.setAmount(amt);
				trx.setC_Currency_ID(currencyID);
				trx.setConvertedAmt(amt);
				trx.setIsReceipt(true);
				trx.setIsReconciled(true);
				trx.setPaymentMethod(MUNSPaymentTrx.PAYMENTMETHOD_Cash);
				trx.setTrxType(MUNSPaymentTrx.TRXTYPE_CapitalIn);
				trx.setProcessed(true);
				trx.setUNS_POS_Session_ID(getUNS_POS_Session_ID());
				trx.saveEx();
				trx.processExchange();
			}
		}
		catch (SQLException ex)
		{
			return ex.getMessage();
		}
		catch (AdempiereException ex)
		{
			return ex.getMessage();
		}
		finally
		{
			DB.close(rs, st);
		}
		return null;
	}

	@Override
	public String completeIt() {
		
//		if(!m_justPrepared)
//		{
//			String status = prepareIt();
//			if(!DocAction.STATUS_InProgress.equals(status))
//				return status;
//		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		boolean autoReturnCap = MSysConfig.getBooleanValue("AUTO_RETURN_ADD_CAPITAL", true, getAD_Client_ID());
		if (autoReturnCap && UNSApps.SERVER_MAIN.equals(UNSApps.SERVER_POS))
		{
			m_processMsg = returnCapital();
			if (m_processMsg != null)
				return DOCSTATUS_Invalid;
			load(get_TrxName());
		}
		
		int loggedInRole_ID = Env.getAD_Role_ID(getCtx());
		int spvRole_ID = MSysConfig.getIntValue("ROLE_POS_SPV", 1000212, getAD_Client_ID());
		
		String desc = getDescription();
		if (desc == null)
			desc = "";
		if (!isReplication() && loggedInRole_ID != spvRole_ID
				&& !desc.contains("**") && !desc.contains("Import"))
		{
			m_processMsg = "POS Session can only completed by Supervisor User.";
			return DOCSTATUS_Invalid;
		}
		
		if(!isReplication() && getSupervisor_ID() <= 0)
		{
			setSupervisor_ID(Env.getAD_User_ID(getCtx()));
		}
		
		if (getSupervisor_ID() == 0)
		{
			m_processMsg = "Mandatory column Supervisor";
			return DOCSTATUS_InProgress;
		}
		
		if (!UNSApps.SERVER_POS.equals(UNSApps.SERVER_TYPE))
		{
			List<MUNSPOSTrx> trxs = Query.get(
					getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPOSTrx.Table_Name, "IsPaid=? AND UNS_POS_Session_ID = ?"
							+ " AND DocStatus IN ('CO', 'CL')", 
					get_TrxName()).setParameters("N", get_ID()).list();
			for (int i=0; i<trxs.size(); i++)
			{
				if (!fixCancelReplication(trxs.get(i)))
				{
					m_processMsg = "Could not remove canceled replication";
					return DOCSTATUS_Invalid;
				}
			}
			load(get_TrxName());
		}

		
		if (DOCACTION_Prepare.equals(getDocAction()))
		{
			return DOCSTATUS_InProgress;
		}
		
		if(!isReplication())
		{
			MUNSSessionCashAccount[] accounts = getCashAcounts(false);
			for (int i=0; i<accounts.length; i++)
			{
				BigDecimal cashDepositAmt = accounts[i].getEndingBalance().subtract(accounts[i].getBeginningBalance());
				accounts[i].setCashDepositAmt(cashDepositAmt);
				accounts[i].setEndingBalance(accounts[i].getBeginningBalance());
				accounts[i].setRecalcEndingBalance(false);
				if (!accounts[i].save())
				{
					m_processMsg = CLogger.retrieveErrorString("Failed when try to update cash account");
					return DOCSTATUS_Invalid;
				}
			}
		}
		if(getDateClosed() == null)
			setDateClosed(new Timestamp (System.currentTimeMillis()));
		
		m_processMsg = deleteNotCompletedPOSTrx();
		if(null != m_processMsg)
		{
			if(!m_processMsg.equals(DOCSTATUS_InProgress))
				return DOCSTATUS_Invalid;
			else
			{
				m_processMsg = "Process has been cancelled.";
				return DOCSTATUS_InProgress;
			}
		}
				
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(isMainServer())
		{
			if(!isTrialMode() && !isReconciled())
			{	
				m_processMsg = MUNSCashReconciliation.createFrom(this, false);
				if(m_processMsg != null)
					return DocAction.STATUS_Invalid;
				m_processMsg = MUNSEDCReconciliation.createFrom(this, false);
				if(m_processMsg != null)
					return DocAction.STATUS_Invalid;
				m_processMsg = MUNSPOSRecap.createFrom(this);
				if(m_processMsg != null)
					return DocAction.STATUS_Invalid;
				setIsReconciled(true);
			}
		}
		
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt()
	{
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		int loggedInRole_ID = Env.getAD_Role_ID(getCtx());
		int spvRole_ID = MSysConfig.getIntValue("ROLE_POS_SPV", 1000212, getAD_Client_ID());
		
		if (!isReplication() && loggedInRole_ID != spvRole_ID)
		{
			m_processMsg = "POS Session can only completed by Supervisor User.";
			return false;
		}
		
		getLines(false);
		for (int i=0; i<m_lines.length; i++)
		{
			if (DOCSTATUS_Completed.equals(m_lines[i].getDocStatus()) ||
					DOCSTATUS_Closed.equals(m_lines[i].getDocStatus()))
			{
				m_processMsg = "Document Completed / Closed @POS Transaction " + m_lines[i].getDocumentNo() + "@";
				return false;
			}
		}
			
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		setDocStatus(DOCSTATUS_Voided);
		return true;
	}

	@Override
	public boolean closeIt() {
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if(m_processMsg != null)
			return false;
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
	
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		
		return false;
	}

	@Override
	public boolean reActivateIt() {
		
		return false;
	}

	@Override
	public String getSummary() {
		
		return null;
	}

	@Override
	public String getDocumentInfo() {
		
		return null;
	}

	@Override
	public File createPDF() {
		
		return null;
	}

	@Override
	public String getProcessMsg() {
		
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() 
	{	
		return getTotalSalesB1();
	}


	/**
	 * 
	 * @param ctx
	 * @param trxName
	 * @param AD_Org_ID
	 * @param salesRep_ID
	 * @return
	 */
	public static MUNSPOSSession get (Properties ctx, String trxName, 
			int AD_Org_ID, int salesRep_ID)
	{
		String whereClause = "AD_Org_ID = ? AND (Cashier_ID = ? OR EXISTS (SELECT * FROM UNS_CashierTandem WHERE Cashier_ID = ? AND "
				+ " UNS_POS_Session_ID = UNS_POS_Session.UNS_POS_SEssion_ID))"
				+ " AND DocStatus IN (?) ";
		return Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				whereClause, trxName).setParameters(AD_Org_ID, 
						salesRep_ID, salesRep_ID, "IP").first();
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{	
		Timestamp now = Timestamp.valueOf(new Timestamp(System.currentTimeMillis()).toString().substring(0,10)+" 00:00:00");
		
		if(getDateDoc().after(now))
		{
			ErrorMsg.setErrorMsg(getCtx(), "POS Session Date", "Document Date dissallowed greater than Date Now");
			return false;
		}
		
		String desc = getDescription();
		if(desc == null)
			desc = "";
		if((desc.contains("**") || desc.contains("Import")) && !desc.contains("RESEND"))
			setDescription(desc.concat("-RESEND"));
			
		
		if (newRecord && !isReplication())
		{
			MUNSPOSConfiguration config = MUNSPOSConfiguration.getActive(get_TrxName(), getAD_Org_ID());
			setBase1Currency_ID(config.getBase1Currency_ID());
			setBase2Currency_ID(config.getBase2Currency_ID());
			String[] macs = getMACS();
			int terminalID = -1;
			for (int i=0; i<macs.length && terminalID <= 0; i++)
			{
				terminalID = MUNSPOSTerminal.getIDByMAC(get_TrxName(), macs[i]);
				if (terminalID > 0)
					break;
			}
			//if date open is not null maybe user run copy record action, set date open and date closed to null
			setDateOpened(null);
			setDateClosed(null);
			
			if (terminalID <= 0)
			{
				log.saveError("Error", "Could not find POS Terminal for device");
				return false;
			}
			setUNS_POSTerminal_ID(terminalID);
			
			String sqql = "SELECT 1 FROM C_BPartner_Location WHERE "
					+ " C_BPartner_ID = (SELECT Store_ID FROM UNS_POSTerminal WHERE UNS_POSTerminal_ID = ?)"
					+ " AND IsActive = ?";
			boolean hasLocation = DB.getSQLValue(get_TrxName(), sqql, 
					terminalID, "Y") > 0;
			if(!hasLocation)
			{
				ErrorMsg.setErrorMsg(getCtx(), "Store Location", "Store has not been set location");
				return false;
			}
			
			String sql = "SELECT M_PriceList_ID FROM C_BPartner WHERE C_BPartner_ID ="
					+ " (SELECT Store_ID FROM UNS_POSTerminal WHERE UNS_POSTerminal_ID = ?)";
			int priceListID = DB.getSQLValue(get_TrxName(), sql, terminalID);
			if(priceListID <=0)
			{
				log.saveError("Error", "Could not find Price List for pos " + getUNS_POSTerminal().getName());
				return false;
			}
			setM_PriceList_ID(priceListID);
			
			if (!desc.contains("**") && !desc.contains("Import"))
			{
				//setShift
				String newShift = MUNSPOSSession.SHIFT_Shift1;
				String sqll = "SELECT Shift FROM UNS_POS_Session WHERE UNS_POS_Session_ID <> ?"
						+ " AND UNS_POSTerminal_ID = ? AND DocStatus IN ('CO','CL') AND DateAcct = ?"
						+ " ORDER BY Created DESC";
				String prevShift = DB.getSQLValueString(
						get_TrxName(), sqll, getUNS_POS_Session_ID(), getUNS_POSTerminal_ID(), getDateAcct());
				if(prevShift != null)
				{
					int intShift = new Integer(prevShift);
					newShift = new DecimalFormat("00").format(intShift+1);
				}
				
				setShift(newShift);
			}
		}
		
		if ((newRecord || is_ValueChanged(COLUMNNAME_TotalSalesB1)) && !isReplication())
		{
			BigDecimal b2 = MConversionRate.convert(
					getCtx(), getTotalSalesB1(), getBase1Currency_ID(), 
					getBase2Currency_ID(), getAD_Client_ID(), 
					getAD_Org_ID());
			setTotalSalesB2(b2);
		}
		if (!isProcessed() && !isReplication())
		{
			String sql = "SELECT DocumentNo FROM UNS_POS_Session WHERE ( "
					+ " Cashier_ID = ? OR UNS_POSTerminal_ID = ?) AND "
					+ " DocStatus IN (?,?,?) AND UNS_POS_Session_ID <> ?";
			
			String docNo = DB.getSQLValueString(get_TrxName(), sql, 
					getCashier_ID(),getUNS_POSTerminal_ID(), "IN","IP","DR",
					getUNS_POS_Session_ID());
			if (null != docNo)
			{
				ErrorMsg.setErrorMsg(getCtx(), "Session Already Open.", 
						"Please close previos session first.");
				return false;
			}
			
			sql = "SELECT name FROM AD_USER WHERE AD_USER_ID = ?";
			String name = DB.getSQLValueString(get_TrxName(), sql, getCashier_ID());
			String strdate = getDateDoc().toString().substring(0, 10);
			setName("POS|"+strdate+"|"+name);
		}
		
		return super.beforeSave(newRecord);
	}
	
	private void addLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		
		log.log(level, msg);
	}
	
	@SuppressWarnings("resource")
	private String deleteNotCompletedPOSTrx ()
	{	
		String sql = "SELECT COUNT(*) FROM UNS_POSTrx WHERE DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
				+ " AND UNS_POS_Session_ID = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, get_ID());
		
		if(count > 0)
		{
			int retVal = 0;
			
			if(!isLocationServer() && !isMainServer())
				retVal = MessageBox.showMsg(this, getCtx(), 
						"This session have " + count + " POS Transactions were not completed."
						+ " Complete action will be remove that POS transaction."
						+ " Do you want to continue this action?", "Confirmation.",
						MessageBox.YESNO, MessageBox.ICONQUESTION);

			if(retVal == MessageBox.RETURN_YES || retVal == MessageBox.RETURN_OK)
			{
//				//DELETE CardTrxDetail
//				sql = "DELETE FROM UNS_CardTrxDetail cd WHERE EXISTS (SELECT 1 FROM UNS_PaymentTrx pt"
//						+ " WHERE pt.UNS_PaymentTrx_ID = cd.UNS_PaymentTrx_ID AND EXISTS"
//						+ " (SELECT 1 FROM UNS_POSPayment pp WHERE pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
//						+ " AND EXISTS (SELECT 1 FROM UNS_POSTrx trx WHERE trx.UNS_POSTrx_ID ="
//						+ " pp.UNS_POSTrx_ID AND trx.DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
//						+ " AND trx.UNS_POS_Session_ID = ?)))";
//				int result = DB.executeUpdate(sql, get_ID(), get_TrxName());
//				if(result < 0)
//					return "Failed when trying clear card transaction.";
//				
//				//DELETE PaymentTrx
//				sql = "DELETE FROM UNS_PaymentTrx pt WHERE EXISTS"
//						+ " (SELECT 1 FROM UNS_POSPayment pp WHERE pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
//						+ " AND EXISTS (SELECT 1 FROM UNS_POSTrx trx WHERE trx.UNS_POSTrx_ID ="
//						+ " pp.UNS_POSTrx_ID AND trx.DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
//						+ " AND trx.UNS_POS_Session_ID = ?))";
//				result = DB.executeUpdate(sql, get_ID(), get_TrxName());
//				if(result < 0)
//					return "Failed when trying clear payment transaction.";
//				
//				//DELETE POSPayment
//				sql = "DELETE FROM UNS_POSPayment pp WHERE EXISTS (SELECT 1 FROM UNS_POSTrx trx WHERE trx.UNS_POSTrx_ID ="
//						+ " pp.UNS_POSTrx_ID AND trx.DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
//						+ " AND trx.UNS_POS_Session_ID = ?)";
//				result = DB.executeUpdate(sql, get_ID(), get_TrxName());
//				if(result < 0)
//					return "Failed when trying clear pos payment.";
				
				//delete POSPayment
				sql = "SELECT UNS_POSPayment_ID FROM UNS_POSPayment pp WHERE EXISTS (SELECT 1 FROM UNS_POSTrx trx WHERE trx.UNS_POSTrx_ID ="
						+ " pp.UNS_POSTrx_ID AND trx.DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
						+ " AND trx.UNS_POS_Session_ID = ?)";
				ResultSet rs = null;
				PreparedStatement st = null;
				try {
					
					st = DB.prepareStatement(sql, get_TrxName());
					st.setInt(1, getUNS_POS_Session_ID());
					rs = st.executeQuery();
					while(rs.next())
					{
						MUNSPOSPayment posPayment = new MUNSPOSPayment(getCtx(), rs.getInt(1), get_TrxName());
						if(!posPayment.delete(true))
							return "Failed when trying clear pos payment.";
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					throw new AdempiereException(ex.getMessage());
				}
				finally{
					DB.close(rs, st);
				}
				
//				//DELETE POSTrxLine
//				sql = "DELETE FROM UNS_POSTrxLine pl WHERE EXISTS (SELECT 1 FROM UNS_POSTrx trx"
//						+ " WHERE trx.UNS_POSTrx_ID = pl.UNS_POSTrx_ID AND"
//						+ " trx.DocStatus NOT IN ('CO', 'CL', 'VO', 'RE') AND trx.UNS_POS_Session_ID = ?)";
//				result = DB.executeUpdate(sql, get_ID(), get_TrxName());
//				if(result < 0)
//					return "Failed when trying clear pos transaction lines.";
//				
//				//DELETE POS
//				sql = "DELETE FROM UNS_POSTrx WHERE UNS_POS_Session_ID = ? AND DocStatus NOT IN ('CO','CL','VO','RE')";
//				result = DB.executeUpdate(sql, get_ID(), get_TrxName());
//				if(result < 0)
//					return "Failed when trying clear pos transactions.";
				
				//delete POSTrx
				sql = "SELECT UNS_POSTrx_ID FROM UNS_POSTrx WHERE UNS_POS_Session_ID = ? AND DocStatus NOT IN ('CO','CL','VO','RE')";
				try {
					
					st = DB.prepareStatement(sql, get_TrxName());
					st.setInt(1, getUNS_POS_Session_ID());
					rs = st.executeQuery();
					while(rs.next())
					{
						MUNSPOSTrx trx = new MUNSPOSTrx(getCtx(), rs.getInt(1), get_TrxName());
						if(!trx.delete(true))
							return "Failed when trying clear pos transactions.";
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					throw new AdempiereException(ex.getMessage());
				}
				finally{
					DB.close(rs, st);
				}
			}
			else
			{
				return DOCSTATUS_InProgress;
			}
		}
		return null;
	}

	@Override
	public int getC_Currency_ID() 
	{
		return getBase1Currency_ID();
	}
	
	private String[] getMACS ()
	{
		List<String> macs = new ArrayList<>();
		try 
		{
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements())
			{
				NetworkInterface network = interfaces.nextElement();
				if (network.isVirtual())
					continue;
				byte[] macAsByte = network.getHardwareAddress();
				String mac = "";
				for (int i=0; macAsByte != null && i<macAsByte.length; i++)
				{
					String tmp = String.format("%02X%s", macAsByte[i], 
							(i < macAsByte.length - 1) ? "-" : "");
					mac += tmp;
				}
				if (!Util.isEmpty(mac))
					macs.add(mac);
			}
		} 
		catch (SocketException e) 
		{
			e.printStackTrace();
		}
		
		String[] macss = new String[macs.size()];
		macs.toArray(macss);
		return macss;
	}
	
	public static boolean updateByPOSTrx (MUNSPOSTrx pos)
	{
		MUNSPOSSession session = new MUNSPOSSession(pos.getCtx(), pos.getUNS_POS_Session_ID(), pos.get_TrxName());
		DB.getDatabase().forUpdate(session, 0);
		BigDecimal totalSales = pos.isReturn() ? Env.ZERO : pos.getGrandTotal();
		BigDecimal totalReturnAmt = pos.isReturn() ? pos.getGrandTotal().negate() : Env.ZERO;
		BigDecimal totalServiceCharge = pos.isReturn() ? Env.ZERO : pos.getServiceCharge();
		BigDecimal totalRefServiceCharge = pos.isReturn() ? pos.getServiceCharge().negate() : Env.ZERO;
		BigDecimal totalTax = pos.isReturn() ? Env.ZERO : pos.getTaxAmt() ;
		BigDecimal totalRefundTax = pos.isReturn() ? pos.getTaxAmt().negate()  : Env.ZERO;
		BigDecimal totalDiscAmt = pos.getDiscountAmt();
		MUNSPOSTrxLine[] lines = pos.getLines(false);
		
		for (int i=0; i<lines.length; i++)
			totalDiscAmt = totalDiscAmt.add(lines[i].getDiscountAmt());
		
		if (session.getBase1Currency_ID() != pos.getC_Currency_ID()) 
		{
			if(!pos.isReturn())
			{	
				totalTax = MConversionRate.convert(
						pos.getCtx(), totalTax, pos.getC_Currency_ID(), 
						session.getBase1Currency_ID(), session.getAD_Client_ID(), 
						session.getAD_Org_ID());
				totalSales = MConversionRate.convert(
					pos.getCtx(), totalSales, pos.getC_Currency_ID(), 
					session.getBase1Currency_ID(), session.getAD_Client_ID(), 
					session.getAD_Org_ID());
				totalServiceCharge = MConversionRate.convert(
						pos.getCtx(), totalServiceCharge, pos.getC_Currency_ID(), 
						session.getBase1Currency_ID(), session.getAD_Client_ID(), 
						session.getAD_Org_ID());
			}
			else
			{	
				totalRefundTax = MConversionRate.convert(
						pos.getCtx(), totalRefundTax, pos.getC_Currency_ID(), 
						session.getBase1Currency_ID(), session.getAD_Client_ID(), 
						session.getAD_Org_ID());
				totalReturnAmt = MConversionRate.convert(
						pos.getCtx(), totalReturnAmt, pos.getC_Currency_ID(), 
						session.getBase1Currency_ID(), session.getAD_Client_ID(), 
						session.getAD_Org_ID());
				totalRefServiceCharge =  MConversionRate.convert(
							pos.getCtx(), totalRefServiceCharge, pos.getC_Currency_ID(), 
							session.getBase1Currency_ID(), session.getAD_Client_ID(), 
							session.getAD_Org_ID());
			}
			totalDiscAmt = MConversionRate.convert(
					pos.getCtx(), totalDiscAmt, pos.getC_Currency_ID(), 
					session.getBase1Currency_ID(), session.getAD_Client_ID(), 
					session.getAD_Org_ID());
		}
		
		totalServiceCharge = totalServiceCharge.add(session.getTotalServiceCharge());
		totalTax = totalTax.add(session.getTotalTaxAmt());
		totalRefundTax = totalRefundTax.add(session.getTotalRefundTaxAmt());
		totalDiscAmt = totalDiscAmt.add(session.getTotalDiscAmt());
		totalSales = totalSales.add(session.getTotalSalesB1());
		totalRefServiceCharge = totalRefServiceCharge.add(session.getTotalRefundSvcChgAmt());
		totalReturnAmt = totalReturnAmt.add(session.getReturnAmt());
		
		session.setTotalServiceCharge(totalServiceCharge);
		session.setTotalDiscAmt(totalDiscAmt);
		session.setTotalTaxAmt(totalTax);
		session.setTotalRefundTaxAmt(totalRefundTax);
		session.setTotalSalesB1(totalSales);
		session.setReturnAmt(totalReturnAmt);
		session.setTotalRefundSvcChgAmt(totalRefServiceCharge);
		
		return session.save();
	}
	
	public MUNSSessionCashAccount getCashAccount (int cashAccoutID)
	{
		getCashAcounts(true);
		for (int i=0; i<m_accounts.length; i++)
			if (m_accounts[i].getUNS_POSTerminalCashAcct_ID() == cashAccoutID)
				return m_accounts[i];
		
		return null;
	}
	
	public boolean loadCashAccount ()
	{
		MBPartner store = new MBPartner(getCtx(), getUNS_POSTerminal().getStore_ID(), get_TrxName());
		MUNSPOSTerminalCashAcct[] cashes = store.getLines(false);
		for (int i=0; i<cashes.length; i++)
		{
			MUNSSessionCashAccount account = getCashAccount(cashes[i].getUNS_POSTerminalCashAcct_ID());
			if (account != null)
				continue;
			account = new MUNSSessionCashAccount(this, cashes[i]);
			if (!account.save())
				return false;
		}
		
		return true;
	}
	
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if(!isReplication())
		{
			if ((!isProcessed() || getCashAcounts(false).length == 0) && !loadCashAccount())
				return false;
		}
		return super.afterSave(newRecord, success);
	}
	
	private boolean isMainServer()
	{
		String serverType = MSysConfig.getValue(MSysConfig.SERVER_TYPE, "");
		String mainServer = MSysConfig.getValue(MSysConfig.MAIN_SERVER, "");
		
		return !Util.isEmpty(serverType) && serverType.equals(mainServer);
	}
	
	private boolean isLocationServer()
	{
		String serverType = MSysConfig.getValue(MSysConfig.SERVER_TYPE, "");
		String locationServer = MSysConfig.getValue(MSysConfig.LOCATION_SERVER, "");
		
		return !Util.isEmpty(serverType) && serverType.equals(locationServer);
	}
	
	public boolean doTakeOver (int cashierID)
	{
		MUNSCashierTandem tandem = MUNSCashierTandem.get(get_TrxName(), get_ID(), cashierID);
		if (tandem == null)
			tandem = new MUNSCashierTandem(this);
		tandem.setCashier_ID(cashierID);
		return tandem.save();
	}
	
	public MUNSCashierTandem[] getTandems (boolean requery)
	{
		if (m_tandems != null && !requery)
		{
			set_TrxName(m_tandems, get_TrxName());
			return m_tandems;
		}
		
		String where = "UNS_POS_Session_ID = ? AND IsActive = 'Y'";
		List<MUNSCashierTandem> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSCashierTandem.Table_Name, where, 
				get_TrxName()).setParameters(get_ID()).
				list();
		m_tandems = new MUNSCashierTandem[list.size()];
		list.toArray(m_tandems);
		return m_tandems;
	}
	
	public MUNSSessionCashAccount getCashAcct(int C_Currency_ID)
	{
		MUNSSessionCashAccount cashAcct = MUNSSessionCashAccount.get(get_TrxName(), get_ID(), C_Currency_ID);
		
		return cashAcct;
	}
	
	public void addLine (MUNSPOSTrx trx)
	{
		getLines(false);
		m_lines = Arrays.copyOf(m_lines, m_lines.length + 1);
		m_lines[m_lines.length-1] = trx;
	}
	
	public MUNSPOSTerminal getPOSDevice ()
	{
		if (m_terminal == null)
			m_terminal = new MUNSPOSTerminal(getCtx(), getUNS_POSTerminal_ID(), get_TrxName());
		return m_terminal;
	}
	
	public boolean fixCancelReplication (MUNSPOSTrx trx)
	{
		if (!trx.fixCancelReplication())
			return false;
		
		//DELETE CardTrxDetail
		String sql = "DELETE FROM UNS_CardTrxDetail cd WHERE EXISTS (SELECT 1 FROM UNS_PaymentTrx pt"
				+ " WHERE pt.UNS_PaymentTrx_ID = cd.UNS_PaymentTrx_ID AND EXISTS"
				+ " (SELECT 1 FROM UNS_POSPayment pp WHERE pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
				+ " AND pp.UNS_POSTrx_ID = ?))";
		int result = DB.executeUpdate(sql, trx.getUNS_POSTrx_ID(), get_TrxName());
		if(result < 0)
			return false;
		
		//DELETE PaymentTrx
		sql = "DELETE FROM UNS_PaymentTrx pt WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_POSPayment pp WHERE pp.UNS_POSPayment_ID = pt.UNS_POSPayment_ID"
				+ " AND pp.UNS_POSTrx_ID = ?)";
		result = DB.executeUpdate(sql, trx.getUNS_POSTrx_ID(), get_TrxName());
		if(result < 0)
			return false;
		
		//DELETE POSPayment
		sql = "DELETE FROM UNS_POSPayment pp WHERE pp.UNS_POSTrx_ID = ?";
		result = DB.executeUpdate(sql, trx.getUNS_POSTrx_ID(), get_TrxName());
		if(result < 0)
			return false;
		
		//DELETE POSTrxLineMA
		sql = "DELETE FROM UNS_POStrxLineMA WHERE "
				+ " UNS_POSTrxLine_ID IN (SELECT UNS_POSTrxLine_ID FROM UNS_POSTrxLine WHERE "
				+ "UNS_POSTrx_ID = ?)";
		result = DB.executeUpdate(sql, trx.getUNS_POSTrx_ID(), false, get_TrxName());
		if (result == -1)
			return false;
		
		//DELETE POSTrxLine
		sql = "DELETE FROM UNS_POStrxLine WHERE UNS_POStrx_ID = ?";
		result = DB.executeUpdate(sql, trx.getUNS_POSTrx_ID(), false, get_TrxName());
		if (result == -1)
			return false;
		
		//DELETE POSTrx
		sql = "DELETE FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
		result = DB.executeUpdate(sql, trx.getUNS_POSTrx_ID(), false, get_TrxName());
		if (result == -1)
			return false;
		return true;
	}
}