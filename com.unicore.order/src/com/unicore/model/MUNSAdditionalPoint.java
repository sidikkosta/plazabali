/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;


/**
 * @author ALBURHANY
 *
 */
public class MUNSAdditionalPoint extends X_UNS_AdditionalPoint {

//	private String m_Message = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = -5009145469621772178L;

	/**
	 * @param ctx
	 * @param UNS_AdditionalPoint_ID
	 * @param trxName
	 */
	public MUNSAdditionalPoint(Properties ctx, int UNS_AdditionalPoint_ID,
			String trxName) {
		super(ctx, UNS_AdditionalPoint_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAdditionalPoint(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean beforeSave (boolean newRecord)
	{		
		MUNSCustomerRedeem cr = new MUNSCustomerRedeem(getCtx(), getUNS_CustomerRedeem_ID(), get_TrxName());
		
		//check business partner exist in other customer redeem not processed.
		if(null != cr.getStatusDoc(getCtx(), getC_BPartner_ID(), get_TrxName()))
			throw new AdempiereException("Business Partner exist in document draft (document no " + cr.getDocumentNo() + ")");
		
		//check business partner exist in other additional point with parent not processed.
		String sql = "SELECT 1 FROM UNS_AdditionalPoint WHERE C_BPartner_ID = " + getC_BPartner_ID()
				+ " AND UNS_AdditionalPoint_ID <> " + get_ID();
		if(DB.getSQLValue(get_TrxName(), sql) >= 1)
			throw new AdempiereException("Business Partner exist in additional point with document un-processed");
		
		//value point cannot 0 or negate.
		if(getValuePoint().compareTo(Env.ZERO) == 0
				|| getValuePoint().compareTo(Env.ZERO) == -1)
			throw new AdempiereException("Value point disallowed 0 or negate.");
		
		//check business partner exist or not in list customer point
		if(MUNSCustomerPoint.getByBPartner(getCtx(), getC_BPartner_ID(), 0, get_TrxName()) == null)
			throw new AdempiereException("Business Partner not exist in Customer Point");
		
		setCurrentPoint(MUNSCustomerPoint.getCurrentPoint(getCtx(), getC_BPartner_ID(), 0, get_TrxName()));
		
		if(getCurrentPoint().compareTo(getValuePoint()) == -1)
			throw new AdempiereException("Not enough Point");
		
		return true;
	}
	
	@Override
	public boolean afterSave (boolean success, boolean newRecord)
	{
		
		return true;
	}
	
	public MUNSAdditionalPoint[] getByParent (int UNS_CustomerRedeem_ID)
	{
		String whereClause = "UNS_CustomerRedeem_ID = ?";
		
		List<MUNSAdditionalPoint> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSAdditionalPoint.Table_Name, whereClause, get_TrxName()).setParameters(UNS_CustomerRedeem_ID)
				.list();
		
		return list.toArray(new MUNSAdditionalPoint[list.size()]);
	}
}