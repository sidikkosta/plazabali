/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.I_C_OrderLine;
import org.compiere.model.MDocType;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInOutLineMA;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTransaction;
import org.compiere.model.X_M_Movement;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.base.model.Query;


/**
 * @author UNTA_Andy
 * 
 */
public class MUNSPackingListLine extends X_UNS_PackingList_Line
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5120654599454101151L;
	
	private boolean m_isReversal = false;

	/**
	 * @param ctx
	 * @param UNS_PackingList_Line_ID
	 * @param trxName
	 */
	public MUNSPackingListLine(Properties ctx, int UNS_PackingList_Line_ID, String trxName)
	{
		super(ctx, UNS_PackingList_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPackingListLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSPackingListLine(com.unicore.base.model.MOrderLine oline) {
        this(oline.getCtx(), 0, oline.get_TrxName());
        
        setClientOrg(oline);
        setC_OrderLine_ID(oline.get_ID());
        setM_Product_ID(oline.getM_Product_ID());
        setC_UOM_ID(oline.getC_UOM_ID());
        setQty(oline.getQtyOrdered().subtract(oline.getQtyDelivered()));        
    }

    /**
	 * 
	 * @param parent
	 */
	public void initline(MUNSPackingListOrder parent)
	{
		setClientOrg(parent);
		setUNS_PackingList_Order_ID(parent.get_ID());

		m_parent = parent;
	}

	MUNSPackingListOrder m_parent = null;
	private boolean m_automatic = false;

	public MUNSPackingListOrder getParent()
	{
		if (m_parent == null)
			m_parent = (MUNSPackingListOrder) getUNS_PackingList_Order();
		
		return m_parent;
	}
	
	public boolean isReversal() {
		return m_isReversal;
	}
	
	public void setIsReversal(boolean reversal) {
		m_isReversal = reversal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#afterSave(boolean, boolean)
	 */
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		getParent().setUpdateParent();
		
		Query query = 
				new Query(getCtx(), MOrderLine.Table_Name, "C_OrderLine_ID=" + getC_OrderLine_ID(), get_TrxName());
		query.setForUpdate(true);
		
		MOrderLine ol = query.first();
		
		if (newRecord && null != ol) {
			ol.setQtyPacked(ol.getQtyPacked().add(isReversal()? getQtyEntered().negate() : getQtyEntered()));
			ol.saveEx();
		}
		else if (is_ValueChanged(COLUMNNAME_QtyEntered) && null != ol) {
			BigDecimal oldEntered = (BigDecimal) get_ValueOld(COLUMNNAME_QtyEntered);
			
			BigDecimal qtyPacked = ol.getQtyPacked();
			
			qtyPacked = qtyPacked.subtract(oldEntered);
			qtyPacked = qtyPacked.add(getQtyEntered());
			
			ol.setQtyPacked(qtyPacked);
			ol.saveEx();
		}
		if(null != ol)
			setDescription(Util.isEmpty(getDescription()) ? ol.getDescription() : getDescription() + " | " + ol.getDescription());
		
		//load product accessories
		if(((newRecord || is_ValueChanged(COLUMNNAME_M_Product_ID)) || is_ValueChanged(COLUMNNAME_QtyEntered)) && !m_isReversal
				&& getReplaceLine_ID() == 0)
		{
			if(is_ValueChanged(COLUMNNAME_M_Product_ID))
			{
				String delLine = "DELETE UNS_PackingList_Line WHERE PackingList_Line_ID = ?";
				int count = DB.executeUpdate(delLine, get_ID(), get_TrxName());
				if(count < 0)
					throw new AdempiereException("failed when trying clean accessories for this line");
			}
			if(MProduct.isWithAccessories(getCtx(), getM_Product_ID()));
			{
				String sql = "SELECT ProductAccessories_ID, Qty FROM UNS_AccessoriesLine WHERE C_OrderLine_ID = ?";
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				try {
					pstmt = DB.prepareStatement(sql, get_TrxName());
					pstmt.setInt(1, getC_OrderLine_ID());
					rs = pstmt.executeQuery();
					while (rs.next())
					{
						String exists = "SELECT UNS_PackingList_Line_ID FROM UNS_PackingList_Line WHERE PackingList_Line_ID=?"
								+ " AND M_Product_ID=?";
						int exist = DB.getSQLValue(get_TrxName(), exists, new Object[]{get_ID(), rs.getInt(1)});
						MUNSPackingListLine pll = null;
						if(exist > 0)
						{
							pll = new MUNSPackingListLine(getCtx(), exist, get_TrxName());
							pll.setQty(rs.getBigDecimal(2).divide(getC_OrderLine().getQtyOrdered()).multiply(getQtyEntered()));
							pll.saveEx();
						}
						else
						{
							pll = new MUNSPackingListLine(getCtx(), 0, get_TrxName());
							pll.setPackingList_Line_ID(get_ID());
							pll.setAD_Org_ID(getUNS_PackingList_Order().getAD_Org_ID());
							pll.setUNS_PackingList_Order_ID(getUNS_PackingList_Order_ID());
							MLocator loc = MProduct.getDefaultLocator(getCtx(), rs.getInt(1), getUNS_PackingList_Order().getM_Warehouse_ID());
							if(null != loc)
								pll.setM_Locator_ID(loc.get_ID());
							else
								pll.setM_Locator_ID(getM_Locator_ID());
							pll.setM_Product_ID(rs.getInt(1));
							pll.setQty((rs.getBigDecimal(2).divide(getC_OrderLine().getQtyOrdered(),2, RoundingMode.HALF_UP)).multiply(getQtyEntered()));
							pll.setisProductAccessories(true);						
							pll.saveEx();
							
						}
					}
				} catch (SQLException e) {
					throw new AdempiereException(e.getMessage());
				} catch (Exception e) {
					throw new AdempiereException("Unable to load product accessories::" + e.getMessage());
				} finally {
					DB.close(rs, pstmt);
				}
			}
		}
		
		return super.afterSave(newRecord, success);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#afterSave(boolean, boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord)
		{
			String duplicateLine = "SELECT 1 FROM UNS_PackingList_Line WHERE UNS_PackingList_Order_ID = " + getUNS_PackingList_Order_ID()
					+ " AND C_OrderLine_ID = " + getC_OrderLine_ID();
			
			if(DB.getSQLValue(get_TrxName(), duplicateLine) >= 1)
				throw new AdempiereUserError("Duplicate Line.");
		}
		
//		if(getC_OrderLine_ID() == 0 && getC_InvoiceLine_ID() == 0)
//		{
//			throw new AdempiereUserError("Please define Order Line or Invoice Line.");
//		}
		
		if (isAutomatic())
			return super.beforeSave(newRecord);
		
		if (!newRecord && is_ValueChanged(COLUMNNAME_QtyEntered) && !isProductAccessories() && !isReversal())
		{
			I_C_OrderLine orderLine = getC_OrderLine();
			BigDecimal qtyPacked = orderLine.getQtyPacked();
			     
			BigDecimal oldEntered = (BigDecimal) get_ValueOld(COLUMNNAME_QtyEntered);
			
			qtyPacked = qtyPacked.subtract(oldEntered);
			
			BigDecimal qtyAvailable = orderLine.getQtyOrdered().subtract(qtyPacked);
			
			if (qtyAvailable.compareTo(getQtyEntered()) < 0) {
				throw new AdempiereUserError(
						"Quantity entered (" + getQtyEntered() + ") is greater than available quantity to pack (" 
						+ qtyAvailable + ")");
			}
		}
		
		if(is_ValueChanged(COLUMNNAME_C_OrderLine_ID) && getC_OrderLine_ID() > 0)
		{
			String sql = "SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_OrderLine_ID = ? AND C_Invoice_ID = ?";
			int C_InvoiceLine_ID = DB.getSQLValue(get_TrxName(), sql, getC_OrderLine_ID(), getParent().getC_Invoice_ID());
			setC_InvoiceLine_ID(C_InvoiceLine_ID);
		}
		else if(is_ValueChanged(COLUMNNAME_C_InvoiceLine_ID) && getC_InvoiceLine_ID() > 0)
		{
			String sql = "SELECT C_OrderLine_ID FROM C_InvoiceLine WHERE C_InvoiceLine_ID = ? AND C_Order_ID = ?";
			int C_OrderLine_ID = DB.getSQLValue(get_TrxName(), sql, getC_InvoiceLine_ID(), getParent().getC_Order_ID());
			setC_OrderLine_ID(C_OrderLine_ID);
		}
		
		
		return super.beforeSave(newRecord);
	}
	
	public boolean beforeDelete()
	{
		if(!isProductAccessories())
		{
			String delLine = "DELETE UNS_PackingList_Line WHERE PackingList_Line_ID = ?";
			int count = DB.executeUpdate(delLine, get_ID(), get_TrxName());
			if(count < 0)
				throw new AdempiereException("failed when trying clean accessories for this line");
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#afterDelete(boolean)
	 */
	@Override
	protected boolean afterDelete(boolean success)
	{
		if (!success)
			return success;
		if(getC_OrderLine_ID() > 0)
		{
			Query query = 
				new Query(getCtx(), MOrderLine.Table_Name, "C_OrderLine_ID=" + getC_OrderLine_ID(), get_TrxName());
			query.setForUpdate(true);
			
			MOrderLine ol = query.first();
			
			ol.setQtyPacked(ol.getQtyPacked().subtract(getQtyEntered()));
			ol.saveEx();
		}
		
		if(getC_InvoiceLine_ID() > 0)
		{
			MInvoiceLine invLine = new MInvoiceLine(getCtx(), getC_InvoiceLine_ID(), get_TrxName());
			invLine.delete(true);
		}
		
		if(getM_InOutLine_ID() > 0)
		{
			MInOutLine ioLine = new MInOutLine(getCtx(), getM_InOutLine_ID(), get_TrxName());
			ioLine.delete(true);
		}			
 
		return true;
	}
	
	public boolean isAutomatic()
	{
		return m_automatic ;
	}
	
	public void setAutomatic(boolean automatic)
	{
		this.m_automatic = automatic;
	}
	
	public void setQty(BigDecimal qty){
	    setTargetQty(qty);
	    setQtyEntered(qty);
	    setMovementQty(qty);
	}
	
	/**
	 * 
	 * @return
	 */
	public String reverseFromShipment(Timestamp dateDoc, int M_InOutLine_ID)
	{
		//MInOutLine iol = (MInOutLine) getM_InOutLine();
		MInOutLine iol = new MInOutLine(getCtx(), M_InOutLine_ID, get_TrxName());
		MLocator intransitLoc = (MLocator) iol.getM_Locator();
//		String intransitLocValue = intransitLoc.getValue();
//		int intransitStrIx = intransitLocValue.indexOf("-" + MWarehouse.INITIAL_INTRANSIT_CUSTOMER_LOC);
//		String oriLocValue = intransitLoc.getValue().substring(0, intransitStrIx);
		
		int M_Warehouse_ID = intransitLoc.getM_Warehouse_ID();
		
//		String sql = "SELECT M_Locator_ID FROM M_Locator WHERE Value=? AND M_Warehouse_ID=?";
//		int oriLoc_ID = DB.getSQLValueEx(get_TrxName(), sql, oriLocValue, M_Warehouse_ID);
		
		int oriLoc_ID = getM_Locator_ID();
		
		if (iol.getM_AttributeSetInstance_ID() == 0) 
		{
			MInOutLineMA[] maList = MInOutLineMA.get(getCtx(), iol.getM_InOutLine_ID(), get_TrxName());
			
			for (MInOutLineMA ma : maList)
			{
				if (!MStorageOnHand.add(getCtx(), M_Warehouse_ID, intransitLoc.getM_Locator_ID(),
						getM_Product_ID(), ma.getM_AttributeSetInstance_ID(), ma.getMovementQty().abs().negate(), 
						ma.getDateMaterialPolicy(), get_TrxName()))
				{
					String lastError = CLogger.retrieveErrorString("");
					return "Cannot correct Intransit Inventory OnHand (MA) - " + lastError;
				}
				
				MTransaction trxFrom =
						new MTransaction(getCtx(), intransitLoc.getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementFrom,
								intransitLoc.getM_Locator_ID(), getM_Product_ID(), ma.getM_AttributeSetInstance_ID()
								, ma.getMovementQty().abs().negate(), dateDoc, get_TrxName());
				if (!trxFrom.save())
				{
					return "Failed when reversing intransit stock. Transaction From not inserted";
				}
				
				if (!MStorageOnHand.add(getCtx(), M_Warehouse_ID, oriLoc_ID, getM_Product_ID(), 
						ma.getM_AttributeSetInstance_ID(), ma.getMovementQty().abs(), null, get_TrxName()))
				{
					String lastError = CLogger.retrieveErrorString("");
					return "Cannot correct move back Intransit Inventory OnHand - " + lastError;
				}
				
				MTransaction trxTo =
						new MTransaction(getCtx(), intransitLoc.getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementTo,
								oriLoc_ID, getM_Product_ID(), ma.getM_AttributeSetInstance_ID()
								, ma.getMovementQty().abs(), dateDoc, get_TrxName());
				if (!trxTo.save())
				{
					return "Failed when reversing intransit stock. Transaction From not inserted";
				}			
			}
		}
		else 
		{			
			if (!MStorageOnHand.add(getCtx(), M_Warehouse_ID, intransitLoc.getM_Locator_ID(),
					getM_Product_ID(), iol.getM_AttributeSetInstance_ID(), iol.getMovementQty().abs().negate(), 
					null, get_TrxName()))
			{
				String lastError = CLogger.retrieveErrorString("");
				return "Cannot correct Intransit Inventory OnHand (MA) - " + lastError;
			}
			
			MTransaction trxFrom =
					new MTransaction(getCtx(), iol.getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementFrom,
							intransitLoc.getM_Locator_ID(), getM_Product_ID(), iol.getM_AttributeSetInstance_ID()
							, iol.getMovementQty().abs().negate(), dateDoc, get_TrxName());
			if (!trxFrom.save())
			{
				return "Failed when reversing intransit stock. Transaction From not inserted";
			}
			
			if (!MStorageOnHand.add(getCtx(), M_Warehouse_ID, oriLoc_ID, getM_Product_ID(), 
					iol.getM_AttributeSetInstance_ID(), iol.getMovementQty().abs(), null, get_TrxName()))
			{
				String lastError = CLogger.retrieveErrorString("");
				return "Cannot correct move back Intransit Inventory OnHand - " + lastError;
			}
			
			MTransaction trxTo =
					new MTransaction(getCtx(), iol.getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementTo,
							oriLoc_ID, getM_Product_ID(), iol.getM_AttributeSetInstance_ID()
							, iol.getMovementQty().abs(), dateDoc, get_TrxName());
			if (!trxTo.save())
			{
				return "Failed when reversing intransit stock. Transaction From not inserted";
			}			
		}
		
		return null;
	}
	
	public static MUNSPackingListLine[] getLines(Properties ctx, int PackingList_Line_ID, String trxName)
	{
		ArrayList<MUNSPackingListLine> list = new ArrayList<MUNSPackingListLine>();
		
		String sql = "SELECT UNS_PackingList_Line_ID FROM UNS_PackingList_Line "
				+ "WHERE PackingList_Line_ID = ? AND isActive='Y'";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, PackingList_Line_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add(new MUNSPackingListLine(ctx, rs.getInt(1),
						trxName));
			rs.close();
			pstmt.close();
			pstmt = null;
		} catch (SQLException ex) {
			throw new AdempiereException("Unable to load list of Packing List Line",
					ex);
		} finally {
			DB.close(rs, pstmt);
		}

		MUNSPackingListLine[] retValue = new MUNSPackingListLine[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	public String moveASI(int M_Warehouse_ID, int intLoc_ID,
			int M_Product_ID, int ASI_ID, BigDecimal moveQty, String trxName)
	{
		String m_processMsg = null;
		MMovement mv = new MMovement(getCtx(), 0, trxName);
		
		String sql = "SELECT COALESCE(SUM(QtyOnHand),0) FROM M_Storage WHERE M_Locator_ID=? AND M_Product_ID=?"
				+ " AND M_AttributeSetInstance_ID=?";
		BigDecimal qtyOnHand = DB.getSQLValueBD(trxName, sql, new Object[]{intLoc_ID, M_Product_ID, ASI_ID});
		
		if(qtyOnHand.compareTo(moveQty) >= 0)
			return null;
		
		moveQty = moveQty.subtract(qtyOnHand);
		
		//buat dapet id ASI sama total qty yang ada distorage
		String storage = "SELECT ASI, Qty FROM"
				+ " (SELECT s.M_AttributeSetInstance_ID AS ASI, SUM(QtyOnHand) AS Qty"
				+ " FROM M_StorageOnHand s WHERE M_Locator_ID=? AND M_Product_ID=? GROUP BY s.M_AttributeSetInstance_ID)"
				+ " AS Master WHERE Qty <> 0 AND ASI<>?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = DB.prepareStatement(storage, get_TrxName());
			stmt.setInt(1, intLoc_ID);
			stmt.setInt(2, M_Product_ID);
			stmt.setInt(3, ASI_ID);
			rs = stmt.executeQuery();
			BigDecimal avaliableQty = Env.ZERO;
			while (rs.next())
			{
				if(rs.isFirst())
					avaliableQty = rs.getBigDecimal(2);
				
				//qty yang digunakan di packing list, ga boleh di ambil
				String checkQtyUse = "SELECT COALESCE(SUM(ma.MovementQty),0) FROM UNS_PL_ConfirmMA ma"
						+ " INNER JOIN UNS_PL_ConfirmProduct cp ON cp.UNS_PL_ConfirmProduct_ID"
						+ " = ma.UNS_PL_ConfirmProduct_ID"
						+ " INNER JOIN UNS_PL_Confirm plc ON plc.UNS_PL_Confirm_ID = cp.UNS_PL_Confirm_ID"
						+ " INNER JOIN UNS_PL_Return plr ON plr.UNS_PackingList_ID = plc.UNS_PackingList_ID"
						+ " INNER JOIN UNS_PackingList_Order plo ON plo.UNS_PackingList_ID = plc.UNS_PackingList_ID"
						+ " WHERE plr.DocStatus NOT IN ('CO', 'CL', 'RE', 'VO') AND plo.M_Warehouse_ID=?"
						+ " AND cp.M_Product_ID=? AND ma.M_AttributeSetInstance_ID=?";
				BigDecimal qtyUse = DB.getSQLValueBD(get_TrxName(), checkQtyUse,
						new Object[]{M_Warehouse_ID, M_Product_ID, rs.getInt(1)});
				
				if(qtyUse.compareTo(avaliableQty) >= 0)
					continue;
				
				avaliableQty = avaliableQty.subtract(qtyUse);
				
				if(avaliableQty.compareTo(Env.ZERO) <= 0)
					continue;
					
				avaliableQty = avaliableQty.subtract(moveQty);
				
				if(mv.get_ID() <= 0)
				{
					mv.setAD_Org_ID(getAD_Org_ID());
					mv.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialMovement));
					mv.setMovementDate(new Timestamp (System.currentTimeMillis ()));
					mv.setDestinationWarehouse_ID(getUNS_PackingList_Order().getM_Warehouse_ID());
					mv.setDescription("Mapping ASI from PackingList :: " + getUNS_PackingList_Order()
											.getUNS_PackingList().getDocumentNo());
					mv.setSalesRep_ID(getCreatedBy());
					if(!mv.save())
						m_processMsg = mv.getProcessMsg();
				}
				
				MMovementLine mvl = new MMovementLine(mv);
				mvl.setM_Product_ID(M_Product_ID);
				mvl.setMovementQty(moveQty);
				mvl.setM_Locator_ID(intLoc_ID);
				mvl.setM_LocatorTo_ID(intLoc_ID);
				mvl.setM_AttributeSetInstance_ID(rs.getInt(1));
				mvl.setM_AttributeSetInstanceTo_ID(ASI_ID);
				if(!mvl.save())
					m_processMsg = "failed when trying save move lines";
				
				if(avaliableQty.compareTo(Env.ZERO) >= 0)
					break;
			}
		}
		catch (Exception e)
		{
			try {
				throw new AdempiereSystemError("System Error : " + e.getLocalizedMessage(), e);
			} catch (AdempiereSystemError e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally
		{
			DB.close(rs, stmt); rs = null; stmt = null;
		}
		
		if(mv.get_ID() > 0)
		{
			mv.processIt(X_M_Movement.DOCACTION_Complete);
			mv.saveEx();
		}
		return m_processMsg;
	}
}