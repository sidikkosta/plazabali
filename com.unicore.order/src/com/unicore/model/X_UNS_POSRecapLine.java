/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POSRecapLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSRecapLine extends PO implements I_UNS_POSRecapLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180907L;

    /** Standard Constructor */
    public X_UNS_POSRecapLine (Properties ctx, int UNS_POSRecapLine_ID, String trxName)
    {
      super (ctx, UNS_POSRecapLine_ID, trxName);
      /** if (UNS_POSRecapLine_ID == 0)
        {
			setisRefund (false);
// N
			setM_Product_ID (0);
			setSKU (null);
			setUNS_POSRecapLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSRecapLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSRecapLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Cost Value.
		@param CostAmt 
		Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt)
	{
		set_Value (COLUMNNAME_CostAmt, CostAmt);
	}

	/** Get Cost Value.
		@return Value with Cost
	  */
	public BigDecimal getCostAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CostAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Amount.
		@param DiscountAmt 
		Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt)
	{
		set_ValueNoCheck (COLUMNNAME_DiscountAmt, DiscountAmt);
	}

	/** Get Discount Amount.
		@return Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DiscountAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set ETBB Code.
		@param ETBBCode ETBB Code	  */
	public void setETBBCode (String ETBBCode)
	{
		set_Value (COLUMNNAME_ETBBCode, ETBBCode);
	}

	/** Get ETBB Code.
		@return ETBB Code	  */
	public String getETBBCode () 
	{
		return (String)get_Value(COLUMNNAME_ETBBCode);
	}

	/** Set Consignment.
		@param IsConsignment Consignment	  */
	public void setIsConsignment (boolean IsConsignment)
	{
		set_Value (COLUMNNAME_IsConsignment, Boolean.valueOf(IsConsignment));
	}

	/** Get Consignment.
		@return Consignment	  */
	public boolean isConsignment () 
	{
		Object oo = get_Value(COLUMNNAME_IsConsignment);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Refund.
		@param isRefund Refund	  */
	public void setisRefund (boolean isRefund)
	{
		set_Value (COLUMNNAME_isRefund, Boolean.valueOf(isRefund));
	}

	/** Get Refund.
		@return Refund	  */
	public boolean isRefund () 
	{
		Object oo = get_Value(COLUMNNAME_isRefund);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Push Money Amount.
		@param PushMoneyAmt Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt)
	{
		set_Value (COLUMNNAME_PushMoneyAmt, PushMoneyAmt);
	}

	/** Get Push Money Amount.
		@return Push Money Amount	  */
	public BigDecimal getPushMoneyAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PushMoneyAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Push Money Per Unit.
		@param PushMoneyPerUnit Push Money Per Unit	  */
	public void setPushMoneyPerUnit (BigDecimal PushMoneyPerUnit)
	{
		set_Value (COLUMNNAME_PushMoneyPerUnit, PushMoneyPerUnit);
	}

	/** Get Push Money Per Unit.
		@return Push Money Per Unit	  */
	public BigDecimal getPushMoneyPerUnit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PushMoneyPerUnit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Revenue Amount.
		@param RevenueAmt Revenue Amount	  */
	public void setRevenueAmt (BigDecimal RevenueAmt)
	{
		set_Value (COLUMNNAME_RevenueAmt, RevenueAmt);
	}

	/** Get Revenue Amount.
		@return Revenue Amount	  */
	public BigDecimal getRevenueAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RevenueAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sales Amount.
		@param SalesAmt Sales Amount	  */
	public void setSalesAmt (BigDecimal SalesAmt)
	{
		set_Value (COLUMNNAME_SalesAmt, SalesAmt);
	}

	/** Get Sales Amount.
		@return Sales Amount	  */
	public BigDecimal getSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Service Charge Amount.
		@param ServiceChargeAmt Service Charge Amount	  */
	public void setServiceChargeAmt (BigDecimal ServiceChargeAmt)
	{
		set_Value (COLUMNNAME_ServiceChargeAmt, ServiceChargeAmt);
	}

	/** Get Service Charge Amount.
		@return Service Charge Amount	  */
	public BigDecimal getServiceChargeAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ServiceChargeAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set SKU.
		@param SKU 
		Stock Keeping Unit
	  */
	public void setSKU (String SKU)
	{
		set_Value (COLUMNNAME_SKU, SKU);
	}

	/** Get SKU.
		@return Stock Keeping Unit
	  */
	public String getSKU () 
	{
		return (String)get_Value(COLUMNNAME_SKU);
	}

	/** Set Tax Amount.
		@param TaxAmt 
		Tax Amount for a document
	  */
	public void setTaxAmt (BigDecimal TaxAmt)
	{
		set_Value (COLUMNNAME_TaxAmt, TaxAmt);
	}

	/** Get Tax Amount.
		@return Tax Amount for a document
	  */
	public BigDecimal getTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_UNS_POSRecap getUNS_POSRecap() throws RuntimeException
    {
		return (I_UNS_POSRecap)MTable.get(getCtx(), I_UNS_POSRecap.Table_Name)
			.getPO(getUNS_POSRecap_ID(), get_TrxName());	}

	/** Set POS Recapitulation.
		@param UNS_POSRecap_ID POS Recapitulation	  */
	public void setUNS_POSRecap_ID (int UNS_POSRecap_ID)
	{
		if (UNS_POSRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_ID, Integer.valueOf(UNS_POSRecap_ID));
	}

	/** Get POS Recapitulation.
		@return POS Recapitulation	  */
	public int getUNS_POSRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set POS Recapitulation Line.
		@param UNS_POSRecapLine_ID POS Recapitulation Line	  */
	public void setUNS_POSRecapLine_ID (int UNS_POSRecapLine_ID)
	{
		if (UNS_POSRecapLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecapLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecapLine_ID, Integer.valueOf(UNS_POSRecapLine_ID));
	}

	/** Get POS Recapitulation Line.
		@return POS Recapitulation Line	  */
	public int getUNS_POSRecapLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSRecapLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSRecapLine_UU.
		@param UNS_POSRecapLine_UU UNS_POSRecapLine_UU	  */
	public void setUNS_POSRecapLine_UU (String UNS_POSRecapLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSRecapLine_UU, UNS_POSRecapLine_UU);
	}

	/** Get UNS_POSRecapLine_UU.
		@return UNS_POSRecapLine_UU	  */
	public String getUNS_POSRecapLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSRecapLine_UU);
	}
}