/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MPeriod;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

/**
 * @author Burhani Adam
 *
 */
public class MUNSVoucherCorrection extends X_UNS_VoucherCorrection implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3713808622835273935L;

	/**
	 * @param ctx
	 * @param UNS_VoucherCorrection_ID
	 * @param trxName
	 */
	public MUNSVoucherCorrection(Properties ctx, int UNS_VoucherCorrection_ID,
			String trxName) {
		super(ctx, UNS_VoucherCorrection_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSVoucherCorrection(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSSalesReconciliation[");
		sb.append(get_ID()).append("-").append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_SalesReconciliation_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		if(getReversal_ID() > 0)
			return true;
		if(newRecord || is_ValueChanged(COLUMNNAME_VoucherCode))
		{
			String sql = "SELECT DocumentNo FROM UNS_VoucherCorrection WHERE VoucherCode = ?"
					+ " AND DocStatus NOT IN ('CO', 'CL', 'VO', 'RE') AND UNS_VoucherCorrection_ID <> ?";
			String otherDoc = DB.getSQLValueString(get_TrxName(), sql, new Object[]{getVoucherCode(), get_ID()});
			if(!Util.isEmpty(otherDoc, true))
			{
				log.saveError("Error", "Duplicate voucher code on other draft document. " + otherDoc);
				return false;
			}
		}
		
		if(getAllocatedAmt().compareTo(getUnusedAmt()) == 1)
		{
			log.saveError("Error", "Over allocated amount.");
			return false;
		}
		
		if(getAllocatedAmt().signum() == 0)
		{
			log.saveError("Error", "Allocated amount cannot zero.");
			return false;
		}
		
		if(newRecord || is_ValueChanged(COLUMNNAME_AllocatedAmt) || is_ValueChanged(COLUMNNAME_UNS_PaymentTrx_ID))
		{
			String sql = "SELECT SUM(AllocatedAmt) FROM UNS_VoucherCorrection WHERE UNS_PaymentTrx_ID = ?"
					+ " AND DocStatus IN ('CO', 'CL') AND UNS_VoucherCorrection_ID <> ?";
			BigDecimal allocatedAmt = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{getUNS_PaymentTrx_ID(), get_ID()});
			
			if(allocatedAmt == null)
				allocatedAmt = Env.ZERO;
			
			BigDecimal balance = getTrxAmt().subtract(allocatedAmt);
			balance = balance.subtract(getAllocatedAmt());
			
			if((getTrxAmt().signum() == 1 && balance.signum() == -1)
					|| getTrxAmt().signum() == -1 && balance.signum() == 1)
			{
				
				log.saveError("Error", "Over correction amount " + balance + ". #Transaction amount :: " + getTrxAmt() 
						+ "#Corrected :: " + allocatedAmt + " #Next Correction :: " + getAllocatedAmt());
				return false;
			}
		}
		
		return true;
	}	
	
	protected boolean beforeDelete()
	{
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		MPeriod.testPeriodOpen(getCtx(), getDateAcct(), getC_DocType_ID(), getAD_Org_ID());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		String sql = "SELECT SUM(AllocatedAmt) FROM UNS_VoucherCorrection WHERE UNS_PaymentTrx_ID = ?"
				+ " AND DocStatus IN ('CO', 'CL') AND UNS_VoucherCorrection_ID <> ?";
		BigDecimal allocatedAmt = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{getUNS_PaymentTrx_ID(), get_ID()});
		
		if(allocatedAmt == null)
			allocatedAmt = Env.ZERO;
		
		BigDecimal balance = getTrxAmt().subtract(allocatedAmt);
		balance = balance.subtract(getAllocatedAmt());
		
		if((getTrxAmt().signum() == 1 && balance.signum() == -1)
				|| getTrxAmt().signum() == -1 && balance.signum() == 1)
		{
			
			m_processMsg = "Over correction amount " + balance + ". #Transaction amount :: " + getTrxAmt() 
					+ "#Corrected :: " + allocatedAmt + " #Next Correction :: " + getAllocatedAmt();
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = updateVoucher();
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = reverse(false);
		if (m_processMsg != null)
			return false;
		
		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = reverse(true);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
//		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
//		// Before reActivate
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
//		if (m_processMsg != null)
//			return false;
//
//	//	setProcessed(false);
//		if (! reverseCorrectIt())
//			return false;
//
//		// After reActivate
//		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
//		if (m_processMsg != null)
//			return false;
		
		m_processMsg = "Action re-activate not implemented for Voucher Correction Document.";

		return false;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(docStatus.equals(DOCSTATUS_InProgress))
			options[index++] = DOCACTION_Void;
		else if(docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Reverse_Accrual;
			options[index++] = DOCACTION_Reverse_Correct;
		}
		return index;
	}
	
	private String updateVoucher()
	{
		MUNSVoucherCode vc = new MUNSVoucherCode(getCtx(), getUNS_VoucherCode_ID(), get_TrxName());
		vc.setUsedAmt(vc.getUsedAmt().add(getAllocatedAmt()));
		if(!vc.save())
			return "Failed when trying update voucher code.";
		
		return null;
	}
	
	private String reverse(boolean accrual)
	{
		Timestamp reversalDate = accrual ? Env.getContextAsDate(getCtx(), "#Date") : getDateAcct();
		if (reversalDate == null) {
			reversalDate = new Timestamp(System.currentTimeMillis());
		}
		
		MPeriod.testPeriodOpen(getCtx(), reversalDate, getC_DocType_ID(), getAD_Org_ID());
		
		MUNSVoucherCorrection reversal = new MUNSVoucherCorrection(getCtx(), 0, get_TrxName());
		reversal.setReversal_ID(get_ID());
		reversal.setAD_Org_ID(getAD_Org_ID());
		reversal.setDocumentNo(getDocumentNo() + "^");
		reversal.setC_DocType_ID(getC_DocType_ID());
		reversal.setDateDoc(new Timestamp(System.currentTimeMillis()));
		reversal.setC_Currency_ID(getC_Currency_ID());
		reversal.setDateTrx(getDateTrx());
		reversal.setDateAcct(reversalDate);
		reversal.setStore_ID(getStore_ID());
		reversal.setUNS_POS_Session_ID(getUNS_POS_Session_ID());
		reversal.setUNS_POSTrx_ID(getUNS_POSTrx_ID());
		reversal.setUNS_PaymentTrx_ID(getUNS_PaymentTrx_ID());
		reversal.setUNS_VoucherCode_ID(getUNS_VoucherCode_ID());
		reversal.setVoucherCode(getVoucherCode());
		reversal.setVoucherAmt(getVoucherAmt());
		reversal.setUnusedAmt(getUnusedAmt());
		reversal.setUsedAmt(getUsedAmt());
		reversal.setTrxAmt(getTrxAmt());
		reversal.setAllocatedAmt(getAllocatedAmt().negate());
		if(!reversal.save())
			return "Failed when trying create reversal document.";
		
		if (!reversal.processIt(DOCACTION_Complete))
		{
			return "Reversal ERROR: " + reversal.getProcessMsg();
		}
		reversal.closeIt();
		reversal.setDocStatus(DOCSTATUS_Reversed);
		reversal.setDocAction(DOCACTION_None);
		if(!reversal.save())
			return "Failed when trying complete reversal document.";
		
		setDocStatus(DOCSTATUS_Reversed);
		setDocAction(DOCACTION_None);
		
		return null;
	}
}