/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MStorageOnHand;
import org.compiere.util.DB;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author root
 *
 */
public class MUNSCvsRptProduct extends X_UNS_Cvs_RptProduct {

	/**
	 * 
	 */
	private static final long serialVersionUID	= 5445753378941958749L;
	private MUNSCvsRpt m_parent					= null;
	private MUNSCvsRptProductMA[] m_lines		= null;

	/**
	 * @param ctx
	 * @param UNS_Cvs_RptProduct_ID
	 * @param trxName
	 */
	public MUNSCvsRptProduct(Properties ctx, int UNS_Cvs_RptProduct_ID,
			String trxName) {
		super(ctx, UNS_Cvs_RptProduct_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCvsRptProduct(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public MUNSCvsRptProduct(MUNSCvsRpt parent)
	{
		this(parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_Cvs_Rpt_ID(parent.get_ID());
	}
	
	public void setParent(MUNSCvsRpt parent)
	{
		this.m_parent = parent;
	}
	
	public MUNSCvsRpt getParent(boolean requery)
	{
		if(null != m_parent && !requery)
			return m_parent;
		
		m_parent = (MUNSCvsRpt) getUNS_Cvs_Rpt();
		return m_parent;
	}
	
	public MUNSCvsRpt getParent()
	{
		return getParent(false);
	}
	
	/**
	 * 
	 * @return
	 */
	public MUNSCvsRptProductMA[] getLines()
	{
		return getLines(false);
	}
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public MUNSCvsRptProductMA[] getLines(boolean requery)
	{
		if(null != m_lines && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSCvsRptProductMA> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSCvsRptProductMA.Table_Name
				, Table_Name + "_ID = ?", get_TrxName())
				.setParameters(get_ID()).list();
		
		m_lines = new MUNSCvsRptProductMA[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}
	
	@Override
	public BigDecimal getQtyAvailable()
	{
		if(super.getQtyAvailable().signum() > 0)
			return super.getQtyAvailable();
		
		BigDecimal qtyAvailable = MStorageOnHand.getQtyOnHandForLocator(
				getM_Product_ID(), getM_Locator_ID(), getM_AttributeSetInstance_ID(), get_TrxName());
		set_Value(COLUMNNAME_QtyAvailable, qtyAvailable);
		return super.getQtyAvailable();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(getQtySold().compareTo(getQtyAvailable()) > 0)
			throw new AdempiereException("Over qty sold. Qty sold can't bigger than qty available");
		return super.beforeSave(newRecord);
	}
	
	public void setLineNetAmt()
	{
		setLineNetAmt(getPriceActual().multiply(getQtySold()));
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		updateHeader();
		return super.afterSave(newRecord, success);
	}
	
	private void updateHeader()
	{
		BigDecimal totalLines = DB.getSQLValueBD(
				get_TrxName()
				, "SELECT COALESCE(SUM(LineNetAmt), 0) FROM UNS_Cvs_RptProduct WHERE UNS_Cvs_Rpt_ID = ?"
				, getParent().get_ID());
		
		String update = "UPDATE UNS_Cvs_Rpt SET TotalLines = ?, GrandTotal = ? WHERE UNS_Cvs_Rpt_ID = ?";
		int ok = DB.executeUpdate(
				update, new Object[]{totalLines, totalLines, getParent().get_ID()}
				, false , get_TrxName());
		if(ok == -1)
		{
			throw new AdempiereException("Failed on update header");
		}
	}
	
	public void setPrice()
	{
		SimplePrice price = new SimplePrice(
				getParent().getC_BPartner_ID(), getM_Product_ID(), get_TrxName());
		price.setPrecission(getParent().getPrecission());
		
		setPriceList(price.getPriceList());
		setPriceLimit(price.getPriceLimit());
		setPriceActual(price.getPriceActual());
	}
}
