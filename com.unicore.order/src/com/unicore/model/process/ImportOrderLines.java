/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.MUNSImportSimpleColumn;
import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Burhani Adam
 *
 */
public class ImportOrderLines extends SvrProcess {
	
	private boolean DeleteOld = false;
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOld"))
				DeleteOld = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window Order");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MOrder order = new MOrder(getCtx(), getRecord_ID(), get_TrxName());
		if(order.isProcessed())
			throw new AdempiereException("Can only import to not-processed Document.");
		
		if(DeleteOld)
		{
			MOrderLine[] lines = order.getLines(true, null);
			for(int i=0;i<lines.length;i++)
			{
				lines[i].deleteEx(true);
			}
		}
		order.setImport(true);
		
		if(Util.isEmpty(order.getFile_Directory(), true))
			throw new AdempiereException("To run this process, you must fill the file with the correct directory / path.");
		
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportPO'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with ImportPO name");
		
		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(order.getFile_Directory());
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length > 1)
			throw new AdempiereException("more than one lines in Import Simple XLS " + simple.getName());
		
		MUNSImportSimpleTable table = tables[0];
		MUNSImportSimpleColumn[] columns = table.getLines(true);
		MUNSImportSimpleColumn column = null;
		for(int i=0;i<columns.length;i++)
		{
			if(columns[i].getAD_Column().getColumnName().equals("C_Order_ID"))
			{
				column = columns[i];
				break;
			}
		}
		
		if(column == null)
		{
			column = new MUNSImportSimpleColumn(getCtx(), 0, get_TrxName());
			column.setUNS_ImportSimpleTable_ID(table.get_ID());
			column.setName("UNT");
			column.setAD_Column_ID(MColumn.getColumn_ID(MOrderLine.Table_Name, "C_Order_ID"));
			column.setColumnNo(6969);
			column.setAD_Reference_ID(11);
			column.setIsEmptyCell(true);
		}
		
		column.setDefaultValue(String.valueOf(order.get_ID()));
		column.saveEx();
		table.getLines(true);
		
		table.saveEx();
		
		SimpleImportXLS si = new SimpleImportXLS(getCtx(), simple, table, false, 0, 0, 0, getAD_Client_ID(), get_TrxName());;
		String success = null;
		try{
			 success = si.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		
		column.deleteEx(true);
		
//		if(upPriceList)
//		{
//			boolean isNewVersion = false;
//			MOrderLine[] lines = order.getLines(true, null);
//			MPriceListVersion v = MPriceListVersion.get(getCtx(), order.getM_PriceList_ID(), order.getDateOrdered(),
//					get_TrxName());
//			if(v == null)
//			{
//				v = new MPriceListVersion((MPriceList) order.getM_PriceList());
//				v.setValidFrom(order.getDateOrdered());
//				v.saveEx();
//				isNewVersion = true;
//			}
//			
//			for(int i=0;i<lines.length;i++)
//			{
//				MProductPrice pp = null;
//				if(!isNewVersion)
//					pp = MProductPrice.get(getCtx(), v.get_ID(), lines[i].getM_Product_ID(), get_TrxName());
//				if(pp == null)
//					pp = new MProductPrice(getCtx(), v.get_ID(), lines[i].getM_Product_ID(), get_TrxName());
//				
//				pp.setPriceList(lines[i].getPriceList());
//				pp.setPriceStd(lines[i].getPriceList());
//				pp.setPriceLimit(lines[i].getPriceList());
//				pp.saveEx();
//			}
//		}
		
		return success;
	}
}