package com.unicore.model.process;

import java.math.BigDecimal;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.globalqss.model.MLCOInvoiceWithholding;


public class RoundingAndScale extends SvrProcess{
	
	private boolean m_isAllTax = false;
	private int C_Tax_ID = 0;
	private int m_roundMode = 0;
	private int m_roundScale = 0;
	
	

	public RoundingAndScale() {
		super ();
	}

	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			if (param.getParameterName() == null);
			else if (param.getParameterName().equals("C_Tax_ID"))
				C_Tax_ID = param.getParameterAsInt();
			else if (param.getParameterName().equals("m_roundMode"))
				m_roundMode = param.getParameterAsInt();
			else if (param.getParameterName().equals("m_roundScale"))
				m_roundScale = param.getParameterAsInt();
			else if (param.getParameterName().equals("allTax"))
				m_isAllTax = param.getParameterAsBoolean();
			else {
				log.log(Level.SEVERE, "Unknowon Parameter" + param.getParameterName());
			}
		}
	}

	@Override
	protected String doIt() throws Exception {

		String whereClause = null;
		
		if (!m_isAllTax) {
			whereClause = " AND C_Tax_ID=" + C_Tax_ID;
		}
		
		int counter = 0;
		for(MLCOInvoiceWithholding invWithholding : MLCOInvoiceWithholding.getOf(
				getCtx(), getRecord_ID(), whereClause, get_TrxName()))
		{
			
			BigDecimal roundedTax = invWithholding.getTaxAmt();
			
			if (m_roundScale > 0)
			{
				BigDecimal bdRoundingScale = BigDecimal.valueOf(m_roundScale);
				
				roundedTax = roundedTax.divide(bdRoundingScale, 0, m_roundMode);
				
				roundedTax = roundedTax.multiply(bdRoundingScale);
			} else  {
				int absRound = m_roundScale * -1;
				roundedTax = roundedTax.setScale(absRound, m_roundMode);
			}
			
			invWithholding.setTaxAmt(roundedTax);
			if (!invWithholding.save())
			{
				throw new AdempiereException("Failed saving Invoice Withholding.");
			}
			
			counter++;
		}
		
		return counter + " record updated";
	}
}
