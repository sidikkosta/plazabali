/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import com.unicore.model.MUNSPOSSession;

/**
 * @author nurse
 *
 */
public class TakeOverSession extends SvrProcess {

	/**
	 * 
	 */
	public TakeOverSession() 
	{
		super ();
	}
	
	private int p_cashierID;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("Cashier_ID".equals(params[i].getParameterName()))
				p_cashierID = params[i].getParameterAsInt();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSPOSSession session = new MUNSPOSSession(getCtx(), getRecord_ID(), get_TrxName());
		if (!DocAction.STATUS_InProgress.equals(session.getDocStatus()))
			return "Process only can be run when session status is drafted";
		if (!session.doTakeOver(p_cashierID))
			throw new AdempiereException();
		return "Success";
	}

}
