/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

import com.unicore.model.MUNSPOSPayment;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSSessionCashAccount;
import com.unicore.model.MUNSSessionEDCSummary;

/**
 * @author Burhani Adam
 *
 */
public class UNSReInitPOSSession extends SvrProcess {
	
	private Timestamp p_DateFrom = null;
	private Timestamp p_DateTo = null;
	private IProcessUI m_process;
	private BigDecimal nol = Env.ZERO;

	/**
	 * 
	 */
	public UNSReInitPOSSession() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			String name = params[i].getParameterName();
			if ("DateFrom".equals(name))
			{
				p_DateFrom = params[i].getParameterAsTimestamp();
			}
			else if("DateTo".equals(name))
			{
				p_DateTo = params[i].getParameterAsTimestamp();
			}
			else
			{
				log.log(Level.SEVERE, "Unknown parameter " + name);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_process = Env.getProcessUI(getCtx());
		
		String sql = "SELECT COUNT(*) FROM UNS_POS_Session WHERE DateAcct BETWEEN ? AND ?"
				+ " AND DocStatus IN ('CO', 'CL')";
		int count = DB.getSQLValue(get_TrxName(), sql, new Object[]{p_DateFrom, p_DateTo});
		
		sql = sql.replace("COUNT(*)", "UNS_POS_Session_ID");
		sql += " ORDER BY DateAcct";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setTimestamp(1, p_DateFrom);
			stmt.setTimestamp(2, p_DateTo);
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSPOSSession ss = new MUNSPOSSession(getCtx(), rs.getInt(1), get_TrxName());
				m_process.statusUpdate("Re init " + count + " records. #" + rs.getRow() + "::" + ss.getDateAcct());
				setToZeroCashAcct(ss);setToZeroEDCSum(ss);setToZeroSession(ss);
				MUNSPOSTrx[] trxs = ss.getLines(true);
				BigDecimal roundAmt = nol;
				for(int i=0;i<trxs.length;i++)
				{
					if(!trxs[i].getDocStatus().equals("CO") && !trxs[i].getDocStatus().equals("CL"))
						continue;
					MUNSPOSPayment pp = MUNSPOSPayment.get(getCtx(), trxs[i].get_ID(), get_TrxName());
					if(pp != null && pp.getDocStatus().equals("CO") || pp.getDocStatus().equals("CL"))
					{
						pp.processLines();
						roundAmt = roundAmt.add(pp.getRoundingAmt());
					}
					MUNSPOSSession.updateByPOSTrx(trxs[i]);
				}
				
				MUNSSessionCashAccount[] accounts = ss.getCashAcounts(true);
				for (int i=0; i<accounts.length; i++)
				{
					BigDecimal cashDepositAmt = accounts[i].getEndingBalance().subtract(accounts[i].getBeginningBalance());
					accounts[i].setCashDepositAmt(cashDepositAmt);
					accounts[i].setEndingBalance(accounts[i].getBeginningBalance());
					accounts[i].setRecalcEndingBalance(false);
					accounts[i].save();
				}
				
				DB.executeUpdate("UPDATE UNS_POS_Session SET RoundingAmt = ? WHERE UNS_POS_Session_ID = ?",
						new Object[]{roundAmt, ss.get_ID()}, false, get_TrxName());
				Trx trx = Trx.get(get_TrxName(), false);
				trx.commit();
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		return "Success re init " + count + " records.";
	}
	
	private void setToZeroSession(MUNSPOSSession ss)
	{
		ss.setTotalSalesB1(nol);
		ss.setTotalSalesB2(nol);
		ss.setTotalDiscAmt(nol);
		ss.setCashSalesAmt(nol);
		ss.setTotalServiceCharge(nol);
		ss.setEDCSalesAmt(nol);
		ss.setTotalTaxAmt(nol);
		ss.setTotalRefundTaxAmt(nol);
		ss.setVoucherAmt(nol);
		ss.setReturnAmt(nol);
		ss.setPayableCashRefund(nol);
		ss.setPayableEDCRefund(nol);
		ss.setTotalShortCashierAmt(nol);
		ss.setTotalEstimationShortCashierAmt(nol);
		ss.setTotalRefundSvcChgAmt(nol);
		ss.setRoundingAmt(nol);
		ss.save();
	}
	private void setToZeroCashAcct(MUNSPOSSession ss)
	{
		MUNSSessionCashAccount[] cashs = ss.getCashAcounts(true);
		for(int i=0;i<cashs.length;i++)
		{
			cashs[i].setCashIn(nol);
			cashs[i].setCashOut(nol);
			cashs[i].setCashDepositAmt(nol);
			cashs[i].setPayableRefundAmt(nol);
			cashs[i].setCashInBase1(nol);
			cashs[i].setCashInBase2(nol);
			cashs[i].setCashOutBase1(nol);
			cashs[i].setCashOutBase2(nol);
			cashs[i].setCashDepositAmtB1(nol);
			cashs[i].setCashDepositAmtB2(nol);
			cashs[i].setPayableRefundAmtB1(nol);
			cashs[i].setPayableRefundAmtB2(nol);
			cashs[i].save();
			cashs[i].load(get_TrxName());
		}
	}
	private void setToZeroEDCSum(MUNSPOSSession ss)
	{
		MUNSSessionEDCSummary[] edcs = ss.getEDCSummary(true);
		for(int i=0;i<edcs.length;i++)
		{
			edcs[i].setTotalAmt(nol);
			edcs[i].setTotalAmtB1(nol);
			edcs[i].setTotalAmtB2(nol);
			edcs[i].setPayableRefundAmt(nol);
			edcs[i].setPayableRefundAmtB1(nol);
			edcs[i].setPayableRefundAmtB2(nol);
			edcs[i].save();
			edcs[i].load(get_TrxName());
		}
	}
}