/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSCustomerPoint;
import com.unicore.model.MUNSCustomerPointLine;
import com.unicore.model.MUNSPSProduct;
import com.unicore.model.MUNSPackingList;
import com.unicore.model.MUNSPackingListOrder;
import com.unicore.model.MUNSPointSchema;
import com.unicore.model.MUNSPointSchemaLine;

/**
 * @author ALBURHANY
 *
 */
public class CalculatePointAll extends SvrProcess {
	
	private Timestamp m_StartDate;
	private Timestamp m_EndDate;
	private int m_Product_Category_ID;
	private MUNSPointSchema m_pointschema = null;
	private MUNSPSProduct m_PSProduct = null;
	private MUNSCustomerPoint m_CustomerPoint = null;
	private MUNSCustomerPointLine m_CustomerPointLine = null;
	private MUNSPointSchemaLine m_PointSchemaLine = null;
	private boolean m_isOneShipment = false;
	private String pointType = null;
	private BigDecimal point = Env.ZERO;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("StartDate"))
				m_StartDate = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("EndDate"))
				m_EndDate = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("M_Product_Category_ID"))
				m_Product_Category_ID = param.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
		String pSchema = "SELECT UNS_PointSchema_ID FROM UNS_PointSchema";
		int idpShcema = DB.getSQLValue(get_TrxName(), pSchema);
		
		m_pointschema = new MUNSPointSchema(getCtx(), idpShcema, get_TrxName());
		
		String sql = null;
		
		if(m_Product_Category_ID == 0)
		{
			sql = "C_Invoice_ID IN (SELECT C_Invoice_ID FROM C_Invoice WHERE isSOTrx = 'Y' AND"
					+ " DocStatus IN ('CO', 'CL') AND C_BPartner_ID IN (SELECT C_BPartner_ID FROM C_BPartner WHERE IsActive = 'Y')"
					+ " AND DateInvoiced BETWEEN '" + m_StartDate + "' AND '" + m_EndDate + "')";
		}
		else if(m_Product_Category_ID != 0)
		{
			sql = "C_Invoice_ID IN (SELECT C_Invoice_ID FROM C_Invoice WHERE isSOTrx = 'Y' AND"
					+ " DocStatus IN ('CO', 'CL') AND C_BPartner_ID IN (SELECT C_BPartner_ID FROM C_BPartner WHERE IsActive = 'Y')"
					+ " AND DateInvoiced BETWEEN '" + m_StartDate + "' AND '" + m_EndDate + "')"
					+ " AND C_Invoice_ID IN (SELECT C_Invoice_ID FROM C_InvoiceLine WHERE M_Product_ID IN"
					+ " (SELECT M_Product_ID FROM M_Product WHERE M_Product_Category_ID = " + m_Product_Category_ID + "))";
		}
		
		List<MInvoice> listInv = new Query(getCtx(), MInvoice.Table_Name, sql, get_TrxName()).list();
		
		for(MInvoice inv : listInv)
		{			
			if(!cekPointSchema(getCtx(), inv, get_TrxName()))
				return null;
			
			if(inv.getC_DocTypeTarget().getDocBaseType().equals(MDocType.DOCBASETYPE_ARInvoice))
			{					
				int m_Revesal_ID = inv.getReversal_ID();
				if(m_Revesal_ID > 0)
					return null;
				
				for (MInvoiceLine invL : inv.getLines(true))
				{
					String whereClausePSProduct = "(M_Product_ID = " + invL.getM_Product_ID()
							+ " OR M_Product_Category_ID = " + invL.getM_Product().getM_Product_Category_ID() + ")";
					
					String orderClausePSProduct = "M_Product_ID";
					m_PSProduct = m_pointschema.getLine(whereClausePSProduct, orderClausePSProduct);
	
					if(m_PSProduct == null)
						continue;
					else
						point = calculated(getCtx(), m_isOneShipment,
									m_PSProduct.get_ID(), invL.getQtyInvoiced(), get_TrxName());
				
					m_CustomerPoint = MUNSCustomerPoint.getByBPartner(getCtx(), inv.getC_BPartner_ID(),
							inv.getAD_Org_ID(), get_TrxName());
					
					if (m_CustomerPoint == null)
					{
						m_CustomerPoint = new MUNSCustomerPoint(inv);
					}
					
					m_CustomerPoint.setLatestTrxDate(inv.getDateInvoiced());
					m_CustomerPoint.saveEx();
					
					createCPLine(getCtx(), m_CustomerPoint, invL, get_TrxName());
				}
			}
			
			if(inv.getC_DocTypeTarget().getDocBaseType().equals(MDocType.DOCBASETYPE_ARCreditMemo))
			{									
				for(MInvoiceLine invLine : inv.getLines(true))
				{
					String whereClausePSProduct = "(M_Product_ID = " + invLine.getM_Product_ID()
							+ " OR M_Product_Category_ID = " + invLine.getM_Product().getM_Product_Category_ID() + ")";
					
					String orderClausePSProduct = "M_Product_ID";
					m_PSProduct = m_pointschema.getLine(whereClausePSProduct, orderClausePSProduct);
	
					if(m_PSProduct == null)
						continue;
					else
						point = (calculated(getCtx(), m_isOneShipment,
									m_PSProduct.get_ID(), invLine.getQtyInvoiced(), get_TrxName())).negate();
					
					m_CustomerPoint = MUNSCustomerPoint.getByBPartner(getCtx(), inv.getC_BPartner_ID(),
							inv.getAD_Org_ID(), get_TrxName());
					
					if (m_CustomerPoint == null)
					{
						m_CustomerPoint = new MUNSCustomerPoint(inv);
					}
					
					m_CustomerPoint.setLatestTrxDate(inv.getDateInvoiced());
					m_CustomerPoint.saveEx();
					
//					m_CustomerPointLine = MUNSCustomerPointLine.getByInvoiceLine(po.getCtx(), invLine.get_ID(),
//							po.get_TrxName());
					
					createCPLine(getCtx(), m_CustomerPoint, invLine, get_TrxName());
				}
			}
		}
		return null;
	}
	
	public boolean cekPointSchema(Properties ctx, MInvoice inv, String trxName)
	{
		m_pointschema = null;
	
		MUNSPackingListOrder plOrder = MUNSPackingListOrder.getByInvoice(ctx, inv.get_ID(), trxName);
		
		int idPL = 0;
		
		if(plOrder != null)
		{
			m_isOneShipment = plOrder.isOneShipment();
			idPL = plOrder.getUNS_PackingList_ID();				
		}
		if(plOrder == null)
		{
			if(inv.getC_Order_ID() <= 0)
			{
				m_isOneShipment = false;
				idPL = 0;
			}
			else
			{
				m_isOneShipment = inv.getC_Order().isOneShipment();
				idPL = 0;
			}
		}
		
		if(idPL > 0)
		{
			MUNSPackingList pl = new MUNSPackingList(ctx, idPL, trxName);
			pointType = pl.getLines(null, null).length == 1 ? "OSOC" : "OSMC";
		}
		else
			pointType = "OSMC";
		
		m_pointschema = MUNSPointSchema.get(ctx, inv.getC_BPartner_ID(), pointType, trxName);
		
		if(m_pointschema.get_ID() <= 0)
			return false;
		
		return true;
	}
	
	public BigDecimal calculated (Properties ctx,
			boolean isOneShipment, int UNS_PS_Product_ID, BigDecimal QtyInv, String trxName)
	{
		BigDecimal point = Env.ZERO;
		BigDecimal Quantity = Env.ZERO;
		BigDecimal getpoint = Env.ZERO;
		BigDecimal pointProposional = Env.ZERO;
		Quantity = QtyInv;
		
		m_PSProduct = new MUNSPSProduct(ctx, UNS_PS_Product_ID, trxName);
		int length = m_PSProduct.getLine().length;
		
		for (int i = 0; i < m_PSProduct.getLine().length;)
		{
			m_PointSchemaLine = new MUNSPointSchemaLine(ctx, m_PSProduct.getLine()[i].get_ID(), trxName);
			
			if(Quantity.compareTo(m_PointSchemaLine.getQty()) == 1)
			{
				point = Quantity.divide(m_PointSchemaLine.getQty(), 0, BigDecimal.ROUND_FLOOR);
				getpoint = getpoint.add((point.multiply(m_PointSchemaLine.getValuePoint())));
				Quantity = Quantity.subtract(point.multiply(m_PointSchemaLine.getQty()));
				
				if(isOneShipment && Quantity.compareTo(Env.ZERO) == 1)
				{
					pointProposional = m_PointSchemaLine.getValuePoint().divide(m_PointSchemaLine.getQty());
					getpoint =  getpoint.add(Quantity.multiply(pointProposional));
					break;
				}
			}
			if(Quantity.compareTo(m_PointSchemaLine.getQty()) == 0)
			{
				getpoint = getpoint.add(m_PointSchemaLine.getValuePoint());
				break;
			}
			if(Quantity.compareTo(m_PointSchemaLine.getQty()) == -1
					&& Quantity.compareTo(Env.ZERO) == 1)
			{
				if(length - i == 1)
				{
					if(m_PSProduct.isCalcRemainingQty())
					{
						pointProposional = m_PointSchemaLine.getValuePoint().divide(m_PointSchemaLine.getQty());
						getpoint = getpoint.add(Quantity).multiply(pointProposional);
					}
				}
			}
			i++;
		}
		return getpoint;
	}
	
	public boolean createCPLine(Properties ctx, MUNSCustomerPoint cPoint,
			MInvoiceLine invLine, String trxName)
	{
		m_CustomerPointLine = MUNSCustomerPointLine.getByInvoiceLine(ctx, invLine.get_ID(), trxName);
		
		if(m_CustomerPointLine == null)
		{
			m_CustomerPointLine = new MUNSCustomerPointLine(ctx, 0, trxName);
			m_CustomerPointLine.setUNS_CustomerPoint_ID(cPoint.get_ID());
			m_CustomerPointLine.setPoint(point);
			m_CustomerPointLine.setUNS_PointSchema_ID(m_pointschema.get_ID());
			m_CustomerPointLine.setM_Product_ID(invLine.getM_Product_ID());
			m_CustomerPointLine.setQty(invLine.getQtyInvoiced());
			m_CustomerPointLine.setDateInvoiced(invLine.getC_Invoice().getDateInvoiced());
			m_CustomerPointLine.setC_InvoiceLine_ID(invLine.getC_InvoiceLine_ID());
			m_CustomerPointLine.saveEx();
		}
		else
		{
			m_CustomerPointLine.setM_Product_ID(invLine.getM_Product_ID());
			m_CustomerPointLine.setQty(invLine.getQtyInvoiced());
			m_CustomerPointLine.setPoint(point);
			m_CustomerPointLine.saveEx();
		}
		
		return true;
	}
}