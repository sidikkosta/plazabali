/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import com.unicore.model.MUNSPOSFNBTable;
import com.uns.util.MessageBox;

/**
 * @author nurse
 *
 */
public class POSFNBTableGenerator extends SvrProcess {

	private String p_prefix = "Table ";
	private int p_start = 1;
	private int p_end = 20;
	/**
	 * 
	 */
	public POSFNBTableGenerator() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if (params[i].getParameterName().equals("Prefix"))
				p_prefix = params[i].getParameterAsString();
			else if (params[i].getParameterName().equals("Start"))
				p_start = params[i].getParameterAsInt();
			else if (params[i].getParameterName().equals("End"))
				p_end = params[i].getParameterAsInt();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSPOSFNBTable table = new MUNSPOSFNBTable(getCtx(), getRecord_ID(), get_TrxName());
		String sql = "SELECT COUNT (*) FROM UNS_POSFNBTableLine WHERE UNS_POSFNBTable_ID = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, getRecord_ID());
		if (count > 0)
		{
			int result = MessageBox.showMsg(getCtx(), getProcessInfo(), 
					"Table already generated before, do you want to remove the record? ",  
					"Delete Confirmation", MessageBox.YESNO, MessageBox.ICONINFORMATION);
			if (result == MessageBox.RETURN_YES)
			{
				sql = "DELETE FROM UNS_POSFNBTableLine WHERE UNS_POSFNBTable_ID = ?";
				result = DB.executeUpdate(sql, getRecord_ID(), get_TrxName());
				if (result == -1)
					throw new AdempiereException();
			}
			
		}
		if (!table.generateTable(p_prefix, p_start, p_end))
			throw new AdempiereException();
		return p_end + " table generated";
	}

}
