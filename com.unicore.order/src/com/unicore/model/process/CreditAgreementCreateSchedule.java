/**
 * 
 */
package com.unicore.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSCreditAgreement;

/**
 * @author Menjangan
 *
 */
public class CreditAgreementCreateSchedule extends SvrProcess {

	
	private MUNSCreditAgreement m_model = null;
	/**
	 * 
	 */
	public CreditAgreementCreateSchedule() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		m_model = new MUNSCreditAgreement(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (!m_model.doCreateSchedule(false))
			throw new AdempiereException(m_model.getProcessMsg());
		
		return "success";
	}

}
