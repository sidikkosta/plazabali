/**
 * 
 */
package com.unicore.model.process;

import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSPLConfirm;
import com.unicore.model.MUNSPackingList;
import com.unicore.model.MUNSPackingListLine;
import com.unicore.model.MUNSPackingListOrder;

/**
 * @author ALBURHANY
 *
 */
public class ProcessReversePL extends SvrProcess {
	
	private boolean reverseAll;
	String m_message = "Success";

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("ReverseAll"))
				reverseAll = param.getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
		if(reverseAll)
		{
			deletePLConfirm();
			reversePackingList();
			reverseOrder();
			reverseShipment();
			reverseInvoice();
		}
		
		if(!reverseAll)
		{
			completeOrder();
			createPLConfrim();
			completePLConfirm();
		}
		
		return null;
	}
	
	private String deletePLConfirm ()
	{
		//delete PL Confirmation :: Product List
		String confirmPLLine ="DELETE FROM UNS_PL_ConfirmLine WHERE UNS_PL_ConfirmProduct_ID IN "
				+ "(SELECT UNS_PL_ConfirmProduct_ID FROM UNS_PL_ConfirmProduct WHERE"
				+ " UNS_PL_Confirm_ID IN (SELECT UNS_PL_Confirm_ID FROM UNS_PL_Confirm WHERE"
				+ " UNS_PackingList_ID = " + getRecord_ID() + "))";
		int delPLConfirmLine = DB.executeUpdate(confirmPLLine, get_TrxName());
		
		if(delPLConfirmLine <=-1)
		{
			m_message = "Error Query when DELETE Packing List Confirmation Line";
			throw new AdempiereException(m_message);
		}
		String confirmPL = "Delete FROM UNS_PL_ConfirmProduct WHERE UNS_PL_Confirm_ID IN"
				+ " (SELECT UNS_PL_Confirm_ID FROM UNS_PL_Confirm"
				+ " WHERE UNS_PackingList_ID = " + getRecord_ID() + ")";
		int delPLConfirm = DB.executeUpdate(confirmPL, get_TrxName());
		
		if(delPLConfirm <= -1)
		{
			m_message = "Error Query when Delete Packing List Confirmation :: Product List";
			throw new AdempiereException(m_message);
		}
		
		//delete PL Confirmation :: PL Confirmation
		String confirm = "Delete FROM UNS_PL_Confirm WHERE UNS_PackingList_ID = " + getRecord_ID();
		int delConfirm = DB.executeUpdate(confirm, get_TrxName());
		
		if(delConfirm <= -1)
		{
			m_message = "Error Query when Delete Packing List Confirmation";
			throw new AdempiereException(m_message);
		}
		
		return null;
	}
	
	private String reversePackingList()
	{
		//drafted PackingList
		MUNSPackingList pList = new MUNSPackingList(getCtx(), getRecord_ID(), get_TrxName());
		
		String whereClause = " UNS_PackingList_ID = ?";
		List<MUNSPackingListOrder> lOrder = new Query(getCtx(), MUNSPackingListOrder.Table_Name, whereClause, get_TrxName())
												.setParameters(getRecord_ID())
													.list();
		for (MUNSPackingListOrder listOrder : lOrder)
		{
			String whereClaus = " UNS_PackingList_Order_ID = ?";
			List<MUNSPackingListLine> plLine = new Query(getCtx(), MUNSPackingListLine.Table_Name, whereClaus, get_TrxName())
													.setParameters(listOrder.get_ID())
														.list();
			for (MUNSPackingListLine listLine : plLine)
			{
				String whereClau = " C_OrderLine_ID = ?";
				
				List<org.compiere.model.MOrderLine> oLine = new Query(getCtx(), org.compiere.model.MOrderLine.Table_Name, whereClau, get_TrxName())
												.setParameters(listLine.getC_OrderLine_ID())
													.list();
				for (org.compiere.model.MOrderLine line : oLine)
				{
					line.setQtyPacked(listLine.getMovementQty());
					line.saveEx();
				}
			}
		}
		
		pList.setProcessed(false);
		pList.setDocStatus("DR");
		pList.setDocAction(DocAction.ACTION_Complete);
		pList.save();
		
		if(!pList.save())
			m_message = "Error when Reactivate Packing List";
		
		return m_message;
	}
	
	private String reverseOrder ()
	{
		//reactivate Order
		MUNSPackingList pList = new MUNSPackingList(getCtx(), getRecord_ID(), get_TrxName());
		for (MUNSPackingListOrder listOrder : pList.getLines(null, null))
		{
			MOrder order = new MOrder(getCtx(), listOrder.getC_Order_ID(), get_TrxName());
			
			for(org.compiere.model.MOrderLine oLine : order.getLines())
			{
//				String sql = "SELECT UNS_PackingList_Line_ID FROM UNS_PackingList_Line"
//						+ " WHERE C_OrderLine_ID = " + oLine.get_ID()
//						+ " AND UNS_PackingList_Order_ID = " + listOrder.get_ID();
//				MUNSPackingListLine listLine = new MUNSPackingListLine(getCtx(), DB.getSQLValue(get_TrxName(), sql),
//						get_TrxName());
				
				oLine.setQtyReserved(Env.ZERO);	
//				if(listOrder.getC_Invoice().getDocStatus().equals("CO")
//						|| listOrder.getC_Invoice().getDocStatus().equals("CL"))
//				oLine.setQtyInvoiced(oLine.getQtyInvoiced().subtract(listLine.getC_InvoiceLine().getQtyInvoiced()));
//				
//				if(listOrder.getM_InOut().getDocStatus().equals("CO")
//						|| listOrder.getM_InOut().getDocStatus().equals("CL"))
//				oLine.setQtyDelivered(oLine.getQtyDelivered().subtract(listLine.getM_InOutLine().getQtyEntered()));
				oLine.setProcessed(false);
				oLine.saveEx();
			}
			
			order.setDocStatus("DR");
			order.setProcessed(false);
			order.setDocAction(DocAction.ACTION_Complete);
			order.saveEx();
		}
		
		return m_message;
	}
	
	private String reverseShipment()
	{
		//reactivate Shipment
		MUNSPackingList pList = new MUNSPackingList(getCtx(), getRecord_ID(), get_TrxName());
		for(MUNSPackingListOrder listOrder : pList.getLines(null, null))
		{
			MInOut io = new MInOut(getCtx(), listOrder.getM_InOut_ID(), get_TrxName());
			
			for(MInOutLine iol : io.getLines())
			{
				iol.setProcessed(false);
				iol.saveEx();
			}
			io.setProcessed(false);
			io.setDocStatus("DR");
			io.setDocAction(DocAction.ACTION_Complete);
			if(!io.save())
				throw new AdempiereException("Error when reactivate Shipment");
		}
		
		return m_message;
	}
	
	private String reverseInvoice()
	{
		//reactivate invoice
		MUNSPackingList pList = new MUNSPackingList(getCtx(), getRecord_ID(), get_TrxName());
		for(MUNSPackingListOrder listOrder : pList.getLines(null, null))
		{
			MInvoice inv = new MInvoice(getCtx(), listOrder.getC_Invoice_ID(), get_TrxName());
			
			for(MInvoiceLine invL : inv.getLines())
			{
				invL.setProcessed(false);
				invL.saveEx();
			}
			
			inv.setProcessed(false);
			inv.setDocStatus("DR");
			inv.setDocAction(DocAction.ACTION_Complete);
			if(!inv.save())
				throw new AdempiereException("Error when reactivate Invoice");
		}
		
		return m_message;
	}
	
	private String completeOrder()
	{
		//complete Order
		String whereClause = " UNS_PackingList_ID = ?";
		List<MUNSPackingListOrder> lOrder = new Query(getCtx(), MUNSPackingListOrder.Table_Name, whereClause, get_TrxName())
												.setParameters(getRecord_ID())
													.list();
		for (MUNSPackingListOrder listOrder : lOrder)
		{
			MOrder order = new MOrder(getCtx(), listOrder.getC_Order_ID(), get_TrxName());
			order.processIt(DocAction.ACTION_Complete);
			order.saveEx();
		}
		
		return m_message;
	}
	
	private String createPLConfrim()
	{
		//create PL Confirm
		MUNSPackingList pList = new MUNSPackingList(getCtx(), getRecord_ID(), get_TrxName());
		
		for(MUNSPackingListOrder plOrder : pList.getLines(null, null))
		{
			for(MUNSPackingListLine pLine : plOrder.getLines())
			{
				MInvoiceLine invL = new MInvoiceLine(getCtx(), pLine.getC_InvoiceLine_ID(), get_TrxName());
				MInOutLine mIOLine = new MInOutLine(getCtx(), pLine.getM_InOutLine_ID(), get_TrxName());
				
				if(pLine.getM_Product_ID() != invL.getM_Product_ID())
				{
					invL.setM_Product_ID(pLine.getM_Product_ID(), pLine.getM_Product().getC_UOM_ID());
					invL.saveEx();
				}
				
				if(pLine.getQtyEntered() != invL.getQtyEntered())
				{
					invL.setQtyEntered(pLine.getQtyEntered());
					invL.saveEx();
				}
				
				if(pLine.getM_Product_ID() != mIOLine.getM_Product_ID())
				{
					mIOLine.setM_Product_ID(pLine.getM_Product_ID(), pLine.getM_Product().getC_UOM_ID());
					mIOLine.saveEx();
				}
				
				if(pLine.getQtyEntered() != mIOLine.getQtyEntered())
				{
					mIOLine.setQtyEntered(pLine.getQtyEntered());
					mIOLine.saveEx();
				}
			}
		}
		
		pList.prepareIt();
		pList.processIt(DocAction.ACTION_Complete);
		pList.saveEx();
		
		return m_message;
	}
	
	private String completePLConfirm()
	{
		//complete PL Confirm
		String sql = "SELECT UNS_PL_Confirm_ID FROM UNS_PL_Confirm WHERE UNS_PackingList_ID = " + getRecord_ID();
		int idPLConfirm = DB.getSQLValue(get_TrxName(), sql.toString());
		
		MUNSPLConfirm plConfirm = new MUNSPLConfirm(getCtx(), idPLConfirm, get_TrxName());
		
		plConfirm.setDateDoc(plConfirm.getUNS_PackingList().getDateDoc());
		plConfirm.processIt(DocAction.ACTION_Complete);
		plConfirm.saveEx();
		
		return m_message;
	}
}