/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.unicore.base.model.MInOut;

/**
 * @author ALBURHANY
 *
 */
public class ProcessReferenceInv extends SvrProcess {

	private String m_ReferenceNo;
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {

		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("ReferenceNo"))
				m_ReferenceNo = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {

		MInOut io = new MInOut(getCtx(), getRecord_ID(), get_TrxName());
		
			MInvoice inv = new MInvoice(getCtx(), io.getC_Invoice_ID(), get_TrxName());
			inv.setReferenceNo(m_ReferenceNo);
			inv.saveEx();
		
		return null;
	}

}
