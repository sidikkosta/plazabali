/**
 * 
 */
package com.unicore.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSCreditAgreement;

/**
 * @author Menjangan
 *
 */
public class UNSRecoveryCreditAgreementInvoice extends SvrProcess {

	private Timestamp p_from = null;
	private Timestamp p_to = null;
	private boolean p_reorderPaidSchedule = false;
	
	
	/**
	 * 
	 */
	public UNSRecoveryCreditAgreementInvoice() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("DateFrom".equals(params[i].getParameterName()))
				p_from = params[i].getParameterAsTimestamp();
			else if ("DateTo".equals(params[i].getParameterName()))
				p_to = params[i].getParameterAsTimestamp();
			else if ("ReorderPaidSchedule".equals(params[i].getParameterName()))
				p_reorderPaidSchedule = params[i].getParameterAsBoolean();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSCreditAgreement[] agreements = MUNSCreditAgreement.getCompletedByDate(
				get_TrxName(), getCtx(), p_from, p_to);
		for (int i=0; i<agreements.length; i++)
			if (!agreements[i].doCreateSchedule(p_reorderPaidSchedule))
				throw new Exception(agreements[i].getDocumentInfo() 
						+ "::" + agreements[i].getProcessMsg());
		return "OK";
	}

}
