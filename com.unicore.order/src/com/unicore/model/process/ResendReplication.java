/**
 * 
 */
package com.unicore.model.process;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.adempiere.exceptions.AdempiereException;
import org.adempiere.process.rpl.exp.ExportHelper;
import org.adempiere.process.rpl.exp.TopicExportProcessor;
import org.compiere.model.MEXPFormat;
import org.compiere.model.MReplicationStrategy;
import org.compiere.model.MTable;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.w3c.dom.Document;

/**
 * @author nurse
 *
 */
public class ResendReplication extends SvrProcess 
{

	private String p_protocol = "tcp";
	private String p_host = "172.19.1.188";
	private int p_port = 2222;
	private String p_user = "admin";
	private String p_password = "admin";
	private String p_topicName = "ActiveMQ.DLQ.ReExport.ForMe";
	private String p_clientID = "ENDPIO-REEXPORT";
	private int p_timeToLive = 2100000000;
	private int p_acknowledgeMode = 0;
	private int p_expFormatID = 1000391;
	private int p_replicationEvent = -1;
	private String p_replicationType = null;
	private int p_replicationMode = -1;
	private String p_uu = null;
	private String p_customWhere = null;
	
	/**
	 * 
	 */
	public ResendReplication() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("Protocol".equals(params[i].getParameterName()))
				p_protocol = params[i].getParameterAsString();
			else if ("Host".equals(params[i].getParameterName()))
				p_host = params[i].getParameterAsString();
			else if ("Port".equals(params[i].getParameterName()))
				p_port = params[i].getParameterAsInt();
			else if ("User".equals(params[i].getParameterName()))
				p_user = params[i].getParameterAsString();
			else if ("Password".equals(params[i].getParameterName()))
				p_password = params[i].getParameterAsString();
			else if ("TopicName".equals(params[i].getParameterName()))
				p_topicName = params[i].getParameterAsString();
			else if ("ClientID".equals(params[i].getParameterName()))
				p_clientID = params[i].getParameterAsString();
			else if ("TimeToLive".equals(params[i].getParameterName()))
				p_timeToLive = params[i].getParameterAsInt();
			else if ("AcknowledgeMode".equals(params[i].getParameterName()))
				p_acknowledgeMode = params[i].getParameterAsInt();
			else if ("ReplicationMode".equals(params[i].getParameterName()))
				p_replicationMode = params[i].getParameterAsInt();
			else if ("ReplicationEvent".equals(params[i].getParameterName()))
				p_replicationEvent = params[i].getParameterAsInt();
			else if ("ReplicationType".equals(params[i].getParameterName()))
				p_replicationType = params[i].getParameterAsString();
			else if ("UU_Record".equals(params[i].getParameterName()))
				p_uu = params[i].getParameterAsString();
			else if ("EXP_Format_ID".equals(params[i].getParameterName()))
				p_expFormatID = params[i].getParameterAsInt();
			else if ("WhereClause".equals(params[i].getParameterName()))
				p_customWhere = params[i].getParameterAsString();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MEXPFormat expFormat = new MEXPFormat(getCtx(), p_expFormatID, get_TrxName());
		MTable table = MTable.get(getCtx(), expFormat.getAD_Table_ID());
		StringBuilder builder = new StringBuilder();
		if (!Util.isEmpty(p_uu, true))
		{
			builder.append(table.getTableName()).append("_UU = '").append(p_uu).append("'");
		}
		if (!Util.isEmpty(p_customWhere, true))
		{
			if (builder.length() > 0)
				builder.append(" AND ");
			builder.append(p_customWhere);
		}
		String where = builder.toString();
		boolean isReplicationDec = MReplicationStrategy.REPLICATION_DOCUMENT == p_replicationMode;
		String currentStatus = null;
		String currentAction = null;
		String prevStatus = null;
		String prevAction = null;
		String sql = null;
		if (isReplicationDec)
		{
			StringBuilder builder2 = new StringBuilder("SELECT DocStatus, DocAction FROM ");
			builder2.append(table.getTableName()).append(" WHERE ").append(where);
			sql = builder2.toString();
			List<List<Object>> record = DB.getSQLArrayObjectsEx(get_TrxName(), sql);
			if (record != null && record.size() > 0)
			{
				currentStatus = record.get(0).get(0).toString();
				currentAction = record.get(0).get(1).toString();
			}
		}
		
		if (!Util.isEmpty(currentStatus, true) && !Util.isEmpty(currentAction, true))
		{
			StringBuilder b2 = new StringBuilder("UPDATE ");
			b2.append(table.getTableName()).append(" SET DocStatus = ?, DocAction = ? WHERE ")
			.append(where);
			sql = b2.toString();
			prevStatus = initPrevStatus(currentStatus);
			prevAction = initPrevAction(currentAction);
			int exec = DB.executeUpdate(sql, new Object[]{prevStatus, prevAction}, false, get_TrxName());
			if (exec == -1)
				throw new AdempiereException("Could not update POS Session");
		}
		processUI.statusUpdate("load record from DB");
		ExportHelper helper = new ExportHelper(getCtx(), getAD_Client_ID());
		List<String> uus = loadUU(table.getTableName(), where);
		for(int i=0; i<uus.size(); i++)
		{
			int count = i+1;
			processUI.statusUpdate("Processing " + count + " from " + uus.size());
			Document doc = helper.exportRecord(expFormat,
					table.getTableName() + "_UU = '" + uus.get(i) + "'",
					p_replicationMode, p_replicationType ,p_replicationEvent);
			TopicExportProcessor tex = new TopicExportProcessor();
			tex.process(getCtx(), p_host, p_port, p_user, p_password, p_protocol, p_topicName, p_clientID, 
						p_timeToLive, true, p_acknowledgeMode, false, doc, 
						Trx.get(get_TrxName(), false));
		}
		
		if (!Util.isEmpty(currentStatus, true) && !Util.isEmpty(currentAction, true))
		{
			StringBuilder b2 = new StringBuilder("UPDATE ");
			b2.append(table.getTableName()).append(" SET DocStatus = ?, DocAction = ? WHERE ").append(where);
			sql = b2.toString();
			int exec = DB.executeUpdate(sql, new Object[]{currentStatus, currentAction}, false, get_TrxName());
			if (exec == -1)
				throw new AdempiereException("Could not update POS Session");
		}
		
		return null;
	}
	
	private List<String> loadUU (String tableName, String where)
	{
		List<String> uus = new ArrayList<>();
		String sql = "SELECT " + tableName + "_UU FROM " + tableName + " WHERE "
				+ where;
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			rs = st.executeQuery();
			while (rs.next())
				uus.add(rs.getString(1));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		return uus;
	}
	
	private String initPrevAction (String currentAction)
	{
		switch (currentAction)
		{
			case "PR" :
				return "PR";
			case "CO" :
				return "PR";
			case "CL" :
				return "CO";
			case "RE" :
				return "CO";
			case "VO" :
				return "CO";
			case "IN" :
				return "CO";
			case "--" :
				return "CL";
			default:
				return null;
		}
	}
	
	private String initPrevStatus (String currentStatus)
	{
		switch (currentStatus)
		{
			case "DR" :
				return "DR";
			case "IP" :
				return "DR";
			case "CO" :
				return "IP";
			case "CL" :
				return "CO";
			case "RE" :
				return "CO";
			case "VO" :
				return "CO";
			default:
				return null;
		}
	}
}
