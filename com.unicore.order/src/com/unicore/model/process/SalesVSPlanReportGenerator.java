/**
 * 
 */
package com.unicore.model.process;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.Adempiere;
import org.compiere.model.MClient;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

/**
 * @author nurse
 *
 */
public class SalesVSPlanReportGenerator extends SvrProcess 
{

	/**
	 * 
	 */
	public SalesVSPlanReportGenerator() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
		 	if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("Date".equals(params[i].getParameterName()))
				p_date = params[i].getParameterAsTimestamp();
			else if ("Shop_ID".equals(params[i].getParameterName()))
				p_shopIDs = params[i].getParameterAsString();
			else if ("Destination".equals(params[i].getParameterName()))
				p_destination = params[i].getParameterAsString();
			else if ("Recipient".equals(params[i].getParameterName()))
				p_recipient = params[i].getParameterAsString();
		}
	}
	
	private String[] m_to = null;
	private String[] m_cc = null;
	private String[] m_bcc = null;
	
	
	private String[] parseRecipient (String recipient)
	{
		String[] ret = recipient.split("&");
		if (ret == null || ret.length == 0)
			return null;
		
		return ret;
	}
	
	private void initRecipient ()
	{
		if (p_recipient == null)
			return;
		
		String[] recipient = p_recipient.split("\\|");
		if (recipient == null || recipient.length == 0)
			return;
		m_to = parseRecipient(recipient[0]);
		if (recipient.length > 1)
			m_cc = parseRecipient(recipient[1]);
		if (recipient.length > 2)
			m_bcc = parseRecipient(recipient[2]);			
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(p_shopIDs, true))
			throw new AdempiereException("Not set parameter shop IDs");
		if (p_date == null)
			p_date = new Timestamp(System.currentTimeMillis());
		m_dateToExport = TimeUtil.addDays(p_date, -1);
		if (!p_fileName.endsWith("_"))
			p_fileName += "_";
		initRecipient();
		if (m_to == null || m_to.length == 0)
			return "No recipient defined";
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(m_dateToExport.getTime());
		int offDaysMonthToDate = cal.get(Calendar.DATE);
		cal.set(Calendar.DATE, 1);
		Timestamp start = new Timestamp(cal.getTimeInMillis());
		int offDaysInMonth = cal.getActualMaximum(Calendar.DATE);
		int shouldBe = (int)((double)((double)offDaysMonthToDate / (double)offDaysInMonth) * 100);
		List<Integer> shopIds = parsingShopIDs(p_shopIDs, "\\|");
		List<ReportDataModel> monthToDate = loadSalesRecord(start, m_dateToExport, shopIds);
		if (monthToDate.size() == 0)
			return "No record generated";
		List<ReportDataModel> today = loadSalesRecord(m_dateToExport, m_dateToExport, shopIds);
		List<ReportDataModel> salesTarget = loadSalesTarget(
				MPeriod.getC_Period_ID(getCtx(), m_dateToExport, 
						Env.getAD_Org_ID(Env.getCtx())), shopIds);
		
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
		String dateStr = format.format(m_dateToExport);
		StringBuilder rsBuild = new StringBuilder("<pre>Sales Per-").append(dateStr)
				.append("\n--------------------\n").append("#OfDays Month To Date\t")
				.append(offDaysMonthToDate)
				.append("\n#OfDays In Month\t").append(offDaysInMonth)
				.append("\n%Should be\t").append(shouldBe).append("\n");
		StringBuilder sb1 = new StringBuilder("                    ");
		for (int i=0; i<5; i++)
		{
			String separator = "  ----";
			if (i<3)
				separator += "--------------";
			sb1.append(separator);
		}
		String sb1Str = sb1.toString();
		rsBuild.append(sb1Str);
		rsBuild.append("\n");
		rsBuild.append("                    ").append("      Today         ").append("   Month To Date    ").append("        Plan        ")
		.append("  %Act").append(" %Proj\n");
		rsBuild.append(sb1Str);
		rsBuild.append("\n");
		DecimalFormat decimalFormat = new DecimalFormat("#,###");
		String prevTerminal = null;
		double totalToday = 0.0;
		double totalMonthToDate = 0.0;
		double totalPlan = 0.0;
		double summToday = 0.0;
		double summMonthToDate = 0.0;
		double summPlan = 0.0;
		
		for (int i=0; i<monthToDate.size(); i++)
		{
			ReportDataModel mtd = monthToDate.get(i);
			if (prevTerminal == null)
				prevTerminal = mtd.p_terminal;
			if (!prevTerminal.equals(mtd.p_terminal))
			{
				createSummary(decimalFormat, rsBuild, prevTerminal, 
						totalToday, totalMonthToDate, totalPlan, offDaysInMonth, 
						offDaysMonthToDate);
				totalToday = 0.0;
				totalMonthToDate = 0.0;
				totalPlan = 0.0;
				prevTerminal = mtd.p_terminal;
			}
			
			double amount = mtd.p_amount.doubleValue();
			double todayAmt = getAmt(mtd.p_store, today);
			double planAmt = getAmt(mtd.p_store, salesTarget);
			totalToday = totalToday + todayAmt;
			totalPlan = totalPlan + planAmt;
			totalMonthToDate = totalMonthToDate + amount;
			summToday = summToday + todayAmt;
			summPlan = summPlan + planAmt;
			summMonthToDate = summMonthToDate + amount;
			createLine(decimalFormat, rsBuild, mtd.p_store, todayAmt, amount,
					planAmt, offDaysInMonth, offDaysMonthToDate);
		}
		
		createSummary(decimalFormat, rsBuild, prevTerminal,
				totalToday, totalMonthToDate, totalPlan, offDaysInMonth, 
				offDaysMonthToDate);
		
		createTotals(decimalFormat, rsBuild, summToday, summMonthToDate, summPlan);
		rsBuild.append("</pre>");
		String result = rsBuild.toString();
		
		String fileName = p_destination;
		if (fileName == null)
			fileName = Adempiere.getAdempiereHome().trim();
		if (!fileName.endsWith("/") && !fileName.endsWith("\\"))
			fileName += File.separator;
		format = new SimpleDateFormat("yyyyMMdd");
		String df = format.format(m_dateToExport);
		fileName += p_fileName;
		if (!fileName.endsWith("_"))
			fileName += "_";
		fileName += df;
		fileName += ".txt";
		File file = new File(fileName);
		FileWriter fw = new FileWriter(file);
		fw.write(result);
		fw.close();
		String subject = p_fileName + "_" + df;
		MClient c = MClient.get(Env.getCtx());
		c.sendEmail(m_to, m_cc, m_bcc, subject, result, null, true);
		return result;
	}
		
	private void createTotals (DecimalFormat decimalFormat, StringBuilder rsBuild, double today, 
			double monthtoDate, double plan)
	{
		StringBuilder sb1 = new StringBuilder("                    ");
		for (int i=0; i<5; i++)
		{
			String separator = "  ----";
			if (i<3)
				separator += "--------------";
			sb1.append(separator);
		}

		String sb1Str = sb1.toString();
		rsBuild.append(sb1Str).append("\n");
		String total = String.format("%1$-20s", "Total");
		rsBuild.append(total);
		String todayStr = numberToText(decimalFormat, "%20s", today);
		String monthtoDateStr = numberToText(decimalFormat, "%20s", monthtoDate);
		String planStr = numberToText(decimalFormat, "%20s", plan);
		
		rsBuild.append(amtSeparator()).append(todayStr)
		.append(amtSeparator()).append(monthtoDateStr)
		.append(amtSeparator()).append(planStr)
		.append("\n");
		rsBuild.append(sb1Str).append("\n");
	}
	
	private void createLine (DecimalFormat decimalFormat, StringBuilder rsBuild, String store, double today, 
			double monthtoDate, double plan, int offDaysInMonth, int offDaysMonthToDate)
	{
		int act = calcAct(monthtoDate, plan);
		int proj = calcProj(monthtoDate, plan, offDaysInMonth, offDaysMonthToDate);

		String amtStr = numberToText(decimalFormat, "%20s", monthtoDate);
		String todayAmtStr = numberToText(decimalFormat, "%20s", today);
		String planAmtStr = numberToText(decimalFormat, "%20s", plan);
		String actStr = numberToText(decimalFormat, "%6s", act);
		String projStr = numberToText(decimalFormat, "%6s", proj);
		
		if (!Util.isEmpty(store, true))
		{
			String shop = String.format("%1$-20s", store);
			rsBuild.append(shop);
		}
		
		rsBuild.append(amtSeparator()).append(todayAmtStr)
		.append(amtSeparator()).append(amtStr)
		.append(amtSeparator()).append(planAmtStr)
		.append(amtSeparator()).append(actStr)
		.append(amtSeparator()).append(projStr).append("\n");
	}
	
	private String amtSeparator()
	{
		return "";
	}
	
	private void createSummary (DecimalFormat decimalFormat, StringBuilder rsBuild, String terminal, double today, 
			double monthtoDate, double plan, int offDaysInMonth, int offDaysMonthToDate)
	{
		StringBuilder sb1 = new StringBuilder("                    ");
		for (int i=0; i<5; i++)
		{
			String separator = "  ----";
			if (i<3)
				separator += "--------------";
			sb1.append(separator);
		}

		String sb1Str = sb1.toString();
		rsBuild.append(sb1Str).append("\n");
		String tmpTerminal = terminal.trim();
		tmpTerminal = "Tot " + tmpTerminal;
		tmpTerminal = String.format("%1$-20s", tmpTerminal);
		rsBuild.append(tmpTerminal);
		createLine(decimalFormat, rsBuild, null, today, monthtoDate, plan, offDaysInMonth, offDaysMonthToDate);
		rsBuild.append(sb1Str).append("\n");
		createAvg(decimalFormat, rsBuild, monthtoDate, plan, offDaysInMonth, offDaysMonthToDate);
	}
	
	private void createAvg (DecimalFormat decimalFormat, StringBuilder rsBuild, double monthtoDate, double plan, 
			int offDaysInMonth, int offDaysMonthToDate)
	{
		double mtd = (double) ((double) monthtoDate / (double)offDaysMonthToDate);
		double p = (double) ((double) plan / (double)offDaysInMonth);
		double estMtd = (double) mtd * (double) offDaysInMonth;
		rsBuild.append("Average             ");
		String mtdStr = numberToText(decimalFormat,"%20s", mtd);
		String pStr = numberToText(decimalFormat,"%20s", p);
		String estMtdStr = numberToText(decimalFormat, "%20s", estMtd);
		
		rsBuild.append(amtSeparator()).append(mtdStr)
		.append(amtSeparator()).append(pStr)
		.append(amtSeparator()).append(estMtdStr)
		.append("\n\n");
	}
	
	private int calcProj (double amount, double planAmt, int offDaysInMonth, int offDaysMonthToDate)
	{
		if (planAmt == 0)
			return 0;
		return  (int) (((double)amount / (((double)planAmt/(double)offDaysInMonth) * (double)offDaysMonthToDate))*(double)100);
	}
	
	private int calcAct (double amount, double planAmt)
	{
		if (planAmt == 0)
			return 0;
		return (int) (((double)amount / (double)planAmt) * (double)100);
	}
	
	private String numberToText (DecimalFormat format, String strFormat, double amt)
	{
		String strAmt = format.format(amt);
		strAmt = String.format(strFormat, strAmt);
		return strAmt;
	}
	
	private String p_fileName;
	private Timestamp p_date;
	private String p_shopIDs;
	private Timestamp m_dateToExport;
	private String p_destination;
	private String p_recipient = null;
	
	private double getAmt (String store, List<ReportDataModel> data)
	{
		double amt =0.0;
		for (int i=0; i<data.size(); i++)
		{
			if (data.get(i).p_store.equals(store))
			{
				amt = data.get(i).p_amount.doubleValue();
				break;
			}
		}
		return amt;
	}
	
	private List<ReportDataModel> loadSalesTarget (int periodID, List<Integer> store)
	{
		List<ReportDataModel> result = new ArrayList<>();
		List<Object> params = new ArrayList<>();
		params.add("CO");
		params.add("CL");
		params.add(periodID);
		StringBuilder sb = new StringBuilder("SELECT bg.Value, bp.Value, dt.NetSalesTarget FROM  UNS_ShopTargetConfig dt ")
		.append(" INNER JOIN  UNS_SubregionTargetConfig st ON st.UNS_SubregionTargetConfig_ID = dt.UNS_SubregionTargetConfig_ID ")
		.append(" AND st.DocStatus IN (?, ?) AND st.C_Period_ID = ? INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = dt.Shop_ID ")
		.append(" INNER JOIN C_BP_Group bg ON bg.C_BP_Group_ID = bp.C_BP_Group_ID ");
		if (store == null)
			store = new ArrayList<>();
		else
			sb.append(" AND dt.Shop_ID IN (");
		for (int i=0; i<store.size(); i++)
		{
			sb.append("?");
			params.add(store.get(i));
			if (i<(store.size()-1))
				sb.append(",");
		}
		if (store.size() > 0)
			sb.append(") ");
		sb.append("ORDER BY bg.Value, bp.Value ");
		String sql = sb.toString();
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			for (int i=0; i<params.size(); i++)
			{
				st.setObject((i+1), params.get(i));
			}
			rs = st.executeQuery();
			while (rs.next())
			{
				ReportDataModel model = new ReportDataModel(rs.getString(1), rs.getString(2), rs.getBigDecimal(3));
				result.add(model);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		return result;
	}
	
	private List<ReportDataModel> loadSalesRecord (Timestamp start, Timestamp end, List<Integer> store)
	{
		List<ReportDataModel> result = new ArrayList<>();
		List<Object> params = new ArrayList<>();
		params.add(start);
		params.add(end);
		params.add("CO");
		params.add("CL");
		params.add("Y");
		StringBuilder sb = new StringBuilder("SELECT bg.Value, bp.Value, SUM (pt.GrandTotal) ")
		.append(" FROM UNS_POSTrx pt INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pt.C_BPartner_ID ")
		.append(" INNER JOIN UNS_POS_Session ss ON ss.UNS_POS_Session_ID = pt.UNS_POS_Session_ID")
		.append(" INNER JOIN C_BP_Group bg ON bg.C_BP_Group_ID = bp.C_BP_Group_ID WHERE ")
		.append(" pt.DateAcct BETWEEN ? AND ? AND pt.DocStatus IN (?,?) AND pt.IsPaid = ? AND ss.IsTrialMode = 'N'");
		if (store == null)
			store = new ArrayList<>();
		else
			sb.append(" AND pt.C_BPartner_ID IN (");
		for (int i=0; i<store.size(); i++)
		{
			sb.append("?");
			params.add(store.get(i));
			if (i<(store.size()-1))
				sb.append(",");
		}
		if (store.size() > 0)
			sb.append(") ");
		sb.append(" GROUP BY bp.Value, bg.Value ORDER BY bg.Value, bp.Value");
		String sql = sb.toString();
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			for (int i=0; i<params.size(); i++)
			{
				st.setObject((i+1), params.get(i));
			}
			rs = st.executeQuery();
			while (rs.next())
			{
				ReportDataModel model = new ReportDataModel(rs.getString(1), rs.getString(2), rs.getBigDecimal(3));
				result.add(model);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		return result;
	}
	

	private List<Integer> parsingShopIDs (String text, String delimiter)
	{
		List<Integer> ret = new ArrayList<>();
		String[] shops = text.split(delimiter);
		for (int i=0; i<shops.length; i++)
			ret.add(Integer.valueOf(shops[i]));
		return ret;
	}
	
	private class ReportDataModel
	{
		private final String p_terminal;
		private final String p_store;
		private final BigDecimal p_amount;
		
		private ReportDataModel (String terminal, String store, BigDecimal amount)
		{
			p_terminal = terminal;
			p_store = store;
			p_amount = amount;
		}
	}
}
