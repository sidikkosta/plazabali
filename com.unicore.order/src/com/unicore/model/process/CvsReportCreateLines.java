/**
 * 
 */
package com.unicore.model.process;

import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSCvsRpt;

/**
 * @author root
 *
 */
public class CvsReportCreateLines extends SvrProcess {

	/**
	 * 
	 */
	public CvsReportCreateLines() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// No Action

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		MUNSCvsRpt record = new MUNSCvsRpt(getCtx(), getRecord_ID(), get_TrxName());
		return record.createLines();
	}

}
