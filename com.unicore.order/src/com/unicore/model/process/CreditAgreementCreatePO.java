/**
 * 
 */
package com.unicore.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import com.unicore.base.model.MOrder;
import com.unicore.model.MUNSCreditAgreement;

/**
 * @author Menjangan
 *
 */
public class CreditAgreementCreatePO extends SvrProcess {

	/**
	 * 
	 */
	public CreditAgreementCreatePO() {
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			ProcessInfoParameter para = params[i];
			String name = para.getParameterName();
			if ("Vendor_ID".equals(name))
				p_Vendor_ID = para.getParameterAsInt();
			else if ("DateOrdered".equals(name))
				p_DateOrdered = para.getParameterAsTimestamp();
			else if ("M_Warehouse_ID".equals(name))
				p_Warehouse_ID = para.getParameterAsInt();
			else if ("M_PriceList_ID".equals(name))
				p_PriceList_ID = para.getParameterAsInt();
			else if ("IsAutocomplete".equals(name))
				p_AutoComplete = para.getParameterAsBoolean();
			else
				log.log(Level.WARNING, "Unknown parameter " + name);
		}
	}
	
	private int p_Vendor_ID = 0;
	private Timestamp p_DateOrdered = null;
	private int p_Warehouse_ID = 0;
	private int p_PriceList_ID = 0;
	private boolean p_AutoComplete = false;
	

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		MUNSCreditAgreement creditAgreement = new MUNSCreditAgreement(
				getCtx(), getRecord_ID(), get_TrxName());
		
		if (creditAgreement.getC_Order_ID() > 0)
		{
			String sql = "SELECT DocStatus FROM C_Order WHERE C_Order_ID = ?";
			String status = DB.getSQLValueString(get_TrxName(), sql, 
					creditAgreement.getC_Order_ID());
			if ("CO".equals(status) || "CL".equals(status))
				return "Shipment already created before.";
			
			if ("DR".equals(status) || "IP".equals(status) || "IN".equals(status))
			{
				// try to complete existing shipment.
				if (this.p_AutoComplete)
				{
					MOrder order = new MOrder(getCtx(), creditAgreement.getC_Order_ID(), get_TrxName());
					boolean ok = order.processIt(DocAction.ACTION_Complete);
					if (!ok)
						throw new AdempiereException(order.getProcessMsg());
					
					order.saveEx();
				}
				
				return "success";
			}
			
			creditAgreement.setC_Order_ID(-1);
		}
		
		if (!creditAgreement.doCreatePO(this.p_Vendor_ID, this.p_DateOrdered, this.p_Warehouse_ID, 
				this.p_PriceList_ID, this.p_AutoComplete))
		{
			throw new AdempiereException(creditAgreement.getProcessMsg());
		}
		
		return null;
	}

}
