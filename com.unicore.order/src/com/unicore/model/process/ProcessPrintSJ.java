/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;

import com.unicore.model.MUNSConfirmPrinting;
import com.unicore.model.MUNSPLConfirm;
import com.unicore.model.MUNSPackingList;
import com.unicore.model.MUNSPackingListOrder;
import com.unicore.model.MUNSPrintingLog;
import com.uns.util.MessageBox;

/**
 * @author ALBURHANY
 *
 */
public class ProcessPrintSJ extends SvrProcess {
	
//TODO commment, unused @Menjangan	private String m_DocumentNo;
	private int m_User;
	private String m_Reason;
	private String m_Type;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{	
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("DocumentNo"));
//				m_DocumentNo = param.getParameterAsString();
			else if(param.getParameterName().equals("AD_User_ID"))
				m_User = param.getParameterAsInt();
			else if(param.getParameterName().equals("Reason"))
				m_Reason = param.getParameterAsString();
			else if(param.getParameterName().equals("TypeSL"))
				m_Type = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
			
		MUNSPLConfirm plConfirm = new MUNSPLConfirm(getCtx(), getRecord_ID(), get_TrxName());
		
		MUNSPackingList pList = new MUNSPackingList(getCtx(), plConfirm.getUNS_PackingList_ID(), get_TrxName());
		MUNSPackingListOrder[] plOrder = pList.getLines(null, null);
		
		MUNSPrintingLog printingLog = MUNSPrintingLog.getMaxCopy(getCtx(), getRecord_ID(), m_Type, get_TrxName());
		
		if(printingLog.get_ID() != 0)
		{
			if(m_Reason == null)
				throw new AdempiereException("Field Reason is Mandatory");
			
			String sql = "SELECT Array_To_String(Array_Agg(DocumentNo), ' : ') FROM M_InOut WHERE M_InOut_ID IN "
					+ "(SELECT M_InOut_ID FROM UNS_PackingList_Order WHERE UNS_PackingList_ID IN "
					+ "(SELECT UNS_PackingList_ID FROM UNS_PL_Confirm WHERE UNS_PL_Confirm_ID =?))";
			String list_docno = DB.getSQLValueString(get_TrxName(), sql.toString(), plConfirm.get_ID());
			
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo()
					, "Do you want to re-print ? (" + list_docno + ")"
					, "Print Document Shipment"
					, MessageBox.YESNO
					, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_YES || retVal == MessageBox.RETURN_OK)
			{									
				for(MUNSPackingListOrder listOrder : plOrder)
				{
					MUNSConfirmPrinting confPrint = new MUNSConfirmPrinting(getCtx(), 0, get_TrxName());
					
					confPrint.setDocumentNo(listOrder.getM_InOut().getDocumentNo());
					confPrint.setDateDoc(DB.getSQLValueTS(get_TrxName(), "SELECT now()"));
					confPrint.setAD_User_ID(m_User);
					confPrint.setUNS_PackingList_ID(plConfirm.getUNS_PackingList_ID());
					confPrint.setUNS_PL_Confirm_ID(plConfirm.get_ID());
					confPrint.setReason(m_Reason);
					confPrint.setTypeSL(m_Type);
					confPrint.setCopyOfPrint(printingLog.getCopyOfPrint() + 1);
					confPrint.saveEx();
				}
			}
			if(retVal == MessageBox.RETURN_NO || retVal == MessageBox.RETURN_NONE || retVal == MessageBox.RETURN_CANCEL)
				throw new AdempiereUserError("Print Aborted");
		}
		
		for(MUNSPackingListOrder listOrder : plOrder)
		{
			MUNSPrintingLog pLog = new MUNSPrintingLog(getCtx(), 0, get_TrxName());
			pLog.create(getCtx(), getRecord_ID(), m_User, m_Type, listOrder.getM_InOut().getDocumentNo(), get_TrxName());
		}
		
		return null;
	}
}
