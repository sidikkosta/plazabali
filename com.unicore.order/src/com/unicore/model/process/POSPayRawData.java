/**
 * 
 */
package com.unicore.model.process;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.util.IProcessUI;

import jxl.Cell;
import jxl.Sheet;

/**
 * @author nurse
 *
 */
public class POSPayRawData 
{
	private static int COUNT = 0;
	public static final int COLUMN_DATE = COUNT++;
	public static final int COLUMN_POSTRX = COUNT++;
	public static final int COLUMN_PAYCURRENCY = COUNT++;
	public static final int COLUMN_PAYMETHOD = COUNT++;
	public static final int COLUMN_PAYAMT = COUNT++;
	public static final int COLUMN_EDC = COUNT++;
	public static final int COLUMN_BATCHNO = COUNT++;
	public static final int COLUMN_CARDTYPE = COUNT++;
	public static final int COLUMN_DESCRIPTION = COUNT++;
	
	private List<String[]> m_rows;
	private Sheet m_sheet;
	
	
	/**
	 * 
	 */
	public POSPayRawData(Sheet sheet) 
	{
		m_sheet = sheet;
		m_rows = new ArrayList<>();
	}

	public List<String[]> parseValues (IProcessUI ui, int rowstart, int rowend) throws Exception
	{
		if (rowstart == -1)
			rowstart = 1;
		if (rowend <= 0)
			rowend = m_sheet.getRows();
		for (int i=rowstart; i<rowend; i++)
		{
			ui.statusUpdate("Parsing record from excel [" + i + " from " + rowend + "]");
			m_rows.add(parseRow(i));
		}
		
		return m_rows;
	}
	
	private String[] parseRow (int rowidx) throws Exception
	{
		String[] row = new String[COUNT];
		for (int i=0; i<row.length; i++)
		{
			Cell cell = m_sheet.getCell(i, rowidx);
			String content = cell.getContents();
			if (content == null)
				content = "";
			content = content.trim();
			row[i] = content;
		}
		return row;
	}
}
