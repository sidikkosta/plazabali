/**
 * 
 */
package com.unicore.model.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.Adempiere;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.uns.model.process.FTPUploader;

/**
 * @author nurse
 *
 */
public class PAPSCHFileExporter extends SvrProcess 
{

	private String p_delimiter;
	private String p_fileName;
	private Timestamp p_date;
	private String p_shopIDs;
	private Hashtable<Integer, String> m_fileMap;
	private List<LocalFileWriter> m_writer;
	private Timestamp m_dateToExport;
	private String p_destination;
	private boolean p_isAutoSendToFTPServer = true;
	private String p_ftpHost = "180.250.69.142";
	private int p_port = 21;
	private String p_ftpUser = "dewataagung";
	private String p_ftpPassword = "D3wat4agun9";
	private String p_ftpDir = "/dutyfree/";
	
	
	/**
	 * 
	 */
	public PAPSCHFileExporter() 
	{
		super ();
		m_fileMap = new Hashtable<>();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("Delimiter".equals(params[i].getParameterName()))
				p_delimiter = params[i].getParameterAsString();
			else if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("Date".equals(params[i].getParameterName()))
				p_date = params[i].getParameterAsTimestamp();
			else if ("Shop_ID".equals(params[i].getParameterName()))
				p_shopIDs = params[i].getParameterAsString();
			else if ("Destination".equals(params[i].getParameterName()))
				p_destination = params[i].getParameterAsString();
			else if ("FTPHost".equals(params[i].getParameterName()))
				p_ftpHost = params[i].getParameterAsString();
			else if ("FTPPort".equals(params[i].getParameterName()))
				p_port = params[i].getParameterAsInt();
			else if ("FTPUser".equals(params[i].getParameterName()))
				p_ftpUser = params[i].getParameterAsString();
			else if ("FTPPassword".equals(params[i].getParameterName()))
				p_ftpPassword = params[i].getParameterAsString();
			else if ("FTPDir".equals(params[i].getParameterName()))
				p_ftpDir = params[i].getParameterAsString();
			else if ("IsAutoSendToFTPServer".equals(params[i].getParameterName()))
				p_isAutoSendToFTPServer = params[i].getParameterAsBoolean();
		}
	}
	
	private List<Integer> parsingShopIDs (String text, String delimiter)
	{
		List<Integer> ret = new ArrayList<>();
		String[] shops = text.split(delimiter);
		if (delimiter.equals("\\|"))
		{
			String fileName = p_destination;
			if (fileName == null)
				fileName = Adempiere.getAdempiereHome().trim();
			if (!fileName.endsWith("/") && !fileName.endsWith("\\"))
				fileName += File.separator;
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			String df = format.format(m_dateToExport);
			fileName += p_fileName;
			fileName += shops[0];
			if (!fileName.endsWith("_"))
				fileName += "_";
			fileName += df;
			fileName += ".csv";
			List<Integer> shopss = parsingShopIDs(shops[1], "&");
			for (int i=0; i<shopss.size(); i++)
				m_fileMap.put(shopss.get(i), fileName);
			ret.addAll(shopss);
			return ret;
		}
		for (int i=0; i<shops.length; i++)
		{
			String shop = shops[i];
			if (shop.contains("|"))
			{
				ret.addAll(parsingShopIDs(shop, "\\|"));
			}
			else if (shop.contains("&"))
			{
				ret.addAll(parsingShopIDs(shop, "&"));
			}
			else
			{
				ret.add(Integer.valueOf(shop));
			}
				
		}
		return ret;
	}
	
	public LocalFileWriter getWriter (int shop)
	{
		String fName = m_fileMap.get(shop);
		LocalFileWriter wr = null;
		for (int i=0; i<m_writer.size(); i++)
		{
			if (m_writer.get(i).m_fileName.equals(fName))
			{
				wr = m_writer.get(i);
				break;
			}
		}
		return wr;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(p_shopIDs, true))
			throw new AdempiereException("Not set parameter shop IDs");
		if (p_date == null)
			p_date = new Timestamp(System.currentTimeMillis());
		if (Util.isEmpty(p_delimiter, true))
			p_delimiter = ";";
		if (!p_fileName.endsWith("_"))
			p_fileName += "_";
		
		p_date = TimeUtil.trunc(p_date, TimeUtil.TRUNC_DAY);
		m_dateToExport = TimeUtil.addDays(p_date, -1);
		if (p_fileName == null)
			throw new AdempiereException("Mandatory parameter File Name");

		List<Integer> shops = parsingShopIDs(p_shopIDs, "#");
		List<Object> params = new ArrayList<>();
		params.add(p_delimiter);
		params.add(m_dateToExport);
		StringBuilder w = new StringBuilder("pt.DateAcct = ? AND pt.C_BPartner_ID IN (");
		for (int i=0; i<shops.size(); i++)
		{
			if (i> 0 && i < shops.size())
				w.append(",");
			w.append("?");
			params.add(shops.get(i));
		}
		w.append(")");
		String clause = w.toString();
		String sql = buildSQL(clause);
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			prepareWriter();
			st = DB.prepareStatement(sql, get_TrxName());
			for (int i=0; i<params.size(); i++)
				st.setObject((i+1), params.get(i));
			rs = st.executeQuery();
			while (rs.next())
			{
				int startCol = 2;
				LocalFileWriter writer = getWriter(rs.getInt(1));
				if (writer == null)
					continue;
				for (int i=0; i<33; i++, startCol++)
				{
					writer.m_bufferedWriter.write(rs.getString(startCol));
					writer.m_bufferedWriter.write(";");
				}
				writer.m_bufferedWriter.newLine();
			}
		}
		catch (SQLException ex)
		{
			closeWriter();
			throw new AdempiereException(ex.getMessage());
		}
		finally
		{
			DB.close(rs, st);
		}
		
		closeWriter();
		if (p_isAutoSendToFTPServer && !sendFileToFTPServer())
			throw new AdempiereException("Could not send file to FTP Server");
		
		return "File exported successfully";
	}
	
	private class LocalFileWriter 
	{
		private FileWriter m_fileWriter;
		private BufferedWriter m_bufferedWriter;
		private String m_fileName;
		
		LocalFileWriter (String fileName) throws IOException
		{
			this.m_fileName = fileName;
			this.m_fileWriter = new FileWriter(m_fileName);
			this.m_bufferedWriter = new BufferedWriter(m_fileWriter);
		}
		
		void Close () throws IOException
		{
			m_bufferedWriter.close();
			m_fileWriter.close();
		}
	}
	
	private void prepareWriter () throws IOException
	{
		m_writer = new ArrayList<>();
		Enumeration<Integer> eFN = m_fileMap.keys();
		while (eFN.hasMoreElements()) 
		{
			int shop = eFN.nextElement();
			LocalFileWriter writer = getWriter(shop);
			if (writer != null)
				continue;
			String fn = m_fileMap.get(shop);
			m_writer.add(new LocalFileWriter(fn));
		}
	}
	
	private void closeWriter () throws IOException
	{
		for (int i=0; i<m_writer.size(); i++)
		{
			m_writer.get(i).Close();
		}
	}
	
	private String buildSQL (String clause)
	{
		String sql = "SELECT pt.C_BPartner_ID, pt.DocumentNo AS DocumentNo, TO_CHAR (pt.DateAcct, 'yyyy/MM/dd HH24:mm')"
				+ " AS DateAcct, 'CGK' AS AirportCode,"
				+ " '2' AS Unknown1,  '' AS Unknown2, ptl.Line AS LineNo, p.Name, p.SKU, '' AS Unknown0,"
				+ " (SELECT CONCAT (COALESCE (c.Description, c.Name), ?, c.Value) FROM M_Product_Category "
				+ " c WHERE M_Product_Category_ID = getProductCategoryTopParent (ptl.M_Product_ID)) AS Category,"
				+ " ROUND ((ptl.QtyOrdered), uom.StdPrecision) "
				+ " AS QtyOrdered, uom.UOMSymbol AS UOMSymbol, "
				+ " ROUND ((ptl.PriceList / CASE WHEN pter.StoreType IN ('RDP') OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END ), currency.StdPrecision) "
				+ " AS PriceList, ROUND ((ptl.DiscountAmt / CASE WHEN pter.StoreType IN ('RDP') OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END ), "
				+ " currency.StdPrecision) AS DiscountAmt, "
				+ " ROUND (ptl.LineNetAmt / CASE WHEN pter.StoreType IN ('RDP') OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END , currency.StdPrecision) "
				+ " AS netto, '0' AS Unknown3, '0' AS Unkonwn4, ROUND (((pt.Discount * ptl.LineNetAmt)/100 ) "
				+ " / CASE WHEN pter.StoreType IN ('RDP') OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END , currency.StdPrecision)"
				+ " AS TotalDisc, ROUND ((ptl.LineNetAmt - (((pt.Discount * ptl.LineNetAmt)/100 ))) "
				+ " / CASE WHEN pter.StoreType IN ('RDP') OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END , Currency.StdPrecision) "
				+ " AS TotalNetto, '0' AS Unknown5, '0' AS Unknown6, '0' AS Unknown7, "
				+ " ROUND ((ptl.LineNetAmt - (((pt.Discount * ptl.LineNetAmt)/100 ))) / CASE WHEN pter.StoreType IN ('RDP') OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END "
				+ " , currency.StdPrecision) AS TotalNetto2,"
				+ " currency.ISO_Code AS Currency, '1' AS Unknown8, '' AS Unknonwn9, '' AS Unknown10, cashier.Name AS CashierName,"
				+ " '' AS Unknown11, COALESCE (cust.Name, '-') AS CustomerName, COALESCE (cust.FlightNo, '-') AS FlightNo, '' AS Unknown12, "
				+ " COALESCE (country.Description,country.Name, '-') AS CountryCode FROM UNS_POSTrx pt INNER JOIN "
				+ " UNS_StoreConfiguration sc ON sc.Store_ID = pt.C_BPartner_ID AND sc.IsActive = 'Y' INNER JOIN UNS_POS_Session "
				+ " ps ON ps.UNS_POS_Session_ID = pt.UNS_POS_Session_ID AND ps.IsTrialMode = 'N' INNER JOIN UNS_POSTerminal pter ON "
				+ " pter.UNS_POSTerminal_ID = ps.UNS_POSTerminal_ID AND pter.StoreType NOT IN ('RBZ','ROL') AND (pter.Description "
				+ " IS NULL OR (pter.Description NOT LIKE '%RBZ%' AND pter.Description NOT LIKE '%ROL%')) INNER JOIN "
				+ " C_Currency currency ON currency.C_Currency_ID = pt.C_Currency_ID INNER JOIN UNS_POSTrxLine ptl "
				+ " ON ptl.UNS_POSTrx_ID = pt.UNS_POSTrx_ID INNER JOIN M_Product p ON p.M_Product_ID = ptl.M_Product_ID "
				+ " INNER JOIN C_UOM uom ON uom.C_UOM_ID = ptl.C_UOM_ID INNER JOIN AD_User cashier ON "
				+ " cashier.AD_User_ID = pt.CreatedBy LEFT JOIN UNS_CustomerInfo cust ON "
				+ " cust.UNS_CustomerInfo_ID = pt.UNS_CustomerInfo_ID LEFT JOIN C_Country country "
				+ " ON country.C_Country_ID = cust.C_Country_ID WHERE ptl.QtyOrdered <> 0 AND pt.DocStatus IN ('CO','CL') "
				+ " AND pt.IsPaid = 'Y' ";
		if (!Util.isEmpty(clause, true))
			sql += " AND " + clause;
		sql += " ORDER BY pt.C_BPartner_ID, pt.DocumentNo, ptl.line";
		return sql;
	}
	
	private boolean sendFileToFTPServer ()
	{
		log.log(Level.INFO, "Try to send file to FTP Server");
		FTPUploader uploader = new FTPUploader(p_ftpHost, p_port, p_ftpUser, p_ftpPassword, p_ftpDir);
		for (String fName : m_fileMap.values())
		{
			uploader.addFile(fName);
		}
		
		return uploader.doUpload(log);
	}
}
