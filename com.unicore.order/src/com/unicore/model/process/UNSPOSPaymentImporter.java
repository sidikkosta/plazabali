/**
 * 
 */
package com.unicore.model.process;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CCache;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Util;

import com.unicore.model.MUNSCardTrxDetail;
import com.unicore.model.MUNSCardType;
import com.unicore.model.MUNSPOSPayment;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPaymentTrx;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class UNSPOSPaymentImporter extends SvrProcess 
{

	private String p_fileName = null;
	private int p_start = -1;
	private int p_end = -1;
	private String p_sheetName = null;
	private CCache<String, List<Object>> l_cache = new CCache<>("UNSPOSPaymentImporter", 10);
	private Hashtable<Integer, MUNSPOSPayment> l_pPay = new Hashtable<>();
	private final String KEY_CURRENCY = "CUR";
	private final String KEY_CARDTYPE = "CARDTYPE";
	private final String KEY_EDC = "EDC";
	
	/**
	 * 
	 */
	public UNSPOSPaymentImporter() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("StartNo".equals(params[i].getParameterName()))
				p_start = params[i].getParameterAsInt();
			else if ("EndNo".equals(params[i].getParameterName()))
				p_end = params[i].getParameterAsInt();
			else if ("SheetName".equals(params[i].getParameterName()))
				p_sheetName = params[i].getParameterAsString();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(p_fileName, true))
			throw new AdempiereException("Mandatory parameter File Name");
		if (!p_fileName.endsWith(".xls"))
			throw new AdempiereException("Unsuported file. Please convert to xls first.");
		File file = new File(p_fileName);
		if (!file.exists())
			throw new AdempiereException("Could not find file " + p_fileName);
		if (Util.isEmpty(p_sheetName, true))
			p_sheetName = "Payment";
		Workbook book = Workbook.getWorkbook(file);
		Sheet[] sheets = book.getSheets();
		Sheet sheet = null;
		for (int i=0; i<sheets.length; i++)
		{
			String name = sheets[i].getName().toUpperCase();
			if (name.equals(p_sheetName))
			{
				sheet = sheets[i];
				break;
			}
		}
		
		if (sheet != null)
		{
			POSPayRawData rawDataInstance = new POSPayRawData(sheet);
			List<String[]> rawData = rawDataInstance.parseValues(processUI, p_start, p_end);
			processRows(rawData);
		}
		return "File importer successfully";
	}

	private void processRows (List<String[]> rows) throws Exception
	{
		for (int i=0; i<rows.size(); i++)
		{
			processUI.statusUpdate("Processing " + (i+1) + " From " + rows.size());
			processRow(rows.get(i));
			DB.commit(true, get_TrxName());
		}
	}
	
	private void processRow (String[] row) throws Exception
	{
		String dateStr = row[POSPayRawData.COLUMN_DATE];
		if (Util.isEmpty(dateStr, true))
			throw new AdempiereException("Mandatory column Date");
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
		Date d = df.parse(dateStr);
		Timestamp dateAcct = new Timestamp(d.getTime());
		String trxDocNo = row[POSPayRawData.COLUMN_POSTRX];
		String desc = row[POSPayRawData.COLUMN_DESCRIPTION];
		String currency = row[POSPayRawData.COLUMN_PAYCURRENCY];
		if ("OTH".equalsIgnoreCase(currency))
			return;
		MUNSPOSTrx trx = initPOSTrx(trxDocNo, dateAcct);
		if (trx == null)
			throw new AdempiereException("Could not find POS Trx " + trxDocNo + " date " + dateStr);
//		String sql = "SELECT 1 FROM UNS_POSPayment WHERE UNS_POSTrx_ID = ?";
//		boolean ok = DB.getSQLValue(get_TrxName(), sql, trx.get_ID()) != -1;
//		if(ok)
//			return;
		
		String payMethod = row[POSPayRawData.COLUMN_PAYMETHOD];
		String payAmtStr = row[POSPayRawData.COLUMN_PAYAMT];
		BigDecimal payAmt = Env.ZERO;
		try
		{
			payAmt = new BigDecimal(payAmtStr);
		}
		catch (NumberFormatException ex)
		{
			ex.printStackTrace();
		}
		String batchNo = row[POSPayRawData.COLUMN_BATCHNO];
		if (Util.isEmpty(batchNo, true))
			batchNo = "0000";
		String edc = row[POSPayRawData.COLUMN_EDC];
		String cardType = row[POSPayRawData.COLUMN_CARDTYPE];
		MUNSPOSPayment pay = initPOSPay(trx, desc);
		int currencyID = initCurrencyID(currency);
		if (currencyID == -1)
		{
			throw new AdempiereException("Could not find currency with iso " + currency);
		}
		
		MUNSPaymentTrx payTrx = new MUNSPaymentTrx(pay);
		payTrx.setUNS_POS_Session_ID(trx.getUNS_POS_Session_ID());
		payTrx.setPaymentMethod(payMethod);
		payTrx.setC_Currency_ID(currencyID);
		payTrx.setTrxType(initTrxType(payMethod, trx.isReturn()));
		payTrx.setAmount(payAmt.abs());
		payTrx.setIsReceipt((!pay.isCreditMemo() && payAmt.signum() == 1) || (pay.isCreditMemo() && payAmt.signum() == -1));
		payTrx.setTrxNo(batchNo);
		payTrx.setIsManual(true);
		payTrx.saveEx();
		if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(payMethod)
				|| MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(payMethod))
		{
			int cardtypeID = initCardType(cardType);
			if (cardtypeID == -1)
				throw new AdempiereException("Could not find card type "  + cardType);
			int edcID = initEDCID(edc);
			if (edcID == -1)
				throw new AdempiereException("Could not find EDC " + edc);
			
			MUNSCardTrxDetail detail = new MUNSCardTrxDetail(payTrx);
			detail.setBatchNo(batchNo);
			detail.setInvoiceNo(batchNo);
			detail.setTraceNo(batchNo);
			detail.setUNS_CardType_ID(cardtypeID);
			detail.setUNS_EDC_ID(edcID);
			detail.saveEx();
		}
	}
	
	private int initEDCID (String edc)
	{
		int edcID = -1;
		if (Util.isEmpty(edc, true))
		{
			String sql = "SELECT UNS_EDC_ID from UNS_EDC WHERE IsActive = ?";
			edcID = DB.getSQLValue(get_TrxName(), sql, "Y");
			return edcID;
		}
		
		List<Object> list = l_cache.get(KEY_EDC);
		
		if (list == null)
		{
			list = new ArrayList<>();
			l_cache.put(KEY_EDC, list);
		}
		
		for (int i=0; i<list.size(); i++)
		{
			if (list.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair) list.get(i);
			if (pair.getName().equals(edc))
			{
				edcID = pair.getKey();
				break;
			}
		}
		
		if (edcID == -1)
		{
			String sql = "SELECT UNS_EDC_ID from UNS_EDC WHERE Name = ?";
			edcID = DB.getSQLValue(get_TrxName(), sql, edc);
			if (edcID != -1)
				list.add(new KeyNamePair(edcID, edc));
		}
		if (edcID == -1)
		{
			String sql = "SELECT UNS_EDC_ID from UNS_EDC WHERE IsActive = ?";
			edcID = DB.getSQLValue(get_TrxName(), sql, "Y");
			if (edcID != -1)
				list.add(new KeyNamePair(edcID, edc));
		}
		return edcID;
	}
	
	private int initCardType (String cardType)
	{
		int cardTypeID = -1;
		if (cardType == null)
			return cardTypeID;
		
		List<Object> list = l_cache.get(KEY_CARDTYPE);
		
		if (list == null)
		{
			list = new ArrayList<>();
			l_cache.put(KEY_CARDTYPE, list);
		}
		
		for (int i=0; i<list.size(); i++)
		{
			if (list.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair) list.get(i);
			if (pair.getName().equals(cardType))
			{
				cardTypeID = pair.getKey();
				break;
			}
		}
		
		if (cardTypeID == -1)
		{
			cardTypeID = MUNSCardType.getIDByNameOrValue(get_TrxName(), cardType);
			if (cardTypeID != -1)
				list.add(new KeyNamePair(cardTypeID, cardType));
		}
		return cardTypeID;
	}
	
	private String initTrxType (String payMethod, boolean refund)
	{
		String trxType = null;
		switch (payMethod)
		{
			case MUNSPaymentTrx.PAYMENTMETHOD_Card : 
				trxType = MUNSPaymentTrx.TRXTYPE_EDCIn;
				if (refund)
					trxType = MUNSPaymentTrx.TRXTYPE_RefundEDC;
				break;
			case MUNSPaymentTrx.PAYMENTMETHOD_Wechat :
				trxType = MUNSPaymentTrx.TRXTYPE_WechatIn;
				if (refund)
					trxType = MUNSPaymentTrx.TRXTYPE_RefundWechat;
				break;
			case MUNSPaymentTrx.PAYMENTMETHOD_Voucher:
				trxType = MUNSPaymentTrx.TRXTYPE_VoucherSales;
				break;
			default :
				trxType = MUNSPaymentTrx.TRXTYPE_CashIn;
				if (refund)
					trxType = MUNSPaymentTrx.TRXTYPE_Refund;
				break;
		}
		return trxType;
	}
	
	private int initCurrencyID (String currISO)
	{
		int currID = -1;
		if (Util.isEmpty(currISO, true))
			return currID;
		List<Object> caches = l_cache.get(KEY_CURRENCY);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_CURRENCY, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(currISO))
			{
				currID = pair.getKey();
				break;
			}
		}
		
		if (currID == -1)
		{
			String sql = "SELECT C_Currency_ID FROM C_Currency WHERE ISO_Code = ?";
			currID = DB.getSQLValue(get_TrxName(), sql, currISO);
			if (currID != -1)
				caches.add(new KeyNamePair(currID, currISO));
		}
		
		return currID;
	}
	
	
	private MUNSPOSPayment initPOSPay (MUNSPOSTrx trx, String desc)
	{
		if (trx == null || trx.get_ID() <= 0)
			return null;
		MUNSPOSPayment pay = l_pPay.get(trx.get_ID());
		if (pay != null)
			return pay;
		
		pay = MUNSPOSPayment.getCreate(trx);
		pay.setPOSAmount(trx.getGrandTotal());
		if (Util.isEmpty(desc, true))
			pay.setDescription(trx.getDescription());
		else
			pay.setDescription(desc);
		pay.saveEx();
		String sql = "DELETE FROM UNS_CardTrxDetail WHERE UNS_PaymentTrx_ID "
				+ " IN (SELECT UNS_PaymentTrx_ID FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID = ?)";
		int result = DB.executeUpdate(sql, pay.get_ID(), false, get_TrxName());
		if (result == -1)
			throw new AdempiereException();
		
		sql = "DELETE FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID = ?";
		result = DB.executeUpdate(sql, pay.get_ID(), false, get_TrxName());
		
		if (result == -1)
			throw new AdempiereException();
		
		l_pPay.put(trx.get_ID(), pay);
		return pay;
	}
	
	private MUNSPOSTrx initPOSTrx (String documentNo, Timestamp dateAcct)
	{
		if (Util.isEmpty(documentNo, true))
			return null;
		
		MUNSPOSTrx trx = Query.get(Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPOSTrx.Table_Name,
				"DocumentNo = ?  AND DateAcct = ?", get_TrxName()).
				setParameters(documentNo, dateAcct).first();
		
		return trx;
	}
}
