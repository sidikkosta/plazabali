/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import com.unicore.model.MUNSWeighbridgeTicket;

/**
 * @author ALBURHANY
 *
 */
public class ChangeTicketContract extends SvrProcess {

	private int m_C_Order_ID = 0;
	private int m_C_OrderLine_ID = 0;
	boolean success = true;
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("C_Order_ID"))
				m_C_Order_ID = param.getParameterAsInt();
//			else if(param.getParameterName().equals("C_OrderLine_ID"))
//				m_C_OrderLine_ID = param.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{			
		MUNSWeighbridgeTicket wt = new MUNSWeighbridgeTicket(getCtx(), getRecord_ID(), get_TrxName());
		int oldOrderLine = wt.getC_OrderLine_ID();
		int M_InOutLine_ID = 0;
		String sql1 = "SELECT M_InOutLine_ID FROM M_InOutLine WHERE M_InOut_ID=?";
		M_InOutLine_ID = DB.getSQLValue(get_TrxName(), sql1, wt.getM_InOut_ID());
		if(m_C_Order_ID == wt.getC_Order_ID())
			throw new AdempiereException("Cannot change to contract same");
			
		if(wt.getC_InvoiceLine_ID() > 0)
			throw new AdempiereException("cannot change contract, ticket has linked with invoice.");
		
		String sql = "SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID=?";
		m_C_OrderLine_ID = DB.getSQLValue(get_TrxName(), sql, m_C_Order_ID) ;
		
		//update order
		String upOrder = "UPDATE UNS_WeighbridgeTicket SET C_Order_ID=?, C_OrderLine_ID=?"
				+ " WHERE UNS_WeighbridgeTicket_ID=?";
		success = DB.executeUpdate(upOrder, new Object[]{m_C_Order_ID,  m_C_OrderLine_ID, getRecord_ID()}, false,
				get_TrxName()) >= 0 ? true : false;
		if(!success)
			throw new AdempiereException("Failed when trying update Order");
		
		//update shipment line
		String upInOutLine = "UPDATE M_InOutLine SET C_OrderLine_ID=? WHERE M_InOut_ID=? AND C_OrderLine_ID=?";
		success = DB.executeUpdate(upInOutLine, new Object[]{m_C_OrderLine_ID, wt.getM_InOut_ID(), oldOrderLine},
				false, get_TrxName()) >= 0 ? true : false;
		if(!success)
			throw new AdempiereException("Failed when trying update Shipment line");
		
		//update match PO
		String upMatchPO = "UPDATE M_MatchPO SET C_OrderLine_ID=? WHERE M_InOutLine_ID=? AND C_OrderLine_ID=?";
		success = DB.executeUpdate(upMatchPO, new Object[]{m_C_OrderLine_ID, M_InOutLine_ID, oldOrderLine},
				false, get_TrxName()) >= 0 ? true : false;
		if(!success)
			throw new AdempiereException("Failed when trying update Match PO");
		
		//update shipment
		String upInOut = "UPDATE M_InOut SET C_Order_ID=? WHERE M_InOut_ID=?";
		success = DB.executeUpdate(upInOut, new Object[]{m_C_Order_ID, wt.getM_InOut_ID()}, false, get_TrxName()) >= 0
				? true : false;
		if(!success)
			throw new AdempiereException("Failed when trying update Shipment");
		
		//update order line
		String upOrderLine = "UPDATE C_OrderLine SET QtyDelivered=QtyDelivered+? WHERE C_OrderLine_ID=?";
		success = DB.executeUpdate(upOrderLine, new Object[]{wt.getNettoII(), m_C_OrderLine_ID}, false, get_TrxName()) >= 0
				? true : false;
		if(!success)
			throw new AdempiereException("Failed when trying update Order Line");
		
		//update old order line
		String upOldOrderLine = "UPDATE C_OrderLine SET QtyDelivered=QtyDelivered-? WHERE C_OrderLine_ID=?";
		success = DB.executeUpdate(upOldOrderLine, new Object[]{wt.getNettoII(), oldOrderLine}, false, get_TrxName()) >= 0
				? true : false;
		if(!success)
			throw new AdempiereException("Failed when trying update Old Order Line");
		
		return "Success";
	}
}