/**
 * 
 */
package com.unicore.model.process;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.Adempiere;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;


/**
 * @author nurse
 *
 */
public class MonthlyPAPReport extends SvrProcess 
{
	private Timestamp p_from = null;
	private Timestamp p_to = null;
	private String p_fileName;
	private String p_shops;
	private String p_cats;
	
	
	/**
	 * 
	 */
	public MonthlyPAPReport() 
	{
		super ();
	}
	
	

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("DateFrom".equals(params[i].getParameterName()))
				p_from = params[i].getParameterAsTimestamp();
			else if ("DateTo".equals(params[i].getParameterName()))
				p_to = params[i].getParameterAsTimestamp();
			else if ("Shop".equals(params[i].getParameterName()))
				p_shops = params[i].getParameterAsString();
			else if ("Categories".equals(params[i].getParameterName()))
				p_cats = params[i].getParameterAsString();
		}
	}
	
	private List<String> extractValues (String values, boolean upper)
	{
		String[] cats = values.split("\\|");
		List<String> list = new ArrayList<>();
		for (int i=0; i<cats.length; i++)
		{
			String value = cats[i].trim();
			if (upper)
				value = value.toUpperCase();
			list.add(value);
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(p_shops, true))
			throw new AdempiereException("Not set parameter shop IDs");
		if (p_from == null)
			throw new AdempiereException("Mandatory parameter Date From");
		if (p_to == null)
			throw new AdempiereException("Mandatory parameter Date To");
		if (!p_fileName.endsWith("_"))
			p_fileName += "_";
		
		if (p_fileName == null)
			throw new AdempiereException("Mandatory parameter File Name");
		
		List<String> shops = extractValues(p_shops, true);
		Collections.sort(shops);
		List<String> cats = extractValues(p_cats, true);
		Collections.sort(cats);
		StringBuilder builder = new StringBuilder("SALES BY CATEGORY\n\n");
		String sql;
		String catTitle = String.format("%1$-25s", "CATEGORY");
		builder.append(catTitle);
		List<Integer> shopIDs = new ArrayList<>();
		BigDecimal[] totalAmtShops = new BigDecimal[shops.size()+1];
		BigDecimal[] totalQtyShops = new BigDecimal[shops.size()+1];
		for (int i=0; i<shops.size(); i++)
		{
			String shopName = String.format("%33s", shops.get(i) + "        ");
			builder.append(shopName);
			sql = "SELECT C_BPartner_ID FROM C_BPartner WHERE Name = ?";
			int shopID = DB.getSQLValue(get_TrxName(), sql, shops.get(i));
			shopIDs.add(shopID);
		}
		
		builder.append(String.format("%33s", "TOTAL        "));
		builder.append("\n");
		builder.append(String.format("%25s", " "));
		int length__ = shops.size() + 1;
		for (int i=0; i<length__; i++)
		{
			builder.append(String.format("%20s", "Amount"));
			builder.append(String.format("%13s", "Quantity"));
		}
		builder.append("\n");
		final String _sql = " SELECT COALESCE (SUM (LineAmt-((ptl.LineAMt * pt.Discount)/100)), 0) / CASE WHEN ptr.StoreType IN ('RDP') "
				+ " OR sc.DefaultPassengerType IN ('A') THEN 1.1 ELSE 1 END , "
				+ " COALESCE (SUM (QtyOrdered), 0) FROM UNS_POSTrxLine ptl INNER JOIN UNS_POSTrx pt "
				+ " ON pt.UNS_POSTrx_ID = ptl.UNS_POSTrx_ID AND pt.DocStatus IN ('CO','CL') AND pt.DateAcct BETWEEN ? AND ? "
				+ " AND pt.IsPaid = 'Y' AND pt.C_BPartner_ID = ? INNER JOIN UNS_POS_Session ps "
				+ " ON ps.UNS_POS_Session_ID = pt.UNS_POS_Session_ID AND ps.IsTrialMode = 'N' "
				+ " INNER JOIN UNS_POSTerminal ptr ON ptr.UNS_POSTerminal_ID = ps.UNS_POSTerminal_ID "
				+ " AND ptr.Store_ID = pt.C_BPartner_ID AND ptr.StoreType NOT IN ('RBZ', 'ROL') "
				+ " AND (ptr.Description IS NULL OR (ptr.Description NOT LIKE '%#RBZ#%' AND ptr.Description NOT LIKE '%#ROL#%')) "
				+ " INNER JOIN M_Product prod ON prod.M_Product_Category_Parent_ID = ? INNER JOIN "
				+ " UNS_StoreConfiguration sc ON sc.Store_ID = pt.C_BPartner_ID  AND sc.IsActive = 'Y' "
				+ " AND prod.M_Product_ID = ptl.M_product_ID WHERE ptl.IsActive = 'Y' AND ptl.Processed = 'Y'"
				+ " GROUP BY sc.DefaultPassengerType, ptr.StoreType";
		
		DecimalFormat format = new DecimalFormat("#,###.##");
		for (int i=0; i<cats.size(); i++)
		{
			sql = "SELECT M_Product_Category_ID, Name FROM M_Product_Category WHERE Value = ?";
			List<Object> catInfo = DB.getSQLValueObjectsEx(get_TrxName(), sql, cats.get(i));
			if (catInfo == null || catInfo.size() == 0)
				continue;
			int categoryID = Integer.valueOf(catInfo.get(0).toString());
			String categoryName = String.format("%1$-25s", catInfo.get(1));
			builder.append(categoryName);
			BigDecimal totalAmt = Env.ZERO;
			BigDecimal totalQty = Env.ZERO;
			for (int j=0; j<shopIDs.size(); j++)
			{
				int shopID = shopIDs.get(j);
				List<List<Object>> amtQty = DB.getSQLArrayObjectsEx(get_TrxName(), _sql, p_from, p_to, shopID, categoryID);
				BigDecimal tAmt = null;
				BigDecimal tQty = null;
				if (amtQty != null && amtQty.size() != 0)
				{
					tAmt = (BigDecimal) amtQty.get(0).get(0);
					tQty = (BigDecimal) amtQty.get(0).get(1);
				}
				
				if (tAmt == null)
					tAmt = Env.ZERO;
				if (tQty == null)
					tQty = Env.ZERO;
				
				totalAmt = totalAmt.add(tAmt);
				totalQty = totalQty.add(tQty);
				String tAmtStr = numberToText(format, "%20s", tAmt.doubleValue());
				builder.append(tAmtStr);
				BigDecimal totalShop = totalAmtShops[j];
				if (totalShop == null)
					totalShop = Env.ZERO;
				totalShop = totalShop.add(tAmt);
				totalAmtShops[j] = totalShop;
				
				String tQtyStr = numberToText(format, "%13s", tQty.doubleValue());
				builder.append(tQtyStr);
				BigDecimal totalQtyShop = totalQtyShops[j];
				if (totalQtyShop == null)
					totalQtyShop = Env.ZERO;
				totalQtyShop = totalQtyShop.add(tQty);
				totalQtyShops[j] = totalQtyShop;
			}
			builder.append(numberToText(format, "%20s", totalAmt.doubleValue()));
			builder.append(numberToText(format, "%13s", totalQty.doubleValue()));
			builder.append("\n");
			BigDecimal totalShop = totalAmtShops[shopIDs.size()];
			if (totalShop == null)
				totalShop = Env.ZERO;
			totalShop = totalShop.add(totalAmt);
			totalAmtShops[shopIDs.size()] = totalShop;
			
			BigDecimal totalQtyShop = totalQtyShops[shopIDs.size()];
			if (totalQtyShop == null)
				totalQtyShop = Env.ZERO;
			totalQtyShop = totalQtyShop.add(totalQty);
			totalQtyShops[shopIDs.size()] = totalQtyShop;
		}
		builder.append(String.format("%1$-25s", "TOTAL"));
		for (int i=0; i<totalAmtShops.length; i++)
		{
			builder.append(numberToText(format, "%20s", totalAmtShops[i].doubleValue()));
			builder.append(numberToText(format, "%13s", totalQtyShops[i].doubleValue()));
		}
		String message = builder.toString();
		String dir = Adempiere.getAdempiereHome();
		if (!dir.endsWith("\\") && !dir.endsWith("/"))
			dir += File.separator;
		String fileName = p_fileName;
		if (!fileName.endsWith(".txt"))
			fileName += ".txt";
		fileName = dir + fileName;
		File file = new File(fileName);
		FileWriter fw = new FileWriter(file);
		fw.write(message);
		fw.flush();
		fw.close();
		processUI.download(file);
		return "File exported successfully " + fileName;
	}
	
	private String numberToText (DecimalFormat format, String strFormat, double amt)
	{
		String strAmt = format.format(amt);
		strAmt = String.format(strFormat, strAmt);
		return strAmt;
	}
}
