/**
 * 
 */
package com.unicore.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MOrg;
import org.compiere.model.MWarehouse;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.MBPartner;

/**
 * @author Burhani Adam
 *
 */
public class CreateSOFromPO extends SvrProcess {

	/**
	 * 
	 */
	public CreateSOFromPO() {
		// TODO Auto-generated constructor stub
	}
	
	private String p_POno = null;
	private int p_WhsID = 0;
	private Timestamp p_Date = null;
	private int p_PriceListID = 0;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("DocumentNo"))
				p_POno = param.getParameterAsString();
			else if(param.getParameterName().equals("M_Warehouse_ID"))
				p_WhsID = param.getParameterAsInt();
			else if(param.getParameterName().equals("DateOrdered"))
				p_Date = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("M_PriceList_ID"))
				p_PriceListID = param.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MWarehouse whs = MWarehouse.get(getCtx(), p_WhsID);
		String sql = "SELECT o.C_Order_ID FROM C_Order o"
				+ " INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = o.C_BPartner_ID"
				+ " INNER JOIN C_DocType dt ON dt.C_DocType_ID = o.C_DocType_ID"
				+ " WHERE o.IsSOTrx = 'N' AND o.DocStatus IN ('CO', 'CL') AND o.DocumentNo = ?"
				+ " AND dt.DocBaseType = 'POO'"
				+ " AND bp.Value = (SELECT org.Value FROM AD_Org org WHERE org.AD_Org_ID = ?)";
		int poID = DB.getSQLValue(get_TrxName(), sql, new Object[]{p_POno, whs.getAD_Org_ID()});
		
		if(poID <= 0)
			return "Document not found.";
		
		MOrder po = new MOrder(getCtx(), poID, get_TrxName());
		if(po.get_ValueAsInt("Ref_Order_ID") > 0)
		{
			String sql2 = "SELECT DocumentNo FROM C_Order WHERE C_Order_ID = ?";
			String docNo = DB.getSQLValueString(get_TrxName(), sql2, po.get_ValueAsInt("Ref_Order_ID"));
			return "PO already has a SO. " + docNo;
		}
		MOrg orgFrom = MOrg.get(getCtx(), po.getAD_Org_ID());
		org.compiere.model.MBPartner bpFrom = MBPartner.get(getCtx(), po.getC_BPartner_ID());
		org.compiere.model.MBPartner bpTo = MBPartner.get(getCtx(), orgFrom.getValue());
		
		if(bpTo == null)
			return "Not found business partner with value " + orgFrom.getValue();
		MBPartnerLocation[] loc = MBPartnerLocation.getForBPartner(getCtx(), bpTo.get_ID(), get_TrxName());
		if(loc.length == 0)
			return "Not found location from business partner " + bpTo.getValue();
		MOrg orgTo = MOrg.get(getCtx(), bpFrom.getValue(), get_TrxName());
		
		MOrder so = new MOrder(getCtx(), 0, get_TrxName());
		so.setAD_Org_ID(orgTo.get_ID());
		so.setC_BPartner_ID(bpTo.get_ID());
		so.setC_BPartner_Location_ID(loc[0].get_ID());
		so.setDateOrdered(p_Date);
		so.setC_DocTypeTarget_ID(DB.getSQLValue(get_TrxName(), "SELECT C_DocType_ID FROM C_DocType WHERE DocSubTypeSO = 'SO'"));
		so.setM_PriceList_ID(p_PriceListID);
		so.setIsSOTrx(true);
		so.setM_Warehouse_ID(whs.get_ID());
		so.saveEx();
		
		MOrderLine[] lFrom = po.getLines();
		
		for(int i=0;i<lFrom.length;i++)
		{
			MOrderLine lTo = new MOrderLine(so);
			lTo.setM_Product_ID(lFrom[i].getM_Product_ID());
			lTo.setQty(lFrom[i].getQtyOrdered());
			lTo.saveEx();
		}
		
		po.set_ValueOfColumn("Ref_Order_ID", so.get_ID());
		po.saveEx();
		
		return "Success created SO. " + so.getDocumentNo();
	}
}