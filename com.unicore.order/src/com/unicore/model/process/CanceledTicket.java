package com.unicore.model.process;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSWBTicketConfirm;
import com.unicore.model.MUNSWeighbridgeTicket;

public class CanceledTicket extends SvrProcess {

	private Timestamp p_DateCanceled = null;
	private String p_time = null; 
	private boolean isCancelledCancel = false;
	private MUNSWeighbridgeTicket m_ticket = null;
	private MUNSWBTicketConfirm m_confirm = null;
	
	public CanceledTicket() {
		
	}

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for(int i = 0;i < para.length; i++)
		{
			String name = para[i].getParameterName();
			
			if(name.equals("DateCanceled"))
				p_DateCanceled = para[i].getParameterAsTimestamp();
			else if(name.equals("isCancelledCancel"))
				isCancelledCancel = para[i].getParameterAsBoolean();
			else if(name.equals("Time"))
				p_time = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter : "+name);
		}
		
		if(getTable_ID() == MUNSWeighbridgeTicket.Table_ID)
		{
			m_ticket = new MUNSWeighbridgeTicket(getCtx(), getRecord_ID(), get_TrxName());
			if(m_ticket == null)
				throw new AdempiereException("Cannot get Weighbridge Ticket");
		}
		else if(getTable_ID() == MUNSWBTicketConfirm.Table_ID)
		{
			m_confirm = new MUNSWBTicketConfirm(getCtx(), getRecord_ID(), get_TrxName());
			if(m_confirm == null)
				throw new AdempiereException("Cannot get Ticket Confirm");
		}
		else
			throw new AdempiereException("Wrong use of process");

	}

	@SuppressWarnings("unused")
	@Override
	protected String doIt() throws Exception {
		
		String retVal = null;
		
		if((p_DateCanceled == null || p_time == null) && !isCancelledCancel)
			throw new AdempiereException("Parameter cannot null");
		
		if(m_ticket == null)
		{
			m_ticket = m_confirm.getTicket();
			if(m_ticket == null)
				throw new AdempiereException("Error when try to get Weighbridge Ticket");
		}
		else if(m_confirm == null)
		{
			m_confirm = MUNSWBTicketConfirm.get(getCtx(), m_ticket.getUNS_WeighbridgeTicket_ID(), get_TrxName());
			if(m_confirm == null)
				throw new AdempiereException("Error when try to get Ticket Confirm");
		}
		
		if((m_ticket.isSplitOrder() && !m_ticket.isOriginalTicket())
				|| (m_confirm.isSplitOrder() && !m_confirm.isOriginalTicket()))
			throw new AdempiereException("Can only be canceled from original");
		
		if((m_confirm.getDocStatus().equals(MUNSWeighbridgeTicket.DOCSTATUS_Completed)
				|| m_confirm.getDocStatus().equals(MUNSWeighbridgeTicket.DOCSTATUS_Closed)) && !isCancelledCancel)
		{
			throw new AdempiereException("Cannot cancel. Document Confirm was complete.");
		}
		
		if(!isCancelledCancel)
		{
			if(m_ticket.isCancelled() && m_confirm.isCancelled())
				throw new AdempiereException("This ticket was canceled");
			
			Timestamp DateCancel = getDate(p_DateCanceled, p_time);
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(DateCancel.getTime());
			cal.add(Calendar.HOUR, 1);
			Timestamp TimeOut = new Timestamp(cal.getTime().getTime());
			
			m_confirm.setIsCancelled(true);
			m_confirm.setTimeIn(DateCancel);
			m_confirm.setTimeOut(TimeOut);
			
			if(m_ticket.isSplitOrder())
			{
				MUNSWeighbridgeTicket splitTicket = 
						new MUNSWeighbridgeTicket(getCtx(), m_ticket.getSplittedTicket_ID(), get_TrxName());
				
				splitTicket.setIsCancelled(true);
				splitTicket.saveEx();
			}
			
			m_ticket.setIsCancelled(true);
			
			if(m_confirm.save() && m_ticket.save())
				retVal = "Succes";
			else
				throw new AdempiereException("Error when try to save ticket");
		}
		else
		{
			if(m_confirm.getDocStatus().equals(MUNSWBTicketConfirm.DOCSTATUS_Completed)
					|| m_confirm.getDocStatus().equals(MUNSWBTicketConfirm.DOCSTATUS_Drafted))
			{
				Boolean isSplit = m_confirm.isSplitOrder();
				MUNSWeighbridgeTicket splitTicket = null;
				MUNSWBTicketConfirm splitConfirm = null;
				
				if(isSplit)
				{
					splitTicket = new MUNSWeighbridgeTicket(getCtx(), m_ticket.getSplittedTicket_ID(), get_TrxName());
					
					if(splitTicket == null)
						throw new AdempiereException("Cannot found Split Ticket");
					
					splitConfirm = MUNSWBTicketConfirm.get(getCtx(), splitTicket.getUNS_WeighbridgeTicket_ID(), get_TrxName());
					
					if(splitConfirm == null)
						throw new AdempiereException("Cannot found Split Ticket Confirm");
				}
				
				if(m_confirm.getDocStatus().equals(MUNSWBTicketConfirm.DOCSTATUS_Completed))
				{
					retVal = m_ticket.move(false, m_ticket.getDateDoc());
					if(retVal != null)
						throw new AdempiereException(retVal);
					
					if(isSplit)
					{
						retVal = splitTicket.move(false, splitTicket.getDateDoc());
						if(retVal != null)
							throw new AdempiereException(retVal);
						
						splitConfirm.setProcessed(false);
						splitConfirm.setDocAction(MUNSWBTicketConfirm.DOCACTION_Complete);
						splitConfirm.setDocStatus(MUNSWBTicketConfirm.DOCSTATUS_Drafted);
					}	
					
					m_confirm.setProcessed(false);
					m_confirm.setDocAction(MUNSWBTicketConfirm.DOCACTION_Complete);
					m_confirm.setDocStatus(MUNSWBTicketConfirm.DOCSTATUS_Drafted);
				}
				
				m_confirm.setIsCancelled(false);
				m_confirm.setGrossWeight(m_confirm.getGrossWeightTarget());
				m_confirm.setTare(m_confirm.getTareTarget());
				m_confirm.setTimeIn(m_ticket.getTimeIn());
				m_confirm.setTimeOut(m_ticket.getTimeOut());
				
				if(isSplit)
				{					
					splitConfirm.setIsCancelled(false);
					splitConfirm.setGrossWeight(splitConfirm.getGrossWeightTarget());
					splitConfirm.setTare(splitConfirm.getTareTarget());
					splitConfirm.setTimeIn(m_ticket.getTimeIn());
					splitConfirm.setTimeOut(m_ticket.getTimeOut());
					
					splitTicket.setIsCancelled(false);
					if(!splitTicket.save() && !splitConfirm.save())
						throw new AdempiereException("Error when try to save split ticket");
				}
				
				m_ticket.setIsCancelled(false);
				
				if(m_confirm.save() && m_ticket.save())
					retVal = "Succes";
				else
					throw new AdempiereException("Error when try to save ticket");
				
			}
			else
			{
				throw new AdempiereException("Cannot process. Check status document, it should be complete or draft");
			}
			
		}
		
		return retVal;
	}
	
	private Timestamp getDate(Timestamp date, String timeString)
	{
		Timestamp datetime = null;
			
		if(timeString.length()!=4)
			throw new AdempiereException();
		
		int timeInt = new Integer(timeString);
		
		if(timeInt > -1 && timeInt < 2360)
		{
			String dateString = date.toString();
			String dateTimeString = dateString.substring(0, 10) + " " + timeString.substring(0, 2) + ":" + timeString.substring(2) + ":00";
			
			datetime = Timestamp.valueOf(dateTimeString);
		}		
		
		return datetime;
	}

}
