/**
 * 
 */
package com.unicore.model.process;

import java.math.RoundingMode;
import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankAccount;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSSASummary;
import com.unicore.model.MUNSStatementOfAccount;
import com.uns.util.MessageBox;

/**
 * @author Burhani Adam
 *
 */
public class UNSSAGeneratePayment extends SvrProcess {

	private int p_BPartnerID = 0;
	private Timestamp p_DateTrx = null;
	private int p_BankAcct_ID = 0;
	private int p_ChequeListID = 0;
	private int p_CurrencyID = 0;
	private MPayment payment = null;
	boolean haveLine = false;
	/**
	 * 
	 */
	public UNSSAGeneratePayment() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter(); 
		for(ProcessInfoParameter param : params)
		{
			if(("C_BankAccount_ID").equals(param.getParameterName()))
				p_BankAcct_ID = param.getParameterAsInt();
			else if(("C_BPartner_ID").equals(param.getParameterName()))
				p_BPartnerID = param.getParameterAsInt();
			else if(("DateTrx").equals(param.getParameterName()))
				p_DateTrx = param.getParameterAsTimestamp();
			else if(("UNS_Cheque_List_ID").equals(param.getParameterName()))
				p_ChequeListID = param.getParameterAsInt();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		boolean isFromHeader = getTable_ID() == MUNSSASummary.Table_ID;
		MBankAccount acct = MBankAccount.get(getCtx(), p_BankAcct_ID);
		p_CurrencyID = acct.getC_Currency_ID();
		if(isFromHeader)
		{
			MUNSSASummary sa = new MUNSSASummary(getCtx(), getRecord_ID(), get_TrxName());
			MUNSStatementOfAccount[] lines = sa.getLines("C_Currency_ID = " + p_CurrencyID);
			for(MUNSStatementOfAccount line : lines)
			{
				if(line.getC_Payment_ID() > 0)
					continue;
				if(payment == null)
				{
					if(!createPayment(sa))
						throw new AdempiereException("Failed when trying update payment on statement of account (summary).!");
				}
				
				if(!createAllocate(line))
					throw new AdempiereException("Failed when trying update payment on statement of account.!");
			}
		}
		else
		{
			MUNSStatementOfAccount soa = new MUNSStatementOfAccount(getCtx(), getRecord_ID(), get_TrxName());
			if(payment == null)
			{
				if(payment == null)
				{
					if(!createPayment(soa))
						throw new AdempiereException("Failed when trying update payment on statement of account (summary).!");
				}
				
				if(!createAllocate(soa))
					throw new AdempiereException("Failed when trying update payment on statement of account.!");
			}
		}
		
		if(!haveLine)
			payment.deleteEx(true);
		else
		{
			payment.load(get_TrxName());
			if(payment.getUNS_Cheque_List_ID() > 0)
			{
				String sql = "SELECT Name FROM UNS_Cheque_List WHERE UNS_Cheque_List_ID = ?";
				String chequeNo = DB.getSQLValueString(payment.get_TrxName(), sql, payment.getUNS_Cheque_List_ID());
				payment.setCheckNo(chequeNo);
				payment.saveEx();
			}
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), "Payment has created, #"
					+ payment.getDocumentNo() + " #" + payment.getTotalAmt().setScale(2, RoundingMode.HALF_UP) + ". Continue for complete.",
					"Complete confirmation.", MessageBox.YESNO, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_YES || MessageBox.RETURN_OK == retVal)
				complete();
		}
		
		return "Payment #" + payment.getDocumentNo();
	}
	
	private boolean createPayment(PO po)
	{
		payment = new MPayment(getCtx(), 0, get_TrxName());
		payment.setC_BankAccount_ID(p_BankAcct_ID);
		payment.setAD_Org_ID(po.getAD_Org_ID());
		payment.setDateTrx(p_DateTrx);
		payment.setDateAcct(p_DateTrx);
		payment.setC_DocType_ID(false);
		payment.setC_BPartner_ID(p_BPartnerID);
		payment.setDescription((String) po.get_Value("Description"));
		payment.setC_Currency_ID(p_CurrencyID);
		if(p_ChequeListID > 0)
		{
			payment.setTenderType(MPayment.TENDERTYPE_ChequeGiro);
			payment.setUNS_Cheque_List_ID(p_ChequeListID);
		}
		else
			payment.setTenderType(MPayment.TENDERTYPE_Transfer);
		if(!payment.save())
			throw new AdempiereException("Payment not created.!");
		
		po.set_ValueOfColumn("C_Payment_ID", payment.get_ID());
		return po.save();
	}
	
	private boolean createAllocate(MUNSStatementOfAccount soa)
	{
		MPaymentAllocate allocate = new MPaymentAllocate(getCtx(), 0, get_TrxName());
		allocate.setC_Payment_ID(payment.get_ID());
		allocate.setAD_Org_ID(soa.getAD_Org_ID());
		allocate.setC_Invoice_ID(soa.getC_Invoice_ID());
		allocate.setC_BPartner_ID(soa.getC_BPartner_ID());
		allocate.setInvoiceAmt(soa.getGrandTotal());
		allocate.setPayToOverUnderAmount(soa.getGrandTotal());
		allocate.setAmount(soa.getGrandTotal());
		allocate.setDescription(soa.getDescription());
		allocate.setOverUnderAmt(Env.ZERO);
		if(!allocate.save())
			throw new AdempiereException("Payment Allocated not created.!");
		soa.setC_Payment_ID(payment.get_ID());
		haveLine = true;
		return soa.save();
	}
	
	private void complete()
	{
		try {
			if(!payment.processIt("CO") || !payment.save())
				throw new AdempiereException(CLogger.retrieveErrorString(payment.getProcessMsg()));
		} catch (Exception e) {
			throw new AdempiereException(e.getMessage());
		}
	}
}