/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MPriceList;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.wf.MWorkflow;

import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.MUNSAccessoriesLine;
import com.unicore.model.MUNSPreOrder;
import com.unicore.model.MUNSPreOrderLine;
import com.unicore.model.UNSOrderDiscountModel;
import com.uns.util.MessageBox;

/**
 * @author ALBURHANY
 *
 */
public class CreateSalesOrder extends SvrProcess {

	private int m_PriceList_ID = 0;
	private int m_Order_ID = 0;
	private int m_C_DocType_ID = 0;
	private int m_SalesRep_ID = 0;
	private int m_Warehouse_ID = 0;
	private int c_paymentterm_ID = 0;
	//private boolean m_BaseProductType;
	private int x;
	/**
	 * 
	 */
	public CreateSalesOrder() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("M_PriceList_ID"))
				m_PriceList_ID = param.getParameterAsInt();
			else if(param.getParameterName().equals("C_DocType_ID"))
				m_C_DocType_ID = param.getParameterAsInt();
			else if(param.getParameterName().equals("SalesRep_ID"))
				m_SalesRep_ID = param.getParameterAsInt();
			else if(param.getParameterName().equals("C_Order_ID"))
				m_Order_ID = param.getParameterAsInt();
			else if(param.getParameterName().equals("M_Warehouse_ID"))
				m_Warehouse_ID = param.getParameterAsInt();
			else if (param.getParameterName().equals("C_PaymentTerm_ID"))
				c_paymentterm_ID = param.getParameterAsInt();
			else if (param.equals("BaseProductType"))
				;//m_BaseProductType = param.getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}
	
	/**
	 * Check parameter
	 * @param C_Order_ID
	 * @param M_PriceList_ID
	 * @param M_Warehouse_ID
	 * @param SalesRep_ID
	 * @param C_Paymentterm_ID
	 * @return null
	 */
	public String cekParams(int C_Order_ID, int M_PriceList_ID, int M_Warehouse_ID, int C_Paymentterm_ID, int SalesRep_ID )
	{
		if(C_Order_ID == 0)
		{
			if(M_PriceList_ID == 0)
				throw new AdempiereException("Please Define PriceList");
			
			if(M_Warehouse_ID == 0)
				throw new AdempiereException("Please Define Warehouse");
			
			if(SalesRep_ID == 0)
				throw new AdempiereException("Please Define Sales Representative");
		}
		
		if (m_C_DocType_ID == 0)
			return "Please fill the Document Target Type of the sales order to be created.";
		
//		if(C_Order_ID != 0)
//		{
//			MOrder order = new MOrder(getCtx(), C_Order_ID, get_TrxName());
//			m_PriceList_ID = order.getM_PriceList_ID();
//			m_Warehouse_ID = order.getM_Warehouse_ID();
//			m_SalesRep_ID = order.getSalesRep_ID();
//		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{	
		String msg = cekParams(m_Order_ID, m_PriceList_ID, m_Warehouse_ID, c_paymentterm_ID, m_SalesRep_ID);
		
		if (msg != null)
			return msg;
		
		MUNSPreOrder preOrder = new MUNSPreOrder(getCtx(), getRecord_ID(), get_TrxName());
		MPriceList priceList = new MPriceList(getCtx(), m_PriceList_ID, get_TrxName());
		
		StringBuffer sql1 = new StringBuffer
				("SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE C_BPartner_ID = " + preOrder.getC_BPartner_ID());
		int idBPL = DB.getSQLValue(get_TrxName(), sql1.toString());
		if (idBPL <= 0)
				throw new AdempiereUserError("The Customer does not have location defined yet.");
		
		int C_DocTypeTarget_ID = m_C_DocType_ID;
//				DB.getSQLValue(get_TrxName(),
//				"SELECT C_DocTYpe_ID FROM C_DocTYpe WHERE DocSubTypeSO = '" + MDocType.DOCSUBTYPESO_StandardOrder +"'");
		String paymentRule = MOrder.PAYMENTRULE_OnCredit;
		MDocType dt = MDocType.get(getCtx(), C_DocTypeTarget_ID);
		
		if (dt.getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_CashOrder))
			paymentRule = MOrder.PAYMENTRULE_Cash;
		
//		String sql = "SELECT COUNT(DISTINCT(ProductType)) FROM M_Product WHERE M_Product_ID IN"
//					+ " (SELECT M_Product_ID FROM M_Product WHERE M_Product_ID IN"
//					+ " (SELECT M_Product_ID FROM UNS_PreOrder_Line WHERE UNS_PreOrder_ID= " + preOrder.get_ID() + "))";
//		int productType = DB.getSQLValue(get_TrxName(), sql.toString());
//		
//		if(productType > 0)
//		{
//			for(int i=1; i <= productType; i++)
		
		String sql = "SELECT COUNT(DISTINCT(ProductType)) FROM M_Product WHERE M_Product_ID IN"
					 + " (SELECT M_Product_ID FROM UNS_PreOrder_Line WHERE UNS_PreOrder_ID= " + preOrder.get_ID()
					 + " ) GROUP BY ProductType";
		int sumType = DB.getSQLValue(get_TrxName(), sql);
		
		for(int i = 0; i < sumType; i++)
		{	
			x = 0;
			MOrder order = new MOrder(getCtx(), m_Order_ID, get_TrxName());
			
			if(m_Order_ID == 0)
			{
				order.setAD_Org_ID(preOrder.getAD_Org_ID());
				order.setC_DocTypeTarget_ID(C_DocTypeTarget_ID);
				order.setPaymentRule(paymentRule);
				order.setC_BPartner_ID(preOrder.getC_BPartner_ID());
				order.setC_BPartner_Location_ID(idBPL);
				order.setM_PriceList_ID(m_PriceList_ID);
				order.setM_Warehouse_ID(m_Warehouse_ID);
				order.setSalesRep_ID(m_SalesRep_ID);
				order.setDescription(preOrder.getDescription());
				order.setIsOneShipment(preOrder.isOneShipment());
				if (c_paymentterm_ID > 0)
					order.setC_PaymentTerm_ID(c_paymentterm_ID);
				order.saveEx();
			}
			
			if(m_Order_ID != 0)
			{
				m_PriceList_ID = order.getM_PriceList_ID();
				m_SalesRep_ID = order.getSalesRep_ID();
				m_Warehouse_ID = order.getM_Warehouse_ID();
				c_paymentterm_ID = order.getC_PaymentTerm_ID();
			}
			
			preOrder.setC_Order_ID(order.get_ID());
			preOrder.setCreateSO("Y");
			preOrder.setDocStatus("CL");
			preOrder.setDocAction("CL");
			preOrder.saveEx();
			
			String sqlProductType = "SELECT Array_To_String(Array_AGG(DISTINCT(ProductType)), ';') FROM M_Product WHERE M_Product_ID IN"
					+ " (SELECT M_Product_ID FROM UNS_PreOrder_Line WHERE UNS_PreOrder_ID= " + preOrder.get_ID()
					+ ")";
					
			String value = DB.getSQLValueString(get_TrxName(), sqlProductType);
			String[] values = value.split(";");
			String whereClause = "M_Product_ID IN (SELECT M_Product_ID FROM M_Product WHERE ProductType = '" + values[i] + "')";
		
			for (MUNSPreOrderLine poLine : preOrder.getLine(whereClause, null))
			{	
				if(poLine.getC_OrderLine_ID() != 0)
					continue;
				
//				String sql2 = "SELECT PriceList FROM M_ProductPrice WHERE M_Product_ID = " + poLine.getM_Product_ID()
//							+ " AND M_PriceList_Version_ID IN "
//							+ " (SELECT M_PriceList_Version_ID FROM M_PriceList_Version WHERE M_PriceList_ID = " + m_PriceList_ID + ")";
				String sql2 = "SELECT PriceList FROM M_ProductPrice pp "
						+ "	INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
						+ " WHERE M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
						+ " ORDER BY plv.ValidFrom DESC";
				BigDecimal price = 
						DB.getSQLValueBD(get_TrxName(), sql2, 
								poLine.getM_Product_ID(), m_PriceList_ID, preOrder.getDatePromised());
				
				if (price == null)
						throw new AdempiereException("Product " + poLine.getM_Product().getName()
								+ " Not In Price List " + priceList.getName());
				
				MOrderLine orderLine = new MOrderLine(getCtx(), 0, get_TrxName());
				
				if (price.compareTo(Env.ZERO) >= 0)
				{
					orderLine.setC_Order_ID(order.get_ID());
					orderLine.setAD_Org_ID(poLine.getAD_Org_ID());
					orderLine.setM_Product_ID(poLine.getM_Product_ID());
					orderLine.setQtyEntered(poLine.getQty());
					orderLine.setC_UOM_ID(poLine.getC_UOM_ID());
					orderLine.setDescription(poLine.getDescription());
					orderLine.setIsManual(false);
					
					if(poLine.getSettingPriceType().equals("DP"))
					{
						orderLine.setPriceEntered(price);
						orderLine.setPriceActual(price);
						orderLine.setPriceList(price);
						orderLine.saveEx();
					}
					if(poLine.getSettingPriceType().equals("MWP"))
					{	
//						orderLine.setPrice(poLine.getPriceEntered());
						orderLine.setPriceList(price);
						orderLine.setDiscountAmt(price.subtract(poLine.getPriceEntered()));
						//orderLine.setPriceEntered(poLine.getPriceEntered());
						orderLine.saveEx();
					}
					if(poLine.getSettingPriceType().equals("MWD"))
					{
						if(poLine.getDiscount().compareTo(Env.ZERO) == 1)
						{
							BigDecimal discAmt = (price.multiply(poLine.getDiscount()).divide(Env.ONEHUNDRED));
//							BigDecimal totalDiscAmt = discAmt.multiply(poLine.getQty());
							
							orderLine.setDiscount(poLine.getDiscount());
							//orderLine.setDiscountAmt(totalDiscAmt);
//							orderLine.setPriceEntered(price.subtract(discAmt));
//							orderLine.setPriceActual(price.subtract(discAmt));
//							orderLine.setPriceEntered(price);
//							orderLine.setPriceActual(price);
							orderLine.setPrice(price.subtract(discAmt));
							orderLine.setPriceList(price);
							orderLine.saveEx();
						}
						else if(poLine.getDiscountAmt().compareTo(Env.ZERO) == 1)
						{
							if(price.compareTo(poLine.getDiscountAmt()) < 0)
								throw new AdempiereUserError("Price cannot negate, please check Pre Order Line"
										+ " (" + poLine.getM_Product().getName() +")");
							
//TODO comment, unused @Menjangan							BigDecimal discount = Env.ZERO;
							
//							if (price.signum() > 0) {
//								discount = poLine.getDiscountAmt().divide(price, 4, BigDecimal.ROUND_HALF_DOWN)
//										.multiply(Env.ONEHUNDRED);
//							}
							
							BigDecimal discAmt = poLine.getDiscountAmt();
							discAmt = discAmt.multiply(poLine.getQty());
							
							//orderLine.setPriceEntered(price);
							//orderLine.setPrice(price.subtract(poLine.getDiscountAmt()));
							orderLine.setPrice(price);
							orderLine.setPriceList(price);
							orderLine.setDiscountAmt(discAmt);
							//orderLine.setDiscount(discount);
							orderLine.saveEx();
						}
						else
						{
							orderLine.setPriceEntered(price);
							orderLine.setPriceActual(price);
							orderLine.setPriceList(price);
							orderLine.saveEx();
						}
					}
					
					for(MUNSAccessoriesLine line : MUNSAccessoriesLine.getLinesByParent(getCtx(), MUNSPreOrderLine.Table_Name,
							poLine.get_ID(), get_TrxName()))
					{
						line.copyFrom(line, MOrderLine.Table_Name, orderLine.get_ID());
					}
					
//					if(poLine.getM_Product().getProductType().equals("S"))
//					{
//						orderLine.setQtyInvoiced(poLine.getQty());
//						orderLine.saveEx();
//						
//						if(x == 0)
//						{
//							MInvoice inv = new MInvoice(getCtx(), 0, get_TrxName());
//						
//							inv.setAD_Org_ID(order.getAD_Org_ID());
//							inv.setC_DocTypeTarget_ID(MDocType.getDocType(MDocType.DOCBASETYPE_ARInvoice));
//							inv.setDateInvoiced(preOrder.getDatePromised());
//							inv.setDateOrdered(order.getDateOrdered());
//							inv.setC_BPartner_ID(order.getC_BPartner_ID());
//							inv.setM_PriceList_ID(order.getM_PriceList_ID());
//							inv.setC_Order_ID(order.get_ID());
//							inv.saveEx();
//							idInv = inv.get_ID();
//						}
//						
//						MInvoiceLine invLine = new MInvoiceLine(getCtx(), 0, get_TrxName());
//						invLine.setC_Invoice_ID(idInv);
//						invLine.setAD_Org_ID(orderLine.getAD_Org_ID());
//						invLine.setQtyEntered(orderLine.getQtyEntered());
//						invLine.setM_Product_ID(orderLine.getM_Product_ID(), orderLine.getC_UOM_ID());
//						invLine.setPriceActual(orderLine.getPriceActual());
//						invLine.setPriceEntered(orderLine.getPriceEntered());
//						invLine.setPriceList(orderLine.getPriceList());
//						invLine.setQtyInvoiced(orderLine.getQtyInvoiced());
//						invLine.setLineNetAmt();
//						invLine.saveEx();
//					}
					x = x + 1;
				}
				poLine.setC_OrderLine_ID(orderLine.get_ID());
				poLine.saveEx();
				
//				String upAccessoriesLine = "UPDATE UNS_AccessoriesLine SET C_OrderLine_ID=? WHERE UNS_PreOrder_Line_ID=?";
//				boolean success = DB.executeUpdate(upAccessoriesLine, new Object[]{orderLine.get_ID(), poLine.get_ID()},
//						false, get_TrxName()) < 0 ? false : true;
//				if(!success)
//					throw new AdempiereUserError("Failed when trying update accessories line to order line");
			}
			
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), 
					"Do you want to run calculate discount automaticaly ? ", 
					"Automaticaly Calculate Discount", 
					MessageBox.YESNO, MessageBox.ICONQUESTION);
			if (MessageBox.RETURN_YES == retVal)
			{
				UNSOrderDiscountModel model = new UNSOrderDiscountModel(order);
				OrderCalculateDiscount calculator = new OrderCalculateDiscount(model);
				calculator.run();
			}
			
			retVal = MessageBox.showMsg(getCtx(), getProcessInfo()
					, "Sales Order has been created. \n" +
					  "Do you want to complete that document ?"
					, "Sales Order auto completion confirmation"
					, MessageBox.YESNO
					, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_YES)
			{
//				if(!order.processIt(DocAction.ACTION_Complete))
//					throw new AdempiereUserError(order.getProcessMsg());
//				if(!order.save())
//					throw new AdempiereException(order.getProcessMsg());
				
				try
				{
					if (!order.isComplete())
					{
						ProcessInfo orderInfo = MWorkflow.runDocumentActionWorkflow(order, DocAction.ACTION_Complete);
							if(orderInfo.isError())
							{
								return orderInfo.getSummary();
							}
					}
				}
				catch (Exception e) {
					return e.getMessage();
				}
			}
			else 
				return null;			
		}	
		
		msg = "Sales order has created";
//		String message = Msg.parseTranslation(getCtx(), "Sales Order has Created (No Document : " + order.getDocumentNo() + ")");
//		addBufferLog(0, order.getDateOrdered(), order.getGrandTotal(), message, order.get_Table_ID(), order.get_ID());
		
		return msg;
	}
}