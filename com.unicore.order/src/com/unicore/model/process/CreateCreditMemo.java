package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MInvoiceLine;
import com.uns.util.MessageBox;

/**
 * 
 * @author Mr Thunder
 *
 */
public class CreateCreditMemo extends SvrProcess {

	private int p_Charge_ID;
	private Timestamp p_DateInvoiced;
	private int p_Qty;
	private BigDecimal p_Amount;
	private int p_Tax_ID;
	private MInvoice m_invoice = null;
	
	private String m_ProcessMsg = null;
	private String m_DocumentNo = null;
	
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(int i=0;i<params.length;i++)
		{
			String name = params[i].getParameterName();
			if(name.equals("C_Charge_ID"))
				p_Charge_ID = params[i].getParameterAsInt();
			else if(name.equals("DateInvoiced"))
				p_DateInvoiced = params[i].getParameterAsTimestamp();
			else if(name.equals("Qty"))
				p_Qty = params[i].getParameterAsInt(); 
			else if(name.equals("Amount"))
				p_Amount = params[i].getParameterAsBigDecimal();
			else if(name.equals("C_Tax_ID"))
				p_Tax_ID = params[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter : "+name);
		}
		
		m_invoice = new MInvoice(getCtx(), getRecord_ID(), get_TrxName());
	}

	@SuppressWarnings("unused")
	@Override
	protected String doIt() throws Exception {

		if(p_Charge_ID <= 0)
			throw new AdempiereException("Please define charge");
		else if(p_DateInvoiced == null)
			throw new AdempiereException("Please define dateinvoiced");
		else if(p_Qty <= 0)
			throw new AdempiereException("Disallowed qty less than 1");
		else if(p_Amount.signum()!=1)
			throw new AdempiereException("Disallowed amount less than 1");
		else if(p_Tax_ID <= 0)
			throw new AdempiereException("Please define Tax");
		else if(m_invoice == null)
			throw new AdempiereException("Invoice Can't found");
		
		if(m_invoice.getCN_Invoice_ID() > 0)
			throw new AdempiereException("This Invoice is Credit Memo");
		
		MInvoice invCredit = null;
		int retVal;
		boolean isCreateNewInv = true;
		
		String description = "{**Create from Invoice Document #" + m_invoice.getDocumentNo() + " **}";
		
		String sql = "SELECT C_Invoice_ID FROM C_Invoice WHERE CN_Invoice_ID = ? AND DocStatus = 'DR'";
		int inv_id = DB.getSQLValue(get_TrxName(), sql, m_invoice.getC_Invoice_ID()); 
		
		if(inv_id > 0)
		{	
			retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), 
					"There was an Invoice Credit Memo has been created. Do you want to create new Invoice?",
					"Exist Invoice", MessageBox.YESNO, MessageBox.ICONQUESTION);
			if(MessageBox.RETURN_YES == retVal)
			{
				isCreateNewInv = true;
			}
			else
			{
				MInvoice prevInv = new MInvoice(getCtx(), inv_id, get_TrxName());
				if(prevInv == null)
					throw new AdempiereException("Invoice Can't found");
				
				if(prevInv.getDateInvoiced().compareTo(p_DateInvoiced) != 0)
				{
					retVal = MessageBox.showMsg(getCtx(), getProcessInfo(),
						"There was an Invoice Credit Memo has been created with different date. Do you want to overwrite that one?",
						"Exist Invoice",
						MessageBox.YESNO, MessageBox.ICONQUESTION);
					
					if(MessageBox.RETURN_YES == retVal)
					{
						prevInv.setDateInvoiced(p_DateInvoiced);
						prevInv.setDateAcct(p_DateInvoiced);
						prevInv.saveEx();
					}
						
				}
				
				m_ProcessMsg = createLine(prevInv);
				if(m_ProcessMsg != null)
					throw new AdempiereException(m_ProcessMsg);
				isCreateNewInv = false;
				
				m_DocumentNo = prevInv.getDocumentNo();
			}
			
		}
		
		if(isCreateNewInv)
		{
			invCredit = new MInvoice(getCtx(), 0, get_TrxName());
			PO.copyValues(m_invoice, invCredit);
			invCredit.setClientOrg(m_invoice.getAD_Client_ID(), m_invoice.getAD_Org_ID());
			invCredit.setC_DocTypeTarget_ID(m_invoice.isSOTrx() ? MDocType.DOCBASETYPE_ARCreditMemo : MDocType.DOCBASETYPE_APCreditMemo);
			invCredit.setCN_Invoice_ID(m_invoice.getC_Invoice_ID());
			invCredit.setDocumentNo(null);
			invCredit.setDescription(description);
			invCredit.setDateInvoiced(p_DateInvoiced);
			invCredit.setDateAcct(p_DateInvoiced);
			invCredit.setProcessed(false);
			invCredit.saveEx();
			
			m_ProcessMsg = createLine(invCredit);
			if(m_ProcessMsg != null)
				throw new AdempiereException(m_ProcessMsg);
			
			m_DocumentNo = invCredit.getDocumentNo();
		}
	
		return m_DocumentNo;
	}
	
	private String createLine(MInvoice inv)
	{
		String sql = "SELECT Line FROM C_InvoiceLine WHERE C_Charge_ID = ? AND C_Invoice_ID = ? AND IsActive = 'Y'";
		int line_id = DB.getSQLValue(get_TrxName(), sql, p_Charge_ID, inv.getC_Invoice_ID());
		
		if(line_id > 0)
			return "There was line use this charge. #Line :: "+line_id;
		
		MInvoiceLine invLine = new MInvoiceLine(inv);
		
		invLine.setC_Charge_ID(p_Charge_ID);
		invLine.setQty(p_Qty);
		invLine.setPrice(p_Amount);
		invLine.setPriceList(p_Amount);
		invLine.setC_Tax_ID(p_Tax_ID);
		invLine.setProcessed(false);
		invLine.saveEx();
		return null;
	}

}
