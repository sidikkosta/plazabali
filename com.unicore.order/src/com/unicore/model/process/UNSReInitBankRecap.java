package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.TimeUtil;

import com.unicore.model.MUNSCardTrxDetail;
import com.unicore.model.MUNSPOSPayment;
import com.unicore.model.MUNSPaymentTrx;
import com.uns.model.MUNSBankEDCBatchRecap;
import com.uns.model.MUNSDailyBankCardTypeRecap;
import com.uns.util.MessageBox;

public class UNSReInitBankRecap extends SvrProcess {

	private Timestamp p_DateFrom = null;
	private Timestamp p_DateTo = null;
	
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			String name = param.getParameterName();
			if(name.equals("DateFrom"))
				p_DateFrom = param.getParameterAsTimestamp();
			else if(name.equals("DateTo"))
				p_DateTo = param.getParameterAsTimestamp();
			else
				throw new AdempiereException("Unknown Parameter Name :"+name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		if(p_DateFrom == null || p_DateTo == null)
			throw new AdempiereException("Mandatory Field Date From and Date To");
		
		if(p_DateFrom.compareTo(p_DateTo) > 0)
			throw new AdempiereException("Disallowed Date From greater than Date To");
		
		int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), "You want to process from "
				+p_DateFrom.toString().substring(0, 10)+" until "+p_DateTo.toString().substring(0, 10)
				+". All data on the date range has been created will be deleted. Continue Process?"
				, "Just Make Sure", MessageBox.YESNO, MessageBox.ICONQUESTION);
		if(MessageBox.RETURN_YES != retVal)
		{
			throw new AdempiereException("Process Cancelled");
		}
		
		p_DateTo = TimeUtil.addDays(p_DateTo, 1);
		
		Object[] parameters = new Object[]{p_DateFrom, p_DateTo};
		
		//delete all recap first		
		String sql = "DELETE FROM UNS_BankEDCBatchRecap WHERE DateTrx BETWEEN ? AND ?";
		boolean ok = DB.executeUpdateEx(sql, parameters, get_TrxName()) != -1;
		if(!ok)
			throw new AdempiereException("Error when try to delete bank edc batch recap");
		
		sql = "DELETE FROM UNS_DailyBankEDCBalance WHERE DateTrx BETWEEN ? AND ?";
		ok = DB.executeUpdateEx(sql, parameters, get_TrxName()) != -1;
		if(!ok)
			throw new AdempiereException("Error when try to delete daily bank edc balance");
	
		sql = "DELETE FROM UNS_DailyBankCardTypeRecap WHERE DateTrx BETWEEN ? AND ?";
		ok = DB.executeUpdateEx(sql, parameters, get_TrxName()) != -1;
		if(!ok)
			throw new AdempiereException("Error when try to delete daily bank card type recap");
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		int count = 1;
		int size = 0;
		
		String sqql = "SELECT pyt.UNS_PaymentTrx_ID FROM UNS_PaymentTrx pyt"
				+ " INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = pyt.UNS_POSPayment_ID"
				+ " INNER JOIN UNS_CardTrxDetail ctd ON ctd.UNS_PaymentTrx_ID = pyt.UNS_PaymentTrx_ID"
				+ " WHERE pp.DateTrx BETWEEN ? AND ? AND pyt.PaymentMethod = ? AND pp.DocStatus IN ('CO','CL')"
				+ " AND pyt.isactive = 'Y' AND ctd.BatchNo <> 'VO' AND NOT EXISTS"
				+ " (SELECT 1 FROM UNS_DisbursRecon_Line l WHERE l.UNS_CardTrxDetail_ID = ctd.UNS_CardTrxDetail_ID"
				+ " AND l.UNS_DisbursRecon_Batch_ID = 1000000)"
				+ " ORDER BY pp.DateTrx, pp.UNS_POSPayment_ID";
		
		ResultSet rs = null;
		PreparedStatement st = null;
		
		try {
			
			st = DB.prepareStatement(sqql, get_TrxName());
			st.setTimestamp(1, p_DateFrom);
			st.setTimestamp(2, p_DateTo);
			st.setString(3, MUNSPaymentTrx.PAYMENTMETHOD_Card);
			rs = st.executeQuery();
			
			while(rs.next())
			{
				size = size+1;
				list.add(rs.getInt(1));
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new AdempiereException(ex.getMessage());
		}
		
		finally{
			DB.close(rs, st);
		}
		
		for(int i=0; i<list.size(); i++)
		{
			processUI.statusUpdate("Processing " + count + " from " + size);
			MUNSPaymentTrx trx = new MUNSPaymentTrx(getCtx(), list.get(i), get_TrxName());
			
			MUNSPOSPayment payment = trx.getParent();
			
			MUNSCardTrxDetail card = MUNSCardTrxDetail.get(trx);
			if(card == null)
				throw new AdempiereException("Cannot found Card Trx Detail. Check POS Payment No: "+payment.getDocumentNo());
			
			BigDecimal amt = trx.getConvertedAmt();
			String sDate = payment.getDateTrx().toString().substring(0, 10);
			Timestamp dateTrx = Timestamp.valueOf(sDate+" 00:00:00");
			
			if(trx.getTrxType().equals(MUNSPaymentTrx.TRXTYPE_RefundEDC))
			{
				MUNSPOSPayment oriPayment = MUNSPOSPayment.getRefPaymentRefund(
						getCtx(), payment.getUNS_POSPayment_ID(), get_TrxName());
				
				if(oriPayment == null)
				{
//					throw new AdempiereException("Cannot found Original Payment for this payment refund");
					continue;
				}
				
				String sqll = "SELECT UNS_POS_Session_ID FROM UNS_POSTrx WHERE"
						+ " UNS_POSTrx_ID = ?";
				int SessionID = DB.getSQLValue(get_TrxName(), sqll, oriPayment.getUNS_POSTrx_ID());
				
				if(SessionID == trx.getUNS_POS_Session_ID())
				{
					String sOriDate = oriPayment.getDateTrx().toString().substring(0, 10);
					dateTrx = Timestamp.valueOf(sOriDate+" 00:00:00");
				}
			}
			
			MUNSDailyBankCardTypeRecap daily = MUNSDailyBankCardTypeRecap.getCreate(
					getCtx(), payment.getAD_Org_ID(), card.getUNS_EDC_ID(), card.getUNS_CardType_ID()
					, dateTrx, get_TrxName());
			
			daily.setTotalARAmount(daily.getTotalARAmount().add(amt));
			if(!daily.save())
				throw new AdempiereException("Error when trying create EDC Recapitulation.");
			
			MUNSBankEDCBatchRecap batch = MUNSBankEDCBatchRecap.getCreate(
					getCtx(), payment.getAD_Org_ID(), card.getUNS_EDC_ID(), dateTrx, card.getBatchNo(), get_TrxName());
			batch.setTotalARAmount(batch.getTotalARAmount().add(amt));
			if(!batch.save())
			{
				throw new AdempiereException("Error when trying create EDC Recapitulation.");
			}
			count++;
			
		}
		
		return "Success";
	}

}
