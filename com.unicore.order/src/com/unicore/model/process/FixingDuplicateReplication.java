/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTransaction;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSCashReconciliation;
import com.unicore.model.MUNSEDCReconciliation;
import com.unicore.model.MUNSPOSRecap;
import com.unicore.model.MUNSPOSRecapLine;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPOSTrxLine;
import com.unicore.model.MUNSPOSTrxLineMA;
import com.unicore.model.MUNSSalesReconciliation;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.UNSApps;

/**
 * @author Burhani Adam
 *
 */
public class FixingDuplicateReplication extends SvrProcess {

	private String serverType = UNSApps.SERVER_TYPE;
//	private String serverLoc = UNSApps.SERVER_LOCATION;
	private String serverPOS = UNSApps.SERVER_POS;
	private String serverMain = UNSApps.SERVER_MAIN;
	private IProcessUI m_process;
	/**
	 * 
	 */
	public FixingDuplicateReplication() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_process = Env.getProcessUI(getCtx());
		if(serverType.equals(serverPOS))
			return "Tidak bisa dijalankan pada database mesin POS.";
		
		if(serverType.equals(serverMain))
		{
			String sql = "SELECT l.UNS_POSTrxLine_ID, ABS(l.QtyOrdered) AS QtyPOS, ABS(SUM(t.MovementQty)) AS QtyTrx,"
					+ " ABS(SUM(t.MovementQty)) - ABS(l.QtyOrdered) AS Diff FROM UNS_POSTrxLine l"
					+ " INNER JOIN UNS_POSTrx tr ON tr.UNS_POSTrx_ID = l.UNS_POSTrx_ID"
					+ " INNER JOIN M_Transaction t ON t.UNS_POSTrxLine_ID = l.UNS_POSTrxLine_ID"
					+ " WHERE tr.DocStatus IN ('CO', 'CL') AND tr.IsPaid = 'Y'"
					+ " GROUP BY l.UNS_POSTrxLine_ID"
					+ " HAVING ABS(SUM(t.MovementQty)) <> ABS(l.QtyOrdered)";
			
			PreparedStatement stmt = null;
			ResultSet rs = null;
			
			try {
				stmt = DB.prepareStatement(sql, get_TrxName());
				rs = stmt.executeQuery();
				while(rs.next())
				{
					m_process.statusUpdate("Sedang perapihan stok dibaris ke " + rs.getRow());
					MUNSPOSTrxLine line = new MUNSPOSTrxLine(getCtx(), rs.getInt(1), get_TrxName());
					MUNSPOSTrxLineMA[] mas = MUNSPOSTrxLineMA.gets(line.get_ID(), get_TrxName());

					for(MUNSPOSTrxLineMA ma : mas)
					{
						BigDecimal moveQty = Env.ZERO;
						List<MTransaction> trxs = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
								MTransaction.Table_Name, "UNS_POSTrxLine_ID = ? AND M_AttributeSetInstance_ID = ?",
									get_TrxName()).setParameters(line.get_ID(), ma.getM_AttributeSetInstance_ID()).list();
						boolean foundOriginalTrx = false;
						for(MTransaction trx : trxs)
						{
							if(!foundOriginalTrx && trx.getMovementQty().compareTo(ma.getMovementQty()) == 0)
							{
								foundOriginalTrx = true;
								continue;
							}
							moveQty = moveQty.add(trx.getMovementQty().negate());
							trx.deleteEx(true);
						}
						
						if (!MStorageOnHand.add(
								getCtx(), ma.getM_Locator().getM_Warehouse_ID(), ma.getM_Locator_ID(), 
								line.getM_Product_ID(), ma.getM_AttributeSetInstance_ID(), 
								moveQty, ma.getDateMaterialPolicy(), 
								get_TrxName()))
							throw new AdempiereException(CLogger.retrieveErrorString("Error"));
					}
				}
			} catch (SQLException e) {
				throw new AdempiereException(e.getMessage());
			}
		}
		
//		m_process.statusUpdate("Sedang perapihan pos session.");
//		reCalcPOSSession();
//		if (serverType.equals(serverLoc))
//		{	
//			m_process.statusUpdate("Sedang perapihan pos recap line.");
//			reCalcPOSRecapLine();
//			m_process.statusUpdate("Sedang perapihan pos recap.");
//			reCalcPOSRecap();
//			m_process.statusUpdate("Sedang perapihan cash reconciliation.");
//			reCalcCashReconcil();
//			m_process.statusUpdate("Sedang perapihan edc reconciliation.");
//			reCalcEDCReconcil();
//		}
		
		return "Success";
	}
	
	@SuppressWarnings("unused")
	private void reCalcPOSSession()
	{
		String sql = "SELECT t.UNS_POS_Session_ID, SUM(CASE WHEN dt.DocBaseType = 'SPR' THEN 0 ELSE t.GrandTotal END),"
				  + " SUM(CASE WHEN dt.DocBaseType = 'SPR' THEN t.GrandTotal ELSE 0 END) * -1,"
				  + " SUM(CASE WHEN dt.DocBaseType = 'SPR' THEN 0 ELSE t.ServiceCharge END),"
				  + " SUM(CASE WHEN dt.DocBaseType = 'SPR' THEN t.ServiceCharge ELSE 0 END) * -1,"
				  + " SUM(CASE WHEN dt.DocBaseType = 'SPR' THEN 0 ELSE t.TaxAmt END),"
				  + " SUM(CASE WHEN dt.DocBaseType = 'SPR' THEN t.TaxAmt ELSE 0 END) * -1,"
				  + " SUM(t.DiscountAmt) + SUM(l.DiscountAmt) FROM UNS_POSTrx t"
				  + " INNER JOIN UNS_POSTrxLine l ON l.UNS_POSTrx_ID = t.UNS_POSTrx_ID"
				  + " INNER JOIN C_DocType dt ON dt.C_DocType_ID = t.C_DocType_ID"
				  + " WHERE t.DocStatus IN ('CO', 'CL') GROUP BY t.UNS_POS_Session_ID";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				m_process.statusUpdate("Perapihan pos session baris ke " + rs.getRow());
				MUNSPOSSession session = new MUNSPOSSession(getCtx(), rs.getInt(1), get_TrxName());
				session.setTotalServiceCharge(rs.getBigDecimal(4));
				session.setTotalDiscAmt(rs.getBigDecimal(8));
				session.setTotalTaxAmt(rs.getBigDecimal(6));
				session.setTotalRefundTaxAmt(rs.getBigDecimal(7));
				session.setTotalSalesB1(rs.getBigDecimal(2));
				session.setReturnAmt(rs.getBigDecimal(3));
				session.setTotalRefundSvcChgAmt(rs.getBigDecimal(5));
				session.saveEx();
			}
		} catch (Exception e) {
			throw new AdempiereException(CLogger.retrieveErrorString("Error"));
		}
	}
	
	@SuppressWarnings("unused")
	private void reCalcPOSRecapLine()
	{
		String sql = "SELECT prl.UNS_POSRecapLine_ID, SUM(l.QtyOrdered) AS Qty, SUM(l.LineNetAmt) AS RevenueAmt,"
				+ " SUM(l.LineAmt) AS SalesAmt, SUM(l.DiscountAmt) AS DiscountAmt, SUM(l.TaxAmt) AS TaxAmt,"
				+ " SUM(l.ServiceCharge) AS ServiceChargeAmt FROM UNS_POSRecapLine prl"
				+ " INNER JOIN UNS_POSTrxLine l ON l.UNS_POSRecapLine_ID = prl.UNS_POSRecapLine_ID"
				+ " GROUP BY prl.UNS_POSRecapLine_ID";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				MUNSPOSRecapLine line = new MUNSPOSRecapLine(getCtx(), rs.getInt(1), get_TrxName());
				line.setQty(rs.getBigDecimal(2));
				line.setRevenueAmt(rs.getBigDecimal(3));
				line.setSalesAmt(rs.getBigDecimal(4));
				line.setDiscountAmt(rs.getBigDecimal(5));
				line.setTaxAmt(rs.getBigDecimal(6));
				line.setServiceChargeAmt(rs.getBigDecimal(7));
				line.saveEx();
			}
		} catch (Exception e) {
			throw new AdempiereException(CLogger.retrieveErrorString("Error"));
		}
	}
	
	@SuppressWarnings("unused")
	private void reCalcCashReconcil()
	{
		String sql = "SELECT bp.C_BP_Group_ID, ss.DateAcct, sca.C_Currency_ID,"
				+ " SUM(sca.CashDepositAmt), SUM(sca.DepositAmtB1),"
				+ " SUM(sca.CashDepositAmtB2), SUM(sca.PayableRefundAmt), SUM(sca.ShortCashierAmt),"
				+ " SUM(sca.EstimationShortCashierAmt) FROM UNS_SessionCashAccount sca"
				+ " INNER JOIN UNS_POS_Session ss ON ss.UNS_POS_Session_ID = sca.UNS_POS_Session_ID"
				+ " INNER JOIN UNS_POSTerminal pt ON pt.UNS_POSTerminal_ID = ss.UNS_POSTerminal_ID"
				+ " INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pt.Store_ID"
				+ " WHERE ss.DocStatus IN ('CO', 'CL')"
				+ " GROUP BY bp.C_BP_Group_ID, ss.DateAcct, sca.C_Currency_ID";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				MUNSSalesReconciliation recon = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
						MUNSSalesReconciliation.Table_Name, "DateTrx = ? AND C_BP_Group_ID = ?", get_TrxName())
							.setParameters(rs.getTimestamp(2), rs.getInt(1)).firstOnly();
				MUNSCashReconciliation cash = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
						MUNSCashReconciliation.Table_Name, "UNS_SalesReconciliation_ID = ? AND C_Currency_ID = ?",
						get_TrxName()).setParameters(recon.get_ID(), rs.getInt(3)).firstOnly();
				cash.setCashDepositAmt(rs.getBigDecimal(4));
				cash.setCashDepositAmtB1(rs.getBigDecimal(5));
				cash.setCashDepositAmtB2(rs.getBigDecimal(6));
				cash.setPayableRefundAmt(rs.getBigDecimal(7));
				cash.setShortCashierAmt(rs.getBigDecimal(8));
				cash.setEstimationShortCashierAmt(rs.getBigDecimal(9));
				cash.saveEx();
			}
		} catch (Exception e) {
			throw new AdempiereException(CLogger.retrieveErrorString("Error"));
		}
	}
	
	@SuppressWarnings("unused")
	private void reCalcEDCReconcil()
	{
		String sql = "SELECT bp.C_BP_Group_ID, ss.DateAcct, edc.UNS_CardType_ID,"
				+ " SUM(edc.TotalAmt), SUM(edc.PayableRefundAmt)"
				+ " FROM UNS_SessionEDCSummary edc "
				+ " INNER JOIN UNS_POS_Session ss ON ss.UNS_POS_Session_ID = edc.UNS_POS_Session_ID"
				+ " INNER JOIN UNS_POSTerminal pt ON pt.UNS_POSTerminal_ID = ss.UNS_POSTerminal_ID"
				+ " INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pt.Store_ID"
				+ " WHERE ss.DocStatus IN ('CO', 'CL')"
				+ " GROUP BY bp.C_BP_Group_ID, ss.DateAcct, edc.UNS_CardType_ID";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				MUNSSalesReconciliation recon = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
						MUNSSalesReconciliation.Table_Name, "DateTrx = ? AND C_BP_Group_ID = ?", get_TrxName())
							.setParameters(rs.getTimestamp(2), rs.getInt(1)).firstOnly();
				MUNSEDCReconciliation edc = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
						MUNSEDCReconciliation.Table_Name, "UNS_SalesReconciliation_ID = ? AND UNS_CardType_ID = ?",
							get_TrxName()).setParameters(recon.get_ID(), rs.getInt(3)).firstOnly();
				edc.setTotalAmt(rs.getBigDecimal(4));
				edc.setPayableRefundAmt(rs.getBigDecimal(5));
				edc.saveEx();
			}
		} catch (Exception e) {
			throw new AdempiereException(CLogger.retrieveErrorString("Error"));
		}
	}
	
	@SuppressWarnings("unused")
	private void reCalcPOSRecap()
	{
		String sql = "SELECT bp.C_BP_Group_ID, ss.DateAcct, pt.Store_ID,"
				+ " SUM(ss.TotalSalesB1), SUM(ss.TotalDiscAmt), SUM(ss.TotalServiceCharge),"
				+ " SUM(CASE WHEN ss.TotalShortCashierAmt < 0 THEN ss.TotalShortCashierAmt ELSE 0 END),"
				+ " SUM(CASE WHEN ss.TotalShortCashierAmt > 0 THEN ss.TotalShortCashierAmt ELSE 0 END),"
				+ " SUM(ss.TotalEstimationShortCashierAmt), SUM(ss.TotalTaxAmt), SUM(ss.VoucherAmt), SUM(ss.ReturnAmt)"
				+ " FROM UNS_POS_Session ss"
				+ " INNER JOIN UNS_POSTerminal pt ON pt.UNS_POSTerminal_ID = ss.UNS_POSTerminal_ID"
				+ " INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = pt.Store_ID"
				+ " WHERE ss.DocStatus IN ('CO', 'CL')"
				+ " GROUP BY bp.C_BP_Group_ID, ss.DateAcct, pt.Store_ID";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				MUNSSalesReconciliation recon = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
						MUNSSalesReconciliation.Table_Name, "DateTrx = ? AND C_BP_Group_ID = ?", get_TrxName())
							.setParameters(rs.getTimestamp(2), rs.getInt(1)).firstOnly();
				MUNSPOSRecap recap = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPOSRecap.Table_Name,
						"UNS_SalesReconciliation_ID = ? AND Store_ID = ?", get_TrxName())
							.setParameters(recon.get_ID(), rs.getInt(3)).firstOnly();
				recap.setTotalSalesAmt(rs.getBigDecimal(4));
				recap.setTotalDiscAmt(rs.getBigDecimal(5));
				recap.setTotalServiceChargeAmt(rs.getBigDecimal(6));
				recap.setTotalShortCashierAmt(rs.getBigDecimal(7));
				recap.setTotalShortCashierAmt1(rs.getBigDecimal(8));
				recap.setTotalEstimationShortCashierAmt(rs.getBigDecimal(9));
				recap.setTotalTaxAmt(rs.getBigDecimal(10));
				recap.setTotalVoucherAmt(rs.getBigDecimal(11));
				recap.setReturnAmt(rs.getBigDecimal(12));
				recap.saveEx();
			}
		} catch (Exception e) {
			throw new AdempiereException(CLogger.retrieveErrorString("Error"));
		}
	}
}