package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.util.UNSApps;

public class GenerateCreditInvoice extends SvrProcess {

	public GenerateCreditInvoice() 
	{
		super ();
	}

	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			ProcessInfoParameter param = params[i];
			String name = param.getParameterName();
			if ("AD_Org_ID".equals(name))
				this.p_AD_Org_ID = param.getParameterAsInt();
			else if ("DateInvoiced".equals(name))
				this.p_DateInvoiced = param.getParameterAsTimestamp();
			else if ("DateFrom".equals(name))
				this.p_DateFrom = param.getParameterAsTimestamp();
			else if ("DateTo".equals(name))
				this.p_DateTo = param.getParameterAsTimestamp();
			else if (("C_BPartner_ID").equals(name))
				this.p_C_BPartner_ID = param.getParameterAsInt();
			else
				log.log(Level.WARNING, "Unknown parameter " + name);
		}
		
		m_processUI = Env.getProcessUI(getCtx());
	}
	
	private int p_AD_Org_ID = 0;
	private Timestamp p_DateInvoiced = null;
	private Timestamp p_DateFrom = null;
	private Timestamp p_DateTo = null;
	private IProcessUI m_processUI = null;
	private int p_C_BPartner_ID = 0;

	@Override
	protected String doIt() throws Exception 
	{
		//Load all not invoiced pos payment or invoiced pos payment but the invoice has been reverse.
		StringBuilder builder = new StringBuilder(" SELECT pp.UNS_POSPayment_ID, pp.DocumentNo, ")
		.append(" pp.PaidAmt, dt.DocBaseType, bp.value, bp.Name, pp.C_BPartner_ID, ")
		.append (" pp.C_BPartner_Location_ID, pp.AD_Org_ID FROM UNS_POSPayment pp INNER JOIN ")
		.append(" C_BPartner bp ON bp.C_BPartner_ID = pp.C_BPartner_ID INNER JOIN ")
		.append(" C_DocType dt ON dt.C_DocType_ID = pp.C_DocType_ID WHERE pp.IsActive = 'Y' ")
		.append(" AND pp.DocStatus IN ('CO', 'CL')").append(" AND (")
		.append("pp.C_Invoice_ID IS NULL OR EXISTS (SELECT * FROM C_Invoice ")
		.append(" WHERE C_Invoice_ID = pp.C_Invoice_ID AND DocStatus IN ('RE','VO') ")
		.append(")) AND PaymentMethod IN ('1') ");
		
		if (this.p_AD_Org_ID > 0)
			builder.append(" AND pp.AD_Org_ID = ").append(this.p_AD_Org_ID);
		if (this.p_DateFrom != null && this.p_DateTo != null)
			builder.append(" AND pp.DateTrx BETWEEN '").append(this.p_DateFrom)
			.append("' AND '").append(this.p_DateTo).append("' ");
		else if (this.p_DateFrom != null)
			builder.append(" AND pp.DateTrx >= '").append(this.p_DateFrom).append("' ");
		else if (this.p_DateTo != null)
			builder.append(" AND pp.DateTrx <= '").append(this.p_DateTo).append("' ");
		
		if (this.p_C_BPartner_ID > 0)
			builder.append(" AND pp.C_BPartner_ID = ").append(this.p_C_BPartner_ID);
		PreparedStatement st = null;
		ResultSet rs = null;
		Hashtable<Integer, MInvoice> mapInvoice= new Hashtable<>();
		String sql = builder.toString();
		String errorMsg = null;
		int C_Charge_ID = UNSApps.getRefAsInt(UNSApps.CHRG_PtgBlmDtgh);
		int C_Tax_ID = DB.getSQLValue(get_TrxName(), "SELECT C_Tax_ID FROM C_Tax WHERE Name = ?",
				"Standard");

		String sqlUpdate = "UPDATE UNS_POSPayment SET C_Invoice_ID = ?, C_InvoiceLine_ID = ? "
				+ " WHERE UNS_POSPayment_ID = ?";
		
		if (C_Tax_ID <= 0)
		{
			throw new AdempiereUserError("Could not find tax with name Standard");
		}
		
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			rs = st.executeQuery();
			while (rs.next())
			{
				int counter = 1;
				int posPay_ID = rs.getInt(counter++);
				String posPayDocno = rs.getString(counter++);
				BigDecimal amount = rs.getBigDecimal(counter++);
				String docbasetype = rs.getString(counter++);
				String customerValue = rs.getString(counter++);
				String customerName = rs.getString(counter++);
				int customer_ID = rs.getInt(counter++);
				int customerLoc_ID = rs.getInt(counter++);
				int AD_Org_ID = rs.getInt(counter++);
				
				String msgInv = "Generating Invoice " + customerValue + "["
						+ customerName + "]";
				m_processUI.statusUpdate(msgInv);
				
				if (MDocType.DOCBASETYPE_ARPOSCreditMemo.equals(docbasetype))
					amount = amount.negate();
				
				MInvoice invoice = mapInvoice.get(customer_ID);
				if (null == invoice)
				{
					invoice = createInvoice(AD_Org_ID, customer_ID, customerLoc_ID, 
							this.p_DateInvoiced);
					if (null == invoice)
					{
						errorMsg = "Could not create invoice of Credit " + posPayDocno + " - "
								+ customerValue + " [" + customerName + "] || ";
						errorMsg += CLogger.retrieveErrorString("Unknown Error");
						break;
					}
					
					mapInvoice.put(customer_ID, invoice);
				}
				
				MInvoiceLine line = createInvoiceLine(invoice, amount, C_Charge_ID, C_Tax_ID);
				if (null == line)
				{
					errorMsg = "Could not create invoice Line of Credit " + posPayDocno + " - "
							+ customerValue + " [" + customerName + "] || ";
					errorMsg += CLogger.retrieveErrorString("Unknown Error");
					break;
				}
				
				int updateOK = DB.executeUpdate(sqlUpdate, new Object[]{invoice.get_ID(), 
						line.get_ID(), posPay_ID}, false, get_TrxName());
				if (updateOK == -1)
				{
					errorMsg = "Could not update POS Payment";
					errorMsg += CLogger.retrieveErrorString("Unknown Error");
					break;
				}
			}
		}
		catch (SQLException ex)
		{
			errorMsg = ex.getMessage();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		if (null != errorMsg)
			throw new AdempiereException(errorMsg);
		
		for (MInvoice inv : mapInvoice.values())
		{
			if (!inv.processIt(DocAction.STATUS_Completed))
			{
				throw new AdempiereException(inv.getProcessMsg());
			}

			inv.saveEx();
		}
		
		return null;
	}
	
	private MInvoice createInvoice (int AD_Org_ID, int C_BPartner_ID, int C_BPartner_Location_ID, 
			Timestamp dateInvoiced)
	{
		MInvoice invoice = new MInvoice(getCtx(), 0, get_TrxName());
		invoice.setAD_Org_ID(AD_Org_ID);
		invoice.setC_BPartner_ID(C_BPartner_ID);
		invoice.setC_BPartner_Location_ID(C_BPartner_Location_ID);
		invoice.setC_Currency_ID(303); //TODO
		invoice.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_ARInvoice));
		invoice.setIsSOTrx(true);
		invoice.setC_DocTypeTarget_ID();
		invoice.setDateAcct(dateInvoiced);
		invoice.setDateInvoiced(dateInvoiced);
		invoice.setDateOrdered(dateInvoiced);
		invoice.setDescription("::Kredit Konsumsi::");
		
		String sql = "SELECT M_PriceList_ID FROM M_PriceList WHERE IsSOPriceList = 'Y'";
		int priceList_ID = DB.getSQLValue(get_TrxName(), sql);
		invoice.setM_PriceList_ID(priceList_ID);
		
		invoice.setPaymentRule(MInvoice.PAYMENTRULE_OnCredit);
		invoice.setSalesRep_ID(Env.getAD_User_ID(getCtx()));
		
		if (!invoice.save())
			return null;
		
		return invoice;
	}
	
	private MInvoiceLine createInvoiceLine (MInvoice invoice, BigDecimal amt, int C_Charge_ID, 
			int C_Tax_ID)
	{
		MInvoiceLine line = new MInvoiceLine(invoice);
		line.setQty(Env.ONE);
		line.setPriceEntered(amt); 
		line.setPriceActual(amt);
		line.setPriceList(amt);		
		line.setC_Tax_ID(C_Tax_ID);
		line.setC_Charge_ID(C_Charge_ID);
		
		if (!line.save())
			return null;
		
		return line;
	}
}
