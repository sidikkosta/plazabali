/**
 * 
 */
package com.unicore.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.base.model.MInOut;
import com.unicore.model.MUNSCreditAgreement;

/**
 * @author Menjangan
 *
 */
public class CreditAgreementCreateShipment extends SvrProcess {

	/**
	 * 
	 */
	public CreditAgreementCreateShipment() 
	{
		super ();
	}
	
	private int m_M_Locator_ID = 0;
	private Timestamp m_MovementDate = null;
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("M_Locator_ID".equals(params[i].getParameterName()))
				m_M_Locator_ID = params[i].getParameterAsInt();
			else if ("MovementDate".equals(params[i].getParameterName()))
				m_MovementDate = params[i].getParameterAsTimestamp();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		MUNSCreditAgreement creditAgreement = new MUNSCreditAgreement(
				getCtx(), getRecord_ID(), get_TrxName());
		
		if (creditAgreement.getM_InOut_ID() > 0)
		{
			String sql = "SELECT DocStatus FROM M_InOut WHERE M_InOut_ID = ?";
			String status = DB.getSQLValueString(get_TrxName(), sql, 
					creditAgreement.getM_InOut_ID());
			if ("CO".equals(status) || "CL".equals(status))
				return "Shipment already created before.";
			
			if ("DR".equals(status) || "IP".equals(status) || "IN".equals(status))
			{
				// try to complete existing shipment.
				MInOut inOut = new MInOut(getCtx(), creditAgreement.getM_InOut_ID(), get_TrxName());
				boolean ok = inOut.processIt(DocAction.ACTION_Complete);
				if (!ok)
					throw new AdempiereException(inOut.getProcessMsg());
				
				inOut.saveEx();
				return "success";
			}
			
			creditAgreement.setM_InOut_ID(-1);
		}
		
		if (!creditAgreement.createShipment(this.m_M_Locator_ID, this.m_MovementDate))
			throw new AdempiereException(creditAgreement.getProcessMsg());
		
		return "Success";
	}

}
