package com.unicore.model.process;

import java.util.logging.Level;

import org.compiere.model.MRequisitionLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

public class CreateReasonToClose extends SvrProcess {

	private String p_Reason = null;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for(int i=0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if(name.equals("Reason"))
				p_Reason = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: "+name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		
		MRequisitionLine line = new MRequisitionLine(getCtx(), getRecord_ID(), get_TrxName());

		line.setCloseReason(p_Reason);
		if(p_Reason == null)
			line.setCreateReason("N");
		else
			line.setCreateReason("Y");
		
		line.saveEx();
		
		return null;
	}

}
