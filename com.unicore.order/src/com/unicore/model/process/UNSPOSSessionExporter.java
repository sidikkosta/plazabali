/**
 * 
 */
package com.unicore.model.process;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Level;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.process.rpl.exp.ExportHelper;
import org.compiere.model.MEXPFormat;
import org.compiere.model.MReplicationStrategy;
import org.compiere.model.ModelValidator;
import org.compiere.model.X_AD_ReplicationTable;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.w3c.dom.Document;

/**
 * @author nurse
 *
 */
public class UNSPOSSessionExporter extends SvrProcess {

	private int p_expFormatID;
	private String p_fileName;
	
	/**
	 * 
	 */
	public UNSPOSSessionExporter() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("EXP_Format_ID".equals(params[i].getParameterName()))
				p_expFormatID = params[i].getParameterAsInt();
			else if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else 
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (!p_fileName.endsWith(".xml"))
			p_fileName += ".xml";
		String where = "UNS_POS_Session_ID = " + getRecord_ID();
		String sql = "SELECT Docstatus FROM UNS_POS_Session WHERE UNS_POS_Session_ID = ?";
		String status = DB.getSQLValueString(get_TrxName(), sql, getRecord_ID());
		if (!DocAction.STATUS_Completed.equals(status))
			throw new AdempiereUserError("Please complete document first");
		sql = "UPDATE UNS_POS_Session SET DocStatus = 'IP', DocAction = 'CO' WHERE UNS_POS_Session_ID = ?";
		int exec = DB.executeUpdate(sql, getRecord_ID(), false, get_TrxName());
		if (exec == -1)
			throw new AdempiereException("Could not update POS Session");
		
		ExportHelper helper = new ExportHelper(getCtx(), getAD_Client_ID());
		MEXPFormat expFormat = new MEXPFormat(getCtx(), p_expFormatID, get_TrxName());
		File file = new File(p_fileName);
		Document doc = helper.exportRecord(expFormat, where, MReplicationStrategy.REPLICATION_DOCUMENT,
				X_AD_ReplicationTable.REPLICATIONTYPE_Merge,ModelValidator.TYPE_AFTER_CHANGE);
        TransformerFactory tranFactory = TransformerFactory.newInstance();
        
        tranFactory.setAttribute("indent-number", 4);
        
        Transformer aTransformer = tranFactory.newTransformer();
        aTransformer.setOutputProperty(OutputKeys.METHOD, "xml");
        aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source src = new DOMSource(doc);
		
        Writer writer = new StringWriter();
        Result dest2 = new StreamResult(writer);
        aTransformer.transform(src, dest2);
        
        try 
        {
        	Result dest = new StreamResult(file);
            aTransformer.transform(src, dest);
            writer.flush();
            writer.close();
        } 
        catch (TransformerException ex) 
        {
        	throw ex;
        }
        
        sql = "UPDATE UNS_POS_Session SET DocStatus = 'CO', DocAction = 'CL' WHERE UNS_POS_Session_ID = ?";
		exec = DB.executeUpdate(sql, getRecord_ID(), false, get_TrxName());
		if (exec == -1)
			throw new AdempiereException("Could not update POS Session");
		
		return "Export Success";
	}

}
