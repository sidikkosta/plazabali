/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.compiere.model.MProcess;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

/**
 * @author Burhani Adam
 *
 */
public class UNSPrintTicketAndGrading extends SvrProcess {

	private boolean m_DirectPrint;
	/**
	 * 
	 */
	public UNSPrintTicketAndGrading() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("IsDirectPrint"))
				m_DirectPrint = param.getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		MProcess pro = new MProcess(getCtx(), getProcessInfo().getAD_Process_ID(), get_TrxName());
		pro.setIsDirectPrint(m_DirectPrint);
		pro.saveEx();
		return null;
	}
}
