/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.FillMandatoryException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Util;

import com.unicore.model.MUNSPackingList;
import com.unicore.model.MUNSPackingListLine;
import com.unicore.model.MUNSPackingListOrder;
//import com.uns.base.model.Query;

/**
 * @author AzHaidar
 *
 */
public class VoidPackingList extends SvrProcess 
{

	private String m_Reason = null;
	private MUNSPackingList oldPL = null;
	private boolean p_CreateReplacement = false;
	
	/**
	 * 
	 */
	public VoidPackingList() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
        ProcessInfoParameter[] para = getParameter();

        for (int i = 0; i < para.length; i++)
        {
            String name = para[i].getParameterName();
            if (name.equals("Reason"))
                m_Reason = para[i].getParameterAsString();
            else if (name.equals("CreateReplacement"))
            	p_CreateReplacement = para[i].getParameterAsBoolean();
            else
            	log.log(Level.SEVERE, "Unknown Parameter: " + name);
        }
        
        oldPL = new MUNSPackingList(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (m_Reason == null || Util.isEmpty(m_Reason, true))
			throw new FillMandatoryException("You have to supply some reason to void a Packing List document.");
		
		if (oldPL == null || oldPL.get_ID() == 0)
			throw new AdempiereException("Cannot void a null document.");
		
		oldPL.addDescription(m_Reason);
		
		if (!oldPL.processIt(MUNSPackingList.ACTION_Void))
//			throw new AdempiereException("Void process is failed.");
			throw new AdempiereException(oldPL.getProcessMsg()); 		
//			m_PL.setDocumentNo(m_PL.getDocumentNo() + "_VO");	
		
//		try
//		{	
//			boolean ok = !m_PL.processIt(MUNSPackingList.ACTION_Void);
//			if (!ok)
//			{
//				m_processMsg = m_PL.getProcessMsg();
//				return m_processMsg;
//			}
//			m_PL.saveEx();
//		}
//		catch (Exception ex)
//		{
//			m_processMsg = ex.getMessage();
//			return m_processMsg;
//		}
		
		try
		{
			oldPL.saveEx();
			String msg = null;
			if (p_CreateReplacement)
			{
				MUNSPackingList replacement = createNewPL();
				
				msg = "The replacement has been created. " + replacement .getDocumentNo() ;
			} 

			else
			{
				msg = "Success.";
			}
			
			return msg;
		}
		catch (Exception ex)
		{
			throw new AdempiereException(ex.getMessage());
		}
	}
	
		//public static MUNSPackingList createNewPL (MUNSPackingList list)
		//{
			//MUNSPackingList pl = new MUNSPackingList(getCtx, getUNS_PackingList_ID, get_TrxName());
			
		
		public MUNSPackingList createNewPL ()
		{
		//create New PL
			MUNSPackingList newPL = new MUNSPackingList(oldPL.getCtx(),0, oldPL.get_TrxName());
			newPL.setAD_Org_ID(oldPL.getAD_Org_ID());
//			list.setDocumentNo(m_PL.getDocumentNo());
			newPL.setName(oldPL.getName());
			newPL.setAD_Org_ID(oldPL.getAD_Org_ID());
			newPL.setVolume(oldPL.getVolume());
			newPL.setDateDoc(oldPL.getDateDoc());	
			newPL.setReferenceNo(oldPL.getReferenceNo());
			newPL.setTonase(oldPL.getTonase());
			newPL.setHelp(oldPL.getHelp());
			newPL.setConsolidateDocument(oldPL.isConsolidateDocument());
			newPL.setConsolidateConfirmation(oldPL.isConsolidateConfirmation());
			newPL.setIsSOTrx(oldPL.isSOTrx());
			newPL.setReplacement_ID(oldPL.getUNS_PackingList_ID());
			newPL.setIsAutoCompleteAfterConfirm(oldPL.isAutoCompleteAfterConfirm());
			newPL.setIsCashOrder(oldPL.isCashOrder());
			newPL.setIsPickup(oldPL.isPickup());
			newPL.setIsApproved(oldPL.isApproved());
			try
			
			{
				newPL.saveEx();
				oldPL.setDocumentNo(oldPL.getDocumentNo() + "_VO");	
				oldPL.setReplacement_ID(newPL.getUNS_PackingList_ID());
				oldPL.saveEx();
				MUNSPackingListOrder[] orders = oldPL.getOrders();
				for (int i=0; i<orders.length; i++)
				{
					NewpOrder(newPL,orders[i]);
				}
			}
			catch (Exception ex)
			{
				throw new AdempiereException(ex.getMessage());
			}
			return newPL;
		}
		public  void NewpOrder (MUNSPackingList newPL, MUNSPackingListOrder oldPLOrder)
			{	
			MUNSPackingListOrder newPLOrder = new MUNSPackingListOrder(oldPLOrder.getCtx(), 0, oldPLOrder.get_TrxName());
			newPLOrder.setUNS_PackingList_ID(newPL.getUNS_PackingList_ID());
			newPLOrder.setUNS_Rayon_ID(oldPLOrder.getUNS_Rayon_ID());
			if (oldPLOrder.getC_Order_ID() == 0)
			{
				newPLOrder.setC_Invoice_ID(oldPLOrder.getC_Invoice_ID());
			}
			
			newPLOrder.setC_Order_ID(oldPLOrder.getC_Order_ID());
			newPLOrder.setAD_Org_ID(oldPLOrder.getAD_Org_ID());
			newPLOrder.setDescription(oldPLOrder.getDescription());
			newPLOrder.setReplaceOrder_ID(oldPLOrder.getUNS_PackingList_Order_ID());
			newPLOrder.setReferenceNo(oldPLOrder.getReferenceNo());
			newPLOrder.setSalesRep_ID(oldPLOrder.getSalesRep_ID());
			newPLOrder.setM_Warehouse_ID(oldPLOrder.getM_Warehouse_ID());
			newPLOrder.setStatus(oldPLOrder.getStatus());
			newPLOrder.setVolume(oldPLOrder.getVolume());
			newPLOrder.setTonase(oldPLOrder.getTonase());
			
			try
			{
				newPLOrder.saveEx();
				oldPLOrder.setReplaceOrder_ID(newPLOrder.getUNS_PackingList_Order_ID());
				oldPLOrder.saveEx();
				MUNSPackingListLine[] lines = oldPLOrder.getLines();
				for (int i=0; i<lines.length; i++)
				{
					newPLLine(lines[i], newPLOrder);
				}
			}
			catch (Exception ex)
			{
				throw new AdempiereException(ex.getMessage());
			}
			
			}	
		public void newPLLine (MUNSPackingListLine oldPLLine, MUNSPackingListOrder oldPLOrder)
		{
		//create PLLine
			MUNSPackingListLine newPLLine = new MUNSPackingListLine(oldPLLine.getCtx(), 0, oldPLLine.get_TrxName());
			newPLLine.setUNS_PackingList_Order_ID(oldPLOrder.getUNS_PackingList_Order_ID());
			newPLLine.setM_Product_ID(oldPLLine.getM_Product_ID());
			newPLLine.setM_Locator_ID(oldPLLine.getM_Locator_ID());
			newPLLine.setReplaceLine_ID(oldPLLine.getUNS_PackingList_Line_ID());
			newPLLine.setAD_Org_ID(oldPLLine.getAD_Org_ID());
			newPLLine.setC_OrderLine_ID(oldPLLine.getC_OrderLine_ID());
			
			if(oldPLLine.isProductAccessories())
			{
				newPLLine.setisProductAccessories(true);
				newPLLine.setPackingList_Line_ID(
						new MUNSPackingListLine(getCtx(), oldPLLine.getPackingList_Line_ID(), get_TrxName()).getReplaceLine_ID());
			}
			
			if (oldPLLine.getC_OrderLine_ID() == 0)
			{
				newPLLine.setC_InvoiceLine_ID(oldPLLine.getC_InvoiceLine_ID());	
			}
			
			newPLLine.setC_UOM_ID(oldPLLine.getC_UOM_ID());
			newPLLine.setDescription(oldPLLine.getDescription());
			newPLLine.setQty(oldPLLine.getMovementQty());
			try
			{
				newPLLine.saveEx();
				oldPLLine.setReplaceLine_ID(newPLLine.getUNS_PackingList_Line_ID());
				oldPLLine.saveEx();
			}
			catch (Exception ex)
			{
				throw new AdempiereException(ex.getMessage());
			}		
			
		}
		
}