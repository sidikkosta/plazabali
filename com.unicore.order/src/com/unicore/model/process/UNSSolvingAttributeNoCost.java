/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Hashtable;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCost;
import org.compiere.model.MDocType;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSAttributeNoCost;
import com.unicore.model.MUNSPOSRecap;
import com.unicore.model.MUNSPOSRecapLine;
import com.unicore.model.MUNSPOSRecapLineMA;
import com.unicore.model.MUNSSalesReconciliation;
import com.uns.base.model.MInventory;

/**
 * @author Burhani Adam
 *
 */
public class UNSSolvingAttributeNoCost extends SvrProcess
{
	private boolean p_ProcessNow = false;
	private boolean p_Generate = false;
	private boolean p_DeleteOld = false;
	/**
	 * 
	 */
	public UNSSolvingAttributeNoCost() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("ProcessNow"))
				p_ProcessNow = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("Generate"))
				p_Generate = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("DeleteOld"))
				p_DeleteOld = param.getParameterAsBoolean();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSSalesReconciliation parent = new MUNSSalesReconciliation(getCtx(), getRecord_ID(), get_TrxName());
		MUNSAttributeNoCost[] lines = null;
		
		if(p_ProcessNow)
		{
			lines = parent.getNoCost("Processed = 'N' AND M_AttributeSetInstanceTo_ID > 0"
					+ " AND MovementQty > 0");
			for(int i=0;i<lines.length;i++)
			{
				if(lines[i].isNoCost())
				{
					MInventory inventory = createInventoryCostAdjusment(parent, lines[i].getM_Locator().getM_Warehouse_ID());
					if(!lines[i].createCostAdjustment(inventory))
					{
						throw new AdempiereException(CLogger.retrieveErrorString("Failed when trying process line."));
					}
				}
				
				if(lines[i].getMovementQty().signum() != 0)
				{
					MInventory inventory = createInventoryStockAdjusment(parent, lines[i].getM_Locator_ID());
					if(!lines[i].createStockAdjusment(inventory))
					{
						throw new AdempiereException(CLogger.retrieveErrorString("Failed when trying process line."));
					}
				}
			}
			completingDoc();
		}
		else if(p_Generate)
		{
			if(p_DeleteOld)
			{
				String sql = "DELETE FROM UNS_AttributeNoCost WHERE UNS_SalesReconciliation_ID = ?"
						+ " AND Processed = 'N'";
				DB.executeUpdate(sql, parent.get_ID(), get_TrxName());
			}
			create(parent);
		}
		return "Sucess";
	}
	
	private void create(MUNSSalesReconciliation sr)
	{
		MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(getCtx(), sr.getAD_Client_ID());
		MUNSPOSRecap[] recaps = sr.getRecap();
		for(int x=0;x<recaps.length;x++)
		{
			MUNSPOSRecapLine[] lines = recaps[x].getLines();
			for(int y=0;y<lines.length;y++)
			{
				if(lines[y].isRefund() || lines[y].isConsignment())
					continue;
				MUNSPOSRecapLineMA[] mas = lines[y].getMAs();
				for(int z=0;z<mas.length;z++)
				{
					BigDecimal cost = null;
					for(MAcctSchema as : ass)
					{
						if(as.getC_Currency_ID() != 303)
							continue;
						cost = MCost.getCurrentCost(MProduct.get(getCtx(), lines[y].getM_Product_ID()),
								mas[z].getM_AttributeSetInstance_ID(), as, sr.getAD_Org_ID(), null, mas[z].getQty(),
								0, false, get_TrxName());
						if(cost == null)
						{
							MUNSAttributeNoCost line = MUNSAttributeNoCost.getCreate(sr, mas[z]);
							if(null == line)
								throw new AdempiereException("Failed when trying create record.");
							line.setNoCost(true);
							line.saveEx();
						}
						else
						{
							String sql = "SELECT SUM(QtyOnHand) FROM M_StorageOnHand WHERE M_Product_ID = ?"
									+ " AND M_AttributeSetInstance_ID = ? AND DateMaterialPolicy = ?"
									+ " AND M_Locator_ID = ?";
							BigDecimal onHand = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
								lines[y].getM_Product_ID(), mas[z].getM_AttributeSetInstance_ID(),
								mas[z].getDateMaterialPolicy(), mas[z].getM_Locator_ID()
							});
							if(onHand == null)
								onHand = Env.ZERO;
							if(onHand.signum() == -1)
							{
								MUNSAttributeNoCost line = MUNSAttributeNoCost.getCreate(sr, mas[z]);
								if(null == line)
									throw new AdempiereException("Failed when trying create record.");
								line.setNoCost(false);
								line.setQtyOnHand(onHand);
								line.saveEx();
							}
						}
						break;
					}
				}
			}
		}
	}
	
	public MInventory createInventoryCostAdjusment(MUNSSalesReconciliation sr, int M_Warehouse_ID)
	{
		MInventory inventory = CostAdjustment.get(M_Warehouse_ID);
		if(inventory != null)
			return inventory;
		inventory = new MInventory(getCtx(), 0, get_TrxName());
		inventory.setAD_Org_ID(sr.getAD_Org_ID());
		MDocType[] dt = MDocType.getOfDocBaseType(getCtx(), MDocType.DOCBASETYPE_MaterialPhysicalInventoryImport);
		inventory.setM_Warehouse_ID(M_Warehouse_ID);
		inventory.setC_DocType_ID(dt[0].get_ID());
		inventory.setMovementDate(sr.getDateTrx());
		inventory.setDateAcct(sr.getDateTrx());
		inventory.saveEx();
		CostAdjustment.put(M_Warehouse_ID, inventory);
		return inventory;
	}
	
	public MInventory createInventoryStockAdjusment(MUNSSalesReconciliation sr, int M_Locator_ID)
	{
		MInventory inventory = StockAdjustment.get(M_Locator_ID);
		if(inventory != null)
			return inventory;
		inventory = new MInventory(getCtx(), 0, get_TrxName());
		inventory.setAD_Org_ID(sr.getAD_Org_ID());
		MDocType[] dt = MDocType.getOfDocSubTypeInv(getCtx(), MDocType.DOCSUBTYPEINV_PhysicalInventory);
		inventory.setC_DocType_ID(dt[0].get_ID());
		inventory.setM_Warehouse_ID(MLocator.get(getCtx(), M_Locator_ID).getM_Warehouse_ID());
		inventory.setMovementDate(sr.getDateTrx());
		inventory.setDateAcct(sr.getDateTrx());
		inventory.saveEx();
		StockAdjustment.put(M_Locator_ID, inventory);
		return inventory;
	}
	
	private void completingDoc()
	{
		if(CostAdjustment.size() > 0)
		{
			Enumeration<Integer> key = CostAdjustment.keys();
			while(key.hasMoreElements())
			{
				Integer M_Warehouse_ID = key.nextElement();
				MInventory inventory = CostAdjustment.get(M_Warehouse_ID);
				try {
					inventory.processIt("CO");
					inventory.saveEx();
				} catch (Exception e) {
					throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
				}
			}
		}
		
		if(StockAdjustment.size() > 0)
		{
			Enumeration<Integer> key = StockAdjustment.keys();
			while(key.hasMoreElements())
			{
				int M_Locator_ID = key.nextElement();
				MInventory inventory = StockAdjustment.get(M_Locator_ID);
				try {
					inventory.processIt("CO");
					inventory.saveEx();
				} catch (Exception e) {
					throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
				}
			}
		}
	}
	
	private Hashtable<Integer, MInventory> CostAdjustment = new Hashtable<>();
	private Hashtable<Integer, MInventory> StockAdjustment = new Hashtable<>();
}