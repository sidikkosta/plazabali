package com.unicore.model.process;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import org.compiere.cm.StringUtil;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.print.PrintUtil;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Ini;
import org.compiere.util.Util;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPaymentTrx;

/**
 * 
 */

/**
 * @author nurse
 *
 */
public class PrintReading extends SvrProcess {

	/**
	 * 
	 */
	public PrintReading() 
	{
		super ();
	}
	
	private  byte[] m_byteDrawerCode = new byte[]{27,112,48,25,-6};
	private String P_drawerCode = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++) 
		{
			if ("DrawerCode".equals(params[i].getParameterName()))
				P_drawerCode = params[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (!Util.isEmpty(P_drawerCode, true))
		{
			String[] tobeBytes = P_drawerCode.split(",");
			m_byteDrawerCode = new byte[tobeBytes.length];
			for (int i=0; i<tobeBytes.length; i++)
			{
				m_byteDrawerCode[i] = Byte.valueOf(tobeBytes[i]);
			}
		}
		
		String drawerCode = new String(m_byteDrawerCode);
		
		MUNSPOSSession session = new MUNSPOSSession(getCtx(), getRecord_ID(), get_TrxName());
		String sql = "SELECT shop.Description FROM C_BPartner shop INNER JOIN UNS_POSTerminal "
				+ " trm ON trm.Store_ID=shop.C_BPartner_ID WHERE trm.UNS_POSTerminal_ID=?";
		String shopName = DB.getSQLValueString(get_TrxName(), sql, session.getUNS_POSTerminal_ID());
		shopName = formatText(shopName, MAX_CHAR_LENGTH, null, POSITION.CENTER);
		String clientName = MClient.get(getCtx(), session.getAD_Client_ID()).getName();
		clientName = formatText(clientName, MAX_CHAR_LENGTH, null, POSITION.CENTER);
		String eoss = formatText("END OF SHIFT SUMMARY", MAX_CHAR_LENGTH, null, POSITION.CENTER);
		String documentNo = formatText(session.getDocumentNo(), MAX_CHAR_LENGTH, null, POSITION.LEFT);
		String shift = "Shift";
		shift = formatText(shift, 15, null, POSITION.LEFT);
		if (session.getShift() == null)
			shift += "-";
		else
			shift += session.getShift();
		shift = formatText(shift, MAX_CHAR_LENGTH, null, POSITION.LEFT);
		String till = "Till";
		till = formatText(till, 15, null, POSITION.LEFT);
		sql = "SELECT Value FROM UNS_POSTerminal WHERE UNS_POSTerminal_ID = ?";
		String storeCode = DB.getSQLValueString(get_TrxName(), sql, session.getUNS_POSTerminal_ID());
		till += storeCode;
		till = formatText(till, MAX_CHAR_LENGTH, null, POSITION.LEFT);
		String cashierName = "Cashier";
		cashierName = formatText(cashierName, 15, null, POSITION.LEFT);
		sql = "SELECT COALESCE(RealName,Name) FROM AD_User WHERE AD_User_ID = ?";
		String cName = DB.getSQLValueString(get_TrxName(), sql, session.getCashier_ID());
		cashierName += cName;
		cashierName = formatText(cashierName, MAX_CHAR_LENGTH, null, POSITION.LEFT);
		Timestamp dateOpen = session.getDateOpened();
		Timestamp dateClose = session.getDateClosed();
		String openStr = "-";
		String closeStr = "-";
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
		if (dateOpen != null)
			openStr = df.format(dateOpen);
		if (dateClose != null)
			closeStr = df.format(dateClose);
		openStr = formatText(openStr, 17, null, POSITION.RIGHT);
		closeStr = formatText(closeStr, 17, null, POSITION.LEFT);
		String betweenSeparator = formatText("To", 6, null, POSITION.CENTER);
		String timeLive = openStr + betweenSeparator + closeStr;
		String doubleSeparator = formatText("=", MAX_CHAR_LENGTH, "=", POSITION.CENTER);
		String singleSeparator = formatText("-", MAX_CHAR_LENGTH, "-", POSITION.CENTER);
		String titleTandem = formatText("CASHIER TENDEM", MAX_CHAR_LENGTH, null, POSITION.CENTER);
		
		StringBuilder b = new StringBuilder(drawerCode);
		b.append(shopName).append("\n").append(clientName).append("\n\n").append(eoss)
		.append("\n").append(documentNo)
		.append("\n").append(shift).append("\n").append(till).append("\n").append(cashierName).append("\n")
		.append(timeLive).append("\n").append(doubleSeparator).append("\n");
		
		//CASHIER TENDEMS
		List<Hashtable<String, Object>> tandems = executeQuery(QUERY_CASHIER_TANDEM, session.get_ID());
		for (int i=0; i<tandems.size(); i++)
		{
			if (i==0)
			{
				b.append(singleSeparator).append("\n").append(titleTandem).append("\n")
				.append(singleSeparator).append("\n");
			}
			Hashtable<String, Object> row = tandems.get(i);
			String name = (String) row.get("name");
			name = formatText(name, MAX_CHAR_LENGTH, null, POSITION.LEFT);
			b.append(name).append("\n\n");
		}
		b.append(singleSeparator).append("\n");
		String rateTitle = formatText("Currency", 12, null, POSITION.CENTER);
		String rateTitle2 = formatText("Rate 1", 14, null, POSITION.RIGHT);
		String rateTitle3 = formatText("Rate 2", 14, null, POSITION.RIGHT);
		rateTitle = rateTitle + rateTitle2 + rateTitle3;
		b.append(rateTitle).append("\n").append(singleSeparator).append("\n");
		DecimalFormat nf = new DecimalFormat("#,###.##");
		
		//Rates
		List<Hashtable<String, Object>> rates = executeQuery(QUERY_RATE, session.get_ID());
		for (int i=0; i<rates.size(); i++)
		{
			Hashtable<String, Object> row = rates.get(i);
			String curName = (String) row.get("description");
			Integer curID = Integer.valueOf(row.get("c_currency_id").toString());
			String __sql = " select crt.multiplyrate "
				+ " FROM C_Conversion_Rate crt WHERE crt.C_Currency_ID = ? AND "
				+ " crt.C_Currency_ID_TO = 303 AND crt.C_ConversionType_ID=114 AND crt.ValidFrom "
				+ " BETWEEN ? AND ? order by validfrom  desc ";
			BigDecimal oo2 = DB.getSQLValueBD(
					get_TrxName(), __sql, curID, session.getDateOpened(), 
					session.getDateClosed());
			
			__sql = "select crt.multiplyrate FROM C_Conversion_Rate crt WHERE crt.C_Currency_ID = "
				+ " ? AND crt.C_Currency_ID_TO = 303 AND crt.C_ConversionType_ID=114 "
				+ " AND  crt.ValidFrom <= ? order by validfrom  desc ";
			BigDecimal oo1 = DB.getSQLValueBD(get_TrxName(), __sql, curID, session.getDateOpened());
			
			String rate1 = "-";
			String rate2 = "-";
			if (oo2 == null && oo1 != null)
				oo2 = oo1;
			if (oo1 != null)
				rate1 = nf.format(oo1.doubleValue());
			if (oo2 != null)
				rate2 = nf.format(oo2.doubleValue());
			curName = formatText(curName, 12, null, POSITION.LEFT);
			rate1 = formatText(rate1, 14, null, POSITION.RIGHT);
			rate2 = formatText(rate2, 14, null, POSITION.RIGHT);
			
			String content = curName + rate1 + rate2;
			b.append(content).append("\n");
		}

		//FLOATS
		List<Hashtable<String, Object>> floats = executeQuery(QUERY_FLOATS, session.get_ID());
		if (floats.size() > 0)
		{
			b.append("\n").append(singleSeparator).append("\n");
			String floatsTitle = formatText("FLOATS", MAX_CHAR_LENGTH, null, POSITION.CENTER);
			b.append(floatsTitle).append("\n").append(singleSeparator).append("\n");
			
			for (int i=0; i<floats.size(); i++)
			{
				Hashtable<String, Object> row = floats.get(i);
				String curName = (String) row.get("code");
				Object oo1 = row.get("floats");
				String float1 = "-";
				if (oo1 != null)
					float1 = nf.format(Double.valueOf(oo1.toString()));
				if (float1.equals("-") || float1.equals("0"))
					continue;
				curName = formatText(curName, 15, null, POSITION.LEFT);
				float1 = formatText(float1, 25, null, POSITION.RIGHT);
				
				String content = curName + float1;
				b.append(content).append("\n");
			}
		}
		
		//ADD CAPITAL
		List<Hashtable<String, Object>> addCaps = executeQuery(QUERY_ADD_CAP, session.get_ID());
		if (addCaps.size() > 0)
		{
			String ac = formatText("ADD CAPITAL", MAX_CHAR_LENGTH, null, POSITION.CENTER); 
			b.append("\n").append(singleSeparator).append("\n").append(ac).append("\n")
			.append(singleSeparator).append("\n");
			
			for (int i=0; i<addCaps.size(); i++)
			{
				Hashtable<String, Object> row = addCaps.get(i);
				String curName = (String) row.get("code");
				Object oo1 = row.get("floats");
				String float1 = "-";
				if (oo1 != null)
					float1 = nf.format(Double.valueOf(oo1.toString()));
				if (float1.equals("-") || float1.equals("0"))
					continue;
				curName = formatText(curName, 15, null, POSITION.LEFT);
				float1 = formatText(float1, 25, null, POSITION.RIGHT);
				
				String content = curName + float1;
				b.append(content).append("\n");
			}
		}
		
		//END OF SHIFT TAKINGS
		String eost = formatText("END OF SHIFT TAKINGS", MAX_CHAR_LENGTH, null, POSITION.CENTER); 
		b.append("\n").append(singleSeparator).append("\n").append(eost).append("\n")
		.append(singleSeparator).append("\n");
		b.append(formatText("Cash", 12, null, POSITION.LEFT)).append(formatText("Transaction", 14, null, POSITION.RIGHT))
		.append(formatText("Eq. Base 1", 14, null, POSITION.RIGHT)).append("\n");
		List<Hashtable<String, Object>> endtakings = executeQuery(QUERY_EOST, session.get_ID());
		double totalEost = 0.0;
		for (int i=0; i<endtakings.size(); i++)
		{
			Hashtable<String, Object> row = endtakings.get(i);
			String curName = (String) row.get("code");
			Object oo1 = row.get("trx");
			Object oo2 = row.get("base1");
			String trx = "-";
			String base1 = "-";
			
			if (oo1 != null)
			{
				double amt = Double.valueOf(oo1.toString());
				trx = nf.format(amt);
			}
			
			if (oo2 != null)
			{
				double amt = Double.valueOf(oo2.toString());
				totalEost += amt;
				base1 = nf.format(amt);
			}
			
			if (trx.equals("-") || trx.equals("0") || base1.equals("-") || base1.equals("0"))
				continue;
			
			curName = formatText(curName, 12, null, POSITION.LEFT);
			trx = formatText(trx, 14, null, POSITION.RIGHT);
			base1 = formatText(base1, 14, null, POSITION.RIGHT);
			
			String content = curName + trx + base1;
			b.append(content).append("\n");			
		}
		
		//CARDS
		String eostTotalTitle = formatText("Total", 12, null, POSITION.LEFT);
		String estTotalAmt = formatText(nf.format(totalEost), 28, null, POSITION.RIGHT);
		String eosttotal = eostTotalTitle + estTotalAmt;
		b.append(eosttotal).append("\n");
		
		List<Hashtable<String, Object>> ccs = executeQuery(QUERY_CC, session.get_ID());
		if (ccs.size() > 0)
		{
			String ccTItle = formatText("CREDIT CARDS", MAX_CHAR_LENGTH, null, POSITION.CENTER); 
			b.append("\n").append(singleSeparator).append("\n").append(ccTItle).append("\n")
			.append(singleSeparator).append("\n");
			b.append(formatText("Card Type", 20, null, POSITION.LEFT)).append(formatText("Transaction", 20, null, POSITION.RIGHT))
			.append("\n");
			double totalTrxCard = 0.0;
			double totalAmtCard = 0.0;
			for (int i=0; i<ccs.size(); i++)
			{
				Hashtable<String, Object> row = ccs.get(i);
				String curName = (String) row.get("name");
				Object oo1 = row.get("satuancard");
				Object oo2 = row.get("amount");
				String trx = "-";
				String base1 = "-";
				
				if (oo1 != null)
				{
					double amt = Double.valueOf(oo1.toString());
					totalTrxCard += amt;
					trx = nf.format(amt);
				}
				
				if (oo2 != null)
				{
					double amt = Double.valueOf(oo2.toString());
					totalAmtCard += amt;
					base1 = nf.format(amt);
				}
				
				if (trx.equals("-") || trx.equals("0") || base1.equals("-") || base1.equals("0"))
					continue;
				
				curName = formatText(curName, 12, null, POSITION.LEFT);
				trx = formatText(trx, 8, null, POSITION.RIGHT);
				base1 = formatText(base1, 20, null, POSITION.RIGHT);
				
				String content = curName + trx + base1;
				b.append(content).append("\n");		
			}
			b.append(formatText("Total", 12, null, POSITION.LEFT));
			b.append(formatText(nf.format(totalTrxCard), 8, null, POSITION.RIGHT));
			b.append(formatText(nf.format(totalAmtCard), 20, null, POSITION.RIGHT));
			b.append("\n");
		}
		
		//DIFFERENCE
		List<Hashtable<String, Object>> diffs = executeQuery(QUERY_DIFF, session.get_ID());
		if (diffs.size() > 0)
		{
			boolean isTitlePrinted = false;
			for (int i=0; i<diffs.size(); i++)
			{
				Hashtable<String, Object> row = diffs.get(i);
				String curName = (String) row.get("iso_code");
				Object oo1 = row.get("overcashier");
				Object oo2 = row.get("shortcashier");
				Object oo3 = row.get("shortcashieramtbase1");
				String trx = "-";
				String base1 = "-";
				String base2 = "-";
				
				if (oo1 != null)
				{
					double amt = Double.valueOf(oo1.toString());
					trx = nf.format(amt);
				}
				
				if (oo2 != null)
				{
					double amt = Double.valueOf(oo2.toString());
					base1 = nf.format(amt);
				}
				if (oo3 != null)
				{
					double amt = Double.valueOf(oo3.toString());
					base2 = nf.format(amt);
				}
				
				if ((trx.equals("-") || trx.equals("0") ) 
						&& (base1.equals("-") || base1.equals("0")) 
						&& (base2.equals("-") || base2.equals("0")))
					continue;
				if (!isTitlePrinted)
				{
					String diffTitle = formatText("DIFFERENCE", MAX_CHAR_LENGTH, null, POSITION.CENTER); 
					b.append("\n").append(singleSeparator).append("\n").append(diffTitle).append("\n")
					.append(singleSeparator).append("\n");
					b.append(formatText("Currency", 10, null, POSITION.LEFT)).append(formatText("Over", 10, null, POSITION.RIGHT))
					.append(formatText("Short", 9, null, POSITION.RIGHT)).append(formatText("Eq.Base 1", 11, null, POSITION.RIGHT))
					.append("\n");
					isTitlePrinted = true;
				}
				curName = formatText(curName, 10, null, POSITION.LEFT);
				trx = formatText(trx, 10, null, POSITION.RIGHT, false);
				base1 = formatText(base1, 9, null, POSITION.RIGHT, false);
				base2 = formatText(base2, 11, null, POSITION.RIGHT, false);
				
				String content = curName + trx + base1 + base2;
				b.append(content).append("\n");		
			}
		}
		
		//VOUCHER
		List<Hashtable<String, Object>> vcrs = executeQuery(QUERY_VCR, session.get_ID());
		if (vcrs.size() > 0 )
		{
			String vcrtitle = formatText("VOUCHER", MAX_CHAR_LENGTH, null, POSITION.CENTER); 
			b.append("\n").append(singleSeparator).append("\n").append(vcrtitle).append("\n")
			.append(singleSeparator).append("\n");

			b.append(formatText("Voucher Type", 20, null, POSITION.LEFT)).append(formatText("Transaction", 20, null, POSITION.RIGHT))
			.append("\n");
			for (int i=0; i<vcrs.size(); i++)
			{
				Hashtable<String, Object> row = vcrs.get(i);
				String curName = (String) row.get("trxno");
				Object oo1 = row.get("namavoucher");
				Object oo2 = row.get("amount");
				String trx = "-";
				String base1 = "-";
				
				if (oo1 != null)
				{
					double amt = Double.valueOf(oo1.toString());
					trx = nf.format(amt);
				}
				
				if (oo2 != null)
				{
					double amt = Double.valueOf(oo2.toString());
					base1 = nf.format(amt);
				}
				
				
				curName = formatText(curName, 12, null, POSITION.LEFT);
				trx = formatText(trx, 8, null, POSITION.RIGHT);
				base1 = formatText(base1, 20, null, POSITION.RIGHT);
				
				String content = curName + trx + base1;
				b.append(content).append("\n");		
			}
		}
		
		b.append("\n").append(doubleSeparator).append("\n");
		
		sql = "SELECT COUNT (UNS_PosTrx_ID) FROM UNS_PosTrx WHERE UNS_Pos_Session_ID="
				+ " ? AND C_DocType_ID=? ";
		int countTrx = DB.getSQLValue(
				get_TrxName(), sql, session.get_ID(), 
				MDocType.getDocType(MDocType.DOCBASETYPE_POSSales));
		int countTrxRefund = DB.getSQLValue(
				get_TrxName(), sql, session.get_ID(), 
				MDocType.getDocType(MDocType.DOCBASETYPE_POSReturn));
		
		sql = "SELECT COALESCE (SUM (GrandTotal),0) FROM UNS_PosTrx WHERE "
				+ " UNS_Pos_Session_ID=? AND C_DocType_ID=?";
		BigDecimal amtSales = DB.getSQLValueBD(
				get_TrxName(), sql, session.get_ID(), 
				MDocType.getDocType(MDocType.DOCBASETYPE_POSSales));
		
		BigDecimal amtrefund = DB.getSQLValueBD(
				get_TrxName(), sql, session.get_ID(), 
				MDocType.getDocType(MDocType.DOCBASETYPE_POSReturn));
		
		sql = "SELECT COALESCE(SUM (ConvertedAmt),0) FROM UNS_PaymentTrx ptrx "
				+ " INNER JOIN UNS_POSPayment pay ON ptrx.UNS_POSPayment_ID="
				+ " pay.UNS_POSPayment_ID AND pay.C_DocType_ID=(SELECT C_DocType_ID FROM C_DocType "
				+ " WHERE DocBaseType='PCM') WHERE pay.UNS_POSTrx_ID IN (SELECT UNS_POSTrx_ID FROM "
				+ " UNS_POSTrx WHERE UNS_POS_Session_ID=?) AND ptrx.PaymentMethod=?";
		
		BigDecimal cashRefund = DB.getSQLValueBD(get_TrxName(), sql, session.get_ID(), MUNSPaymentTrx.PAYMENTMETHOD_Cash);
		BigDecimal edcRefund = DB.getSQLValueBD(get_TrxName(), sql, session.get_ID(), MUNSPaymentTrx.PAYMENTMETHOD_Card);
		BigDecimal wcRefund = DB.getSQLValueBD(get_TrxName(), sql, session.get_ID(), MUNSPaymentTrx.PAYMENTMETHOD_Wechat);
		BigDecimal vcrRefund = DB.getSQLValueBD(get_TrxName(), sql, session.get_ID(), MUNSPaymentTrx.PAYMENTMETHOD_Voucher);
		
		String minTrx = DB.getSQLValueString(
				get_TrxName(), "SELECT MIN (TrxNo) FROM UNS_PosTrx Where UNS_Pos_Session_ID=?", 
				session.get_ID());
		String maxTrx = DB.getSQLValueString(
				get_TrxName(), "SELECT Max (TrxNo) FROM UNS_PosTrx Where UNS_Pos_Session_ID=?", 
				session.get_ID());
		
		String tTransTitle = formatText("Total Trans", 15, null, POSITION.LEFT);
		String countTrxText = formatText(nf.format(countTrx), 5, null, POSITION.RIGHT);
		String amtTrxText = formatText(nf.format(amtSales.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(tTransTitle).append(countTrxText).append(amtTrxText).append("\n");
		
		String rfndCashTitle = formatText("  Refund Cash", 20, null, POSITION.LEFT);
		String rfndCashAmtText = formatText(nf.format(cashRefund.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(rfndCashTitle).append(rfndCashAmtText).append("\n");
		

		rfndCashTitle = formatText("  Refund EDC", 20, null, POSITION.LEFT);
		rfndCashAmtText = formatText(nf.format(edcRefund.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(rfndCashTitle).append(rfndCashAmtText).append("\n");
		

		rfndCashTitle = formatText("  Refund Wechat", 20, null, POSITION.LEFT);
		rfndCashAmtText = formatText(nf.format(wcRefund.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(rfndCashTitle).append(rfndCashAmtText).append("\n");
		

		rfndCashTitle = formatText("  Refund Voucher", 20, null, POSITION.LEFT);
		rfndCashAmtText = formatText(nf.format(vcrRefund.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(rfndCashTitle).append(rfndCashAmtText).append("\n");
		
		tTransTitle = formatText("Total Refund", 15, null, POSITION.LEFT);
		countTrxText = formatText(nf.format(countTrxRefund), 5, null, POSITION.RIGHT);
		amtTrxText = formatText(nf.format(amtrefund.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(tTransTitle).append(countTrxText).append(amtTrxText).append("\n");
		
		int trxMinRef = countTrx + countTrxRefund;
		BigDecimal amtMinRef = amtSales.add(amtrefund);
		tTransTitle = formatText("Trans - Refund", 15, null, POSITION.LEFT);
		countTrxText = formatText(nf.format(trxMinRef), 5, null, POSITION.RIGHT);
		amtTrxText = formatText(nf.format(amtMinRef.doubleValue()), 20, null, POSITION.RIGHT);
		b.append(tTransTitle).append(countTrxText).append(amtTrxText).append("\n");
		
		String receiptNo = formatText("Receipt No", 16, null, POSITION.LEFT);
		String trxMInText = formatText(minTrx, 9, null, POSITION.RIGHT);
		String to = formatText("To", 6, null, POSITION.CENTER);
		String trxMaxText = formatText(maxTrx, 9, null, POSITION.LEFT);
		b.append(receiptNo).append(trxMInText).append(to).append(trxMaxText).append("\n");
		b.append(doubleSeparator).append("\n");
		
		String receiver = formatText("Receiver", MAX_CHAR_LENGTH, null, POSITION.CENTER);
		b.append(receiver).append("\n");
		if (DocAction.STATUS_Drafted.equals(session.getDocStatus())
				|| DocAction.STATUS_InProgress.equals(session.getDocStatus())
				|| DocAction.STATUS_Invalid.equals(session.getDocStatus())
				|| DocAction.STATUS_NotApproved.equals(session.getDocStatus())
				|| DocAction.STATUS_Approved.equals(session.getDocStatus()))
		{
			b.append(formatText("DRAFTED", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
			b.append(formatText("DRAFTED", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
			b.append(formatText("DRAFTED", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
		}
		else
		{
			b.append("\n\n\n");
		}
		if (session.getSupervisor_ID() != 0)
		{
			sql = "SELECT Name FROM AD_User WHERE AD_User_ID = ?";
			String spv = DB.getSQLValueString(get_TrxName(), sql, session.getSupervisor_ID());
			b.append(formatText(spv, MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
		}
		else
			b.append(formatText("-", 20, null, POSITION.CENTER)).append("\n");
		
		b.append(singleSeparator).append("\n")
		.append(formatText("Sales Cashier Supervisor", MAX_CHAR_LENGTH, null, POSITION.CENTER))
		.append("\n").append(formatText("Depositor", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
		
		if (DocAction.STATUS_Drafted.equals(session.getDocStatus())
				|| DocAction.STATUS_InProgress.equals(session.getDocStatus())
				|| DocAction.STATUS_Invalid.equals(session.getDocStatus())
				|| DocAction.STATUS_NotApproved.equals(session.getDocStatus())
				|| DocAction.STATUS_Approved.equals(session.getDocStatus()))
		{
			b.append(formatText("DRAFTED", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
			b.append(formatText("DRAFTED", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
			b.append(formatText("DRAFTED", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n");
		}
		else
		{
			b.append("\n\n\n");
		}
		
		b.append(formatText(cName, MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n")
		.append(singleSeparator).append("\n").append(formatText("Cashier, ", MAX_CHAR_LENGTH, null, POSITION.LEFT))
		.append("\n\n").append("Plaza Bali ").append(df.format(new Timestamp(System.currentTimeMillis())))
		.append("\n").append(formatText("==", MAX_CHAR_LENGTH, null, POSITION.CENTER)).append("\n\n\n");

		String finalDisplay = b.toString();
		final ByteArrayInputStream bys = new ByteArrayInputStream(finalDisplay.getBytes());
		
		String printerName = Ini.getProperty(Ini.P_PRINTER);
		PrintService service = null;
		PrintService[] services = PrintUtil.getPrintServices();
		
		for (int i=0; i<services.length; i++)
		{
			if (services[i].getName().equals(printerName))
			{
				service = services[i];
				break;
			}
		}
		
		DocPrintJob job = service.createPrintJob();
		DocFlavor f = DocFlavor.INPUT_STREAM.AUTOSENSE;
		SimpleDoc doc = new SimpleDoc(bys, f, null);
		job.print(doc, null);
		
		return "File send to printer";
	}
	
	private enum POSITION
	{
		CENTER,RIGHT, LEFT
	}
	
	private String formatText (String text, int allocated, String replacement,  POSITION position, 
			boolean cutWhenOverFlow)
	{
		if (Util.isEmpty(text, true))
			text = "-";
		int unallocated = allocated;
		if (unallocated == 0)
			return "";
		if (text.length() > allocated && cutWhenOverFlow)
			return text.substring(0,allocated);
		
		StringBuilder formatBuilder = new StringBuilder("%");
		if (POSITION.CENTER.equals(position))
		{
			unallocated = unallocated - text.length();
			if (unallocated < 0)
			{
				text = text.substring(0,allocated);
				unallocated = 0;
			}
			int blank = unallocated / 2;
			blank = text.length() + blank;
			formatBuilder.append(blank);
			formatBuilder.append("s");
			blank = unallocated;
		}
		else if (POSITION.LEFT.equals(position))
		{
			formatBuilder.append("1$-");
			formatBuilder.append(unallocated);
			formatBuilder.append("s");
			unallocated = 0;
		}
		else 
		{
			formatBuilder.append(unallocated);
			formatBuilder.append("s");
			unallocated = 0;
		}
		
		String format = formatBuilder.toString();
		String result = String.format(format, text);
		if (unallocated > 0)
		{
			result = formatText(result, allocated, replacement, POSITION.LEFT);
		}
		
		if (!Util.isEmpty(replacement, true))
			result = result.replace(" ", replacement);
		return result;
	}
	
	private String formatText (String text, int allocated, String replacement,  POSITION position)
	{
		return formatText(text, allocated, replacement, position, true);
	}
	
	private List<Hashtable<String, Object>> executeQuery (String sql, int param)
	{
		List<Hashtable<String, Object>> result = new ArrayList<>();
		int paramCount = StringUtil.count(sql, "?");
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			for (int i=0; i<paramCount;)
			{
				int paramIdx = ++i;
				st.setInt(paramIdx, param);
			}
			rs = st.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			int colCount = meta.getColumnCount();
			while (rs.next())
			{
				Hashtable<String, Object> row = new Hashtable<>();
				result.add(row);
				for (int i=1; i<=colCount; i++)
				{
					String colName = meta.getColumnName(i);
					Object value = rs.getObject(i);
					row.put(colName, value);
				}
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return result;
	}
	
	private final int MAX_CHAR_LENGTH = 40;
	
	private final String QUERY_RATE = "SELECT cur.Description, cur.C_Currency_ID::INTEGER "
			+ " FROM UNS_POS_Session ses INNER JOIN	UNS_SessionCashAccount sca ON "
			+ " sca.UNS_POS_Session_ID = ses.uns_pos_session_ID  AND "
			+ " sca.C_Currency_ID <> 303 INNER JOIN C_Currency cur ON cur.C_Currency_ID = "
			+ " sca.C_Currency_ID WHERE ses.UNS_POS_Session_ID = ? "
			+ " ORDER BY cur.Description";
	
	private final String QUERY_CASHIER_TANDEM = "SELECT COALESCE (u.RealName,u.Name) AS Name "
			+ " FROM UNS_CashierTandem ct INNER JOIN AD_User AS u ON ct.Cashier_ID = "
			+ " u.AD_User_ID INNER JOIN UNS_POS_Session ps ON ps.UNS_POS_Session_ID="
			+ " ct.UNS_POS_Session_ID WHERE ps.UNS_POS_Session_ID=? "
			+ " AND ct.Cashier_ID != ps.Cashier_ID";
	
	private final String QUERY_FLOATS = "SELECT DISTINCT c.Description AS Code, "
			+ " usc.BeginningBalance AS Floats FROM UNS_POS_Session AS up INNER JOIN "
			+ " UNS_POSTrx AS upt ON upt.UNS_POS_Session_ID = up.UNS_POS_Session_ID "
			+ " INNER JOIN UNS_SessionCashAccount usc ON usc.UNS_POS_Session_ID = up.UNS_POS_Session_ID"
			+ " INNER JOIN C_Currency AS c ON usc.C_Currency_ID = c.C_Currency_ID "
			+ " WHERE up.DocStatus IN('CO','IP','CL') AND up.uns_pos_session_id=?"
			+ " ORDER BY c.Description ASC";
	
	private final String QUERY_ADD_CAP = "SELECT DISTINCT c.Description AS Code, SUM(pay.trxamt) AS "
			+ " Floats FROM UNS_POS_Session sesi INNER JOIN UNS_PaymentTrx pay ON "
			+ " pay.UNS_POS_Session_ID = sesi.UNS_POS_Session_ID AND pay.trxtype='M+' "
			+ " AND pay.trxamt>0 INNER JOIN C_Currency c ON c.C_Currency_ID = "
			+ " pay.C_Currency_ID WHERE sesi.DocStatus IN('CO','IP','CL') AND "
			+ " sesi.uns_pos_session_id=? GROUP BY "
			+ " code ORDER BY c.Description ASC";
	
	//END OF SHIFT TAKINGS
	private final String QUERY_EOST = "SELECT c.description AS Code, CASE WHEN SUM(pay.trxamt)!=0 THEN "
			+ " COALESCE ((CashIn-CashOut)-(SUM(pay.trxamt)),0) ELSE COALESCE ((CashIn-CashOut),0) END "
			+ " AS Trx, CASE WHEN SUM(pay.trxamt)!=0 THEN COALESCE ((CashInBase1-CashOutBase1)-(SUM(pay.convertedamt)),0) "
			+ " ELSE  COALESCE ((CashInBase1-CashOutBase1),0) END AS Base1, CASE WHEN (SELECT (SUM(CashIn-CashOut)) "
			+ " FROM UNS_SessionCashAccount WHERE UNS_POS_Session_ID = ?) != 0 THEN 1 "
			+ " ELSE 0 END AS Condition FROM UNS_SessionCashAccount sca INNER JOIN C_Currency c ON "
			+ " c.C_Currency_ID=sca.C_Currency_ID LEFT JOIN UNS_PaymentTrx pay ON "
			+ " pay.UNS_POS_Session_ID=sca.UNS_POS_Session_ID AND pay.C_Currency_ID=sca.C_Currency_ID AND trxtype='M+' "
			+ " WHERE sca.UNS_POS_Session_ID=? GROUP BY code, sca.UNS_SessionCashAccount_ID";
	
	//CREDIT CARD
	private final String QUERY_CC = "SELECT DISTINCT COUNT(ctd.uns_cardType_id) AS SatuanCard, ct.Name, "
			+ " sum(ptx.Amount) Amount, CASE WHEN (SELECT SUM(TotalAmt) FROM UNS_SessionEDCSummary "
			+ " WHERE UNS_POS_Session_ID = ?) != 0 THEN 1 ELSE 0 END AS Condition "
			+ " FROM UNS_CardTrxDetail ctd INNER JOIN UNS_CardType ct ON ctd.UNS_CardType_ID = "
			+ " ct.UNS_CardType_ID INNER JOIN UNS_PaymentTrx ptx ON ptx.UNS_PaymentTrx_ID = "
			+ " ctd.UNS_PaymentTrx_ID INNER JOIN UNS_POSPayment pp ON pp.UNS_POSPayment_ID = "
			+ " ptx.UNS_POSPayment_ID LEFT JOIN UNS_POSTrx trx ON trx.Reference_ID = pp.UNS_POSTrx_ID "
			+ " INNER JOIN UNS_POS_Session ps ON ps.UNS_POS_Session_ID = ptx.UNS_POS_Session_ID "
			+ " INNER JOIN C_DocType dt ON dt.C_DocType_ID = pp.C_DocType_ID WHERE "
			+ " ps.UNS_Pos_Session_ID=? AND ptx.TrxType IN('D+','W+') "
			+ " AND ptx.UNS_POS_Session_ID = ps.UNS_POS_Session_ID AND (trx.UNS_POSTrx_ID IS NULL "
			+ " OR trx.UNS_POSTrx_ID <= 0) GROUP BY ct.name ORDER BY ct.Name";
	
	//DIFFERENCE
	private final String QUERY_DIFF = "SELECT ShortCashierAmt, ABS(ShortCashierAmtBase1) AS ShortCashierAmtBase1, "
			+ " c.description AS ISO_Code, CASE WHEN (SELECT SUM(ShortCashierAmt) FROM UNS_SessionCashAccount "
			+ " WHERE UNS_POS_Session_ID =? ) != 0 THEN 1 ELSE 0 END AS Condition,"
			+ " (Select CASE WHEN ShortCashierAmt > 0 THEN 0 ELSE ShortCashierAmt END AS OverCashier)* -1 "
			+ " OverCashier, CASE WHEN ShortCashierAmt < 0 THEN 0 ELSE ShortCashierAmt END AS ShortCashier "
			+ " FROM UNS_SessionCashAccount sca INNER join C_Currency c ON c.C_Currency_ID=sca.C_Currency_ID "
			+ " WHERE UNS_POS_Session_ID=?";
	
	//VOUCHER
	private final String QUERY_VCR = "SELECT SUBSTRING(ppt.trxno,1,4) trxno, COUNT(SUBSTRING(ppt.trxno,1,4)) "
			+ " NamaVoucher , SUM(ppt.Amount) Amount FROM UNS_PaymentTrx ppt INNER JOIN UNS_POSPayment pp on "
			+ " ppt.UNS_POSPayment_ID = pp.UNS_POSPayment_ID INNER JOIN UNS_POSTrx pt on pt.UNS_POSTrx_ID = "
			+ " pp.UNS_POSTrx_ID INNER JOIN UNS_POS_Session ps on ps.UNS_POS_Session_ID = "
			+ " ppt.UNS_POS_Session_ID WHERE TrxType = 'V+' AND pp.C_DocType_ID IN (SELECT C_DocType_ID "
			+ " FROM C_DocType WHERE DocBaseType='PRR') AND ps.UNS_POS_Session_ID=? "
			+ " AND pt.UNS_POSTrx_ID NOT IN (SELECT Reference_ID FROM UNS_POSTrx pos WHERE pos.Reference_ID="
			+ " pt.UNS_POSTrx_ID) GROUP BY SUBSTRING(ppt.TrxNo,1,4)";
}