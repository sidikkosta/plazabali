package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import com.unicore.base.model.MInOut;
import com.unicore.base.model.MInOutLine;
import com.unicore.model.MUNSWeighbridgeTicket;

public class CreateLineTicket extends SvrProcess {
	
	private MUNSWeighbridgeTicket m_ticket;
	private int m_ticketid = 0;
	private int m_doctypeid = 0;

	
	public CreateLineTicket() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter para : params)
		{
			if(para.getParameterName().equals("UNS_WeighbridgeTicket_ID"))
				m_ticketid = para.getParameterAsInt();
			else if(para.getParameterName().equals("C_DocType_ID"))
				m_doctypeid = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter : "+para.getParameterName());
		}
		
		m_ticket = new MUNSWeighbridgeTicket(getCtx(), m_ticketid, get_TrxName());
	}

	@Override
	protected String doIt() throws Exception {
		
		MDocType mdoctype = new MDocType(getCtx(), m_doctypeid, get_TrxName());
		
		if("MM Receipt".equals(mdoctype.getName()))
		{
			MInOut receipt = new MInOut(getCtx(), getRecord_ID(), get_TrxName());
	
//			int war = receipt.getM_Warehouse_ID();
//			int loc = m_ticket.getM_Locator_ID();

			if(receipt.getM_InOut_ID() <= 0)
			{
				throw new AdempiereException("Error when try to get m_inout");
			}
			String sql = "SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = ? AND M_Locator_ID = ?";
			int locator = DB.getSQLValue(get_TrxName(), sql, receipt.getM_Warehouse_ID(), m_ticket.getM_Locator_ID());
			
			sql = "SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?";
			int uom = DB.getSQLValue(get_TrxName(), sql, m_ticket.getM_Product_ID());
			
			MInOutLine line = new MInOutLine(getCtx() , 0 , get_TrxName());
			line.setAD_Org_ID(receipt.getAD_Org_ID());
			line.setM_InOut_ID(receipt.getM_InOut_ID());
			line.setM_Product_ID(m_ticket.getM_Product_ID());
			line.setC_UOM_ID(uom);
			line.setM_Locator_ID(locator);
			line.setQtyEntered(m_ticket.getNettoII());
			if(!line.save())
				throw new AdempiereException("Error when try to save line");
			
			updateGeneralTicket();
			
		}
		else if("Material Movement".equals(mdoctype.getName()) || "Intransit Material Movement".equals(mdoctype.getName()))
		{
			MMovement move = new MMovement(getCtx(), getRecord_ID(), get_TrxName());

			if(move.getM_Movement_ID() <= 0)
			{
				throw new AdempiereException("Error when try to get m_movement");
			}
			int destinationid = move.getDestinationWarehouse_ID();
			
			String sql = "SELECT M_Locator_ID FROM M_Locator WHERE M_WareHouse_ID = ? AND IsDefault = ?";
			int locatorto = DB.getSQLValue(get_TrxName(), sql, destinationid, "Y");
			if(locatorto <= 0)
			{
				sql = "SELECT M_Locator_ID FROM M_Locator WHERE M_WareHouse_ID = ? ";
				locatorto = DB.getSQLValue(get_TrxName(), sql, destinationid);
			}	

			MMovementLine line = new MMovementLine(getCtx(), 0, get_TrxName());
			line.setAD_Org_ID(move.getAD_Org_ID());
			line.setM_Movement_ID(getRecord_ID());
			line.setM_Product_ID(m_ticket.getM_Product_ID());
			line.setMovementQty(m_ticket.getNettoII());
			line.setM_Locator_ID(m_ticket.getM_Locator_ID());
			line.setM_LocatorTo_ID(locatorto);
			if(!line.save())
				throw new AdempiereException("Error when try to save line");
			
			updateGeneralTicket();
		}
		return null;
	}
	
	public void updateGeneralTicket(){
//		m_ticket.setAD_Table_ID(getTable_ID());
//		m_ticket.setRecord_ID(getRecord_ID());
//		if(!m_ticket.save()){
//			throw new AdempiereException(Msg.getMsg(getCtx(), "Error when update General Ticket"));
//		}
	}

}
