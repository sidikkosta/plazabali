/**
 * 
 */
package com.unicore.model.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;

import com.unicore.model.MUNSVoucherBook;

/**
 * @author Burhani Adam
 *
 */
public class UNSUpdateValidToVoucherBook extends SvrProcess {

	/**
	 * 
	 */
	public UNSUpdateValidToVoucherBook() {
		// TODO Auto-generated constructor stub
	}
	
	private Timestamp p_validTo = null;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("ValidTo"))
				p_validTo = param.getParameterAsTimestamp();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		} 
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MUNSVoucherBook vb = new MUNSVoucherBook(getCtx(), getRecord_ID(), get_TrxName());
		vb.setValidTo(p_validTo);
		if(!vb.save())
			throw new AdempiereException(CLogger.retrieveErrorString("Failed.!"));
		
		return "Success";
	}
}