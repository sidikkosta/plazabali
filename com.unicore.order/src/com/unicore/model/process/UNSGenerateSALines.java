/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MDocType;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSSASummary;
import com.unicore.model.MUNSStatementOfAccount;

/**
 * @author Burhani Adam
 *
 */
public class UNSGenerateSALines extends SvrProcess {
	
	private IProcessUI m_process;

	/**
	 * 
	 */
	public UNSGenerateSALines() {
		// TODO Auto-generated constructor stub
	}
	

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window Statement Of Account (Summary)");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		m_process = Env.getProcessUI(getCtx());
		MUNSSASummary sum = new MUNSSASummary(getCtx(), getRecord_ID(), get_TrxName());
		
		String sql = "DELETE FROM UNS_StatementOfAcct_Trx t WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_StatementOfAcct_Line l WHERE l.UNS_StatementOfAcct_Line_ID"
				+ " = t.UNS_StatementOfAcct_Line_ID AND EXISTS "
				+ " (SELECT 1 FROM UNS_StatementOfAccount a WHERE a.UNS_StatementOfAccount_ID"
				+ " = l.UNS_StatementOfAccount_ID AND a.UNS_SA_Summary_ID = ?))";
		DB.executeUpdate(sql, sum.get_ID(), get_TrxName());
		sql = "DELETE FROM UNS_StatementOfAcct_Line l WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_StatementOfAccount a WHERE a.UNS_StatementOfAccount_ID"
				+ " = l.UNS_StatementOfAccount_ID AND a.UNS_SA_Summary_ID = ?)";
		DB.executeUpdate(sql, sum.get_ID(), get_TrxName());
		
//		sql = "DELETE FROM UNS_StatementOfAccount a WHERE a.UNS_SA_Summary_ID = ?";
//		DB.executeUpdate(sql, sum.get_ID(), get_TrxName());
		
		sql = "SELECT COUNT(DISTINCT(po.C_BPartner_ID)) FROM M_Product_PO po"
				+ " WHERE EXISTS (SELECT 1 FROM M_Product p WHERE p.M_Product_ID"
				+ " = po.M_Product_ID AND p.IsConsignment = 'Y')"
				+ " AND (EXISTS (SELECT 1 FROM M_Transaction t WHERE t.M_Product_ID"
				+ " = po.M_Product_ID AND (t.MovementDate BETWEEN ? AND ?) AND t.AD_Org_ID = ?)"
				+ " OR EXISTS (SELECT 1 FROM M_StorageOnHand soh WHERE soh.M_Product_ID"
				+ " = po.M_Product_ID AND soh.QtyOnHand != 0 AND soh.AD_Org_ID = ? AND EXISTS"
				+ " (SELECT 1 FROM M_Locator loc WHERE loc.M_Locator_ID = soh.M_Locator_ID"
				+ " AND EXISTS (SELECT 1 FROM M_Warehouse whs WHERE whs.M_Warehouse_ID"
				+ " = loc.M_Warehouse_ID AND whs.UNS_Division_ID = ?))))";
		int counts = DB.getSQLValue(get_TrxName(), sql, new Object[]{
			sum.getDateFrom(), sum.getDateTo(), sum.getAD_Org_ID(), sum.getAD_Org_ID(),
				sum.getUNS_Division_ID()});
		sql = sql.replace("COUNT(DISTINCT(po.C_BPartner_ID))", "DISTINCT(po.C_BPartner_ID), po.C_Currency_ID");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setTimestamp(1, sum.getDateFrom());
			stmt.setTimestamp(2, sum.getDateTo());
			stmt.setInt(3, sum.getAD_Org_ID());
			stmt.setInt(4, sum.getAD_Org_ID());
			stmt.setInt(5, sum.getUNS_Division_ID());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				m_process.statusUpdate("Sedang proses data ke " + rs.getRow() + " dari " + counts + " data supplier.");
				System.out.println("========================== data ke " + rs.getRow() + " ==========================");
				MUNSStatementOfAccount sa = MUNSStatementOfAccount.get(sum, rs.getInt(1));
				sa.setC_BPartner_ID(rs.getInt(1));
				sa.setC_Currency_ID(rs.getInt(2));
				sa.setC_BPartner_Location_ID(MBPartnerLocation.getForBPartner(getCtx(), rs.getInt(1), get_TrxName())[0].get_ID());
				sa.setDescription(sum.getDescription());
				if(rs.getInt(2) == 303)
					sa.setDocTypeInvoice_ID(MDocType.getOfSubTypeInvoice(MDocType.DOCSUBTYPEINVOICE_DutyFreeLocal));
				else
					sa.setDocTypeInvoice_ID(MDocType.getOfSubTypeInvoice(MDocType.DOCSUBTYPEINVOICE_DutyFreeImport));
				sa.setDescription(sum.getDescription());
				
//				if(sum.getDocTypeInvoice_ID() > 0)
//					sa.setDocTypeInvoice_ID(sum.getDocTypeInvoice_ID());
				sa.save();
				System.out.println("xxxxxxxxxxxxxxxxx save sa = 1 = "+sa.get_ID() );

				if(sa.getGrandTotal().signum() == 0)
					sa.deleteEx(true);
				else
					count++;
			}
		} catch (SQLException e) {
			throw new AdempiereException("failed when trying load all partner consignment");
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		sum.setTotalLines(BigDecimal.valueOf(count));
		sum.save();
		
		return "Success.";
	}
	
	private boolean getBpartnerVat (int C_BPartner_ID) {
		boolean vat = false;
		String sql = "select IsPKP from c_bpartner where c_bpartner_id ="+C_BPartner_ID+"  ";
		String vat_ = DB.getSQLValueString(get_TrxName(), sql);
		if(vat_.equalsIgnoreCase("Y"))
			vat = true;
		else {vat = false;}
		
		return vat;
	}
}