/**
 * 
 */
package com.unicore.model.process;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jxl.Sheet;
import jxl.Workbook;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MProduct;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CCache;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Util;
import com.unicore.model.MUNSCustomerInfo;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPOSTrxLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class UNSPOSImporter extends SvrProcess 
{
	
	private String p_fileName = null;
	private int p_start = -1;
	private int p_end = -1;
	private String p_sheetName = null;
	private CCache<String, List<Object>> l_cache = new CCache<>("UNSPOSImporter", 10);
	private List<MUNSPOSSession> c_session = new ArrayList<>();
	private CCache<String, MProduct> c_product = new CCache<>("UNSPOSImpProduct", 100);
	private final String KEY_ORGANIZATION = "ORG";
	private final String KEY_CURRENCY = "CUR";
	private final String KEY_USER = "USR";
	private final String KEY_SHOP_LOCATION = "SPL";
	private final String KEY_DISC_REFF = "DISC_REFF";
	private final String KEY_AIRLINE = "AIRLINE";
	private final String KEY_COUNTRY = "COUNTRY";

	/**
	 * 
	 */
	public UNSPOSImporter() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("StartNo".equals(params[i].getParameterName()))
				p_start = params[i].getParameterAsInt();
			else if ("EndNo".equals(params[i].getParameterName()))
				p_end = params[i].getParameterAsInt();
			else if ("SheetName".equals(params[i].getParameterName()))
				p_sheetName = params[i].getParameterAsString();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(p_fileName, true))
			throw new AdempiereException("Mandatory parameter File Name");
		if (!p_fileName.endsWith(".xls"))
			throw new AdempiereException("Unsuported file. Please convert to xls first.");
		File file = new File(p_fileName);
		if (!file.exists())
			throw new AdempiereException("Could not find file " + p_fileName);
		if (Util.isEmpty(p_sheetName, true))
			p_sheetName = "POS";
		
		Workbook book = Workbook.getWorkbook(file);
		Sheet[] sheets = book.getSheets();
		Sheet sheet = null;
		for (int i=0; i<sheets.length; i++)
		{
			String name = sheets[i].getName().toUpperCase();
			if (name.equals(p_sheetName))
			{
				sheet = sheets[i];
				break;
			}
		}
		
		if (sheet != null)
		{
			POSImportRawData rawDataInstance = new POSImportRawData(sheet);
			List<String[]> rawData = rawDataInstance.parseValues(processUI, p_start, p_end);
			processRows(rawData);
		}
		
		return "File imported successfully";
	}
	
	private void processRows (List<String[]> rows) throws Exception
	{
		for (int i=0; i<rows.size(); i++)
		{
			processUI.statusUpdate("Processing " + (i+1) + " From " + rows.size());
			processRow(rows.get(i));
			DB.commit(true, get_TrxName());
		}
	}
	
	private void processRow (String[] row) throws Exception
	{
		BigDecimal rate = new BigDecimal(15000);
		String orgVal = row[POSImportRawData.COLUMN_ORG];
		int orgID = initOrgID(orgVal);
		if (orgID == -1)
			throw new AdempiereException("Could not find org with value " + orgVal);
		String documentNo = row[POSImportRawData.COLUMN_DOCUMENTNO];
		String trxNo = row[POSImportRawData.COLUMN_TRXNO];
		if (Util.isEmpty(documentNo, true) && Util.isEmpty(trxNo, true))
			throw new AdempiereException("Required value of Document No Or Trx No");
		if (Util.isEmpty(trxNo, true))
			trxNo = documentNo;
		else if (Util.isEmpty(documentNo, true))
			documentNo = trxNo;
		String currISO = row[POSImportRawData.COLUMN_CURRENCY];
		int currID = initCurrencyID(currISO);
		if (currID == -1)
			throw new AdempiereException("Could not find currency with iso " + currISO);
		String dateStr = row[POSImportRawData.COLUMN_DATEACCT];
		SimpleDateFormat dateAcctFormat = new SimpleDateFormat("MM/dd/yy");
		Date d = dateAcctFormat.parse(dateStr);
		Timestamp dateAcct = new Timestamp(d.getTime());
		dateStr = row[POSImportRawData.COLUMN_DATETRX];
		SimpleDateFormat dateTrxFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		d = dateTrxFormat.parse(dateStr);
		Timestamp dateTrx = new Timestamp(d.getTime());
		BigDecimal totDiscAmt = Env.ZERO;
		
		String totDiscAmtStr = row[POSImportRawData.COLUMN_DISCOUNTTOTALAMT];
		
		if (!Util.isEmpty(totDiscAmtStr, true))
		{
			try
			{
				totDiscAmt = new BigDecimal(totDiscAmtStr);
			}
			catch (NumberFormatException ex)
			{
				ex.printStackTrace();
				totDiscAmt = Env.ZERO;
			}
		}
		totDiscAmt = totDiscAmt.multiply(rate);
		String desc = row[POSImportRawData.COLUMN_DESCRIPTION];		
		String shift = row[POSImportRawData.COLUMN_SHIFT];
		
		if (Util.isEmpty(shift, true))
			throw new AdempiereException("Mandatory column shift");
		shift = String.format("%2s", shift).replace(" ", "0");
		
		String cashier = row[POSImportRawData.COLUMN_CASHIER];
		String sales = row[POSImportRawData.COLUMN_SALES];
		
		int cashierID = initUserID(cashier);
		if (cashierID == -1)
			throw new AdempiereException("Could not find user with name " + cashier);

		int salesID = initUserID(sales);
		if (salesID == -1)
			throw new AdempiereException("Could not find user with name " + cashier);
		
		String discReffH = row[POSImportRawData.COLUMN_DISCREFF];
		int totDiscReffID = initDiscReff(discReffH);
		
		MUNSPOSSession session = initSession(orgID, cashierID, shift, dateAcct, desc);
		if (session == null)
			throw new AdempiereException("Could not initial session ");
		int shopID = session.getPOSDevice().getStore_ID();
		int whsID = Env.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID");
		int shopLocationID = initShopLocation(Integer.toString(shopID));
		if (shopLocationID == -1)
			throw new AdempiereException("Can't set shop location");
		boolean isRefund = false;
		String qtyStr = row[POSImportRawData.COLUMN_QTY];
		BigDecimal qty = Env.ZERO;
		if (!Util.isEmpty(qtyStr, true))
		{
			try
			{
				qty = new BigDecimal(qtyStr);
			}
			catch (NumberFormatException ex)
			{
				throw new AdempiereException(ex);
			}
		}
		
		if (qty.signum() == -1)
			isRefund = true;
		
		MUNSPOSTrx trx = initPOSTrx(session, trxNo, salesID, shopID, shopLocationID, whsID, dateTrx, isRefund);
		
		MUNSCustomerInfo info = new MUNSCustomerInfo(getCtx(), trx.getUNS_CustomerInfo_ID(), get_TrxName());	
		info.setUNS_AirLine_ID(initAirLineID(row[POSImportRawData.COLUMN_AIRLINE]));
		info.setC_Country_ID(initCountryID(row[POSImportRawData.COLUMN_COUNTRY]));
		info.setCustomerType(row[POSImportRawData.COLUMN_CUSTOMERTYPE]);
		info.setFlightNo(row[POSImportRawData.COLUMN_FLIGHTNO]);
		info.setName(row[POSImportRawData.COLUMN_CUSTOMERNAME]);
		info.setNIK(row[POSImportRawData.COLUMN_NIK]);
		info.setPassengerType(row[POSImportRawData.COLUMN_PASSENGERTYPE]);
		info.setPassportNo(row[POSImportRawData.COLUMN_PASPORTNO]);
		info.setVIPType(row[POSImportRawData.COLUMN_VIPTYPE]);
		info.setBoardingPass(row[POSImportRawData.COLUMN_BOARDINGPASS]);
		info.setGateNo(row[POSImportRawData.COLUMN_GATENO]);
		info.setDescription(desc);
		info.saveEx();
		if (trx.getUNS_CustomerInfo_ID() == 0)
		{
			trx.setUNS_CustomerInfo_ID(info.get_ID());
			trx.setCustomerName(row[POSImportRawData.COLUMN_CUSTOMERNAME]);
			trx.saveEx();
			DB.commit(true, get_TrxName());
		}
		
		String sku = row[POSImportRawData.COLUMN_SKU];
		MProduct product = initProduct(sku);
		if (product == null)
			throw new AdempiereException("Could not find product " + sku);
		
		String priceStr = row[POSImportRawData.COLUMN_PRICE];
		BigDecimal price = Env.ZERO;
		if (!Util.isEmpty(qtyStr, true))
		{
			try
			{
				price = new BigDecimal(priceStr);
			}
			catch (NumberFormatException ex)
			{
				throw new AdempiereException(ex);
			}
		}
		price = price.multiply(rate);
		String discLStr = row[POSImportRawData.COLUMN_DISCOUNTLINEAMT];
		BigDecimal discLineAmt = Env.ZERO;
		if (!Util.isEmpty(discLStr, true))
		{
			try
			{
				discLineAmt = new BigDecimal(discLStr);
			}
			catch (NumberFormatException ex)
			{
				throw new AdempiereException(ex);
			}
		}
		
		discLineAmt = discLineAmt.multiply(rate);
		String discRefLine = row[POSImportRawData.COLUMN_DISCOUNTREFFLINE];
		int discRefLineID = initDiscReff(discRefLine);
		
		String etbb = row[POSImportRawData.COLUMN_ETBB];
		
		MUNSPOSTrxLine line = initLine(trx, product, etbb);
		line.setQtyEntered(line.getQtyEntered().add(qty));
		line.setQtyOrdered(line.getQtyOrdered().add(qty));
		line.setPrice(price);
		line.setDiscountAmt(line.getDiscountAmt().add(discLineAmt));
		line.setDiscReff(discRefLine);
		line.set_ValueOfColumn("UNS_DiscountRef_ID", discRefLineID);
		line.setDescription(desc);
		line.saveEx();
		
		trx.load(get_TrxName());
//		trx.setDiscountAmt(totDiscAmt);
		trx.setUNS_DiscountReff_ID(totDiscReffID);
		trx.setDiscReff(discReffH);
		trx.setDescription(desc);
		trx.saveEx();
	}
	
	private int initAirLineID (String keyName)
	{
		int airLine = -1;
		if (Util.isEmpty(keyName, true))
			return airLine;
		List<Object> caches = l_cache.get(KEY_AIRLINE);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_AIRLINE, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(keyName))
			{
				airLine = pair.getKey();
				break;
			}
		}
		
		if (airLine == -1)
		{
			String sql = "SELECT UNS_AirLine_ID FROM UNS_AirLine WHERE Value = ? OR Name = ? ORDER BY IsActive DESC";
			airLine = DB.getSQLValue(get_TrxName(), sql, keyName, keyName);
			if (airLine != -1)
				caches.add(new KeyNamePair(airLine, keyName));
		}
		
		return airLine;
	}
	
	private int initCountryID (String isoCode3)
	{
		int countryID = -1;
		if (Util.isEmpty(isoCode3, true))
			return countryID;
		List<Object> caches = l_cache.get(KEY_COUNTRY);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_COUNTRY, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(isoCode3))
			{
				countryID = pair.getKey();
				break;
			}
		}
		
		if (countryID == -1)
		{
			String sql = "SELECT C_Country_ID FROM C_Country WHERE CountryCode3 = ? ORDER BY IsActive DESC";
			countryID = DB.getSQLValue(get_TrxName(), sql, isoCode3);
			if (countryID == -1)
				countryID = DB.getSQLValue(get_TrxName(), sql, "OTH");
			if (countryID != -1)
				caches.add(new KeyNamePair(countryID, isoCode3));
		}
		
		return countryID;
	}
	
	
	private MUNSPOSTrxLine initLine (MUNSPOSTrx trx, MProduct product, String etbb)
	{
		MUNSPOSTrxLine[] lines = trx.getLines(false);
		MUNSPOSTrxLine line = null;
		for (int i=0; i<lines.length; i++)
		{
			if (lines[i].getM_Product_ID() == product.get_ID() && lines[i].getETBBCode().equals(etbb))
			{
				line = lines[i];
				break;
			}
		}
		
		if (line == null)
		{
			line = new MUNSPOSTrxLine(trx);
			line.setM_Product_ID(product.get_ID()); 
			line.setC_UOM_ID(product.getC_UOM_ID()); 
			line.setETBBCode(etbb);
			line.setSKU(product.getSKU());
			String bc = line.getSKU() + line.getETBBCode();
			line.setBarcode(bc);
			line.setPrice(Env.ZERO);
			line.setQtyOrdered(Env.ZERO);
			line.setQtyEntered(Env.ZERO);
			trx.addLines(line);
		}
		
		return line;
	}
	
	private MUNSPOSSession initSession (int orgID, int cashierID, String shift, Timestamp dateAcct,
			String desc) throws Exception
	{
		MUNSPOSSession session = null;
		for (int i=0; i<c_session.size(); i++)
		{
			if (c_session.get(i).getCashier_ID() == cashierID && c_session.get(i).getShift().equals(shift)
					&& c_session.get(i).getDateAcct().equals(dateAcct))
			{
				session = c_session.get(i);
				break;
			}
		}
		
		if (session == null)
		{
			session = loadSessionFromDB(orgID, cashierID, shift, dateAcct);
			if (session != null)
			{
				MUNSPOSTrx[] trxs = session.getLines(false);
				for (int i=0; i<trxs.length; i++)
				{
					MUNSPOSTrxLine[] lines = trxs[i].getLines(false);
					for (int j=0;j< lines.length; j++) 
					{
						lines[j].setQtyEntered(Env.ZERO);
						lines[j].setQtyOrdered(Env.ZERO);
						lines[j].setDiscountAmt(Env.ZERO);
						lines[j].saveEx();
					}
				}
				c_session.add(session);
			}
		}
		
		if (session == null)
		{
			session = new MUNSPOSSession(getCtx(), 0, get_TrxName());
			session.setAD_Org_ID(orgID);
			session.setDateAcct(dateAcct);
			session.setDateDoc(dateAcct);
			session.setCashier_ID(cashierID);
			session.setShift(shift);
			session.setDescription(desc);
			session.setProcessed(true);
			session.saveEx();
			DB.commit(true, get_TrxName());
			c_session.add(session);
		}
		
		if (!session.getDocStatus().equals("IP"))
		{
			if (!session.processIt(DocAction.ACTION_Prepare))
				return null;
			session.saveEx();
		}
		
		return session;
	}
	
	private MUNSPOSSession loadSessionFromDB (int orgID, int cashierID, String shift, Timestamp dateAcct)
	{
		StringBuilder where = new StringBuilder(MUNSPOSSession.COLUMNNAME_Cashier_ID).append("= ? AND ")
				.append(MUNSPOSSession.COLUMNNAME_Shift).append("= ? AND ")
				.append(MUNSPOSSession.COLUMNNAME_DateAcct).append("=? AND ")
				.append(MUNSPOSSession.COLUMNNAME_DocStatus).append("=? AND AD_Org_ID = ? ");
		String wc = where.toString();
		MUNSPOSSession session = Query.get(
				getCtx(), 
				UNSOrderModelFactory.EXTENSION_ID, 
				MUNSPOSSession.Table_Name, wc, get_TrxName()).
				setParameters(cashierID, shift, dateAcct, "IP", orgID).
				firstOnly();
		return session;
	}
	
	private MUNSPOSTrx initPOSTrx (MUNSPOSSession session, String trxNo, int salesID, 
			int shopID, int locID, int whsID, Timestamp dateTrx, boolean isRefund)
	{
		MUNSPOSTrx[] trxs = session.getLines(false);
		MUNSPOSTrx trx = null;
		for (int i=0; i<trxs.length; i++)
		{
			if (trxs[i].getUNS_POS_Session_ID() == session.get_ID() 
					&& trxs[i].getTrxNo().equals(trxNo) 
					&& trxs[i].getSalesRep_ID() == salesID)
			{
				trx = trxs[i];
				break;
			}
		}
		if (trx == null)
		{
			trx = new MUNSPOSTrx(session);
			trx.setTrxNo(trxNo);
			trx.setDocumentNo(trxNo);
			trx.setC_BPartner_ID(shopID);
			trx.setC_BPartner_Location_ID(locID);
			trx.setM_Warehouse_ID(whsID);
			trx.setSalesRep_ID(salesID);
			String docBaseType = MDocType.DOCBASETYPE_POSSales;
			if (isRefund)
			{
				docBaseType = MDocType.DOCBASETYPE_POSReturn;
			}
			String sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType = ?";
			int doctypeID = DB.getSQLValue(get_TrxName(), sql, docBaseType);
			trx.setC_DocType_ID(doctypeID);
			trx.setDateAcct(session.getDateAcct());
			trx.setDateDoc(session.getDateDoc());
			trx.setDateTrx(dateTrx);
			trx.setGrandTotal(Env.ZERO);
			trx.setTotalAmt(Env.ZERO);
			trx.setDiscount(Env.ZERO);
			trx.setDiscountAmt(Env.ZERO);
			trx.setDocAction("CO");
			trx.setDocStatus("DR");
			trx.setIsActive(true);
			trx.setDescription("**");
			trx.saveEx();
			session.addLine(trx);
		}
		
		return trx;
	}
	
	private int initShopLocation (String shopIDStr)
	{
		int locationID = -1;
		if (Util.isEmpty(shopIDStr, true))
			return locationID;
		List<Object> caches = l_cache.get(KEY_SHOP_LOCATION);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_SHOP_LOCATION, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(shopIDStr))
			{
				locationID = pair.getKey();
				break;
			}
		}
		
		if (locationID == -1)
		{
			String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE C_BPartner_ID = ? ORDER BY IsActive DESC";
			locationID = DB.getSQLValue(get_TrxName(), sql, Integer.valueOf(shopIDStr));
			if (locationID != -1)
				caches.add(new KeyNamePair(locationID, shopIDStr));
		}
		
		return locationID;
	}
	
	private int initDiscReff (String value)
	{
		int discReffID = -1;
		if (Util.isEmpty(value, true))
			return discReffID;
		List<Object> caches = l_cache.get(KEY_DISC_REFF);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_DISC_REFF, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(value))
			{
				discReffID = pair.getKey();
				break;
			}
		}
		
		if (discReffID == -1)
		{
			String sql = "SELECT UNS_DiscountReff_ID FROM UNS_DiscountReff WHERE Value = ? AND AD_Client_ID = ?";
			discReffID = DB.getSQLValue(get_TrxName(), sql, value, getAD_Client_ID());
			if (discReffID != -1)
				caches.add(new KeyNamePair(discReffID, value));
		}
		
		return discReffID;
	}
	
	private int initUserID (String name)
	{
		int userID = -1;
		if (Util.isEmpty(name, true))
			return userID;
		List<Object> caches = l_cache.get(KEY_USER);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_USER, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(name))
			{
				userID = pair.getKey();
				break;
			}
		}
		
		if (userID == -1)
		{
			String sql = "SELECT AD_User_ID FROM AD_User WHERE Name = ?";
			userID = DB.getSQLValue(get_TrxName(), sql, name);
			if (userID != -1)
				caches.add(new KeyNamePair(userID, name));
		}
		
		return userID;
	}
	
	private int initCurrencyID (String currISO)
	{
		int currID = -1;
		if (Util.isEmpty(currISO, true))
			return currID;
		List<Object> caches = l_cache.get(KEY_CURRENCY);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_CURRENCY, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(currISO))
			{
				currID = pair.getKey();
				break;
			}
		}
		
		if (currID == -1)
		{
			String sql = "SELECT C_Currency_ID FROM C_Currency WHERE ISO_Code = ?";
			currID = DB.getSQLValue(get_TrxName(), sql, currISO);
			if (currID != -1)
				caches.add(new KeyNamePair(currID, currISO));
		}
		
		return currID;
	}
	
	private int initOrgID (String orgValue)
	{
		int orgID = -1;
		if (Util.isEmpty(orgValue, true))
			return orgID;
		List<Object> caches = l_cache.get(KEY_ORGANIZATION);
		if (caches == null)
		{
			caches = new ArrayList<>();
			l_cache.put(KEY_ORGANIZATION, caches);
		}
		for (int i=0; i<caches.size(); i++)
		{
			if (caches.get(i) instanceof KeyNamePair == false)
				continue;
			KeyNamePair pair = (KeyNamePair)caches.get(i);
			if (pair.getName().equals(orgValue))
			{
				orgID = pair.getKey();
				break;
			}
		}
		
		if (orgID == -1)
		{
			String sql = "SELECT AD_Org_ID FROM AD_Org WHERE Value = ?";
			orgID = DB.getSQLValue(get_TrxName(), sql, orgValue);
			if (orgID != -1)
				caches.add(new KeyNamePair(orgID, orgValue));
		}
		
		return orgID;
	}
	
	/**
	 * 
	 * @param sku
	 * @return
	 */
	private MProduct initProduct (String sku)
	{
		MProduct product = c_product.get(sku);
		if (product == null)
		{
			product = MProduct.getOfSKU(getCtx(), sku, get_TrxName());
			if (product != null)
				c_product.put(sku, product);
		}
		return product;
	}
}

