/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.unicore.model.MUNSOthersCustomer;
import com.unicore.model.MUNSPreOrder;

/**
 * @author ALBURHANY
 *
 */
public class CreateOthersCustomer extends SvrProcess {
	
	private String m_Name;
	private String m_Address;
	private String m_Phone;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("Name"))
				m_Name = param.getParameterAsString();
			else if(param.getParameterName().equals("Address"))
				m_Address = param.getParameterAsString();
			else if(param.getParameterName().equals("Phone"))
				m_Phone = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
		MUNSPreOrder pOrder = new MUNSPreOrder(getCtx(), getRecord_ID(), get_TrxName());
		MUNSOthersCustomer oCustomer = new MUNSOthersCustomer(getCtx(), 0, get_TrxName());
		
		oCustomer.setAD_Org_ID(pOrder.getAD_Org_ID());
		oCustomer.setDateDoc(pOrder.getDateDoc());
		oCustomer.setC_BPartner_ID(pOrder.getC_BPartner_ID());
		oCustomer.setName(m_Name);
		oCustomer.setAddress(m_Address);
		oCustomer.setPhone(m_Phone);
		oCustomer.setDateDoc(pOrder.getDateDoc());
		oCustomer.setIsActive(true);
		oCustomer.saveEx();
		
		pOrder.setUNS_OthersCustomer_ID(oCustomer.get_ID());
		
		if(m_Phone != null)
			m_Name = m_Name + " - " + oCustomer.getPhone();
		pOrder.setAttName(m_Name);
		pOrder.setAttAddress(oCustomer.getAddress());
		pOrder.saveEx();
		
		return null;
	}
}
