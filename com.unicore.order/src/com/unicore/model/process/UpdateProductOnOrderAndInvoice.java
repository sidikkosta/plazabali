/**
 * 
 */
package com.unicore.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MInvoice;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MOrder;

/**
 * @author Burhani Adam
 *
 */
public class UpdateProductOnOrderAndInvoice extends SvrProcess {

	/**
	 * 
	 */
	public UpdateProductOnOrderAndInvoice() {
		// TODO Auto-generated constructor stub
	}
	
	int C_OrderLine_ID = 0;
	int M_Product_ID = 0;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName() == null)
				;
			else if(param.getParameterName().equals("C_OrderLine_ID"))
				C_OrderLine_ID = param.getParameterAsInt();
			else if(param.getParameterName().equals("M_Product_ID"))
				M_Product_ID = param.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if(C_OrderLine_ID <= 0 || M_Product_ID <= 0)
			return "";
		
		MOrder order = new MOrder(getCtx(), getRecord_ID(), get_TrxName());
		String sql = "SELECT COUNT(pp.*) FROM M_ProductPrice pp"
				+ " INNER JOIN M_PriceList_Version plv ON"
				+ " plv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID"
				+ " WHERE pp.M_Product_ID = ? AND plv.ValidFrom <= ?"
				+ " AND plv.M_PriceList_ID = ? AND pp.IsActive = 'Y'";
		boolean valid = DB.getSQLValue(get_TrxName(), sql, new Object[]
				{M_Product_ID, order.getDateOrdered(), order.getM_PriceList_ID()}) > 0;
		
		if(!valid)
			throw new AdempiereException("Product is not on price list");

		BigDecimal QtyDelivered = DB.getSQLValueBD(get_TrxName(), 
				"SELECT QtyDelivered FROM C_OrderLine WHERE C_OrderLine_ID = ?", C_OrderLine_ID);
		if(QtyDelivered.compareTo(Env.ZERO) == 1)
			throw new AdempiereException("This action can not be continued,"
					+ " because there is already a quantity delivery");
		
		BigDecimal QtyPacked = DB.getSQLValueBD(get_TrxName(), 
				"SELECT QtyPacked FROM C_OrderLine WHERE C_OrderLine_ID = ?", C_OrderLine_ID);
		if(QtyPacked.compareTo(Env.ZERO) == 1)
			throw new AdempiereException("This action can not be continued,"
					+ " because there is already a quantity packed");
		
		String upOrderLine = "UPDATE C_OrderLine SET M_Product_ID = ? WHERE C_OrderLine_ID = ?";
		DB.executeUpdate(upOrderLine, new Object[]{M_Product_ID, C_OrderLine_ID}, false, get_TrxName());
		String upInvLine = "UPDATE C_InvoiceLine SET M_Product_ID = ? WHERE C_OrderLine_ID = ?";
		DB.executeUpdate(upInvLine, new Object[]{M_Product_ID, C_OrderLine_ID}, false, get_TrxName());
		
		sql = "SELECT C_Invoice_ID FROM C_InvoiceLine WHERE C_OrderLine_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, C_OrderLine_ID);
			rs = stmt.executeQuery();
			while (rs.next())
			{
				DocManager.postDocument(MAcctSchema.getClientAcctSchema(getCtx(), order.getAD_Client_ID(), get_TrxName()),
						MInvoice.Table_ID, rs.getInt(1), true, true, get_TrxName());
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		return "Success update line";
	}
}
