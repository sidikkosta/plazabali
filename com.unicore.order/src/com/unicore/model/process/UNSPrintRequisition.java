/**
 * 
 */
package com.unicore.model.process;

import org.compiere.model.MRequisition;
import org.compiere.process.SvrProcess;

/**
 * @author Burhani Adam
 *
 */
public class UNSPrintRequisition extends SvrProcess {

	/**
	 * 
	 */
	public UNSPrintRequisition() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MRequisition req = new MRequisition(getCtx(), getRecord_ID(), get_TrxName());
		if(req.isProcessed())
		{
			req.setIsPrinted(true);
			req.saveEx();
		}
		return "";
	}
}