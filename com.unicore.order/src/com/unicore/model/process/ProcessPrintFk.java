/**
 * 
 */
package com.unicore.model.process;

import org.compiere.model.MInvoice;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;

import com.unicore.model.MUNSPLConfirm;
import com.unicore.model.MUNSPrintingLog;

/**
 * @author ALBURHANY
 *
 */
public class ProcessPrintFk extends SvrProcess {
	
	private String m_Type = null;
	private String[] value;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
		if(getTable_ID() == MUNSPLConfirm.Table_ID)
		{
			m_Type = "FkPLC";
			String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(C_Invoice_ID), ';') FROM C_Invoice WHERE DocStatus IN ('CO', 'CL')"
					+ " AND C_Invoice_ID IN (SELECT C_Invoice_ID FROM UNS_PackingList_Order"
					+ " WHERE UNS_PackingList_ID IN (SELECT UNS_PackingList_ID FROM UNS_PL_Confirm"
					+ " WHERE UNS_PL_Confirm_ID = " + getRecord_ID() + "))";
			String values = DB.getSQLValueString(get_TrxName(), sql);
			
			if(values == null)
				throw new AdempiereUserError("Invoice NOT Found");
			
			value = values.split(";");
			
			for(String idString : value)
			{
				Integer idAsInt = new Integer(idString);
				MInvoice inv = new MInvoice(getCtx(), idAsInt, get_TrxName());
				
				inv.setIsPaid(true);
				inv.saveEx();
				
				MUNSPrintingLog pLog = new MUNSPrintingLog(getCtx(), 0, get_TrxName());
				pLog.create(getCtx(), getRecord_ID(), getProcessInfo().getAD_User_ID(), m_Type, inv.getDocumentNo(),
						get_TrxName());
			}
		}
		
		return null;
	}

}
