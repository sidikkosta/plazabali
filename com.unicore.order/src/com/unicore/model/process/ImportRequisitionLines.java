/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.base.model.MRequisition;
import com.unicore.base.model.MRequisitionLine;
import com.unicore.model.MUNSImportSimpleColumn;
import com.unicore.model.MUNSImportSimpleTable;
import com.unicore.model.MUNSImportSimpleXLS;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Burhani Adam
 *
 */
public class ImportRequisitionLines extends SvrProcess {

	/**
	 * 
	 */
	public ImportRequisitionLines() {
		// TODO Auto-generated constructor stub
	}
	
	private boolean DeleteOld = false;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOld"))
				DeleteOld = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window Order");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MRequisition req = new MRequisition(getCtx(), getRecord_ID(), get_TrxName());
		if(req.isProcessed())
			throw new AdempiereException("Document has processed.");
		
		if(DeleteOld)
		{
			String sql = "DELETE FROM M_RequisitionLine WHERE M_Requisition_ID = ?";
			DB.executeUpdate(sql, req.get_ID(), get_TrxName());
		}
		req.setImportLines("Y");
		
		if(Util.isEmpty(req.getFile_Directory(), true))
			throw new AdempiereException("To run this process, you must fill the file with the correct directory / path.");
		
		String sql = "SELECT UNS_ImportSimpleXLS_ID FROM UNS_ImportSimpleXLS WHERE Name = 'ImportRequisition'";
		int id = DB.getSQLValue(get_TrxName(), sql);
		if(id <= 0)
			throw new AdempiereException("Not found Simple Import XLS with ImportRequisition name");
		
		MUNSImportSimpleXLS simple = new MUNSImportSimpleXLS(getCtx(), id, get_TrxName());
		simple.setFile_Directory(req.getFile_Directory());
		simple.saveEx();
		
		MUNSImportSimpleTable tables[] = simple.getLines(true);
		
		if(tables.length > 1)
			throw new AdempiereException("more than one lines in Import Simple XLS " + simple.getName());
		
		MUNSImportSimpleTable table = tables[0];
		MUNSImportSimpleColumn[] columns = table.getLines(true);
		MUNSImportSimpleColumn column = null;
		for(int i=0;i<columns.length;i++)
		{
			if(columns[i].getAD_Column().getColumnName().equals("M_Requisition_ID"))
			{
				column = columns[i];
				break;
			}
		}
		
		if(column == null)
		{
			column = new MUNSImportSimpleColumn(getCtx(), 0, get_TrxName());
			column.setUNS_ImportSimpleTable_ID(table.get_ID());
			column.setName("UNT");
			column.setAD_Column_ID(MColumn.getColumn_ID(MRequisitionLine.Table_Name, "M_Requisition_ID"));
			column.setColumnNo(6969);
			column.setAD_Reference_ID(11);
			column.setIsEmptyCell(true);
		}
		
		column.setDefaultValue(String.valueOf(req.get_ID()));
		column.saveEx();
		
		MUNSImportSimpleColumn columnOrg = null;
		for(int i=0;i<columns.length;i++)
		{
			if(columns[i].getAD_Column().getColumnName().equals("AD_Org_ID"))
			{
				columnOrg = columns[i];
				break;
			}
		}
		
		if(columnOrg == null)
		{
			columnOrg = new MUNSImportSimpleColumn(getCtx(), 0, get_TrxName());
			columnOrg.setUNS_ImportSimpleTable_ID(table.get_ID());
			columnOrg.setName("UNS");
			columnOrg.setAD_Column_ID(MColumn.getColumn_ID(MRequisitionLine.Table_Name, "AD_Org_ID"));
			columnOrg.setColumnNo(7171);
			columnOrg.setAD_Reference_ID(11);
			columnOrg.setIsEmptyCell(true);
		}
		
		columnOrg.setDefaultValue(String.valueOf(req.getAD_Org_ID()));
		columnOrg.saveEx();
		
		table.getLines(true);
		
		SimpleImportXLS si = new SimpleImportXLS(getCtx(), simple, table, false, 0, 0, 0, getAD_Client_ID(), get_TrxName());;
		String success = null;
		try{
			 success = si.doIt();
		}
		catch (Exception e)
		{
			throw new AdempiereException(CLogger.retrieveErrorString(e.getMessage()));
		}
		
		column.deleteEx(true);
		columnOrg.deleteEx(true);
		
		return success;
	}
}