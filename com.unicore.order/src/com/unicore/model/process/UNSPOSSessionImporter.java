/**
 * 
 */
package com.unicore.model.process;

import java.util.logging.Level;
import org.adempiere.process.rpl.XMLHelper;
import org.adempiere.process.rpl.imp.ImportHelper;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import org.w3c.dom.Document;

/**
 * @author nurse
 *
 */
public class UNSPOSSessionImporter extends SvrProcess {

	private String p_fileName;
	
	/**
	 * 
	 */
	public UNSPOSSessionImporter() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else 
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		StringBuffer result = new StringBuffer("");
		Document documentToBeImported = XMLHelper.createDocumentFromFile(p_fileName);
		ImportHelper impHelper = new ImportHelper(getCtx());
		impHelper.importXMLDocument(result, documentToBeImported, get_TrxName());
		addLog(0, null, null, Msg.getMsg(getCtx(), "ImportModelProcessResult") + "\n" + result.toString());
		return result.toString();
	}

}
