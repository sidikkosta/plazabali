/**
 * 
 */
package com.unicore.model.process;

import java.util.Properties;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.Env;

import com.unicore.model.MUNSAccessoriesLine;
import com.unicore.model.MUNSProductAccessories;

/**
 * @author AlfiRahmaa
 *
 */
public class CopyFromAccessories extends SvrProcess {
	
//	private int p_Record_ID = 0;
	private int p_UNS_ProductAccessories_ID = 0;
//	private int no = 0;
	private Properties ctx = Env.getCtx();

	/**
	 * 
	 */
	public CopyFromAccessories() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] params = getParameter();
		for (int i = 0; i < params.length; i++) 
		{
			String name = params[i].getParameterName();
			if (params[i].getParameter() == null);
			else if ("UNS_ProductAccessories_ID".equals(name))
			{
				p_UNS_ProductAccessories_ID = params[i].getParameterAsInt();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
//		p_Record_ID = getRecord_ID();

	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		
		MUNSProductAccessories fromAccs = new MUNSProductAccessories(ctx, p_UNS_ProductAccessories_ID, get_TrxName());
		MUNSProductAccessories toAccs = new MUNSProductAccessories(ctx, p_UNS_ProductAccessories_ID, get_TrxName());
		if (toAccs.getLines().length > 0)
		{
			throw new AdempiereSystemError("@Error@ Existing Accessories Line(s)");
		}
		
		MUNSAccessoriesLine[] fromAccsLines = fromAccs.getLines();
		for (MUNSAccessoriesLine fromAccsLine : fromAccsLines)
		{
			MUNSAccessoriesLine toAccsLine = new MUNSAccessoriesLine(ctx, 0, get_TrxName());
			MUNSAccessoriesLine.copyValues(fromAccsLine, toAccsLine);
			toAccsLine.setUNS_ProductAccessories_ID(toAccs.getUNS_ProductAccessories_ID());
			toAccsLine.saveEx();
//			++no;
		}
		
		// TODO Auto-generated method stub
		return "OK";
	}

}
