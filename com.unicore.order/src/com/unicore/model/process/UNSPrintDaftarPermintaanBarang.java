package com.unicore.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MRequisition;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

public class UNSPrintDaftarPermintaanBarang extends SvrProcess {

	public UNSPrintDaftarPermintaanBarang() {
		
	}

	@Override
	protected void prepare() {
		
	}

	@Override
	protected String doIt() throws Exception {

		MRequisition m_req = new MRequisition(getCtx(), getRecord_ID(), get_TrxName());
		m_req.setIsPrinted(true);
		m_req.setPrintDaftarPermintaanBarang(Env.getContext(getCtx(), "#AD_User_Name"));
		if(!m_req.save())
			throw new AdempiereException("Error when try update column isprinted");
		
		return null;
	}

}
