/**
 * 
 */
package com.unicore.model.process;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.util.IProcessUI;

import jxl.Cell;
import jxl.Sheet;

/**
 * @author nurse
 *
 */
public class POSImportRawData 
{
	private static int COUNT = 0;
	public static final int COLUMN_ORG = COUNT++;
	public static final int COLUMN_DOCUMENTNO = COUNT++;
	public static final int COLUMN_TRXNO = COUNT++;
	public static final int COLUMN_SHIFT = COUNT++;
	public static final int COLUMN_CURRENCY = COUNT++;
	public static final int COLUMN_DATEACCT = COUNT++;
	public static final int COLUMN_DATETRX = COUNT++;
	public static final int COLUMN_DISCOUNTTOTALAMT = COUNT++;
	public static final int COLUMN_CASHIER = COUNT++;
	public static final int COLUMN_SALES = COUNT++;
	public static final int COLUMN_DISCREFF = COUNT++;
	public static final int COLUMN_CUSTOMERNAME = COUNT++;
	public static final int COLUMN_COUNTRY = COUNT++;
	public static final int COLUMN_CUSTOMERTYPE = COUNT++;
	public static final int COLUMN_FLIGHTNO = COUNT++;
	public static final int COLUMN_NIK = COUNT++;
	public static final int COLUMN_PASPORTNO = COUNT++;
	public static final int COLUMN_VIPTYPE = COUNT++;
	public static final int COLUMN_PASSENGERTYPE = COUNT++;
	public static final int COLUMN_BOARDINGPASS = COUNT++;
	public static final int COLUMN_AIRLINE = COUNT++;
	public static final int COLUMN_GATENO = COUNT++;
	public static final int COLUMN_SKU = COUNT++;
	public static final int COLUMN_ETBB = COUNT++;
	public static final int COLUMN_QTY = COUNT++;
	public static final int COLUMN_PRICE = COUNT++;
	public static final int COLUMN_DISCOUNTLINEAMT = COUNT++;
	public static final int COLUMN_DISCOUNTREFFLINE = COUNT++;
	public static final int COLUMN_DESCRIPTION = COUNT++;
	private List<String[]> m_rows;
	private Sheet m_sheet;
	
	public POSImportRawData (Sheet sheet)
	{
		m_rows = new ArrayList<>();
		m_sheet = sheet;
	}

	public List<String[]> parseValues (IProcessUI ui, int rowstart, int rowend) throws Exception
	{
		if (rowstart == -1)
			rowstart = 1;
		if (rowend <= 0)
			rowend = m_sheet.getRows();
		for (int i=rowstart; i<rowend; i++)
		{
			ui.statusUpdate("Parsing record from excel [" + i + " from " + rowend + "]");
			m_rows.add(parseRow(i));
		}
		
		return m_rows;
	}
	
	private String[] parseRow (int rowidx) throws Exception
	{
		String[] row = new String[COUNT];
		for (int i=0; i<row.length; i++)
		{
			Cell cell = m_sheet.getCell(i, rowidx);
			String content = cell.getContents();
			if (content == null)
				content = "";
			row[i] = content.trim();
		}
		return row;
	}
}
