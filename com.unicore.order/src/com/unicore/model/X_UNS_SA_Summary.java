/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_SA_Summary
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_SA_Summary extends PO implements I_UNS_SA_Summary, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191129L;

    /** Standard Constructor */
    public X_UNS_SA_Summary (Properties ctx, int UNS_SA_Summary_ID, String trxName)
    {
      super (ctx, UNS_SA_Summary_ID, trxName);
      /** if (UNS_SA_Summary_ID == 0)
        {
			setC_DocType_ID (0);
			setC_Period_ID (0);
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
			setDateFrom (new Timestamp( System.currentTimeMillis() ));
			setDateTo (new Timestamp( System.currentTimeMillis() ));
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setGenerateList (null);
// N
			setIsApproved (false);
// N
			setProcessed (false);
// N
			setShopType (null);
			setTotalLines (Env.ZERO);
// 0
			setUNS_SA_Summary_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_SA_Summary (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_SA_Summary[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Payment getC_Payment() throws RuntimeException
    {
		return (org.compiere.model.I_C_Payment)MTable.get(getCtx(), org.compiere.model.I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_Value (COLUMNNAME_C_Payment_ID, null);
		else 
			set_Value (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create Payment.
		@param CreatePayment Create Payment	  */
	public void setCreatePayment (String CreatePayment)
	{
		set_Value (COLUMNNAME_CreatePayment, CreatePayment);
	}

	/** Get Create Payment.
		@return Create Payment	  */
	public String getCreatePayment () 
	{
		return (String)get_Value(COLUMNNAME_CreatePayment);
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_Value (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Date From.
		@param DateFrom 
		Starting date for a range
	  */
	public void setDateFrom (Timestamp DateFrom)
	{
		set_Value (COLUMNNAME_DateFrom, DateFrom);
	}

	/** Get Date From.
		@return Starting date for a range
	  */
	public Timestamp getDateFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFrom);
	}

	/** Set Date To.
		@param DateTo 
		End date of a date range
	  */
	public void setDateTo (Timestamp DateTo)
	{
		set_Value (COLUMNNAME_DateTo, DateTo);
	}

	/** Get Date To.
		@return End date of a date range
	  */
	public Timestamp getDateTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTo);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	public org.compiere.model.I_C_DocType getDocTypeInvoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getDocTypeInvoice_ID(), get_TrxName());	}

	/** Set DocType Target Invoice.
		@param DocTypeInvoice_ID DocType Target Invoice	  */
	public void setDocTypeInvoice_ID (int DocTypeInvoice_ID)
	{
		if (DocTypeInvoice_ID < 1) 
			set_Value (COLUMNNAME_DocTypeInvoice_ID, null);
		else 
			set_Value (COLUMNNAME_DocTypeInvoice_ID, Integer.valueOf(DocTypeInvoice_ID));
	}

	/** Get DocType Target Invoice.
		@return DocType Target Invoice	  */
	public int getDocTypeInvoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DocTypeInvoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Generate List.
		@param GenerateList 
		Generate List
	  */
	public void setGenerateList (String GenerateList)
	{
		set_Value (COLUMNNAME_GenerateList, GenerateList);
	}

	/** Get Generate List.
		@return Generate List
	  */
	public String getGenerateList () 
	{
		return (String)get_Value(COLUMNNAME_GenerateList);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Print History Status Report Per Supplier.
		@param PrintHistoryStatusReportPerSup Print History Status Report Per Supplier	  */
	public void setPrintHistoryStatusReportPerSup (String PrintHistoryStatusReportPerSup)
	{
		set_Value (COLUMNNAME_PrintHistoryStatusReportPerSup, PrintHistoryStatusReportPerSup);
	}

	/** Get Print History Status Report Per Supplier.
		@return Print History Status Report Per Supplier	  */
	public String getPrintHistoryStatusReportPerSup () 
	{
		return (String)get_Value(COLUMNNAME_PrintHistoryStatusReportPerSup);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Report - Summary Push Money By Product.
		@param ReportSummaryPMoneyByProduct Report - Summary Push Money By Product	  */
	public void setReportSummaryPMoneyByProduct (String ReportSummaryPMoneyByProduct)
	{
		set_Value (COLUMNNAME_ReportSummaryPMoneyByProduct, ReportSummaryPMoneyByProduct);
	}

	/** Get Report - Summary Push Money By Product.
		@return Report - Summary Push Money By Product	  */
	public String getReportSummaryPMoneyByProduct () 
	{
		return (String)get_Value(COLUMNNAME_ReportSummaryPMoneyByProduct);
	}

	/** Bazaar = BZ */
	public static final String SHOPTYPE_Bazaar = "BZ";
	/** Shop Retail Duty Free = DF */
	public static final String SHOPTYPE_ShopRetailDutyFree = "DF";
	/** Shop Retail Duty Paid = DP */
	public static final String SHOPTYPE_ShopRetailDutyPaid = "DP";
	/** Shop F&B = FB */
	public static final String SHOPTYPE_ShopFB = "FB";
	/** Online = OL */
	public static final String SHOPTYPE_Online = "OL";
	/** Set Shop Type.
		@param ShopType Shop Type	  */
	public void setShopType (String ShopType)
	{

		set_ValueNoCheck (COLUMNNAME_ShopType, ShopType);
	}

	/** Get Shop Type.
		@return Shop Type	  */
	public String getShopType () 
	{
		return (String)get_Value(COLUMNNAME_ShopType);
	}

	/** Set Print Summary SA.
		@param summarysa Print Summary SA	  */
	public void setsummarysa (String summarysa)
	{
		set_Value (COLUMNNAME_summarysa, summarysa);
	}

	/** Get Print Summary SA.
		@return Print Summary SA	  */
	public String getsummarysa () 
	{
		return (String)get_Value(COLUMNNAME_summarysa);
	}

	/** Set Total Lines.
		@param TotalLines 
		Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines)
	{
		set_Value (COLUMNNAME_TotalLines, TotalLines);
	}

	/** Get Total Lines.
		@return Total of all document lines
	  */
	public BigDecimal getTotalLines () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLines);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Division.
		@param UNS_Division_ID Division	  */
	public void setUNS_Division_ID (int UNS_Division_ID)
	{
		if (UNS_Division_ID < 1) 
			set_Value (COLUMNNAME_UNS_Division_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Division_ID, Integer.valueOf(UNS_Division_ID));
	}

	/** Get Division.
		@return Division	  */
	public int getUNS_Division_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Division_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Statement Of Account (Summary Period).
		@param UNS_SA_Summary_ID Statement Of Account (Summary Period)	  */
	public void setUNS_SA_Summary_ID (int UNS_SA_Summary_ID)
	{
		if (UNS_SA_Summary_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SA_Summary_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SA_Summary_ID, Integer.valueOf(UNS_SA_Summary_ID));
	}

	/** Get Statement Of Account (Summary Period).
		@return Statement Of Account (Summary Period)	  */
	public int getUNS_SA_Summary_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SA_Summary_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_SA_Summary_UU.
		@param UNS_SA_Summary_UU UNS_SA_Summary_UU	  */
	public void setUNS_SA_Summary_UU (String UNS_SA_Summary_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_SA_Summary_UU, UNS_SA_Summary_UU);
	}

	/** Get UNS_SA_Summary_UU.
		@return UNS_SA_Summary_UU	  */
	public String getUNS_SA_Summary_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_SA_Summary_UU);
	}
}