/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

//import org.compiere.model.MInvoice;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.util.MessageBox;

/**
 * @author ALBURHANY
 *
 */
public class MUNSPreOrderLine extends X_UNS_PreOrder_Line {

	String m_message = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = -4451565112722116825L;

	/**
	 * @param ctx
	 * @param UNS_PreOrder_Line_ID
	 * @param trxName
	 */
	public MUNSPreOrderLine(Properties ctx, int UNS_PreOrder_Line_ID,
			String trxName) {
		super(ctx, UNS_PreOrder_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPreOrderLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean beforeSave (boolean newRecord)
	{
		setC_UOM_ID(getM_Product().getC_UOM_ID());
		
		if(is_ValueChanged(COLUMNNAME_Discount) || is_ValueChanged(COLUMNNAME_DiscountAmt)
				|| is_ValueChanged(COLUMNNAME_PriceEntered) || is_ValueChanged(COLUMNNAME_SettingPriceType))
			if(getUNS_PreOrder().getDocStatus().equals("CL"))
			{
				m_message = "Cannot edit price, document has processed. Contact Sales Order Admin.";
				throw new AdempiereUserError(m_message);
			}
		
		if(is_ValueChanged(COLUMNNAME_SettingPriceType))
		{
			if(getSettingPriceType().equals("DP"))
			{
				setDiscount(Env.ZERO);
				setDiscountAmt(Env.ZERO);
				setPriceEntered(Env.ZERO);
			}
			
			if(getSettingPriceType().equals("MWD"))
				setPriceEntered(Env.ZERO);
			
			if(getSettingPriceType().equals("MWP"))
			{
				setDiscount(Env.ZERO);
				setDiscountAmt(Env.ZERO);
			}
		}
		
		if(newRecord || is_ValueChanged("M_Product_ID"))
		{
			String sql = "SELECT 1 FROM UNS_PreOrder_Line WHERE M_Product_ID = " + getM_Product_ID()
						+ " AND UNS_PreOrder_ID = " + getUNS_PreOrder_ID() + " AND UNS_PreOrder_Line_ID <> " + get_ID();
			if (DB.getSQLValue(get_TrxName(), sql) >= 1)
			{	
				int retVal = MessageBox.showMsg(this, getCtx()
						, "Duplicate product in one Pre Order. \n" +
						  "Do you want to continue this action ?"
						, "Duplicate product confirmation"
						, MessageBox.YESNO
						, MessageBox.ICONQUESTION);
				if(retVal == MessageBox.RETURN_YES)
				{
					if(Util.isEmpty(getDescription()))
					{
						m_message = "for duplicate product in one pre order, you must give description";
						throw new AdempiereUserError(m_message);
					}
				}
				if(retVal == MessageBox.RETURN_NO || retVal == MessageBox.RETURN_CANCEL)
				{
					m_message = "failed when trying save this document with duplicate product";
					throw new AdempiereUserError(m_message);
				}
			}
		}
		
		if(getQty().signum() <= 0)
		{
			m_message = "Please update quantity (quantity cannot 0 OR negate)";
			throw new AdempiereUserError(m_message);
		}
		
		if(getPriceEntered().compareTo(Env.ZERO) != 0 && getDiscountAmt().compareTo(Env.ZERO) != 0
				|| getPriceEntered().compareTo(Env.ZERO) != 0 && getDiscount().compareTo(Env.ZERO) != 0
					|| getDiscountAmt().compareTo(Env.ZERO) != 0 && getDiscount().compareTo(Env.ZERO) != 0)
		{
			m_message = "Please choice between Price / Discount / Discount Amount";
			throw new AdempiereUserError(m_message);
		}
		
		if(getDiscount().compareTo(Env.ONEHUNDRED) == 1)
		{
			m_message = "Discount % cannot greater than 100";
			throw new AdempiereUserError(m_message);
		}
		
		return true;
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		if(newRecord || is_ValueChanged(COLUMNNAME_M_Product_ID) || is_ValueChanged(COLUMNNAME_Qty))
		{
			if(!Util.isEmpty(
					MUNSAccessoriesLine.CreateAccessories(getCtx(), getM_Product_ID(), getQty(),
							get_ID(), Table_Name, get_TrxName(), newRecord)))
			return false;
		}
		
		return true;
	}
	
	public boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_AccessoriesLine WHERE UNS_PreOrder_Line_ID=?";
		DB.executeUpdate(sql, get_ID(), get_TrxName());
				
		return true;
	}
}
