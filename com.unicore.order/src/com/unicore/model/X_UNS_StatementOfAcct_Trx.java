/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_StatementOfAcct_Trx
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_StatementOfAcct_Trx extends PO implements I_UNS_StatementOfAcct_Trx, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190211L;

    /** Standard Constructor */
    public X_UNS_StatementOfAcct_Trx (Properties ctx, int UNS_StatementOfAcct_Trx_ID, String trxName)
    {
      super (ctx, UNS_StatementOfAcct_Trx_ID, trxName);
      /** if (UNS_StatementOfAcct_Trx_ID == 0)
        {
			setPriceActual (Env.ZERO);
// 0
			setQty (Env.ZERO);
// 0
			setStore_ID (0);
			setTotalLines (Env.ZERO);
// 0
			setUNS_StatementOfAcct_Trx_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_StatementOfAcct_Trx (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_StatementOfAcct_Trx[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_ValueNoCheck (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Unit Price.
		@param PriceActual 
		Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual)
	{
		set_Value (COLUMNNAME_PriceActual, PriceActual);
	}

	/** Get Unit Price.
		@return Actual Price
	  */
	public BigDecimal getPriceActual () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceActual);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_Value (COLUMNNAME_Store_ID, null);
		else 
			set_Value (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Lines.
		@param TotalLines 
		Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines)
	{
		set_Value (COLUMNNAME_TotalLines, TotalLines);
	}

	/** Get Total Lines.
		@return Total of all document lines
	  */
	public BigDecimal getTotalLines () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLines);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_StatementOfAcct_Line getUNS_StatementOfAcct_Line() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_StatementOfAcct_Line)MTable.get(getCtx(), com.unicore.model.I_UNS_StatementOfAcct_Line.Table_Name)
			.getPO(getUNS_StatementOfAcct_Line_ID(), get_TrxName());	}

	/** Set Statement Of Account Lines.
		@param UNS_StatementOfAcct_Line_ID Statement Of Account Lines	  */
	public void setUNS_StatementOfAcct_Line_ID (int UNS_StatementOfAcct_Line_ID)
	{
		if (UNS_StatementOfAcct_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Line_ID, Integer.valueOf(UNS_StatementOfAcct_Line_ID));
	}

	/** Get Statement Of Account Lines.
		@return Statement Of Account Lines	  */
	public int getUNS_StatementOfAcct_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StatementOfAcct_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Statement Of Account (Transaction).
		@param UNS_StatementOfAcct_Trx_ID Statement Of Account (Transaction)	  */
	public void setUNS_StatementOfAcct_Trx_ID (int UNS_StatementOfAcct_Trx_ID)
	{
		if (UNS_StatementOfAcct_Trx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Trx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Trx_ID, Integer.valueOf(UNS_StatementOfAcct_Trx_ID));
	}

	/** Get Statement Of Account (Transaction).
		@return Statement Of Account (Transaction)	  */
	public int getUNS_StatementOfAcct_Trx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StatementOfAcct_Trx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_StatementOfAcct_Trx_UU.
		@param UNS_StatementOfAcct_Trx_UU UNS_StatementOfAcct_Trx_UU	  */
	public void setUNS_StatementOfAcct_Trx_UU (String UNS_StatementOfAcct_Trx_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Trx_UU, UNS_StatementOfAcct_Trx_UU);
	}

	/** Get UNS_StatementOfAcct_Trx_UU.
		@return UNS_StatementOfAcct_Trx_UU	  */
	public String getUNS_StatementOfAcct_Trx_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_StatementOfAcct_Trx_UU);
	}
}