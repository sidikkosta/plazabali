/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

/**
 * @author Burhani Adam
 *
 */
public class MUNSStatementOfAcctLine extends X_UNS_StatementOfAcct_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2108618503003370389L;

	/**
	 * @param ctx
	 * @param UNS_StatementOfAcct_Line_ID
	 * @param trxName
	 */
	public MUNSStatementOfAcctLine(Properties ctx,
			int UNS_StatementOfAcct_Line_ID, String trxName) {
		super(ctx, UNS_StatementOfAcct_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSStatementOfAccount m_Parent = null;
	private BigDecimal m_QtySales = Env.ZERO;
	private BigDecimal m_BeginStock = Env.ZERO;
	private BigDecimal m_EndStock = Env.ZERO;
	private BigDecimal m_MoveQty = Env.ZERO;
	private BigDecimal m_ReceiptQty = Env.ZERO;
	private BigDecimal m_ReturnedQty = Env.ZERO;
	private BigDecimal m_AdjustQty = Env.ZERO;
	private BigDecimal m_ConvQty = Env.ZERO;

	@Override
	public MUNSStatementOfAccount getParent()
	{
		if(m_Parent == null)
			m_Parent = new MUNSStatementOfAccount(getCtx(), getUNS_StatementOfAccount_ID(), get_TrxName());
		
		return m_Parent;
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSStatementOfAcctLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		
		if(newRecord)
		{
			String sql = "SELECT pp.PriceStd FROM M_ProductPrice pp"
					+ " INNER JOIN M_PriceList_Version v ON pp.M_PriceList_Version_ID = v.M_PriceList_Version_ID"
					+ " INNER JOIN M_PriceList pl ON pl.M_PriceList_ID = v.M_PriceList_ID"
					+ " WHERE pl.IsActive = 'Y' AND pl.IsSOPriceList = 'N' AND pl.C_Currency_ID = ?"
					+ " AND v.ValidFrom <= ? AND pp.M_product_ID = ? ORDER BY v.ValidFrom DESC";
			BigDecimal cost = DB.getSQLValueBD(
					get_TrxName(), sql, new Object[]{getUNS_StatementOfAccount().getC_Currency_ID(),
						getUNS_StatementOfAccount().getDateAcct(), getM_Product_ID()});
			
			if(cost == null)
				cost = Env.ZERO;
			
			setPriceActual(cost);
			sql = sql.replace("'N'", "'Y'");
//			String shopType = getUNS_StatementOfAccount().getShopType();
//			int currID = shopType.equals(MUNSStatementOfAccount.SHOPTYPE_ShopRetailDutyPaid) ? 303 : 100;
			BigDecimal sell = DB.getSQLValueBD(
					get_TrxName(), sql, new Object[]{getUNS_StatementOfAccount().getC_Currency_ID(), 
						getUNS_StatementOfAccount().getDateAcct(), getM_Product_ID()});
			if(sell == null)
				sell = Env.ZERO;
			setSellingPrice(sell);
			if(isReinitQty())
				initQty();
			setQty(getQtySales());
			setBeginningStock(getBeginStock());
			setEndingStock(getEndStock());
			setMovementQty(getMoveQty());
			setReceiptQty(getReceiptQty());
			setReturnedQty(getReturnedQty());
			setAdjustmentQty(getAdjustStock());
			setConversionQty(getConvQty());
		}
		
		if(newRecord || is_ValueChanged(COLUMNNAME_Qty) || is_ValueChanged(COLUMNNAME_PriceActual))
		{
			setTotalLines(getPriceActual().multiply(getQty()));
//			if(getQty().signum() > 0)
				setPushMoneyAmt(getPushMoney());
		}
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(newRecord && !Util.isEmpty(createLines(), true))
		{
			log.saveError("Error", "Failed when trying create transaction.");
			return false;
		}
		
		System.out.println("xxxxxxxxxxxxxxxxx create line = 3 = "+get_ID() );			
		return true;
	}
	
	private String createLines()
	{
		BigDecimal total = Env.ZERO;
		String sql = "SELECT sr.DateTrx, pr.Store_ID, SUM(prl.Qty) FROM UNS_POSRecapLine prl"
				+ " INNER JOIN UNS_POSRecap pr ON pr.UNS_POSRecap_ID = prl.UNS_POSRecap_ID"
				+ " INNER JOIN UNS_SalesReconciliation sr ON sr.UNS_SalesReconciliation_ID = pr.UNS_SalesReconciliation_ID"
				+ " INNER JOIN C_BPartner st ON st.C_BPartner_ID = pr.Store_ID"
				+ " INNER JOIN UNS_StoreConfiguration bpg ON bpg.Store_ID = st.C_BPartner_ID"
				+ " WHERE sr.DateTrx BETWEEN ? AND ? AND prl.IsConsignment = 'Y'"
				+ " AND sr.DocStatus IN ('CO', 'CL') AND bpg.ShopType = ? AND prl.M_Product_ID = ?"
				+ " GROUP BY sr.DateTrx, pr.Store_ID";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setTimestamp(1, getParent().getDateFrom());
			stmt.setTimestamp(2, getParent().getDateTo());
			stmt.setString(3, getParent().getShopType());
			stmt.setInt(4, getM_Product_ID());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSStatementOfAcctTrx trx = new MUNSStatementOfAcctTrx(getCtx(), 0, get_TrxName());
				trx.setUNS_StatementOfAcct_Line_ID(get_ID());
				trx.setAD_Org_ID(getAD_Org_ID());
				trx.setDateTrx(rs.getTimestamp(1));
				trx.setStore_ID(rs.getInt(2));
				trx.setQty(rs.getBigDecimal(3));
				trx.setPriceActual(getPriceActual());
				trx.setTotalLines(rs.getBigDecimal(3).multiply(getPriceActual()));
				trx.save();
				
				total = total.add(trx.getTotalLines());
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		return null;
	}
	
	private BigDecimal getPushMoney()
	{
		BigDecimal amt = MUNSPushMoneyProduct.get(getCtx(), getM_Product_ID(),
				getParent().getDateFrom(), getParent().getDateTo(), getParent().getUNS_SA_Summary().getUNS_Division_ID(),
				get_TrxName());
		
		return getQty().multiply(amt);
	}
	
	private void initSalesQty()
	{
		String sql = "SELECT SUM(prl.Qty) FROM UNS_POSRecapLine prl"
				+ " INNER JOIN UNS_POSRecap pr ON pr.UNS_POSRecap_ID = prl.UNS_POSRecap_ID"
				+ " INNER JOIN UNS_SalesReconciliation sr ON sr.UNS_SalesReconciliation_ID = pr.UNS_SalesReconciliation_ID"
				+ " INNER JOIN M_Product_PO bp ON bp.M_Product_ID = prl.M_Product_ID"
				+ " INNER JOIN C_BPartner st ON st.C_BPartner_ID = pr.Store_ID"
				+ " INNER JOIN UNS_StoreConfiguration bpg ON bpg.Store_ID = st.C_BPartner_ID"
				+ " WHERE bp.C_BPartner_ID = ? AND sr.DateTrx BETWEEN ? AND ? AND prl.M_Product_ID = ?"
				+ " AND sr.DocStatus IN ('CO', 'CL') AND bpg.ShopType = ?";
		
		m_QtySales = DB.getSQLValueBD(get_TrxName(), sql, 
				new Object[]{getParent().getC_BPartner_ID(), getParent().getDateFrom()
					, getParent().getDateTo(), getM_Product_ID(), getParent().getShopType()});
		
		if(m_QtySales == null)
			m_QtySales = Env.ZERO;
	}
	
	public BigDecimal getQtySales()
	{
		return m_QtySales;
	}
	
	private void initBeginStock()
	{
		String sql = "SELECT SUM(TotalStockByWarehouse(?, whs.M_Warehouse_ID, ?))"
				+ " FROM M_Warehouse whs WHERE whs.AD_Org_ID = ? AND whs.UNS_Division_ID = ?";
		m_BeginStock = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{getM_Product_ID(), 
			getParent().getDateFrom(), getAD_Org_ID(), getDivision_ID()});
		
		if(m_BeginStock == null)
			m_BeginStock = Env.ZERO;
	}
	
	public BigDecimal getBeginStock()
	{
		return m_BeginStock;
	}
	
	private void initEndStock()
	{
		java.sql.Timestamp nextDay = TimeUtil.addDays(getParent().getDateTo(), 1);
		String sql = "SELECT SUM(TotalStockByWarehouse(?, whs.M_Warehouse_ID,"
				+ " ?)) FROM M_Warehouse whs"
				+ " WHERE whs.AD_Org_ID = ? AND whs.UNS_Division_ID = ?";
		m_EndStock = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{getM_Product_ID(),
			nextDay, getAD_Org_ID(), getDivision_ID()});
		if(m_EndStock == null)
			m_EndStock = Env.ZERO;
	}
	
	public BigDecimal getEndStock()
	{	
		return m_EndStock;
	}
	
	private void initMoveQty()
	{
		String sql = "SELECT SUM(trx.MovementQty) FROM M_Transaction trx WHERE trx.MovementDate BETWEEN ? AND ?"
				+ " AND trx.AD_Org_ID = ? AND trx.M_Product_ID = ? AND trx.M_MovementLine_ID > 0"
				+ " AND EXISTS (SELECT 1 FROM M_Locator loc WHERE loc.M_Locator_ID = trx.M_Locator_ID"
				+ " AND EXISTS (SELECT 1 FROM M_Warehouse whs WHERE whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " AND whs.UNS_Division_ID = ?))";
		m_MoveQty = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getParent().getDateFrom(), getParent().getDateTo(), getAD_Org_ID(), getM_Product_ID(),
				getDivision_ID()});
		if(m_MoveQty == null)
			m_MoveQty = Env.ZERO;
	}
	
	public BigDecimal getMoveQty()
	{
		return m_MoveQty;
	}
	
	private void initReceiptQty()
	{
		String sql = "SELECT SUM(trx.MovementQty) FROM M_Transaction trx WHERE trx.MovementDate BETWEEN ? AND ?"
				+ " AND trx.AD_Org_ID = ? AND trx.M_Product_ID = ? AND trx.MovementType = 'V+'"
				+ " AND EXISTS (SELECT 1 FROM M_Locator loc WHERE loc.M_Locator_ID = trx.M_Locator_ID"
				+ " AND EXISTS (SELECT 1 FROM M_Warehouse whs WHERE whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " AND whs.UNS_Division_ID = ?))";
		m_ReceiptQty = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getParent().getDateFrom(), getParent().getDateTo(), getAD_Org_ID(), getM_Product_ID(),
				getDivision_ID()
		});
		if(m_ReceiptQty == null)
			m_ReceiptQty = Env.ZERO;
	}
	
	public BigDecimal getReceiptQty()
	{
		return m_ReceiptQty;
	}
	private void initReturnedQty()
	{
		String sql = "SELECT ABS(SUM(trx.MovementQty)) FROM M_Transaction trx WHERE trx.MovementDate BETWEEN ? AND ?"
				+ " AND trx.AD_Org_ID = ? AND trx.M_Product_ID = ? AND trx.MovementType = 'V-'"
				+ " AND EXISTS (SELECT 1 FROM M_Locator loc WHERE loc.M_Locator_ID = trx.M_Locator_ID"
				+ " AND EXISTS (SELECT 1 FROM M_Warehouse whs WHERE whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " AND whs.UNS_Division_ID = ?))";
		m_ReturnedQty = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getParent().getDateFrom(), getParent().getDateTo(), getAD_Org_ID(), getM_Product_ID(),
				getDivision_ID()
		});
		if(m_ReturnedQty == null)
			m_ReturnedQty = Env.ZERO;
	}
	
	public BigDecimal getReturnedQty()
	{
		return m_ReturnedQty;
	}
	
	private void initAdjustStock()
	{
		String sql = "SELECT SUM(trx.MovementQty) FROM M_Transaction trx"
				+ " INNER JOIN M_InventoryLine l ON l.M_InventoryLine_ID = trx.M_InventoryLine_ID"
				+ " INNER JOIN M_Inventory i ON i.M_Inventory_ID = l.M_Inventory_ID"
				+ " INNER JOIN C_DocType dt ON dt.C_DocType_ID = i.C_DocType_ID"
				+ " WHERE trx.MovementDate BETWEEN ? AND ?"
				+ " AND trx.AD_Org_ID = ? AND trx.M_Product_ID = ?"
				+ " AND (dt.DocBaseType = 'MII' OR dt.DocSubTypeInv ='PI')"
				+ " AND EXISTS (SELECT 1 FROM M_Locator loc WHERE loc.M_Locator_ID = trx.M_Locator_ID"
				+ " AND EXISTS (SELECT 1 FROM M_Warehouse whs WHERE whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " AND whs.UNS_Division_ID = ?))";
		m_AdjustQty = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getParent().getDateFrom(), getParent().getDateTo(), getAD_Org_ID(), getM_Product_ID(),
				getDivision_ID()});
		if(m_AdjustQty == null)
			m_AdjustQty = Env.ZERO;
	}
	
	public BigDecimal getAdjustStock()
	{
		return m_AdjustQty;
	}
	
	private void initConvQty()
	{
		String sql = "SELECT SUM(trx.MovementQty) FROM M_Transaction trx"
				+ " INNER JOIN M_InventoryLine l ON l.M_InventoryLine_ID = trx.M_InventoryLine_ID"
				+ " INNER JOIN M_Inventory i ON i.M_Inventory_ID = l.M_Inventory_ID"
				+ " INNER JOIN C_DocType dt ON dt.C_DocType_ID = i.C_DocType_ID"
				+ " WHERE trx.MovementDate BETWEEN ? AND ?"
				+ " AND trx.AD_Org_ID = ? AND trx.M_Product_ID = ?"
				+ " AND dt.DocSubTypeInv IN ('PC', 'IU')"
				+ " AND EXISTS (SELECT 1 FROM M_Locator loc WHERE loc.M_Locator_ID = trx.M_Locator_ID"
				+ " AND EXISTS (SELECT 1 FROM M_Warehouse whs WHERE whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " AND whs.UNS_Division_ID = ?))";
		m_ConvQty = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getParent().getDateFrom(), getParent().getDateTo(), getAD_Org_ID(), getM_Product_ID(),
				getDivision_ID()});
		if(m_ConvQty == null)
			m_ConvQty = Env.ZERO;
	}
	
	public BigDecimal getConvQty()
	{
		return m_ConvQty;
	}
	
	public void initQty()
	{
		initAdjustStock();initBeginStock();initConvQty();initEndStock();initMoveQty();initSalesQty();initReceiptQty();initReturnedQty();
	}
	
	private boolean m_reInitQty = true;
	
	private boolean isReinitQty()
	{
		return m_reInitQty;
	}
	
	public void setReInitQty(boolean reInitQty)
	{
		m_reInitQty = reInitQty;
	}
	
	private int m_divisionID = 0;
	
	private int getDivision_ID()
	{
		if(m_divisionID == 0)
			m_divisionID = getParent().getUNS_SA_Summary().getUNS_Division_ID();
		
		return m_divisionID;
	}
}