/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.compiere.util.Util;

/**
 * @author Burhani Adam
 *
 */
public class MUNSUnLoadingConfig extends X_UNS_UnLoadingConfig implements DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5685154103337707653L;
	private String m_msg = null;

	public MUNSUnLoadingConfig(Properties ctx, int UNS_UnLoadingConfig_ID,
			String trxName) {
		super(ctx, UNS_UnLoadingConfig_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSUnLoadingConfig getByPartner(Properties ctx, int C_BPartner_ID, Timestamp dateTrx ,String trxName)
	{
		int UNS_UnLoadingConfig_ID = 0;
		MUNSUnLoadingConfig lc = null;
		
		String sql = "SELECT UNS_UnLoadingConfig_ID FROM UNS_UnLoadingConfig WHERE DocStatus = 'CO' AND C_BPartner_ID=? AND"
				+ " ValidFrom <= ? ORDER BY ValidFrom DESC";
		UNS_UnLoadingConfig_ID = DB.getSQLValue(trxName, sql, C_BPartner_ID, dateTrx);
		
		if(UNS_UnLoadingConfig_ID > 0)
		{
			lc = new MUNSUnLoadingConfig(ctx, UNS_UnLoadingConfig_ID, trxName);
			return lc;
		}
		
		sql = "SELECT UNS_UnLoadingConfig_ID FROM UNS_UnLoadingConfig WHERE DocStatus = 'CO' AND C_BP_Group_ID="
				+ " (SELECT C_BP_Group_ID FROM C_BPartner WHERE C_BPartner_ID = ?) AND ValidFrom <= ? ORDER BY ValidFrom DESC ";
		UNS_UnLoadingConfig_ID = DB.getSQLValue(trxName, sql, C_BPartner_ID, dateTrx);
		
		if(UNS_UnLoadingConfig_ID > 0)
		{
			lc = new MUNSUnLoadingConfig(ctx, UNS_UnLoadingConfig_ID, trxName);
			return lc;
		}
		
		sql = "SELECT UNS_UnLoadingConfig_ID FROM UNS_UnLoadingConfig WHERE (C_BPartner_ID IS NULL OR C_BPartner_ID = 0)"
				+ " AND (C_BP_Group_ID IS NULL OR C_BP_Group_ID = 0) AND DocStatus = 'CO' AND ValidFrom <= ? ORDER BY ValidFrom DESC";
		UNS_UnLoadingConfig_ID = DB.getSQLValue(trxName, sql, dateTrx);
		
		if(UNS_UnLoadingConfig_ID > 0)
		{
			lc = new MUNSUnLoadingConfig(ctx, UNS_UnLoadingConfig_ID, trxName);
			return lc;
		}
		
		return null;
	}
	
	public MUNSUnLoadingConfigLine[] getLines(String orderClause)
	{
		List<MUNSUnLoadingConfigLine> list = new Query(getCtx(), MUNSUnLoadingConfigLine.Table_Name,
				COLUMNNAME_UNS_UnLoadingConfig_ID + "=?", get_TrxName()).setParameters(get_ID()).setOrderBy(orderClause).list();
		
		return list.toArray(new MUNSUnLoadingConfigLine[list.size()]);
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		String sql = null;
		String docNo = null;
		
		if(getC_BPartner_ID() > 0)
		{
			setC_BP_Group_ID(-1);
			sql = "SELECT DocumentNo FROM UNS_UnLoadingConfig WHERE C_BPartner_ID= ? AND DocStatus NOT IN ('VO', 'RE', 'CL')"
					+ " AND UNS_UnLoadingConfig_ID <> ? AND ValidFrom = ?";
			docNo = DB.getSQLValueString(get_TrxName(), sql, getC_BPartner_ID(), get_ID(), getValidFrom());
		}
		else if(getC_BP_Group_ID() > 0)
		{
			sql = "SELECT DocumentNo FROM UNS_UnLoadingConfig WHERE C_BP_Group_ID=? AND DocStatus NOT IN ('VO', 'RE', 'CL')"
					+ " AND UNS_UnLoadingConfig_ID<>? AND ValidFrom = ?";
			docNo = DB.getSQLValueString(get_TrxName(), sql, getC_BP_Group_ID(), get_ID(), getValidFrom());
		}
		else if(getC_BP_Group_ID() <= 0 && getC_BPartner_ID() <= 0)
		{
			sql = "SELECT DocumentNo FROM UNS_UnLoadingConfig WHERE C_BP_Group_ID <= 0 AND C_BPartner_ID <= 0"
					+ " AND DocStatus NOT IN ('VO', 'RE', 'CL') AND UNS_UnLoadingConfig_ID<>? AND ValidFrom = ?";
			docNo = DB.getSQLValueString(get_TrxName(), sql, getValidFrom());
		}
		
		if(!Util.isEmpty(docNo, true))
		{
			m_msg = "Duplicate configuration with document " + docNo;
			log.saveError("Error", Msg.parseTranslation(getCtx(), m_msg));
			return false;
		}
		return true;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSUnLoadingConfig[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_UnLoadingConfig_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_UnLoadingConfig_Line WHERE UNS_UnLoadingConfig_ID=?";
		int count = DB.executeUpdate(sql, get_ID(), get_TrxName());
		if(count > 0)
		{
			log.saveError("Error", "Failed when delete lines");
			return false;
		}
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
}