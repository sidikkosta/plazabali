/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_POSFNBTableLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSFNBTableLine extends PO implements I_UNS_POSFNBTableLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181008L;

    /** Standard Constructor */
    public X_UNS_POSFNBTableLine (Properties ctx, int UNS_POSFNBTableLine_ID, String trxName)
    {
      super (ctx, UNS_POSFNBTableLine_ID, trxName);
      /** if (UNS_POSFNBTableLine_ID == 0)
        {
			setisReserved (false);
// N
			setName (null);
			setUNS_POSFNBTable_ID (0);
			setUNS_POSFNBTableLine_ID (0);
			setX (null);
			setY (null);
			setZ (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSFNBTableLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSFNBTableLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set isReserved.
		@param isReserved isReserved	  */
	public void setisReserved (boolean isReserved)
	{
		set_Value (COLUMNNAME_isReserved, Boolean.valueOf(isReserved));
	}

	/** Get isReserved.
		@return isReserved	  */
	public boolean isReserved () 
	{
		Object oo = get_Value(COLUMNNAME_isReserved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public com.unicore.model.I_UNS_POSFNBTable getUNS_POSFNBTable() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSFNBTable)MTable.get(getCtx(), com.unicore.model.I_UNS_POSFNBTable.Table_Name)
			.getPO(getUNS_POSFNBTable_ID(), get_TrxName());	}

	/** Set FNB Table.
		@param UNS_POSFNBTable_ID FNB Table	  */
	public void setUNS_POSFNBTable_ID (int UNS_POSFNBTable_ID)
	{
		if (UNS_POSFNBTable_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTable_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTable_ID, Integer.valueOf(UNS_POSFNBTable_ID));
	}

	/** Get FNB Table.
		@return FNB Table	  */
	public int getUNS_POSFNBTable_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSFNBTable_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Table .
		@param UNS_POSFNBTableLine_ID Table 	  */
	public void setUNS_POSFNBTableLine_ID (int UNS_POSFNBTableLine_ID)
	{
		if (UNS_POSFNBTableLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTableLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTableLine_ID, Integer.valueOf(UNS_POSFNBTableLine_ID));
	}

	/** Get Table .
		@return Table 	  */
	public int getUNS_POSFNBTableLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSFNBTableLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSFNBTableLine_UU.
		@param UNS_POSFNBTableLine_UU UNS_POSFNBTableLine_UU	  */
	public void setUNS_POSFNBTableLine_UU (String UNS_POSFNBTableLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTableLine_UU, UNS_POSFNBTableLine_UU);
	}

	/** Get UNS_POSFNBTableLine_UU.
		@return UNS_POSFNBTableLine_UU	  */
	public String getUNS_POSFNBTableLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSFNBTableLine_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Aisle (X).
		@param X 
		X dimension, e.g., Aisle
	  */
	public void setX (String X)
	{
		set_ValueNoCheck (COLUMNNAME_X, X);
	}

	/** Get Aisle (X).
		@return X dimension, e.g., Aisle
	  */
	public String getX () 
	{
		return (String)get_Value(COLUMNNAME_X);
	}

	/** Set Bin (Y).
		@param Y 
		Y dimension, e.g., Bin
	  */
	public void setY (String Y)
	{
		set_ValueNoCheck (COLUMNNAME_Y, Y);
	}

	/** Get Bin (Y).
		@return Y dimension, e.g., Bin
	  */
	public String getY () 
	{
		return (String)get_Value(COLUMNNAME_Y);
	}

	/** Set Level (Z).
		@param Z 
		Z dimension, e.g., Level
	  */
	public void setZ (String Z)
	{
		set_ValueNoCheck (COLUMNNAME_Z, Z);
	}

	/** Get Level (Z).
		@return Z dimension, e.g., Level
	  */
	public String getZ () 
	{
		return (String)get_Value(COLUMNNAME_Z);
	}
}