/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSTrx
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSTrx 
{

    /** TableName=UNS_POSTrx */
    public static final String Table_Name = "UNS_POSTrx";

    /** AD_Table_ID=1000387 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Shop.
	  * The Shop
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Shop.
	  * The Shop
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_BPartner_Location_ID */
    public static final String COLUMNNAME_C_BPartner_Location_ID = "C_BPartner_Location_ID";

	/** Set Shop Location.
	  * Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID);

	/** Get Shop Location.
	  * Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID();

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException;

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name C_DocType_ID */
    public static final String COLUMNNAME_C_DocType_ID = "C_DocType_ID";

	/** Set Document Type.
	  * Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID);

	/** Get Document Type.
	  * Document type or rules
	  */
	public int getC_DocType_ID();

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateAcct */
    public static final String COLUMNNAME_DateAcct = "DateAcct";

	/** Set Account Date.
	  * Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct);

	/** Get Account Date.
	  * Accounting Date
	  */
	public Timestamp getDateAcct();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DiscNote */
    public static final String COLUMNNAME_DiscNote = "DiscNote";

	/** Set Discount Note	  */
	public void setDiscNote (String DiscNote);

	/** Get Discount Note	  */
	public String getDiscNote();

    /** Column name Discount */
    public static final String COLUMNNAME_Discount = "Discount";

	/** Set Discount %.
	  * Discount in percent
	  */
	public void setDiscount (BigDecimal Discount);

	/** Get Discount %.
	  * Discount in percent
	  */
	public BigDecimal getDiscount();

    /** Column name DiscountAmt */
    public static final String COLUMNNAME_DiscountAmt = "DiscountAmt";

	/** Set Discount Amount.
	  * Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt);

	/** Get Discount Amount.
	  * Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt();

    /** Column name DiscReff */
    public static final String COLUMNNAME_DiscReff = "DiscReff";

	/** Set Discount Refference	  */
	public void setDiscReff (String DiscReff);

	/** Get Discount Refference	  */
	public String getDiscReff();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name GrandTotal */
    public static final String COLUMNNAME_GrandTotal = "GrandTotal";

	/** Set Grand Total.
	  * Total amount of document
	  */
	public void setGrandTotal (BigDecimal GrandTotal);

	/** Get Grand Total.
	  * Total amount of document
	  */
	public BigDecimal getGrandTotal();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name IsPaid */
    public static final String COLUMNNAME_IsPaid = "IsPaid";

	/** Set Paid.
	  * The document is paid
	  */
	public void setIsPaid (boolean IsPaid);

	/** Get Paid.
	  * The document is paid
	  */
	public boolean isPaid();

    /** Column name M_PriceList_ID */
    public static final String COLUMNNAME_M_PriceList_ID = "M_PriceList_ID";

	/** Set Price List.
	  * Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID);

	/** Get Price List.
	  * Unique identifier of a Price List
	  */
	public int getM_PriceList_ID();

	public org.compiere.model.I_M_PriceList getM_PriceList() throws RuntimeException;

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name PrintDocument */
    public static final String COLUMNNAME_PrintDocument = "PrintDocument";

	/** Set Print Document	  */
	public void setPrintDocument (String PrintDocument);

	/** Get Print Document	  */
	public String getPrintDocument();

    /** Column name PrintPOSReturn */
    public static final String COLUMNNAME_PrintPOSReturn = "PrintPOSReturn";

	/** Set Print POS Return	  */
	public void setPrintPOSReturn (String PrintPOSReturn);

	/** Get Print POS Return	  */
	public String getPrintPOSReturn();

    /** Column name PrintStrukRetur */
    public static final String COLUMNNAME_PrintStrukRetur = "PrintStrukRetur";

	/** Set PrintStrukRetur	  */
	public void setPrintStrukRetur (String PrintStrukRetur);

	/** Get PrintStrukRetur	  */
	public String getPrintStrukRetur();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name Reference_ID */
    public static final String COLUMNNAME_Reference_ID = "Reference_ID";

	/** Set Refrerence Record	  */
	public void setReference_ID (int Reference_ID);

	/** Get Refrerence Record	  */
	public int getReference_ID();

	public com.unicore.model.I_UNS_POSTrx getReference() throws RuntimeException;

    /** Column name ReferenceUU */
    public static final String COLUMNNAME_ReferenceUU = "ReferenceUU";

	/** Set Reference UU	  */
	public void setReferenceUU (String ReferenceUU);

	/** Get Reference UU	  */
	public String getReferenceUU();

    /** Column name RepCurrRate */
    public static final String COLUMNNAME_RepCurrRate = "RepCurrRate";

	/** Set Report Currency Rate	  */
	public void setRepCurrRate (String RepCurrRate);

	/** Get Report Currency Rate	  */
	public String getRepCurrRate();

    /** Column name SalesRep_ID */
    public static final String COLUMNNAME_SalesRep_ID = "SalesRep_ID";

	/** Set Sales Representative.
	  * Sales Representative or Company Agent
	  */
	public void setSalesRep_ID (int SalesRep_ID);

	/** Get Sales Representative.
	  * Sales Representative or Company Agent
	  */
	public int getSalesRep_ID();

	public org.compiere.model.I_AD_User getSalesRep() throws RuntimeException;

    /** Column name ServiceCharge */
    public static final String COLUMNNAME_ServiceCharge = "ServiceCharge";

	/** Set Service Charge	  */
	public void setServiceCharge (BigDecimal ServiceCharge);

	/** Get Service Charge	  */
	public BigDecimal getServiceCharge();

    /** Column name TaxAmt */
    public static final String COLUMNNAME_TaxAmt = "TaxAmt";

	/** Set Tax Amount.
	  * Tax Amount for a document
	  */
	public void setTaxAmt (BigDecimal TaxAmt);

	/** Get Tax Amount.
	  * Tax Amount for a document
	  */
	public BigDecimal getTaxAmt();

    /** Column name TotalAmt */
    public static final String COLUMNNAME_TotalAmt = "TotalAmt";

	/** Set Total Amount.
	  * Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt);

	/** Get Total Amount.
	  * Total Amount
	  */
	public BigDecimal getTotalAmt();

    /** Column name TrxNo */
    public static final String COLUMNNAME_TrxNo = "TrxNo";

	/** Set Transaction No.
	  * Transaction Number
	  */
	public void setTrxNo (String TrxNo);

	/** Get Transaction No.
	  * Transaction Number
	  */
	public String getTrxNo();

    /** Column name UNS_CustomerInfo_ID */
    public static final String COLUMNNAME_UNS_CustomerInfo_ID = "UNS_CustomerInfo_ID";

	/** Set Customer Info	  */
	public void setUNS_CustomerInfo_ID (int UNS_CustomerInfo_ID);

	/** Get Customer Info	  */
	public int getUNS_CustomerInfo_ID();

	public com.unicore.model.I_UNS_CustomerInfo getUNS_CustomerInfo() throws RuntimeException;

    /** Column name UNS_DiscountReff_ID */
    public static final String COLUMNNAME_UNS_DiscountReff_ID = "UNS_DiscountReff_ID";

	/** Set Discount Refference	  */
	public void setUNS_DiscountReff_ID (int UNS_DiscountReff_ID);

	/** Get Discount Refference	  */
	public int getUNS_DiscountReff_ID();

	public com.unicore.model.I_UNS_DiscountReff getUNS_DiscountReff() throws RuntimeException;

    /** Column name UNS_POSFNBTableLine_ID */
    public static final String COLUMNNAME_UNS_POSFNBTableLine_ID = "UNS_POSFNBTableLine_ID";

	/** Set Table 	  */
	public void setUNS_POSFNBTableLine_ID (int UNS_POSFNBTableLine_ID);

	/** Get Table 	  */
	public int getUNS_POSFNBTableLine_ID();

	public com.unicore.model.I_UNS_POSFNBTableLine getUNS_POSFNBTableLine() throws RuntimeException;

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException;

    /** Column name UNS_POSTerminal_ID */
    public static final String COLUMNNAME_UNS_POSTerminal_ID = "UNS_POSTerminal_ID";

	/** Set POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID);

	/** Get POS Terminal	  */
	public int getUNS_POSTerminal_ID();

	public com.unicore.model.I_UNS_POSTerminal getUNS_POSTerminal() throws RuntimeException;

    /** Column name UNS_POSTrx_ID */
    public static final String COLUMNNAME_UNS_POSTrx_ID = "UNS_POSTrx_ID";

	/** Set POS Transactions	  */
	public void setUNS_POSTrx_ID (int UNS_POSTrx_ID);

	/** Get POS Transactions	  */
	public int getUNS_POSTrx_ID();

    /** Column name UNS_POSTrx_UU */
    public static final String COLUMNNAME_UNS_POSTrx_UU = "UNS_POSTrx_UU";

	/** Set UNS_POSTrx_UU	  */
	public void setUNS_POSTrx_UU (String UNS_POSTrx_UU);

	/** Get UNS_POSTrx_UU	  */
	public String getUNS_POSTrx_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
	
    /** Column name CustomerName */
    public static final String COLUMNNAME_CustomerName = "CustomerName";

	/** Set Customer Name	  */
	public void setCustomerName (String CustomerName);

	/** Get Customer Name	  */
	public String getCustomerName();
}
