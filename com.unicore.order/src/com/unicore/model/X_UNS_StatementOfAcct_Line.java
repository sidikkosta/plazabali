/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_StatementOfAcct_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_StatementOfAcct_Line extends PO implements I_UNS_StatementOfAcct_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190319L;

    /** Standard Constructor */
    public X_UNS_StatementOfAcct_Line (Properties ctx, int UNS_StatementOfAcct_Line_ID, String trxName)
    {
      super (ctx, UNS_StatementOfAcct_Line_ID, trxName);
      /** if (UNS_StatementOfAcct_Line_ID == 0)
        {
			setAdjustmentQty (Env.ZERO);
// 0
			setBeginningStock (Env.ZERO);
// 0
			setConversionQty (Env.ZERO);
// 0
			setEndingStock (Env.ZERO);
// 0
			setMovementQty (Env.ZERO);
// 0
			setM_Product_ID (0);
			setPriceActual (Env.ZERO);
// 0
			setPushMoneyAmt (Env.ZERO);
// 0
			setQty (Env.ZERO);
// 0
			setReceiptQty (Env.ZERO);
// 0
			setReturnedQty (Env.ZERO);
// 0
			setSellingPrice (Env.ZERO);
// 0
			setTotalLines (Env.ZERO);
// 0
			setUNS_StatementOfAcct_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_StatementOfAcct_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_StatementOfAcct_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Adjustment Qty.
		@param AdjustmentQty Adjustment Qty	  */
	public void setAdjustmentQty (BigDecimal AdjustmentQty)
	{
		set_Value (COLUMNNAME_AdjustmentQty, AdjustmentQty);
	}

	/** Get Adjustment Qty.
		@return Adjustment Qty	  */
	public BigDecimal getAdjustmentQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AdjustmentQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Beginning Stock.
		@param BeginningStock Beginning Stock	  */
	public void setBeginningStock (BigDecimal BeginningStock)
	{
		set_Value (COLUMNNAME_BeginningStock, BeginningStock);
	}

	/** Get Beginning Stock.
		@return Beginning Stock	  */
	public BigDecimal getBeginningStock () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BeginningStock);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Conversion Qty.
		@param ConversionQty Conversion Qty	  */
	public void setConversionQty (BigDecimal ConversionQty)
	{
		set_Value (COLUMNNAME_ConversionQty, ConversionQty);
	}

	/** Get Conversion Qty.
		@return Conversion Qty	  */
	public BigDecimal getConversionQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ConversionQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Ending Stock.
		@param EndingStock Ending Stock	  */
	public void setEndingStock (BigDecimal EndingStock)
	{
		set_Value (COLUMNNAME_EndingStock, EndingStock);
	}

	/** Get Ending Stock.
		@return Ending Stock	  */
	public BigDecimal getEndingStock () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EndingStock);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Movement Quantity.
		@param MovementQty 
		Quantity of a product moved.
	  */
	public void setMovementQty (BigDecimal MovementQty)
	{
		set_Value (COLUMNNAME_MovementQty, MovementQty);
	}

	/** Get Movement Quantity.
		@return Quantity of a product moved.
	  */
	public BigDecimal getMovementQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MovementQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getM_Product_ID()));
    }

	/** Set Unit Price.
		@param PriceActual 
		Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual)
	{
		set_Value (COLUMNNAME_PriceActual, PriceActual);
	}

	/** Get Unit Price.
		@return Actual Price
	  */
	public BigDecimal getPriceActual () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceActual);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Push Money Amount.
		@param PushMoneyAmt Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt)
	{
		set_Value (COLUMNNAME_PushMoneyAmt, PushMoneyAmt);
	}

	/** Get Push Money Amount.
		@return Push Money Amount	  */
	public BigDecimal getPushMoneyAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PushMoneyAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Receipt Qty.
		@param ReceiptQty Receipt Qty	  */
	public void setReceiptQty (BigDecimal ReceiptQty)
	{
		set_Value (COLUMNNAME_ReceiptQty, ReceiptQty);
	}

	/** Get Receipt Qty.
		@return Receipt Qty	  */
	public BigDecimal getReceiptQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReceiptQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Returned Quantity.
		@param ReturnedQty Returned Quantity	  */
	public void setReturnedQty (BigDecimal ReturnedQty)
	{
		set_ValueNoCheck (COLUMNNAME_ReturnedQty, ReturnedQty);
	}

	/** Get Returned Quantity.
		@return Returned Quantity	  */
	public BigDecimal getReturnedQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnedQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Selling Price.
		@param SellingPrice Selling Price	  */
	public void setSellingPrice (BigDecimal SellingPrice)
	{
		set_Value (COLUMNNAME_SellingPrice, SellingPrice);
	}

	/** Get Selling Price.
		@return Selling Price	  */
	public BigDecimal getSellingPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SellingPrice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Lines.
		@param TotalLines 
		Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines)
	{
		set_Value (COLUMNNAME_TotalLines, TotalLines);
	}

	/** Get Total Lines.
		@return Total of all document lines
	  */
	public BigDecimal getTotalLines () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLines);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_StatementOfAccount getUNS_StatementOfAccount() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_StatementOfAccount)MTable.get(getCtx(), com.unicore.model.I_UNS_StatementOfAccount.Table_Name)
			.getPO(getUNS_StatementOfAccount_ID(), get_TrxName());	}

	/** Set Statement Of Account.
		@param UNS_StatementOfAccount_ID Statement Of Account	  */
	public void setUNS_StatementOfAccount_ID (int UNS_StatementOfAccount_ID)
	{
		if (UNS_StatementOfAccount_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAccount_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAccount_ID, Integer.valueOf(UNS_StatementOfAccount_ID));
	}

	/** Get Statement Of Account.
		@return Statement Of Account	  */
	public int getUNS_StatementOfAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StatementOfAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Statement Of Account Lines.
		@param UNS_StatementOfAcct_Line_ID Statement Of Account Lines	  */
	public void setUNS_StatementOfAcct_Line_ID (int UNS_StatementOfAcct_Line_ID)
	{
		if (UNS_StatementOfAcct_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Line_ID, Integer.valueOf(UNS_StatementOfAcct_Line_ID));
	}

	/** Get Statement Of Account Lines.
		@return Statement Of Account Lines	  */
	public int getUNS_StatementOfAcct_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StatementOfAcct_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_StatementOfAcct_Line_UU.
		@param UNS_StatementOfAcct_Line_UU UNS_StatementOfAcct_Line_UU	  */
	public void setUNS_StatementOfAcct_Line_UU (String UNS_StatementOfAcct_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAcct_Line_UU, UNS_StatementOfAcct_Line_UU);
	}

	/** Get UNS_StatementOfAcct_Line_UU.
		@return UNS_StatementOfAcct_Line_UU	  */
	public String getUNS_StatementOfAcct_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_StatementOfAcct_Line_UU);
	}
}