/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_EDCReconciliation
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_EDCReconciliation 
{

    /** TableName=UNS_EDCReconciliation */
    public static final String Table_Name = "UNS_EDCReconciliation";

    /** AD_Table_ID=1000411 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name PayableRefundAmt */
    public static final String COLUMNNAME_PayableRefundAmt = "PayableRefundAmt";

	/** Set Payable Refund Amount.
	  * The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt);

	/** Get Payable Refund Amount.
	  * The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt();

    /** Column name TotalAmt */
    public static final String COLUMNNAME_TotalAmt = "TotalAmt";

	/** Set Total Amount.
	  * Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt);

	/** Get Total Amount.
	  * Total Amount
	  */
	public BigDecimal getTotalAmt();

    /** Column name UNS_CardType_ID */
    public static final String COLUMNNAME_UNS_CardType_ID = "UNS_CardType_ID";

	/** Set Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID);

	/** Get Card Type	  */
	public int getUNS_CardType_ID();

    /** Column name UNS_EDCReconciliation_ID */
    public static final String COLUMNNAME_UNS_EDCReconciliation_ID = "UNS_EDCReconciliation_ID";

	/** Set EDC Reconciliation	  */
	public void setUNS_EDCReconciliation_ID (int UNS_EDCReconciliation_ID);

	/** Get EDC Reconciliation	  */
	public int getUNS_EDCReconciliation_ID();

    /** Column name UNS_EDCReconciliation_UU */
    public static final String COLUMNNAME_UNS_EDCReconciliation_UU = "UNS_EDCReconciliation_UU";

	/** Set UNS_EDCReconciliation_UU	  */
	public void setUNS_EDCReconciliation_UU (String UNS_EDCReconciliation_UU);

	/** Get UNS_EDCReconciliation_UU	  */
	public String getUNS_EDCReconciliation_UU();

    /** Column name UNS_SalesReconciliation_ID */
    public static final String COLUMNNAME_UNS_SalesReconciliation_ID = "UNS_SalesReconciliation_ID";

	/** Set Sales Reconciliation	  */
	public void setUNS_SalesReconciliation_ID (int UNS_SalesReconciliation_ID);

	/** Get Sales Reconciliation	  */
	public int getUNS_SalesReconciliation_ID();

	public I_UNS_SalesReconciliation getUNS_SalesReconciliation() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
