/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Incentive
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Incentive 
{

    /** TableName=UNS_Incentive */
    public static final String Table_Name = "UNS_Incentive";

    /** AD_Table_ID=1000148 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_PaymentTerm_ID */
    public static final String COLUMNNAME_C_PaymentTerm_ID = "C_PaymentTerm_ID";

	/** Set Payment Term.
	  * The terms of Payment (timing, discount)
	  */
	public void setC_PaymentTerm_ID (int C_PaymentTerm_ID);

	/** Get Payment Term.
	  * The terms of Payment (timing, discount)
	  */
	public int getC_PaymentTerm_ID();

	public org.compiere.model.I_C_PaymentTerm getC_PaymentTerm() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Incentive */
    public static final String COLUMNNAME_Incentive = "Incentive";

	/** Set Incentive %	  */
	public void setIncentive (BigDecimal Incentive);

	/** Get Incentive %	  */
	public BigDecimal getIncentive();

    /** Column name IncentiveNewOutlet */
    public static final String COLUMNNAME_IncentiveNewOutlet = "IncentiveNewOutlet";

	/** Set Incentive New Outlet %	  */
	public void setIncentiveNewOutlet (BigDecimal IncentiveNewOutlet);

	/** Get Incentive New Outlet %	  */
	public BigDecimal getIncentiveNewOutlet();

    /** Column name IncentiveType */
    public static final String COLUMNNAME_IncentiveType = "IncentiveType";

	/** Set Incentive Type.
	  * Type of incentive (Sales Incentive/Billing Incentive)
	  */
	public void setIncentiveType (String IncentiveType);

	/** Get Incentive Type.
	  * Type of incentive (Sales Incentive/Billing Incentive)
	  */
	public String getIncentiveType();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Product_Category_ID */
    public static final String COLUMNNAME_M_Product_Category_ID = "M_Product_Category_ID";

	/** Set Product Category.
	  * Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID);

	/** Get Product Category.
	  * Category of a Product
	  */
	public int getM_Product_Category_ID();

	public org.compiere.model.I_M_Product_Category getM_Product_Category() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name UNS_Incentive_ID */
    public static final String COLUMNNAME_UNS_Incentive_ID = "UNS_Incentive_ID";

	/** Set Incentive	  */
	public void setUNS_Incentive_ID (int UNS_Incentive_ID);

	/** Get Incentive	  */
	public int getUNS_Incentive_ID();

    /** Column name UNS_IncentiveSchema_ID */
    public static final String COLUMNNAME_UNS_IncentiveSchema_ID = "UNS_IncentiveSchema_ID";

	/** Set Incenive Schema	  */
	public void setUNS_IncentiveSchema_ID (int UNS_IncentiveSchema_ID);

	/** Get Incenive Schema	  */
	public int getUNS_IncentiveSchema_ID();

	public com.unicore.model.I_UNS_IncentiveSchema getUNS_IncentiveSchema() throws RuntimeException;

    /** Column name UNS_Incentive_UU */
    public static final String COLUMNNAME_UNS_Incentive_UU = "UNS_Incentive_UU";

	/** Set UNS_Incentive_UU	  */
	public void setUNS_Incentive_UU (String UNS_Incentive_UU);

	/** Get UNS_Incentive_UU	  */
	public String getUNS_Incentive_UU();

    /** Column name UNS_Outlet_Grade_ID */
    public static final String COLUMNNAME_UNS_Outlet_Grade_ID = "UNS_Outlet_Grade_ID";

	/** Set Outlet Grade	  */
	public void setUNS_Outlet_Grade_ID (int UNS_Outlet_Grade_ID);

	/** Get Outlet Grade	  */
	public int getUNS_Outlet_Grade_ID();

    /** Column name UNS_Outlet_Type_ID */
    public static final String COLUMNNAME_UNS_Outlet_Type_ID = "UNS_Outlet_Type_ID";

	/** Set Outlet Type	  */
	public void setUNS_Outlet_Type_ID (int UNS_Outlet_Type_ID);

	/** Get Outlet Type	  */
	public int getUNS_Outlet_Type_ID();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
