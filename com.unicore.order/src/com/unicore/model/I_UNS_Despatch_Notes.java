/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Despatch_Notes
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Despatch_Notes 
{

    /** TableName=UNS_Despatch_Notes */
    public static final String Table_Name = "UNS_Despatch_Notes";

    /** AD_Table_ID=1000282 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name AssignedBy */
    public static final String COLUMNNAME_AssignedBy = "AssignedBy";

	/** Set Assigned By.
	  * The person / contact to assign the document
	  */
	public void setAssignedBy (String AssignedBy);

	/** Get Assigned By.
	  * The person / contact to assign the document
	  */
	public String getAssignedBy();

    /** Column name C_Order_ID */
    public static final String COLUMNNAME_C_Order_ID = "C_Order_ID";

	/** Set Order.
	  * Order
	  */
	public void setC_Order_ID (int C_Order_ID);

	/** Get Order.
	  * Order
	  */
	public int getC_Order_ID();

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name Dirt */
    public static final String COLUMNNAME_Dirt = "Dirt";

	/** Set Dirt.
	  * Dirt
	  */
	public void setDirt (BigDecimal Dirt);

	/** Get Dirt.
	  * Dirt
	  */
	public BigDecimal getDirt();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EndDespatch */
    public static final String COLUMNNAME_EndDespatch = "EndDespatch";

	/** Set End Despatch.
	  * The ending time of despatching product into a vehicle
	  */
	public void setEndDespatch (Timestamp EndDespatch);

	/** Get End Despatch.
	  * The ending time of despatching product into a vehicle
	  */
	public Timestamp getEndDespatch();

    /** Column name FFA */
    public static final String COLUMNNAME_FFA = "FFA";

	/** Set FFA.
	  * Free Fatty Acid
	  */
	public void setFFA (BigDecimal FFA);

	/** Get FFA.
	  * Free Fatty Acid
	  */
	public BigDecimal getFFA();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name IsOverwriteDespatchTime */
    public static final String COLUMNNAME_IsOverwriteDespatchTime = "IsOverwriteDespatchTime";

	/** Set Overwrite Despatch Time	  */
	public void setIsOverwriteDespatchTime (boolean IsOverwriteDespatchTime);

	/** Get Overwrite Despatch Time	  */
	public boolean isOverwriteDespatchTime();

    /** Column name IsQtyPercentage */
    public static final String COLUMNNAME_IsQtyPercentage = "IsQtyPercentage";

	/** Set Is Qty Percentage.
	  * Indicate that this component is based in % Quantity
	  */
	public void setIsQtyPercentage (boolean IsQtyPercentage);

	/** Get Is Qty Percentage.
	  * Indicate that this component is based in % Quantity
	  */
	public boolean isQtyPercentage();

    /** Column name Locator1_ID */
    public static final String COLUMNNAME_Locator1_ID = "Locator1_ID";

	/** Set Locator I	  */
	public void setLocator1_ID (int Locator1_ID);

	/** Get Locator I	  */
	public int getLocator1_ID();

	public org.compiere.model.I_M_Locator getLocator1() throws RuntimeException;

    /** Column name Locator2_ID */
    public static final String COLUMNNAME_Locator2_ID = "Locator2_ID";

	/** Set Locator II	  */
	public void setLocator2_ID (int Locator2_ID);

	/** Get Locator II	  */
	public int getLocator2_ID();

	public org.compiere.model.I_M_Locator getLocator2() throws RuntimeException;

    /** Column name Locator3_ID */
    public static final String COLUMNNAME_Locator3_ID = "Locator3_ID";

	/** Set Locator III	  */
	public void setLocator3_ID (int Locator3_ID);

	/** Get Locator III	  */
	public int getLocator3_ID();

	public org.compiere.model.I_M_Locator getLocator3() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name MI */
    public static final String COLUMNNAME_MI = "MI";

	/** Set MI.
	  * Moisture & Impurity
	  */
	public void setMI (BigDecimal MI);

	/** Get MI.
	  * Moisture & Impurity
	  */
	public BigDecimal getMI();

    /** Column name Moisture */
    public static final String COLUMNNAME_Moisture = "Moisture";

	/** Set Moisture.
	  * Moisture
	  */
	public void setMoisture (BigDecimal Moisture);

	/** Get Moisture.
	  * Moisture
	  */
	public BigDecimal getMoisture();

    /** Column name PercentageLoc1 */
    public static final String COLUMNNAME_PercentageLoc1 = "PercentageLoc1";

	/** Set Percentage Locator I	  */
	public void setPercentageLoc1 (BigDecimal PercentageLoc1);

	/** Get Percentage Locator I	  */
	public BigDecimal getPercentageLoc1();

    /** Column name PercentageLoc2 */
    public static final String COLUMNNAME_PercentageLoc2 = "PercentageLoc2";

	/** Set Percentage Locator II	  */
	public void setPercentageLoc2 (BigDecimal PercentageLoc2);

	/** Get Percentage Locator II	  */
	public BigDecimal getPercentageLoc2();

    /** Column name PercentageLoc3 */
    public static final String COLUMNNAME_PercentageLoc3 = "PercentageLoc3";

	/** Set Percentage Locator III	  */
	public void setPercentageLoc3 (BigDecimal PercentageLoc3);

	/** Get Percentage Locator III	  */
	public BigDecimal getPercentageLoc3();

    /** Column name POReference */
    public static final String COLUMNNAME_POReference = "POReference";

	/** Set Order Reference.
	  * Transaction Reference Number (Sales Order, Purchase Order) of your Business Partner
	  */
	public void setPOReference (String POReference);

	/** Get Order Reference.
	  * Transaction Reference Number (Sales Order, Purchase Order) of your Business Partner
	  */
	public String getPOReference();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name QtyLoc1 */
    public static final String COLUMNNAME_QtyLoc1 = "QtyLoc1";

	/** Set Qty Locator I	  */
	public void setQtyLoc1 (BigDecimal QtyLoc1);

	/** Get Qty Locator I	  */
	public BigDecimal getQtyLoc1();

    /** Column name QtyLoc2 */
    public static final String COLUMNNAME_QtyLoc2 = "QtyLoc2";

	/** Set Qty Locator II	  */
	public void setQtyLoc2 (BigDecimal QtyLoc2);

	/** Get Qty Locator II	  */
	public BigDecimal getQtyLoc2();

    /** Column name QtyLoc3 */
    public static final String COLUMNNAME_QtyLoc3 = "QtyLoc3";

	/** Set Qty Locator III	  */
	public void setQtyLoc3 (BigDecimal QtyLoc3);

	/** Get Qty Locator III	  */
	public BigDecimal getQtyLoc3();

    /** Column name SealNoFrom */
    public static final String COLUMNNAME_SealNoFrom = "SealNoFrom";

	/** Set Seal No From.
	  * The start number of the seal attched (used)
	  */
	public void setSealNoFrom (int SealNoFrom);

	/** Get Seal No From.
	  * The start number of the seal attched (used)
	  */
	public int getSealNoFrom();

    /** Column name SealNoTo */
    public static final String COLUMNNAME_SealNoTo = "SealNoTo";

	/** Set Seal No To.
	  * The end of seal number attached (used)
	  */
	public void setSealNoTo (int SealNoTo);

	/** Get Seal No To.
	  * The end of seal number attached (used)
	  */
	public int getSealNoTo();

    /** Column name SealNumber */
    public static final String COLUMNNAME_SealNumber = "SealNumber";

	/** Set Seal Number.
	  * The Field is description for seal number
	  */
	public void setSealNumber (String SealNumber);

	/** Get Seal Number.
	  * The Field is description for seal number
	  */
	public String getSealNumber();

    /** Column name SealNumeric */
    public static final String COLUMNNAME_SealNumeric = "SealNumeric";

	/** Set Seal Is Numeric Range.
	  * The record is description for seal
	  */
	public void setSealNumeric (boolean SealNumeric);

	/** Get Seal Is Numeric Range.
	  * The record is description for seal
	  */
	public boolean isSealNumeric();

    /** Column name StartDespatch */
    public static final String COLUMNNAME_StartDespatch = "StartDespatch";

	/** Set Start Despatch.
	  * The starting time despatching the product into the vehicle
	  */
	public void setStartDespatch (Timestamp StartDespatch);

	/** Get Start Despatch.
	  * The starting time despatching the product into the vehicle
	  */
	public Timestamp getStartDespatch();

    /** Column name Temperature */
    public static final String COLUMNNAME_Temperature = "Temperature";

	/** Set Temperature.
	  * The temperature
	  */
	public void setTemperature (int Temperature);

	/** Get Temperature.
	  * The temperature
	  */
	public int getTemperature();

    /** Column name TimeInNumber */
    public static final String COLUMNNAME_TimeInNumber = "TimeInNumber";

	/** Set Time In Number.
	  * Must 4 Number for parsing to date Time In
	  */
	public void setTimeInNumber (String TimeInNumber);

	/** Get Time In Number.
	  * Must 4 Number for parsing to date Time In
	  */
	public String getTimeInNumber();

    /** Column name TimeOutNumber */
    public static final String COLUMNNAME_TimeOutNumber = "TimeOutNumber";

	/** Set Time Out Number.
	  * Must 4 Number for parsing to date Time Out
	  */
	public void setTimeOutNumber (String TimeOutNumber);

	/** Get Time Out Number.
	  * Must 4 Number for parsing to date Time Out
	  */
	public String getTimeOutNumber();

    /** Column name TotalSeal */
    public static final String COLUMNNAME_TotalSeal = "TotalSeal";

	/** Set Total Seal.
	  * The total seal being attched (used)
	  */
	public void setTotalSeal (int TotalSeal);

	/** Get Total Seal.
	  * The total seal being attched (used)
	  */
	public int getTotalSeal();

    /** Column name UNS_Despatch_Notes_ID */
    public static final String COLUMNNAME_UNS_Despatch_Notes_ID = "UNS_Despatch_Notes_ID";

	/** Set SO Despatch Notes	  */
	public void setUNS_Despatch_Notes_ID (int UNS_Despatch_Notes_ID);

	/** Get SO Despatch Notes	  */
	public int getUNS_Despatch_Notes_ID();

    /** Column name UNS_Despatch_Notes_UU */
    public static final String COLUMNNAME_UNS_Despatch_Notes_UU = "UNS_Despatch_Notes_UU";

	/** Set UNS_Despatch_Notes_UU	  */
	public void setUNS_Despatch_Notes_UU (String UNS_Despatch_Notes_UU);

	/** Get UNS_Despatch_Notes_UU	  */
	public String getUNS_Despatch_Notes_UU();

    /** Column name UNS_WeighbridgeTicket_ID */
    public static final String COLUMNNAME_UNS_WeighbridgeTicket_ID = "UNS_WeighbridgeTicket_ID";

	/** Set Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID);

	/** Get Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID();

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VehicleDriverName */
    public static final String COLUMNNAME_VehicleDriverName = "VehicleDriverName";

	/** Set Vehicle Driver Name.
	  * The person name of vehicle by whom it is drived
	  */
	public void setVehicleDriverName (String VehicleDriverName);

	/** Get Vehicle Driver Name.
	  * The person name of vehicle by whom it is drived
	  */
	public String getVehicleDriverName();

    /** Column name VehicleNo */
    public static final String COLUMNNAME_VehicleNo = "VehicleNo";

	/** Set Vehicle No.
	  * The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo);

	/** Get Vehicle No.
	  * The identification number of the vehicle being weighted
	  */
	public String getVehicleNo();
}
