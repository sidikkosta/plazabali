/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_CreditPaySchedule
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CreditPaySchedule extends PO implements I_UNS_CreditPaySchedule, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170503L;

    /** Standard Constructor */
    public X_UNS_CreditPaySchedule (Properties ctx, int UNS_CreditPaySchedule_ID, String trxName)
    {
      super (ctx, UNS_CreditPaySchedule_ID, trxName);
      /** if (UNS_CreditPaySchedule_ID == 0)
        {
			setC_Invoice_ID (0);
			setIsPaid (false);
// N
			setScheduledDate (new Timestamp( System.currentTimeMillis() ));
			setUNS_CreditAgreement_ID (0);
			setUNS_CreditPaySchedule_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CreditPaySchedule (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CreditPaySchedule[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_Invoice)MTable.get(getCtx(), org.compiere.model.I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Invoice Amt.
		@param InvoiceAmt Invoice Amt	  */
	public void setInvoiceAmt (BigDecimal InvoiceAmt)
	{
		set_Value (COLUMNNAME_InvoiceAmt, InvoiceAmt);
	}

	/** Get Invoice Amt.
		@return Invoice Amt	  */
	public BigDecimal getInvoiceAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_InvoiceAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Paid.
		@param IsPaid 
		The document is paid
	  */
	public void setIsPaid (boolean IsPaid)
	{
		set_ValueNoCheck (COLUMNNAME_IsPaid, Boolean.valueOf(IsPaid));
	}

	/** Get Paid.
		@return The document is paid
	  */
	public boolean isPaid () 
	{
		Object oo = get_Value(COLUMNNAME_IsPaid);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Paid Amount.
		@param PaidAmt Paid Amount	  */
	public void setPaidAmt (BigDecimal PaidAmt)
	{
		set_Value (COLUMNNAME_PaidAmt, PaidAmt);
	}

	/** Get Paid Amount.
		@return Paid Amount	  */
	public BigDecimal getPaidAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PaidAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Paid Date.
		@param PaidDate Paid Date	  */
	public void setPaidDate (Timestamp PaidDate)
	{
		set_Value (COLUMNNAME_PaidDate, PaidDate);
	}

	/** Get Paid Date.
		@return Paid Date	  */
	public Timestamp getPaidDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_PaidDate);
	}

	/** Set Reference No.
		@param ReferenceNo 
		Your customer or vendor number at the Business Partner's site
	  */
	public void setReferenceNo (String ReferenceNo)
	{
		set_ValueNoCheck (COLUMNNAME_ReferenceNo, ReferenceNo);
	}

	/** Get Reference No.
		@return Your customer or vendor number at the Business Partner's site
	  */
	public String getReferenceNo () 
	{
		return (String)get_Value(COLUMNNAME_ReferenceNo);
	}

	/** Set Scheduled Date.
		@param ScheduledDate Scheduled Date	  */
	public void setScheduledDate (Timestamp ScheduledDate)
	{
		set_Value (COLUMNNAME_ScheduledDate, ScheduledDate);
	}

	/** Get Scheduled Date.
		@return Scheduled Date	  */
	public Timestamp getScheduledDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ScheduledDate);
	}

	/** Set Credit Agreement.
		@param UNS_CreditAgreement_ID Credit Agreement	  */
	public void setUNS_CreditAgreement_ID (int UNS_CreditAgreement_ID)
	{
		if (UNS_CreditAgreement_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CreditAgreement_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CreditAgreement_ID, Integer.valueOf(UNS_CreditAgreement_ID));
	}

	/** Get Credit Agreement.
		@return Credit Agreement	  */
	public int getUNS_CreditAgreement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CreditAgreement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Credit Pay Schedule.
		@param UNS_CreditPaySchedule_ID Credit Pay Schedule	  */
	public void setUNS_CreditPaySchedule_ID (int UNS_CreditPaySchedule_ID)
	{
		if (UNS_CreditPaySchedule_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CreditPaySchedule_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CreditPaySchedule_ID, Integer.valueOf(UNS_CreditPaySchedule_ID));
	}

	/** Get Credit Pay Schedule.
		@return Credit Pay Schedule	  */
	public int getUNS_CreditPaySchedule_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CreditPaySchedule_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CreditPaySchedule_UU.
		@param UNS_CreditPaySchedule_UU UNS_CreditPaySchedule_UU	  */
	public void setUNS_CreditPaySchedule_UU (String UNS_CreditPaySchedule_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CreditPaySchedule_UU, UNS_CreditPaySchedule_UU);
	}

	/** Get UNS_CreditPaySchedule_UU.
		@return UNS_CreditPaySchedule_UU	  */
	public String getUNS_CreditPaySchedule_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CreditPaySchedule_UU);
	}
}