/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_CashReconciliation
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CashReconciliation extends PO implements I_UNS_CashReconciliation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190916L;

    /** Standard Constructor */
    public X_UNS_CashReconciliation (Properties ctx, int UNS_CashReconciliation_ID, String trxName)
    {
      super (ctx, UNS_CashReconciliation_ID, trxName);
      /** if (UNS_CashReconciliation_ID == 0)
        {
			setEstimationShortCashierAmt (Env.ZERO);
// 0
			setPayableRefundAmt (Env.ZERO);
// 0
			setShortCashierAmt (Env.ZERO);
// 0
			setShortCashierAmtBase1 (Env.ZERO);
// 0
			setShortCashierAmtBase2 (Env.ZERO);
// 0
			setTotalSalesAmt (Env.ZERO);
// 0
			setUNS_CashReconciliation_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CashReconciliation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CashReconciliation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cash Deposit Amount.
		@param CashDepositAmt Cash Deposit Amount	  */
	public void setCashDepositAmt (BigDecimal CashDepositAmt)
	{
		set_Value (COLUMNNAME_CashDepositAmt, CashDepositAmt);
	}

	/** Get Cash Deposit Amount.
		@return Cash Deposit Amount	  */
	public BigDecimal getCashDepositAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashDepositAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Deposit Amount Base 1.
		@param CashDepositAmtB1 Cash Deposit Amount Base 1	  */
	public void setCashDepositAmtB1 (BigDecimal CashDepositAmtB1)
	{
		set_Value (COLUMNNAME_CashDepositAmtB1, CashDepositAmtB1);
	}

	/** Get Cash Deposit Amount Base 1.
		@return Cash Deposit Amount Base 1	  */
	public BigDecimal getCashDepositAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashDepositAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Deposit Amount Base 2.
		@param CashDepositAmtB2 Cash Deposit Amount Base 2	  */
	public void setCashDepositAmtB2 (BigDecimal CashDepositAmtB2)
	{
		set_Value (COLUMNNAME_CashDepositAmtB2, CashDepositAmtB2);
	}

	/** Get Cash Deposit Amount Base 2.
		@return Cash Deposit Amount Base 2	  */
	public BigDecimal getCashDepositAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashDepositAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Estimation Short Cashier Amount.
		@param EstimationShortCashierAmt Estimation Short Cashier Amount	  */
	public void setEstimationShortCashierAmt (BigDecimal EstimationShortCashierAmt)
	{
		set_Value (COLUMNNAME_EstimationShortCashierAmt, EstimationShortCashierAmt);
	}

	/** Get Estimation Short Cashier Amount.
		@return Estimation Short Cashier Amount	  */
	public BigDecimal getEstimationShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EstimationShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payable Refund Amount.
		@param PayableRefundAmt 
		The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt)
	{
		set_Value (COLUMNNAME_PayableRefundAmt, PayableRefundAmt);
	}

	/** Get Payable Refund Amount.
		@return The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount.
		@param ShortCashierAmt 
		The amount differences between cash-fisically with cash to deposit in system.
	  */
	public void setShortCashierAmt (BigDecimal ShortCashierAmt)
	{
		set_Value (COLUMNNAME_ShortCashierAmt, ShortCashierAmt);
	}

	/** Get Short Cashier Amount.
		@return The amount differences between cash-fisically with cash to deposit in system.
	  */
	public BigDecimal getShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount Base 1.
		@param ShortCashierAmtBase1 Short Cashier Amount Base 1	  */
	public void setShortCashierAmtBase1 (BigDecimal ShortCashierAmtBase1)
	{
		set_Value (COLUMNNAME_ShortCashierAmtBase1, ShortCashierAmtBase1);
	}

	/** Get Short Cashier Amount Base 1.
		@return Short Cashier Amount Base 1	  */
	public BigDecimal getShortCashierAmtBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmtBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount Base 2.
		@param ShortCashierAmtBase2 Short Cashier Amount Base 2	  */
	public void setShortCashierAmtBase2 (BigDecimal ShortCashierAmtBase2)
	{
		set_Value (COLUMNNAME_ShortCashierAmtBase2, ShortCashierAmtBase2);
	}

	/** Get Short Cashier Amount Base 2.
		@return Short Cashier Amount Base 2	  */
	public BigDecimal getShortCashierAmtBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmtBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Sales Amount.
		@param TotalSalesAmt Total Sales Amount	  */
	public void setTotalSalesAmt (BigDecimal TotalSalesAmt)
	{
		set_Value (COLUMNNAME_TotalSalesAmt, TotalSalesAmt);
	}

	/** Get Total Sales Amount.
		@return Total Sales Amount	  */
	public BigDecimal getTotalSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalSalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Reconciliation.
		@param UNS_CashReconciliation_ID Cash Reconciliation	  */
	public void setUNS_CashReconciliation_ID (int UNS_CashReconciliation_ID)
	{
		if (UNS_CashReconciliation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CashReconciliation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CashReconciliation_ID, Integer.valueOf(UNS_CashReconciliation_ID));
	}

	/** Get Cash Reconciliation.
		@return Cash Reconciliation	  */
	public int getUNS_CashReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CashReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CashReconciliation_UU.
		@param UNS_CashReconciliation_UU UNS_CashReconciliation_UU	  */
	public void setUNS_CashReconciliation_UU (String UNS_CashReconciliation_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CashReconciliation_UU, UNS_CashReconciliation_UU);
	}

	/** Get UNS_CashReconciliation_UU.
		@return UNS_CashReconciliation_UU	  */
	public String getUNS_CashReconciliation_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CashReconciliation_UU);
	}

	public com.unicore.model.I_UNS_SalesReconciliation getUNS_SalesReconciliation() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_SalesReconciliation)MTable.get(getCtx(), com.unicore.model.I_UNS_SalesReconciliation.Table_Name)
			.getPO(getUNS_SalesReconciliation_ID(), get_TrxName());	}

	/** Set Sales Reconciliation.
		@param UNS_SalesReconciliation_ID Sales Reconciliation	  */
	public void setUNS_SalesReconciliation_ID (int UNS_SalesReconciliation_ID)
	{
		if (UNS_SalesReconciliation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SalesReconciliation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SalesReconciliation_ID, Integer.valueOf(UNS_SalesReconciliation_ID));
	}

	/** Get Sales Reconciliation.
		@return Sales Reconciliation	  */
	public int getUNS_SalesReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SalesReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}