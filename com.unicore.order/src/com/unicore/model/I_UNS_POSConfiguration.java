/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSConfiguration
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSConfiguration 
{

    /** TableName=UNS_POSConfiguration */
    public static final String Table_Name = "UNS_POSConfiguration";

    /** AD_Table_ID=1000399 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Base1Currency_ID */
    public static final String COLUMNNAME_Base1Currency_ID = "Base1Currency_ID";

	/** Set Base 1 Currency	  */
	public void setBase1Currency_ID (int Base1Currency_ID);

	/** Get Base 1 Currency	  */
	public int getBase1Currency_ID();

	public org.compiere.model.I_C_Currency getBase1Currency() throws RuntimeException;

    /** Column name Base2Currency_ID */
    public static final String COLUMNNAME_Base2Currency_ID = "Base2Currency_ID";

	/** Set Base 2 Currency	  */
	public void setBase2Currency_ID (int Base2Currency_ID);

	/** Get Base 2 Currency	  */
	public int getBase2Currency_ID();

	public org.compiere.model.I_C_Currency getBase2Currency() throws RuntimeException;

    /** Column name C_Tax_ID */
    public static final String COLUMNNAME_C_Tax_ID = "C_Tax_ID";

	/** Set Tax.
	  * Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID);

	/** Get Tax.
	  * Tax identifier
	  */
	public int getC_Tax_ID();

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name MinBarcodeLength */
    public static final String COLUMNNAME_MinBarcodeLength = "MinBarcodeLength";

	/** Set Min Barcode Length	  */
	public void setMinBarcodeLength (int MinBarcodeLength);

	/** Get Min Barcode Length	  */
	public int getMinBarcodeLength();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name PaymentRounding */
    public static final String COLUMNNAME_PaymentRounding = "PaymentRounding";

	/** Set Payment Rounding	  */
	public void setPaymentRounding (String PaymentRounding);

	/** Get Payment Rounding	  */
	public String getPaymentRounding();

    /** Column name ServiceChargeRate */
    public static final String COLUMNNAME_ServiceChargeRate = "ServiceChargeRate";

	/** Set Service Charge Rate	  */
	public void setServiceChargeRate (BigDecimal ServiceChargeRate);

	/** Get Service Charge Rate	  */
	public BigDecimal getServiceChargeRate();

    /** Column name UNS_POSConfiguration_ID */
    public static final String COLUMNNAME_UNS_POSConfiguration_ID = "UNS_POSConfiguration_ID";

	/** Set Global POS Configuration	  */
	public void setUNS_POSConfiguration_ID (int UNS_POSConfiguration_ID);

	/** Get Global POS Configuration	  */
	public int getUNS_POSConfiguration_ID();

    /** Column name UNS_POSConfiguration_UU */
    public static final String COLUMNNAME_UNS_POSConfiguration_UU = "UNS_POSConfiguration_UU";

	/** Set UNS_POSConfiguration_UU	  */
	public void setUNS_POSConfiguration_UU (String UNS_POSConfiguration_UU);

	/** Get UNS_POSConfiguration_UU	  */
	public String getUNS_POSConfiguration_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
