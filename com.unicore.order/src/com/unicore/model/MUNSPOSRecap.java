/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPOSRecap extends X_UNS_POSRecap {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1675180487939473122L;

	/**
	 * @param ctx
	 * @param UNS_POSRecap_ID
	 * @param trxName
	 */
	public MUNSPOSRecap(Properties ctx, int UNS_POSRecap_ID, String trxName) {
		super(ctx, UNS_POSRecap_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSRecap(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		
		return true;
	}
	
	public MUNSPOSRecap(MUNSPOSTrx pos, int Store_ID, MUNSSalesReconciliation recon)
	{
		super(pos.getCtx(), 0, pos.get_TrxName());
		
		setReconciliation(recon);
		setAD_Org_ID(pos.getAD_Org_ID());
		setStore_ID(Store_ID);
		setTotalCostAmt(Env.ZERO);
		setTotalDiscAmt(Env.ZERO);
		setTotalRevenueAmt(Env.ZERO);
		setTotalSalesAmt(Env.ZERO);
		setTotalServiceChargeAmt(Env.ZERO);
	}
	
	public MUNSPOSRecap(MUNSPOSSession ss, int Store_ID)
	{
		super(ss.getCtx(), 0, ss.get_TrxName());
		
		MUNSSalesReconciliation recon = MUNSSalesReconciliation.getCreate(ss.getCtx(),
				Store_ID, ss.getDateAcct(), ss.getAD_Org_ID(), ss.getC_Currency_ID(), ss.get_TrxName());
		
		setReconciliation(recon);
		setAD_Org_ID(ss.getAD_Org_ID());
		setStore_ID(Store_ID);
		setTotalCostAmt(Env.ZERO);
		setTotalDiscAmt(Env.ZERO);
		setTotalRevenueAmt(Env.ZERO);
		setTotalSalesAmt(Env.ZERO);
		setTotalServiceChargeAmt(Env.ZERO);
		setTotalShortCashierAmt(Env.ZERO);
		setTotalShortCashierAmt1(Env.ZERO);
		setTotalEstimationShortCashierAmt(Env.ZERO);
	}
	
	private void setReconciliation(MUNSSalesReconciliation recon)
	{
		setUNS_SalesReconciliation_ID(recon.get_ID());
		setDateTrx(recon.getDateTrx());
	}
	
	public static MUNSPOSRecap getCreate(MUNSPOSTrx pos)
	{
		String sql = "SELECT Store_ID FROM UNS_POSTerminal WHERE UNS_POSTerminal_ID ="
				+ " (SELECT UNS_POSTerminal_ID FROM UNS_POS_Session WHERE UNS_POS_Session_ID = ?)";
		int store = DB.getSQLValue(pos.get_TrxName(), sql, pos.getUNS_POS_Session_ID());
		MUNSSalesReconciliation recon = MUNSSalesReconciliation.getCreate(pos.getCtx(),
				store, pos.getDateAcct(), pos.getAD_Org_ID(), pos.getC_Currency_ID(), pos.get_TrxName());
		String whereClause = "Store_ID = ? AND DateTrx = ? AND UNS_SalesReconciliation_ID = ?";
		MUNSPOSRecap recap = Query.get(pos.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				whereClause, pos.get_TrxName()).setParameters(store, pos.getDateAcct(), recon.get_ID())
				.firstOnly();
		
		if(recap == null)
		{
			recap = new MUNSPOSRecap(pos, store, recon);
			recap.saveEx();
		}
		
		return recap;
	}
	
	public MUNSPOSRecapLine[] getLines()
	{
		List<MUNSPOSRecapLine> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSPOSRecapLine.Table_Name, Table_Name + "_ID = ?", get_TrxName()).setParameters(get_ID()).list();
		
		return list.toArray(new MUNSPOSRecapLine[list.size()]);
	}
	
	public static String createFrom(MUNSPOSSession ss)
	{
		int store = ss.getUNS_POSTerminal().getStore_ID();
		MUNSSalesReconciliation recon = MUNSSalesReconciliation.getCreate(ss.getCtx(),
				store, ss.getDateAcct(), ss.getAD_Org_ID(), ss.getBase1Currency_ID(), ss.get_TrxName());
		String whereClause = "Store_ID = ? AND DateTrx = ? AND UNS_SalesReconciliation_ID = ?";
		MUNSPOSRecap recap = Query.get(ss.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				whereClause, ss.get_TrxName()).setParameters(store, ss.getDateAcct(), recon.get_ID())
				.firstOnly();
		
		if(recap == null)
		{
			recap = new MUNSPOSRecap(ss, store);
			recap.saveEx();
		}
		
		recap.setTotalSalesAmt(recap.getTotalSalesAmt().add(ss.getTotalSalesB1()));
		recap.setTotalDiscAmt(recap.getTotalDiscAmt().add(ss.getTotalDiscAmt()));
		recap.setTotalServiceChargeAmt(recap.getTotalServiceChargeAmt().add(ss.getTotalServiceCharge()));
		if(ss.getTotalShortCashierAmt().signum() < 0)
			recap.setTotalShortCashierAmt(recap.getTotalShortCashierAmt().add(ss.getTotalShortCashierAmt()));
		else if(ss.getTotalShortCashierAmt().signum() > 0)
			recap.setTotalShortCashierAmt1(recap.getTotalShortCashierAmt1().add(ss.getTotalShortCashierAmt()));
		recap.setTotalEstimationShortCashierAmt(
				recap.getTotalEstimationShortCashierAmt().add(ss.getTotalEstimationShortCashierAmt()));
		recap.setTotalTaxAmt(recap.getTotalTaxAmt().add(ss.getTotalTaxAmt()));
		recap.setTotalVoucherAmt(recap.getTotalVoucherAmt().add(ss.getVoucherAmt()));
		recap.setReturnAmt(recap.getReturnAmt().add(ss.getReturnAmt()));
		if(!recap.save())
			return "Failed when trying create recapitulations.";
		
		return null;
	}
}