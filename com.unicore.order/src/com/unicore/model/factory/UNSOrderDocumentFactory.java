/**
 * 
 */
package com.unicore.model.factory;

import java.sql.ResultSet;

import org.compiere.acct.Doc;
import org.compiere.model.MAcctSchema;

import com.unicore.base.model.MInvoice;
import com.unicore.model.MUNSBonusClaim;
import com.unicore.model.MUNSSalesReconciliation;
import com.unicore.model.MUNSShipping;
import com.unicore.model.MUNSShortCashierCorrection;
import com.unicore.model.MUNSStatementOfAccount;
import com.unicore.model.MUNSVoucherBook;
import com.unicore.model.MUNSVoucherCorrection;
import com.unicore.model.acct.Doc_UNSBonusClaim;
import com.unicore.model.acct.Doc_UNSSalesReconciliation;
import com.unicore.model.acct.Doc_UNSShortCashierCorrection;
import com.unicore.model.acct.Doc_UNSStatementOfAccount;
import com.unicore.model.acct.Doc_UNSVoucherCorrection;
import com.unicore.model.doc.DOC_UNSVoucher;
import com.unicore.model.doc.Doc_Invoice;
import com.uns.base.UNSAbstractDocumentFactory;
import com.uns.model.acct.Doc_UNSChargeRS;

/**
 * @author user
 *
 */
public class UNSOrderDocumentFactory extends UNSAbstractDocumentFactory
{

	/**
	 * 
	 */
	public UNSOrderDocumentFactory()
	{
		// TODO Auto-generated constructor stub
	}


	@Override
	public Doc getDocument(MAcctSchema as, int AD_Table_ID, ResultSet rs, String trxName)
	{
		Doc doc = null;
		if(MUNSBonusClaim.Table_ID == AD_Table_ID)
			doc = new Doc_UNSBonusClaim(as, rs, trxName);
		else if(MUNSShipping.Table_ID == AD_Table_ID)
			doc = new Doc_UNSChargeRS(as, MUNSShipping.class, rs, null, trxName);
		else if(MUNSSalesReconciliation.Table_ID == AD_Table_ID)
		{
			doc = new Doc_UNSSalesReconciliation(as, MUNSSalesReconciliation.class, rs, "SRC", trxName);
			doc.setIsMultiCurrency(true);
		}
		else if (MUNSVoucherBook.Table_ID == AD_Table_ID)
			doc = new DOC_UNSVoucher(as, rs, trxName);
		else if (MUNSStatementOfAccount.Table_ID == AD_Table_ID)
			doc = new Doc_UNSStatementOfAccount(as, MUNSStatementOfAccount.class, rs, "SOA", trxName);
		else if (MInvoice.Table_ID == AD_Table_ID)
			doc = new Doc_Invoice(as, rs, trxName);
		else if (MUNSShortCashierCorrection.Table_ID == AD_Table_ID)
			doc = new Doc_UNSShortCashierCorrection(as, MUNSShortCashierCorrection.class, rs, "SCC", trxName);
		else if (MUNSVoucherCorrection.Table_ID == AD_Table_ID)
			doc = new Doc_UNSVoucherCorrection(as, MUNSVoucherCorrection.class, rs, "VCC", trxName);
		else
			doc = super.getDocument(as, AD_Table_ID, rs, trxName);
		
		return doc;
	}
	
	/* (non-Javadoc)
	 * @see com.uns.base.UNSAbstractDocumentFactory#getInternalClass(java.lang.String)
	 */
	@Override
	protected Class<?> getInternalClass(String className) throws ClassNotFoundException
	{
		Class<?> clazz = Class.forName(className);
		return clazz;
	}
}