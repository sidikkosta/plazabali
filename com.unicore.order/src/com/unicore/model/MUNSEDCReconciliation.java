/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSEDCReconciliation extends X_UNS_EDCReconciliation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1358976037110051186L;

	/**
	 * @param ctx
	 * @param UNS_EDCReconciliation_ID
	 * @param trxName
	 */
	public MUNSEDCReconciliation(Properties ctx, int UNS_EDCReconciliation_ID,
			String trxName) {
		super(ctx, UNS_EDCReconciliation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEDCReconciliation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSEDCReconciliation getCreate(Properties ctx, 
			int C_BPartner_ID, java.sql.Timestamp dateTrx, int UNS_CardType_ID, int AD_Org_ID, int BaseCurrency_ID, String trxName)
	{
		MUNSSalesReconciliation salesRecon = MUNSSalesReconciliation.getCreate(ctx, C_BPartner_ID, dateTrx,
				AD_Org_ID, BaseCurrency_ID, trxName);
		String whereClause = "UNS_CardType_ID = ? AND UNS_SalesReconciliation_ID = ?";
		MUNSEDCReconciliation edcRecon = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(UNS_CardType_ID, salesRecon.get_ID())
				.firstOnly();
		
		if(edcRecon == null)
		{
			edcRecon = new MUNSEDCReconciliation(ctx, 0, trxName);
			edcRecon.setUNS_SalesReconciliation_ID(salesRecon.get_ID());
			edcRecon.setUNS_CardType_ID(UNS_CardType_ID);
			edcRecon.setTotalAmt(Env.ZERO);
			edcRecon.setPayableRefundAmt(Env.ZERO);
			edcRecon.saveEx();
		}
		
		return edcRecon;
	}
	
	public static String createFrom(MUNSPOSSession ss, boolean reverse)
	{
		MUNSSessionEDCSummary[] edc = ss.getEDCSummary(true);
		
		for(int i=0;i<edc.length;i++)
		{
			if(reverse)
			{
				edc[i].setTotalAmt(edc[i].getTotalAmt().negate());
				edc[i].setPayableRefundAmt(edc[i].getPayableRefundAmt().negate());
			}
			MUNSEDCReconciliation recon = MUNSEDCReconciliation.getCreate(ss.getCtx(), ss.getUNS_POSTerminal().getStore_ID(),
					ss.getDateAcct(), edc[i].getUNS_CardType_ID(), ss.getAD_Org_ID(), ss.getC_Currency_ID(), ss.get_TrxName());
			recon.setTotalAmt(recon.getTotalAmt().add(edc[i].getTotalAmt()));
			recon.setPayableRefundAmt(recon.getPayableRefundAmt().add(edc[i].getPayableRefundAmt()));
			if(!recon.save())
				return "Failed when trying create edc reconciliation";
		}
		
		return null;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		String sql = "UPDATE UNS_SalesReconciliation sr SET TotalEDCAmt ="
				+ " (SELECT COALESCE(SUM(er.TotalAmt),0) FROM UNS_EDCReconciliation er"
				+ " WHERE er.UNS_SalesReconciliation_ID = sr.UNS_SalesReconciliation_ID)"
				+ " WHERE sr.UNS_SalesReconciliation_ID = ?";
		
		if(DB.executeUpdate(sql, getUNS_SalesReconciliation_ID(), get_TrxName()) < 0)
		{
			log.saveError("Error", "Failed when trying update total sales amount on sales reconciliation.");
			return false;
		}
		
		MUNSSalesReconciliation.reCalcGrandTotal(getUNS_SalesReconciliation_ID(), get_TrxName());
		
		return true;
	}
}