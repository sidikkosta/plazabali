package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

public class MUNSPOSConfigurationCurr extends X_UNS_POSConfiguration_Curr {

	/**
	 * 
	 */
	private static final long serialVersionUID = 725528238781507989L;
	MUNSPOSConfiguration m_parent = null;
	public MUNSPOSConfiguration getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPOSConfiguration(getCtx(), getUNS_POSConfiguration_ID(), get_TrxName());
		return m_parent;
	}
	public MUNSPOSConfigurationCurr(Properties ctx,
			int UNS_POSConfiguration_Curr_ID, String trxName) {
		super(ctx, UNS_POSConfiguration_Curr_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSPOSConfigurationCurr(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSPOSConfigurationCurr get(Properties ctx, int AD_Org_ID, int C_Currency_ID, String trxName)
	{
		MUNSPOSConfigurationCurr curr = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, COLUMNNAME_AD_Org_ID + "=? AND " + COLUMNNAME_C_Currency_ID + "=?", trxName)
					.setParameters(AD_Org_ID, C_Currency_ID).firstOnly();
		
		if(curr == null)
		{
			curr = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
					Table_Name, COLUMNNAME_AD_Org_ID + "=? AND " + COLUMNNAME_C_Currency_ID + "=?", trxName)
						.setParameters(0, C_Currency_ID).firstOnly();
		}
		
		return curr;
	}
	
	public static BigDecimal getMinimumTrxAmt (int AD_Org_ID, int C_Currency_ID, String trxName)
	{
		String sql = "SELECT MinimumAmt FROM UNS_POSConfiguration_Curr WHERE AD_Org_ID IN (0,?) "
				+ " AND C_Currency_ID = ? ORDER BY AD_Org_ID DESC";
		BigDecimal minAmt = DB.getSQLValueBD(trxName, sql, AD_Org_ID, C_Currency_ID);
		if (minAmt == null)
			minAmt = Env.ZERO;
		return minAmt;
	}
}