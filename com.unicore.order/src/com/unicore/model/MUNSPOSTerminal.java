/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSPOSTerminal extends X_UNS_POSTerminal {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1061302456108450776L;

	public static MUNSPOSTerminal get (String trxName, String MACAddress)
	{
		return Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				Table_Name, COLUMNNAME_MACAddress + " + ?", trxName).
				setParameters(MACAddress).setOnlyActiveRecords(true).first();
	}
	
	public static int getIDByMAC (String trxName, String MACAddress)
	{
		String sql = "SELECT UNS_POSTerminal_ID FROM UNS_POSTerminal WHERE MACAddress = ?";
		int mac = DB.getSQLValue(trxName, sql, MACAddress);
		return mac;
	}
	
	/**
	 * @param ctx
	 * @param UNS_POSTerminal_ID
	 * @param trxName
	 */
	public MUNSPOSTerminal(Properties ctx, int UNS_POSTerminal_ID,
			String trxName) 
	{
		super(ctx, UNS_POSTerminal_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSTerminal(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		
		return super.afterSave(newRecord, success);
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord || is_ValueChanged(COLUMNNAME_MACAddress) || is_ValueChanged(COLUMNNAME_IsActive))
		{
			String sql = "SELECT Name FROM UNS_POSTerminal WHERE MACAddress = ?"
					+ " AND UNS_POSTerminal_ID <> ? AND IsActive = 'Y'";
			String name = DB.getSQLValueString(get_TrxName(), sql, getMACAddress(), get_ID());
			
			if(!Util.isEmpty(name, true))
			{
				log.saveError("Error", "Duplicate mac address. " + name);
				return false;
			}
		}
		
		return true;
	}
}