/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSTrxLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSTrxLine 
{

    /** TableName=UNS_POSTrxLine */
    public static final String Table_Name = "UNS_POSTrxLine";

    /** AD_Table_ID=1000388 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Barcode */
    public static final String COLUMNNAME_Barcode = "Barcode";

	/** Set Barcode	  */
	public void setBarcode (String Barcode);

	/** Get Barcode	  */
	public String getBarcode();

    /** Column name C_Charge_ID */
    public static final String COLUMNNAME_C_Charge_ID = "C_Charge_ID";

	/** Set Charge.
	  * Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID);

	/** Get Charge.
	  * Additional document charges
	  */
	public int getC_Charge_ID();

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException;

    /** Column name C_Tax_ID */
    public static final String COLUMNNAME_C_Tax_ID = "C_Tax_ID";

	/** Set Tax.
	  * Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID);

	/** Get Tax.
	  * Tax identifier
	  */
	public int getC_Tax_ID();

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DiscNote */
    public static final String COLUMNNAME_DiscNote = "DiscNote";

	/** Set Discount Note	  */
	public void setDiscNote (String DiscNote);

	/** Get Discount Note	  */
	public String getDiscNote();

    /** Column name Discount */
    public static final String COLUMNNAME_Discount = "Discount";

	/** Set Discount %.
	  * Discount in percent
	  */
	public void setDiscount (BigDecimal Discount);

	/** Get Discount %.
	  * Discount in percent
	  */
	public BigDecimal getDiscount();

    /** Column name DiscountAmt */
    public static final String COLUMNNAME_DiscountAmt = "DiscountAmt";

	/** Set Discount Amount.
	  * Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt);

	/** Get Discount Amount.
	  * Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt();

    /** Column name DiscReff */
    public static final String COLUMNNAME_DiscReff = "DiscReff";

	/** Set Discount Refference	  */
	public void setDiscReff (String DiscReff);

	/** Get Discount Refference	  */
	public String getDiscReff();

    /** Column name ETBBCode */
    public static final String COLUMNNAME_ETBBCode = "ETBBCode";

	/** Set ETBB Code	  */
	public void setETBBCode (String ETBBCode);

	/** Get ETBB Code	  */
	public String getETBBCode();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsBOM */
    public static final String COLUMNNAME_IsBOM = "IsBOM";

	/** Set Bill of Materials.
	  * Bill of Materials
	  */
	public void setIsBOM (boolean IsBOM);

	/** Get Bill of Materials.
	  * Bill of Materials
	  */
	public boolean isBOM();

    /** Column name isProductBonuses */
    public static final String COLUMNNAME_isProductBonuses = "isProductBonuses";

	/** Set Product Bonuses.
	  * Product Bonuses
	  */
	public void setisProductBonuses (boolean isProductBonuses);

	/** Get Product Bonuses.
	  * Product Bonuses
	  */
	public boolean isProductBonuses();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name LineAmt */
    public static final String COLUMNNAME_LineAmt = "LineAmt";

	/** Set Line Amount.
	  * The amount for this line, generally it is after discount amount.
	  */
	public void setLineAmt (BigDecimal LineAmt);

	/** Get Line Amount.
	  * The amount for this line, generally it is after discount amount.
	  */
	public BigDecimal getLineAmt();

    /** Column name LineNetAmt */
    public static final String COLUMNNAME_LineNetAmt = "LineNetAmt";

	/** Set Line Amount.
	  * Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public void setLineNetAmt (BigDecimal LineNetAmt);

	/** Get Line Amount.
	  * Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public BigDecimal getLineNetAmt();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name PriceActual */
    public static final String COLUMNNAME_PriceActual = "PriceActual";

	/** Set Unit Price.
	  * Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual);

	/** Get Unit Price.
	  * Actual Price
	  */
	public BigDecimal getPriceActual();

    /** Column name PriceLimit */
    public static final String COLUMNNAME_PriceLimit = "PriceLimit";

	/** Set Limit Price.
	  * Lowest price for a product
	  */
	public void setPriceLimit (BigDecimal PriceLimit);

	/** Get Limit Price.
	  * Lowest price for a product
	  */
	public BigDecimal getPriceLimit();

    /** Column name PriceList */
    public static final String COLUMNNAME_PriceList = "PriceList";

	/** Set List Price.
	  * List Price
	  */
	public void setPriceList (BigDecimal PriceList);

	/** Get List Price.
	  * List Price
	  */
	public BigDecimal getPriceList();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name QtyBonuses */
    public static final String COLUMNNAME_QtyBonuses = "QtyBonuses";

	/** Set Bonuses Qty.
	  * Bonuses Qty
	  */
	public void setQtyBonuses (BigDecimal QtyBonuses);

	/** Get Bonuses Qty.
	  * Bonuses Qty
	  */
	public BigDecimal getQtyBonuses();

    /** Column name QtyEntered */
    public static final String COLUMNNAME_QtyEntered = "QtyEntered";

	/** Set Quantity.
	  * The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered);

	/** Get Quantity.
	  * The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered();

    /** Column name QtyOrdered */
    public static final String COLUMNNAME_QtyOrdered = "QtyOrdered";

	/** Set Ordered Quantity.
	  * Ordered Quantity
	  */
	public void setQtyOrdered (BigDecimal QtyOrdered);

	/** Get Ordered Quantity.
	  * Ordered Quantity
	  */
	public BigDecimal getQtyOrdered();

    /** Column name Reference_ID */
    public static final String COLUMNNAME_Reference_ID = "Reference_ID";

	/** Set Refrerence Record	  */
	public void setReference_ID (int Reference_ID);

	/** Get Refrerence Record	  */
	public int getReference_ID();

	public com.unicore.model.I_UNS_POSTrxLine getReference() throws RuntimeException;

    /** Column name ServiceCharge */
    public static final String COLUMNNAME_ServiceCharge = "ServiceCharge";

	/** Set Service Charge	  */
	public void setServiceCharge (BigDecimal ServiceCharge);

	/** Get Service Charge	  */
	public BigDecimal getServiceCharge();

    /** Column name SKU */
    public static final String COLUMNNAME_SKU = "SKU";

	/** Set SKU.
	  * Stock Keeping Unit
	  */
	public void setSKU (String SKU);

	/** Get SKU.
	  * Stock Keeping Unit
	  */
	public String getSKU();

    /** Column name TaxAmt */
    public static final String COLUMNNAME_TaxAmt = "TaxAmt";

	/** Set Tax Amount.
	  * Tax Amount for a document
	  */
	public void setTaxAmt (BigDecimal TaxAmt);

	/** Get Tax Amount.
	  * Tax Amount for a document
	  */
	public BigDecimal getTaxAmt();

    /** Column name UNS_POSRecapLine_ID */
    public static final String COLUMNNAME_UNS_POSRecapLine_ID = "UNS_POSRecapLine_ID";

	/** Set POS Recapitulation Line	  */
	public void setUNS_POSRecapLine_ID (int UNS_POSRecapLine_ID);

	/** Get POS Recapitulation Line	  */
	public int getUNS_POSRecapLine_ID();

    /** Column name UNS_POSTrx_ID */
    public static final String COLUMNNAME_UNS_POSTrx_ID = "UNS_POSTrx_ID";

	/** Set POS Transactions	  */
	public void setUNS_POSTrx_ID (int UNS_POSTrx_ID);

	/** Get POS Transactions	  */
	public int getUNS_POSTrx_ID();

	public com.unicore.model.I_UNS_POSTrx getUNS_POSTrx() throws RuntimeException;

    /** Column name UNS_POSTrxLine_ID */
    public static final String COLUMNNAME_UNS_POSTrxLine_ID = "UNS_POSTrxLine_ID";

	/** Set POS Trx Line	  */
	public void setUNS_POSTrxLine_ID (int UNS_POSTrxLine_ID);

	/** Get POS Trx Line	  */
	public int getUNS_POSTrxLine_ID();

    /** Column name UNS_POSTrxLine_UU */
    public static final String COLUMNNAME_UNS_POSTrxLine_UU = "UNS_POSTrxLine_UU";

	/** Set UNS_POSTrxLine_UU	  */
	public void setUNS_POSTrxLine_UU (String UNS_POSTrxLine_UU);

	/** Get UNS_POSTrxLine_UU	  */
	public String getUNS_POSTrxLine_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VoidLine_ID */
    public static final String COLUMNNAME_VoidLine_ID = "VoidLine_ID";

	/** Set Voided Line.
	  * Voided Line
	  */
	public void setVoidLine_ID (int VoidLine_ID);

	/** Get Voided Line.
	  * Voided Line
	  */
	public int getVoidLine_ID();

	public com.unicore.model.I_UNS_POSTrxLine getVoidLine() throws RuntimeException;
}
