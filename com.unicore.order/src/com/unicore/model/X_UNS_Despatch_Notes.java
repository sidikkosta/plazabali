/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Despatch_Notes
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Despatch_Notes extends PO implements I_UNS_Despatch_Notes, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170322L;

    /** Standard Constructor */
    public X_UNS_Despatch_Notes (Properties ctx, int UNS_Despatch_Notes_ID, String trxName)
    {
      super (ctx, UNS_Despatch_Notes_ID, trxName);
      /** if (UNS_Despatch_Notes_ID == 0)
        {
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setIsQtyPercentage (false);
// N
			setProcessed (false);
// N
			setUNS_Despatch_Notes_ID (0);
			setUNS_WeighbridgeTicket_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Despatch_Notes (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Despatch_Notes[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_Value (COLUMNNAME_AD_User_ID, null);
		else 
			set_Value (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Assigned By.
		@param AssignedBy 
		The person / contact to assign the document
	  */
	public void setAssignedBy (String AssignedBy)
	{
		set_Value (COLUMNNAME_AssignedBy, AssignedBy);
	}

	/** Get Assigned By.
		@return The person / contact to assign the document
	  */
	public String getAssignedBy () 
	{
		return (String)get_Value(COLUMNNAME_AssignedBy);
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		throw new IllegalArgumentException ("C_Order_ID is virtual column");	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Dirt.
		@param Dirt 
		Dirt
	  */
	public void setDirt (BigDecimal Dirt)
	{
		set_Value (COLUMNNAME_Dirt, Dirt);
	}

	/** Get Dirt.
		@return Dirt
	  */
	public BigDecimal getDirt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Dirt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_ValueNoCheck (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set End Despatch.
		@param EndDespatch 
		The ending time of despatching product into a vehicle
	  */
	public void setEndDespatch (Timestamp EndDespatch)
	{
		set_Value (COLUMNNAME_EndDespatch, EndDespatch);
	}

	/** Get End Despatch.
		@return The ending time of despatching product into a vehicle
	  */
	public Timestamp getEndDespatch () 
	{
		return (Timestamp)get_Value(COLUMNNAME_EndDespatch);
	}

	/** Set FFA.
		@param FFA 
		Free Fatty Acid
	  */
	public void setFFA (BigDecimal FFA)
	{
		set_Value (COLUMNNAME_FFA, FFA);
	}

	/** Get FFA.
		@return Free Fatty Acid
	  */
	public BigDecimal getFFA () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FFA);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_ValueNoCheck (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Overwrite Despatch Time.
		@param IsOverwriteDespatchTime Overwrite Despatch Time	  */
	public void setIsOverwriteDespatchTime (boolean IsOverwriteDespatchTime)
	{
		set_Value (COLUMNNAME_IsOverwriteDespatchTime, Boolean.valueOf(IsOverwriteDespatchTime));
	}

	/** Get Overwrite Despatch Time.
		@return Overwrite Despatch Time	  */
	public boolean isOverwriteDespatchTime () 
	{
		Object oo = get_Value(COLUMNNAME_IsOverwriteDespatchTime);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Qty Percentage.
		@param IsQtyPercentage 
		Indicate that this component is based in % Quantity
	  */
	public void setIsQtyPercentage (boolean IsQtyPercentage)
	{
		set_Value (COLUMNNAME_IsQtyPercentage, Boolean.valueOf(IsQtyPercentage));
	}

	/** Get Is Qty Percentage.
		@return Indicate that this component is based in % Quantity
	  */
	public boolean isQtyPercentage () 
	{
		Object oo = get_Value(COLUMNNAME_IsQtyPercentage);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Locator getLocator1() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getLocator1_ID(), get_TrxName());	}

	/** Set Locator I.
		@param Locator1_ID Locator I	  */
	public void setLocator1_ID (int Locator1_ID)
	{
		if (Locator1_ID < 1) 
			set_Value (COLUMNNAME_Locator1_ID, null);
		else 
			set_Value (COLUMNNAME_Locator1_ID, Integer.valueOf(Locator1_ID));
	}

	/** Get Locator I.
		@return Locator I	  */
	public int getLocator1_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Locator1_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Locator getLocator2() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getLocator2_ID(), get_TrxName());	}

	/** Set Locator II.
		@param Locator2_ID Locator II	  */
	public void setLocator2_ID (int Locator2_ID)
	{
		if (Locator2_ID < 1) 
			set_Value (COLUMNNAME_Locator2_ID, null);
		else 
			set_Value (COLUMNNAME_Locator2_ID, Integer.valueOf(Locator2_ID));
	}

	/** Get Locator II.
		@return Locator II	  */
	public int getLocator2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Locator2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Locator getLocator3() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getLocator3_ID(), get_TrxName());	}

	/** Set Locator III.
		@param Locator3_ID Locator III	  */
	public void setLocator3_ID (int Locator3_ID)
	{
		if (Locator3_ID < 1) 
			set_Value (COLUMNNAME_Locator3_ID, null);
		else 
			set_Value (COLUMNNAME_Locator3_ID, Integer.valueOf(Locator3_ID));
	}

	/** Get Locator III.
		@return Locator III	  */
	public int getLocator3_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Locator3_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set MI.
		@param MI 
		Moisture & Impurity
	  */
	public void setMI (BigDecimal MI)
	{
		set_Value (COLUMNNAME_MI, MI);
	}

	/** Get MI.
		@return Moisture & Impurity
	  */
	public BigDecimal getMI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Moisture.
		@param Moisture 
		Moisture
	  */
	public void setMoisture (BigDecimal Moisture)
	{
		set_Value (COLUMNNAME_Moisture, Moisture);
	}

	/** Get Moisture.
		@return Moisture
	  */
	public BigDecimal getMoisture () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Moisture);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Percentage Locator I.
		@param PercentageLoc1 Percentage Locator I	  */
	public void setPercentageLoc1 (BigDecimal PercentageLoc1)
	{
		set_Value (COLUMNNAME_PercentageLoc1, PercentageLoc1);
	}

	/** Get Percentage Locator I.
		@return Percentage Locator I	  */
	public BigDecimal getPercentageLoc1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PercentageLoc1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Percentage Locator II.
		@param PercentageLoc2 Percentage Locator II	  */
	public void setPercentageLoc2 (BigDecimal PercentageLoc2)
	{
		set_Value (COLUMNNAME_PercentageLoc2, PercentageLoc2);
	}

	/** Get Percentage Locator II.
		@return Percentage Locator II	  */
	public BigDecimal getPercentageLoc2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PercentageLoc2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Percentage Locator III.
		@param PercentageLoc3 Percentage Locator III	  */
	public void setPercentageLoc3 (BigDecimal PercentageLoc3)
	{
		set_Value (COLUMNNAME_PercentageLoc3, PercentageLoc3);
	}

	/** Get Percentage Locator III.
		@return Percentage Locator III	  */
	public BigDecimal getPercentageLoc3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PercentageLoc3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Order Reference.
		@param POReference 
		Transaction Reference Number (Sales Order, Purchase Order) of your Business Partner
	  */
	public void setPOReference (String POReference)
	{
		throw new IllegalArgumentException ("POReference is virtual column");	}

	/** Get Order Reference.
		@return Transaction Reference Number (Sales Order, Purchase Order) of your Business Partner
	  */
	public String getPOReference () 
	{
		return (String)get_Value(COLUMNNAME_POReference);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_ValueNoCheck (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Qty Locator I.
		@param QtyLoc1 Qty Locator I	  */
	public void setQtyLoc1 (BigDecimal QtyLoc1)
	{
		set_Value (COLUMNNAME_QtyLoc1, QtyLoc1);
	}

	/** Get Qty Locator I.
		@return Qty Locator I	  */
	public BigDecimal getQtyLoc1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyLoc1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Locator II.
		@param QtyLoc2 Qty Locator II	  */
	public void setQtyLoc2 (BigDecimal QtyLoc2)
	{
		set_Value (COLUMNNAME_QtyLoc2, QtyLoc2);
	}

	/** Get Qty Locator II.
		@return Qty Locator II	  */
	public BigDecimal getQtyLoc2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyLoc2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Locator III.
		@param QtyLoc3 Qty Locator III	  */
	public void setQtyLoc3 (BigDecimal QtyLoc3)
	{
		set_Value (COLUMNNAME_QtyLoc3, QtyLoc3);
	}

	/** Get Qty Locator III.
		@return Qty Locator III	  */
	public BigDecimal getQtyLoc3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyLoc3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Seal No From.
		@param SealNoFrom 
		The start number of the seal attched (used)
	  */
	public void setSealNoFrom (int SealNoFrom)
	{
		set_Value (COLUMNNAME_SealNoFrom, Integer.valueOf(SealNoFrom));
	}

	/** Get Seal No From.
		@return The start number of the seal attched (used)
	  */
	public int getSealNoFrom () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SealNoFrom);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Seal No To.
		@param SealNoTo 
		The end of seal number attached (used)
	  */
	public void setSealNoTo (int SealNoTo)
	{
		set_Value (COLUMNNAME_SealNoTo, Integer.valueOf(SealNoTo));
	}

	/** Get Seal No To.
		@return The end of seal number attached (used)
	  */
	public int getSealNoTo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SealNoTo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Seal Number.
		@param SealNumber 
		The Field is description for seal number
	  */
	public void setSealNumber (String SealNumber)
	{
		set_Value (COLUMNNAME_SealNumber, SealNumber);
	}

	/** Get Seal Number.
		@return The Field is description for seal number
	  */
	public String getSealNumber () 
	{
		return (String)get_Value(COLUMNNAME_SealNumber);
	}

	/** Set Seal Is Numeric Range.
		@param SealNumeric 
		The record is description for seal
	  */
	public void setSealNumeric (boolean SealNumeric)
	{
		set_Value (COLUMNNAME_SealNumeric, Boolean.valueOf(SealNumeric));
	}

	/** Get Seal Is Numeric Range.
		@return The record is description for seal
	  */
	public boolean isSealNumeric () 
	{
		Object oo = get_Value(COLUMNNAME_SealNumeric);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Start Despatch.
		@param StartDespatch 
		The starting time despatching the product into the vehicle
	  */
	public void setStartDespatch (Timestamp StartDespatch)
	{
		set_Value (COLUMNNAME_StartDespatch, StartDespatch);
	}

	/** Get Start Despatch.
		@return The starting time despatching the product into the vehicle
	  */
	public Timestamp getStartDespatch () 
	{
		return (Timestamp)get_Value(COLUMNNAME_StartDespatch);
	}

	/** Set Temperature.
		@param Temperature 
		The temperature
	  */
	public void setTemperature (int Temperature)
	{
		set_Value (COLUMNNAME_Temperature, Integer.valueOf(Temperature));
	}

	/** Get Temperature.
		@return The temperature
	  */
	public int getTemperature () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Temperature);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Time In Number.
		@param TimeInNumber 
		Must 4 Number for parsing to date Time In
	  */
	public void setTimeInNumber (String TimeInNumber)
	{
		set_Value (COLUMNNAME_TimeInNumber, TimeInNumber);
	}

	/** Get Time In Number.
		@return Must 4 Number for parsing to date Time In
	  */
	public String getTimeInNumber () 
	{
		return (String)get_Value(COLUMNNAME_TimeInNumber);
	}

	/** Set Time Out Number.
		@param TimeOutNumber 
		Must 4 Number for parsing to date Time Out
	  */
	public void setTimeOutNumber (String TimeOutNumber)
	{
		set_Value (COLUMNNAME_TimeOutNumber, TimeOutNumber);
	}

	/** Get Time Out Number.
		@return Must 4 Number for parsing to date Time Out
	  */
	public String getTimeOutNumber () 
	{
		return (String)get_Value(COLUMNNAME_TimeOutNumber);
	}

	/** Set Total Seal.
		@param TotalSeal 
		The total seal being attched (used)
	  */
	public void setTotalSeal (int TotalSeal)
	{
		set_Value (COLUMNNAME_TotalSeal, Integer.valueOf(TotalSeal));
	}

	/** Get Total Seal.
		@return The total seal being attched (used)
	  */
	public int getTotalSeal () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_TotalSeal);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set SO Despatch Notes.
		@param UNS_Despatch_Notes_ID SO Despatch Notes	  */
	public void setUNS_Despatch_Notes_ID (int UNS_Despatch_Notes_ID)
	{
		if (UNS_Despatch_Notes_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Despatch_Notes_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Despatch_Notes_ID, Integer.valueOf(UNS_Despatch_Notes_ID));
	}

	/** Get SO Despatch Notes.
		@return SO Despatch Notes	  */
	public int getUNS_Despatch_Notes_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Despatch_Notes_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Despatch_Notes_UU.
		@param UNS_Despatch_Notes_UU UNS_Despatch_Notes_UU	  */
	public void setUNS_Despatch_Notes_UU (String UNS_Despatch_Notes_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Despatch_Notes_UU, UNS_Despatch_Notes_UU);
	}

	/** Get UNS_Despatch_Notes_UU.
		@return UNS_Despatch_Notes_UU	  */
	public String getUNS_Despatch_Notes_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Despatch_Notes_UU);
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getUNS_WeighbridgeTicket_ID(), get_TrxName());	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Vehicle Driver Name.
		@param VehicleDriverName 
		The person name of vehicle by whom it is drived
	  */
	public void setVehicleDriverName (String VehicleDriverName)
	{
		throw new IllegalArgumentException ("VehicleDriverName is virtual column");	}

	/** Get Vehicle Driver Name.
		@return The person name of vehicle by whom it is drived
	  */
	public String getVehicleDriverName () 
	{
		return (String)get_Value(COLUMNNAME_VehicleDriverName);
	}

	/** Set Vehicle No.
		@param VehicleNo 
		The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo)
	{
		throw new IllegalArgumentException ("VehicleNo is virtual column");	}

	/** Get Vehicle No.
		@return The identification number of the vehicle being weighted
	  */
	public String getVehicleNo () 
	{
		return (String)get_Value(COLUMNNAME_VehicleNo);
	}
}