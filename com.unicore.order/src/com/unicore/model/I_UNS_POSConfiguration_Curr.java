/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSConfiguration_Curr
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSConfiguration_Curr 
{

    /** TableName=UNS_POSConfiguration_Curr */
    public static final String Table_Name = "UNS_POSConfiguration_Curr";

    /** AD_Table_ID=1000400 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Cash_Acct_ID */
    public static final String COLUMNNAME_Cash_Acct_ID = "Cash_Acct_ID";

	/** Set Cash Account	  */
	public void setCash_Acct_ID (int Cash_Acct_ID);

	/** Get Cash Account	  */
	public int getCash_Acct_ID();

	public I_C_ValidCombination getCash_Acct() throws RuntimeException;

    /** Column name C_BankAccount_ID */
    public static final String COLUMNNAME_C_BankAccount_ID = "C_BankAccount_ID";

	/** Set Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID);

	/** Get Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID();

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException;

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsUsedForChange */
    public static final String COLUMNNAME_IsUsedForChange = "IsUsedForChange";

	/** Set Used For Change ?	  */
	public void setIsUsedForChange (boolean IsUsedForChange);

	/** Get Used For Change ?	  */
	public boolean isUsedForChange();

    /** Column name MinimumAmt */
    public static final String COLUMNNAME_MinimumAmt = "MinimumAmt";

	/** Set Minimum Amt.
	  * Minimum Amount in Document Currency
	  */
	public void setMinimumAmt (BigDecimal MinimumAmt);

	/** Get Minimum Amt.
	  * Minimum Amount in Document Currency
	  */
	public BigDecimal getMinimumAmt();

    /** Column name UNS_POSConfiguration_Curr_ID */
    public static final String COLUMNNAME_UNS_POSConfiguration_Curr_ID = "UNS_POSConfiguration_Curr_ID";

	/** Set Currency Configuration	  */
	public void setUNS_POSConfiguration_Curr_ID (int UNS_POSConfiguration_Curr_ID);

	/** Get Currency Configuration	  */
	public int getUNS_POSConfiguration_Curr_ID();

    /** Column name UNS_POSConfiguration_Curr_UU */
    public static final String COLUMNNAME_UNS_POSConfiguration_Curr_UU = "UNS_POSConfiguration_Curr_UU";

	/** Set UNS_POSConfiguration_Curr_UU	  */
	public void setUNS_POSConfiguration_Curr_UU (String UNS_POSConfiguration_Curr_UU);

	/** Get UNS_POSConfiguration_Curr_UU	  */
	public String getUNS_POSConfiguration_Curr_UU();

    /** Column name UNS_POSConfiguration_ID */
    public static final String COLUMNNAME_UNS_POSConfiguration_ID = "UNS_POSConfiguration_ID";

	/** Set Global POS Configuration	  */
	public void setUNS_POSConfiguration_ID (int UNS_POSConfiguration_ID);

	/** Get Global POS Configuration	  */
	public int getUNS_POSConfiguration_ID();

	public com.unicore.model.I_UNS_POSConfiguration getUNS_POSConfiguration() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
