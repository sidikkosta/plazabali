/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPushMoneyProduct extends X_UNS_PushMoneyProduct {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7181633660137716807L;

	/**
	 * @param ctx
	 * @param UNS_PushMoneyProduct_ID
	 * @param trxName
	 */
	public MUNSPushMoneyProduct(Properties ctx, int UNS_PushMoneyProduct_ID,
			String trxName) {
		super(ctx, UNS_PushMoneyProduct_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPushMoneyProduct(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT COUNT(*) FROM UNS_PushMoneyProduct WHERE M_Product_ID = ?"
				+ " AND UNS_PushMoney_ID = ? AND UNS_PushMoneyProduct_ID <> ?";
		int count = DB.getSQLValue(get_TrxName(), sql, getM_Product_ID(), getUNS_PushMoney_ID(), get_ID());
		
		if(count > 0)
		{
			log.saveError("Error", "Duplicate product.");
			return false;
		}
		
		sql = "SELECT pm.DocumentNo FROM UNS_PushMoney pm"
				+ " INNER JOIN UNS_PushMoneyProduct pmp ON pmp.UNS_PushMoney_ID = pm.UNS_PushMoney_ID"
				+ " WHERE pm.ValidFrom = ? AND pmp.M_Product_ID = ?"
				+ " AND pm.UNS_PushMoney_ID <> ? AND pm.DocStatus IN ('CO', 'CL')"
				+ " AND (pm.UNS_Division_ID = ? OR pm.UNS_Division_ID IS NULL OR pm.UNS_Division_ID <= 0)";
		String docNo = DB.getSQLValueString(
				get_TrxName(), sql, getUNS_PushMoney().getValidFrom(), getM_Product_ID(), 
					getUNS_PushMoney_ID(), getUNS_PushMoney().getUNS_Division_ID());
		if(!Util.isEmpty(docNo, true))
		{
			log.saveError("Error", "Duplicate product. " + docNo);
			return false;
		}
		
		return true;
	}
	
	public static BigDecimal get(Properties ctx, int M_Product_ID, Timestamp date, Timestamp dateTo, int DivisionID, String trxName)
	{
		BigDecimal amount = Env.ZERO;
		
		String sql = "SELECT Amount FROM UNS_PushMoneyProduct pmp"
				+ " INNER JOIN UNS_PushMoney pm ON pm.UNS_PushMoney_ID = pmp.UNS_PushMoney_ID"
				+ " WHERE pmp.M_Product_ID = ? AND pm.DocStatus IN ('CO', 'CL')"
				+ " AND pm.ValidFrom <= ? AND (pm.ValidTo IS NULL OR ? <= pm.ValidTo)"
				+ " AND (pm.UNS_Division_ID = ? OR pm.UNS_Division_ID IS NULL OR pm.UNS_Division_ID <= 0)"
				+ " ORDER BY pm.ValidFrom DESC";
		
		amount = DB.getSQLValueBD(trxName, sql, M_Product_ID, date, dateTo, DivisionID);
		
		if(amount == null)
			amount = Env.ZERO;
		
		return amount;
	}
	
	public static BigDecimal getAll(Properties ctx, int M_Product_ID, Timestamp date, Timestamp dateTo, String trxName)
	{
		BigDecimal amount = Env.ZERO;
		
		String sql = "SELECT Amount FROM UNS_PushMoneyProduct pmp"
				+ " INNER JOIN UNS_PushMoney pm ON pm.UNS_PushMoney_ID = pmp.UNS_PushMoney_ID"
				+ " WHERE pmp.M_Product_ID = ? AND pm.DocStatus IN ('CO', 'CL')"
				+ " AND pm.ValidFrom <= ? AND (pm.ValidTo IS NULL OR ? <= pm.ValidTo)"
				+ " ORDER BY pm.ValidFrom DESC";
		
		amount = DB.getSQLValueBD(trxName, sql, M_Product_ID, date, dateTo);
		
		if(amount == null)
			amount = Env.ZERO;
		
		return amount;
	}
}