package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MUNSBPartnerDriver extends X_UNS_BPartner_Driver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7102333096176124377L;


	public MUNSBPartnerDriver(Properties ctx, int UNS_BPartner_Driver_ID,
			String trxName) {
		super(ctx, UNS_BPartner_Driver_ID, trxName);
		
	}

	public MUNSBPartnerDriver(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		
		return true;
	}
}
