/**
 * 
 */
package com.unicore.model;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.imageio.ImageIO;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.util.Env;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSAdvertisement extends X_UNS_Advertisement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3332353615384553199L;
	private Image[] m_images = null;

	/**
	 * @param ctx
	 * @param UNS_Advertisement_ID
	 * @param trxName
	 */
	public MUNSAdvertisement(Properties ctx, int UNS_Advertisement_ID,
			String trxName) 
	{
		super(ctx, UNS_Advertisement_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSAdvertisement(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public static MUNSAdvertisement[] get (int orgID, int storeID, String trxName)
	{
		String wc = "AD_Org_ID IN (?,?) AND (Store_ID IS NULL OR Store_ID = ?) "
				+ " AND IsActive = ? ";
		List<MUNSAdvertisement> list = Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				Table_Name, wc, trxName).setParameters(
						0, orgID, storeID, "Y").list();
		MUNSAdvertisement[] advert = new MUNSAdvertisement[list.size()];
		list.toArray(advert);
		
		return advert;
	}
	
	public Image[] getImages (boolean requery)
	{
		if (m_images != null && !requery)
			return m_images;
		
		List<Image> list = getImages();
		m_images = new Image[list.size()];
		list.toArray(m_images);
		return m_images;
	}
	
	public final List<Image> getImages ()
	{
		MAttachment attach = getAttachment();
		MAttachmentEntry[] values = attach.getEntries();
		List<Image> list = new ArrayList<>();
		for (int x=0; x<values.length; x++)
		{
			String type = values[x].getContentType();
			if (!type.startsWith("image"))
				continue;

			Image image = Toolkit.getDefaultToolkit().createImage(values[x].getData());
			list.add(image);
		}
		return list;
	}
	
	public static Image[] getImages (int orgID, int storeID, String trxName)
	{
		MUNSAdvertisement[] advert = get(orgID, storeID, trxName);
		List<Image> list = new ArrayList<>();
		for (int i=0; i<advert.length; i++)
		{
			list.addAll(advert[i].getImages());
		}
		Image[] images = new Image[list.size()];
		list.toArray(images);
		return images;
	}
	
	public static BufferedImage[] getBufferedImages (int orgID, int storeID, String trxName)
	{
		MUNSAdvertisement[] advert = get(orgID, storeID, trxName);
		List<BufferedImage> list = new ArrayList<>();
		for (int i=0; i<advert.length; i++)
		{
			MAttachment attach = advert[i].getAttachment();
			MAttachmentEntry[] values = attach.getEntries();
			for (int x=0; x<values.length; x++)
			{
				String type = values[x].getContentType();
				if (!type.startsWith("image"))
					continue;
				byte[] bytes = values[x].getData();
				ByteArrayInputStream in = new ByteArrayInputStream(bytes);
				try 
				{
					BufferedImage image = ImageIO.read(in);
					list.add(image);
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		
		BufferedImage[] images = new BufferedImage[list.size()];
		list.toArray(images);
		
		return images;
	}
}
