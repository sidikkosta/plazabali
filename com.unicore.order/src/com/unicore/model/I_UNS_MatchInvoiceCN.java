/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_MatchInvoiceCN
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_MatchInvoiceCN 
{

    /** TableName=UNS_MatchInvoiceCN */
    public static final String Table_Name = "UNS_MatchInvoiceCN";

    /** AD_Table_ID=1000337 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Amount */
    public static final String COLUMNNAME_Amount = "Amount";

	/** Set Amount.
	  * Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount);

	/** Get Amount.
	  * Amount in a defined currency
	  */
	public BigDecimal getAmount();

    /** Column name AmountApplied */
    public static final String COLUMNNAME_AmountApplied = "AmountApplied";

	/** Set Amount Applied	  */
	public void setAmountApplied (BigDecimal AmountApplied);

	/** Get Amount Applied	  */
	public BigDecimal getAmountApplied();

    /** Column name Balance */
    public static final String COLUMNNAME_Balance = "Balance";

	/** Set Balance	  */
	public void setBalance (BigDecimal Balance);

	/** Get Balance	  */
	public BigDecimal getBalance();

    /** Column name C_InvoiceLine_ID */
    public static final String COLUMNNAME_C_InvoiceLine_ID = "C_InvoiceLine_ID";

	/** Set Invoice Line.
	  * Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID);

	/** Get Invoice Line.
	  * Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID();

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsFirstRecord */
    public static final String COLUMNNAME_IsFirstRecord = "IsFirstRecord";

	/** Set First Record	  */
	public void setIsFirstRecord (boolean IsFirstRecord);

	/** Get First Record	  */
	public boolean isFirstRecord();

    /** Column name IsQuantityBased */
    public static final String COLUMNNAME_IsQuantityBased = "IsQuantityBased";

	/** Set Quantity based.
	  * Trade discount break level based on Quantity (not value)
	  */
	public void setIsQuantityBased (boolean IsQuantityBased);

	/** Get Quantity based.
	  * Trade discount break level based on Quantity (not value)
	  */
	public boolean isQuantityBased();

    /** Column name M_InOutLine_ID */
    public static final String COLUMNNAME_M_InOutLine_ID = "M_InOutLine_ID";

	/** Set Shipment/Receipt Line.
	  * Line on Shipment or Receipt document
	  */
	public void setM_InOutLine_ID (int M_InOutLine_ID);

	/** Get Shipment/Receipt Line.
	  * Line on Shipment or Receipt document
	  */
	public int getM_InOutLine_ID();

	public org.compiere.model.I_M_InOutLine getM_InOutLine() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name QtySubtract */
    public static final String COLUMNNAME_QtySubtract = "QtySubtract";

	/** Set Subtract Quantity.
	  * Quantity to subtract when generating commissions
	  */
	public void setQtySubtract (BigDecimal QtySubtract);

	/** Get Subtract Quantity.
	  * Quantity to subtract when generating commissions
	  */
	public BigDecimal getQtySubtract();

    /** Column name TotalAmountApplied */
    public static final String COLUMNNAME_TotalAmountApplied = "TotalAmountApplied";

	/** Set Total Amount Applied	  */
	public void setTotalAmountApplied (BigDecimal TotalAmountApplied);

	/** Get Total Amount Applied	  */
	public BigDecimal getTotalAmountApplied();

    /** Column name TotalQtyApplied */
    public static final String COLUMNNAME_TotalQtyApplied = "TotalQtyApplied";

	/** Set Total Qty Applied	  */
	public void setTotalQtyApplied (BigDecimal TotalQtyApplied);

	/** Get Total Qty Applied	  */
	public BigDecimal getTotalQtyApplied();

    /** Column name UNS_MatchInvoiceCN_ID */
    public static final String COLUMNNAME_UNS_MatchInvoiceCN_ID = "UNS_MatchInvoiceCN_ID";

	/** Set Match Invoice CN	  */
	public void setUNS_MatchInvoiceCN_ID (int UNS_MatchInvoiceCN_ID);

	/** Get Match Invoice CN	  */
	public int getUNS_MatchInvoiceCN_ID();

    /** Column name UNS_MatchInvoiceCN_UU */
    public static final String COLUMNNAME_UNS_MatchInvoiceCN_UU = "UNS_MatchInvoiceCN_UU";

	/** Set UNS_MatchInvoiceCN_UU	  */
	public void setUNS_MatchInvoiceCN_UU (String UNS_MatchInvoiceCN_UU);

	/** Get UNS_MatchInvoiceCN_UU	  */
	public String getUNS_MatchInvoiceCN_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
