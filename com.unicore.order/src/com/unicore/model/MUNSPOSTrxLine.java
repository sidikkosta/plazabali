package com.unicore.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MCurrency;
import org.compiere.model.MProductPricing;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.uns.util.ErrorMsg;

public class MUNSPOSTrxLine extends X_UNS_POSTrxLine {
	
	private MUNSPOSTrx m_parent = null;
	private int m_BPartner_ID = 0; //member/customer
	private int m_PriceList_ID = 0; //pricelist
	private int m_whs_ID = 0; //warehouse
	private int m_curr_ID = 0; //currency
	private int m_BpartnerLoc_ID = 0; //member location
	private MProductPricing m_pricing = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6010967188812465346L;

	public MUNSPOSTrxLine(Properties ctx, int UNS_POSTrxLine_ID, String trxName) 
	{
		super(ctx, UNS_POSTrxLine_ID, trxName);
	}

	public MUNSPOSTrxLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public MUNSPOSTrxLine (MUNSPOSTrx parent)
	{
		this(parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_POSTrx_ID(parent.get_ID());
		setHeaderInfo(parent);
	}
	
	public MUNSPOSTrx getParent ()
	{
		if (null == m_parent)
		{
			m_parent = new MUNSPOSTrx(getCtx(), getUNS_POSTrx_ID(), get_TrxName());
		}
		
		return m_parent;
	}
	
	public void setPrice (BigDecimal price)
	{
		setPriceList(price);
		setPriceActual(price);
	}
	
	public void addDescription (String text)
	{
		if (null == text)
		{
			return;
		}
		
		StringBuilder desc = new StringBuilder();
		
		if (!Util.isEmpty(getDescription(), true))
		{
			desc.append(getDescription());
			desc.append(" | ");
		}
		
		desc.append(text);
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		if (isProcessed())
		{
			ErrorMsg.setErrorMsg(getCtx(), "SaveError", "Record Processed");
			return false;
		}
		
		return super.beforeDelete();
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if(getParent().isReplication() || isReplication())
		{
			if (getC_UOM_ID() <= 0)
			{
				String sql = "SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?";
				int uom = DB.getSQLValue(get_TrxName(), sql, getM_Product_ID());
				if(uom <= 0)
				{
					ErrorMsg.setErrorMsg(getCtx(), "Error UOM", "UOM has not been set");
					return false;
				}
				setC_UOM_ID(uom);
			}
			return true;
		}
		if (getLine() == 0)
		{
			String sql = "SELECT MAX (Line) FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?";
			int lineNO = DB.getSQLValue(get_TrxName(), sql, getUNS_POSTrx_ID());
			setLine(lineNO + 1);
		}
		
		if (getParent().isComplete())
		{
			ErrorMsg.setErrorMsg(getCtx(), "ParentComplete", "Parent is complete.");
			return false;
		}
		else if (getM_Product_ID() == 0 && getC_Charge_ID() == 0)
		{
			ErrorMsg.setErrorMsg(getCtx(), 
					"MandatoryProductOrCharge", "Mandatory field Product or Charge");
			return false;
		}
		else if (getC_Charge_ID() != 0 && getM_Product_ID() != 0)
		{
			ErrorMsg.setErrorMsg(getCtx(), "AmbiguousProductAndCharge", 
					"product and charge cannot be defined simultaneously");
			return false;
		}
		if (getBarcode() != null)
		{
			String sku = null;
			String etbb = null;
			if (getBarcode() == null || getBarcode().length() < 8)
			{
				String sql = "SELECT SKU FROM M_Product WHERE M_Product_ID = ?";
				sku = DB.getSQLValueString(get_TrxName(), sql, getM_Product_ID());
				setBarcode(sku);
			}
			else
				sku = getBarcode().substring(0, 8);
			if (getBarcode().length() >= 16)
				etbb = getBarcode().substring(8,16);
			
			setSKU(sku);
			setETBBCode(etbb);
		}
		if (isBOM())
			setPrice(Env.ZERO);
		
		setQtyOrdered(getQtyEntered().add(getQtyBonuses()));
		String pdesc = getParent().getDescription(); 
		if (pdesc == null)
			pdesc = "";
		if (getParent().isReturn() && !pdesc .contains("**") && !pdesc .contains("Import"))
		{
			MUNSPOSTrxLine oriLine = null;
			if (getReference_ID() == 0)
			{
				MUNSPOSTrx originalTrx = getParent().getOriginalTrx();
				MUNSPOSTrxLine[] oLines = originalTrx.getLines(false);
				for (int i=0; i<oLines.length; i++)
				{
					if (oLines[i].getM_Product_ID() != getM_Product_ID())
						continue;
					String oSKU = oLines[i].getSKU();
					String sku = getSKU();
					if (sku == null && oSKU != null)
						continue;
					if (sku != null && oSKU == null)
						continue;
					if (sku != null && oSKU != null && !sku.equals(oSKU))
						continue;
					String etbb = getETBBCode();
					String oETBB = oLines[i].getETBBCode();
					if (etbb != null && oETBB == null)
						continue;
					if (etbb == null && oETBB != null)
						continue;
					if (etbb != null && oETBB != null && !etbb.equals(oETBB))
						continue;
					
					setReference_ID(oLines[i].get_ID());
					oriLine = oLines[i];
				}
			}
			else 
				oriLine = new MUNSPOSTrxLine(getCtx(), getReference_ID(), get_TrxName());
			
			if (oriLine == null)
			{
				log.log(Level.SEVERE, "Could not match refund with original trx");
				return false;
			}
			if (getQtyOrdered().abs().compareTo(oriLine.getRefundableQty()) == 1)
			{
				log.log(Level.SEVERE, "Refund qty greater than original qty");
				return false;
			}
			
			setPrice(oriLine.getPriceActual());
		}
		
		if (m_BPartner_ID == 0 || m_BpartnerLoc_ID == 0 || m_PriceList_ID == 0
				|| m_whs_ID == 0 || m_curr_ID == 0)
		{
			setHeaderInfo(getParent());
		}
		
		if (getM_Product_ID() != 0 && !isBOM())
		{
			if (m_pricing == null 
					&&  Env.ZERO.compareTo(getPriceActual()) == 0
					&&  Env.ZERO.compareTo(getPriceList()) == 0
					&&	!isProductBonuses() && !getParent().isReturn() && !setPrice())
			{
				return false;
			}
		}
		
		String sql = "SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?";
		int uom = DB.getSQLValue(get_TrxName(), sql, getM_Product_ID());
		if(uom <= 0)
		{
			ErrorMsg.setErrorMsg(getCtx(), "Error UOM", "UOM has not been set");
			return false;
		}
		
		setC_UOM_ID(uom);
		BigDecimal bdiscountLAmt = getPriceList().multiply(getQtyOrdered());
		int precision = MCurrency.getStdPrecision(getCtx(), getParent().getC_Currency_ID());
		if(is_ValueChanged(COLUMNNAME_DiscountAmt) ||  pdesc.contains("**") || pdesc.contains("Import"))
		{
			BigDecimal discAMt = getDiscountAmt().setScale(precision, RoundingMode.HALF_DOWN);
			setDiscountAmt(discAMt);
			BigDecimal discount = Env.ZERO;
			if (bdiscountLAmt.signum() != 0)
				discount = getDiscountAmt().divide(bdiscountLAmt, 16, 
						RoundingMode.HALF_DOWN).multiply(Env.ONEHUNDRED);
			
			setDiscount(discount);
		}
		else
		{
			BigDecimal discAMt = (bdiscountLAmt.multiply(getDiscount())).divide(Env.ONEHUNDRED);
			discAMt = discAMt.setScale(precision, RoundingMode.HALF_DOWN);
			setDiscountAmt(discAMt);
		}
		
		BigDecimal afterDiscount = bdiscountLAmt.subtract(getDiscountAmt());
		if (getQtyOrdered().signum() != 0)
		{
			BigDecimal priceActual = afterDiscount.divide(getQtyOrdered(), 16, RoundingMode.HALF_UP);
			setPriceActual(priceActual);
		}
		
		setLineNetAmt();
		initTaxAndServiceCharge();
		setLineAmt();
		
		if (Util.isEmpty(getDescription(), true))
		{
			if (getM_Product_ID() > 0)
			{
				sql = "SELECT CONCAT(Value,'-',Name) FROM M_Product WHERE M_Product_ID = ?";
				String desc = DB.getSQLValueString(get_TrxName(), sql, getM_Product_ID());
				setDescription(desc);
			}
			else if (getC_Charge_ID() > 0)
			{
				sql = "SELECT Name FROM C_Charge WHERE C_Charge_ID = ?";
				String desc = DB.getSQLValueString(get_TrxName(), sql, getC_Charge_ID());
				setDescription(desc);
			}
		}
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!getParent().isReplication() && !isReplication())
			updateHeader();
		
		return super.afterSave(newRecord, success);
	}
	
	@Override
	protected boolean afterDelete(boolean success)
	{	
		updateHeader();
		
		return super.afterDelete(success);
	}
	
	public void setLineNetAmt ()
	{
		BigDecimal lineNetAmt = getPriceActual().multiply(getQtyOrdered());
		setLineNetAmt(lineNetAmt);
	}
	
	public void setLineAmt()
	{
		BigDecimal lineAmt = getLineNetAmt();
		if (!isPriceIncludedTax())
			lineAmt = lineAmt.add(getTaxAmt());
		if (!isServiceChargeIncludedTax())
			lineAmt = lineAmt.add(getServiceCharge());
		setLineAmt(lineAmt);
	}
	
	public void setHeaderInfo (MUNSPOSTrx header)
	{
		this.m_BPartner_ID = header.getC_BPartner_ID();
		this.m_PriceList_ID = header.getM_PriceList_ID();
		this.m_whs_ID = header.getM_Warehouse_ID();
		this.m_curr_ID = header.getC_Currency_ID();
		this.m_BpartnerLoc_ID = header.getC_BPartner_Location_ID();
		this.m_parent = header;
	}
	
	public boolean setPrice ()
	{
		if (getM_Product_ID() == 0)
		{
			return true;
		}
		else if (m_PriceList_ID == 0)
		{
			ErrorMsg.setErrorMsg(getCtx(), "UndefinedPrice", "Undefined Price List");
			return false;
		}
		
		if(getPriceList().signum()==0)
		{
			m_pricing = new MProductPricing(getM_Product_ID(), m_BPartner_ID, getQtyOrdered(), true, 
					m_BpartnerLoc_ID);
			m_pricing.setM_PriceList_ID(m_PriceList_ID);
			
			if (!m_pricing.calculatePrice())
			{
				ErrorMsg.setErrorMsg(getCtx(), "ProductNotOnPriceList", "Product Not On Price List");
				return false;
			}
			
			setPriceActual(m_pricing.getPriceStd());
			setPriceLimit(m_pricing.getPriceLimit());
			setPriceList(m_pricing.getPriceList());
		}
		
		return true;
	}
	
	public BigDecimal getTotalLineAmt()
	{
		String sql = "SELECT COALESCE(SUM(LineNetAmt),0) FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?";
		BigDecimal value =  DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSTrx_ID());
		return value;
	}
	
	
	public boolean updateHeader()
	{
		String sql =  "SELECT COALESCE(SUM(LineNetAmt),0) FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?";
		BigDecimal totalAmt = DB.getSQLValueBD(get_TrxName(), sql,  getUNS_POSTrx_ID());
		MUNSPOSTrx parent = getParent();
		parent.setTotalAmt(totalAmt);
		return parent.save();
	}
	
	private void initTaxAndServiceCharge ()
	{
		String sql = "SELECT pt.Store_ID FROM UNS_POSTerminal pt"
				+ " INNER JOIN UNS_POS_Session ss ON ss.UNS_POSTerminal_ID = pt.UNS_POSTerminal_ID"
				+ " INNER JOIN UNS_POSTrx trx ON trx.UNS_POS_Session_ID = ss.UNS_POS_Session_ID"
				+ " WHERE trx.UNS_POSTrx_ID = ?";
		int storeID = DB.getSQLValue(get_TrxName(), sql, getUNS_POSTrx_ID());
		MUNSStoreConfiguration config = MUNSStoreConfiguration.get(getCtx(), storeID, get_TrxName());
		if (config == null)
			return;
		boolean isPB1Tax = config.isPB1Tax();
		if (isPB1Tax)
			setC_Tax_ID(config.getC_Tax_ID());
	}
	
	public boolean isPriceIncludedTax ()
	{
		String sql = "SELECT IsTaxIncluded FROM M_PriceList WHERE M_PriceList_ID = ?";
		boolean isPriceTaxIncl = "Y".equals(DB.getSQLValueString(get_TrxName(), sql, getParent().getM_PriceList_ID()));
		return isPriceTaxIncl;
	}
	
	public boolean isServiceChargeIncludedTax ()
	{
		String sql = "SELECT IsServiceChargeIncluded FROM M_PriceList WHERE M_PriceList_ID = ?";
		boolean isServiceChargeIncl = "Y".equals(DB.getSQLValueString(get_TrxName(), sql, getParent().getM_PriceList_ID()));
		return isServiceChargeIncl;
	}
	
	public BigDecimal getRefundableQty ()
	{
		String sql = "SELECT SUM (QtyEntered) FROM UNS_POSTrxLine WHERE Reference_ID = ? "
				+ " AND EXISTS (SELECT UNS_POSTrx_ID FROM UNS_POSTrx WHERE UNS_POSTrx_ID = "
				+ " UNS_POSTrxLine.UNS_POSTrx_ID AND DocStatus IN ('CO','CL'))";
		BigDecimal refundQty = DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSTrxLine_ID());
		if (refundQty == null)
			refundQty = Env.ZERO;
		refundQty = refundQty.abs();
		BigDecimal refundable = getQtyOrdered().subtract(refundQty);
		return refundable;
	}
}
