/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_AccessoriesLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_AccessoriesLine 
{

    /** TableName=UNS_AccessoriesLine */
    public static final String Table_Name = "UNS_AccessoriesLine";

    /** AD_Table_ID=1000313 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AccessoriesType */
    public static final String COLUMNNAME_AccessoriesType = "AccessoriesType";

	/** Set Accessories Type	  */
	public void setAccessoriesType (String AccessoriesType);

	/** Get Accessories Type	  */
	public String getAccessoriesType();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_InvoiceLine_ID */
    public static final String COLUMNNAME_C_InvoiceLine_ID = "C_InvoiceLine_ID";

	/** Set Invoice Line.
	  * Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID);

	/** Get Invoice Line.
	  * Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID();

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException;

    /** Column name C_OrderLine_ID */
    public static final String COLUMNNAME_C_OrderLine_ID = "C_OrderLine_ID";

	/** Set Sales Order Line.
	  * Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID);

	/** Get Sales Order Line.
	  * Sales Order Line
	  */
	public int getC_OrderLine_ID();

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name ProductAccessories_ID */
    public static final String COLUMNNAME_ProductAccessories_ID = "ProductAccessories_ID";

	/** Set Product Accessories	  */
	public void setProductAccessories_ID (int ProductAccessories_ID);

	/** Get Product Accessories	  */
	public int getProductAccessories_ID();

	public org.compiere.model.I_M_Product getProductAccessories() throws RuntimeException;

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name UNS_AccessoriesLine_ID */
    public static final String COLUMNNAME_UNS_AccessoriesLine_ID = "UNS_AccessoriesLine_ID";

	/** Set Accessories Line	  */
	public void setUNS_AccessoriesLine_ID (int UNS_AccessoriesLine_ID);

	/** Get Accessories Line	  */
	public int getUNS_AccessoriesLine_ID();

    /** Column name UNS_AccessoriesLine_UU */
    public static final String COLUMNNAME_UNS_AccessoriesLine_UU = "UNS_AccessoriesLine_UU";

	/** Set UNS_AccessoriesLine_UU	  */
	public void setUNS_AccessoriesLine_UU (String UNS_AccessoriesLine_UU);

	/** Get UNS_AccessoriesLine_UU	  */
	public String getUNS_AccessoriesLine_UU();

    /** Column name UNS_PreOrder_Line_ID */
    public static final String COLUMNNAME_UNS_PreOrder_Line_ID = "UNS_PreOrder_Line_ID";

	/** Set Pre Order Detail	  */
	public void setUNS_PreOrder_Line_ID (int UNS_PreOrder_Line_ID);

	/** Get Pre Order Detail	  */
	public int getUNS_PreOrder_Line_ID();

    /** Column name UNS_ProductAccessories_ID */
    public static final String COLUMNNAME_UNS_ProductAccessories_ID = "UNS_ProductAccessories_ID";

	/** Set Product Accessories	  */
	public void setUNS_ProductAccessories_ID (int UNS_ProductAccessories_ID);

	/** Get Product Accessories	  */
	public int getUNS_ProductAccessories_ID();

	public com.unicore.model.I_UNS_ProductAccessories getUNS_ProductAccessories() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
