/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_EDC
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_EDC extends PO implements I_UNS_EDC, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20200102L;

    /** Standard Constructor */
    public X_UNS_EDC (Properties ctx, int UNS_EDC_ID, String trxName)
    {
      super (ctx, UNS_EDC_ID, trxName);
      /** if (UNS_EDC_ID == 0)
        {
			setC_BankAccount_ID (0);
			setEDCType (null);
			setName (null);
			setUNS_EDC_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_EDC (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_EDC[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankAccount)MTable.get(getCtx(), org.compiere.model.I_C_BankAccount.Table_Name)
			.getPO(getC_BankAccount_ID(), get_TrxName());	}

	/** Set Cash / Bank Account.
		@param C_BankAccount_ID 
		Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID)
	{
		if (C_BankAccount_ID < 1) 
			set_Value (COLUMNNAME_C_BankAccount_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankAccount_ID, Integer.valueOf(C_BankAccount_ID));
	}

	/** Get Cash / Bank Account.
		@return Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Communication.
		@param Communication Communication	  */
	public void setCommunication (String Communication)
	{
		set_Value (COLUMNNAME_Communication, Communication);
	}

	/** Get Communication.
		@return Communication	  */
	public String getCommunication () 
	{
		return (String)get_Value(COLUMNNAME_Communication);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** We Chat = WCH */
	public static final String EDCTYPE_WeChat = "WCH";
	/** BCA = BCA */
	public static final String EDCTYPE_BCA = "BCA";
	/** MANDIRI = MAN */
	public static final String EDCTYPE_MANDIRI = "MAN";
	/** BNI = BNI */
	public static final String EDCTYPE_BNI = "BNI";
	/** CUP Indopay = CUP */
	public static final String EDCTYPE_CUPIndopay = "CUP";
	/** Set EDC Type.
		@param EDCType EDC Type	  */
	public void setEDCType (String EDCType)
	{

		set_Value (COLUMNNAME_EDCType, EDCType);
	}

	/** Get EDC Type.
		@return EDC Type	  */
	public String getEDCType () 
	{
		return (String)get_Value(COLUMNNAME_EDCType);
	}

	/** Set ID No.
		@param IDNo ID No	  */
	public void setIDNo (String IDNo)
	{
		set_Value (COLUMNNAME_IDNo, IDNo);
	}

	/** Get ID No.
		@return ID No	  */
	public String getIDNo () 
	{
		return (String)get_Value(COLUMNNAME_IDNo);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set EDC.
		@param UNS_EDC_ID EDC	  */
	public void setUNS_EDC_ID (int UNS_EDC_ID)
	{
		if (UNS_EDC_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDC_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDC_ID, Integer.valueOf(UNS_EDC_ID));
	}

	/** Get EDC.
		@return EDC	  */
	public int getUNS_EDC_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDC_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDC Processor.
		@param UNS_EDCProcessor_ID EDC Processor	  */
	public void setUNS_EDCProcessor_ID (int UNS_EDCProcessor_ID)
	{
		if (UNS_EDCProcessor_ID < 1) 
			set_Value (COLUMNNAME_UNS_EDCProcessor_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_EDCProcessor_ID, Integer.valueOf(UNS_EDCProcessor_ID));
	}

	/** Get EDC Processor.
		@return EDC Processor	  */
	public int getUNS_EDCProcessor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDCProcessor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_EDC_UU.
		@param UNS_EDC_UU UNS_EDC_UU	  */
	public void setUNS_EDC_UU (String UNS_EDC_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_EDC_UU, UNS_EDC_UU);
	}

	/** Get UNS_EDC_UU.
		@return UNS_EDC_UU	  */
	public String getUNS_EDC_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_EDC_UU);
	}

	public com.unicore.model.I_UNS_POSTerminal getUNS_POSTerminal() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTerminal)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTerminal.Table_Name)
			.getPO(getUNS_POSTerminal_ID(), get_TrxName());	}

	/** Set POS Terminal.
		@param UNS_POSTerminal_ID POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID)
	{
		if (UNS_POSTerminal_ID < 1) 
			set_Value (COLUMNNAME_UNS_POSTerminal_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_POSTerminal_ID, Integer.valueOf(UNS_POSTerminal_ID));
	}

	/** Get POS Terminal.
		@return POS Terminal	  */
	public int getUNS_POSTerminal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTerminal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}