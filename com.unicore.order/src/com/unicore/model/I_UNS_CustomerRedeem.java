/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_CustomerRedeem
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_CustomerRedeem 
{

    /** TableName=UNS_CustomerRedeem */
    public static final String Table_Name = "UNS_CustomerRedeem";

    /** AD_Table_ID=1000254 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BalancePoint */
    public static final String COLUMNNAME_BalancePoint = "BalancePoint";

	/** Set Balance Point.
	  * Balance of Point
	  */
	public void setBalancePoint (BigDecimal BalancePoint);

	/** Get Balance Point.
	  * Balance of Point
	  */
	public BigDecimal getBalancePoint();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Location_ID */
    public static final String COLUMNNAME_C_Location_ID = "C_Location_ID";

	/** Set Address.
	  * Location or Address
	  */
	public void setC_Location_ID (int C_Location_ID);

	/** Get Address.
	  * Location or Address
	  */
	public int getC_Location_ID();

	public I_C_Location getC_Location() throws RuntimeException;

    /** Column name C_Year_ID */
    public static final String COLUMNNAME_C_Year_ID = "C_Year_ID";

	/** Set Year.
	  * Calendar Year
	  */
	public void setC_Year_ID (int C_Year_ID);

	/** Get Year.
	  * Calendar Year
	  */
	public int getC_Year_ID();

	public org.compiere.model.I_C_Year getC_Year() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CurrentPoint */
    public static final String COLUMNNAME_CurrentPoint = "CurrentPoint";

	/** Set CurrentPoint	  */
	public void setCurrentPoint (BigDecimal CurrentPoint);

	/** Get CurrentPoint	  */
	public BigDecimal getCurrentPoint();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EMail */
    public static final String COLUMNNAME_EMail = "EMail";

	/** Set EMail Address.
	  * Electronic Mail Address
	  */
	public void setEMail (String EMail);

	/** Get EMail Address.
	  * Electronic Mail Address
	  */
	public String getEMail();

    /** Column name Fax */
    public static final String COLUMNNAME_Fax = "Fax";

	/** Set Fax.
	  * Facsimile number
	  */
	public void setFax (String Fax);

	/** Get Fax.
	  * Facsimile number
	  */
	public String getFax();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name OwnerAddress_ID */
    public static final String COLUMNNAME_OwnerAddress_ID = "OwnerAddress_ID";

	/** Set Owner Address_ID.
	  * Address Owner from Business Partner
	  */
	public void setOwnerAddress_ID (int OwnerAddress_ID);

	/** Get Owner Address_ID.
	  * Address Owner from Business Partner
	  */
	public int getOwnerAddress_ID();

	public I_C_Location getOwnerAddress() throws RuntimeException;

    /** Column name OwnerEmail */
    public static final String COLUMNNAME_OwnerEmail = "OwnerEmail";

	/** Set Owner Email	  */
	public void setOwnerEmail (String OwnerEmail);

	/** Get Owner Email	  */
	public String getOwnerEmail();

    /** Column name OwnerFax */
    public static final String COLUMNNAME_OwnerFax = "OwnerFax";

	/** Set Owner Fax	  */
	public void setOwnerFax (String OwnerFax);

	/** Get Owner Fax	  */
	public String getOwnerFax();

    /** Column name OwnerName */
    public static final String COLUMNNAME_OwnerName = "OwnerName";

	/** Set Owner Name.
	  * The name of the owner of the business partner
	  */
	public void setOwnerName (String OwnerName);

	/** Get Owner Name.
	  * The name of the owner of the business partner
	  */
	public String getOwnerName();

    /** Column name OwnerPhone */
    public static final String COLUMNNAME_OwnerPhone = "OwnerPhone";

	/** Set Owner Phone	  */
	public void setOwnerPhone (String OwnerPhone);

	/** Get Owner Phone	  */
	public String getOwnerPhone();

    /** Column name Phone */
    public static final String COLUMNNAME_Phone = "Phone";

	/** Set Phone.
	  * Identifies a telephone number
	  */
	public void setPhone (String Phone);

	/** Get Phone.
	  * Identifies a telephone number
	  */
	public String getPhone();

    /** Column name PointRedeem */
    public static final String COLUMNNAME_PointRedeem = "PointRedeem";

	/** Set Point Redeem	  */
	public void setPointRedeem (BigDecimal PointRedeem);

	/** Get Point Redeem	  */
	public BigDecimal getPointRedeem();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name TotalAddPoint */
    public static final String COLUMNNAME_TotalAddPoint = "TotalAddPoint";

	/** Set Total Additional Point	  */
	public void setTotalAddPoint (BigDecimal TotalAddPoint);

	/** Get Total Additional Point	  */
	public BigDecimal getTotalAddPoint();

    /** Column name UNS_CustomerPoint_ID */
    public static final String COLUMNNAME_UNS_CustomerPoint_ID = "UNS_CustomerPoint_ID";

	/** Set Customer Point	  */
	public void setUNS_CustomerPoint_ID (int UNS_CustomerPoint_ID);

	/** Get Customer Point	  */
	public int getUNS_CustomerPoint_ID();

	public com.unicore.model.I_UNS_CustomerPoint getUNS_CustomerPoint() throws RuntimeException;

    /** Column name UNS_CustomerRedeem_ID */
    public static final String COLUMNNAME_UNS_CustomerRedeem_ID = "UNS_CustomerRedeem_ID";

	/** Set Customer Redeem Point	  */
	public void setUNS_CustomerRedeem_ID (int UNS_CustomerRedeem_ID);

	/** Get Customer Redeem Point	  */
	public int getUNS_CustomerRedeem_ID();

    /** Column name UNS_CustomerRedeem_UU */
    public static final String COLUMNNAME_UNS_CustomerRedeem_UU = "UNS_CustomerRedeem_UU";

	/** Set UNS_CustomerRedeem_UU	  */
	public void setUNS_CustomerRedeem_UU (String UNS_CustomerRedeem_UU);

	/** Get UNS_CustomerRedeem_UU	  */
	public String getUNS_CustomerRedeem_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
