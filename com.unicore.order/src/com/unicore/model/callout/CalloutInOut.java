/**
 * 
 */
package com.unicore.model.callout;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInOutLineMA;
import org.compiere.util.DB;

import com.unicore.model.MBPartner;

/**
 * @author Burhani Adam
 *
 */
public class CalloutInOut implements IColumnCallout{

	/**
	 * 
	 */
	public CalloutInOut() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		if(mField.getColumnName().equals(MInOut.COLUMNNAME_MovementDate))
			return movementDate(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(MInOut.COLUMNNAME_C_BPartner_ID))
			return bpID(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(MInOut.COLUMNNAME_UNS_Port_ID))
			return portID(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(MInOut.COLUMNNAME_IsDuttyPaid))
			return isDuttyPaid(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(MInOutLineMA.COLUMNNAME_ETBBCode))
			return ETBBCode(ctx, WindowNo, mTab, mField, value);
			
		return null;
	}
	
	private String portID(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		boolean isDuttyPaid = (Boolean) mTab.getValue(MInOut.COLUMNNAME_IsDuttyPaid);
		
		if(isDuttyPaid)
			return null;
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MInOut.COLUMNNAME_ValueOfPort, null);
			return null;
		}
		
		String sql = "SELECT Value FROM UNS_Port WHERE UNS_Port_ID = ?";
		String valOfPort = DB.getSQLValueString(null, sql, (Integer) value);
		mTab.setValue(MInOut.COLUMNNAME_ValueOfPort, valOfPort);
		
		return null;
	}
	
	private String bpID(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if(null == (String) mField.get_ValueAsString(MInOut.COLUMNNAME_IsDuttyPaid)
				|| ("").equals((String) mField.get_ValueAsString(MInOut.COLUMNNAME_IsDuttyPaid)))
			return null;
		boolean isDuttyPaid = (Boolean) mTab.getValue(MInOut.COLUMNNAME_IsDuttyPaid);
		
		if(!isDuttyPaid)
			return null;
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MInOut.COLUMNNAME_BCCode, null);
			return null;
		}
		
		mTab.setValue(MInOut.COLUMNNAME_BCCode, MBPartner.get(ctx, (Integer) value).getValue().substring(0,5));
		
		return null;
	}
	
	private String movementDate(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if(null == (String) mField.get_ValueAsString(MInOut.COLUMNNAME_IsDuttyPaid)
				|| ("").equals((String) mField.get_ValueAsString(MInOut.COLUMNNAME_IsDuttyPaid)))
			return null;
		boolean isDuttyPaid = (Boolean) mTab.getValue(MInOut.COLUMNNAME_IsDuttyPaid);
		
		if(!isDuttyPaid)
			return null;
		
		mTab.setValue(MInOut.COLUMNNAME_BCDate, (Timestamp) value);
		String dateCode = analyzeDateCode((Timestamp) value);
		mTab.setValue(MInOut.COLUMNNAME_ValueOfPort, dateCode);
		
		return null;
	}
	
	private String isDuttyPaid(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		boolean duttyPaid = (boolean) mTab.getValue(MInOut.COLUMNNAME_IsDuttyPaid);
		
		if(duttyPaid)
		{
			movementDate(ctx, WindowNo, mTab, mField, (Timestamp) mTab.getValue(MInOut.COLUMNNAME_MovementDate));
			bpID(ctx, WindowNo, mTab, mField, (Integer) mTab.getValue(MInOut.COLUMNNAME_C_BPartner_ID));
		}
		else
		{
			mTab.setValue(MInOut.COLUMNNAME_BCDate, null);
			mTab.setValue(MInOut.COLUMNNAME_BCCode, null);
			portID(ctx, WindowNo, mTab, mField, (Integer) mTab.getValue(MInOut.COLUMNNAME_UNS_Port_ID));
		}
			
		return null;
	}
	
	private String analyzeDateCode(java.sql.Timestamp date)
	{
		Integer code[] = {0,9,7,5,3,1,8,6,4,2};
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
//		String year = (new Integer (cal.get(Calendar.YEAR))).toString();
//		year = year.substring(3, 4);
//		int yearCode = Integer.valueOf(year);
//		year = code[yearCode].toString();
		String month1 = null;
		String month2 = null;
		String month = (new Integer (cal.get(Calendar.MONTH) + 1)).toString();
		if(month.length() == 1)
		{
			month1 = code[0].toString();
			month2 = code[Integer.valueOf(month)].toString();
		}
		else
		{
			month1 = code[(Integer.valueOf(month.substring(0, 1)))].toString();
			month2 = code[(Integer.valueOf(month.substring(1, 2)))].toString();
		}
		
		month = month1 + "" + month2;
		
		return month;
	}
	
	private String ETBBCode(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if(value == null || ((String) value).equals(""))
			return null;
		
		MInOutLine parent = new MInOutLine(ctx, (Integer) mTab.getValue(MInOutLineMA.COLUMNNAME_M_InOutLine_ID), null);
		String sql = "SELECT DISTINCT(asi.M_AttributeSetInstance_ID) FROM M_StorageOnHand soh"
				+ " INNER JOIN M_AttributeSetInstance asi ON asi.M_AttributeSetInstance_ID = soh.M_AttributeSetInstance_ID"
				+ " INNER JOIN M_Locator loc ON loc.M_Locator_ID = soh.M_Locator_ID"
				+ " INNER JOIN M_Warehouse whs ON whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " WHERE soh.M_Product_ID = ? AND whs.UNS_Division_ID = ? AND TRIM(asi.Description) = TRIM(?)"
				+ " AND DATE_TRUNC('Day', asi.Created) = asi.Created";
		int asiID = DB.getSQLValue(null, sql, parent.getM_Product_ID(), 
				parent.getM_InOut().getM_Warehouse().getUNS_Division_ID(),
					(String) value);
		if(asiID <= 0)
		{
			sql = "SELECT DISTINCT(asi.M_AttributeSetInstance_ID) FROM M_StorageOnHand soh"
					+ " INNER JOIN M_AttributeSetInstance asi ON asi.M_AttributeSetInstance_ID = soh.M_AttributeSetInstance_ID"
					+ " INNER JOIN M_Locator loc ON loc.M_Locator_ID = soh.M_Locator_ID"
					+ " INNER JOIN M_Warehouse whs ON whs.M_Warehouse_ID = loc.M_Warehouse_ID"
					+ " WHERE soh.M_Product_ID = ? AND whs.UNS_Division_ID = ? AND TRIM(asi.Description) = TRIM(?)";
			asiID = DB.getSQLValue(null, sql, parent.getM_Product_ID(), 
					parent.getM_InOut().getM_Warehouse().getUNS_Division_ID(),
						(String) value);
		}
		
		if(asiID <= 0)
		{
			mTab.setValue(MInOutLineMA.COLUMNNAME_DateMaterialPolicy, null);
			mTab.setValue(MInOutLineMA.COLUMNNAME_M_AttributeSetInstance_ID, null);
//			mTab.fireDataStatusEEvent("New ETBB.?", "ETBB with code " + (String) value + " is not found. "
//					+ "This action will be create new ETBB with ETBB date based on movement date.", false);
			mTab.fireDataStatusEEvent("Warning.", "ETBB Not Found. " + (String) value, false);
			mTab.setValue(MInOutLineMA.COLUMNNAME_ETBBCode, null);
		}
		else
		{
			mTab.setValue(MInOutLineMA.COLUMNNAME_DateMaterialPolicy, 
					DB.getSQLValueTS(null, "SELECT DATE_TRUNC('Day', Created) FROM M_AttributeSetInstance"
							+ " WHERE M_AttributeSetInstance_ID = ?", asiID));
			mTab.setValue(MInOutLineMA.COLUMNNAME_M_AttributeSetInstance_ID, asiID);
		}
		
		return null;
	}
}