/**
 * 
 */
package com.unicore.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.model.MPeriod;
import org.compiere.util.DB;

import com.unicore.model.MUNSSASummary;
import com.unicore.model.MUNSStatementOfAccount;

/**
 * @author Burhani Adam
 *
 */
public class CalloutStatementOfAccount implements IColumnCallout{

	/**
	 * 
	 */
	public CalloutStatementOfAccount() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(mTab.getTableName().equals(MUNSStatementOfAccount.Table_Name))
		{
			if(mField.getColumnName().equals(MUNSStatementOfAccount.COLUMNNAME_C_Period_ID))
				return period(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSStatementOfAccount.COLUMNNAME_AD_Client_ID))
				return client(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		else if(mTab.getTableName().equals(MUNSSASummary.Table_Name))
		{
			if(mField.getColumnName().equals(MUNSSASummary.COLUMNNAME_C_Period_ID))
				return period(ctx, WindowNo, mTab, mField, value, oldValue);
		}
			
		return null;
	}
	
	private String period(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_DateFrom, null);
			mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_DateTo, null);
			mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_DateAcct, null);
			return null;
		}
		
		MPeriod period = MPeriod.get(ctx, (Integer) value);
		mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_DateFrom, period.getStartDate());
		mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_DateTo, period.getEndDate());
		mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_DateAcct, period.getEndDate());
		
		return null;
	}
	
	private String client(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		String sql = "SELECT d.UNS_Consignment_Acct FROM C_AcctSchema_Default d WHERE EXISTS"
				+ " (SELECT 1 FROM C_AcctSchema a WHERE a.C_AcctSchema_ID = d.C_AcctSchema_ID"
				+ " AND a.C_Currency_ID = 303)";
		int acct = DB.getSQLValue(null, sql);
		
		MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(ctx, (Integer) value);
		MAcctSchema as = null;
		for(int i=0;i<ass.length;i++)
		{
			if(ass[i].getC_Currency_ID() == 303)
			{
				as = ass[i];
				break;
			}
		}
		
		int chargeID = MCharge.getOfAccount(acct, as);
		
		if(chargeID > 0)
			mTab.setValue(MUNSStatementOfAccount.COLUMNNAME_C_Charge_ID, chargeID);
		
		return null;
	}
}