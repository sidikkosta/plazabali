/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;
import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;

/**
 * @author setyaka
 *
 */
public class CalloutOrder extends CalloutEngine implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutOrder() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		String retVal = null;
		if(mTab.getTableName().equals(MOrderLine.Table_Name) 
				&& mField.getColumnName().equals(MOrderLine.COLUMNNAME_TargetLineNetAmt))
			retVal  = TargetLineNetAmt(ctx, WindowNo, mTab, mField, value, oldValue);
		else if(mTab.getTableName().equals(MOrderLine.Table_Name) 
				&& mField.getColumnName().equals(MOrderLine.COLUMNNAME_DiscountAmt))
			;//retVal  = DiscountAmt(ctx, WindowNo, mTab, mField, value, oldValue);
		else if((mTab.getTableName().equals(MOrder.Table_Name) && mField.getColumnName().equals(MOrder.COLUMNNAME_Discount)) 
				|| (mTab.getTableName().equals(MOrder.Table_Name) && mField.getColumnName().equals(MOrder.COLUMNNAME_DiscountAmt)))
			retVal  = setAdditionalDiscount(ctx, WindowNo, mTab, mField, value, oldValue);
		
		return retVal;
	}

	private String setAdditionalDiscount(
			Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value, Object oldValue) 
	{
		if (value == null)
			value = Env.ZERO;
		
		BigDecimal discountPersen = Env.ZERO;
		BigDecimal discountAmt = Env.ZERO;
		Object totalLinesObjt = mTab.getValue(MOrder.COLUMNNAME_TotalLines);
		BigDecimal totalLines = (totalLinesObjt == null)? Env.ZERO : (BigDecimal) totalLinesObjt;
		String columnToChange = null;
		
		if (mField.getColumnName().equals(MOrder.COLUMNNAME_Discount)) {
			discountPersen = (BigDecimal) value;
			discountAmt = totalLines.multiply(discountPersen.divide(Env.ONEHUNDRED, 10, BigDecimal.ROUND_HALF_DOWN));
			columnToChange = MOrder.COLUMNNAME_DiscountAmt;
		}
		else if (mField.getColumnName().equals(MOrder.COLUMNNAME_DiscountAmt) 
				&& totalLines.signum() != 0) {
			discountAmt = (BigDecimal) value;
			discountPersen = discountAmt.divide(totalLines, 10, BigDecimal.ROUND_HALF_DOWN);
			columnToChange = MOrder.COLUMNNAME_Discount;
		}
		
		mTab.setValue(columnToChange, discountPersen);
		
		return null;
	}

	@Deprecated
	String DiscountAmt(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value,
			Object oldValue) {
		BigDecimal dicountAmt = (BigDecimal) value;
		BigDecimal qtyOrdered = (BigDecimal) mTab.getValue(MOrderLine.COLUMNNAME_QtyOrdered);
		BigDecimal priceList = (BigDecimal) mTab.getValue(MOrderLine.COLUMNNAME_PriceList);

		BigDecimal lineNetAmt = priceList.multiply(qtyOrdered);
		BigDecimal newLineNetAmt = lineNetAmt.subtract(dicountAmt);
		BigDecimal targetPrice = newLineNetAmt.divide(qtyOrdered, 2, RoundingMode.HALF_UP);
		
		BigDecimal targetLineNetAmt = lineNetAmt.multiply(new BigDecimal(1.1)).setScale(2, RoundingMode.UP);
		
		mTab.setValue(MOrderLine.COLUMNNAME_PriceEntered, targetPrice);
		mTab.setValue(MOrderLine.COLUMNNAME_PriceActual, targetPrice);

		mTab.setValue(MOrderLine.COLUMNNAME_TargetLineNetAmt, targetLineNetAmt);
		
		return null;
	}

	private String TargetLineNetAmt(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value,
			Object oldValue) {
		if(value == null)
			return "";
		
		BigDecimal targetLineNetAmt = (BigDecimal) value;
		BigDecimal qtyOrdered = (BigDecimal) mTab.getValue(MOrderLine.COLUMNNAME_QtyOrdered);
		BigDecimal targetAmtNonPPN = targetLineNetAmt.divide(new BigDecimal(1.1), 10, RoundingMode.HALF_UP);
		BigDecimal targetPrice = targetAmtNonPPN.divide(qtyOrdered, 2, RoundingMode.HALF_UP);
		
		BigDecimal priceList = (BigDecimal) mTab.getValue(MOrderLine.COLUMNNAME_PriceList);
		BigDecimal lineNetAmt = priceList.multiply(qtyOrdered);
		BigDecimal dicountAmt = lineNetAmt.subtract(targetLineNetAmt.divide(new BigDecimal(1.1), 2, RoundingMode.UP));
		
		mTab.setValue(MOrderLine.COLUMNNAME_PriceEntered, targetPrice);
		mTab.setValue(MOrderLine.COLUMNNAME_PriceActual, targetPrice);

		mTab.setValue(MOrderLine.COLUMNNAME_DiscountAmt, dicountAmt);
		
		return null;
	}
}
