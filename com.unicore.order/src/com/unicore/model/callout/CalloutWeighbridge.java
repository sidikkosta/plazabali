/**
 * 
 */
package com.unicore.model.callout;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MOrderLine;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.base.model.MOrder;
import com.unicore.model.I_UNS_WBTicket_Confirm;
import com.unicore.model.I_UNS_WeighbridgeTicket;
import com.unicore.model.MUNSWBTicketConfirm;
import com.unicore.model.MUNSWeighbridgeTicket;
import com.uns.model.X_UNS_ArmadaType;

/**
 * @author AzHaidar
 *
 */
public class CalloutWeighbridge extends CalloutEngine implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutWeighbridge() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) 
	{
		String retVal = null;
		
		if (mTab.getTableName().equals(MUNSWeighbridgeTicket.Table_Name))
		{
			if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_C_DocType_ID)) {
				retVal = updateDocType(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_C_Order_ID)
					|| mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_M_Product_ID)) {
				retVal = updateOrderDetail(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_GrossWeight)
					|| mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_Tare)) {
				retVal = recalculateNetto(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_UNS_ArmadaType_ID)) {
				retVal = changeArmadaType(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_IsSplitOrder)) {
				retVal = changeSplitStatus(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_Transporter_ID)) {
				retVal = fillTransporterName(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_TimeInNumber)) {
				retVal = TimeASString(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_TimeOutNumber)) {
				retVal = TimeASString(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_UNS_BPartner_Driver_ID)) {
				retVal = updateDriver(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			
		}
		else if (mTab.getTableName().equals(I_UNS_WBTicket_Confirm.Table_Name))
		{
			if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_GrossWeight)
					|| mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_Tare)) {
				retVal = recalculateNetto(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if(mField.getColumnName().equals(MUNSWBTicketConfirm.COLUMNNAME_NettoII))
			{
				retVal =  recalculateWeight(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			
		}
				
		return retVal;
	} //start
	
	public String fillTransporterName(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (value == null)
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_TransporterName, value);
		else {
			Integer transporter_ID = (Integer) value;
			MBPartner transporter = MBPartner.get(ctx, transporter_ID);
			
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_TransporterName, 
					transporter.getName());
		}
		
		return null;
	} //fillTransporterName
	
	public String changeSplitStatus(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		Boolean isSplit = value == null? false : value.equals(true)? true : false;
		
		I_UNS_WeighbridgeTicket wbt = GridTabWrapper.create(mTab, I_UNS_WeighbridgeTicket.class);
		
		if (!isSplit)
		{
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalNettoI, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalNettoII, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalReflection, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_IsOriginalTicket, false);
		}
		else {
			
			wbt.setOriginalReflection(wbt.getReflection());
			wbt.setOriginalNettoI(wbt.getGrossWeight().subtract(wbt.getTare()));
			wbt.setOriginalNettoII(wbt.getOriginalNettoI().subtract(wbt.getOriginalReflection()));
			wbt.setIsOriginalTicket(true);
		}
		
		return null;
	} //changeSplitStatus
	
	public String changeSplitConfirmStatus(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		Boolean isSplit = value == null? false : value.equals(true)? true : false;
		
		if (!isSplit)
		{
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalNettoI, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalNettoII, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalReflection, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_IsOriginalTicket, false);
		}
		else {
			I_UNS_WBTicket_Confirm wbt = GridTabWrapper.create(mTab, I_UNS_WBTicket_Confirm.class);
			
			wbt.setOriginalReflection(wbt.getReflection());
			wbt.setOriginalNettoI(wbt.getGrossWeight().subtract(wbt.getTare()));
			wbt.setOriginalNettoII(wbt.getOriginalNettoI().subtract(wbt.getOriginalReflection()));
			wbt.setIsOriginalTicket(true);
		}
		
		return null;
	} //changeSplitStatus
	
	/*
	 * Update weighbridge ticket detail based on new selected document type 
	 * in the UNS_WeighbridgeTicket table/window.
	 */
	public String updateDocType(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (value == null)
			return null;
		
		int C_DocType_ID = (Integer) value;
		
		MDocType dt = MDocType.get(ctx, C_DocType_ID);
		
		//String isSOTrx = dt.isSOTrx()? "Y" : "N";
		mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_IsSOTrx, dt.isSOTrx());
		mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_C_Order_ID, null);
		//mTab.setV
		
		return null;
	}
	
	/*
	 * Update order detail in the UNS_WeighbridgeTicket table/window.
	 */
	public String updateOrderDetail(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (isCalloutActive())
			return "";

		if (value == null || (Integer) value == 0)
			return null;
		
		int C_Order_ID = 0;
		int M_Product_ID = 0;
		
		if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_C_Order_ID))
		{
			C_Order_ID = (Integer) value;
			MOrder order = new MOrder(ctx, C_Order_ID, null);
			MOrderLine[] lines = order.getLines();
			
			mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_C_BPartner_ID, order.getC_BPartner_ID());
			mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_IsSOTrx, order.isSOTrx());
			mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_Transporter_ID, order.getC_BPartner_ID());
			mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_TransporterName, order.getC_BPartner().getName());
			mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_AD_Org_ID, order.getAD_Org_ID());
			
			Object ProdID = mTab.getValue(MUNSWeighbridgeTicket.COLUMNNAME_M_Product_ID);
			
			if (ProdID == null) 
			{
				if (lines.length > 0) 
				{
					mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_M_Product_ID, lines[0].getM_Product_ID());
					M_Product_ID = lines[0].getM_Product_ID();
				}
				else
					return "Order doesn't have any lines yet.";
			}
			else
				M_Product_ID = (Integer) ProdID;
		}
		else if (mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_M_Product_ID)) 
		{
			M_Product_ID = (Integer) value;
			Object OrderID = mTab.getValue(MUNSWeighbridgeTicket.COLUMNNAME_C_Order_ID);
			
			if (OrderID == null)
				return null;
			else
				C_Order_ID = (Integer) OrderID;
		}
		
		String sql = "SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID=? AND M_Product_ID=?";
		Integer C_OrderLine_ID = DB.getSQLValueEx(null, sql, C_Order_ID, M_Product_ID);
		
		if (C_OrderLine_ID < 0)
			C_OrderLine_ID = null;
		
		mTab.setValue(MUNSWeighbridgeTicket.COLUMNNAME_C_OrderLine_ID, C_OrderLine_ID);
		
		return null;
	}

	/*
	 * Recalculate field Netto-I dan Netto-II based on user input.
	 */
	public String recalculateNetto(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (value == null)
			return "Please enter a number for " + mField.getColumnName();
		
		if (((BigDecimal) value).signum() < 0) {
			mTab.setValue(mField, ((BigDecimal) value).abs());
			//showNegativeWarning = true;
		}
		
		//I_UNS_WeighbridgeTicket ticket = GridTabWrapper.create(mTab, I_UNS_WeighbridgeTicket.class);
		Boolean isSplitOrder = (Boolean) mTab.getValue(I_UNS_WeighbridgeTicket.COLUMNNAME_IsSplitOrder);
		Boolean isOriginalTicket = (Boolean) mTab.getValue(I_UNS_WeighbridgeTicket.COLUMNNAME_IsOriginalTicket);
		
		BigDecimal grossWeight = (BigDecimal) mTab.getValue("GrossWeight");
		BigDecimal tare = (BigDecimal) mTab.getValue("Tare");
		BigDecimal reflection = (BigDecimal) mTab.getValue("Reflection");
		
		BigDecimal netto1  = grossWeight.subtract(tare);
		BigDecimal netto2  = netto1.subtract(reflection);

		if (isSplitOrder != null && isOriginalTicket != null && isSplitOrder && isOriginalTicket)
		{
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalNettoI, netto1);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_OriginalNettoII, netto2);
		}
		else {
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_NettoI, netto1);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_NettoII, netto2);
		}
//		if (showNegativeWarning)
//			return "Please enter only positive value.";
//		else
		return null;
	}
	
	public String recalculateWeight(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue){
		
		if (value == null)
			return "Please enter a number for " + mField.getColumnName();
		
		Boolean isSplitOrder = (Boolean) mTab.getValue(I_UNS_WBTicket_Confirm.COLUMNNAME_IsSplitOrder);
		
		BigDecimal netto2 = (BigDecimal) mTab.getValue("NettoII");
		BigDecimal reflection = (BigDecimal) mTab.getValue("Reflection");
		BigDecimal tare = (BigDecimal) mTab.getValue("Tare");
		
		BigDecimal netto1 = netto2.add(reflection);
		BigDecimal grossWeight = netto1.add(tare);
		
		if(!isSplitOrder)
		{
			mTab.setValue(I_UNS_WBTicket_Confirm.COLUMNNAME_NettoI, netto1);
			mTab.setValue(I_UNS_WBTicket_Confirm.COLUMNNAME_GrossWeight, grossWeight);
		}
		return null;
	}
	
	public String changeArmadaType(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		Integer armadaType_ID = (Integer) value;
		
		mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_VehicleType, 
				(new X_UNS_ArmadaType(ctx, armadaType_ID, null).getVehicleType()));
		
		return null;
	} // changeArmadaType
	
	private String TimeASString(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{		
		String timeString = (String) value;
		
		if(Util.isEmpty(timeString, true))
			return null;
		
		int length = timeString.length();
		if(length != 4)
			return null;
		int timeInt = new Integer (timeString);
		
		if(timeInt > -1 && timeInt < 2360)
		{
			Timestamp dateDoc = (Timestamp) mTab.getValue(I_UNS_WeighbridgeTicket.COLUMNNAME_DateDoc);
			String date = dateDoc.toString();
			String dateTimeString = date.substring(0, 10) + " " + timeString.substring(0, 2) + ":" + timeString.substring(2) + ":00";
			Timestamp dateTime = null;
			dateTime = Timestamp.valueOf(dateTimeString);
			
			if(mField.getColumnName().equals(MUNSWeighbridgeTicket.COLUMNNAME_TimeInNumber))
			{
				mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_TimeIn, dateTime);
			}
			else
			{
				mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_TimeOut, dateTime);
			}
		}			
		
		return null;
	}
	
	public String updateDriver(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue){
		
		if(value == null)
		{
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_VehicleDriverName, null);
			mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_DriverLicense, null);
			return null;
		}
		
		int driverid = (Integer) value;
		String sql = "SELECT CONCAT(Name,'-',DriverLicense) FROM UNS_BPartner_Driver WHERE UNS_BPartner_Driver_ID = ? ";
		String values = DB.getSQLValueString(null, sql, driverid);
		String[] split = values.split("-");
		
		mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_VehicleDriverName, split[0]);
		mTab.setValue(I_UNS_WeighbridgeTicket.COLUMNNAME_DriverLicense, split[1]);
		
		return null;
	}
}
