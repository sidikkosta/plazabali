/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MCost;
import org.compiere.model.MProduct;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.I_UNS_AttributeNoCost;
import com.unicore.model.MUNSAttributeNoCost;

/**
 * @author Burhani Adam
 *
 */
public class CalloutAttributeNoCost implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutAttributeNoCost() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(mTab.getTableName().equals(MUNSAttributeNoCost.Table_Name))
		{
			if(mField.getColumnName().equals(MUNSAttributeNoCost.COLUMNNAME_M_AttributeSetInstanceTo_ID))
				return asiTo(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSAttributeNoCost.COLUMNNAME_ETBBCode))
				return ETBB(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		return null;
	}
	
	public String asiTo(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(value == null || (Integer) value == 0)
		{
			mTab.setValue(MUNSAttributeNoCost.COLUMNNAME_CostAmt, null);
			return null;
		}
		I_UNS_AttributeNoCost record = GridTabWrapper.create(mTab, I_UNS_AttributeNoCost.class);
		MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(ctx, record.getAD_Client_ID());
		for(MAcctSchema as : ass)
		{
			if(as.getC_Currency_ID() != 303)
				continue;
			BigDecimal cost = MCost.getCurrentCost(MProduct.get(ctx, record.getM_Product_ID()),
					(Integer) value, as, record.getAD_Org_ID(), null, Env.ONE,
					0, false, null);
			mTab.setValue(MUNSAttributeNoCost.COLUMNNAME_CostAmt, cost);
			break;
		}
		MAttributeSetInstance asi = new MAttributeSetInstance(ctx, (Integer) value, null);
		record.setDateMaterialPolicyTo(asi.getCreated());
//		record.setMovementQty(record.getRemainingQty());
		
		return null;
	}
	
	public String ETBB(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(value == null)
		{
			return asiTo(ctx, WindowNo, mTab, mField, null, null);
		}
		
		I_UNS_AttributeNoCost record = GridTabWrapper.create(mTab, I_UNS_AttributeNoCost.class);
		String sql = "SELECT DISTINCT(asi.M_AttributeSetInstance_ID) FROM M_StorageOnHand soh"
				+ " INNER JOIN M_AttributeSetInstance asi ON asi.M_AttributeSetInstance_ID = soh.M_AttributeSetInstance_ID"
				+ " INNER JOIN M_Locator loc ON loc.M_Locator_ID = soh.M_Locator_ID"
				+ " INNER JOIN M_Warehouse whs ON whs.M_Warehouse_ID = loc.M_Warehouse_ID"
				+ " WHERE soh.M_Product_ID = ? AND whs.UNS_Division_ID = ? AND TRIM(asi.Description) = TRIM(?)"
				+ " AND DATE_TRUNC('Day', asi.Created) = asi.Created";
		int asiID = DB.getSQLValue(null, sql, record.getM_Product_ID(), 
				record.getM_Locator().getM_Warehouse().getUNS_Division_ID(),
					(String) value);
		if(asiID <= 0)
		{
			sql = "SELECT DISTINCT(asi.M_AttributeSetInstance_ID) FROM M_StorageOnHand soh"
					+ " INNER JOIN M_AttributeSetInstance asi ON asi.M_AttributeSetInstance_ID = soh.M_AttributeSetInstance_ID"
					+ " INNER JOIN M_Locator loc ON loc.M_Locator_ID = soh.M_Locator_ID"
					+ " INNER JOIN M_Warehouse whs ON whs.M_Warehouse_ID = loc.M_Warehouse_ID"
					+ " WHERE soh.M_Product_ID = ? AND whs.UNS_Division_ID = ? AND TRIM(asi.Description) = TRIM(?)";
			asiID = DB.getSQLValue(null, sql, record.getM_Product_ID(), 
					record.getM_Locator().getM_Warehouse().getUNS_Division_ID(),
						(String) value);
		}
		
		if(asiID <= 0)
		{
			mTab.setValue(MUNSAttributeNoCost.COLUMNNAME_DateMaterialPolicyTo, null);
			mTab.setValue(MUNSAttributeNoCost.COLUMNNAME_M_AttributeSetInstanceTo_ID, null);
//			mTab.fireDataStatusEEvent("New ETBB.?", "ETBB with code " + (String) value + " is not found. "
//					+ "This action will be create new ETBB with ETBB date based on movement date.", false);
			mTab.fireDataStatusEEvent("Warning.", "ETBB Not Found. " + (String) value, false);
			mTab.setValue(MUNSAttributeNoCost.COLUMNNAME_ETBBCode, null);
		}
		else
		{mTab.setValue(MUNSAttributeNoCost.COLUMNNAME_M_AttributeSetInstanceTo_ID, asiID);
			return asiTo(ctx, WindowNo, mTab, mField, asiID, null);
		}
		
		return null;
	}
}