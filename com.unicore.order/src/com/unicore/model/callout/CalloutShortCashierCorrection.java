/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MConversionRate;
import org.compiere.model.MConversionType;
import org.compiere.model.MUser;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSSessionCashAccount;
import com.unicore.model.MUNSShortCashierCorrLine;

/**
 * @author Burhani Adam
 *
 */
public class CalloutShortCashierCorrection implements IColumnCallout{

	/**
	 * 
	 */
	public CalloutShortCashierCorrection() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID)
					|| mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID))
			return pos(ctx, WindowNo, mTab, mField, value, oldValue);
		else if(mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID))
			return cashier(ctx, WindowNo, mTab, mField, value, oldValue);
		else if(mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_UNS_Employee_ID))
			return employee(ctx, WindowNo, mTab, mField, value, oldValue);
		else if(mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmt)
				|| mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB1)
					|| mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB2))
			return correctionAmt(ctx, WindowNo, mTab, mField, value, oldValue);
		
		return null;
	}
	
	private String pos(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		 int session = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID) == null ? 
				 0 :(Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID);
		 int currency = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID) == null ?
				 0 : (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID);
		 
		 if(session > 0)
		 {
			 MUNSPOSSession ps = new MUNSPOSSession(ctx, session, null);
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_Base1Currency_ID, ps.getBase1Currency_ID());
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_Base2Currency_ID, ps.getBase2Currency_ID());
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID, ps.getCashier_ID());
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_DateTrx, ps.getDateAcct());
			 MUser user = MUser.get(ctx, ps.getCashier_ID());
			 if(user != null & user.getUNS_Employee_ID() > 0)
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_Employee_ID, user.getUNS_Employee_ID());
			 if(currency <= 0)
			 {
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, Env.ZERO);
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, Env.ZERO);
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, Env.ZERO);
			 }
			 else
			 {
				 MUNSSessionCashAccount cashAcct = ps.getCashAcct(currency);
				 if(cashAcct != null)
				 {
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, cashAcct.getShortCashierAmt());
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, cashAcct.getShortCashierAmtBase1());
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, cashAcct.getShortCashierAmtBase2());
				 }
				 else
				 {
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, Env.ZERO);
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, Env.ZERO);
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, Env.ZERO);
				 }
			 }
			 
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_Store_ID, ps.getUNS_POSTerminal().getStore_ID());
		 }
		 else
		 {
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, Env.ZERO);
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, Env.ZERO);
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, Env.ZERO);
		 }
		
		return null;
	}
	
	private String cashier(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		 int cashier = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID) == null ?
				 0 : (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID);
		 if(cashier > 0)
		 {
			 MUser user = MUser.get(ctx, cashier);
			 if(user.getUNS_Employee_ID() <= 0)
				 throw new AdempiereException("Cashier doesn't have employee record.");
			 else
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_Employee_ID, user.getUNS_Employee_ID());
		 }
		 else
			 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_Employee_ID, null);
		 
		 int session = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID) == null ? 
				 0 :(Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID);
		 int currency = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID) == null ?
				 0 : (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID);
		 if(session > 0 && cashier > 0)
		 {
			 MUNSPOSSession ss = new MUNSPOSSession(ctx, session, null);
			 if(ss.getCashier_ID() != cashier)
			 {
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, Env.ZERO);
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, Env.ZERO);
				 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, Env.ZERO);
			 }
			 else
			 {
				 MUNSSessionCashAccount cashAcct = ss.getCashAcct(currency);
				 if(cashAcct != null)
				 {
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, cashAcct.getShortCashierAmt());
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, cashAcct.getShortCashierAmtBase1());
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, cashAcct.getShortCashierAmtBase2());
				 }
				 else
				 {
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmt, Env.ZERO);
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB1, Env.ZERO);
					 mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_OriginalAmtB2, Env.ZERO);
				 }
			 }
		 }
		 
		 return null;
	}
	
	private String correctionAmt(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		int session = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID) == null ? 
				 0 :(Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_UNS_POS_Session_ID);
		int currency = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID) == null ?
				 0 : (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_C_Currency_ID);
		
		if(session > 0 && currency > 0)
		{
			BigDecimal amt = value == null ? Env.ZERO : (BigDecimal) value;
			MUNSPOSSession ss = new MUNSPOSSession(ctx, session, null);
			int conversionTypeID = MConversionType.getDefault((Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_AD_Client_ID));
			Timestamp conversionDate = ss.getDateClosed();
			if(ss != null)
			{
				if(mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmt))
				{
					if(currency == ss.getBase1Currency_ID())
						mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB1, amt);
					else
						mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB1, 
								getConvertedCorrection(ctx, (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_AD_Client_ID),
										(Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_AD_Org_ID), currency, 
											ss.getBase1Currency_ID(), amt, conversionDate, conversionTypeID));
						
					if(currency == ss.getBase2Currency_ID())
						mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB2, amt);
					else
						mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB2,
								getConvertedCorrection(ctx, (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_AD_Client_ID),
										(Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_AD_Org_ID), ss.getBase1Currency_ID(), 
											ss.getBase2Currency_ID(), (BigDecimal) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB1), conversionDate, conversionTypeID));
				}
//				else if(mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB1))
//				{
//					if(currency == ss.getBase1Currency_ID())
//						mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmt, amt);
//				}
//				else if(mField.getColumnName().equals(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmtB2))
//				{
//					if(currency == ss.getBase2Currency_ID())
//						mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_CorrectionAmt, amt);
//				}
			}
		}
		
		return null;
	}
	private String employee(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null || (Integer) value == 0)
		{
			 int cashier = mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID) == null ?
					 0 : (Integer) mTab.getValue(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID);
			 return cashier(ctx, WindowNo, mTab, mField, cashier, null);
		}
		
		String sql = "SELECT AD_User_ID FROM AD_User WHERE UNS_Employee_ID = ?";
		int user = DB.getSQLValue(null, sql, (Integer) value);
		if(user <= 0)
			throw new AdempiereException("Employee doesn't have user cashier record.");
		mTab.setValue(MUNSShortCashierCorrLine.COLUMNNAME_Cashier_ID, user);
		return cashier(ctx, WindowNo, mTab, mField, user, null);
	}
	
	private BigDecimal getConvertedCorrection(Properties ctx, int ClientID, int OrgID, 
			int CurrencyFromID, int CurrencyToID, BigDecimal amt, Timestamp conversionDate, int ConversiontTypeID)
	{
		BigDecimal convertedAmt = MConversionRate.convert(ctx, amt, CurrencyFromID, CurrencyToID, conversionDate, 
				ConversiontTypeID, ClientID, OrgID);
		if(convertedAmt == null)
			convertedAmt = Env.ZERO;
		return convertedAmt;
	}
}