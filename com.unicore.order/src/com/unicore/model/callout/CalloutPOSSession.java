package com.unicore.model.callout;

import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.unicore.model.I_UNS_POS_Session;
import com.unicore.model.MUNSPOSSession;

public class CalloutPOSSession extends CalloutEngine implements IColumnCallout {

	public CalloutPOSSession() {
		super();
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) 
	{
		String retVal = null;
		
		if(mTab.getTableName().equals(MUNSPOSSession.Table_Name))
		{
			if(mField.getColumnName().equals(MUNSPOSSession.COLUMNNAME_DateDoc))
				retVal = mTab.setValue(I_UNS_POS_Session.COLUMNNAME_DateAcct, (Timestamp) value);	
		}
		
		return retVal;
	}
}