package com.unicore.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.unicore.model.I_UNS_VoucherBook;
import com.unicore.model.MUNSVoucherBook;

public class CalloutVoucherBook extends CalloutEngine implements IColumnCallout {

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		String retVal = null;
		if(mTab.getTableName().equals(MUNSVoucherBook.Table_Name)){
			if(mField.getColumnName().equals(MUNSVoucherBook.COLUMNNAME_EndNo) ||
				mField.getColumnName().equals(MUNSVoucherBook.COLUMNNAME_StartNo) ||
				mField.getColumnName().equals(MUNSVoucherBook.COLUMNNAME_Count))
				retVal = countNo(ctx, WindowNo, mTab, mField, value, oldValue);
				
		}
		return retVal;
	}

	public String countNo(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue){
		if(value == null)
			return null;
		
		int startNo = (Integer) mTab.getValue(I_UNS_VoucherBook.COLUMNNAME_StartNo);
		int endNo = (Integer) mTab.getValue(I_UNS_VoucherBook.COLUMNNAME_EndNo);
		int count = (Integer) mTab.getValue(I_UNS_VoucherBook.COLUMNNAME_Count);
		
		if(startNo > endNo)
			return null;
		
		if(mField.getColumnName().equals(I_UNS_VoucherBook.COLUMNNAME_Count))
		{
			endNo = startNo + count - 1;
			mTab.setValue(I_UNS_VoucherBook.COLUMNNAME_EndNo, endNo);
			return null;
		}
		
		count = endNo - startNo + 1;
		mTab.setValue(I_UNS_VoucherBook.COLUMNNAME_Count, count);
		
		return null;
	}
	
}
