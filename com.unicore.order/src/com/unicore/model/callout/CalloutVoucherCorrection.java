/**
 * 
 */
package com.unicore.model.callout;

import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.I_UNS_VoucherCorrection;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPaymentTrx;
import com.unicore.model.MUNSVoucherCode;
import com.unicore.model.MUNSVoucherCorrection;

/**
 * @author Burhani Adam
 *
 */
public class CalloutVoucherCorrection implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutVoucherCorrection() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if(mField.getColumnName().equals(MUNSVoucherCorrection.COLUMNNAME_UNS_POS_Session_ID))
			return session(ctx, WindowNo, mTab, mField, value, oldValue);
		if(mField.getColumnName().equals(MUNSVoucherCorrection.COLUMNNAME_VoucherCode))
			return voucherCode(ctx, WindowNo, mTab, mField, value, oldValue);
		if(mField.getColumnName().equals(MUNSVoucherCorrection.COLUMNNAME_UNS_VoucherCode_ID))
			return voucherCodeID(ctx, WindowNo, mTab, mField, value, oldValue);
		if(mField.getColumnName().equals(MUNSVoucherCorrection.COLUMNNAME_UNS_PaymentTrx_ID))
			return paymentTrx(ctx, WindowNo, mTab, mField, value, oldValue);
		
					
		return null;
	}
	
	private String session(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null || (Integer) value == 0)
		{
			return null;
		}
		
		MUNSPOSSession ss = new MUNSPOSSession(ctx, (Integer) value, null);
		I_UNS_VoucherCorrection corr = GridTabWrapper.create(mTab, I_UNS_VoucherCorrection.class);
		corr.setStore_ID(ss.getUNS_POSTerminal().getStore_ID());
			
		return null;
	}
	
	private String voucherCode(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null)
		{
			return voucherCodeID(ctx, WindowNo, mTab, mField, null, null);
		}
		String sql = "SELECT vc.UNS_VoucherCode_ID FROM UNS_VoucherCode vc WHERE vc.Name = ? AND EXISTS"
				+ " (SELECT 1 FROM UNS_VoucherBook vb WHERE vb.UNS_VoucherBook_ID = vc.UNS_VoucherBook_ID"
				+ " AND vb.DocStatus IN ('CO', 'CL') AND vb.C_Currency_ID = ? AND ? BETWEEN vb.ValidFrom AND vb.ValidTo)"
				+ " AND vc.UnusedAmt > 0 AND vc.isInvalidate = 'N'";
		int voucherCodeID = DB.getSQLValue(null, sql, new Object[]{
				value, (Integer) mTab.getValue(MUNSVoucherCorrection.COLUMNNAME_C_Currency_ID),
						(Timestamp) mTab.getValue(MUNSVoucherCorrection.COLUMNNAME_DateTrx)
				});
		if(voucherCodeID <= 0)
			mTab.fireDataStatusEEvent("Error", "Voucher not found or has been used.!", true);
		
		mTab.setValue(MUNSVoucherCorrection.COLUMNNAME_UNS_VoucherCode_ID, voucherCodeID);
		return voucherCodeID(ctx, WindowNo, mTab, mField, voucherCodeID, null);
	}
	
	private String voucherCodeID(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		I_UNS_VoucherCorrection corr = GridTabWrapper.create(mTab, I_UNS_VoucherCorrection.class);
		if(value == null)
		{
			corr.setVoucherAmt(Env.ZERO);
			corr.setUnusedAmt(Env.ZERO);
			corr.setUsedAmt(Env.ZERO);
			return null;
		}
		
		MUNSVoucherCode vc = new MUNSVoucherCode(ctx, (Integer) value, null);
		corr.setVoucherAmt(vc.getVoucherAmt());
		corr.setUnusedAmt(vc.getUnusedAmt());
		corr.setUsedAmt(vc.getUsedAmt());
		corr.setVoucherCode(vc.getName());
		
		return null;
	}
	
	private String paymentTrx(
			Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null)
		{
			mTab.setValue(MUNSVoucherCorrection.COLUMNNAME_TrxAmt, Env.ZERO);
			return null;
		}
		
		MUNSPaymentTrx trx = new MUNSPaymentTrx(ctx, (Integer) value, null);
		mTab.setValue(MUNSVoucherCorrection.COLUMNNAME_TrxAmt, trx.getTrxAmt());
		
		return null;
	}
}