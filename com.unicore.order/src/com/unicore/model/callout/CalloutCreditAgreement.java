/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MProductPricing;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.I_UNS_CreditAgreement;

/**
 * @author Menjangan
 *
 */
public class CalloutCreditAgreement implements IColumnCallout 
{

	/**
	 * 
	 */
	public CalloutCreditAgreement() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) 
	{
		String msg = null;
		if (null == value)
			return null;
		I_UNS_CreditAgreement iCreditAgreement = GridTabWrapper.create(mTab, 
				I_UNS_CreditAgreement.class);
		
		if ("NIP".equals(mField.getColumnName())) 
		{
			msg = NIP(iCreditAgreement);
		} 
		else if ("C_BPartner_ID".equals(mField.getColumnName())) 
		{
			msg = C_BPartner_ID(iCreditAgreement);
		}
		else if ("DateDoc".equals(mField.getColumnName())) 
		{
			msg = DocumentDate(iCreditAgreement);
		}
		else if ("M_Product_ID".equals(mField.getColumnName())) 
		{
			msg = M_Product_ID(iCreditAgreement);
		}
		else if ("MonthlyInstallmentAmt".equals(mField.getColumnName()))
		{
			msg = monthlyInstallmentAmt(iCreditAgreement);
		}
		else if ("TDP".equals(mField.getColumnName()))
		{
			msg = TDP(iCreditAgreement);
		}
		else if ("Discount".equals(mField.getColumnName()))
		{
			msg = discount(iCreditAgreement);
		}
		else if ("DiscountAmt".equals(mField.getColumnName()))
		{
			msg = discountAmt(iCreditAgreement);
		}
		else if ("InstallmentPeriod".equals(mField.getColumnName()))
		{
			msg = installmentPeriod(iCreditAgreement);
		}
		else if ("Margin".equals(mField.getColumnName()))
		{
			msg = margin(iCreditAgreement);
		}
		else if ("MarginAmt".equals(mField.getColumnName()))
		{
			msg = marginAmt(iCreditAgreement);
		}
		else if ("MonthlyInterestAmt".equals(mField.getColumnName()))
		{
			msg = monthlyMarginAmt(iCreditAgreement);
		}
		else if ("MonthlyInterest".equals(mField.getColumnName()))
		{
			msg = monthlyMargin(iCreditAgreement);
		}
		else if ("Qty".equals(mField.getColumnName())
				|| "PriceActual".equals(mField.getColumnName())
				|| "PriceList".equals(mField.getColumnName()))
		{
			msg = onPriceQty(iCreditAgreement);
		}
		return msg;
	}
	
	private String onPriceQty (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal priceActual = iCreditAgreement.getPriceActual();
		BigDecimal priceList = iCreditAgreement.getPriceList();
		BigDecimal difference = priceList.subtract(priceActual);
		
		if (difference.signum() != 0)
		{
			BigDecimal discountAmt = difference.multiply(iCreditAgreement.getQty());
			iCreditAgreement.setDiscountAmt(discountAmt);
			return discountAmt(iCreditAgreement);
		}
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String DocumentDate (I_UNS_CreditAgreement iCreditAgreement)
	{
		iCreditAgreement.setDateAcct(iCreditAgreement.getDateDoc());
		return null;
	}
	
	private String C_BPartner_ID (I_UNS_CreditAgreement iCreditAgreement)
	{
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE "
				+ " C_BPartner_ID = ? AND IsActive = ? ORDER BY IsBillTo DESC";
		
		int C_BPartner_Location_ID = DB.getSQLValue(null, sql, 
				iCreditAgreement.getC_BPartner_ID(), "Y");
		
		iCreditAgreement.setC_BPartner_Location_ID(C_BPartner_Location_ID);
		
		return null;
	}
	
	private String NIP (I_UNS_CreditAgreement iCreditAgreement)
	{
		String sql = "SELECT C_BPartner_ID FROM C_BPartner WHERE Value = ? ";
		int partner_ID = DB.getSQLValue(null, sql, iCreditAgreement.getNIP());
		iCreditAgreement.setC_BPartner_ID(partner_ID);
		
		return C_BPartner_ID(iCreditAgreement);
	}
	
	private String M_Product_ID (I_UNS_CreditAgreement iCreditAgreement)
	{		
		MProductPricing pp = new MProductPricing(iCreditAgreement.getM_Product_ID(), 
				iCreditAgreement.getC_BPartner_ID(), iCreditAgreement.getQty(), true, 
				iCreditAgreement.getC_BPartner_Location_ID());
		
		pp.setM_PriceList_ID(iCreditAgreement.getM_PriceList_ID ());
		pp.setPriceDate(iCreditAgreement.getDateDoc());
		
		if (pp.calculatePrice()) 
		{
			iCreditAgreement.setPriceList(pp.getPriceList());
			iCreditAgreement.setPriceActual(pp.getPriceStd());
			iCreditAgreement.setPriceLimit(pp.getPriceLimit());
		}
		
		if (iCreditAgreement.getPriceList().compareTo(iCreditAgreement.getPriceActual()) != 0)
		{
			BigDecimal discountAmt = iCreditAgreement.getPriceList().subtract(
					iCreditAgreement.getPriceActual());
			discountAmt = discountAmt.multiply(new BigDecimal(iCreditAgreement.getInstallmentPeriod()));
			iCreditAgreement.setDiscountAmt(discountAmt);
		}
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String TDP (I_UNS_CreditAgreement iCreditAgreement)
	{	
		return doInitTotals(iCreditAgreement);
	}
	
	private String monthlyInstallmentAmt (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal installmentAmt = iCreditAgreement.getMonthlyInstallmentAmt();
		if (null !=  installmentAmt && installmentAmt.signum() != 0 
				&& iCreditAgreement.getGrandTotal().signum() != 0)
		{
			int period = iCreditAgreement.getInstallmentPeriod();
			BigDecimal periodBD = new BigDecimal(period);
			BigDecimal totalLoanAmt = installmentAmt.multiply(periodBD);
			iCreditAgreement.setTotalLoanAmt(totalLoanAmt);
			BigDecimal marginAmt = totalLoanAmt.subtract(iCreditAgreement.getGrandTotal());
			BigDecimal margin = marginAmt.divide(iCreditAgreement.getGrandTotal(), 10, 
					RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED);
			iCreditAgreement.setMarginAmt(marginAmt);
			iCreditAgreement.setMargin(margin);
			BigDecimal monthlyMarginAmt = marginAmt.divide(periodBD, 10, RoundingMode.HALF_UP);
			BigDecimal monthlyMargin = margin.divide(periodBD, 10, RoundingMode.HALF_UP);
			iCreditAgreement.setMonthlyInterest(monthlyMargin);
			iCreditAgreement.setMonthlyInterestAmt(monthlyMarginAmt);
			
			return null;
		}
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String discount (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal discount = iCreditAgreement.getDiscount();
		BigDecimal lineBefore = iCreditAgreement.getPriceList().multiply(iCreditAgreement.getQty());
		
		if (lineBefore.signum() == 0)
			return null;
		
		BigDecimal discountAmt = discount.multiply(lineBefore).divide(Env.ONEHUNDRED, 10, 
				RoundingMode.HALF_DOWN);
		
		iCreditAgreement.setDiscountAmt(discountAmt);
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String discountAmt (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal discountAmt = iCreditAgreement.getDiscountAmt();
		BigDecimal lineBefore = iCreditAgreement.getPriceList().multiply(iCreditAgreement.getQty());
		
		if (lineBefore.signum() == 0)
			return null;
		
		BigDecimal discount = discountAmt.divide(lineBefore, 10, RoundingMode.HALF_DOWN).
				multiply(Env.ONEHUNDRED);
		iCreditAgreement.setDiscount(discount);
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String margin (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal marginAmt = iCreditAgreement.getMargin();
		marginAmt = marginAmt.multiply(iCreditAgreement.getGrandTotal()).divide(Env.ONEHUNDRED, 10, 
				RoundingMode.HALF_UP);
		iCreditAgreement.setMarginAmt(marginAmt);
		
		BigDecimal periodBD = new BigDecimal(iCreditAgreement.getInstallmentPeriod());
		BigDecimal monthlyMarginAmt = marginAmt.divide(periodBD, 10, RoundingMode.HALF_UP);
		BigDecimal monthlyMargin = iCreditAgreement.getMargin().divide(periodBD, 10, 
				RoundingMode.HALF_UP);
		
		iCreditAgreement.setMonthlyInterest(monthlyMargin);
		iCreditAgreement.setMonthlyInterestAmt(monthlyMarginAmt);
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String marginAmt (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal margin = iCreditAgreement.getMarginAmt();
		if (iCreditAgreement.getGrandTotal().signum() == 0)
			return null;
		
		margin = margin.divide(iCreditAgreement.getGrandTotal(), 10, 
				RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED);
		iCreditAgreement.setMargin(margin);
		
		BigDecimal periodBD = new BigDecimal(iCreditAgreement.getInstallmentPeriod());
		BigDecimal monthlyMarginAmt = iCreditAgreement.getMarginAmt().divide(
				periodBD, 10, RoundingMode.HALF_UP);
		BigDecimal monthlyMargin = margin.divide(periodBD, 10, 
				RoundingMode.HALF_UP);
		iCreditAgreement.setMonthlyInterest(monthlyMargin);
		iCreditAgreement.setMonthlyInterestAmt(monthlyMarginAmt);
		
		return doInitTotals(iCreditAgreement);
	}
	
	private String monthlyMarginAmt (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal monthlyMarginAmt = iCreditAgreement.getMonthlyInterestAmt();
		
		if (iCreditAgreement.getGrandTotal().signum() == 0)
			return null;
		
		BigDecimal monthlyMargin = monthlyMarginAmt.divide(iCreditAgreement.getGrandTotal(), 10 , 
				RoundingMode.HALF_UP).multiply(Env.ONEHUNDRED);
		iCreditAgreement.setMonthlyInterest(monthlyMargin);
		
		BigDecimal periodBD = new BigDecimal(iCreditAgreement.getInstallmentPeriod());
		
		BigDecimal marginAmt = monthlyMarginAmt.multiply(periodBD);
		iCreditAgreement.setMarginAmt(marginAmt);
		
		BigDecimal margin = monthlyMargin.multiply(periodBD);
		iCreditAgreement.setMargin(margin);

		return doInitTotals(iCreditAgreement);
	}
	
	private String monthlyMargin (I_UNS_CreditAgreement iCreditAgreement)
	{
		BigDecimal monthlyMargin = iCreditAgreement.getMonthlyInterest();
		
		if (iCreditAgreement.getGrandTotal().signum() == 0)
			return null;
		BigDecimal monthlyMarginAmt = monthlyMargin.multiply(iCreditAgreement.getGrandTotal()).
				divide(Env.ONEHUNDRED, 10 , RoundingMode.HALF_UP);
		iCreditAgreement.setMonthlyInterestAmt(monthlyMarginAmt);
		
		BigDecimal periodBD = new BigDecimal(iCreditAgreement.getInstallmentPeriod());
		
		BigDecimal marginAmt = monthlyMarginAmt.multiply(periodBD);
		iCreditAgreement.setMarginAmt(marginAmt);
		
		BigDecimal margin = monthlyMargin.multiply(periodBD);
		iCreditAgreement.setMargin(margin);

		return doInitTotals(iCreditAgreement);
	}
	
	private String installmentPeriod (I_UNS_CreditAgreement iCreditAgreement)
	{
		margin(iCreditAgreement);
		return doInitTotals(iCreditAgreement);
	}
	
	private String doInitTotals (I_UNS_CreditAgreement iCreditAgreement) 
	{
		BigDecimal priceList = iCreditAgreement.getPriceList();
		BigDecimal discountAmt = iCreditAgreement.getDiscountAmt();
		BigDecimal downPayment = iCreditAgreement.getTDP();
		BigDecimal qty = iCreditAgreement.getQty();
		
		BigDecimal lineNetAmt = priceList.multiply(qty);
		lineNetAmt = lineNetAmt.subtract(discountAmt);
		iCreditAgreement.setLineNetAmt (lineNetAmt);
		
		BigDecimal priceActual = lineNetAmt.divide(qty, 10, RoundingMode.UP);
		iCreditAgreement.setPriceActual(priceActual);
		
		BigDecimal grandTotal = lineNetAmt.subtract(downPayment);
		iCreditAgreement.setGrandTotal(grandTotal);
		
		BigDecimal totalLoanAmt = grandTotal.add(iCreditAgreement.getMarginAmt());
		iCreditAgreement.setTotalLoanAmt(totalLoanAmt);
		
		BigDecimal totalperiod = new BigDecimal(iCreditAgreement.getInstallmentPeriod());
		BigDecimal monthlyInstallmentAmt = totalLoanAmt.divide(totalperiod, 0, RoundingMode.UP);
		iCreditAgreement.setMonthlyInstallmentAmt(monthlyInstallmentAmt);
		
		return null;
	}
}
