/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.util.DB;

import com.unicore.model.MUNSCustomerPoint;
import com.unicore.model.MUNSItemRewardSelection;
import com.unicore.model.X_UNS_AdditionalPoint;
import com.unicore.model.X_UNS_CustomerRedeem;
import com.unicore.model.X_UNS_CustomerRedeem_Line;

/**
 * @author ALBURHANY
 *
 */
public class CalloutPointSchema implements IColumnCallout {

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		String retValue = null;
		
		if(mField.getColumnName().equals(X_UNS_CustomerRedeem_Line.COLUMNNAME_UNS_ItemReward_Selection_ID))
			retValue = this.UNS_ItemReward_Selection_ID(ctx, WindowNo, mTab, mField, value);
		
		if(mField.getColumnName().equals(X_UNS_CustomerRedeem.COLUMNNAME_UNS_CustomerRedeem_ID))
			retValue = this.UNS_CustomerPoint_ID(ctx, WindowNo, mTab, mField, value);
		
		if(mField.getColumnName().equals(X_UNS_CustomerRedeem.COLUMNNAME_UNS_CustomerPoint_ID))
			retValue = this.UNS_CustomerPoint_ID(ctx, WindowNo, mTab, mField, value);
		
		if(mField.getColumnName().equals(X_UNS_CustomerRedeem.COLUMNNAME_C_BPartner_ID))
			retValue = this.C_BPartner_ID(ctx, WindowNo, mTab, mField, value);
		
		if(mField.getColumnName().equals(X_UNS_AdditionalPoint.COLUMNNAME_C_BPartner_ID))
			retValue = this.AdditionalPoint(ctx, WindowNo, mTab, mField, value);
		
		return retValue;
	}
	
	private String UNS_ItemReward_Selection_ID(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		if(null == value || (Integer) value <=0 )
		{
			mTab.setValue(X_UNS_CustomerRedeem_Line.COLUMNNAME_M_Product_ID, null);
			mTab.setValue(X_UNS_CustomerRedeem_Line.COLUMNNAME_ValuePoint, null);
			return "";
		}
		
		MUNSItemRewardSelection irSelection = new MUNSItemRewardSelection(ctx, (Integer) value, null);
		
		mTab.setValue(X_UNS_CustomerRedeem_Line.COLUMNNAME_M_Product_ID, irSelection.getM_Product_ID());
		mTab.setValue(X_UNS_CustomerRedeem_Line.COLUMNNAME_ValuePoint, irSelection.getValuePoint());
		
		return null;
	}
	
	private String UNS_CustomerPoint_ID(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		if(null == value || (Integer) value <= 0)
		{
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_CurrentPoint, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_C_BPartner_ID, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_C_Location_ID, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Phone, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Fax, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_EMail, null);
			return "";
		}
		
		MUNSCustomerPoint cP = new MUNSCustomerPoint(ctx, (Integer) value, null);
		
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location"
				+ " WHERE C_BPartner_ID = " + cP.getC_BPartner_ID();
		MBPartnerLocation bpLocation = new MBPartnerLocation(ctx, DB.getSQLValue(null, sql),
				null);
		
		BigDecimal CurrentPoint = MUNSCustomerPoint.getCurrentPoint(ctx, cP.getC_BPartner_ID(), cP.getAD_Org_ID(), null);
		
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_CurrentPoint, CurrentPoint);
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_C_BPartner_ID, cP.getC_BPartner_ID());
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_C_Location_ID, bpLocation.getC_Location_ID());
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Phone, bpLocation.getPhone());
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Fax, bpLocation.getFax());
		
		return null;
	}
	
	private String C_BPartner_ID(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		if(null == value || (Integer) value <= 0)
		{
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_CurrentPoint, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_UNS_CustomerPoint_ID, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_C_Location_ID, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Phone, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Fax, null);
			mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_EMail, null);
			return "";
		}
		
		MBPartner bp = new MBPartner(ctx, (Integer) value, null);
		
		MUNSCustomerPoint cp = MUNSCustomerPoint.getByBPartner(ctx, bp.get_ID(), 0, null);
		
		if(cp == null)
			throw new AdempiereException("Business Partner not exist in Customer Point list");
			
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location"
				+ " WHERE C_BPartner_ID = " + bp.get_ID();
		MBPartnerLocation bpLocation = new MBPartnerLocation(ctx, DB.getSQLValue(null, sql),
				null);
		
		BigDecimal CurrentPoint = MUNSCustomerPoint.getCurrentPoint(ctx, cp.getC_BPartner_ID(), cp.getAD_Org_ID(), cp.get_TrxName());
		
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_CurrentPoint, CurrentPoint);
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_UNS_CustomerPoint_ID, cp.get_ID());
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_C_Location_ID, bpLocation.getC_Location_ID());
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Phone, bpLocation.getPhone());
		mTab.setValue(X_UNS_CustomerRedeem.COLUMNNAME_Fax, bpLocation.getFax());
		
		return null;
	}
	
	private String AdditionalPoint(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		if(null == value || (Integer) value <= 0)
		{
			mTab.setValue(X_UNS_AdditionalPoint.COLUMNNAME_ValuePoint, null);
			return "";
		}
		
		BigDecimal CurrentPoint = MUNSCustomerPoint.getCurrentPoint(ctx, (Integer) value, 0, null);
		
		mTab.setValue(X_UNS_AdditionalPoint.COLUMNNAME_CurrentPoint, CurrentPoint);
		
		return null;
	}
}