package com.unicore.model.callout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;

import com.unicore.model.I_UNS_ReflectionRevision_Line;

public class CalloutReflectionRevision implements IColumnCallout {

	BigDecimal qtyPayment = Env.ZERO;
	BigDecimal NettoI = Env.ZERO;
	BigDecimal reflection = Env.ZERO;
	BigDecimal reflectionFlat = Env.ZERO;
	BigDecimal reflectionTicket = Env.ZERO;
	Boolean isFlatDeduction = false;
	
	
	public CalloutReflectionRevision() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		String retVal = null;
		qtyPayment = (BigDecimal) mTab.getValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_QtyPayment); 
		NettoI = (BigDecimal) mTab.getValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_NettoI);
		reflectionFlat = (BigDecimal) mTab.getValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_ReflectionFlat);
		reflectionTicket = (BigDecimal) mTab.getValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_ReflectionTicket);
		isFlatDeduction = (Boolean) mTab.getValueAsBoolean(I_UNS_ReflectionRevision_Line.COLUMNNAME_isFlatDeduction);
		
		reflection = isFlatDeduction ? reflectionFlat : reflectionTicket;
		
		if(mField.getColumnName().equals("RevisionPercent"))
		{
			retVal = calcRevisionQty(ctx, WindowNo, mTab, mField, value, oldValue);
		}else if(mField.getColumnName().equals("RevisionQty"))
		{
			retVal = calcRevisionPercent(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		
		mTab.setValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_QtyPayment, qtyPayment);
		
		return retVal;
	}
	
	public String calcRevisionQty(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		qtyPayment = NettoI.subtract(reflection);
		BigDecimal revision = (BigDecimal) value;
		
		BigDecimal convertReflection = (revision.multiply((qtyPayment.add(reflection)))).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_UP);
		qtyPayment = qtyPayment.subtract(convertReflection).add(reflection);
		mTab.setValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_RevisionQty, convertReflection);
		
		
		return null;
	}
	
	public String calcRevisionPercent(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		
		qtyPayment = NettoI.subtract(reflection);
		BigDecimal revision = (BigDecimal) value;
		
		BigDecimal convertReflection = (revision.divide(qtyPayment.add(reflection), 5, RoundingMode.HALF_UP)).multiply(Env.ONEHUNDRED);
		qtyPayment = qtyPayment.subtract(revision).add(reflection);
		mTab.setValue(I_UNS_ReflectionRevision_Line.COLUMNNAME_RevisionPercent, convertReflection);
		
		return null;
	}
}
