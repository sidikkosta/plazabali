package com.unicore.model.callout;

import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.TimeUtil;
import org.compiere.util.Util;

import com.unicore.model.I_UNS_Despatch_Notes;
import com.unicore.model.MUNSDespatchNotes;
import com.unicore.model.MUNSWeighbridgeTicket;

public class CalloutDespatchNotes extends CalloutEngine implements
		IColumnCallout {

	public CalloutDespatchNotes() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		String retVal = null;
		
		if(mTab.getTableName().equals(MUNSDespatchNotes.Table_Name))
		{
			if(mField.getColumnName().equals(MUNSDespatchNotes.COLUMNNAME_TimeInNumber))
				retVal = TimeASString(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSDespatchNotes.COLUMNNAME_TimeOutNumber))
				retVal = TimeASString(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSDespatchNotes.COLUMNNAME_IsOverwriteDespatchTime))
				retVal = updateTime(ctx, WindowNo, mTab, mField, value, oldValue);
			else if(mField.getColumnName().equals(MUNSDespatchNotes.COLUMNNAME_SealNumeric))
				retVal = emptySealNumber(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		return retVal;
	}
	
	private String TimeASString(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{		
		String timeString = (String) value;
		
		if(Util.isEmpty(timeString, true))
			return null;
		
		int length = timeString.length();
		if(length != 4)
			return null;
		int timeInt = new Integer (timeString);
		
		if(timeInt > -1 && timeInt < 2360)
		{
			Timestamp dateDoc = (Timestamp) mTab.getValue(I_UNS_Despatch_Notes.COLUMNNAME_DateDoc);
			String date = dateDoc.toString();
			String dateTimeString = date.substring(0, 10) + " " + timeString.substring(0, 2) + ":" + timeString.substring(2) + ":00";
			Timestamp dateTime = null;
			dateTime = Timestamp.valueOf(dateTimeString);
			
			if(mField.getColumnName().equals(MUNSDespatchNotes.COLUMNNAME_TimeInNumber))
			{
				mTab.setValue(I_UNS_Despatch_Notes.COLUMNNAME_StartDespatch, dateTime);
			}
			else
			{
				mTab.setValue(I_UNS_Despatch_Notes.COLUMNNAME_EndDespatch, dateTime);
			}
		}			
		
		return null;
	}
	
	private String updateTime(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{	
		Boolean isoverwrite = (Boolean) value;
		int ticketid = (Integer) mTab.getValue(I_UNS_Despatch_Notes.COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if(isoverwrite)
		{
			MUNSWeighbridgeTicket wbt = new MUNSWeighbridgeTicket(ctx, ticketid, null);
			Timestamp start = wbt.getTimeIn();
			Timestamp end = wbt.getTimeOut();
			
			if(end.after(start))
				mTab.setValue(I_UNS_Despatch_Notes.COLUMNNAME_EndDespatch, end);
			else
				mTab.setValue(I_UNS_Despatch_Notes.COLUMNNAME_EndDespatch, TimeUtil.addMinutess(start, 60));
			
			mTab.setValue(I_UNS_Despatch_Notes.COLUMNNAME_StartDespatch, start);
		}
		
		return null;
	}
	
	private String emptySealNumber(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		Boolean isSealNumeric = (Boolean) value;
		
		if(!isSealNumeric)
			mTab.setValue(I_UNS_Despatch_Notes.COLUMNNAME_SealNumber, null);
		
		return null;
	}
}
