/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;

import com.unicore.base.model.MInOutLine;
import com.unicore.model.MUNSMatchInvoiceCN;
import com.unicore.model.X_UNS_MatchInvoiceCN;

/**
 * @author Burhani Adam
 *
 */
public class CalloutMatchInvoiceCN implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutMatchInvoiceCN() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{	
		String retValue = null;
		
		if(mField.getColumnName().equals(X_UNS_MatchInvoiceCN.COLUMNNAME_M_InOutLine_ID))
			retValue = M_InOutLine_ID(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(X_UNS_MatchInvoiceCN.COLUMNNAME_AmountApplied))
			retValue = AmountApplied(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(X_UNS_MatchInvoiceCN.COLUMNNAME_Amount))
			retValue = amount(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(X_UNS_MatchInvoiceCN.COLUMNNAME_QtySubtract))
			retValue = qtySubtract(ctx, WindowNo, mTab, mField, value);
		else if(mField.getColumnName().equals(X_UNS_MatchInvoiceCN.COLUMNNAME_IsQuantityBased))
			retValue = qtyBased(ctx, WindowNo, mTab, mField, value);
		
		return retValue;
	}
	
	private String M_InOutLine_ID(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		Integer idInOut = (Integer) mTab.getValue(X_UNS_MatchInvoiceCN.COLUMNNAME_M_InOutLine_ID);
		if(null == idInOut || (Integer) idInOut <=0 )
		{
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_M_Product_ID, null);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Qty, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalAmountApplied, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Amount, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_IsFirstRecord, false);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_QtySubtract, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalQtyApplied, Env.ZERO);
			return "";
		}
		
		MInOutLine line = new MInOutLine(ctx, (Integer) idInOut, null);
		boolean isQtyBased = mTab.getValueAsBoolean(X_UNS_MatchInvoiceCN.COLUMNNAME_IsQuantityBased);
		Integer RecordID = (Integer) mTab.getValue(X_UNS_MatchInvoiceCN.COLUMNNAME_UNS_MatchInvoiceCN_ID);
		if(RecordID == null)
			RecordID = 0;
		mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_M_Product_ID, line.getM_Product_ID());
		mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Qty, line.getMovementQty());
		
		if(isQtyBased)
		{
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalQtyApplied, 
					MUNSMatchInvoiceCN.getTotalQtyApplied(line.get_ID(), RecordID, null));
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalAmountApplied, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Amount, Env.ZERO);
		}
		else
		{
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalQtyApplied, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_QtySubtract, Env.ZERO);
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalAmountApplied, 
					MUNSMatchInvoiceCN.getTotalAmountApplied(line.get_ID(), RecordID, null));
			BigDecimal price = MUNSMatchInvoiceCN.getPrevAmount(line.get_ID(), null);
			if(price == null || price.compareTo(Env.ZERO) == 0)
			{
				int invLine = (Integer) mTab.getValue(X_UNS_MatchInvoiceCN.COLUMNNAME_C_InvoiceLine_ID);
				price = line.getMovementQty().multiply(MUNSMatchInvoiceCN.getPrice(ctx, invLine, line.get_ID(), null));
				mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_IsFirstRecord, true);
			}
			mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Amount, price);
		}
		
		return null;
	}
	
	private String AmountApplied(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		BigDecimal amount = (BigDecimal) mTab.getValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Amount);
		BigDecimal totalApplied = (BigDecimal) mTab.getValue(X_UNS_MatchInvoiceCN.COLUMNNAME_TotalAmountApplied);
		BigDecimal balance = Env.ZERO;
		BigDecimal applied = value == null ? Env.ZERO : (BigDecimal) value;
		if(applied.compareTo(Env.ZERO) == 0)
			balance = amount.subtract(totalApplied);
		else
			balance = amount.subtract(totalApplied).subtract(applied);
		
		mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Balance, balance);
		
		return null;
	}
	
	private String amount(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		BigDecimal amount = value == null ? Env.ZERO : (BigDecimal) value;
		BigDecimal applied = (BigDecimal) mTab.getValue(X_UNS_MatchInvoiceCN.COLUMNNAME_AmountApplied);
		BigDecimal balance = amount.subtract(applied);
		
		mTab.setValue(X_UNS_MatchInvoiceCN.COLUMNNAME_Balance, balance);
		
		return null;
	}
	
	private String qtySubtract(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		return M_InOutLine_ID(ctx, windowNo, mTab, mField, value);
	}
	
	private String qtyBased(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value)
	{
		return M_InOutLine_ID(ctx, windowNo, mTab, mField, value);
	}
}
