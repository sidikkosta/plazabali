/**
 * 
 */
package com.unicore.model.callout;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

/**
 * @author Menjangan
 *
 */
public class CalloutPOSTrx implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutPOSTrx() {
		super ();
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		String tableName = mTab.getTableName();
		String columnname = mField.getColumnName();
		if ("UNS_POSTrx".equals(tableName) && "Reference_ID".equals(columnname))
			return reference_ID(ctx, WindowNo, mTab, mField, value, oldValue);
		else if ("UNS_POSTrxLine".equals(tableName) && "Reference_ID".equals(columnname))
			return POSTrxLinereference_ID(ctx, WindowNo, mTab, mField, value, oldValue);
		return null;
	}
	
	public String POSTrxLinereference_ID (Properties ctx, int WindowNo, GridTab mTab, 
			GridField mField, Object value, 
			Object oldValue)
	{
		if (null == value)
			return null;
		
		Integer UNS_POSTrxLine_ID = (Integer) value;
		String sql = "SELECT M_Product_ID, C_UOM_ID, M_Locator_ID, C_Charge_ID,  "
				+ " M_AttributeSetInstance_ID, QtyEntered, QtyOrdered, QtyBonuses, "
				+ " PriceList, PriceActual, PriceLimit, C_Tax_ID, Discount, DiscountAmt "
				+ " FROM UNS_POSTrxLine WHERE UNS_POSTrxLine_ID = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = DB.prepareStatement(sql, null);
			st.setInt(1, UNS_POSTrxLine_ID);
			rs = st.executeQuery();
			if (rs.next())
			{
				mTab.setValue("M_Product_ID", rs.getInt(1));
				mTab.setValue("C_UOM_ID", rs.getInt(2));
				mTab.setValue("M_Locator_ID", rs.getInt(3));
				mTab.setValue("C_Charge_ID", rs.getInt(4));
				mTab.setValue("M_AttributeSetInstance_ID", rs.getInt(5));
				mTab.setValue("QtyEntered", rs.getBigDecimal(6));
				mTab.setValue("QtyOrdered", rs.getBigDecimal(7));
				mTab.setValue("QtyBonuses", rs.getBigDecimal(8));
				mTab.setValue("PriceList", rs.getBigDecimal(9));
				mTab.setValue("PriceActual", rs.getBigDecimal(10));
				mTab.setValue("PriceLimit", rs.getBigDecimal(11));
				mTab.setValue("C_Tax_ID", rs.getInt(12));
				mTab.setValue("Discount", rs.getBigDecimal(13));
				mTab.setValue("DiscountAmt", rs.getBigDecimal(14));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return null;
	}

	public String reference_ID (Properties ctx, int WindowNo, GridTab mTab, 
			GridField mField, Object value, 
			Object oldValue)
	{
		if (null == value)
			return null;
		
		Integer UNS_POSTrx_ID = (Integer) value;
		String sql = "SELECT C_BPartner_ID, C_BPartner_Location_ID, M_PriceList_ID "
				+ " FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = DB.prepareStatement(sql, null);
			st.setInt(1, UNS_POSTrx_ID);
			rs = st.executeQuery();
			if (rs.next())
			{
				mTab.setValue("C_BPartner_ID", rs.getInt(1));
				mTab.setValue("C_BPartner_Location_ID", rs.getInt(2));
				mTab.setValue("M_PriceList_ID", rs.getInt(3));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return null;
	}
}
