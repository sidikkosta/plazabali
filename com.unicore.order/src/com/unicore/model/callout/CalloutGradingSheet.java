/**
 * 
 */
package com.unicore.model.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.adempiere.model.GridTabWrapper;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.Env;

import com.unicore.model.I_UNS_Grading_Sheet_All;
import com.unicore.model.I_UNS_WeighbridgeTicket;
import com.unicore.model.MUNSGradingSheetAll;
import com.uns.model.X_UNS_ArmadaType;
import com.uns.util.MessageBox;

/**
 * @author AzHaidar
 *
 */
public class CalloutGradingSheet extends CalloutEngine implements IColumnCallout 
{
	
	final static String PREFIX_PERCENT = "Percent";
	final static String PREFIX_ESTIMATION = "Est";

	/**
	 * 
	 *
	public CalloutGradingSheet() {
		super();
	}
	*/

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) 
	{
		String retVal = null;
		
		if (mTab.getTableName().equals(MUNSGradingSheetAll.Table_Name))
		{
			if (mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_IsQtyPercentage))
			{
				//retVal = setQtyPercentage(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_CompulsoryDeduction)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_AddDeduct)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_BunchFailure)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_DirtyBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_DripWet)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_Dura)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_EmptyBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_LongStalk)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_OldBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_RottenBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_Underdone)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_Unripe)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_SmallFruit)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_OverRipe)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Percent_Dirt)) {
				retVal = recalcUsingPercent(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if (mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_CompulsoryDeduction)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_AddDeductQty)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_BunchFailure)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_DirtyBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_DripWet)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_Dura)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_EmptyBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_LongStalk)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_OldBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_RottenBunch)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_Underdone)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_Unripe)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_OverRipe)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_SmallFruit)
					|| mField.getColumnName().equals(MUNSGradingSheetAll.COLUMNNAME_Est_Dirt)) {
				retVal = recalcUsingEstimation(ctx, WindowNo, mTab, mField, value, oldValue);
			}
		}
				
		return retVal;
	} //start
	
	
	public String setQtyPercentage(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (isCalloutActive())
			return null;
		
		if (value == null)
			return null;
		
		Boolean isQtyPercentage = (Boolean) value;
		
		if (isQtyPercentage)// && 
				//!gsa.getFFBReceiptCondition().equals(X_UNS_Grading_Sheet_All.FFBRECEIPTCONDITION_GraderOverwritten))
		{
			String msg = "You decide to calculate grading using Qty-Percentage. "
					+ "Recalculate percentage & estimation compulsory deduction?";
			String title = "Recalculate compulsory deduction?";
			
			int answer = MessageBox.showMsg(
					null, ctx, msg, title, MessageBox.YESNO, MessageBox.ICONQUESTION);
			if (answer == MessageBox.RETURN_NO) {
				
			}
		}
		
		return null;
	}
	
	public String recalcUsingPercent(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (isCalloutActive())
			return null;
		
		if (value == null)
			return null;
		
		I_UNS_Grading_Sheet_All gsa = GridTabWrapper.create(mTab, I_UNS_Grading_Sheet_All.class);
		
		if (!gsa.isQtyPercentage())
			return null;
		
		I_UNS_WeighbridgeTicket wbt = gsa.getUNS_WeighbridgeTicket();
		
		BigDecimal grossWeight = wbt.getGrossWeight();
		BigDecimal defaultTare = gsa.getDefaultTare();
		
		if (defaultTare.compareTo(Env.ZERO) <= 0) {
			defaultTare = new X_UNS_ArmadaType(ctx, wbt.getUNS_ArmadaType_ID(), null).getDefaultTare();
			gsa.setDefaultTare(defaultTare);
		}
		
		BigDecimal estNetto = grossWeight.subtract(defaultTare);
		BigDecimal percent = ((BigDecimal) value).divide(Env.ONEHUNDRED, 4, BigDecimal.ROUND_HALF_UP);
		
		BigDecimal estQty = estNetto.multiply(percent);
		
		String colName = mField.getColumnName().substring(PREFIX_PERCENT.length() + 1);
		String estColName = PREFIX_ESTIMATION + "_" + colName;
		
		if (mField.getColumnName().equals(I_UNS_Grading_Sheet_All.COLUMNNAME_Percent_AddDeduct))
			estColName += "Qty";
			
			
		mTab.setValue(estColName, estQty);
		
		return null;
	} //recalcUsingPercent
	
	
	public String recalcUsingEstimation(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (isCalloutActive())
			return null;
		
		if (value == null)
			return null;
		
		I_UNS_Grading_Sheet_All gsa = GridTabWrapper.create(mTab, I_UNS_Grading_Sheet_All.class);
		
		if (!gsa.isQtyPercentage())
			return null;
		
		I_UNS_WeighbridgeTicket wbt = gsa.getUNS_WeighbridgeTicket();
		
		BigDecimal grossWeight = wbt.getGrossWeight();
		BigDecimal defaultTare = gsa.getDefaultTare();
		
		if (defaultTare.compareTo(Env.ZERO) <= 0) {
			defaultTare = new X_UNS_ArmadaType(ctx, wbt.getUNS_ArmadaType_ID(), null).getDefaultTare();
			mTab.setValue(I_UNS_Grading_Sheet_All.COLUMNNAME_DefaultTare, defaultTare);
		}
		
		BigDecimal estNetto = grossWeight.subtract(defaultTare);
		BigDecimal estimation = (BigDecimal) value;
		
		BigDecimal percent = estimation.divide(estNetto, 4, BigDecimal.ROUND_HALF_UP).multiply(Env.ONEHUNDRED);

		String percentColName = "";
		String colName = mField.getColumnName().substring(PREFIX_ESTIMATION.length() + 1);
		
		if (mField.getColumnName().equals(I_UNS_Grading_Sheet_All.COLUMNNAME_Est_AddDeductQty)) {
			percentColName = I_UNS_Grading_Sheet_All.COLUMNNAME_Percent_AddDeduct;
		}
		else {
			//String colName = mField.getColumnName().substring(PREFIX_ESTIMATION.length() + 1);
			
			percentColName = PREFIX_PERCENT + "_" + colName;
		}
		
		mTab.setValue(percentColName, percent);
		mTab.setValue(colName, estimation);
		
		return null;
	} //recalcUsingEstimation
}
