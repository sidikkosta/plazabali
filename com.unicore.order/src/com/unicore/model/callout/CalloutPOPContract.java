/**
 * 
 */
package com.unicore.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.I_C_Bank;
import org.compiere.model.I_C_BankAccount;
import org.compiere.model.MBPBankAccount;
import org.compiere.model.MBankAccount;
import org.compiere.util.Util;

import com.unicore.model.I_UNS_Order_Contract;
import com.unicore.model.MUNSOrderContract;

/**
 * @author AzHaidar
 *
 */
public class CalloutPOPContract implements IColumnCallout {

	/**
	 * 
	 */
	public CalloutPOPContract() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.adempiere.base.IColumnCallout#start(java.util.Properties, int, org.compiere.model.GridTab, org.compiere.model.GridField, java.lang.Object, java.lang.Object)
	 */
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) 
	{
		String retVal = null;
		
		if (mTab.getTableName().equals(I_UNS_Order_Contract.Table_Name))
		{
			if (mField.getColumnName().equals(I_UNS_Order_Contract.COLUMNNAME_C_BankAccount_ID)) {
				retVal = changeBankAccount(ctx, WindowNo, mTab, mField, value, oldValue);
			}
			else if(mField.getColumnName().equals(I_UNS_Order_Contract.COLUMNNAME_C_BP_BankAccount_ID))
				retVal = accountContract(ctx, WindowNo, mTab, mField, value, oldValue);
		}
		
		return retVal;
	} //start
	
	/**
	 * 
	 */
	private String changeBankAccount(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue)
	{
		if (value == null)
			return null;
		
		Integer bankAcct_ID = (Integer) value;
		
		I_C_BankAccount ba = new MBankAccount(ctx, bankAcct_ID, null);
		I_C_Bank bank = ba.getC_Bank();
		
		mTab.setValue(I_UNS_Order_Contract.COLUMNNAME_C_Bank_ID, ba.getC_Bank_ID());
		mTab.setValue(I_UNS_Order_Contract.COLUMNNAME_BankAddress, ba.getC_Location().toString());
		mTab.setValue(I_UNS_Order_Contract.COLUMNNAME_SwiftCode, bank.getSwiftCode());
		mTab.setValue(I_UNS_Order_Contract.COLUMNNAME_Beneficiary, ba.getName());
		mTab.setValue(I_UNS_Order_Contract.COLUMNNAME_AccountNo, ba.getAccountNo());
		mTab.setValue(I_UNS_Order_Contract.COLUMNNAME_C_Currency_ID, ba.getC_Currency_ID());
		
		return null;
	} //changeBankAccount
	
	private String accountContract(Properties ctx, int windowNo, GridTab mTab, GridField mField, Object value, Object oldValue)
	{
		if(value == null)
			return "";
		
		MBPBankAccount account = new MBPBankAccount(ctx, (Integer) value, null);
		
		mTab.setValue(MUNSOrderContract.COLUMNNAME_C_Bank_ID, account.getC_Bank_ID());
		mTab.setValue(MUNSOrderContract.COLUMNNAME_Beneficiary, account.getA_Name());
		mTab.setValue(MUNSOrderContract.COLUMNNAME_BankAddress, Util.isEmpty(account.getA_Street(), true)
																			? "-" : account.getA_Street());
		mTab.setValue(MUNSOrderContract.COLUMNNAME_AccountNo, account.getAccountNo());
		
		return null;
	}
}
