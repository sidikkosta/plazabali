/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;

/**
 * @author AzHaidar
 *
 */
public class MUNSGradingSheet extends X_UNS_Grading_Sheet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1996740529795163721L;

	/**
	 * @param ctx
	 * @param UNS_Grading_Sheet_ID
	 * @param trxName
	 */
	public MUNSGradingSheet(Properties ctx, int UNS_Grading_Sheet_ID,
			String trxName) {
		super(ctx, UNS_Grading_Sheet_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSGradingSheet(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
    public boolean beforeSave(boolean newRecord) 
    {
		if (getDeductionAmt().signum() == 0) {
			throw new AdempiereUserError("The deduction amount cannot be left to zero value.");
		}
		
		if (getDeductionAmt().signum() < 0) {
			setDeductionAmt(getDeductionAmt().abs());
		}
			
		return true;
    }
	
	@Override
	public boolean afterSave(boolean newRecord, boolean success)
	{
		String errMsg = updateHeader();
		if (errMsg != null)
		{
			log.saveError("CannotUpdateHeader", errMsg);
			return false;
		}
		
		return true;
	}
	
	@Override
	protected boolean beforeDelete()
	{
		MUNSGrading gradingComp = new MUNSGrading(getCtx(), getUNS_Grading_ID(), get_TrxName());
		if (gradingComp.getValue().equals("COMP"))
		{
			log.saveError("CompulsoryCannotBeRemoved.", "Compulsory deduction component cannot be removed.");
			return false;
		}
			
		return true;
	}

	@Override
	protected boolean afterDelete(boolean success)
	{
		if (success)
		{
			String errMsg = updateHeader();
			if (errMsg != null)
			{
				log.saveError("CannotUpdateHeader", errMsg);
				return false;
			}
		}
		
		return success;
	} // afterDelete
	
	/**
	 * 
	 * @return
	 */
	public String updateHeader()
	{
		String sql = "SELECT SUM(DeductionAmt) FROM UNS_Grading_Sheet "
				+ " WHERE UNS_WeighbridgeTicket_ID=" + getUNS_WeighbridgeTicket_ID();
		
		BigDecimal totalReflection = DB.getSQLValueBDEx(get_TrxName(), sql);

		I_UNS_WeighbridgeTicket wt = getUNS_WeighbridgeTicket();
		
		BigDecimal netto2 = wt.getNettoI().subtract(totalReflection);

		sql = "UPDATE UNS_WeighbridgeTicket wt SET Reflection=?, NettoII=? "
				+ " WHERE UNS_WeighbridgeTicket_ID=" + getUNS_WeighbridgeTicket_ID();
		
		int count = DB.executeUpdateEx(sql, new Object[]{totalReflection, netto2}, get_TrxName());
		
		if (count == 1)
			return null;
		else
			return "Cannot update Reflection amount.";
	}
	
} //MUNSGradingSheet
