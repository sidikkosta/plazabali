package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTransaction;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.X_C_DocType;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.model.MUNSBankEDCBatchRecap;
import com.uns.model.MUNSDailyBankCardTypeRecap;
import com.uns.util.ErrorMsg;
import com.uns.util.UNSApps;

public class MUNSPOSPayment extends X_UNS_POSPayment implements DocAction,
		DocOptions {

	/**
	 * 
	 */
	private static final long 			serialVersionUID = -8404649044099537797L;
	private MUNSPOSTrx					m_transaction 		= null;
	private boolean						m_justPrepared		= false;
	private String						m_processMsg		= null;
	private CLogger						m_logger			= CLogger.getCLogger(this.getClass());
	private int							m_bankStmtLine_ID	= 0;
	private String						m_docBaseType		= null;

	public MUNSPOSPayment(Properties ctx, int UNS_POSPayment_ID, String trxName) 
	{
		super(ctx, UNS_POSPayment_ID, trxName);
	}

	public MUNSPOSPayment(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public MUNSPOSPayment (MUNSPOSTrx transaction)
	{
		this (transaction.getCtx(), 0, transaction.get_TrxName());
		this.m_transaction = transaction;
		setUNS_POSTrx_ID(transaction.get_ID());
		setC_BPartner_ID(this.m_transaction.getC_BPartner_ID());
		setC_BPartner_Location_ID(transaction.getC_BPartner_Location_ID());
		setC_Currency_ID(transaction.getC_Currency_ID());
		setSalesRep_ID(transaction.getSalesRep_ID());		
		int trxDocType_ID = transaction.getC_DocType_ID();
		String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?";
		String docBaseType = DB.getSQLValueString(transaction.get_TrxName(), sql, trxDocType_ID);
		int C_DocTYpe_ID = -1;
		if (X_C_DocType.DOCBASETYPE_POSSales.equals(docBaseType))
		{
			C_DocTYpe_ID = MDocType.getDocType(X_C_DocType.DOCBASETYPE_ARPOSReceipt);
		}
		else if (X_C_DocType.DOCBASETYPE_POSReturn.equals(docBaseType))
		{
			C_DocTYpe_ID = MDocType.getDocType(X_C_DocType.DOCBASETYPE_ARPOSCreditMemo);
		}
		setC_DocType_ID (C_DocTYpe_ID);
		setDateAcct (transaction.getDateAcct());
		setDateDoc (transaction.getDateDoc());
		setDateTrx (transaction.getDateTrx());
		setDocAction (X_UNS_POSPayment.DOCACTION_Complete);
		setDocStatus (X_UNS_POSPayment.DOCSTATUS_Drafted);
		setIsSOTrx (true);
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (DOCSTATUS_Completed.equals(getDocStatus()))
		{
			options[index++] = DOCACTION_Reverse_Correct;
			options[index++] = DOCACTION_Reverse_Accrual;
			options[index++] = DOCACTION_Void;
		}
		return index;
	}

	@Override
	public boolean processIt(String action) throws Exception {
		addLog(Level.INFO, "Process It");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		addLog(Level.INFO, "Unlock it");
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		addLog(Level.INFO, "Invalidate it");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		addLog(Level.INFO, "Prepare It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		if (!DOCSTATUS_Completed.equals(getPOSTransaction().getDocStatus())
				&& !DOCSTATUS_Reversed.equals(getPOSTransaction().getDocStatus()))
		{
			String desc = getDescription();
			if(desc == null)
				desc = "";
			if(desc.contains("**") || desc.contains("Import"))
			{
				try {
					if(!getPOSTransaction().processIt(MUNSPOSTrx.ACTION_Complete))
					{
						m_processMsg = "Cannot complete POS Trx from Import Skema by POS Payment";
						return DOCSTATUS_Invalid;
					}
					
					getPOSTransaction().saveEx();
				} catch (Exception e) {
					e.printStackTrace();
					m_processMsg = e.getMessage();
					return DOCSTATUS_Invalid;
				}

			}
			else
			{
				m_processMsg = "Unable to process POS Transaction document. Please complete POS Transaction first.";
				return DOCSTATUS_Invalid;
			}
		}
		
		String sql = "SELECT GrandTotal FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
		BigDecimal balance = DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSTrx_ID());
		balance = balance.subtract(getPaidAmt()).add(getRoundingAmt());
		if (balance.signum() != 0 && !isReplication())
		{
			m_processMsg = "Not balanced POS Amount <> Paid Amount";
			return DocAction.STATUS_Invalid;
		}
		MUNSPaymentTrx[] trxs = getLines(false);
		boolean isSameSession = true;
		if (isCreditMemo())
		{
			sql = "SELECT UNS_POS_Session_ID FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
			int oriSessionID = DB.getSQLValue(get_TrxName(), sql, getPOSTransaction().getReference_ID());
			isSameSession = getPOSTransaction().getUNS_POS_Session_ID() == oriSessionID;
		}
		for (int i=0; i<trxs.length; i++)
		{
			if(isReplication())
				continue;
			String desc = getDescription();
			if (desc == null)
				desc = "";
			if (desc.contains("**") || desc.contains("Import"))
				continue;
			if (!MUNSPaymentTrx.PAYMENTMETHOD_Cash.equals(trxs[i].getPaymentMethod()))
				continue;
			if (trxs[i].getTrxAmt().signum() == 1)
				continue;
			if (!isSameSession)
				continue;
			BigDecimal cashBalance = MUNSSessionCashAccount.getBalance(
					get_TrxName(), trxs[i].getUNS_POS_Session_ID(), trxs[i].getC_Currency_ID());
			if (cashBalance.signum() == -1)
				cashBalance = Env.ZERO;
			if (trxs[i].getTrxAmt().abs().compareTo(cashBalance) == 1)
			{
				m_processMsg = "Insufficient cash";
				return DOCSTATUS_Invalid;
			}
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		this.m_justPrepared = true;
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		addLog(Level.INFO, "Approve It");
		setProcessed(true);
		setIsApproved (true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		addLog(Level.INFO, "Reject It");
		setProcessed(false);
		setIsApproved (false);
		return true;
	}

	@Override
	public String completeIt() 
	{
		addLog(Level.INFO, "Complete It");
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DOCSTATUS_InProgress.equals(status))
			{
				return DOCSTATUS_Invalid;
			}
		}
		
		if (DOCACTION_Prepare.equals(getDocAction()))
		{
			return DOCSTATUS_InProgress;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		if (!processLines())
		{
			m_processMsg = CLogger.retrieveErrorString("Error when trying process lines.");
			return DocAction.STATUS_Invalid;
		}
		
		if (getRoundingAmt().signum() != 0 && !isReplication())
		{
			try
			{
				MUNSPOSSession session = new MUNSPOSSession(
						getCtx(), getPOSTransaction().getUNS_POS_Session_ID(), get_TrxName());
				DB.getDatabase().forUpdate(session, 0);
				BigDecimal roundAmt = getRoundingAmt();
				roundAmt = roundAmt.add(session.getRoundingAmt());
				session.setRoundingAmt (roundAmt);
				session.save();	
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return DOCSTATUS_Invalid;
			}
		}
		
		String sql = "UPDATE UNS_POSTrx SET IsPaid = 'Y' WHERE UNS_POSTrx_ID = ?";
		int success = DB.executeUpdate(sql, getUNS_POSTrx_ID(), false, get_TrxName());
		if (success == -1)
		{
			m_processMsg = CLogger.retrieveErrorString("Could not update POS Transaction");
			return DOCSTATUS_Invalid;
		}

		if (!isCreditMemo())
		{
			sql = "UPDATE UNS_POSFNBTableLine SET IsReserved = ? WHERE UNS_POSFNBTableLine_ID = ?";
			int updateOK = DB.executeUpdate(sql, new Object[]{"N", getPOSTransaction().getUNS_POSFNBTableLine_ID()}, false, get_TrxName());
			if (updateOK == -1)
				return DOCSTATUS_Invalid;
		}
		
		
		if (!isReplication() && !MUNSPOSSession.updateByPOSTrx(getPOSTransaction()))
		{
			m_processMsg = CLogger.retrieveErrorString("Could not update SESSION");
			return DOCSTATUS_Invalid;
		}
		
		if (!isApproved() && !approveIt())
		{
			m_processMsg = "Could not approve document.";
			return DOCSTATUS_Invalid;
		}
		
		if (UNSApps.SERVER_MAIN.equals(UNSApps.SERVER_TYPE) && !getPOSTransaction().getSession().isTrialMode())
		{
			if (!getPOSTransaction().isReturn())
				getPOSTransaction().generateBOM();
			MUNSPOSTrxLine[] plts = getPOSTransaction().getLines(true);
			for (int i=0; i<plts.length; i++)
			{
				MUNSPOSTrxLine line = plts[i];
				MProduct product = MProduct.get(getCtx(), line.getM_Product_ID());
				if (!product.isStocked() || !product.isItem())
					continue;
				if (!line.isBOM() && MProductBOM.getBOMLines(
						getCtx(), line.getM_Product_ID(), get_TrxName()).length > 0)
					continue;
				BigDecimal movementQty = line.getQtyOrdered();
				BigDecimal manual = MUNSPOSTrxLineMA.getManualQty(line.get_ID(), get_TrxName());
				if (manual.abs().compareTo(line.getQtyOrdered().abs()) > 0)
				{
					m_processMsg = "Over Qty MA";
					return DOCSTATUS_Invalid;
				}
				if (!getPOSTransaction().checkMaterialPolicy(line, movementQty.subtract(manual)))
					return DOCSTATUS_Invalid;

				MTransaction trx = null;
				MUNSPOSTrxLineMA[] mas = MUNSPOSTrxLineMA.gets(line.get_ID(), get_TrxName());
				for (int j=0; j<mas.length; j++)
				{
					MUNSPOSTrxLineMA ma = mas[j];
					BigDecimal qty = ma.getMovementQty();
					if (!MStorageOnHand.add(getCtx(), getPOSTransaction().getM_Warehouse_ID(), 
							ma.getM_Locator_ID(), line.getM_Product_ID(), 
							ma.getM_AttributeSetInstance_ID(), 
							qty.negate(), ma.getDateMaterialPolicy(), 
							get_TrxName()))
					{
						m_processMsg = CLogger.retrieveErrorString("could not update storage");
						m_processMsg += " | " + product.getName(); 
						return DOCSTATUS_Invalid;
					}
					
					trx = new MTransaction(getCtx(), getAD_Org_ID(), 
							MTransaction.MOVEMENTTYPE_POSOrder, 
							ma.getM_Locator_ID(), line.getM_Product_ID(), 
							ma.getM_AttributeSetInstance_ID(), 
							qty.negate(), getPOSTransaction().getDateAcct(), get_TrxName());
					trx.setUNS_POSTrxLine_ID(line.get_ID());
					if (!trx.save())
					{
						m_processMsg = CLogger.retrieveErrorString("Could not save transaction.");
						m_processMsg += " | " + product.getName(); 
						return DOCSTATUS_Invalid;
					}
				}
			}
			
			MUNSPaymentTrx[] trxs = getLines(false);
			for(int x=0; x<trxs.length; x++)
			{
				if(!trxs[x].getPaymentMethod().equals(MUNSPaymentTrx.PAYMENTMETHOD_Card))
					continue;
				
				BigDecimal amt = trxs[x].getConvertedAmt();
				String sDate = getDateTrx().toString().substring(0, 10);
				Timestamp dateTrx = Timestamp.valueOf(sDate+" 00:00:00");
				
				if(trxs[x].getTrxType().equals(MUNSPaymentTrx.TRXTYPE_RefundEDC))
				{
					MUNSPOSPayment oriPayment = MUNSPOSPayment.getRefPaymentRefund(
							getCtx(), getUNS_POSPayment_ID(), get_TrxName());
					
					if(oriPayment == null)
					{
						m_processMsg = "Cannot found Original Payment for this payment refund";
						return DOCSTATUS_Invalid;
					}
					
					String sqll = "SELECT UNS_POS_Session_ID FROM UNS_POSTrx WHERE"
							+ " UNS_POSTrx_ID = ?";
					int SessionID = DB.getSQLValue(get_TrxName(), sqll, oriPayment.getUNS_POSTrx_ID());
					
					if(SessionID == trxs[x].getUNS_POS_Session_ID())
					{
						String sOriDate = oriPayment.getDateTrx().toString().substring(0, 10);
						dateTrx = Timestamp.valueOf(sOriDate+" 00:00:00");
					}
				}
				
				MUNSCardTrxDetail cardDetail = MUNSCardTrxDetail.get(trxs[x]);
				
				MUNSDailyBankCardTypeRecap daily = MUNSDailyBankCardTypeRecap.getCreate(
						getCtx(), getAD_Org_ID(), cardDetail.getUNS_EDC_ID(), cardDetail.getUNS_CardType_ID()
						, dateTrx, get_TrxName());
				
				daily.setTotalARAmount((daily.getTotalARAmount().add(amt)).setScale(2, RoundingMode.HALF_DOWN));
				if(!daily.save())
				{
					m_processMsg = "Error when trying create EDC Recapitulation.";
					return DOCSTATUS_Invalid;
				}
				
				MUNSBankEDCBatchRecap batch = MUNSBankEDCBatchRecap.getCreate(
						getCtx(), getAD_Org_ID(), cardDetail.getUNS_EDC_ID(), dateTrx, cardDetail.getBatchNo(), get_TrxName());
				
				batch.setTotalARAmount((batch.getTotalARAmount().add(amt)).setScale(2, RoundingMode.HALF_DOWN));
				if(!batch.save())
				{
					m_processMsg = "Error when trying create EDC Recapitulation.";
					return DOCSTATUS_Invalid;
				}
			}
		}
		
		if(UNSApps.SERVER_MAIN.equals(UNSApps.SERVER_TYPE))
		{
			MUNSPOSSession session = getPOSTransaction().getSession();
			if(!session.isTrialMode())
			{
				m_processMsg = getPOSTransaction().createReconciliation(
						getPOSTransaction().isReturn(), false);
				if(null != m_processMsg)
					return DOCSTATUS_Invalid;
			}
			
			if (isCreditMemo())
			{
				String __sql = "SELECT UNS_POS_Session_ID FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
				int oriSesID = DB.getSQLValue(get_TrxName(), __sql, getPOSTransaction().getReference_ID());
				int currentSes = getPOSTransaction().getUNS_POS_Session_ID();
				boolean isSameSession = oriSesID == currentSes;
				String _sql = "UPDATE UNS_CardTrxDetail SET BatchNo = ? WHERE UNS_PaymentTrx_ID IN ("
						+ " SELECT UNS_PaymentTrx_ID FROM UNS_PaymentTrx WHERE UNS_POSPayment_ID IN ("
						+ " SELECT UNS_POSPayment_ID FROM UNS_POSPayment WHERE UNS_POSTrx_ID IN (SELECT "
						+ " UNS_POSTrx_ID FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?)))";
				
				String batchNo = "0";
				if (isSameSession)
					batchNo = "VO";
				
				int recordUpdated = DB.executeUpdate(_sql, new Object[]{batchNo,getPOSTransaction().getReference_ID()}, false, get_TrxName());
				log.info(recordUpdated + " updated ");
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
//		boolean isRefund = getPOSAmount().signum() < 0 ? true : false;
//		String serverType = MSysConfig.getValue(MSysConfig.SERVER_TYPE, "");
//		String locationServer = MSysConfig.getValue(MSysConfig.LOCATION_SERVER, "");
//		if(!Util.isEmpty(locationServer, true) && serverType.equals(locationServer))
//		{
//			m_processMsg = createReconciliation(isRefund);
//			if(null != m_processMsg)
//				return DOCSTATUS_Invalid;
//		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		addLog(Level.INFO, "Void it");
		m_processMsg = "Invalid action Void It";
		return false;
	}

	@Override
	public boolean closeIt() {
		addLog(Level.INFO, "Close It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		m_processMsg = "Invalid action Reverse Correct";		
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		m_processMsg = "Invalid action Reverse Accrual";		
		return false;
	}

	@Override
	public boolean reActivateIt() {
		addLog(Level.WARNING, "Trying to reactivate document.");
		m_processMsg = "Not implemented action Reactivate- it";
		return false;
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File createPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() 
	{
		return getPOSTransaction().getC_Currency_ID();
	}

	@Override
	public BigDecimal getApprovalAmt() 
	{
		return getPaidAmt();
	}

	public MUNSPOSTrx getPOSTransaction ()
	{
		if (null == m_transaction)
		{
			m_transaction = new MUNSPOSTrx(getCtx(), getUNS_POSTrx_ID(), get_TrxName());
		}
		
		return m_transaction;
	}
	
	public MUNSPaymentTrx[] getTrx()
	{
		List<MUNSPaymentTrx> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSPaymentTrx.Table_Name, COLUMNNAME_UNS_POSPayment_ID + "=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSPaymentTrx[list.size()]);
	}
	
	private void addLog (Level level, String msg)
	{
		if (!m_logger.isLoggable(level))
		{
			return;
		}
		
		m_logger.log(level, msg);
	}
	
	public void addDescription (String description)
	{
		StringBuilder desc = new StringBuilder();
		if (Util.isEmpty(getDescription(), true))
		{
			desc.append(getDescription());
			desc.append(" | ");
		}
		
		desc.append(description);
	}
	
	private void setRoundingAmt()
	{
		MUNSPOSConfiguration config = MUNSPOSConfiguration.getActive(get_TrxName(), getAD_Org_ID());
		BigDecimal remaining = getPOSAmount().subtract(getPaidAmt());
		if (isCreditMemo())
			remaining = remaining.negate();
		BigDecimal roundAmt = config.getRoundingAmount(remaining);
		if (isCreditMemo())
			roundAmt = roundAmt.negate();
		setRoundingAmt(roundAmt);
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if ((isCreditMemo() && !getPOSTransaction().isReturn())
				|| (!isCreditMemo() && getPOSTransaction().isReturn()))
		{
			log.saveError("SaveError", "Invalid record.");
			return false;
		}
		String desc = getDescription();
		if (desc == null)
			desc = "";
		if (desc.contains("**") || desc.contains("Import"))
		{
			if(getC_BPartner_Location_ID() <= 0)
			{
				String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE"
						+ " C_BPartner_ID = ? AND IsActive = 'Y'";
				int partnerLocID = DB.getSQLValue(get_TrxName(), sql, getC_BPartner_ID());
				setC_BPartner_Location_ID(partnerLocID);
			}
			
			BigDecimal round = getPOSAmount().subtract(getPaidAmt());
			round = round.negate();
			setRoundingAmt(round);
			
			if(!desc.contains("RESEND"))
				setDescription(desc.concat("-RESEND"));
		}
		else
			setRoundingAmt();
		return super.beforeSave(newRecord);
	}
	
	public int getC_BankStatementLine_ID ()
	{
		if (m_bankStmtLine_ID == 0)
		{
			String sql = "SELECT C_BankStatementLine_ID FROM C_BankStatementLine WHERE "
					+ " UNS_POSPayment_ID = ?";
			m_bankStmtLine_ID = DB.getSQLValue(get_TrxName(), sql, get_ID());
		}
		
		return m_bankStmtLine_ID;
	}
	
	@Override 
	protected boolean beforeDelete ()
	{
		if (isProcessed())
		{
			ErrorMsg.setErrorMsg(getCtx(), "RecordProcessed", "Record Processed!");
			return false;
		}
		MUNSPaymentTrx[] trx = getLines(true);
		for (int i=0; i<trx.length; i++)
			if (!trx[i].delete(true))
				return false;
		
		return super.beforeDelete();
	}
	
	public static MUNSPOSPayment getCreate (MUNSPOSTrx trx)
	{
		MUNSPOSPayment pay = get(trx.getCtx(), trx.get_ID(), trx.get_TrxName());
		if (pay == null)
			pay = new MUNSPOSPayment(trx);
		return pay;
	}
	
	public static MUNSPOSPayment get (Properties ctx, int posTrx_ID, String trxName)
	{
		return Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				"UNS_POSTrx_ID = ? AND DocStatus NOT IN (?,?)", trxName).
				setParameters(posTrx_ID, "RE", "VO").setOrderBy("DocumentNo").first();
	}
	
	public boolean isCreditMemo ()
	{
		if (null == m_docBaseType)
		{
			String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?";
			m_docBaseType = DB.getSQLValueString(get_TrxName(), sql, getC_DocType_ID());
		}
		
		return MDocType.DOCBASETYPE_ARPOSCreditMemo.equals(m_docBaseType);
	}
	
//	private String createReconciliation(boolean isRefund)
//	{
//		MUNSPOSConfiguration config = MUNSPOSConfiguration.getActive(get_TrxName(), getAD_Org_ID());
//		MUNSPaymentTrx[] trxs = getTrx();
//		String sql = "SELECT Store_ID FROM UNS_POSTerminal WHERE UNS_POSTerminal_ID ="
//				+ " (SELECT UNS_POSTerminal_ID FROM UNS_POS_Session WHERE UNS_POS_Session_ID = ?)";
//		for(int i=0;i<trxs.length;i++)
//		{
//			int store = DB.getSQLValue(get_TrxName(), sql, trxs[i].getUNS_POS_Session_ID());
//			if(trxs[i].isTrxCash())
//			{
//				MUNSCashReconciliation cash = MUNSCashReconciliation.getCreate(
//						getCtx(), store, getDateTrx(), getAD_Org_ID(), trxs[i].getC_Currency_ID(), get_TrxName());
//				cash.setCashDepositAmt(cash.getCashDepositAmt().add(trxs[i].getTrxAmt()));
//				cash.setCashDepositAmtB1(cash.getCashDepositAmtB1().add(MConversionRate.convert(getCtx(), trxs[i].getTrxAmt(),
//						trxs[i].getC_Currency_ID(), config.getBase1Currency_ID(),
//						getDateTrx(), 0, getAD_Client_ID(), getAD_Org_ID())));
//				cash.setCashDepositAmtB2(cash.getCashDepositAmtB2().add(MConversionRate.convert(getCtx(), trxs[i].getTrxAmt(),
//						trxs[i].getC_Currency_ID(), config.getBase2Currency_ID(),
//						getDateTrx(), 0, getAD_Client_ID(), getAD_Org_ID())));
//				cash.saveEx();
//				DB.executeUpdate("UPDATE UNS_PaymentTrx SET UNS_CashReconciliation_ID = ?"
//						+ " WHERE UNS_PaymentTrx_ID = ?", new Object[]{cash.get_ID(), trxs[i].get_ID()}, false, get_TrxName());
//			}
//			else if(trxs[i].isTrxEDC())
//			{
//				MUNSCardTrxDetail detail = MUNSCardTrxDetail.get(trxs[i]);
//				MUNSEDCReconciliation edc = MUNSEDCReconciliation.getCreate(getCtx(),
//						store, getDateTrx(), detail.getUNS_CardType_ID(), getAD_Org_ID(), get_TrxName());
//				edc.setTotalAmt(trxs[i].getAmount());
//				edc.setPayableRefundAmt(trxs[i].getAmount());
//				edc.saveEx();
//			}
//		}
//		
//		return null;
//	}
	
	private MUNSPaymentTrx[] m_lines = null;
	public MUNSPaymentTrx[] getLines (boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSPaymentTrx> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPaymentTrx.Table_Name, 
				MUNSPOSPayment.Table_Name + "_ID = ?", get_TrxName()).setParameters(get_ID()).
				list();
		m_lines = new MUNSPaymentTrx[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}
	
	public boolean processLines ()
	{
		if(isReplication())
			return true;
		MUNSPaymentTrx[] lines = getLines(false);
		boolean isSameSession = true;
		if (isCreditMemo())
		{
			String sql = "SELECT UNS_POS_Session_ID FROM UNS_POSTrx WHERE UNS_POSTrx_ID = ?";
			int oriSessionID = DB.getSQLValue(get_TrxName(), sql, getPOSTransaction().getReference_ID());
			isSameSession = getPOSTransaction().getUNS_POS_Session_ID() == oriSessionID;
		}
		for (int i=0; i<lines.length; i++)
		{
			lines[i].setProcessed(true);
			if(!lines[i].save())
				return false;
			if (MUNSPaymentTrx.PAYMENTMETHOD_Cash.equals(lines[i].getPaymentMethod()))
			{
				if (!MUNSSessionCashAccount.update(
						get_TrxName(), getPOSTransaction().getUNS_POS_Session_ID(), 
						lines[i].getC_Currency_ID(), lines[i].getTrxType(), 
						lines[i].getTrxAmt(), true))
					return false;
			}
			else if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(lines[i].getPaymentMethod()))
			{
				MUNSCardTrxDetail detail = MUNSCardTrxDetail.get(lines[i]);
				if (!MUNSSessionEDCSummary.updadte(get_TrxName(), getPOSTransaction().getUNS_POS_Session_ID(), 
						detail.getUNS_EDC_ID(), detail.getUNS_CardType_ID(), lines[i].getTrxAmt(), isSameSession))
					return false;
			}
			else if (MUNSPaymentTrx.PAYMENTMETHOD_Voucher.equals(lines[i].getPaymentMethod()))
			{
				MUNSPOSSession session = new MUNSPOSSession(
						getCtx(), getPOSTransaction().getUNS_POS_Session_ID(), 
						get_TrxName());
				DB.getDatabase().forUpdate(session, 0);
				BigDecimal voucherAmt = session.getVoucherAmt();
				voucherAmt = voucherAmt.add(lines[i].getConvertedAmt());
				session.setVoucherAmt(voucherAmt);
				if (!session.save())
					return false;
//				MUNSVoucherCode vCode = MUNSVoucherCode.getCode(getCtx(), lines[i].getTrxNo(),
//						lines[i].getC_Currency_ID(), get_TrxName());
//				vCode.setUsedAmt(vCode.getUsedAmt().add(lines[i].getTrxAmt()));
//				if(!vCode.save())
//					return false;
			}
			else if (MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(lines[i].getPaymentMethod()))
			{
				MUNSCardTrxDetail detail = MUNSCardTrxDetail.get(lines[i]);
//				if(detail != null)
//				{
					if (!MUNSSessionEDCSummary.updadte(get_TrxName(), getPOSTransaction().getUNS_POS_Session_ID(), 
							detail.getUNS_EDC_ID(), detail.getUNS_CardType_ID(), lines[i].getTrxAmt(), isSameSession))
						return false;
//				}
			}
		}
		
		return true;
	}
	
	/**
	 * to get reference Payment Refund
	 * @param ctx
	 * @param UNS_POSPayment_ID
	 * @param trx
	 * @return
	 */
	public static MUNSPOSPayment getRefPaymentRefund(Properties ctx, int UNS_POSPayment_ID, String trxName)
	{
		MUNSPOSPayment reference = null;
		
		String sql = "SELECT pp.UNS_POSPayment_ID FROM UNS_POSPayment pp "
				+ " INNER JOIN UNS_POSTrx pt ON pt.Reference_ID = pp.UNS_POSTrx_ID"
				+ " INNER JOIN UNS_POSPayment ppr ON ppr.UNS_POSTrx_ID = pt.UNS_POSTrx_ID"
				+ " WHERE ppr.UNS_POSPayment_ID = ?";
		int ID = DB.getSQLValue(trxName, sql, UNS_POSPayment_ID);
		
		if(ID <=0)
			return null;
		
		reference = new MUNSPOSPayment(ctx, ID, trxName);
		
		return reference;
	}
}
