/**
 * 
 */
package com.unicore.model;

import java.util.Properties;

import org.compiere.util.DB;

/**
 * @author ALBURHANY
 *
 */
public class MUNSPrintingLog extends X_UNS_PrintingLog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8437106205625023016L;

	public MUNSPrintingLog(Properties ctx, int UNS_PrintingLog_ID,
			String trxName) {
		super(ctx, UNS_PrintingLog_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	 
	/**
	 * 
	 * @param ctx
	 * @param Record_ID
	 * @param AD_User_ID
	 * @param Value
	 * @param trxName
	 * @param DocumentNo
	 * @return
	 */
	public String create(Properties ctx, int Record_ID, int AD_User_ID, String Value, String DocumentNo, String trxName)
	{
		setRecord_ID(Record_ID);
		setDocumentNo(DocumentNo);
		setPrintedBy(AD_User_ID);
		setDatePrinted(DB.getSQLValueTS(trxName, "SELECT now()"));
		setValue(Value);
		
		String sql = "SELECT MAX(CopyOfPrint) FROM UNS_PrintingLog WHERE Record_ID = " + Record_ID
						+ " AND Value = '" + Value + "' AND DocumentNo = '" + DocumentNo + "'";
		int print = DB.getSQLValue(trxName, sql);
		
		if(print == -1)
			setCopyOfPrint(1);
		else
			setCopyOfPrint(print + 1);
		saveEx();

		return null;
	}
	
	public static MUNSPrintingLog getMaxCopy(Properties ctx, int Record_ID, String Value, String trxName)
	{
//		String whereClause = "Record_ID = ? AND Value = ?";
//		
//		MUNSPrintingLog printingLog = new Query(ctx, Table_Name, whereClause, trxName)
//										.setParameters(Record_ID, Value).setOrderBy(COLUMNNAME_CopyOfPrint + " DESC")
//											.first();
		MUNSPrintingLog printLog;
		
		String sql = "SELECT UNS_PrintingLog_ID FROM UNS_PrintingLog WHERE Record_ID = " + Record_ID
						+ " AND Value = '" + Value + "' Order By CopyOfPrint DESC";
		int idPrintLog = DB.getSQLValue(trxName, sql);
		if(idPrintLog <= 0)
			printLog = new MUNSPrintingLog(ctx, 0, trxName);
		else
			printLog = new MUNSPrintingLog(ctx, idPrintLog, trxName);
		
		return printLog;
	}

}
