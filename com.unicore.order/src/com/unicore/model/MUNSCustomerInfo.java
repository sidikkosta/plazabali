/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.DB;

/**
 * @author nurse
 *
 */
public class MUNSCustomerInfo extends X_UNS_CustomerInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -960867295635413047L;
	public static final int REFERENCE_CUSTOMER_TYPE = 1000281;
	public static final int REFERENCE_VIP_TYPE = 1000282;

	/**
	 * @param ctx
	 * @param UNS_CustomerInfo_ID
	 * @param trxName
	 */
	public MUNSCustomerInfo(Properties ctx, int UNS_CustomerInfo_ID,
			String trxName) 
	{
		super(ctx, UNS_CustomerInfo_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCustomerInfo(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if  (newRecord || is_ValueChanged(COLUMNNAME_FlightNo))
		{
			String flightNo = getFlightNo();
			if (flightNo == null)
				flightNo = "";
			if (flightNo.length() > 2)
				flightNo = flightNo.substring(0,2);
			String sql = "SELECT COUNT(*) FROM UNS_AirLine WHERE Value = ?";
			int exists = DB.getSQLValue(get_TrxName(), sql, flightNo);
			String desc = getDescription();
			if (desc == null)
				desc = "";
			if (exists <= 0 && !desc.contains("**") && !desc.contains("Import "))
			{
				sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(CONCAT(Value,'(',Name,')')), ', ') FROM UNS_AirLine WHERE IsActive = 'Y'";
				String availAirLine = DB.getSQLValueString(get_TrxName(), sql);
				log.saveError("Flight Not registered","Flight not registered!\nAvailable Flight " + availAirLine);
				return false;
			}
		}
		
		return super.beforeSave(newRecord);
	}
}
