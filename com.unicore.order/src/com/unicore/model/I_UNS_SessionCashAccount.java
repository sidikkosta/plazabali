/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_SessionCashAccount
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_SessionCashAccount 
{

    /** TableName=UNS_SessionCashAccount */
    public static final String Table_Name = "UNS_SessionCashAccount";

    /** AD_Table_ID=1000393 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BeginningBalance */
    public static final String COLUMNNAME_BeginningBalance = "BeginningBalance";

	/** Set Beginning Balance.
	  * Balance prior to any transactions
	  */
	public void setBeginningBalance (BigDecimal BeginningBalance);

	/** Get Beginning Balance.
	  * Balance prior to any transactions
	  */
	public BigDecimal getBeginningBalance();

    /** Column name BeginningBalanceBase1 */
    public static final String COLUMNNAME_BeginningBalanceBase1 = "BeginningBalanceBase1";

	/** Set Begining Balance Base 1	  */
	public void setBeginningBalanceBase1 (BigDecimal BeginningBalanceBase1);

	/** Get Begining Balance Base 1	  */
	public BigDecimal getBeginningBalanceBase1();

    /** Column name BeginningBalanceBase2 */
    public static final String COLUMNNAME_BeginningBalanceBase2 = "BeginningBalanceBase2";

	/** Set Begining Balance Base 2	  */
	public void setBeginningBalanceBase2 (BigDecimal BeginningBalanceBase2);

	/** Get Begining Balance Base 2	  */
	public BigDecimal getBeginningBalanceBase2();

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name CashDepositAmt */
    public static final String COLUMNNAME_CashDepositAmt = "CashDepositAmt";

	/** Set Cash Deposit Amount	  */
	public void setCashDepositAmt (BigDecimal CashDepositAmt);

	/** Get Cash Deposit Amount	  */
	public BigDecimal getCashDepositAmt();

    /** Column name CashDepositAmtB1 */
    public static final String COLUMNNAME_CashDepositAmtB1 = "CashDepositAmtB1";

	/** Set Cash Deposit Amount Base 1	  */
	public void setCashDepositAmtB1 (BigDecimal CashDepositAmtB1);

	/** Get Cash Deposit Amount Base 1	  */
	public BigDecimal getCashDepositAmtB1();

    /** Column name CashDepositAmtB2 */
    public static final String COLUMNNAME_CashDepositAmtB2 = "CashDepositAmtB2";

	/** Set Cash Deposit Amount Base 2	  */
	public void setCashDepositAmtB2 (BigDecimal CashDepositAmtB2);

	/** Get Cash Deposit Amount Base 2	  */
	public BigDecimal getCashDepositAmtB2();

    /** Column name CashIn */
    public static final String COLUMNNAME_CashIn = "CashIn";

	/** Set Cash In	  */
	public void setCashIn (BigDecimal CashIn);

	/** Get Cash In	  */
	public BigDecimal getCashIn();

    /** Column name CashInBase1 */
    public static final String COLUMNNAME_CashInBase1 = "CashInBase1";

	/** Set Cash In Base 1	  */
	public void setCashInBase1 (BigDecimal CashInBase1);

	/** Get Cash In Base 1	  */
	public BigDecimal getCashInBase1();

    /** Column name CashInBase2 */
    public static final String COLUMNNAME_CashInBase2 = "CashInBase2";

	/** Set Cash In Base 2	  */
	public void setCashInBase2 (BigDecimal CashInBase2);

	/** Get Cash In Base 2	  */
	public BigDecimal getCashInBase2();

    /** Column name CashOut */
    public static final String COLUMNNAME_CashOut = "CashOut";

	/** Set Cash Out	  */
	public void setCashOut (BigDecimal CashOut);

	/** Get Cash Out	  */
	public BigDecimal getCashOut();

    /** Column name CashOutBase1 */
    public static final String COLUMNNAME_CashOutBase1 = "CashOutBase1";

	/** Set Cash Out Base 1	  */
	public void setCashOutBase1 (BigDecimal CashOutBase1);

	/** Get Cash Out Base 1	  */
	public BigDecimal getCashOutBase1();

    /** Column name CashOutBase2 */
    public static final String COLUMNNAME_CashOutBase2 = "CashOutBase2";

	/** Set Cash Out Base 2	  */
	public void setCashOutBase2 (BigDecimal CashOutBase2);

	/** Get Cash Out Base 2	  */
	public BigDecimal getCashOutBase2();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name EndingBalance */
    public static final String COLUMNNAME_EndingBalance = "EndingBalance";

	/** Set Ending balance.
	  * Ending  or closing balance
	  */
	public void setEndingBalance (BigDecimal EndingBalance);

	/** Get Ending balance.
	  * Ending  or closing balance
	  */
	public BigDecimal getEndingBalance();

    /** Column name EndingBalanceBase1 */
    public static final String COLUMNNAME_EndingBalanceBase1 = "EndingBalanceBase1";

	/** Set Ending Balance Base 1	  */
	public void setEndingBalanceBase1 (BigDecimal EndingBalanceBase1);

	/** Get Ending Balance Base 1	  */
	public BigDecimal getEndingBalanceBase1();

    /** Column name EndingBalanceBase2 */
    public static final String COLUMNNAME_EndingBalanceBase2 = "EndingBalanceBase2";

	/** Set Ending Balance Base 2	  */
	public void setEndingBalanceBase2 (BigDecimal EndingBalanceBase2);

	/** Get Ending Balance Base 2	  */
	public BigDecimal getEndingBalanceBase2();

    /** Column name EstimationShortCashierAmt */
    public static final String COLUMNNAME_EstimationShortCashierAmt = "EstimationShortCashierAmt";

	/** Set Estimation Short Cashier Amount	  */
	public void setEstimationShortCashierAmt (BigDecimal EstimationShortCashierAmt);

	/** Get Estimation Short Cashier Amount	  */
	public BigDecimal getEstimationShortCashierAmt();

    /** Column name EstimationShortCashierAmtBase1 */
    public static final String COLUMNNAME_EstimationShortCashierAmtBase1 = "EstimationShortCashierAmtBase1";

	/** Set Estimation Short Cashier Amount Base 1	  */
	public void setEstimationShortCashierAmtBase1 (BigDecimal EstimationShortCashierAmtBase1);

	/** Get Estimation Short Cashier Amount Base 1	  */
	public BigDecimal getEstimationShortCashierAmtBase1();

    /** Column name EstimationShortCashierAmtBase2 */
    public static final String COLUMNNAME_EstimationShortCashierAmtBase2 = "EstimationShortCashierAmtBase2";

	/** Set Estimation Short Cashier Amount Base 2	  */
	public void setEstimationShortCashierAmtBase2 (BigDecimal EstimationShortCashierAmtBase2);

	/** Get Estimation Short Cashier Amount Base 2	  */
	public BigDecimal getEstimationShortCashierAmtBase2();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsUsedForChange */
    public static final String COLUMNNAME_IsUsedForChange = "IsUsedForChange";

	/** Set Used For Change ?	  */
	public void setIsUsedForChange (boolean IsUsedForChange);

	/** Get Used For Change ?	  */
	public boolean isUsedForChange();

    /** Column name PayableRefundAmt */
    public static final String COLUMNNAME_PayableRefundAmt = "PayableRefundAmt";

	/** Set Payable Refund Amount.
	  * The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt);

	/** Get Payable Refund Amount.
	  * The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt();

    /** Column name PayableRefundAmtB1 */
    public static final String COLUMNNAME_PayableRefundAmtB1 = "PayableRefundAmtB1";

	/** Set Payable Refund Amount B1.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB1 (BigDecimal PayableRefundAmtB1);

	/** Get Payable Refund Amount B1.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB1();

    /** Column name PayableRefundAmtB2 */
    public static final String COLUMNNAME_PayableRefundAmtB2 = "PayableRefundAmtB2";

	/** Set Payable Refund Amount B2.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB2 (BigDecimal PayableRefundAmtB2);

	/** Get Payable Refund Amount B2.
	  * The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB2();

    /** Column name ShortCashierAmt */
    public static final String COLUMNNAME_ShortCashierAmt = "ShortCashierAmt";

	/** Set Short Cashier Amount.
	  * The amount differences between cash-fisically with cash to deposit in system.
	  */
	public void setShortCashierAmt (BigDecimal ShortCashierAmt);

	/** Get Short Cashier Amount.
	  * The amount differences between cash-fisically with cash to deposit in system.
	  */
	public BigDecimal getShortCashierAmt();

    /** Column name ShortCashierAmtBase1 */
    public static final String COLUMNNAME_ShortCashierAmtBase1 = "ShortCashierAmtBase1";

	/** Set Short Cashier Amount Base 1	  */
	public void setShortCashierAmtBase1 (BigDecimal ShortCashierAmtBase1);

	/** Get Short Cashier Amount Base 1	  */
	public BigDecimal getShortCashierAmtBase1();

    /** Column name ShortCashierAmtBase2 */
    public static final String COLUMNNAME_ShortCashierAmtBase2 = "ShortCashierAmtBase2";

	/** Set Short Cashier Amount Base 2	  */
	public void setShortCashierAmtBase2 (BigDecimal ShortCashierAmtBase2);

	/** Get Short Cashier Amount Base 2	  */
	public BigDecimal getShortCashierAmtBase2();

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException;

    /** Column name UNS_POSTerminalCashAcct_ID */
    public static final String COLUMNNAME_UNS_POSTerminalCashAcct_ID = "UNS_POSTerminalCashAcct_ID";

	/** Set Cash Account	  */
	public void setUNS_POSTerminalCashAcct_ID (int UNS_POSTerminalCashAcct_ID);

	/** Get Cash Account	  */
	public int getUNS_POSTerminalCashAcct_ID();

	public com.unicore.model.I_UNS_POSTerminalCashAcct getUNS_POSTerminalCashAcct() throws RuntimeException;

    /** Column name UNS_SessionCashAccount_ID */
    public static final String COLUMNNAME_UNS_SessionCashAccount_ID = "UNS_SessionCashAccount_ID";

	/** Set Session Cash Account	  */
	public void setUNS_SessionCashAccount_ID (int UNS_SessionCashAccount_ID);

	/** Get Session Cash Account	  */
	public int getUNS_SessionCashAccount_ID();

    /** Column name UNS_SessionCashAccount_UU */
    public static final String COLUMNNAME_UNS_SessionCashAccount_UU = "UNS_SessionCashAccount_UU";

	/** Set UNS_SessionCashAccount_UU	  */
	public void setUNS_SessionCashAccount_UU (String UNS_SessionCashAccount_UU);

	/** Get UNS_SessionCashAccount_UU	  */
	public String getUNS_SessionCashAccount_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
