/**
 * 
 */
package com.unicore.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.util.Properties;
import com.uns.edc.IEDCPaymentProcessor;
import com.uns.model.MUNSEDCProcessor;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSEDC extends X_UNS_EDC {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2554571078780508259L;

	/**
	 * @param ctx
	 * @param UNS_EDC_ID
	 * @param trxName
	 */
	public MUNSEDC(Properties ctx, int UNS_EDC_ID, String trxName) {
		super(ctx, UNS_EDC_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSEDC(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public IEDCPaymentProcessor getOnlinePayProcessor ()
	{
		String comm = getCommunication();
		if (null == comm)
			return null;
		if (getUNS_EDCProcessor_ID() <= 0)
			return null;
		
		MUNSEDCProcessor procConf = new MUNSEDCProcessor(getCtx(), getUNS_EDCProcessor_ID(), get_TrxName());
		String className = procConf.getClassname();
		if (className == null)
			return null;
		Class<?> c;
		IEDCPaymentProcessor processor = null;
		try 
		{
			c = Class.forName(className);
			Constructor<?> constructor = c.getConstructor(String.class, MUNSEDCProcessor.class);
			processor = (IEDCPaymentProcessor) constructor.newInstance(comm, procConf);
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchMethodException e) 
		{
			e.printStackTrace();
		}
		catch (SecurityException e) 
		{
			e.printStackTrace();
		} 
		catch (InstantiationException e) 
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		}
		catch (InvocationTargetException e) 
		{
			e.printStackTrace();
		}
		
		return processor;
	}
	
	public static MUNSEDC get(Properties ctx, String IDNo, String trxName)
	{
		MUNSEDC edc = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, COLUMNNAME_IDNo + "=?", trxName).setParameters(IDNo)
					.firstOnly();
		
		return edc;
	}
}