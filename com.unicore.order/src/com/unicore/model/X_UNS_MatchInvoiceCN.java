/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_MatchInvoiceCN
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_MatchInvoiceCN extends PO implements I_UNS_MatchInvoiceCN, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180218L;

    /** Standard Constructor */
    public X_UNS_MatchInvoiceCN (Properties ctx, int UNS_MatchInvoiceCN_ID, String trxName)
    {
      super (ctx, UNS_MatchInvoiceCN_ID, trxName);
      /** if (UNS_MatchInvoiceCN_ID == 0)
        {
			setAmountApplied (Env.ZERO);
// 0
			setBalance (Env.ZERO);
// 0
			setIsFirstRecord (false);
// N
			setIsQuantityBased (true);
// Y
			setM_InOutLine_ID (0);
			setM_Product_ID (0);
			setQty (Env.ZERO);
// 0
			setQtySubtract (Env.ZERO);
// 0
			setTotalAmountApplied (Env.ZERO);
// 0
			setTotalQtyApplied (Env.ZERO);
// 0
			setUNS_MatchInvoiceCN_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_MatchInvoiceCN (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_MatchInvoiceCN[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount Applied.
		@param AmountApplied Amount Applied	  */
	public void setAmountApplied (BigDecimal AmountApplied)
	{
		set_Value (COLUMNNAME_AmountApplied, AmountApplied);
	}

	/** Get Amount Applied.
		@return Amount Applied	  */
	public BigDecimal getAmountApplied () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmountApplied);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Balance.
		@param Balance Balance	  */
	public void setBalance (BigDecimal Balance)
	{
		set_Value (COLUMNNAME_Balance, Balance);
	}

	/** Get Balance.
		@return Balance	  */
	public BigDecimal getBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Balance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set First Record.
		@param IsFirstRecord First Record	  */
	public void setIsFirstRecord (boolean IsFirstRecord)
	{
		set_Value (COLUMNNAME_IsFirstRecord, Boolean.valueOf(IsFirstRecord));
	}

	/** Get First Record.
		@return First Record	  */
	public boolean isFirstRecord () 
	{
		Object oo = get_Value(COLUMNNAME_IsFirstRecord);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Quantity based.
		@param IsQuantityBased 
		Trade discount break level based on Quantity (not value)
	  */
	public void setIsQuantityBased (boolean IsQuantityBased)
	{
		set_ValueNoCheck (COLUMNNAME_IsQuantityBased, Boolean.valueOf(IsQuantityBased));
	}

	/** Get Quantity based.
		@return Trade discount break level based on Quantity (not value)
	  */
	public boolean isQuantityBased () 
	{
		Object oo = get_Value(COLUMNNAME_IsQuantityBased);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_InOutLine getM_InOutLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_InOutLine)MTable.get(getCtx(), org.compiere.model.I_M_InOutLine.Table_Name)
			.getPO(getM_InOutLine_ID(), get_TrxName());	}

	/** Set Shipment/Receipt Line.
		@param M_InOutLine_ID 
		Line on Shipment or Receipt document
	  */
	public void setM_InOutLine_ID (int M_InOutLine_ID)
	{
		if (M_InOutLine_ID < 1) 
			set_Value (COLUMNNAME_M_InOutLine_ID, null);
		else 
			set_Value (COLUMNNAME_M_InOutLine_ID, Integer.valueOf(M_InOutLine_ID));
	}

	/** Get Shipment/Receipt Line.
		@return Line on Shipment or Receipt document
	  */
	public int getM_InOutLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InOutLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Subtract Quantity.
		@param QtySubtract 
		Quantity to subtract when generating commissions
	  */
	public void setQtySubtract (BigDecimal QtySubtract)
	{
		set_Value (COLUMNNAME_QtySubtract, QtySubtract);
	}

	/** Get Subtract Quantity.
		@return Quantity to subtract when generating commissions
	  */
	public BigDecimal getQtySubtract () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtySubtract);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Amount Applied.
		@param TotalAmountApplied Total Amount Applied	  */
	public void setTotalAmountApplied (BigDecimal TotalAmountApplied)
	{
		set_Value (COLUMNNAME_TotalAmountApplied, TotalAmountApplied);
	}

	/** Get Total Amount Applied.
		@return Total Amount Applied	  */
	public BigDecimal getTotalAmountApplied () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmountApplied);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Qty Applied.
		@param TotalQtyApplied Total Qty Applied	  */
	public void setTotalQtyApplied (BigDecimal TotalQtyApplied)
	{
		set_Value (COLUMNNAME_TotalQtyApplied, TotalQtyApplied);
	}

	/** Get Total Qty Applied.
		@return Total Qty Applied	  */
	public BigDecimal getTotalQtyApplied () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalQtyApplied);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Match Invoice CN.
		@param UNS_MatchInvoiceCN_ID Match Invoice CN	  */
	public void setUNS_MatchInvoiceCN_ID (int UNS_MatchInvoiceCN_ID)
	{
		if (UNS_MatchInvoiceCN_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_MatchInvoiceCN_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_MatchInvoiceCN_ID, Integer.valueOf(UNS_MatchInvoiceCN_ID));
	}

	/** Get Match Invoice CN.
		@return Match Invoice CN	  */
	public int getUNS_MatchInvoiceCN_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_MatchInvoiceCN_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_MatchInvoiceCN_UU.
		@param UNS_MatchInvoiceCN_UU UNS_MatchInvoiceCN_UU	  */
	public void setUNS_MatchInvoiceCN_UU (String UNS_MatchInvoiceCN_UU)
	{
		set_Value (COLUMNNAME_UNS_MatchInvoiceCN_UU, UNS_MatchInvoiceCN_UU);
	}

	/** Get UNS_MatchInvoiceCN_UU.
		@return UNS_MatchInvoiceCN_UU	  */
	public String getUNS_MatchInvoiceCN_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_MatchInvoiceCN_UU);
	}
}