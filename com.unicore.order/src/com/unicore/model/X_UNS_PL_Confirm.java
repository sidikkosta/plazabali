/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_PL_Confirm
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PL_Confirm extends PO implements I_UNS_PL_Confirm, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20151219L;

    /** Standard Constructor */
    public X_UNS_PL_Confirm (Properties ctx, int UNS_PL_Confirm_ID, String trxName)
    {
      super (ctx, UNS_PL_Confirm_ID, trxName);
      /** if (UNS_PL_Confirm_ID == 0)
        {
			setConfirmType (null);
			setDocAction (null);
// PR
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setIsLineBasedConfirmation (true);
// Y
			setProcessed (false);
// N
			setUNS_PL_Confirm_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PL_Confirm (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PL_Confirm[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Confirmation No.
		@param ConfirmationNo 
		Confirmation Number
	  */
	public void setConfirmationNo (String ConfirmationNo)
	{
		set_Value (COLUMNNAME_ConfirmationNo, ConfirmationNo);
	}

	/** Get Confirmation No.
		@return Confirmation Number
	  */
	public String getConfirmationNo () 
	{
		return (String)get_Value(COLUMNNAME_ConfirmationNo);
	}

	/** Packing List Confirmation = PC */
	public static final String CONFIRMTYPE_PackingListConfirmation = "PC";
	/** Shipment Return Confirmation = RC */
	public static final String CONFIRMTYPE_ShipmentReturnConfirmation = "RC";
	/** Set Confirmation Type.
		@param ConfirmType 
		Type of confirmation
	  */
	public void setConfirmType (String ConfirmType)
	{

		set_ValueNoCheck (COLUMNNAME_ConfirmType, ConfirmType);
	}

	/** Get Confirmation Type.
		@return Type of confirmation
	  */
	public String getConfirmType () 
	{
		return (String)get_Value(COLUMNNAME_ConfirmType);
	}

	/** Set Copy Of Print Shipping Letter.
		@param CopyOfPrintSL Copy Of Print Shipping Letter	  */
	public void setCopyOfPrintSL (int CopyOfPrintSL)
	{
		set_Value (COLUMNNAME_CopyOfPrintSL, Integer.valueOf(CopyOfPrintSL));
	}

	/** Get Copy Of Print Shipping Letter.
		@return Copy Of Print Shipping Letter	  */
	public int getCopyOfPrintSL () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CopyOfPrintSL);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Copy Of Print Shipping Letter Location.
		@param CopyOfPrintSLL Copy Of Print Shipping Letter Location	  */
	public void setCopyOfPrintSLL (int CopyOfPrintSLL)
	{
		set_Value (COLUMNNAME_CopyOfPrintSLL, Integer.valueOf(CopyOfPrintSLL));
	}

	/** Get Copy Of Print Shipping Letter Location.
		@return Copy Of Print Shipping Letter Location	  */
	public int getCopyOfPrintSLL () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CopyOfPrintSLL);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_ValueNoCheck (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getDocumentNo());
    }

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_ValueNoCheck (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Line Based Confirmation.
		@param IsLineBasedConfirmation 
		To indicate if the confirmation will be calculated by order line confirmation instead of per product quantity
	  */
	public void setIsLineBasedConfirmation (boolean IsLineBasedConfirmation)
	{
		set_Value (COLUMNNAME_IsLineBasedConfirmation, Boolean.valueOf(IsLineBasedConfirmation));
	}

	/** Get Is Line Based Confirmation.
		@return To indicate if the confirmation will be calculated by order line confirmation instead of per product quantity
	  */
	public boolean isLineBasedConfirmation () 
	{
		Object oo = get_Value(COLUMNNAME_IsLineBasedConfirmation);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Printed.
		@param IsPrinted 
		Indicates if this document / line is printed
	  */
	public void setIsPrinted (boolean IsPrinted)
	{
		set_ValueNoCheck (COLUMNNAME_IsPrinted, Boolean.valueOf(IsPrinted));
	}

	/** Get Printed.
		@return Indicates if this document / line is printed
	  */
	public boolean isPrinted () 
	{
		Object oo = get_Value(COLUMNNAME_IsPrinted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Print Berita Acara Serah Terima Surat Jalan.
		@param PBASTSJ Print Berita Acara Serah Terima Surat Jalan	  */
	public void setPBASTSJ (String PBASTSJ)
	{
		set_Value (COLUMNNAME_PBASTSJ, PBASTSJ);
	}

	/** Get Print Berita Acara Serah Terima Surat Jalan.
		@return Print Berita Acara Serah Terima Surat Jalan	  */
	public String getPBASTSJ () 
	{
		return (String)get_Value(COLUMNNAME_PBASTSJ);
	}

	/** Set Print Faktur Penjualan.
		@param PrintFakturPenjualan Print Faktur Penjualan	  */
	public void setPrintFakturPenjualan (String PrintFakturPenjualan)
	{
		set_Value (COLUMNNAME_PrintFakturPenjualan, PrintFakturPenjualan);
	}

	/** Get Print Faktur Penjualan.
		@return Print Faktur Penjualan	  */
	public String getPrintFakturPenjualan () 
	{
		return (String)get_Value(COLUMNNAME_PrintFakturPenjualan);
	}

	/** Set Print Surat Jalan.
		@param PrintSJ Print Surat Jalan	  */
	public void setPrintSJ (String PrintSJ)
	{
		set_Value (COLUMNNAME_PrintSJ, PrintSJ);
	}

	/** Get Print Surat Jalan.
		@return Print Surat Jalan	  */
	public String getPrintSJ () 
	{
		return (String)get_Value(COLUMNNAME_PrintSJ);
	}

	/** Set Print Surat Jalan Balikan TMG.
		@param PrintSJBalikan Print Surat Jalan Balikan TMG	  */
	public void setPrintSJBalikan (String PrintSJBalikan)
	{
		set_Value (COLUMNNAME_PrintSJBalikan, PrintSJBalikan);
	}

	/** Get Print Surat Jalan Balikan TMG.
		@return Print Surat Jalan Balikan TMG	  */
	public String getPrintSJBalikan () 
	{
		return (String)get_Value(COLUMNNAME_PrintSJBalikan);
	}

	/** Set Print Surat Jalan PPN.
		@param PrintSJPPN Print Surat Jalan PPN	  */
	public void setPrintSJPPN (String PrintSJPPN)
	{
		set_Value (COLUMNNAME_PrintSJPPN, PrintSJPPN);
	}

	/** Get Print Surat Jalan PPN.
		@return Print Surat Jalan PPN	  */
	public String getPrintSJPPN () 
	{
		return (String)get_Value(COLUMNNAME_PrintSJPPN);
	}

	/** Set Print Shipping Letter.
		@param PrintSL Print Shipping Letter	  */
	public void setPrintSL (String PrintSL)
	{
		set_Value (COLUMNNAME_PrintSL, PrintSL);
	}

	/** Get Print Shipping Letter.
		@return Print Shipping Letter	  */
	public String getPrintSL () 
	{
		return (String)get_Value(COLUMNNAME_PrintSL);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_ValueNoCheck (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.unicore.model.I_UNS_PL_Confirm getReversal() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PL_Confirm)MTable.get(getCtx(), com.unicore.model.I_UNS_PL_Confirm.Table_Name)
			.getPO(getReversal_ID(), get_TrxName());	}

	/** Set Reversal ID.
		@param Reversal_ID 
		ID of document reversal
	  */
	public void setReversal_ID (int Reversal_ID)
	{
		if (Reversal_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Reversal_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Reversal_ID, Integer.valueOf(Reversal_ID));
	}

	/** Get Reversal ID.
		@return ID of document reversal
	  */
	public int getReversal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Reversal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tonase.
		@param Tonase 
		Indicate total tonase
	  */
	public void setTonase (BigDecimal Tonase)
	{
		set_ValueNoCheck (COLUMNNAME_Tonase, Tonase);
	}

	/** Get Tonase.
		@return Indicate total tonase
	  */
	public BigDecimal getTonase () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Tonase);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_PackingList getUNS_PackingList() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PackingList)MTable.get(getCtx(), com.unicore.model.I_UNS_PackingList.Table_Name)
			.getPO(getUNS_PackingList_ID(), get_TrxName());	}

	/** Set Packing List.
		@param UNS_PackingList_ID Packing List	  */
	public void setUNS_PackingList_ID (int UNS_PackingList_ID)
	{
		if (UNS_PackingList_ID < 1) 
			set_Value (COLUMNNAME_UNS_PackingList_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_PackingList_ID, Integer.valueOf(UNS_PackingList_ID));
	}

	/** Get Packing List.
		@return Packing List	  */
	public int getUNS_PackingList_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PackingList_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Packing List Confirmation.
		@param UNS_PL_Confirm_ID Packing List Confirmation	  */
	public void setUNS_PL_Confirm_ID (int UNS_PL_Confirm_ID)
	{
		if (UNS_PL_Confirm_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PL_Confirm_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PL_Confirm_ID, Integer.valueOf(UNS_PL_Confirm_ID));
	}

	/** Get Packing List Confirmation.
		@return Packing List Confirmation	  */
	public int getUNS_PL_Confirm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PL_Confirm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PL_Confirm_UU.
		@param UNS_PL_Confirm_UU UNS_PL_Confirm_UU	  */
	public void setUNS_PL_Confirm_UU (String UNS_PL_Confirm_UU)
	{
		set_Value (COLUMNNAME_UNS_PL_Confirm_UU, UNS_PL_Confirm_UU);
	}

	/** Get UNS_PL_Confirm_UU.
		@return UNS_PL_Confirm_UU	  */
	public String getUNS_PL_Confirm_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PL_Confirm_UU);
	}

	public com.unicore.model.I_UNS_PL_Return getUNS_PL_Return() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PL_Return)MTable.get(getCtx(), com.unicore.model.I_UNS_PL_Return.Table_Name)
			.getPO(getUNS_PL_Return_ID(), get_TrxName());	}

	/** Set Packing List Return.
		@param UNS_PL_Return_ID Packing List Return	  */
	public void setUNS_PL_Return_ID (int UNS_PL_Return_ID)
	{
		if (UNS_PL_Return_ID < 1) 
			set_Value (COLUMNNAME_UNS_PL_Return_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_PL_Return_ID, Integer.valueOf(UNS_PL_Return_ID));
	}

	/** Get Packing List Return.
		@return Packing List Return	  */
	public int getUNS_PL_Return_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PL_Return_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Volume.
		@param Volume 
		Volume of a product
	  */
	public void setVolume (BigDecimal Volume)
	{
		set_ValueNoCheck (COLUMNNAME_Volume, Volume);
	}

	/** Get Volume.
		@return Volume of a product
	  */
	public BigDecimal getVolume () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Volume);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}