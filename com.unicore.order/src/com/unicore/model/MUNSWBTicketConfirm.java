/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.unicore.ui.ISortTabRecord;
import com.uns.base.model.Query;


/**
 * @author AzHaidar
 *
 */
public class MUNSWBTicketConfirm extends X_UNS_WBTicket_Confirm implements DocAction, ISortTabRecord, DocOptions 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6784125710591720459L;

	String m_processMsg = null;
	private boolean m_justPrepared = false;
	private MUNSWeighbridgeTicket m_ticket = null;
	
	public MUNSWeighbridgeTicket getTicket ()
	{
		if (null == m_ticket)
		{
			m_ticket = new MUNSWeighbridgeTicket(
					getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
		}
		
		return m_ticket;
	}

	/**
	 * @param ctx
	 * @param UNS_WBTicket_Confirm_ID
	 * @param trxName
	 */
	public MUNSWBTicketConfirm(Properties ctx, int UNS_WBTicket_Confirm_ID,
			String trxName) {
		super(ctx, UNS_WBTicket_Confirm_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSWBTicketConfirm(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/**
	 * 
	 * @param ctx
	 * @param UNS_WeighbridgeTicket_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSWBTicketConfirm get(Properties ctx, int UNS_WeighbridgeTicket_ID, String trxName) 
	{
		MUNSWBTicketConfirm retVal = 
				Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
						"UNS_WeighbridgeTicket_ID=?", trxName)
				.setParameters(UNS_WeighbridgeTicket_ID)
				.first();
		
		return retVal;
	}
	
	/**
	 * 
	 * @return
	 */
	public static MUNSWBTicketConfirm getCreate(Properties ctx, MUNSWeighbridgeTicket ticket, String trxName)
	{
		MUNSWBTicketConfirm retVal = 
				Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, MUNSWBTicketConfirm.Table_Name, 
						"UNS_WeighbridgeTicket_ID=?", trxName)
				.setParameters(ticket.getUNS_WeighbridgeTicket_ID())
				.first();		
		
		if (retVal == null)
		{
			retVal = new MUNSWBTicketConfirm(ctx, 0, trxName);
			retVal.setUNS_WeighbridgeTicket_ID(ticket.get_ID());
			retVal.setAD_Org_ID(ticket.getAD_Org_ID());
			retVal.setC_Order_ID(ticket.getC_Order_ID());
			retVal.setC_BPartner_ID(ticket.getC_BPartner_ID());
			retVal.setM_Product_ID(ticket.getM_Product_ID());
			retVal.setVehicleNo(ticket.getVehicleNo());
			retVal.setVehicleType(ticket.getVehicleType());
			retVal.setTransporter_ID(ticket.getTransporter_ID());
			retVal.setTransporterName(ticket.getTransporterName());
			retVal.setVehicleDriverName(ticket.getVehicleDriverName());
			retVal.setAD_User_ID(ticket.getAD_User_ID());
			retVal.setTimeIn(ticket.getTimeIn());
			retVal.setTimeOut(ticket.getTimeOut());
			retVal.setGrossWeight(ticket.getGrossWeight());
			retVal.setGrossWeightTarget(ticket.getGrossWeight());
			retVal.setTare(ticket.getTare());
			retVal.setTareTarget(ticket.getTare());
			retVal.setReflection(ticket.getReflection());
			retVal.setReflectionTarget(ticket.getReflection());
			retVal.setNettoI(ticket.getNettoI());
			retVal.setNettoITarget(ticket.getNettoI());
			retVal.setNettoII(ticket.getNettoII());
			retVal.setNettoIITarget(ticket.getNettoII());	
			retVal.setIsSplitOrder(ticket.isSplitOrder());
			retVal.setIsOriginalTicket(ticket.isOriginalTicket());
			retVal.setSplittedTicket_ID(ticket.getSplittedTicket_ID());
			retVal.setOriginalNettoI(ticket.getOriginalNettoI());
			retVal.setOriginalNettoII(ticket.getOriginalNettoII());
			retVal.setSplittedOrder_ID(ticket.getSplittedOrder_ID());			
			retVal.setDocStatus(DOCSTATUS_Drafted);
			retVal.setDocAction(DOCACTION_Complete);
			
			if (!retVal.save()) {
				return null;
			}
		}
		
		retVal.m_ticket = ticket;
		return retVal;
	} // createCustomerWeighbridgeConfirmation

	@Override
    protected boolean beforeSave(boolean newRecord) 
    {
		getTicket();
		MUNSWBTicketConfirm split = get(getCtx(), m_ticket.getSplittedTicket_ID(), get_TrxName());
		
		if (!newRecord)
		{
			if (getTimeIn() == null || getTimeOut() == null) {
				log.saveError("TimeInOutNotEntered", "Please enter Time-In and Time-Out respectively.");
				return false;
			}
			
			boolean timeInOutValid = getTimeOut().after(getTimeIn());
			if (!timeInOutValid) {
				log.saveError("TimeInOutNotValid", "Please make sure Time-Out greater than Time-In.");
				return false;
			}
			
//			MUNSWeighbridgeTicket wbt = 
//					new MUNSWeighbridgeTicket(getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
			
			if (getTimeIn().compareTo(m_ticket.getTimeIn()) == 0 || getTimeOut().compareTo(m_ticket.getTimeOut()) == 0) {
				log.info("Time In-Out at customer must be different with internal time in-out.\n "
						+ "Please change the time as they are at the customer's site.");
			}

			if (getGrossWeight().signum() <= 0 || getTare().signum() <= 0) {
				log.saveError("FieldMandatory", "Please enter mandatory for field: Gross Weight & Tare.");
				return false;
			}
			
			if(isSplitOrder() && !isReplication())
			{
				
				if(!isOriginalTicket())
				{
					BigDecimal originalnetto = split.getNettoII().add(getNettoII());
					BigDecimal gross = getTare().add(originalnetto);
					
					setGrossWeight(gross);
					setOriginalNettoI(originalnetto);
					setOriginalNettoII(originalnetto);

				}
				
				setNettoI(getNettoII().subtract(getReflection()));
			}
			
			BigDecimal diff = Env.ZERO;
			if(getNettoII().compareTo(getNettoIITarget()) <= 0 )
				diff = getNettoIITarget().subtract(getNettoII());
			else
				diff = getNettoII().subtract(getNettoIITarget());
			
			setDifferenceQty(diff);
			
		}

		return true;
    } // beforeSave
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		if(isSplitOrder() && isOriginalTicket() && !isReplication())
		{
			BigDecimal NettoI = getOriginalNettoI().subtract(getNettoI());
			BigDecimal NettoII = getOriginalNettoII().subtract(getNettoII());
			
			String sql = "UPDATE UNS_WBTicket_Confirm SET GrossWeight = "+getGrossWeight()
					+ ", Tare = "+getTare()
					+ ", OriginalNettoI = "+getOriginalNettoI()
					+ ", OriginalNettoII =  "+getOriginalNettoII()
					+ ", NettoI = "+NettoI
					+ ", NettoII = "+NettoII
					+ ", DifferenceQty = (CASE WHEN "+NettoII
					+ " >= NettoIITarget THEN "+NettoII
					+ " - NettoIITarget ELSE NettoIITarget - "+NettoII
					+ " END) WHERE UNS_WeighbridgeTicket_ID = ?";
			DB.executeUpdate(sql, getSplittedTicket_ID(), get_TrxName());
		
		}
		else if(isSplitOrder() && !isOriginalTicket() && !isReplication())
		{
			String sql = "UPDATE UNS_WBTicket_Confirm SET GrossWeight = "+getGrossWeight()
					+ ", OriginalNettoI = "+getOriginalNettoI()
					+ ", OriginalNettoII = "+getOriginalNettoII()
					+ " WHERE UNS_WeighBridgeTicket_ID = ?";
			DB.executeUpdate(sql, getSplittedTicket_ID() , get_TrxName());
					
		}
		return super.afterSave(newRecord, success);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String processAction) throws Exception 
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		//setProcessing(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	} // prepareIt
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		StringBuilder info = new StringBuilder();
		
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		
//		if (isSplitOrder() && isOriginalTicket() && !isReplication())
//		{
//			if (getSplittedTicket_ID() == 0)
//			{
//				setSplittedTicket_ID(getTicket().getSplittedTicket_ID());
//			}
//			
//			MUNSWeighbridgeTicket splittedTicket = new MUNSWeighbridgeTicket(
//					getCtx(), getSplittedTicket_ID(), get_TrxName());
//			
//			MUNSWBTicketConfirm splittedConfirm = getCreate(
//					getCtx(), splittedTicket, get_TrxName());
//			try {
//				if (!splittedConfirm.processIt(DOCACTION_Complete)) {
//					m_processMsg = "Failed when try to complete splitted document." + splittedConfirm.getProcessMsg();
//					return DocAction.STATUS_Invalid;
//				}
//			}
//			catch (Exception ex) {
//				ex.printStackTrace();
//				m_processMsg = ex.getMessage() + ". Please try to complete it manually.";
//				return DocAction.STATUS_InProgress;
//			}
//		}
		
//		try 
//		{
//			MUNSWeighbridgeTicket wbTicket = 
//					new MUNSWeighbridgeTicket(getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
//			
//			if (!wbTicket.processIt(DOCACTION_Complete))
//			{
//				m_processMsg = 
//						"Failed while automatically completing it's Weihbridge Ticket: " + CLogger.retrieveError();
//				return DOCSTATUS_InProgress;
//			}
//			wbTicket.saveEx();
//		}
//		catch (Exception ex) {
//			m_processMsg = ex.getMessage();
//			ex.printStackTrace();
//			return DOCSTATUS_Invalid;
//		}
		
		String replica = MSysConfig.getValue(MSysConfig.IS_REPLICA, "N");
		boolean isReplica = "Y".equals(replica);
		if (!isReplica && !isCancelled()) {
			m_processMsg = getTicket().processShipReceipt(DOCACTION_Complete);
			if (m_processMsg != null)	
			{
				return DOCSTATUS_Invalid ;
			}
		}
		else if(!isReplica && isCancelled())
		{
			m_processMsg = cancelTicket();
			if (m_processMsg != null)	
			{
				return DOCSTATUS_Invalid ;
			}
		}
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			if (info.length() > 0)
				info.append(" - ");
			info.append(valid);
			m_processMsg = info.toString();
			return DocAction.STATUS_Invalid;
		}

		setProcessed(true);	
 		m_processMsg = info.toString();
//
//		setDocStatus(DOCSTATUS_Completed);

		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt
	
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		if (isSplitOrder() && !isReplication())
		{
			try
			{
				
				MUNSWBTicketConfirm split = MUNSWBTicketConfirm.get(
						getCtx(), getSplittedTicket_ID(), get_TrxName());
				
				if (isOriginalTicket())
				{
					split.m_justPrepared = true;
					if (!split.getDocStatus().equals(DOCSTATUS_Voided) && !split.processIt(DOCACTION_Void))
					{
						m_processMsg = split.getProcessMsg();
						return false;
					}
					split.m_justPrepared = false;
					split.saveEx();
				}
				else if (!m_justPrepared && !DOCSTATUS_Voided.equals(split.getDocStatus ()))
				{
					m_processMsg = "Please void original ticket first.";
					return false;
				}
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return false;
			}
		}
		
		String replica = MSysConfig.getValue(MSysConfig.IS_REPLICA, "N");
		boolean isReplica = "Y".equals(replica);
		if (!isReplica && !isCancelled()) {
			if (getTicket().getM_InOut_ID() > 0)
			{
				m_processMsg = getTicket().processShipReceipt(DOCACTION_Void);
				if (m_processMsg != null)	
				{
					return false;
				}
			}
			
		}
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;	
		
		if(isCancelled())
		{
			m_processMsg = "Ticket has been canceled. Can't re-active it";
			return false;
		}
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		//	: Grand Total = 123.00 (#1)
		sb.append(": ").
			append("Netto-1").append("=").append(getNettoI())
			.append("Netto-2").append("=").append(getNettoII()).append(")");
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo()
	{
		return  "Customer Weighbridge Ticket Confirmation " + getDocumentNo();
	}	//	getDocumentInfo

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
		ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.SHIPMENT, getC_Order_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if(format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo ("", format.getJasperProcess_ID());
			pi.setRecord_ID ( getC_Order_ID() );
			pi.setIsBatch(true);
			
			ServerProcessCtl.process(pi, null);
			
			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	}	//	createPDF
	
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{	
		return getAD_User_ID();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt()
	{
		return getNettoII();
	}	//	getApprovalAmt

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) {
		
		return null;
	}

	@Override
	public String beforeRemoveSelection() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		if (DOCSTATUS_Completed.equals(docStatus)) {
			options[index++] = DOCACTION_Void;
			options[index++] = DOCACTION_Re_Activate;
		}
		return index;
	}
	
	@SuppressWarnings("unused")
	public String cancelTicket()
	{
		String retVal = null;
		
		if(m_ticket==null)
			getTicket();
		
		if(m_ticket.getDocStatus().equals(MUNSWeighbridgeTicket.DOCSTATUS_Voided)
				|| m_ticket.getDocStatus().equals(MUNSWeighbridgeTicket.DOCSTATUS_Reversed))
			return "Please check document ticket first. Status is not complete";
		
		if(m_ticket.isSplitOrder() && m_ticket.isOriginalTicket())
		{
			MUNSWeighbridgeTicket splitTicket = 
					new MUNSWeighbridgeTicket(getCtx(), m_ticket.getSplittedTicket_ID(), get_TrxName());
			if(splitTicket == null)
				return "Cannot found split ticket";
			
			if(splitTicket.getDocStatus().equals(MUNSWeighbridgeTicket.DOCSTATUS_Voided)
					|| splitTicket.getDocStatus().equals(MUNSWeighbridgeTicket.DOCSTATUS_Reversed))
				return "Please check document ticket first. Status is not complete";
			
			MUNSWBTicketConfirm splitConfirm = 
					MUNSWBTicketConfirm.get(getCtx(), splitTicket.getUNS_WeighbridgeTicket_ID(), get_TrxName());
			
			splitConfirm.setIsCancelled(true);
			splitConfirm.setTimeIn(getTimeIn());
			splitConfirm.setTimeOut(getTimeOut());
			
			try{
				if(!splitConfirm.processIt(MUNSWBTicketConfirm.DOCACTION_Complete))
					return "Error when try to complete split ticket";
			}
			catch (Exception ex){
				ex.printStackTrace();
				return ex.getMessage();
			}
			 
			splitConfirm.saveEx();
			
			//add DocumentNo with _OS on splited ticket
			for (int i=0; i<100; i++)
			{
				String docNo = splitTicket.getDocumentNo() + "_OS" + i;
				StringBuilder sqlBuilder = new StringBuilder("SELECT 1 FROM ")
				.append(X_UNS_WeighbridgeTicket.Table_Name).append(" WHERE ").append(X_UNS_WeighbridgeTicket.COLUMNNAME_DocumentNo)
				.append(" = ? ");
				String sql = sqlBuilder.toString();
				int value = DB.getSQLValue(get_TrxName(), sql, docNo);
				if (value > 0)
					continue;
				
				splitTicket.setDocumentNo(docNo);
				
				break;
			}
			
			splitTicket.saveEx();
			
		}
		
		//add DocumentNo with _OS on ticket
		for (int i=0; i<100; i++)
		{
			String docNo = m_ticket.getDocumentNo() + "_OS" + i;
			StringBuilder sqlBuilder = new StringBuilder("SELECT 1 FROM ")
			.append(X_UNS_WeighbridgeTicket.Table_Name).append(" WHERE ").append(X_UNS_WeighbridgeTicket.COLUMNNAME_DocumentNo)
			.append(" = ? ");
			String sql = sqlBuilder.toString();
			int value = DB.getSQLValue(get_TrxName(), sql, docNo);
			if (value > 0)
				continue;
			
			m_ticket.setDocumentNo(docNo);
			
			break;
		}
		
		m_ticket.saveEx();
		
		retVal = m_ticket.move(true, getTimeIn());
		
		return retVal;
	}
}