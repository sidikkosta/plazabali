/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_WBTicket_Confirm
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_WBTicket_Confirm extends PO implements I_UNS_WBTicket_Confirm, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180130L;

    /** Standard Constructor */
    public X_UNS_WBTicket_Confirm (Properties ctx, int UNS_WBTicket_Confirm_ID, String trxName)
    {
      super (ctx, UNS_WBTicket_Confirm_ID, trxName);
      /** if (UNS_WBTicket_Confirm_ID == 0)
        {
			setAD_User_ID (0);
			setC_BPartner_ID (0);
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setDocumentNo (null);
			setIsApproved (false);
// N
			setIsSplitOrder (false);
// N
			setNettoI (Env.ZERO);
			setNettoII (Env.ZERO);
			setProcessed (false);
// N
			setQtyClaimed (Env.ZERO);
// 0
			setTare (Env.ZERO);
			setTimeIn (new Timestamp( System.currentTimeMillis() ));
			setTimeOut (new Timestamp( System.currentTimeMillis() ));
			setTransporterName (null);
			setUNS_WBTicket_Confirm_ID (0);
			setUNS_WeighbridgeTicket_ID (0);
			setVehicleNo (null);
			setVehicleType (null);
// DUMP
        } */
    }

    /** Load Constructor */
    public X_UNS_WBTicket_Confirm (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_WBTicket_Confirm[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cancelled Cancel Process.
		@param CancelledCancelProcess Cancelled Cancel Process	  */
	public void setCancelledCancelProcess (String CancelledCancelProcess)
	{
		set_Value (COLUMNNAME_CancelledCancelProcess, CancelledCancelProcess);
	}

	/** Get Cancelled Cancel Process.
		@return Cancelled Cancel Process	  */
	public String getCancelledCancelProcess () 
	{
		return (String)get_Value(COLUMNNAME_CancelledCancelProcess);
	}

	/** Set Add Or Change Qty Claimed.
		@param ChangeQtyClaimed Add Or Change Qty Claimed	  */
	public void setChangeQtyClaimed (String ChangeQtyClaimed)
	{
		set_Value (COLUMNNAME_ChangeQtyClaimed, ChangeQtyClaimed);
	}

	/** Get Add Or Change Qty Claimed.
		@return Add Or Change Qty Claimed	  */
	public String getChangeQtyClaimed () 
	{
		return (String)get_Value(COLUMNNAME_ChangeQtyClaimed);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Difference.
		@param DifferenceQty 
		Difference Quantity
	  */
	public void setDifferenceQty (BigDecimal DifferenceQty)
	{
		set_Value (COLUMNNAME_DifferenceQty, DifferenceQty);
	}

	/** Get Difference.
		@return Difference Quantity
	  */
	public BigDecimal getDifferenceQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Gross Weight.
		@param GrossWeight 
		Weight of a product (Gross)
	  */
	public void setGrossWeight (BigDecimal GrossWeight)
	{
		set_Value (COLUMNNAME_GrossWeight, GrossWeight);
	}

	/** Get Gross Weight.
		@return Weight of a product (Gross)
	  */
	public BigDecimal getGrossWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrossWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Gross Weight Target.
		@param GrossWeightTarget 
		Gross weight target to be confirmed of a product (Gross)
	  */
	public void setGrossWeightTarget (BigDecimal GrossWeightTarget)
	{
		set_ValueNoCheck (COLUMNNAME_GrossWeightTarget, GrossWeightTarget);
	}

	/** Get Gross Weight Target.
		@return Gross weight target to be confirmed of a product (Gross)
	  */
	public BigDecimal getGrossWeightTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrossWeightTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_ValueNoCheck (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Cancelled.
		@param IsCancelled 
		The transaction was cancelled
	  */
	public void setIsCancelled (boolean IsCancelled)
	{
		set_Value (COLUMNNAME_IsCancelled, Boolean.valueOf(IsCancelled));
	}

	/** Get Cancelled.
		@return The transaction was cancelled
	  */
	public boolean isCancelled () 
	{
		Object oo = get_Value(COLUMNNAME_IsCancelled);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Original Ticket.
		@param IsOriginalTicket 
		This is to indicate if this ticket is the ticket record being splitted into another order.
	  */
	public void setIsOriginalTicket (boolean IsOriginalTicket)
	{
		set_ValueNoCheck (COLUMNNAME_IsOriginalTicket, Boolean.valueOf(IsOriginalTicket));
	}

	/** Get Is Original Ticket.
		@return This is to indicate if this ticket is the ticket record being splitted into another order.
	  */
	public boolean isOriginalTicket () 
	{
		Object oo = get_Value(COLUMNNAME_IsOriginalTicket);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Split Order.
		@param IsSplitOrder 
		Is this document being split into two different orders?
	  */
	public void setIsSplitOrder (boolean IsSplitOrder)
	{
		set_Value (COLUMNNAME_IsSplitOrder, Boolean.valueOf(IsSplitOrder));
	}

	/** Get Is Split Order.
		@return Is this document being split into two different orders?
	  */
	public boolean isSplitOrder () 
	{
		Object oo = get_Value(COLUMNNAME_IsSplitOrder);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Netto-I.
		@param NettoI 
		The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI)
	{
		set_Value (COLUMNNAME_NettoI, NettoI);
	}

	/** Get Netto-I.
		@return The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Netto-II.
		@param NettoII 
		The nett weight of the second group calculation
	  */
	public void setNettoII (BigDecimal NettoII)
	{
		set_Value (COLUMNNAME_NettoII, NettoII);
	}

	/** Get Netto-II.
		@return The nett weight of the second group calculation
	  */
	public BigDecimal getNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Netto-II Target.
		@param NettoIITarget 
		The target nett weight of the second group calculation
	  */
	public void setNettoIITarget (BigDecimal NettoIITarget)
	{
		set_ValueNoCheck (COLUMNNAME_NettoIITarget, NettoIITarget);
	}

	/** Get Netto-II Target.
		@return The target nett weight of the second group calculation
	  */
	public BigDecimal getNettoIITarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoIITarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Netto-I Target.
		@param NettoITarget 
		The target nett weight of the first group calculation
	  */
	public void setNettoITarget (BigDecimal NettoITarget)
	{
		set_ValueNoCheck (COLUMNNAME_NettoITarget, NettoITarget);
	}

	/** Get Netto-I Target.
		@return The target nett weight of the first group calculation
	  */
	public BigDecimal getNettoITarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoITarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Netto-I.
		@param OriginalNettoI 
		The nett weight of the first group calculation
	  */
	public void setOriginalNettoI (BigDecimal OriginalNettoI)
	{
		set_ValueNoCheck (COLUMNNAME_OriginalNettoI, OriginalNettoI);
	}

	/** Get Original Netto-I.
		@return The nett weight of the first group calculation
	  */
	public BigDecimal getOriginalNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Netto-II.
		@param OriginalNettoII 
		The nett weight of the second group calculation
	  */
	public void setOriginalNettoII (BigDecimal OriginalNettoII)
	{
		set_ValueNoCheck (COLUMNNAME_OriginalNettoII, OriginalNettoII);
	}

	/** Get Original Netto-II.
		@return The nett weight of the second group calculation
	  */
	public BigDecimal getOriginalNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Reflection.
		@param OriginalReflection 
		The deduction to the gross weight
	  */
	public void setOriginalReflection (BigDecimal OriginalReflection)
	{
		set_ValueNoCheck (COLUMNNAME_OriginalReflection, OriginalReflection);
	}

	/** Get Original Reflection.
		@return The deduction to the gross weight
	  */
	public BigDecimal getOriginalReflection () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalReflection);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Qty Claimed.
		@param QtyClaimed Qty Claimed	  */
	public void setQtyClaimed (BigDecimal QtyClaimed)
	{
		set_Value (COLUMNNAME_QtyClaimed, QtyClaimed);
	}

	/** Get Qty Claimed.
		@return Qty Claimed	  */
	public BigDecimal getQtyClaimed () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyClaimed);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reflection.
		@param Reflection 
		The deduction to the gross weight
	  */
	public void setReflection (BigDecimal Reflection)
	{
		set_Value (COLUMNNAME_Reflection, Reflection);
	}

	/** Get Reflection.
		@return The deduction to the gross weight
	  */
	public BigDecimal getReflection () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Reflection);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reflection Target.
		@param ReflectionTarget 
		The target deduction to the gross weight
	  */
	public void setReflectionTarget (BigDecimal ReflectionTarget)
	{
		set_ValueNoCheck (COLUMNNAME_ReflectionTarget, ReflectionTarget);
	}

	/** Get Reflection Target.
		@return The target deduction to the gross weight
	  */
	public BigDecimal getReflectionTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReflectionTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Order getSplittedOrder() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getSplittedOrder_ID(), get_TrxName());	}

	/** Set Splitted Order.
		@param SplittedOrder_ID 
		The other order (ticket) by which this ticket is splitted to
	  */
	public void setSplittedOrder_ID (int SplittedOrder_ID)
	{
		if (SplittedOrder_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_SplittedOrder_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_SplittedOrder_ID, Integer.valueOf(SplittedOrder_ID));
	}

	/** Get Splitted Order.
		@return The other order (ticket) by which this ticket is splitted to
	  */
	public int getSplittedOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SplittedOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getSplittedTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getSplittedTicket_ID(), get_TrxName());	}

	/** Set Splitted Ticket.
		@param SplittedTicket_ID 
		The ticket by which this ticket is splitted to
	  */
	public void setSplittedTicket_ID (int SplittedTicket_ID)
	{
		if (SplittedTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_SplittedTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_SplittedTicket_ID, Integer.valueOf(SplittedTicket_ID));
	}

	/** Get Splitted Ticket.
		@return The ticket by which this ticket is splitted to
	  */
	public int getSplittedTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SplittedTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Tare.
		@param Tare 
		The weight as comparison
	  */
	public void setTare (BigDecimal Tare)
	{
		set_Value (COLUMNNAME_Tare, Tare);
	}

	/** Get Tare.
		@return The weight as comparison
	  */
	public BigDecimal getTare () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Tare);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Tare Target.
		@param TareTarget 
		The tare weight target as comparison
	  */
	public void setTareTarget (BigDecimal TareTarget)
	{
		set_ValueNoCheck (COLUMNNAME_TareTarget, TareTarget);
	}

	/** Get Tare Target.
		@return The tare weight target as comparison
	  */
	public BigDecimal getTareTarget () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TareTarget);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Time In.
		@param TimeIn 
		The date and time of when it was in
	  */
	public void setTimeIn (Timestamp TimeIn)
	{
		set_Value (COLUMNNAME_TimeIn, TimeIn);
	}

	/** Get Time In.
		@return The date and time of when it was in
	  */
	public Timestamp getTimeIn () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TimeIn);
	}

	/** Set Time Out.
		@param TimeOut 
		The date and time of when it was out
	  */
	public void setTimeOut (Timestamp TimeOut)
	{
		set_Value (COLUMNNAME_TimeOut, TimeOut);
	}

	/** Get Time Out.
		@return The date and time of when it was out
	  */
	public Timestamp getTimeOut () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TimeOut);
	}

	public org.compiere.model.I_C_BPartner getTransporter() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getTransporter_ID(), get_TrxName());	}

	/** Set Transporter.
		@param Transporter_ID 
		The transporter
	  */
	public void setTransporter_ID (int Transporter_ID)
	{
		if (Transporter_ID < 1) 
			set_Value (COLUMNNAME_Transporter_ID, null);
		else 
			set_Value (COLUMNNAME_Transporter_ID, Integer.valueOf(Transporter_ID));
	}

	/** Get Transporter.
		@return The transporter
	  */
	public int getTransporter_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Transporter_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Transporter Name.
		@param TransporterName 
		The name of transporter
	  */
	public void setTransporterName (String TransporterName)
	{
		set_Value (COLUMNNAME_TransporterName, TransporterName);
	}

	/** Get Transporter Name.
		@return The name of transporter
	  */
	public String getTransporterName () 
	{
		return (String)get_Value(COLUMNNAME_TransporterName);
	}

	/** Set Weighbridge Ticket Confirmation.
		@param UNS_WBTicket_Confirm_ID Weighbridge Ticket Confirmation	  */
	public void setUNS_WBTicket_Confirm_ID (int UNS_WBTicket_Confirm_ID)
	{
		if (UNS_WBTicket_Confirm_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WBTicket_Confirm_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WBTicket_Confirm_ID, Integer.valueOf(UNS_WBTicket_Confirm_ID));
	}

	/** Get Weighbridge Ticket Confirmation.
		@return Weighbridge Ticket Confirmation	  */
	public int getUNS_WBTicket_Confirm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WBTicket_Confirm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_WBTicket_Confirm_UU.
		@param UNS_WBTicket_Confirm_UU UNS_WBTicket_Confirm_UU	  */
	public void setUNS_WBTicket_Confirm_UU (String UNS_WBTicket_Confirm_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_WBTicket_Confirm_UU, UNS_WBTicket_Confirm_UU);
	}

	/** Get UNS_WBTicket_Confirm_UU.
		@return UNS_WBTicket_Confirm_UU	  */
	public String getUNS_WBTicket_Confirm_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_WBTicket_Confirm_UU);
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getUNS_WeighbridgeTicket_ID(), get_TrxName());	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Vehicle Driver Name.
		@param VehicleDriverName 
		The person name of vehicle by whom it is drived
	  */
	public void setVehicleDriverName (String VehicleDriverName)
	{
		set_Value (COLUMNNAME_VehicleDriverName, VehicleDriverName);
	}

	/** Get Vehicle Driver Name.
		@return The person name of vehicle by whom it is drived
	  */
	public String getVehicleDriverName () 
	{
		return (String)get_Value(COLUMNNAME_VehicleDriverName);
	}

	/** Set Vehicle No.
		@param VehicleNo 
		The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo)
	{
		set_Value (COLUMNNAME_VehicleNo, VehicleNo);
	}

	/** Get Vehicle No.
		@return The identification number of the vehicle being weighted
	  */
	public String getVehicleNo () 
	{
		return (String)get_Value(COLUMNNAME_VehicleNo);
	}

	/** Vesel Truck = BAKT */
	public static final String VEHICLETYPE_VeselTruck = "BAKT";
	/** Dump Truck = DUMP */
	public static final String VEHICLETYPE_DumpTruck = "DUMP";
	/** Tank Truck = TANK */
	public static final String VEHICLETYPE_TankTruck = "TANK";
	/** Unknown = UNKN */
	public static final String VEHICLETYPE_Unknown = "UNKN";
	/** Set Vehicle Type.
		@param VehicleType 
		The type of vehicle
	  */
	public void setVehicleType (String VehicleType)
	{

		set_Value (COLUMNNAME_VehicleType, VehicleType);
	}

	/** Get Vehicle Type.
		@return The type of vehicle
	  */
	public String getVehicleType () 
	{
		return (String)get_Value(COLUMNNAME_VehicleType);
	}
}