/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Order_Contract
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Order_Contract extends PO implements I_UNS_Order_Contract, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170521L;

    /** Standard Constructor */
    public X_UNS_Order_Contract (Properties ctx, int UNS_Order_Contract_ID, String trxName)
    {
      super (ctx, UNS_Order_Contract_ID, trxName);
      /** if (UNS_Order_Contract_ID == 0)
        {
			setAccountNo (null);
			setBacklogCompulsoryDeduction (Env.ZERO);
// 0
			setBankAddress (null);
			setBeneficiary (null);
			setC_Order_ID (0);
			setDeliveryTerm (null);
			setFreshCompulsoryDeduction (Env.ZERO);
// 0
			setInvoiceFlatDeduction (Env.ZERO);
// 0
			setIsMandatoryWithholding (true);
// Y
			setQualityAgreement (null);
			setQualityTerm (null);
			setRottenBunchDeduction (Env.ZERO);
// 0
			setUNS_Order_Contract_ID (0);
			setWeighCalcBased (null);
// BUY
        } */
    }

    /** Load Constructor */
    public X_UNS_Order_Contract (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Order_Contract[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Account No.
		@param AccountNo 
		Account Number
	  */
	public void setAccountNo (String AccountNo)
	{
		set_Value (COLUMNNAME_AccountNo, AccountNo);
	}

	/** Get Account No.
		@return Account Number
	  */
	public String getAccountNo () 
	{
		return (String)get_Value(COLUMNNAME_AccountNo);
	}

	/** Set Account No-2.
		@param AccountNo2 
		The second bank account number in same bank with different currency with Account No-1
	  */
	public void setAccountNo2 (String AccountNo2)
	{
		set_Value (COLUMNNAME_AccountNo2, AccountNo2);
	}

	/** Get Account No-2.
		@return The second bank account number in same bank with different currency with Account No-1
	  */
	public String getAccountNo2 () 
	{
		return (String)get_Value(COLUMNNAME_AccountNo2);
	}

	/** Set Backlog Compulsory.
		@param BacklogCompulsoryDeduction 
		The compulsory deduction in % if goods receipt have (lot of) backlog/restan
	  */
	public void setBacklogCompulsoryDeduction (BigDecimal BacklogCompulsoryDeduction)
	{
		set_Value (COLUMNNAME_BacklogCompulsoryDeduction, BacklogCompulsoryDeduction);
	}

	/** Get Backlog Compulsory.
		@return The compulsory deduction in % if goods receipt have (lot of) backlog/restan
	  */
	public BigDecimal getBacklogCompulsoryDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BacklogCompulsoryDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Bank Address.
		@param BankAddress 
		The address of the bank of which the bank account registered
	  */
	public void setBankAddress (String BankAddress)
	{
		set_Value (COLUMNNAME_BankAddress, BankAddress);
	}

	/** Get Bank Address.
		@return The address of the bank of which the bank account registered
	  */
	public String getBankAddress () 
	{
		return (String)get_Value(COLUMNNAME_BankAddress);
	}

	/** Set Beneficiary.
		@param Beneficiary 
		Business Partner to whom payment is made
	  */
	public void setBeneficiary (String Beneficiary)
	{
		set_Value (COLUMNNAME_Beneficiary, Beneficiary);
	}

	/** Get Beneficiary.
		@return Business Partner to whom payment is made
	  */
	public String getBeneficiary () 
	{
		return (String)get_Value(COLUMNNAME_Beneficiary);
	}

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException
    {
		return (org.compiere.model.I_C_BankAccount)MTable.get(getCtx(), org.compiere.model.I_C_BankAccount.Table_Name)
			.getPO(getC_BankAccount_ID(), get_TrxName());	}

	/** Set Cash / Bank Account.
		@param C_BankAccount_ID 
		Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID)
	{
		if (C_BankAccount_ID < 1) 
			set_Value (COLUMNNAME_C_BankAccount_ID, null);
		else 
			set_Value (COLUMNNAME_C_BankAccount_ID, Integer.valueOf(C_BankAccount_ID));
	}

	/** Get Cash / Bank Account.
		@return Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BankAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Bank getC_Bank() throws RuntimeException
    {
		return (org.compiere.model.I_C_Bank)MTable.get(getCtx(), org.compiere.model.I_C_Bank.Table_Name)
			.getPO(getC_Bank_ID(), get_TrxName());	}

	/** Set Bank.
		@param C_Bank_ID 
		Bank
	  */
	public void setC_Bank_ID (int C_Bank_ID)
	{
		if (C_Bank_ID < 1) 
			set_Value (COLUMNNAME_C_Bank_ID, null);
		else 
			set_Value (COLUMNNAME_C_Bank_ID, Integer.valueOf(C_Bank_ID));
	}

	/** Get Bank.
		@return Bank
	  */
	public int getC_Bank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Bank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BP_BankAccount getC_BP_BankAccount() throws RuntimeException
    {
		return (org.compiere.model.I_C_BP_BankAccount)MTable.get(getCtx(), org.compiere.model.I_C_BP_BankAccount.Table_Name)
			.getPO(getC_BP_BankAccount_ID(), get_TrxName());	}

	/** Set Partner Bank Account.
		@param C_BP_BankAccount_ID 
		Bank Account of the Business Partner
	  */
	public void setC_BP_BankAccount_ID (int C_BP_BankAccount_ID)
	{
		if (C_BP_BankAccount_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BP_BankAccount_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BP_BankAccount_ID, Integer.valueOf(C_BP_BankAccount_ID));
	}

	/** Get Partner Bank Account.
		@return Bank Account of the Business Partner
	  */
	public int getC_BP_BankAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BP_BankAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency2() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency2_ID(), get_TrxName());	}

	/** Set Currency 2.
		@param C_Currency2_ID 
		The Currency for this second record
	  */
	public void setC_Currency2_ID (int C_Currency2_ID)
	{
		if (C_Currency2_ID < 1) 
			set_Value (COLUMNNAME_C_Currency2_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency2_ID, Integer.valueOf(C_Currency2_ID));
	}

	/** Get Currency 2.
		@return The Currency for this second record
	  */
	public int getC_Currency2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** FOB-Free On Board = FOB */
	public static final String DELIVERYTERM_FOB_FreeOnBoard = "FOB";
	/** FOD-Free On Board Destination = FOD */
	public static final String DELIVERYTERM_FOD_FreeOnBoardDestination = "FOD";
	/** FOL-Franco Location = FOL */
	public static final String DELIVERYTERM_FOL_FrancoLocation = "FOL";
	/** LCL-Loco Location = LCL */
	public static final String DELIVERYTERM_LCL_LocoLocation = "LCL";
	/** Set Delivery Term.
		@param DeliveryTerm 
		The term of delivery that indicates how the goods to be delivered to buyer. This is to follow the international term of delivery (FOB, FOD, etc.)
	  */
	public void setDeliveryTerm (String DeliveryTerm)
	{

		set_Value (COLUMNNAME_DeliveryTerm, DeliveryTerm);
	}

	/** Get Delivery Term.
		@return The term of delivery that indicates how the goods to be delivered to buyer. This is to follow the international term of delivery (FOB, FOD, etc.)
	  */
	public String getDeliveryTerm () 
	{
		return (String)get_Value(COLUMNNAME_DeliveryTerm);
	}

	/** Set Fresh Compulsory.
		@param FreshCompulsoryDeduction 
		The compulsory deduction in % if goods receipt is (mostly) fresh
	  */
	public void setFreshCompulsoryDeduction (BigDecimal FreshCompulsoryDeduction)
	{
		set_Value (COLUMNNAME_FreshCompulsoryDeduction, FreshCompulsoryDeduction);
	}

	/** Get Fresh Compulsory.
		@return The compulsory deduction in % if goods receipt is (mostly) fresh
	  */
	public BigDecimal getFreshCompulsoryDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FreshCompulsoryDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Invoice Flat Deduction.
		@param InvoiceFlatDeduction Invoice Flat Deduction	  */
	public void setInvoiceFlatDeduction (BigDecimal InvoiceFlatDeduction)
	{
		set_Value (COLUMNNAME_InvoiceFlatDeduction, InvoiceFlatDeduction);
	}

	/** Get Invoice Flat Deduction.
		@return Invoice Flat Deduction	  */
	public BigDecimal getInvoiceFlatDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_InvoiceFlatDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Mandatory Withholding.
		@param IsMandatoryWithholding 
		Monies must be withheld
	  */
	public void setIsMandatoryWithholding (boolean IsMandatoryWithholding)
	{
		set_Value (COLUMNNAME_IsMandatoryWithholding, Boolean.valueOf(IsMandatoryWithholding));
	}

	/** Get Mandatory Withholding.
		@return Monies must be withheld
	  */
	public boolean isMandatoryWithholding () 
	{
		Object oo = get_Value(COLUMNNAME_IsMandatoryWithholding);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Notes.
		@param Notes Notes	  */
	public void setNotes (String Notes)
	{
		set_Value (COLUMNNAME_Notes, Notes);
	}

	/** Get Notes.
		@return Notes	  */
	public String getNotes () 
	{
		return (String)get_Value(COLUMNNAME_Notes);
	}

	/** Set PPh After Unloading Cost.
		@param PPhAfterUnloadingCost PPh After Unloading Cost	  */
	public void setPPhAfterUnloadingCost (boolean PPhAfterUnloadingCost)
	{
		set_Value (COLUMNNAME_PPhAfterUnloadingCost, Boolean.valueOf(PPhAfterUnloadingCost));
	}

	/** Get PPh After Unloading Cost.
		@return PPh After Unloading Cost	  */
	public boolean isPPhAfterUnloadingCost () 
	{
		Object oo = get_Value(COLUMNNAME_PPhAfterUnloadingCost);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Quality Agreement.
		@param QualityAgreement 
		The aggreement for quality to deliver
	  */
	public void setQualityAgreement (String QualityAgreement)
	{
		set_Value (COLUMNNAME_QualityAgreement, QualityAgreement);
	}

	/** Get Quality Agreement.
		@return The aggreement for quality to deliver
	  */
	public String getQualityAgreement () 
	{
		return (String)get_Value(COLUMNNAME_QualityAgreement);
	}

	/** Set Quality Term.
		@param QualityTerm 
		The term of quality to be delivered/receipt.
	  */
	public void setQualityTerm (String QualityTerm)
	{
		set_Value (COLUMNNAME_QualityTerm, QualityTerm);
	}

	/** Get Quality Term.
		@return The term of quality to be delivered/receipt.
	  */
	public String getQualityTerm () 
	{
		return (String)get_Value(COLUMNNAME_QualityTerm);
	}

	/** Set Rotten Bunch Deduction (%).
		@param RottenBunchDeduction Rotten Bunch Deduction (%)	  */
	public void setRottenBunchDeduction (BigDecimal RottenBunchDeduction)
	{
		set_Value (COLUMNNAME_RottenBunchDeduction, RottenBunchDeduction);
	}

	/** Get Rotten Bunch Deduction (%).
		@return Rotten Bunch Deduction (%)	  */
	public BigDecimal getRottenBunchDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RottenBunchDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Swift code.
		@param SwiftCode 
		Swift Code or BIC
	  */
	public void setSwiftCode (String SwiftCode)
	{
		set_Value (COLUMNNAME_SwiftCode, SwiftCode);
	}

	/** Get Swift code.
		@return Swift Code or BIC
	  */
	public String getSwiftCode () 
	{
		return (String)get_Value(COLUMNNAME_SwiftCode);
	}

	/** Set Additional Contract Order.
		@param UNS_Order_Contract_ID Additional Contract Order	  */
	public void setUNS_Order_Contract_ID (int UNS_Order_Contract_ID)
	{
		if (UNS_Order_Contract_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Order_Contract_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Order_Contract_ID, Integer.valueOf(UNS_Order_Contract_ID));
	}

	/** Get Additional Contract Order.
		@return Additional Contract Order	  */
	public int getUNS_Order_Contract_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Order_Contract_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Order_Contract_UU.
		@param UNS_Order_Contract_UU UNS_Order_Contract_UU	  */
	public void setUNS_Order_Contract_UU (String UNS_Order_Contract_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Order_Contract_UU, UNS_Order_Contract_UU);
	}

	/** Get UNS_Order_Contract_UU.
		@return UNS_Order_Contract_UU	  */
	public String getUNS_Order_Contract_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Order_Contract_UU);
	}

	/** Buyer = BUY */
	public static final String WEIGHCALCBASED_Buyer = "BUY";
	/** Seller = SEL */
	public static final String WEIGHCALCBASED_Seller = "SEL";
	/** Set Weigh Calc Based.
		@param WeighCalcBased 
		Whose weight calucation will be used, buyer or seller.
	  */
	public void setWeighCalcBased (String WeighCalcBased)
	{

		set_Value (COLUMNNAME_WeighCalcBased, WeighCalcBased);
	}

	/** Get Weigh Calc Based.
		@return Whose weight calucation will be used, buyer or seller.
	  */
	public String getWeighCalcBased () 
	{
		return (String)get_Value(COLUMNNAME_WeighCalcBased);
	}
}