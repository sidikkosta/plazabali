/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.Query;

/**
 * @author ALBURHANY
 *
 */
public class MUNSCustomerPointLine extends X_UNS_CustomerPoint_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 223013700303074186L;

	/**
	 * @param ctx
	 * @param UNS_CustomerPoint_Line_ID
	 * @param trxName
	 */
	public MUNSCustomerPointLine(Properties ctx, int UNS_CustomerPoint_Line_ID,
			String trxName) {
		super(ctx, UNS_CustomerPoint_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCustomerPointLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		return true;
	}
	
	public static MUNSCustomerPointLine getByInvoiceLine(Properties ctx, int C_InvoiceLine_ID, String trxName)
	{
		MUNSCustomerPointLine cpLine = new Query(ctx, MUNSCustomerPointLine.Table_Name,
											COLUMNNAME_C_InvoiceLine_ID + "=?", trxName)
												.setParameters(C_InvoiceLine_ID).first();
		return cpLine;
	}
}
