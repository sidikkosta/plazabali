/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_VoucherCorrection
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_VoucherCorrection extends PO implements I_UNS_VoucherCorrection, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190415L;

    /** Standard Constructor */
    public X_UNS_VoucherCorrection (Properties ctx, int UNS_VoucherCorrection_ID, String trxName)
    {
      super (ctx, UNS_VoucherCorrection_ID, trxName);
      /** if (UNS_VoucherCorrection_ID == 0)
        {
			setAllocatedAmt (Env.ZERO);
// 0
			setC_DocType_ID (0);
// @SQL=SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType = 'VCC'
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDateTrx (new Timestamp( System.currentTimeMillis() ));
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setIsApproved (false);
// N
			setPosted (false);
// N
			setProcessed (false);
// N
			setStore_ID (0);
			setTrxAmt (Env.ZERO);
// 0
			setUNS_PaymentTrx_ID (0);
			setUNS_POS_Session_ID (0);
			setUNS_POSTrx_ID (0);
			setUNS_VoucherCode_ID (0);
			setUNS_VoucherCorrection_ID (0);
			setUnusedAmt (Env.ZERO);
// 0
			setUsedAmt (Env.ZERO);
// 0
			setVoucherAmt (Env.ZERO);
// 0
			setVoucherCode (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_VoucherCorrection (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_VoucherCorrection[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Allocated Amountt.
		@param AllocatedAmt 
		Amount allocated to this document
	  */
	public void setAllocatedAmt (BigDecimal AllocatedAmt)
	{
		set_Value (COLUMNNAME_AllocatedAmt, AllocatedAmt);
	}

	/** Get Allocated Amountt.
		@return Amount allocated to this document
	  */
	public BigDecimal getAllocatedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AllocatedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_ValueNoCheck (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_Value (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Posted.
		@param Posted 
		Posting status
	  */
	public void setPosted (boolean Posted)
	{
		set_Value (COLUMNNAME_Posted, Boolean.valueOf(Posted));
	}

	/** Get Posted.
		@return Posting status
	  */
	public boolean isPosted () 
	{
		Object oo = get_Value(COLUMNNAME_Posted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.unicore.model.I_UNS_VoucherCorrection getReversal() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_VoucherCorrection)MTable.get(getCtx(), com.unicore.model.I_UNS_VoucherCorrection.Table_Name)
			.getPO(getReversal_ID(), get_TrxName());	}

	/** Set Reversal ID.
		@param Reversal_ID 
		ID of document reversal
	  */
	public void setReversal_ID (int Reversal_ID)
	{
		if (Reversal_ID < 1) 
			set_Value (COLUMNNAME_Reversal_ID, null);
		else 
			set_Value (COLUMNNAME_Reversal_ID, Integer.valueOf(Reversal_ID));
	}

	/** Get Reversal ID.
		@return ID of document reversal
	  */
	public int getReversal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Reversal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_Value (COLUMNNAME_Store_ID, null);
		else 
			set_Value (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getStore_ID()));
    }

	/** Set Transaction Amount.
		@param TrxAmt 
		Amount of a transaction
	  */
	public void setTrxAmt (BigDecimal TrxAmt)
	{
		set_Value (COLUMNNAME_TrxAmt, TrxAmt);
	}

	/** Get Transaction Amount.
		@return Amount of a transaction
	  */
	public BigDecimal getTrxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TrxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_PaymentTrx getUNS_PaymentTrx() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PaymentTrx)MTable.get(getCtx(), com.unicore.model.I_UNS_PaymentTrx.Table_Name)
			.getPO(getUNS_PaymentTrx_ID(), get_TrxName());	}

	/** Set Payment Transaction.
		@param UNS_PaymentTrx_ID Payment Transaction	  */
	public void setUNS_PaymentTrx_ID (int UNS_PaymentTrx_ID)
	{
		if (UNS_PaymentTrx_ID < 1) 
			set_Value (COLUMNNAME_UNS_PaymentTrx_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_PaymentTrx_ID, Integer.valueOf(UNS_PaymentTrx_ID));
	}

	/** Get Payment Transaction.
		@return Payment Transaction	  */
	public int getUNS_PaymentTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PaymentTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_Value (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POSTrx getUNS_POSTrx() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTrx)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTrx.Table_Name)
			.getPO(getUNS_POSTrx_ID(), get_TrxName());	}

	/** Set POS Transactions.
		@param UNS_POSTrx_ID POS Transactions	  */
	public void setUNS_POSTrx_ID (int UNS_POSTrx_ID)
	{
		if (UNS_POSTrx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_ID, Integer.valueOf(UNS_POSTrx_ID));
	}

	/** Get POS Transactions.
		@return POS Transactions	  */
	public int getUNS_POSTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_VoucherCode getUNS_VoucherCode() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_VoucherCode)MTable.get(getCtx(), com.unicore.model.I_UNS_VoucherCode.Table_Name)
			.getPO(getUNS_VoucherCode_ID(), get_TrxName());	}

	/** Set Voucher Code.
		@param UNS_VoucherCode_ID Voucher Code	  */
	public void setUNS_VoucherCode_ID (int UNS_VoucherCode_ID)
	{
		if (UNS_VoucherCode_ID < 1) 
			set_Value (COLUMNNAME_UNS_VoucherCode_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_VoucherCode_ID, Integer.valueOf(UNS_VoucherCode_ID));
	}

	/** Get Voucher Code.
		@return Voucher Code	  */
	public int getUNS_VoucherCode_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_VoucherCode_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Voucher Correction.
		@param UNS_VoucherCorrection_ID Voucher Correction	  */
	public void setUNS_VoucherCorrection_ID (int UNS_VoucherCorrection_ID)
	{
		if (UNS_VoucherCorrection_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_VoucherCorrection_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_VoucherCorrection_ID, Integer.valueOf(UNS_VoucherCorrection_ID));
	}

	/** Get Voucher Correction.
		@return Voucher Correction	  */
	public int getUNS_VoucherCorrection_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_VoucherCorrection_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_VoucherCorrection_UU.
		@param UNS_VoucherCorrection_UU UNS_VoucherCorrection_UU	  */
	public void setUNS_VoucherCorrection_UU (String UNS_VoucherCorrection_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_VoucherCorrection_UU, UNS_VoucherCorrection_UU);
	}

	/** Get UNS_VoucherCorrection_UU.
		@return UNS_VoucherCorrection_UU	  */
	public String getUNS_VoucherCorrection_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_VoucherCorrection_UU);
	}

	/** Set Unused Amt.
		@param UnusedAmt Unused Amt	  */
	public void setUnusedAmt (BigDecimal UnusedAmt)
	{
		set_Value (COLUMNNAME_UnusedAmt, UnusedAmt);
	}

	/** Get Unused Amt.
		@return Unused Amt	  */
	public BigDecimal getUnusedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UnusedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Used Amount.
		@param UsedAmt Used Amount	  */
	public void setUsedAmt (BigDecimal UsedAmt)
	{
		set_Value (COLUMNNAME_UsedAmt, UsedAmt);
	}

	/** Get Used Amount.
		@return Used Amount	  */
	public BigDecimal getUsedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UsedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Voucher Amount.
		@param VoucherAmt Voucher Amount	  */
	public void setVoucherAmt (BigDecimal VoucherAmt)
	{
		set_Value (COLUMNNAME_VoucherAmt, VoucherAmt);
	}

	/** Get Voucher Amount.
		@return Voucher Amount	  */
	public BigDecimal getVoucherAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VoucherAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Voucher Code.
		@param VoucherCode 
		Voucher Code
	  */
	public void setVoucherCode (String VoucherCode)
	{
		set_Value (COLUMNNAME_VoucherCode, VoucherCode);
	}

	/** Get Voucher Code.
		@return Voucher Code
	  */
	public String getVoucherCode () 
	{
		return (String)get_Value(COLUMNNAME_VoucherCode);
	}
}