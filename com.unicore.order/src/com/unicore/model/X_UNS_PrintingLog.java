/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_PrintingLog
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PrintingLog extends PO implements I_UNS_PrintingLog, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20151017L;

    /** Standard Constructor */
    public X_UNS_PrintingLog (Properties ctx, int UNS_PrintingLog_ID, String trxName)
    {
      super (ctx, UNS_PrintingLog_ID, trxName);
      /** if (UNS_PrintingLog_ID == 0)
        {
			setUNS_PrintingLog_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PrintingLog (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 2 - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PrintingLog[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Copy Of Print.
		@param CopyOfPrint Copy Of Print	  */
	public void setCopyOfPrint (int CopyOfPrint)
	{
		set_Value (COLUMNNAME_CopyOfPrint, Integer.valueOf(CopyOfPrint));
	}

	/** Get Copy Of Print.
		@return Copy Of Print	  */
	public int getCopyOfPrint () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CopyOfPrint);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date printed.
		@param DatePrinted 
		Date the document was printed.
	  */
	public void setDatePrinted (Timestamp DatePrinted)
	{
		set_ValueNoCheck (COLUMNNAME_DatePrinted, DatePrinted);
	}

	/** Get Date printed.
		@return Date the document was printed.
	  */
	public Timestamp getDatePrinted () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DatePrinted);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set PrintedBy.
		@param PrintedBy PrintedBy	  */
	public void setPrintedBy (int PrintedBy)
	{
		set_Value (COLUMNNAME_PrintedBy, Integer.valueOf(PrintedBy));
	}

	/** Get PrintedBy.
		@return PrintedBy	  */
	public int getPrintedBy () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PrintedBy);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Record ID.
		@param Record_ID 
		Direct internal record ID
	  */
	public void setRecord_ID (int Record_ID)
	{
		if (Record_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_Record_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Record_ID, Integer.valueOf(Record_ID));
	}

	/** Get Record ID.
		@return Direct internal record ID
	  */
	public int getRecord_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Record_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Printing Log.
		@param UNS_PrintingLog_ID Printing Log	  */
	public void setUNS_PrintingLog_ID (int UNS_PrintingLog_ID)
	{
		if (UNS_PrintingLog_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PrintingLog_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PrintingLog_ID, Integer.valueOf(UNS_PrintingLog_ID));
	}

	/** Get Printing Log.
		@return Printing Log	  */
	public int getUNS_PrintingLog_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PrintingLog_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PrintingLog_UU.
		@param UNS_PrintingLog_UU UNS_PrintingLog_UU	  */
	public void setUNS_PrintingLog_UU (String UNS_PrintingLog_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PrintingLog_UU, UNS_PrintingLog_UU);
	}

	/** Get UNS_PrintingLog_UU.
		@return UNS_PrintingLog_UU	  */
	public String getUNS_PrintingLog_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PrintingLog_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}