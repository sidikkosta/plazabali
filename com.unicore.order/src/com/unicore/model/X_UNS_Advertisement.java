/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_Advertisement
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Advertisement extends PO implements I_UNS_Advertisement, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180828L;

    /** Standard Constructor */
    public X_UNS_Advertisement (Properties ctx, int UNS_Advertisement_ID, String trxName)
    {
      super (ctx, UNS_Advertisement_ID, trxName);
      /** if (UNS_Advertisement_ID == 0)
        {
			setName (null);
			setUNS_Advertisement_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Advertisement (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Advertisement[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Display Time.
		@param DisplayTime Display Time	  */
	public void setDisplayTime (int DisplayTime)
	{
		set_Value (COLUMNNAME_DisplayTime, Integer.valueOf(DisplayTime));
	}

	/** Get Display Time.
		@return Display Time	  */
	public int getDisplayTime () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DisplayTime);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_Value (COLUMNNAME_Store_ID, null);
		else 
			set_Value (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Advertisement.
		@param UNS_Advertisement_ID Advertisement	  */
	public void setUNS_Advertisement_ID (int UNS_Advertisement_ID)
	{
		if (UNS_Advertisement_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Advertisement_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Advertisement_ID, Integer.valueOf(UNS_Advertisement_ID));
	}

	/** Get Advertisement.
		@return Advertisement	  */
	public int getUNS_Advertisement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Advertisement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Advertisement_UU.
		@param UNS_Advertisement_UU UNS_Advertisement_UU	  */
	public void setUNS_Advertisement_UU (String UNS_Advertisement_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Advertisement_UU, UNS_Advertisement_UU);
	}

	/** Get UNS_Advertisement_UU.
		@return UNS_Advertisement_UU	  */
	public String getUNS_Advertisement_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Advertisement_UU);
	}
}