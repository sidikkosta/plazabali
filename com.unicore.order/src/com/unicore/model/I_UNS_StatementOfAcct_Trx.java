/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_StatementOfAcct_Trx
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_StatementOfAcct_Trx 
{

    /** TableName=UNS_StatementOfAcct_Trx */
    public static final String Table_Name = "UNS_StatementOfAcct_Trx";

    /** AD_Table_ID=1000506 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name PriceActual */
    public static final String COLUMNNAME_PriceActual = "PriceActual";

	/** Set Unit Price.
	  * Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual);

	/** Get Unit Price.
	  * Actual Price
	  */
	public BigDecimal getPriceActual();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name Store_ID */
    public static final String COLUMNNAME_Store_ID = "Store_ID";

	/** Set Store	  */
	public void setStore_ID (int Store_ID);

	/** Get Store	  */
	public int getStore_ID();

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException;

    /** Column name TotalLines */
    public static final String COLUMNNAME_TotalLines = "TotalLines";

	/** Set Total Lines.
	  * Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines);

	/** Get Total Lines.
	  * Total of all document lines
	  */
	public BigDecimal getTotalLines();

    /** Column name UNS_StatementOfAcct_Line_ID */
    public static final String COLUMNNAME_UNS_StatementOfAcct_Line_ID = "UNS_StatementOfAcct_Line_ID";

	/** Set Statement Of Account Lines	  */
	public void setUNS_StatementOfAcct_Line_ID (int UNS_StatementOfAcct_Line_ID);

	/** Get Statement Of Account Lines	  */
	public int getUNS_StatementOfAcct_Line_ID();

	public com.unicore.model.I_UNS_StatementOfAcct_Line getUNS_StatementOfAcct_Line() throws RuntimeException;

    /** Column name UNS_StatementOfAcct_Trx_ID */
    public static final String COLUMNNAME_UNS_StatementOfAcct_Trx_ID = "UNS_StatementOfAcct_Trx_ID";

	/** Set Statement Of Account (Transaction)	  */
	public void setUNS_StatementOfAcct_Trx_ID (int UNS_StatementOfAcct_Trx_ID);

	/** Get Statement Of Account (Transaction)	  */
	public int getUNS_StatementOfAcct_Trx_ID();

    /** Column name UNS_StatementOfAcct_Trx_UU */
    public static final String COLUMNNAME_UNS_StatementOfAcct_Trx_UU = "UNS_StatementOfAcct_Trx_UU";

	/** Set UNS_StatementOfAcct_Trx_UU	  */
	public void setUNS_StatementOfAcct_Trx_UU (String UNS_StatementOfAcct_Trx_UU);

	/** Get UNS_StatementOfAcct_Trx_UU	  */
	public String getUNS_StatementOfAcct_Trx_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
