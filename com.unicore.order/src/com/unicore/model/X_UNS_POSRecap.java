/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POSRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSRecap extends PO implements I_UNS_POSRecap, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180927L;

    /** Standard Constructor */
    public X_UNS_POSRecap (Properties ctx, int UNS_POSRecap_ID, String trxName)
    {
      super (ctx, UNS_POSRecap_ID, trxName);
      /** if (UNS_POSRecap_ID == 0)
        {
			setDateTrx (new Timestamp( System.currentTimeMillis() ));
			setReturnAmt (Env.ZERO);
// 0
			setStore_ID (0);
			setTotalCostAmt (Env.ZERO);
// 0
			setTotalDiscAmt (Env.ZERO);
// 0
			setTotalEstimationShortCashierAmt (Env.ZERO);
// 0
			setTotalRefundTaxAmt (Env.ZERO);
// 0
			setTotalRevenueAmt (Env.ZERO);
// 0
			setTotalRoundingAmt (Env.ZERO);
// 0
			setTotalSalesAmt (Env.ZERO);
// 0
			setTotalServiceChargeAmt (Env.ZERO);
// 0
			setTotalShortCashierAmt (Env.ZERO);
// 0
			setTotalShortCashierAmt1 (Env.ZERO);
// 0
			setTotalTaxAmt (Env.ZERO);
// 0
			setTotalVoucherAmt (Env.ZERO);
// 0
			setUNS_POSRecap_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSRecap (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSRecap[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_Value (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Return Amount.
		@param ReturnAmt Return Amount	  */
	public void setReturnAmt (BigDecimal ReturnAmt)
	{
		set_Value (COLUMNNAME_ReturnAmt, ReturnAmt);
	}

	/** Get Return Amount.
		@return Return Amount	  */
	public BigDecimal getReturnAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_Value (COLUMNNAME_Store_ID, null);
		else 
			set_Value (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Cost Amount.
		@param TotalCostAmt Total Cost Amount	  */
	public void setTotalCostAmt (BigDecimal TotalCostAmt)
	{
		set_Value (COLUMNNAME_TotalCostAmt, TotalCostAmt);
	}

	/** Get Total Cost Amount.
		@return Total Cost Amount	  */
	public BigDecimal getTotalCostAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalCostAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Discount Amount.
		@param TotalDiscAmt Total Discount Amount	  */
	public void setTotalDiscAmt (BigDecimal TotalDiscAmt)
	{
		set_Value (COLUMNNAME_TotalDiscAmt, TotalDiscAmt);
	}

	/** Get Total Discount Amount.
		@return Total Discount Amount	  */
	public BigDecimal getTotalDiscAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalDiscAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Estimation Short Cashier Amount.
		@param TotalEstimationShortCashierAmt Total Estimation Short Cashier Amount	  */
	public void setTotalEstimationShortCashierAmt (BigDecimal TotalEstimationShortCashierAmt)
	{
		set_Value (COLUMNNAME_TotalEstimationShortCashierAmt, TotalEstimationShortCashierAmt);
	}

	/** Get Total Estimation Short Cashier Amount.
		@return Total Estimation Short Cashier Amount	  */
	public BigDecimal getTotalEstimationShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalEstimationShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Refund Tax Amount.
		@param TotalRefundTaxAmt Total Refund Tax Amount	  */
	public void setTotalRefundTaxAmt (BigDecimal TotalRefundTaxAmt)
	{
		set_Value (COLUMNNAME_TotalRefundTaxAmt, TotalRefundTaxAmt);
	}

	/** Get Total Refund Tax Amount.
		@return Total Refund Tax Amount	  */
	public BigDecimal getTotalRefundTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalRefundTaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Revenue Amount.
		@param TotalRevenueAmt Total Revenue Amount	  */
	public void setTotalRevenueAmt (BigDecimal TotalRevenueAmt)
	{
		set_Value (COLUMNNAME_TotalRevenueAmt, TotalRevenueAmt);
	}

	/** Get Total Revenue Amount.
		@return Total Revenue Amount	  */
	public BigDecimal getTotalRevenueAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalRevenueAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Rounding Amount.
		@param TotalRoundingAmt Total Rounding Amount	  */
	public void setTotalRoundingAmt (BigDecimal TotalRoundingAmt)
	{
		set_Value (COLUMNNAME_TotalRoundingAmt, TotalRoundingAmt);
	}

	/** Get Total Rounding Amount.
		@return Total Rounding Amount	  */
	public BigDecimal getTotalRoundingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalRoundingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Sales Amount.
		@param TotalSalesAmt Total Sales Amount	  */
	public void setTotalSalesAmt (BigDecimal TotalSalesAmt)
	{
		set_Value (COLUMNNAME_TotalSalesAmt, TotalSalesAmt);
	}

	/** Get Total Sales Amount.
		@return Total Sales Amount	  */
	public BigDecimal getTotalSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalSalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Service Charge Amount.
		@param TotalServiceChargeAmt Total Service Charge Amount	  */
	public void setTotalServiceChargeAmt (BigDecimal TotalServiceChargeAmt)
	{
		set_Value (COLUMNNAME_TotalServiceChargeAmt, TotalServiceChargeAmt);
	}

	/** Get Total Service Charge Amount.
		@return Total Service Charge Amount	  */
	public BigDecimal getTotalServiceChargeAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalServiceChargeAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Short Cashier Amount (+).
		@param TotalShortCashierAmt Total Short Cashier Amount (+)	  */
	public void setTotalShortCashierAmt (BigDecimal TotalShortCashierAmt)
	{
		set_Value (COLUMNNAME_TotalShortCashierAmt, TotalShortCashierAmt);
	}

	/** Get Total Short Cashier Amount (+).
		@return Total Short Cashier Amount (+)	  */
	public BigDecimal getTotalShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Short Cashier Amount (-).
		@param TotalShortCashierAmt1 Total Short Cashier Amount (-)	  */
	public void setTotalShortCashierAmt1 (BigDecimal TotalShortCashierAmt1)
	{
		set_Value (COLUMNNAME_TotalShortCashierAmt1, TotalShortCashierAmt1);
	}

	/** Get Total Short Cashier Amount (-).
		@return Total Short Cashier Amount (-)	  */
	public BigDecimal getTotalShortCashierAmt1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalShortCashierAmt1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Tax Amount.
		@param TotalTaxAmt Total Tax Amount	  */
	public void setTotalTaxAmt (BigDecimal TotalTaxAmt)
	{
		set_Value (COLUMNNAME_TotalTaxAmt, TotalTaxAmt);
	}

	/** Get Total Tax Amount.
		@return Total Tax Amount	  */
	public BigDecimal getTotalTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalTaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Voucher Amount.
		@param TotalVoucherAmt Total Voucher Amount	  */
	public void setTotalVoucherAmt (BigDecimal TotalVoucherAmt)
	{
		set_Value (COLUMNNAME_TotalVoucherAmt, TotalVoucherAmt);
	}

	/** Get Total Voucher Amount.
		@return Total Voucher Amount	  */
	public BigDecimal getTotalVoucherAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalVoucherAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set POS Recapitulation.
		@param UNS_POSRecap_ID POS Recapitulation	  */
	public void setUNS_POSRecap_ID (int UNS_POSRecap_ID)
	{
		if (UNS_POSRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_ID, Integer.valueOf(UNS_POSRecap_ID));
	}

	/** Get POS Recapitulation.
		@return POS Recapitulation	  */
	public int getUNS_POSRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSRecap_UU.
		@param UNS_POSRecap_UU UNS_POSRecap_UU	  */
	public void setUNS_POSRecap_UU (String UNS_POSRecap_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_UU, UNS_POSRecap_UU);
	}

	/** Get UNS_POSRecap_UU.
		@return UNS_POSRecap_UU	  */
	public String getUNS_POSRecap_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSRecap_UU);
	}

	public I_UNS_SalesReconciliation getUNS_SalesReconciliation() throws RuntimeException
    {
		return (I_UNS_SalesReconciliation)MTable.get(getCtx(), I_UNS_SalesReconciliation.Table_Name)
			.getPO(getUNS_SalesReconciliation_ID(), get_TrxName());	}

	/** Set Sales Reconciliation.
		@param UNS_SalesReconciliation_ID Sales Reconciliation	  */
	public void setUNS_SalesReconciliation_ID (int UNS_SalesReconciliation_ID)
	{
		if (UNS_SalesReconciliation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SalesReconciliation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SalesReconciliation_ID, Integer.valueOf(UNS_SalesReconciliation_ID));
	}

	/** Get Sales Reconciliation.
		@return Sales Reconciliation	  */
	public int getUNS_SalesReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SalesReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}