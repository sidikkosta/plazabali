/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MConversionRate;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSSessionEDCSummary extends X_UNS_SessionEDCSummary {

	/**
	 * 
	 */
	private static final long serialVersionUID = -220116000743859757L;
	private MUNSPOSSession m_parent = null;
	
	public MUNSPOSSession getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPOSSession(getCtx(), getUNS_POS_Session_ID(), get_TrxName());
		return m_parent;
	}
	
	public static MUNSSessionEDCSummary getCreate (String trxName, int sessionID, int edcID, int cardTypeID)
	{
		MUNSSessionEDCSummary edcSummary = get(trxName, sessionID, edcID, cardTypeID);
		if (edcSummary != null)
			return edcSummary;
		edcSummary = new MUNSSessionEDCSummary(Env.getCtx(), 0, trxName);
		String sql = "SELECT AD_Org_ID FROM UNS_POS_Session WHERE UNS_POS_Session_ID = ?";
		int orgID = DB.getSQLValue(trxName, sql, sessionID);
		edcSummary.setAD_Org_ID(orgID);
		edcSummary.setUNS_POS_Session_ID(sessionID);
		edcSummary.setUNS_CardType_ID(cardTypeID);
		edcSummary.setUNS_EDC_ID(edcID);
		if (!edcSummary.save())
			return null;
		return edcSummary;
	}
	
	public static MUNSSessionEDCSummary get (String trxName, int sessionID, int edcID, int cardTypeID)
	{
		String clause = "UNS_POS_Session_ID = ? AND UNS_EDC_ID = ? AND UNS_CardType_ID = ?";
		return Query.get(Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				Table_Name, clause, trxName).setParameters(sessionID, edcID, cardTypeID).
				first();
	}
	
	public static boolean updadte (String trxName, int sessionID, int edcID, 
			int cardTypeID, BigDecimal amount, boolean isSameSession)
	{
		MUNSSessionEDCSummary edc = getCreate(trxName, sessionID, edcID, cardTypeID);
		if (edc == null)
		{
			CLogger.getLogger(MUNSSessionEDCSummary.class.getName()).log(
					Level.SEVERE, "Could not find EDC Session Summary");
			return false;
		}
		if (!DB.getDatabase().forUpdate(edc, 0))
		{
			return false;
		}
		if (isSameSession)
		{
			amount = amount.add(edc.getTotalAmt());
			edc.setTotalAmt(amount);
		}
		else
		{
			amount = amount.add(edc.getPayableRefundAmt());
			edc.setPayableRefundAmt(amount);
		}
		return edc.save();
	}

	/**
	 * @param ctx
	 * @param UNS_SessionEDCSummary_ID
	 * @param trxName
	 */
	public MUNSSessionEDCSummary(Properties ctx, int UNS_SessionEDCSummary_ID,
			String trxName) {
		super(ctx, UNS_SessionEDCSummary_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSSessionEDCSummary(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (!updateHeader())
			return false;
		return super.afterSave(newRecord, success);
	}
	
	private boolean updateHeader ()
	{
		String sql = "SELECT SUM (TotalAmtB1), SUM (PayableRefundAmtB1) FROM UNS_SessionEDCSummary WHERE UNS_POS_Session_ID = ?";
		List<Object> objs = DB.getSQLValueObjectsEx(get_TrxName(), sql, getUNS_POS_Session_ID());
		if (objs == null)
			return false;
		if (objs.size() != 2)
			return false;
		BigDecimal totalAmtB1 = (BigDecimal) objs.get(0);
		BigDecimal payRefAmtB1 = (BigDecimal) objs.get(1);
		MUNSPOSSession parent = getParent();
		DB.getDatabase().forUpdate(parent, 0);
		parent.setEDCSalesAmt(totalAmtB1);
		parent.setPayableEDCRefund(payRefAmtB1);
		return parent.save();
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_PayableRefundAmt))
		{
			BigDecimal b1 = convertToBase1(getPayableRefundAmt());
			BigDecimal b2 = convertToBase2(b1);
			setPayableRefundAmtB1(b1);
			setPayableRefundAmtB2(b2);
		}
		if (newRecord || is_ValueChanged(COLUMNNAME_TotalAmt))
		{
			BigDecimal b1 = convertToBase1(getTotalAmt());
			BigDecimal b2 = convertToBase2(b1);
			setTotalAmtB1(b1);
			setTotalAmtB2(b2);
		}
		
		return super.beforeSave(newRecord);
	}
	
	private BigDecimal convertToBase1 (BigDecimal amt)
	{
		String sql = "SELECT C_Currency_ID FROM C_BankAccount WHERE C_BankAccount_ID = "
				+ "(SELECT C_BankAccount_ID FROM UNS_EDC WHERE UNS_EDC_ID = ?)";
		int currencyID = DB.getSQLValue(get_TrxName(), sql, getUNS_EDC_ID());
		BigDecimal b1 = MConversionRate.convert(getCtx(), 
				amt, currencyID, 
				getParent().getBase1Currency_ID(), getAD_Client_ID(), 
				getAD_Org_ID());
		return b1;
	}
	
	private BigDecimal convertToBase2 (BigDecimal amt)
	{
		BigDecimal b2 = MConversionRate.convert(getCtx(), 
				amt, getParent().getBase1Currency_ID(), getParent().getBase2Currency_ID(), 
				getAD_Client_ID(), getAD_Org_ID());
		return b2;
	}
}