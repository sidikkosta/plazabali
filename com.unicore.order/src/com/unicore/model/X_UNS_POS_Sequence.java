/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_POS_Sequence
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POS_Sequence extends PO implements I_UNS_POS_Sequence, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181024L;

    /** Standard Constructor */
    public X_UNS_POS_Sequence (Properties ctx, int UNS_POS_Sequence_ID, String trxName)
    {
      super (ctx, UNS_POS_Sequence_ID, trxName);
      /** if (UNS_POS_Sequence_ID == 0)
        {
			setPeriodReset (null);
// 01
        } */
    }

    /** Load Constructor */
    public X_UNS_POS_Sequence (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POS_Sequence[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Current.
		@param Current Current	  */
	public void setCurrent (int Current)
	{
		set_Value (COLUMNNAME_Current, Integer.valueOf(Current));
	}

	/** Get Current.
		@return Current	  */
	public int getCurrent () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Current);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Length.
		@param Length Length	  */
	public void setLength (int Length)
	{
		set_Value (COLUMNNAME_Length, Integer.valueOf(Length));
	}

	/** Get Length.
		@return Length	  */
	public int getLength () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Length);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Next.
		@param Next Next	  */
	public void setNext (int Next)
	{
		set_Value (COLUMNNAME_Next, Integer.valueOf(Next));
	}

	/** Get Next.
		@return Next	  */
	public int getNext () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Next);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** No Reset = 01 */
	public static final String PERIODRESET_NoReset = "01";
	/** Yearly = 02 */
	public static final String PERIODRESET_Yearly = "02";
	/** Monthly = 03 */
	public static final String PERIODRESET_Monthly = "03";
	/** Daily = 04 */
	public static final String PERIODRESET_Daily = "04";
	/** Set Period Reset.
		@param PeriodReset Period Reset	  */
	public void setPeriodReset (String PeriodReset)
	{

		set_Value (COLUMNNAME_PeriodReset, PeriodReset);
	}

	/** Get Period Reset.
		@return Period Reset	  */
	public String getPeriodReset () 
	{
		return (String)get_Value(COLUMNNAME_PeriodReset);
	}

	/** Set Prefix.
		@param Prefix 
		Prefix before the sequence number
	  */
	public void setPrefix (String Prefix)
	{
		set_Value (COLUMNNAME_Prefix, Prefix);
	}

	/** Get Prefix.
		@return Prefix before the sequence number
	  */
	public String getPrefix () 
	{
		return (String)get_Value(COLUMNNAME_Prefix);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Start No.
		@param StartNo 
		Starting number/position
	  */
	public void setStartNo (int StartNo)
	{
		set_Value (COLUMNNAME_StartNo, Integer.valueOf(StartNo));
	}

	/** Get Start No.
		@return Starting number/position
	  */
	public int getStartNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_StartNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Sufix.
		@param Sufix Sufix	  */
	public void setSufix (String Sufix)
	{
		set_Value (COLUMNNAME_Sufix, Sufix);
	}

	/** Get Sufix.
		@return Sufix	  */
	public String getSufix () 
	{
		return (String)get_Value(COLUMNNAME_Sufix);
	}

	/** Set Trx.
		@param Trx Trx	  */
	public void setTrx (String Trx)
	{
		set_Value (COLUMNNAME_Trx, Trx);
	}

	/** Get Trx.
		@return Trx	  */
	public String getTrx () 
	{
		return (String)get_Value(COLUMNNAME_Trx);
	}

	/** Set UNS_POS_Sequence_ID.
		@param UNS_POS_Sequence_ID UNS_POS_Sequence_ID	  */
	public void setUNS_POS_Sequence_ID (int UNS_POS_Sequence_ID)
	{
		if (UNS_POS_Sequence_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Sequence_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Sequence_ID, Integer.valueOf(UNS_POS_Sequence_ID));
	}

	/** Get UNS_POS_Sequence_ID.
		@return UNS_POS_Sequence_ID	  */
	public int getUNS_POS_Sequence_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Sequence_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}