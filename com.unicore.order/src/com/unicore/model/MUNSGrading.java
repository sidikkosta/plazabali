/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author AzHaidar
 *
 */
public class MUNSGrading extends X_UNS_Grading {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2520170003522625796L;
	
	public static final String GRADING_COMPULSORY_DEDUCTION = "COMP";

	/**
	 * @param ctx
	 * @param UNS_Grading_ID
	 * @param trxName
	 */
	public MUNSGrading(Properties ctx, int UNS_Grading_ID, String trxName) {
		super(ctx, UNS_Grading_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSGrading(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/**
	 * 
	 * @param ctx
	 * @param gradingValue
	 * @param trxName
	 * @return
	 */
	public static MUNSGrading get(Properties ctx, String gradingValue, String trxName)
	{
		if (gradingValue == null)
			return null;
		
		return Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, "Value = ?", trxName)
				.setParameters(gradingValue.toUpperCase())
				.first();
	}
	
	/**
	 * Get grading id of a grading component key.
	 * @param ctx
	 * @param gradingValue
	 * @param trxName
	 * @return
	 */
	public static int getGrading_ID(Properties ctx, String gradingValue, String trxName)
	{
		String sql = "SELECT UNS_Grading_ID FROM UNS_Grading WHERE Value=?";
		return DB.getSQLValueEx(trxName, sql, gradingValue.toUpperCase());
	}
}
