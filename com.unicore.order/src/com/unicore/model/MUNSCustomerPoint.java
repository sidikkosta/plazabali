/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MInvoice;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author ALBURHANY
 *
 */
public class MUNSCustomerPoint extends X_UNS_CustomerPoint {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1898140316267335056L;

	/**
	 * @param ctx
	 * @param UNS_CustomerPoint_ID
	 * @param trxName
	 */
	public MUNSCustomerPoint(Properties ctx, int UNS_CustomerPoint_ID,
			String trxName) {
		super(ctx, UNS_CustomerPoint_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCustomerPoint(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * get SUM Point Valid
	 * @param C_BPartner_ID
	 * @return Total Point Valid
	 */
	public BigDecimal getTotalValidPoint (int C_BPartner_ID)
	{
		BigDecimal totalPoint = Env.ZERO;
		
		String sql = "SELECT COALESCE(SUM(Point),0) FROM UNS_CustomerPoint_Line WHERE C_InvoiceLine_ID IN"
				+ " (SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_Invoice_ID IN"
				+ " (SELECT C_Invoice_ID FROM C_Invoice WHERE InvoiceOpen(C_Invoice_ID, null) <= 0))"
				+ " AND UNS_CustomerPoint_ID IN (SELECT UNS_CustomerPoint_ID FROM UNS_CustomerPoint"
				+ " WHERE C_BPartner_ID = " + C_BPartner_ID  + ")";
		totalPoint = DB.getSQLValueBDEx(get_TrxName(), sql);
		
		return totalPoint;
	}
	
	/**
	 * get SUM Point Not Yet Valid
	 * @param C_BPartner_ID
	 * @return Total Point Not Yet Valid
	 */
	public static BigDecimal getTotalNotYetValidPoint (Properties ctx, int C_BPartner_ID, String trxName)
	{
		BigDecimal totalPoint = Env.ZERO;
		
		String sql = "SELECT COALECE(SUM(Point),0) FROM UNS_CustomerPoint_Line WHERE C_InvoiceLine_ID IN"
				+ " (SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_Invoice_ID IN"
				+ " (SELECT C_Invoice_ID FROM C_Invoice WHERE InvoiceOpen(C_Invoice_ID, null) > 0))"
				+ " AND UNS_CustomerPoint_ID IN (SELECT UNS_CustomerPoint_ID FROM UNS_CustomerPoint"
				+ " WHERE C_BPartner_ID = " + C_BPartner_ID  + ")";
		totalPoint = DB.getSQLValueBD(trxName, sql);
		
		return totalPoint;
	}
	
	/**
	 * @param ctx
	 * @param C_BPartner_ID
	 * @param trxName
	 * @return Total Point (Valid + Not Yet valid + Redeemed)
	 */
	public static BigDecimal getTotalPoint (Properties ctx, int C_BPartner_ID, String trxName)
	{
		BigDecimal totalPoint = Env.ZERO;
		
		String sql = "SELECT COALESCE(SUM(Point,0)) FROM UNS_CustomerPoint_Line WHERE"
				+ " UNS_CustomerPoint_ID IN"
				+ " (SELECT UNS_CustomerPoint_ID FROM UNS_CustomerPoint WHERE C_BPartner_ID = " + C_BPartner_ID + ")" ;
		totalPoint = DB.getSQLValueBD(trxName, sql);
		
		return totalPoint;		
	}
	
	/**
	 * @param C_BPartner_ID
	 * @return CurrentPoint (Point Valid - Point Redeemed)
	 */

	public static BigDecimal getCurrentPoint(Properties ctx, int C_BPartner_ID, int AD_Org_ID, String trxName)
	{
		MUNSCustomerPoint cPoint = MUNSCustomerPoint.getByBPartner(ctx, C_BPartner_ID, AD_Org_ID, trxName);
		
		BigDecimal currentPoint = cPoint.getTotalValidPoint(C_BPartner_ID).subtract(cPoint.getAccumulatedRedeemed());
		
		return currentPoint;
	}
	
	public MUNSCustomerPoint (MInvoice inv)
	{
		this(inv.getCtx(), 0, inv.get_TrxName());
		setInvoice(inv);
		saveEx();
	}
	
	public void setInvoice (MInvoice inv)
	{
		if (inv == null)
			return;
		
		if(MSysConfig.getBooleanValue(MSysConfig.MERGE_ORG_POINT, true))
			setAD_Org_ID(0);
		else
			setAD_Org_ID(inv.getAD_Org_ID());
		setC_BPartner_ID(inv.getC_BPartner_ID());
		setStartTrxDate(inv.getDateInvoiced());
		setLatestTrxDate(inv.getDateInvoiced());
		setIsSOTrx(inv.isSOTrx());
	}
	
	public MUNSCustomerPointLine[] getLine(int UNS_PointSchema_ID)
	{
		List<MUNSCustomerPointLine> cpLine = new Query(getCtx(), MUNSCustomerPointLine.Table_Name,
				COLUMNNAME_UNS_CustomerPoint_ID + "=?", get_TrxName()).setParameters(UNS_PointSchema_ID).list();
		
		return cpLine.toArray(new MUNSCustomerPointLine[cpLine.size()]);
	}
	
	/**
	 * @param ctx
	 * @param C_BPartner_ID
	 * @param AD_Org_ID
	 * @param trxName
	 * @return {@link MUNSCustomerPoint}
	 */
	public static MUNSCustomerPoint getByBPartner (Properties ctx, int C_BPartner_ID, int AD_Org_ID, String trxName)
	{
		boolean isMergeOrgPoint = MSysConfig.getBooleanValue(MSysConfig.MERGE_ORG_POINT, true);
		MUNSCustomerPoint cPoint= null;
		if(isMergeOrgPoint)			
			cPoint = new Query(ctx, MUNSCustomerPoint.Table_Name, 
								COLUMNNAME_C_BPartner_ID + "=?", trxName)
									.setParameters(C_BPartner_ID).first();
		else
			cPoint = new Query(ctx, MUNSCustomerPoint.Table_Name, 
					COLUMNNAME_C_BPartner_ID + "=? AND " + COLUMNNAME_AD_Org_ID + "=?", trxName)
						.setParameters(C_BPartner_ID, AD_Org_ID).first();
		
		return cPoint;
	}
}