/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Grading_Sheet
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Grading_Sheet 
{

    /** TableName=UNS_Grading_Sheet */
    public static final String Table_Name = "UNS_Grading_Sheet";

    /** AD_Table_ID=1000277 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name DeductionAmt */
    public static final String COLUMNNAME_DeductionAmt = "DeductionAmt";

	/** Set Deduction Amount.
	  * The amount to be deducted to the original (total) calculation
	  */
	public void setDeductionAmt (BigDecimal DeductionAmt);

	/** Get Deduction Amount.
	  * The amount to be deducted to the original (total) calculation
	  */
	public BigDecimal getDeductionAmt();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name UNS_Grading_ID */
    public static final String COLUMNNAME_UNS_Grading_ID = "UNS_Grading_ID";

	/** Set Grading Component	  */
	public void setUNS_Grading_ID (int UNS_Grading_ID);

	/** Get Grading Component	  */
	public int getUNS_Grading_ID();

	public com.unicore.model.I_UNS_Grading getUNS_Grading() throws RuntimeException;

    /** Column name UNS_Grading_Sheet_ID */
    public static final String COLUMNNAME_UNS_Grading_Sheet_ID = "UNS_Grading_Sheet_ID";

	/** Set Grading Sheet	  */
	public void setUNS_Grading_Sheet_ID (int UNS_Grading_Sheet_ID);

	/** Get Grading Sheet	  */
	public int getUNS_Grading_Sheet_ID();

    /** Column name UNS_Grading_Sheet_UU */
    public static final String COLUMNNAME_UNS_Grading_Sheet_UU = "UNS_Grading_Sheet_UU";

	/** Set UNS_Grading_Sheet_UU	  */
	public void setUNS_Grading_Sheet_UU (String UNS_Grading_Sheet_UU);

	/** Get UNS_Grading_Sheet_UU	  */
	public String getUNS_Grading_Sheet_UU();

    /** Column name UNS_WeighbridgeTicket_ID */
    public static final String COLUMNNAME_UNS_WeighbridgeTicket_ID = "UNS_WeighbridgeTicket_ID";

	/** Set Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID);

	/** Get Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID();

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
