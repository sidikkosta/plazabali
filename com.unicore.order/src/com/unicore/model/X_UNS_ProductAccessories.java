/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_ProductAccessories
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ProductAccessories extends PO implements I_UNS_ProductAccessories, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160608L;

    /** Standard Constructor */
    public X_UNS_ProductAccessories (Properties ctx, int UNS_ProductAccessories_ID, String trxName)
    {
      super (ctx, UNS_ProductAccessories_ID, trxName);
      /** if (UNS_ProductAccessories_ID == 0)
        {
			setAccessoriesType (null);
			setC_UOM_ID (0);
			setLine (0);
// @SQL=SELECT NVL(MAX(Line),0)+10 AS DefaultValue FROM UNS_ProductAccessories WHERE UNS_ProductAccessories=@UNS_ProductAccessories@
			setM_Product_ID (0);
			setProductAccessories_ID (0);
			setQty (Env.ZERO);
// 0
			setUNS_ProductAccessories_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ProductAccessories (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ProductAccessories[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Mandatory = MND */
	public static final String ACCESSORIESTYPE_Mandatory = "MND";
	/** Optional = OPT */
	public static final String ACCESSORIESTYPE_Optional = "OPT";
	/** Set Accessories Type.
		@param AccessoriesType Accessories Type	  */
	public void setAccessoriesType (String AccessoriesType)
	{

		set_Value (COLUMNNAME_AccessoriesType, AccessoriesType);
	}

	/** Get Accessories Type.
		@return Accessories Type	  */
	public String getAccessoriesType () 
	{
		return (String)get_Value(COLUMNNAME_AccessoriesType);
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_ValueNoCheck (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getLine()));
    }

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getProductAccessories() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getProductAccessories_ID(), get_TrxName());	}

	/** Set Product Accessories.
		@param ProductAccessories_ID Product Accessories	  */
	public void setProductAccessories_ID (int ProductAccessories_ID)
	{
		if (ProductAccessories_ID < 1) 
			set_Value (COLUMNNAME_ProductAccessories_ID, null);
		else 
			set_Value (COLUMNNAME_ProductAccessories_ID, Integer.valueOf(ProductAccessories_ID));
	}

	/** Get Product Accessories.
		@return Product Accessories	  */
	public int getProductAccessories_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ProductAccessories_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Product Accessories.
		@param UNS_ProductAccessories_ID Product Accessories	  */
	public void setUNS_ProductAccessories_ID (int UNS_ProductAccessories_ID)
	{
		if (UNS_ProductAccessories_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ProductAccessories_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ProductAccessories_ID, Integer.valueOf(UNS_ProductAccessories_ID));
	}

	/** Get Product Accessories.
		@return Product Accessories	  */
	public int getUNS_ProductAccessories_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ProductAccessories_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ProductAccessories_UU.
		@param UNS_ProductAccessories_UU UNS_ProductAccessories_UU	  */
	public void setUNS_ProductAccessories_UU (String UNS_ProductAccessories_UU)
	{
		set_Value (COLUMNNAME_UNS_ProductAccessories_UU, UNS_ProductAccessories_UU);
	}

	/** Get UNS_ProductAccessories_UU.
		@return UNS_ProductAccessories_UU	  */
	public String getUNS_ProductAccessories_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ProductAccessories_UU);
	}
}