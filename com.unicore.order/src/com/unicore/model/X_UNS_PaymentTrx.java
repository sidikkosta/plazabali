/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PaymentTrx
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PaymentTrx extends PO implements I_UNS_PaymentTrx, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180914L;

    /** Standard Constructor */
    public X_UNS_PaymentTrx (Properties ctx, int UNS_PaymentTrx_ID, String trxName)
    {
      super (ctx, UNS_PaymentTrx_ID, trxName);
      /** if (UNS_PaymentTrx_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setC_Currency_ID (0);
			setIsManual (false);
// N
			setIsReceipt (false);
// N
			setIsReconciled (false);
// N
			setPaymentMethod (null);
// 0
			setProcessed (false);
// N
			setTrxAmt (Env.ZERO);
			setTrxType (null);
			setUNS_PaymentTrx_ID (0);
			setUNS_POS_Session_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PaymentTrx (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PaymentTrx[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Converted Amount.
		@param ConvertedAmt 
		Converted Amount
	  */
	public void setConvertedAmt (BigDecimal ConvertedAmt)
	{
		set_Value (COLUMNNAME_ConvertedAmt, ConvertedAmt);
	}

	/** Get Converted Amount.
		@return Converted Amount
	  */
	public BigDecimal getConvertedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ConvertedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Manual.
		@param IsManual 
		This is a manual process
	  */
	public void setIsManual (boolean IsManual)
	{
		set_Value (COLUMNNAME_IsManual, Boolean.valueOf(IsManual));
	}

	/** Get Manual.
		@return This is a manual process
	  */
	public boolean isManual () 
	{
		Object oo = get_Value(COLUMNNAME_IsManual);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Receipt.
		@param IsReceipt 
		This is a sales transaction (receipt)
	  */
	public void setIsReceipt (boolean IsReceipt)
	{
		set_Value (COLUMNNAME_IsReceipt, Boolean.valueOf(IsReceipt));
	}

	/** Get Receipt.
		@return This is a sales transaction (receipt)
	  */
	public boolean isReceipt () 
	{
		Object oo = get_Value(COLUMNNAME_IsReceipt);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Reconciled.
		@param IsReconciled 
		Payment is reconciled with bank statement
	  */
	public void setIsReconciled (boolean IsReconciled)
	{
		set_ValueNoCheck (COLUMNNAME_IsReconciled, Boolean.valueOf(IsReconciled));
	}

	/** Get Reconciled.
		@return Payment is reconciled with bank statement
	  */
	public boolean isReconciled () 
	{
		Object oo = get_Value(COLUMNNAME_IsReconciled);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Cash = 0 */
	public static final String PAYMENTMETHOD_Cash = "0";
	/** Card = 1 */
	public static final String PAYMENTMETHOD_Card = "1";
	/** Voucher = 3 */
	public static final String PAYMENTMETHOD_Voucher = "3";
	/** Wechat = 2 */
	public static final String PAYMENTMETHOD_Wechat = "2";
	/** Set Result Method.
		@param PaymentMethod Result Method	  */
	public void setPaymentMethod (String PaymentMethod)
	{

		set_Value (COLUMNNAME_PaymentMethod, PaymentMethod);
	}

	/** Get Result Method.
		@return Result Method	  */
	public String getPaymentMethod () 
	{
		return (String)get_Value(COLUMNNAME_PaymentMethod);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Refrerence Record.
		@param Reference_ID Refrerence Record	  */
	public void setReference_ID (int Reference_ID)
	{
		if (Reference_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Reference_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Reference_ID, Integer.valueOf(Reference_ID));
	}

	/** Get Refrerence Record.
		@return Refrerence Record	  */
	public int getReference_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Reference_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Transaction Amount.
		@param TrxAmt 
		Amount of a transaction
	  */
	public void setTrxAmt (BigDecimal TrxAmt)
	{
		set_Value (COLUMNNAME_TrxAmt, TrxAmt);
	}

	/** Get Transaction Amount.
		@return Amount of a transaction
	  */
	public BigDecimal getTrxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TrxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Transaction No.
		@param TrxNo 
		Transaction Number
	  */
	public void setTrxNo (String TrxNo)
	{
		set_ValueNoCheck (COLUMNNAME_TrxNo, TrxNo);
	}

	/** Get Transaction No.
		@return Transaction Number
	  */
	public String getTrxNo () 
	{
		return (String)get_Value(COLUMNNAME_TrxNo);
	}

	/** Cash In = C+ */
	public static final String TRXTYPE_CashIn = "C+";
	/** Change = C- */
	public static final String TRXTYPE_Change = "C-";
	/** Exchange In = E+ */
	public static final String TRXTYPE_ExchangeIn = "E+";
	/** Exchange Out = E- */
	public static final String TRXTYPE_ExchangeOut = "E-";
	/** EDC In = D+ */
	public static final String TRXTYPE_EDCIn = "D+";
	/** Wechat In = W+ */
	public static final String TRXTYPE_WechatIn = "W+";
	/** Refund = R- */
	public static final String TRXTYPE_Refund = "R-";
	/** Cash Sales Deposit = S- */
	public static final String TRXTYPE_CashSalesDeposit = "S-";
	/** Voucher Sales = V+ */
	public static final String TRXTYPE_VoucherSales = "V+";
	/** Capital In = M+ */
	public static final String TRXTYPE_CapitalIn = "M+";
	/** Payable Cash Refund = P- */
	public static final String TRXTYPE_PayableCashRefund = "P-";
	/** Refund EDC = D- */
	public static final String TRXTYPE_RefundEDC = "D-";
	/** Refund Wechat = W- */
	public static final String TRXTYPE_RefundWechat = "W-";
	/** Set Transaction Type.
		@param TrxType 
		Type of credit card transaction
	  */
	public void setTrxType (String TrxType)
	{

		set_Value (COLUMNNAME_TrxType, TrxType);
	}

	/** Get Transaction Type.
		@return Type of credit card transaction
	  */
	public String getTrxType () 
	{
		return (String)get_Value(COLUMNNAME_TrxType);
	}

	/** Set Cash Reconciliation.
		@param UNS_CashReconciliation_ID Cash Reconciliation	  */
	public void setUNS_CashReconciliation_ID (int UNS_CashReconciliation_ID)
	{
		if (UNS_CashReconciliation_ID < 1) 
			set_Value (COLUMNNAME_UNS_CashReconciliation_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_CashReconciliation_ID, Integer.valueOf(UNS_CashReconciliation_ID));
	}

	/** Get Cash Reconciliation.
		@return Cash Reconciliation	  */
	public int getUNS_CashReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CashReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Payment Transaction.
		@param UNS_PaymentTrx_ID Payment Transaction	  */
	public void setUNS_PaymentTrx_ID (int UNS_PaymentTrx_ID)
	{
		if (UNS_PaymentTrx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PaymentTrx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PaymentTrx_ID, Integer.valueOf(UNS_PaymentTrx_ID));
	}

	/** Get Payment Transaction.
		@return Payment Transaction	  */
	public int getUNS_PaymentTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PaymentTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PaymentTrx_UU.
		@param UNS_PaymentTrx_UU UNS_PaymentTrx_UU	  */
	public void setUNS_PaymentTrx_UU (String UNS_PaymentTrx_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PaymentTrx_UU, UNS_PaymentTrx_UU);
	}

	/** Get UNS_PaymentTrx_UU.
		@return UNS_PaymentTrx_UU	  */
	public String getUNS_PaymentTrx_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PaymentTrx_UU);
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POSPayment getUNS_POSPayment() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSPayment)MTable.get(getCtx(), com.unicore.model.I_UNS_POSPayment.Table_Name)
			.getPO(getUNS_POSPayment_ID(), get_TrxName());	}

	/** Set POS Payment.
		@param UNS_POSPayment_ID POS Payment	  */
	public void setUNS_POSPayment_ID (int UNS_POSPayment_ID)
	{
		if (UNS_POSPayment_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSPayment_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSPayment_ID, Integer.valueOf(UNS_POSPayment_ID));
	}

	/** Get POS Payment.
		@return POS Payment	  */
	public int getUNS_POSPayment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSPayment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}