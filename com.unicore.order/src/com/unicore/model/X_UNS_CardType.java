/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_CardType
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CardType extends PO implements I_UNS_CardType, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181219L;

    /** Standard Constructor */
    public X_UNS_CardType (Properties ctx, int UNS_CardType_ID, String trxName)
    {
      super (ctx, UNS_CardType_ID, trxName);
      /** if (UNS_CardType_ID == 0)
        {
			setName (null);
			setUNS_CardType_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CardType (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CardType[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public I_C_ValidCombination getEDCAR_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getEDCAR_Acct(), get_TrxName());	}

	/** Set EDC AR Account.
		@param EDCAR_Acct EDC AR Account	  */
	public void setEDCAR_Acct (int EDCAR_Acct)
	{
		set_Value (COLUMNNAME_EDCAR_Acct, Integer.valueOf(EDCAR_Acct));
	}

	/** Get EDC AR Account.
		@return EDC AR Account	  */
	public int getEDCAR_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_EDCAR_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getEDCCommission_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getEDCCommission_Acct(), get_TrxName());	}

	/** Set EDC Commission Account.
		@param EDCCommission_Acct EDC Commission Account	  */
	public void setEDCCommission_Acct (int EDCCommission_Acct)
	{
		set_Value (COLUMNNAME_EDCCommission_Acct, Integer.valueOf(EDCCommission_Acct));
	}

	/** Get EDC Commission Account.
		@return EDC Commission Account	  */
	public int getEDCCommission_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_EDCCommission_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public I_C_ValidCombination getOvercharge_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getOvercharge_Acct(), get_TrxName());	}

	/** Set Overcharge Account.
		@param Overcharge_Acct Overcharge Account	  */
	public void setOvercharge_Acct (int Overcharge_Acct)
	{
		set_Value (COLUMNNAME_Overcharge_Acct, Integer.valueOf(Overcharge_Acct));
	}

	/** Get Overcharge Account.
		@return Overcharge Account	  */
	public int getOvercharge_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Overcharge_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Card Type.
		@param UNS_CardType_ID Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID)
	{
		if (UNS_CardType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, Integer.valueOf(UNS_CardType_ID));
	}

	/** Get Card Type.
		@return Card Type	  */
	public int getUNS_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CardType_UU.
		@param UNS_CardType_UU UNS_CardType_UU	  */
	public void setUNS_CardType_UU (String UNS_CardType_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CardType_UU, UNS_CardType_UU);
	}

	/** Get UNS_CardType_UU.
		@return UNS_CardType_UU	  */
	public String getUNS_CardType_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CardType_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}