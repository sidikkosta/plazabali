/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_AttributeNoCost
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_AttributeNoCost extends PO implements I_UNS_AttributeNoCost, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191209L;

    /** Standard Constructor */
    public X_UNS_AttributeNoCost (Properties ctx, int UNS_AttributeNoCost_ID, String trxName)
    {
      super (ctx, UNS_AttributeNoCost_ID, trxName);
      /** if (UNS_AttributeNoCost_ID == 0)
        {
			setC_DocType_ID (0);
			setM_AttributeSetInstance_ID (0);
			setM_Locator_ID (0);
			setM_Product_ID (0);
			setProcessed (false);
// N
			setQty (Env.ZERO);
// 0
			setQtyOnHand (Env.ZERO);
// 0
			setRemainingQty (Env.ZERO);
// 0
        } */
    }

    /** Load Constructor */
    public X_UNS_AttributeNoCost (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_AttributeNoCost[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cost Value.
		@param CostAmt 
		Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt)
	{
		set_Value (COLUMNNAME_CostAmt, CostAmt);
	}

	/** Get Cost Value.
		@return Value with Cost
	  */
	public BigDecimal getCostAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CostAmt);
//		if (bd == null)
//			 return Env.ZERO;
		return bd;
	}

	/** Set Date  Material Policy.
		@param DateMaterialPolicy 
		Time used for LIFO and FIFO Material Policy
	  */
	public void setDateMaterialPolicy (Timestamp DateMaterialPolicy)
	{
		set_ValueNoCheck (COLUMNNAME_DateMaterialPolicy, DateMaterialPolicy);
	}

	/** Get Date  Material Policy.
		@return Time used for LIFO and FIFO Material Policy
	  */
	public Timestamp getDateMaterialPolicy () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateMaterialPolicy);
	}

	/** Set Date Material Policy To.
		@param DateMaterialPolicyTo Date Material Policy To	  */
	public void setDateMaterialPolicyTo (Timestamp DateMaterialPolicyTo)
	{
		set_Value (COLUMNNAME_DateMaterialPolicyTo, DateMaterialPolicyTo);
	}

	/** Get Date Material Policy To.
		@return Date Material Policy To	  */
	public Timestamp getDateMaterialPolicyTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateMaterialPolicyTo);
	}

	/** Set ETBB Code.
		@param ETBBCode ETBB Code	  */
	public void setETBBCode (String ETBBCode)
	{
		set_Value (COLUMNNAME_ETBBCode, ETBBCode);
	}

	/** Get ETBB Code.
		@return ETBB Code	  */
	public String getETBBCode () 
	{
		return (String)get_Value(COLUMNNAME_ETBBCode);
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstanceTo() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstanceTo_ID(), get_TrxName());	}

	/** Set Attribute Set Instance To.
		@param M_AttributeSetInstanceTo_ID 
		Target Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstanceTo_ID (int M_AttributeSetInstanceTo_ID)
	{
		if (M_AttributeSetInstanceTo_ID < 1) 
			set_Value (COLUMNNAME_M_AttributeSetInstanceTo_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstanceTo_ID, Integer.valueOf(M_AttributeSetInstanceTo_ID));
	}

	/** Get Attribute Set Instance To.
		@return Target Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstanceTo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstanceTo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_InventoryLine getM_InventoryLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_InventoryLine)MTable.get(getCtx(), org.compiere.model.I_M_InventoryLine.Table_Name)
			.getPO(getM_InventoryLine_ID(), get_TrxName());	}

	/** Set Phys.Inventory Line.
		@param M_InventoryLine_ID 
		Unique line in an Inventory document
	  */
	public void setM_InventoryLine_ID (int M_InventoryLine_ID)
	{
		if (M_InventoryLine_ID < 1) 
			set_Value (COLUMNNAME_M_InventoryLine_ID, null);
		else 
			set_Value (COLUMNNAME_M_InventoryLine_ID, Integer.valueOf(M_InventoryLine_ID));
	}

	/** Get Phys.Inventory Line.
		@return Unique line in an Inventory document
	  */
	public int getM_InventoryLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InventoryLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Locator getM_Locator() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Movement Quantity.
		@param MovementQty 
		Quantity of a product moved.
	  */
	public void setMovementQty (BigDecimal MovementQty)
	{
		set_Value (COLUMNNAME_MovementQty, MovementQty);
	}

	/** Get Movement Quantity.
		@return Quantity of a product moved.
	  */
	public BigDecimal getMovementQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MovementQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set No Cost.
		@param NoCost No Cost	  */
	public void setNoCost (boolean NoCost)
	{
		set_Value (COLUMNNAME_NoCost, Boolean.valueOf(NoCost));
	}

	/** Get No Cost.
		@return No Cost	  */
	public boolean isNoCost () 
	{
		Object oo = get_Value(COLUMNNAME_NoCost);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set On Hand Quantity.
		@param QtyOnHand 
		On Hand Quantity
	  */
	public void setQtyOnHand (BigDecimal QtyOnHand)
	{
		set_Value (COLUMNNAME_QtyOnHand, QtyOnHand);
	}

	/** Get On Hand Quantity.
		@return On Hand Quantity
	  */
	public BigDecimal getQtyOnHand () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyOnHand);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Remaining Qty.
		@param RemainingQty 
		The total remaining quantity
	  */
	public void setRemainingQty (BigDecimal RemainingQty)
	{
		set_Value (COLUMNNAME_RemainingQty, RemainingQty);
	}

	/** Get Remaining Qty.
		@return The total remaining quantity
	  */
	public BigDecimal getRemainingQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Attribute No Cost.
		@param UNS_AttributeNoCost_ID Attribute No Cost	  */
	public void setUNS_AttributeNoCost_ID (int UNS_AttributeNoCost_ID)
	{
		if (UNS_AttributeNoCost_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AttributeNoCost_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AttributeNoCost_ID, Integer.valueOf(UNS_AttributeNoCost_ID));
	}

	/** Get Attribute No Cost.
		@return Attribute No Cost	  */
	public int getUNS_AttributeNoCost_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AttributeNoCost_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_AttributeNoCost_UU.
		@param UNS_AttributeNoCost_UU UNS_AttributeNoCost_UU	  */
	public void setUNS_AttributeNoCost_UU (String UNS_AttributeNoCost_UU)
	{
		set_Value (COLUMNNAME_UNS_AttributeNoCost_UU, UNS_AttributeNoCost_UU);
	}

	/** Get UNS_AttributeNoCost_UU.
		@return UNS_AttributeNoCost_UU	  */
	public String getUNS_AttributeNoCost_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_AttributeNoCost_UU);
	}

	public com.unicore.model.I_UNS_SalesReconciliation getUNS_SalesReconciliation() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_SalesReconciliation)MTable.get(getCtx(), com.unicore.model.I_UNS_SalesReconciliation.Table_Name)
			.getPO(getUNS_SalesReconciliation_ID(), get_TrxName());	}

	/** Set Sales Reconciliation.
		@param UNS_SalesReconciliation_ID Sales Reconciliation	  */
	public void setUNS_SalesReconciliation_ID (int UNS_SalesReconciliation_ID)
	{
		if (UNS_SalesReconciliation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SalesReconciliation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SalesReconciliation_ID, Integer.valueOf(UNS_SalesReconciliation_ID));
	}

	/** Get Sales Reconciliation.
		@return Sales Reconciliation	  */
	public int getUNS_SalesReconciliation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SalesReconciliation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}