/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_HSCode
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_HSCode extends PO implements I_UNS_HSCode, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190126L;

    /** Standard Constructor */
    public X_UNS_HSCode (Properties ctx, int UNS_HSCode_ID, String trxName)
    {
      super (ctx, UNS_HSCode_ID, trxName);
      /** if (UNS_HSCode_ID == 0)
        {
			setCode (null);
			setUNS_HSCode_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_HSCode (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_HSCode[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Validation code.
		@param Code 
		Validation Code
	  */
	public void setCode (String Code)
	{
		set_Value (COLUMNNAME_Code, Code);
	}

	/** Get Validation code.
		@return Validation Code
	  */
	public String getCode () 
	{
		return (String)get_Value(COLUMNNAME_Code);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set HS Code.
		@param UNS_HSCode_ID HS Code	  */
	public void setUNS_HSCode_ID (int UNS_HSCode_ID)
	{
		if (UNS_HSCode_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_HSCode_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_HSCode_ID, Integer.valueOf(UNS_HSCode_ID));
	}

	/** Get HS Code.
		@return HS Code	  */
	public int getUNS_HSCode_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_HSCode_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_HSCode_UU.
		@param UNS_HSCode_UU UNS_HSCode_UU	  */
	public void setUNS_HSCode_UU (String UNS_HSCode_UU)
	{
		set_Value (COLUMNNAME_UNS_HSCode_UU, UNS_HSCode_UU);
	}

	/** Get UNS_HSCode_UU.
		@return UNS_HSCode_UU	  */
	public String getUNS_HSCode_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_HSCode_UU);
	}
}