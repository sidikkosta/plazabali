/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;

/**
 * @author Burhani Adam
 *
 */
public class MUNSUnLoadingConfigLine extends X_UNS_UnLoadingConfig_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = -391253568585711429L;

	/**
	 * @param ctx
	 * @param UNS_UnLoadingConfig_Line_ID
	 * @param trxName
	 */
	public MUNSUnLoadingConfigLine(Properties ctx,
			int UNS_UnLoadingConfigLine_ID, String trxName) {
		super(ctx, UNS_UnLoadingConfigLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUnLoadingConfigLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSUnLoadingConfigLine getByParent(Properties ctx, MUNSUnLoadingConfig lc,
			String shippedBy, String calculateBase, String trxName)
	{
		MUNSUnLoadingConfigLine value = null;
		for(MUNSUnLoadingConfigLine lcl : lc.getLines(null))
		{
			if(shippedBy.equals(lcl.getShippedBy()) && calculateBase.equals(lcl.getCalculateBase()))
			{
				value = (MUNSUnLoadingConfigLine) lcl;
				break;
			}
		}
		return value;
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT 1 FROM UNS_UnloadingConfig_Line WHERE M_Product_Category_ID=? AND M_Product_ID=?"
				+ " AND CalculateBase=? AND ShippedBy=? AND isDumpTruck=? AND (UNS_ArmadaType_ID=? OR isAllArmada='Y')";
		int count = DB.getSQLValue(get_TrxName(), sql, new Object[]{getM_Product_Category_ID(), getM_Product_ID(),
						getCalculateBase(), getShippedBy(), isDumpTruck(), getUNS_ArmadaType_ID()});
		if(count > 0)
		{
			log.saveError("Error", "duplicate detail in one configuration");
			return false;
		}
		return true;
	}
}