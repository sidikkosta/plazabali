/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCharge;
import org.compiere.model.MInvoice;
import org.compiere.model.MPriceList;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;

import com.unicore.base.model.MInvoiceLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSStatementOfAccount extends X_UNS_StatementOfAccount implements DocAction, DocOptions{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8957889727940452699L;

	/**
	 * @param ctx
	 * @param UNS_StatementOfAccount_ID
	 * @param trxName
	 */
	public MUNSStatementOfAccount(Properties ctx,
			int UNS_StatementOfAccount_ID, String trxName) {
		super(ctx, UNS_StatementOfAccount_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSStatementOfAccount(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSStatementOfAccount(MUNSSASummary summary)
	{
		super(summary.getCtx(), 0, summary.get_TrxName());
		setUNS_SA_Summary_ID(summary.get_ID());
		setAD_Client_ID(summary.getAD_Client_ID());
		setAD_Org_ID(summary.getAD_Org_ID());
		setDateAcct(summary.getDateAcct());
		setDateFrom(summary.getDateFrom());
		setDateTo(summary.getDateTo());
		setC_Charge_ID(summary.getC_Charge_ID());
		setC_DocType_ID(summary.getC_DocType_ID());
		setC_Period_ID(summary.getC_Period_ID());
		setShopType(summary.getShopType());
	}
	
	public static MUNSStatementOfAccount get(MUNSSASummary summary, int BPartnerID)
	{
		MUNSStatementOfAccount acct = Query.get(summary.getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, COLUMNNAME_UNS_SA_Summary_ID + "=? AND " + COLUMNNAME_C_BPartner_ID + "=?", summary.get_TrxName())
					.setParameters(summary.get_ID(), BPartnerID).firstOnly();
		
		if(acct == null)
		{
			acct = new MUNSStatementOfAccount(summary);
			acct.setC_BPartner_ID(BPartnerID);
			acct.setC_BPartner_Location_ID(MBPartnerLocation.getForBPartner(summary.getCtx(), BPartnerID, summary.get_TrxName())[0].get_ID());
		}
		
		return acct;
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSStatementOfAccount[");
		sb.append(get_ID()).append("-").append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_StatementOfAccount_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		if(newRecord && getUNS_SA_Summary_ID() <= 0)
			setContributionAmt(getContributionAmt());
		
		if(!newRecord && (is_ValueChanged(COLUMNNAME_C_BPartner_ID) || is_ValueChanged(COLUMNNAME_DateFrom)
				|| is_ValueChanged(COLUMNNAME_DateTo) || is_ValueChanged(COLUMNNAME_Description)))
			setContributionAmt(getContributionAmt());
		
		BigDecimal GT = Env.ZERO;
			
		if(!getShopType().equalsIgnoreCase("DP")){
			GT = getTotalLines().subtract(getOtherCosts()).
					subtract(getContributionAmt()).subtract(getShippingCost()).subtract(getPushMoneyAmt());
		}		
				
		setGrandTotal(GT);

		if(getC_Charge_ID() <= 0)
		{
			String sql = "SELECT UNS_Consignment_Acct FROM C_AcctSchema_Default WHERE C_AcctSchema_ID = ?";
			MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
			MAcctSchema as = null;
			for(int i=0;i<ass.length;i++)
			{
				if(ass[i].getC_Currency_ID() == 303)
				{
					as = ass[i];
					break;
				}
			}
			int validCombination = DB.getSQLValue(get_TrxName(), sql, as.get_ID());
			int chargeID = MCharge.getOfAccount(validCombination, as);
			setC_Charge_ID(chargeID);
		}
		
		return true;
	}
	
	@Override
	public BigDecimal getContributionAmt()
	{
		int divisionID = 0;
		if(getUNS_SA_Summary_ID() <= 0)
		{
			String sql = "SELECT UNS_Division_ID FROM UNS_Division WHERE value LIKE '%" + getShopType() + "%'";
			divisionID = DB.getSQLValue(get_TrxName(), sql);
		}
		else
			divisionID = getUNS_SA_Summary().getUNS_Division_ID();
		
		MUNSVendorContribution contribution = MUNSVendorContribution.get(
				getCtx(), getAD_Org_ID(), getC_BPartner_ID(), getDateAcct(), 
					divisionID, get_TrxName());
		
		if(contribution == null)
			return Env.ZERO;
		else
		{
			if(contribution.isFixedValue())
				return contribution.getContributionAmt();
			else
			{
				String sql = "SELECT SUM(Qty*SellingPrice) FROM UNS_StatementOfAcct_Line"
						+ " WHERE UNS_StatementOfAccount_ID = ?";
				BigDecimal salesAmt = DB.getSQLValueBD(get_TrxName(), sql, get_ID());
				if(salesAmt == null || salesAmt.compareTo(Env.ZERO) == 0)
					return Env.ZERO;
				BigDecimal result = (salesAmt.multiply(contribution.getContributionAmt()))
						.divide(Env.ONEHUNDRED, 2, RoundingMode.HALF_UP);
				return result;
			}
		}
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(isRunValidator())
		{
			if(newRecord || is_ValueChanged(COLUMNNAME_DateFrom) || is_ValueChanged(COLUMNNAME_DateTo)
					|| is_ValueChanged(COLUMNNAME_C_BPartner_ID) || is_ValueChanged(COLUMNNAME_Description))
			{
				if(!newRecord && !deleteLines())
					
				{
					log.saveError("Error", "Failed when trying delete lines.");
					return false;
				}
				m_processMsg = generateLines();
				if(m_processMsg != null)
				{
					log.saveError("Error", m_processMsg);
					return false;
				}
			}
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		if(!deleteLines())
		{
			log.saveError("Error", "Failed when trying delete lines.");
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	private String createLines()
	{
		BigDecimal total = Env.ZERO;
		String sql = "SELECT sr.DateTrx, pr.Store_ID, prl.M_Product_ID, SUM(prl.Qty) FROM UNS_POSRecapLine prl"
				+ " INNER JOIN UNS_POSRecap pr ON pr.UNS_POSRecap_ID = prl.UNS_POSRecap_ID"
				+ " INNER JOIN UNS_SalesReconciliation sr ON sr.UNS_SalesReconciliation_ID = pr.UNS_SalesReconciliation_ID"
				+ " INNER JOIN C_BPartner_Product bp ON bp.M_Product_ID = prl.M_Product_ID"
				+ " INNER JOIN C_BPartner st ON st.C_BPartner_ID = pr.Store_ID"
				+ " INNER JOIN UNS_StoreConfiguration bpg ON bpg.Store_ID = st.C_BPartner_ID"
				+ " WHERE bp.C_BPartner_ID = ? AND sr.DateTrx BETWEEN ? AND ? AND prl.IsConsignment = 'Y'"
				+ " AND sr.DocStatus IN ('CO', 'CL') AND bpg.ShopType = ?"
				+ " GROUP BY sr.UNS_SalesReconciliation_ID, pr.UNS_POSRecap_ID, prl.M_Product_ID";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, getC_BPartner_ID());
			java.sql.Timestamp nextDay = TimeUtil.addDays(getDateFrom(), -1);
			stmt.setTimestamp(2, nextDay);
			nextDay = TimeUtil.addDays(getDateTo(), 1);
			stmt.setTimestamp(3, nextDay);
			stmt.setString(4, getShopType());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSStatementOfAcctLine line = new MUNSStatementOfAcctLine(getCtx(), 0, get_TrxName());
				line.setUNS_StatementOfAccount_ID(get_ID());
				line.setAD_Org_ID(getAD_Org_ID());
				line.setM_Product_ID(rs.getInt(3));
				line.setQty(rs.getBigDecimal(4));
				if(!line.save())
					return "Failed when trying create lines.";
				
				total = total.add(line.getTotalLines());
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		}
		
		setTotalLines(total);
		setRunValidator(false);
		save();

		return null;
	}
	
	
	private boolean getBpartnerVat (int C_BPartner_ID) {
		boolean vat = false;
		String sql = "select IsPKP from c_bpartner where c_bpartner_id ="+C_BPartner_ID+"  ";
		String vat_ = DB.getSQLValueString(get_TrxName(), sql);
		if(vat_.equalsIgnoreCase("Y"))
			vat = true;
		else {vat = false;}
		
		return vat;
	}	
	
	
	private String generateLines()
	{
		BigDecimal total = Env.ZERO;
		BigDecimal pushMoney = Env.ZERO;
		String sql = "SELECT DISTINCT(p.M_Product_ID), po.C_Currency_ID, p.C_UOM_ID FROM M_Product p"
				+ " INNER JOIN M_Product_PO po ON po.M_Product_ID = p.M_Product_ID"
				+ " WHERE po.C_BPartner_ID = ? AND p.IsConsignment = 'Y' AND p.IsActive = 'Y'";
		if(getC_Currency_ID() > 0)
			sql += " AND po.C_Currency_ID = " + getC_Currency_ID();
			
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int currencyID = 0;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, getC_BPartner_ID());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSStatementOfAcctLine line = new MUNSStatementOfAcctLine(getCtx(), 0, get_TrxName());
				line.setUNS_StatementOfAccount_ID(get_ID());
				line.setAD_Org_ID(getAD_Org_ID());
				line.setM_Product_ID(rs.getInt(1));
				line.setC_UOM_ID(rs.getInt(3));
				line.initQty();
				if(line.getQtySales().signum() == 0 
						&& line.getBeginStock().signum() == 0
							&& line.getEndStock().signum() == 0
								&& line.getMoveQty().signum() == 0
									&& line.getReceiptQty().signum() == 0
										&& line.getAdjustStock().signum() == 0
											&& line.getConvQty().signum() == 0
												&& line.getReturnedQty().signum() == 0)
					continue;
				else
				{
					line.setReInitQty(false);
					line.save();
//					System.out.println("xxxxxxxxxxxxxxxxx save sa = 2 = "+line.get_ID() );
				}
				currencyID = rs.getInt(2);
				total = total.add(line.getTotalLines());
				pushMoney = pushMoney.add(line.getPushMoneyAmt());
			}
		} catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		setTotalLines(total);
		
		

		BigDecimal bvat_ = Env.ZERO;
		BigDecimal gt = Env.ZERO;
		
		if(getBpartnerVat (getC_BPartner_ID()) == true){
			bvat_ =  getTotalLines().multiply(new BigDecimal(0.1)).setScale(2, RoundingMode.HALF_UP);
			set_ValueOfColumn("VAT",bvat_); 

			if(getShopType().equalsIgnoreCase("DP")){
				gt = getTotalLines().subtract(getOtherCosts()).
						subtract(getContributionAmt()).subtract(getShippingCost()).
						subtract(getPushMoneyAmt()).add(bvat_).setScale(2, RoundingMode.HALF_UP);
			}else{
				gt = getTotalLines().subtract(getOtherCosts()).
						subtract(getContributionAmt()).subtract(getShippingCost()).
						subtract(getPushMoneyAmt()).setScale(2, RoundingMode.HALF_UP);
			}
			setGrandTotal(gt);

		}else {
			set_ValueOfColumn("VAT", Env.ZERO);
			gt = getTotalLines().subtract(getOtherCosts()).
					subtract(getContributionAmt()).subtract(getShippingCost()).
					subtract(getPushMoneyAmt()).setScale(2, RoundingMode.HALF_UP);
			setGrandTotal(gt);
		}
		
		System.out.println("bpartner = "+getC_BPartner_ID());
		System.out.println("bpartner vat = "+getBpartnerVat (getC_BPartner_ID()));
		
		System.out.println("total line = "+getTotalLines());
		System.out.println("vat = "+get_Value("VAT"));
		System.out.println("grand total = "+get_Value("GrandTotal"));
		System.out.println("grand total = "+getGrandTotal());
//				
		setRunValidator(false);
		setC_Currency_ID(currencyID);
		setPushMoneyAmt(pushMoney);
		setContributionAmt(getContributionAmt());
		save();
		
		return null;
	}
	
	private boolean m_RunValidator = true;
	
	private boolean isRunValidator()
	{
		return m_RunValidator;
	}
	
	private void setRunValidator(boolean runValidator)
	{
		m_RunValidator = runValidator;
	}
	
	private boolean deleteLines()
	{
		String sql = "DELETE FROM UNS_StatementOfAcct_Trx t WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_StatementOfAcct_Line l WHERE l.UNS_StatementOfAcct_Line_ID"
				+ " = t.UNS_StatementOfAcct_Line_ID AND l.UNS_StatementOfAccount_ID = ?)";
		DB.executeUpdate(sql, get_ID(), get_TrxName());
		sql = "DELETE FROM UNS_StatementOfAcct_Line WHERE UNS_StatementOfAccount_ID = ?";
		boolean success = DB.executeUpdate(sql, get_ID(), get_TrxName()) >= 0; 
		
		return success;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(getC_Charge_ID() <= 0 && getUNS_SA_Summary_ID() > 0)
		{
			setC_Charge_ID(getUNS_SA_Summary().getC_Charge_ID());
		}
		else if(getC_Charge_ID() <= 0)
		{
			m_processMsg = "Field Mandatory Charge.";
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		m_processMsg = doInvoice(true);
		if (m_processMsg != null)
		{
			m_processMsg = m_processMsg + " #" + getDocumentNo();
			return DocAction.STATUS_Invalid;
		}
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = doInvoice(false);
		if (m_processMsg != null)
			return false;
		
		String sql = "DELETE FROM Fact_Acct WHERE AD_Table_ID = ? AND Record_ID = ?";
		DB.executeUpdate(sql, new Object[]{Table_ID, get_ID()}, false, get_TrxName());
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		m_processMsg = doInvoice(false);
		if (m_processMsg != null)
			return false;
		
		setC_Invoice_ID(-1);

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt()
	{
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt()
	{
		setProcessed(false);
		return true;
	}
	
	private String doInvoice(boolean isComplete)
	{
		if((isComplete && getC_Invoice_ID() > 0) || (!isComplete && getC_Invoice_ID() < 0))
			return null;
		
		if(getGrandTotal().signum() == 0)
			return null;
		
		boolean isSOTrx = getGrandTotal().signum() < 0;
		if(isComplete)
		{
			if(getDocTypeInvoice_ID() <= 0)
				return "Please set Invoice Doc. Type.";
			com.unicore.base.model.MInvoice inv = new com.unicore.base.model.MInvoice(getCtx(), 0, get_TrxName());
			inv.setAD_Org_ID(getAD_Org_ID());
			inv.setDateAcct(getDateAcct());
			inv.setDateInvoiced(getDateAcct());
			inv.setC_BPartner_ID(getC_BPartner_ID());
			inv.setC_BPartner_Location_ID(getC_BPartner_Location_ID());
			inv.setC_Currency_ID(getC_Currency_ID());
			inv.setDescription(getDescription());
			final String whereClause = "AD_Client_ID=? AND IsSOPriceList=? AND C_Currency_ID = ?";
			MPriceList pl = new Query(getCtx(), MPriceList.Table_Name, whereClause, null)
							.setParameters(getAD_Client_ID(), "N", getC_Currency_ID())
							.setOnlyActiveRecords(true)
							.setOrderBy("IsDefault DESC")
							.first();
			inv.setM_PriceList_ID(pl.get_ID());
			inv.setIsSOTrx(isSOTrx);
//			String sql = "SELECT C_DocType_ID FROM C_DocType WHERE UPPER(Name) LIKE '%MERCHANDISING%'"
//					+ " AND DocBaseType = ?";
//			int dt = DB.getSQLValue(get_TrxName(), sql, MDocType.DOCBASETYPE_APInvoice);
//			if(dt > 0)
//			{
//				inv.setC_DocType_ID(dt);
//				inv.setC_DocTypeTarget_ID(dt);
//			}
//			else
//				inv.setC_DocTypeTarget_ID();
			inv.setC_DocType_ID(getDocTypeInvoice_ID());
			inv.setC_DocTypeTarget_ID(getDocTypeInvoice_ID());
			inv.setC_ConversionType_ID(201);
			inv.save();
			
			MInvoiceLine line = new MInvoiceLine(inv);
			line.setC_Charge_ID(getC_Charge_ID());
			line.setQty(Env.ONE);
			line.setPrice(getGrandTotal());
			line.setPriceList(getGrandTotal());
			line.save();
			
			if(!inv.processIt(DOCACTION_Complete) || !inv.save())
				return inv.getProcessMsg();
			setC_Invoice_ID(inv.get_ID());
		}
		else
		{
			MInvoice inv = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
			if(!inv.processIt(DOCACTION_Reverse_Correct) || !inv.save())
				return inv.getProcessMsg();
		}
		
		return null;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if(docStatus.equals(STATUS_Completed))
		{
			options[index++] = ACTION_Void;
		}
		
		return index;
	}
	
	public MUNSStatementOfAcctLine[] getLines()
	{
		List<MUNSStatementOfAcctLine> list = Query.get(getCtx(),
				UNSOrderModelFactory.EXTENSION_ID, MUNSStatementOfAcctLine.Table_Name, Table_Name + "_ID=?",
					get_TrxName()).setParameters(get_ID()).list();
		
		return list.toArray(new MUNSStatementOfAcctLine[list.size()]);
	}
}