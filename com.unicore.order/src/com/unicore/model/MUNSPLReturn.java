/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.compiere.wf.MWorkflow;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author UNTA_Andy
 * 
 */
public class MUNSPLReturn extends X_UNS_PL_Return implements DocAction, DocOptions
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8667250410414034575L;
	
	/**
	 * @param ctx
	 * @param UNS_PL_Return_ID
	 * @param trxName
	 */
	public MUNSPLReturn(Properties ctx, int UNS_PL_Return_ID, String trxName)
	{
		super(ctx, UNS_PL_Return_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPLReturn(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}

	/**************************************************************************
	 * Process document
	 * 
	 * @param processAction document action
	 * @return true if performed
	 */
	public boolean processIt(String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(processAction, getDocAction());
	} // processIt

	/** Process Message */
	String m_processMsg = null;
	/** Just Prepared Flag */
	private boolean m_justPrepared = false;
	private MUNSPLReturnOrder[] m_ROrder;

	/**
	 * Unlock Document.
	 * 
	 * @return true if success
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	} // unlockIt

	/**
	 * Invalidate Document
	 * 
	 * @return true if success
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	} // invalidateIt

	/**************************************************************************
	 * Prepare Document
	 * 
	 * @return new status (In Progress or Invalid)
	 */
	public String prepareIt()
	{
		if (getDateDoc() == null || getDateDoc().before(getUNS_PackingList().getDateDoc()))
		{
			m_processMsg = "Mandatory or invalid Document Date!";
			return DocAction.STATUS_Drafted;
		}

		if (log.isLoggable(Level.INFO))
			log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		// Lines
		MUNSPLReturnOrder[] rorderList = getROders(true, null);
		if (rorderList.length == 0)
		{
			m_processMsg = "@NoLines@";
			return DocAction.STATUS_Invalid;
		}
		
		for (MUNSPLReturnOrder rorder : rorderList)
		{
			if (rorder.isPartialCancelation())
			{
				String sql = "SELECT count(*) FROM UNS_PL_ReturnLine WHERE CancelledQty = 0.0 AND UNS_PL_ReturnOrder_ID=?";
				int count = DB.getSQLValueEx(get_TrxName(), sql, rorder.get_ID());
				
				if (count > 0)
				{
					m_processMsg = "There " + (count > 1? "are" : "is") + count 
							+ " cancelled line" + (count > 1? "s" : "") + " with zero quantity. "
							+ "Please either remove the zero quantity line "
							+ "or set it to non zero cancelled quantity.";
					return DocAction.STATUS_Invalid;
				}
			}
		}
		
		//create confirm DO receipt
//		String sql = "SELECT 1 FROM UNS_Handover_Document WHERE UNS_PL_Return_ID = " + getUNS_PL_Return_ID();
//		
//		if (DB.getSQLValue(get_TrxName(), sql) < 0)
//		{
//			MUNSHandoverDocument hod = new MUNSHandoverDocument(getCtx(), 0, get_TrxName());
//			hod.setAD_Org_ID(getAD_Org_ID());
//			hod.setC_DocType_ID(MDocType.getDocType("HOD"));
//			hod.setUNS_PL_Return_ID(getUNS_PL_Return_ID());
//			hod.setDateDoc(getDateDoc());
//			hod.setDescription(getDescription());
//			hod.setName(getUNS_PackingList().getName());
//			hod.saveEx();
//			
//			String whereClause = " UNS_PL_Return_ID = ?";
//			List <MUNSPLReturnOrder> plrOrder = new Query(getCtx(), MUNSPLReturnOrder.Table_Name, whereClause, get_TrxName())
//												.setParameters(getUNS_PL_Return_ID()).list();
//			for (MUNSPLReturnOrder rOrder : plrOrder)
//			{
//				MUNSHODLine hodLine = new MUNSHODLine(getCtx(), 0, get_TrxName());
//				hodLine.setAD_Org_ID(rOrder.getAD_Org_ID());
//				hodLine.setUNS_Handover_Document_ID(hod.get_ID());
//				hodLine.setUNS_PackingList_Order_ID(rOrder.getUNS_PackingList_Order_ID());
//				hodLine.setM_InOut_ID(rOrder.getUNS_PackingList_Order().getM_InOut_ID());
//				hodLine.setIsCancelled(rOrder.isCancelled());
//				hodLine.setIsReceipt(true);
//				if(rOrder.isCancelled())
//					hodLine.setDescription(rOrder.getDescription());
//				hodLine.saveEx();
//			}	
//		}
		m_processMsg =ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		m_justPrepared = true;

		return DocAction.STATUS_InProgress;
	} // prepareIt

	/**
	 * Approve Document
	 * 
	 * @return true if success
	 */
	public boolean approveIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	} // approveIt

	/**
	 * Reject Approval
	 * 
	 * @return true if success
	 */
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	} // rejectIt

	@Override
	public String completeIt()
	{
		// Just prepare
		if (DOCACTION_Prepare.equals(getDocAction()))
		{
			setProcessed(false);
			return DocAction.STATUS_InProgress;
		}

		// Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;


		if (!MUNSPLConfirm.isCompleting(getCtx(), getDocumentNo()))
		{
			m_processMsg = MUNSPLConfirm.create(this);
			if(!Util.isEmpty(m_processMsg, true))
			{
				setProcessed(true);
				return DocAction.STATUS_InProgress;
			}
		}
		
		
		m_processMsg = moveItems();
		if (!Util.isEmpty(m_processMsg,  true)) 
		{
			setProcessed(true);
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = completingOtherDocument(this);
		if (m_processMsg != null) {
			setProcessed(true);
			return DocAction.STATUS_Invalid;
		}

		// Implicit Approval
		if (!isApproved())
			approveIt();


		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		setProcessed(true);
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt

	/**
	 * 
	 * @param plReturn
	 * @return
	 */
	private static String completingOtherDocument(MUNSPLReturn plReturn)
	{
		for (MUNSPLReturnOrder ro : plReturn.getROders())
		{			
			MUNSPackingListOrder plo =
					new MUNSPackingListOrder(ro.getCtx(), ro.getUNS_PackingList_Order_ID(), ro.get_TrxName());
			String docAction =
					(ro.isCancelled() && !ro.isPartialCancelation())? 
							DocAction.ACTION_Void : DocAction.ACTION_Complete;
			
			MInOut io = (MInOut) plo.getM_InOut();
			if(!io.isComplete())
			{	
				try
				{
					ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(io, docAction);
					if(pi.isError())
					{
						throw new AdempiereException("Error when trying to complete Shipment (Customer) " + pi.getSummary());
					}
					
					io.saveEx();
				}
				catch (Exception e)
				{
					throw new AdempiereException("Error when trying to complete Shipment (Customer) :: " + e.getMessage());
				}
			}
			
			MInvoice iv = (MInvoice) plo.getC_Invoice();
			if(!iv.isComplete())
			{
				if (docAction.equals(ACTION_Void))
				{
					try
					{
						iv.setC_DocType_ID(iv.getC_DocTypeTarget_ID());
						ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(iv, docAction);
						
						if(pi.isError())
						{
							throw new AdempiereException("Error when trying to complete Invoice (Customer) " + pi.getSummary());
						}
	
						iv.saveEx();
					}
					catch (Exception e)
					{
						throw new AdempiereException("Error when trying to complete Invoice (customer) :: " + e.getMessage());
					}
				} 
				else {
					iv.setDocStatus(DOCSTATUS_InProgress);
					iv.saveEx();
				}
			}
			
			if (!plo.reserveStock(plo.getLines(), false))
				throw new AdempiereException("Error when try remove reverse shipping qty.");
		}
		return null;
	}

	@Override
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		addDescription(Msg.getMsg(getCtx(), "Voided"));

		
		MUNSPLConfirm[] plReturnConfirmList = MUNSPLConfirm.gets(this);
		
		for (MUNSPLConfirm plConfirm : plReturnConfirmList)
		{
			plConfirm.addDescription("{Voided by Packing List Result (Return) void action (PL Return:" 
									+ getDocumentNo() + ")}.");
			
			if (!plConfirm.processIt(DOCACTION_Void)) {
				m_processMsg = "Failed when void PL Confirmation of " + plConfirm.getDocumentNo();
				return false;
			}
			
			plConfirm.saveEx();	
		}
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		return true;
	}

	/**
	 * Add to Description
	 * 
	 * @param description text
	 */
	public void addDescription(String description)
	{
		String desc = getDescription();
		if (desc == null)
			setDescription(description);
		else
			setDescription(desc + " | " + description);
	} // addDescription

	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Close

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;

		return true;
	}

	/**
	 * Reverse Correction - same void
	 * 
	 * @return true if success
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return voidIt();
	} // reverseCorrectionIt

	/**
	 * Reverse Accrual - none
	 * 
	 * @return false
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		return false;
	} // reverseAccrualIt

	@Override
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		// TODO palce coding reActivateIt here

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/*************************************************************************
	 * Get Summary
	 * 
	 * @return Summary of Document
	 */
	public String getSummary()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		if (m_ROrder != null)
			sb.append(" (#").append(m_ROrder.length).append(")");
		// - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		return sb.toString();
	} // getSummary

	/**************************************************************************
	 * String Representation
	 * 
	 * @return info
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer("MPackingSlipReturn[").append(get_ID()).append("-")
						.append(getDocumentNo()).append(", PackingList_ID=")
						.append(getUNS_PackingList().getUNS_PackingList_ID()).append("]");
		return sb.toString();
	} // toString

	/**
	 * Get Document Info
	 * 
	 * @return document info (untranslated)
	 */
	public String getDocumentInfo()
	{
		return "Packing List :" + getDocumentNo();
	} // getDocumentInfo

	@Override
	public File createPDF()
	{
		try
		{
			File temp = File.createTempFile(get_TableName() + get_ID() + "_", ".pdf");
			return createPDF(temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;

	}

	/**
	 * Create PDF file
	 * 
	 * @param file output file
	 * @return file if success
	 */
	public File createPDF(File file)
	{
		ReportEngine re = ReportEngine.get(getCtx(), ReportEngine.ORDER, get_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if (format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo("", format.getJasperProcess_ID());
			pi.setRecord_ID(get_ID());
			pi.setIsBatch(true);

			ServerProcessCtl.process(pi, null);

			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	} // createPDF

	@Override
	public String getProcessMsg()
	{

		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID()
	{

		return getUpdatedBy();
	}

	@Override
	public BigDecimal getApprovalAmt()
	{
		return Env.ZERO;
	}

	@Override
	public int getC_Currency_ID()
	{
		return 0;
	}

	/**************************************************************************
	 * Get Orders
	 * 
	 * @param whereClause where clause or null (starting with AND)
	 * @param orderClause order clause
	 * @return orders
	 */
	public MUNSPLReturnOrder[] getROders(String whereClause, String orderClause)
	{
		// red1 - using new Query class from Teo / Victor's MDDOrder.java implementation
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLReturnOrder.COLUMNNAME_UNS_PL_Return_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (orderClause.length() == 0)
			orderClause = MUNSPLReturnOrder.COLUMNNAME_UNS_PL_ReturnOrder_ID;
		//
		List<MUNSPLReturnOrder> list =
				Query
						.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLReturnOrder.Table_Name,
								whereClauseFinal.toString(), get_TrxName()).setParameters(get_ID())
						.setOrderBy(orderClause).list();

		return list.toArray(new MUNSPLReturnOrder[list.size()]);
	} // getLines

	/**
	 * Get Lines of Order
	 * 
	 * @param requery requery
	 * @param orderBy optional order by column
	 * @return lines
	 */
	public MUNSPLReturnOrder[] getROders(boolean requery, String orderBy)
	{
		if (m_ROrder != null && !requery)
		{
			set_TrxName(m_ROrder, get_TrxName());
			return m_ROrder;
		}
		//
		String orderClause = "";
		if (orderBy != null && orderBy.length() > 0)
			orderClause += orderBy;

		m_ROrder = getROders(null, orderClause);
		return m_ROrder;
	} // getLines

	/**
	 * Get Lines of Order. (used by web store)
	 * 
	 * @return lines
	 */
	public MUNSPLReturnOrder[] getROders()
	{
		return getROders(false, null);
	} // getLines

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{

		return super.beforeSave(newRecord);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete()
	{
		for (MUNSPLReturnOrder rorder : getROders())
		{
			if (!rorder.delete(true))
				return false;
		}

		return super.beforeDelete();
	}

	public static void createPLReturn(MUNSPackingList pl)
	{
		MUNSPLReturn plr = new MUNSPLReturn(pl.getCtx(), 0, pl.get_TrxName());

		plr.initReturn(pl);
		plr.saveEx();

		for (MUNSPackingListOrder order : pl.getOrders())
		{
			MUNSPLReturnOrder plro = new MUNSPLReturnOrder(order.getCtx(), 0, order.get_TrxName());
			plro.setPLOrder(plr);
			plro.setUNS_PackingList_Order_ID(order.get_ID());
			plro.saveEx();
		}
	}

	private void initReturn(MUNSPackingList pl)
	{
		setClientOrg(pl);
		setUNS_PackingList_ID(pl.get_ID());
	}
	
	@Override
	public void setProcessed(boolean processed) {
		for (MUNSPLReturnOrder order : getROders(true, "")){
			order.setProcessed(processed);
			order.saveEx();
		}
		
		super.setProcessed(processed);
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing, String orderType,
			String isSOTrx, int AD_Table_ID, String[] docAction, String[] options, int index)
	{
		if (docStatus.equals(DocAction.STATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		return index;
	}
	
	
	public String moveItems()
	{
		StringBuilder sb = new StringBuilder();
		MUNSPLConfirm[] confirms = MUNSPLConfirm.gets(this);
		for(MUNSPLConfirm confirm : confirms)
		{
			String error = confirm.move();
			if(Util.isEmpty(error, true))
				continue;
			
			sb.append(error);
		}
		
		String msg = sb.toString();
		if(!Util.isEmpty(msg, true))
			return msg;
		
		return null;
	}
}