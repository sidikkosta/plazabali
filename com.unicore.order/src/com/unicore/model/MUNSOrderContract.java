/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.MBPBankAccount;
import org.compiere.model.MDocType;
import org.compiere.model.MOrder;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;


/**
 * @author AzHaidar
 *
 */
public class MUNSOrderContract extends X_UNS_Order_Contract 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5141743507026801279L;
	
	private MOrder m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_Order_Contract_ID
	 * @param trxName
	 */
	public MUNSOrderContract(Properties ctx, int UNS_Order_Contract_ID,
			String trxName) {
		super(ctx, UNS_Order_Contract_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSOrderContract(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	/**
	 * Get instance of MUNSOrderContract for the given C_Order_ID.
	 * @param ctx
	 * @param C_Order_ID
	 * @param trxName
	 * @return
	 */
	public static MUNSOrderContract getOfOrder(Properties ctx, int C_Order_ID, String trxName)
	{
		return Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, "C_Order_ID=?", trxName)
				.setParameters(C_Order_ID)
				.first();
	}
	
	/**
	 * The contract has compulsory deduction if the fresh/backlog compulsory deduction not zero.
	 * @return
	 */
	public boolean hasCompulsoryDeduction()
	{
		return getFreshCompulsoryDeduction().signum() > 0 || getBacklogCompulsoryDeduction().signum() > 0;
	}
	
	/**
	 * 
	 */
	public MOrder getParent(boolean... requery)
	{
		if ((m_parent == null && getC_Order_ID() > 0) || (requery != null && requery[0]))
			m_parent = new com.unicore.base.model.MOrder(getCtx(), getC_Order_ID(), get_TrxName());
		
		return m_parent;
	}
	
	@Override
	public MOrder getParent()
	{
		return getParent(false);
	}

    @Override
    public boolean beforeSave(boolean newRecord) 
    {
    	if (newRecord)
    	{
	       	String sql = "SELECT 1 FROM UNS_Order_Contract WHERE C_Order_ID=" + getC_Order_ID();
	       	boolean contractCreated = DB.getSQLValueEx(get_TrxName(), sql) > 0;
	       	
	       	if (contractCreated) 
	       	{
	       		log.saveError("OnlyOneAdditionalContractAllowed", 
	       					  "You can only create one additional contract line.");
	       		return false;
	       	}
    	}
    	
    	MDocType dt = MDocType.get(getCtx(), getParent().getC_DocTypeTarget_ID());
    	
    	if (dt.getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_SalesContract))
    	{
    		if (getC_BankAccount_ID() == 0)
    			throw new AdempiereUserError("No Bank Account selected.");
    	}
    	
    	if(getC_BP_BankAccount_ID() <= 0)
    	{
    		int C_BPartner_ID = getC_Order().getC_BPartner_ID();
    		
    		String check = "SELECT C_BP_BankAccount_ID FROM C_BP_BankAccount WHERE C_Bank_ID=? AND A_Name =?"
    				+ " AND C_BPartner_ID=?";
    		int C_BP_BankAccount_ID = DB.getSQLValue(get_TrxName(), check, new Object[]{getC_Bank_ID(), getBeneficiary()
    			, C_BPartner_ID});
    		
    		if(C_BP_BankAccount_ID <= 0)
    		{
    			MBPBankAccount account = new MBPBankAccount(getCtx(), 0, get_TrxName());
    			account.setC_BPartner_ID(C_BPartner_ID);
    			account.setIsACH(true);
    			account.setC_Bank_ID(getC_Bank_ID());
    			account.setA_Name(getBeneficiary());
    			account.setA_Street(getBankAddress());
    			account.setAccountNo(getAccountNo());
    			if(!account.save())
    			{
    				log.saveError("Error", Msg.parseTranslation(getCtx(), "Failed when trying create new BP Bank Account"));
    				return false;
    			}
    			C_BP_BankAccount_ID = account.get_ID();
    		}
    		setC_BP_BankAccount_ID(C_BP_BankAccount_ID);
    	}

       	return true;
    }
}
