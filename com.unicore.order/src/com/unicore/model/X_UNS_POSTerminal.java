/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_POSTerminal
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSTerminal extends PO implements I_UNS_POSTerminal, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180724L;

    /** Standard Constructor */
    public X_UNS_POSTerminal (Properties ctx, int UNS_POSTerminal_ID, String trxName)
    {
      super (ctx, UNS_POSTerminal_ID, trxName);
      /** if (UNS_POSTerminal_ID == 0)
        {
			setMACAddress (null);
			setName (null);
			setStore_ID (0);
			setStoreType (null);
			setUNS_POSTerminal_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSTerminal (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSTerminal[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set MAC Address.
		@param MACAddress MAC Address	  */
	public void setMACAddress (String MACAddress)
	{
		set_Value (COLUMNNAME_MACAddress, MACAddress);
	}

	/** Get MAC Address.
		@return MAC Address	  */
	public String getMACAddress () 
	{
		return (String)get_Value(COLUMNNAME_MACAddress);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_Value (COLUMNNAME_Store_ID, null);
		else 
			set_Value (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Retail Food And Beverage = RFB */
	public static final String STORETYPE_RetailFoodAndBeverage = "RFB";
	/** Retail Duty Free = RDF */
	public static final String STORETYPE_RetailDutyFree = "RDF";
	/** Retail Duty Paid = RDP */
	public static final String STORETYPE_RetailDutyPaid = "RDP";
	/** Set Store Type.
		@param StoreType Store Type	  */
	public void setStoreType (String StoreType)
	{

		set_Value (COLUMNNAME_StoreType, StoreType);
	}

	/** Get Store Type.
		@return Store Type	  */
	public String getStoreType () 
	{
		return (String)get_Value(COLUMNNAME_StoreType);
	}

	/** Set POS Terminal.
		@param UNS_POSTerminal_ID POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID)
	{
		if (UNS_POSTerminal_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminal_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminal_ID, Integer.valueOf(UNS_POSTerminal_ID));
	}

	/** Get POS Terminal.
		@return POS Terminal	  */
	public int getUNS_POSTerminal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTerminal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSTerminal_UU.
		@param UNS_POSTerminal_UU UNS_POSTerminal_UU	  */
	public void setUNS_POSTerminal_UU (String UNS_POSTerminal_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSTerminal_UU, UNS_POSTerminal_UU);
	}

	/** Get UNS_POSTerminal_UU.
		@return UNS_POSTerminal_UU	  */
	public String getUNS_POSTerminal_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSTerminal_UU);
	}
}