/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Burhani Adam
 *
 */
public class MUNSShortCashierCorrLine extends X_UNS_ShortCashierCorr_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3716962524138199L;

	/**
	 * @param ctx
	 * @param UNS_ShortCashierCorr_Line_ID
	 * @param trxName
	 */
	public MUNSShortCashierCorrLine(Properties ctx,
			int UNS_ShortCashierCorr_Line_ID, String trxName) {
		super(ctx, UNS_ShortCashierCorr_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSShortCashierCorrLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(getCorrectionAmt().compareTo(getOriginalAmt()) == 0)
		{
			log.saveError("Error", "Original Amount = Correction Amount");
			return false;
		}
		
		if((getCorrectionAmt().signum() != 0 && getCorrectionAmtB1().signum() == 0 && getCorrectionAmtB2().signum() == 0)
				||
			(getCorrectionAmt().signum() == 0 && getCorrectionAmtB1().signum() != 0 && getCorrectionAmtB2().signum() == 0)
				||
			(getCorrectionAmt().signum() == 0 && getCorrectionAmtB1().signum() == 0 && getCorrectionAmtB2().signum() != 0)
				||
			(getCorrectionAmt().signum() != 0 && getCorrectionAmtB1().signum() != 0 && getCorrectionAmtB2().signum() == 0)
				||
			(getCorrectionAmt().signum() == 0 && getCorrectionAmtB1().signum() != 0 && getCorrectionAmtB2().signum() != 0)
				||
			(getCorrectionAmt().signum() != 0 && getCorrectionAmtB1().signum() == 0 && getCorrectionAmtB2().signum() != 0))
		{
			log.saveError("Error", "Invalid input correction amount, correction amount base 1, correction amount base 2.");
			return false;
		}
		
		return true;
	}
}