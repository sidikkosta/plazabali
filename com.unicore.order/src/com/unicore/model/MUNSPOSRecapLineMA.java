/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPOSRecapLineMA extends X_UNS_POSRecapLine_MA {

	/**
	 * 
	 */
	private static final long serialVersionUID = -159340604895237852L;

	/**
	 * @param ctx
	 * @param UNS_POSRecapLine_MA_ID
	 * @param trxName
	 */
	public MUNSPOSRecapLineMA(Properties ctx, int UNS_POSRecapLine_MA_ID,
			String trxName) {
		super(ctx, UNS_POSRecapLine_MA_ID, trxName);
		if (UNS_POSRecapLine_MA_ID != 0)
			throw new IllegalArgumentException("Multi-Key");
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSRecapLineMA(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSPOSRecapLineMA(MUNSPOSRecapLine parent)
	{
		this(parent.getCtx(), 0, parent.get_TrxName());
		
		setAD_Org_ID(parent.getAD_Org_ID());
		setUNS_POSRecapLine_ID(parent.get_ID());
		setQty(Env.ZERO);
	}
	
	public static MUNSPOSRecapLineMA getCreate(Properties ctx, MUNSPOSRecapLine parent,
			int M_AttributeSetInstance_ID, Timestamp date, int M_Locator_ID, String trxName)
	{
		String whereClause = "UNS_POSRecapLine_ID = ? AND M_AttributeSetInstance_ID = ?"
				+ " AND DateMaterialPolicy = ? AND M_Locator_ID = ?";
		MUNSPOSRecapLineMA ma = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(parent.get_ID(), M_AttributeSetInstance_ID, date, M_Locator_ID
						)
					.firstOnly();
		
		if(ma == null)
		{
			ma = new MUNSPOSRecapLineMA(parent);
			ma.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
			ma.setDateMaterialPolicy(date);
			ma.setM_Locator_ID(M_Locator_ID);
		}
		
		return ma;
	}
	
	public static BigDecimal getManualQty (int UNS_POSRecapLine_ID, String trxName)
	{
		String sql = "SELECT SUM(Qty) FROM UNS_POSRecapLine_MA ma WHERE "
				+ " ma.UNS_POSRecapLine_ID = ?";
		BigDecimal totalQty = DB.getSQLValueBD(trxName, sql, UNS_POSRecapLine_ID);
		return totalQty==null?Env.ZERO:totalQty;
	}
}