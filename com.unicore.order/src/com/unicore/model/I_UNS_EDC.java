/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_EDC
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_EDC 
{

    /** TableName=UNS_EDC */
    public static final String Table_Name = "UNS_EDC";

    /** AD_Table_ID=1000390 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BankAccount_ID */
    public static final String COLUMNNAME_C_BankAccount_ID = "C_BankAccount_ID";

	/** Set Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID);

	/** Get Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID();

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException;

    /** Column name Communication */
    public static final String COLUMNNAME_Communication = "Communication";

	/** Set Communication	  */
	public void setCommunication (String Communication);

	/** Get Communication	  */
	public String getCommunication();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name EDCType */
    public static final String COLUMNNAME_EDCType = "EDCType";

	/** Set EDC Type	  */
	public void setEDCType (String EDCType);

	/** Get EDC Type	  */
	public String getEDCType();

    /** Column name IDNo */
    public static final String COLUMNNAME_IDNo = "IDNo";

	/** Set ID No	  */
	public void setIDNo (String IDNo);

	/** Get ID No	  */
	public String getIDNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name UNS_EDC_ID */
    public static final String COLUMNNAME_UNS_EDC_ID = "UNS_EDC_ID";

	/** Set EDC	  */
	public void setUNS_EDC_ID (int UNS_EDC_ID);

	/** Get EDC	  */
	public int getUNS_EDC_ID();

    /** Column name UNS_EDCProcessor_ID */
    public static final String COLUMNNAME_UNS_EDCProcessor_ID = "UNS_EDCProcessor_ID";

	/** Set EDC Processor	  */
	public void setUNS_EDCProcessor_ID (int UNS_EDCProcessor_ID);

	/** Get EDC Processor	  */
	public int getUNS_EDCProcessor_ID();

    /** Column name UNS_EDC_UU */
    public static final String COLUMNNAME_UNS_EDC_UU = "UNS_EDC_UU";

	/** Set UNS_EDC_UU	  */
	public void setUNS_EDC_UU (String UNS_EDC_UU);

	/** Get UNS_EDC_UU	  */
	public String getUNS_EDC_UU();

    /** Column name UNS_POSTerminal_ID */
    public static final String COLUMNNAME_UNS_POSTerminal_ID = "UNS_POSTerminal_ID";

	/** Set POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID);

	/** Get POS Terminal	  */
	public int getUNS_POSTerminal_ID();

	public com.unicore.model.I_UNS_POSTerminal getUNS_POSTerminal() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
