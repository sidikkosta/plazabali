package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttribute;
import org.compiere.model.MAttributeInstance;
import org.compiere.model.MAttributeSet;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MBPartner;
import org.compiere.model.MClient;
import org.compiere.model.MCurrency;
import org.compiere.model.MDocType;
import org.compiere.model.MPeriod;
import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.ErrorMsg;
import com.uns.util.UNSApps;

public class MUNSPOSTrx extends X_UNS_POSTrx implements DocAction, DocOptions {
	
	private MUNSPOSSession m_session = null;
	private MUNSPOSTrxLine[] m_lines = null;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private String m_docBaseType = null;
	
	public static void main (String[] args)
	{
		Timestamp x = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat y = new SimpleDateFormat("YY");
		System.out.println(y.format(x));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1112552166234024386L;

	public MUNSPOSTrx(Properties ctx, int UNS_POSTrx_ID, String trxName) 
	{
		super(ctx, UNS_POSTrx_ID, trxName);
	}

	public MUNSPOSTrx(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (DOCSTATUS_Completed.equals(docStatus))
		{
			options[index++] = DOCACTION_Reverse_Correct;
			options[index++] = DOCACTION_Reverse_Accrual;
		}
		
		return index;
	}

	@Override
	public boolean processIt(String action) throws Exception 
	{
		addLog(Level.INFO, "Process Document");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() 
	{
		addLog(Level.INFO, "Unlock it");
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		addLog(Level.INFO, "Invalidate it");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		addLog(Level.INFO, "Preparing Document...");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}

		if (DOCSTATUS_Completed.equals(getSession().getDocStatus())
				|| DOCSTATUS_Closed.equals(getSession().getDocStatus())
				|| DOCSTATUS_Reversed.equals(getSession().getDocStatus())
				|| DOCSTATUS_Voided.equals(getSession().getDocStatus()))
		{
			m_processMsg = "Could not update session. session completed. Session name " + getSession().getName();
			return DOCSTATUS_Invalid;
		}
		//TODO Write check Code here
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_justPrepared=true;
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		addLog(Level.INFO, "Approve it");
		setProcessed(true);
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		addLog(Level.INFO, "Reject It");
		setProcessed(false);
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		if (log.isLoggable(Level.INFO)) log.info("Complete Document");
		
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DOCSTATUS_InProgress.equals(status))
			{
				return DOCSTATUS_Invalid;
			}
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		if(Util.isEmpty(getTrxNo()))
		{
			setTrxNo(MUNSPOSSequence.getNextSequence(get_TrxName(), getAD_Org_ID(), getC_DocType_ID(), getDateAcct()));
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		addLog(Level.INFO, "Void-it");
		if (DOCSTATUS_Closed.equals(getDocStatus())
				|| DOCSTATUS_Reversed.equals(getDocStatus())
				|| DOCSTATUS_Voided.equals(getDocStatus()))
		{
			m_processMsg = "Document closed";
			return false;
		}
		else if (DOCSTATUS_Completed.equals(getDocStatus()))
		{
			String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?";
			String dbType = DB.getSQLValueString(get_TrxName(), sql, getC_DocType_ID());
			boolean isPeriodOpen = MPeriod.isOpen(getCtx(), getDateAcct(), dbType, getAD_Org_ID());
			
			if (isPeriodOpen)
			{
				isPeriodOpen = DOCSTATUS_InProgress.equals(getSession().getDocStatus());
			}
			
			if (isPeriodOpen)
			{
				return reverseCorrectIt();
			}
			else
			{
				return reverseAccrualIt();
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		getLines(true);
		String salesName = Env.getContext(getCtx(), "#AD_User_Name");
		String text = "Voided by : " + salesName + "; Grand Total = " + getGrandTotal();
		addDescription(text);
		
		for (int i=0; i<m_lines.length; i++)
		{
			MUNSPOSTrxLine line = m_lines[i];
			
			StringBuilder desc = new StringBuilder("Voided By ").append(salesName)
					.append("; Qty Entered = ").append(line.getQtyEntered())
					.append("; Qty Bonuses = ").append(line.getQtyBonuses())
					.append("; Total Qty = ").append(line.getQtyOrdered())
					.append("; Total Line Net Amt = ").append(line.getLineNetAmt());
			
			String description = desc.toString();
			line.addDescription(description);
			line.setQtyEntered(Env.ZERO);
			line.setQtyBonuses(Env.ZERO);
			if (!line.save())
			{
				m_processMsg = CLogger.retrieveErrorString("Could not save line.");
				return false;
			}
		}
		
		if (!isReturn())
		{
			String sql = "UPDATE UNS_POSFNBTableLine SET IsReserved = ? WHERE UNS_POSFNBTableLine_ID = ?";
			int updateOK = DB.executeUpdate(sql, new Object[]{"N", getUNS_POSFNBTableLine_ID()}, false, get_TrxName());
			if (updateOK == -1)
				return false;	
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		addLog(Level.INFO, "Close it");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(
				this, ModelValidator.TIMING_AFTER_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean reverseCorrectIt() 
	{
		m_processMsg = "Invalid action Reverse Correct";
		return false;
	}

	@Override
	public boolean reverseAccrualIt() 
	{
		m_processMsg = "Invalid action Reverse Accrual";
		return true;
	}

	@Override
	public boolean reActivateIt() 
	{
		m_processMsg = "Reactivate is not implemented!";
		return false;
	}

	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder(getDocumentNo())
		.append("#Date Doc : ").append(getDateDoc()).append("#Sales Rep : ")
		.append(Env.getContext(getCtx(), "#AD_User_Name")).append("#GrandTotal : ")
		.append(getGrandTotal());
		
		String summary = sb.toString();
		
		return summary;
	}

	@Override
	public String getDocumentInfo() 
	{
		return getSummary();
	}

	@Override
	public File createPDF() 
	{
		addLog(Level.INFO, "Not implemented method MUNSPOSTrx.createPDF ()");
		return null;
	}

	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() 
	{
		return getGrandTotal();
	}
	
	public void setSession (MUNSPOSSession session)
	{
		setUNS_POS_Session_ID(session.get_ID());
		setC_Currency_ID(session.getC_Currency_ID());
		setM_PriceList_ID(session.getM_PriceList_ID());
		m_session = session;
	}
	
	public MUNSPOSTrx (MUNSPOSSession session)
	{
		this (session.getCtx(), 0, session.get_TrxName());
		setClientOrg(session);
		setSession(session);
	}
	
	public MUNSPOSSession getSession ()
	{
		if (null == m_session)
		{
			m_session = new MUNSPOSSession(getCtx(), getUNS_POS_Session_ID(), 
					get_TrxName());
		}
		
		return m_session;
	}
	
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public MUNSPOSTrxLine[] getLines (boolean requery)
	{
		if (null != this.m_lines && !requery)
		{
			set_TrxName(this.m_lines, get_TrxName());
			
			return this.m_lines;
		}
		
		String whereClause = "UNS_POSTrx_ID = ? AND IsActive = ?";
		List<MUNSPOSTrxLine> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				"UNS_POSTrxLine", whereClause, get_TrxName()).
				setParameters(get_ID(), "Y").setOrderBy("Line").
				list();
		
		this.m_lines = new MUNSPOSTrxLine[list.size()];
		list.toArray(this.m_lines);
		
		return this.m_lines;
	}
	
	/**
	 * 
	 * @param line
	 * @param movementQty
	 * @return
	 */
	public boolean checkMaterialPolicy (MUNSPOSTrxLine line, BigDecimal movementQty)
	{
		if (!MUNSPOSTrxLineMA.delete(line.get_ID(), true, get_TrxName()))
		{
			m_processMsg = CLogger.retrieveErrorString("Could not remove ma.");
			return false;
		}
		else if (movementQty.signum() == 0)
		{
			return true;
		}
		
		MProduct product = MProduct.get(getCtx(), line.getM_Product_ID());
		BigDecimal qtyToDeliver = movementQty;
		
		String docbasetype = DB.getSQLValueString(get_TrxName(), 
				"SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?", getC_DocType_ID());
		if (MDocType.DOCBASETYPE_POSReturn.equals(docbasetype) && line.getReference_ID() > 0)
		{
			MUNSPOSTrxLineMA[] mas = MUNSPOSTrxLineMA.gets(line.getReference_ID(), get_TrxName());
			for (int i=0; qtyToDeliver.signum() != 0 && i<mas.length; i++)
			{
				BigDecimal returnable = mas[i].getMovementQty();
				returnable = returnable.negate();
				BigDecimal move = qtyToDeliver;
				if (move.compareTo(returnable) == -1)
					move = returnable;
				
				MUNSPOSTrxLineMA newMA = new MUNSPOSTrxLineMA(
						line, mas[i].getM_AttributeSetInstance_ID(), 
						mas[i].getDateMaterialPolicy(), move, 
						true);
				newMA.setM_Locator_ID(mas[i].getM_Locator_ID());
				if (!newMA.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Could not save MA.");
					return false;
				}
				
				qtyToDeliver = qtyToDeliver.subtract(move);
			}
		}
		else
		{
			String mmPolicy = product.getMMPolicy();
			Timestamp minGuarantedDate = getDateTrx();
			MStorageOnHand[] storages = MStorageOnHand.getWarehouse(
					getCtx(), getM_Warehouse_ID(), line.getM_Product_ID(), 
					0, minGuarantedDate, 
					MClient.MMPOLICY_FiFo.equals(mmPolicy), true, 
					0, get_TrxName());
			
			for (int i=0; i<storages.length; i++)
			{
				MStorageOnHand storage = storages[i];
				String sql = "SELECT IsIntransit FROM M_Locator WHERE M_Locator_ID = ?";
				String intransit = DB.getSQLValueString(get_TrxName(), sql, storage.getM_Locator_ID());
				boolean isIntransit = "Y".equals(intransit);
				if (isIntransit)
					continue;
				if (!Util.isEmpty(line.getETBBCode(), true))
				{
					sql = "SELECT Description FROM M_AttributeSetInstance WHERE M_AttributeSetInstance_ID = ?";
					String desc = DB.getSQLValueString(get_TrxName(), sql, storage.getM_AttributeSetInstance_ID());
					if (!line.getETBBCode().equals(desc))
						continue;
				}
				BigDecimal qtyOnHand = storage.getQtyOnHand();
				if (qtyOnHand.compareTo(qtyToDeliver) >= 0)
				{
					MUNSPOSTrxLineMA ma = new MUNSPOSTrxLineMA(
							line, storage.getM_AttributeSetInstance_ID(), 
							storage.getDateMaterialPolicy(), qtyToDeliver, true);
					ma.setM_Locator_ID(storage.getM_Locator_ID());
					if (!ma.save())
					{
						m_processMsg = CLogger.retrieveErrorString("Could not save MA.");
						return false;
					}
					qtyToDeliver = Env.ZERO;
				}
				else
				{
					MUNSPOSTrxLineMA ma = new MUNSPOSTrxLineMA(
							line, storage.getM_AttributeSetInstance_ID(), 
							storage.getDateMaterialPolicy(), qtyOnHand, true);
					ma.setM_Locator_ID(storage.getM_Locator_ID());
					if (!ma.save())
					{
						m_processMsg = CLogger.retrieveErrorString("Could not save MA.");
						return false;
					}
					qtyToDeliver = qtyToDeliver.subtract(qtyOnHand);
				}
				
				if (qtyToDeliver.signum() == 0)
				{
					break;
				}
			}
		
			
			if (qtyToDeliver.signum() != 0)
			{
				MAttributeSetInstance instance = null;
				String desc = line.getETBBCode();
				if (desc != null && desc.length() == 8)
				{
					String sql = "SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance"
							+ " WHERE Description = ? AND M_AttributeSet_ID = ?";
					int asiID = DB.getSQLValue(get_TrxName(), sql, desc, product.getM_AttributeSet_ID());
					if (asiID > 0)
						instance = new MAttributeSetInstance(getCtx(), asiID, get_TrxName());
					else
					{
						instance = MAttributeSetInstance.create(
								getCtx(), product, get_TrxName());
						MAttributeSet attrSet = instance.getMAttributeSet();
						MAttribute[] attrs = attrSet.getMAttributes(true);
						String bcCode = desc.substring(0, 5);
						String year = desc.substring(5, 6);
						String port = desc.substring(6,8);
						
						for (int i=0; i<attrs.length; i++)
						{
							String val = null;
							if (attrs[i].getName().equalsIgnoreCase("BC Code"))
							{
								val = bcCode;
							}
							else if (attrs[i].getName().equalsIgnoreCase("Year"))
							{
								val = year;
							}
							else if (attrs[i].getName().equalsIgnoreCase("Port"))
							{
								val = port;
							}
							else
								continue;
							MAttributeInstance attInstance = MAttributeInstance.getNoSave(
									getCtx(), instance.getM_AttributeSetInstance_ID(),
									attrs[i].getM_Attribute_ID(), get_TrxName());
							if (null == attInstance)
								continue;

							attInstance.setValue(val);
							attInstance.saveEx();
						}
						
						instance.setDescription();
						instance.saveEx();
						instance.setCreated(getDateAcct());
						instance.load(get_TrxName());
					}
				}
				
				MUNSPOSTrxLineMA ma = MUNSPOSTrxLineMA.addOrCreate(
						line, instance == null ? 0 : instance.get_ID(), 
								qtyToDeliver, instance == null ? getDateAcct() : instance.getCreated(), 
										true); 
				String sql = "SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = ? ORDER BY IsDefault DESC";
				int locID = DB.getSQLValue(get_TrxName(), sql, getM_Warehouse_ID());
				ma.setM_Locator_ID(locID);
				if (!ma.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Could not save MA.");
					return false;
				}
			}
		}
		
		return true;
	}
	
	private void addLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		
		log.log(level, msg);
	}
	
	public void addDescription (String text)
	{
		if (null == text)
		{
			return;
		}
		
		StringBuilder desc = new StringBuilder();
		if (null != getDescription())
		{
			desc.append(getDescription());
			desc.append(" | ");
		}
		
		setDescription(desc.toString());
	}
	
	public boolean isComplete ()
	{
		return DOCSTATUS_Completed.equals(getDocStatus())
				|| DOCSTATUS_Closed.equals(getDocStatus())
				|| DOCSTATUS_Reversed.equals(getDocStatus())
				|| DOCSTATUS_Voided.equals(getDocStatus());
	}
	
	public boolean deleteLines ()
	{
		MUNSPOSTrxLine[] lines = getLines(true);
		for(int i=0; i<lines.length; i++)
		{
			if(!lines[i].delete(true))
				return false;
		}
//		String sql = "DELETE FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = ?";
//		int result = DB.executeUpdate(sql, get_ID(), get_TrxName());
//		boolean success = result != -1;
		return true;
	}
	
	public boolean deleteLine (int UNS_POSTrxLine_ID)
	{
		boolean success = true;
		MUNSPOSTrxLine line = new MUNSPOSTrxLine(
				getCtx(), UNS_POSTrxLine_ID, get_TrxName());
		if(!line.delete(true))
			success = false;
//		String sql = "DELETE FROM UNS_POSTrxLine WHERE UNS_POSTrxLine_ID = ?";
//		int result = DB.executeUpdate(sql, UNS_POSTrxLine_ID, get_TrxName());
//		boolean success = result != -1;
		if (success)
		{
			if (doUpdate())
				load(get_TrxName());
			else
				success = false;
		}
		
		return success;
	}
	
	@Override
	protected boolean beforeDelete ()
	{
		if (isProcessed())
		{
			ErrorMsg.setErrorMsg(getCtx(), "SaveError", "Record Processed!");
			return false;
		}
		else if (!deleteLines())
		{
			String lastError = CLogger.retrieveErrorString("::UnknownError::");
			ErrorMsg.setErrorMsg(getCtx(), "Could not delete lines.",
					"Failed when try to delete lines. " + lastError);
			return false;
		}
		
		if (!isReturn())
		{
			String sql = "UPDATE UNS_POSFNBTableLine SET IsReserved = ? WHERE UNS_POSFNBTableLine_ID = ?";
			int updateOK = DB.executeUpdate(sql, new Object[]{"N", getUNS_POSFNBTableLine_ID()}, false, get_TrxName());
			if (updateOK == -1)
				return false;	
		}
		return super.beforeDelete();
	}
	
	public void setBPartner (MBPartner partner)
	{
		if (null == partner)
		{
			return;
		}
		
		setC_BPartner_ID(partner.get_ID());
		if (partner.getM_PriceList_ID() > 0)
		{
			setM_PriceList_ID(partner.getM_PriceList_ID());
		}
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(UNSApps.SERVER_TYPE.equals(UNSApps.SERVER_MAIN)
				&& getUNS_POS_Session_ID() > 0)
			setUNS_POSTerminal_ID(getUNS_POS_Session().getUNS_POSTerminal_ID());
		
		if(isReplication())
			return true;
		if(getC_BPartner_ID() < 0)
		{
			log.saveError("Error", "Business Partner is Mandatory Not Null");
			return false;
		}
		String desc = getDescription();
		if (desc == null)
			desc = "";
		
		if(!desc.contains("**") && !desc.contains("Import"))
		{
			if (isReturn() && get_Value("Reference_ID") == null)
			{
				log.saveError("SaveError", "Mandatory field Reference");
				return false;
			}
		}
		else
		{
			if(!desc.contains("RESEND"))
				setDescription(desc.concat("-RESEND"));
		}
		
		String sql;
		
		if (getC_BPartner_Location_ID() == 0)
		{
			sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE C_BPartner_ID = ?";
			int locationID = DB.getSQLValue(get_TrxName(), sql, getC_BPartner_ID());
			setC_BPartner_Location_ID(locationID);
		}
		
		if (is_ValueChanged(COLUMNNAME_DiscountAmt) || is_ValueChanged(COLUMNNAME_TotalAmt))
		{
			if (getTotalAmt().signum() == 0)
			{
//				setDiscount(Env.ZERO);
				setDiscountAmt(Env.ZERO);
			}
			else
			{
				BigDecimal disc = getDiscountAmt().divide(getTotalAmt(), 12, RoundingMode.HALF_DOWN).multiply(Env.ONEHUNDRED);
				setDiscount(disc);
			}
		}
		else if (is_ValueChanged(COLUMNNAME_Discount))
		{
			int prc = MCurrency.getStdPrecision(getCtx(), getC_Currency_ID());
			BigDecimal discAmt = (getDiscount().multiply(getTotalAmt())).
					divide(Env.ONEHUNDRED, prc, RoundingMode.HALF_DOWN);
			setDiscountAmt(discAmt);
		}
			
		setGrandTotal();
		if (null == getDateTrx())
		{
			setDateTrx(new Timestamp(System.currentTimeMillis()));
		}
		
		if (!isReturn() && (newRecord || is_ValueChanged(COLUMNNAME_UNS_POSFNBTableLine_ID)))
		{
			sql = "UPDATE UNS_POSFNBTableLine SET IsReserved = ? WHERE UNS_POSFNBTableLine_ID = ?";
			int updateOK = DB.executeUpdate(sql, new Object[]{"Y", getUNS_POSFNBTableLine_ID()}, false, get_TrxName());
			if (updateOK == -1)
				return false;
			Object oo = get_ValueOld(COLUMNNAME_UNS_POSFNBTableLine_ID);
			if (oo != null && (Integer) 00 != getUNS_POSFNBTableLine_ID())
				updateOK = DB.executeUpdate(sql, new Object[]{"N", (Integer)oo}, false, get_TrxName());
			if (updateOK == -1)
				return false;
		}
		
		return true;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		return true;
	}
	
	@Override
	public void setProcessed (boolean processed)
	{
		String sql = "UPDATE UNS_POSTrxLine SET Processed = ? WHERE UNS_POSTrx_ID = ?";
		int success = DB.executeUpdate(sql, new Object[]{processed ? "Y" : "N", getUNS_POSTrx_ID()}, 
				false, get_TrxName());
		if (success == -1)
		{
			throw new AdempiereException(CLogger.retrieveErrorString("Could not update lines"));
		}
		
		super.setProcessed(processed);
	}
	
	public void addLines (MUNSPOSTrxLine line)
	{
		getLines(false);
		m_lines = Arrays.copyOf(m_lines, m_lines.length +1);
		m_lines[m_lines.length-1] = line;
	}
	
	public static MUNSPOSTrx get (String trxName, String trxNo)
	{
		return Query.get(Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, "TrxNo = ?", 
				trxName).setParameters(trxNo).first();
	}
	
	public boolean doUpdate()
	{
		boolean success = true;
		
		String sql =  "UPDATE UNS_POSTrx p SET "
				+ "TotalAmt = (SELECT COALESCE(SUM(LineNetAmt),0) FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = p.UNS_POSTrx_ID),"
				+ "TaxAmt = (SELECT COALESCE(SUM(TaxAmt),0) FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = p.UNS_POSTrx_ID),"
				+ "ServiceCharge = (SELECT COALESCE(SUM(ServiceCharge),0) FROM UNS_POSTrxLine WHERE UNS_POSTrx_ID = p.UNS_POSTrx_ID)"
				+ "WHERE p.UNS_POSTrx_ID = ?";
		success = DB.executeUpdate(sql,  getUNS_POSTrx_ID(), get_TrxName()) != -1;
		if (!success)
			return false;
		
		sql =  "UPDATE UNS_POSTrx SET "
				+ "DiscountAmt = COALESCE ((Round((Discount*TotalAmt)/100,2)),0) "
				+ "WHERE UNS_POSTrx_ID = ?";
		DB.executeUpdate(sql,  getUNS_POSTrx_ID(), get_TrxName());
		if (isPriceIncludedTax() && isServiceChargeIncludedTax())
		{
			sql =  "UPDATE UNS_POSTrx SET "
					+ "GrandTotal = COALESCE ((TotalAmt-DiscountAmt),0) "
					+ "WHERE UNS_POSTrx_ID = ?";
		}
		else if(isPriceIncludedTax() && !isServiceChargeIncludedTax())
		{
			sql =  "UPDATE UNS_POSTrx SET "
					+ "GrandTotal = COALESCE (((TotalAmt+ServiceCharge)-DiscountAmt),0) "
					+ "WHERE UNS_POSTrx_ID = ?";
		}
		else if(!isPriceIncludedTax() && isServiceChargeIncludedTax())
		{
			sql =  "UPDATE UNS_POSTrx SET "
					+ "GrandTotal = COALESCE (((TotalAmt+TaxAmt)-DiscountAmt),0) "
					+ "WHERE UNS_POSTrx_ID = ?";
		}
		else if(!isPriceIncludedTax() && !isServiceChargeIncludedTax())
		{
			sql =  "UPDATE UNS_POSTrx SET "
					+ "GrandTotal = COALESCE (((TotalAmt+ServiceCharge+TaxAmt)-DiscountAmt),0) "
					+ "WHERE UNS_POSTrx_ID = ?";
		}
		
		success = DB.executeUpdate(sql,  getUNS_POSTrx_ID(), get_TrxName()) != -1;
		
		return success;
	}
	
	public boolean isReturn ()
	{
		if (null == m_docBaseType)
		{
			String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?";
			m_docBaseType = DB.getSQLValueString(get_TrxName(), sql, getC_DocType_ID());
		}
		
		return MDocType.DOCBASETYPE_POSReturn.equals(m_docBaseType);
	}
	
	public String createReconciliation(boolean isRefund, boolean dontUpdateLinePOS)
	{
		MUNSPOSTrxLine[] line = getLines(false);
		MUNSPOSRecap recap = MUNSPOSRecap.getCreate(this);
		if(recap == null)
			return "Failed when trying create reconciliation.";
		for(int i=0;i<line.length;i++)
		{
			if(line[i].getUNS_POSRecapLine_ID() > 0)
				continue;
			MUNSPOSRecapLine recapLine = MUNSPOSRecapLine.getCreate(recap, line[i], isRefund);
			recapLine.setQty(recapLine.getQty().add(line[i].getQtyOrdered()));
			recapLine.setRevenueAmt(recapLine.getRevenueAmt().add(line[i].getLineNetAmt()));
			recapLine.setSalesAmt(recapLine.getSalesAmt().add(line[i].getLineAmt()));
			recapLine.setDiscountAmt(recapLine.getDiscountAmt().add(line[i].getDiscountAmt()));
			recapLine.setTaxAmt(recapLine.getTaxAmt().add(line[i].getTaxAmt()));
			recapLine.setServiceChargeAmt(recapLine.getServiceChargeAmt().add(line[i].getServiceCharge()));
			recapLine.setisRefund(isRefund);
			if(!recapLine.save())
				return "Failed when trying create reconciliation.";
			if (!dontUpdateLinePOS)
			{
				String sql = "UPDATE UNS_POSTrxLine SET UNS_POSRecapLine_ID = ?"
						+ " WHERE UNS_POSTrxLine_ID = ?";
				DB.executeUpdate(sql, new Object[]{recapLine.get_ID(), line[i].get_ID()}, false, get_TrxName());
				line[i].load(get_TrxName());
			}
			
			MUNSPOSTrxLineMA[] mas = MUNSPOSTrxLineMA.gets(line[i].get_ID(), get_TrxName());
			
			for(int j=0;j<mas.length;j++)
			{
				MUNSPOSRecapLineMA ma = MUNSPOSRecapLineMA.getCreate(getCtx(), recapLine, 
						mas[j].getM_AttributeSetInstance_ID(), mas[j].getDateMaterialPolicy(), mas[j].getM_Locator_ID(), get_TrxName());
				ma.setQty(ma.getQty().add(mas[j].getMovementQty()));
				ma.saveEx();
			}
		}
		
		return null;
	}
	
	private MUNSPOSTrx m_originalTrx = null;
	
	public MUNSPOSTrx getOriginalTrx ()
	{
		if (m_originalTrx == null)
			m_originalTrx = new MUNSPOSTrx(getCtx(), getReference_ID(), get_TrxName());
		return m_originalTrx;
	}
	
	public boolean isPriceIncludedTax ()
	{
		String sql = "SELECT IsTaxIncluded FROM M_PriceList WHERE M_PriceList_ID = ?";
		boolean isPriceTaxIncl = "Y".equals(DB.getSQLValueString(get_TrxName(), sql, getM_PriceList_ID()));
		return isPriceTaxIncl;
	}
	
	public boolean isServiceChargeIncludedTax ()
	{
		String sql = "SELECT IsServiceChargeIncluded FROM M_PriceList WHERE M_PriceList_ID = ?";
		boolean isServiceChargeIncl = "Y".equals(DB.getSQLValueString(get_TrxName(), sql, getM_PriceList_ID()));
		return isServiceChargeIncl;
	}
	
	public String generateBOM ()
	{
		MUNSPOSTrxLine[] lines = getLines(false);
		for (int i=0; i<lines.length; i++) 
		{
			if (lines[i].isBOM())
				continue;
		
			MProductBOM[] boms = MProductBOM.getBOMLines(
					getCtx(), lines[i].getM_Product_ID(), get_TrxName());
			
			for (int j=0; j<boms.length; j++) 
			{
				MProduct product = MProduct.get(getCtx(), boms[j].getM_ProductBOM_ID());
				MUNSPOSTrxLine line = new MUNSPOSTrxLine(this);
				line.setM_Product_ID(product.get_ID());
				line.setC_UOM_ID(product.getC_UOM_ID());
				line.setBarcode(product.getValue());
				line.setPrice(Env.ZERO);
				line.setQtyEntered(boms[j].getBOMQty().multiply(lines[i].getQtyOrdered()));
				line.setIsBOM(true);
				if (!line.save())
				{
					return CLogger.retrieveErrorString("Could not save bom lines");
				}
			}
		}
		
		return null;
	}
	
	public boolean fixCancelReplication ()
	{
		setGrandTotal(getGrandTotal().negate());
		setServiceCharge(getServiceCharge().negate());
		setTaxAmt(getTaxAmt().negate());
		setDiscountAmt(getDiscountAmt().negate());
		setTotalAmt(getTotalAmt().negate());
		MUNSPOSTrxLine[] pLines = getLines(false);
		for (int i=0; i<pLines.length; i++)
		{
			pLines[i].setDiscountAmt(pLines[i].getDiscountAmt().negate());
			pLines[i].setQtyOrdered(pLines[i].getQtyOrdered().negate());
			pLines[i].setLineAmt(pLines[i].getLineAmt().negate());
			pLines[i].setLineNetAmt(pLines[i].getLineNetAmt().negate());
			pLines[i].setTaxAmt(pLines[i].getTaxAmt().negate());
			pLines[i].setServiceCharge(pLines[i].getServiceCharge().negate());
			pLines[i].setUNS_POSRecapLine_ID(-1);
			
			if (UNSApps.SERVER_TYPE.equals(UNSApps.SERVER_MAIN))
			{
				MUNSPOSTrxLineMA[] mas = MUNSPOSTrxLineMA.gets(
						pLines[i].getUNS_POSTrxLine_ID(), get_TrxName());
				
				for (int j=0; j<mas.length; j++)
				{
					String sql = "SELECT SUM(MovementQty) FROM M_Transaction"
							+ " WHERE UNS_POSTrxLine_ID = ? AND M_AttributeSetInstance_ID = ?";
					BigDecimal moveQty = DB.getSQLValueBD(get_TrxName(), sql,
							new Object[]{pLines[i].get_ID(), mas[j].getM_AttributeSetInstance_ID()});
					if(moveQty == null)
						moveQty = Env.ZERO;
					moveQty = moveQty.negate();
					if (!MStorageOnHand.add(
							getCtx(), getM_Warehouse_ID(), mas[j].getM_Locator_ID(), 
							pLines[i].getM_Product_ID(), mas[j].getM_AttributeSetInstance_ID(), 
							moveQty, mas[j].getDateMaterialPolicy(), 
							get_TrxName()))
						return false;
				}

				String sql ="DELETE FROM M_Transaction WHERE UNS_POStrxLine_ID = ?";
				int result = DB.executeUpdate(
						sql, pLines[i].getUNS_POSTrxLine_ID(), false, get_TrxName());
				if (result == -1)
					return false;
			}
		}
		

		if (!MUNSPOSSession.updateByPOSTrx(this))
			return false;
		
		if(UNSApps.SERVER_TYPE.equals(UNSApps.SERVER_LOCATION))
		{
			String err = createReconciliation(isReturn(), true);
			if(null != err)
				return false;
		}
		
		return true;
	}
	
	private void initTaxAndServiceCharge ()
	{
		String sql = "SELECT pt.Store_ID FROM UNS_POSTerminal pt"
				+ " INNER JOIN UNS_POS_Session ss ON ss.UNS_POSTerminal_ID = pt.UNS_POSTerminal_ID"
				+ " INNER JOIN UNS_POSTrx trx ON trx.UNS_POS_Session_ID = ss.UNS_POS_Session_ID"
				+ " WHERE trx.UNS_POSTrx_ID = ?";
		int storeID = DB.getSQLValue(get_TrxName(), sql, getUNS_POSTrx_ID());
		MUNSStoreConfiguration config = MUNSStoreConfiguration.get(
				getCtx(), storeID, get_TrxName());
		if (config == null)
			return;
		boolean isServiceCharge = config.isServiceCharge();
		boolean isPB1Tax = config.isPB1Tax();
		BigDecimal svcChargeRate = Env.ZERO;
		BigDecimal taxRate = Env.ZERO;
		BigDecimal taxAmt = Env.ZERO;
		BigDecimal svcChargeAmt = Env.ZERO;
		BigDecimal lineNetAmt =  getGrandTotal();
		int precision = MCurrency.getStdPrecision(getCtx(), getC_Currency_ID());
		if (isServiceCharge)
		{
			svcChargeRate = config.getServiceCharge();
			if(svcChargeRate.signum() != 0)
				svcChargeRate = svcChargeRate.divide(Env.ONEHUNDRED, 10, RoundingMode.UP);
			else
				svcChargeRate = Env.ZERO;
		}
		if (isPB1Tax)
		{
			sql = "SELECT Rate FROM C_Tax WHERE C_Tax_ID = ?";
			taxRate = DB.getSQLValueBD(get_TrxName(), sql, config.getC_Tax_ID());
			if(taxRate.signum() != 0)
				taxRate = taxRate.divide(Env.ONEHUNDRED, 10, RoundingMode.UP);
			else
				taxRate = Env.ZERO;
		}
		
		BigDecimal rate = Env.ONE;
		if (isServiceChargeIncludedTax())
			rate = rate.add(taxRate);
		
		if (isPriceIncludedTax())
			rate = rate.add(taxRate);
		
		BigDecimal beforeTaxAndSvcChargeAmt = lineNetAmt.divide(rate, 10, RoundingMode.HALF_EVEN);
		
		svcChargeAmt = beforeTaxAndSvcChargeAmt.multiply(svcChargeRate);
		taxAmt = beforeTaxAndSvcChargeAmt.multiply(taxRate);
		
		setServiceCharge(svcChargeAmt.setScale(precision, RoundingMode.HALF_UP));
		setTaxAmt(taxAmt.setScale(precision, RoundingMode.HALF_UP));
	}
	
	public void setGrandTotal ()
	{
		setGrandTotal(getTotalAmt().subtract(getDiscountAmt()));
		initTaxAndServiceCharge();
		if (!getM_PriceList().isTaxIncluded())
			setGrandTotal(getGrandTotal().add(getTaxAmt()));
		if (!getM_PriceList().isServiceChargeIncluded())
			setGrandTotal(getGrandTotal().add(getServiceCharge()));
	}
}