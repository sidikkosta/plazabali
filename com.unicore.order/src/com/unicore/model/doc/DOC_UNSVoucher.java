/**
 * 
 */
package com.unicore.model.doc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.util.Env;
import com.unicore.model.MUNSVoucherBook;
import com.uns.util.UNSApps;

/**
 * @author menjangan
 *
 */
public class DOC_UNSVoucher extends Doc {

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public DOC_UNSVoucher(MAcctSchema as, Class<?> clazz, ResultSet rs,
			String defaultDocumentType, String trxName) 
	{
		super(as, clazz, rs, defaultDocumentType, trxName);
	}
	
	public DOC_UNSVoucher (MAcctSchema as, ResultSet rs, String trxName)
	{
		this (as, MUNSVoucherBook.class, rs, null, trxName);
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() 
	{
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) 
	{
		ArrayList<Fact> facts = new ArrayList<>();
		MUNSVoucherBook book = (MUNSVoucherBook) getPO();
		if (!book.isPostDocument())
			return facts;
		setDateAcct(book.getDateAcct ());
		setDateDoc(book.getDateDoc());
		if (book.getC_Charge_ID() == 0)// if charge is not zero there is not sales transaction
			setC_BankAccount_ID (book.getC_BankAccount_ID());
		
		BigDecimal chargeAmt = book.getChargeVoucherAmt();
		BigDecimal voucherAmt = book.getTotalVoucherAmt();
		BigDecimal totalAmt = book.getTotalAmt();
		BigDecimal diff = totalAmt.subtract(chargeAmt).subtract(voucherAmt);
		
		MAccount drAcct = book.getC_Charge_ID() > 0 ? MCharge.getAccount(book.getC_Charge_ID(), as)
				: getAccount(ACCTTYPE_BankInTransit, as);
		MAccount crAcct = MAccount.get(getCtx(), UNSApps.getRefAsInt(UNSApps.ACCT_CashVoucher));
		FactLine dr = null;
		FactLine cr = null;
		setC_Currency_ID(as.getC_Currency_ID());
		Fact fact = new Fact(this, as, Fact.POST_Actual);		
		
		dr = fact.createLine(null, drAcct, getC_Currency_ID(), totalAmt);
		if (null == dr)
		{
			p_Error = "DR not created: " + this.toString();
		    log.log(Level.WARNING, p_Error);
		    return null;
		}
		
		cr = fact.createLine(null, crAcct, getC_Currency_ID(), voucherAmt.negate());
		if (null == cr)
		{
			p_Error = "CR not created: " + this.toString();
		    log.log(Level.WARNING, p_Error);
		    return null;
		}
		
		if (book.getChargeVoucher_ID() > 0 && chargeAmt.signum() != 0)
		{
			crAcct = MCharge.getAccount(book.getChargeVoucher_ID(), as);
			cr = fact.createLine(null, crAcct, getC_Currency_ID(), chargeAmt.negate());
			if (null == cr)
			{
				p_Error = "CR not created: " + this.toString();
			    log.log(Level.WARNING, p_Error);
			    return null;
			}
		}
		
		if (diff.signum() != 0)
		{
			//difference sebagai pendapatan lain lain atau diskon penjualan
			MAccount acct = null;
			if (diff.signum() == 1) // TODO pendapatn lain lain
				acct =(MAccount) as.getAcctSchemaDefault().getP_Revenue_A();
			if (diff.signum() == -1)
				acct = (MAccount) as.getAcctSchemaDefault().getP_TradeDiscountGrant_A();
			
			dr = fact.createLine(null, acct, getC_Currency_ID(), diff.negate());
			if (null == dr)
			{
				p_Error = "DR not created: " + this.toString();
			    log.log(Level.WARNING, p_Error);
			    return null;
			}
		}
		
		facts.add(fact);
		return facts;
	}

}
