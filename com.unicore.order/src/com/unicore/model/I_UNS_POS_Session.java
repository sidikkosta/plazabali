/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POS_Session
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POS_Session 
{

    /** TableName=UNS_POS_Session */
    public static final String Table_Name = "UNS_POS_Session";

    /** AD_Table_ID=1000381 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Base1Currency_ID */
    public static final String COLUMNNAME_Base1Currency_ID = "Base1Currency_ID";

	/** Set Base 1 Currency	  */
	public void setBase1Currency_ID (int Base1Currency_ID);

	/** Get Base 1 Currency	  */
	public int getBase1Currency_ID();

	public org.compiere.model.I_C_Currency getBase1Currency() throws RuntimeException;

    /** Column name Base2Currency_ID */
    public static final String COLUMNNAME_Base2Currency_ID = "Base2Currency_ID";

	/** Set Base 2 Currency	  */
	public void setBase2Currency_ID (int Base2Currency_ID);

	/** Get Base 2 Currency	  */
	public int getBase2Currency_ID();

	public org.compiere.model.I_C_Currency getBase2Currency() throws RuntimeException;

    /** Column name Cashier_ID */
    public static final String COLUMNNAME_Cashier_ID = "Cashier_ID";

	/** Set Cashier	  */
	public void setCashier_ID (int Cashier_ID);

	/** Get Cashier	  */
	public int getCashier_ID();

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException;

    /** Column name CashSalesAmt */
    public static final String COLUMNNAME_CashSalesAmt = "CashSalesAmt";

	/** Set Cash Sales Amt.
	  * The amount from selling using direct cash
	  */
	public void setCashSalesAmt (BigDecimal CashSalesAmt);

	/** Get Cash Sales Amt.
	  * The amount from selling using direct cash
	  */
	public BigDecimal getCashSalesAmt();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateAcct */
    public static final String COLUMNNAME_DateAcct = "DateAcct";

	/** Set Account Date.
	  * Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct);

	/** Get Account Date.
	  * Accounting Date
	  */
	public Timestamp getDateAcct();

    /** Column name DateClosed */
    public static final String COLUMNNAME_DateClosed = "DateClosed";

	/** Set Date Closed	  */
	public void setDateClosed (Timestamp DateClosed);

	/** Get Date Closed	  */
	public Timestamp getDateClosed();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DateOpened */
    public static final String COLUMNNAME_DateOpened = "DateOpened";

	/** Set Date Opened	  */
	public void setDateOpened (Timestamp DateOpened);

	/** Get Date Opened	  */
	public Timestamp getDateOpened();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name EDCSalesAmt */
    public static final String COLUMNNAME_EDCSalesAmt = "EDCSalesAmt";

	/** Set EDC Sales Amount	  */
	public void setEDCSalesAmt (BigDecimal EDCSalesAmt);

	/** Get EDC Sales Amount	  */
	public BigDecimal getEDCSalesAmt();

    /** Column name ExportRecord */
    public static final String COLUMNNAME_ExportRecord = "ExportRecord";

	/** Set Export	  */
	public void setExportRecord (String ExportRecord);

	/** Get Export	  */
	public String getExportRecord();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name IsReconciled */
    public static final String COLUMNNAME_IsReconciled = "IsReconciled";

	/** Set Reconciled.
	  * Payment is reconciled with bank statement
	  */
	public void setIsReconciled (boolean IsReconciled);

	/** Get Reconciled.
	  * Payment is reconciled with bank statement
	  */
	public boolean isReconciled();

    /** Column name IsTrialMode */
    public static final String COLUMNNAME_IsTrialMode = "IsTrialMode";

	/** Set Trial Mode?	  */
	public void setIsTrialMode (boolean IsTrialMode);

	/** Get Trial Mode?	  */
	public boolean isTrialMode();

    /** Column name M_PriceList_ID */
    public static final String COLUMNNAME_M_PriceList_ID = "M_PriceList_ID";

	/** Set Price List.
	  * Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID);

	/** Get Price List.
	  * Unique identifier of a Price List
	  */
	public int getM_PriceList_ID();

	public org.compiere.model.I_M_PriceList getM_PriceList() throws RuntimeException;

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name PayableCashRefund */
    public static final String COLUMNNAME_PayableCashRefund = "PayableCashRefund";

	/** Set Payable Cash Refund.
	  * The cash to be refunded to customer by transfer to customer's bank account.
	  */
	public void setPayableCashRefund (BigDecimal PayableCashRefund);

	/** Get Payable Cash Refund.
	  * The cash to be refunded to customer by transfer to customer's bank account.
	  */
	public BigDecimal getPayableCashRefund();

    /** Column name PayableEDCRefund */
    public static final String COLUMNNAME_PayableEDCRefund = "PayableEDCRefund";

	/** Set Payable EDC Refund.
	  * The EDC transaction amount to be refunded to customer by transfer to customer's bank account.
	  */
	public void setPayableEDCRefund (BigDecimal PayableEDCRefund);

	/** Get Payable EDC Refund.
	  * The EDC transaction amount to be refunded to customer by transfer to customer's bank account.
	  */
	public BigDecimal getPayableEDCRefund();

    /** Column name PrintReading */
    public static final String COLUMNNAME_PrintReading = "PrintReading";

	/** Set Print Reading	  */
	public void setPrintReading (String PrintReading);

	/** Get Print Reading	  */
	public String getPrintReading();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name RepCurrRate */
    public static final String COLUMNNAME_RepCurrRate = "RepCurrRate";

	/** Set Report Currency Rate	  */
	public void setRepCurrRate (String RepCurrRate);

	/** Get Report Currency Rate	  */
	public String getRepCurrRate();

    /** Column name ReturnAmt */
    public static final String COLUMNNAME_ReturnAmt = "ReturnAmt";

	/** Set Return Amount	  */
	public void setReturnAmt (BigDecimal ReturnAmt);

	/** Get Return Amount	  */
	public BigDecimal getReturnAmt();

    /** Column name RoundingAmt */
    public static final String COLUMNNAME_RoundingAmt = "RoundingAmt";

	/** Set Rounding Amt	  */
	public void setRoundingAmt (BigDecimal RoundingAmt);

	/** Get Rounding Amt	  */
	public BigDecimal getRoundingAmt();

    /** Column name Shift */
    public static final String COLUMNNAME_Shift = "Shift";

	/** Set Shift	  */
	public void setShift (String Shift);

	/** Get Shift	  */
	public String getShift();

    /** Column name Supervisor_ID */
    public static final String COLUMNNAME_Supervisor_ID = "Supervisor_ID";

	/** Set Supervisor.
	  * Supervisor for this user/organization - used for escalation and approval
	  */
	public void setSupervisor_ID (int Supervisor_ID);

	/** Get Supervisor.
	  * Supervisor for this user/organization - used for escalation and approval
	  */
	public int getSupervisor_ID();

	public org.compiere.model.I_AD_User getSupervisor() throws RuntimeException;

    /** Column name TakeOver */
    public static final String COLUMNNAME_TakeOver = "TakeOver";

	/** Set Process Take Over	  */
	public void setTakeOver (String TakeOver);

	/** Get Process Take Over	  */
	public String getTakeOver();

    /** Column name TotalDiscAmt */
    public static final String COLUMNNAME_TotalDiscAmt = "TotalDiscAmt";

	/** Set Total Discount Amount	  */
	public void setTotalDiscAmt (BigDecimal TotalDiscAmt);

	/** Get Total Discount Amount	  */
	public BigDecimal getTotalDiscAmt();

    /** Column name TotalEstimationShortCashierAmt */
    public static final String COLUMNNAME_TotalEstimationShortCashierAmt = "TotalEstimationShortCashierAmt";

	/** Set Total Estimation Short Cashier Amount	  */
	public void setTotalEstimationShortCashierAmt (BigDecimal TotalEstimationShortCashierAmt);

	/** Get Total Estimation Short Cashier Amount	  */
	public BigDecimal getTotalEstimationShortCashierAmt();

    /** Column name TotalPayDiscAmt */
    public static final String COLUMNNAME_TotalPayDiscAmt = "TotalPayDiscAmt";

	/** Set Total Pay Disc Amount	  */
	public void setTotalPayDiscAmt (BigDecimal TotalPayDiscAmt);

	/** Get Total Pay Disc Amount	  */
	public BigDecimal getTotalPayDiscAmt();

    /** Column name TotalRefundSvcChgAmt */
    public static final String COLUMNNAME_TotalRefundSvcChgAmt = "TotalRefundSvcChgAmt";

	/** Set Total Refund Service Charge	  */
	public void setTotalRefundSvcChgAmt (BigDecimal TotalRefundSvcChgAmt);

	/** Get Total Refund Service Charge	  */
	public BigDecimal getTotalRefundSvcChgAmt();

    /** Column name TotalRefundTaxAmt */
    public static final String COLUMNNAME_TotalRefundTaxAmt = "TotalRefundTaxAmt";

	/** Set Total Refund Tax Amount	  */
	public void setTotalRefundTaxAmt (BigDecimal TotalRefundTaxAmt);

	/** Get Total Refund Tax Amount	  */
	public BigDecimal getTotalRefundTaxAmt();

    /** Column name TotalSalesB1 */
    public static final String COLUMNNAME_TotalSalesB1 = "TotalSalesB1";

	/** Set Total Sales	  */
	public void setTotalSalesB1 (BigDecimal TotalSalesB1);

	/** Get Total Sales	  */
	public BigDecimal getTotalSalesB1();

    /** Column name TotalSalesB2 */
    public static final String COLUMNNAME_TotalSalesB2 = "TotalSalesB2";

	/** Set Total Sales Base 2	  */
	public void setTotalSalesB2 (BigDecimal TotalSalesB2);

	/** Get Total Sales Base 2	  */
	public BigDecimal getTotalSalesB2();

    /** Column name TotalServiceCharge */
    public static final String COLUMNNAME_TotalServiceCharge = "TotalServiceCharge";

	/** Set Total Service Charge	  */
	public void setTotalServiceCharge (BigDecimal TotalServiceCharge);

	/** Get Total Service Charge	  */
	public BigDecimal getTotalServiceCharge();

    /** Column name TotalShortCashierAmt */
    public static final String COLUMNNAME_TotalShortCashierAmt = "TotalShortCashierAmt";

	/** Set Total Short Cashier Amount (+)	  */
	public void setTotalShortCashierAmt (BigDecimal TotalShortCashierAmt);

	/** Get Total Short Cashier Amount (+)	  */
	public BigDecimal getTotalShortCashierAmt();

    /** Column name TotalTaxAmt */
    public static final String COLUMNNAME_TotalTaxAmt = "TotalTaxAmt";

	/** Set Total Tax Amount	  */
	public void setTotalTaxAmt (BigDecimal TotalTaxAmt);

	/** Get Total Tax Amount	  */
	public BigDecimal getTotalTaxAmt();

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

    /** Column name UNS_POS_Session_UU */
    public static final String COLUMNNAME_UNS_POS_Session_UU = "UNS_POS_Session_UU";

	/** Set UNS_POS_Session_UU	  */
	public void setUNS_POS_Session_UU (String UNS_POS_Session_UU);

	/** Get UNS_POS_Session_UU	  */
	public String getUNS_POS_Session_UU();

    /** Column name UNS_POSTerminal_ID */
    public static final String COLUMNNAME_UNS_POSTerminal_ID = "UNS_POSTerminal_ID";

	/** Set POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID);

	/** Get POS Terminal	  */
	public int getUNS_POSTerminal_ID();

	public com.unicore.model.I_UNS_POSTerminal getUNS_POSTerminal() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VoucherAmt */
    public static final String COLUMNNAME_VoucherAmt = "VoucherAmt";

	/** Set Voucher Amount	  */
	public void setVoucherAmt (BigDecimal VoucherAmt);

	/** Get Voucher Amount	  */
	public BigDecimal getVoucherAmt();
}
