/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_OrderValidationConf
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_OrderValidationConf extends PO implements I_UNS_OrderValidationConf, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181020L;

    /** Standard Constructor */
    public X_UNS_OrderValidationConf (Properties ctx, int UNS_OrderValidationConf_ID, String trxName)
    {
      super (ctx, UNS_OrderValidationConf_ID, trxName);
      /** if (UNS_OrderValidationConf_ID == 0)
        {
			setPercent (Env.ZERO);
// 0
			setProcurementType (null);
			setUNS_OrderValidationConf_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_OrderValidationConf (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_OrderValidationConf[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Percent.
		@param Percent 
		Percentage
	  */
	public void setPercent (BigDecimal Percent)
	{
		set_Value (COLUMNNAME_Percent, Percent);
	}

	/** Get Percent.
		@return Percentage
	  */
	public BigDecimal getPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Purchasing = PCH */
	public static final String PROCUREMENTTYPE_Purchasing = "PCH";
	/** Merchandising = MRC */
	public static final String PROCUREMENTTYPE_Merchandising = "MRC";
	/** Food And Beverages = FNB */
	public static final String PROCUREMENTTYPE_FoodAndBeverages = "FNB";
	/** Set Procurement Type.
		@param ProcurementType Procurement Type	  */
	public void setProcurementType (String ProcurementType)
	{

		set_Value (COLUMNNAME_ProcurementType, ProcurementType);
	}

	/** Get Procurement Type.
		@return Procurement Type	  */
	public String getProcurementType () 
	{
		return (String)get_Value(COLUMNNAME_ProcurementType);
	}

	/** Set Order Validation Conf..
		@param UNS_OrderValidationConf_ID Order Validation Conf.	  */
	public void setUNS_OrderValidationConf_ID (int UNS_OrderValidationConf_ID)
	{
		if (UNS_OrderValidationConf_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_OrderValidationConf_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_OrderValidationConf_ID, Integer.valueOf(UNS_OrderValidationConf_ID));
	}

	/** Get Order Validation Conf..
		@return Order Validation Conf.	  */
	public int getUNS_OrderValidationConf_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_OrderValidationConf_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_OrderValidationConf_UU.
		@param UNS_OrderValidationConf_UU UNS_OrderValidationConf_UU	  */
	public void setUNS_OrderValidationConf_UU (String UNS_OrderValidationConf_UU)
	{
		set_Value (COLUMNNAME_UNS_OrderValidationConf_UU, UNS_OrderValidationConf_UU);
	}

	/** Get UNS_OrderValidationConf_UU.
		@return UNS_OrderValidationConf_UU	  */
	public String getUNS_OrderValidationConf_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_OrderValidationConf_UU);
	}
}