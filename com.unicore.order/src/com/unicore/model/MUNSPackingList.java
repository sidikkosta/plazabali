/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MPeriod;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

import com.unicore.base.model.MInOut;
import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MInvoiceLine;
import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.unicore.model.process.InvoiceCalculateDiscount;
import com.unicore.util.AutoCompletion;
import com.uns.base.model.Query;

/**
 * @author UNTA_Andy
 * 
 */
public class MUNSPackingList extends X_UNS_PackingList implements DocAction, DocOptions, AutoCompletion {

	private String oldNoInOut = null;
	private String oldNoInvoice = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2162010234301294254L;
	private boolean m_isPLReturnCompleted = false;
//	private final boolean m_isEventTest = 
//			MSysConfig.getBooleanValue(MSysConfig.FILL_QTY_ON_PACKINGLIST_EVENT_TEST, false);
	
	/**
	 * @param ctx
	 * @param UNS_PackingList_ID
	 * @param trxName
	 */
	public MUNSPackingList(Properties ctx, int UNS_PackingList_ID, String trxName) {
		super(ctx, UNS_PackingList_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPackingList(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	/**************************************************************************
	 * Process document
	 * 
	 * @param processAction document action
	 * @return true if performed
	 */
	public boolean processIt(String processAction) {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(processAction, getDocAction());
	} // processIt

	/** Process Message */
	private String m_processMsg = null;
	/** Just Prepared Flag */
	private boolean m_justPrepared = false;
	private MUNSPackingListOrder[] m_orders;

	/**
	 * Unlock Document.
	 * 
	 * @return true if success
	 */
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO))
			log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	} // unlockIt

	/**
	 * Invalidate Document
	 * 
	 * @return true if success
	 */
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	} // invalidateIt

	public  void createShipmentInvoice(MUNSPackingList pl) 
	{
		for (MUNSPackingListOrder plo : pl.getOrders())
		{
			if(plo.getReplaceOrder_ID() > 0)
			{
				boolean ioChanged = plo.getM_InOut_ID() > 0 ? true : false;
				boolean invChanged = plo.getC_Invoice_ID() > 0 ? true : false;
				
				MInOut ioOld = new MInOut(getCtx(), plo.getReplaceOrder().getM_InOut_ID(), get_TrxName());
				MInvoice invOld = new MInvoice(getCtx(), plo.getReplaceOrder().getC_Invoice_ID(), get_TrxName());
				oldNoInOut = ioOld.getDocumentNo();
				oldNoInvoice = invOld.getDocumentNo();
				for (int i=0; i<100; i++)
				{
					if(ioChanged && invChanged)
						break;
					
					String strVO = "_VO"+i;
					String docNoIo = ioOld.getDocumentNo().concat(strVO);
					String docNoInv = invOld.getDocumentNo().concat(strVO);
					String sqlIo = "SELECT 1 FROM M_InOut WHERE DocumentNo = ?";
					int valueIo = DB.getSQLValue(get_TrxName(), sqlIo, 
							docNoIo);
					
					if(valueIo < 1 && !ioChanged)
					{
						ioOld.setDocumentNo(docNoIo);
						ioOld.saveEx();
						
						//update document reversed
						if(ioOld.getReversal_ID() > 0)
						{
							MInOut revIoOld = new MInOut(getCtx(), ioOld.getReversal_ID(), get_TrxName());
							revIoOld.setDocumentNo(revIoOld.getDocumentNo().concat(strVO));
							revIoOld.saveEx();
						}
						
						ioChanged = true;
					}
					
					String sqlInv = "SELECT 1 FROM C_Invoice WHERE DocumentNo = ?";
					int valueInv = DB.getSQLValue(get_TrxName(), sqlInv,
							docNoInv);
					if(valueInv < 1 && !invChanged)
					{
						invOld.setDocumentNo(docNoInv);
						invOld.saveEx();
						
						//update document reversal
						if(invOld.getReversal_ID() > 0)
						{
							MInvoice revInvOld = new MInvoice(getCtx(), invOld.getReversal_ID(), get_TrxName());
							revInvOld.setDocumentNo(invOld.getDocumentNo().concat(strVO));
							revInvOld.saveEx();
						}
						
						invChanged = true;
					}
				}
				
			}
			boolean isAllowedToOverwriteInvoice = false;
			if (plo.getC_Invoice_ID() == 0 && plo.getC_Order_ID() == 0)
			{
				throw new AdempiereUserError("PL Order does not have invoice and order.");
			}
			
			MOrder order = null;
			MInvoice invoice = null;
			if(plo.getC_Order_ID() > 0)
			{
				order = new MOrder(plo.getCtx(), plo.getC_Order_ID(), plo.get_TrxName());
				String orderDocSubtype = MDocType.get(getCtx(), order.getC_DocTypeTarget_ID()).getDocSubTypeSO();
				if(orderDocSubtype.equals(MDocType.DOCSUBTYPESO_CashOrder)
						|| orderDocSubtype.equals(MDocType.DOCSUBTYPESO_PrepayOrder))
				{
					if(plo.getC_Invoice_ID() == 0)
					{
						throw new AdempiereException("Cash Order or Prepay Order has not been invoiced yet.");
					}
					
					invoice = new MInvoice(getCtx(), plo.getC_Invoice_ID(), get_TrxName());
					if(!invoice.isComplete())
					{
						throw new AdempiereUserError("Invoice of Cash Order or Prepay Order is not completed.");
					}
					isAllowedToOverwriteInvoice = !invoice.isComplete();	
				}
			}
			
			if(plo.getC_Invoice_ID() > 0 && invoice == null)
			{
				invoice = new MInvoice(getCtx(), plo.getC_Invoice_ID(), get_TrxName());
				isAllowedToOverwriteInvoice = !invoice.isComplete();
			}
			
			MInOut io = null;
			if (plo.getM_InOut_ID() == 0)
			{
				if (order != null)
				{
					io = new MInOut(order, MDocType.getDocType(MDocType.DOCBASETYPE_MaterialDelivery),
									pl.getDateDoc());	
					io.setM_Warehouse_ID(plo.getM_Warehouse_ID());
				}
				else if (invoice != null)
				{
					io = new MInOut(invoice, MDocType.getDocType(MDocType.DOCBASETYPE_MaterialDelivery),
									pl.getDateDoc(), plo.getM_Warehouse_ID());
				}
				
				io.setDocumentNo(oldNoInOut);
				io.saveEx();
				plo.setM_InOut_ID(io.get_ID());
				plo.saveEx();
			} 
			else
			{
				io = new MInOut(plo.getCtx(), plo.getM_InOut_ID(), plo.get_TrxName());
			}

			if (invoice == null)
			{
				invoice = new MInvoice(order, MDocType.getDocType(MDocType.DOCBASETYPE_ARInvoice), pl.getDateDoc());
				if(invoice.getAD_Org_ID() != io.getAD_Org_ID())
					invoice.setAD_Org_ID(io.getAD_Org_ID());
				invoice.setOrder(order);
				invoice.setGridTab(this.getGridTab());
				
				String documentno =  io.getDocumentNo();
				
				if(plo.getReplaceOrder_ID() > 0)
				{
					documentno = oldNoInvoice;
				}
				else
				{
					int region_ID = MSysConfig.getIntValue(MSysConfig.Invoice_Use_DocumentSequence, 0,
							plo.getAD_Client_ID());
					if(region_ID > 0)
					{
						String sql = "SELECT 1 FROM AD_Org WHERE AD_Org_ID = ? AND C_SalesRegion_ID = ?";
						boolean ok = DB.getSQLValue(plo.get_TrxName(), sql, plo.getAD_Org_ID(), region_ID) > 0;
						if(ok)
						{
							documentno = null;
						}
					}
				}
				
				invoice.setDocumentNo(documentno);
				invoice.saveEx();
				io.setC_Invoice_ID(invoice.get_ID());
				io.saveEx();
				plo.setC_Invoice_ID(invoice.get_ID());
				plo.saveEx();
				isAllowedToOverwriteInvoice = true;
			}
			else
			{
				if(!io.isComplete())
				{
					io.setC_Invoice_ID(invoice.get_ID());
					io.saveEx();
				}
			}
			
			if (order != null && invoice != null &&  invoice.getC_Order_ID() != order.get_ID())
			{
				throw new AdempiereException("Invoice and Order not matched.");
			}
			else if (order != null && io != null && io.getC_Order_ID() != order.get_ID())
			{
				throw new AdempiereException("Order and Shipment not matched.");
			}
			else if (io != null && invoice != null && io.getC_Invoice_ID() != invoice.get_ID())
			{
				throw new AdempiereException("Shipment and Invoice not matched.");
			}
			
			for (MUNSPackingListLine pll : plo.getLines())
			{
				MOrderLine line = null;
				boolean isFullShipment = false;
				BigDecimal qtyBonus = Env.ZERO;
				if(!pll.isProductAccessories())
				{
					line = new MOrderLine(pll.getCtx(), pll.getC_OrderLine_ID(), pll.get_TrxName());
					isFullShipment = line.getQtyOrdered().doubleValue()
							== pll.getMovementQty().doubleValue();
					qtyBonus = line.getQtyBonuses();
				}
				
				MInvoiceLine iline = null;
				MInOutLine ioline = null;
				if (pll.getC_InvoiceLine_ID() == 0 && !pll.isProductAccessories())
				{
					if(!isAllowedToOverwriteInvoice)
					{
						throw new AdempiereException("Can't add line to the completed invoice.");
					}
					
					iline = new MInvoiceLine(invoice);
					iline.setAD_Org_ID(pll.getAD_Org_ID());
					iline.setOrderLine(line);
					
					if(isFullShipment)
					{
						iline.setQty(pll.getMovementQty().subtract(qtyBonus));
						iline.setQtyBonuses(qtyBonus);
						if(null != line)
							iline.setDiscountAmt(line.getDiscountAmt());
					}
					else
					{
						iline.setQty(pll.getMovementQty());
						if(null != line)
							iline.setDiscount(line.getDiscount());
					}
					
					iline.setLineNetAmt();
					iline.saveEx();
					pll.setC_InvoiceLine_ID(iline.get_ID());
					pll.setAutomatic(true);
					pll.saveEx();
					pll.setAutomatic(false);
					
					m_processMsg = MUNSAccessoriesLine.plTOInvoice(pll.getCtx(), pll.get_ID(), pll.getM_Product_ID(), pll.get_TrxName());
					if(null != m_processMsg)
						throw new AdempiereException(m_processMsg);
				}
				else if (!pll.isProductAccessories())
				{
					iline = new MInvoiceLine(pll.getCtx(), pll.getC_InvoiceLine_ID(), pll.get_TrxName());
					if(iline.getQtyInvoiced().compareTo(pll.getMovementQty()) != 0
							&& isAllowedToOverwriteInvoice)
					{
						//hanya masuk ke sini jika invoicenya belum complete.
						// ini bisa terjadi jika user membuat packing list berdasarkan order dan ordernya bukan prepay atau cash.
						if (isFullShipment)
						{
							iline.setQtyEntered(pll.getMovementQty().subtract(qtyBonus));
							iline.setQtyBonuses(qtyBonus);
							if(null != line)
								iline.setDiscountAmt(line.getDiscountAmt());
						}
						else
						{
							iline.setQty(pll.getMovementQty());
							if(null != line)
								iline.setDiscount(line.getDiscount());
						}
						iline.saveEx();
					}
				}

				if (pll.getM_InOutLine_ID() == 0)
				{
					ioline = new MInOutLine(io);
					if (pll.getM_Locator_ID() == 0)
						ioline.setM_Locator_ID(pll.getTargetQty());
					else
						ioline.setM_Locator_ID(pll.getM_Locator_ID());

					if(!pll.isProductAccessories())
					{
						ioline.setInvoiceLine(iline, ioline.getM_Locator_ID(), pll.getTargetQty());
						ioline.setisProductAccessories(false);
					}		
					else
					{
						ioline.setM_Product_ID(pll.getM_Product_ID());
						ioline.setC_UOM_ID(pll.getC_UOM_ID());
						ioline.setisProductAccessories(true);
						ioline.setInOutLine_ID(new MUNSPackingListLine(
								pll.getCtx(), pll.getPackingList_Line_ID(), pll.get_TrxName()).getM_InOutLine_ID());
					}
					ioline.setQty(pll.getMovementQty());
					ioline.saveEx();
					pll.setM_InOutLine_ID(ioline.get_ID());
					pll.setAutomatic(true);
					pll.saveEx();
					pll.setAutomatic(false);
				} 
				else
				{
					ioline = new MInOutLine(pll.getCtx(), pll.getM_InOutLine_ID(), pll.get_TrxName());
					if(ioline.getMovementQty().compareTo(pll.getMovementQty()) != 0)
					{
						ioline.setQty(pll.getMovementQty());
						ioline.saveEx();
					}
				}
				
				if(!pll.isProductAccessories())
				{
					if(ioline.getC_OrderLine_ID() == 0 )
					{
						ioline.setC_OrderLine_ID(line.get_ID());
						ioline.saveEx();
					}
					if(iline.getM_InOutLine_ID() == 0)
					{
						iline.setM_InOutLine_ID(ioline.get_ID());
						iline.saveEx();
					}
				}
			}
		}
	}

	/**
	 * Approve Document
	 * 
	 * @return true if success
	 */
	public boolean approveIt() {
		if (log.isLoggable(Level.INFO))
			log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	} // approveIt

	/**
	 * Reject Approval
	 * 
	 * @return true if success
	 */
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO))
			log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	} // rejectIt

	/**************************************************************************
	 * Prepare Document
	 * 
	 * @return new status (In Progress or Invalid)
	 */
	public String prepareIt() 
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		// Lines
		MUNSPackingListOrder[] orders = getOrders(true, null);
		if (orders.length == 0)
		{
			m_processMsg = "@NoLines@";
			return DocAction.STATUS_Invalid;
		} 
		else if (!isReversal())
		{
			for (MUNSPackingListOrder order : orders)
			{
				//MUNSPackingListLine[] lines = order.getLines();
				String sql = "SELECT 1 FROM UNS_PackingList_Line WHERE UNS_PackingList_Order_ID=" + order.get_ID();
				boolean hasLines = DB.getSQLValueEx(get_TrxName(), sql) == 1;
				if (!hasLines)
				{
					m_processMsg = "@NoLines@";
					return DocAction.STATUS_Invalid;
				}
				
				m_processMsg = order.checkLimit();
				if(!Util.isEmpty(m_processMsg, true))
					return DocAction.STATUS_Invalid;
				
				//boolean isOrderCash = order.getC_Order().getC_DocTypeTarget().getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_CashOrder);
				sql = "SELECT C_DocTypeTarget_ID FROM C_Order WHERE C_Order_ID=" + order.getC_Order_ID();
				int orderDocTypeTarget_ID = DB.getSQLValueEx(get_TrxName(), sql);
				boolean isOrderCash = 
						MDocType.get(getCtx(), orderDocTypeTarget_ID)
						.getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_CashOrder);
				if(isOrderCash)
				{
					if(order.getC_Invoice_ID() <= 0)
					{
						m_processMsg = "Can't pack order. Unpaid order " + order.getC_Order().getDocumentNo();
						return DocAction.STATUS_Invalid;
					}
					
					sql = "SELECT IsPaid FROM C_Invoice WHERE C_Invoice_ID=" + order.getC_Invoice_ID();
					boolean isInvoicePaid = DB.getSQLValueStringEx(get_TrxName(), sql).equals("Y");
					if(!isInvoicePaid)
					{
						m_processMsg = "Can't pack order. Unpaid order " + order.getC_Order().getDocumentNo();
						return DocAction.STATUS_Invalid;
					}
				}
			}
			
			String sql = "SELECT pll.M_Locator_ID, pll.M_Product_ID, SUM(QtyEntered) FROM UNS_PackingList_Line pll "
					+ "	INNER JOIN UNS_PackingList_Order plo ON pll.UNS_PackingList_Order_ID=plo.UNS_PackingList_Order_ID "
					+ " INNER JOIN UNS_PackingList pl ON pl.UNS_PackingList_ID=plo.UNS_PackingList_ID "
					+ " INNER JOIN M_Product mp ON mp.M_Product_ID=pll.M_Product_ID "
					+ " WHERE mp.ProductType='I' AND pl.UNS_PackingList_ID=" + get_ID()
					+ " GROUP BY pll.M_Locator_ID, pll.M_Product_ID";
			
			StringBuilder buildErrorMsg = new StringBuilder();

			PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
			ResultSet rs = null;
			try {
				rs = stmt.executeQuery();
				while (rs.next())
				{
					int M_Locator_ID = rs.getInt(1);
//					if(m_reversal)
//					{
//						MLocator originLoc = new MLocator(getCtx(), M_Locator_ID, get_TrxName());					
//						MLocator intransitLoc = null;
//						MWarehouse wh = MWarehouse.get(getCtx(), originLoc.getM_Warehouse_ID());
//						MLocator[] locators = wh.getLocators(true);
//						for(int i=0; i<locators.length; i++)
//						{
//							if(!locators[i].isActive())
//								continue;
//							if(!locators[i].getValue().equals(wh.getValue().concat("-").concat(MWarehouse.INITIAL_INTRANSIT_CUSTOMER_LOC)))
//								continue;
//							if(!locators[i].isInTransit())
//								continue;
//							intransitLoc = locators[i];
//							break;
//						}
//						M_Locator_ID = intransitLoc.get_ID();
//					}
					int M_Product_ID = rs.getInt(2);
					BigDecimal totalQtyEntered = rs.getBigDecimal(3);
					
					BigDecimal qtyAvailable =
							MStorageOnHand.getQtyOnHandForLocator(M_Product_ID, M_Locator_ID, 0, get_TrxName());
					if (qtyAvailable.compareTo(totalQtyEntered) == -1)
					{
						if(!Util.isEmpty(buildErrorMsg.toString(), true));
						{
							buildErrorMsg.append(" && ");
						}
						buildErrorMsg.append("@NotEnoughStocked@: ").append(MProduct.get(getCtx(), M_Product_ID))
						.append(" At Locator of ").append(MLocator.get(getCtx(), M_Locator_ID));
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
				m_processMsg = e.getMessage();
			}
			finally {
				DB.close(rs, stmt);
			}
			
			String errorMsg = buildErrorMsg.toString();
			if(!Util.isEmpty(errorMsg, true))
			{
				m_processMsg = errorMsg;
				return DocAction.STATUS_Invalid;
			}
			
			try
			{
				if (!MUNSPLConfirm.isCompleting(getCtx(), getDocumentNo()))
				{
					for (MUNSPackingListOrder order : orders)
					{
						order.reserveStock(order.getLines(), true);
					}
				}
				
				if (!m_reversal)
					createShipmentInvoice(this);
			} 
			catch (Exception e)
			{
				throw new AdempiereException("Error while doing process, ".concat(e.toString()));
			}
		}

		m_justPrepared = true;
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		setProcessed(true);
		saveEx();

		return DocAction.STATUS_InProgress;
	} // prepareIt

	
	@Override
	public String completeIt() 
	{
		// Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (DocAction.STATUS_InProgress.equals(status)) 
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		if (!isReversal())
		{
			if (!MUNSPLConfirm.isCompleting(getCtx(), getDocumentNo()))
			{
				m_processMsg = MUNSPLConfirm.create(this);
				if(!Util.isEmpty(m_processMsg, true))
				{
					log.log(Level.INFO, m_processMsg);
					return DocAction.STATUS_InProgress;
				}
			}
		}
		
		boolean needMovement = !isReversal();
		if (!needMovement)
		{
			needMovement = !isPLReturnCompleted();
		}
			
		if (!isPickup() && needMovement)
		{
			m_processMsg = moveItems();
			if (m_processMsg != null)
			{
				return DocAction.STATUS_Invalid;
			}
		}
			
		if (!isReversal())
		{
			MUNSPackingListOrder[] orders = getOrders();//getOrders(true, null);
			if (log.isLoggable(Level.INFO))
				log.info(toString());
			StringBuilder info = new StringBuilder();
		
			try
			{	
				/** Make it faster for organization not having discount transaction implemented */
				String sql = "SELECT 1 FROM UNS_DiscountTrx disc "
						+ "	INNER JOIN C_Order o ON disc.C_Order_ID=o.C_Order_ID"
						+ " INNER JOIN UNS_PackingList_Order plo ON plo.C_Order_ID=o.C_Order_ID "
						+ " WHERE plo.UNS_PackingList_ID=" + getUNS_PackingList_ID();
				boolean haveDiscTrx = DB.getSQLValueEx(get_TrxName(), sql) > 0;
				
				if (haveDiscTrx)
				{
					for(int i=0; i< orders.length; i++)
					{
						checkOrder(orders[i]);
					}
				}
			
				if (isPickup())
				{
					shipOrder();
				}
				else
				{
					MUNSPLReturn.createPLReturn(this);
				}
				
			} catch (Exception e)
			{
				throw new AdempiereException("Error when create PL Return, ".concat(e.toString()));
			}
		
			m_processMsg = info.toString();
		}
		
		// Implicit Approval
		if (!isApproved())
			approveIt();
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		setProcessed(true);
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt

	@Override
	public boolean voidIt() 
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		if (DOCSTATUS_Drafted.equals(getDocStatus())
				|| DOCSTATUS_Invalid.equals(getDocStatus())
				|| DOCSTATUS_InProgress.equals(getDocStatus())
				|| DOCSTATUS_Approved.equals(getDocStatus())
				|| DOCSTATUS_NotApproved.equals(getDocStatus()) )
		{
			addDescription(Msg.getMsg(getCtx(), "Voided"));
			
			cancelIt();
			
			if (m_processMsg != null) {
				return false;
			}
		}
		else if (getDocStatus().equals(DOCSTATUS_Completed) || getDocStatus().equals(DOCSTATUS_Closed))
		{
			if (reverse(false) == null)
				return false;
		}

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		return true;
	}
	

	/**
	 * Cancel this Packing List document.
	 */
	private void cancelIt() 
	{
		MUNSPackingListOrder[] ploList = getLines(null, null);
		
		for (MUNSPackingListOrder plo : ploList)
		{
			boolean isCashOrder = 
					MDocType.get(getCtx(), plo.getC_Order().getC_DocTypeTarget_ID())
					.equals(MDocType.DOCSUBTYPESO_CashOrder);
			
			m_processMsg = plo.voidIt(this, isCashOrder);
			if (m_processMsg != null) {
				return;
			}
			
			MUNSPackingListLine[] plLineList = plo.getLines();
			
			for (MUNSPackingListLine plLine : plLineList)
			{
				if(plLine.getC_OrderLine_ID() > 0)
				{	
					org.compiere.model.MOrderLine ol = (org.compiere.model.MOrderLine) plLine.getC_OrderLine();
					ol.setQtyPacked(ol.getQtyPacked().subtract(plLine.getMovementQty()));
					ol.saveEx();
				}
				
				plLine.setQty(Env.ZERO);
				plLine.saveEx();
			}
		}
		
		MUNSPLConfirm[] plConfirmList = MUNSPLConfirm.gets(this);
		
		for (MUNSPLConfirm plConfirm : plConfirmList)
		{
			plConfirm.addDescription("{Voided by Packing List void action (PL:" + getDocumentNo() + ")}.");
			
			if (!plConfirm.processIt(DOCACTION_Void)) {
				m_processMsg = "Failed when void PL Confirmation of " + plConfirm.getDocumentNo();
				return;
			}
			
			plConfirm.saveEx();			
		}
		
//		MUNSPLReturn plReturn = 
//				Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLReturn.Table_Name, 
//						MUNSPLReturn.COLUMNNAME_UNS_PackingList_ID + " = " + get_ID(), get_TrxName())
//					.first();
//		if (plReturn == null 
//				|| plReturn.getDocStatus().equals(DOCSTATUS_Completed)
//				|| plReturn.getDocStatus().equals(DOCSTATUS_Closed))
//			return;
//		
//		plReturn.addDescription("{Voided by Packing List void action (PL:" + getDocumentNo() + ")}.");
//		
//		if (!plReturn.processIt(DOCSTATUS_Voided)) {
//			m_processMsg = "Failed when void PL Confirmation of " + plReturn.getDocumentNo();
//			return;
//		}
//		
//		plReturn.saveEx();
	}
	
	private boolean m_reversal = false;
	
	public boolean isReversal()
	{
		return m_reversal;
	}
	
	private void setReversal (boolean reverse)
	{
		m_reversal = reverse;
	}
	
	/**
	 * 
	 * @param accrual
	 * @return
	 */
	private MUNSPackingList reverse(boolean accrual)
	{
		Timestamp reversalDate = accrual ? Env.getContextAsDate(getCtx(), "#Date") : getDateDoc();
		if (reversalDate == null) {
			reversalDate = new Timestamp(System.currentTimeMillis());
		}

		if (!MPeriod.isOpen(getCtx(), reversalDate, MDocType.DOCBASETYPE_MaterialDelivery, getAD_Org_ID()))
		{
			m_processMsg = "@PeriodClosed@";
			return null;
		}
		
		MUNSPackingList reversal = new MUNSPackingList(getCtx(), 0, get_TrxName());
		copyValues(this, reversal);
		
		reversal.setReversal(true);
		reversal.setDocumentNo(this.getDocumentNo() + "^");
		StringBuilder msgadd = new StringBuilder("{->").append(getDocumentNo()).append(")");
		reversal.addDescription(msgadd.toString());
		reversal.setReversal_ID(get_ID());
		reversal.setAD_Org_ID(this.getAD_Org_ID());
		reversal.saveEx();
		setReversal_ID(reversal.get_ID());
		if(!save())
			throw new AdempiereException("Error when update reversal ID");
		MUNSPLReturn plReturn = 
		Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLReturn.Table_Name, 
				MUNSPLReturn.COLUMNNAME_UNS_PackingList_ID + " = " + get_ID(), get_TrxName())
				.first();
		
		
		
		if (plReturn != null)
		{
			setPLReturnCompleted ( 
					plReturn.getDocStatus().equals(DOCSTATUS_Completed) ||
					plReturn.getDocStatus().equals(DOCSTATUS_Closed));
			reversal.setPLReturnCompleted(isPLReturnCompleted());
			
			plReturn.addDescription("{Voided by Packing List reversal action (PL:" + getDocumentNo() + ")}.");
			
			if (!plReturn.processIt(DOCSTATUS_Voided)) {
				throw new AdempiereException("Failed when void PL Confirmation of " + plReturn.getDocumentNo());
			}
			
			plReturn.saveEx();
		}
		
		MUNSPackingListOrder[] ploList = getLines(null, null);
		
		for (MUNSPackingListOrder plo : ploList)
		{
			String docSubTypeSO = null;
			if(plo.getC_Order_ID() > 0)
			{
				MOrder order = new MOrder(getCtx(), plo.getC_Order_ID(), get_TrxName());
				docSubTypeSO = order.getC_DocTypeTarget().getDocSubTypeSO();
			}
			
			boolean isCashOrder = false;
			boolean isPrepayOrder = false;
			if(docSubTypeSO != null)
			{
				isCashOrder = docSubTypeSO.equals(MDocType.DOCSUBTYPESO_CashOrder);
				isPrepayOrder = docSubTypeSO.equals(MDocType.DOCSUBTYPESO_PrepayOrder);
			}
			int reversalInvoice_ID = plo.getC_Invoice_ID();
			int reversalInOut_ID = plo.getM_InOut_ID();
			
			if (!isPLReturnCompleted())
			{
				m_processMsg = plo.voidIt(this, isCashOrder);
			}
			else 
			{
				org.compiere.model.MInOut io = (org.compiere.model.MInOut) plo.getM_InOut();
				
				if(!io.getDocStatus().equals("VO") && !io.getDocStatus().equals("RE"))
				{
					if (!io.processIt(DOCACTION_Void)) 
					{
						m_processMsg = "Failed while reversing Customer Delivery document of " + io.getDocumentNo()
								+ ". " + io.getProcessMsg();
						return null;
					}
					io.saveEx();
				}
				
				reversalInOut_ID = io.getReversal_ID();
				
				if (!isCashOrder && !isPrepayOrder)
				{
					org.compiere.model.MInvoice inv = (org.compiere.model.MInvoice) plo.getC_Invoice();
					if(!inv.getDocStatus().equals("VO") && !inv.getDocStatus().equals("RE"))
					{
						if (!inv.processIt(DOCACTION_Void))
						{
							m_processMsg = "Failed while reversing AR Invoice document of " + inv.getDocumentNo()
									+ ". " + inv.getProcessMsg();
							return null;
						}
						reversalInvoice_ID = inv.getReversal_ID();
						inv.saveEx();
					}	
				}
			}
			
			MUNSPackingListOrder reversalPLO = new MUNSPackingListOrder(getCtx(), 0, get_TrxName());
			copyValues(plo, reversalPLO);
			reversalPLO.setUNS_PackingList_ID(reversal.get_ID());
			reversalPLO.setC_Invoice_ID(reversalInvoice_ID);
			reversalPLO.setM_InOut_ID(reversalInOut_ID);
			reversalPLO.setC_Order_ID(plo.getC_Order_ID());
			reversalPLO.setReversalLine_ID(plo.get_ID());
			reversalPLO.setAD_Org_ID(plo.getAD_Org_ID());
			reversalPLO.saveEx();
			
			plo.setReversalLine_ID(reversalPLO.get_ID());
			plo.saveEx();
			
			MUNSPackingListLine pllList[] = plo.getLines();
			
			for (MUNSPackingListLine pll : pllList)
			{
				MUNSPackingListLine reversalPLL = new MUNSPackingListLine(getCtx(), 0, get_TrxName());
				copyValues(pll, reversalPLL);
				reversalPLL.setMovementQty(pll.getMovementQty());
				reversalPLL.setQtyEntered(reversalPLL.getMovementQty());
				reversalPLL.setUNS_PackingList_Order_ID(reversalPLO.get_ID());
				reversalPLL.setC_OrderLine_ID(pll.getC_OrderLine_ID());
				reversalPLL.setAD_Org_ID(pll.getAD_Org_ID());
				reversalPLL.setM_Product_ID(pll.getM_Product_ID());
				reversalPLL.setC_UOM_ID(pll.getC_UOM_ID());
				reversalPLL.setM_Locator_ID(pll.getM_Locator_ID());
				reversalPLL.setM_InOutLine_ID(pll.getM_InOutLine_ID());
				reversalPLL.setisProductAccessories(pll.isProductAccessories());
				reversalPLL.setIsReversal(true);
				int reversalIOLine_ID = 0;
				if (isPLReturnCompleted()) {
					String sql = "SELECT M_InOutLine_ID FROM M_InOutLine WHERE ReversalLine_ID=?";
					reversalIOLine_ID = DB.getSQLValueEx(get_TrxName(), sql, pll.getM_InOutLine_ID());
					reversalPLL.setM_InOutLine_ID(reversalIOLine_ID);
					
					if (!isCashOrder) 
					{
						sql = "SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE M_InOutLine_ID=? AND C_Invoice_ID=?";
						int reversalInvoiceLine_ID = 
								DB.getSQLValueEx(get_TrxName(), sql, pll.getM_InOutLine_ID(), reversalInvoice_ID);
						reversalPLL.setC_InvoiceLine_ID(reversalInvoiceLine_ID);
					}
				}
				else {
					reversalPLL.setM_InOutLine_ID(pll.getM_InOutLine_ID());
					reversalPLL.setC_InvoiceLine_ID(pll.getC_InvoiceLine_ID());
				}
				if(!reversalPLL.save())
					m_processMsg = "failed when try create reverese for packing list line";
				
				if (isPLReturnCompleted() && reversalIOLine_ID > 0) 
				{ 
					m_processMsg = reversalPLL.reverseFromShipment(reversalDate, reversalIOLine_ID);
					if (m_processMsg != null)
						return null;
				}
			}
		}
		
		MUNSPLConfirm[] plConfirmList = MUNSPLConfirm.gets(this);
		
		for (MUNSPLConfirm plConfirm : plConfirmList)
		{
			plConfirm.addDescription("{Voided by Packing List void action (PL:" + getDocumentNo() + ")}.");
			
			boolean voidOrReservedOK = !isPLReturnCompleted()? 
					plConfirm.processIt(DOCACTION_Void) : plConfirm.processIt(DOCACTION_Reverse_Correct);
					
			if (!voidOrReservedOK) {
				m_processMsg = "Failed when void PL Confirmation of " + plConfirm.getDocumentNo();
				return null;
			}
			plConfirm.saveEx();			
		}
		
		
		if (!reversal.processIt(DocAction.ACTION_Complete)
				|| !reversal.getDocStatus().equals(DocAction.STATUS_Completed))
		{
			m_processMsg = "Reversal ERROR: " + reversal.getProcessMsg();
			return null;
		}
		reversal.closeIt();
		reversal.setProcessing (false);
		reversal.setDocStatus(DOCSTATUS_Reversed);
		reversal.setDocAction(DOCACTION_None);
		reversal.saveEx(get_TrxName());
		//
		msgadd = new StringBuilder("(").append(reversal.getDocumentNo()).append("<-)");
		addDescription(msgadd.toString());
		
		//
		// Void Confirmations
		setDocStatus(DOCSTATUS_Reversed); // need to set & save docstatus to be able to check it in MInOutConfirm.voidIt()
		saveEx();
		//FR1948157
		this.setReversal_ID(reversal.getUNS_PackingList_ID());

		return reversal;
	}

	/**
	 * Add to Description
	 * 
	 * @param description text
	 */
	public void addDescription(String description) {
		String desc = getDescription();
		if (desc == null)
			setDescription(description);
		else
			setDescription(desc + " | " + description);
	} // addDescription

	@Override
	public boolean closeIt() {
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Close

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;

		return true;
	}

	/**
	 * Reverse Correction - same void
	 * 
	 * @return true if success
	 */
	public boolean reverseCorrectIt() {
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		MUNSPackingList reversal = reverse(false);
		if (reversal == null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		m_processMsg = reversal.getDocumentNo();
		setProcessed(true);
		setDocStatus(DOCSTATUS_Reversed);		//	 may come from void
		setDocAction(DOCACTION_None);
		
		return true;
	} // reverseCorrectionIt

	/**
	 * Reverse Accrual - none
	 * 
	 * @return false
	 */
	public boolean reverseAccrualIt() {
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		MUNSPackingList reversal = reverse(true);
		if (reversal == null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		m_processMsg = reversal.getDocumentNo();
		setProcessed(true);
		setDocStatus(DOCSTATUS_Reversed);		//	 may come from void
		setDocAction(DOCACTION_None);
		
		return true;
	} // reverseAccrualIt

	@Override
	public boolean reActivateIt() {
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		// TODO palce coding reActivateIt here

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/*************************************************************************
	 * Get Summary
	 * 
	 * @return Summary of Document
	 */
	public String getSummary() {
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		// : Grand Total = 123.00 (#1)
		sb.append(": ").append(Msg.translate(getCtx(), "Tonase")).append("=").append(getTonase())
				.append(Msg.translate(getCtx(), "Volume")).append("=").append(getVolume());
		if (m_orders != null)
			sb.append(" (#").append(m_orders.length).append(")");
		// - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		return sb.toString();
	} // getSummary

	/**************************************************************************
	 * String Representation
	 * 
	 * @return info
	 */
	public String toString() {
		StringBuffer sb =
				new StringBuffer("MPackingSlip[").append(get_ID()).append("-").append(getDocumentNo())
						.append(", Tonase=").append(getTonase()).append(", Volume=").append(getVolume()).append("]");
		return sb.toString();
	} // toString

	/**
	 * Get Document Info
	 * 
	 * @return document info (untranslated)
	 */
	public String getDocumentInfo() {
		return "Packing List :" + getDocumentNo();
	} // getDocumentInfo

	@Override
	public File createPDF() {
		try
		{
			File temp = File.createTempFile(get_TableName() + get_ID() + "_", ".pdf");
			return createPDF(temp);
		} catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;

	}

	/**
	 * Create PDF file
	 * 
	 * @param file output file
	 * @return file if success
	 */
	public File createPDF(File file) {
		ReportEngine re = ReportEngine.get(getCtx(), ReportEngine.ORDER, get_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if (format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo("", format.getJasperProcess_ID());
			pi.setRecord_ID(get_ID());
			pi.setIsBatch(true);

			ServerProcessCtl.process(pi, null);

			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	} // createPDF

	@Override
	public String getProcessMsg() {

		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {

		return getUpdatedBy();
	}

	@Override
	public BigDecimal getApprovalAmt() {
		return Env.ZERO;
	}

	/**************************************************************************
	 * Get Orders
	 * 
	 * @param whereClause where clause or null (starting with AND)
	 * @param orderClause order clause
	 * @return orders
	 */
	public MUNSPackingListOrder[] getLines(String whereClause, String orderClause) 
	{
		StringBuilder whereClauseFinal = new StringBuilder(MUNSPackingListOrder.COLUMNNAME_UNS_PackingList_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(" AND ").append(whereClause);

		if (orderClause == null || orderClause.length() == 0)
			orderClause = MUNSPackingListOrder.COLUMNNAME_UNS_PackingList_Order_ID;
		
		List<MUNSPackingListOrder> list =
				Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPackingListOrder.Table_Name,
						whereClauseFinal.toString(), get_TrxName()).setParameters(get_ID()).setOrderBy(orderClause)
						.list();

		return list.toArray(new MUNSPackingListOrder[list.size()]);
	} // getLines

	/**
	 * Get Lines of Order
	 * 
	 * @param requery requery
	 * @param orderBy optional order by column
	 * @return lines
	 */
	public MUNSPackingListOrder[] getOrders(boolean requery, String orderBy) {
		if (m_orders != null && !requery)
		{
			set_TrxName(m_orders, get_TrxName());
			return m_orders;
		}
		//
		String orderClause = "";
		if (orderBy != null && orderBy.length() > 0)
			orderClause += orderBy;

		m_orders = getLines(null, orderClause);
		return m_orders;
	} // getLines

	/**
	 * Get Lines of Order. (used by web store)
	 * 
	 * @return lines
	 */
	public MUNSPackingListOrder[] getOrders() {
		return getOrders(false, null);
	} // getLines

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord) {

		return super.beforeSave(newRecord);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete() {
		for (MUNSPackingListOrder order : getOrders())
		{
			if (!order.delete(true))
				return false;
		}

		return super.beforeDelete();
	}

	@Override
	public int getC_Currency_ID() {
		return 0;
	}

	public void setUpdateParent() {
		BigDecimal tonase = Env.ZERO;
		BigDecimal volume = Env.ZERO;
		for (MUNSPackingListOrder order : getOrders())
		{
			tonase = tonase.add(order.getTonase());
			volume = volume.add(order.getVolume());
		}

		StringBuilder sb =
				new StringBuilder("UPDATE ").append(MUNSPackingList.Table_Name).append(" SET ")
						.append(MUNSPackingList.COLUMNNAME_Tonase).append("=").append(tonase).append(", ")
						.append(MUNSPackingList.COLUMNNAME_Volume).append("=").append(volume).append(" WHERE ")
						.append(MUNSPackingList.COLUMNNAME_UNS_PackingList_ID).append("=").append(get_ID());

		if (DB.executeUpdate(sb.toString(), get_TrxName()) == -1)
			throw new AdempiereException("Error when update parent.");

	}

	@Override
	public int customizeValidActions(String docStatus, Object processing, String orderType, String isSOTrx,
			int AD_Table_ID, String[] docAction, String[] options, int index) {
		if (docStatus.equals(DocAction.STATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_Reverse_Accrual;
			options[index++] = DocAction.ACTION_Reverse_Correct;
		}
		
		return index;
	}

	public String createOrderList(int SalesRep_ID, Timestamp dateFrom, int UNS_Rayon_ID) {
		MUNSPackingListOrder[] plos = getLines(" SalesRep_ID=" + SalesRep_ID + " ", null);

		List<com.unicore.base.model.MOrder> lorders =
				com.unicore.base.model.MOrder.getNotShipBySalesAndDate(getCtx(), SalesRep_ID
						, UNS_Rayon_ID, dateFrom, getDateDoc(),isSOTrx() ? "Y" : "N", get_TrxName());

		int orderCount = 0;
		for (com.unicore.base.model.MOrder order : lorders)
		{
			boolean skip = false;
			for (MUNSPackingListOrder plo : plos)
			{
				if (plo.getC_Order_ID() != order.get_ID())
					continue;

				skip = true;
				break;
			}

			if (skip)
				continue;

			orderCount++;
			MUNSPackingListOrder plo = new MUNSPackingListOrder(order);

			plo.setUNS_PackingList_ID(get_ID());

			if (!plo.save())
				throw new AdempiereException("Error while try create order list");

			for (com.unicore.base.model.MOrderLine oline : order.getLines())
			{
				if(oline.getQtyOrdered().compareTo(oline.getQtyDelivered()) <= 0)
					continue;
				
				MUNSPackingListLine pll = new MUNSPackingListLine(oline);

				MLocator loc = MLocator.getDefault(MWarehouse.get(getCtx(), oline.getM_Warehouse_ID()));
				pll.setM_Locator_ID(loc.get_ID());
				pll.setUNS_PackingList_Order_ID(plo.get_ID());
				try {
					pll.saveEx();
				} catch (Exception e) {
					throw new AdempiereException("Error while try create Order Line" + e.getMessage());
				}
			}
		}

		return "Created " + orderCount + " lines of order.";
	}
	
	/**
	 * 
	 * @param order
	 */
	private void checkOrder(MUNSPackingListOrder order)
	{
		if(MInvoice.isComplete(get_TrxName(), order.getC_Invoice_ID()))
			return;
		
//		boolean isAnyDiscount = MUNSDiscountTrx.get(invoice).length > 0;
		boolean isNeedRecalcDiscount = false;
		MUNSPackingListLine[] lines = order.getLines();
		for(int i=0; i<lines.length; i++)
		{
			MUNSPackingListLine line = lines[i];

			String sql = "SELECT 1 FROM UNS_DiscountTrx WHERE C_OrderLine_ID=?";
			boolean discTrxExists = DB.getSQLValueEx(get_TrxName(), sql, line.getC_OrderLine_ID()) > 0;
			
			if(!discTrxExists)
				continue;
			else {
				sql = "SELECT QtyEntered FROM C_OrderLine WHERE C_OrderLine_ID=" + line.getC_OrderLine_ID();
				BigDecimal orderQtyEntered = DB.getSQLValueBDEx(get_TrxName(), sql);
				
				sql = "SELECT QtyEntered FROM C_InvoiceLine WHERE C_InvoiceLine_ID=" + line.getC_InvoiceLine_ID();
				BigDecimal invoiceQtyEntered = DB.getSQLValueBDEx(get_TrxName(), sql);
				
				if (orderQtyEntered.compareTo(invoiceQtyEntered) == 0)
					continue;
			}
			
			isNeedRecalcDiscount = true;
			break;
		}
		
		if(!isNeedRecalcDiscount)
			return;
		
		MInvoice invoice = new MInvoice(getCtx(), order.getC_Invoice_ID(), get_TrxName());

		invoice.setProcessed(true);
		invoice.saveEx();

		invoice.setProcessInfo(this.getProcessInfo());
		
		UNSInvoiceDiscountModel model = new UNSInvoiceDiscountModel(invoice);
		InvoiceCalculateDiscount calc = new InvoiceCalculateDiscount(model);
		calc.run();
	}
	
	
	protected String moveItems()
	{
		StringBuilder sb = new StringBuilder();
		MUNSPLConfirm[] confirms = MUNSPLConfirm.gets(this);
		for(MUNSPLConfirm confirm : confirms)
		{
			confirm.setGridTab(getGridTab());
			confirm.setProcessInfo(getProcessInfo());
			confirm.setReversal(isReversal());
			String error = confirm.move();
			if(Util.isEmpty(error, true))
				continue;
			
			sb.append(error);
		}
		
		String msg = sb.toString();
		if(!Util.isEmpty(msg, true))
			return msg;
		
		return null;
	}
	
	protected String shipOrder()
	{
		MUNSPLConfirm confirm = MUNSPLConfirm.get(this);
		MUNSPLConfirmProduct[] products = confirm.getProducts();
		for(int i=0; i<products.length; i++)
		{
			BigDecimal confirmedQty = products[i].getConfirmedQty();
			MUNSPLConfirmLine[] details = products[i].getLines();
			for(int j=0; j<details.length; j++)
			{
				MUNSPLConfirmLine detail = details[j];
				MUNSPackingListLine pll = (MUNSPackingListLine) detail.getUNS_PackingList_Line();
				MInOutLine ioLine = new MInOutLine(getCtx(), detail.getM_InOutLine_ID(), get_TrxName());
				ioLine.setQty(confirmedQty);
				pll.setMovementQty(confirmedQty);
				pll.setQtyEntered(confirmedQty);
				pll.setQtyShipping(confirmedQty);
				confirmedQty = confirmedQty.subtract(ioLine.getQtyEntered());
				try
				{
					ioLine.saveEx();
					pll.saveEx();
				}
				catch (Exception e)
				{
					throw new AdempiereException(e.getMessage());
				}
			}
		}
		
		MUNSPackingListOrder[] orders = getOrders();
		for (int o=0; o<orders.length; o++)
		{
			MInOut inout = new MInOut(getCtx(), orders[o].getM_InOut_ID(), get_TrxName());
			boolean ok = inout.processIt(DocAction.ACTION_Complete);
			if (!ok)
			{
				return "Can't complete Shipment Document : " + inout.getProcessMsg();
			}
		}
		
		return null;
	}
	
	private Hashtable<String, MMovementLine> m_mapMoveLine = new Hashtable<>();
	private Hashtable<String, MInventoryLine> m_mapInveLine = new Hashtable<>();
	
	@Deprecated
	void completeInventoryOrMove()
	{
		Hashtable<String, MMovement> mapMove = new Hashtable<>();
		Hashtable<String, MInventory> mapInv = new Hashtable<>();
		for(MMovementLine line : m_mapMoveLine.values())
		{
			String key = "" + line.getAD_Org_ID() + "-" + line.getM_LocatorTo().getM_Warehouse_ID();
			MMovement move = mapMove.get(key);
			if(null == move)
			{
				move = new MMovement(getCtx(), 0, get_TrxName());
				move.setAD_Org_ID(line.getAD_Org_ID());
				move.setDestinationWarehouse_ID(line.getM_LocatorTo().getM_Warehouse_ID());
				move.setSalesRep_ID(Env.getAD_User_ID(getCtx()));
				move.setMovementDate(getDateDoc());
				move.setIsInTransit(false);
				move.setIsInternalWarehouseConfirm(false);
				move.setAD_OrgTrx_ID(line.getAD_Org_ID());
				move.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialMovement));
				move.setDeliveryRule(MMovement.DELIVERYRULE_Manual);
				move.setDescription("Auto create on test packing list " + getDocumentNo());
				move.setDocumentNo("MPL-" + getDocumentNo());
				move.saveEx();
				mapMove.put(key, move);
			}
			line.setM_Movement_ID(move.get_ID());
			line.saveEx();
		}
		
		for(MInventoryLine line : m_mapInveLine.values())
		{
			String key = "" + line.getAD_Org_ID() + "-" + line.getM_Locator().getM_Warehouse_ID();
			MInventory inventory= mapInv.get(key);
			if(null == inventory)
			{
				inventory = new MInventory(getCtx(), 0, get_TrxName());
				inventory.setAD_Org_ID(line.getAD_Org_ID());
				inventory.setM_Warehouse_ID(line.getM_Locator().getM_Warehouse_ID());
				inventory.setMovementDate(getDateDoc());
				inventory.setAD_OrgTrx_ID(line.getAD_Org_ID());
				inventory.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventory));
				inventory.setDescription("Auto create on test packing list " + getDocumentNo());
				inventory.setDocumentNo("IPL-" + getDocumentNo());
				inventory.saveEx();
				mapInv.put(key, inventory);
			}
			line.setM_Inventory_ID(inventory.get_ID());
			line.saveEx();
		}
		
		for(MInventory Inv : mapInv.values())
		{
			boolean success = Inv.processIt(DocAction.ACTION_Complete);
			if(!success)
			{
				m_processMsg = Inv.getProcessMsg();
				break;
			}
		}
		
		for(MMovement move : mapMove.values())
		{
			boolean success = move.processIt(DocAction.ACTION_Complete);
			if(!success)
			{
				m_processMsg = move.getProcessMsg();
				break;
			}
		}
	}

	@Override
	public String doAutoComplete() 
	{
		if (getDocStatus().equals(DOCSTATUS_Completed))
		{
			setDocStatus(DOCSTATUS_Drafted);
			setDocAction(DOCACTION_Complete);
			setProcessed(false);
			disableModelValidation();
			saveEx();
			enableModelValidation();
		}
		
		MUNSPLConfirm confirm = MUNSPLConfirm.get(this);
		
		confirm.setDocStatus(DOCSTATUS_Drafted);
		confirm.setDocAction(DOCACTION_Complete);
		confirm.setProcessed(false);
		confirm.disableModelValidation();
		confirm.saveEx();
		confirm.enableModelValidation();
		
		if (!confirm.processIt(DOCACTION_Complete))
			return confirm.m_processMsg;
		
		if (!processIt(DOCACTION_Complete))
			return m_processMsg;
		
		MUNSPLReturn plReturn = 
				Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
						MUNSPLReturn.Table_Name, "UNS_PackingList_ID=?", get_TrxName())
				.setParameters(get_ID())
				.first();
		
		if (plReturn == null)
			return "Failed. PL Return not created after completing Packing List no " + getDocumentNo();
		
		if (!plReturn.processIt(DOCACTION_Complete))
			return plReturn.m_processMsg;
		
		return null;
	}

	@Override
	public void resetDocStatus() {
		// Do nothing. handle it on doAutoComplete method instead.
	}
	
	public boolean isPLReturnCompleted ()
	{
		return m_isPLReturnCompleted;
	}
	
	public void setPLReturnCompleted (boolean isPLReturnCompleted)
	{
		m_isPLReturnCompleted = isPLReturnCompleted;
	}
}
