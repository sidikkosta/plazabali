/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * @author ALBURHANY
 *
 */
public class MUNSCustomerRedeem extends X_UNS_CustomerRedeem implements DocAction {

	private String m_Message = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8572423883114971226L;

	/**
	 * @param ctx
	 * @param UNS_CustomerRedeem_ID
	 * @param trxName
	 */
	public MUNSCustomerRedeem(Properties ctx, int UNS_CustomerRedeem_ID,
			String trxName) {
		super(ctx, UNS_CustomerRedeem_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCustomerRedeem(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSCustomerRedeem[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_CustomerRedeem_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{		
		if(checkDuplicate() != null)
			throw new AdempiereException(m_Message);
		return true; //	beforeSave
	}	
	
	protected boolean beforeDelete()
	{
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(getLine(get_ID()).length == 0)
		{
			m_processMsg = "Cannot process this document, have must one or more redeem line";
			return DocAction.STATUS_Invalid;
		}
		
		if(getBalancePoint().compareTo(Env.ZERO) == -1)
			throw new AdempiereException("Balance point disallowed negate");
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		
		if(getLine(get_ID()).length == 0)
			throw new AdempiereUserError("Must have one or more line for process this document");
		
		if(upCustomerPoint() != null)
			throw new AdempiereException(m_Message);
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public MUNSCustomerRedeemLine[] getLine (int UNS_CustomerRedeem_ID)
	{
		List<MUNSCustomerRedeemLine> cRedeem = new Query(getCtx(), MUNSCustomerRedeemLine.Table_Name,
				COLUMNNAME_UNS_CustomerRedeem_ID + "=?", get_TrxName()).setParameters(UNS_CustomerRedeem_ID)
					.list();
		
		return cRedeem.toArray(new MUNSCustomerRedeemLine[cRedeem.size()]);
	}
	
	public MUNSAdditionalPoint[] getAddPoint(int UNS_CustomerRedeem_ID)
	{
		List<MUNSAdditionalPoint> addPoint = new Query(getCtx(), MUNSAdditionalPoint.Table_Name,
				COLUMNNAME_UNS_CustomerRedeem_ID + "=?", get_TrxName()).setParameters(UNS_CustomerRedeem_ID)
					.list();
		
		return addPoint.toArray(new MUNSAdditionalPoint[addPoint.size()]);
	}
	
	public boolean cekPoint()
	{
		return true;
	}
	
	public String checkDuplicate()
	{		
		m_Message = null;
		
		String sql = "SELECT UNS_CustomerRedeem_ID FROM UNS_CustomerRedeem WHERE C_BPartner_ID = " + getC_BPartner_ID()
				+ " AND DocStatus IN ('DR', 'IV', 'IP') AND UNS_CustomerRedeem_ID <> " + get_ID();
		if(!MSysConfig.getBooleanValue(MSysConfig.MERGE_ORG_POINT, true))
			sql = sql + " AND AD_Org_ID = " + getAD_Org_ID();
		
		int exist = DB.getSQLValue(get_TrxName(), sql);
		if(exist >= 1)
		{
			MUNSCustomerRedeem crOld = new MUNSCustomerRedeem(getCtx(), exist, get_TrxName());
			m_Message = "Has occured document redeem in progress for BPartner :: " + getC_BPartner().getName()
							+ " in document number :: " + crOld.getDocumentNo();
		}
		return m_Message;
	}
	
	public BigDecimal getBalancePoint()
	{
		BigDecimal balancePoint = getCurrentPoint().subtract(getPointRedeem()).add(getTotalAdditionalPoint());
		
		return balancePoint;
	}
	
	public String finallyCheckPoint()
	{
		m_Message = null;
		
		BigDecimal point = getPointRedeem().subtract(getTotalAddPoint());
		
		if(point.compareTo(Env.ZERO) == -1)
			return m_Message = "Additional Point cannot bigger than Total Redeemed Point";
			
		MUNSCustomerRedeem cr = new MUNSCustomerRedeem(getCtx(), getUNS_CustomerRedeem_ID(), get_TrxName());
		
		cr.setCurrentPoint(point);
		if(!cr.save())
			m_Message = "Error occured when trying update customer redeem";
		
		return m_Message;
	}
	
	public String upCustomerPoint()
	{
		m_Message = null;
		
		MUNSCustomerPoint cp = new MUNSCustomerPoint(getCtx(), getUNS_CustomerPoint_ID(), get_TrxName());
		
		cp.setAccumulatedRedeemed(cp.getAccumulatedRedeemed().add((getPointRedeem().subtract(getTotalAdditionalPoint()))));
		
		BigDecimal AdditionalPoint = getTotalAdditionalPoint();
		if(AdditionalPoint.compareTo(Env.ZERO) == 1)
		{	
			cp.setGiftedPointFromOthers(cp.getGiftedPointFromOthers().add(getTotalAdditionalPoint()));
			for(MUNSAdditionalPoint ap : getAddPoint(get_ID()))
			{
				String sql = "UPDATE UNS_CustomerPoint SET AwardedPointToOthers = COALESCE(AwardedPointToOthers,0) + " + ap.getValuePoint()
								+ " WHERE C_BPartner_ID = " + ap.getC_BPartner_ID();
				
				if(DB.executeUpdate(sql, get_TrxName())<= 0)
					m_Message = "Error when trying update Customer Point";
			}
		}
		cp.setLatestRedemptionDate(getDateDoc());
		if(!cp.save())
			throw new AdempiereException("Error occured when trying update Point Redeem in customer point");

		return m_Message;
	}
	
	public BigDecimal getTotalAdditionalPoint()
	{
		BigDecimal AdditionalPoint = Env.ZERO;
		
		String sql = "SELECT COALESCE(SUM(ValuePoint),0) FROM UNS_AdditionalPoint WHERE isActive = 'Y'"
				+ " AND UNS_CustomerRedeem_ID = " + get_ID();
		AdditionalPoint = DB.getSQLValueBD(get_TrxName(), sql);
		
		return AdditionalPoint;
	}
	
	public BigDecimal getRedemeedPoint()
	{
		BigDecimal RedemeedPoint = Env.ZERO;
		
		String sql = "SELECT COALESCE(SUM(ValuePoint),0) FROM UNS_CustomerRedeem_Line WHERE isActive = 'Y'"
				+ " AND UNS_CustomerRedeem_ID = " + get_ID();
		RedemeedPoint = DB.getSQLValueBD(get_TrxName(), sql);
		
		return RedemeedPoint;
	}
	
	public MUNSCustomerRedeem getStatusDoc(Properties ctx, int C_BPartner_ID, String trxName)
	{
		MUNSCustomerRedeem cr = null;
		String whereClause = "C_BPartner_ID = ? AND DocStatus IN ('DR', 'IP', 'IV')";
		
		cr = new Query(ctx, Table_Name, whereClause, trxName).setParameters(C_BPartner_ID).first();
		
		return cr;
	}
}