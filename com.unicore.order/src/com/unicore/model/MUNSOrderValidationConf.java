/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSOrderValidationConf extends X_UNS_OrderValidationConf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9015205234887783437L;

	/**
	 * @param ctx
	 * @param UNS_OrderValidationConf_ID
	 * @param trxName
	 */
	public MUNSOrderValidationConf(Properties ctx,
			int UNS_OrderValidationConf_ID, String trxName) {
		super(ctx, UNS_OrderValidationConf_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSOrderValidationConf(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT COUNT(*) FROM UNS_OrderValidationConf"
				+ " WHERE UNS_OrderValidationConf_ID <> ?"
				+ " AND ProcurementType = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, get_ID(), getProcurementType());
		
		if(count > 0)
		{
			log.saveError("Error", "Duplicate configuration for this type.");
			return false;
		}
		
		if(getPercent().compareTo(Env.ZERO) <= 0)
		{
			log.saveError("Error", "You must define a value more than zero");
			return false;
		}
		
		return true;
	}
	
	public static BigDecimal getPercent(String procurementType)
	{
		String sql = "SELECT Percent FROM UNS_OrderValidationConf"
				+ " WHERE ProcurementType = ? AND IsActive = 'Y'";
		BigDecimal perc = DB.getSQLValueBD(null, sql, procurementType);
		if(perc == null)
			perc = Env.ZERO;
		return perc;
	}
}