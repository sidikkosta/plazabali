/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBankStatement;
import org.compiere.model.MBankStatementLine;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.model.IUNSApprovalInfo;
import com.uns.util.UNSApps;

/**
 * @author Menjangan
 *
 */
public class MUNSVoucherBook extends X_UNS_VoucherBook implements DocAction, DocOptions, IUNSApprovalInfo {

	private static final long serialVersionUID = -7403596698616129178L;
	private boolean m_justPrepared = false;
	private String m_processMsg = null;
	private MUNSVoucherCode[] m_vouchers = null;
	/**
	 * @param ctx
	 * @param UNS_VoucherBook_ID
	 * @param trxName
	 */
	public MUNSVoucherBook(Properties ctx, int UNS_VoucherBook_ID,
			String trxName) 
	{
		super(ctx, UNS_VoucherBook_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSVoucherBook(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (DOCSTATUS_Completed.equals(docStatus))
		{
			options[index++] = DOCACTION_Void;
			options[index++] = DOCACTION_Close;
		}
		return index;
	}

	@Override
	public boolean processIt(String action) throws Exception 
	{
		addLog(Level.INFO, "Process-it");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() 
	{
		addLog(Level.INFO, "Unlock-it");
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() 
	{
		addLog(Level.INFO, "Invalidate-It");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() 
	{
		addLog(Level.INFO, "Prepare-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		getLines(true);
		
//		if (m_vouchers.length == 0)
//		{
//			m_processMsg = "@NoLines@";
//			return DOCSTATUS_Invalid;
//		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() 
	{
		addLog(Level.INFO, "Approve-It");
		setProcessed(true);
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() 
	{
		addLog(Level.INFO, "Reject-It");
		setProcessed(false);
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() 
	{
		addLog(Level.INFO, "Complete-It");
		if (m_justPrepared && !DOCSTATUS_InProgress.equals(prepareIt()))
			return DOCSTATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		if(!isReplication() && !generateVoucherCode())
			return DOCSTATUS_Invalid;
		
		if(getC_Charge_ID() <= 0 && !isReplication())
		{
			if(!createStatement(false))
			{
				m_processMsg = "Error when try to create statement";
				return DOCSTATUS_Invalid;
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DOCSTATUS_Invalid;
		
		if (!isApproved())
			approveIt();
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		
		addLog(Level.INFO, "Void it");
		String status = getDocStatus();
		
		if(DOCSTATUS_Closed.equals(status))
		{
			m_processMsg = "Document Closed";
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		if (getTotalUsedAmt().signum() != 0)
		{
			m_processMsg = "Couldn't void document. Some voucher already used";
			return false;
		}
		
		if(getC_Charge_ID() <= 0 && !isReplication())
		{
			if(!createStatement(true))
			{
				m_processMsg = "Error when try to create reversal statement";
				return false;
			}
		}
		
		String sql = "UPDATE UNS_VoucherCode set IsInvalidate = 'Y' WHERE UNS_VoucherBook_ID = ? ";
		int ok = DB.executeUpdate(sql, getUNS_VoucherBook_ID(), get_TrxName());
		if(ok == -1)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		addLog(Level.INFO, "Void-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_CLOSE);
		if (null != m_processMsg)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_CLOSE);
		if (null != m_processMsg)
			return false;
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		m_processMsg = "Invalid action Reverse CorrectIt";
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		m_processMsg = "Invalid action Reverse AccrualIt";
		return false;
	}

	@Override
	public boolean reActivateIt() {
		m_processMsg = "Invalid action Reactivate-It";
		return false;
	}

	@Override
	public String getSummary() {
		StringBuilder info = new StringBuilder("Document No:")
		.append(getDocumentNo()).append(", Name:").append(getName())
		.append(",Voucher Amt:").append(getVoucherAmt()).append(",Total Amount:")
		.append(getTotalAmt());
		String summary = info.toString();
		return summary;
	}

	@Override
	public String getDocumentInfo() {
		return getSummary();
	}

	@Override
	public File createPDF() {
		//TODO not implemented
		return null;
	}

	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO static value
		return 303;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		return getTotalAmt();
	}

	private void addLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		
		log.log(level, msg);
	}
	
	public void addDescription (String desc)
	{
		String description = getDescription();
		if (null == description)
			description = desc;
		else
			description += " | " + desc;
		
		setDescription(description);
	}
	
	public MUNSVoucherCode[] getLines (boolean requery)
	{
		if (m_vouchers != null && !requery)
		{
			set_TrxName(m_vouchers, get_TrxName());
			return m_vouchers;
		}
		
		List<MUNSVoucherCode> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSVoucherCode.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).
				setOrderBy(MUNSVoucherCode.COLUMNNAME_Name).
				list();
		
		m_vouchers = new MUNSVoucherCode[list.size()];
		list.toArray(m_vouchers);
		
		return m_vouchers;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (getValidFrom().after(getValidTo()))
		{
			log.saveError("SaveError", "Valid To must be greather than Valid From");
			return false;
		}
		
		if(isProcessed())
			return true;
		
		if (getStartNo() > getEndNo())
		{
			log.saveError("SaveError", "End No must be greather than Start No");
			return false;
		}
		
		if (!isValidVoucherText())
		{
			log.saveError("SaveError", "Max length must be equals or greater than "
					+ "total character length of Prefix+Code+Suffix.");
			return false;
		}
		
		if(!isValidCode())
		{
			log.saveError("SaveError", "The code has been used");
			return false;
		}

		BigDecimal totalVoucherAmt = new BigDecimal(getCount()).multiply(getVoucherAmt());
		BigDecimal totalAmt = getChargeVoucherAmt().add(getVoucherPrice());
		
		setTotalVoucherAmt(totalVoucherAmt);
		setTotalAmt(totalAmt);
		
		return super.beforeSave(newRecord);
	}
	
	public boolean isDistributed (String voucherCode)
	{
		boolean isDistributed = false;
		String sql = "SELECT Count (*) FROM UNS_VoucherCode WHERE UNS_VoucherBook_ID = ?"
				+ " AND Name = ?";
		int counter = DB.getSQLValue(get_TrxName(), sql, getUNS_VoucherBook_ID(), voucherCode);
		isDistributed = counter > 0;
		return isDistributed;
	}
	
	private boolean isValidVoucherText ()
	{
		String maxVoucher = "";
		if (getPrefix() != null)
			maxVoucher += getPrefix();
		maxVoucher += getEndNo();
		if (getSufix() != null)
			maxVoucher += getSufix();
		
		int length = maxVoucher.length();
		
		return length <= getMaxLength();
	}
	
	public boolean generateVoucherCode()
	{
		String insert = new StringBuffer("INSERT INTO UNS_VoucherCode")
		.append("(AD_Org_ID, AD_Client_ID, created, createdby, updated, updatedby, processed, ")
		.append("UNS_VoucherBook_ID, UNS_VoucherCode_ID, UNS_VoucherCode_UU, Description, Name, Value, ")
		.append("VoucherAmt, UsedAmt, UnusedAmt, IsInvalidate)").toString();
		
		StringBuffer values = new StringBuffer("VALUES (");
		
		int counter = 0;

		for(int i = getStartNo() ; i <= getEndNo() ; i++)
		{
			String voucherCode = "";
			String prefix = getPrefix() != null ? getPrefix() : "";
			String sufix = getSufix() != null ? getSufix() : "";
			
			voucherCode += prefix;
			
			String code = String.valueOf(i);
			
			while(code.length() < (getMaxLength() - prefix.length() - sufix.length()))
				code = "0"+code;
			
			voucherCode += code;
			
			voucherCode += sufix;
			
			if(counter == 0) {
				values = new StringBuffer(" VALUES ");
			}
			else {
				values.append(", ");
			}
			
			values.append("(" + this.getAD_Org_ID() + ", " + this.getAD_Client_ID() 
					+ ", now(), " + Env.getContext(getCtx(), "#AD_User_ID")
					+ ", now(), " + Env.getContext(getCtx(), "#AD_User_ID") + ", 'Y', "
					+ getUNS_VoucherBook_ID() + ", getnextid('UNS_VoucherCode'::Character Varying), generate_uuid(), '', "
					+ "'" + voucherCode + "', '" + voucherCode + "', "
					+ getVoucherAmt() + ", 0, " + getVoucherAmt() + ", 'N')");
			
			//counter = counter == 500? 0 : counter+1;
			if (counter == 500 || i == getEndNo())
			{
				String sql = values.insert(0, insert).toString();
				PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
				try {
					int count = stmt.executeUpdate();
					if (count == 0)
						throw new SQLException("Failed when inserting new voucher-codes.");
				} 
				catch (SQLException e) {
					e.printStackTrace();
					throw new AdempiereException(e.getMessage());
				}
				finally {
					DB.close(stmt);
				}
				counter = 0;
			}
			else {
				counter++;
			}

			/**
			MUNSVoucherCode vouchCode = new MUNSVoucherCode(getCtx(), 0, get_TrxName());
			vouchCode.setAD_Org_ID(getAD_Org_ID());
			vouchCode.setUNS_VoucherBook_ID(getUNS_VoucherBook_ID());
			vouchCode.setName(voucherCode);
			vouchCode.setVoucherAmt(getVoucherAmt());
			vouchCode.setUsedAmt(Env.ZERO);
			vouchCode.setUnusedAmt(getVoucherAmt());
			vouchCode.setProcessed(true);
			vouchCode.setisInvalidate(false);
			vouchCode.setNeedUpHeader(false);
			vouchCode.saveEx();
			**/
		}
		
		return MUNSVoucherCode.updateHeader(this);
	}
	
	public boolean isValidCode()
	{		
	
		String sql = "SELECT 1 FROM UNS_VoucherBook WHERE ";
		if(getPrefix() != null)
			sql += "Prefix = '"+getPrefix()+"'";
		else
			sql += "Prefix is null";
		
		if (!sql.endsWith("WHERE "))
			sql += " AND ";
		
		if(getSufix() != null)
			sql += "Sufix = '"+getSufix()+"'";
		else
			sql += "Sufix is null";
		if (!sql.endsWith("WHERE "))
			sql += " AND ";
		
		sql += " UNS_VoucherBook_ID <> ? AND DocStatus NOT IN ('VO','RE') ";
		sql += " and (((? >= StartNo AND ? <= EndNo) OR (? >= StartNo AND ? <= EndNo)) "
					+ "OR ((StartNo >= ? AND StartNo <= ?) OR (EndNo >= ? AND EndNo <= ?)))";
		boolean exist = DB.getSQLValue(get_TrxName(), sql, 
				getUNS_VoucherBook_ID(),getStartNo(), getStartNo(), getEndNo(), getEndNo()
				, getStartNo(), getEndNo(), getStartNo(), getEndNo()) > 0;
		
//		if(exist)
//		{
//			String sqql = "SELECT 1 FROM UNS_VoucherBook WHERE  AND UNS_VoucherBook_ID <> ? "
//					+ "AND DocStatus NOT IN ('RE','VO')";
//			exist = DB.getSQLValue(get_TrxName(), sqql, getStartNo(), getStartNo(), getEndNo(), getEndNo()
//					, getStartNo(), getEndNo(), getStartNo(), getEndNo(), getUNS_VoucherBook_ID()) > 0;
//			
			if(exist)
				return false;
//		}
		
		return true;
	}
	
	public boolean createStatement(boolean reversal)
	{
		MBankStatementLine line = null;
		if (getC_BankStatementLine_ID() > 0)
			line = new MBankStatementLine(getCtx(), getC_BankStatementLine_ID(), 
					get_TrxName());
		if (null == line && !reversal)
		{
			MBankStatement stmt = MBankStatement.getOpen(getC_BankAccount_ID(), 
					get_TrxName(), true);
			line = new MBankStatementLine(stmt);
			line.setStatementLineDate(getDateAcct());
			line.setTransactionType("ART");
			line.setDescription("Auto Generated From Voucher Book :: "+getName());
			line.setStmtAmt(getTotalAmt());
			line.setAmount(getTotalAmt());
			line.setChargeAmt(getTotalAmt());
			line.setC_Currency_ID(getC_Currency_ID());
			int charge_ID = UNSApps.getRefAsInt(UNSApps.CHRG_INTRANSITCASH);
			line.setC_Charge_ID(charge_ID);
			line.saveEx();
		}
		else if(reversal)
		{
			if(line == null)
				throw new AdempiereException("Cannot get Statement Line");
			
			MBankStatementLine revLine = new MBankStatementLine(new MBankStatement(getCtx(),
					line.getC_BankStatement_ID(), get_TrxName()));
	
			revLine.setStatementLineDate(getDateAcct());
			revLine.setTransactionType("ART");
			revLine.setDescription("Auto Generated From Voucher Book :: "+getName());
			revLine.setStmtAmt(getTotalAmt().negate());
			revLine.setAmount(getTotalAmt().negate());
			revLine.setChargeAmt(getTotalAmt().negate());
			revLine.setC_Currency_ID(getC_Currency_ID());
			revLine.setC_Charge_ID(line.getC_Charge_ID());
			revLine.saveEx();
			
		}
		setC_BankStatementLine_ID(line.getC_BankStatementLine_ID());
		return true;
	}
	
	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		
		List<Object[]> list = new ArrayList<>();
		list.add(new Object[]{String.class, true}); //nama
		list.add(new Object[]{String.class, true}); //jumlah voucher
		list.add(new Object[]{BigDecimal.class, true}); //no voucher
		list.add(new Object[]{String.class, true}); //type
		list.add(new Object[]{String.class, true}); //validFrom
		list.add(new Object[]{String.class, true}); //ValidTo
		list.add(new Object[]{String.class, true}); //Cash/bank Account
		list.add(new Object[]{String.class, true}); //Charge
		list.add(new Object[]{String.class, true}); // ChargeVoucher
		list.add(new Object[]{BigDecimal.class, true}); // ChargeVoucherAmount
		list.add(new Object[]{BigDecimal.class, true}); //VoucherAmount
		list.add(new Object[]{BigDecimal.class, true}); //VoucherPrice
		list.add(new Object[]{BigDecimal.class, true}); //TotalVoucherAmount
		
		return list;
	}
	
	@Override
	public String[] getDetailTableHeader() {
		
		String[] header = new String[]{"Nama","No Voucher","Jml Voucher","Type","Valid From","Valid To","Cash/Bank Account"
				,"Charge","Charge Voucher","Charge Voucher Amount","Voucher Amount","Voucher Price","Total Amount"};
		
		return header;
	}
	
	@Override
	public List<Object[]> getDetailTableContent() {
		
		List<Object[]> list = new ArrayList<>();
		String sql = "SELECT vb.name AS vName,"
				+ " ROUND(vb.count,0),"
				+ " (CASE WHEN vb.type='VO' THEN 'One Transaction' ELSE 'Multy Transaction' END) AS type,"
				+ " vb.validfrom,"
				+ " vb.validto,"
				+ " acc.name AS bankAccount,"
				+ " COALESCE(crg.name,'-') AS charge,"
				+ " COALESCE(vcrg.name,'-') AS chargeVoucher,"
				+ " COALESCE(vb.chargevoucheramt,'0'),"
				+ " vb.voucheramt,"
				+ " vb.voucherprice,"
				+ " vb.totalamt"
				+ " FROM uns_voucherbook vb"
				+ " LEFT OUTER JOIN c_charge crg ON crg.c_charge_id = vb.c_charge_id"
				+ " LEFT OUTER JOIN c_charge vcrg ON vcrg.c_charge_id = vb.chargevoucher_id"
				+ " LEFT OUTER JOIN c_bankaccount acc ON acc.c_bankaccount_id = vb.c_bankaccount_id"
				+ " WHERE vb.uns_voucherbook_id = ?";
		
		ResultSet rs = null;
		PreparedStatement sta = null;
		
		try {
			sta = DB.prepareStatement(sql, get_TrxName());
			sta.setInt(1, getUNS_VoucherBook_ID());
			rs = sta.executeQuery();
			while(rs.next())
			{
				String startVCode = initVoucherCode(getStartNo());
				String endVCode = initVoucherCode(getEndNo());
				
				int count = 0;
				Object[] obj = new Object[13];
				
				obj[count++] = rs.getObject(1);
				obj[count++] = startVCode+" - "+endVCode;
				obj[count++] = rs.getObject(2);
				obj[count++] = rs.getObject(3);
				obj[count++] = rs.getObject(4).toString().substring(0,10);
				obj[count++] = rs.getObject(5).toString().substring(0,10);
				obj[count++] = rs.getObject(6);
				obj[count++] = rs.getObject(7);
				obj[count++] = rs.getObject(8);
				obj[count++] = rs.getObject(9);
				obj[count++] = rs.getObject(10);
				obj[count++] = rs.getObject(11);
				obj[count++] = rs.getObject(12);
				
				list.add(obj);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally{
			DB.close(rs);
		}
		
		return list;
	}
	
	private String initVoucherCode(int no)
	{
		String voucherCode = "";
		String prefix = getPrefix() != null ? getPrefix() : "";
		String sufix = getSufix() != null ? getSufix() : "";
		
		voucherCode += prefix;
		
		String code = Integer.toString(no);
		
		while(code.length() < (getMaxLength() - prefix.length() - sufix.length()))
			code = "0"+code;
		
		voucherCode += code;
		
		voucherCode += sufix;
		
		return voucherCode;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}
