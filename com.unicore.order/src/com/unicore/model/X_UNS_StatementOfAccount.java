/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_StatementOfAccount
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_StatementOfAccount extends PO implements I_UNS_StatementOfAccount, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191129L;

    /** Standard Constructor */
    public X_UNS_StatementOfAccount (Properties ctx, int UNS_StatementOfAccount_ID, String trxName)
    {
      super (ctx, UNS_StatementOfAccount_ID, trxName);
      /** if (UNS_StatementOfAccount_ID == 0)
        {
			setC_BPartner_ID (0);
			setC_BPartner_Location_ID (0);
			setC_DocType_ID (0);
			setContributionAmt (Env.ZERO);
// 0
			setC_Period_ID (0);
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
			setDateFrom (new Timestamp( System.currentTimeMillis() ));
			setDateTo (new Timestamp( System.currentTimeMillis() ));
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setGrandTotal (Env.ZERO);
// 0
			setIsApproved (false);
// N
			setOtherCosts (Env.ZERO);
// 0
			setPosted (false);
// N
			setProcessed (false);
// N
			setPushMoneyAmt (Env.ZERO);
// 0
			setShippingCost (Env.ZERO);
// 0
			setShopType (null);
			setTotalLines (Env.ZERO);
// 0
			setUNS_StatementOfAccount_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_StatementOfAccount (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_StatementOfAccount[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner_Location)MTable.get(getCtx(), org.compiere.model.I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_Invoice)MTable.get(getCtx(), org.compiere.model.I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Contribution Amount.
		@param ContributionAmt Contribution Amount	  */
	public void setContributionAmt (BigDecimal ContributionAmt)
	{
		set_Value (COLUMNNAME_ContributionAmt, ContributionAmt);
	}

	/** Get Contribution Amount.
		@return Contribution Amount	  */
	public BigDecimal getContributionAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ContributionAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Payment getC_Payment() throws RuntimeException
    {
		return (org.compiere.model.I_C_Payment)MTable.get(getCtx(), org.compiere.model.I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_Value (COLUMNNAME_C_Payment_ID, null);
		else 
			set_Value (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Period getC_Period() throws RuntimeException
    {
		return (org.compiere.model.I_C_Period)MTable.get(getCtx(), org.compiere.model.I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create Payment.
		@param CreatePayment Create Payment	  */
	public void setCreatePayment (String CreatePayment)
	{
		set_Value (COLUMNNAME_CreatePayment, CreatePayment);
	}

	/** Get Create Payment.
		@return Create Payment	  */
	public String getCreatePayment () 
	{
		return (String)get_Value(COLUMNNAME_CreatePayment);
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_Value (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Date From.
		@param DateFrom 
		Starting date for a range
	  */
	public void setDateFrom (Timestamp DateFrom)
	{
		set_Value (COLUMNNAME_DateFrom, DateFrom);
	}

	/** Get Date From.
		@return Starting date for a range
	  */
	public Timestamp getDateFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFrom);
	}

	/** Set Date To.
		@param DateTo 
		End date of a date range
	  */
	public void setDateTo (Timestamp DateTo)
	{
		set_Value (COLUMNNAME_DateTo, DateTo);
	}

	/** Get Date To.
		@return End date of a date range
	  */
	public Timestamp getDateTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTo);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	public org.compiere.model.I_C_DocType getDocTypeInvoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getDocTypeInvoice_ID(), get_TrxName());	}

	/** Set DocType Target Invoice.
		@param DocTypeInvoice_ID DocType Target Invoice	  */
	public void setDocTypeInvoice_ID (int DocTypeInvoice_ID)
	{
		if (DocTypeInvoice_ID < 1) 
			set_Value (COLUMNNAME_DocTypeInvoice_ID, null);
		else 
			set_Value (COLUMNNAME_DocTypeInvoice_ID, Integer.valueOf(DocTypeInvoice_ID));
	}

	/** Get DocType Target Invoice.
		@return DocType Target Invoice	  */
	public int getDocTypeInvoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DocTypeInvoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Grand Total.
		@param GrandTotal 
		Total amount of document
	  */
	public void setGrandTotal (BigDecimal GrandTotal)
	{
		set_Value (COLUMNNAME_GrandTotal, GrandTotal);
	}

	/** Get Grand Total.
		@return Total amount of document
	  */
	public BigDecimal getGrandTotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrandTotal);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_C_Charge getOtherCharge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getOtherCharge_ID(), get_TrxName());	}

	/** Set Other Charge.
		@param OtherCharge_ID Other Charge	  */
	public void setOtherCharge_ID (int OtherCharge_ID)
	{
		if (OtherCharge_ID < 1) 
			set_Value (COLUMNNAME_OtherCharge_ID, null);
		else 
			set_Value (COLUMNNAME_OtherCharge_ID, Integer.valueOf(OtherCharge_ID));
	}

	/** Get Other Charge.
		@return Other Charge	  */
	public int getOtherCharge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_OtherCharge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Other Cost Description.
		@param OtherCostDesc Other Cost Description	  */
	public void setOtherCostDesc (String OtherCostDesc)
	{
		set_Value (COLUMNNAME_OtherCostDesc, OtherCostDesc);
	}

	/** Get Other Cost Description.
		@return Other Cost Description	  */
	public String getOtherCostDesc () 
	{
		return (String)get_Value(COLUMNNAME_OtherCostDesc);
	}

	/** Set Other Costs.
		@param OtherCosts Other Costs	  */
	public void setOtherCosts (BigDecimal OtherCosts)
	{
		set_Value (COLUMNNAME_OtherCosts, OtherCosts);
	}

	/** Get Other Costs.
		@return Other Costs	  */
	public BigDecimal getOtherCosts () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OtherCosts);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Posted.
		@param Posted 
		Posting status
	  */
	public void setPosted (boolean Posted)
	{
		set_Value (COLUMNNAME_Posted, Boolean.valueOf(Posted));
	}

	/** Get Posted.
		@return Posting status
	  */
	public boolean isPosted () 
	{
		Object oo = get_Value(COLUMNNAME_Posted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Print Document.
		@param PrintDocument Print Document	  */
	public void setPrintDocument (String PrintDocument)
	{
		set_Value (COLUMNNAME_PrintDocument, PrintDocument);
	}

	/** Get Print Document.
		@return Print Document	  */
	public String getPrintDocument () 
	{
		return (String)get_Value(COLUMNNAME_PrintDocument);
	}

	/** Set Print History Status Report Per Supplier.
		@param PrintHistoryStatusReportPerSup Print History Status Report Per Supplier	  */
	public void setPrintHistoryStatusReportPerSup (String PrintHistoryStatusReportPerSup)
	{
		set_Value (COLUMNNAME_PrintHistoryStatusReportPerSup, PrintHistoryStatusReportPerSup);
	}

	/** Get Print History Status Report Per Supplier.
		@return Print History Status Report Per Supplier	  */
	public String getPrintHistoryStatusReportPerSup () 
	{
		return (String)get_Value(COLUMNNAME_PrintHistoryStatusReportPerSup);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Push Money Amount.
		@param PushMoneyAmt Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt)
	{
		set_Value (COLUMNNAME_PushMoneyAmt, PushMoneyAmt);
	}

	/** Get Push Money Amount.
		@return Push Money Amount	  */
	public BigDecimal getPushMoneyAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PushMoneyAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Shipping Cost.
		@param ShippingCost Shipping Cost	  */
	public void setShippingCost (BigDecimal ShippingCost)
	{
		set_Value (COLUMNNAME_ShippingCost, ShippingCost);
	}

	/** Get Shipping Cost.
		@return Shipping Cost	  */
	public BigDecimal getShippingCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShippingCost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Bazaar = BZ */
	public static final String SHOPTYPE_Bazaar = "BZ";
	/** Shop Retail Duty Free = DF */
	public static final String SHOPTYPE_ShopRetailDutyFree = "DF";
	/** Shop Retail Duty Paid = DP */
	public static final String SHOPTYPE_ShopRetailDutyPaid = "DP";
	/** Shop F&B = FB */
	public static final String SHOPTYPE_ShopFB = "FB";
	/** Online = OL */
	public static final String SHOPTYPE_Online = "OL";
	/** Set Shop Type.
		@param ShopType Shop Type	  */
	public void setShopType (String ShopType)
	{

		set_Value (COLUMNNAME_ShopType, ShopType);
	}

	/** Get Shop Type.
		@return Shop Type	  */
	public String getShopType () 
	{
		return (String)get_Value(COLUMNNAME_ShopType);
	}

	/** Set Total Lines.
		@param TotalLines 
		Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines)
	{
		set_Value (COLUMNNAME_TotalLines, TotalLines);
	}

	/** Get Total Lines.
		@return Total of all document lines
	  */
	public BigDecimal getTotalLines () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLines);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_SA_Summary getUNS_SA_Summary() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_SA_Summary)MTable.get(getCtx(), com.unicore.model.I_UNS_SA_Summary.Table_Name)
			.getPO(getUNS_SA_Summary_ID(), get_TrxName());	}

	/** Set Statement Of Account (Summary Period).
		@param UNS_SA_Summary_ID Statement Of Account (Summary Period)	  */
	public void setUNS_SA_Summary_ID (int UNS_SA_Summary_ID)
	{
		if (UNS_SA_Summary_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SA_Summary_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SA_Summary_ID, Integer.valueOf(UNS_SA_Summary_ID));
	}

	/** Get Statement Of Account (Summary Period).
		@return Statement Of Account (Summary Period)	  */
	public int getUNS_SA_Summary_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SA_Summary_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Statement Of Account.
		@param UNS_StatementOfAccount_ID Statement Of Account	  */
	public void setUNS_StatementOfAccount_ID (int UNS_StatementOfAccount_ID)
	{
		if (UNS_StatementOfAccount_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAccount_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAccount_ID, Integer.valueOf(UNS_StatementOfAccount_ID));
	}

	/** Get Statement Of Account.
		@return Statement Of Account	  */
	public int getUNS_StatementOfAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StatementOfAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_StatementOfAccount_UU.
		@param UNS_StatementOfAccount_UU UNS_StatementOfAccount_UU	  */
	public void setUNS_StatementOfAccount_UU (String UNS_StatementOfAccount_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_StatementOfAccount_UU, UNS_StatementOfAccount_UU);
	}

	/** Get UNS_StatementOfAccount_UU.
		@return UNS_StatementOfAccount_UU	  */
	public String getUNS_StatementOfAccount_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_StatementOfAccount_UU);
	}
}