/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author nurse
 *
 */
public class MUNSPOSFNBTableLine extends X_UNS_POSFNBTableLine {

	private MUNSPOSFNBTable m_parent = null;
	private static final long serialVersionUID = -170027435344298144L;
	
	public MUNSPOSFNBTable getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPOSFNBTable(getCtx(), getUNS_POSFNBTable_ID(), get_TrxName());
		return m_parent;
	}
	
	public MUNSPOSFNBTableLine (MUNSPOSFNBTable parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		m_parent = parent;
		setClientOrg(parent);
		setUNS_POSFNBTable_ID(parent.get_ID());
	}

	/**
	 * @param ctx
	 * @param UNS_POSFNBTableLine_ID
	 * @param trxName
	 */
	public MUNSPOSFNBTableLine(Properties ctx, int UNS_POSFNBTableLine_ID,
			String trxName) 
	{
		super(ctx, UNS_POSFNBTableLine_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSFNBTableLine(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	public boolean doReserve()
	{
		if (isReserved() && getParent().isStrictReservation())
			return false;
		
		return true;
	}
}
