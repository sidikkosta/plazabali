package com.unicore.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.PO;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.unicore.ui.ISortTabRecord;
import com.uns.base.model.Query;

/**
 * @author AzHaidar
 *
 */
public class MUNSPLReturnLine extends X_UNS_PL_ReturnLine implements ISortTabRecord 
{

	private MUNSPLReturnOrder m_parent = null;
	
	private boolean m_isTabRecordAction = false;
	private MUNSPackingListLine m_PackingListLine = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4380666607260103662L;

	public MUNSPLReturnLine(Properties ctx, int UNS_PL_ReturnLine_ID,
			String trxName) {
		super(ctx, UNS_PL_ReturnLine_ID, trxName);
	}

	public MUNSPLReturnLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	@Override
	public PO getParent()
	{
		if (m_parent == null)
			m_parent = new MUNSPLReturnOrder(getCtx(), getUNS_PL_ReturnOrder_ID(), get_TrxName());
		
		return m_parent;
	}
	
	//private boolean isSavingTabRecordSelection = false;
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if (!newRecord && 
				(getCancelledQty().compareTo(getMovementQty()) > 0 || 
					(getCancelledQty().signum() <= 0 && !m_isTabRecordAction)))
		{
			log.saveError("InvalidCancelledQty", 
					"Quantity to be cancelled must greater than zero and less than movement quantity.");
			return false;
		}
		
		if (is_ValueChanged(COLUMNNAME_CancelledQty))
		{
			String whereClause = "PLReturnLine_ID=? AND isProductAccessories='Y'";
			List<MUNSPLReturnLine> accessoriesList = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
					MUNSPLReturnLine.Table_Name, whereClause, get_TrxName())
					.setParameters(get_ID())
					.list();
			
			for (MUNSPLReturnLine plReturnAccessories : accessoriesList)
			{
				MUNSProductAccessories productAcc = 
						MUNSProductAccessories.getOfProduct(
								getCtx(), getM_Product_ID(), plReturnAccessories.getM_Product_ID(), get_TrxName());
				
				plReturnAccessories.setCancelledQty(getCancelledQty().multiply(productAcc.getQty()));
				
				if (!plReturnAccessories.save())
				{
					log.saveError("", "Product accessories not canceled. Product#" + productAcc.getM_Product());
					return false;
				}
			}
		}
		
		m_isTabRecordAction = false;
		return true;
	}
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (m_PackingListLine != null)
		{
			String whereClause = "UNS_PackingList_Order_ID=? AND PackingList_Line_ID=?";
			List<MUNSPackingListLine> accessoriesList = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
					MUNSPackingListLine.Table_Name, whereClause, get_TrxName())
					.setParameters(getUNS_PackingList_Order_ID(), m_PackingListLine.getUNS_PackingList_Line_ID())
					.list();
			
			for (MUNSPackingListLine plAcccessories : accessoriesList)
			{
				MUNSPLReturnLine rLine = new MUNSPLReturnLine(getCtx(), 0, get_TrxName());
				
				rLine.setUNS_PL_ReturnOrder_ID(getUNS_PL_ReturnOrder_ID());
				
				rLine.setClientOrg(plAcccessories);
				rLine.setC_InvoiceLine_ID(plAcccessories.getC_InvoiceLine_ID());
				rLine.setM_InOutLine_ID(plAcccessories.getM_InOutLine_ID());
				rLine.setM_Product_ID(plAcccessories.getM_Product_ID());
				rLine.setC_UOM_ID(plAcccessories.getC_UOM_ID());
				rLine.setM_Locator_ID(plAcccessories.getM_Locator_ID());
				rLine.setUNS_PackingList_Order_ID(getUNS_PackingList_Order_ID());
				rLine.setC_OrderLine_ID(plAcccessories.getC_OrderLine_ID());
				rLine.setReason(getReason());
				rLine.setCancelledQty(Env.ZERO);
				rLine.setMovementQty(plAcccessories.getMovementQty());
				rLine.setPLReturnLine_ID(get_ID());
				rLine.setisProductAccessories(true);
				if (!rLine.save()) {
					log.saveError(null, "Error while returning acceccories product of " + rLine.getM_Product());
				}
			}
		}
		
		return true;
	}

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) 
	{
		this.m_isTabRecordAction = true;
		
		this.setUNS_PL_ReturnOrder_ID(parentRecord_ID);
		
		MUNSPLReturnOrder returnOrder = (MUNSPLReturnOrder) getParent();
		
		String whereClause = "M_Product_ID=? AND UNS_PackingList_Order_ID=? AND "
				+ "(PackingList_Line_ID IS NULL OR PackingList_Line_ID=0)";
		MUNSPackingListLine pll = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSPackingListLine.Table_Name, whereClause, get_TrxName())
				.setParameters(getM_Product_ID(), returnOrder.getUNS_PackingList_Order_ID())
				.firstOnly();
		
		m_PackingListLine = pll;
		
		this.setClientOrg(pll);
		this.setC_InvoiceLine_ID(pll.getC_InvoiceLine_ID());
		this.setM_InOutLine_ID(pll.getM_InOutLine_ID());
		this.setC_UOM_ID(pll.getC_UOM_ID());
		this.setM_Locator_ID(pll.getM_Locator_ID());
		this.setUNS_PackingList_Order_ID(returnOrder.getUNS_PackingList_Order_ID());
		this.setC_OrderLine_ID(pll.getC_OrderLine_ID());
		this.setReason(returnOrder.getReason());
		//this.isSavingTabRecordSelection = true;
		this.setCancelledQty(Env.ZERO);
		this.setMovementQty(pll.getMovementQty());
		
		return null;
	}

	@Override
	public String beforeRemoveSelection() {
		// TODO Auto-generated method stub
		return null;
	}

}
