/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Incentive
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Incentive extends PO implements I_UNS_Incentive, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150317L;

    /** Standard Constructor */
    public X_UNS_Incentive (Properties ctx, int UNS_Incentive_ID, String trxName)
    {
      super (ctx, UNS_Incentive_ID, trxName);
      /** if (UNS_Incentive_ID == 0)
        {
			setIncentive (Env.ZERO);
// 0
			setIncentiveType (null);
			setProcessed (false);
// N
			setUNS_Incentive_ID (0);
			setUNS_IncentiveSchema_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Incentive (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Incentive[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_PaymentTerm getC_PaymentTerm() throws RuntimeException
    {
		return (org.compiere.model.I_C_PaymentTerm)MTable.get(getCtx(), org.compiere.model.I_C_PaymentTerm.Table_Name)
			.getPO(getC_PaymentTerm_ID(), get_TrxName());	}

	/** Set Payment Term.
		@param C_PaymentTerm_ID 
		The terms of Payment (timing, discount)
	  */
	public void setC_PaymentTerm_ID (int C_PaymentTerm_ID)
	{
		if (C_PaymentTerm_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_PaymentTerm_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_PaymentTerm_ID, Integer.valueOf(C_PaymentTerm_ID));
	}

	/** Get Payment Term.
		@return The terms of Payment (timing, discount)
	  */
	public int getC_PaymentTerm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_PaymentTerm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Incentive %.
		@param Incentive Incentive %	  */
	public void setIncentive (BigDecimal Incentive)
	{
		set_Value (COLUMNNAME_Incentive, Incentive);
	}

	/** Get Incentive %.
		@return Incentive %	  */
	public BigDecimal getIncentive () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Incentive);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Incentive New Outlet %.
		@param IncentiveNewOutlet Incentive New Outlet %	  */
	public void setIncentiveNewOutlet (BigDecimal IncentiveNewOutlet)
	{
		set_Value (COLUMNNAME_IncentiveNewOutlet, IncentiveNewOutlet);
	}

	/** Get Incentive New Outlet %.
		@return Incentive New Outlet %	  */
	public BigDecimal getIncentiveNewOutlet () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_IncentiveNewOutlet);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Billing Incentive = BI */
	public static final String INCENTIVETYPE_BillingIncentive = "BI";
	/** Sales Incentive = SI */
	public static final String INCENTIVETYPE_SalesIncentive = "SI";
	/** Set Incentive Type.
		@param IncentiveType 
		Type of incentive (Sales Incentive/Billing Incentive)
	  */
	public void setIncentiveType (String IncentiveType)
	{

		set_Value (COLUMNNAME_IncentiveType, IncentiveType);
	}

	/** Get Incentive Type.
		@return Type of incentive (Sales Incentive/Billing Incentive)
	  */
	public String getIncentiveType () 
	{
		return (String)get_Value(COLUMNNAME_IncentiveType);
	}

	public org.compiere.model.I_M_Product_Category getM_Product_Category() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product_Category)MTable.get(getCtx(), org.compiere.model.I_M_Product_Category.Table_Name)
			.getPO(getM_Product_Category_ID(), get_TrxName());	}

	/** Set Product Category.
		@param M_Product_Category_ID 
		Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID)
	{
		if (M_Product_Category_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_Category_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_Category_ID, Integer.valueOf(M_Product_Category_ID));
	}

	/** Get Product Category.
		@return Category of a Product
	  */
	public int getM_Product_Category_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Incentive.
		@param UNS_Incentive_ID Incentive	  */
	public void setUNS_Incentive_ID (int UNS_Incentive_ID)
	{
		if (UNS_Incentive_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Incentive_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Incentive_ID, Integer.valueOf(UNS_Incentive_ID));
	}

	/** Get Incentive.
		@return Incentive	  */
	public int getUNS_Incentive_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Incentive_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_IncentiveSchema getUNS_IncentiveSchema() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_IncentiveSchema)MTable.get(getCtx(), com.unicore.model.I_UNS_IncentiveSchema.Table_Name)
			.getPO(getUNS_IncentiveSchema_ID(), get_TrxName());	}

	/** Set Incenive Schema.
		@param UNS_IncentiveSchema_ID Incenive Schema	  */
	public void setUNS_IncentiveSchema_ID (int UNS_IncentiveSchema_ID)
	{
		if (UNS_IncentiveSchema_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_IncentiveSchema_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_IncentiveSchema_ID, Integer.valueOf(UNS_IncentiveSchema_ID));
	}

	/** Get Incenive Schema.
		@return Incenive Schema	  */
	public int getUNS_IncentiveSchema_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_IncentiveSchema_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Incentive_UU.
		@param UNS_Incentive_UU UNS_Incentive_UU	  */
	public void setUNS_Incentive_UU (String UNS_Incentive_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Incentive_UU, UNS_Incentive_UU);
	}

	/** Get UNS_Incentive_UU.
		@return UNS_Incentive_UU	  */
	public String getUNS_Incentive_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Incentive_UU);
	}

	/** Set Outlet Grade.
		@param UNS_Outlet_Grade_ID Outlet Grade	  */
	public void setUNS_Outlet_Grade_ID (int UNS_Outlet_Grade_ID)
	{
		if (UNS_Outlet_Grade_ID < 1) 
			set_Value (COLUMNNAME_UNS_Outlet_Grade_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Outlet_Grade_ID, Integer.valueOf(UNS_Outlet_Grade_ID));
	}

	/** Get Outlet Grade.
		@return Outlet Grade	  */
	public int getUNS_Outlet_Grade_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Outlet_Grade_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Outlet Type.
		@param UNS_Outlet_Type_ID Outlet Type	  */
	public void setUNS_Outlet_Type_ID (int UNS_Outlet_Type_ID)
	{
		if (UNS_Outlet_Type_ID < 1) 
			set_Value (COLUMNNAME_UNS_Outlet_Type_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Outlet_Type_ID, Integer.valueOf(UNS_Outlet_Type_ID));
	}

	/** Get Outlet Type.
		@return Outlet Type	  */
	public int getUNS_Outlet_Type_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Outlet_Type_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}