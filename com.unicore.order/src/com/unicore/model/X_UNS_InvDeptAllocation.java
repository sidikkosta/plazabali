/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_InvDeptAllocation
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_InvDeptAllocation extends PO implements I_UNS_InvDeptAllocation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181231L;

    /** Standard Constructor */
    public X_UNS_InvDeptAllocation (Properties ctx, int UNS_InvDeptAllocation_ID, String trxName)
    {
      super (ctx, UNS_InvDeptAllocation_ID, trxName);
      /** if (UNS_InvDeptAllocation_ID == 0)
        {
			setC_InvoiceLine_ID (0);
			setQty (Env.ZERO);
// 0
			setSectionOfDept_ID (0);
			setUNS_InvDeptAllocation_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_InvDeptAllocation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_InvDeptAllocation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getSectionOfDept() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getSectionOfDept_ID(), get_TrxName());	}

	/** Set SectionOfDept_ID.
		@param SectionOfDept_ID SectionOfDept_ID	  */
	public void setSectionOfDept_ID (int SectionOfDept_ID)
	{
		if (SectionOfDept_ID < 1) 
			set_Value (COLUMNNAME_SectionOfDept_ID, null);
		else 
			set_Value (COLUMNNAME_SectionOfDept_ID, Integer.valueOf(SectionOfDept_ID));
	}

	/** Get SectionOfDept_ID.
		@return SectionOfDept_ID	  */
	public int getSectionOfDept_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SectionOfDept_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Invoice Dept Allocation.
		@param UNS_InvDeptAllocation_ID Invoice Dept Allocation	  */
	public void setUNS_InvDeptAllocation_ID (int UNS_InvDeptAllocation_ID)
	{
		if (UNS_InvDeptAllocation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_InvDeptAllocation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_InvDeptAllocation_ID, Integer.valueOf(UNS_InvDeptAllocation_ID));
	}

	/** Get Invoice Dept Allocation.
		@return Invoice Dept Allocation	  */
	public int getUNS_InvDeptAllocation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_InvDeptAllocation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_InvDeptAllocation_UU.
		@param UNS_InvDeptAllocation_UU UNS_InvDeptAllocation_UU	  */
	public void setUNS_InvDeptAllocation_UU (String UNS_InvDeptAllocation_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_InvDeptAllocation_UU, UNS_InvDeptAllocation_UU);
	}

	/** Get UNS_InvDeptAllocation_UU.
		@return UNS_InvDeptAllocation_UU	  */
	public String getUNS_InvDeptAllocation_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_InvDeptAllocation_UU);
	}
}