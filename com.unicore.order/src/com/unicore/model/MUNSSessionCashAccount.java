/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.compiere.model.MConversionRate;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSSessionCashAccount extends X_UNS_SessionCashAccount {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2383495444257487350L;
	private MUNSPOSSession m_parent = null;
	
	public MUNSPOSSession getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPOSSession(getCtx(), getUNS_POS_Session_ID(), get_TrxName());
		return m_parent;
	}

	/**
	 * @param ctx
	 * @param UNS_SessionCashAccount_ID
	 * @param trxName
	 */
	public MUNSSessionCashAccount(Properties ctx,
			int UNS_SessionCashAccount_ID, String trxName) 
	{
		super(ctx, UNS_SessionCashAccount_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSSessionCashAccount(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if(!isReplication())
		{
			if (!newRecord && 
					(is_ValueChanged(COLUMNNAME_ShortCashierAmt) 
					 || is_ValueChanged(COLUMNNAME_EstimationShortCashierAmt)))
			{
				MUNSPOSSession session = getParent();
				if (session.getDocStatus().equals(MUNSPOSSession.DOCSTATUS_Completed)
						|| session.getDocStatus().equals(MUNSPOSSession.DOCSTATUS_Closed))
				{
					log.saveError("Error", "Short cashier cannot be changed. The Session has been completed.");
					return false;
				}
			}
			
			if(!newRecord && getEstimationShortCashierAmt().signum() < 0)
			{
				log.saveError("Error", "Estimation short cashier can't negate.");
				return false;
			}
//			if (newRecord)
//			{
//				setBeginningBalance(getPrevEndBalance());
//			}
			
			if(isRecalcEndingBalance())
				setEndingBalance();
			
			if (newRecord || is_ValueChanged(COLUMNNAME_BeginningBalance))
			{
				BigDecimal b1 = convertToBase1(getBeginningBalance());
				BigDecimal b2 = convertToBase2(b1);
				setBeginningBalanceBase1(b1);
				setBeginningBalanceBase2(b2);
			}
			if (newRecord || is_ValueChanged(COLUMNNAME_CashDepositAmt))
			{
				BigDecimal b1 = convertToBase1(getCashDepositAmt());
				BigDecimal b2 = convertToBase2(b1);
				setCashDepositAmtB1(b1);
				setCashDepositAmtB2(b2);
			}
			if (newRecord || is_ValueChanged(COLUMNNAME_CashIn))
			{
				BigDecimal b1 = convertToBase1(getCashIn());
				BigDecimal b2 = convertToBase2(b1);
				setCashInBase1(b1);
				setCashInBase2(b2);
			}
			if (newRecord || is_ValueChanged(COLUMNNAME_CashOut))
			{
				BigDecimal b1 = convertToBase1(getCashOut());
				BigDecimal b2 = convertToBase2(b1);
				setCashOutBase1(b1);
				setCashOutBase2(b2);
			}
			if (newRecord || is_ValueChanged(COLUMNNAME_EndingBalance))
			{
				BigDecimal b1 = convertToBase1(getEndingBalance());
				BigDecimal b2 = convertToBase2(b1);
				setEndingBalanceBase1(b1);
				setEndingBalanceBase2(b2);
			}
			if(newRecord || is_ValueChanged(COLUMNNAME_ShortCashierAmt))
			{
				BigDecimal b1 = convertToBase1(getShortCashierAmt());
				BigDecimal b2 = convertToBase2(b1);
				setShortCashierAmtBase1(b1);
				setShortCashierAmtBase2(b2);
			}
			if(newRecord || is_ValueChanged(COLUMNNAME_EstimationShortCashierAmt))
			{
				if(getEstimationShortCashierAmt().signum() < 0)
				{
					log.saveError("Error", "Estimation short cashier amount can't be less than zero.");
					return false;
				}
				BigDecimal b1 = convertToBase1(getEstimationShortCashierAmt());
				BigDecimal b2 = convertToBase2(b1);
				setEstimationShortCashierAmtBase1(b1);
				setEstimationShortCashierAmtBase2(b2);
			}
			if (newRecord || is_ValueChanged(COLUMNNAME_PayableRefundAmt))
			{
				BigDecimal b1 = convertToBase1(getPayableRefundAmt());
				BigDecimal b2 = convertToBase2(b1);
				setPayableRefundAmtB1(b1);
				setPayableRefundAmtB2(b2);
			}
		}
		
		return super.beforeSave(newRecord);
	}
	
	private void setEndingBalance ()
	{
		BigDecimal endBal = (getBeginningBalance().add(getCashIn().add(getShortCashierAmt().negate()))).subtract(
				getCashOut().add(getCashDepositAmt().add(getEstimationShortCashierAmt())));
		setEndingBalance(endBal);
	}
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if (!updateHeader())
			return false;
		return super.afterSave (newRecord, success);
	}
	
	private BigDecimal convertToBase1 (BigDecimal amt)
	{
		BigDecimal b1 = MConversionRate.convert(getCtx(), 
				amt, getC_Currency_ID(), 
				getParent().getBase1Currency_ID(), getAD_Client_ID(), 
				getAD_Org_ID());
		return b1;
	}
	
	private BigDecimal convertToBase2 (BigDecimal amt)
	{
		if(amt == null)
			amt = Env.ZERO;
		BigDecimal b2 = MConversionRate.convert(getCtx(), 
				amt, getParent().getBase1Currency_ID(), getParent().getBase2Currency_ID(), 
				getAD_Client_ID(), getAD_Org_ID());
		return b2;
	}
	
	public boolean updateBalance (boolean isReceipt, BigDecimal amt, boolean isSameSession)
	{
		String colName = COLUMNNAME_CashIn;
		if (!isReceipt)
		{
			colName = COLUMNNAME_CashOut;
			if (!isSameSession)
				colName = COLUMNNAME_PayableRefundAmt;
			amt = amt.negate();
		}
		
		BigDecimal inOut = (BigDecimal)get_Value(colName);
		if (inOut == null)
			inOut = Env.ZERO;
		inOut = inOut.add(amt);
		set_ValueOfColumn(colName, inOut);
		return save();
	}
	public static final boolean update (String trxName, int sessionID, int currencyID, String trxType, BigDecimal amt, boolean isSameSession)
	{
		MUNSSessionCashAccount account = get(trxName, sessionID, currencyID);
		if (account == null)
			return false;
		if (!DB.getDatabase().forUpdate(account, 0))
			return false;
		boolean isReceipt = amt.signum() == 1;
		return account.updateBalance(isReceipt, amt,isSameSession);
	}
	
	public static MUNSSessionCashAccount get (String trxName, int sessionID, int currencyID)
	{
		String clause = "UNS_POS_Session_ID = ? AND C_Currency_ID = ?";
		MUNSSessionCashAccount cashAcct = Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				clause, trxName).setParameters(sessionID, currencyID).
				first();
		
		return cashAcct;
	}
	
	public static BigDecimal getBalance (String trxName, int sessionID, int currencyID)
	{
		String sql = "SELECT EndingBalance FROM UNS_SessionCashAccount WHERE UNS_POS_Session_ID = ? AND C_Currency_ID = ?";
		BigDecimal balance = DB.getSQLValueBD(trxName, sql, sessionID, currencyID);
		if (balance == null)
			balance = Env.ZERO;
		return balance;
	}
	
	public MUNSSessionCashAccount (MUNSPOSSession session, MUNSPOSTerminalCashAcct cash)
	{
		this (session.getCtx(), 0, session.get_TrxName());
		setUNS_POS_Session_ID(session.get_ID());
		setClientOrg(session);
		setC_Currency_ID(cash.getC_Currency_ID());
		setIsUsedForChange(cash.isUsedForChange());
		setUNS_POSTerminalCashAcct_ID(cash.get_ID());
		setBeginningBalance(cash.getBeginningBalance());
		setShortCashierAmt(Env.ZERO);
		setShortCashierAmtBase1(Env.ZERO);
		setShortCashierAmtBase2(Env.ZERO);
	}
	
	public BigDecimal getPrevEndBalance ()
	{
		String sql = "SELECT Balance FROM UNS_POSTerminalCashAcct WHERE UNS_POSTerminalCashAcct_ID = ?";
		BigDecimal prev = DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSTerminalCashAcct_ID());
		return prev;
	}
	
	private boolean updateHeader ()
	{
		String sql = "SELECT SUM(CashInBase1-CashOutBase1), SUM(ShortCashierAmtBase1), SUM(EstimationShortCashierAmtBase1), "
				+ " SUM (PayableRefundAmtB1) FROM UNS_SessionCashAccount WHERE UNS_POS_Session_ID = ?";
		List<Object> columns = DB.getSQLValueObjectsEx(get_TrxName(), sql, getUNS_POS_Session_ID());
		BigDecimal base1 = (BigDecimal) columns.get(0);
		if (base1 == null)
			base1 = Env.ZERO;
		BigDecimal shortCashier = (BigDecimal) columns.get(1);
		if(shortCashier == null)
			shortCashier = Env.ZERO;
		BigDecimal estimationShortCashier = (BigDecimal) columns.get(2);
		if(estimationShortCashier == null)
			estimationShortCashier = Env.ZERO;
		BigDecimal payableRefundAmtB1 = (BigDecimal) columns.get(3);
		if (payableRefundAmtB1 == null)
			payableRefundAmtB1 = Env.ZERO;
		
		MUNSPOSSession parent = getParent();
		DB.getDatabase().forUpdate(parent, 0);
		parent.setCashSalesAmt(base1);
		parent.setTotalShortCashierAmt(shortCashier);
		parent.setTotalEstimationShortCashierAmt(estimationShortCashier);
		parent.setPayableCashRefund(payableRefundAmtB1);
		return parent.save();
	}
	
	private boolean m_reCalcEndingBalance = true;
	private boolean isRecalcEndingBalance()
	{
		return m_reCalcEndingBalance;
	}
	public void setRecalcEndingBalance(boolean reCalcEndingBalance)
	{
		m_reCalcEndingBalance = reCalcEndingBalance;
	}
	
}