/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POSConfiguration
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSConfiguration extends PO implements I_UNS_POSConfiguration, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180924L;

    /** Standard Constructor */
    public X_UNS_POSConfiguration (Properties ctx, int UNS_POSConfiguration_ID, String trxName)
    {
      super (ctx, UNS_POSConfiguration_ID, trxName);
      /** if (UNS_POSConfiguration_ID == 0)
        {
			setBase1Currency_ID (0);
			setBase2Currency_ID (0);
			setMinBarcodeLength (0);
// 16
			setName (null);
			setServiceChargeRate (Env.ZERO);
// 0
			setUNS_POSConfiguration_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSConfiguration (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSConfiguration[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getBase1Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getBase1Currency_ID(), get_TrxName());	}

	/** Set Base 1 Currency.
		@param Base1Currency_ID Base 1 Currency	  */
	public void setBase1Currency_ID (int Base1Currency_ID)
	{
		if (Base1Currency_ID < 1) 
			set_Value (COLUMNNAME_Base1Currency_ID, null);
		else 
			set_Value (COLUMNNAME_Base1Currency_ID, Integer.valueOf(Base1Currency_ID));
	}

	/** Get Base 1 Currency.
		@return Base 1 Currency	  */
	public int getBase1Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Base1Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getBase2Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getBase2Currency_ID(), get_TrxName());	}

	/** Set Base 2 Currency.
		@param Base2Currency_ID Base 2 Currency	  */
	public void setBase2Currency_ID (int Base2Currency_ID)
	{
		if (Base2Currency_ID < 1) 
			set_Value (COLUMNNAME_Base2Currency_ID, null);
		else 
			set_Value (COLUMNNAME_Base2Currency_ID, Integer.valueOf(Base2Currency_ID));
	}

	/** Get Base 2 Currency.
		@return Base 2 Currency	  */
	public int getBase2Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Base2Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException
    {
		return (org.compiere.model.I_C_Tax)MTable.get(getCtx(), org.compiere.model.I_C_Tax.Table_Name)
			.getPO(getC_Tax_ID(), get_TrxName());	}

	/** Set Tax.
		@param C_Tax_ID 
		Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID)
	{
		if (C_Tax_ID < 1) 
			set_Value (COLUMNNAME_C_Tax_ID, null);
		else 
			set_Value (COLUMNNAME_C_Tax_ID, Integer.valueOf(C_Tax_ID));
	}

	/** Get Tax.
		@return Tax identifier
	  */
	public int getC_Tax_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Tax_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Min Barcode Length.
		@param MinBarcodeLength Min Barcode Length	  */
	public void setMinBarcodeLength (int MinBarcodeLength)
	{
		set_Value (COLUMNNAME_MinBarcodeLength, Integer.valueOf(MinBarcodeLength));
	}

	/** Get Min Barcode Length.
		@return Min Barcode Length	  */
	public int getMinBarcodeLength () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MinBarcodeLength);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** No Rounding = 00 */
	public static final String PAYMENTROUNDING_NoRounding = "00";
	/** 10 = 01 */
	public static final String PAYMENTROUNDING_10 = "01";
	/** Half Up 50 = 02 */
	public static final String PAYMENTROUNDING_HalfUp50 = "02";
	/** Half Down 50 = 03 */
	public static final String PAYMENTROUNDING_HalfDown50 = "03";
	/** 100 = 04 */
	public static final String PAYMENTROUNDING_100 = "04";
	/** 1000 = 05 */
	public static final String PAYMENTROUNDING_1000 = "05";
	/** Set Payment Rounding.
		@param PaymentRounding Payment Rounding	  */
	public void setPaymentRounding (String PaymentRounding)
	{

		set_Value (COLUMNNAME_PaymentRounding, PaymentRounding);
	}

	/** Get Payment Rounding.
		@return Payment Rounding	  */
	public String getPaymentRounding () 
	{
		return (String)get_Value(COLUMNNAME_PaymentRounding);
	}

	/** Set Service Charge Rate.
		@param ServiceChargeRate Service Charge Rate	  */
	public void setServiceChargeRate (BigDecimal ServiceChargeRate)
	{
		set_Value (COLUMNNAME_ServiceChargeRate, ServiceChargeRate);
	}

	/** Get Service Charge Rate.
		@return Service Charge Rate	  */
	public BigDecimal getServiceChargeRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ServiceChargeRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Global POS Configuration.
		@param UNS_POSConfiguration_ID Global POS Configuration	  */
	public void setUNS_POSConfiguration_ID (int UNS_POSConfiguration_ID)
	{
		if (UNS_POSConfiguration_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_ID, Integer.valueOf(UNS_POSConfiguration_ID));
	}

	/** Get Global POS Configuration.
		@return Global POS Configuration	  */
	public int getUNS_POSConfiguration_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSConfiguration_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSConfiguration_UU.
		@param UNS_POSConfiguration_UU UNS_POSConfiguration_UU	  */
	public void setUNS_POSConfiguration_UU (String UNS_POSConfiguration_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSConfiguration_UU, UNS_POSConfiguration_UU);
	}

	/** Get UNS_POSConfiguration_UU.
		@return UNS_POSConfiguration_UU	  */
	public String getUNS_POSConfiguration_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSConfiguration_UU);
	}
}