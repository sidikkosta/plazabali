/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POSRecapLine_MA
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSRecapLine_MA extends PO implements I_UNS_POSRecapLine_MA, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180913L;

    /** Standard Constructor */
    public X_UNS_POSRecapLine_MA (Properties ctx, int UNS_POSRecapLine_MA_ID, String trxName)
    {
      super (ctx, UNS_POSRecapLine_MA_ID, trxName);
      /** if (UNS_POSRecapLine_MA_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_POSRecapLine_MA (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSRecapLine_MA[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date  Material Policy.
		@param DateMaterialPolicy 
		Time used for LIFO and FIFO Material Policy
	  */
	public void setDateMaterialPolicy (Timestamp DateMaterialPolicy)
	{
		set_Value (COLUMNNAME_DateMaterialPolicy, DateMaterialPolicy);
	}

	/** Get Date  Material Policy.
		@return Time used for LIFO and FIFO Material Policy
	  */
	public Timestamp getDateMaterialPolicy () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateMaterialPolicy);
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_Value (COLUMNNAME_M_Locator_ID, null);
		else 
			set_Value (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_POSRecapLine getUNS_POSRecapLine() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSRecapLine)MTable.get(getCtx(), com.unicore.model.I_UNS_POSRecapLine.Table_Name)
			.getPO(getUNS_POSRecapLine_ID(), get_TrxName());	}

	/** Set POS Recapitulation Line.
		@param UNS_POSRecapLine_ID POS Recapitulation Line	  */
	public void setUNS_POSRecapLine_ID (int UNS_POSRecapLine_ID)
	{
		if (UNS_POSRecapLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecapLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecapLine_ID, Integer.valueOf(UNS_POSRecapLine_ID));
	}

	/** Get POS Recapitulation Line.
		@return POS Recapitulation Line	  */
	public int getUNS_POSRecapLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSRecapLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSRecapLine_MA_UU.
		@param UNS_POSRecapLine_MA_UU UNS_POSRecapLine_MA_UU	  */
	public void setUNS_POSRecapLine_MA_UU (String UNS_POSRecapLine_MA_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSRecapLine_MA_UU, UNS_POSRecapLine_MA_UU);
	}

	/** Get UNS_POSRecapLine_MA_UU.
		@return UNS_POSRecapLine_MA_UU	  */
	public String getUNS_POSRecapLine_MA_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSRecapLine_MA_UU);
	}
}