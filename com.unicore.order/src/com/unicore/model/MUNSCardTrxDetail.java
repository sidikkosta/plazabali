/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.edc.ISourceEDCModel;

/**
 * @author nurse
 *
 */
public class MUNSCardTrxDetail extends X_UNS_CardTrxDetail implements ISourceEDCModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3421747073619527066L;
	private MUNSPaymentTrx m_parent = null;
	
	public MUNSPaymentTrx getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPaymentTrx(getCtx(), getUNS_PaymentTrx_ID(), get_TrxName());
		return m_parent;
	}

	public static MUNSCardTrxDetail get (MUNSPaymentTrx  payTrx)
	{
		String clause = "UNS_PaymentTrx_ID = ?";
		MUNSCardTrxDetail retVal = Query.get(
				payTrx.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				clause, payTrx.get_TrxName()).setParameters(payTrx.get_ID()).
				setOnlyActiveRecords(true).firstOnly();
		if (retVal != null)
			retVal.m_parent = payTrx;
		
		return retVal;
	}
	/**
	 * @param ctx
	 * @param UNS_CardTrxDetail_ID
	 * @param trxName
	 */
	public MUNSCardTrxDetail(Properties ctx, int UNS_CardTrxDetail_ID,
			String trxName) 
	{
		super(ctx, UNS_CardTrxDetail_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCardTrxDetail(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	public String validateRecord ()
	{

		if (MUNSPaymentTrx.PAYMENTMETHOD_Card.equals(getParent().getPaymentMethod()))
		{
			if (getBatchNo() == null)
				return "Mandatory field Batch No";
		}
		
		return null;
	}

	@Override
	public void setTransactionAmount(double transactionAmt) 
	{
		getParent().setTrxAmt(new BigDecimal(transactionAmt));
	}

	@Override
	public double getTransactionAmount() 
	{
		return getParent().getAmount().doubleValue();
	}

	@Override
	public void setOtherAmount(double otherAmount) 
	{
		//do nothing
	}

	@Override
	public double getOtherAmount() 
	{
		return 0;
	}

	@Override
	public String getOutlateInvNo() 
	{
		return getParent().getParent().getDocumentNo();
	}


	@Override
	public String getISOCurrency() 
	{
		String sql = "SELECT Iso_Code FROM C_Currency WHERE C_Currency_ID = ?";
		String isoCode = DB.getSQLValueString(get_TrxName(), sql, getC_Currency_ID());
		return isoCode;
	}


	@Override
	public int getC_Currency_ID() 
	{
		return getParent().getC_Currency_ID();
	}
	
	@Override
	public void setCardType (String cardType)
	{
		int ct = MUNSCardType.getIDByNameOrValue(get_TrxName(), cardType);
		setUNS_CardType_ID(ct);
	}
	
	public MUNSCardTrxDetail (MUNSPaymentTrx parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_PaymentTrx_ID(parent.get_ID());
		m_parent = parent;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{		
		if (MUNSPaymentTrx.PAYMENTMETHOD_Wechat.equals(getParent().getPaymentMethod()))
		{
			if (getUNS_CardType_ID() == 0)
			{
				String sql = "SELECT UNS_CardType_ID FROM UNS_CardType WHERE Value = ? OR Name Like ?";
				int ctID = DB.getSQLValue(get_TrxName(), sql, "WC", "%Wechat%");
				if (ctID > 0)
					setUNS_CardType_ID(ctID);
			}
		}
		if (getUNS_CardType_ID() == 0)
		{
			String sql = "SELECT UNS_CardType_ID FROM UNS_CardType WHERE Value = ? OR Name Like ?";
			int ctID = DB.getSQLValue(get_TrxName(), sql, "08", "%Other%");
			if (ctID > 0)
				setUNS_CardType_ID(ctID);
		}
		if (getParent().isManual())
		{
			if (Util.isEmpty(getInvoiceNo(), true))
				setInvoiceNo(getBatchNo());
			if (Util.isEmpty(getTraceNo(), true))
				setTraceNo(getBatchNo());
		}
		
		if (getUNS_CardType_ID() == 0)
		{
			ValueNamePair msg = new ValueNamePair("MandatoryCardType", "Mandatory Column Card Type");
			Env.getCtx().put("org.compiere.util.CLogger.lastError", msg);
			return false;
		}
		if (Util.isEmpty(getBatchNo(), true))
		{
			ValueNamePair msg = new ValueNamePair("MandatoryBatchNo", "Mandatory Column Batch No");
			Env.getCtx().put("org.compiere.util.CLogger.lastError", msg);
			return false;
		}
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	public void setAccountNo (String accountNo)
	{
		getParent().setTrxNo(accountNo);
	}
	
	public String getAccountNo ()
	{
		return getParent().getTrxNo();
	}
}
