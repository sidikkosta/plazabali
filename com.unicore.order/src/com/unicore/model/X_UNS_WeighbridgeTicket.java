/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_WeighbridgeTicket
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_WeighbridgeTicket extends PO implements I_UNS_WeighbridgeTicket, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171006L;

    /** Standard Constructor */
    public X_UNS_WeighbridgeTicket (Properties ctx, int UNS_WeighbridgeTicket_ID, String trxName)
    {
      super (ctx, UNS_WeighbridgeTicket_ID, trxName);
      /** if (UNS_WeighbridgeTicket_ID == 0)
        {
			setAD_User_ID (0);
// @#AD_User_ID@
			setC_BPartner_ID (0);
			setC_DocType_ID (0);
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setDocumentNo (null);
			setIsApproved (false);
// N
			setIsDumpTruck (false);
// N
			setIsOriginalTicket (true);
// Y
			setIsSOTrx (false);
// N
			setIsSplitOrder (false);
// N
			setNettoI (Env.ZERO);
			setNettoII (Env.ZERO);
			setProcessed (false);
// N
			setQtyPayment (Env.ZERO);
// 0
			setRevisionPercent (Env.ZERO);
// 0
			setRevisionQty (Env.ZERO);
// 0
			setTare (Env.ZERO);
			setTimeIn (new Timestamp( System.currentTimeMillis() ));
// @SQL=SELECT NOW()
			setTransporterName (null);
			setUNS_ArmadaType_ID (0);
// 1000001
			setUNS_WeighbridgeTicket_ID (0);
			setVehicleNo (null);
        } */
    }

    /** Load Constructor */
    public X_UNS_WeighbridgeTicket (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_WeighbridgeTicket[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_Value (COLUMNNAME_AD_User_ID, null);
		else 
			set_Value (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cancel Process.
		@param CancelProcess Cancel Process	  */
	public void setCancelProcess (String CancelProcess)
	{
		set_Value (COLUMNNAME_CancelProcess, CancelProcess);
	}

	/** Get Cancel Process.
		@return Cancel Process	  */
	public String getCancelProcess () 
	{
		return (String)get_Value(COLUMNNAME_CancelProcess);
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner_Location)MTable.get(getCtx(), org.compiere.model.I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getC_DocType_ID()));
    }

	/** Set Change Order.
		@param ChangeOrder Change Order	  */
	public void setChangeOrder (String ChangeOrder)
	{
		set_Value (COLUMNNAME_ChangeOrder, ChangeOrder);
	}

	/** Get Change Order.
		@return Change Order	  */
	public String getChangeOrder () 
	{
		return (String)get_Value(COLUMNNAME_ChangeOrder);
	}

	/** Set Change Ticket Contract.
		@param ChangeTicketContract Change Ticket Contract	  */
	public void setChangeTicketContract (String ChangeTicketContract)
	{
		set_Value (COLUMNNAME_ChangeTicketContract, ChangeTicketContract);
	}

	/** Get Change Ticket Contract.
		@return Change Ticket Contract	  */
	public String getChangeTicketContract () 
	{
		return (String)get_Value(COLUMNNAME_ChangeTicketContract);
	}

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (org.compiere.model.I_C_Invoice)MTable.get(getCtx(), org.compiere.model.I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_Value (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_Value (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getC_OrderLine_ID(), get_TrxName());	}

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create Intransit Iventory Move.
		@param CreateIntransitMovement Create Intransit Iventory Move	  */
	public void setCreateIntransitMovement (String CreateIntransitMovement)
	{
		set_Value (COLUMNNAME_CreateIntransitMovement, CreateIntransitMovement);
	}

	/** Get Create Intransit Iventory Move.
		@return Create Intransit Iventory Move	  */
	public String getCreateIntransitMovement () 
	{
		return (String)get_Value(COLUMNNAME_CreateIntransitMovement);
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Driver's License.
		@param DriverLicense 
		Driver's License Number
	  */
	public void setDriverLicense (String DriverLicense)
	{
		set_Value (COLUMNNAME_DriverLicense, DriverLicense);
	}

	/** Get Driver's License.
		@return Driver's License Number
	  */
	public String getDriverLicense () 
	{
		return (String)get_Value(COLUMNNAME_DriverLicense);
	}

	/** Set Gross Weight.
		@param GrossWeight 
		Weight of a product (Gross)
	  */
	public void setGrossWeight (BigDecimal GrossWeight)
	{
		set_Value (COLUMNNAME_GrossWeight, GrossWeight);
	}

	/** Get Gross Weight.
		@return Weight of a product (Gross)
	  */
	public BigDecimal getGrossWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrossWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_ValueNoCheck (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Cancelled.
		@param IsCancelled 
		The transaction was cancelled
	  */
	public void setIsCancelled (boolean IsCancelled)
	{
		set_Value (COLUMNNAME_IsCancelled, Boolean.valueOf(IsCancelled));
	}

	/** Get Cancelled.
		@return The transaction was cancelled
	  */
	public boolean isCancelled () 
	{
		Object oo = get_Value(COLUMNNAME_IsCancelled);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Dump Truck.
		@param IsDumpTruck Is Dump Truck	  */
	public void setIsDumpTruck (boolean IsDumpTruck)
	{
		set_Value (COLUMNNAME_IsDumpTruck, Boolean.valueOf(IsDumpTruck));
	}

	/** Get Is Dump Truck.
		@return Is Dump Truck	  */
	public boolean isDumpTruck () 
	{
		Object oo = get_Value(COLUMNNAME_IsDumpTruck);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Original Ticket.
		@param IsOriginalTicket 
		This is to indicate if this ticket is the ticket record being splitted into another order.
	  */
	public void setIsOriginalTicket (boolean IsOriginalTicket)
	{
		set_Value (COLUMNNAME_IsOriginalTicket, Boolean.valueOf(IsOriginalTicket));
	}

	/** Get Is Original Ticket.
		@return This is to indicate if this ticket is the ticket record being splitted into another order.
	  */
	public boolean isOriginalTicket () 
	{
		Object oo = get_Value(COLUMNNAME_IsOriginalTicket);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Sales Transaction.
		@param IsSOTrx 
		This is a Sales Transaction
	  */
	public void setIsSOTrx (boolean IsSOTrx)
	{
		set_ValueNoCheck (COLUMNNAME_IsSOTrx, Boolean.valueOf(IsSOTrx));
	}

	/** Get Sales Transaction.
		@return This is a Sales Transaction
	  */
	public boolean isSOTrx () 
	{
		Object oo = get_Value(COLUMNNAME_IsSOTrx);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Split Order.
		@param IsSplitOrder 
		Is this document being split into two different orders?
	  */
	public void setIsSplitOrder (boolean IsSplitOrder)
	{
		set_Value (COLUMNNAME_IsSplitOrder, Boolean.valueOf(IsSplitOrder));
	}

	/** Get Is Split Order.
		@return Is this document being split into two different orders?
	  */
	public boolean isSplitOrder () 
	{
		Object oo = get_Value(COLUMNNAME_IsSplitOrder);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_InOut getM_InOut() throws RuntimeException
    {
		return (org.compiere.model.I_M_InOut)MTable.get(getCtx(), org.compiere.model.I_M_InOut.Table_Name)
			.getPO(getM_InOut_ID(), get_TrxName());	}

	/** Set Shipment/Receipt.
		@param M_InOut_ID 
		Material Shipment Document
	  */
	public void setM_InOut_ID (int M_InOut_ID)
	{
		if (M_InOut_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_InOut_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_InOut_ID, Integer.valueOf(M_InOut_ID));
	}

	/** Get Shipment/Receipt.
		@return Material Shipment Document
	  */
	public int getM_InOut_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InOut_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Locator getM_Locator() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_Value (COLUMNNAME_M_Locator_ID, null);
		else 
			set_Value (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_MovementLineConfirm getM_MovementLineConfirm() throws RuntimeException
    {
		return (org.compiere.model.I_M_MovementLineConfirm)MTable.get(getCtx(), org.compiere.model.I_M_MovementLineConfirm.Table_Name)
			.getPO(getM_MovementLineConfirm_ID(), get_TrxName());	}

	/** Set Move Line Confirm.
		@param M_MovementLineConfirm_ID 
		Inventory Move Line Confirmation
	  */
	public void setM_MovementLineConfirm_ID (int M_MovementLineConfirm_ID)
	{
		if (M_MovementLineConfirm_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_MovementLineConfirm_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_MovementLineConfirm_ID, Integer.valueOf(M_MovementLineConfirm_ID));
	}

	/** Get Move Line Confirm.
		@return Inventory Move Line Confirmation
	  */
	public int getM_MovementLineConfirm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_MovementLineConfirm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_MovementLine getM_MovementLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_MovementLine)MTable.get(getCtx(), org.compiere.model.I_M_MovementLine.Table_Name)
			.getPO(getM_MovementLine_ID(), get_TrxName());	}

	/** Set Move Line.
		@param M_MovementLine_ID 
		Inventory Move document Line
	  */
	public void setM_MovementLine_ID (int M_MovementLine_ID)
	{
		if (M_MovementLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_MovementLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_MovementLine_ID, Integer.valueOf(M_MovementLine_ID));
	}

	/** Get Move Line.
		@return Inventory Move document Line
	  */
	public int getM_MovementLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_MovementLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Netto-I.
		@param NettoI 
		The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI)
	{
		set_Value (COLUMNNAME_NettoI, NettoI);
	}

	/** Get Netto-I.
		@return The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Netto-II.
		@param NettoII 
		The nett weight of the second group calculation
	  */
	public void setNettoII (BigDecimal NettoII)
	{
		set_Value (COLUMNNAME_NettoII, NettoII);
	}

	/** Get Netto-II.
		@return The nett weight of the second group calculation
	  */
	public BigDecimal getNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Netto-I.
		@param OriginalNettoI 
		The nett weight of the first group calculation
	  */
	public void setOriginalNettoI (BigDecimal OriginalNettoI)
	{
		set_ValueNoCheck (COLUMNNAME_OriginalNettoI, OriginalNettoI);
	}

	/** Get Original Netto-I.
		@return The nett weight of the first group calculation
	  */
	public BigDecimal getOriginalNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalNettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Netto-II.
		@param OriginalNettoII 
		The nett weight of the second group calculation
	  */
	public void setOriginalNettoII (BigDecimal OriginalNettoII)
	{
		set_ValueNoCheck (COLUMNNAME_OriginalNettoII, OriginalNettoII);
	}

	/** Get Original Netto-II.
		@return The nett weight of the second group calculation
	  */
	public BigDecimal getOriginalNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalNettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Reflection.
		@param OriginalReflection 
		The deduction to the gross weight
	  */
	public void setOriginalReflection (BigDecimal OriginalReflection)
	{
		set_ValueNoCheck (COLUMNNAME_OriginalReflection, OriginalReflection);
	}

	/** Get Original Reflection.
		@return The deduction to the gross weight
	  */
	public BigDecimal getOriginalReflection () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalReflection);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Print Grading Sheet.
		@param PrintGradingSheet 
		Print the Grading Sheet of a ticket.
	  */
	public void setPrintGradingSheet (String PrintGradingSheet)
	{
		set_Value (COLUMNNAME_PrintGradingSheet, PrintGradingSheet);
	}

	/** Get Print Grading Sheet.
		@return Print the Grading Sheet of a ticket.
	  */
	public String getPrintGradingSheet () 
	{
		return (String)get_Value(COLUMNNAME_PrintGradingSheet);
	}

	/** Set Print Ticket And Despatch.
		@param PrintJoinTicketAndDespatch Print Ticket And Despatch	  */
	public void setPrintJoinTicketAndDespatch (String PrintJoinTicketAndDespatch)
	{
		set_Value (COLUMNNAME_PrintJoinTicketAndDespatch, PrintJoinTicketAndDespatch);
	}

	/** Get Print Ticket And Despatch.
		@return Print Ticket And Despatch	  */
	public String getPrintJoinTicketAndDespatch () 
	{
		return (String)get_Value(COLUMNNAME_PrintJoinTicketAndDespatch);
	}

	/** Set Print Ticket And Grading Sheet.
		@param PrintJoinTicketAndGrading Print Ticket And Grading Sheet	  */
	public void setPrintJoinTicketAndGrading (String PrintJoinTicketAndGrading)
	{
		set_Value (COLUMNNAME_PrintJoinTicketAndGrading, PrintJoinTicketAndGrading);
	}

	/** Get Print Ticket And Grading Sheet.
		@return Print Ticket And Grading Sheet	  */
	public String getPrintJoinTicketAndGrading () 
	{
		return (String)get_Value(COLUMNNAME_PrintJoinTicketAndGrading);
	}

	/** Set Print Ticket.
		@param PrintTicket 
		Print weighbridge ticket
	  */
	public void setPrintTicket (String PrintTicket)
	{
		set_Value (COLUMNNAME_PrintTicket, PrintTicket);
	}

	/** Get Print Ticket.
		@return Print weighbridge ticket
	  */
	public String getPrintTicket () 
	{
		return (String)get_Value(COLUMNNAME_PrintTicket);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Qty Payment.
		@param QtyPayment Qty Payment	  */
	public void setQtyPayment (BigDecimal QtyPayment)
	{
		set_Value (COLUMNNAME_QtyPayment, QtyPayment);
	}

	/** Get Qty Payment.
		@return Qty Payment	  */
	public BigDecimal getQtyPayment () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyPayment);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reference No.
		@param ReferenceNo 
		Your customer or vendor number at the Business Partner's site
	  */
	public void setReferenceNo (String ReferenceNo)
	{
		set_Value (COLUMNNAME_ReferenceNo, ReferenceNo);
	}

	/** Get Reference No.
		@return Your customer or vendor number at the Business Partner's site
	  */
	public String getReferenceNo () 
	{
		return (String)get_Value(COLUMNNAME_ReferenceNo);
	}

	/** Set Reflection.
		@param Reflection 
		The deduction to the gross weight
	  */
	public void setReflection (BigDecimal Reflection)
	{
		set_Value (COLUMNNAME_Reflection, Reflection);
	}

	/** Get Reflection.
		@return The deduction to the gross weight
	  */
	public BigDecimal getReflection () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Reflection);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Print Despatch Note.
		@param ReportDespatch Print Despatch Note	  */
	public void setReportDespatch (String ReportDespatch)
	{
		set_Value (COLUMNNAME_ReportDespatch, ReportDespatch);
	}

	/** Get Print Despatch Note.
		@return Print Despatch Note	  */
	public String getReportDespatch () 
	{
		return (String)get_Value(COLUMNNAME_ReportDespatch);
	}

	/** Set Revision %.
		@param RevisionPercent Revision %	  */
	public void setRevisionPercent (BigDecimal RevisionPercent)
	{
		set_Value (COLUMNNAME_RevisionPercent, RevisionPercent);
	}

	/** Get Revision %.
		@return Revision %	  */
	public BigDecimal getRevisionPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RevisionPercent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Revision Qty.
		@param RevisionQty Revision Qty	  */
	public void setRevisionQty (BigDecimal RevisionQty)
	{
		set_Value (COLUMNNAME_RevisionQty, RevisionQty);
	}

	/** Get Revision Qty.
		@return Revision Qty	  */
	public BigDecimal getRevisionQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RevisionQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Air = Air */
	public static final String SHIPPEDBY_Air = "Air";
	/** Darat = DRT */
	public static final String SHIPPEDBY_Darat = "DRT";
	/** Set Shipped By.
		@param ShippedBy Shipped By	  */
	public void setShippedBy (String ShippedBy)
	{

		set_Value (COLUMNNAME_ShippedBy, ShippedBy);
	}

	/** Get Shipped By.
		@return Shipped By	  */
	public String getShippedBy () 
	{
		return (String)get_Value(COLUMNNAME_ShippedBy);
	}

	public org.compiere.model.I_C_Order getSplittedOrder() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getSplittedOrder_ID(), get_TrxName());	}

	/** Set Splitted Order.
		@param SplittedOrder_ID 
		The other order (ticket) by which this ticket is splitted to
	  */
	public void setSplittedOrder_ID (int SplittedOrder_ID)
	{
		if (SplittedOrder_ID < 1) 
			set_Value (COLUMNNAME_SplittedOrder_ID, null);
		else 
			set_Value (COLUMNNAME_SplittedOrder_ID, Integer.valueOf(SplittedOrder_ID));
	}

	/** Get Splitted Order.
		@return The other order (ticket) by which this ticket is splitted to
	  */
	public int getSplittedOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SplittedOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getSplittedTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getSplittedTicket_ID(), get_TrxName());	}

	/** Set Splitted Ticket.
		@param SplittedTicket_ID 
		The ticket by which this ticket is splitted to
	  */
	public void setSplittedTicket_ID (int SplittedTicket_ID)
	{
		if (SplittedTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_SplittedTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_SplittedTicket_ID, Integer.valueOf(SplittedTicket_ID));
	}

	/** Get Splitted Ticket.
		@return The ticket by which this ticket is splitted to
	  */
	public int getSplittedTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SplittedTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Sync To Destination Warehouse Confirm.
		@param SyncToDestinationWhsConfirm Sync To Destination Warehouse Confirm	  */
	public void setSyncToDestinationWhsConfirm (String SyncToDestinationWhsConfirm)
	{
		set_Value (COLUMNNAME_SyncToDestinationWhsConfirm, SyncToDestinationWhsConfirm);
	}

	/** Get Sync To Destination Warehouse Confirm.
		@return Sync To Destination Warehouse Confirm	  */
	public String getSyncToDestinationWhsConfirm () 
	{
		return (String)get_Value(COLUMNNAME_SyncToDestinationWhsConfirm);
	}

	/** Set Tare.
		@param Tare 
		The weight as comparison
	  */
	public void setTare (BigDecimal Tare)
	{
		set_Value (COLUMNNAME_Tare, Tare);
	}

	/** Get Tare.
		@return The weight as comparison
	  */
	public BigDecimal getTare () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Tare);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Time In.
		@param TimeIn 
		The date and time of when it was in
	  */
	public void setTimeIn (Timestamp TimeIn)
	{
		set_Value (COLUMNNAME_TimeIn, TimeIn);
	}

	/** Get Time In.
		@return The date and time of when it was in
	  */
	public Timestamp getTimeIn () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TimeIn);
	}

	/** Set Time In Number.
		@param TimeInNumber 
		Must 4 Number for parsing to date Time In
	  */
	public void setTimeInNumber (String TimeInNumber)
	{
		set_Value (COLUMNNAME_TimeInNumber, TimeInNumber);
	}

	/** Get Time In Number.
		@return Must 4 Number for parsing to date Time In
	  */
	public String getTimeInNumber () 
	{
		return (String)get_Value(COLUMNNAME_TimeInNumber);
	}

	/** Set Time Out.
		@param TimeOut 
		The date and time of when it was out
	  */
	public void setTimeOut (Timestamp TimeOut)
	{
		set_Value (COLUMNNAME_TimeOut, TimeOut);
	}

	/** Get Time Out.
		@return The date and time of when it was out
	  */
	public Timestamp getTimeOut () 
	{
		return (Timestamp)get_Value(COLUMNNAME_TimeOut);
	}

	/** Set Time Out Number.
		@param TimeOutNumber 
		Must 4 Number for parsing to date Time Out
	  */
	public void setTimeOutNumber (String TimeOutNumber)
	{
		set_Value (COLUMNNAME_TimeOutNumber, TimeOutNumber);
	}

	/** Get Time Out Number.
		@return Must 4 Number for parsing to date Time Out
	  */
	public String getTimeOutNumber () 
	{
		return (String)get_Value(COLUMNNAME_TimeOutNumber);
	}

	public org.compiere.model.I_C_BPartner getTransporter() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getTransporter_ID(), get_TrxName());	}

	/** Set Transporter.
		@param Transporter_ID 
		The transporter
	  */
	public void setTransporter_ID (int Transporter_ID)
	{
		if (Transporter_ID < 1) 
			set_Value (COLUMNNAME_Transporter_ID, null);
		else 
			set_Value (COLUMNNAME_Transporter_ID, Integer.valueOf(Transporter_ID));
	}

	/** Get Transporter.
		@return The transporter
	  */
	public int getTransporter_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Transporter_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Transporter Name.
		@param TransporterName 
		The name of transporter
	  */
	public void setTransporterName (String TransporterName)
	{
		set_Value (COLUMNNAME_TransporterName, TransporterName);
	}

	/** Get Transporter Name.
		@return The name of transporter
	  */
	public String getTransporterName () 
	{
		return (String)get_Value(COLUMNNAME_TransporterName);
	}

	public org.compiere.model.I_C_InvoiceLine getUnLoadInvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getUnLoadInvoiceLine_ID(), get_TrxName());	}

	/** Set UnLoading Invoice Line.
		@param UnLoadInvoiceLine_ID UnLoading Invoice Line	  */
	public void setUnLoadInvoiceLine_ID (int UnLoadInvoiceLine_ID)
	{
		if (UnLoadInvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_UnLoadInvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_UnLoadInvoiceLine_ID, Integer.valueOf(UnLoadInvoiceLine_ID));
	}

	/** Get UnLoading Invoice Line.
		@return UnLoading Invoice Line	  */
	public int getUnLoadInvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UnLoadInvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Armada Type.
		@param UNS_ArmadaType_ID Armada Type	  */
	public void setUNS_ArmadaType_ID (int UNS_ArmadaType_ID)
	{
		if (UNS_ArmadaType_ID < 1) 
			set_Value (COLUMNNAME_UNS_ArmadaType_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_ArmadaType_ID, Integer.valueOf(UNS_ArmadaType_ID));
	}

	/** Get Armada Type.
		@return Armada Type	  */
	public int getUNS_ArmadaType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ArmadaType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_BPartner_Driver getUNS_BPartner_Driver() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_BPartner_Driver)MTable.get(getCtx(), com.unicore.model.I_UNS_BPartner_Driver.Table_Name)
			.getPO(getUNS_BPartner_Driver_ID(), get_TrxName());	}

	/** Set UNS_BPartner_Driver.
		@param UNS_BPartner_Driver_ID UNS_BPartner_Driver	  */
	public void setUNS_BPartner_Driver_ID (int UNS_BPartner_Driver_ID)
	{
		if (UNS_BPartner_Driver_ID < 1) 
			set_Value (COLUMNNAME_UNS_BPartner_Driver_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_BPartner_Driver_ID, Integer.valueOf(UNS_BPartner_Driver_ID));
	}

	/** Get UNS_BPartner_Driver.
		@return UNS_BPartner_Driver	  */
	public int getUNS_BPartner_Driver_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BPartner_Driver_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_Handover_Ticket getUNS_Handover_Ticket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_Handover_Ticket)MTable.get(getCtx(), com.unicore.model.I_UNS_Handover_Ticket.Table_Name)
			.getPO(getUNS_Handover_Ticket_ID(), get_TrxName());	}

	/** Set Handover Ticket.
		@param UNS_Handover_Ticket_ID Handover Ticket	  */
	public void setUNS_Handover_Ticket_ID (int UNS_Handover_Ticket_ID)
	{
		if (UNS_Handover_Ticket_ID < 1) 
			set_Value (COLUMNNAME_UNS_Handover_Ticket_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Handover_Ticket_ID, Integer.valueOf(UNS_Handover_Ticket_ID));
	}

	/** Get Handover Ticket.
		@return Handover Ticket	  */
	public int getUNS_Handover_Ticket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Handover_Ticket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_ReflectionRevision getUNS_ReflectionRevision() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_ReflectionRevision)MTable.get(getCtx(), com.unicore.model.I_UNS_ReflectionRevision.Table_Name)
			.getPO(getUNS_ReflectionRevision_ID(), get_TrxName());	}

	/** Set Reflection Revision.
		@param UNS_ReflectionRevision_ID Reflection Revision	  */
	public void setUNS_ReflectionRevision_ID (int UNS_ReflectionRevision_ID)
	{
		if (UNS_ReflectionRevision_ID < 1) 
			set_Value (COLUMNNAME_UNS_ReflectionRevision_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_ReflectionRevision_ID, Integer.valueOf(UNS_ReflectionRevision_ID));
	}

	/** Get Reflection Revision.
		@return Reflection Revision	  */
	public int getUNS_ReflectionRevision_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ReflectionRevision_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_ReflectionRevision_Line getUNS_ReflectionRevision_Line() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_ReflectionRevision_Line)MTable.get(getCtx(), com.unicore.model.I_UNS_ReflectionRevision_Line.Table_Name)
			.getPO(getUNS_ReflectionRevision_Line_ID(), get_TrxName());	}

	/** Set Revision Line.
		@param UNS_ReflectionRevision_Line_ID Revision Line	  */
	public void setUNS_ReflectionRevision_Line_ID (int UNS_ReflectionRevision_Line_ID)
	{
		if (UNS_ReflectionRevision_Line_ID < 1) 
			set_Value (COLUMNNAME_UNS_ReflectionRevision_Line_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_ReflectionRevision_Line_ID, Integer.valueOf(UNS_ReflectionRevision_Line_ID));
	}

	/** Get Revision Line.
		@return Revision Line	  */
	public int getUNS_ReflectionRevision_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ReflectionRevision_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_UnLoadingCost getUNS_UnLoadingCost() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_UnLoadingCost)MTable.get(getCtx(), com.unicore.model.I_UNS_UnLoadingCost.Table_Name)
			.getPO(getUNS_UnLoadingCost_ID(), get_TrxName());	}

	/** Set Unloading Cost.
		@param UNS_UnLoadingCost_ID Unloading Cost	  */
	public void setUNS_UnLoadingCost_ID (int UNS_UnLoadingCost_ID)
	{
		if (UNS_UnLoadingCost_ID < 1) 
			set_Value (COLUMNNAME_UNS_UnLoadingCost_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_UnLoadingCost_ID, Integer.valueOf(UNS_UnLoadingCost_ID));
	}

	/** Get Unloading Cost.
		@return Unloading Cost	  */
	public int getUNS_UnLoadingCost_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingCost_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_UnLoadingCost_Line getUNS_UnLoadingCost_Line() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_UnLoadingCost_Line)MTable.get(getCtx(), com.unicore.model.I_UNS_UnLoadingCost_Line.Table_Name)
			.getPO(getUNS_UnLoadingCost_Line_ID(), get_TrxName());	}

	/** Set UnLoading Cost Line.
		@param UNS_UnLoadingCost_Line_ID UnLoading Cost Line	  */
	public void setUNS_UnLoadingCost_Line_ID (int UNS_UnLoadingCost_Line_ID)
	{
		if (UNS_UnLoadingCost_Line_ID < 1) 
			set_Value (COLUMNNAME_UNS_UnLoadingCost_Line_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_UnLoadingCost_Line_ID, Integer.valueOf(UNS_UnLoadingCost_Line_ID));
	}

	/** Get UnLoading Cost Line.
		@return UnLoading Cost Line	  */
	public int getUNS_UnLoadingCost_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingCost_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_WeighbridgeTicket_UU.
		@param UNS_WeighbridgeTicket_UU UNS_WeighbridgeTicket_UU	  */
	public void setUNS_WeighbridgeTicket_UU (String UNS_WeighbridgeTicket_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_UU, UNS_WeighbridgeTicket_UU);
	}

	/** Get UNS_WeighbridgeTicket_UU.
		@return UNS_WeighbridgeTicket_UU	  */
	public String getUNS_WeighbridgeTicket_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_UU);
	}

	/** Set Vehicle Driver Name.
		@param VehicleDriverName 
		The person name of vehicle by whom it is drived
	  */
	public void setVehicleDriverName (String VehicleDriverName)
	{
		set_Value (COLUMNNAME_VehicleDriverName, VehicleDriverName);
	}

	/** Get Vehicle Driver Name.
		@return The person name of vehicle by whom it is drived
	  */
	public String getVehicleDriverName () 
	{
		return (String)get_Value(COLUMNNAME_VehicleDriverName);
	}

	/** Set Vehicle No.
		@param VehicleNo 
		The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo)
	{
		set_Value (COLUMNNAME_VehicleNo, VehicleNo);
	}

	/** Get Vehicle No.
		@return The identification number of the vehicle being weighted
	  */
	public String getVehicleNo () 
	{
		return (String)get_Value(COLUMNNAME_VehicleNo);
	}

	/** Vesel Truck = BAKT */
	public static final String VEHICLETYPE_VeselTruck = "BAKT";
	/** Dump Truck = DUMP */
	public static final String VEHICLETYPE_DumpTruck = "DUMP";
	/** Tank Truck = TANK */
	public static final String VEHICLETYPE_TankTruck = "TANK";
	/** Unknown = UNKN */
	public static final String VEHICLETYPE_Unknown = "UNKN";
	/** Set Vehicle Type.
		@param VehicleType 
		The type of vehicle
	  */
	public void setVehicleType (String VehicleType)
	{

		set_Value (COLUMNNAME_VehicleType, VehicleType);
	}

	/** Get Vehicle Type.
		@return The type of vehicle
	  */
	public String getVehicleType () 
	{
		return (String)get_Value(COLUMNNAME_VehicleType);
	}
}