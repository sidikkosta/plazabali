/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Grading_Sheet_All
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Grading_Sheet_All 
{

    /** TableName=UNS_Grading_Sheet_All */
    public static final String Table_Name = "UNS_Grading_Sheet_All";

    /** AD_Table_ID=1000290 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Additional_Deduction */
    public static final String COLUMNNAME_Additional_Deduction = "Additional_Deduction";

	/** Set Additional Deduction	  */
	public void setAdditional_Deduction (String Additional_Deduction);

	/** Get Additional Deduction	  */
	public String getAdditional_Deduction();

    /** Column name Additional_Qty */
    public static final String COLUMNNAME_Additional_Qty = "Additional_Qty";

	/** Set Additional Qty	  */
	public void setAdditional_Qty (BigDecimal Additional_Qty);

	/** Get Additional Qty	  */
	public BigDecimal getAdditional_Qty();

    /** Column name BunchFailure */
    public static final String COLUMNNAME_BunchFailure = "BunchFailure";

	/** Set Bunch Failure.
	  * Tandan Ujung (Bunch Failure)
	  */
	public void setBunchFailure (BigDecimal BunchFailure);

	/** Get Bunch Failure.
	  * Tandan Ujung (Bunch Failure)
	  */
	public BigDecimal getBunchFailure();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name CompulsoryDeduction */
    public static final String COLUMNNAME_CompulsoryDeduction = "CompulsoryDeduction";

	/** Set Compulsory Deduction.
	  * Compulsory Deduction (Potongan Wajib)
	  */
	public void setCompulsoryDeduction (BigDecimal CompulsoryDeduction);

	/** Get Compulsory Deduction.
	  * Compulsory Deduction (Potongan Wajib)
	  */
	public BigDecimal getCompulsoryDeduction();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DefaultTare */
    public static final String COLUMNNAME_DefaultTare = "DefaultTare";

	/** Set Default Tare.
	  * The Default Tare is the default weight of an armada type.
	  */
	public void setDefaultTare (BigDecimal DefaultTare);

	/** Get Default Tare.
	  * The Default Tare is the default weight of an armada type.
	  */
	public BigDecimal getDefaultTare();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name Dirt */
    public static final String COLUMNNAME_Dirt = "Dirt";

	/** Set Dirt.
	  * Dirt
	  */
	public void setDirt (BigDecimal Dirt);

	/** Get Dirt.
	  * Dirt
	  */
	public BigDecimal getDirt();

    /** Column name DirtyBunch */
    public static final String COLUMNNAME_DirtyBunch = "DirtyBunch";

	/** Set Dirty Bunch.
	  * Dirty Bunch (Buah Kotor)
	  */
	public void setDirtyBunch (BigDecimal DirtyBunch);

	/** Get Dirty Bunch.
	  * Dirty Bunch (Buah Kotor)
	  */
	public BigDecimal getDirtyBunch();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name DripWet */
    public static final String COLUMNNAME_DripWet = "DripWet";

	/** Set Drip Wet.
	  * Drip Wet (Banyak Kandungan Air)
	  */
	public void setDripWet (BigDecimal DripWet);

	/** Get Drip Wet.
	  * Drip Wet (Banyak Kandungan Air)
	  */
	public BigDecimal getDripWet();

    /** Column name Dura */
    public static final String COLUMNNAME_Dura = "Dura";

	/** Set Dura.
	  * Dura (Kernel Tebal / Buah Tipis)
	  */
	public void setDura (BigDecimal Dura);

	/** Get Dura.
	  * Dura (Kernel Tebal / Buah Tipis)
	  */
	public BigDecimal getDura();

    /** Column name EmptyBunch */
    public static final String COLUMNNAME_EmptyBunch = "EmptyBunch";

	/** Set Empty Bunch.
	  * Empty Bunch (Tandan Kosong)
	  */
	public void setEmptyBunch (BigDecimal EmptyBunch);

	/** Get Empty Bunch.
	  * Empty Bunch (Tandan Kosong)
	  */
	public BigDecimal getEmptyBunch();

    /** Column name Est_AddDeductQty */
    public static final String COLUMNNAME_Est_AddDeductQty = "Est_AddDeductQty";

	/** Set Est. Additional Deduction	  */
	public void setEst_AddDeductQty (BigDecimal Est_AddDeductQty);

	/** Get Est. Additional Deduction	  */
	public BigDecimal getEst_AddDeductQty();

    /** Column name Est_BunchFailure */
    public static final String COLUMNNAME_Est_BunchFailure = "Est_BunchFailure";

	/** Set Est. Bunch Failure.
	  * Estimated Tandan Ujung (Bunch Failure)
	  */
	public void setEst_BunchFailure (BigDecimal Est_BunchFailure);

	/** Get Est. Bunch Failure.
	  * Estimated Tandan Ujung (Bunch Failure)
	  */
	public BigDecimal getEst_BunchFailure();

    /** Column name Est_CompulsoryDeduction */
    public static final String COLUMNNAME_Est_CompulsoryDeduction = "Est_CompulsoryDeduction";

	/** Set Est. Compulsory Deduction.
	  * Estimated Compulsory Deduction (Potongan Wajib)
	  */
	public void setEst_CompulsoryDeduction (BigDecimal Est_CompulsoryDeduction);

	/** Get Est. Compulsory Deduction.
	  * Estimated Compulsory Deduction (Potongan Wajib)
	  */
	public BigDecimal getEst_CompulsoryDeduction();

    /** Column name Est_Dirt */
    public static final String COLUMNNAME_Est_Dirt = "Est_Dirt";

	/** Set Est. Dirt.
	  * Estimated Dirt (Sampah)
	  */
	public void setEst_Dirt (BigDecimal Est_Dirt);

	/** Get Est. Dirt.
	  * Estimated Dirt (Sampah)
	  */
	public BigDecimal getEst_Dirt();

    /** Column name Est_DirtyBunch */
    public static final String COLUMNNAME_Est_DirtyBunch = "Est_DirtyBunch";

	/** Set Est. Dirty Bunch.
	  * Estimated Dirty Bunch (Buah Kotor)
	  */
	public void setEst_DirtyBunch (BigDecimal Est_DirtyBunch);

	/** Get Est. Dirty Bunch.
	  * Estimated Dirty Bunch (Buah Kotor)
	  */
	public BigDecimal getEst_DirtyBunch();

    /** Column name Est_DripWet */
    public static final String COLUMNNAME_Est_DripWet = "Est_DripWet";

	/** Set Est. Drip Wet.
	  * Estimated Drip Wet (Banyak Kandungan Air)
	  */
	public void setEst_DripWet (BigDecimal Est_DripWet);

	/** Get Est. Drip Wet.
	  * Estimated Drip Wet (Banyak Kandungan Air)
	  */
	public BigDecimal getEst_DripWet();

    /** Column name Est_Dura */
    public static final String COLUMNNAME_Est_Dura = "Est_Dura";

	/** Set Est.  Dura.
	  * Estimated Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public void setEst_Dura (BigDecimal Est_Dura);

	/** Get Est.  Dura.
	  * Estimated Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public BigDecimal getEst_Dura();

    /** Column name Est_EmptyBunch */
    public static final String COLUMNNAME_Est_EmptyBunch = "Est_EmptyBunch";

	/** Set Est. Empty Bunch.
	  * Estimated Empty Bunch (Tandan Kosong) in percentages
	  */
	public void setEst_EmptyBunch (BigDecimal Est_EmptyBunch);

	/** Get Est. Empty Bunch.
	  * Estimated Empty Bunch (Tandan Kosong) in percentages
	  */
	public BigDecimal getEst_EmptyBunch();

    /** Column name Est_LongStalk */
    public static final String COLUMNNAME_Est_LongStalk = "Est_LongStalk";

	/** Set Est. Long Stalk.
	  * Estimated Long Stalk (Tandan Panjang) in percentages
	  */
	public void setEst_LongStalk (BigDecimal Est_LongStalk);

	/** Get Est. Long Stalk.
	  * Estimated Long Stalk (Tandan Panjang) in percentages
	  */
	public BigDecimal getEst_LongStalk();

    /** Column name Est_NettoI */
    public static final String COLUMNNAME_Est_NettoI = "Est_NettoI";

	/** Set Est. Netto I.
	  * Netto 1 in estimation. It is Grossweight deducted with DefaultTare.
	  */
	public void setEst_NettoI (BigDecimal Est_NettoI);

	/** Get Est. Netto I.
	  * Netto 1 in estimation. It is Grossweight deducted with DefaultTare.
	  */
	public BigDecimal getEst_NettoI();

    /** Column name Est_OldBunch */
    public static final String COLUMNNAME_Est_OldBunch = "Est_OldBunch";

	/** Set Est. Old Bunch.
	  * Estimated Old Bunch (Tandan Lama)
	  */
	public void setEst_OldBunch (BigDecimal Est_OldBunch);

	/** Get Est. Old Bunch.
	  * Estimated Old Bunch (Tandan Lama)
	  */
	public BigDecimal getEst_OldBunch();

    /** Column name Est_OverRipe */
    public static final String COLUMNNAME_Est_OverRipe = "Est_OverRipe";

	/** Set Est. Over Ripe.
	  * Estimated Over Ripe (Buah Lewat Masak)
	  */
	public void setEst_OverRipe (BigDecimal Est_OverRipe);

	/** Get Est. Over Ripe.
	  * Estimated Over Ripe (Buah Lewat Masak)
	  */
	public BigDecimal getEst_OverRipe();

    /** Column name Est_RottenBunch */
    public static final String COLUMNNAME_Est_RottenBunch = "Est_RottenBunch";

	/** Set Est. Rotten Bunch.
	  * Estimated Rotten Bunch (Tandan Busuk)
	  */
	public void setEst_RottenBunch (BigDecimal Est_RottenBunch);

	/** Get Est. Rotten Bunch.
	  * Estimated Rotten Bunch (Tandan Busuk)
	  */
	public BigDecimal getEst_RottenBunch();

    /** Column name Est_SmallFruit */
    public static final String COLUMNNAME_Est_SmallFruit = "Est_SmallFruit";

	/** Set Est. Small Fruit.
	  * Estimated Small Fruit (Buah Kecil)
	  */
	public void setEst_SmallFruit (BigDecimal Est_SmallFruit);

	/** Get Est. Small Fruit.
	  * Estimated Small Fruit (Buah Kecil)
	  */
	public BigDecimal getEst_SmallFruit();

    /** Column name Est_Underdone */
    public static final String COLUMNNAME_Est_Underdone = "Est_Underdone";

	/** Set Est.  Underdone.
	  * Underdone (Kurang Masak) in percentages
	  */
	public void setEst_Underdone (BigDecimal Est_Underdone);

	/** Get Est.  Underdone.
	  * Underdone (Kurang Masak) in percentages
	  */
	public BigDecimal getEst_Underdone();

    /** Column name Est_Unripe */
    public static final String COLUMNNAME_Est_Unripe = "Est_Unripe";

	/** Set Est. Unripe.
	  * Estimated Unripe (Sangat Mentah)
	  */
	public void setEst_Unripe (BigDecimal Est_Unripe);

	/** Get Est. Unripe.
	  * Estimated Unripe (Sangat Mentah)
	  */
	public BigDecimal getEst_Unripe();

    /** Column name FFBReceiptCondition */
    public static final String COLUMNNAME_FFBReceiptCondition = "FFBReceiptCondition";

	/** Set FFB Receipt Condition.
	  * The condition of FFB receipt, either dominantly fresh or backlog.
	  */
	public void setFFBReceiptCondition (String FFBReceiptCondition);

	/** Get FFB Receipt Condition.
	  * The condition of FFB receipt, either dominantly fresh or backlog.
	  */
	public String getFFBReceiptCondition();

    /** Column name Grader_ID */
    public static final String COLUMNNAME_Grader_ID = "Grader_ID";

	/** Set Grader	  */
	public void setGrader_ID (int Grader_ID);

	/** Get Grader	  */
	public int getGrader_ID();

	public org.compiere.model.I_AD_User getGrader() throws RuntimeException;

    /** Column name GrossWeight */
    public static final String COLUMNNAME_GrossWeight = "GrossWeight";

	/** Set Gross Weight.
	  * Weight of a product (Gross)
	  */
	public void setGrossWeight (BigDecimal GrossWeight);

	/** Get Gross Weight.
	  * Weight of a product (Gross)
	  */
	public BigDecimal getGrossWeight();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name IsDumpTruck */
    public static final String COLUMNNAME_IsDumpTruck = "IsDumpTruck";

	/** Set Is Dump Truck	  */
	public void setIsDumpTruck (boolean IsDumpTruck);

	/** Get Is Dump Truck	  */
	public boolean isDumpTruck();

    /** Column name IsQtyPercentage */
    public static final String COLUMNNAME_IsQtyPercentage = "IsQtyPercentage";

	/** Set Is Qty Percentage.
	  * Indicate that this component is based in % Quantity
	  */
	public void setIsQtyPercentage (boolean IsQtyPercentage);

	/** Get Is Qty Percentage.
	  * Indicate that this component is based in % Quantity
	  */
	public boolean isQtyPercentage();

    /** Column name LongStalk */
    public static final String COLUMNNAME_LongStalk = "LongStalk";

	/** Set Long Stalk.
	  * Long Stalk (Tandan Panjang)
	  */
	public void setLongStalk (BigDecimal LongStalk);

	/** Get Long Stalk.
	  * Long Stalk (Tandan Panjang)
	  */
	public BigDecimal getLongStalk();

    /** Column name NettoI */
    public static final String COLUMNNAME_NettoI = "NettoI";

	/** Set Netto-I.
	  * The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI);

	/** Get Netto-I.
	  * The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI();

    /** Column name OldBunch */
    public static final String COLUMNNAME_OldBunch = "OldBunch";

	/** Set Old Bunch.
	  * Old Bunch (Tandan Lama)
	  */
	public void setOldBunch (BigDecimal OldBunch);

	/** Get Old Bunch.
	  * Old Bunch (Tandan Lama)
	  */
	public BigDecimal getOldBunch();

    /** Column name OverRipe */
    public static final String COLUMNNAME_OverRipe = "OverRipe";

	/** Set Over Ripe.
	  * Over Ripe (Buah Lewat Masak)
	  */
	public void setOverRipe (BigDecimal OverRipe);

	/** Get Over Ripe.
	  * Over Ripe (Buah Lewat Masak)
	  */
	public BigDecimal getOverRipe();

    /** Column name Percent_AddDeduct */
    public static final String COLUMNNAME_Percent_AddDeduct = "Percent_AddDeduct";

	/** Set (%) Additional Deduction	  */
	public void setPercent_AddDeduct (BigDecimal Percent_AddDeduct);

	/** Get (%) Additional Deduction	  */
	public BigDecimal getPercent_AddDeduct();

    /** Column name Percent_BunchFailure */
    public static final String COLUMNNAME_Percent_BunchFailure = "Percent_BunchFailure";

	/** Set (%) Bunch Failure.
	  * (%) Bunch Failure
	  */
	public void setPercent_BunchFailure (BigDecimal Percent_BunchFailure);

	/** Get (%) Bunch Failure.
	  * (%) Bunch Failure
	  */
	public BigDecimal getPercent_BunchFailure();

    /** Column name Percent_CompulsoryDeduction */
    public static final String COLUMNNAME_Percent_CompulsoryDeduction = "Percent_CompulsoryDeduction";

	/** Set (%) Compulsory Deduction.
	  * Compulsory Deduction (Potongan Wajib) in percentage
	  */
	public void setPercent_CompulsoryDeduction (BigDecimal Percent_CompulsoryDeduction);

	/** Get (%) Compulsory Deduction.
	  * Compulsory Deduction (Potongan Wajib) in percentage
	  */
	public BigDecimal getPercent_CompulsoryDeduction();

    /** Column name Percent_Dirt */
    public static final String COLUMNNAME_Percent_Dirt = "Percent_Dirt";

	/** Set (%) Dirt.
	  * Dirt (Sampah) in percentages
	  */
	public void setPercent_Dirt (BigDecimal Percent_Dirt);

	/** Get (%) Dirt.
	  * Dirt (Sampah) in percentages
	  */
	public BigDecimal getPercent_Dirt();

    /** Column name Percent_DirtyBunch */
    public static final String COLUMNNAME_Percent_DirtyBunch = "Percent_DirtyBunch";

	/** Set (%) Dirty Bunch.
	  * Dirty Bunch (Buah Kotor) in percentage
	  */
	public void setPercent_DirtyBunch (BigDecimal Percent_DirtyBunch);

	/** Get (%) Dirty Bunch.
	  * Dirty Bunch (Buah Kotor) in percentage
	  */
	public BigDecimal getPercent_DirtyBunch();

    /** Column name Percent_DripWet */
    public static final String COLUMNNAME_Percent_DripWet = "Percent_DripWet";

	/** Set (%) Drip Wet.
	  * Drip Wet (Banyak Kandungan Air) in percentages
	  */
	public void setPercent_DripWet (BigDecimal Percent_DripWet);

	/** Get (%) Drip Wet.
	  * Drip Wet (Banyak Kandungan Air) in percentages
	  */
	public BigDecimal getPercent_DripWet();

    /** Column name Percent_Dura */
    public static final String COLUMNNAME_Percent_Dura = "Percent_Dura";

	/** Set (%) Dura.
	  * Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public void setPercent_Dura (BigDecimal Percent_Dura);

	/** Get (%) Dura.
	  * Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public BigDecimal getPercent_Dura();

    /** Column name Percent_EmptyBunch */
    public static final String COLUMNNAME_Percent_EmptyBunch = "Percent_EmptyBunch";

	/** Set (%) Empty Bunch.
	  * Empty Bunch (Tandan Kosong) in percentages
	  */
	public void setPercent_EmptyBunch (BigDecimal Percent_EmptyBunch);

	/** Get (%) Empty Bunch.
	  * Empty Bunch (Tandan Kosong) in percentages
	  */
	public BigDecimal getPercent_EmptyBunch();

    /** Column name Percent_LongStalk */
    public static final String COLUMNNAME_Percent_LongStalk = "Percent_LongStalk";

	/** Set (%) Long Stalk.
	  * Long Stalk (Tandan Panjang) in percentages
	  */
	public void setPercent_LongStalk (BigDecimal Percent_LongStalk);

	/** Get (%) Long Stalk.
	  * Long Stalk (Tandan Panjang) in percentages
	  */
	public BigDecimal getPercent_LongStalk();

    /** Column name Percent_OldBunch */
    public static final String COLUMNNAME_Percent_OldBunch = "Percent_OldBunch";

	/** Set (%) Old Bunch.
	  * Old Bunch (Tandan Lama) in percentages
	  */
	public void setPercent_OldBunch (BigDecimal Percent_OldBunch);

	/** Get (%) Old Bunch.
	  * Old Bunch (Tandan Lama) in percentages
	  */
	public BigDecimal getPercent_OldBunch();

    /** Column name Percent_OverRipe */
    public static final String COLUMNNAME_Percent_OverRipe = "Percent_OverRipe";

	/** Set (%) Over Ripe.
	  * Over Ripe (Lewat Mentah) in percentages
	  */
	public void setPercent_OverRipe (BigDecimal Percent_OverRipe);

	/** Get (%) Over Ripe.
	  * Over Ripe (Lewat Mentah) in percentages
	  */
	public BigDecimal getPercent_OverRipe();

    /** Column name Percent_RottenBunch */
    public static final String COLUMNNAME_Percent_RottenBunch = "Percent_RottenBunch";

	/** Set (%) Rotten Bunch.
	  * Rotten Bunch (Tandan Busuk) in percentages
	  */
	public void setPercent_RottenBunch (BigDecimal Percent_RottenBunch);

	/** Get (%) Rotten Bunch.
	  * Rotten Bunch (Tandan Busuk) in percentages
	  */
	public BigDecimal getPercent_RottenBunch();

    /** Column name Percent_SmallFruit */
    public static final String COLUMNNAME_Percent_SmallFruit = "Percent_SmallFruit";

	/** Set (%) Small Fruit.
	  * Small Fruit (Buah Kecil) in percentages
	  */
	public void setPercent_SmallFruit (BigDecimal Percent_SmallFruit);

	/** Get (%) Small Fruit.
	  * Small Fruit (Buah Kecil) in percentages
	  */
	public BigDecimal getPercent_SmallFruit();

    /** Column name Percent_Underdone */
    public static final String COLUMNNAME_Percent_Underdone = "Percent_Underdone";

	/** Set (%) Underdone.
	  * Underdone (Kurang Masak) in percentages
	  */
	public void setPercent_Underdone (BigDecimal Percent_Underdone);

	/** Get (%) Underdone.
	  * Underdone (Kurang Masak) in percentages
	  */
	public BigDecimal getPercent_Underdone();

    /** Column name Percent_Unripe */
    public static final String COLUMNNAME_Percent_Unripe = "Percent_Unripe";

	/** Set (%) Unripe.
	  * Unripe (Sangat Mentah) in percentages
	  */
	public void setPercent_Unripe (BigDecimal Percent_Unripe);

	/** Get (%) Unripe.
	  * Unripe (Sangat Mentah) in percentages
	  */
	public BigDecimal getPercent_Unripe();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name RottenBunch */
    public static final String COLUMNNAME_RottenBunch = "RottenBunch";

	/** Set Rotten Bunch.
	  * Rotten Bunch (Tandan Busuk)
	  */
	public void setRottenBunch (BigDecimal RottenBunch);

	/** Get Rotten Bunch.
	  * Rotten Bunch (Tandan Busuk)
	  */
	public BigDecimal getRottenBunch();

    /** Column name RoundingScale */
    public static final String COLUMNNAME_RoundingScale = "RoundingScale";

	/** Set Rounding Scale.
	  * Scale to be rounding (ie. scale to unit, dozens, hundred, thousands)
	  */
	public void setRoundingScale (String RoundingScale);

	/** Get Rounding Scale.
	  * Scale to be rounding (ie. scale to unit, dozens, hundred, thousands)
	  */
	public String getRoundingScale();

    /** Column name ShippedBy */
    public static final String COLUMNNAME_ShippedBy = "ShippedBy";

	/** Set Shipped By	  */
	public void setShippedBy (String ShippedBy);

	/** Get Shipped By	  */
	public String getShippedBy();

    /** Column name SmallFruit */
    public static final String COLUMNNAME_SmallFruit = "SmallFruit";

	/** Set Small Fruit.
	  * Small Fruit (Buah Kecil)
	  */
	public void setSmallFruit (BigDecimal SmallFruit);

	/** Get Small Fruit.
	  * Small Fruit (Buah Kecil)
	  */
	public BigDecimal getSmallFruit();

    /** Column name Tare */
    public static final String COLUMNNAME_Tare = "Tare";

	/** Set Tare.
	  * The weight as comparison
	  */
	public void setTare (BigDecimal Tare);

	/** Get Tare.
	  * The weight as comparison
	  */
	public BigDecimal getTare();

    /** Column name TotalEstimation */
    public static final String COLUMNNAME_TotalEstimation = "TotalEstimation";

	/** Set Total Estimation.
	  * Total estimation value
	  */
	public void setTotalEstimation (BigDecimal TotalEstimation);

	/** Get Total Estimation.
	  * Total estimation value
	  */
	public BigDecimal getTotalEstimation();

    /** Column name TotalPercent */
    public static final String COLUMNNAME_TotalPercent = "TotalPercent";

	/** Set Total Percent.
	  * The grand total of percentages
	  */
	public void setTotalPercent (BigDecimal TotalPercent);

	/** Get Total Percent.
	  * The grand total of percentages
	  */
	public BigDecimal getTotalPercent();

    /** Column name TotalQty */
    public static final String COLUMNNAME_TotalQty = "TotalQty";

	/** Set Total Quantity.
	  * Total Quantity
	  */
	public void setTotalQty (BigDecimal TotalQty);

	/** Get Total Quantity.
	  * Total Quantity
	  */
	public BigDecimal getTotalQty();

    /** Column name Underdone */
    public static final String COLUMNNAME_Underdone = "Underdone";

	/** Set Underdone.
	  * Underdone (Kurang Masak)
	  */
	public void setUnderdone (BigDecimal Underdone);

	/** Get Underdone.
	  * Underdone (Kurang Masak)
	  */
	public BigDecimal getUnderdone();

    /** Column name Unripe */
    public static final String COLUMNNAME_Unripe = "Unripe";

	/** Set Unripe.
	  * Unripe (Sangat Mentah)
	  */
	public void setUnripe (BigDecimal Unripe);

	/** Get Unripe.
	  * Unripe (Sangat Mentah)
	  */
	public BigDecimal getUnripe();

    /** Column name UNS_Grading_Sheet_All_ID */
    public static final String COLUMNNAME_UNS_Grading_Sheet_All_ID = "UNS_Grading_Sheet_All_ID";

	/** Set Grading Sheet	  */
	public void setUNS_Grading_Sheet_All_ID (int UNS_Grading_Sheet_All_ID);

	/** Get Grading Sheet	  */
	public int getUNS_Grading_Sheet_All_ID();

    /** Column name UNS_Grading_Sheet_All_UU */
    public static final String COLUMNNAME_UNS_Grading_Sheet_All_UU = "UNS_Grading_Sheet_All_UU";

	/** Set UNS_Grading_Sheet_All_UU	  */
	public void setUNS_Grading_Sheet_All_UU (String UNS_Grading_Sheet_All_UU);

	/** Get UNS_Grading_Sheet_All_UU	  */
	public String getUNS_Grading_Sheet_All_UU();

    /** Column name UNS_WeighbridgeTicket_ID */
    public static final String COLUMNNAME_UNS_WeighbridgeTicket_ID = "UNS_WeighbridgeTicket_ID";

	/** Set Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID);

	/** Get Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID();

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VehicleNo */
    public static final String COLUMNNAME_VehicleNo = "VehicleNo";

	/** Set Vehicle No.
	  * The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo);

	/** Get Vehicle No.
	  * The identification number of the vehicle being weighted
	  */
	public String getVehicleNo();
}
