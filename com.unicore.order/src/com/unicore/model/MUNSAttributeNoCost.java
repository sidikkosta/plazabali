/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MCost;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MProduct;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.MInventory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSAttributeNoCost extends X_UNS_AttributeNoCost
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2352947264432250586L;

	public MUNSAttributeNoCost(Properties ctx, int UNS_AttributeNoCost_ID,
			String trxName) {
		super(ctx, UNS_AttributeNoCost_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSAttributeNoCost(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSAttributeNoCost(MUNSSalesReconciliation sr, MUNSPOSRecapLineMA ma)
	{
		this(sr.getCtx(), 0, sr.get_TrxName());
		setAD_Org_ID(sr.getAD_Org_ID());
		setM_AttributeSetInstance_ID(ma.getM_AttributeSetInstance_ID());
		setQty(ma.getQty());
		setMovementQty(ma.getQty());
		setM_Product_ID(ma.getUNS_POSRecapLine().getM_Product_ID());
		setM_Locator_ID(ma.getM_Locator_ID());
		setUNS_SalesReconciliation_ID(sr.get_ID());
		setC_DocType_ID(sr.getC_DocType_ID());
		setDateMaterialPolicy(ma.getDateMaterialPolicy());
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(getM_AttributeSetInstanceTo_ID() > 0)
		{
			if(getCostAmt() == null)
			{
				log.saveError("Error", "No Cost");
				return false;
			}
			if(getM_AttributeSetInstance_ID() == getM_AttributeSetInstanceTo_ID())
			{
				log.saveError("Error", "ETBB From = ETBB To.");
				return false;
			}
		}
		
//		if((getM_AttributeSetInstanceTo_ID() > 0 && getMovementQty().signum() == 0)
//				|| getM_AttributeSetInstanceTo_ID() <= 0 && getMovementQty().signum() != 0)
//		{
//			log.saveError("Error", "ASI To <> 0 & MoveQty = 0 OR ASI To = 0 & MoveQty <> 0");
//			return false;
//		}
		
		if(getM_AttributeSetInstanceTo_ID() > 0)
		{
			setDateMaterialPolicy(getM_AttributeSetInstance().getCreated());
			setRemainingQty(getRemaining());
		}
		
//		if(getMovementQty().compareTo(getRemainingQty()) == 1)
//		{
//			log.saveError("Error", "Over movement qty. Remaining Qty :: " + getRemainingQty());
//			return false;
//		}
		
		if(isManual() && newRecord)
		{
			setQtyOnHand(getOnHand());
			MAcctSchema[] ass = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
			for(MAcctSchema as : ass)
			{
				if(as.getC_Currency_ID() != 303)
					continue;
				BigDecimal cost = MCost.getCurrentCost(MProduct.get(getCtx(), getM_Product_ID()),
						getM_AttributeSetInstance_ID(), as, getAD_Org_ID(), null, Env.ONE,
						0, false, null);
				setNoCost(cost == null);
			}
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		if(isProcessed())
		{
			log.saveError("Error", "Record has processed.!");
			return false;
		}
		
		return true;
	}
	
	public static MUNSAttributeNoCost getCreate(MUNSSalesReconciliation sr, MUNSPOSRecapLineMA ma)
	{
		MUNSAttributeNoCost result = MUNSAttributeNoCost.get(sr, ma);
		if(result == null)
		{
			result = new MUNSAttributeNoCost(sr, ma);
			result.saveEx();
		}
		return result;
	}
	
	public boolean createCostAdjustment(MInventory inv)
	{	
		MInventoryLine line = MInventoryLine.get(inv, getM_Locator_ID(), getM_Product_ID(), getM_AttributeSetInstance_ID());
		if(line == null)
		{
			line = new MInventoryLine(getCtx(), 0, get_TrxName());
			line.setM_Inventory_ID(inv.get_ID());
			line.setAD_Org_ID(getAD_Org_ID());
			line.setM_Product_ID(getM_Product_ID());
			line.setM_AttributeSetInstance_ID(getM_AttributeSetInstance_ID());
			line.setM_Locator_ID(getM_Locator_ID());
			line.validateQtyCount();
			line.setQtyRealCount(getMovementQty());
			line.setNewCostPrice(getCostAmt().multiply(getMovementQty()));
		}
		else
		{
			line.setQtyRealCount(line.getQtyRealCount().add(getMovementQty()));
			line.setNewCostPrice(line.getNewCostPrice().add(getCostAmt().multiply(getMovementQty())));
		}
		line.saveEx();

		return true;
	}
	
	public boolean createStockAdjusment(MInventory inv)
	{
		if(!isNoCost())
		{
			MInventoryLine line = MInventoryLine.get(inv, getM_Locator_ID(), getM_Product_ID(), getM_AttributeSetInstance_ID());
			if(line == null)
			{
				line = new MInventoryLine(getCtx(), 0, get_TrxName());
				line.setM_Inventory_ID(inv.get_ID());
				line.setAD_Org_ID(getAD_Org_ID());
				line.setM_Product_ID(getM_Product_ID());
				line.setM_AttributeSetInstance_ID(getM_AttributeSetInstance_ID());
				line.setM_Locator_ID(getM_Locator_ID());
				line.validateQtyCount();
				line.saveEx();
				line.load(get_TrxName());
				line.setQtyRealCount(line.getQtyBook().add(getMovementQty()));
			}
			else
				line.setQtyRealCount(line.getQtyRealCount().add(getMovementQty()));
			line.saveEx();
		}
		
		MInventoryLine line1 = MInventoryLine.get(inv, getM_Locator_ID(), getM_Product_ID(), getM_AttributeSetInstanceTo_ID());
		if(line1 == null)
		{
			line1 = new MInventoryLine(getCtx(), 0, get_TrxName());
			line1.setM_Inventory_ID(inv.get_ID());
			line1.setAD_Org_ID(getAD_Org_ID());
			line1.setM_Product_ID(getM_Product_ID());
			line1.setM_AttributeSetInstance_ID(getM_AttributeSetInstanceTo_ID());
			line1.setM_Locator_ID(getM_Locator_ID());
			line1.validateQtyCount();
			line1.saveEx();
			line1.load(get_TrxName());
			line1.setQtyRealCount(line1.getQtyBook().subtract(getMovementQty()));
		}
		else
			line1.setQtyRealCount(line1.getQtyRealCount().subtract(getMovementQty()));
		line1.saveEx();
		
		setM_InventoryLine_ID(line1.get_ID());
		setProcessed(true);
		return save();
	}
	
	public static MUNSAttributeNoCost get(MUNSSalesReconciliation sr, MUNSPOSRecapLineMA ma)
	{
		MUNSAttributeNoCost result = Query.get(sr.getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, "UNS_SalesReconciliation_ID = ? AND M_AttributeSetInstance_ID = ?"
						+ " AND M_Locator_ID = ? AND M_Product_ID = ? AND DateMaterialPolicy = ?"
						+ " AND Processed = 'N'", sr.get_TrxName()).
							setParameters(sr.get_ID(), ma.getM_AttributeSetInstance_ID(), ma.getM_Locator_ID()
									, ma.getUNS_POSRecapLine().getM_Product_ID(), ma.getDateMaterialPolicy()).first();
		
		return result;
	}
	
	private BigDecimal getRemaining()
	{
		String sql = "SELECT SUM(MovementQty) FROM UNS_AttributeNoCost WHERE M_Product_ID = ?"
				+ " AND M_AttributeSetInstance_ID = ? AND DateMaterialPolicy = ? AND M_Locator_ID = ?"
				+ " AND UNS_SalesReconciliation_ID = ? AND UNS_AttributeNoCost_ID <> ?";
		BigDecimal moveQty = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getM_Product_ID(), getM_AttributeSetInstance_ID(), getDateMaterialPolicy(),
			getM_Locator_ID(), getUNS_SalesReconciliation_ID(), get_ID()
		});

		return moveQty != null ? getQty().subtract(moveQty) : getQty();
	}
	
	private BigDecimal getOnHand()
	{
		String sql = "SELECT SUM(QtyOnHand) FROM M_StorageOnHand WHERE M_Product_ID = ?"
				+ " AND M_AttributeSetInstance_ID = ? AND DateMaterialPolicy = ?"
				+ " AND M_Locator_ID = ?";
		BigDecimal onHand = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{
			getM_Product_ID(), getM_AttributeSetInstance_ID(), getDateMaterialPolicy(),
			getM_Locator_ID()
		});
		if(onHand == null)
			onHand = Env.ZERO;
		
		return onHand;
	}
	
	private boolean m_IsManual = true;
	
	private boolean isManual()
	{
		return m_IsManual;
	}
	
	public void setManual(boolean isManual)
	{
		m_IsManual = isManual;
	}
}