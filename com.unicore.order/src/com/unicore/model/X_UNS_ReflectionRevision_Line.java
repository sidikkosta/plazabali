/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_ReflectionRevision_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ReflectionRevision_Line extends PO implements I_UNS_ReflectionRevision_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170309L;

    /** Standard Constructor */
    public X_UNS_ReflectionRevision_Line (Properties ctx, int UNS_ReflectionRevision_Line_ID, String trxName)
    {
      super (ctx, UNS_ReflectionRevision_Line_ID, trxName);
      /** if (UNS_ReflectionRevision_Line_ID == 0)
        {
			setisFlatDeduction (false);
// N
			setReflectionFlat (Env.ZERO);
// 0
			setReflectionFlatPerc (Env.ZERO);
// 0
			setReflectionTicket (Env.ZERO);
// 0
			setRevisionPercent (Env.ZERO);
// 0
			setRevisionQty (Env.ZERO);
// 0
			setUNS_ReflectionRevision_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ReflectionRevision_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ReflectionRevision_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Dura.
		@param Dura 
		Dura (Kernel Tebal / Buah Tipis)
	  */
	public void setDura (BigDecimal Dura)
	{
		set_Value (COLUMNNAME_Dura, Dura);
	}

	/** Get Dura.
		@return Dura (Kernel Tebal / Buah Tipis)
	  */
	public BigDecimal getDura () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Dura);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Flat Deduction.
		@param isFlatDeduction Flat Deduction	  */
	public void setisFlatDeduction (boolean isFlatDeduction)
	{
		set_Value (COLUMNNAME_isFlatDeduction, Boolean.valueOf(isFlatDeduction));
	}

	/** Get Flat Deduction.
		@return Flat Deduction	  */
	public boolean isFlatDeduction () 
	{
		Object oo = get_Value(COLUMNNAME_isFlatDeduction);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Netto-I.
		@param NettoI 
		The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI)
	{
		set_Value (COLUMNNAME_NettoI, NettoI);
	}

	/** Get Netto-I.
		@return The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Netto-II.
		@param NettoII 
		The nett weight of the second group calculation
	  */
	public void setNettoII (BigDecimal NettoII)
	{
		set_Value (COLUMNNAME_NettoII, NettoII);
	}

	/** Get Netto-II.
		@return The nett weight of the second group calculation
	  */
	public BigDecimal getNettoII () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoII);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Payment.
		@param QtyPayment Qty Payment	  */
	public void setQtyPayment (BigDecimal QtyPayment)
	{
		set_Value (COLUMNNAME_QtyPayment, QtyPayment);
	}

	/** Get Qty Payment.
		@return Qty Payment	  */
	public BigDecimal getQtyPayment () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyPayment);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reflection Flat.
		@param ReflectionFlat Reflection Flat	  */
	public void setReflectionFlat (BigDecimal ReflectionFlat)
	{
		set_Value (COLUMNNAME_ReflectionFlat, ReflectionFlat);
	}

	/** Get Reflection Flat.
		@return Reflection Flat	  */
	public BigDecimal getReflectionFlat () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReflectionFlat);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reflection Flat %.
		@param ReflectionFlatPerc Reflection Flat %	  */
	public void setReflectionFlatPerc (BigDecimal ReflectionFlatPerc)
	{
		set_Value (COLUMNNAME_ReflectionFlatPerc, ReflectionFlatPerc);
	}

	/** Get Reflection Flat %.
		@return Reflection Flat %	  */
	public BigDecimal getReflectionFlatPerc () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReflectionFlatPerc);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reflection %.
		@param ReflectionPercent Reflection %	  */
	public void setReflectionPercent (BigDecimal ReflectionPercent)
	{
		set_Value (COLUMNNAME_ReflectionPercent, ReflectionPercent);
	}

	/** Get Reflection %.
		@return Reflection %	  */
	public BigDecimal getReflectionPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReflectionPercent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reflection Ticket.
		@param ReflectionTicket Reflection Ticket	  */
	public void setReflectionTicket (BigDecimal ReflectionTicket)
	{
		set_Value (COLUMNNAME_ReflectionTicket, ReflectionTicket);
	}

	/** Get Reflection Ticket.
		@return Reflection Ticket	  */
	public BigDecimal getReflectionTicket () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReflectionTicket);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Revision %.
		@param RevisionPercent Revision %	  */
	public void setRevisionPercent (BigDecimal RevisionPercent)
	{
		set_Value (COLUMNNAME_RevisionPercent, RevisionPercent);
	}

	/** Get Revision %.
		@return Revision %	  */
	public BigDecimal getRevisionPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RevisionPercent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Revision Qty.
		@param RevisionQty Revision Qty	  */
	public void setRevisionQty (BigDecimal RevisionQty)
	{
		set_Value (COLUMNNAME_RevisionQty, RevisionQty);
	}

	/** Get Revision Qty.
		@return Revision Qty	  */
	public BigDecimal getRevisionQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RevisionQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_ReflectionRevision getUNS_ReflectionRevision() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_ReflectionRevision)MTable.get(getCtx(), com.unicore.model.I_UNS_ReflectionRevision.Table_Name)
			.getPO(getUNS_ReflectionRevision_ID(), get_TrxName());	}

	/** Set Reflection Revision.
		@param UNS_ReflectionRevision_ID Reflection Revision	  */
	public void setUNS_ReflectionRevision_ID (int UNS_ReflectionRevision_ID)
	{
		if (UNS_ReflectionRevision_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ReflectionRevision_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ReflectionRevision_ID, Integer.valueOf(UNS_ReflectionRevision_ID));
	}

	/** Get Reflection Revision.
		@return Reflection Revision	  */
	public int getUNS_ReflectionRevision_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ReflectionRevision_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Revision Line.
		@param UNS_ReflectionRevision_Line_ID Revision Line	  */
	public void setUNS_ReflectionRevision_Line_ID (int UNS_ReflectionRevision_Line_ID)
	{
		if (UNS_ReflectionRevision_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ReflectionRevision_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ReflectionRevision_Line_ID, Integer.valueOf(UNS_ReflectionRevision_Line_ID));
	}

	/** Get Revision Line.
		@return Revision Line	  */
	public int getUNS_ReflectionRevision_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ReflectionRevision_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ReflectionRevision_Line_UU.
		@param UNS_ReflectionRevision_Line_UU UNS_ReflectionRevision_Line_UU	  */
	public void setUNS_ReflectionRevision_Line_UU (String UNS_ReflectionRevision_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ReflectionRevision_Line_UU, UNS_ReflectionRevision_Line_UU);
	}

	/** Get UNS_ReflectionRevision_Line_UU.
		@return UNS_ReflectionRevision_Line_UU	  */
	public String getUNS_ReflectionRevision_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ReflectionRevision_Line_UU);
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getUNS_WeighbridgeTicket_ID(), get_TrxName());	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}