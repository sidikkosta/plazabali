/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_POSTerminalCashAcct
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSTerminalCashAcct extends PO implements I_UNS_POSTerminalCashAcct, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181018L;

    /** Standard Constructor */
    public X_UNS_POSTerminalCashAcct (Properties ctx, int UNS_POSTerminalCashAcct_ID, String trxName)
    {
      super (ctx, UNS_POSTerminalCashAcct_ID, trxName);
      /** if (UNS_POSTerminalCashAcct_ID == 0)
        {
			setBalance (Env.ZERO);
// 0
			setBeginningBalance (Env.ZERO);
// 0
			setC_Currency_ID (0);
			setIsUsedForChange (false);
// N
			setName (null);
			setUNS_POSTerminalCashAcct_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSTerminalCashAcct (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSTerminalCashAcct[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Balance.
		@param Balance Balance	  */
	public void setBalance (BigDecimal Balance)
	{
		set_ValueNoCheck (COLUMNNAME_Balance, Balance);
	}

	/** Get Balance.
		@return Balance	  */
	public BigDecimal getBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Balance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Beginning Balance.
		@param BeginningBalance 
		Balance prior to any transactions
	  */
	public void setBeginningBalance (BigDecimal BeginningBalance)
	{
		set_Value (COLUMNNAME_BeginningBalance, BeginningBalance);
	}

	/** Get Beginning Balance.
		@return Balance prior to any transactions
	  */
	public BigDecimal getBeginningBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BeginningBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getCash_Acct() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getCash_Acct_ID(), get_TrxName());	}

	/** Set Cash Account.
		@param Cash_Acct_ID Cash Account	  */
	public void setCash_Acct_ID (int Cash_Acct_ID)
	{
		if (Cash_Acct_ID < 1) 
			set_Value (COLUMNNAME_Cash_Acct_ID, null);
		else 
			set_Value (COLUMNNAME_Cash_Acct_ID, Integer.valueOf(Cash_Acct_ID));
	}

	/** Get Cash Account.
		@return Cash Account	  */
	public int getCash_Acct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Cash_Acct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Used For Change ?.
		@param IsUsedForChange Used For Change ?	  */
	public void setIsUsedForChange (boolean IsUsedForChange)
	{
		set_Value (COLUMNNAME_IsUsedForChange, Boolean.valueOf(IsUsedForChange));
	}

	/** Get Used For Change ?.
		@return Used For Change ?	  */
	public boolean isUsedForChange () 
	{
		Object oo = get_Value(COLUMNNAME_IsUsedForChange);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Store_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getStore_ID()));
    }

	/** Set Cash Account.
		@param UNS_POSTerminalCashAcct_ID Cash Account	  */
	public void setUNS_POSTerminalCashAcct_ID (int UNS_POSTerminalCashAcct_ID)
	{
		if (UNS_POSTerminalCashAcct_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminalCashAcct_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminalCashAcct_ID, Integer.valueOf(UNS_POSTerminalCashAcct_ID));
	}

	/** Get Cash Account.
		@return Cash Account	  */
	public int getUNS_POSTerminalCashAcct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTerminalCashAcct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSTerminalCashAcct_UU.
		@param UNS_POSTerminalCashAcct_UU UNS_POSTerminalCashAcct_UU	  */
	public void setUNS_POSTerminalCashAcct_UU (String UNS_POSTerminalCashAcct_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSTerminalCashAcct_UU, UNS_POSTerminalCashAcct_UU);
	}

	/** Get UNS_POSTerminalCashAcct_UU.
		@return UNS_POSTerminalCashAcct_UU	  */
	public String getUNS_POSTerminalCashAcct_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSTerminalCashAcct_UU);
	}
}