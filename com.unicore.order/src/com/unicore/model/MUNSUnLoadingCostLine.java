/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSUnLoadingCostLine extends X_UNS_UnLoadingCost_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9177954387926992613L;
	private BigDecimal m_BaseAmount = Env.ZERO;
	private static final String NETTO_I = "NettoI";
	private static final String NETTO_II = "NettoII";
	String ColumnName = null;
	String m_Message = null;
	int UNS_UnLoadingConfig_Line_ID = 0;
	
	/**
	 * @param ctx
	 * @param UNS_UnLoadingCost_Line_ID
	 * @param trxName
	 */
	public MUNSUnLoadingCostLine(Properties ctx, int UNS_UnLoadingCost_Line_ID,
			String trxName) {
		super(ctx, UNS_UnLoadingCost_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUnLoadingCostLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public boolean beforeSave(boolean newRecord)
	{
		if(getUNS_WeighbridgeTicket_ID() > 0 && newRecord)
		{
			MUNSWeighbridgeTicket wt = (MUNSWeighbridgeTicket) getUNS_WeighbridgeTicket();
			MUNSUnLoadingConfig lc = MUNSUnLoadingConfig.getByPartner(getCtx(), wt.getC_BPartner_ID(), wt.getDateDoc() ,get_TrxName());
			
			setAD_Org_ID(wt.getAD_Org_ID());
			setC_BPartner_ID(wt.getC_BPartner_ID());
			setM_Product_ID(wt.getM_Product_ID());
			setC_UOM_ID(wt.getM_Product().getC_UOM_ID());
			setIsDumpTruck(wt.isDumpTruck());
			if(lc == null)
			{
				setBaseAmount(Env.ZERO);
				setQty(Env.ZERO);
			}
			else
			{
				BigDecimal amount = getBaseAmount(lc, wt);
				setBaseAmount(amount);
				setAmount(amount);
				if(amount != Env.ZERO)
					setQty((BigDecimal) wt.get_Value(ColumnName));
			}
			setUNS_UnLoadingConfig_Line_ID(UNS_UnLoadingConfig_Line_ID);
		}
		
		if(is_ValueChanged(COLUMNNAME_Qty) && !newRecord)
		{
			MUNSUnLoadingConfigLine confline = new MUNSUnLoadingConfigLine(getCtx(), getUNS_UnLoadingConfig_Line_ID(), get_TrxName());
			if(!confline.isFixCalculation())
				setAmount(getQty().multiply(confline.getAmount()).setScale(0, RoundingMode.HALF_UP));
		}
		return true;
	}
	
	public boolean afterSave(boolean newRecord, boolean success)
	{
		if(success)
		{
			if(null != upHeaderAmount())
				return false;
		}
	
		return true;
	}
	
	public boolean beforeDelete()
	{
		String sql = "UPDATE UNS_WeighbridgeTicket SET UNS_UnLoadingCost_Line_ID = null, UNS_UnLoadingCost_ID = null"
				+ " WHERE UNS_UnLoadingCost_Line_ID = ?";
		DB.executeUpdate(sql, get_ID(), get_TrxName());
		
		return true;
	}
	
	public boolean afterDelete(boolean success)
	{
		if(success)
		{
			if(null != upHeaderAmount())
				return false;
		}
	
		return true;
	}
	
	public BigDecimal getBaseAmount(MUNSUnLoadingConfig lc, MUNSWeighbridgeTicket ticket)
	{
		MUNSGradingSheetAll gsa = MUNSGradingSheetAll.get(getCtx(), ticket.get_ID(), get_TrxName());
		for(MUNSUnLoadingConfigLine lcl : lc.getLines("M_Product_ID ASC"))
		{
			if(lcl.getM_Product_ID() != ticket.getM_Product_ID())
			{
				if(lcl.getM_Product_Category_ID() != ticket.getM_Product().getM_Product_Category_ID())
					break;
			}
			
			if(!lcl.isFixCalculation())
			{
				if(lcl.getCalculateBase().equals(X_UNS_UnLoadingConfig_Line.CALCULATEBASE_NettoI))
					ColumnName = NETTO_I;
				
				if(lcl.getCalculateBase().equals(X_UNS_UnLoadingConfig_Line.CALCULATEBASE_NettoII))
					ColumnName = NETTO_II;
			}

			boolean matchArmada = lcl.isAllArmada() ? lcl.isAllArmada() 
									: lcl.getUNS_ArmadaType_ID() == ticket.getUNS_ArmadaType_ID(); 
			String dumpTruckConfg = lcl.isDumpTruck() ? "Y" : "N";
			String dumpTruckGrading = gsa.isDumpTruck() ? "Y" : "N";
			if(lcl.getShippedBy().equals(gsa.getShippedBy())
					&& dumpTruckConfg.equals(dumpTruckGrading)
					&& matchArmada)
			{
				if(!lcl.isFixCalculation())
					m_BaseAmount = lcl.getAmount().multiply((BigDecimal) ticket.get_Value(ColumnName)).setScale(0, RoundingMode.HALF_UP);
				else
					m_BaseAmount = lcl.getAmount();
				
				UNS_UnLoadingConfig_Line_ID = lcl.get_ID();
				break;
			}
		}	
		return m_BaseAmount;
	}
	
	public String upHeaderAmount()
	{
		BigDecimal Amount = Env.ZERO;
		
		String sumAmount = "SELECT SUM(Amount) FROM UNS_UnLoadingCost_Line WHERE isActive = 'Y' AND UNS_UnLoadingCost_ID = ?";
		Amount = DB.getSQLValueBD(get_TrxName(), sumAmount, getUNS_UnLoadingCost_ID());
		
		if(null == Amount)
			Amount = Env.ZERO;
		
		MUNSUnLoadingCost lc = new MUNSUnLoadingCost(getCtx(), getUNS_UnLoadingCost_ID(), get_TrxName());
		lc.setGrandTotal(Amount);
		if(!lc.save(get_TrxName()))
			m_Message = "Failed when trying update header amount";
		
		return m_Message;
	}
	
	/**
	 * @param ctx
	 * @param UNS_WeighbridgeTicket_ID
	 * @param trxName
	 * @return {@link MUNSUnLoadingCostLine} w/ parent (CO, CL)
	 */
	public static MUNSUnLoadingCostLine getByTicket(Properties ctx, int UNS_WeighbridgeTicket_ID, String trxName)
	{
//		String whereClause = " AND EXISTS (SELECT 1 FROM UNS_UnLoadingCost uc"
//				+ " WHERE uc.UNS_UnLoadingCost_ID = UNS_UnLoadingCost_Line.UNS_UnLoadingCost_ID"
//				+ " AND uc.DocStatus IN ('CO', 'CL'))";
//		MUNSUnLoadingCostLine costLine = new Query(ctx, MUNSUnLoadingCostLine.Table_Name, COLUMNNAME_UNS_WeighbridgeTicket_ID + "=?"
//				+ whereClause, trxName).setParameters(UNS_WeighbridgeTicket_ID).first();
		
		String whereClause = " UNS_UnloadingCost_Line_ID = (SELECT UNS_UnloadingCost_Line_ID FROM UNS_WeighbridgeTicket WHERE "
				+ " UNS_WeighbridgeTicket_ID = ? ) ";
		MUNSUnLoadingCostLine costLine = new Query(ctx, MUNSUnLoadingCostLine.Table_Name,
				 whereClause, trxName).setParameters(UNS_WeighbridgeTicket_ID).first();
		
		return costLine;
	}
}