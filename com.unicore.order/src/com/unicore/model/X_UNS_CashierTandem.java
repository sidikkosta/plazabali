/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_CashierTandem
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CashierTandem extends PO implements I_UNS_CashierTandem, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181101L;

    /** Standard Constructor */
    public X_UNS_CashierTandem (Properties ctx, int UNS_CashierTandem_ID, String trxName)
    {
      super (ctx, UNS_CashierTandem_ID, trxName);
      /** if (UNS_CashierTandem_ID == 0)
        {
			setCashier_ID (0);
			setIsLogin (false);
// N
			setIsMainCashier (false);
// N
			setIsSessionActive (false);
// N
			setUNS_CashierTandem_ID (0);
			setUNS_POS_Session_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CashierTandem (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CashierTandem[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getCashier_ID(), get_TrxName());	}

	/** Set Cashier.
		@param Cashier_ID Cashier	  */
	public void setCashier_ID (int Cashier_ID)
	{
		if (Cashier_ID < 1) 
			set_Value (COLUMNNAME_Cashier_ID, null);
		else 
			set_Value (COLUMNNAME_Cashier_ID, Integer.valueOf(Cashier_ID));
	}

	/** Get Cashier.
		@return Cashier	  */
	public int getCashier_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Cashier_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Login?.
		@param IsLogin Login?	  */
	public void setIsLogin (boolean IsLogin)
	{
		set_Value (COLUMNNAME_IsLogin, Boolean.valueOf(IsLogin));
	}

	/** Get Login?.
		@return Login?	  */
	public boolean isLogin () 
	{
		Object oo = get_Value(COLUMNNAME_IsLogin);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Main Cashier?.
		@param IsMainCashier Main Cashier?	  */
	public void setIsMainCashier (boolean IsMainCashier)
	{
		set_Value (COLUMNNAME_IsMainCashier, Boolean.valueOf(IsMainCashier));
	}

	/** Get Main Cashier?.
		@return Main Cashier?	  */
	public boolean isMainCashier () 
	{
		Object oo = get_Value(COLUMNNAME_IsMainCashier);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Session Is Active?.
		@param IsSessionActive Session Is Active?	  */
	public void setIsSessionActive (boolean IsSessionActive)
	{
		set_Value (COLUMNNAME_IsSessionActive, Boolean.valueOf(IsSessionActive));
	}

	/** Get Session Is Active?.
		@return Session Is Active?	  */
	public boolean isSessionActive () 
	{
		Object oo = get_Value(COLUMNNAME_IsSessionActive);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Cahier Tandem.
		@param UNS_CashierTandem_ID Cahier Tandem	  */
	public void setUNS_CashierTandem_ID (int UNS_CashierTandem_ID)
	{
		if (UNS_CashierTandem_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CashierTandem_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CashierTandem_ID, Integer.valueOf(UNS_CashierTandem_ID));
	}

	/** Get Cahier Tandem.
		@return Cahier Tandem	  */
	public int getUNS_CashierTandem_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CashierTandem_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CashierTandem_UU.
		@param UNS_CashierTandem_UU UNS_CashierTandem_UU	  */
	public void setUNS_CashierTandem_UU (String UNS_CashierTandem_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CashierTandem_UU, UNS_CashierTandem_UU);
	}

	/** Get UNS_CashierTandem_UU.
		@return UNS_CashierTandem_UU	  */
	public String getUNS_CashierTandem_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CashierTandem_UU);
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}