/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author nurse
 *
 */
public class MUNSPOSTerminalCashAcct extends X_UNS_POSTerminalCashAcct {

	/**
	 * 
	 */
	private static final long serialVersionUID = -281893633899476950L;
	private MBPartner m_parent = null;
	
	public MBPartner getParent ()
	{
		if (m_parent == null)
			m_parent = new MBPartner(getCtx(), getStore_ID(), get_TrxName());
		return m_parent;
	}
	
	/**
	 * @param ctx
	 * @param UNS_POSTerminalCashAcct_ID
	 * @param trxName
	 */
	public MUNSPOSTerminalCashAcct(Properties ctx,
			int UNS_POSTerminalCashAcct_ID, String trxName) {
		super(ctx, UNS_POSTerminalCashAcct_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSTerminalCashAcct(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
}