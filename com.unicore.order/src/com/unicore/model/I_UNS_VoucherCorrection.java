/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_VoucherCorrection
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_VoucherCorrection 
{

    /** TableName=UNS_VoucherCorrection */
    public static final String Table_Name = "UNS_VoucherCorrection";

    /** AD_Table_ID=1000517 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AllocatedAmt */
    public static final String COLUMNNAME_AllocatedAmt = "AllocatedAmt";

	/** Set Allocated Amountt.
	  * Amount allocated to this document
	  */
	public void setAllocatedAmt (BigDecimal AllocatedAmt);

	/** Get Allocated Amountt.
	  * Amount allocated to this document
	  */
	public BigDecimal getAllocatedAmt();

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name C_DocType_ID */
    public static final String COLUMNNAME_C_DocType_ID = "C_DocType_ID";

	/** Set Document Type.
	  * Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID);

	/** Get Document Type.
	  * Document type or rules
	  */
	public int getC_DocType_ID();

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateAcct */
    public static final String COLUMNNAME_DateAcct = "DateAcct";

	/** Set Account Date.
	  * Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct);

	/** Get Account Date.
	  * Accounting Date
	  */
	public Timestamp getDateAcct();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name Posted */
    public static final String COLUMNNAME_Posted = "Posted";

	/** Set Posted.
	  * Posting status
	  */
	public void setPosted (boolean Posted);

	/** Get Posted.
	  * Posting status
	  */
	public boolean isPosted();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name Reversal_ID */
    public static final String COLUMNNAME_Reversal_ID = "Reversal_ID";

	/** Set Reversal ID.
	  * ID of document reversal
	  */
	public void setReversal_ID (int Reversal_ID);

	/** Get Reversal ID.
	  * ID of document reversal
	  */
	public int getReversal_ID();

	public com.unicore.model.I_UNS_VoucherCorrection getReversal() throws RuntimeException;

    /** Column name Store_ID */
    public static final String COLUMNNAME_Store_ID = "Store_ID";

	/** Set Store	  */
	public void setStore_ID (int Store_ID);

	/** Get Store	  */
	public int getStore_ID();

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException;

    /** Column name TrxAmt */
    public static final String COLUMNNAME_TrxAmt = "TrxAmt";

	/** Set Transaction Amount.
	  * Amount of a transaction
	  */
	public void setTrxAmt (BigDecimal TrxAmt);

	/** Get Transaction Amount.
	  * Amount of a transaction
	  */
	public BigDecimal getTrxAmt();

    /** Column name UNS_PaymentTrx_ID */
    public static final String COLUMNNAME_UNS_PaymentTrx_ID = "UNS_PaymentTrx_ID";

	/** Set Payment Transaction	  */
	public void setUNS_PaymentTrx_ID (int UNS_PaymentTrx_ID);

	/** Get Payment Transaction	  */
	public int getUNS_PaymentTrx_ID();

	public com.unicore.model.I_UNS_PaymentTrx getUNS_PaymentTrx() throws RuntimeException;

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException;

    /** Column name UNS_POSTrx_ID */
    public static final String COLUMNNAME_UNS_POSTrx_ID = "UNS_POSTrx_ID";

	/** Set POS Transactions	  */
	public void setUNS_POSTrx_ID (int UNS_POSTrx_ID);

	/** Get POS Transactions	  */
	public int getUNS_POSTrx_ID();

	public com.unicore.model.I_UNS_POSTrx getUNS_POSTrx() throws RuntimeException;

    /** Column name UNS_VoucherCode_ID */
    public static final String COLUMNNAME_UNS_VoucherCode_ID = "UNS_VoucherCode_ID";

	/** Set Voucher Code	  */
	public void setUNS_VoucherCode_ID (int UNS_VoucherCode_ID);

	/** Get Voucher Code	  */
	public int getUNS_VoucherCode_ID();

	public com.unicore.model.I_UNS_VoucherCode getUNS_VoucherCode() throws RuntimeException;

    /** Column name UNS_VoucherCorrection_ID */
    public static final String COLUMNNAME_UNS_VoucherCorrection_ID = "UNS_VoucherCorrection_ID";

	/** Set Voucher Correction	  */
	public void setUNS_VoucherCorrection_ID (int UNS_VoucherCorrection_ID);

	/** Get Voucher Correction	  */
	public int getUNS_VoucherCorrection_ID();

    /** Column name UNS_VoucherCorrection_UU */
    public static final String COLUMNNAME_UNS_VoucherCorrection_UU = "UNS_VoucherCorrection_UU";

	/** Set UNS_VoucherCorrection_UU	  */
	public void setUNS_VoucherCorrection_UU (String UNS_VoucherCorrection_UU);

	/** Get UNS_VoucherCorrection_UU	  */
	public String getUNS_VoucherCorrection_UU();

    /** Column name UnusedAmt */
    public static final String COLUMNNAME_UnusedAmt = "UnusedAmt";

	/** Set Unused Amt	  */
	public void setUnusedAmt (BigDecimal UnusedAmt);

	/** Get Unused Amt	  */
	public BigDecimal getUnusedAmt();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name UsedAmt */
    public static final String COLUMNNAME_UsedAmt = "UsedAmt";

	/** Set Used Amount	  */
	public void setUsedAmt (BigDecimal UsedAmt);

	/** Get Used Amount	  */
	public BigDecimal getUsedAmt();

    /** Column name VoucherAmt */
    public static final String COLUMNNAME_VoucherAmt = "VoucherAmt";

	/** Set Voucher Amount	  */
	public void setVoucherAmt (BigDecimal VoucherAmt);

	/** Get Voucher Amount	  */
	public BigDecimal getVoucherAmt();

    /** Column name VoucherCode */
    public static final String COLUMNNAME_VoucherCode = "VoucherCode";

	/** Set Voucher Code.
	  * Voucher Code
	  */
	public void setVoucherCode (String VoucherCode);

	/** Get Voucher Code.
	  * Voucher Code
	  */
	public String getVoucherCode();
}
