/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MConversionRate;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSShortCashier extends X_UNS_ShortCashier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2568069266454234065L;

	/**
	 * @param ctx
	 * @param UNS_ShortCashier_ID
	 * @param trxName
	 */
	public MUNSShortCashier(Properties ctx, int UNS_ShortCashier_ID,
			String trxName) {
		super(ctx, UNS_ShortCashier_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSShortCashier(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT COUNT(*) FROM UNS_ShortCashier"
				+ " WHERE UNS_POS_Recap_ID = ? AND C_Currency_ID = ?"
				+ " AND UNS_POS_Session_ID = ? AND UNS_ShortCashier_ID <> ?";
		
		int count = DB.getSQLValue(get_TrxName(), sql, getUNS_POSRecap_ID(), 
				getC_Currency_ID(), getUNS_POS_Session_ID(), get_ID());
		
		if(count > 0)
		{
			log.saveError("SaveError", "Duplicate currency in one session.");
			return false;
		}
		
		int currency1 = getUNS_POS_Session().getBase1Currency_ID();
		int currency2 = getUNS_POS_Session().getBase2Currency_ID();
		BigDecimal shortCashierAmtB1 = MConversionRate.convert(getCtx(), getShortCashierAmt(),
				getC_Currency_ID(), currency1, getAD_Client_ID(), getAD_Org_ID());;
		BigDecimal shortCashierAmtB2 = MConversionRate.convert(getCtx(), getShortCashierAmt(),
				getC_Currency_ID(), currency2, getAD_Client_ID(), getAD_Org_ID());
		setShortCashierAmtBase1(shortCashierAmtB1);
		setShortCashierAmtBase2(shortCashierAmtB2);
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!success)
			return false;
		
		MUNSCashReconciliation recon = getCashReconciliation();
		recon.setShortCashierAmt(recon.getShortCashierAmt().add(getShortCashierAmtBase1()));
		recon.setReCalcDepositAmt(true);
		recon.saveEx();
		
		return upHeader();
	}
	
	private boolean upHeader()
	{
		String sql = "SELECT SUM(ShortCashierAmtBase1) FROM UNS_ShortCashier"
				+ " WHERE UNS_POSRecap_ID = ? AND ShortCashierAmt > 0";
		BigDecimal negativeAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSRecap_ID());
		if(negativeAmt == null)
			negativeAmt = Env.ZERO;
		
		sql = sql.replace(">", "<");
		BigDecimal positifAmt = DB.getSQLValueBD(get_TrxName(), sql, getUNS_POSRecap_ID());
		if(positifAmt == null)
			positifAmt = Env.ZERO;
		
		MUNSPOSRecap recap = new MUNSPOSRecap(getCtx(), getUNS_POSRecap_ID(), get_TrxName());
		recap.setTotalShortCashierAmt(positifAmt);
		recap.setTotalShortCashierAmt1(negativeAmt);
		
		return recap.save();
	}
	
	private MUNSCashReconciliation getCashReconciliation()
	{
		String sql = "SELECT cr.UNS_CashReconciliation_ID FROM UNS_CashReconciliation cr"
				+ " INNER JOIN UNS_POSRecap pr ON pr.UNS_SalesReconliation_ID = cr.UNS_SalesReconciliation_ID"
				+ " WHERE cr.C_Currency_ID = ? AND pr.UNS_POSRecap_ID = ?";
		int recordID = DB.getSQLValue(get_TrxName(), sql, getC_Currency_ID(), getUNS_POSRecap_ID());
		
		return new MUNSCashReconciliation(getCtx(), recordID, get_TrxName());
	}
}