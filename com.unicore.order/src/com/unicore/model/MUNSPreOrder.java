/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.MessageBox;

/**
 * @author ALBURHANY
 *
 */
public class MUNSPreOrder extends X_UNS_PreOrder implements DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1553152160424097452L;
	private String attName = null;
	/**
	 * @param ctx
	 * @param UNS_PreOrder_ID
	 * @param trxName
	 */
	public MUNSPreOrder(Properties ctx, int UNS_PreOrder_ID, String trxName) {
		super(ctx, UNS_PreOrder_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */ 
	public MUNSPreOrder(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param whereClause
	 * @param orderClause
	 * @return {@link MUNSPreOrderLine}
	 */ 
	public MUNSPreOrderLine[] getLine(String whereClause, String orderClause)
	{
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPreOrderLine.COLUMNNAME_UNS_PreOrder_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append("AND ").append(whereClause);
		if (orderClause == null)
			orderClause = MUNSPreOrderLine.COLUMNNAME_UNS_PreOrder_Line_ID;
	
		List<MUNSPreOrderLine> list =
				Query
						.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPreOrderLine.Table_Name,
								whereClauseFinal.toString(), get_TrxName()).setParameters(get_ID())
						.setOrderBy(orderClause).list();

		return list.toArray(new MUNSPreOrderLine[list.size()]);
	}
	
	public MUNSPreOrderLine[] getLine(String whereClause, String orderClause, int getPOID)
	{
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPreOrderLine.COLUMNNAME_UNS_PreOrder_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (orderClause == null)
			orderClause = MUNSPreOrderLine.COLUMNNAME_UNS_PreOrder_Line_ID;
	
		List<MUNSPreOrderLine> list =
				Query
						.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPreOrderLine.Table_Name,
								whereClauseFinal.toString(), get_TrxName()).setParameters(getPOID)
						.setOrderBy(orderClause).list();

		return list.toArray(new MUNSPreOrderLine[list.size()]);
	}
	@Override
	protected boolean beforeSave (boolean newRecord)
	{		
		if (!newRecord && getC_Order_ID() > 0) 
		{
			String cekPL = "SELECT UNS_PackingList_ID FROM UNS_PackingList WHERE DocStatus IN (?, ?) AND UNS_PackingList_ID IN ("
					+ "	SELECT UNS_PackingList_ID FROM UNS_PackingList_Order WHERE C_Order_ID = " + getC_Order_ID() + " )";
			int processedPL_ID = DB.getSQLValueEx(get_TrxName(), cekPL, DOCSTATUS_Closed, DOCSTATUS_Completed);
			
			if(processedPL_ID > 0)
				throw new AdempiereUserError("Cannot edit Pre-Order. The Shipment has been processed.");
		}
		
		if(getAD_Org_ID() == 0)
			throw new AdempiereException("Please Define Organization (Organization * is disallowed.)");
		
		if(newRecord)
			setAD_User_ID(getCreatedBy());
	
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE C_BPartner_ID = " + getC_BPartner_ID();
		int idLocation = DB.getSQLValue(get_TrxName(), sql.toString());
		if(idLocation <= -1)
		{
			String msg = Msg.getMsg(getCtx(), "BPLocationNotFoundMsg");
			String title = Msg.getMsg(getCtx(), "BPLocationNotFoundTitle");
			int retVal = MessageBox.showMsg(this,
					getCtx()
					, msg
					, title
					, MessageBox.YESNO
					, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_NO || retVal == MessageBox.RETURN_NONE)
				return false;
		}
		else
		{
			MBPartnerLocation bpLocation = new MBPartnerLocation(getCtx(), idLocation, get_TrxName());
			setUNS_Rayon_ID(bpLocation.getUNS_Rayon_ID());
		}
		
		cekLocation();
		
		if(getDatePromised().compareTo(getDateDoc()) < 0)
			throw new AdempiereException("Date Promised not later than Document Date");
		
		if(is_ValueChanged(COLUMNNAME_UNS_OthersCustomer_ID))
		{
			if(getUNS_OthersCustomer_ID() > 0)
			{
				if(getUNS_OthersCustomer().getPhone() != null)
					attName = getUNS_OthersCustomer().getName() + " - " + getUNS_OthersCustomer().getPhone();
				else
					attName = getUNS_OthersCustomer().getName();
				setAttName(attName);
				setAttAddress(getUNS_OthersCustomer().getAddress());
			}
			if(getUNS_OthersCustomer_ID() == 0)
			{
				setAttName(null);
				setAttAddress(null);
			}
		}
		
		if ((newRecord || is_ValueChanged(COLUMNNAME_C_BPartner_ID))
				&& (getUNS_OthersCustomer_ID() == 0 && Util.isEmpty(getAttName(), true)))
		{
			sql = "SELECT UNS_Shipping_Track_ID FROM C_BPartner_Location loc "
					+ "WHERE C_BPartner_ID=" + getC_BPartner_ID();
			
			int shippingTrack_ID = DB.getSQLValueEx(get_TrxName(), sql); 
			
			if (shippingTrack_ID <= 0 && getUNS_Shipping_Track_ID() == 0)
				throw new AdempiereException("Customer's Shipping Track not defined yet.\n "
						+ "Please ask your system admin with Business Partner Role to define it, \n"
						+ "or just select into wich Shipping Track this Pre-Order will be shipped to.");
			else if (shippingTrack_ID > 0)
				setUNS_Shipping_Track_ID(shippingTrack_ID);
		}
		else if ((getUNS_OthersCustomer_ID() > 0 || !Util.isEmpty(getAttName(), true))
				&& getUNS_Shipping_Track_ID() == 0) {
			throw new AdempiereException("Customer's Or End User's Shipping Track not defined yet.\n "
					+ "Please select into wich Shipping Track this Pre-Order will be shipped to.");
		}
		
		if (getUNS_Shipping_Track_ID() == TRACK_SERBA_SERBI) 
		{
			if (getC_BPartner_ID() == BP_SERBA_SERBI 
					|| getC_BPartner_ID() == BP_SERBA_SERBI_DK 
					|| getC_BPartner_ID() == BP_SERBA_SERBI_LK) {
				setIsNeedAppointment(true);
			}
			else {
				throw new AdempiereException("Cannot set shipping track to Serba-Serbi for non SERBA-SERBI Customer.");
			}
		}
		
		return true;
	}
	
	public static final int TRACK_SERBA_SERBI = 1000012;
	public static final int BP_SERBA_SERBI = 1013841;
	public static final int BP_SERBA_SERBI_DK = 1013336;
	public static final int BP_SERBA_SERBI_LK = 1013335;
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSPreOrder[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_PreOrder_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	
	protected boolean beforeDelete()
	{
		for(MUNSPreOrderLine line : getLine(null, null))
		{
			if(!line.delete(false))
			{
				m_processMsg = "Failed when trying delete lines";
				log.saveError("Error", Msg.parseTranslation(getCtx(), m_processMsg));
				return false;
			}
		}
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//TODO if already for limit by times
		
		if(!limitOk())
			return DocAction.STATUS_Invalid;
			
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		//sudah revisi penambahan validasi bp
		int lastPO = DB.getSQLValue(get_TrxName(), "Select min(uns_preorder_id) from uns_preorder where docstatus in ('DR','IP','IN') "
				+ " and c_bpartner_id =  "+getC_BPartner_ID());
		
		if(lastPO!=get_ID())
		{
			for(MUNSPreOrderLine lastLine : getLine(null, null,lastPO))
			{
				for(MUNSPreOrderLine currLine : getLine(null, null,get_ID()))
				{
					if(lastLine.getM_Product_ID()==currLine.getM_Product_ID())
					{
						throw new AdempiereException("Pesanan sudah ada");
					}
				}
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (getLine(null, null).length == 0)
				throw new AdempiereException("Please define Pre Order line");
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);

		setDateFinish(DB.getSQLValueTS(get_TrxName(), "SELECT now()"));
		
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(null != okVoid())
			throw new AdempiereException(m_processMsg);
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String cekLocation()
	{		
		if(getAttName() != null && getAttAddress() == null)
			throw new AdempiereException("Please Define Attention Address for completed Shipping");
		if(getAttName() == null && getAttAddress() != null)
			throw new AdempiereException("Please Define Attention Name for completed Shipping");
		return null;
	}
	
	public String okVoid()
	{
		m_processMsg = null;
		
		if(getC_Order_ID() > 0)
			m_processMsg = "Cannot voided this document, Sales Order has created. Please contact admin sales order";
		
		return m_processMsg;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {

		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_Void;
		}
		
		return index;
	}
	
	public static MUNSPreOrder getByOrder (Properties ctx, int C_Order_ID, String trxName)
	{
		MUNSPreOrder po = new Query(ctx, MUNSPreOrder.Table_Name, "C_Order_ID=?", trxName)
							.setParameters(C_Order_ID).first();
		return po;
	}
	
	public boolean limitOk()
	{
		m_processMsg = null;
		MBPartner bp = new MBPartner(getCtx(), getC_BPartner_ID(), get_TrxName());
		
//		if(!bp.isLimitTimes() && bp.isLimitedSO())
		if(bp.isLimitedSO())	
		{
//			if(bp.getM_PriceList_ID() <= 0)
//				return true;
//			if(!bp.getM_PriceList().isActive())
//				return true;

			if (MBPartner.SOCREDITSTATUS_CreditStop.equals(bp.getSOCreditStatus()))
			{
				m_processMsg = "@BPartnerCreditStop@ - @TotalOpenBalance@=" 
						+ bp.getTotalOpenBalance()
						+ ", @SO_CreditLimit@=" + bp.getSO_CreditLimit();
				return false;
			}
			if (MBPartner.SOCREDITSTATUS_CreditHold.equals(bp.getSOCreditStatus()))
			{
				m_processMsg = "@BPartnerCreditHold@ - @TotalOpenBalance@=" 
						+ bp.getTotalOpenBalance() 
						+ ", @SO_CreditLimit@=" + bp.getSO_CreditLimit();
				return false;
			}
			
			String whereClause = " M_Product_ID IN (SELECT M_Product_ID FROM M_Product WHERE ProductType = 'I')";
			BigDecimal grandTotal = Env.ZERO;
			for(MUNSPreOrderLine poLine : getLine(whereClause, null))
			{
				String sql = "SELECT PriceStd FROM M_ProductPrice WHERE M_Product_ID=? AND M_PriceList_Version_ID IN"
						+ " (SELECT M_PriceList_Version_ID FROM M_PriceList_Version WHERE IsActive = 'Y' AND ValidFrom <= now()"
						+ " AND M_PriceList_ID=? ORDER BY ValidFrom DESC)";
				BigDecimal PriceStd = DB.getSQLValueBD(get_TrxName(), sql,
										new Object[]{poLine.getM_Product_ID(), bp.getM_PriceList_ID()});
				
				if(PriceStd == null)
					continue;
				
				BigDecimal totalLine = PriceStd.multiply(poLine.getQty());
				if(poLine.getSettingPriceType().equals(X_UNS_PreOrder_Line.SETTINGPRICETYPE_ModificationWithDiscount))
				{
					if(poLine.getDiscountAmt().compareTo(Env.ZERO) == 0)
					{
						BigDecimal totalDiscount = Env.ZERO;
						totalDiscount = totalLine.multiply(poLine.getDiscount()).divide(Env.ONEHUNDRED, 2);
						totalLine = totalLine.subtract(totalDiscount);
					}
					if(poLine.getDiscount().compareTo(Env.ZERO) == 0)
						totalLine = totalLine.subtract(poLine.getDiscountAmt().multiply(poLine.getQty()));
				}
				if(poLine.getSettingPriceType().equals(X_UNS_PreOrder_Line.SETTINGPRICETYPE_ModificationWithPrice))
					totalLine = poLine.getPriceEntered().multiply(poLine.getQty());
				
				grandTotal = grandTotal.add(totalLine);
			}
			
			if (MBPartner.SOCREDITSTATUS_CreditHold.equals(bp.getSOCreditStatus(grandTotal)))
			{
				m_processMsg = "@BPartnerOverOCreditHold@ - @TotalOpenBalance@=" 
						+ bp.getTotalOpenBalance() + ", @GrandTotal@=" + grandTotal.setScale(2)
						+ ", @SO_CreditLimit@=" + bp.getSO_CreditLimit();
				return false;
			}
		}
		
//		if(bp.isLimitTimes())
//		{
//			m_processMsg = bp.invoiceCreditStatus(bp);
//			
//			if(m_processMsg != null)
//				return false;
//		}
		return true;
	}
}