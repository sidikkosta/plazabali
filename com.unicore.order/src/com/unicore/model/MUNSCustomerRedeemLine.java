/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
//import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author ALBURHANY
 *
 */
public class MUNSCustomerRedeemLine extends X_UNS_CustomerRedeem_Line {

//	private String m_Message = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1200871406047018362L;

	/**
	 * @param ctx
	 * @param UNS_CustomerRedeem_Line_ID
	 * @param trxName
	 */
	public MUNSCustomerRedeemLine(Properties ctx,
			int UNS_CustomerRedeem_Line_ID, String trxName) {
		super(ctx, UNS_CustomerRedeem_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCustomerRedeemLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public boolean beforeSave (boolean newRecord)
	{					
		if(getQty().compareTo(Env.ZERO) <= 0)
			throw new AdempiereException("Qty disallowed zero or negate");
		
		if(newRecord || is_ValueChanged(COLUMNNAME_Qty) || is_ValueChanged(COLUMNNAME_UNS_ItemReward_Selection_ID))
			setValuePoint(getQty().multiply(getUNS_ItemReward_Selection().getValuePoint()));
		
		return true;
	}
	
	@Override
	public boolean afterSave(boolean success, boolean newRecord)
	{	
//		if(upTotalPoint() != null)
//			throw new AdempiereException(m_Message);
		
		return true;
	}
	
//	public String upTotalPoint()
//	{
//		m_Message = null;
//		
//		String sql = "SELECT COALESCE(SUM(ValuePoint), 0) FROM UNS_CustomerRedeem_Line WHERE"
//				+ " UNS_CustomerRedeem_ID = " + getUNS_CustomerRedeem_ID();
//		
//		MUNSCustomerRedeem cr = new MUNSCustomerRedeem(getCtx(), getUNS_CustomerRedeem_ID(), get_TrxName());
//		
//		cr.setPointRedeem(DB.getSQLValueBD(get_TrxName(), sql));
//		cr.setPointRedeem(cr.getPointRedeem().add(getValuePoint()));
//		if(!cr.save())
//			m_Message = "Error occurred when trying update header";
//		
//		return m_Message;
//	}
}
