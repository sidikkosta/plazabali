/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Menjangan
 *
 */
public class MUNSVoucherCode extends X_UNS_VoucherCode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 996223053291564839L;
	private MUNSVoucherBook m_book = null;

	/**
	 * @param ctx
	 * @param UNS_VoucherCode_ID
	 * @param trxName
	 */
	public MUNSVoucherCode(Properties ctx, int UNS_VoucherCode_ID,
			String trxName) 
	{
		super(ctx, UNS_VoucherCode_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSVoucherCode(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public MUNSVoucherCode (MUNSVoucherBook book)
	{
		this (book.getCtx(), 0, book.get_TrxName());
		m_book = book;
		setClientOrg(book);
		setVoucherAmt(m_book.getVoucherAmt());
		setUnusedAmt(getVoucherAmt());
		setUsedAmt(Env.ZERO);
	}
	
	public MUNSVoucherBook getBook ()
	{
		if (m_book == null)
			m_book = new MUNSVoucherBook(getCtx(), getUNS_VoucherBook_ID(), get_TrxName());
		
		return m_book;
	}
	
	public static MUNSVoucherCode getCode(Properties ctx, Timestamp date, String code, int C_Currency_ID, String trxName)
	{
		date = TimeUtil.trunc(date, TimeUtil.TRUNC_DAY);
		String whereClause = "Name = ? AND ? = (SELECT vb.C_Currency_ID FROM UNS_VoucherBook vb"
				+ " WHERE vb.UNS_VoucherBook_ID = UNS_VoucherCode.UNS_VoucherBook_ID AND ? BETWEEN "
				+ " vb.ValidFrom AND vb.ValidTo)";
		MUNSVoucherCode vCode = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				I_UNS_VoucherCode.Table_Name, whereClause, trxName)
				.setParameters(code, C_Currency_ID, date).firstOnly();
		
		return vCode;
	}
	
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		BigDecimal remainingAmt = getUnusedAmt();
		setUnusedAmt(getVoucherAmt().subtract(getUsedAmt()));
		
		if(getUnusedAmt().signum() < 0)
		{
			log.saveError("Error", "Unused Amount should not be less than paid amount. Remaining amount " + remainingAmt);
			return false;
		}
		if((getParent().getType().equals(X_UNS_VoucherBook.TYPE_ValidForOneTransaction)
				&& getUsedAmt().signum() != 0) 
				|| getUsedAmt().compareTo(getVoucherAmt()) == 0 )
		{
			setisInvalidate(true);
		}
		else if((getParent().getType().equals(X_UNS_VoucherBook.TYPE_ValidForOneTransaction)
				&& getUsedAmt().signum() == 0) 
				|| getUsedAmt().compareTo(getVoucherAmt()) == -1 )
		{
			setisInvalidate(false);
		}
		return true;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{	
		if(isNeedUpHeader())
			return MUNSVoucherCode.updateHeader((MUNSVoucherBook) getUNS_VoucherBook());
		
		return true;
	}
	
	private boolean m_needUpHeader = true;
	private boolean isNeedUpHeader()
	{
		return m_needUpHeader;
	}
	public void setNeedUpHeader(boolean isNeedUpHeader)
	{
		m_needUpHeader = isNeedUpHeader;
	}
	public static boolean updateHeader(MUNSVoucherBook vb)
	{	
		String sql = "UPDATE UNS_VoucherBook vb SET TotalUsedAmt = (SELECT SUM(UsedAmt) FROM UNS_VoucherCode"
				+ " WHERE UNS_VoucherBook_ID = vb.UNS_VoucherBook_ID) , TotalUnusedAmt = (SELECT SUM(UnusedAmt) FROM UNS_VoucherCode"
				+ " WHERE UNS_VoucherBook_ID = vb.UNS_VoucherBook_ID), TotalVoucherAmt = (SELECT SUM(VoucherAmt) FROM UNS_VoucherCode"
				+ " WHERE UNS_VoucherBook_ID = vb.UNS_VoucherBook_ID) WHERE UNS_VoucherBook_ID = ? ";
		DB.executeUpdate(sql, vb.get_ID(), vb.get_TrxName());
		
		return true;
	}
	
	public boolean isValidVoucher(Timestamp dateTrx)
	{
		getBook();
		dateTrx = TimeUtil.trunc(dateTrx, TimeUtil.TRUNC_DAY);
		if(m_book == null)
			{
			log.log(Level.SEVERE, "Cannot get Voucher Book");
			return false;
			}
			
		if(dateTrx.before(m_book.getValidFrom()) || dateTrx.after(m_book.getValidTo()) || isInvalidate())
			{
			log.log(Level.SEVERE, "Invalid voucher");
			return false;
			}
		
		return true;
	}
	
	public boolean updateVoucherCode(boolean isReversal, BigDecimal paidAmt)
	{
		if(m_book == null)
			getBook();
		
		BigDecimal usedAmt = getUsedAmt().add(paidAmt);
		BigDecimal unusedAmt = getUnusedAmt().subtract(paidAmt);
		
		setUsedAmt(usedAmt);
		setUnusedAmt(unusedAmt);
		
		if(!isReversal)
		{
			if(m_book.getType().equals(X_UNS_VoucherBook.TYPE_ValidForOneTransaction) 
					|| getUsedAmt().compareTo(getVoucherAmt()) == 0 )
				setisInvalidate(true);
		}
		else
		{
			setisInvalidate(false);
		}
		
		return save();
	}
	
	public MUNSVoucherBook getParent ()
	{
		return getBook();
	}
}
