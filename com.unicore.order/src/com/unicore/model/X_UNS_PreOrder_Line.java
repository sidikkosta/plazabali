/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PreOrder_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PreOrder_Line extends PO implements I_UNS_PreOrder_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20151201L;

    /** Standard Constructor */
    public X_UNS_PreOrder_Line (Properties ctx, int UNS_PreOrder_Line_ID, String trxName)
    {
      super (ctx, UNS_PreOrder_Line_ID, trxName);
      /** if (UNS_PreOrder_Line_ID == 0)
        {
			setQty (Env.ZERO);
			setUNS_PreOrder_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PreOrder_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PreOrder_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getC_OrderLine_ID(), get_TrxName());	}

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Discount %.
		@param Discount 
		Discount in percent
	  */
	public void setDiscount (BigDecimal Discount)
	{
		set_Value (COLUMNNAME_Discount, Discount);
	}

	/** Get Discount %.
		@return Discount in percent
	  */
	public BigDecimal getDiscount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Discount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Amount.
		@param DiscountAmt 
		Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt)
	{
		set_Value (COLUMNNAME_DiscountAmt, DiscountAmt);
	}

	/** Get Discount Amount.
		@return Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DiscountAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Is Project.
		@param isProject Is Project	  */
	public void setisProject (boolean isProject)
	{
		set_Value (COLUMNNAME_isProject, Boolean.valueOf(isProject));
	}

	/** Get Is Project.
		@return Is Project	  */
	public boolean isProject () 
	{
		Object oo = get_Value(COLUMNNAME_isProject);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Price.
		@param PriceEntered 
		Price Entered - the price based on the selected/base UoM
	  */
	public void setPriceEntered (BigDecimal PriceEntered)
	{
		set_Value (COLUMNNAME_PriceEntered, PriceEntered);
	}

	/** Get Price.
		@return Price Entered - the price based on the selected/base UoM
	  */
	public BigDecimal getPriceEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Product.
		@param Product Product	  */
	public void setProduct (String Product)
	{
		set_Value (COLUMNNAME_Product, Product);
	}

	/** Get Product.
		@return Product	  */
	public String getProduct () 
	{
		return (String)get_Value(COLUMNNAME_Product);
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Delivered Quantity.
		@param QtyDelivered 
		Delivered Quantity
	  */
	public void setQtyDelivered (BigDecimal QtyDelivered)
	{
		throw new IllegalArgumentException ("QtyDelivered is virtual column");	}

	/** Get Delivered Quantity.
		@return Delivered Quantity
	  */
	public BigDecimal getQtyDelivered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyDelivered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity Invoiced.
		@param QtyInvoiced 
		Invoiced Quantity
	  */
	public void setQtyInvoiced (BigDecimal QtyInvoiced)
	{
		throw new IllegalArgumentException ("QtyInvoiced is virtual column");	}

	/** Get Quantity Invoiced.
		@return Invoiced Quantity
	  */
	public BigDecimal getQtyInvoiced () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyInvoiced);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ordered Quantity.
		@param QtyOrdered 
		Ordered Quantity
	  */
	public void setQtyOrdered (BigDecimal QtyOrdered)
	{
		throw new IllegalArgumentException ("QtyOrdered is virtual column");	}

	/** Get Ordered Quantity.
		@return Ordered Quantity
	  */
	public BigDecimal getQtyOrdered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyOrdered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity Packed.
		@param QtyPacked 
		The quantity of the order that has been packed on packing list
	  */
	public void setQtyPacked (BigDecimal QtyPacked)
	{
		throw new IllegalArgumentException ("QtyPacked is virtual column");	}

	/** Get Quantity Packed.
		@return The quantity of the order that has been packed on packing list
	  */
	public BigDecimal getQtyPacked () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyPacked);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Default Price = DP */
	public static final String SETTINGPRICETYPE_DefaultPrice = "DP";
	/** Modification With Discount = MWD */
	public static final String SETTINGPRICETYPE_ModificationWithDiscount = "MWD";
	/** Modification With Price = MWP */
	public static final String SETTINGPRICETYPE_ModificationWithPrice = "MWP";
	/** Set Setting Price Type.
		@param SettingPriceType Setting Price Type	  */
	public void setSettingPriceType (String SettingPriceType)
	{

		set_Value (COLUMNNAME_SettingPriceType, SettingPriceType);
	}

	/** Get Setting Price Type.
		@return Setting Price Type	  */
	public String getSettingPriceType () 
	{
		return (String)get_Value(COLUMNNAME_SettingPriceType);
	}

	public com.unicore.model.I_UNS_PreOrder getUNS_PreOrder() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PreOrder)MTable.get(getCtx(), com.unicore.model.I_UNS_PreOrder.Table_Name)
			.getPO(getUNS_PreOrder_ID(), get_TrxName());	}

	/** Set Pre Order.
		@param UNS_PreOrder_ID Pre Order	  */
	public void setUNS_PreOrder_ID (int UNS_PreOrder_ID)
	{
		if (UNS_PreOrder_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_ID, Integer.valueOf(UNS_PreOrder_ID));
	}

	/** Get Pre Order.
		@return Pre Order	  */
	public int getUNS_PreOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PreOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Pre Order Detail.
		@param UNS_PreOrder_Line_ID Pre Order Detail	  */
	public void setUNS_PreOrder_Line_ID (int UNS_PreOrder_Line_ID)
	{
		if (UNS_PreOrder_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_Line_ID, Integer.valueOf(UNS_PreOrder_Line_ID));
	}

	/** Get Pre Order Detail.
		@return Pre Order Detail	  */
	public int getUNS_PreOrder_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PreOrder_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}