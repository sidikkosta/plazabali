/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSRecap
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSRecap 
{

    /** TableName=UNS_POSRecap */
    public static final String Table_Name = "UNS_POSRecap";

    /** AD_Table_ID=1000413 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name ReturnAmt */
    public static final String COLUMNNAME_ReturnAmt = "ReturnAmt";

	/** Set Return Amount	  */
	public void setReturnAmt (BigDecimal ReturnAmt);

	/** Get Return Amount	  */
	public BigDecimal getReturnAmt();

    /** Column name Store_ID */
    public static final String COLUMNNAME_Store_ID = "Store_ID";

	/** Set Store	  */
	public void setStore_ID (int Store_ID);

	/** Get Store	  */
	public int getStore_ID();

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException;

    /** Column name TotalCostAmt */
    public static final String COLUMNNAME_TotalCostAmt = "TotalCostAmt";

	/** Set Total Cost Amount	  */
	public void setTotalCostAmt (BigDecimal TotalCostAmt);

	/** Get Total Cost Amount	  */
	public BigDecimal getTotalCostAmt();

    /** Column name TotalDiscAmt */
    public static final String COLUMNNAME_TotalDiscAmt = "TotalDiscAmt";

	/** Set Total Discount Amount	  */
	public void setTotalDiscAmt (BigDecimal TotalDiscAmt);

	/** Get Total Discount Amount	  */
	public BigDecimal getTotalDiscAmt();

    /** Column name TotalEstimationShortCashierAmt */
    public static final String COLUMNNAME_TotalEstimationShortCashierAmt = "TotalEstimationShortCashierAmt";

	/** Set Total Estimation Short Cashier Amount	  */
	public void setTotalEstimationShortCashierAmt (BigDecimal TotalEstimationShortCashierAmt);

	/** Get Total Estimation Short Cashier Amount	  */
	public BigDecimal getTotalEstimationShortCashierAmt();

    /** Column name TotalRefundTaxAmt */
    public static final String COLUMNNAME_TotalRefundTaxAmt = "TotalRefundTaxAmt";

	/** Set Total Refund Tax Amount	  */
	public void setTotalRefundTaxAmt (BigDecimal TotalRefundTaxAmt);

	/** Get Total Refund Tax Amount	  */
	public BigDecimal getTotalRefundTaxAmt();

    /** Column name TotalRevenueAmt */
    public static final String COLUMNNAME_TotalRevenueAmt = "TotalRevenueAmt";

	/** Set Total Revenue Amount	  */
	public void setTotalRevenueAmt (BigDecimal TotalRevenueAmt);

	/** Get Total Revenue Amount	  */
	public BigDecimal getTotalRevenueAmt();

    /** Column name TotalRoundingAmt */
    public static final String COLUMNNAME_TotalRoundingAmt = "TotalRoundingAmt";

	/** Set Total Rounding Amount	  */
	public void setTotalRoundingAmt (BigDecimal TotalRoundingAmt);

	/** Get Total Rounding Amount	  */
	public BigDecimal getTotalRoundingAmt();

    /** Column name TotalSalesAmt */
    public static final String COLUMNNAME_TotalSalesAmt = "TotalSalesAmt";

	/** Set Total Sales Amount	  */
	public void setTotalSalesAmt (BigDecimal TotalSalesAmt);

	/** Get Total Sales Amount	  */
	public BigDecimal getTotalSalesAmt();

    /** Column name TotalServiceChargeAmt */
    public static final String COLUMNNAME_TotalServiceChargeAmt = "TotalServiceChargeAmt";

	/** Set Total Service Charge Amount	  */
	public void setTotalServiceChargeAmt (BigDecimal TotalServiceChargeAmt);

	/** Get Total Service Charge Amount	  */
	public BigDecimal getTotalServiceChargeAmt();

    /** Column name TotalShortCashierAmt */
    public static final String COLUMNNAME_TotalShortCashierAmt = "TotalShortCashierAmt";

	/** Set Total Short Cashier Amount (+)	  */
	public void setTotalShortCashierAmt (BigDecimal TotalShortCashierAmt);

	/** Get Total Short Cashier Amount (+)	  */
	public BigDecimal getTotalShortCashierAmt();

    /** Column name TotalShortCashierAmt1 */
    public static final String COLUMNNAME_TotalShortCashierAmt1 = "TotalShortCashierAmt1";

	/** Set Total Short Cashier Amount (-)	  */
	public void setTotalShortCashierAmt1 (BigDecimal TotalShortCashierAmt1);

	/** Get Total Short Cashier Amount (-)	  */
	public BigDecimal getTotalShortCashierAmt1();

    /** Column name TotalTaxAmt */
    public static final String COLUMNNAME_TotalTaxAmt = "TotalTaxAmt";

	/** Set Total Tax Amount	  */
	public void setTotalTaxAmt (BigDecimal TotalTaxAmt);

	/** Get Total Tax Amount	  */
	public BigDecimal getTotalTaxAmt();

    /** Column name TotalVoucherAmt */
    public static final String COLUMNNAME_TotalVoucherAmt = "TotalVoucherAmt";

	/** Set Total Voucher Amount	  */
	public void setTotalVoucherAmt (BigDecimal TotalVoucherAmt);

	/** Get Total Voucher Amount	  */
	public BigDecimal getTotalVoucherAmt();

    /** Column name UNS_POSRecap_ID */
    public static final String COLUMNNAME_UNS_POSRecap_ID = "UNS_POSRecap_ID";

	/** Set POS Recapitulation	  */
	public void setUNS_POSRecap_ID (int UNS_POSRecap_ID);

	/** Get POS Recapitulation	  */
	public int getUNS_POSRecap_ID();

    /** Column name UNS_POSRecap_UU */
    public static final String COLUMNNAME_UNS_POSRecap_UU = "UNS_POSRecap_UU";

	/** Set UNS_POSRecap_UU	  */
	public void setUNS_POSRecap_UU (String UNS_POSRecap_UU);

	/** Get UNS_POSRecap_UU	  */
	public String getUNS_POSRecap_UU();

    /** Column name UNS_SalesReconciliation_ID */
    public static final String COLUMNNAME_UNS_SalesReconciliation_ID = "UNS_SalesReconciliation_ID";

	/** Set Sales Reconciliation	  */
	public void setUNS_SalesReconciliation_ID (int UNS_SalesReconciliation_ID);

	/** Get Sales Reconciliation	  */
	public int getUNS_SalesReconciliation_ID();

	public I_UNS_SalesReconciliation getUNS_SalesReconciliation() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
