/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_StoreConfiguration
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_StoreConfiguration extends PO implements I_UNS_StoreConfiguration, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180921L;

    /** Standard Constructor */
    public X_UNS_StoreConfiguration (Properties ctx, int UNS_StoreConfiguration_ID, String trxName)
    {
      super (ctx, UNS_StoreConfiguration_ID, trxName);
      /** if (UNS_StoreConfiguration_ID == 0)
        {
			setDefaultPassengerType (null);
// D
			setIsPB1Printed (false);
// N
			setIsPB1Tax (false);
// N
			setIsServChgPrinted (false);
// N
			setIsServiceCharge (false);
// N
			setServiceCharge (Env.ZERO);
// 0
			setUNS_StoreConfiguration_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_StoreConfiguration (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_StoreConfiguration[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException
    {
		return (org.compiere.model.I_C_Tax)MTable.get(getCtx(), org.compiere.model.I_C_Tax.Table_Name)
			.getPO(getC_Tax_ID(), get_TrxName());	}

	/** Set Tax.
		@param C_Tax_ID 
		Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID)
	{
		if (C_Tax_ID < 1) 
			set_Value (COLUMNNAME_C_Tax_ID, null);
		else 
			set_Value (COLUMNNAME_C_Tax_ID, Integer.valueOf(C_Tax_ID));
	}

	/** Get Tax.
		@return Tax identifier
	  */
	public int getC_Tax_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Tax_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Departure = D */
	public static final String DEFAULTPASSENGERTYPE_Departure = "D";
	/** In-Transit = I */
	public static final String DEFAULTPASSENGERTYPE_In_Transit = "I";
	/** Arrival = A */
	public static final String DEFAULTPASSENGERTYPE_Arrival = "A";
	/** Set Default Passenger Type.
		@param DefaultPassengerType Default Passenger Type	  */
	public void setDefaultPassengerType (String DefaultPassengerType)
	{

		set_Value (COLUMNNAME_DefaultPassengerType, DefaultPassengerType);
	}

	/** Get Default Passenger Type.
		@return Default Passenger Type	  */
	public String getDefaultPassengerType () 
	{
		return (String)get_Value(COLUMNNAME_DefaultPassengerType);
	}

	/** Set PB1 Tax Printed ?.
		@param IsPB1Printed PB1 Tax Printed ?	  */
	public void setIsPB1Printed (boolean IsPB1Printed)
	{
		set_Value (COLUMNNAME_IsPB1Printed, Boolean.valueOf(IsPB1Printed));
	}

	/** Get PB1 Tax Printed ?.
		@return PB1 Tax Printed ?	  */
	public boolean isPB1Printed () 
	{
		Object oo = get_Value(COLUMNNAME_IsPB1Printed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set PB 1 Tax ?.
		@param IsPB1Tax PB 1 Tax ?	  */
	public void setIsPB1Tax (boolean IsPB1Tax)
	{
		set_Value (COLUMNNAME_IsPB1Tax, Boolean.valueOf(IsPB1Tax));
	}

	/** Get PB 1 Tax ?.
		@return PB 1 Tax ?	  */
	public boolean isPB1Tax () 
	{
		Object oo = get_Value(COLUMNNAME_IsPB1Tax);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Service Charge Printed ?.
		@param IsServChgPrinted Service Charge Printed ?	  */
	public void setIsServChgPrinted (boolean IsServChgPrinted)
	{
		set_Value (COLUMNNAME_IsServChgPrinted, Boolean.valueOf(IsServChgPrinted));
	}

	/** Get Service Charge Printed ?.
		@return Service Charge Printed ?	  */
	public boolean isServChgPrinted () 
	{
		Object oo = get_Value(COLUMNNAME_IsServChgPrinted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Service Charge ?.
		@param IsServiceCharge Service Charge ?	  */
	public void setIsServiceCharge (boolean IsServiceCharge)
	{
		set_Value (COLUMNNAME_IsServiceCharge, Boolean.valueOf(IsServiceCharge));
	}

	/** Get Service Charge ?.
		@return Service Charge ?	  */
	public boolean isServiceCharge () 
	{
		Object oo = get_Value(COLUMNNAME_IsServiceCharge);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Service Charge.
		@param ServiceCharge Service Charge	  */
	public void setServiceCharge (BigDecimal ServiceCharge)
	{
		set_Value (COLUMNNAME_ServiceCharge, ServiceCharge);
	}

	/** Get Service Charge.
		@return Service Charge	  */
	public BigDecimal getServiceCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ServiceCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Store_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Store Configuration.
		@param UNS_StoreConfiguration_ID Store Configuration	  */
	public void setUNS_StoreConfiguration_ID (int UNS_StoreConfiguration_ID)
	{
		if (UNS_StoreConfiguration_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_StoreConfiguration_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_StoreConfiguration_ID, Integer.valueOf(UNS_StoreConfiguration_ID));
	}

	/** Get Store Configuration.
		@return Store Configuration	  */
	public int getUNS_StoreConfiguration_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_StoreConfiguration_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_StoreConfiguration_UU.
		@param UNS_StoreConfiguration_UU UNS_StoreConfiguration_UU	  */
	public void setUNS_StoreConfiguration_UU (String UNS_StoreConfiguration_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_StoreConfiguration_UU, UNS_StoreConfiguration_UU);
	}

	/** Get UNS_StoreConfiguration_UU.
		@return UNS_StoreConfiguration_UU	  */
	public String getUNS_StoreConfiguration_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_StoreConfiguration_UU);
	}
}