/**
 * 
 */
package com.unicore.model.validator;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MUNSCustomerPoint;
import com.unicore.model.MUNSCustomerPointLine;
import com.unicore.model.MUNSPSProduct;
import com.unicore.model.MUNSPackingList;
import com.unicore.model.MUNSPackingListOrder;
import com.unicore.model.MUNSPointSchema;
import com.unicore.model.MUNSPointSchemaLine;

/**
 * @author ALBURHANY
 *
 */
public class UNSPointSchemaValidator implements ModelValidator {
	
	private MUNSPointSchema m_PointSchema = null;
	private MUNSPSProduct m_PSProduct = null;
	private MUNSCustomerPoint m_CustomerPoint = null;
	private MUNSCustomerPointLine m_CustomerPointLine = null;
	private MUNSPointSchemaLine m_PointSchemaLine = null;
	private boolean m_isOneShipment = false;
	private String pointType = null;
	private BigDecimal point = Env.ZERO;

	/**
	 * 
	 */
	public UNSPointSchemaValidator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		
		engine.addDocValidate(MInvoice.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		
		if(po.get_Table_ID() != MInvoice.Table_ID)
			return null;
		if(type != TYPE_BEFORE_CHANGE && type != TYPE_BEFORE_NEW)
			return null;

		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {

		if (po.get_TableName().equals(MInvoice.Table_Name) && timing == TIMING_AFTER_COMPLETE
				&& po.get_ValueAsBoolean("IsSOTrx"))
		{	
			MInvoice inv = (MInvoice) po;
			
			if(!cekPointSchema(po.getCtx(), inv, po.get_TrxName()))
				return null;
			
			if(inv.getC_DocTypeTarget().getDocBaseType().equals(MDocType.DOCBASETYPE_ARInvoice))
			{					
				int m_Revesal_ID = inv.getReversal_ID();
				if(m_Revesal_ID > 0)
					return null;
				
				for (MInvoiceLine invL : inv.getLines(true))
				{
					String whereClausePSProduct = "(M_Product_ID = " + invL.getM_Product_ID()
							+ " OR M_Product_Category_ID = " + invL.getM_Product().getM_Product_Category_ID() + ")";
					
					String orderClausePSProduct = "M_Product_ID";
					m_PSProduct = m_PointSchema.getLine(whereClausePSProduct, orderClausePSProduct);
	
					if(m_PSProduct == null)
						continue;
					else
						point = calculated(po.getCtx(), m_isOneShipment,
									m_PSProduct.get_ID(), invL.getQtyInvoiced(), po.get_TrxName());
				
					m_CustomerPoint = MUNSCustomerPoint.getByBPartner(po.getCtx(), inv.getC_BPartner_ID(),
							inv.getAD_Org_ID(), po.get_TrxName());
					
					if (m_CustomerPoint == null)
					{
						m_CustomerPoint = new MUNSCustomerPoint(inv);
					}
					
					m_CustomerPoint.setLatestTrxDate(inv.getDateInvoiced());
					m_CustomerPoint.saveEx();
					
					createCPLine(po.getCtx(), m_CustomerPoint, invL, po.get_TrxName());
				}
			}
			
			if(inv.getC_DocTypeTarget().getDocBaseType().equals(MDocType.DOCBASETYPE_ARCreditMemo))
			{									
				for(MInvoiceLine invLine : inv.getLines(true))
				{
					String whereClausePSProduct = "(M_Product_ID = " + invLine.getM_Product_ID()
							+ " OR M_Product_Category_ID = " + invLine.getM_Product().getM_Product_Category_ID() + ")";
					
					String orderClausePSProduct = "M_Product_ID";
					m_PSProduct = m_PointSchema.getLine(whereClausePSProduct, orderClausePSProduct);
	
					if(m_PSProduct == null)
						continue;
					else
						point = (calculated(po.getCtx(), m_isOneShipment,
									m_PSProduct.get_ID(), invLine.getQtyInvoiced(), po.get_TrxName())).negate();
					
					m_CustomerPoint = MUNSCustomerPoint.getByBPartner(po.getCtx(), inv.getC_BPartner_ID(),
							inv.getAD_Org_ID(), po.get_TrxName());
					
					if (m_CustomerPoint == null)
					{
						m_CustomerPoint = new MUNSCustomerPoint(inv);
					}
					
					m_CustomerPoint.setLatestTrxDate(inv.getDateInvoiced());
					m_CustomerPoint.saveEx();
					
//					m_CustomerPointLine = MUNSCustomerPointLine.getByInvoiceLine(po.getCtx(), invLine.get_ID(),
//							po.get_TrxName());
					
					createCPLine(po.getCtx(), m_CustomerPoint, invLine, po.get_TrxName());
				}
			}
		}

		if (po.get_TableName().equals(MInvoice.Table_Name) && po.get_ValueAsBoolean("IsSOTrx"))
		{
			if(timing == TIMING_BEFORE_REVERSEACCRUAL || timing == TIMING_BEFORE_VOID || timing == TIMING_BEFORE_REVERSECORRECT)
			{
				StringBuffer sql = new StringBuffer
						("DELETE FROM UNS_CustomerPoint_Line WHERE C_InvoiceLine_ID IN"
								+ " (SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_Invoice_ID = " + po.get_ID() + ")");
				DB.executeUpdate(sql.toString(), po.get_TrxName());
			}
		}
		return null;
	}
	
	public boolean cekPointSchema(Properties ctx, MInvoice inv, String trxName)
	{
		m_PointSchema = null;
	
		MUNSPackingListOrder plOrder = MUNSPackingListOrder.getByInvoice(ctx, inv.get_ID(), trxName);
		
		int idPL = 0;
		
		if(plOrder != null)
		{
			m_isOneShipment = plOrder.isOneShipment();
			idPL = plOrder.getUNS_PackingList_ID();				
		}
		if(plOrder == null)
		{
			if(inv.getC_Order_ID() <= 0)
			{
				m_isOneShipment = false;
				idPL = 0;
			}
			else
			{
				m_isOneShipment = inv.getC_Order().isOneShipment();
				idPL = 0;
			}
		}
		
		if(idPL > 0)
		{
			MUNSPackingList pl = new MUNSPackingList(ctx, idPL, trxName);
			pointType = pl.getLines(null, null).length == 1 ? "OSOC" : "OSMC";
		}
		else
			pointType = "OSMC";
		
		m_PointSchema = MUNSPointSchema.get(ctx, inv.getC_BPartner_ID(), pointType, trxName);
		
		if(m_PointSchema.get_ID() <= 0)
			return false;
		
		return true;
	}
	
	public BigDecimal calculated (Properties ctx,
			boolean isOneShipment, int UNS_PS_Product_ID, BigDecimal QtyInv, String trxName)
	{
		BigDecimal point = Env.ZERO;
		BigDecimal Quantity = Env.ZERO;
		BigDecimal getpoint = Env.ZERO;
		BigDecimal pointProposional = Env.ZERO;
		Quantity = QtyInv;
		
		m_PSProduct = new MUNSPSProduct(ctx, UNS_PS_Product_ID, trxName);
		int length = m_PSProduct.getLine().length;
		
		for (int i = 0; i < m_PSProduct.getLine().length;)
		{
			m_PointSchemaLine = new MUNSPointSchemaLine(ctx, m_PSProduct.getLine()[i].get_ID(), trxName);
			
			if(Quantity.compareTo(m_PointSchemaLine.getQty()) == 1)
			{
				point = Quantity.divide(m_PointSchemaLine.getQty(), 0, BigDecimal.ROUND_FLOOR);
				getpoint = getpoint.add((point.multiply(m_PointSchemaLine.getValuePoint())));
				Quantity = Quantity.subtract(point.multiply(m_PointSchemaLine.getQty()));
				
				if(isOneShipment && Quantity.compareTo(Env.ZERO) == 1)
				{
					pointProposional = m_PointSchemaLine.getValuePoint().divide(m_PointSchemaLine.getQty());
					getpoint =  getpoint.add(Quantity.multiply(pointProposional));
					break;
				}
			}
			if(Quantity.compareTo(m_PointSchemaLine.getQty()) == 0)
			{
				getpoint = getpoint.add(m_PointSchemaLine.getValuePoint());
				break;
			}
			if(Quantity.compareTo(m_PointSchemaLine.getQty()) == -1
					&& Quantity.compareTo(Env.ZERO) == 1)
			{
				if(length - i == 1)
				{
					if(m_PSProduct.isCalcRemainingQty())
					{
						pointProposional = m_PointSchemaLine.getValuePoint().divide(m_PointSchemaLine.getQty());
						getpoint = getpoint.add(Quantity).multiply(pointProposional);
					}
				}
			}
			i++;
		}
		return getpoint;
	}
	
	public boolean createCPLine(Properties ctx, MUNSCustomerPoint cPoint,
			MInvoiceLine invLine, String trxName)
	{
		m_CustomerPointLine = MUNSCustomerPointLine.getByInvoiceLine(ctx, invLine.get_ID(), trxName);
		
		if(m_CustomerPointLine == null)
		{
			m_CustomerPointLine = new MUNSCustomerPointLine(ctx, 0, trxName);
			m_CustomerPointLine.setUNS_CustomerPoint_ID(cPoint.get_ID());
			m_CustomerPointLine.setPoint(point);
			m_CustomerPointLine.setUNS_PointSchema_ID(m_PointSchema.get_ID());
			m_CustomerPointLine.setM_Product_ID(invLine.getM_Product_ID());
			m_CustomerPointLine.setQty(invLine.getQtyInvoiced());
			m_CustomerPointLine.setDateInvoiced(invLine.getC_Invoice().getDateInvoiced());
			m_CustomerPointLine.setC_InvoiceLine_ID(invLine.getC_InvoiceLine_ID());
			m_CustomerPointLine.saveEx();
		}
		else
		{
			m_CustomerPointLine.setM_Product_ID(invLine.getM_Product_ID());
			m_CustomerPointLine.setQty(invLine.getQtyInvoiced());
			m_CustomerPointLine.setPoint(point);
			m_CustomerPointLine.saveEx();
		}
		
		return true;
	}
	
}