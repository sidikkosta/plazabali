/**
 * 
 */
package com.unicore.model.validator;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MBPartnerProduct;
import org.compiere.model.MClient;
import org.compiere.model.MCost;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRequisition;
import org.compiere.model.MRequisitionLine;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.X_C_InvoiceLine;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_C_OrderLine;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.wf.MWorkflow;

import com.unicore.model.MUNSAccessoriesLine;
import com.unicore.model.MUNSCreditAgreement;
import com.unicore.model.MUNSDiscountTrx;
import com.unicore.model.MUNSHandleRequests;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPOSTrxLine;
import com.unicore.model.MUNSOrderValidationConf;
import com.unicore.model.MUNSPackingList;
import com.unicore.model.MUNSPackingListOrder;
import com.unicore.model.MUNSPreOrder;
import com.unicore.model.MUNSRequisitionLineMA;
import com.unicore.model.MUNSStorageReservation;
import com.unicore.model.MUNSVoucherBook;
import com.unicore.model.MUNSVoucherCode;
import com.uns.base.model.Query;
import com.uns.util.MessageBox;

/**
 * @author root
 * copy right 2015 RumahKijang@UntaSoft
 */
public class UNSOrderValidator implements ModelValidator {

	/**
	 * 
	 */
	public UNSOrderValidator() {
		super();
	}
	
	int m_AD_Client_ID = 0;
	CLogger log = CLogger.getCLogger(UNSOrderValidator.class);

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#initialize(org.compiere.model.ModelValidationEngine, org.compiere.model.MClient)
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		
		if(client != null)
		{
			m_AD_Client_ID = client.getAD_Client_ID();
			log.log(Level.INFO, client.toString());
		}
		else
		{
			log.log(Level.INFO, "Initializing global validator -" + this.toString());
		}
		
		engine.addDocValidate(MOrder.Table_Name, this);
		engine.addDocValidate(MInvoice.Table_Name, this);
		engine.addDocValidate(MInOut.Table_Name, this);
		engine.addModelChange(MOrder.Table_Name, this);
		engine.addModelChange(MOrderLine.Table_Name, this);
		engine.addModelChange(MInvoice.Table_Name, this);
		engine.addModelChange(MInvoiceLine.Table_Name, this);
		engine.addModelChange(MPaymentAllocate.Table_Name, this);
		engine.addDocValidate(MPayment.Table_Name, this);
		engine.addDocValidate(MUNSPackingList.Table_Name, this);
		engine.addModelChange(MRequisitionLine.Table_Name, this);
		engine.addModelChange(com.unicore.base.model.MRequisition.Table_Name, this);
		engine.addDocValidate(MUNSPOSTrx.Table_Name, this);
		engine.addDocValidate(MMovement.Table_Name, this);
		engine.addDocValidate(MRequisition.Table_Name, this);
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#getAD_Client_ID()
	 */
	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#login(int, int, int)
	 */
	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		return null;
	}
	
	
	private String orderLine (PO po) 
	{
		MOrderLine oLine = new MOrderLine(po.getCtx(), po.get_ID(), po.get_TrxName());
		if(!oLine.getC_Order().isSOTrx())
		{
			MBPartnerProduct bpProduct = MBPartnerProduct.get(oLine.getM_Product_ID(),
					oLine.getC_Order().getC_BPartner_ID(), po.get_TrxName());
			
			if(bpProduct != null)
			{
				String sql = "SELECT M_Product_ID FROM M_Product WHERE Value = '" + bpProduct.getVendorProductNo() + "'";
				MProduct product = new MProduct(po.getCtx(), DB.getSQLValue(po.get_TrxName(),
										sql.toString()), po.get_TrxName());
				oLine.setC_BPartner_Product_Key_ID(product.get_ID());
			}
			if(bpProduct == null)
				oLine.setC_BPartner_Product_Key_ID(oLine.getM_Product_ID());
		}
		oLine.saveEx();
		
		return null;
	}
	/**
	 * 
	 * @param po
	 * @return
	 */
	private String invoiceLineOnCreate(PO po, int type)
	{
		MInvoiceLine doc = (MInvoiceLine) po;
		if(doc.getC_OrderLine_ID() <= 0 || doc.isReversal())
			return null;
		
		MOrderLine oLine = new MOrderLine(doc.getCtx(), doc.getC_OrderLine_ID(), doc.get_TrxName());
		
//		String sql = "SELECT MAX(UNS_SOCOnfirmation_ID) FROM UNS_SOConfirmation "
//				+ " WHERE C_Order_ID = ? AND ConfirmationType = ?";
//		int value = DB.getSQLValue(po.get_TrxName(), sql, doc.getC_OrderLine().getC_Order_ID()
//				, MUNSSOConfirmation.CONFIRMATIONTYPE_OrderToInvoice);
//		if(value <= 0)
//		{
//			if(oLine.getQtyEntered().compareTo(doc.getQtyEntered()) != 0)
//			{
//				doc.setDiscount(Env.ZERO);
//				doc.setDiscountAmt(Env.ZERO);
//				doc.setQtyBonuses(Env.ZERO);
//				doc.setPrice(doc.getPriceList());
//				return null;
//			}
			
			if(type == TYPE_AFTER_NEW)
			{
				copyDiscount(oLine, doc);
			}
			else if(type == TYPE_BEFORE_NEW)
			{
				doc.setQtyBonuses(oLine.getQtyBonuses());
			}
//		}
//		else
//		{
//			MUNSSOConfirmation soConfirmation = new MUNSSOConfirmation(po.getCtx(), value, po.get_TrxName());
//			if(null == soConfirmation.getConfirmChanges())
//			{
//				return "Not completed SO Confirmation... Please open So Confirmation of Order " 
//						+ oLine.getC_Order().getDocumentNo();
//			}
//			if(soConfirmation.getConfirmChanges().equals(MUNSSOConfirmation.CONFIRMCHANGES_Approve))
//			{
//				if(oLine.getQtyEntered().compareTo(doc.getQtyEntered()) != 0)
//				{
//					doc.setDiscount(Env.ZERO);
//					doc.setDiscountAmt(Env.ZERO);
//					doc.setQtyBonuses(Env.ZERO);
//					doc.setPrice(doc.getPriceList());
//					
//					return null;
//				}
//
//				if(type == TYPE_AFTER_NEW)
//				{
//					copyDiscount(oLine, doc);
//				}
//				else if(type == TYPE_BEFORE_NEW)
//				{
//					doc.setQtyBonuses(oLine.getQtyBonuses());
//					doc.setDiscount(oLine.getDiscount());
//					doc.setDiscountAmt(oLine.getDiscountAmt());
//				}
//			}
//			else if(soConfirmation.getConfirmChanges().equals(MUNSSOConfirmation.CONFIRMCHANGES_Recalculate)
//					&& type == TYPE_BEFORE_NEW)
//			{
//				MUNSSOConfirmationLine line = soConfirmation.getLineOrder(doc.getC_OrderLine_ID());
//				if(line == null)
//				{
//					return null;
//				}
//				
//				doc.setDiscount(Env.ZERO);
//				doc.setDiscountAmt(Env.ZERO);
//				doc.setQtyBonuses(Env.ZERO);
//				doc.setPrice();
//			}
//			else if(soConfirmation.getConfirmChanges().equals(MUNSSOConfirmation.CONFIRMCHANGES_Customize)
//					&& type == TYPE_BEFORE_NEW)
//			{
//				MUNSSOConfirmationLine line = soConfirmation.getLineOrder(doc.getC_OrderLine_ID());
//				if(line == null)
//					return null;
//				
//				doc.setDiscount(Env.ZERO);
//				doc.setDiscountAmt(Env.ZERO);
//				doc.setQtyBonuses(Env.ZERO);
//				doc.setPrice(line.getPriceActual());
//				doc.setPriceList(line.getPriceList());
////			}
//		}		
		return null;
	}
	
	/**
	 * Validate order on Change
	 * @param po
	 */
	private String orderOnChange(PO po)
	{
		MOrder order = (MOrder) po;
		Timestamp datePromised = order.getDatePromised();
		Timestamp dateOrdered = order.getDateOrdered();
		
		if(datePromised == null || datePromised.before(dateOrdered))
		{
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(((Timestamp) po.get_Value(MOrder.COLUMNNAME_DateOrdered)).getTime());
			cal.add(Calendar.DATE, 2);
			datePromised = new Timestamp(cal.getTimeInMillis());
			order.setDatePromised(datePromised); 	
		}
		return null;
	}
	
	/**
	 * Get Lines of PO. 
	 * @param po
	 * @return {@link PO}[]
	 */
	private PO[] getLines(PO po)
	{
		PO[] pos = null;
		if(po instanceof MInvoice)
		{
			MInvoice invoice =  (MInvoice) po;
			pos =  invoice.getLines();
		}
		else if(po instanceof MOrder)
		{
			MOrder order = (MOrder) po;
			pos = order.getLines();
		}
		else if(po instanceof MInOut)
		{
			MInOut inout = (MInOut) po;
			pos = inout.getLines();
		}
		else if (po instanceof MUNSPOSTrx)
		{
			MUNSPOSTrx trx = (MUNSPOSTrx) po;
			pos = trx.getLines(false);
		}
		else
		{
			pos = new PO[0];
		}
		
		return pos;
	}
	
	/**
	 * 
	 * @param order
	 * @param date
	 * @return
	 *
	private String checkValidPrice(PO po, Timestamp date)
	{
		StringBuilder msg = new StringBuilder();
		PO[] lines = getLines(po);
		for(int i=0; i<lines.length; i++)
		{
			MProductPricing pp = getProductPricing(
					po.get_ValueAsInt("C_BPartner_ID"), po.get_ValueAsInt("C_BPartner_Location_ID")
					, (BigDecimal)lines[i].get_Value("QtyEntered"), po.get_ValueAsBoolean("IsSOTrx")
					, po.get_ValueAsInt("M_PriceList_ID"), lines[i].get_ValueAsInt("M_Product_ID")
					, date, po.get_TrxName());
			
			boolean condition1 = ((BigDecimal) lines[i].get_Value("PriceList")).compareTo(pp.getPriceList()) == 0;
			
			if(!condition1)
			{
				msg.append("Price List has ben diferent with Price List in Price List on line " 
							+ lines[i].get_ValueAsInt("Line"));
			}
		}
		
		return msg.toString();
	}
	*/
	/**
	 * 
	 * @param po
	 * @return
	 */
	private String invoiceOnChange(PO po)
	{
		MInvoice doc = (MInvoice) po;
		if (doc.isReversal())
			return null;
		if(!doc.is_ValueChanged("C_Order_ID"))
			return null;
		if(!doc.isSOTrx())
			return null;
		
		//TODO penambahan additional discount di invoice ketika 1 invoice lebih dari 1 order dan mungkin partial line. (tidak seluruh line dari order di includ kan dalam 1 invoice)
//		Integer oldOrder = doc.get_ValueOldAsInt("C_Order_ID");
//		if(oldOrder > 0 && oldOrder != doc.getC_Order_ID())
//			return "Not Updatable C_Order";
		
		if(doc.getC_Order_ID() <= 0)
			return null;
		
		MOrder order = new MOrder(doc.getCtx(), doc.getC_Order_ID(), doc.get_TrxName());
		
		if (doc.is_ValueChanged(MInvoice.COLUMNNAME_C_Order_ID)
				&& order.getDiscountAmt().signum() == 1)
		{
			doc.setAddDiscountAmt(order.getDiscountAmt());
		}
		
//		if(order.isLockPricing())
//		{
//			boolean confirmOK = confirmOK(order, doc.getDateInvoiced());
//			if(confirmOK)
//				return null;
//		}
//		
//		String validPrice = checkValidPrice(order, doc.getDateInvoiced());
//		String validDiscount = checkDiscount(order, doc.getDateInvoiced());
		String retVal = null;
		
//		if(!Util.isEmpty(validPrice, true) || !Util.isEmpty(validDiscount, true))
//			retVal = validPrice + validDiscount;
		
//		if(retVal != null && !confirmOK(order, doc.getDateInvoiced()))
//		{
//			createConfirmation(
//					order, doc.getDateInvoiced(), retVal
//					,MUNSSOConfirmation.CONFIRMATIONTYPE_OrderToInvoice);
//		}
//		else
//		{
//			String sql = "SELECT MAX(UNS_SOCOnfirmation_ID) FROM UNS_SOConfirmation "
//					+ " WHERE C_Order_ID = ? AND ConfirmationType = ?";
//			int value = DB.getSQLValue(po.get_TrxName(), sql, order.get_ID()
//					, MUNSSOConfirmation.CONFIRMATIONTYPE_OrderToInvoice);
//			if(value <= 0)
//			{
//				copyDiscount(order, doc);
//			}
//			retVal = null;
//		}
		
		String toString = retVal; //20150226 jika langsung return retval maka akan menjadi null. makanya diginiin. kondisi pakai java 1.7 eclipse kepler idempiere3.0
		return toString;
	}
	
	/**
	 * Validate invoice on create
	 * @param po
	 */
	private String invoiceOnCreate(PO po)
	{
		MInvoice invoice = (MInvoice) po;
		if (invoice.isReversal())
			return null;
		if(invoice.getC_Order_ID() <= 0)
			return null;
		if(!invoice.isSOTrx())
			return null;
		
		MOrder order = new MOrder(invoice.getCtx(), invoice.getC_Order_ID(), 
				invoice.get_TrxName());
		if (order.getDiscountAmt().signum() == 1)
		{
			invoice.setAddDiscountAmt(order.getDiscountAmt());
		}
//		if(order.isLockPricing())
//		{
//			boolean confirmOK = confirmOK(order, invoice.getDateInvoiced());
//			if(confirmOK)
//				return null;
//		}
		
		String retval = null;
//		String invalidMsg = checkValidPrice(order, invoice.getDateInvoiced());
//		String validateDiscountMsg = checkDiscount(order, invoice.getDateInvoiced());

//		if((!Util.isEmpty(invalidMsg, true) || !Util.isEmpty(validateDiscountMsg, true)))
//			retval = invalidMsg + validateDiscountMsg;
//		
//		if(retval != null && !confirmOK(order, invoice.getDateInvoiced()))
//		{
//			createConfirmation(order, invoice.getDateInvoiced(), invalidMsg.concat(validateDiscountMsg)
//					, MUNSSOConfirmation.CONFIRMATIONTYPE_OrderToInvoice);
//			if(order.is_ValueChanged(MOrder.COLUMNNAME_DatePromised))
//				invoice.setDateInvoiced(order.getDatePromised());
//		}
//		else
//		{
//			String sql = "SELECT MAX(UNS_SOCOnfirmation_ID) FROM UNS_SOConfirmation "
//					+ " WHERE C_Order_ID = ? AND ConfirmationType = ?";
//			int value = DB.getSQLValue(po.get_TrxName(), sql, order.get_ID()
//					, MUNSSOConfirmation.CONFIRMATIONTYPE_OrderToInvoice);
//			if(value <= 0)
//			{
//				copyDiscount(order, invoice);
//			}
//			retval = null;
//		}
		return retval;
	}
	
	/**
	 * 
	 * @param C_Order_ID
	 * @param trxName
	 * @return
	 *
	private boolean confirmOK(PO po, Timestamp date)
	{
		MUNSSOConfirmation[] confirmation = MUNSSOConfirmation.get(po, po.get_TrxName());
		int confirmCount = confirmation.length;
		int invalidCount = 0;
		Timestamp overwritenDate = null;
		
		for(MUNSSOConfirmation confirm : confirmation)
		{
			if(!confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_Completed)
					&& !confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_Closed)
					&& Env.getContextAsInt(Env.getCtx(), MUNSSOConfirmation.IS_EVENT_FROM_CONFIRMATION) <= 0)
			{
				invalidCount++;
				continue;
			}
			Timestamp validDate = confirm.getValidTo();
			
			if(null == validDate)
			{
				return true;
			}
			
			boolean notValid = date.after(validDate);
			if(notValid)
			{
				invalidCount++;
				continue;
			}
			
			if(confirm.getConfirmChanges().equals(MUNSSOConfirmation.CONFIRMCHANGES_Void))
			{
				invalidCount++;
				continue;
			}
			
			if(confirm.getOverwriteDate() != null)
				overwritenDate = confirm.getOverwriteDate();
		}
		
		if(confirmCount > 0 && invalidCount < confirmCount)
		{
			if(overwritenDate != null)
			{
				if(po instanceof MOrder)
				{
					MOrder order = (MOrder) po;
					order.setDatePromised(overwritenDate);
				}
				else if(po instanceof MInvoice)
				{
					MInvoice invoice = (MInvoice) po;
					invoice.setDateInvoiced(overwritenDate);
				}
			}
			return true;
		}
		
		return false;
	}
	*/

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#modelChange(org.compiere.model.PO, int)
	 */
	@Override
	public String modelChange(PO po, int type) throws Exception {
		String msg = null;
		boolean newRecord = type == TYPE_AFTER_NEW ? true : false;
		if(po.get_TableName().equals(MOrder.Table_Name))
		{
			if(type != TYPE_BEFORE_CHANGE && type != TYPE_BEFORE_NEW)
				return null;
			
			msg =  orderOnChange(po);
		}
		if(po.get_TableName().equals(MOrderLine.Table_Name))
		{
			if(type == TYPE_AFTER_CHANGE)
			{
				msg = orderLine(po);
			}
			
			if(type == TYPE_AFTER_NEW || type == TYPE_AFTER_CHANGE)
			{
				msg = productAccessories(po, newRecord);
			}
			if(type == TYPE_BEFORE_DELETE)
			{
				msg = delHandleRequest(po, true, false);
			}
			if(type == TYPE_BEFORE_CHANGE)
			{
				msg = delHandleRequest(po, false, false);
				if(msg != null)
				{
					msg = validateOrderLine(po);
				}
			}
		}
		else if(po.get_TableName().equals(MInvoice.Table_Name))
		{
			MInvoice invoice = (MInvoice) po;
			if (invoice.isReversal())
			{
				return msg;
			}

			if(type == TYPE_BEFORE_NEW)
				msg = invoiceOnCreate(po);
			else if(type == TYPE_BEFORE_CHANGE)
				msg = invoiceOnChange(po);
		}
		else if(po.get_TableName().equals(MInvoiceLine.Table_Name))
		{
			if (type == TYPE_AFTER_CHANGE)
			{
				String sql = "SELECT DocumentNo FROM C_Invoice WHERE "
						+ " C_Invoice_ID IN (SELECT C_Invoice_ID FROM "
						+ " C_InvoiceLine WHERE M_InoutLine_ID = ? ) "
						+ " AND DocStatus NOT IN ('VO','RE')";
				String result = DB.getSQLValueString(po.get_TrxName(), sql, 
						po.get_ValueAsInt("M_InOutLine_ID"));
				if (!Util.isEmpty(result, true))
				{
					msg = "Invoice already exists, invoice doc no " + result;
				}
			}
			
			if(type == TYPE_AFTER_NEW || type == TYPE_AFTER_CHANGE)
			{
				msg = productAccessories(po, newRecord);
			}

			MInvoiceLine invLine = (MInvoiceLine) po;
			if (invLine.getRef_InvoiceLine_ID() > 0)
				return msg;
			if(type == TYPE_AFTER_NEW)
				msg = invoiceLineOnCreate(po, type);
			else if(type == TYPE_BEFORE_NEW)
				msg = invoiceLineOnCreate(po, type);
			else if(type == TYPE_BEFORE_CHANGE)
				msg = invoiceLineOnChange(po);
			else if(type == TYPE_BEFORE_DELETE)
			{
				String sql = "UPDATE UNS_WeighbridgeTicket SET UnLoadInvoiceLine_ID = null WHERE UnLoadInvoiceLine_ID = ?";
				DB.executeUpdate(sql, new Object[]{invLine.get_ID()}, false, invLine.get_TrxName());
				
				String del = "DELETE FROM UNS_AccessoriesLine WHERE C_InvoiceLine_ID=?";
				DB.executeUpdate(del,new Object[]{invLine.get_ID()}, false, po.get_TrxName());
			}
		}
		else if(po.get_TableName().equals(MPaymentAllocate.Table_Name))
		{
			if(type != TYPE_AFTER_CHANGE && type != TYPE_AFTER_NEW)
				return null;
			
			if(type == TYPE_AFTER_CHANGE && !po.is_ValueChanged("C_Invoice_ID"))
				return null;
			
			MPaymentAllocate allocate = (MPaymentAllocate) po;
			if(allocate.getC_Invoice_ID() <= 0)
				return null;
			
			MInvoice invoice = new MInvoice(po.getCtx(), allocate.getC_Invoice_ID(), po.get_TrxName());
			String c_invoices_id = DB.getSQLValueString(
					po.get_TrxName(), "SELECT ARRAY_TO_STRING(ARRAY_AGG(C_Invoice_ID), ';') FROM C_Invoice WHERE CN_Invoice_ID = ?"
							+ " AND DocStatus IN ('CO', 'CL')", invoice.getC_Invoice_ID());
			
			if(null == c_invoices_id || c_invoices_id.length() == 0)
				return null;
			String[] c_invStrings = c_invoices_id.split(";");
			if(c_invStrings == null || c_invStrings.length == 0)
				return null;
			BigDecimal overUnderPayment = Env.ZERO;
			for(int i=0; i<c_invStrings.length; i++)
			{
				if(overUnderPayment.compareTo(Env.ZERO) == 0 && i > 0)
					break;
				Integer c_invoice_id = new Integer(c_invStrings[i]);
				String sql = "SELECT 1 FROM C_PaymentAllocate WHERE C_Payment_ID = ? AND C_Invoice_ID = ?";
				boolean exists = DB.getSQLValue(po.get_TrxName(), sql, allocate.getC_Payment_ID(), c_invoice_id) > 0;
				if(exists)
					continue;
				sql = "SELECT invoiceopen(?, 0)";
				BigDecimal amt = Env.ZERO;
				amt = DB.getSQLValueBD(po.get_TrxName(), sql, c_invoice_id).negate();
				boolean paid = amt.compareTo(Env.ZERO) == 0 ? true : false;
				if(paid)
					continue;
				MInvoice cnInvoice = new MInvoice(po.getCtx(), c_invoice_id, po.get_TrxName());
				overUnderPayment = allocate.getPayToOverUnderAmount().subtract(amt);
				if(overUnderPayment.compareTo(Env.ZERO) == -1)
					amt = allocate.getPayToOverUnderAmount();
				else
					overUnderPayment = Env.ZERO;
				
				MPaymentAllocate cnAllocate = new MPaymentAllocate(po.getCtx(), 0, po.get_TrxName());
				cnAllocate.setAD_Org_ID(allocate.getAD_Org_ID());
				cnAllocate.setC_Payment_ID(allocate.getC_Payment_ID());
				cnAllocate.setC_Invoice_ID(cnInvoice.get_ID());
				cnAllocate.setAmount(amt.negate());
				cnAllocate.setOverUnderAmt(overUnderPayment);
				cnAllocate.setPayToOverUnderAmount(amt.negate());
				cnAllocate.setInvoiceAmt(DB.getSQLValueBD(po.get_TrxName(), "SELECT invoiceopen(?, 0)", c_invoice_id));
				
				try
				{
					cnAllocate.saveEx();
				}
				catch (Exception e)
				{
					msg = e.getLocalizedMessage();
				}
			}
		}
//		else if(MRequisitionLine.Table_ID == po.get_Table_ID()
//				&& type == TYPE_BEFORE_NEW)
//		{
//			upQtyOnHand(po);
//		}
		
		return msg;
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#docValidate(org.compiere.model.PO, int)
	 */
	@Override
	public String docValidate(PO po, int timing) {
		if (po.get_TableName().equals(MUNSPOSTrx.Table_Name))
		{
			if (timing == TIMING_AFTER_COMPLETE)
				updateDiscountBudget(po, false);
		}
		if(po.get_TableName().equals(MOrder.Table_Name))
		{
			if(timing == TIMING_BEFORE_REACTIVATE)
			{
				String sqql = "SELECT C_DocType_ID FROM C_DocType WHERE DocSubTypeSO = ? ";
				int doctype = DB.getSQLValue(po.get_TrxName(), sqql, po.get_ValueAsBoolean("IsSOTrx") ? "SC" : "PC");
				if(po.get_Value("C_DocTypeTarget_ID").equals(doctype))
				{
					String sql = "SELECT 1 FROM C_Invoice WHERE C_Order_ID = ? AND DocStatus NOT IN ('VO','RE')";
					boolean exist = DB.getSQLValue(po.get_TrxName(), sql, po.get_ID()) > 0;
					if(exist)
						return "Existing Invoice was created using this order";
				}
				else
				{
					String sql = "SELECT M_InOut_ID FROM M_InOut WHERE C_Order_ID = ? AND DocStatus NOT IN ('VO','RE')";
					String documentno = DB.getSQLValueString(po.get_TrxName(), sql, po.get_ID());
					if(documentno != null)
						return "Existing Receipt was created using this order.";
				}
			}
			else if(timing == TIMING_AFTER_COMPLETE)
			{
				updateDiscountBudget(po, false);
				if(!po.get_ValueAsBoolean("isSOTrx"))
				{
					String errMsg = delHandleRequest(po, false, true);
					if(!Util.isEmpty(errMsg, true))
						return errMsg;
					errMsg = upPriceListOnOrder(po);
					if(!Util.isEmpty(errMsg, true))
						return errMsg;
				}
				if(po.get_Value("OrderContractType").equals(X_C_Order.ORDERCONTRACTTYPE_Non_BindingContractOrder))
				{
					String errMsg = closePrevOrder(po);
					if(!Util.isEmpty(errMsg, true))
						return errMsg;
				}
			}
			else if(timing == TIMING_AFTER_REACTIVATE)
			{
				updateDiscountBudget(po, true);
				updateQtyReserved(po);
			}
			else if(timing == TIMING_AFTER_REVERSEACCRUAL)
			{
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_REVERSECORRECT)
			{
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_VOID)
			{
				confirmSOVoid(po);
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_PREPARE)
			{
//				MOrder order = (MOrder) po;
//				if(order.isLockPricing() && !confirmOK(order, order.getDatePromised()))
//				{
//					String clause = "LOCK PRICING CONFIRMATION";
//					createConfirmation(order, order.getDatePromised(), clause
//							, MUNSSOConfirmation.CONFIRMATIONTYPE_OrderConfirmation);
//					return "Waiting approval of locked order";
//				}

//				String invalidDiscountMsg = checkDiscount(order, order.getDatePromised());
//				String invalidPriceMsg = checkValidPrice(order, order.getDatePromised());
//				if((!Util.isEmpty(invalidDiscountMsg, true) || !Util.isEmpty(invalidPriceMsg, true))
//						&& !confirmOK(order, order.getDatePromised()))
//				{
//					createConfirmation(
//							order, order.getDatePromised(), invalidPriceMsg.concat(invalidDiscountMsg)
//							, MUNSSOConfirmation.CONFIRMATIONTYPE_OrderConfirmation);
//					return invalidDiscountMsg + invalidPriceMsg;
//				}
			}
			else if(timing == TIMING_BEFORE_VOID)
			{
				String errMsg = checkOtherDoc((MOrder) po);
				if(!Util.isEmpty(errMsg, true))
					return errMsg;
			}
		}
		else if(po.get_TableName().equals(MInvoice.Table_Name))
		{
			//not used for validate amount credit memo > amount invoice
//			if(timing == TIMING_BEFORE_PREPARE)
//			{
//				int CN_Invoice_ID = 0;
//				CN_Invoice_ID = po.get_ValueAsInt(X_C_Invoice.COLUMNNAME_CN_Invoice_ID);
//				
//				if(CN_Invoice_ID > 0)
//				{
//					return validateCreditNote(po, CN_Invoice_ID);
//				}
//			}
			if(timing == TIMING_BEFORE_VOID)
			{
				String errormsg = isInvoiceAllocated(po);
				if(null != errormsg)
					return errormsg;
			}
			else if(timing == TIMING_BEFORE_REVERSEACCRUAL)
			{
				String errormsg = isInvoiceAllocated(po);
				if(null != errormsg)
					return errormsg;
			}
			else if(timing == TIMING_BEFORE_REVERSECORRECT)
			{
				String errormsg = isInvoiceAllocated(po);
				if(null != errormsg)
					return errormsg;
			}
			else if(timing == TIMING_AFTER_COMPLETE)
			{
				updateDiscountBudget(po, false);
				autoPaid(po);
//				noticeUnProcessedInvoice(po);
			}
			else if(timing == TIMING_AFTER_REACTIVATE)
			{
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_REVERSEACCRUAL)
			{
				unSelectionTicket(po);
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_REVERSECORRECT)
			{
				unSelectionTicket(po);
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_VOID)
			{
				unSelectionTicket(po);
				updateDiscountBudget(po, true);
			}
			else if(timing == TIMING_AFTER_PREPARE)
			{
				String sql = "SELECT DocumentNo FROM UNS_PL_Return WHERE UNS_PackingList_ID = "
						+ " (SELECT MAX(UNS_PackingList_ID) FROM UNS_PackingList_Order WHERE "
						+ " C_Invoice_ID = ?) AND DocStatus IN ('DR','IP','IN')";
				String theDocumentNo = DB.getSQLValueString(po.get_TrxName(), sql, po.get_ID());
				if (!Util.isEmpty(theDocumentNo))
				{
					return "Could not run action. the invoice document already linked with "
							+ " packing list document. please complete Packing List Return Document "
							+ " first. Packing List Return No " + theDocumentNo;
				}
				
				MInvoice invoice = (MInvoice) po;	
				if(invoice.getC_Order_ID() <= 0)
					return null;
				
				MOrder order = (MOrder) invoice.getC_Order();
//				boolean confirmOK = false;
//				if(order.isLockPricing())
//					confirmOK = confirmOK(order, invoice.getDateInvoiced());
//				
//				if(confirmOK)
//					return checkDiferentPrice(invoice);
				
				String docSubTypeCash = MDocType.DOCSUBTYPESO_CashOrder;
				
				if(docSubTypeCash.equals(order.getC_DocTypeTarget()
						.getDocSubTypeSO()))
				{
					return null;
				}
				
//				if(order.isLockPricing())
//					confirmOK = confirmOK(order, invoice.getDateInvoiced());
//				
//				if(confirmOK)
//					return checkDiferentPrice(invoice);
				
//				String invalidDiscountMsg = checkDiscount(invoice, invoice.getDateInvoiced());
//				String invalidPriceMsg = checkValidPrice(invoice, invoice.getDateInvoiced());
				
//				if((!Util.isEmpty(invalidDiscountMsg, true) || !Util.isEmpty(invalidPriceMsg, true))
//					&& !confirmOK(invoice, invoice.getDateInvoiced()))
//				{
//					createConfirmation(
//							invoice, invoice.getDateInvoiced(), invalidDiscountMsg.concat(invalidPriceMsg)
//							, MUNSSOConfirmation.CONFIRMATIONTYPE_InvoiceConfirmation);
//				
//					return invalidDiscountMsg + invalidPriceMsg;
//				}
				
				MInvoiceLine[] lines = invoice.getLines();
				for(MInvoiceLine line : lines)
				{
					if(line.getC_OrderLine_ID() <= 0)
						continue;
					
					MOrderLine oLine = new MOrderLine(po.getCtx(), line.getC_OrderLine_ID(), po.get_TrxName());
					if(oLine.getQtyBonuses().compareTo(line.getQtyBonuses()) == 0)
						continue;
					
					oLine.setQtyBonuses(line.getQtyBonuses());
					oLine.saveEx();
				}
			}
		}
		else if(po.get_TableName().equals(MInOut.Table_Name))
		{
			if(timing == TIMING_AFTER_COMPLETE)
				updateDiscountBudget(po, false);
			else if(timing == TIMING_AFTER_REACTIVATE)
				updateDiscountBudget(po, true);
			else if(timing == TIMING_AFTER_REVERSEACCRUAL)
				updateDiscountBudget(po, true);
			else if(timing == TIMING_AFTER_REVERSECORRECT)
				updateDiscountBudget(po, true);
			else if(timing == TIMING_AFTER_VOID)
				updateDiscountBudget(po, true);
			else if(timing == TIMING_BEFORE_PREPARE)
				return validateOnShipmentProcessAutoComplete(po);
				
		}
		else if(po.get_TableName().equals(MUNSPackingList.Table_Name))
		{
			if(timing == TIMING_AFTER_PREPARE)
			{
				String whereClause = "UNS_PackingList_ID = ?";
				
				List<MUNSPackingListOrder> plOrder = new Query(po.getCtx(), MUNSPackingListOrder.Table_Name,
						whereClause, po.get_TrxName()).setParameters(po.get_ID())
							.list();
				
				for (MUNSPackingListOrder listOrder : plOrder)
				{
					if(listOrder.getReferenceNo() == null)
						continue;
					else
					{
						MInOut io = new MInOut(po.getCtx(), listOrder.getM_InOut_ID(), po.get_TrxName());
						io.setPOReference(listOrder.getReferenceNo());
						io.saveEx();
					}
				}
			}
		}
		else if(po.get_TableName().equals(MUNSVoucherBook.Table_Name))
		{
			if(timing == TIMING_BEFORE_VOID)
			{
				MUNSVoucherBook vBook = (MUNSVoucherBook) po;
	
				for(MUNSVoucherCode vCode : vBook.getLines(false))
				{
					if(vCode.getVoucherAmt().compareTo(vCode.getUnusedAmt()) == 1)
						return "There was Voucher Code has been used";
				}
				
			}
		}
		else if(po.get_TableName().equals(MMovement.Table_Name))
		{
			if(timing == TIMING_BEFORE_COMPLETE)
			{
				MMovement move = new MMovement(po.getCtx(), po.get_ID(), po.get_TrxName());
				if(move.getM_Requisition_ID() > 0)
				{
					String statusRequisition = move.getM_Requisition().getDocStatus();
					if(statusRequisition.equals("CL")
							|| statusRequisition.equals("VO")
								|| statusRequisition.equals("RE"))
						return null;
					String sql = "UPDATE M_Requisition SET DocStatus = 'CL', DocAction = '--' WHERE M_Requisition_ID = ?";
					DB.executeUpdate(sql, move.getM_Requisition_ID(), move.get_TrxName());
					boolean generateMA = MSysConfig.getBooleanValue(MSysConfig.REQUISITION_USE_STORAGE_RESERVATION, false);
					if(generateMA)
					{
						com.unicore.base.model.MRequisition req = new com.unicore.base.model.MRequisition
								(move.getCtx(), move.getM_Requisition_ID(), move.get_TrxName());
						com.unicore.base.model.MRequisitionLine[] lines = req.getLines();
						for(int i=0;i<lines.length;i++)
						{
							MUNSRequisitionLineMA[] mas = lines[i].getMAs();
							for(int j=0;j<mas.length;j++)
							{
								MUNSStorageReservation.addOrCreate(
										move.getCtx(), req.getDestinationWarehouse_ID(), lines[i].getM_Product_ID(),
											mas[j].getM_AttributeSetInstance_ID(), move.getReversal_ID() > 0 ?
													mas[j].getMovementQty() : mas[j].getMovementQty().negate(), move.get_TrxName());
							}
						}
					}
				}
			}
			else if(timing == TIMING_AFTER_REVERSECORRECT || TIMING_AFTER_REVERSEACCRUAL == timing
					|| timing == TIMING_AFTER_VOID)
			{
				MMovement move = new MMovement(po.getCtx(), po.get_ID(), po.get_TrxName());
				if(move.getM_Requisition_ID() > 0)
				{
					String sql = "UPDATE M_Requisition SET DocStatus = 'CO', DocAction = 'CL' WHERE M_Requisition_ID = ?";
					DB.executeUpdate(sql, move.getM_Requisition_ID(), move.get_TrxName());
					sql = "DELETE FROM UNS_HandleRequests s WHERE s.M_MovementLine_ID > 0 AND s.M_MovementLine_ID IN"
							+ " (SELECT l.M_MovementLine_ID FROM M_MovementLine l WHERE l.M_Movement_ID = ?)";
					DB.executeUpdate(sql, move.get_ID(), move.get_TrxName());
				}
			}
		}
		else if(po.get_TableName().equals(MRequisition.Table_Name)
				&& (timing == TIMING_BEFORE_CLOSE || timing == TIMING_BEFORE_VOID
				|| timing == TIMING_BEFORE_REACTIVATE))
		{
			boolean generateMA = MSysConfig.getBooleanValue(MSysConfig.REQUISITION_USE_STORAGE_RESERVATION, false);
			if(generateMA)
			{
				com.unicore.base.model.MRequisition req = (com.unicore.base.model.MRequisition) po;
				com.unicore.base.model.MRequisitionLine[] lines = req.getLines();
				for(int i=0;i<lines.length;i++)
				{
					MUNSRequisitionLineMA[] mas = lines[i].getMAs();
					for(int j=0;j<mas.length;j++)
					{
						MUNSStorageReservation.addOrCreate(
							po.getCtx(), req.getDestinationWarehouse_ID(), lines[i].getM_Product_ID(),
									mas[j].getM_AttributeSetInstance_ID(), mas[j].getMovementQty().negate(), po.get_TrxName());
					}
				}
			}
		}
		
		//for KPDJBC
		if (("M_InOut".equals(po.get_TableName()) || "C_Invoice".equals(po.get_TableName())
				|| "C_Payment".equals(po.get_TableName())) && (timing == TIMING_AFTER_COMPLETE
				|| timing == TIMING_AFTER_REVERSEACCRUAL || timing == TIMING_AFTER_REVERSECORRECT))
		{
			return doUpdateCreditAgreementPaySchedule(po, timing != TIMING_AFTER_COMPLETE);
		}
		return null;
	}
	
	private String isInvoiceAllocated(PO po)
	{
		String errormsg = null;
		String sql = "SELECT array_to_string(array_agg(p.DocumentNo),' & ') FROM C_Payment p"
				+ " WHERE (p.C_Invoice_ID = ? OR C_Payment_ID IN (SELECT C_Payment_ID FROM"
				+ " C_PaymentAllocate pa WHERE pa.C_Invoice_ID = ?)) AND DocStatus NOT IN ('VO','RE')";
		String documentno = DB.getSQLValueString(po.get_TrxName(), sql, po.get_ID(), po.get_ID() );
		if(documentno != null)
			errormsg = "Invoice has allocated. Please check Payment with Document No = "+documentno;
		
		return errormsg;
	}
	/**
	 * 
	 * @param invoice
	 * @return
	 *
	private String checkDiferentPrice(MInvoice invoice)
	{
		MInvoiceLine[] lines = invoice.getLines();
		for(int i=0; i<lines.length; i++)
		{
			MInvoiceLine line = lines[i];
			String sql = "SELECT PriceActual FROM C_OrderLine WHERE C_OrderLine_ID = ?";
			BigDecimal orderPriceActual = DB.getSQLValueBD(invoice.get_TrxName(), sql, line.getC_OrderLine_ID());
			if(orderPriceActual.compareTo(line.getPriceActual()) != 0)
				return "Order is Locked but price diferent has found in price on order and invoice";
		}
		return null;
	}
	*/
	
	
	/**
	 * 
	 * @param listDiscount
	 * @param date
	 * @return
	 *
	private String checkEffectivedateOfDiscount(
			MUNSDiscountTrx discountTrx, Timestamp date)
	{
		StringBuilder msg = new StringBuilder();
		if(discountTrx.isZeroDiscount())
			discountTrx.deleteEx(true);
		
		String sql = null;
		int param_ID = 0;
		
		if(discountTrx.getM_DiscountSchema_ID() > 0)
		{
			sql = "SELECT ValidTo FROM M_DiscountSchema WHERE M_DiscountSchema_ID = ?";
			param_ID = discountTrx.getM_DiscountSchema_ID();
		}
		else if(discountTrx.getM_DiscountSchemaBreak_ID() > 0)
		{
			sql = "SELECT ValidTo FROM M_DiscountSchema WHERE M_DiscountSchema_ID = "
					+ " (SELECT M_DiscountSchema_ID FROM M_DiscountSchemaBreak WHERE M_DiscountSchemaBreak_ID = ?)";
			param_ID = discountTrx.getM_DiscountSchemaBreak_ID();
		}
		else if(discountTrx.getUNS_DSBreakLine_ID() > 0)
		{
			sql = "SELECT ValidTo FROM M_DiscountSchema WHERE M_DiscountSchema_ID = "
					+ " (SELECT M_DiscountSchema_ID FROM M_DiscountSchemaBreak WHERE M_DiscountSchemaBreak_ID = "
					+ " (SELECT M_DiscountSchemaBreak_ID FROM UNS_DSBreakLine WHERE UNS_DSBreakLine_ID = ?))";
			param_ID = discountTrx.getUNS_DSBreakLine_ID();
		}
		
		if(null == sql)
			return msg.toString();
		
		Timestamp ValidDate = DB.getSQLValueTS(discountTrx.get_TrxName(), sql, param_ID);
		
		if(null == ValidDate)
			return msg.toString();
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ValidDate.getTime());
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMaximum(Calendar.MILLISECOND));
		ValidDate = new Timestamp(cal.getTimeInMillis());
		
		if(ValidDate.compareTo(date) == 1)
			return msg.toString();
		
		msg.append("Discount kadaluarsa pada nomor urut :")
		.append(discountTrx.getSeqNo()).append("\n");
		return msg.toString();
	}
	
	
	/**
	 * 
	 * @param order
	 * @return
	 *
	private MUNSSOConfirmation createConfirmation(PO po, Timestamp date, String clause, String confirmationEvent)
	{
		MUNSSOConfirmation[] confirms = MUNSSOConfirmation.get(po, po.get_TrxName());
		MUNSSOConfirmation confirm = null;
		for(int i=0; i<confirms.length; i++)
		{
			confirm = confirms[i];
			if(confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_Completed)
					|| confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_Closed))
			{
				Timestamp validDate = confirm.getValidTo();
				
				if(null == validDate)
					break;
				
				int compare = validDate.compareTo(date);
				if(compare < 0)
				{
					confirm = null;
					continue;
				}
			}
			
			if(!confirmationEvent.equals(confirm.getConfirmationType()))
				confirm = null;
		}
		
		if(confirm == null)
		{	
			Trx trx = Trx.get(Trx.createTrxName(), true);
			trx.start();
			confirm = new MUNSSOConfirmation(po);
			confirm.setValidTo(date);
			confirm.setDescription(clause);
			confirm.setConfirmationType(confirmationEvent);
			try {
				confirm.saveEx(trx.getTrxName());
				confirm.createLines();
				Thread thread = new Thread(new ThreadSave(trx));
				thread.setName("Try Commit Trx");
				thread.start();
			} catch (Exception ex) {
				throw new AdempiereException(ex.getMessage());
			} finally {
			}
		}
		
		if(confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_Drafted)
				|| confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_InProgress)
				|| confirm.getDocStatus().equals(MUNSSOConfirmation.DOCSTATUS_Invalid))
		{
			MessageBox.showMsg(po,
					po.getCtx(), "Please open SO Confirmation!".concat(confirm.getDocumentNo())
					, "Information", MessageBox.OK, MessageBox.ICONINFORMATION);
		}
		
		return confirm;
	}
	
	/**
	 * 
	 * @param C_BPartner_ID
	 * @param Qty
	 * @param IsSOTrx
	 * @param M_PriceList_ID
	 * @param M_Product_ID
	 * @param date
	 * @param trxName
	 * @return
	 *
	private MProductPricing getProductPricing(
			int C_BPartner_ID, int C_BPartner_Location_ID, BigDecimal Qty, boolean IsSOTrx
			, int M_PriceList_ID, int M_Product_ID, Timestamp date, String trxName)
	{
		MProductPricing pp = new MProductPricing (M_Product_ID, C_BPartner_ID, Qty, IsSOTrx, C_BPartner_Location_ID);
		pp.setM_PriceList_ID(M_PriceList_ID);
		
		if(M_PriceList_ID <= 0)
			throw new AdempiereException("Price list not set");	
	
		String sql = "SELECT plv.M_PriceList_Version_ID "
			+ "FROM M_PriceList_Version plv "
			+ "WHERE plv.M_PriceList_ID=? "						//	1
			+ " AND plv.ValidFrom <= ? ORDER BY plv.ValidFrom DESC ";
		

		int M_PriceList_Version_ID = DB.getSQLValueEx(trxName, sql, M_PriceList_ID, date);
		if (M_PriceList_Version_ID > 0 )
			pp.setM_PriceList_Version_ID(M_PriceList_Version_ID);
		
		pp.setPriceDate(date);
		return pp;
	}
	*/
	
	/**
	 * 
	 * @param po
	 * @return
	 */
	public MUNSDiscountTrx[] getDiscountTrx(PO po)
	{
		String sql = "SELECT * FROM UNS_DiscountTrx WHERE "
				+ po.get_TableName() + "_ID = ?";
		
		List<MUNSDiscountTrx> list = new ArrayList<>();
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql, po.get_TrxName());
			st.setInt(1, po.get_ID());
			rs = st.executeQuery();
			while (rs.next())
			{
				MUNSDiscountTrx trx = new MUNSDiscountTrx(po.getCtx(), rs, po.get_TrxName());
				list.add(trx);
			}
		} catch (SQLException e) {
			throw new AdempiereException(e.getMessage());
		} finally {
			DB.close(rs, st);
		}
		
		MUNSDiscountTrx[] retVal = new MUNSDiscountTrx[list.size()];
		list.toArray(retVal);
		return retVal;
	}
	
	/**
	 * 
	 * @param po
	 * @param to
	 */
	private void copyDiscount(PO po, PO to)
	{
		MUNSDiscountTrx[] trxs = MUNSDiscountTrx.get(po);
		for(int i=0; i<trxs.length; i++)
		{
			MUNSDiscountTrx newTrx = new MUNSDiscountTrx(to.getCtx(), 0, to.get_TrxName());
			PO.copyValues(trxs[i], newTrx);
			newTrx.setIsChangedByUser(false);
			newTrx.set_ValueOfColumn(po.get_TableName() + "_ID", null);
			newTrx.set_ValueOfColumn(to.get_TableName() + "_ID", to.get_ID());
			newTrx.setAD_Org_ID(to.getAD_Org_ID());
			newTrx.saveEx();
		}
	}
	
	
	/**
	 * 
	 * @param po
	 * @param revert
	 */
	private void updateDiscountBudget(PO po, boolean revert)
	{
		PO refPO = null;
		if(po instanceof MInOut)
		{
			if(po.get_ValueAsInt(MInOut.COLUMNNAME_C_Invoice_ID) > 0)
			{
				refPO = new MInvoice(
						po.getCtx(), po.get_ValueAsInt(MInOut.COLUMNNAME_C_Invoice_ID), po.get_TrxName());
			}
			else if(po.get_ValueAsInt(MInOut.COLUMNNAME_C_Order_ID) > 0)
			{
				refPO = new MOrder(
						po.getCtx(), po.get_ValueAsInt(MInOut.COLUMNNAME_C_Order_ID), po.get_TrxName());
			}
		}
		else if(po instanceof MInOutLine)
		{
			String sql = "SELECT ".concat(MInvoiceLine.COLUMNNAME_C_InvoiceLine_ID)
					.concat( " FROM ").concat(MInvoiceLine.Table_Name)
					.concat(" WHERE ").concat(MInvoiceLine.COLUMNNAME_M_InOutLine_ID)
					.concat("=?");
			int retVal = DB.getSQLValue(po.get_TrxName(), sql, po.get_ID());
			if(retVal > 0)
			{
				refPO = new MInvoiceLine(po.getCtx(), retVal, po.get_TrxName());
			}
			else
			{
				sql = "SELECT ".concat(MInOutLine.COLUMNNAME_C_OrderLine_ID)
						.concat(" FROM ").concat(MInOutLine.Table_Name)
						.concat(" WHERE ").concat(MInOutLine.COLUMNNAME_M_InOutLine_ID)
						.concat("=?");
				retVal = DB.getSQLValue(po.get_TrxName(), sql, po.get_ID());
				if(retVal > 0)
				{
					refPO = new MOrderLine(po.getCtx(), retVal, po.get_TrxName());
				}
			}
			
		}
		else
		{
			refPO = po;
		}
		
		if(refPO == null)
			return;
		
		MUNSDiscountTrx[] trxs = MUNSDiscountTrx.get(refPO);
		
		for(int i=0; i<trxs.length; i++)
		{
			MUNSDiscountTrx trx = trxs[i];
			boolean alreadyProcessed = false;
			for (int j=0; j<i; j++)
			{
				if (trx.getM_DiscountSchema_ID() == trxs[j].getM_DiscountSchema_ID()
						|| trx.getM_DiscountSchemaBreak_ID() == trxs[j].getM_DiscountSchemaBreak_ID()
						|| trx.getUNS_DSBreakLine_ID() == trxs[j].getUNS_DSBreakLine_ID())
				{
					alreadyProcessed = true;
					break;
				}
			}
			
			if (alreadyProcessed)
				continue;
			
			if(po.get_TableName().equals(MOrder.Table_Name)
					|| po.get_TableName().equals(MUNSPOSTrx.Table_Name))
				trx.updateReserveQty(revert);
			else if(po.get_TableName().equals(MInvoice.Table_Name))
				trx.updateInvoicedQty(revert);
			else if(po.get_TableName().equals(MInOut.Table_Name))
				trx.updateShipReceiptQty(revert);
			else if(po.get_TableName().equals(MOrderLine.Table_Name)
					|| po.get_TableName().equals(MUNSPOSTrxLine.Table_Name))
				trx.updateReserveQty(revert);
			else if(po.get_TableName().equals(MInvoiceLine.Table_Name))
				trx.updateInvoicedQty(revert);
			else if(po.get_TableName().equals(MInOutLine.Table_Name))
				trx.updateShipReceiptQty(revert);
			else
				throw new AdempiereException("Unhandled table " + po.get_TableName());
		}
		
		PO[] pos = getLines(po);
		for(int i=0; i<pos.length; i++)
		{
			updateDiscountBudget(pos[i], revert);
		}
	}
	
	private String invoiceLineOnChange(PO po)
	{
		MInvoiceLine line = (MInvoiceLine) po;
		if(!line.is_ValueChanged(MInvoiceLine.COLUMNNAME_QtyEntered))
			return null;
		
		String msg = null;
		try
		{
			int deleted = deleteDiscountTrx(line);
			if(deleted == 0)
				return null;

			//reversal diindikasikan dengan quantiti < 0
			if (line.getQtyInvoiced().signum() != -1) {
				line.setDiscount(Env.ZERO);
				line.setDiscountAmt(Env.ZERO);
				line.setQtyBonuses(Env.ZERO);
				line.setPrice(line.getPriceList());
			}
			
			MInvoice invoice = (MInvoice) line.getC_Invoice();
			deleted = deleteDiscountTrx(invoice);
			
			for(MInvoiceLine otherLine : invoice.getLines())
			{
				if(otherLine.get_ID() == line.get_ID())
					continue;
				
				deleted = deleteDiscountTrx(otherLine);
			}
		}
		catch (Exception e)
		{
			msg = e.getMessage();
		}
		return msg;
	}
	
	/**
	 * 
	 * @param po
	 * @return
	 */
	private int deleteDiscountTrx(PO po)
	{
		int deleted = 0;
		MUNSDiscountTrx[] discsTrx = MUNSDiscountTrx.get(po);		
		for(MUNSDiscountTrx discTrx : discsTrx)
		{
			discTrx.deleteEx(true);
			deleted++;
		}
		return deleted;
	}
	
	private static final String[] 	LocValPart_ORDER = new String[] {"PST", "KTB", "TLB", "BTA"}; 
	private static final String 	GOODLOC_PREFIX = "Good-";
	private static final String		STOCKCARE_PREFIX = "StkCare-";
	
	/**
	 * 
	 * @param po
	 * @return
	 */
	private String validateOnShipmentProcessAutoComplete(PO po)
	{
		String event = "N"; //Env.getContext(Env.getCtx(), "ON_IMPORT");
		boolean ok = !Util.isEmpty(event, true) && event.equals("Y");
		
		if(!ok)
			return null;
		
		MInOut io = (MInOut) po;
		Hashtable<String, PhysicalTmp> physicalTmpMap = new Hashtable<>();
		

		if(!io.getMovementType().equals(MInOut.MOVEMENTTYPE_CustomerShipment)
				&& !io.getMovementType().equals(MInOut.MOVEMENTTYPE_VendorReturns))
			return null;	
		
		for (MInOutLine line : io.getLines())
		{
			if(line.getM_Product().getM_AttributeSet_ID() > 0 && !io.isSOTrx()
					&& !io.getMovementType().equals(MInOut.MOVEMENTTYPE_VendorReturns))
			{	
				MAttributeSetInstance.initAttributeValuesFrom(
						line, 
						MInOutLine.COLUMNNAME_M_Product_ID, 
						MInOutLine.COLUMNNAME_M_AttributeSetInstance_ID, 
						po.get_TrxName());

				line.saveEx();
			}
		}
		
		MInOutLine[] lines = io.getLines();
		for(MInOutLine line : lines)
		{
			String keyTmp = "Stock-" + line.getM_Product_ID() + "-" + line.getM_Locator_ID() + "-" 
						+ line.getM_AttributeSetInstance_ID();
			PhysicalTmp tmp = physicalTmpMap.get(keyTmp);
			if(null == tmp)
			{
				tmp = new PhysicalTmp(
						line.getM_Locator_ID(), line.getM_Product_ID(), line.getM_AttributeSetInstance_ID(), Env.ZERO);
				physicalTmpMap.put(keyTmp, tmp);
			}
			
			tmp.Quantity = tmp.Quantity.add(line.getMovementQty());
		}
		
		Properties ctx = io.getCtx();
		String trxName = io.get_TrxName();
		
		Hashtable<String, MMovement> mapMovement = new Hashtable<>();
		Hashtable<String, MInventory> mapInventory = new Hashtable<>();

		String IOOrgValue = MOrg.get(ctx, io.getAD_Org_ID()).getValue();
		String IODepoStr = IOOrgValue.substring(IOOrgValue.indexOf('-') + 1);
		String orgDivPart = IOOrgValue.substring(0, IOOrgValue.indexOf('-'));
		String orgDivPusat = orgDivPart + "-PST";
		
		int orgMoveFrom_ID = MOrg.get(ctx, orgDivPusat, trxName).get_ID();
		
		String locatorSQL = "SELECT M_Locator_ID FROM M_Locator WHERE Value=?";
		
		String documentNo = PREVIX_PHYSICAL_DOCNO.concat(io.getDocumentNo());
		MWarehouse whs = MWarehouse.get(ctx, io.getM_Warehouse_ID());
		String key = (new StringBuilder(documentNo).append(whs.getValue())).toString();
		
		for(PhysicalTmp required : physicalTmpMap.values())
		{
			BigDecimal ioOrgOnHand = MStorageOnHand.getQtyOnHandForLocator(
					required.M_Product_ID, required.M_Locator_ID, required.M_AttributeSetInstance_ID, po.get_TrxName());			
			
//			if(required.Quantity.compareTo(onHand) <= 0)
//				continue;

			MCost ioOrgCost = new Query(ctx, MCost.Table_Name, 
					"AD_Org_ID=? AND M_Product_ID=? AND M_CostElement_ID=1000004", trxName)
				.setParameters(io.getAD_Org_ID(), required.M_Product_ID)
				.first();
			
			ioOrgOnHand = ioOrgCost == null? Env.ZERO : 
				ioOrgCost.getCurrentQty().compareTo(ioOrgOnHand) < 0? ioOrgCost.getCurrentQty() : ioOrgOnHand;
			
			if (ioOrgOnHand.compareTo(required.Quantity) >= 0)
				continue;
			
			BigDecimal requiredQty = required.Quantity.subtract(ioOrgOnHand);
			
			MMovement move = mapMovement.get(key);
			
			String sql = "SELECT SUM(soh.QtyOnHand) FROM M_StorageOnHand soh, M_Locator loc "
						+ "WHERE soh.M_Product_ID=? AND soh.M_Locator_ID=loc.M_Locator_ID AND loc.Value IN (?, ?)";
			BigDecimal totalOHAtDepo = DB.getSQLValueBDEx(
					trxName, sql, required.M_Product_ID, GOODLOC_PREFIX + IOOrgValue, STOCKCARE_PREFIX + IOOrgValue);
			
			if (totalOHAtDepo != null && totalOHAtDepo.compareTo(requiredQty) < 0) 
			{
				String whGudang = "G." + IODepoStr;
				int whGudang_ID = DB.getSQLValueEx(trxName, "SELECT M_Warehouse_ID FROM M_Warehouse WHERE Value=?", whGudang);
				MStorageOnHand[] sohListAtGudang = 
						MStorageOnHand.getWarehouse(ctx, whGudang_ID, required.M_Product_ID, required.M_AttributeSetInstance_ID, 
								null, true, true, 0, trxName, false);
				
				for (MStorageOnHand sohAtGudang : sohListAtGudang) 
				{
					BigDecimal ohQty = sohAtGudang.getQtyOnHand();

					BigDecimal moveQty = ohQty.compareTo(requiredQty) >= 0? requiredQty : ohQty;
					if(ohQty.compareTo(requiredQty.setScale(0, BigDecimal.ROUND_UP)) >= 0)
						moveQty = requiredQty.setScale(0, BigDecimal.ROUND_UP);
					
					requiredQty = requiredQty.subtract(moveQty);
					
					move = createMovementToFulfillShipment(
							move, io, required, documentNo, orgMoveFrom_ID, sohAtGudang.getM_Locator_ID(), moveQty);
					
					if (requiredQty.signum() <=0)
						break;
				}
			}
			
			if (requiredQty.signum() <= 0) {
				mapMovement.put(key, move);
				continue;
			}
			
			for(String depoStr : LocValPart_ORDER) 
			{
				if (IODepoStr.equals(depoStr))
					continue;
				
				String depoLocator = 
					new  StringBuilder(GOODLOC_PREFIX).append(orgDivPart).append("-").append(depoStr).toString();
				
				int locFrom_ID = DB.getSQLValueEx(trxName, locatorSQL,  depoLocator);
				
				BigDecimal onHandDepo = 
					MStorageOnHand.getQtyOnHandForLocator(required.M_Product_ID, locFrom_ID,
														  required.M_AttributeSetInstance_ID, trxName);
				
				if (onHandDepo.signum() > 0) {
					
					MCost depoCost = new Query(ctx, MCost.Table_Name, 
							"AD_Org_ID=? AND M_Product_ID=? AND M_CostElement_ID=1000004", trxName)
						.setParameters(MLocator.get(ctx, locFrom_ID).getAD_Org_ID(), required.M_Product_ID)
						.first();
					
					if (depoCost == null || depoCost.getCurrentQty().signum() <= 0)
						continue;
					
					if (depoCost.getCurrentQty().compareTo(onHandDepo) < 0)
						onHandDepo = depoCost.getCurrentQty();
					
					BigDecimal moveQty = onHandDepo.compareTo(requiredQty) >= 0 ? requiredQty : onHandDepo;
					if(onHandDepo.compareTo(requiredQty.setScale(0, BigDecimal.ROUND_UP)) >= 0)
						moveQty = requiredQty.setScale(0, BigDecimal.ROUND_UP);
					
					requiredQty = requiredQty.subtract(moveQty);
					
					move = createMovementToFulfillShipment(
							move, io, required, documentNo, orgMoveFrom_ID, locFrom_ID, moveQty);
				}
				
				if (requiredQty.signum() <=0)
					break;
			}
			
			if (requiredQty.signum() <= 0) {
				mapMovement.put(key, move);
				continue;
			}
			
			// If still not fulfilled then move it from stock care locator. Start with IO's org stock care loc.
			String IOStkCareLocator = 
					new  StringBuilder(STOCKCARE_PREFIX).append(IOOrgValue).toString();
			int IOStkCareLocator_ID = DB.getSQLValueEx(trxName, locatorSQL,  IOStkCareLocator);
			BigDecimal stkCareIOOnHand = 
					MStorageOnHand.getQtyOnHandForLocator(required.M_Product_ID, IOStkCareLocator_ID,
														  required.M_AttributeSetInstance_ID, trxName);
			if (stkCareIOOnHand.signum() > 0) 
			{
				BigDecimal availableCostQty = ioOrgCost.getCurrentQty().subtract(ioOrgOnHand);
				if (availableCostQty.signum() > 0) 
				{
					stkCareIOOnHand = availableCostQty.compareTo(stkCareIOOnHand) >= 0? stkCareIOOnHand : availableCostQty;
					
					BigDecimal moveQty = stkCareIOOnHand.compareTo(requiredQty) >= 0 ? requiredQty : stkCareIOOnHand;
					if(stkCareIOOnHand.compareTo(requiredQty.setScale(0, BigDecimal.ROUND_UP)) >= 0)
						moveQty = requiredQty.setScale(0, BigDecimal.ROUND_UP);
					
					requiredQty = requiredQty.subtract(moveQty);
					
					move = createMovementToFulfillShipment(
							move, io, required, documentNo, orgMoveFrom_ID, IOStkCareLocator_ID, moveQty);
				}
			}
			
			if (requiredQty.signum() <=0) {
				mapMovement.put(key, move);
				continue;
			}
			
			// If still not fulfilled then move it from other depos' stock care.
			for(String depoStr : LocValPart_ORDER) 
			{
				if (IODepoStr.equals(depoStr))
					continue;
				
				String depoLocator = 
					new  StringBuilder(STOCKCARE_PREFIX).append(orgDivPart).append("-")
					.append(depoStr).toString();
				
				int locFrom_ID = DB.getSQLValueEx(trxName, locatorSQL,  depoLocator);
				
				BigDecimal onHandDepo = 
					MStorageOnHand.getQtyOnHandForLocator(required.M_Product_ID, locFrom_ID,
														  required.M_AttributeSetInstance_ID, trxName);
				
				if (onHandDepo.signum() > 0) {
					
					BigDecimal moveQty = onHandDepo.compareTo(requiredQty) >= 0 ? requiredQty : onHandDepo;
					if(onHandDepo.compareTo(requiredQty.setScale(0, BigDecimal.ROUND_UP)) >= 0)
						moveQty = requiredQty.setScale(0, BigDecimal.ROUND_UP);
					
					requiredQty = requiredQty.subtract(moveQty);
					
					move = createMovementToFulfillShipment(
							move, io, required, documentNo, orgMoveFrom_ID, locFrom_ID, moveQty);
				}
				
				if (requiredQty.signum() <=0)
					break;
			}			

			if (requiredQty.signum() <= 0) {
				mapMovement.put(key, move);
			}
			else { // Masih belum terpenuhi juga, maka buat physical.
				
				MCost cost = new Query(ctx, MCost.Table_Name, 
						"AD_Org_ID=? AND M_Product_ID=? AND M_CostElement_ID=1000004", trxName)
					.setParameters(io.getAD_Org_ID(), required.M_Product_ID)
					.first();
				
				MInventory inventory = mapInventory.get(key);
				
				
				if(null == inventory)
				{
					inventory = new MInventory(whs, trxName);
					inventory.setDocumentNo(documentNo);
					if (cost == null 
							|| cost.getCurrentQty().signum() <= 0 
							|| cost.getCurrentQty().compareTo(requiredQty) < 0) {
						inventory.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventoryImport));
						ioOrgOnHand = Env.ZERO;
					}
					else {
						inventory.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventory));
					}
					inventory.setDescription("Atomatically created to meet the needs of items on shipment document "
							.concat(io.getDocumentNo()));
					inventory.setMovementDate(io.getMovementDate());
					inventory.saveEx();
					mapInventory.put(key, inventory);
				}
				
				MInventoryLine iLine = new MInventoryLine(
						inventory, required.M_Locator_ID, required.M_Product_ID
						, required.M_AttributeSetInstance_ID
						, ioOrgOnHand, required.Quantity);
				
				iLine.setIsUPCBasedCounter(false);
				iLine.setInventoryType(MInventoryLine.INVENTORYTYPE_InventoryDifference);
				
				if (inventory.getC_DocType_ID() == MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventoryImport)) 
				{
					String whereClause = "M_Product_ID=? AND C_Invoice_ID=? AND PriceList > 0";
					MInvoiceLine invLine = new Query(ctx, MInvoiceLine.Table_Name, whereClause, trxName)
						.setParameters(required.M_Product_ID, io.getC_Invoice_ID()).first();
				
					if (invLine != null) {
						BigDecimal landedCost = invLine.getPriceList().multiply(BigDecimal.valueOf(0.12));
						
						iLine.setNewCostPrice(requiredQty.multiply(landedCost));
					}
				}
				
				iLine.saveEx();
				
//				return "Tidak ada stok yang dapat dipindahkan untuk DO Pengiriman " + io.getDocumentNo() +
//						" untuk produk " + MProduct.get(ctx, required.M_Product_ID);
			}
		}
		
		for(MMovement move : mapMovement.values())
		{
			try
			{
				ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(
						move, DocAction.ACTION_Complete);
				if(pi.isError())
				{
					return pi.getSummary();
				}
			}
			catch (Exception e)
			{
				return e.getMessage();
			}
		}
		
		for(MInventory inventory : mapInventory.values())
		{
			try
			{
				ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(
						inventory, DocAction.ACTION_Complete);
				if(pi.isError())
				{
					return pi.getSummary();
				}
			}
			catch (Exception e)
			{
				return e.getMessage();
			}
		}
		
		return null;
	}
	
	private final String PREVIX_PHYSICAL_DOCNO = "IO-";
	
	private MMovement createMovementToFulfillShipment(MMovement move,
			MInOut io, PhysicalTmp required, String documentNo, int orgFrom_ID, int locFrom_ID, BigDecimal qty)
	{
		if (move == null) {
			move = new MMovement(io.getCtx(), 0, io.get_TrxName());
			move.setDocumentNo(documentNo);
			move.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialMovement));
			move.setAD_Org_ID(orgFrom_ID);
			move.setDescription("Atomatically created to meet the needs of items on shipment document "
					.concat(io.getDocumentNo()));
			move.setMovementDate(io.getMovementDate());
			move.setDestinationWarehouse_ID(io.getM_Warehouse_ID());
			
			move.saveEx();
		}
		
		MMovementLine moveLine = new MMovementLine(move);
		
		int orgLocator_ID = MLocator.get(io.getCtx(), locFrom_ID).getAD_Org_ID();
		moveLine.setAD_Org_ID(orgLocator_ID);
		
		if (required.M_AttributeSetInstance_ID > 0)
			moveLine.setM_AttributeSetInstance_ID(required.M_AttributeSetInstance_ID);
		
		moveLine.setM_Product_ID(required.M_Product_ID);
		moveLine.setM_LocatorTo_ID(required.M_Locator_ID);
		moveLine.setM_Locator_ID(locFrom_ID);
		moveLine.setMovementQty(qty);
		
		moveLine.saveEx();
		
		return move;
	}
	
	/**
	 * This method used to set QtyReserved become Zero.
	 * @param po
	 */
	private void updateQtyReserved(PO po)
	{
		MOrder order = (MOrder) po;
		MOrderLine[] lines = order.getLines();
		for(MOrderLine line : lines)
		{
			BigDecimal qty = line.getQtyReserved();
			line.setQtyReserved(line.getQtyReserved().subtract(qty));
			if(!line.save())
				throw new AdempiereException("Error when try to update qtyreserved");
		}
	}
	
	public String confirmSOVoid(PO po)
	{
		MOrder order = (MOrder) po;
		MUNSPreOrder pOrder = MUNSPreOrder.getByOrder(order.getCtx(), order.get_ID(), order.get_TrxName()); 
		if(null != pOrder && pOrder.get_ID() > 0)
		{
			int retVal = MessageBox.showMsg(order.getCtx(), order.getProcessInfo()
					, "Sales Order has been voided. \n" +
					  "Do you want to void document pre order?"
					, "Pre Order confirmation void"
					, MessageBox.YESNO
					, MessageBox.ICONQUESTION);
			if(retVal == MessageBox.RETURN_YES)
			{
				try
				{
					if (!pOrder.getDocStatus().equals(DocAction.STATUS_Voided))
					{
						pOrder.setDocStatus("VO");
						pOrder.saveEx();
					}					
				}
				catch (Exception e) {
					return e.getMessage();
				}
			}
			else 
				return null;			
		}	
		return null;
	}
	
	public String closePrevOrder(PO po)
	{
		StringBuilder msg = new StringBuilder();
		MOrder order = (MOrder) po;
		
		String sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType=? AND DocSubTypeSO=?";
		int ctrDocType_ID = DB.getSQLValueEx(po.get_TrxName(), sql, MDocType.DOCBASETYPE_PurchaseOrder,
						MDocType.DOCSUBTYPESO_PurchasingContract);
		
		if (order.getC_DocType_ID() != ctrDocType_ID)
			return null;
		
		String whereClause = "C_BPartner_ID=? AND ORDERCONTRACTTYPE=? AND DocStatus = 'CO' AND "
				+ " C_DocType_ID = ? AND C_Order_ID != ? AND M_PriceList_ID=?";
		
		List<MOrder> prevContractList = 
				new Query(po.getCtx(), MOrder.Table_Name, whereClause, po.get_TrxName())
				.setParameters(order.getC_BPartner_ID(),
						X_C_Order.ORDERCONTRACTTYPE_Non_BindingContractOrder,
						ctrDocType_ID,
						po.get_ID(),
						order.getM_PriceList_ID())
				.list();
		
		for (MOrder prevContract : prevContractList)
		{
			if (!prevContract.processIt(MOrder.DOCACTION_Close))
			{
				return "Error while closing the old order (contract) of vendor [" 
						+ order.getC_BPartner().getName() + "-" + order.getDocumentNo() + "]";
			}
			prevContract.saveEx();
		}
		
//		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(C_Order_ID), ', ') FROM C_Order WHERE C_BPartner_ID=?"
//				+ " AND ORDERCONTRACTTYPE=? AND DocStatus = 'CO' AND "
//				+ " C_DocType_ID = (SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType=? AND DocSubTypeSO=?)";
//		
//		String listID = DB.getSQLValueString(po.get_TrxName(), sql, new Object[]{order.getC_BPartner_ID(),
//							X_C_Order.ORDERCONTRACTTYPE_Non_BindingContractOrder,
//							MDocType.DOCBASETYPE_PurchaseOrder,
//							MDocType.DOCSUBTYPESO_PurchasingContract});
//		if(listID != null)
//		{
//			sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DocumentNo), '; ') FROM C_Order WHERE C_Order_ID IN ("+listID+")";
//			String listDocNo = DB.getSQLValueString(po.get_TrxName(), sql);
//			int retVal = MessageBox.showMsg(order.getCtx(), order.getProcessInfo()
//					, "Order has complete. \n" +
//					  "The previous opened contract will be closed. ("+listDocNo+")"
//					, "Previous order confirmation close"
//					, MessageBox.OK
//					, MessageBox.ICONINFORMATION);
//			if(retVal == MessageBox.RETURN_OK)
//			{
//				String upOrder = "UPDATE C_Order SET DocStatus = 'CL', DocAction = '--' WHERE C_Order_ID IN("+listID+")";
//				int count = DB.executeUpdate(upOrder, po.get_TrxName());
//				if(count < 0)
//				{
//					msg.append("Failed when trying close previous order");
//				}
//			}
//			else 
//				return null;			
//		}	
		return msg.toString();
	}
	
	public String validateCreditNote(PO po, int CN_Invoice_ID)
	{
		BigDecimal totalCN = Env.ZERO;
		StringBuilder msg = new StringBuilder();
		MInvoice inv = new MInvoice(po.getCtx(), CN_Invoice_ID, po.get_TrxName());
		
		String sql = "SELECT COALESCE(SUM(invoiceopen(C_Invoice_ID, 0)),0) FROM C_Invoice WHERE CN_Invoice_ID=?"
				+ " AND DocStatus IN ('CO', 'CL')";
		BigDecimal prevTotalCN = DB.getSQLValueBD(po.get_TrxName(), sql, inv.get_ID()).negate();
		
		totalCN = prevTotalCN.add((BigDecimal) po.get_Value("GrandTotal"));
				
		if(inv.getGrandTotal().compareTo(totalCN) == -1)
			msg.append("Credit Memo value (")
			.append(totalCN.intValue())
			.append(") greater than AR Invoice (")
			.append(inv.getGrandTotal().intValue())
			.append(")");
		
		return msg.toString();
	}
	
	public String productAccessories(PO po, boolean newRecord)
	{
		StringBuilder msg = new StringBuilder();
		
		if(!po.get_ValueAsBoolean("isManual") && (newRecord || !po.is_ValueChanged("M_Product_ID")))
			return msg.toString();
		
		String columnQty = null;
		if(po.get_TableName().equals(MInvoiceLine.Table_Name))
			columnQty = X_C_InvoiceLine.COLUMNNAME_QtyEntered;
		else if(po.get_TableName().equals(MOrderLine.Table_Name))
			columnQty = X_C_OrderLine.COLUMNNAME_QtyOrdered;
	
		if(po.get_ValueAsInt("M_Product_ID") < 0)
			return msg.toString();
		if(po.is_ValueChanged("M_Product_ID"))
		{
			String sql = "DELETE FROM UNS_AccessoriesLine WHERE " + po.get_TableName() + "_ID=?";
			boolean success = DB.executeUpdate(sql, po.get_ID(), false, po.get_TrxName()) < 0 ? false : true;
			if(!success)
			{
				msg.append("Cannot clear product accessories for this product");
				return msg.toString();
			}
		}
		msg.append(MUNSAccessoriesLine.CreateAccessories(po.getCtx(), po.get_ValueAsInt("M_Product_ID"),
				(BigDecimal) po.get_Value(columnQty), po.get_ID(), po.get_TableName(), po.get_TrxName(), newRecord));
		
		return msg.toString();	
	}
	
	/* special case for order type Purchasing Contract OR Sales Contract*/
	public String unSelectionTicket(PO po)
	{
		StringBuilder msg = new StringBuilder();
		MOrder order = new MOrder(po.getCtx(), po.get_ValueAsInt("C_Order_ID"), po.get_TrxName());
		if(order.get_ID() <= 0)
			return msg.toString();
		
		MDocType dt = MDocType.get(order.getCtx(), order.getC_DocType_ID());
    	String docSubType = dt.getDocSubTypeSO();
    	
    	if (docSubType != null && (docSubType.equals(MDocType.DOCSUBTYPESO_PurchasingContract) 
    			|| docSubType.equals(MDocType.DOCSUBTYPESO_SalesContract)))
		{
			StringBuffer sql = new StringBuffer("UPDATE C_InvoiceLine il SET Description = COALESCE(Description,'') || ' || ' || 'Old Ticket Number :: '"
					+ " || (SELECT ");
			if(docSubType.equals(MDocType.DOCSUBTYPESO_PurchasingContract))
				sql.append(" wt.DocumentNo ");
			else
				sql.append(" concat(array_agg(wt.DocumentNo)) ");
			
			sql.append("FROM UNS_WeighbridgeTicket wt WHERE wt.C_InvoiceLine_ID = il.C_InvoiceLine_ID)"
					+ " WHERE C_Invoice_ID = ?");
			
			boolean unSelect = DB.executeUpdate(sql.toString(), po.get_ID(), po.get_TrxName()) < 0 ? false : true;
			if(unSelect)
			{
				String sqql = "UPDATE UNS_WeighbridgeTicket SET C_Invoice_ID = null, C_InvoiceLine_ID = null WHERE C_InvoiceLine_ID IN (SELECT C_InvoiceLine_ID"
						+ " FROM C_InvoiceLine WHERE C_Invoice_ID=?)";
				unSelect = DB.executeUpdate(sqql, po.get_ID(), po.get_TrxName()) < 0 ? false : true;;
			}
			if(!unSelect)
				msg.append("Error when trying unselect ticket");
		}
		return msg.toString();
	}
	
	public String delHandleRequest(PO po, boolean isDelete, boolean isActionComplete)
	{
		StringBuilder msg = new StringBuilder();
		
		if(isDelete)
		{
			MOrderLine line = (MOrderLine) po;
			
//			String sql = "SELECT M_RequisitionLine_ID FROM UNS_HandleRequests WHERE C_OrderLine_ID=" + line.get_ID();
//			
//			int M_RequisitionLine_ID = DB.getSQLValue(po.get_TrxName(), sql);
			
			String sql = "UPDATE M_Requisition SET DocStatus = 'CO' WHERE M_Requisition_ID IN (SELECT M_Requisition_ID"
					+ " FROM M_RequisitionLine WHERE M_RequisitionLine_ID IN (SELECT M_RequisitionLine_ID"
					+ " FROM UNS_HandleRequests WHERE C_OrderLine_ID = ?)) AND DocStatus = 'CL'";
			boolean success = DB.executeUpdate(sql, line.get_ID(), false, line.get_TrxName()) < 0 ? false : true;
			if(!success)
				msg.append("Failed when trying update status requisition.");
			
			sql = "UPDATE M_RequisitionLine SET C_OrderLine_ID = NULL WHERE C_OrderLine_ID = ?";
			success = DB.executeUpdate(sql, line.get_ID(), false, line.get_TrxName()) < 0 ? false : true;
			if(!success)
				msg.append("Failed when trying update requisition line.");
					
			String del = "DELETE FROM UNS_HandleRequests WHERE C_OrderLine_ID = ?";
			success = DB.executeUpdate(del, line.get_ID(), false, line.get_TrxName()) < 0 ? false : true;
			if(!success)
				msg.append("Failed when trying delete handle request");
			
//			MRequisition.analyzeStatusByLineChanges(po.getCtx(), M_RequisitionLine_ID, po.get_TrxName());
		}
		else if(!isDelete && !isActionComplete)
		{
			MOrderLine line = (MOrderLine) po;
			MUNSHandleRequests[] hr = MUNSHandleRequests.getByOrderLine(line.getCtx(), line.get_ID(), line.get_TrxName());
			if(hr != null && hr.length > 0)
			{
				if(hr[0].getQty().compareTo(hr[0].getM_RequisitionLine().getQty()) != 0)
				{
					BigDecimal diff = line.getQtyEntered().subtract((BigDecimal) line.get_ValueOld("QtyEntered"));
					hr[0].setQty(hr[0].getQty().add(diff));
					if(!hr[0].save())
						msg.append("Failed when trying update handle request");
				}
				
//				MRequisition.analyzeStatusByLineChanges(
//						po.getCtx(), hr[0].getM_RequisitionLine_ID(), po.get_TrxName());
			}
		}
//		else if(!isDelete && isActionComplete)
//		{
//			String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(r.M_Requisition_ID), ';') FROM M_Requisition r"
//					+ " WHERE EXISTS"
//					+ " (SELECT 1 FROM M_RequisitionLine rl WHERE rl.M_Requisition_ID = r.M_Requisition_ID"
//					+ " AND EXISTS"
//					+ " (SELECT 1 FROM UNS_HandleRequests hr WHERE hr.M_RequisitionLine_ID = rl.M_RequisitionLine_ID"
//					+ " AND EXISTS"
//					+ " (SELECT 1 FROM C_OrderLine ol WHERE ol.C_Order_ID = ? AND ol.C_OrderLine_ID = hr.C_OrderLine_ID"
//					+ " )))";
//			String values = DB.getSQLValueString(po.get_TrxName(), sql, po.get_ID());
//			if(Util.isEmpty(values, true))
//				return msg.toString();
//			
//			String value[] = values.split(";");
//			
//			for(int i = 0; i < value.length; i++)
//			{
//				com.unicore.base.model.MRequisition req = new com.unicore.base.model.MRequisition(
//						po.getCtx(), new Integer(value[i]), po.get_TrxName());
//				if("CO".equals(req.getDocStatus()) && req.isFulfilled())
//					if(!req.processIt("CL") || !req.save())
//					{
//						msg.append("Failed when trying close requisition");
//						break;
//					}
//			}
//		}
		
		return msg.toString();
	}
	
	public String checkOtherDoc(MOrder order)
	{
		PreparedStatement sta = null;
		ResultSet rs = null;
		
		//check Packing List
		String sql = "SELECT * FROM UNS_PackingList pl WHERE EXISTS(SELECT * FROM UNS_PackingList_Order WHERE isActive = 'Y'"
				+ " AND C_Order_ID = ? AND UNS_PackingList_ID = pl.UNS_PackingList_ID) AND DocStatus NOT IN ('RE','VO')";
		try{
			sta = DB.prepareStatement(sql, order.get_TrxName());
			sta.setInt(1, order.getC_Order_ID());
			rs = sta.executeQuery();
			while(rs.next())
			{
				MUNSPackingList pl = new MUNSPackingList(order.getCtx(), rs, order.get_TrxName());
				String DocStatus = pl.getDocStatus();
				boolean isProcessed = pl.isProcessed();
				
				if((DocStatus.equals(MUNSPackingList.DOCSTATUS_InProgress)
						|| DocStatus.equals(MUNSPackingList.DOCSTATUS_Invalid))
					&& isProcessed)
				{
					return "Cannot Void Order. Please contact your administrator. #Check Packing List";
				}
				else if(DocStatus.equals(MUNSPackingList.DOCSTATUS_Completed)
							|| DocStatus.equals(MUNSPackingList.DOCSTATUS_Closed))
				{
					String sqll = "SELECT 1 FROM UNS_PL_Return plr WHERE UNS_PackingList_ID = ?"
							+ " AND EXISTS(SELECT * FROM UNS_PL_ReturnOrder WHERE UNS_PackingList_Order_ID IN"
							+ " (SELECT UNS_PackingList_Order_ID FROM UNS_PackingList_Order WHERE UNS_PackingList_ID = ?)"
							+ " AND UNS_PL_Return_ID = plr.UNS_PL_Return_ID"
							+ " AND isCancelled = 'N')";
					boolean exists = DB.getSQLValue(pl.get_TrxName(), sqll, pl.getUNS_PackingList_ID(), pl.getUNS_PackingList_ID()) > 0;
					if(exists)
						return "Cannot Void Order. Please contact your administrator. #Check Packing List Return";
				}
				else if(DocStatus.equals(MUNSPackingList.DOCSTATUS_Drafted))
				{
					//unlink sales order from packing list order
					String sqql = "SELECT UNS_PackingList_Order_ID FROM UNS_PackingList_Order WHERE UNS_PackingList_ID = ?"
							+ " AND C_Order_ID = ? AND isActive = 'Y' ";
					int id = DB.getSQLValue(pl.get_TrxName(), sqql, pl.getUNS_PackingList_ID(), order.getC_Order_ID());
					if(id <= 0)
						return "Cannot get Packing List Order";
					
					MUNSPackingListOrder plo = new MUNSPackingListOrder(pl.getCtx(), id, pl.get_TrxName());
					
					if(plo.getM_InOut_ID() <= 0 && plo.getC_Invoice_ID() <= 0)
					{
						int success = DB.executeUpdate("DELETE FROM UNS_PackingList_Line WHERE UNS_PackingList_Order_ID = "
								+plo.getUNS_PackingList_Order_ID(), plo.get_TrxName());
						if(success < 0)
							return "Error when try to delete Packing List Line";
						
						success = DB.executeUpdate("DELETE FROM UNS_PackingList_Order WHERE UNS_PackingList_Order_ID = "
								+plo.getUNS_PackingList_Order_ID(), plo.get_TrxName());
						if(success < 0)
							return "Error when try to delete Packing List Order";
					}
					
				}
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
			return ex.getMessage();
		}

		//check inout
		sql = "SELECT * FROM M_InOut WHERE C_Order_ID = ? AND isActive = 'Y' AND DocStatus NOT IN ('RE','VO')";
		
		try{
			sta = DB.prepareStatement(sql, order.get_TrxName());
			sta.setInt(1, order.getC_Order_ID());
			rs = sta.executeQuery();
			while(rs.next())
			{
				MInOut inout = new MInOut(order.getCtx(), rs, order.get_TrxName());
				boolean isProcessed = inout.isProcessed();
				
				if(isProcessed)
				{
					return "Cannot Void Order. Please contact your administrator. #Check InOut";
				}
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
			return ex.getMessage();
		}
		
		//check Invoice
		sql = "SELECT * FROM C_Invoice WHERE isActive = 'Y' AND C_Order_ID = ? AND DocStatus NOT IN ('VO','RE')";
		try{
			sta = DB.prepareStatement(sql, order.get_TrxName());
			sta.setInt(1, order.getC_Order_ID());
			rs = sta.executeQuery();
			while(rs.next())
			{
				MInvoice inv = new MInvoice(order.getCtx(), rs, order.get_TrxName());
				boolean isProcessed = inv.isProcessed();
				
				if(isProcessed)
				{
					return "Cannot Void Order. Please contact your administrator. #Check Invoice";
				}
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
			return ex.getMessage();
		}
		
		return null;
	}
	
	private void autoPaid(PO po){
		
		BigDecimal grandTotal = (BigDecimal) po.get_Value("GrandTotal");
		
		if(grandTotal.compareTo(Env.ZERO) == 0)
		{
			po.set_ValueOfColumn("IsPaid", true);
			po.saveEx();
		}
		
	}
//	private void noticeUnProcessedInvoice(PO po)
//	{
//		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DocumentNo), ' :: ') FROM C_Invoice"
//				+ " WHERE DateInvoiced < ? AND C_DocType_ID = ? AND C_BPartner_ID = ? AND Processed = 'N'";
//		String listDoc = DB.getSQLValueString(po.get_TrxName(), sql,
//				new Object[]{po.get_Value("DateInvoiced"), po.get_ValueAsInt("C_DocTypeTarget_ID"),
//				po.get_ValueAsInt("C_BPartner_ID")});
//		if(!Util.isEmpty(listDoc, true))
//		{
//			MessageBox.showMsg(
//					po.getCtx(), po.getProcessInfo(), 
//					"Invoice not processed has found for this business partner, "
//					+ listDoc
//					, "Notification unprocess invoice"
//					, MessageBox.OK, MessageBox.ICONINFORMATION);
//		}		
//	}

	/**
	 * 
	 * @param po
	 * @param date
	 * @return
	 *
	private String checkDiscount (PO po, Timestamp date)
	{
		StringBuilder prepareMsg = new StringBuilder();
		MUNSDiscountTrx[] trxs = MUNSDiscountTrx.get(po);
		for(MUNSDiscountTrx trx : trxs)
		{
			if(trx.isOverBudget())
			{
				prepareMsg.append(
						"Quantiti over budget discount trx " + trx.getName());
			}
			else if (trx.isNeedRecalculate())
			{
				String msg = "Some transaction has changed by user."
						+ " to take the efect of discount you must to "
						+ " run recalculate process. Automaticaly run"
						+ " calculation ? ";
				int retVal = MessageBox.showMsg(po, po.getCtx(), msg, 
						"Recaulculate Discount ?", MessageBox.YESNO, 
						MessageBox.ICONWARNING);
				
				if (retVal == MessageBox.RETURN_NO)
				{
					return "Please run recalculate discount first";
				}
				
				I_DiscountModel model = po.get_TableName().equals(
						MOrder.Table_Name) ? new UNSOrderDiscountModel(po) 
					: po.get_TableName().equals(MInvoice.Table_Name) ? 
							new UNSInvoiceDiscountModel(po) : null;
				CalculateDiscount calc = new CalculateDiscount(model);
				calc.setIsUpdate(true);
				calc.run();
			}
			else if (trx.isChangedByUser())
			{
				prepareMsg.append(
						"Custom discount on trx " + trx.getName());
			}
			
			prepareMsg.append(checkEffectivedateOfDiscount(trx, date));
		}
		
		PO[] pos = getLines(po);
		for(PO poLine : pos)
		{
			prepareMsg.append(checkDiscount(poLine, date));
		}
		
		String msg = prepareMsg.toString();
		if(Util.isEmpty(msg, true))
		{
			return "";
		}
		
		return msg;
	}
	*/
	
	private String doUpdateCreditAgreementPaySchedule (PO po, boolean onCancel)
	{
		String msg = null;
		if (po instanceof MInvoice && onCancel)
		{
			int C_Invoice_ID = po.get_ValueAsInt("C_Invoice_ID");
			String sql = "UPDATE UNS_CreditPaySchedule SET PaidAmt = 0, "
					+ " IsPaid = 'N', PaidDate = null  WHERE C_Invoice_ID = ?";
			int updated = DB.executeUpdate(sql, po.get_ID(), false, po.get_TrxName());
			if (updated == -1)
				msg = CLogger.retrieveErrorString("Failed when try to update "
						+ "Credit Agreement Pay Schedule");
			
			if (msg == null && C_Invoice_ID > 0)
				msg = MUNSCreditAgreement.doUpdateByPaySchedule(C_Invoice_ID, po.get_TrxName());
			
			if (null == msg)
			{
				sql = "UPDATE UNS_CreditPaySchedule SET C_Invoice_ID = null WHERE C_Invoice_ID = ?";

				if (updated == -1)
					msg = CLogger.retrieveErrorString("Failed when try to update "
							+ "Credit Agreement Pay Schedule");
			}
		}
		else if (po instanceof MPayment)
		{
			Timestamp paidDate = onCancel ? null : (Timestamp)po.get_Value("DateTrx");
			String sql = "SELECT C_Invoice_ID, (invoiceamt - overunderamt) AS Amount, OverUnderAmt "
					+ " FROM C_PaymentAllocate ca WHERE C_Payment_ID = ? AND EXISTS ( "
					+ " SELECT * FROM UNS_CreditPaySchedule s WHERE s.C_Invoice_ID = "
					+ " ca.C_Invoice_ID )";
			
			PreparedStatement st = null;
			ResultSet rs = null;
			
			try
			{
				st = DB.prepareStatement(sql, po.get_TrxName());
				st.setInt(1, po.get_ID());
				rs = st.executeQuery();
				while (rs.next())
				{
					int C_Invoice_ID = rs.getInt(1);
					BigDecimal amount = rs.getBigDecimal(2);
					BigDecimal overUnder = rs.getBigDecimal(3);
					
					boolean isPaid = overUnder.signum() == 0;
					
					if (onCancel)
					{
						amount = amount.negate();
						isPaid = false;
					}
					
					sql = "UPDATE UNS_CreditPaySchedule SET PaidAmt = PaidAmt + ?, PaidDate = ?, "
							+ " IsPaid = ? WHERE C_Invoice_ID = ?";
					int updated = DB.executeUpdate(sql, new Object[] {amount, paidDate, 
							isPaid ? "Y" : "N", C_Invoice_ID}, 
							false, po.get_TrxName());
					if (updated == -1)
						msg = CLogger.retrieveErrorString("Failed when try to update "
								+ "Credit Agreement Pay Schedule");
					
					if (msg == null && C_Invoice_ID > 0)
						msg = MUNSCreditAgreement.doUpdateByPaySchedule(
								C_Invoice_ID, po.get_TrxName());
					
					if (null != msg)
						break;
				}
			}
			catch (SQLException ex)
			{
				msg = ex.getMessage();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		else if (po instanceof MInOut && onCancel)
		{
			String sql = "UPDATE UNS_CreditAgreement SET M_InOut_ID = null WHERE M_InOut_ID = ?";
			int updated = DB.executeUpdate(sql, po.get_ID(), false, po.get_TrxName());
			if (updated == -1)
				msg = CLogger.retrieveErrorString("Failed when try to update Credit Agreement");
		}
		
		return msg;
	}
	
//	private String upQtyOnHand(PO po)
//	{
//		MRequisitionLine line = (com.unicore.base.model.MRequisitionLine) po;
//		com.unicore.base.model.MRequisition req = 
//				new com.unicore.base.model.MRequisition(line.getCtx(), line.getM_Requisition_ID(), line.get_TrxName());
//		
//		BigDecimal qty = MStorageOnHand.getQtyOnHandForLocator(line.getM_Product_ID(), req.getLocator().get_ID(),
//				0, line.get_TrxName());
//		
//		line.setQtyOnHand(qty);
//		
//		return null;
//	}
	
	private String upPriceListOnOrder(PO po)
	{
		MOrder order = (MOrder) po;
		if(!order.isUpdatePriceList())
			return null;
		
		MOrderLine[] lines = order.getLines();
		
		MPriceListVersion v = MPriceListVersion.getSameValidFrom(order.getCtx(), order.getM_PriceList_ID(), order.getDateOrdered(),
				order.get_TrxName());
		if(v == null)
		{
			v = new MPriceListVersion((MPriceList) order.getM_PriceList());
			v.setValidFrom(order.getDateOrdered());
			v.saveEx();
		}
		
		for(int i=0;i<lines.length;i++)
		{
			MProductPrice pp = MProductPrice.get(order.getCtx(), v.get_ID(), lines[i].getM_Product_ID(), order.get_TrxName());
			if(pp == null)
				pp = new MProductPrice(order.getCtx(), v.get_ID(), lines[i].getM_Product_ID(), order.get_TrxName());
			
			pp.setPriceList(lines[i].getPriceList());
			pp.setPriceStd(lines[i].getPriceList());
			pp.setPriceLimit(lines[i].getPriceList());
			pp.saveEx();
		}
		
		return null;
	}
	
	private String validateOrderLine(PO po)
	{	
		if(!po.is_ValueChanged("LineNetAmt"))
			return null;
		String sql = "SELECT SUM(LineNetAmt) FROM M_RequisitionLine"
				+ " WHERE C_OrderLine_ID = ?";
		BigDecimal lineNetAmt = DB.getSQLValueBD(po.get_TrxName(), sql, po.get_ID());
		if(lineNetAmt == null || lineNetAmt.signum() == 0)
			return null;
		
		MOrderLine line = (MOrderLine) po;
		String procurementType = line.getC_Order().getC_DocTypeTarget().getDocSubTypePO();
		if(procurementType == null)
			return null;
		BigDecimal tolerance = MUNSOrderValidationConf.getPercent(procurementType);
		if(tolerance.compareTo(Env.ZERO) == 0)
			return null;
		if(lineNetAmt.compareTo(line.getLineNetAmt()) >= 0)
		{
			line.setIsNeedApproval(false);
			return null;
		}
		
		BigDecimal diff = line.getLineNetAmt().subtract(lineNetAmt);
		diff = diff.divide(line.getLineNetAmt(),2);
		diff = diff.multiply(Env.ONEHUNDRED);
		
		if(diff.compareTo(tolerance) == 1)
			line.setIsNeedApproval(true);
			
		return null;
	}
}

class PhysicalTmp {
	int M_Locator_ID = 0;
	int M_Product_ID = 0;
	int M_AttributeSetInstance_ID = 0;
	BigDecimal Quantity = Env.ZERO;
	
	public PhysicalTmp(
			int M_Locator_ID, int M_Product_ID,int M_AttributeSetInstance_ID, BigDecimal qty)
	{
		this.M_Locator_ID = M_Locator_ID;
		this.M_Product_ID = M_Product_ID;
		this.M_AttributeSetInstance_ID = M_AttributeSetInstance_ID;
		this.Quantity = qty;
	}
}

class ThreadSave implements Runnable {
	private Trx m_trx;

	@Override
	public void run() {
		m_trx.commit();
		m_trx.close();
	}
	
	public ThreadSave(Trx trx) {
		m_trx = trx;
	}
}