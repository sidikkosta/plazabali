/**
 * 
 */
package com.unicore.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Hashtable;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.IProcessUI;
import org.compiere.model.MDocType;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class ReInitSalesReconciliation extends SvrProcess {

	/**
	 * 
	 */
	public ReInitSalesReconciliation() {
		// TODO Auto-generated constructor stub
	}
	
	private int p_LocationID = 0;
	private Timestamp p_DateStart = null;
	private Timestamp p_EndDate = null;
	private IProcessUI m_process;
	private boolean p_DeleteCompletedTrx = false;
	private boolean p_isFromWindow = false;
	private int p_SalesReconciliationID = 0;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			if (param.getParameterName().equals("C_BP_Group_ID"))
				p_LocationID = param.getParameterAsInt();
			else if (param.getParameterName().equals("DateStart"))
				p_DateStart = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("EndDate"))
				p_EndDate = param.getParameterAsTimestamp();
			else if(param.getParameterName().equals("DeleteCompletedTrx"))
				p_DeleteCompletedTrx = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("IsFromWindow"))
				p_isFromWindow = param.getParameterAsBoolean();
			else if(param.getParameterName().equals("UNS_SalesReconciliation_ID"))
				p_SalesReconciliationID = param.getParameterAsInt();
		}
		
		if(p_isFromWindow && p_SalesReconciliationID <= 0)
			throw new AdempiereException("Record ID can't null or 0.");
		
		if(p_isFromWindow)
		{
			MUNSSalesReconciliation sr = new MUNSSalesReconciliation(getCtx(), p_SalesReconciliationID, get_TrxName());
			p_DateStart = sr.getDateTrx();
			p_EndDate = sr.getDateTrx();
			p_LocationID = sr.getC_BP_Group_ID();
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		if(!p_isFromWindow)
		{
			m_process = Env.getProcessUI(getCtx());
			m_process.statusUpdate("Sedang hapus data yang sudah ada.");
		}
		if(!deleteExistingRecord())
			return "Failed when trying delete existing record.";
		
		initReconciliationByTrx();
		initReconciliationBySession();
		
		String sql = "UPDATE UNS_SalesReconciliation s SET IsValidated = 'Y' WHERE 1=1 " + getWhereClause();
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			throw new AdempiereException("failed when trying update is Validated.");
		
		return "Success";
	}
	
	private boolean deleteExistingRecord()
	{
		//UPDATE POSTrxLine
		String sql = "UPDATE UNS_POSTrxLine ptl SET UNS_POSRecapLine_ID = NULL WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_POSRecapLine l WHERE l.UNS_POSRecapLine_ID = ptl.UNS_POSRecapLine_ID AND EXISTS"
				+ " (SELECT 1 FROM UNS_POSRecap r WHERE r.UNS_POSRecap_ID = l.UNS_POSRecap_ID AND EXISTS"
				+ " (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID = r.UNS_SalesReconciliation_ID "
				+ getWhereClause() + ")))";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		//DELETE POSRecapLine_MA
		sql = "DELETE FROM UNS_POSRecapLine_MA ma WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_POSRecapLine l WHERE l.UNS_POSRecapLine_ID = ma.UNS_POSRecapLine_ID AND EXISTS"
				+ " (SELECT 1 FROM UNS_POSRecap r WHERE r.UNS_POSRecap_ID = l.UNS_POSRecap_ID AND EXISTS"
				+ " (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID = r.UNS_SalesReconciliation_ID "
				+ getWhereClause() + ")))";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		//DELETE POSRecapLine
		sql = "DELETE FROM UNS_POSRecapLine l WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_POSRecap r WHERE r.UNS_POSRecap_ID = l.UNS_POSRecap_ID AND EXISTS"
				+ " (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID = r.UNS_SalesReconciliation_ID "
				+ getWhereClause() + "))";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		//DELETE POSRecap
		sql = "DELETE FROM UNS_POSRecap r WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID = r.UNS_SalesReconciliation_ID "
				+ getWhereClause() + ")";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		//DELETE CashReconciliation
		sql = "DELETE FROM UNS_CashReconciliation r WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID = r.UNS_SalesReconciliation_ID "
				+ getWhereClause() + ")";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		//DELETE EDCReconciliation
		sql = "DELETE FROM UNS_EDCReconciliation r WHERE EXISTS"
				+ " (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID = r.UNS_SalesReconciliation_ID "
				+ getWhereClause() + ")";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		//DELETE FactAcct
		sql = "DELETE FROM Fact_Acct fa WHERE fa.AD_Table_ID = " + MUNSSalesReconciliation.Table_ID 
				+ " AND EXISTS (SELECT 1 FROM UNS_SalesReconciliation s WHERE s.UNS_SalesReconciliation_ID"
				+ " = fa.Record_ID " + getWhereClause() + ")";
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		if(p_isFromWindow)
			return true;
		//DELETE SalesReconciliation
		sql = " DELETE FROM UNS_SalesReconciliation s WHERE 1=1"
				+ getWhereClause();
		if(DB.executeUpdate(sql, false, get_TrxName()) < 0)
			return false;
		
		return true;
	}
	
	private String getWhereClause()
	{
		String whereClause = "";
		if(p_isFromWindow)
			return " AND s.UNS_SalesReconciliation_ID = " + p_SalesReconciliationID;
		if(!p_DeleteCompletedTrx)
			whereClause += " AND s.DocStatus NOT IN ('CO', 'CL')";
		if(p_LocationID > 0)
			whereClause += " AND s.C_BP_Group_ID = " + p_LocationID;
		if(p_DateStart != null)
			whereClause += " AND s.DateTrx >= '" + p_DateStart + "'";
		if(p_EndDate != null)
			whereClause += " AND s.DateTrx <= '" + p_EndDate + "'";
		
		return whereClause;
	}
	
	private Hashtable<Integer, MUNSPOSSession> mapSession = new Hashtable<>();
	
	private void initReconciliationByTrx()
	{
		String sql = "SELECT COUNT(pt.*) FROM UNS_POSTrx pt"
				+ " INNER JOIN UNS_POS_Session sessi ON sessi.UNS_POS_Session_ID = pt.UNS_POS_Session_ID"
				+ " WHERE pt.DocStatus IN ('CO', 'CL') AND sessi.IsTrialMode = 'N'";
		
		if(p_LocationID > 0)
			sql += " AND EXISTS (SELECT 1 FROM UNS_POS_Session ss WHERE ss.UNS_POS_Session_ID"
					+ " = pt.UNS_POS_Session_ID AND EXISTS (SELECT 1 FROM UNS_POSTerminal tm"
					+ " WHERE tm.UNS_POSTerminal_ID = ss.UNS_POSTerminal_ID AND EXISTS"
					+ " (SELECT 1 FROM C_BPartner bp WHERE bp.C_BPartner_ID = tm.Store_ID"
					+ " AND bp.C_BP_Group_ID = " + p_LocationID + ")))";
		if(p_DateStart != null)
			sql += " AND pt.DateAcct >= '" + p_DateStart + "'";
		if(p_EndDate != null)
			sql += " AND pt.DateAcct <= '" + p_EndDate + "'";
		
		int count = DB.getSQLValue(get_TrxName(), sql);
		
		sql = sql.replace("COUNT(pt.*)", "pt.*");
		
		sql += " ORDER BY pt.DateAcct";;
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				MUNSPOSTrx trx = new MUNSPOSTrx(getCtx(), rs, get_TrxName());
				System.out.println(trx.getDocumentNo() + "-" + trx.getTrxNo());
				if(!p_isFromWindow)
					m_process.statusUpdate("Sedang init ulang dari " + count + " pos transaction."
							+ " Data ke " + rs.getRow() + " tanggal "
							+ trx.getDateAcct());
				boolean isRefund = MDocType.DOCBASETYPE_POSReturn.equals(trx.getC_DocType().getDocBaseType());
				trx.createReconciliation(isRefund, false);
				mapSession.put(trx.getUNS_POS_Session_ID(), 
						new MUNSPOSSession(getCtx(), trx.getUNS_POS_Session_ID(), get_TrxName()));
			}
		} catch (SQLException e) {
			throw new AdempiereException("Failed when trying load pos transaction. " + e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
	}
	
	private void initReconciliationBySession()
	{
		Enumeration<Integer> mapKey = mapSession.keys();
		int count = 1;
		while (mapKey.hasMoreElements())
		{
			MUNSPOSSession session = mapSession.get(mapKey.nextElement());
			if(!p_isFromWindow)
			{
				m_process.statusUpdate("Sedang init ulang dari " + mapSession.size() + " pos session."
						+ " Data ke " + count + " tanggal " + session.getDateAcct());
				count++;
			}
			if(session == null || (!session.getDocStatus().equals("CO") && !session.getDocStatus().equals("CL")))
				continue;
			
			if(!session.isTrialMode())
			{
				MUNSCashReconciliation.createFrom(session, false);
				MUNSEDCReconciliation.createFrom(session, false);
				MUNSPOSRecap.createFrom(session);
				session.setIsReconciled(true);
				session.saveEx();
			}
		}
	}
}
