/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoiceLine;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MOrderLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author UNTA_Andy
 *
 */
public class MUNSPLConfirmLine extends X_UNS_PL_ConfirmLine
{
		
	/*
	 * 
	 */
	private static final long serialVersionUID = 3844404755693468053L;
	private MUNSPLConfirmProduct m_parent;

	/**
	 * @param ctx
	 * @param UNS_PL_ConfirmLine_ID
	 * @param trxName
	 */
	public MUNSPLConfirmLine(Properties ctx, int UNS_PL_ConfirmLine_ID, String trxName)
	{
		super(ctx, UNS_PL_ConfirmLine_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPLConfirmLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		String sql = "SELECT plConfirm.ConfirmType FROM UNS_PL_Confirm plConfirm "
				+ "	INNER JOIN UNS_PL_ConfirmProduct cProduct ON plConfirm.UNS_PL_Confirm_ID=cProduct.UNS_PL_Confirm_ID "
				+ " WHERE cProduct.UNS_PL_ConfirmProduct_ID=?";
		String confirmType = DB.getSQLValueStringEx(get_TrxName(), sql, getUNS_PL_ConfirmProduct_ID());
		
		if (confirmType.equals(MUNSPLConfirm.CONFIRMTYPE_ShipmentReturnConfirmation)
				&& getQtyEntered().compareTo(getTargetQty()) > 0)
		{
			log.saveError("ConfirmQtyMustLessThanTargetQty", 
					"Confirmed quantity must be equals or less than target qty.");
			return false;
		}
		return true;
	}
	
	protected boolean isSavingProductList = false;
	
	@Override
	protected boolean afterSave (boolean newRecord, boolean sucess)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_QtyEntered))
		{
			if (!isSavingProductList)
			{				
				//update PL Confirm Product
				String sql = "SELECT SUM(QtyEntered) FROM UNS_PL_ConfirmLine WHERE UNS_PL_ConfirmProduct_ID=?";
				BigDecimal sumQty = DB.getSQLValueBDEx(get_TrxName(), sql, getUNS_PL_ConfirmProduct_ID());
				
				if (sumQty == null)
					sumQty = Env.ZERO;

				/** enhancement -- tune up the process. */
				sql = "UPDATE UNS_PL_ConfirmProduct SET ConfirmedQty = ?"// + sumQty
						//+ ", MovementQty = ?" + sumQty +", DifferenceQty = (? - TargetQty)"// + sumQty.subtract(getUNS_PL_ConfirmProduct().getTargetQty())
						+ ", MovementQty = ?, DifferenceQty = (? - TargetQty) "
						+ " WHERE UNS_PL_ConfirmProduct_ID = " + getUNS_PL_ConfirmProduct_ID();
				int count = DB.executeUpdateEx(sql, new Object[]{sumQty, sumQty, sumQty}, get_TrxName());
				if (count <= 0) {
					log.saveError("CannotUpdateProductList", "Cannot update product list confirmed quantity.");
				}
			}
			
//			BigDecimal diffQtyPacked = getTargetQty().subtract(getQtyEntered());
//
//			String sql = "SELECT plConfirm.ConfirmType FROM UNS_PL_Confirm plConfirm "
//					+ "	INNER JOIN UNS_PL_ConfirmProduct cProduct ON plConfirm.UNS_PL_Confirm_ID=cProduct.UNS_PL_Confirm_ID "
//					+ " WHERE cProduct.UNS_PL_ConfirmProduct_ID=?";
//			String confirmType = DB.getSQLValueStringEx(get_TrxName(), sql, getUNS_PL_ConfirmProduct_ID());
//			
//			if (confirmType.equals(MUNSPLConfirm.CONFIRMTYPE_ShipmentReturnConfirmation))
//			{
//				diffQtyPacked = getQtyEntered();
//			}
//				
//			sql = "UPDATE C_OrderLine SET QtyPacked=(QtyPacked - ?) WHERE C_OrderLine_ID=" + getC_OrderLine_ID();
//			int count = DB.executeUpdateEx(sql, new Object[]{diffQtyPacked}, get_TrxName());
//			if (count <= 0) {
//				log.saveError("CannotOrderLineQtyPacked", "Cannot update order line's quantity packed.");
//				return false;
//			}
		}

		return super.afterSave(newRecord, sucess);
	} // afterSave
	
	/**
	 * Update the shipment and invoice line of this confirmed line.
	 * 
	 * @return
	 */
	protected String confirmLine()
	{
		String sql = "SELECT plConfirm.ConfirmType FROM UNS_PL_Confirm plConfirm "
				+ "	INNER JOIN UNS_PL_ConfirmProduct cProduct ON plConfirm.UNS_PL_Confirm_ID=cProduct.UNS_PL_Confirm_ID "
				+ " WHERE cProduct.UNS_PL_ConfirmProduct_ID=?";
		String confirmType = DB.getSQLValueStringEx(get_TrxName(), sql, getUNS_PL_ConfirmProduct_ID());
		
		BigDecimal shipMovementQty = getQtyEntered();
		
		if (confirmType.equals(MUNSPLConfirm.CONFIRMTYPE_ShipmentReturnConfirmation)) {
			sql = "SELECT pll.MovementQty FROM UNS_PackingList_Line pll "
					+ " WHERE pll.UNS_PackingList_line_ID=" + getUNS_PackingList_Line_ID();
			BigDecimal targetQty = DB.getSQLValueBDEx(get_TrxName(), sql);
			
			shipMovementQty = targetQty.subtract(getQtyEntered());
		}
		
		int pll_id = getUNS_PackingList_Line_ID();
		
		sql = "UPDATE UNS_PackingList_Line SET MovementQty=? WHERE UNS_PackingList_Line_ID=?";
		int count = DB.executeUpdateEx(sql, new Object[]{shipMovementQty, pll_id}, get_TrxName());
		
		if (count <= 0) {
			return "Error while updating packing list line's movement quantity.";
		}
		
		boolean isFullShipment = false;
		BigDecimal qtyBonus = Env.ZERO;
		if(getC_OrderLine_ID() > 0)
		{
			MOrderLine line = new MOrderLine(getCtx(), getC_OrderLine_ID(), get_TrxName());
			isFullShipment = line.getQtyOrdered() == shipMovementQty;
			qtyBonus = line.getQtyBonuses();
		}
		
		//update Invoice Line
		if(getC_InvoiceLine_ID() > 0)
		{
			MInvoiceLine invLine = new MInvoiceLine(getCtx(), getC_InvoiceLine_ID(), get_TrxName());
	
			if (isFullShipment)
			{
				invLine.setQtyBonuses(qtyBonus);
				invLine.setQty(shipMovementQty);
			}
			else {
				invLine.setQty(shipMovementQty);
			}
			invLine.saveEx();
		}
		
		//update Shipment Line
		int iol_id = getM_InOutLine_ID();
		
		MInOutLine ioLine = new MInOutLine(getCtx(), iol_id, get_TrxName());
		ioLine.setQty(shipMovementQty);
		ioLine.saveEx();
		
		BigDecimal diffQtyPacked = getTargetQty().subtract(getQtyEntered());

		if (confirmType.equals(MUNSPLConfirm.CONFIRMTYPE_ShipmentReturnConfirmation))
		{
			diffQtyPacked = getQtyEntered();
		}
			
		if (getC_OrderLine_ID() > 0) 
		{
			sql = "UPDATE C_OrderLine SET QtyPacked=(QtyPacked - ?) WHERE C_OrderLine_ID=" + getC_OrderLine_ID();
			count = DB.executeUpdateEx(sql, new Object[]{diffQtyPacked}, get_TrxName());
			if (count <= 0) {
				return "Cannot update order line's quantity packed.";
			}
		}
		
		return null;
	} // confirmLine

	public static MUNSPLConfirmLine getNewConfirmLine(MUNSPackingListLine PLLine, MUNSPLConfirmProduct header)
	{
		MUNSPLConfirmLine confirm = null;
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmLine.COLUMNNAME_UNS_PackingList_Line_ID).append("=? AND ")
				.append(MUNSPLConfirmLine.COLUMNNAME_UNS_PL_ConfirmProduct_ID).append("=?");

		confirm =
				Query
						.get(PLLine.getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmLine.Table_Name,
								whereClauseFinal.toString(), PLLine.get_TrxName())
						.setParameters(PLLine.get_ID(), header.get_ID()).firstOnly();

		if (confirm == null)
			confirm = new MUNSPLConfirmLine(PLLine.getCtx(), 0, PLLine.get_TrxName());
		
		return confirm;
	}

	public static MUNSPLConfirmLine getNewReturnConfirmLine(MUNSPLReturnLine returnLine, MUNSPLConfirmProduct header)
	{
		
		String sql = "SELECT pll.UNS_PackingList_Line_ID FROM UNS_PackingList_Line pll "
				+ "	WHERE pll.UNS_PackingList_Order_ID=? AND M_Product_ID=? AND M_Locator_ID=? AND isProductAccessories=?";
		
		if (returnLine.isProductAccessories()) 
		{
			String parentProductSQL = "SELECT M_Product_ID FROM UNS_PL_ReturnLine "
					+ " WHERE UNS_PL_ReturnLine_ID=" + returnLine.getPLReturnLine_ID();

			int parentProduct_ID = DB.getSQLValueEx(returnLine.get_TrxName(), parentProductSQL);
			
			String sqlPLFromAcc = "SELECT UNS_PackingList_Line_ID FROM UNS_PackingList_Line "
					+ "	WHERE UNS_PackingList_Order_ID=? AND M_Product_ID=? AND isProductAccessories='N' AND M_Locator_ID=?";
			
			int parentProductPLL_ID = DB.getSQLValueEx(returnLine.get_TrxName(), sqlPLFromAcc, 
					returnLine.getUNS_PackingList_Order_ID(), parentProduct_ID, returnLine.getM_Locator_ID());
			
			sql += " AND PackingList_Line_ID=" + parentProductPLL_ID;
		}
		
		int pll_ID = DB.getSQLValueEx(header.get_TrxName(), sql, returnLine.getUNS_PackingList_Order_ID(), 
				returnLine.getM_Product_ID(), returnLine.getM_Locator_ID(), returnLine.isProductAccessories());
		
		MUNSPLConfirmLine confirm = null;
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmLine.COLUMNNAME_UNS_PL_ConfirmProduct_ID).append("=?")
				//.append(" AND ").append(MUNSPLConfirmLine.COLUMNNAME_M_InOutLine_ID).append("=?")
				.append(" AND ").append(MUNSPLConfirmLine.COLUMNNAME_M_Product_ID).append("=?")
				.append(" AND UNS_PackingList_Line_ID=?");
				//.append(" AND UNS_PackingList_Line_ID= ("
				//		+ "		SELECT pll.UNS_PackingList_Line_ID FROM UNS_PackingList_Line pll "
				//		+ "		WHERE pll.UNS_PackingList_Order_ID=? AND pll.M_Product_ID=UNS_PL_ConfirmLine.M_Product_ID"
				//		+ "			pll.M_Locator_ID=? AND pll.isProductAccessories=?)");

		confirm = Query.get(
					returnLine.getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmLine.Table_Name,
					whereClauseFinal.toString(), returnLine.get_TrxName())
				.setParameters(header.getUNS_PL_ConfirmProduct_ID(), returnLine.getM_Product_ID(), pll_ID)
				.firstOnly();

		if (confirm == null) {
			confirm = new MUNSPLConfirmLine(returnLine.getCtx(), 0, returnLine.get_TrxName());
			confirm.setUNS_PackingList_Line_ID(pll_ID);
		}
		
		return confirm;
	}

	public void setHeader(MUNSPLConfirmProduct plCProduct)
	{
		setClientOrg(plCProduct);
		setM_Product_ID(plCProduct.getM_Product_ID());
		setUNS_PL_ConfirmProduct_ID(plCProduct.get_ID());
		
		m_parent = plCProduct;
	}
	
	public MUNSPLConfirmProduct getParent(){
		if (m_parent == null || m_parent.get_ID() <= 0)
			m_parent = (MUNSPLConfirmProduct) getUNS_PL_ConfirmProduct();
		
		return m_parent;
		
	}

	public void setLine(MUNSPackingListLine line)
	{
		setUNS_PackingList_Line_ID(line.get_ID());
		
		setQtyEntered(line.getQtyEntered());
		setTargetQty(line.getTargetQty());
	}
}
