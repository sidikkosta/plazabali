/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.I_M_Warehouse;
import org.compiere.model.MClient;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInOutLineMA;
import org.compiere.model.MLocator;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTransaction;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.compiere.wf.MWorkflow;
import com.unicore.base.model.MInvoiceLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.model.MProduct;
import com.uns.util.MessageBox;

/**
 * 
 * 
 */
public class MUNSPLConfirm extends X_UNS_PL_Confirm implements DocAction
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7908608207497071993L;
	
	public static final String COMPLETE_CONFIRM_CTX = "COMPLETE_CONFIRM_";
	public static final String CONFIRM_CTX_VALUE = "Y";

	/**
	 * @param ctx
	 * @param UNS_PL_Confirm_ID
	 * @param trxName
	 */
	public MUNSPLConfirm(Properties ctx, int UNS_PL_Confirm_ID, String trxName)
	{
		super(ctx, UNS_PL_Confirm_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPLConfirm(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Check if it is currently auto completing Packing List (Return).
	 * @param ctx
	 * @param targetPLDocNo
	 * @return true if currently the context says so.
	 */
	public static boolean isCompleting(Properties ctx, String targetPLDocNo)
	{
		String confirmingCtx = COMPLETE_CONFIRM_CTX + targetPLDocNo;
		
		String valCtx = Env.getContext(ctx, confirmingCtx);
		
		return valCtx.equals(CONFIRM_CTX_VALUE);
	} // isCompleting

	/**************************************************************************
	 * Process document
	 * 
	 * @param processAction document action
	 * @return true if performed
	 */
	public boolean processIt(String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(processAction, getDocAction());
	} // processIt

	/** Process Message */
	String m_processMsg = null;
	/** Just Prepared Flag */
	private boolean m_justPrepared = false;
	private MUNSPLConfirmProduct[] m_Products;

	/**
	 * Unlock Document.
	 * 
	 * @return true if success
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	} // unlockIt

	/**
	 * Invalidate Document
	 * 
	 * @return true if success
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	} // invalidateIt

	/**************************************************************************
	 * Prepare Document
	 * 
	 * @return new status (In Progress or Invalid)
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		// Lines
		MUNSPLConfirmProduct[] products = getProducts(true, null);
		if (products.length == 0)
		{
			m_processMsg = "@NoLines@";
			return DocAction.STATUS_Invalid;
		}

		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		m_justPrepared = true;
		setProcessed(true);

		return DocAction.STATUS_InProgress;
	} // prepareIt

	/**
	 * Approve Document
	 * 
	 * @return true if success
	 */
	public boolean approveIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	} // approveIt

	/**
	 * Reject Approval
	 * 
	 * @return true if success
	 */
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	} // rejectIt

	@Override
	public String completeIt()
	{
//		// Just prepare
//		if (DOCACTION_Prepare.equals(getDocAction()))
//		{
//			setProcessed(false);
//			return DocAction.STATUS_InProgress;
//		}
//
		// Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

//		if (!isReversal() && getConfirmType().endsWith(CONFIRMTYPE_PackingListConfirmation))
//		{
//			MUNSShipping shipping = MUNSShipping.getShipping((MUNSPackingList) getUNS_PackingList());
//			if (shipping == null || shipping.get_ID() == 0)
//			{
//				String msg = Msg.getMsg(getCtx(), "CompletePLWithoutShippingMsg");
//				String title = Msg.getMsg(getCtx(), "CompletePLWithoutShippingTitle");
//				int userChoise = MessageBox.showMsg(this, 
//						getCtx(), msg, title, MessageBox.YESNO, MessageBox.ICONQUESTION);
//				
//				if(userChoise == MessageBox.RETURN_NO)
//				{
//					m_processMsg = "Proses dibatalkan. Silahkan buat dokumen pengiriman terlebih dahulu.";
//				}
//			}
//		}
//		if (m_processMsg != null)
//		{
//			return DocAction.STATUS_Invalid;
//		}
		

		if (!isReversal())// && getConfirmType().equals(CONFIRMTYPE_PackingListConfirmation))
		{
			confirmLines();
		}
		
		// Implicit Approval
		if (!isApproved())
			approveIt();

		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (getConfirmType().equals(CONFIRMTYPE_PackingListConfirmation) && getUNS_PackingList_ID() > 0)
		{
			MUNSPackingList pl = new MUNSPackingList(getCtx(), getUNS_PackingList_ID(), get_TrxName());
			
			if (pl.isAutoCompleteAfterConfirm() && !isReversal())
			{
				String confirmCtx = COMPLETE_CONFIRM_CTX + pl.getDocumentNo();
				
				Env.setContext(pl.getCtx(), confirmCtx, CONFIRM_CTX_VALUE);
				ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(pl, DocAction.ACTION_Complete);
				
				if(pi.isError())
				{
					m_processMsg = "Warning: The Packing List cannot automatically be completed. "
							+ "Please complete it manually!";
				}
				
				pl.getCtx().remove(confirmCtx);
				pl.saveEx();
			}
		}
		else if (getUNS_PL_Return_ID() > 0) 
		{
			MUNSPLReturn plReturn = new MUNSPLReturn(getCtx(), getUNS_PL_Return_ID(), get_TrxName());
			
			if (plReturn.isAutoCompleteAfterConfirm() && !isReversal())
			{
				String confirmCtx = COMPLETE_CONFIRM_CTX + plReturn.getDocumentNo();

				Env.setContext(plReturn.getCtx(), confirmCtx, CONFIRM_CTX_VALUE);
				ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(plReturn, DocAction.ACTION_Complete);
				
				if(pi.isError())
				{
					m_processMsg = "Warning: The Packing List Return cannot automatically be completed. "
							+ "Please complete it manually!";
				}
				
				plReturn.getCtx().remove(confirmCtx);
				plReturn.saveEx();
			}
		}

		setProcessed(true);
//		m_processMsg = info.toString();
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt
	
	/**
	 * 
	 */
	private void confirmLines()
	{
		String whereClause = "UNS_PL_ConfirmProduct_ID IN (SELECT plcp.UNS_PL_ConfirmProduct_ID "
				+ "		FROM UNS_PL_ConfirmProduct plcp WHERE plcp.UNS_PL_Confirm_ID=?)";
				//+ " AND QtyEntered <> TargetQty";
		
		List<MUNSPLConfirmLine> confirmLines =
				Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmLine.Table_Name, 
						whereClause, get_TrxName())
					.setParameters(getUNS_PL_Confirm_ID())
					.setOrderBy(MUNSPLConfirmLine.COLUMNNAME_UNS_PackingList_Line_ID)
					.list();
		
		for (int i=0; i < confirmLines.size(); i++)
 		{
			MUNSPLConfirmLine line = confirmLines.get(i);
 			String errMsg = line.confirmLine();
 			if (errMsg != null)
				throw new AdempiereException(errMsg);
		}
	} // confirmLines

	/**
	 * 
	 * @param munsplConfirm
	 * @param shipping
	 * @param isShipment
	 * @return
	 */
	protected String move()
	{
		StringBuilder diffConfirmedMsg = new StringBuilder();
		
		for (MUNSPLConfirmProduct cp : getProducts())
		{
			if(!cp.getM_Product().isStocked())
				continue;
			List<ValueMapping> attrMaps = new ArrayList<>();
			BigDecimal totalQty = Env.ZERO;
			MLocator intransitLoc = null;
			MWarehouse wh = MWarehouse.get(getCtx(), cp.getM_Warehouse_ID());
			int M_AttributeSetInstance_ID = cp.getM_AttributeSetInstance_ID();
			MLocator[] locators = wh.getLocators(true);
			for(int i=0; i<locators.length; i++)
			{
				if(!locators[i].isActive())
					continue;
				if(!locators[i].getValue().equals(wh.getValue().concat("-").concat(MWarehouse.INITIAL_INTRANSIT_CUSTOMER_LOC)))
					continue;
				if(!locators[i].isInTransit())
					continue;
				intransitLoc = locators[i];
				break;
			}
			
			if(intransitLoc == null)
			{
				intransitLoc = new MLocator(wh, wh.getValue());
				intransitLoc.setValue(wh.getValue().concat("-").concat(MWarehouse.INITIAL_INTRANSIT_CUSTOMER_LOC));
				intransitLoc.setX(wh.getValue());
				intransitLoc.setY(wh.getName());
				intransitLoc.setZ(MWarehouse.INITIAL_INTRANSIT_CUSTOMER_LOC);
				intransitLoc.setIsInTransit(true);
				intransitLoc.saveEx();
			}
			
			if(M_AttributeSetInstance_ID > 0)
			{
				if (!MStorageOnHand.add(getCtx(), cp.getM_Warehouse_ID(), cp.getM_Locator_ID(),
						cp.getM_Product_ID(), M_AttributeSetInstance_ID
						, (isShipment() && !isReversal() ? cp.getConfirmedQty().negate() : cp.getConfirmedQty())
						, null, get_TrxName()))
				{
					{
						String lastError = CLogger.retrieveErrorString("");
						return "Cannot correct Inventory OnHand (MA) - " + lastError;
					}
				}

				// move to armada locator
				if (!MStorageOnHand.add(getCtx(), intransitLoc.getM_Warehouse_ID(), intransitLoc.get_ID()
						, cp.getM_Product_ID(), M_AttributeSetInstance_ID, 
						(isShipment() && !isReversal()? cp.getConfirmedQty() : cp.getConfirmedQty().negate())
						, null, get_TrxName()))
				{

					String lastError = CLogger.retrieveErrorString("");
					return "Cannot correct Inventory OnHand (MA) - " + lastError;
				}

				//
				MTransaction trxFrom =
						new MTransaction(getCtx(), cp.getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementFrom,
								cp.getM_Locator_ID(), cp.getM_Product_ID(),M_AttributeSetInstance_ID
								, (isShipment() && !isReversal() ? cp.getConfirmedQty().negate() : cp.getConfirmedQty()),
								getDateDoc(), get_TrxName());
				trxFrom.setUNS_PL_ConfirmProduct_ID(cp.get_ID());
				if (!trxFrom.save())
				{
					m_processMsg = "Transaction From not inserted";
					return DocAction.STATUS_Invalid;
				}
				//
				MTransaction trxTo =
						new MTransaction(getCtx(), getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementTo,
								intransitLoc.get_ID(), cp.getM_Product_ID(), M_AttributeSetInstance_ID
								, (isShipment() && !isReversal() ? cp.getConfirmedQty() : cp.getConfirmedQty().negate())
								, getDateDoc(), get_TrxName());
				trxTo.setUNS_PL_ConfirmProduct_ID(cp.get_ID());
				if (!trxTo.save())
				{
					m_processMsg = "Transaction To not inserted";
					return DocAction.STATUS_Invalid;
				}
				
				totalQty = totalQty.add(cp.getConfirmedQty());
			}
			else
			{
				initialProductAttribute(cp);
				MUNSPLConfirmMA[] mas = cp.getMAs(true, null);
				for(MUNSPLConfirmMA ma : mas)
				{
					if (!MStorageOnHand.add(getCtx(), cp.getM_Warehouse_ID(), cp.getM_Locator_ID(),
							cp.getM_Product_ID(), ma.getM_AttributeSetInstance_ID()
							, (isShipment() && !isReversal() ? ma.getMovementQty().negate() : ma.getMovementQty())
							, ma.getDateMaterialPolicy(), get_TrxName()))
					{
						{
							String lastError = CLogger.retrieveErrorString("");
							return "Cannot correct Inventory OnHand (MA) - " + lastError;
						}
					}

					// move to armada locator
					if (!MStorageOnHand.add(getCtx(), intransitLoc.getM_Warehouse_ID(), intransitLoc.get_ID(), cp.getM_Product_ID(),
							ma.getM_AttributeSetInstance_ID(), 
							(isShipment() && !isReversal() ? ma.getMovementQty() : ma.getMovementQty().negate()), 
							ma.getDateMaterialPolicy(), get_TrxName()))
					{

						String lastError = CLogger.retrieveErrorString("");
						return "Cannot correct Inventory OnHand (MA) - " + lastError;
					}

					//
					MTransaction trxFrom =
							new MTransaction(getCtx(), cp.getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementFrom,
									cp.getM_Locator_ID(), cp.getM_Product_ID(), ma.getM_AttributeSetInstance_ID()
									, (isShipment() && !isReversal() ? ma.getMovementQty().negate() : ma.getMovementQty()),
									getDateDoc(), get_TrxName());
					trxFrom.setUNS_PL_ConfirmProduct_ID(cp.get_ID());
					if (!trxFrom.save())
					{
						m_processMsg = "Transaction From not inserted";
						return DocAction.STATUS_Invalid;
					}
					//
					MTransaction trxTo =
							new MTransaction(getCtx(), getAD_Org_ID(), MTransaction.MOVEMENTTYPE_MovementTo,
									intransitLoc.get_ID(), cp.getM_Product_ID(), ma.getM_AttributeSetInstance_ID()
									, (isShipment() && !isReversal() ? ma.getMovementQty() : ma.getMovementQty().negate())
									, getDateDoc(), get_TrxName());
					trxTo.setUNS_PL_ConfirmProduct_ID(cp.get_ID());
					if (!trxTo.save())
					{
						m_processMsg = "Transaction To not inserted";
						return DocAction.STATUS_Invalid;
					}
					
					attrMaps.add(new ValueMapping(ma.getM_AttributeSetInstance_ID(), 
							ma.getDateMaterialPolicy(), ma.getMovementQty()));
					totalQty = totalQty.add(ma.getMovementQty());
				}
			}
			
			if(isShipment() && !isReversal())
			{
				for (MUNSPLConfirmLine line : cp.getLines())
				{
					MUNSPackingListLine pll = (MUNSPackingListLine) line.getUNS_PackingList_Line();
					boolean needCheckInvoice = !pll.isProductAccessories();
					MInvoiceLine iLine = new MInvoiceLine(getCtx(), line.getC_InvoiceLine_ID(), get_TrxName());
					MInOutLine ioLine = new MInOutLine(getCtx(), line.getM_InOutLine_ID(), get_TrxName());
					ioLine.setM_Locator_ID(intransitLoc.get_ID());
					ioLine.setAD_Org_ID(line.getAD_Org_ID());
					iLine.setAD_Org_ID(line.getAD_Org_ID());
					boolean isAllowedToOverwriteInvoice = !iLine.getParent().isComplete() && needCheckInvoice; 
					if(M_AttributeSetInstance_ID > 0)
					{
						ioLine.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
						if(isAllowedToOverwriteInvoice && needCheckInvoice)
						{
							iLine.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
						}
					}
					else
					{
						ioLine.setM_AttributeSetInstance_ID(-1);
						BigDecimal required = ioLine.getMovementQty();
						for (int i=0; i<attrMaps.size(); i++) {
							if (required.signum()== 0) {
								break;
							}
							BigDecimal allocated = attrMaps.get(i).MOVEMENT_QTY;
							BigDecimal movementQty;
							if (allocated.compareTo(required) == 1) {
								allocated = allocated.subtract(required);
								movementQty = required;
								required = Env.ZERO;
							} else {
								required = required.subtract(allocated);
								movementQty = allocated;
								allocated = Env.ZERO;
							}
							
							MInOutLineMA ioMA = MInOutLineMA.addOrCreate(
									ioLine, attrMaps.get(i).ATTRIBUTE_INSTANCE_ID, 
									movementQty, attrMaps.get(i).
									DATE_MATERIAL_POLICY, false);
							ioMA.setAD_Org_ID(ioLine.getAD_Org_ID());
							ioMA.saveEx();
							
							if (allocated.signum() == 0) {
								attrMaps.remove(i);
								--i;
								continue;
							}
							
							attrMaps.get(i).MOVEMENT_QTY = allocated;
						}
						if(isAllowedToOverwriteInvoice && needCheckInvoice)
						{
							iLine.setM_AttributeSetInstance_ID(-1);
						}
					}
					
					if(ioLine.getQtyEntered().compareTo(totalQty) > 0)
					{
						if(MProduct.get(getCtx(), ioLine.getM_Product_ID()).getProductType().equals("I"))
							ioLine.setQty(totalQty);
						if(isAllowedToOverwriteInvoice && MProduct.get(getCtx(), iLine.getM_Product_ID()).getProductType().equals("I")
								&& needCheckInvoice)
						{
							iLine.setQty(totalQty);
							iLine.setQtyBonuses(Env.ZERO);
						}
						
						if(MProduct.get(getCtx(), pll.getM_Product_ID()).getProductType().equals("I"))
						{
							pll.setMovementQty(totalQty);
							pll.setQtyEntered(totalQty);
							pll.setQtyShipping(totalQty);
						}
						
						if(pll.getC_OrderLine_ID() > 0)
						{
							String sql = "UPDATE C_OrderLine SET QtyPacked=? WHERE C_OrderLine_ID=?"
									+ " AND M_Product_ID IN (SELECT M_Product_ID FROM M_Product WHERE ProductType <> 'S')";
							int count = DB.executeUpdateEx(sql, 
									new Object[]{totalQty, pll.getC_OrderLine_ID()},  get_TrxName());
							
							if (count < 0)
								return "Failed when updating quantity packed of product " 
										+ MProduct.get(getCtx(), pll.getM_Product_ID());
						}
						/*
						MOrderLine ol = (MOrderLine) pll.getC_OrderLine();
						ol.setQtyPacked(totalQty);
						ol.saveEx();
						
						org.compiere.model.MOrder o = (org.compiere.model.MOrder) ol.getC_Order();
						
						diffConfirmedMsg.append(Msg.getMsg(getCtx(), "DifferentConfirmedPackQty", 
								new Object[]{o.getDocumentNo(), o.getC_BPartner().getName(), 
								cp.getM_Product().getName(), line.getTargetQty(), totalQty})).append(" \n ");
						*/
					}
					
					totalQty = totalQty.subtract(ioLine.getQtyEntered());
					try
					{
						if(isAllowedToOverwriteInvoice && needCheckInvoice)
						{
							iLine.saveEx();
						}
						ioLine.saveEx();
						pll.saveEx();
					}
					catch (Exception e)
					{
						throw new AdempiereException(e.getMessage());
					}
				}
			}
		}
		
		if (diffConfirmedMsg.length() > 0 && !isReversal())
		{
			StringBuilder warningMsg = new StringBuilder("There are different confirmed quantities for order of: \n ")
			.append(diffConfirmedMsg);
			String title = "Please confirm for new confirmed quantities!";
			
			int answered = MessageBox.showMsg(this, getCtx(), warningMsg.toString(), 
					title, MessageBox.OKCANCEL, MessageBox.ICONWARNING);
			
			if (answered == MessageBox.RETURN_CANCEL) {
				m_processMsg = new StringBuilder("Differences: ").append(diffConfirmedMsg).toString();
				return DOCSTATUS_InProgress;
			}
		}
		
		return null;
	}

	@Override
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Void
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(getUNS_PackingList().getDocStatus().equals(DOCSTATUS_Completed))
			return reverseCorrectIt();
		
		MUNSPLConfirmProduct[] productList = getProducts();
		
		for (MUNSPLConfirmProduct confirmProduct : productList)
		{
			MUNSPLConfirmLine[] confirmLineList = confirmProduct.getLines();
			
			for (MUNSPLConfirmLine plConfirmLine : confirmLineList)
			{
				plConfirmLine.setQtyEntered(Env.ZERO);
				plConfirmLine.setTargetQty(Env.ZERO);
				plConfirmLine.saveEx();
			}
			
			confirmProduct.setConfirmedQty(Env.ZERO);
			confirmProduct.setTargetQty(Env.ZERO);
			confirmProduct.setMovementQty(Env.ZERO);
			confirmProduct.setDifferenceQty(Env.ZERO);
			confirmProduct.saveEx();
		}
		
		addDescription(Msg.getMsg(getCtx(), "Voided"));

		// After Void
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocStatus(DOCSTATUS_Voided);
		setDocAction(DOCACTION_None);
		return true;
	}

	/**
	 * Add to Description
	 * 
	 * @param description text
	 */
	public void addDescription(String description)
	{
		String desc = getDescription();
		if (desc == null)
			setDescription(description);
		else
			setDescription(desc + " | " + description);
	} // addDescription

	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Close

		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;

		return true;
	}

	/**
	 * Reverse Correction - same void
	 * 
	 * @return true if success
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseCorrect
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this,
						ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		MUNSPLConfirm reversal = reverse();
		if (reversal == null)
			return false;

		// After reverseAccrual
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this,
						ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		m_processMsg = reversal.getDocumentNo();
		setProcessed(true);
		setDocStatus(DOCSTATUS_Reversed);
		setDocAction(DOCACTION_None);
		
		return true;
	} // reverseCorrectionIt

	/**
	 * Reverse Accrual - none
	 * 
	 * @return false
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseAccrual
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this,
						ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		MUNSPLConfirm reversal = reverse();
		if (reversal == null)
			return false;

		// After reverseAccrual
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this,
						ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		m_processMsg = reversal.getDocumentNo();
		setProcessed(true);
		setDocStatus(DOCSTATUS_Reversed);		//	 may come from void
		setDocAction(DOCACTION_None);
		
		return true;
	} // reverseAccrualIt
	
	private boolean m_reversal = false;
	
	public boolean isReversal()
	{
		return m_reversal;
	}
	
	public void setReversal (boolean reverse)
	{
		m_reversal = reverse;
	}

	/**
	 * 
	 * @return
	 */
	private MUNSPLConfirm reverse() 
	{
		MUNSPackingList oriPL = new MUNSPackingList(getCtx(), getUNS_PackingList_ID(), get_TrxName());
		MUNSPackingList reversalPL = new MUNSPackingList(getCtx(), oriPL.getReversal_ID(), get_TrxName());
		
		MUNSPLConfirm reversal = new MUNSPLConfirm(getCtx(), 0, get_TrxName());
		copyValues(this, reversal);
		reversal.setAD_Org_ID(getAD_Org_ID());
		reversal.setUNS_PackingList_ID(reversalPL.get_ID());
		reversal.setDocumentNo(getDocumentNo() + "^"); 
		reversal.setDateDoc(reversalPL.getDateDoc());
		reversal.setReversal_ID(this.get_ID());
		reversal.setReversal(true);
		reversal.saveEx();
		
		MUNSPLConfirmProduct[] confirmProductList = getProducts();
		
		for (MUNSPLConfirmProduct plcp : confirmProductList)
		{
			MUNSPLConfirmProduct reversalProduct = new MUNSPLConfirmProduct(getCtx(), 0, get_TrxName());
			copyValues(plcp, reversalProduct);
			reversalProduct.setAD_Org_ID(reversal.getAD_Org_ID());
			reversalProduct.setUNS_PL_Confirm_ID(reversal.get_ID());
			reversalProduct.setM_Product_ID(plcp.getM_Product_ID());
			reversalProduct.setMovementQty(plcp.getMovementQty());
			reversalProduct.setDifferenceQty(plcp.getDifferenceQty());
			reversalProduct.setTargetQty(plcp.getTargetQty());
			reversalProduct.setConfirmedQty(plcp.getConfirmedQty());
			reversalProduct.setM_Warehouse_ID(plcp.getM_Warehouse_ID());
			reversalProduct.setM_Locator_ID(plcp.getM_Locator_ID());
			reversalProduct.setC_UOM_ID(plcp.getC_UOM_ID());
			reversalProduct.setM_AttributeSetInstance_ID(plcp.getM_AttributeSetInstance_ID());
			reversalProduct.saveEx();
			
			if (plcp.getM_AttributeSetInstance_ID() > 0)
				continue;
			
			MUNSPLConfirmMA[] maList = plcp.getMAs();
			
			for (MUNSPLConfirmMA ma : maList)
			{
				MUNSPLConfirmMA reversalMA = new MUNSPLConfirmMA(getCtx(), 0, get_TrxName());
				//copyValues(ma, reversalMA);
				if (ma.getM_AttributeSetInstance_ID() > 0)
					reversalMA.setM_AttributeSetInstance_ID(ma.getM_AttributeSetInstance_ID());
				
				reversalMA.setAD_Org_ID(ma.getAD_Org_ID());
				reversalMA.setMovementQty(ma.getMovementQty());
				reversalMA.setDateMaterialPolicy(ma.getDateMaterialPolicy());
				reversalMA.setIsAutoGenerated(false);
				reversalMA.setUNS_PL_ConfirmProduct_ID(reversalProduct.get_ID());
				reversalMA.saveEx();
			}
			
//			hanya digunakan ketika perapihan asi.
//			try
//			{
//				MappingASI(plcp);
//			}
//			catch (AdempiereSystemError e)
//			{
//				e.printStackTrace();
//			}
		}
		
		if (!reversal.processIt(DocAction.ACTION_Complete))
				//|| !reversal.getDocStatus().equals(DocAction.STATUS_Completed))
		{
			m_processMsg = "Reversal ERROR: " + reversal.getProcessMsg();
			return null;
		}
		
		reversal.closeIt();
		reversal.setProcessing (false);
		reversal.setDocStatus(DOCSTATUS_Reversed);
		reversal.setDocAction(DOCACTION_None);
		reversal.saveEx(get_TrxName());
		//
		StringBuilder msgadd = new StringBuilder("(").append(reversal.getDocumentNo()).append("<-)");
		addDescription(msgadd.toString());
		
		//
		// Void Confirmations
		setDocStatus(DOCSTATUS_Reversed);
		saveEx();
		setReversal_ID(reversal.getUNS_PackingList_ID());

		return reversal;
//		MUNSPackingListOrder[] reversalPLOList = reversalPL.getLines(null, null);
//		
//		for (MUNSPackingListOrder reversalPLO : reversalPLOList)
//		{
//			
//		}
	}

	@Override
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reActivate
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

		// TODO palce coding reActivateIt here

		// After reActivate
		m_processMsg =
				ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/*************************************************************************
	 * Get Summary
	 * 
	 * @return Summary of Document
	 */
	public String getSummary()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		// : Grand Total = 123.00 (#1)
		sb.append(": ").append(Msg.translate(getCtx(), "Tonase")).append("=").append(getTonase())
				.append(Msg.translate(getCtx(), "Volume")).append("=").append(getVolume());
		if (m_Products != null)
			sb.append(" (#").append(m_Products.length).append(")");
		// - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		return sb.toString();
	} // getSummary

	/**************************************************************************
	 * String Representation
	 * 
	 * @return info
	 */
	public String toString()
	{
		StringBuffer sb =
				new StringBuffer("MPLConfirm[").append(get_ID()).append("-").append(getDocumentNo())
						.append(", Tonase=").append(getTonase()).append(", Volume=").append(getVolume())
						.append("]");
		return sb.toString();
	} // toString

	/**
	 * Get Document Info
	 * 
	 * @return document info (untranslated)
	 */
	public String getDocumentInfo()
	{
		return "Packing List :" + getDocumentNo();
	} // getDocumentInfo

	@Override
	public File createPDF()
	{
		try
		{
			File temp = File.createTempFile(get_TableName() + get_ID() + "_", ".pdf");
			return createPDF(temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;

	}

	/**
	 * Create PDF file
	 * 
	 * @param file output file
	 * @return file if success
	 */
	public File createPDF(File file)
	{
		ReportEngine re = ReportEngine.get(getCtx(), ReportEngine.ORDER, get_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if (format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo("", format.getJasperProcess_ID());
			pi.setRecord_ID(get_ID());
			pi.setIsBatch(true);

			ServerProcessCtl.process(pi, null);

			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	} // createPDF

	@Override
	public String getProcessMsg()
	{

		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID()
	{

		return getUpdatedBy();
	}

	@Override
	public BigDecimal getApprovalAmt()
	{
		return Env.ZERO;
	}

	@Override
	public int getC_Currency_ID()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/**************************************************************************
	 * Get Orders
	 * 
	 * @param whereClause where clause or null (starting with AND)
	 * @param orderClause order clause
	 * @return orders
	 */
	public MUNSPLConfirmProduct[] getProducts(String whereClause, String orderClause)
	{
		// red1 - using new Query class from Teo / Victor's MDDOrder.java implementation
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmProduct.COLUMNNAME_UNS_PL_Confirm_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (orderClause.length() == 0)
			orderClause = MUNSPLConfirmProduct.COLUMNNAME_UNS_PL_ConfirmProduct_ID;
		//
		List<MUNSPLConfirmProduct> list =
				Query
						.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmProduct.Table_Name,
								whereClauseFinal.toString(), get_TrxName()).setParameters(get_ID())
						.setOrderBy(orderClause).list();

		return list.toArray(new MUNSPLConfirmProduct[list.size()]);
	} // getLines

	/**
	 * Get Lines of Order
	 * 
	 * @param requery requery
	 * @param orderBy optional order by column
	 * @return lines
	 */
	public MUNSPLConfirmProduct[] getProducts(boolean requery, String orderBy)
	{
		if (m_Products != null && !requery)
		{
			set_TrxName(m_Products, get_TrxName());
			return m_Products;
		}
		//
		String orderClause = "";
		if (orderBy != null && orderBy.length() > 0)
			orderClause += orderBy;

		m_Products = getProducts(null, orderClause);
		return m_Products;
	} // getLines

	/**
	 * Get Lines of Order. (used by web store)
	 * 
	 * @return lines
	 */
	public MUNSPLConfirmProduct[] getProducts()
	{
		return getProducts(false, null);
	} // getLines

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord)
		{
			setCopyOfPrintSL(-1);
			setCopyOfPrintSLL(-1);
		}
		return super.beforeSave(newRecord);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete()
	{
		for (MUNSPLConfirmProduct product : getProducts())
		{
			if (!product.delete(true))
				return false;
		}

		return super.beforeDelete();
	}
	
	public static MUNSPLConfirm get(MUNSPackingList pl)
	{
		MUNSPLConfirm retval = null;
		
		StringBuilder sql =
				new StringBuilder("SELECT ").append(COLUMNNAME_UNS_PL_Confirm_ID)
						.append(" FROM ").append(Table_Name).append(" WHERE ").append(COLUMNNAME_UNS_PackingList_ID)
						.append("=").append(pl.get_ID());
		int ID = DB.getSQLValue(pl.get_TrxName(), sql.toString());
		
		if (ID <=0)
			 return retval;
		retval =  new MUNSPLConfirm(pl.getCtx(), ID, pl.get_TrxName());
		 
		return retval;
	}

	/**
	 * 
	 * @param po {@link PO}
	 */
	public static String createConfirmation(PO po)
	{
		String retval = null;
		String headerColumnName = null;
		boolean packinglist = true;

		if (po.get_TableName().equalsIgnoreCase(MUNSPackingList.Table_Name))
		{
			headerColumnName = MUNSPackingList.COLUMNNAME_UNS_PackingList_ID;
		}
		else if (po.get_TableName().equalsIgnoreCase(MUNSPLReturn.Table_Name))
		{
			headerColumnName = MUNSPLReturn.COLUMNNAME_UNS_PL_Return_ID;
			packinglist = false;
		}

		StringBuilder sql =
				new StringBuilder("SELECT ").append(MUNSPLConfirm.COLUMNNAME_UNS_PL_Confirm_ID)
						.append(" FROM ").append(MUNSPLConfirm.Table_Name).append(" WHERE ").append(headerColumnName)
						.append("=").append(po.get_ID());
		int ID = DB.getSQLValue(po.get_TrxName(), sql.toString());

		MUNSPLConfirm confirm = new MUNSPLConfirm(po.getCtx(), ID == -1 ? 0 : ID, po.get_TrxName());
		if (ID > 0)
		{
			if (confirm.getDocStatus().equalsIgnoreCase(MUNSPLConfirm.DOCSTATUS_Completed)
					|| confirm.getDocStatus().equalsIgnoreCase(MUNSPLConfirm.DOCSTATUS_Closed))
				return retval;

			retval = "Please complete packing list confirmation #" + confirm.getDocumentNo() + " first.";
		}
		else
		{
			confirm.initConfirm(po);
			confirm.saveEx();

			if(!confirm.generateLines(po, packinglist)){
				confirm.deleteEx(true);
				
				return null;
			}
			retval =
					"Packing List Confirm has been created, Document No = ".concat(confirm.getDocumentNo());
		}

		return retval;
	}

	public void initConfirm(PO po)
	{
		setClientOrg(po);

		setDateDoc(new Timestamp(System.currentTimeMillis()));
		setTonase(Env.ZERO);
		setVolume(Env.ZERO);

		if (po.get_TableName().equalsIgnoreCase(MUNSPackingList.Table_Name))
		{
			setUNS_PackingList_ID(po.get_ID());
			setConfirmType(CONFIRMTYPE_PackingListConfirmation);
		}
		else if (po.get_TableName().equalsIgnoreCase(MUNSPLReturn.Table_Name))
		{
			setUNS_PL_Return_ID(po.get_ID());
			setConfirmType(CONFIRMTYPE_ShipmentReturnConfirmation);
		}
	}

	private boolean generateLines(PO po, boolean isPackingList)
	{
		if (isPackingList)
		{
			MUNSPackingList packingList = (MUNSPackingList) po;
			for (MUNSPackingListOrder order : packingList.getOrders())
			{
				createlines(order);
			}
		}
		else
		{
			MUNSPLReturn plReturn = (MUNSPLReturn) po;
			setUNS_PackingList_ID(plReturn.getUNS_PackingList_ID());
			setUNS_PL_Return_ID(plReturn.getUNS_PL_Return_ID());
			saveEx();

			int counter = 0;
			for (MUNSPLReturnOrder rorder : plReturn.getROders())
			{
				if (!rorder.isCancelled() && !rorder.isPartialCancelation())
					continue;

				counter++;
				
				if (rorder.isPartialCancelation()) {
					createReturnLines(rorder);
				}
				else {
					MUNSPackingListOrder plo = new MUNSPackingListOrder(
							getCtx(), rorder.getUNS_PackingList_Order_ID(), get_TrxName());
					createlines(plo);
				}
			}
			
			if (counter == 0)
				return false;
		}
		
		return true;
	}

	private void createlines(MUNSPackingListOrder plo)
	{
		for (MUNSPackingListLine line : plo.getLines())
		{
			MUNSPLConfirmProduct plCProduct = MUNSPLConfirmProduct.getNewConfirmProduct(line, this);
			plCProduct.setQty(line.getMovementQty());
			plCProduct.saveEx();
			MUNSPLConfirmLine plCLine = MUNSPLConfirmLine.getNewConfirmLine(line, plCProduct);
			plCLine.setHeader(plCProduct);
			plCLine.setLine(line);
			plCLine.saveEx();
		}
	}

	private void createReturnLines(MUNSPLReturnOrder rorder)
	{
		for (MUNSPLReturnLine line : rorder.getLines())
		{
			MUNSPLConfirmProduct plCProduct = MUNSPLConfirmProduct.getNewReturnConfirmProduct(line, this);
			
			plCProduct.setQty(line.getMovementQty());
			plCProduct.saveEx();
			
			MUNSPLConfirmLine plCLine = MUNSPLConfirmLine.getNewReturnConfirmLine(line, plCProduct);
			
			plCLine.setHeader(plCProduct);
			plCLine.setQtyEntered(line.getCancelledQty());
			plCLine.setTargetQty(line.getCancelledQty());

			plCLine.saveEx();
		}
	}

	public void updateTonaseVolume()
	{
		BigDecimal tonase = Env.ZERO;
		BigDecimal volume = Env.ZERO;

		for (MUNSPLConfirmProduct product : getProducts())
		{
			tonase = tonase.add(product.getMovementQty().multiply(product.getM_Product().getGrossWeight())
							.multiply(new BigDecimal(0.001)));
			volume = volume.add(product.getMovementQty().multiply(product.getM_Product().getVolume())
							.multiply(new BigDecimal(0.001)).multiply(new BigDecimal(0.001)));
		}

		StringBuilder sb =
				new StringBuilder("UPDATE ").append(MUNSPLConfirm.Table_Name).append(" SET ")
						.append(MUNSPLConfirm.COLUMNNAME_Tonase).append("=").append(tonase).append(", ")
						.append(MUNSPLConfirm.COLUMNNAME_Volume).append("=").append(volume).append(" WHERE ")
						.append(MUNSPLConfirm.COLUMNNAME_UNS_PL_Confirm_ID).append("=")
						.append(get_ID());

		if (DB.executeUpdate(sb.toString(), get_TrxName()) == -1)
			throw new AdempiereException("Error when update parent.");

	}


	/**
	@Override
	public int customizeValidActions(String docStatus, Object processing, String orderType,
			String isSOTrx, int AD_Table_ID, String[] docAction, String[] options, int index)
	{
		if (docStatus.equals(DocAction.STATUS_Drafted))
		{
			options[index++] = DocAction.ACTION_Prepare;
		}
		return index;
	}
	*/
	
	private void initReturnProductAttr (MUNSPLConfirmProduct cp) {
		if (cp.getM_AttributeSetInstance_ID() > 0) {
			return;
		} else if (!CONFIRMTYPE_ShipmentReturnConfirmation.equals(getConfirmType())) {
			return;
		} else if(!cp.deleteLinesMA()) {
			throw new AdempiereException("Can't delete old line MA");
		}
		
		BigDecimal movementQty = cp.getConfirmedQty();
		
		MUNSPLConfirmLine[] confirmLines = cp.getLines(true, null);
		MUNSPLConfirmMA[] manuals = cp.getMAs(true, null);
		//0 = asi, 1 = datempolicy, 2 = movementqty
		List<Object[]> listManual = new ArrayList<>();
		for (int i=0; i<manuals.length; i++) {
			listManual.add(new Object[]{manuals[i].getM_AttributeSetInstance_ID(), 
					manuals[i].getDateMaterialPolicy(), manuals[i].getMovementQty()});
		}
		for (int i=0; i<confirmLines.length; i++) {
			MUNSPLConfirmLine cLine = confirmLines[i];
			BigDecimal lineMovementQty = cLine.getQtyEntered();
			movementQty = movementQty.subtract(lineMovementQty);
			MInOutLineMA[] mas = MInOutLineMA.get(getCtx(), cLine.getM_InOutLine_ID(), get_TrxName());
			for (int j=0; j<mas.length; j++) {
				for (int k=0; k<listManual.size(); k++) {
					if (((BigDecimal) listManual.get(k)[0]).signum() == 0) {
						listManual.remove(k);
						--k;
						continue;
					} else if (((Integer) listManual.get(k)[0]) == mas[j].getM_AttributeSetInstance_ID()
							&& ((Timestamp) listManual.get(k)[1]).equals(mas[j].getDateMaterialPolicy())) {
						if (((BigDecimal) listManual.get(k)[2]).compareTo(mas[j].getMovementQty()) == 1) {
							listManual.get(k)[2] = ((BigDecimal) listManual.get(k)[2]).subtract(mas[j].getMovementQty());
							lineMovementQty = lineMovementQty.subtract(mas[j].getMovementQty());
							mas[j].setMovementQty(Env.ZERO);
						} else {
							lineMovementQty = lineMovementQty.subtract((BigDecimal) listManual.get(k)[2]);
							listManual.get(k)[2] = Env.ZERO;
							mas[j].setMovementQty(mas[j].getMovementQty().subtract((BigDecimal) listManual.get(k)[2]));
						}
						
						if (mas[j].getMovementQty().signum() == 0) {
							break;
						}
					}
				}
				
				if (mas[j].getMovementQty().signum() == 0) {
					mas[j].deleteEx(true);
					continue;
				} else if (mas[j].getMovementQty().compareTo(lineMovementQty) == 1) {
					MUNSPLConfirmMA ma = MUNSPLConfirmMA.getCreate(cp, mas[j].getM_AttributeSetInstance_ID(), 
							mas[j].getDateMaterialPolicy());
					ma.setMovementQty(ma.getMovementQty().add(lineMovementQty));
					ma.saveEx();
					mas[j].setMovementQty(mas[j].getMovementQty().subtract(lineMovementQty));
					mas[j].saveEx();
					lineMovementQty = Env.ZERO;
				} else {
					MUNSPLConfirmMA ma = MUNSPLConfirmMA.getCreate(cp, mas[j].getM_AttributeSetInstance_ID(), 
							mas[j].getDateMaterialPolicy());
					ma.setMovementQty(ma.getMovementQty().add(mas[j].getMovementQty()));
					ma.saveEx();
					lineMovementQty = lineMovementQty.subtract(mas[j].getMovementQty());
					mas[j].deleteEx(true);
				}
				
				if (lineMovementQty.signum() == 0) {
					break;
				}
			}
			
			if (lineMovementQty.signum() != 0) {
				MUNSPLConfirmMA ma = new MUNSPLConfirmMA(cp, 0, getDateDoc());
				ma.setMovementQty(lineMovementQty);
				ma.saveEx();
			}
		}
		
		if (movementQty.signum() != 0) {
			m_processMsg = "Difference line Qty And Qty on confirm product.";
		}
	}
	
	/**
	 * 
	 * @param cp
	 */
	private void initialProductAttribute(MUNSPLConfirmProduct cp)
	{
		if (CONFIRMTYPE_ShipmentReturnConfirmation.equals(getConfirmType())) {
			initReturnProductAttr(cp);
			return;
		}

		if(!cp.deleteLinesMA())
			throw new AdempiereException("Can't delete old line MA");
		
		BigDecimal qty = cp.getConfirmedQty();
		qty = qty.subtract(MUNSPLConfirmMA.getTotalQty(true, cp.get_ID(), get_TrxName()));
		
		Timestamp guarantedDate = cp.getParent().getDateDoc();
		MWarehouse wh = (MWarehouse) cp.getM_Warehouse();
		MProduct product = MProduct.get(getCtx(), cp.getM_Product_ID());
		
		int M_Locator_ID = 0;
		if ((isShipment() && !isReversal()) || (!isShipment() && isReversal()))
		{
			M_Locator_ID = cp.getM_Locator_ID();
		}
		else if ((!isShipment() && !isReversal()) || (isShipment() && isReversal()))
		{
			M_Locator_ID = DB.getSQLValue(get_TrxName(), "SELECT M_Locator_ID FROM M_Locator WHERE Value = ? AND M_Warehouse_ID = ?"
					, wh.getValue().concat("-").concat(MWarehouse.INITIAL_INTRANSIT_CUSTOMER_LOC), wh.get_ID());
		}
		
		MStorageOnHand[] storages = MStorageOnHand.getWarehouse(
				getCtx(), cp.getM_Warehouse_ID(), cp.getM_Product_ID(), 0, guarantedDate
				, MClient.MMPOLICY_FiFo.equals(product.getMMPolicy()), wh.isDisallowNegativeInv()
				, M_Locator_ID, get_TrxName(), true);
		
		MUNSPLConfirmMA[] manualMAs = cp.getMAs(true, MUNSPLConfirmMA.COLUMNNAME_M_AttributeSetInstance_ID);
		
		for(MStorageOnHand storage : storages)
		{
			MUNSPLConfirmMA manualMA = null;
			for (MUNSPLConfirmMA ma : manualMAs)
			{
				if (ma.getM_AttributeSetInstance_ID() == storage.getM_AttributeSetInstance_ID()) {
					manualMA = ma;
					break;
				}
			}
			
			if (manualMA != null) {
				storage.setQtyOnHand(storage.getQtyOnHand().subtract(manualMA.getMovementQty()));
			}
			
			if (storage.getQtyOnHand().compareTo(Env.ZERO) <= 0)
				continue;
			
			if (storage.getQtyOnHand().compareTo(qty) >= 0)
			{
				MUNSPLConfirmMA ma = MUNSPLConfirmMA.getCreate(
						cp, storage.getM_AttributeSetInstance_ID(), storage.getDateMaterialPolicy());
				ma.setUNS_PL_ConfirmProduct_ID(cp.get_ID());
				ma.setMovementQty(qty);
				ma.setIsAutoGenerated(true);
				ma.saveEx();
				qty = Env.ZERO;
			}
			else
			{
				MUNSPLConfirmMA ma = MUNSPLConfirmMA.getCreate(
						cp, storage.getM_AttributeSetInstance_ID(), storage.getDateMaterialPolicy());
				ma.setMovementQty(storage.getQtyOnHand());
				ma.setUNS_PL_ConfirmProduct_ID(cp.get_ID());
				ma.setIsAutoGenerated(true);
				ma.saveEx();
				qty = qty.subtract(storage.getQtyOnHand());
				if (log.isLoggable(Level.FINE)) log.fine( ma + ", QtyToDeliver=" + qty);
			}

			if (qty.signum() == 0)
				break;
		}
		
		if (qty.signum() != 0)
		{
			if (log.isLoggable(Level.WARNING)) log.fine("##difference : " + qty);
		}
	}
	
	public boolean isShipment()
	{
		return getConfirmType().endsWith(CONFIRMTYPE_PackingListConfirmation);
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	public static MUNSPLConfirm[] gets(PO model)
	{
		String whereclause = (new StringBuilder(model.get_TableName()).append("_ID = ?")).toString();
		List<MUNSPackingList> list = Query.get(
				model.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name
				, whereclause, model.get_TrxName()).setParameters(model.get_ID())
				.setOrderBy(COLUMNNAME_DocumentNo).list();
		MUNSPLConfirm[] records = new MUNSPLConfirm[list.size()];
		list.toArray(records);
		return records;
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	public static String create(PO model)
	{
		boolean consolidateConfirm = false;
		String confirmType = "";
		if(model instanceof I_UNS_PackingList)
		{
			I_UNS_PackingList pl = (I_UNS_PackingList) model;
			consolidateConfirm = pl.isConsolidateConfirmation();
			confirmType = "Packing List";
		}
		else if(model instanceof I_UNS_PL_Return)
		{
			I_UNS_PL_Return plr = (I_UNS_PL_Return) model;
			String consolidate = DB.getSQLValueString(
					model.get_TrxName()
					, "SELECT ConsolidateConfirmation FROM UNS_PackingList WHERE UNS_PackingList_ID = ? "
					,plr.getUNS_PackingList_ID());
			consolidateConfirm = consolidate != null && consolidate.equals("Y");
			confirmType = "Packing List Return";
		}
		
		if(consolidateConfirm)
			return MUNSPLConfirm.createConfirmation(model);
		
		MUNSPLConfirm[] confirms = MUNSPLConfirm.gets(model);
		if(null == confirms || confirms.length == 0)
		{
			confirms = createConsolidateConfirm(model);
		}
		
		StringBuilder msg = new StringBuilder();
		
		for(MUNSPLConfirm confirm : MUNSPLConfirm.gets(model))
		{
			if(!confirm.getDocStatus().equals(MUNSPLConfirm.DOCSTATUS_Completed)
					&& !confirm.getDocStatus().equals(MUNSPLConfirm.DOCSTATUS_Closed))
			{	
				if(!Util.isEmpty(msg.toString(), true))
					msg.append(" && ");
				
				msg.append("Please complete " + confirmType + " warehouse confirmation --> ")
					.append(confirm.getDocumentNo());
			}
		}
		
		if(!Util.isEmpty(msg.toString(), true))
		{
			String title = Msg.getMsg(model.getCtx(), "Confirmation needed");
			MessageBox.showMsg(model, 
					model.getCtx(), msg.toString(), title, MessageBox.OK
					, MessageBox.ICONINFORMATION);
		}
		
		return msg.toString();
	}
	
	/**
	 * 
	 * @param model
	 */
	public static MUNSPLConfirm[] createConsolidateConfirm(PO model)
	{
		Hashtable<Integer, MUNSPLConfirm> mappingByWarehouse = new Hashtable<>();
		if(model instanceof I_UNS_PackingList)
		{
			MUNSPackingList packingList = (MUNSPackingList) model;
			for (MUNSPackingListOrder order : packingList.getOrders())
			{
				int M_Warehouse_ID = order.getM_Warehouse_ID();
				MUNSPLConfirm confirm = mappingByWarehouse.get(M_Warehouse_ID);
				if(null == confirm)
				{
					confirm = new MUNSPLConfirm(model.getCtx(), 0, model.get_TrxName());
					I_M_Warehouse whs = order.getM_Warehouse();
					confirm.initConfirm(model);
					//overwrite org
					confirm.setAD_Org_ID(whs.getAD_Org_ID());
					confirm.setAD_Client_ID(whs.getAD_Client_ID());
					confirm.saveEx();
					
					mappingByWarehouse.put(M_Warehouse_ID, confirm);
				}
								
				confirm.createlines(order);
			}
		}
		else if( model instanceof I_UNS_PL_Return)
		{
			MUNSPLReturn plReturn = (MUNSPLReturn) model;
			for (MUNSPLReturnOrder order : plReturn.getROders())
			{
				if (!order.isCancelled())
					continue;
				
				MUNSPackingListOrder plo =
						new MUNSPackingListOrder(
								model.getCtx(), order.getUNS_PackingList_Order_ID()
								, model.get_TrxName());

				int M_Warehouse_ID = plo.getM_Warehouse_ID();
				MUNSPLConfirm confirm = mappingByWarehouse.get(M_Warehouse_ID);
				if(null == confirm)
				{
					confirm = new MUNSPLConfirm(model.getCtx(), 0, model.get_TrxName());
					I_M_Warehouse whs = plo.getM_Warehouse();
					confirm.initConfirm(model);
					//overwrite org
					confirm.setAD_Org_ID(whs.getAD_Org_ID());
					confirm.setAD_Client_ID(whs.getAD_Client_ID());
					confirm.saveEx();
					
					mappingByWarehouse.put(M_Warehouse_ID, confirm);
				}

				confirm.createlines(plo);
			}
		}
		else
		{
			throw new AdempiereException("Unable to create instance. " + model.get_TableName());
		}
		
		MUNSPLConfirm[] confirms = new MUNSPLConfirm[mappingByWarehouse.size()];
		mappingByWarehouse.values().toArray(confirms);
		return confirms;
	}
}

class ValueMapping {
	int ATTRIBUTE_INSTANCE_ID;
	Timestamp DATE_MATERIAL_POLICY;
	BigDecimal MOVEMENT_QTY;
	
	public ValueMapping (int attrInstance_ID, Timestamp dateMPolicy, 
			BigDecimal movementQty) {
		ATTRIBUTE_INSTANCE_ID = attrInstance_ID;
		DATE_MATERIAL_POLICY = dateMPolicy;
		MOVEMENT_QTY = movementQty;
	}
}