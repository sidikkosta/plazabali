/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_POS_SequencePeriodic
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POS_SequencePeriodic extends PO implements I_UNS_POS_SequencePeriodic, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20181024L;

    /** Standard Constructor */
    public X_UNS_POS_SequencePeriodic (Properties ctx, int UNS_POS_SequencePeriodic_ID, String trxName)
    {
      super (ctx, UNS_POS_SequencePeriodic_ID, trxName);
      /** if (UNS_POS_SequencePeriodic_ID == 0)
        {
			setUniqueCode (null);
// -
			setUNS_POS_SequencePeriodic_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POS_SequencePeriodic (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POS_SequencePeriodic[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Current.
		@param Current Current	  */
	public void setCurrent (int Current)
	{
		set_Value (COLUMNNAME_Current, Integer.valueOf(Current));
	}

	/** Get Current.
		@return Current	  */
	public int getCurrent () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Current);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Next.
		@param Next Next	  */
	public void setNext (int Next)
	{
		set_Value (COLUMNNAME_Next, Integer.valueOf(Next));
	}

	/** Get Next.
		@return Next	  */
	public int getNext () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Next);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Unique Code.
		@param UniqueCode 
		Code Unique for transaction
	  */
	public void setUniqueCode (String UniqueCode)
	{
		set_Value (COLUMNNAME_UniqueCode, UniqueCode);
	}

	/** Get Unique Code.
		@return Code Unique for transaction
	  */
	public String getUniqueCode () 
	{
		return (String)get_Value(COLUMNNAME_UniqueCode);
	}

	public com.unicore.model.I_UNS_POS_Sequence getUNS_POS_Sequence() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Sequence)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Sequence.Table_Name)
			.getPO(getUNS_POS_Sequence_ID(), get_TrxName());	}

	/** Set UNS_POS_Sequence_ID.
		@param UNS_POS_Sequence_ID UNS_POS_Sequence_ID	  */
	public void setUNS_POS_Sequence_ID (int UNS_POS_Sequence_ID)
	{
		if (UNS_POS_Sequence_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Sequence_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Sequence_ID, Integer.valueOf(UNS_POS_Sequence_ID));
	}

	/** Get UNS_POS_Sequence_ID.
		@return UNS_POS_Sequence_ID	  */
	public int getUNS_POS_Sequence_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Sequence_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Periodic Sequence.
		@param UNS_POS_SequencePeriodic_ID Periodic Sequence	  */
	public void setUNS_POS_SequencePeriodic_ID (int UNS_POS_SequencePeriodic_ID)
	{
		if (UNS_POS_SequencePeriodic_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_SequencePeriodic_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_SequencePeriodic_ID, Integer.valueOf(UNS_POS_SequencePeriodic_ID));
	}

	/** Get Periodic Sequence.
		@return Periodic Sequence	  */
	public int getUNS_POS_SequencePeriodic_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_SequencePeriodic_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POS_SequencePeriodic_UU.
		@param UNS_POS_SequencePeriodic_UU UNS_POS_SequencePeriodic_UU	  */
	public void setUNS_POS_SequencePeriodic_UU (String UNS_POS_SequencePeriodic_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POS_SequencePeriodic_UU, UNS_POS_SequencePeriodic_UU);
	}

	/** Get UNS_POS_SequencePeriodic_UU.
		@return UNS_POS_SequencePeriodic_UU	  */
	public String getUNS_POS_SequencePeriodic_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POS_SequencePeriodic_UU);
	}
}