package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MCost;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSPOSRecapLine extends X_UNS_POSRecapLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 757377450186679017L;

	public MUNSPOSRecapLine(Properties ctx, int UNS_POSRecapLine_ID,
			String trxName) {
		super(ctx, UNS_POSRecapLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSPOSRecapLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSPOSRecapLine(MUNSPOSRecap parent, MUNSPOSTrxLine line)
	{
		this(line.getCtx(), 0, line.get_TrxName());
		
		MProduct p = MProduct.get(getCtx(), line.getM_Product_ID());
		setM_Product_ID(p.get_ID());
		setSKU(p.getSKU());
		setIsConsignment(p.isConsignment());
		setUNS_POSRecap_ID(parent.get_ID());
		setAD_Org_ID(line.getAD_Org_ID());
		setQty(Env.ZERO);
		setRevenueAmt(Env.ZERO);
		setSalesAmt(Env.ZERO);
		setDiscountAmt(Env.ZERO);
		setTaxAmt(Env.ZERO);
		setServiceChargeAmt(Env.ZERO);
	}
	
	public static MUNSPOSRecapLine getCreate(MUNSPOSRecap parent, MUNSPOSTrxLine line, boolean isRefund)
	{
		String whereClause = "UNS_POSRecap_ID = ? AND M_Product_ID = ? AND IsRefund = ?";
		MUNSPOSRecapLine result = Query.get(line.getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, whereClause, line.get_TrxName()).setParameters(parent.get_ID(), line.getM_Product_ID(),
						isRefund ? "Y" : "N")
				.firstOnly();
		
		if(result == null)
		{
			result = new MUNSPOSRecapLine(parent, line);
		}
		return result;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		
		return true;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(!success)
			return false;
		
		if(!isIgnoreValidationSave())
		{
			String serverType = MSysConfig.getValue(MSysConfig.SERVER_TYPE, "");
			String mainServer = MSysConfig.getValue(MSysConfig.MAIN_SERVER, "");
			if(!Util.isEmpty(mainServer, true) && serverType.equals(mainServer))
			{
				MProduct p = MProduct.get(getCtx(), getM_Product_ID());
				BigDecimal cost = Env.ZERO;
				if(p.isConsignment())
				{
					String sql = "SELECT pp.PriceStd FROM M_ProductPrice pp"
							+ " INNER JOIN M_PriceList_Version v ON pp.M_PriceList_Version_ID = v.M_PriceList_Version_ID"
							+ " INNER JOIN M_PriceList pl ON pl.M_PriceList_ID = v.M_PriceList_ID"
							+ " WHERE pl.IsActive = 'Y' AND pl.IsSOPriceList = 'N'"
							+ " AND v.ValidFrom <= ? AND pp.M_product_ID = ? ORDER BY v.ValidFrom DESC";
					cost = DB.getSQLValueBD(get_TrxName(), sql, new Object[]{getUNS_POSRecap().getDateTrx(), getM_Product_ID()});
					
					if(cost == null)
						cost = Env.ZERO;
					
					setCostAmt(cost.multiply(getQty()));
				}
				else
				{
					MUNSPOSRecapLineMA[] mas = getMAs();
					for(int i=0;i<mas.length;i++)
					{
						MAcctSchema as = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID())[0];
						BigDecimal costMA = MCost.getCurrentCost(p, mas[i].getM_AttributeSetInstance_ID(),
								MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID())[0],
									getAD_Org_ID(), p.getCostingMethod(as), mas[i].getQty(),
										0, true, get_TrxName());
						cost = cost.add(costMA);
					}
					setCostAmt(cost);
				}
				
				setIgnoreValidationSave(true);
				upHeader();
				saveEx();
			}
		}
		
		return true;
	}
	
	private boolean m_IgnoreValidationSave = false;
	
	private void setIgnoreValidationSave(boolean isIgnoreValidationSave)
	{
		m_IgnoreValidationSave = isIgnoreValidationSave;
	}
	
	private boolean isIgnoreValidationSave()
	{
		return m_IgnoreValidationSave;
	}
	
	
	private void upHeader()
	{
		String sql = "SELECT SUM(RevenueAmt), SUM(CostAmt)"
				+ " FROM UNS_POSRecapLine WHERE UNS_POSRecap_ID = ?";
		
		List<Object> object = DB.getSQLValueObjectsEx(get_TrxName(), sql, getUNS_POSRecap_ID());
		
		MUNSPOSRecap parent = new MUNSPOSRecap(getCtx(), getUNS_POSRecap_ID(), get_TrxName());
		parent.setTotalRevenueAmt((BigDecimal) object.get(0));
		parent.setTotalCostAmt((BigDecimal) object.get(1));
		parent.saveEx();
	}
	
	public MUNSPOSRecapLineMA[] getMAs()
	{
		List<MUNSPOSRecapLineMA> mas = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSPOSRecapLineMA.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return mas.toArray(new MUNSPOSRecapLineMA[mas.size()]);
	}
}