/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSFNBTableLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSFNBTableLine 
{

    /** TableName=UNS_POSFNBTableLine */
    public static final String Table_Name = "UNS_POSFNBTableLine";

    /** AD_Table_ID=1000439 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name isReserved */
    public static final String COLUMNNAME_isReserved = "isReserved";

	/** Set isReserved	  */
	public void setisReserved (boolean isReserved);

	/** Get isReserved	  */
	public boolean isReserved();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name UNS_POSFNBTable_ID */
    public static final String COLUMNNAME_UNS_POSFNBTable_ID = "UNS_POSFNBTable_ID";

	/** Set FNB Table	  */
	public void setUNS_POSFNBTable_ID (int UNS_POSFNBTable_ID);

	/** Get FNB Table	  */
	public int getUNS_POSFNBTable_ID();

	public com.unicore.model.I_UNS_POSFNBTable getUNS_POSFNBTable() throws RuntimeException;

    /** Column name UNS_POSFNBTableLine_ID */
    public static final String COLUMNNAME_UNS_POSFNBTableLine_ID = "UNS_POSFNBTableLine_ID";

	/** Set Table 	  */
	public void setUNS_POSFNBTableLine_ID (int UNS_POSFNBTableLine_ID);

	/** Get Table 	  */
	public int getUNS_POSFNBTableLine_ID();

    /** Column name UNS_POSFNBTableLine_UU */
    public static final String COLUMNNAME_UNS_POSFNBTableLine_UU = "UNS_POSFNBTableLine_UU";

	/** Set UNS_POSFNBTableLine_UU	  */
	public void setUNS_POSFNBTableLine_UU (String UNS_POSFNBTableLine_UU);

	/** Get UNS_POSFNBTableLine_UU	  */
	public String getUNS_POSFNBTableLine_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();

    /** Column name X */
    public static final String COLUMNNAME_X = "X";

	/** Set Aisle (X).
	  * X dimension, e.g., Aisle
	  */
	public void setX (String X);

	/** Get Aisle (X).
	  * X dimension, e.g., Aisle
	  */
	public String getX();

    /** Column name Y */
    public static final String COLUMNNAME_Y = "Y";

	/** Set Bin (Y).
	  * Y dimension, e.g., Bin
	  */
	public void setY (String Y);

	/** Get Bin (Y).
	  * Y dimension, e.g., Bin
	  */
	public String getY();

    /** Column name Z */
    public static final String COLUMNNAME_Z = "Z";

	/** Set Level (Z).
	  * Z dimension, e.g., Level
	  */
	public void setZ (String Z);

	/** Get Level (Z).
	  * Z dimension, e.g., Level
	  */
	public String getZ();
}
