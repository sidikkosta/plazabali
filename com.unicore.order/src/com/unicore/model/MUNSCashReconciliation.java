/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSCashReconciliation extends X_UNS_CashReconciliation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3533366785397956949L;

	/**
	 * @param ctx
	 * @param UNS_CashReconciliation_ID
	 * @param trxName
	 */
	public MUNSCashReconciliation(Properties ctx,
			int UNS_CashReconciliation_ID, String trxName) {
		super(ctx, UNS_CashReconciliation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCashReconciliation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSCashReconciliation getCreate(Properties ctx, int C_BPartner_ID, java.sql.Timestamp dateTrx,
			int AD_Org_ID, int C_Currency_ID, int BaseCurrency_ID, String trxName)
	{
		MUNSSalesReconciliation recon = MUNSSalesReconciliation.getCreate(ctx, C_BPartner_ID, dateTrx, 
				AD_Org_ID, BaseCurrency_ID, trxName);
		String whereClause = "UNS_SalesReconciliation_ID = ? AND C_Currency_ID = ?";
		MUNSCashReconciliation result = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, whereClause, trxName).setParameters(recon.get_ID(), C_Currency_ID).firstOnly();
		
		if(result == null)
		{
			result = new MUNSCashReconciliation(ctx, 0, trxName);
			result.setUNS_SalesReconciliation_ID(recon.get_ID());
			result.setC_Currency_ID(C_Currency_ID);
			result.setCashDepositAmt(Env.ZERO);
			result.setCashDepositAmtB1(Env.ZERO);
			result.setCashDepositAmtB2(Env.ZERO);
			result.setPayableRefundAmt(Env.ZERO);
			result.saveEx();
		}
		
		return result;
	}
	
	public static String createFrom(MUNSPOSSession ss, boolean reverse)
	{
		MUNSSessionCashAccount[] cashAccts = ss.getCashAcounts(true);
		
		for(int i=0;i<cashAccts.length;i++)
		{
			if(cashAccts[i].getCashDepositAmt().compareTo(Env.ZERO) == 0
					&& cashAccts[i].getShortCashierAmt().compareTo(Env.ZERO) == 0
						&& cashAccts[i].getPayableRefundAmt().compareTo(Env.ZERO) == 0
							&& cashAccts[i].getEstimationShortCashierAmt().compareTo(Env.ZERO) == 0)
				continue;
			
			if(reverse)
			{
				cashAccts[i].setCashDepositAmt(cashAccts[i].getCashDepositAmt().negate());
				cashAccts[i].setCashDepositAmtB1(cashAccts[i].getCashDepositAmtB1().negate());
				cashAccts[i].setCashDepositAmtB2(cashAccts[i].getCashDepositAmtB2().negate());
				cashAccts[i].setPayableRefundAmt(cashAccts[i].getPayableRefundAmt().negate());
				cashAccts[i].setShortCashierAmt(cashAccts[i].getShortCashierAmt().negate());
				cashAccts[i].setEstimationShortCashierAmt(cashAccts[i].getEstimationShortCashierAmt().negate());
			}
			
			MUNSCashReconciliation recon = MUNSCashReconciliation.getCreate(ss.getCtx(), ss.getUNS_POSTerminal().getStore_ID(),
					ss.getDateAcct(), ss.getAD_Org_ID(), cashAccts[i].getC_Currency_ID(), ss.getC_Currency_ID(), ss.get_TrxName());
			recon.setCashDepositAmt(recon.getCashDepositAmt().add(cashAccts[i].getCashDepositAmt()));
			recon.setCashDepositAmtB1(recon.getCashDepositAmtB1().add(cashAccts[i].getCashDepositAmtB1()));
			recon.setCashDepositAmtB2(recon.getCashDepositAmtB2().add(cashAccts[i].getCashDepositAmtB2()));
			recon.setPayableRefundAmt(recon.getPayableRefundAmt().add(cashAccts[i].getPayableRefundAmtB1()));
			recon.setShortCashierAmt(recon.getShortCashierAmt().add(cashAccts[i].getShortCashierAmt()));
			recon.setShortCashierAmtBase1(recon.getShortCashierAmtBase1().add(cashAccts[i].getShortCashierAmtBase1()));
			recon.setShortCashierAmtBase2(recon.getShortCashierAmtBase2().add(cashAccts[i].getShortCashierAmtBase2()));
			recon.setEstimationShortCashierAmt(recon.getEstimationShortCashierAmt().
					add(cashAccts[i].getEstimationShortCashierAmtBase1()));
			if(!recon.save())
				return "Failed when trying create cash reconciliation.";
		}
		
		return null;
	}
	
	public boolean beforeSave(boolean newRecord)
	{
		if(!newRecord && is_ValueChanged(COLUMNNAME_ShortCashierAmt) && isReCalcDepositAmt())
		{
			BigDecimal oldValue = (BigDecimal) get_ValueOld(COLUMNNAME_ShortCashierAmt);
			setCashDepositAmt(getCashDepositAmt().add(oldValue).subtract(getShortCashierAmt()));
		}
		setTotalSalesAmt(getCashDepositAmt().add(getShortCashierAmt()).subtract(getEstimationShortCashierAmt()));
		return true;
	}
	
	private boolean m_reCalcDepositAmt = false;
	
	private boolean isReCalcDepositAmt()
	{
		return m_reCalcDepositAmt;
	}
	
	public void setReCalcDepositAmt(boolean reCalcDepositAmt)
	{
		m_reCalcDepositAmt = reCalcDepositAmt;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{		
		String sql = "UPDATE UNS_SalesReconciliation sr SET TotalSalesAmt ="
				+ " (SELECT COALESCE(SUM(cr.CashDepositAmtB1),0) FROM UNS_CashReconciliation cr"
				+ " WHERE cr.UNS_SalesReconciliation_ID = sr.UNS_SalesReconciliation_ID),"
				+ " TotalShortCashierAmt ="
				+ " (SELECT COALESCE(SUM(cr.ShortCashierAmtBase1),0) FROM UNS_CashReconciliation cr"
				+ " WHERE cr.UNS_SalesReconciliation_ID = sr.UNS_SalesReconciliation_ID)"
				+ " WHERE sr.UNS_SalesReconciliation_ID = ?";
		
		if(DB.executeUpdate(sql, getUNS_SalesReconciliation_ID(), get_TrxName()) < 0)
		{
			log.saveError("Error", "Failed when trying update total sales amount on sales reconciliation.");
			return false;
		}
		
		MUNSSalesReconciliation.reCalcGrandTotal(getUNS_SalesReconciliation_ID(), get_TrxName());
		
		return true;
	}
}