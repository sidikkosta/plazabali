/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_BPartner_Driver
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_BPartner_Driver extends PO implements I_UNS_BPartner_Driver, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170328L;

    /** Standard Constructor */
    public X_UNS_BPartner_Driver (Properties ctx, int UNS_BPartner_Driver_ID, String trxName)
    {
      super (ctx, UNS_BPartner_Driver_ID, trxName);
      /** if (UNS_BPartner_Driver_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_UNS_BPartner_Driver (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_BPartner_Driver[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Driver's License.
		@param DriverLicense 
		Driver's License Number
	  */
	public void setDriverLicense (String DriverLicense)
	{
		set_Value (COLUMNNAME_DriverLicense, DriverLicense);
	}

	/** Get Driver's License.
		@return Driver's License Number
	  */
	public String getDriverLicense () 
	{
		return (String)get_Value(COLUMNNAME_DriverLicense);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set UNS_BPartner_Driver.
		@param UNS_BPartner_Driver_ID UNS_BPartner_Driver	  */
	public void setUNS_BPartner_Driver_ID (int UNS_BPartner_Driver_ID)
	{
		if (UNS_BPartner_Driver_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_BPartner_Driver_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_BPartner_Driver_ID, Integer.valueOf(UNS_BPartner_Driver_ID));
	}

	/** Get UNS_BPartner_Driver.
		@return UNS_BPartner_Driver	  */
	public int getUNS_BPartner_Driver_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_BPartner_Driver_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_BPartner_Driver_UU.
		@param UNS_BPartner_Driver_UU UNS_BPartner_Driver_UU	  */
	public void setUNS_BPartner_Driver_UU (String UNS_BPartner_Driver_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_BPartner_Driver_UU, UNS_BPartner_Driver_UU);
	}

	/** Get UNS_BPartner_Driver_UU.
		@return UNS_BPartner_Driver_UU	  */
	public String getUNS_BPartner_Driver_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_BPartner_Driver_UU);
	}
}