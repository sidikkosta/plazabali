/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_POSTrx
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POSTrx extends PO implements I_UNS_POSTrx, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20191122L;

    /** Standard Constructor */
    public X_UNS_POSTrx (Properties ctx, int UNS_POSTrx_ID, String trxName)
    {
      super (ctx, UNS_POSTrx_ID, trxName);
      /** if (UNS_POSTrx_ID == 0)
        {
			setC_BPartner_ID (0);
			setC_Currency_ID (0);
			setC_DocType_ID (0);
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setDocumentNo (null);
			setGrandTotal (Env.ZERO);
			setIsApproved (false);
// N
			setIsPaid (false);
// N
			setM_PriceList_ID (0);
			setM_Warehouse_ID (0);
			setProcessed (false);
			setSalesRep_ID (0);
			setTaxAmt (Env.ZERO);
// 0
			setTotalAmt (Env.ZERO);
			setUNS_POS_Session_ID (0);
			setUNS_POSTrx_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POSTrx (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POSTrx[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Shop.
		@param C_BPartner_ID 
		The Shop
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Shop.
		@return The Shop
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner_Location)MTable.get(getCtx(), org.compiere.model.I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Shop Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Shop Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException
    {
		return (org.compiere.model.I_C_DocType)MTable.get(getCtx(), org.compiere.model.I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_ValueNoCheck (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_Value (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Discount Note.
		@param DiscNote Discount Note	  */
	public void setDiscNote (String DiscNote)
	{
		set_Value (COLUMNNAME_DiscNote, DiscNote);
	}

	/** Get Discount Note.
		@return Discount Note	  */
	public String getDiscNote () 
	{
		return (String)get_Value(COLUMNNAME_DiscNote);
	}

	/** Set Discount %.
		@param Discount 
		Discount in percent
	  */
	public void setDiscount (BigDecimal Discount)
	{
		set_Value (COLUMNNAME_Discount, Discount);
	}

	/** Get Discount %.
		@return Discount in percent
	  */
	public BigDecimal getDiscount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Discount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Amount.
		@param DiscountAmt 
		Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt)
	{
		set_Value (COLUMNNAME_DiscountAmt, DiscountAmt);
	}

	/** Get Discount Amount.
		@return Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DiscountAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Refference.
		@param DiscReff Discount Refference	  */
	public void setDiscReff (String DiscReff)
	{
		set_Value (COLUMNNAME_DiscReff, DiscReff);
	}

	/** Get Discount Refference.
		@return Discount Refference	  */
	public String getDiscReff () 
	{
		return (String)get_Value(COLUMNNAME_DiscReff);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Grand Total.
		@param GrandTotal 
		Total amount of document
	  */
	public void setGrandTotal (BigDecimal GrandTotal)
	{
		set_ValueNoCheck (COLUMNNAME_GrandTotal, GrandTotal);
	}

	/** Get Grand Total.
		@return Total amount of document
	  */
	public BigDecimal getGrandTotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrandTotal);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_ValueNoCheck (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Paid.
		@param IsPaid 
		The document is paid
	  */
	public void setIsPaid (boolean IsPaid)
	{
		set_Value (COLUMNNAME_IsPaid, Boolean.valueOf(IsPaid));
	}

	/** Get Paid.
		@return The document is paid
	  */
	public boolean isPaid () 
	{
		Object oo = get_Value(COLUMNNAME_IsPaid);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_PriceList getM_PriceList() throws RuntimeException
    {
		return (org.compiere.model.I_M_PriceList)MTable.get(getCtx(), org.compiere.model.I_M_PriceList.Table_Name)
			.getPO(getM_PriceList_ID(), get_TrxName());	}

	/** Set Price List.
		@param M_PriceList_ID 
		Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID)
	{
		if (M_PriceList_ID < 1) 
			set_Value (COLUMNNAME_M_PriceList_ID, null);
		else 
			set_Value (COLUMNNAME_M_PriceList_ID, Integer.valueOf(M_PriceList_ID));
	}

	/** Get Price List.
		@return Unique identifier of a Price List
	  */
	public int getM_PriceList_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_PriceList_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Print Document.
		@param PrintDocument Print Document	  */
	public void setPrintDocument (String PrintDocument)
	{
		set_Value (COLUMNNAME_PrintDocument, PrintDocument);
	}

	/** Get Print Document.
		@return Print Document	  */
	public String getPrintDocument () 
	{
		return (String)get_Value(COLUMNNAME_PrintDocument);
	}

	/** Set Print POS Return.
		@param PrintPOSReturn Print POS Return	  */
	public void setPrintPOSReturn (String PrintPOSReturn)
	{
		set_Value (COLUMNNAME_PrintPOSReturn, PrintPOSReturn);
	}

	/** Get Print POS Return.
		@return Print POS Return	  */
	public String getPrintPOSReturn () 
	{
		return (String)get_Value(COLUMNNAME_PrintPOSReturn);
	}

	/** Set PrintStrukRetur.
		@param PrintStrukRetur PrintStrukRetur	  */
	public void setPrintStrukRetur (String PrintStrukRetur)
	{
		set_Value (COLUMNNAME_PrintStrukRetur, PrintStrukRetur);
	}

	/** Get PrintStrukRetur.
		@return PrintStrukRetur	  */
	public String getPrintStrukRetur () 
	{
		return (String)get_Value(COLUMNNAME_PrintStrukRetur);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.unicore.model.I_UNS_POSTrx getReference() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTrx)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTrx.Table_Name)
			.getPO(getReference_ID(), get_TrxName());	}

	/** Set Refrerence Record.
		@param Reference_ID Refrerence Record	  */
	public void setReference_ID (int Reference_ID)
	{
		if (Reference_ID < 1) 
			set_Value (COLUMNNAME_Reference_ID, null);
		else 
			set_Value (COLUMNNAME_Reference_ID, Integer.valueOf(Reference_ID));
	}

	/** Get Refrerence Record.
		@return Refrerence Record	  */
	public int getReference_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Reference_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Reference UU.
		@param ReferenceUU Reference UU	  */
	public void setReferenceUU (String ReferenceUU)
	{
		set_Value (COLUMNNAME_ReferenceUU, ReferenceUU);
	}

	/** Get Reference UU.
		@return Reference UU	  */
	public String getReferenceUU () 
	{
		return (String)get_Value(COLUMNNAME_ReferenceUU);
	}

	/** Set Report Currency Rate.
		@param RepCurrRate Report Currency Rate	  */
	public void setRepCurrRate (String RepCurrRate)
	{
		set_Value (COLUMNNAME_RepCurrRate, RepCurrRate);
	}

	/** Get Report Currency Rate.
		@return Report Currency Rate	  */
	public String getRepCurrRate () 
	{
		return (String)get_Value(COLUMNNAME_RepCurrRate);
	}

	public org.compiere.model.I_AD_User getSalesRep() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getSalesRep_ID(), get_TrxName());	}

	/** Set Sales Representative.
		@param SalesRep_ID 
		Sales Representative or Company Agent
	  */
	public void setSalesRep_ID (int SalesRep_ID)
	{
		if (SalesRep_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_SalesRep_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_SalesRep_ID, Integer.valueOf(SalesRep_ID));
	}

	/** Get Sales Representative.
		@return Sales Representative or Company Agent
	  */
	public int getSalesRep_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SalesRep_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Service Charge.
		@param ServiceCharge Service Charge	  */
	public void setServiceCharge (BigDecimal ServiceCharge)
	{
		set_Value (COLUMNNAME_ServiceCharge, ServiceCharge);
	}

	/** Get Service Charge.
		@return Service Charge	  */
	public BigDecimal getServiceCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ServiceCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Tax Amount.
		@param TaxAmt 
		Tax Amount for a document
	  */
	public void setTaxAmt (BigDecimal TaxAmt)
	{
		set_Value (COLUMNNAME_TaxAmt, TaxAmt);
	}

	/** Get Tax Amount.
		@return Tax Amount for a document
	  */
	public BigDecimal getTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Amount.
		@param TotalAmt 
		Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt)
	{
		set_Value (COLUMNNAME_TotalAmt, TotalAmt);
	}

	/** Get Total Amount.
		@return Total Amount
	  */
	public BigDecimal getTotalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Transaction No.
		@param TrxNo 
		Transaction Number
	  */
	public void setTrxNo (String TrxNo)
	{
		set_ValueNoCheck (COLUMNNAME_TrxNo, TrxNo);
	}

	/** Get Transaction No.
		@return Transaction Number
	  */
	public String getTrxNo () 
	{
		return (String)get_Value(COLUMNNAME_TrxNo);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getTrxNo());
    }

	public com.unicore.model.I_UNS_CustomerInfo getUNS_CustomerInfo() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_CustomerInfo)MTable.get(getCtx(), com.unicore.model.I_UNS_CustomerInfo.Table_Name)
			.getPO(getUNS_CustomerInfo_ID(), get_TrxName());	}

	/** Set Customer Info.
		@param UNS_CustomerInfo_ID Customer Info	  */
	public void setUNS_CustomerInfo_ID (int UNS_CustomerInfo_ID)
	{
		if (UNS_CustomerInfo_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CustomerInfo_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CustomerInfo_ID, Integer.valueOf(UNS_CustomerInfo_ID));
	}

	/** Get Customer Info.
		@return Customer Info	  */
	public int getUNS_CustomerInfo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CustomerInfo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_DiscountReff getUNS_DiscountReff() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_DiscountReff)MTable.get(getCtx(), com.unicore.model.I_UNS_DiscountReff.Table_Name)
			.getPO(getUNS_DiscountReff_ID(), get_TrxName());	}

	/** Set Discount Refference.
		@param UNS_DiscountReff_ID Discount Refference	  */
	public void setUNS_DiscountReff_ID (int UNS_DiscountReff_ID)
	{
		if (UNS_DiscountReff_ID < 1) 
			set_Value (COLUMNNAME_UNS_DiscountReff_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_DiscountReff_ID, Integer.valueOf(UNS_DiscountReff_ID));
	}

	/** Get Discount Refference.
		@return Discount Refference	  */
	public int getUNS_DiscountReff_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_DiscountReff_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POSFNBTableLine getUNS_POSFNBTableLine() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSFNBTableLine)MTable.get(getCtx(), com.unicore.model.I_UNS_POSFNBTableLine.Table_Name)
			.getPO(getUNS_POSFNBTableLine_ID(), get_TrxName());	}

	/** Set Table .
		@param UNS_POSFNBTableLine_ID Table 	  */
	public void setUNS_POSFNBTableLine_ID (int UNS_POSFNBTableLine_ID)
	{
		if (UNS_POSFNBTableLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTableLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSFNBTableLine_ID, Integer.valueOf(UNS_POSFNBTableLine_ID));
	}

	/** Get Table .
		@return Table 	  */
	public int getUNS_POSFNBTableLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSFNBTableLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POSTerminal getUNS_POSTerminal() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTerminal)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTerminal.Table_Name)
			.getPO(getUNS_POSTerminal_ID(), get_TrxName());	}

	/** Set POS Terminal.
		@param UNS_POSTerminal_ID POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID)
	{
		if (UNS_POSTerminal_ID < 1) 
			set_Value (COLUMNNAME_UNS_POSTerminal_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_POSTerminal_ID, Integer.valueOf(UNS_POSTerminal_ID));
	}

	/** Get POS Terminal.
		@return POS Terminal	  */
	public int getUNS_POSTerminal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTerminal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set POS Transactions.
		@param UNS_POSTrx_ID POS Transactions	  */
	public void setUNS_POSTrx_ID (int UNS_POSTrx_ID)
	{
		if (UNS_POSTrx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_ID, Integer.valueOf(UNS_POSTrx_ID));
	}

	/** Get POS Transactions.
		@return POS Transactions	  */
	public int getUNS_POSTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POSTrx_UU.
		@param UNS_POSTrx_UU UNS_POSTrx_UU	  */
	public void setUNS_POSTrx_UU (String UNS_POSTrx_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POSTrx_UU, UNS_POSTrx_UU);
	}

	/** Get UNS_POSTrx_UU.
		@return UNS_POSTrx_UU	  */
	public String getUNS_POSTrx_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POSTrx_UU);
	}
	
	/** Set Customer Name.
	@param CustomerName Customer Name	  */
	public void setCustomerName (String CustomerName)
	{
		set_Value (COLUMNNAME_CustomerName, CustomerName);
	}
	
	/** Get Customer Name.
		@return Customer Name	  */
	public String getCustomerName () 
	{
		return (String)get_Value(COLUMNNAME_CustomerName);
	}
}