/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSCardType extends X_UNS_CardType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8917824266250311514L;

	/**
	 * @param ctx
	 * @param UNS_CardType_ID
	 * @param trxName
	 */
	public MUNSCardType(Properties ctx, int UNS_CardType_ID, String trxName) {
		super(ctx, UNS_CardType_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCardType(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	
	public static int getIDByNameOrValue (String trxName, String param)
	{
		String sql = "SELECT UNS_CardType_ID FROM UNS_CardType WHERE UPPER(Value) = UPPER(?) OR UPPER(Name) = UPPER(?)";
		int id = DB.getSQLValue(trxName, sql, param, param);
		if (id <= 0)
			id = DB.getSQLValue(trxName, sql, "08", "08");
		return id;
	}
	
	private List<String> parseValue (String value, String splited)
	{
		List<String> list = new ArrayList<>();
		String[] values = value.split(splited);
		for (int i=0; i<values.length; i++)
		{
			if (values[i].contains("-"))
				list.addAll(parseValue(values[i], "-"));
			else
				list.add(values[i]);
		}
		
		if (splited.equals("-") && list.size() == 2)
		{
			String startStr = list.get(0);
			String endStr = list.get(1);
			int start = -1;
			int end = -1;
			try
			{
				start = Integer.valueOf(startStr);
				end = Integer.valueOf(endStr);
			}
			catch (NumberFormatException ex)
			{
				ex.printStackTrace();
			}
			
			end--;
			
			while (end > start)
			{
				list.add(Integer.toString(end));
				end--;
			}
		}
		
		return list;
	}
	
	public String[] getParsedValue ()
	{
		List<String> values = parseValue(getValue(), ",");
		String[] retVals = new String[values.size()];
		values.toArray(retVals);
		Arrays.sort(retVals);
		return retVals;
	}
	
	public static MUNSCardType[] get (String trxName)
	{
		List<MUNSCardType> list = Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, "IsActive = 'Y'", 
				trxName).list();
		MUNSCardType[] retVal = new MUNSCardType[list.size()];
		list.toArray(retVal);
		return retVal;
	}
}
