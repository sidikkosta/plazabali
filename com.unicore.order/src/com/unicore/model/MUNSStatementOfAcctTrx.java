/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Burhani Adam
 *
 */
public class MUNSStatementOfAcctTrx extends X_UNS_StatementOfAcct_Trx {

	/**
	 * 
	 */
	private static final long serialVersionUID = 395568676535691882L;

	/**
	 * @param ctx
	 * @param UNS_StatementOfAcct_Trx_ID
	 * @param trxName
	 */
	public MUNSStatementOfAcctTrx(Properties ctx,
			int UNS_StatementOfAcct_Trx_ID, String trxName) {
		super(ctx, UNS_StatementOfAcct_Trx_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSStatementOfAcctTrx(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

}
