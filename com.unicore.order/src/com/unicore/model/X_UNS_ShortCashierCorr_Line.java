/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_ShortCashierCorr_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ShortCashierCorr_Line extends PO implements I_UNS_ShortCashierCorr_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190207L;

    /** Standard Constructor */
    public X_UNS_ShortCashierCorr_Line (Properties ctx, int UNS_ShortCashierCorr_Line_ID, String trxName)
    {
      super (ctx, UNS_ShortCashierCorr_Line_ID, trxName);
      /** if (UNS_ShortCashierCorr_Line_ID == 0)
        {
			setBase1Currency_ID (0);
			setBase2Currency_ID (0);
			setCashier_ID (0);
			setCorrectionAmt (Env.ZERO);
// 0
			setCorrectionAmtB1 (Env.ZERO);
// 0
			setCorrectionAmtB2 (Env.ZERO);
// 0
			setDateTrx (new Timestamp( System.currentTimeMillis() ));
			setOriginalAmt (Env.ZERO);
// 0
			setOriginalAmtB1 (Env.ZERO);
// 0
			setOriginalAmtB2 (Env.ZERO);
// 0
			setStore_ID (0);
			setUNS_Employee_ID (0);
			setUNS_POS_Session_ID (0);
			setUNS_ShortCashierCorrection_ID (0);
			setUNS_ShortCashierCorr_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ShortCashierCorr_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ShortCashierCorr_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getBase1Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getBase1Currency_ID(), get_TrxName());	}

	/** Set Base 1 Currency.
		@param Base1Currency_ID Base 1 Currency	  */
	public void setBase1Currency_ID (int Base1Currency_ID)
	{
		if (Base1Currency_ID < 1) 
			set_Value (COLUMNNAME_Base1Currency_ID, null);
		else 
			set_Value (COLUMNNAME_Base1Currency_ID, Integer.valueOf(Base1Currency_ID));
	}

	/** Get Base 1 Currency.
		@return Base 1 Currency	  */
	public int getBase1Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Base1Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getBase2Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getBase2Currency_ID(), get_TrxName());	}

	/** Set Base 2 Currency.
		@param Base2Currency_ID Base 2 Currency	  */
	public void setBase2Currency_ID (int Base2Currency_ID)
	{
		if (Base2Currency_ID < 1) 
			set_Value (COLUMNNAME_Base2Currency_ID, null);
		else 
			set_Value (COLUMNNAME_Base2Currency_ID, Integer.valueOf(Base2Currency_ID));
	}

	/** Get Base 2 Currency.
		@return Base 2 Currency	  */
	public int getBase2Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Base2Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getCashier_ID(), get_TrxName());	}

	/** Set Cashier.
		@param Cashier_ID Cashier	  */
	public void setCashier_ID (int Cashier_ID)
	{
		if (Cashier_ID < 1) 
			set_Value (COLUMNNAME_Cashier_ID, null);
		else 
			set_Value (COLUMNNAME_Cashier_ID, Integer.valueOf(Cashier_ID));
	}

	/** Get Cashier.
		@return Cashier	  */
	public int getCashier_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Cashier_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Correction Amt Periodic.
		@param CorrectionAmt Correction Amt Periodic	  */
	public void setCorrectionAmt (BigDecimal CorrectionAmt)
	{
		set_Value (COLUMNNAME_CorrectionAmt, CorrectionAmt);
	}

	/** Get Correction Amt Periodic.
		@return Correction Amt Periodic	  */
	public BigDecimal getCorrectionAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CorrectionAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Correction Amount Base 1.
		@param CorrectionAmtB1 Correction Amount Base 1	  */
	public void setCorrectionAmtB1 (BigDecimal CorrectionAmtB1)
	{
		set_Value (COLUMNNAME_CorrectionAmtB1, CorrectionAmtB1);
	}

	/** Get Correction Amount Base 1.
		@return Correction Amount Base 1	  */
	public BigDecimal getCorrectionAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CorrectionAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Correction Amount Base 2.
		@param CorrectionAmtB2 Correction Amount Base 2	  */
	public void setCorrectionAmtB2 (BigDecimal CorrectionAmtB2)
	{
		set_Value (COLUMNNAME_CorrectionAmtB2, CorrectionAmtB2);
	}

	/** Get Correction Amount Base 2.
		@return Correction Amount Base 2	  */
	public BigDecimal getCorrectionAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CorrectionAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Transaction Date.
		@param DateTrx 
		Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx)
	{
		set_ValueNoCheck (COLUMNNAME_DateTrx, DateTrx);
	}

	/** Get Transaction Date.
		@return Transaction Date
	  */
	public Timestamp getDateTrx () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTrx);
	}

	/** Set Original Amount.
		@param OriginalAmt Original Amount	  */
	public void setOriginalAmt (BigDecimal OriginalAmt)
	{
		set_Value (COLUMNNAME_OriginalAmt, OriginalAmt);
	}

	/** Get Original Amount.
		@return Original Amount	  */
	public BigDecimal getOriginalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Amount Base 1.
		@param OriginalAmtB1 Original Amount Base 1	  */
	public void setOriginalAmtB1 (BigDecimal OriginalAmtB1)
	{
		set_Value (COLUMNNAME_OriginalAmtB1, OriginalAmtB1);
	}

	/** Get Original Amount Base 1.
		@return Original Amount Base 1	  */
	public BigDecimal getOriginalAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Amount Base 2.
		@param OriginalAmtB2 Original Amount Base 2	  */
	public void setOriginalAmtB2 (BigDecimal OriginalAmtB2)
	{
		set_Value (COLUMNNAME_OriginalAmtB2, OriginalAmtB2);
	}

	/** Get Original Amount Base 2.
		@return Original Amount Base 2	  */
	public BigDecimal getOriginalAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OriginalAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getStore_ID(), get_TrxName());	}

	/** Set Store.
		@param Store_ID Store	  */
	public void setStore_ID (int Store_ID)
	{
		if (Store_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Store_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Store_ID, Integer.valueOf(Store_ID));
	}

	/** Get Store.
		@return Store	  */
	public int getStore_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getStore_ID()));
    }

	/** Set Employee.
		@param UNS_Employee_ID Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID)
	{
		if (UNS_Employee_ID < 1) 
			set_Value (COLUMNNAME_UNS_Employee_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Employee_ID, Integer.valueOf(UNS_Employee_ID));
	}

	/** Get Employee.
		@return Employee	  */
	public int getUNS_Employee_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Employee_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_ShortCashierCorrection getUNS_ShortCashierCorrection() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_ShortCashierCorrection)MTable.get(getCtx(), com.unicore.model.I_UNS_ShortCashierCorrection.Table_Name)
			.getPO(getUNS_ShortCashierCorrection_ID(), get_TrxName());	}

	/** Set Short Cashier Correction.
		@param UNS_ShortCashierCorrection_ID Short Cashier Correction	  */
	public void setUNS_ShortCashierCorrection_ID (int UNS_ShortCashierCorrection_ID)
	{
		if (UNS_ShortCashierCorrection_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShortCashierCorrection_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShortCashierCorrection_ID, Integer.valueOf(UNS_ShortCashierCorrection_ID));
	}

	/** Get Short Cashier Correction.
		@return Short Cashier Correction	  */
	public int getUNS_ShortCashierCorrection_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShortCashierCorrection_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Short Cashier Correction Line.
		@param UNS_ShortCashierCorr_Line_ID Short Cashier Correction Line	  */
	public void setUNS_ShortCashierCorr_Line_ID (int UNS_ShortCashierCorr_Line_ID)
	{
		if (UNS_ShortCashierCorr_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShortCashierCorr_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShortCashierCorr_Line_ID, Integer.valueOf(UNS_ShortCashierCorr_Line_ID));
	}

	/** Get Short Cashier Correction Line.
		@return Short Cashier Correction Line	  */
	public int getUNS_ShortCashierCorr_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShortCashierCorr_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ShortCashierCorr_Line_UU.
		@param UNS_ShortCashierCorr_Line_UU UNS_ShortCashierCorr_Line_UU	  */
	public void setUNS_ShortCashierCorr_Line_UU (String UNS_ShortCashierCorr_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ShortCashierCorr_Line_UU, UNS_ShortCashierCorr_Line_UU);
	}

	/** Get UNS_ShortCashierCorr_Line_UU.
		@return UNS_ShortCashierCorr_Line_UU	  */
	public String getUNS_ShortCashierCorr_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ShortCashierCorr_Line_UU);
	}
}