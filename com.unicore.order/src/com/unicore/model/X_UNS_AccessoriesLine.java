/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_AccessoriesLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_AccessoriesLine extends PO implements I_UNS_AccessoriesLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160608L;

    /** Standard Constructor */
    public X_UNS_AccessoriesLine (Properties ctx, int UNS_AccessoriesLine_ID, String trxName)
    {
      super (ctx, UNS_AccessoriesLine_ID, trxName);
      /** if (UNS_AccessoriesLine_ID == 0)
        {
			setAccessoriesType (null);
			setC_UOM_ID (0);
			setProductAccessories_ID (0);
			setQty (Env.ZERO);
// 0
			setUNS_AccessoriesLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_AccessoriesLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_AccessoriesLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Mandatory = MND */
	public static final String ACCESSORIESTYPE_Mandatory = "MND";
	/** Optional = OPT */
	public static final String ACCESSORIESTYPE_Optional = "OPT";
	/** Set Accessories Type.
		@param AccessoriesType Accessories Type	  */
	public void setAccessoriesType (String AccessoriesType)
	{

		set_Value (COLUMNNAME_AccessoriesType, AccessoriesType);
	}

	/** Get Accessories Type.
		@return Accessories Type	  */
	public String getAccessoriesType () 
	{
		return (String)get_Value(COLUMNNAME_AccessoriesType);
	}

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getC_OrderLine_ID(), get_TrxName());	}

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_Product getProductAccessories() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getProductAccessories_ID(), get_TrxName());	}

	/** Set Product Accessories.
		@param ProductAccessories_ID Product Accessories	  */
	public void setProductAccessories_ID (int ProductAccessories_ID)
	{
		if (ProductAccessories_ID < 1) 
			set_Value (COLUMNNAME_ProductAccessories_ID, null);
		else 
			set_Value (COLUMNNAME_ProductAccessories_ID, Integer.valueOf(ProductAccessories_ID));
	}

	/** Get Product Accessories.
		@return Product Accessories	  */
	public int getProductAccessories_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ProductAccessories_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Accessories Line.
		@param UNS_AccessoriesLine_ID Accessories Line	  */
	public void setUNS_AccessoriesLine_ID (int UNS_AccessoriesLine_ID)
	{
		if (UNS_AccessoriesLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AccessoriesLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AccessoriesLine_ID, Integer.valueOf(UNS_AccessoriesLine_ID));
	}

	/** Get Accessories Line.
		@return Accessories Line	  */
	public int getUNS_AccessoriesLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AccessoriesLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_AccessoriesLine_UU.
		@param UNS_AccessoriesLine_UU UNS_AccessoriesLine_UU	  */
	public void setUNS_AccessoriesLine_UU (String UNS_AccessoriesLine_UU)
	{
		set_Value (COLUMNNAME_UNS_AccessoriesLine_UU, UNS_AccessoriesLine_UU);
	}

	/** Get UNS_AccessoriesLine_UU.
		@return UNS_AccessoriesLine_UU	  */
	public String getUNS_AccessoriesLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_AccessoriesLine_UU);
	}

	/** Set Pre Order Detail.
		@param UNS_PreOrder_Line_ID Pre Order Detail	  */
	public void setUNS_PreOrder_Line_ID (int UNS_PreOrder_Line_ID)
	{
		if (UNS_PreOrder_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_Line_ID, Integer.valueOf(UNS_PreOrder_Line_ID));
	}

	/** Get Pre Order Detail.
		@return Pre Order Detail	  */
	public int getUNS_PreOrder_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PreOrder_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_ProductAccessories getUNS_ProductAccessories() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_ProductAccessories)MTable.get(getCtx(), com.unicore.model.I_UNS_ProductAccessories.Table_Name)
			.getPO(getUNS_ProductAccessories_ID(), get_TrxName());	}

	/** Set Product Accessories.
		@param UNS_ProductAccessories_ID Product Accessories	  */
	public void setUNS_ProductAccessories_ID (int UNS_ProductAccessories_ID)
	{
		if (UNS_ProductAccessories_ID < 1) 
			set_Value (COLUMNNAME_UNS_ProductAccessories_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_ProductAccessories_ID, Integer.valueOf(UNS_ProductAccessories_ID));
	}

	/** Get Product Accessories.
		@return Product Accessories	  */
	public int getUNS_ProductAccessories_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ProductAccessories_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}