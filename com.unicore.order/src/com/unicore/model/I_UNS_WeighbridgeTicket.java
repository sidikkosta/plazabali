/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_WeighbridgeTicket
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_WeighbridgeTicket 
{

    /** TableName=UNS_WeighbridgeTicket */
    public static final String Table_Name = "UNS_WeighbridgeTicket";

    /** AD_Table_ID=1000278 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name CancelProcess */
    public static final String COLUMNNAME_CancelProcess = "CancelProcess";

	/** Set Cancel Process	  */
	public void setCancelProcess (String CancelProcess);

	/** Get Cancel Process	  */
	public String getCancelProcess();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_BPartner_Location_ID */
    public static final String COLUMNNAME_C_BPartner_Location_ID = "C_BPartner_Location_ID";

	/** Set Partner Location.
	  * Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID);

	/** Get Partner Location.
	  * Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID();

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException;

    /** Column name C_DocType_ID */
    public static final String COLUMNNAME_C_DocType_ID = "C_DocType_ID";

	/** Set Document Type.
	  * Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID);

	/** Get Document Type.
	  * Document type or rules
	  */
	public int getC_DocType_ID();

	public org.compiere.model.I_C_DocType getC_DocType() throws RuntimeException;

    /** Column name ChangeOrder */
    public static final String COLUMNNAME_ChangeOrder = "ChangeOrder";

	/** Set Change Order	  */
	public void setChangeOrder (String ChangeOrder);

	/** Get Change Order	  */
	public String getChangeOrder();

    /** Column name ChangeTicketContract */
    public static final String COLUMNNAME_ChangeTicketContract = "ChangeTicketContract";

	/** Set Change Ticket Contract	  */
	public void setChangeTicketContract (String ChangeTicketContract);

	/** Get Change Ticket Contract	  */
	public String getChangeTicketContract();

    /** Column name C_Invoice_ID */
    public static final String COLUMNNAME_C_Invoice_ID = "C_Invoice_ID";

	/** Set Invoice.
	  * Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID);

	/** Get Invoice.
	  * Invoice Identifier
	  */
	public int getC_Invoice_ID();

	public org.compiere.model.I_C_Invoice getC_Invoice() throws RuntimeException;

    /** Column name C_InvoiceLine_ID */
    public static final String COLUMNNAME_C_InvoiceLine_ID = "C_InvoiceLine_ID";

	/** Set Invoice Line.
	  * Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID);

	/** Get Invoice Line.
	  * Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID();

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException;

    /** Column name C_Order_ID */
    public static final String COLUMNNAME_C_Order_ID = "C_Order_ID";

	/** Set Order.
	  * Order
	  */
	public void setC_Order_ID (int C_Order_ID);

	/** Get Order.
	  * Order
	  */
	public int getC_Order_ID();

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException;

    /** Column name C_OrderLine_ID */
    public static final String COLUMNNAME_C_OrderLine_ID = "C_OrderLine_ID";

	/** Set Sales Order Line.
	  * Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID);

	/** Get Sales Order Line.
	  * Sales Order Line
	  */
	public int getC_OrderLine_ID();

	public org.compiere.model.I_C_OrderLine getC_OrderLine() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CreateIntransitMovement */
    public static final String COLUMNNAME_CreateIntransitMovement = "CreateIntransitMovement";

	/** Set Create Intransit Iventory Move	  */
	public void setCreateIntransitMovement (String CreateIntransitMovement);

	/** Get Create Intransit Iventory Move	  */
	public String getCreateIntransitMovement();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name DriverLicense */
    public static final String COLUMNNAME_DriverLicense = "DriverLicense";

	/** Set Driver's License.
	  * Driver's License Number
	  */
	public void setDriverLicense (String DriverLicense);

	/** Get Driver's License.
	  * Driver's License Number
	  */
	public String getDriverLicense();

    /** Column name GrossWeight */
    public static final String COLUMNNAME_GrossWeight = "GrossWeight";

	/** Set Gross Weight.
	  * Weight of a product (Gross)
	  */
	public void setGrossWeight (BigDecimal GrossWeight);

	/** Get Gross Weight.
	  * Weight of a product (Gross)
	  */
	public BigDecimal getGrossWeight();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsApproved */
    public static final String COLUMNNAME_IsApproved = "IsApproved";

	/** Set Approved.
	  * Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved);

	/** Get Approved.
	  * Indicates if this document requires approval
	  */
	public boolean isApproved();

    /** Column name IsCancelled */
    public static final String COLUMNNAME_IsCancelled = "IsCancelled";

	/** Set Cancelled.
	  * The transaction was cancelled
	  */
	public void setIsCancelled (boolean IsCancelled);

	/** Get Cancelled.
	  * The transaction was cancelled
	  */
	public boolean isCancelled();

    /** Column name IsDumpTruck */
    public static final String COLUMNNAME_IsDumpTruck = "IsDumpTruck";

	/** Set Is Dump Truck	  */
	public void setIsDumpTruck (boolean IsDumpTruck);

	/** Get Is Dump Truck	  */
	public boolean isDumpTruck();

    /** Column name IsOriginalTicket */
    public static final String COLUMNNAME_IsOriginalTicket = "IsOriginalTicket";

	/** Set Is Original Ticket.
	  * This is to indicate if this ticket is the ticket record being splitted into another order.
	  */
	public void setIsOriginalTicket (boolean IsOriginalTicket);

	/** Get Is Original Ticket.
	  * This is to indicate if this ticket is the ticket record being splitted into another order.
	  */
	public boolean isOriginalTicket();

    /** Column name IsSOTrx */
    public static final String COLUMNNAME_IsSOTrx = "IsSOTrx";

	/** Set Sales Transaction.
	  * This is a Sales Transaction
	  */
	public void setIsSOTrx (boolean IsSOTrx);

	/** Get Sales Transaction.
	  * This is a Sales Transaction
	  */
	public boolean isSOTrx();

    /** Column name IsSplitOrder */
    public static final String COLUMNNAME_IsSplitOrder = "IsSplitOrder";

	/** Set Is Split Order.
	  * Is this document being split into two different orders?
	  */
	public void setIsSplitOrder (boolean IsSplitOrder);

	/** Get Is Split Order.
	  * Is this document being split into two different orders?
	  */
	public boolean isSplitOrder();

    /** Column name M_InOut_ID */
    public static final String COLUMNNAME_M_InOut_ID = "M_InOut_ID";

	/** Set Shipment/Receipt.
	  * Material Shipment Document
	  */
	public void setM_InOut_ID (int M_InOut_ID);

	/** Get Shipment/Receipt.
	  * Material Shipment Document
	  */
	public int getM_InOut_ID();

	public org.compiere.model.I_M_InOut getM_InOut() throws RuntimeException;

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public org.compiere.model.I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_MovementLineConfirm_ID */
    public static final String COLUMNNAME_M_MovementLineConfirm_ID = "M_MovementLineConfirm_ID";

	/** Set Move Line Confirm.
	  * Inventory Move Line Confirmation
	  */
	public void setM_MovementLineConfirm_ID (int M_MovementLineConfirm_ID);

	/** Get Move Line Confirm.
	  * Inventory Move Line Confirmation
	  */
	public int getM_MovementLineConfirm_ID();

	public org.compiere.model.I_M_MovementLineConfirm getM_MovementLineConfirm() throws RuntimeException;

    /** Column name M_MovementLine_ID */
    public static final String COLUMNNAME_M_MovementLine_ID = "M_MovementLine_ID";

	/** Set Move Line.
	  * Inventory Move document Line
	  */
	public void setM_MovementLine_ID (int M_MovementLine_ID);

	/** Get Move Line.
	  * Inventory Move document Line
	  */
	public int getM_MovementLine_ID();

	public org.compiere.model.I_M_MovementLine getM_MovementLine() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name NettoI */
    public static final String COLUMNNAME_NettoI = "NettoI";

	/** Set Netto-I.
	  * The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI);

	/** Get Netto-I.
	  * The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI();

    /** Column name NettoII */
    public static final String COLUMNNAME_NettoII = "NettoII";

	/** Set Netto-II.
	  * The nett weight of the second group calculation
	  */
	public void setNettoII (BigDecimal NettoII);

	/** Get Netto-II.
	  * The nett weight of the second group calculation
	  */
	public BigDecimal getNettoII();

    /** Column name OriginalNettoI */
    public static final String COLUMNNAME_OriginalNettoI = "OriginalNettoI";

	/** Set Original Netto-I.
	  * The nett weight of the first group calculation
	  */
	public void setOriginalNettoI (BigDecimal OriginalNettoI);

	/** Get Original Netto-I.
	  * The nett weight of the first group calculation
	  */
	public BigDecimal getOriginalNettoI();

    /** Column name OriginalNettoII */
    public static final String COLUMNNAME_OriginalNettoII = "OriginalNettoII";

	/** Set Original Netto-II.
	  * The nett weight of the second group calculation
	  */
	public void setOriginalNettoII (BigDecimal OriginalNettoII);

	/** Get Original Netto-II.
	  * The nett weight of the second group calculation
	  */
	public BigDecimal getOriginalNettoII();

    /** Column name OriginalReflection */
    public static final String COLUMNNAME_OriginalReflection = "OriginalReflection";

	/** Set Original Reflection.
	  * The deduction to the gross weight
	  */
	public void setOriginalReflection (BigDecimal OriginalReflection);

	/** Get Original Reflection.
	  * The deduction to the gross weight
	  */
	public BigDecimal getOriginalReflection();

    /** Column name PrintGradingSheet */
    public static final String COLUMNNAME_PrintGradingSheet = "PrintGradingSheet";

	/** Set Print Grading Sheet.
	  * Print the Grading Sheet of a ticket.
	  */
	public void setPrintGradingSheet (String PrintGradingSheet);

	/** Get Print Grading Sheet.
	  * Print the Grading Sheet of a ticket.
	  */
	public String getPrintGradingSheet();

    /** Column name PrintJoinTicketAndDespatch */
    public static final String COLUMNNAME_PrintJoinTicketAndDespatch = "PrintJoinTicketAndDespatch";

	/** Set Print Ticket And Despatch	  */
	public void setPrintJoinTicketAndDespatch (String PrintJoinTicketAndDespatch);

	/** Get Print Ticket And Despatch	  */
	public String getPrintJoinTicketAndDespatch();

    /** Column name PrintJoinTicketAndGrading */
    public static final String COLUMNNAME_PrintJoinTicketAndGrading = "PrintJoinTicketAndGrading";

	/** Set Print Ticket And Grading Sheet	  */
	public void setPrintJoinTicketAndGrading (String PrintJoinTicketAndGrading);

	/** Get Print Ticket And Grading Sheet	  */
	public String getPrintJoinTicketAndGrading();

    /** Column name PrintTicket */
    public static final String COLUMNNAME_PrintTicket = "PrintTicket";

	/** Set Print Ticket.
	  * Print weighbridge ticket
	  */
	public void setPrintTicket (String PrintTicket);

	/** Get Print Ticket.
	  * Print weighbridge ticket
	  */
	public String getPrintTicket();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name QtyPayment */
    public static final String COLUMNNAME_QtyPayment = "QtyPayment";

	/** Set Qty Payment	  */
	public void setQtyPayment (BigDecimal QtyPayment);

	/** Get Qty Payment	  */
	public BigDecimal getQtyPayment();

    /** Column name ReferenceNo */
    public static final String COLUMNNAME_ReferenceNo = "ReferenceNo";

	/** Set Reference No.
	  * Your customer or vendor number at the Business Partner's site
	  */
	public void setReferenceNo (String ReferenceNo);

	/** Get Reference No.
	  * Your customer or vendor number at the Business Partner's site
	  */
	public String getReferenceNo();

    /** Column name Reflection */
    public static final String COLUMNNAME_Reflection = "Reflection";

	/** Set Reflection.
	  * The deduction to the gross weight
	  */
	public void setReflection (BigDecimal Reflection);

	/** Get Reflection.
	  * The deduction to the gross weight
	  */
	public BigDecimal getReflection();

    /** Column name ReportDespatch */
    public static final String COLUMNNAME_ReportDespatch = "ReportDespatch";

	/** Set Print Despatch Note	  */
	public void setReportDespatch (String ReportDespatch);

	/** Get Print Despatch Note	  */
	public String getReportDespatch();

    /** Column name RevisionPercent */
    public static final String COLUMNNAME_RevisionPercent = "RevisionPercent";

	/** Set Revision %	  */
	public void setRevisionPercent (BigDecimal RevisionPercent);

	/** Get Revision %	  */
	public BigDecimal getRevisionPercent();

    /** Column name RevisionQty */
    public static final String COLUMNNAME_RevisionQty = "RevisionQty";

	/** Set Revision Qty	  */
	public void setRevisionQty (BigDecimal RevisionQty);

	/** Get Revision Qty	  */
	public BigDecimal getRevisionQty();

    /** Column name ShippedBy */
    public static final String COLUMNNAME_ShippedBy = "ShippedBy";

	/** Set Shipped By	  */
	public void setShippedBy (String ShippedBy);

	/** Get Shipped By	  */
	public String getShippedBy();

    /** Column name SplittedOrder_ID */
    public static final String COLUMNNAME_SplittedOrder_ID = "SplittedOrder_ID";

	/** Set Splitted Order.
	  * The other order (ticket) by which this ticket is splitted to
	  */
	public void setSplittedOrder_ID (int SplittedOrder_ID);

	/** Get Splitted Order.
	  * The other order (ticket) by which this ticket is splitted to
	  */
	public int getSplittedOrder_ID();

	public org.compiere.model.I_C_Order getSplittedOrder() throws RuntimeException;

    /** Column name SplittedTicket_ID */
    public static final String COLUMNNAME_SplittedTicket_ID = "SplittedTicket_ID";

	/** Set Splitted Ticket.
	  * The ticket by which this ticket is splitted to
	  */
	public void setSplittedTicket_ID (int SplittedTicket_ID);

	/** Get Splitted Ticket.
	  * The ticket by which this ticket is splitted to
	  */
	public int getSplittedTicket_ID();

	public com.unicore.model.I_UNS_WeighbridgeTicket getSplittedTicket() throws RuntimeException;

    /** Column name SyncToDestinationWhsConfirm */
    public static final String COLUMNNAME_SyncToDestinationWhsConfirm = "SyncToDestinationWhsConfirm";

	/** Set Sync To Destination Warehouse Confirm	  */
	public void setSyncToDestinationWhsConfirm (String SyncToDestinationWhsConfirm);

	/** Get Sync To Destination Warehouse Confirm	  */
	public String getSyncToDestinationWhsConfirm();

    /** Column name Tare */
    public static final String COLUMNNAME_Tare = "Tare";

	/** Set Tare.
	  * The weight as comparison
	  */
	public void setTare (BigDecimal Tare);

	/** Get Tare.
	  * The weight as comparison
	  */
	public BigDecimal getTare();

    /** Column name TimeIn */
    public static final String COLUMNNAME_TimeIn = "TimeIn";

	/** Set Time In.
	  * The date and time of when it was in
	  */
	public void setTimeIn (Timestamp TimeIn);

	/** Get Time In.
	  * The date and time of when it was in
	  */
	public Timestamp getTimeIn();

    /** Column name TimeInNumber */
    public static final String COLUMNNAME_TimeInNumber = "TimeInNumber";

	/** Set Time In Number.
	  * Must 4 Number for parsing to date Time In
	  */
	public void setTimeInNumber (String TimeInNumber);

	/** Get Time In Number.
	  * Must 4 Number for parsing to date Time In
	  */
	public String getTimeInNumber();

    /** Column name TimeOut */
    public static final String COLUMNNAME_TimeOut = "TimeOut";

	/** Set Time Out.
	  * The date and time of when it was out
	  */
	public void setTimeOut (Timestamp TimeOut);

	/** Get Time Out.
	  * The date and time of when it was out
	  */
	public Timestamp getTimeOut();

    /** Column name TimeOutNumber */
    public static final String COLUMNNAME_TimeOutNumber = "TimeOutNumber";

	/** Set Time Out Number.
	  * Must 4 Number for parsing to date Time Out
	  */
	public void setTimeOutNumber (String TimeOutNumber);

	/** Get Time Out Number.
	  * Must 4 Number for parsing to date Time Out
	  */
	public String getTimeOutNumber();

    /** Column name Transporter_ID */
    public static final String COLUMNNAME_Transporter_ID = "Transporter_ID";

	/** Set Transporter.
	  * The transporter
	  */
	public void setTransporter_ID (int Transporter_ID);

	/** Get Transporter.
	  * The transporter
	  */
	public int getTransporter_ID();

	public org.compiere.model.I_C_BPartner getTransporter() throws RuntimeException;

    /** Column name TransporterName */
    public static final String COLUMNNAME_TransporterName = "TransporterName";

	/** Set Transporter Name.
	  * The name of transporter
	  */
	public void setTransporterName (String TransporterName);

	/** Get Transporter Name.
	  * The name of transporter
	  */
	public String getTransporterName();

    /** Column name UnLoadInvoiceLine_ID */
    public static final String COLUMNNAME_UnLoadInvoiceLine_ID = "UnLoadInvoiceLine_ID";

	/** Set UnLoading Invoice Line	  */
	public void setUnLoadInvoiceLine_ID (int UnLoadInvoiceLine_ID);

	/** Get UnLoading Invoice Line	  */
	public int getUnLoadInvoiceLine_ID();

	public org.compiere.model.I_C_InvoiceLine getUnLoadInvoiceLine() throws RuntimeException;

    /** Column name UNS_ArmadaType_ID */
    public static final String COLUMNNAME_UNS_ArmadaType_ID = "UNS_ArmadaType_ID";

	/** Set Armada Type	  */
	public void setUNS_ArmadaType_ID (int UNS_ArmadaType_ID);

	/** Get Armada Type	  */
	public int getUNS_ArmadaType_ID();

    /** Column name UNS_BPartner_Driver_ID */
    public static final String COLUMNNAME_UNS_BPartner_Driver_ID = "UNS_BPartner_Driver_ID";

	/** Set UNS_BPartner_Driver	  */
	public void setUNS_BPartner_Driver_ID (int UNS_BPartner_Driver_ID);

	/** Get UNS_BPartner_Driver	  */
	public int getUNS_BPartner_Driver_ID();

	public com.unicore.model.I_UNS_BPartner_Driver getUNS_BPartner_Driver() throws RuntimeException;

    /** Column name UNS_Handover_Ticket_ID */
    public static final String COLUMNNAME_UNS_Handover_Ticket_ID = "UNS_Handover_Ticket_ID";

	/** Set Handover Ticket	  */
	public void setUNS_Handover_Ticket_ID (int UNS_Handover_Ticket_ID);

	/** Get Handover Ticket	  */
	public int getUNS_Handover_Ticket_ID();

	public com.unicore.model.I_UNS_Handover_Ticket getUNS_Handover_Ticket() throws RuntimeException;

    /** Column name UNS_ReflectionRevision_ID */
    public static final String COLUMNNAME_UNS_ReflectionRevision_ID = "UNS_ReflectionRevision_ID";

	/** Set Reflection Revision	  */
	public void setUNS_ReflectionRevision_ID (int UNS_ReflectionRevision_ID);

	/** Get Reflection Revision	  */
	public int getUNS_ReflectionRevision_ID();

	public com.unicore.model.I_UNS_ReflectionRevision getUNS_ReflectionRevision() throws RuntimeException;

    /** Column name UNS_ReflectionRevision_Line_ID */
    public static final String COLUMNNAME_UNS_ReflectionRevision_Line_ID = "UNS_ReflectionRevision_Line_ID";

	/** Set Revision Line	  */
	public void setUNS_ReflectionRevision_Line_ID (int UNS_ReflectionRevision_Line_ID);

	/** Get Revision Line	  */
	public int getUNS_ReflectionRevision_Line_ID();

	public com.unicore.model.I_UNS_ReflectionRevision_Line getUNS_ReflectionRevision_Line() throws RuntimeException;

    /** Column name UNS_UnLoadingCost_ID */
    public static final String COLUMNNAME_UNS_UnLoadingCost_ID = "UNS_UnLoadingCost_ID";

	/** Set Unloading Cost	  */
	public void setUNS_UnLoadingCost_ID (int UNS_UnLoadingCost_ID);

	/** Get Unloading Cost	  */
	public int getUNS_UnLoadingCost_ID();

	public com.unicore.model.I_UNS_UnLoadingCost getUNS_UnLoadingCost() throws RuntimeException;

    /** Column name UNS_UnLoadingCost_Line_ID */
    public static final String COLUMNNAME_UNS_UnLoadingCost_Line_ID = "UNS_UnLoadingCost_Line_ID";

	/** Set UnLoading Cost Line	  */
	public void setUNS_UnLoadingCost_Line_ID (int UNS_UnLoadingCost_Line_ID);

	/** Get UnLoading Cost Line	  */
	public int getUNS_UnLoadingCost_Line_ID();

	public com.unicore.model.I_UNS_UnLoadingCost_Line getUNS_UnLoadingCost_Line() throws RuntimeException;

    /** Column name UNS_WeighbridgeTicket_ID */
    public static final String COLUMNNAME_UNS_WeighbridgeTicket_ID = "UNS_WeighbridgeTicket_ID";

	/** Set Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID);

	/** Get Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID();

    /** Column name UNS_WeighbridgeTicket_UU */
    public static final String COLUMNNAME_UNS_WeighbridgeTicket_UU = "UNS_WeighbridgeTicket_UU";

	/** Set UNS_WeighbridgeTicket_UU	  */
	public void setUNS_WeighbridgeTicket_UU (String UNS_WeighbridgeTicket_UU);

	/** Get UNS_WeighbridgeTicket_UU	  */
	public String getUNS_WeighbridgeTicket_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VehicleDriverName */
    public static final String COLUMNNAME_VehicleDriverName = "VehicleDriverName";

	/** Set Vehicle Driver Name.
	  * The person name of vehicle by whom it is drived
	  */
	public void setVehicleDriverName (String VehicleDriverName);

	/** Get Vehicle Driver Name.
	  * The person name of vehicle by whom it is drived
	  */
	public String getVehicleDriverName();

    /** Column name VehicleNo */
    public static final String COLUMNNAME_VehicleNo = "VehicleNo";

	/** Set Vehicle No.
	  * The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo);

	/** Get Vehicle No.
	  * The identification number of the vehicle being weighted
	  */
	public String getVehicleNo();

    /** Column name VehicleType */
    public static final String COLUMNNAME_VehicleType = "VehicleType";

	/** Set Vehicle Type.
	  * The type of vehicle
	  */
	public void setVehicleType (String VehicleType);

	/** Get Vehicle Type.
	  * The type of vehicle
	  */
	public String getVehicleType();
}
