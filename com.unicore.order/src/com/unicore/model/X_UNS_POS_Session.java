/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_POS_Session
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_POS_Session extends PO implements I_UNS_POS_Session, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190130L;

    /** Standard Constructor */
    public X_UNS_POS_Session (Properties ctx, int UNS_POS_Session_ID, String trxName)
    {
      super (ctx, UNS_POS_Session_ID, trxName);
      /** if (UNS_POS_Session_ID == 0)
        {
			setCashier_ID (0);
// @#AD_User_ID@
			setDateAcct (new Timestamp( System.currentTimeMillis() ));
// @DateDoc@
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setEDCSalesAmt (Env.ZERO);
// 0
			setIsApproved (false);
// N
			setIsReconciled (false);
// N
			setIsTrialMode (false);
// N
			setPayableCashRefund (Env.ZERO);
// 0
			setPayableEDCRefund (Env.ZERO);
// 0
			setProcessed (false);
			setReturnAmt (Env.ZERO);
// 0
			setTotalDiscAmt (Env.ZERO);
// 0
			setTotalEstimationShortCashierAmt (Env.ZERO);
// 0
			setTotalPayDiscAmt (Env.ZERO);
// 0
			setTotalRefundTaxAmt (Env.ZERO);
// 0
			setTotalSalesB1 (Env.ZERO);
// 0
			setTotalSalesB2 (Env.ZERO);
// 0
			setTotalServiceCharge (Env.ZERO);
// 0
			setTotalShortCashierAmt (Env.ZERO);
// 0
			setTotalTaxAmt (Env.ZERO);
// 0
			setUNS_POS_Session_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_POS_Session (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_POS_Session[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getBase1Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getBase1Currency_ID(), get_TrxName());	}

	/** Set Base 1 Currency.
		@param Base1Currency_ID Base 1 Currency	  */
	public void setBase1Currency_ID (int Base1Currency_ID)
	{
		if (Base1Currency_ID < 1) 
			set_Value (COLUMNNAME_Base1Currency_ID, null);
		else 
			set_Value (COLUMNNAME_Base1Currency_ID, Integer.valueOf(Base1Currency_ID));
	}

	/** Get Base 1 Currency.
		@return Base 1 Currency	  */
	public int getBase1Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Base1Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getBase2Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getBase2Currency_ID(), get_TrxName());	}

	/** Set Base 2 Currency.
		@param Base2Currency_ID Base 2 Currency	  */
	public void setBase2Currency_ID (int Base2Currency_ID)
	{
		if (Base2Currency_ID < 1) 
			set_Value (COLUMNNAME_Base2Currency_ID, null);
		else 
			set_Value (COLUMNNAME_Base2Currency_ID, Integer.valueOf(Base2Currency_ID));
	}

	/** Get Base 2 Currency.
		@return Base 2 Currency	  */
	public int getBase2Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Base2Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getCashier_ID(), get_TrxName());	}

	/** Set Cashier.
		@param Cashier_ID Cashier	  */
	public void setCashier_ID (int Cashier_ID)
	{
		if (Cashier_ID < 1) 
			set_Value (COLUMNNAME_Cashier_ID, null);
		else 
			set_Value (COLUMNNAME_Cashier_ID, Integer.valueOf(Cashier_ID));
	}

	/** Get Cashier.
		@return Cashier	  */
	public int getCashier_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Cashier_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cash Sales Amt.
		@param CashSalesAmt 
		The amount from selling using direct cash
	  */
	public void setCashSalesAmt (BigDecimal CashSalesAmt)
	{
		set_Value (COLUMNNAME_CashSalesAmt, CashSalesAmt);
	}

	/** Get Cash Sales Amt.
		@return The amount from selling using direct cash
	  */
	public BigDecimal getCashSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashSalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Account Date.
		@param DateAcct 
		Accounting Date
	  */
	public void setDateAcct (Timestamp DateAcct)
	{
		set_Value (COLUMNNAME_DateAcct, DateAcct);
	}

	/** Get Account Date.
		@return Accounting Date
	  */
	public Timestamp getDateAcct () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateAcct);
	}

	/** Set Date Closed.
		@param DateClosed Date Closed	  */
	public void setDateClosed (Timestamp DateClosed)
	{
		set_Value (COLUMNNAME_DateClosed, DateClosed);
	}

	/** Get Date Closed.
		@return Date Closed	  */
	public Timestamp getDateClosed () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateClosed);
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Date Opened.
		@param DateOpened Date Opened	  */
	public void setDateOpened (Timestamp DateOpened)
	{
		set_Value (COLUMNNAME_DateOpened, DateOpened);
	}

	/** Get Date Opened.
		@return Date Opened	  */
	public Timestamp getDateOpened () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateOpened);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set EDC Sales Amount.
		@param EDCSalesAmt EDC Sales Amount	  */
	public void setEDCSalesAmt (BigDecimal EDCSalesAmt)
	{
		set_Value (COLUMNNAME_EDCSalesAmt, EDCSalesAmt);
	}

	/** Get EDC Sales Amount.
		@return EDC Sales Amount	  */
	public BigDecimal getEDCSalesAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EDCSalesAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Export.
		@param ExportRecord Export	  */
	public void setExportRecord (String ExportRecord)
	{
		set_Value (COLUMNNAME_ExportRecord, ExportRecord);
	}

	/** Get Export.
		@return Export	  */
	public String getExportRecord () 
	{
		return (String)get_Value(COLUMNNAME_ExportRecord);
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Reconciled.
		@param IsReconciled 
		Payment is reconciled with bank statement
	  */
	public void setIsReconciled (boolean IsReconciled)
	{
		set_Value (COLUMNNAME_IsReconciled, Boolean.valueOf(IsReconciled));
	}

	/** Get Reconciled.
		@return Payment is reconciled with bank statement
	  */
	public boolean isReconciled () 
	{
		Object oo = get_Value(COLUMNNAME_IsReconciled);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Trial Mode?.
		@param IsTrialMode Trial Mode?	  */
	public void setIsTrialMode (boolean IsTrialMode)
	{
		set_Value (COLUMNNAME_IsTrialMode, Boolean.valueOf(IsTrialMode));
	}

	/** Get Trial Mode?.
		@return Trial Mode?	  */
	public boolean isTrialMode () 
	{
		Object oo = get_Value(COLUMNNAME_IsTrialMode);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_PriceList getM_PriceList() throws RuntimeException
    {
		return (org.compiere.model.I_M_PriceList)MTable.get(getCtx(), org.compiere.model.I_M_PriceList.Table_Name)
			.getPO(getM_PriceList_ID(), get_TrxName());	}

	/** Set Price List.
		@param M_PriceList_ID 
		Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID)
	{
		if (M_PriceList_ID < 1) 
			set_Value (COLUMNNAME_M_PriceList_ID, null);
		else 
			set_Value (COLUMNNAME_M_PriceList_ID, Integer.valueOf(M_PriceList_ID));
	}

	/** Get Price List.
		@return Unique identifier of a Price List
	  */
	public int getM_PriceList_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_PriceList_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Payable Cash Refund.
		@param PayableCashRefund 
		The cash to be refunded to customer by transfer to customer's bank account.
	  */
	public void setPayableCashRefund (BigDecimal PayableCashRefund)
	{
		set_Value (COLUMNNAME_PayableCashRefund, PayableCashRefund);
	}

	/** Get Payable Cash Refund.
		@return The cash to be refunded to customer by transfer to customer's bank account.
	  */
	public BigDecimal getPayableCashRefund () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableCashRefund);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payable EDC Refund.
		@param PayableEDCRefund 
		The EDC transaction amount to be refunded to customer by transfer to customer's bank account.
	  */
	public void setPayableEDCRefund (BigDecimal PayableEDCRefund)
	{
		set_Value (COLUMNNAME_PayableEDCRefund, PayableEDCRefund);
	}

	/** Get Payable EDC Refund.
		@return The EDC transaction amount to be refunded to customer by transfer to customer's bank account.
	  */
	public BigDecimal getPayableEDCRefund () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableEDCRefund);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Print Reading.
		@param PrintReading Print Reading	  */
	public void setPrintReading (String PrintReading)
	{
		set_Value (COLUMNNAME_PrintReading, PrintReading);
	}

	/** Get Print Reading.
		@return Print Reading	  */
	public String getPrintReading () 
	{
		return (String)get_Value(COLUMNNAME_PrintReading);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Report Currency Rate.
		@param RepCurrRate Report Currency Rate	  */
	public void setRepCurrRate (String RepCurrRate)
	{
		set_Value (COLUMNNAME_RepCurrRate, RepCurrRate);
	}

	/** Get Report Currency Rate.
		@return Report Currency Rate	  */
	public String getRepCurrRate () 
	{
		return (String)get_Value(COLUMNNAME_RepCurrRate);
	}

	/** Set Return Amount.
		@param ReturnAmt Return Amount	  */
	public void setReturnAmt (BigDecimal ReturnAmt)
	{
		set_Value (COLUMNNAME_ReturnAmt, ReturnAmt);
	}

	/** Get Return Amount.
		@return Return Amount	  */
	public BigDecimal getReturnAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ReturnAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Rounding Amt.
		@param RoundingAmt Rounding Amt	  */
	public void setRoundingAmt (BigDecimal RoundingAmt)
	{
		set_Value (COLUMNNAME_RoundingAmt, RoundingAmt);
	}

	/** Get Rounding Amt.
		@return Rounding Amt	  */
	public BigDecimal getRoundingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RoundingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Shift 1 = 01 */
	public static final String SHIFT_Shift1 = "01";
	/** Shift 2 = 02 */
	public static final String SHIFT_Shift2 = "02";
	/** Shift 3 = 03 */
	public static final String SHIFT_Shift3 = "03";
	/** Shift 4 = 04 */
	public static final String SHIFT_Shift4 = "04";
	/** Shift 5 = 05 */
	public static final String SHIFT_Shift5 = "05";
	/** Shift 6 = 06 */
	public static final String SHIFT_Shift6 = "06";
	/** Shift 7 = 07 */
	public static final String SHIFT_Shift7 = "07";
	/** Shift 8 = 08 */
	public static final String SHIFT_Shift8 = "08";
	/** Shift 9 = 09 */
	public static final String SHIFT_Shift9 = "09";
	/** Shift 10 = 10 */
	public static final String SHIFT_Shift10 = "10";
	/** Set Shift.
		@param Shift Shift	  */
	public void setShift (String Shift)
	{

		set_Value (COLUMNNAME_Shift, Shift);
	}

	/** Get Shift.
		@return Shift	  */
	public String getShift () 
	{
		return (String)get_Value(COLUMNNAME_Shift);
	}

	public org.compiere.model.I_AD_User getSupervisor() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getSupervisor_ID(), get_TrxName());	}

	/** Set Supervisor.
		@param Supervisor_ID 
		Supervisor for this user/organization - used for escalation and approval
	  */
	public void setSupervisor_ID (int Supervisor_ID)
	{
		if (Supervisor_ID < 1) 
			set_Value (COLUMNNAME_Supervisor_ID, null);
		else 
			set_Value (COLUMNNAME_Supervisor_ID, Integer.valueOf(Supervisor_ID));
	}

	/** Get Supervisor.
		@return Supervisor for this user/organization - used for escalation and approval
	  */
	public int getSupervisor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Supervisor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Process Take Over.
		@param TakeOver Process Take Over	  */
	public void setTakeOver (String TakeOver)
	{
		set_Value (COLUMNNAME_TakeOver, TakeOver);
	}

	/** Get Process Take Over.
		@return Process Take Over	  */
	public String getTakeOver () 
	{
		return (String)get_Value(COLUMNNAME_TakeOver);
	}

	/** Set Total Discount Amount.
		@param TotalDiscAmt Total Discount Amount	  */
	public void setTotalDiscAmt (BigDecimal TotalDiscAmt)
	{
		set_Value (COLUMNNAME_TotalDiscAmt, TotalDiscAmt);
	}

	/** Get Total Discount Amount.
		@return Total Discount Amount	  */
	public BigDecimal getTotalDiscAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalDiscAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Estimation Short Cashier Amount.
		@param TotalEstimationShortCashierAmt Total Estimation Short Cashier Amount	  */
	public void setTotalEstimationShortCashierAmt (BigDecimal TotalEstimationShortCashierAmt)
	{
		set_Value (COLUMNNAME_TotalEstimationShortCashierAmt, TotalEstimationShortCashierAmt);
	}

	/** Get Total Estimation Short Cashier Amount.
		@return Total Estimation Short Cashier Amount	  */
	public BigDecimal getTotalEstimationShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalEstimationShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Pay Disc Amount.
		@param TotalPayDiscAmt Total Pay Disc Amount	  */
	public void setTotalPayDiscAmt (BigDecimal TotalPayDiscAmt)
	{
		set_Value (COLUMNNAME_TotalPayDiscAmt, TotalPayDiscAmt);
	}

	/** Get Total Pay Disc Amount.
		@return Total Pay Disc Amount	  */
	public BigDecimal getTotalPayDiscAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalPayDiscAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Refund Service Charge.
		@param TotalRefundSvcChgAmt Total Refund Service Charge	  */
	public void setTotalRefundSvcChgAmt (BigDecimal TotalRefundSvcChgAmt)
	{
		set_Value (COLUMNNAME_TotalRefundSvcChgAmt, TotalRefundSvcChgAmt);
	}

	/** Get Total Refund Service Charge.
		@return Total Refund Service Charge	  */
	public BigDecimal getTotalRefundSvcChgAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalRefundSvcChgAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Refund Tax Amount.
		@param TotalRefundTaxAmt Total Refund Tax Amount	  */
	public void setTotalRefundTaxAmt (BigDecimal TotalRefundTaxAmt)
	{
		set_Value (COLUMNNAME_TotalRefundTaxAmt, TotalRefundTaxAmt);
	}

	/** Get Total Refund Tax Amount.
		@return Total Refund Tax Amount	  */
	public BigDecimal getTotalRefundTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalRefundTaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Sales.
		@param TotalSalesB1 Total Sales	  */
	public void setTotalSalesB1 (BigDecimal TotalSalesB1)
	{
		set_Value (COLUMNNAME_TotalSalesB1, TotalSalesB1);
	}

	/** Get Total Sales.
		@return Total Sales	  */
	public BigDecimal getTotalSalesB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalSalesB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Sales Base 2.
		@param TotalSalesB2 Total Sales Base 2	  */
	public void setTotalSalesB2 (BigDecimal TotalSalesB2)
	{
		set_Value (COLUMNNAME_TotalSalesB2, TotalSalesB2);
	}

	/** Get Total Sales Base 2.
		@return Total Sales Base 2	  */
	public BigDecimal getTotalSalesB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalSalesB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Service Charge.
		@param TotalServiceCharge Total Service Charge	  */
	public void setTotalServiceCharge (BigDecimal TotalServiceCharge)
	{
		set_Value (COLUMNNAME_TotalServiceCharge, TotalServiceCharge);
	}

	/** Get Total Service Charge.
		@return Total Service Charge	  */
	public BigDecimal getTotalServiceCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalServiceCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Short Cashier Amount (+).
		@param TotalShortCashierAmt Total Short Cashier Amount (+)	  */
	public void setTotalShortCashierAmt (BigDecimal TotalShortCashierAmt)
	{
		set_Value (COLUMNNAME_TotalShortCashierAmt, TotalShortCashierAmt);
	}

	/** Get Total Short Cashier Amount (+).
		@return Total Short Cashier Amount (+)	  */
	public BigDecimal getTotalShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Tax Amount.
		@param TotalTaxAmt Total Tax Amount	  */
	public void setTotalTaxAmt (BigDecimal TotalTaxAmt)
	{
		set_Value (COLUMNNAME_TotalTaxAmt, TotalTaxAmt);
	}

	/** Get Total Tax Amount.
		@return Total Tax Amount	  */
	public BigDecimal getTotalTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalTaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_POS_Session_UU.
		@param UNS_POS_Session_UU UNS_POS_Session_UU	  */
	public void setUNS_POS_Session_UU (String UNS_POS_Session_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_UU, UNS_POS_Session_UU);
	}

	/** Get UNS_POS_Session_UU.
		@return UNS_POS_Session_UU	  */
	public String getUNS_POS_Session_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_POS_Session_UU);
	}

	public com.unicore.model.I_UNS_POSTerminal getUNS_POSTerminal() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTerminal)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTerminal.Table_Name)
			.getPO(getUNS_POSTerminal_ID(), get_TrxName());	}

	/** Set POS Terminal.
		@param UNS_POSTerminal_ID POS Terminal	  */
	public void setUNS_POSTerminal_ID (int UNS_POSTerminal_ID)
	{
		if (UNS_POSTerminal_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminal_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminal_ID, Integer.valueOf(UNS_POSTerminal_ID));
	}

	/** Get POS Terminal.
		@return POS Terminal	  */
	public int getUNS_POSTerminal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTerminal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Voucher Amount.
		@param VoucherAmt Voucher Amount	  */
	public void setVoucherAmt (BigDecimal VoucherAmt)
	{
		set_Value (COLUMNNAME_VoucherAmt, VoucherAmt);
	}

	/** Get Voucher Amount.
		@return Voucher Amount	  */
	public BigDecimal getVoucherAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VoucherAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}