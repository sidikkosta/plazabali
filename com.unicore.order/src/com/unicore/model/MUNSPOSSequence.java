/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Properties;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author MUHAMIN
 *
 */
public class MUNSPOSSequence extends X_UNS_POS_Sequence 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4009848646986747343L;

	/**
	 * @param ctx
	 * @param UNS_POS_Sequence_ID
	 * @param trxName
	 */
	public MUNSPOSSequence(Properties ctx, int UNS_POS_Sequence_ID,
			String trxName) 
	{
		super(ctx, UNS_POS_Sequence_ID, trxName);
	}
	
	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSSequence(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}
	
	public String getTrxSequence()
	{
		StringBuilder trxBuild = new StringBuilder();
		if (null != getPrefix())
		{
			trxBuild.append(getPrefix());
		}
		
		int max = getLength();
		String next = Integer.toString(getNext());
		
		while (next.length() < max)
		{
			next = "0" + next;
		}
		
		trxBuild.append(next);
		
		if (getSufix() != null)
		{
			trxBuild.append(getSufix());
		}
		
		String trxno = trxBuild.toString();
		
		setCurrent(getNext());
		setNext(getCurrent() + 1);
		setTrx(trxno);
		return getTrx();
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		if (newRecord)
		{
			setNext(getStartNo());
		}

		return super.beforeSave(newRecord);
	}
	
	public static String getNextSequence (String trxName, int AD_Org_ID, int docTypeID, Timestamp date)
	{
		MUNSPOSSequence sequence = MUNSPOSSequence.get(trxName, AD_Org_ID, docTypeID);
		if (null == sequence)
		{
			return "UndefinedDocSequence";
		}
		
		String resetType = sequence.getPeriodReset();
		SimpleDateFormat format;
		String unique;
		String trxNo;
		if (PERIODRESET_Daily.equals(resetType))
		{
			format = new SimpleDateFormat("dd-MM-yyyy");
			unique = format.format(date);
			trxNo = MUNSPOSSequencePeriodic.getNextSequence(sequence, unique);
		}
		else if (PERIODRESET_Monthly.equals(resetType))
		{
			format = new SimpleDateFormat("MM-yyyy");
			unique = format.format(date);
			trxNo = MUNSPOSSequencePeriodic.getNextSequence(sequence, unique);
		}
		else if (PERIODRESET_Yearly.equals(resetType))
		{
			format = new SimpleDateFormat("yyyy");
			unique = format.format(date);
			trxNo = MUNSPOSSequencePeriodic.getNextSequence(sequence, unique);
		}
		else
		{
			DB.getDatabase().forUpdate(sequence, 0);
			trxNo = sequence.getTrxSequence();
			sequence.saveEx();
		}
		
		return trxNo;
	}
	
	public String getPeriodicSequence (String key)
	{
		return MUNSPOSSequencePeriodic.getNextSequence(this, key);
	}
	
	public static MUNSPOSSequence get(
			String trxName, int AD_Org_ID, int docTypeID)
	{	
		return Query.get(Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID , 
				Table_Name , "AD_Org_ID IN (?, ?) AND (C_DocType_ID IS NULL OR C_DocType_ID = ?)", trxName).
				setParameters(0, AD_Org_ID, docTypeID).setOrderBy("AD_Org_ID Desc, C_DocType_ID Desc").first();		
	}
		
}