/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSPOSFNBTable extends X_UNS_POSFNBTable {

	private MUNSPOSFNBTableLine[] m_lines = null;
	private static final long serialVersionUID = -4739619421351054719L;

	public MUNSPOSFNBTableLine[] getLines (boolean requery)
	{
		if (m_lines != null & !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSPOSFNBTableLine> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSPOSFNBTableLine.Table_Name, Table_Name +"_ID = ?", 
				get_TrxName()).
				setParameters(get_ID()).setOnlyActiveRecords(true).
				setOrderBy("value").
				list();
		m_lines = new MUNSPOSFNBTableLine[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}
	
	public MUNSPOSFNBTableLine[] getLines ()
	{
		return getLines(false);
	}
	
	public static MUNSPOSFNBTable get (String trxName, int storeID)
	{
		return Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				"UNS_POSFNBTable", "Store_ID = ?", trxName).
				setParameters(storeID).setOnlyActiveRecords(true).
				first();
	}
	
	/**
	 * @param ctx
	 * @param UNS_POSFNBTable_ID
	 * @param trxName
	 */
	public MUNSPOSFNBTable(Properties ctx, int UNS_POSFNBTable_ID,
			String trxName) 
	{
		super(ctx, UNS_POSFNBTable_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSFNBTable(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	
	public boolean generateTable (String prefix, int start, int end)
	{
		for (int i=start; i<=end; i++)
		{
			int length = Integer.toString(end).length();
			String format = "%" + length + "d";
			String value = String.format(format, i).replace(" ", "0");
			String name = prefix + " " + value;
			MUNSPOSFNBTableLine line = new MUNSPOSFNBTableLine(this);
			line.setValue(value);
			line.setName(name);
			line.setX(value);
			line.setY(name);
			line.setZ(name);
			if (!line.save())
				return false;
		}
		return true;
	}
}
