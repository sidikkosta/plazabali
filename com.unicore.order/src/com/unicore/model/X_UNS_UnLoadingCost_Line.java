/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_UnLoadingCost_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_UnLoadingCost_Line extends PO implements I_UNS_UnLoadingCost_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170804L;

    /** Standard Constructor */
    public X_UNS_UnLoadingCost_Line (Properties ctx, int UNS_UnLoadingCost_Line_ID, String trxName)
    {
      super (ctx, UNS_UnLoadingCost_Line_ID, trxName);
      /** if (UNS_UnLoadingCost_Line_ID == 0)
        {
			setAmount (Env.ZERO);
// 0
			setC_UOM_ID (0);
			setIsDumpTruck (false);
// N
			setUNS_UnLoadingCost_ID (0);
			setUNS_UnLoadingCost_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UnLoadingCost_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UnLoadingCost_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Base Amount.
		@param BaseAmount Base Amount	  */
	public void setBaseAmount (BigDecimal BaseAmount)
	{
		set_ValueNoCheck (COLUMNNAME_BaseAmount, BaseAmount);
	}

	/** Get Base Amount.
		@return Base Amount	  */
	public BigDecimal getBaseAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BaseAmount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Is Dump Truck.
		@param IsDumpTruck Is Dump Truck	  */
	public void setIsDumpTruck (boolean IsDumpTruck)
	{
		set_Value (COLUMNNAME_IsDumpTruck, Boolean.valueOf(IsDumpTruck));
	}

	/** Get Is Dump Truck.
		@return Is Dump Truck	  */
	public boolean isDumpTruck () 
	{
		Object oo = get_Value(COLUMNNAME_IsDumpTruck);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_UnLoadingConfig_Line getUNS_UnLoadingConfig_Line() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_UnLoadingConfig_Line)MTable.get(getCtx(), com.unicore.model.I_UNS_UnLoadingConfig_Line.Table_Name)
			.getPO(getUNS_UnLoadingConfig_Line_ID(), get_TrxName());	}

	/** Set UnLoading Configurator Line.
		@param UNS_UnLoadingConfig_Line_ID UnLoading Configurator Line	  */
	public void setUNS_UnLoadingConfig_Line_ID (int UNS_UnLoadingConfig_Line_ID)
	{
		if (UNS_UnLoadingConfig_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_Line_ID, Integer.valueOf(UNS_UnLoadingConfig_Line_ID));
	}

	/** Get UnLoading Configurator Line.
		@return UnLoading Configurator Line	  */
	public int getUNS_UnLoadingConfig_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingConfig_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_UnLoadingCost getUNS_UnLoadingCost() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_UnLoadingCost)MTable.get(getCtx(), com.unicore.model.I_UNS_UnLoadingCost.Table_Name)
			.getPO(getUNS_UnLoadingCost_ID(), get_TrxName());	}

	/** Set Unloading Cost.
		@param UNS_UnLoadingCost_ID Unloading Cost	  */
	public void setUNS_UnLoadingCost_ID (int UNS_UnLoadingCost_ID)
	{
		if (UNS_UnLoadingCost_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingCost_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingCost_ID, Integer.valueOf(UNS_UnLoadingCost_ID));
	}

	/** Get Unloading Cost.
		@return Unloading Cost	  */
	public int getUNS_UnLoadingCost_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingCost_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UnLoading Cost Line.
		@param UNS_UnLoadingCost_Line_ID UnLoading Cost Line	  */
	public void setUNS_UnLoadingCost_Line_ID (int UNS_UnLoadingCost_Line_ID)
	{
		if (UNS_UnLoadingCost_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingCost_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingCost_Line_ID, Integer.valueOf(UNS_UnLoadingCost_Line_ID));
	}

	/** Get UnLoading Cost Line.
		@return UnLoading Cost Line	  */
	public int getUNS_UnLoadingCost_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingCost_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UnLoadingCost_Line_UU.
		@param UNS_UnLoadingCost_Line_UU UNS_UnLoadingCost_Line_UU	  */
	public void setUNS_UnLoadingCost_Line_UU (String UNS_UnLoadingCost_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingCost_Line_UU, UNS_UnLoadingCost_Line_UU);
	}

	/** Get UNS_UnLoadingCost_Line_UU.
		@return UNS_UnLoadingCost_Line_UU	  */
	public String getUNS_UnLoadingCost_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UnLoadingCost_Line_UU);
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getUNS_WeighbridgeTicket_ID(), get_TrxName());	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}