/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_VoucherCode
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_VoucherCode extends PO implements I_UNS_VoucherCode, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190415L;

    /** Standard Constructor */
    public X_UNS_VoucherCode (Properties ctx, int UNS_VoucherCode_ID, String trxName)
    {
      super (ctx, UNS_VoucherCode_ID, trxName);
      /** if (UNS_VoucherCode_ID == 0)
        {
			setisInvalidate (false);
// N
			setUNS_VoucherBook_ID (0);
			setUNS_VoucherCode_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_VoucherCode (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_VoucherCode[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Invalidate.
		@param isInvalidate Invalidate	  */
	public void setisInvalidate (boolean isInvalidate)
	{
		set_Value (COLUMNNAME_isInvalidate, Boolean.valueOf(isInvalidate));
	}

	/** Get Invalidate.
		@return Invalidate	  */
	public boolean isInvalidate () 
	{
		Object oo = get_Value(COLUMNNAME_isInvalidate);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public com.unicore.model.I_UNS_VoucherBook getUNS_VoucherBook() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_VoucherBook)MTable.get(getCtx(), com.unicore.model.I_UNS_VoucherBook.Table_Name)
			.getPO(getUNS_VoucherBook_ID(), get_TrxName());	}

	/** Set Voucher Book.
		@param UNS_VoucherBook_ID Voucher Book	  */
	public void setUNS_VoucherBook_ID (int UNS_VoucherBook_ID)
	{
		if (UNS_VoucherBook_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_VoucherBook_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_VoucherBook_ID, Integer.valueOf(UNS_VoucherBook_ID));
	}

	/** Get Voucher Book.
		@return Voucher Book	  */
	public int getUNS_VoucherBook_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_VoucherBook_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Voucher Code.
		@param UNS_VoucherCode_ID Voucher Code	  */
	public void setUNS_VoucherCode_ID (int UNS_VoucherCode_ID)
	{
		if (UNS_VoucherCode_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_VoucherCode_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_VoucherCode_ID, Integer.valueOf(UNS_VoucherCode_ID));
	}

	/** Get Voucher Code.
		@return Voucher Code	  */
	public int getUNS_VoucherCode_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_VoucherCode_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Voucher Code UU.
		@param UNS_VoucherCode_UU Voucher Code UU	  */
	public void setUNS_VoucherCode_UU (String UNS_VoucherCode_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_VoucherCode_UU, UNS_VoucherCode_UU);
	}

	/** Get Voucher Code UU.
		@return Voucher Code UU	  */
	public String getUNS_VoucherCode_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_VoucherCode_UU);
	}

	/** Set Unused Amt.
		@param UnusedAmt Unused Amt	  */
	public void setUnusedAmt (BigDecimal UnusedAmt)
	{
		set_Value (COLUMNNAME_UnusedAmt, UnusedAmt);
	}

	/** Get Unused Amt.
		@return Unused Amt	  */
	public BigDecimal getUnusedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UnusedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Used Amount.
		@param UsedAmt Used Amount	  */
	public void setUsedAmt (BigDecimal UsedAmt)
	{
		set_Value (COLUMNNAME_UsedAmt, UsedAmt);
	}

	/** Get Used Amount.
		@return Used Amount	  */
	public BigDecimal getUsedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UsedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Voucher Amount.
		@param VoucherAmt Voucher Amount	  */
	public void setVoucherAmt (BigDecimal VoucherAmt)
	{
		set_Value (COLUMNNAME_VoucherAmt, VoucherAmt);
	}

	/** Get Voucher Amount.
		@return Voucher Amount	  */
	public BigDecimal getVoucherAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_VoucherAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}