/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for UNS_Grading_Sheet_All
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Grading_Sheet_All extends PO implements I_UNS_Grading_Sheet_All, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20170417L;

    /** Standard Constructor */
    public X_UNS_Grading_Sheet_All (Properties ctx, int UNS_Grading_Sheet_All_ID, String trxName)
    {
      super (ctx, UNS_Grading_Sheet_All_ID, trxName);
      /** if (UNS_Grading_Sheet_All_ID == 0)
        {
			setAdditional_Qty (Env.ZERO);
// 0
			setBunchFailure (Env.ZERO);
// 0
			setCompulsoryDeduction (Env.ZERO);
// 0
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDirt (Env.ZERO);
// 0
			setDirtyBunch (Env.ZERO);
// 0
			setDocAction (null);
// CO
			setDocStatus (null);
// DR
			setDocumentNo (null);
			setDripWet (Env.ZERO);
// 0
			setDura (Env.ZERO);
// 0
			setEmptyBunch (Env.ZERO);
// 0
			setEst_CompulsoryDeduction (Env.ZERO);
// 0
			setFFBReceiptCondition (null);
// B
			setIsApproved (false);
// N
			setIsDumpTruck (false);
// N
			setIsQtyPercentage (true);
// Y
			setLongStalk (Env.ZERO);
// 0
			setOldBunch (Env.ZERO);
// 0
			setOverRipe (Env.ZERO);
// 0
			setPercent_AddDeduct (Env.ZERO);
// 0
			setPercent_CompulsoryDeduction (Env.ZERO);
// 0
			setProcessed (false);
// N
			setRottenBunch (Env.ZERO);
// 0
			setRoundingScale (null);
// 1
			setShippedBy (null);
			setSmallFruit (Env.ZERO);
// 0
			setUnderdone (Env.ZERO);
// 0
			setUnripe (Env.ZERO);
// 0
			setUNS_Grading_Sheet_All_ID (0);
			setUNS_WeighbridgeTicket_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Grading_Sheet_All (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Grading_Sheet_All[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Additional Deduction.
		@param Additional_Deduction Additional Deduction	  */
	public void setAdditional_Deduction (String Additional_Deduction)
	{
		set_Value (COLUMNNAME_Additional_Deduction, Additional_Deduction);
	}

	/** Get Additional Deduction.
		@return Additional Deduction	  */
	public String getAdditional_Deduction () 
	{
		return (String)get_Value(COLUMNNAME_Additional_Deduction);
	}

	/** Set Additional Qty.
		@param Additional_Qty Additional Qty	  */
	public void setAdditional_Qty (BigDecimal Additional_Qty)
	{
		set_Value (COLUMNNAME_Additional_Qty, Additional_Qty);
	}

	/** Get Additional Qty.
		@return Additional Qty	  */
	public BigDecimal getAdditional_Qty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Additional_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Bunch Failure.
		@param BunchFailure 
		Tandan Ujung (Bunch Failure)
	  */
	public void setBunchFailure (BigDecimal BunchFailure)
	{
		set_Value (COLUMNNAME_BunchFailure, BunchFailure);
	}

	/** Get Bunch Failure.
		@return Tandan Ujung (Bunch Failure)
	  */
	public BigDecimal getBunchFailure () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BunchFailure);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		throw new IllegalArgumentException ("C_BPartner_ID is virtual column");	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Compulsory Deduction.
		@param CompulsoryDeduction 
		Compulsory Deduction (Potongan Wajib)
	  */
	public void setCompulsoryDeduction (BigDecimal CompulsoryDeduction)
	{
		set_Value (COLUMNNAME_CompulsoryDeduction, CompulsoryDeduction);
	}

	/** Get Compulsory Deduction.
		@return Compulsory Deduction (Potongan Wajib)
	  */
	public BigDecimal getCompulsoryDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CompulsoryDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Default Tare.
		@param DefaultTare 
		The Default Tare is the default weight of an armada type.
	  */
	public void setDefaultTare (BigDecimal DefaultTare)
	{
		set_Value (COLUMNNAME_DefaultTare, DefaultTare);
	}

	/** Get Default Tare.
		@return The Default Tare is the default weight of an armada type.
	  */
	public BigDecimal getDefaultTare () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DefaultTare);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Dirt.
		@param Dirt 
		Dirt
	  */
	public void setDirt (BigDecimal Dirt)
	{
		set_Value (COLUMNNAME_Dirt, Dirt);
	}

	/** Get Dirt.
		@return Dirt
	  */
	public BigDecimal getDirt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Dirt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Dirty Bunch.
		@param DirtyBunch 
		Dirty Bunch (Buah Kotor)
	  */
	public void setDirtyBunch (BigDecimal DirtyBunch)
	{
		set_Value (COLUMNNAME_DirtyBunch, DirtyBunch);
	}

	/** Get Dirty Bunch.
		@return Dirty Bunch (Buah Kotor)
	  */
	public BigDecimal getDirtyBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DirtyBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getDocumentNo());
    }

	/** Set Drip Wet.
		@param DripWet 
		Drip Wet (Banyak Kandungan Air)
	  */
	public void setDripWet (BigDecimal DripWet)
	{
		set_Value (COLUMNNAME_DripWet, DripWet);
	}

	/** Get Drip Wet.
		@return Drip Wet (Banyak Kandungan Air)
	  */
	public BigDecimal getDripWet () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DripWet);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Dura.
		@param Dura 
		Dura (Kernel Tebal / Buah Tipis)
	  */
	public void setDura (BigDecimal Dura)
	{
		set_Value (COLUMNNAME_Dura, Dura);
	}

	/** Get Dura.
		@return Dura (Kernel Tebal / Buah Tipis)
	  */
	public BigDecimal getDura () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Dura);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Empty Bunch.
		@param EmptyBunch 
		Empty Bunch (Tandan Kosong)
	  */
	public void setEmptyBunch (BigDecimal EmptyBunch)
	{
		set_Value (COLUMNNAME_EmptyBunch, EmptyBunch);
	}

	/** Get Empty Bunch.
		@return Empty Bunch (Tandan Kosong)
	  */
	public BigDecimal getEmptyBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EmptyBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Additional Deduction.
		@param Est_AddDeductQty Est. Additional Deduction	  */
	public void setEst_AddDeductQty (BigDecimal Est_AddDeductQty)
	{
		set_Value (COLUMNNAME_Est_AddDeductQty, Est_AddDeductQty);
	}

	/** Get Est. Additional Deduction.
		@return Est. Additional Deduction	  */
	public BigDecimal getEst_AddDeductQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_AddDeductQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Bunch Failure.
		@param Est_BunchFailure 
		Estimated Tandan Ujung (Bunch Failure)
	  */
	public void setEst_BunchFailure (BigDecimal Est_BunchFailure)
	{
		set_Value (COLUMNNAME_Est_BunchFailure, Est_BunchFailure);
	}

	/** Get Est. Bunch Failure.
		@return Estimated Tandan Ujung (Bunch Failure)
	  */
	public BigDecimal getEst_BunchFailure () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_BunchFailure);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Compulsory Deduction.
		@param Est_CompulsoryDeduction 
		Estimated Compulsory Deduction (Potongan Wajib)
	  */
	public void setEst_CompulsoryDeduction (BigDecimal Est_CompulsoryDeduction)
	{
		set_Value (COLUMNNAME_Est_CompulsoryDeduction, Est_CompulsoryDeduction);
	}

	/** Get Est. Compulsory Deduction.
		@return Estimated Compulsory Deduction (Potongan Wajib)
	  */
	public BigDecimal getEst_CompulsoryDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_CompulsoryDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Dirt.
		@param Est_Dirt 
		Estimated Dirt (Sampah)
	  */
	public void setEst_Dirt (BigDecimal Est_Dirt)
	{
		set_Value (COLUMNNAME_Est_Dirt, Est_Dirt);
	}

	/** Get Est. Dirt.
		@return Estimated Dirt (Sampah)
	  */
	public BigDecimal getEst_Dirt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_Dirt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Dirty Bunch.
		@param Est_DirtyBunch 
		Estimated Dirty Bunch (Buah Kotor)
	  */
	public void setEst_DirtyBunch (BigDecimal Est_DirtyBunch)
	{
		set_Value (COLUMNNAME_Est_DirtyBunch, Est_DirtyBunch);
	}

	/** Get Est. Dirty Bunch.
		@return Estimated Dirty Bunch (Buah Kotor)
	  */
	public BigDecimal getEst_DirtyBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_DirtyBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Drip Wet.
		@param Est_DripWet 
		Estimated Drip Wet (Banyak Kandungan Air)
	  */
	public void setEst_DripWet (BigDecimal Est_DripWet)
	{
		set_Value (COLUMNNAME_Est_DripWet, Est_DripWet);
	}

	/** Get Est. Drip Wet.
		@return Estimated Drip Wet (Banyak Kandungan Air)
	  */
	public BigDecimal getEst_DripWet () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_DripWet);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est.  Dura.
		@param Est_Dura 
		Estimated Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public void setEst_Dura (BigDecimal Est_Dura)
	{
		set_Value (COLUMNNAME_Est_Dura, Est_Dura);
	}

	/** Get Est.  Dura.
		@return Estimated Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public BigDecimal getEst_Dura () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_Dura);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Empty Bunch.
		@param Est_EmptyBunch 
		Estimated Empty Bunch (Tandan Kosong) in percentages
	  */
	public void setEst_EmptyBunch (BigDecimal Est_EmptyBunch)
	{
		set_Value (COLUMNNAME_Est_EmptyBunch, Est_EmptyBunch);
	}

	/** Get Est. Empty Bunch.
		@return Estimated Empty Bunch (Tandan Kosong) in percentages
	  */
	public BigDecimal getEst_EmptyBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_EmptyBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Long Stalk.
		@param Est_LongStalk 
		Estimated Long Stalk (Tandan Panjang) in percentages
	  */
	public void setEst_LongStalk (BigDecimal Est_LongStalk)
	{
		set_Value (COLUMNNAME_Est_LongStalk, Est_LongStalk);
	}

	/** Get Est. Long Stalk.
		@return Estimated Long Stalk (Tandan Panjang) in percentages
	  */
	public BigDecimal getEst_LongStalk () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_LongStalk);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Netto I.
		@param Est_NettoI 
		Netto 1 in estimation. It is Grossweight deducted with DefaultTare.
	  */
	public void setEst_NettoI (BigDecimal Est_NettoI)
	{
		throw new IllegalArgumentException ("Est_NettoI is virtual column");	}

	/** Get Est. Netto I.
		@return Netto 1 in estimation. It is Grossweight deducted with DefaultTare.
	  */
	public BigDecimal getEst_NettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_NettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Old Bunch.
		@param Est_OldBunch 
		Estimated Old Bunch (Tandan Lama)
	  */
	public void setEst_OldBunch (BigDecimal Est_OldBunch)
	{
		set_Value (COLUMNNAME_Est_OldBunch, Est_OldBunch);
	}

	/** Get Est. Old Bunch.
		@return Estimated Old Bunch (Tandan Lama)
	  */
	public BigDecimal getEst_OldBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_OldBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Over Ripe.
		@param Est_OverRipe 
		Estimated Over Ripe (Buah Lewat Masak)
	  */
	public void setEst_OverRipe (BigDecimal Est_OverRipe)
	{
		set_Value (COLUMNNAME_Est_OverRipe, Est_OverRipe);
	}

	/** Get Est. Over Ripe.
		@return Estimated Over Ripe (Buah Lewat Masak)
	  */
	public BigDecimal getEst_OverRipe () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_OverRipe);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Rotten Bunch.
		@param Est_RottenBunch 
		Estimated Rotten Bunch (Tandan Busuk)
	  */
	public void setEst_RottenBunch (BigDecimal Est_RottenBunch)
	{
		set_Value (COLUMNNAME_Est_RottenBunch, Est_RottenBunch);
	}

	/** Get Est. Rotten Bunch.
		@return Estimated Rotten Bunch (Tandan Busuk)
	  */
	public BigDecimal getEst_RottenBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_RottenBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Small Fruit.
		@param Est_SmallFruit 
		Estimated Small Fruit (Buah Kecil)
	  */
	public void setEst_SmallFruit (BigDecimal Est_SmallFruit)
	{
		set_Value (COLUMNNAME_Est_SmallFruit, Est_SmallFruit);
	}

	/** Get Est. Small Fruit.
		@return Estimated Small Fruit (Buah Kecil)
	  */
	public BigDecimal getEst_SmallFruit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_SmallFruit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est.  Underdone.
		@param Est_Underdone 
		Underdone (Kurang Masak) in percentages
	  */
	public void setEst_Underdone (BigDecimal Est_Underdone)
	{
		set_Value (COLUMNNAME_Est_Underdone, Est_Underdone);
	}

	/** Get Est.  Underdone.
		@return Underdone (Kurang Masak) in percentages
	  */
	public BigDecimal getEst_Underdone () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_Underdone);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Est. Unripe.
		@param Est_Unripe 
		Estimated Unripe (Sangat Mentah)
	  */
	public void setEst_Unripe (BigDecimal Est_Unripe)
	{
		set_Value (COLUMNNAME_Est_Unripe, Est_Unripe);
	}

	/** Get Est. Unripe.
		@return Estimated Unripe (Sangat Mentah)
	  */
	public BigDecimal getEst_Unripe () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Est_Unripe);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Dominantly Backlog = B */
	public static final String FFBRECEIPTCONDITION_DominantlyBacklog = "B";
	/** Dominantly Fresh = F */
	public static final String FFBRECEIPTCONDITION_DominantlyFresh = "F";
	/** Grader Overwritten = G */
	public static final String FFBRECEIPTCONDITION_GraderOverwritten = "G";
	/** Set FFB Receipt Condition.
		@param FFBReceiptCondition 
		The condition of FFB receipt, either dominantly fresh or backlog.
	  */
	public void setFFBReceiptCondition (String FFBReceiptCondition)
	{

		set_Value (COLUMNNAME_FFBReceiptCondition, FFBReceiptCondition);
	}

	/** Get FFB Receipt Condition.
		@return The condition of FFB receipt, either dominantly fresh or backlog.
	  */
	public String getFFBReceiptCondition () 
	{
		return (String)get_Value(COLUMNNAME_FFBReceiptCondition);
	}

	public org.compiere.model.I_AD_User getGrader() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getGrader_ID(), get_TrxName());	}

	/** Set Grader.
		@param Grader_ID Grader	  */
	public void setGrader_ID (int Grader_ID)
	{
		if (Grader_ID < 1) 
			set_Value (COLUMNNAME_Grader_ID, null);
		else 
			set_Value (COLUMNNAME_Grader_ID, Integer.valueOf(Grader_ID));
	}

	/** Get Grader.
		@return Grader	  */
	public int getGrader_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Grader_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Gross Weight.
		@param GrossWeight 
		Weight of a product (Gross)
	  */
	public void setGrossWeight (BigDecimal GrossWeight)
	{
		throw new IllegalArgumentException ("GrossWeight is virtual column");	}

	/** Get Gross Weight.
		@return Weight of a product (Gross)
	  */
	public BigDecimal getGrossWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrossWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approved.
		@param IsApproved 
		Indicates if this document requires approval
	  */
	public void setIsApproved (boolean IsApproved)
	{
		set_Value (COLUMNNAME_IsApproved, Boolean.valueOf(IsApproved));
	}

	/** Get Approved.
		@return Indicates if this document requires approval
	  */
	public boolean isApproved () 
	{
		Object oo = get_Value(COLUMNNAME_IsApproved);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Dump Truck.
		@param IsDumpTruck Is Dump Truck	  */
	public void setIsDumpTruck (boolean IsDumpTruck)
	{
		set_Value (COLUMNNAME_IsDumpTruck, Boolean.valueOf(IsDumpTruck));
	}

	/** Get Is Dump Truck.
		@return Is Dump Truck	  */
	public boolean isDumpTruck () 
	{
		Object oo = get_Value(COLUMNNAME_IsDumpTruck);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Qty Percentage.
		@param IsQtyPercentage 
		Indicate that this component is based in % Quantity
	  */
	public void setIsQtyPercentage (boolean IsQtyPercentage)
	{
		set_Value (COLUMNNAME_IsQtyPercentage, Boolean.valueOf(IsQtyPercentage));
	}

	/** Get Is Qty Percentage.
		@return Indicate that this component is based in % Quantity
	  */
	public boolean isQtyPercentage () 
	{
		Object oo = get_Value(COLUMNNAME_IsQtyPercentage);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Long Stalk.
		@param LongStalk 
		Long Stalk (Tandan Panjang)
	  */
	public void setLongStalk (BigDecimal LongStalk)
	{
		set_Value (COLUMNNAME_LongStalk, LongStalk);
	}

	/** Get Long Stalk.
		@return Long Stalk (Tandan Panjang)
	  */
	public BigDecimal getLongStalk () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LongStalk);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Netto-I.
		@param NettoI 
		The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI)
	{
		throw new IllegalArgumentException ("NettoI is virtual column");	}

	/** Get Netto-I.
		@return The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NettoI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Old Bunch.
		@param OldBunch 
		Old Bunch (Tandan Lama)
	  */
	public void setOldBunch (BigDecimal OldBunch)
	{
		set_Value (COLUMNNAME_OldBunch, OldBunch);
	}

	/** Get Old Bunch.
		@return Old Bunch (Tandan Lama)
	  */
	public BigDecimal getOldBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OldBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Over Ripe.
		@param OverRipe 
		Over Ripe (Buah Lewat Masak)
	  */
	public void setOverRipe (BigDecimal OverRipe)
	{
		set_Value (COLUMNNAME_OverRipe, OverRipe);
	}

	/** Get Over Ripe.
		@return Over Ripe (Buah Lewat Masak)
	  */
	public BigDecimal getOverRipe () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OverRipe);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Additional Deduction.
		@param Percent_AddDeduct (%) Additional Deduction	  */
	public void setPercent_AddDeduct (BigDecimal Percent_AddDeduct)
	{
		set_Value (COLUMNNAME_Percent_AddDeduct, Percent_AddDeduct);
	}

	/** Get (%) Additional Deduction.
		@return (%) Additional Deduction	  */
	public BigDecimal getPercent_AddDeduct () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_AddDeduct);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Bunch Failure.
		@param Percent_BunchFailure 
		(%) Bunch Failure
	  */
	public void setPercent_BunchFailure (BigDecimal Percent_BunchFailure)
	{
		set_Value (COLUMNNAME_Percent_BunchFailure, Percent_BunchFailure);
	}

	/** Get (%) Bunch Failure.
		@return (%) Bunch Failure
	  */
	public BigDecimal getPercent_BunchFailure () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_BunchFailure);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Compulsory Deduction.
		@param Percent_CompulsoryDeduction 
		Compulsory Deduction (Potongan Wajib) in percentage
	  */
	public void setPercent_CompulsoryDeduction (BigDecimal Percent_CompulsoryDeduction)
	{
		set_Value (COLUMNNAME_Percent_CompulsoryDeduction, Percent_CompulsoryDeduction);
	}

	/** Get (%) Compulsory Deduction.
		@return Compulsory Deduction (Potongan Wajib) in percentage
	  */
	public BigDecimal getPercent_CompulsoryDeduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_CompulsoryDeduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Dirt.
		@param Percent_Dirt 
		Dirt (Sampah) in percentages
	  */
	public void setPercent_Dirt (BigDecimal Percent_Dirt)
	{
		set_Value (COLUMNNAME_Percent_Dirt, Percent_Dirt);
	}

	/** Get (%) Dirt.
		@return Dirt (Sampah) in percentages
	  */
	public BigDecimal getPercent_Dirt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_Dirt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Dirty Bunch.
		@param Percent_DirtyBunch 
		Dirty Bunch (Buah Kotor) in percentage
	  */
	public void setPercent_DirtyBunch (BigDecimal Percent_DirtyBunch)
	{
		set_Value (COLUMNNAME_Percent_DirtyBunch, Percent_DirtyBunch);
	}

	/** Get (%) Dirty Bunch.
		@return Dirty Bunch (Buah Kotor) in percentage
	  */
	public BigDecimal getPercent_DirtyBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_DirtyBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Drip Wet.
		@param Percent_DripWet 
		Drip Wet (Banyak Kandungan Air) in percentages
	  */
	public void setPercent_DripWet (BigDecimal Percent_DripWet)
	{
		set_Value (COLUMNNAME_Percent_DripWet, Percent_DripWet);
	}

	/** Get (%) Drip Wet.
		@return Drip Wet (Banyak Kandungan Air) in percentages
	  */
	public BigDecimal getPercent_DripWet () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_DripWet);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Dura.
		@param Percent_Dura 
		Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public void setPercent_Dura (BigDecimal Percent_Dura)
	{
		set_Value (COLUMNNAME_Percent_Dura, Percent_Dura);
	}

	/** Get (%) Dura.
		@return Dura (Kernel Tebal / Buah Tipis) in percentages
	  */
	public BigDecimal getPercent_Dura () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_Dura);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Empty Bunch.
		@param Percent_EmptyBunch 
		Empty Bunch (Tandan Kosong) in percentages
	  */
	public void setPercent_EmptyBunch (BigDecimal Percent_EmptyBunch)
	{
		set_Value (COLUMNNAME_Percent_EmptyBunch, Percent_EmptyBunch);
	}

	/** Get (%) Empty Bunch.
		@return Empty Bunch (Tandan Kosong) in percentages
	  */
	public BigDecimal getPercent_EmptyBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_EmptyBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Long Stalk.
		@param Percent_LongStalk 
		Long Stalk (Tandan Panjang) in percentages
	  */
	public void setPercent_LongStalk (BigDecimal Percent_LongStalk)
	{
		set_Value (COLUMNNAME_Percent_LongStalk, Percent_LongStalk);
	}

	/** Get (%) Long Stalk.
		@return Long Stalk (Tandan Panjang) in percentages
	  */
	public BigDecimal getPercent_LongStalk () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_LongStalk);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Old Bunch.
		@param Percent_OldBunch 
		Old Bunch (Tandan Lama) in percentages
	  */
	public void setPercent_OldBunch (BigDecimal Percent_OldBunch)
	{
		set_Value (COLUMNNAME_Percent_OldBunch, Percent_OldBunch);
	}

	/** Get (%) Old Bunch.
		@return Old Bunch (Tandan Lama) in percentages
	  */
	public BigDecimal getPercent_OldBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_OldBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Over Ripe.
		@param Percent_OverRipe 
		Over Ripe (Lewat Mentah) in percentages
	  */
	public void setPercent_OverRipe (BigDecimal Percent_OverRipe)
	{
		set_Value (COLUMNNAME_Percent_OverRipe, Percent_OverRipe);
	}

	/** Get (%) Over Ripe.
		@return Over Ripe (Lewat Mentah) in percentages
	  */
	public BigDecimal getPercent_OverRipe () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_OverRipe);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Rotten Bunch.
		@param Percent_RottenBunch 
		Rotten Bunch (Tandan Busuk) in percentages
	  */
	public void setPercent_RottenBunch (BigDecimal Percent_RottenBunch)
	{
		set_Value (COLUMNNAME_Percent_RottenBunch, Percent_RottenBunch);
	}

	/** Get (%) Rotten Bunch.
		@return Rotten Bunch (Tandan Busuk) in percentages
	  */
	public BigDecimal getPercent_RottenBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_RottenBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Small Fruit.
		@param Percent_SmallFruit 
		Small Fruit (Buah Kecil) in percentages
	  */
	public void setPercent_SmallFruit (BigDecimal Percent_SmallFruit)
	{
		set_Value (COLUMNNAME_Percent_SmallFruit, Percent_SmallFruit);
	}

	/** Get (%) Small Fruit.
		@return Small Fruit (Buah Kecil) in percentages
	  */
	public BigDecimal getPercent_SmallFruit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_SmallFruit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Underdone.
		@param Percent_Underdone 
		Underdone (Kurang Masak) in percentages
	  */
	public void setPercent_Underdone (BigDecimal Percent_Underdone)
	{
		set_Value (COLUMNNAME_Percent_Underdone, Percent_Underdone);
	}

	/** Get (%) Underdone.
		@return Underdone (Kurang Masak) in percentages
	  */
	public BigDecimal getPercent_Underdone () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_Underdone);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set (%) Unripe.
		@param Percent_Unripe 
		Unripe (Sangat Mentah) in percentages
	  */
	public void setPercent_Unripe (BigDecimal Percent_Unripe)
	{
		set_Value (COLUMNNAME_Percent_Unripe, Percent_Unripe);
	}

	/** Get (%) Unripe.
		@return Unripe (Sangat Mentah) in percentages
	  */
	public BigDecimal getPercent_Unripe () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Percent_Unripe);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Rotten Bunch.
		@param RottenBunch 
		Rotten Bunch (Tandan Busuk)
	  */
	public void setRottenBunch (BigDecimal RottenBunch)
	{
		set_Value (COLUMNNAME_RottenBunch, RottenBunch);
	}

	/** Get Rotten Bunch.
		@return Rotten Bunch (Tandan Busuk)
	  */
	public BigDecimal getRottenBunch () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RottenBunch);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** No Scale = 0 */
	public static final String ROUNDINGSCALE_NoScale = "0";
	/** Units = 1 */
	public static final String ROUNDINGSCALE_Units = "1";
	/** Dozens = 10 */
	public static final String ROUNDINGSCALE_Dozens = "10";
	/** Hundreds = 100 */
	public static final String ROUNDINGSCALE_Hundreds = "100";
	/** Thousands = 1000 */
	public static final String ROUNDINGSCALE_Thousands = "1000";
	/** One Decimal Place = -1 */
	public static final String ROUNDINGSCALE_OneDecimalPlace = "-1";
	/** Two Decimal Places = -2 */
	public static final String ROUNDINGSCALE_TwoDecimalPlaces = "-2";
	/** Set Rounding Scale.
		@param RoundingScale 
		Scale to be rounding (ie. scale to unit, dozens, hundred, thousands)
	  */
	public void setRoundingScale (String RoundingScale)
	{

		set_Value (COLUMNNAME_RoundingScale, RoundingScale);
	}

	/** Get Rounding Scale.
		@return Scale to be rounding (ie. scale to unit, dozens, hundred, thousands)
	  */
	public String getRoundingScale () 
	{
		return (String)get_Value(COLUMNNAME_RoundingScale);
	}

	/** Air = Air */
	public static final String SHIPPEDBY_Air = "Air";
	/** Darat = DRT */
	public static final String SHIPPEDBY_Darat = "DRT";
	/** Set Shipped By.
		@param ShippedBy Shipped By	  */
	public void setShippedBy (String ShippedBy)
	{

		set_Value (COLUMNNAME_ShippedBy, ShippedBy);
	}

	/** Get Shipped By.
		@return Shipped By	  */
	public String getShippedBy () 
	{
		return (String)get_Value(COLUMNNAME_ShippedBy);
	}

	/** Set Small Fruit.
		@param SmallFruit 
		Small Fruit (Buah Kecil)
	  */
	public void setSmallFruit (BigDecimal SmallFruit)
	{
		set_Value (COLUMNNAME_SmallFruit, SmallFruit);
	}

	/** Get Small Fruit.
		@return Small Fruit (Buah Kecil)
	  */
	public BigDecimal getSmallFruit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SmallFruit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Tare.
		@param Tare 
		The weight as comparison
	  */
	public void setTare (BigDecimal Tare)
	{
		throw new IllegalArgumentException ("Tare is virtual column");	}

	/** Get Tare.
		@return The weight as comparison
	  */
	public BigDecimal getTare () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Tare);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Estimation.
		@param TotalEstimation 
		Total estimation value
	  */
	public void setTotalEstimation (BigDecimal TotalEstimation)
	{
		throw new IllegalArgumentException ("TotalEstimation is virtual column");	}

	/** Get Total Estimation.
		@return Total estimation value
	  */
	public BigDecimal getTotalEstimation () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalEstimation);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Percent.
		@param TotalPercent 
		The grand total of percentages
	  */
	public void setTotalPercent (BigDecimal TotalPercent)
	{
		throw new IllegalArgumentException ("TotalPercent is virtual column");	}

	/** Get Total Percent.
		@return The grand total of percentages
	  */
	public BigDecimal getTotalPercent () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalPercent);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Quantity.
		@param TotalQty 
		Total Quantity
	  */
	public void setTotalQty (BigDecimal TotalQty)
	{
		throw new IllegalArgumentException ("TotalQty is virtual column");	}

	/** Get Total Quantity.
		@return Total Quantity
	  */
	public BigDecimal getTotalQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Underdone.
		@param Underdone 
		Underdone (Kurang Masak)
	  */
	public void setUnderdone (BigDecimal Underdone)
	{
		set_Value (COLUMNNAME_Underdone, Underdone);
	}

	/** Get Underdone.
		@return Underdone (Kurang Masak)
	  */
	public BigDecimal getUnderdone () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Underdone);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Unripe.
		@param Unripe 
		Unripe (Sangat Mentah)
	  */
	public void setUnripe (BigDecimal Unripe)
	{
		set_Value (COLUMNNAME_Unripe, Unripe);
	}

	/** Get Unripe.
		@return Unripe (Sangat Mentah)
	  */
	public BigDecimal getUnripe () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Unripe);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Grading Sheet.
		@param UNS_Grading_Sheet_All_ID Grading Sheet	  */
	public void setUNS_Grading_Sheet_All_ID (int UNS_Grading_Sheet_All_ID)
	{
		if (UNS_Grading_Sheet_All_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Grading_Sheet_All_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Grading_Sheet_All_ID, Integer.valueOf(UNS_Grading_Sheet_All_ID));
	}

	/** Get Grading Sheet.
		@return Grading Sheet	  */
	public int getUNS_Grading_Sheet_All_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Grading_Sheet_All_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Grading_Sheet_All_UU.
		@param UNS_Grading_Sheet_All_UU UNS_Grading_Sheet_All_UU	  */
	public void setUNS_Grading_Sheet_All_UU (String UNS_Grading_Sheet_All_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Grading_Sheet_All_UU, UNS_Grading_Sheet_All_UU);
	}

	/** Get UNS_Grading_Sheet_All_UU.
		@return UNS_Grading_Sheet_All_UU	  */
	public String getUNS_Grading_Sheet_All_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Grading_Sheet_All_UU);
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getUNS_WeighbridgeTicket_ID(), get_TrxName());	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Vehicle No.
		@param VehicleNo 
		The identification number of the vehicle being weighted
	  */
	public void setVehicleNo (String VehicleNo)
	{
		throw new IllegalArgumentException ("VehicleNo is virtual column");	}

	/** Get Vehicle No.
		@return The identification number of the vehicle being weighted
	  */
	public String getVehicleNo () 
	{
		return (String)get_Value(COLUMNNAME_VehicleNo);
	}
}