/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MLocator;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author UNTA_Andy
 * 
 */
public class MUNSPLConfirmProduct extends X_UNS_PL_ConfirmProduct
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3808189968306020691L;
	private MUNSPLConfirmLine[] m_Lines;
	private MUNSPLConfirmMA[] m_MALines;
	private MUNSPLConfirm m_parent;

	/**
	 * @param ctx
	 * @param UNS_PL_ConfirmProduct_ID
	 * @param trxName
	 */
	public MUNSPLConfirmProduct(Properties ctx, int UNS_PL_ConfirmProduct_ID, String trxName)
	{
		super(ctx, UNS_PL_ConfirmProduct_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPLConfirmProduct(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public MUNSPLConfirmProduct(MUNSPLConfirm confirm, int UNS_PL_ConfimProduct_ID)
	{
		this(confirm.getCtx(), UNS_PL_ConfimProduct_ID, confirm.get_TrxName());

		setClientOrg(confirm);
		setUNS_PL_Confirm_ID(confirm.get_ID());

		m_parent = confirm;
	}

	public MUNSPLConfirm getParent()
	{
		if (m_parent == null)
			m_parent = (MUNSPLConfirm) getUNS_PL_Confirm();

		return m_parent;

	}


	/**************************************************************************
	 * Get Orders
	 * 
	 * @param whereClause where clause or null (starting with AND)
	 * @param orderClause order clause
	 * @return orders
	 */
	public MUNSPLConfirmLine[] getLines(String whereClause, String orderClause)
	{
		// red1 - using new Query class from Teo / Victor's MDDOrder.java implementation
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmLine.COLUMNNAME_UNS_PL_ConfirmProduct_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (orderClause.length() == 0)
			orderClause = MUNSPLConfirmLine.COLUMNNAME_UNS_PL_ConfirmLine_ID;
		//
		List<MUNSPLConfirmLine> list =
				Query
						.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmLine.Table_Name,
								whereClauseFinal.toString(), get_TrxName()).setParameters(get_ID())
						.setOrderBy(orderClause).list();

		return list.toArray(new MUNSPLConfirmLine[list.size()]);
	} // getLines

	/**
	 * Get Lines of Order
	 * 
	 * @param requery requery
	 * @param orderBy optional order by column
	 * @return lines
	 */
	public MUNSPLConfirmLine[] getLines(boolean requery, String orderBy)
	{
		if (m_Lines != null && !requery)
		{
			set_TrxName(m_Lines, get_TrxName());
			return m_Lines;
		}
		//
		String orderClause = "";
		if (orderBy != null && orderBy.length() > 0)
			orderClause += orderBy;

		m_Lines = getLines(null, orderClause);
		return m_Lines;
	} // getLines

	/**
	 * Get Lines of Order. (used by web store)
	 * 
	 * @return lines
	 */
	public MUNSPLConfirmLine[] getLines()
	{
		return getLines(false, null);
	} // getLines



	/**************************************************************************
	 * Get Orders
	 * 
	 * @param whereClause where clause or null (starting with AND)
	 * @param orderClause order clause
	 * @return orders
	 */
	public MUNSPLConfirmMA[] getMAs(String whereClause, String orderClause)
	{
		// red1 - using new Query class from Teo / Victor's MDDOrder.java implementation
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmMA.COLUMNNAME_UNS_PL_ConfirmProduct_ID + "=? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (orderClause.length() == 0)
			orderClause = MUNSPLConfirmMA.COLUMNNAME_UNS_PL_ConfirmMA_UU;
		//
		List<MUNSPLConfirmMA> list =
				Query
						.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmMA.Table_Name,
								whereClauseFinal.toString(), get_TrxName()).setParameters(get_ID())
						.setOrderBy(orderClause).list();
		m_MALines = new MUNSPLConfirmMA[list.size()];
		list.toArray(m_MALines);
		return m_MALines; 
	} // getLines

	/**
	 * Get Lines of Order
	 * 
	 * @param requery requery
	 * @param orderBy optional order by column
	 * @return lines
	 */
	public MUNSPLConfirmMA[] getMAs(boolean requery, String orderBy)
	{
		if (m_MALines != null && !requery)
		{
			set_TrxName(m_MALines, get_TrxName());
			return m_MALines;
		}
		//
		String orderClause = "";
		if (orderBy != null && orderBy.length() > 0)
			orderClause += orderBy;

		m_MALines = getMAs(null, orderClause);
		return m_MALines;
	} // getLines

	/**
	 * Get Lines of Order. (used by web store)
	 * 
	 * @return lines
	 */
	public MUNSPLConfirmMA[] getMAs()
	{
		return getMAs(false, null);
	} // getLines

	/* ************************************************************************ */

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete()
	{
		for (MUNSPLConfirmLine line : getLines())
		{
			if (!line.delete(true))
				return false;
		}

		for (MUNSPLConfirmMA ma : getMAs())
		{
			if (!ma.delete(true))
				return false;
		}

		return super.beforeDelete();
	}

	public void setProduct(MUNSPackingListLine line)
	{
		setM_Product_ID(line.getM_Product_ID());
		setC_UOM_ID(line.getC_UOM_ID());
	}

	public void setQty(BigDecimal qty)
	{
		setConfirmedQty(getConfirmedQty().add(qty));
		setTargetQty(getTargetQty().add(qty));
		setMovementQty(getConfirmedQty());//getTargetQty());
		setDifferenceQty(getTargetQty().subtract(getConfirmedQty()));
	}

	public static MUNSPLConfirmProduct getNewConfirmProduct(MUNSPackingListLine line, MUNSPLConfirm parent)
	{
		MUNSPLConfirmProduct confirm = null;
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmProduct.COLUMNNAME_M_Product_ID).append("=? AND ")
						.append(MUNSPLConfirmProduct.COLUMNNAME_M_Warehouse_ID).append("=? AND ")
						.append(MUNSPLConfirmProduct.COLUMNNAME_M_Locator_ID).append("=? AND ")
						.append(MUNSPLConfirmProduct.COLUMNNAME_UNS_PL_Confirm_ID).append("=?");

		confirm =
				Query
						.get(parent.getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmProduct.Table_Name,
								whereClauseFinal.toString(), parent.get_TrxName())
						.setParameters(line.getM_Product_ID(), line.getParent().getM_Warehouse_ID()
								, line.getM_Locator_ID(), parent.getUNS_PL_Confirm_ID())
						.firstOnly();

		if (confirm != null)
			return confirm;
		
		confirm = new MUNSPLConfirmProduct(parent, 0);
		confirm.setProduct(line);
		confirm.setM_Locator_ID(line.getM_Locator_ID());
		confirm.setM_Warehouse_ID(line.getParent().getM_Warehouse_ID());
		
		return confirm;
	}

	public static MUNSPLConfirmProduct getNewReturnConfirmProduct(MUNSPLReturnLine line, MUNSPLConfirm parent)
	{
		MUNSPLConfirmProduct confirm = null;
		StringBuilder whereClauseFinal =
				new StringBuilder(MUNSPLConfirmProduct.COLUMNNAME_M_Product_ID).append("=? AND ")
						.append(MUNSPLConfirmProduct.COLUMNNAME_M_Warehouse_ID).append("=? AND ")
						.append(MUNSPLConfirmProduct.COLUMNNAME_M_Locator_ID).append("=? AND ")
						.append(MUNSPLConfirmProduct.COLUMNNAME_UNS_PL_Confirm_ID).append("=?");

		int whs_id = MLocator.get(line.getCtx(), line.getM_Locator_ID()).getM_Warehouse_ID();
		
		confirm =
				Query
						.get(parent.getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSPLConfirmProduct.Table_Name,
								whereClauseFinal.toString(), parent.get_TrxName())
						.setParameters(line.getM_Product_ID(), whs_id, line.getM_Locator_ID(), 
										parent.getUNS_PL_Confirm_ID())
						.firstOnly();
		if (confirm != null)
			return confirm;
		
		confirm = new MUNSPLConfirmProduct(parent, 0);
		confirm.setM_Product_ID(line.getM_Product_ID());
		confirm.setC_UOM_ID(line.getC_UOM_ID());
		confirm.setM_Locator_ID(line.getM_Locator_ID());
		confirm.setM_Warehouse_ID(whs_id);
		
		return confirm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#afterSave(boolean, boolean)
	 */
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		getParent().updateTonaseVolume();
		
		if(!newRecord)
		{
			if(getTargetQty().compareTo(getConfirmedQty()) != 0)
			{
				BigDecimal newAllocatedQty = getConfirmedQty();//getDifferenceQty();
				
				for(MUNSPLConfirmLine line : getLines())
				{
					if(newAllocatedQty.compareTo(line.getTargetQty()) >= 0)
					{
						newAllocatedQty = newAllocatedQty.subtract(line.getTargetQty());
					}
					else //if(newAllocatedQty.compareTo(line.getTargetQty()) > 0 )
					{
						line.setQtyEntered(newAllocatedQty);
						line.isSavingProductList = true;
						line.saveEx();
						line.isSavingProductList = false;
						newAllocatedQty = Env.ZERO;
					}
				}
			}
		}
		
		return super.afterSave(newRecord, success);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.PO#afterSave(boolean, boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if (getConfirmedQty().compareTo(getTargetQty()) > 0)
			throw new AdempiereUserError("Confirmed Qty cannot greater than Targeted Qty.");

		setDifferenceQty(getMovementQty().subtract(getConfirmedQty()));
		
		return super.beforeSave(newRecord);
	}
	
	public boolean deleteLinesMA()
	{
		String sql = "DELETE FROM UNS_PL_ConfirmMA WHERE UNS_PL_ConfirmProduct_ID = ? AND IsAutoGenerated='Y'";
		int retVal = DB.executeUpdate(sql, getUNS_PL_ConfirmProduct_ID(), get_TrxName());
		
		return retVal != -1;
	}

}
