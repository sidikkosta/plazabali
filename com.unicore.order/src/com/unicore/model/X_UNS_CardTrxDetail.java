/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_CardTrxDetail
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CardTrxDetail extends PO implements I_UNS_CardTrxDetail, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180925L;

    /** Standard Constructor */
    public X_UNS_CardTrxDetail (Properties ctx, int UNS_CardTrxDetail_ID, String trxName)
    {
      super (ctx, UNS_CardTrxDetail_ID, trxName);
      /** if (UNS_CardTrxDetail_ID == 0)
        {
			setBatchNo (null);
			setUNS_CardTrxDetail_ID (0);
			setUNS_EDC_ID (0);
			setUNS_PaymentTrx_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CardTrxDetail (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CardTrxDetail[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Batch No.
		@param BatchNo Batch No	  */
	public void setBatchNo (String BatchNo)
	{
		set_Value (COLUMNNAME_BatchNo, BatchNo);
	}

	/** Get Batch No.
		@return Batch No	  */
	public String getBatchNo () 
	{
		return (String)get_Value(COLUMNNAME_BatchNo);
	}

	/** Set Cardholder Name.
		@param CardholderName Cardholder Name	  */
	public void setCardholderName (String CardholderName)
	{
		set_Value (COLUMNNAME_CardholderName, CardholderName);
	}

	/** Get Cardholder Name.
		@return Cardholder Name	  */
	public String getCardholderName () 
	{
		return (String)get_Value(COLUMNNAME_CardholderName);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Invoice No.
		@param InvoiceNo Invoice No	  */
	public void setInvoiceNo (String InvoiceNo)
	{
		set_Value (COLUMNNAME_InvoiceNo, InvoiceNo);
	}

	/** Get Invoice No.
		@return Invoice No	  */
	public String getInvoiceNo () 
	{
		return (String)get_Value(COLUMNNAME_InvoiceNo);
	}

	/** Set Original Invoice No.
		@param OriginalInvoiceNo Original Invoice No	  */
	public void setOriginalInvoiceNo (String OriginalInvoiceNo)
	{
		set_Value (COLUMNNAME_OriginalInvoiceNo, OriginalInvoiceNo);
	}

	/** Get Original Invoice No.
		@return Original Invoice No	  */
	public String getOriginalInvoiceNo () 
	{
		return (String)get_Value(COLUMNNAME_OriginalInvoiceNo);
	}

	/** Set Trace No.
		@param TraceNo Trace No	  */
	public void setTraceNo (String TraceNo)
	{
		set_Value (COLUMNNAME_TraceNo, TraceNo);
	}

	/** Get Trace No.
		@return Trace No	  */
	public String getTraceNo () 
	{
		return (String)get_Value(COLUMNNAME_TraceNo);
	}

	/** Set Transaction Type.
		@param TransactionType Transaction Type	  */
	public void setTransactionType (String TransactionType)
	{
		set_Value (COLUMNNAME_TransactionType, TransactionType);
	}

	/** Get Transaction Type.
		@return Transaction Type	  */
	public String getTransactionType () 
	{
		return (String)get_Value(COLUMNNAME_TransactionType);
	}

	/** Set Card Trx Detail.
		@param UNS_CardTrxDetail_ID Card Trx Detail	  */
	public void setUNS_CardTrxDetail_ID (int UNS_CardTrxDetail_ID)
	{
		if (UNS_CardTrxDetail_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardTrxDetail_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardTrxDetail_ID, Integer.valueOf(UNS_CardTrxDetail_ID));
	}

	/** Get Card Trx Detail.
		@return Card Trx Detail	  */
	public int getUNS_CardTrxDetail_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardTrxDetail_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CardTrxDetail_UU.
		@param UNS_CardTrxDetail_UU UNS_CardTrxDetail_UU	  */
	public void setUNS_CardTrxDetail_UU (String UNS_CardTrxDetail_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CardTrxDetail_UU, UNS_CardTrxDetail_UU);
	}

	/** Get UNS_CardTrxDetail_UU.
		@return UNS_CardTrxDetail_UU	  */
	public String getUNS_CardTrxDetail_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CardTrxDetail_UU);
	}

	public com.unicore.model.I_UNS_CardType getUNS_CardType() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_CardType)MTable.get(getCtx(), com.unicore.model.I_UNS_CardType.Table_Name)
			.getPO(getUNS_CardType_ID(), get_TrxName());	}

	/** Set Card Type.
		@param UNS_CardType_ID Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID)
	{
		if (UNS_CardType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, Integer.valueOf(UNS_CardType_ID));
	}

	/** Get Card Type.
		@return Card Type	  */
	public int getUNS_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_EDC getUNS_EDC() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_EDC)MTable.get(getCtx(), com.unicore.model.I_UNS_EDC.Table_Name)
			.getPO(getUNS_EDC_ID(), get_TrxName());	}

	/** Set EDC.
		@param UNS_EDC_ID EDC	  */
	public void setUNS_EDC_ID (int UNS_EDC_ID)
	{
		if (UNS_EDC_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDC_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDC_ID, Integer.valueOf(UNS_EDC_ID));
	}

	/** Get EDC.
		@return EDC	  */
	public int getUNS_EDC_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDC_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_PaymentTrx getUNS_PaymentTrx() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_PaymentTrx)MTable.get(getCtx(), com.unicore.model.I_UNS_PaymentTrx.Table_Name)
			.getPO(getUNS_PaymentTrx_ID(), get_TrxName());	}

	/** Set Payment Transaction.
		@param UNS_PaymentTrx_ID Payment Transaction	  */
	public void setUNS_PaymentTrx_ID (int UNS_PaymentTrx_ID)
	{
		if (UNS_PaymentTrx_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PaymentTrx_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PaymentTrx_ID, Integer.valueOf(UNS_PaymentTrx_ID));
	}

	/** Get Payment Transaction.
		@return Payment Transaction	  */
	public int getUNS_PaymentTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PaymentTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}