package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.unicore.base.model.MRequisitionLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

public class MRequisitionLineConfirm extends X_M_RequisitionLine_Confirm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7973491901013520533L;
	public static final String Approve = "APP";
	public static final String Reject = "RJT";
	private MRequisitionConfirm m_parent = null;

	public MRequisitionLineConfirm(Properties ctx,
			int M_RequisitionLine_Confirm_ID, String trxName) {
		super(ctx, M_RequisitionLine_Confirm_ID, trxName);
	}

	public MRequisitionLineConfirm(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		getParent();
		
		if (newRecord && m_parent.isComplete()) {
			log.saveError("ParentComplete", Msg.translate(getCtx(), "M_RequisitionLine_Confirm"));
			return false;
		}
		
		if(!newRecord && getApproval() == null){
			log.saveError("You must declare this requisition line is approved or rejected", Msg.translate(getCtx(), "M_RequisitionLine_Confirm"));
			return false;
		}
		
		if(!newRecord && getApproval().equals(APPROVAL_Reject))
			setConfirmedQty(Env.ZERO);
		
		if(!newRecord && getApproval().equals(APPROVAL_Approve))
		{
			if(getConfirmedQty().signum() != 1)
			{
				log.saveError("You have chosen approve, so confirmed quantity can't zero or negative quantity", Msg.translate(getCtx(), "M_RequisitionLine_Confirm"));
				return false;
			}	
		}
		
		setTotalLines(getPriceActual().multiply(getConfirmedQty()));
	
		return true;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		
		return true;
	}
	
	public MRequisitionConfirm getParent()
	{
		if(m_parent == null)
			m_parent = new MRequisitionConfirm(getCtx(), getM_RequisitionConfirm_ID(), get_TrxName());
		
		return m_parent;
	}
	
	public static MRequisitionLineConfirm getOfReference(Properties ctx, int ReqLine_ID, String trxName)
	{
		MRequisitionLineConfirm value = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				MRequisitionLineConfirm.Table_Name, MRequisitionLine.Table_Name + "_ID=?", trxName)
					.setParameters(ReqLine_ID).first();
		
		return value;
	}
}