/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.unicore.base.model.MInvoiceLine;
import com.uns.model.IUNSApprovalInfo;

/**
 * @author Burhani Adam
 *
 */
public class MUNSReflectionRevision extends X_UNS_ReflectionRevision implements DocAction, IUNSApprovalInfo, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7100010696466077319L;

	/**
	 * @param ctx
	 * @param UNS_ReflectionRevision_ID
	 * @param trxName
	 */
	public MUNSReflectionRevision(Properties ctx,
			int UNS_ReflectionRevision_ID, String trxName) {
		super(ctx, UNS_ReflectionRevision_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSReflectionRevision(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		if(getDateFrom().compareTo(getDateTo()) > 0)
		{
			log.saveError("Error", "Date From < Date To.");
			return false;
		}
		
		return true;
	}
	
	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSReflectionRevision[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_ReflectionRevision_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessed(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Approve Document
	 * 	@return true if success 
	 */
	public boolean  approveIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}	//	approveIt
	
	/**
	 * 	Reject Approval
	 * 	@return true if success 
	 */
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}	//	rejectIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		
		for(MUNSReflectionRevisionLine line : getLines())
		{
			MUNSWeighbridgeTicket wt = new MUNSWeighbridgeTicket(getCtx(), line.getUNS_WeighbridgeTicket_ID(), get_TrxName());
			wt.setRevisionQty(line.getRevisionQty());
			wt.setQtyPayment(line.getQtyPayment());
			wt.setRevisionPercent(line.getRevisionPercent());
			wt.saveEx();
			
			String sql = "SELECT l.C_InvoiceLine_ID FROM C_InvoiceLine l WHERE l.C_InvoiceLine_ID=?"
					+ " AND EXISTS (SELECT i.C_Invoice_ID FROM C_Invoice i WHERE i.C_Invoice_ID = l.C_Invoice_ID"
					+ " AND i.DocStatus NOT IN ('VO', 'RE'))";
			int idLine = DB.getSQLValue(get_TrxName(), sql, wt.getC_InvoiceLine_ID());
			if(idLine > 0)
			{
				MInvoiceLine iLine = new MInvoiceLine(getCtx(), idLine, get_TrxName());
				if(iLine.isProcessed())
				{
					m_processMsg = "Invoice has processed, remove ticket (" + wt.getDocumentNo() + ")"
							+ " OR Void Invoice (" + iLine.getC_Invoice().getDocumentNo() + ")";
					return DocAction.STATUS_Invalid;
				}
				else
				{
					iLine.set_ValueOfColumn("QtyInvoiceTicket", wt.getQtyPayment());
					if(!iLine.save())
					{
						m_processMsg = "Failed when trying update invoice line";
						return DocAction.STATUS_Invalid;
					}
				}
			}
		}
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setProcessed(false);
		setDocAction(DOCACTION_Complete);
		return true;
	}	//	reActivateIt
	
	/*************************************************************************
	 * 	Get Summary
	 *	@return Summary of Document
	 */
	public String getSummary()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		return sb.toString();
	}	//	getSummary
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	
	/**
	 * 	Get Document Owner
	 *	@return AD_User_ID
	 */
	public int getDoc_User_ID()
	{
		return getAD_User_ID();
	}
	
	/**
	 * 	Get Document Currency
	 *	@return C_Currency_ID
	 */
	public int getC_Currency_ID()
	{
		return 0;
	}

	/**
	 * 	Get Document Approval Amount
	 *	@return amount
	 */
	public BigDecimal getApprovalAmt()
	{
		return Env.ZERO;
	}
	
	protected List<MUNSReflectionRevisionLine> getLines()
	{
		List<MUNSReflectionRevisionLine> lines = new Query(getCtx(), MUNSReflectionRevisionLine.Table_Name,
				COLUMNNAME_UNS_ReflectionRevision_ID + "=?", get_TrxName()).setParameters(get_ID()).list();
		
		return lines;
	}

	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable()
	{	
		List<Object[]> list = new ArrayList<>();	
		list.add(new Object[]{String.class, true}); 			//Ticket
		list.add(new Object[]{String.class, true}); 			//Supplier
		list.add(new Object[]{String.class, true}); 			//NettoI-NettoII
		list.add(new Object[]{String.class, true});				//ReflectionTicket(%)
		list.add(new Object[]{String.class, true});				//ReflectionFlat(%)
		list.add(new Object[]{String.class, true});				//Revision(%)
		list.add(new Object[]{BigDecimal.class, true});			//QtyPayment
		
		return list;
	}

	@Override
	public String[] getDetailTableHeader()
	{
		String def[] = new String[]{"Ticket", "Supplier", "NettoI-NettoII", "Reflection Ticket(%)",
				"Reflection Flat(%)", "Revision(%)", "Qty Payment"};
		
		return def;
	}

	@Override
	public List<Object[]> getDetailTableContent()
	{	
		List<Object[]> list = new ArrayList<>();
		
		String sql = "SELECT wt.DocumentNo AS Ticket, bp.Name AS Supplier," 									//1..2
				+ " CONCAT(ROUND(rl.NettoI,2), '-', ROUND(rl.NettoII,2)) AS Netto," 												//3
				+ " CONCAT(ROUND(rl.ReflectionTicket,2), '(', ROUND(rl.ReflectionPercent,2), ')') AS ReflectionTicket,"					//4
				+ " CONCAT(ROUND(rl.ReflectionFlat,2), '(', ROUND(rl.ReflectionFlatPerc,2), ')') AS ReflectionFlat,"					//5
				+ " CONCAT(ROUND(rl.RevisionQty,2), '(', ROUND(rl.RevisionPercent,2), ')') AS Revision, rl.QtyPayment AS QtyPayment"	//6..7
				+ " FROM UNS_ReflectionRevision rev"
				+ " INNER JOIN UNS_ReflectionRevision_Line rl "
				+ "				ON rl.UNS_ReflectionRevision_ID = rev.UNS_ReflectionRevision_ID"
				+ " INNER JOIN UNS_WeighbridgeTicket wt"
				+ "				ON wt.UNS_WeighbridgeTicket_ID = rl.UNS_WeighbridgeTicket_ID"
				+ " INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = rl.C_BPartner_ID"
				+ " WHERE rl.UNS_ReflectionRevision_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			while(rs.next()){
				int count = 0;
				Object[] rowData = new Object[7];
				rowData[count] = rs.getObject("Ticket");
				rowData[++count] = rs.getObject("Supplier");
				rowData[++count] = rs.getObject("Netto");
				rowData[++count] = rs.getObject("ReflectionTicket");
				rowData[++count] = rs.getObject("ReflectionFlat");
				rowData[++count] = rs.getObject("Revision");
				rowData[++count] = rs.getObject("QtyPayment");
				
				list.add(rowData);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		if (docStatus.equals(DocAction.STATUS_Completed))
		{
			options[index++] = DocAction.ACTION_Void;
		}
		return index;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}