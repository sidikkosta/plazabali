/**
 * 
 */
package com.unicore.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.unicore.model.MUNSPOSConfigurationCurr;
import com.unicore.model.MUNSVoucherBook;
import com.unicore.model.MUNSVoucherCorrection;

/**
 * @author Burhani Adam
 *
 */
public class Doc_UNSVoucherCorrection extends Doc {

	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_UNSVoucherCorrection(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSVoucherCorrection m_VC = null;

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails()
	{
		m_VC = (MUNSVoucherCorrection) getPO();
		setDateAcct(m_VC.getDateAcct());
		setDateDoc(m_VC.getDateDoc());
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() {
		// TODO Auto-generated method stub
		return Env.ZERO;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		
		MUNSVoucherBook vb = new MUNSVoucherBook(getCtx(), m_VC.getUNS_VoucherCode().getUNS_VoucherBook_ID(), m_VC.get_TrxName());
		MAccount drAcct = MCharge.getAccount(vb.getC_Charge_ID(), as);
		MAccount crAcct = getAcct(as);
		FactLine dr = fact.createLine(null, drAcct, m_VC.getC_Currency_ID(), m_VC.getAllocatedAmt(), null);
		if(dr == null)
		{
			p_Error = "Dr not Created.";
			log.log(Level.WARNING, p_Error);
			return null;
		}
		dr.setAD_Org_ID(m_VC.getAD_Org_ID());
		
		FactLine cr = fact.createLine(null, crAcct, m_VC.getC_Currency_ID(), null, m_VC.getAllocatedAmt());
		if(cr == null)
		{
			p_Error = "Dr not Created.";
			log.log(Level.WARNING, p_Error);
			return null;
		}
		cr.setAD_Org_ID(m_VC.getAD_Org_ID());
		
		facts.add(fact);
		
		return facts;
	}
	
	private MAccount getAcct(MAcctSchema as)
	{
		MUNSPOSConfigurationCurr curr = MUNSPOSConfigurationCurr.get(
				getCtx(), m_VC.getAD_Org_ID(), m_VC.getC_Currency_ID(), getTrxName());
		
		if(curr == null || curr.getC_BankAccount_ID() <= 0)
		{
			p_Error = "Bank/Cash Account not set for currency " + m_VC.getC_Currency().getISO_Code();
			log.log(Level.WARNING, p_Error);
			return null;
		}
		
		String sql = "SELECT B_Asset_Acct FROM C_BankAccount_Acct WHERE C_BankAccount_ID=? AND C_AcctSchema_ID=?";
		int validCombination = DB.getSQLValue(getTrxName(), sql, curr.getC_BankAccount_ID(), as.get_ID());
		
		return new MAccount(getCtx(), validCombination, getTrxName());
	}
}