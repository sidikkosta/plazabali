/**
 * 
 */
package com.unicore.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.util.Env;

import com.unicore.model.MUNSStatementOfAccount;

/**
 * @author Burhani Adam
 *
 */
public class Doc_UNSStatementOfAccount extends Doc{

	public Doc_UNSStatementOfAccount(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSStatementOfAccount record = null;

	@Override
	protected String loadDocumentDetails()
	{
		record = (MUNSStatementOfAccount) getPO();
		setDateAcct(record.getDateAcct());
		setDateDoc(record.getDateAcct());
		return null;
	}

	@Override
	public BigDecimal getBalance() {
		// TODO Auto-generated method stub
		return Env.ZERO;
	}

	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		FactLine dr = null;
		FactLine cr = null;
		MAccount drAcct = MCharge.getAccount(record.getC_Charge_ID(), as);
		MAccount crAcct = null;
//		MUNSStatementOfAcctLine[] lines = record.getLines();
		
		if(record.getTotalLines().signum() != 0)
		{
			dr = fact.createLine(null, drAcct, record.getC_Currency_ID(), record.getTotalLines(), null);
//			if(as.getC_Currency_ID() != record.getC_Currency_ID())
//				dr.setAmtSourceDr(record.getTotalLines());
			dr.setAD_Org_ID(record.getAD_Org_ID());
			dr.setC_BPartner_ID(record.getC_BPartner_ID());
			dr.setDescription("Consignment Sales " + record.getDescription());
//			dr.setC_Currency_ID(as.getC_Currency_ID());
		}
		
		//TotalLines
		if(record.getGrandTotal().signum() != 0)
		{
			crAcct = getAccount(ACCTTYPE_ConsignmentProduct, as);
			cr = fact.createLine(null, crAcct, record.getC_Currency_ID(), null, record.getGrandTotal());
			cr.setAD_Org_ID(record.getAD_Org_ID());
			cr.setC_BPartner_ID(record.getC_BPartner_ID());
			cr.setDescription(record.getDescription());
//			cr.setC_Currency_ID(as.getC_Currency_ID());
		}
		
		//PushMoney
		if(record.getPushMoneyAmt().signum() != 0)
		{
			crAcct = getAccount(ACCTTYPE_PushMoney, as);
			cr = fact.createLine(null, crAcct, record.getC_Currency_ID(), null, record.getPushMoneyAmt());
			cr.setAD_Org_ID(record.getAD_Org_ID());
			cr.setC_BPartner_ID(record.getC_BPartner_ID());
			cr.setDescription("Push Money " + record.getDescription());
//			cr.setC_Currency_ID(as.getC_Currency_ID());
		}
		
		//VendorContribution
		if(record.getContributionAmt().signum() != 0)
		{	
			crAcct = getAccount(ACCTTYPE_VendorContribution, as);
			cr = fact.createLine(null, crAcct, record.getC_Currency_ID(), null, record.getContributionAmt());
			cr.setAD_Org_ID(record.getAD_Org_ID());
			cr.setC_BPartner_ID(record.getC_BPartner_ID());
			cr.setDescription("Contribution " + record.getDescription());
//			cr.setC_Currency_ID(as.getC_Currency_ID());
		}
		
		//ShippingCost
		if(record.getShippingCost().signum() != 0)
		{
			crAcct = getAccount(ACCTTYPE_ShippingCost, as);
			cr = fact.createLine(null, crAcct, record.getC_Currency_ID(), null, record.getShippingCost());
			cr.setAD_Org_ID(record.getAD_Org_ID());
			cr.setC_BPartner_ID(record.getC_BPartner_ID());
			cr.setDescription("Shipping Cost " + record.getDescription());
//			cr.setC_Currency_ID(as.getC_Currency_ID());
		}
		
		//OtherCost
		if(record.getOtherCharge_ID() > 0)
		{
			crAcct = MCharge.getAccount(record.getOtherCharge_ID(), as);
			cr = fact.createLine(null, crAcct, record.getC_Currency_ID(), null, record.getOtherCosts());
			cr.setAD_Org_ID(record.getAD_Org_ID());
			cr.setC_BPartner_ID(record.getC_BPartner_ID());
			cr.setDescription("Other Cost " + record.getOtherCostDesc());
//			cr.setC_Currency_ID(as.getC_Currency_ID());
		}
		
		facts.add(fact);
		
//		for(int i=0;i<lines.length;i++)
//		{
//			FactLine dr = null;
//			FactLine cr = null;
//			MAccount crAcct = null;
//			MAccount drAcct = null;
//			DocLine docLine = new DocLine(lines[i], this);
//			BigDecimal costs = lines[i].getPriceActual();
//			drAcct = docLine.getAccount(ProductCost.ACCTTYPE_P_Cogs, as);
//			crAcct = docLine.getAccount(ProductCost.ACCTTYPE_P_Asset, as);
//			docLine.setQty(lines[i].getQty(), true);
//			if (costs == null)
//			{
//				p_Error = "No Costs for " + lines[i].getM_Product().getName();
//				log.log(Level.WARNING, p_Error);
//				return null;
//			}
//			dr = fact.createLine(docLine, drAcct, record.getC_Currency_ID(), costs, null);
//			if (dr == null)
//			{
//				p_Error = "FactLine DR not created: " + lines[i];
//				log.log(Level.WARNING, p_Error);
//				return null;
//			}
//			dr.setAD_Org_ID(lines[i].getAD_Org_ID());
//			dr.setQty(lines[i].getQty());
//			
//			cr = fact.createLine(docLine, crAcct, record.getC_Currency_ID(), null, costs);
//			if (cr == null)
//			{
//				p_Error = "FactLine CR not created: " + lines[i];
//				log.log(Level.WARNING, p_Error);
//				return null;
//			}
//			cr.setAD_Org_ID(lines[i].getAD_Org_ID());
//			cr.setQty(lines[i].getQty());
//		}
		
		return facts;
	}
}