/**
 * 
 */
package com.unicore.model.acct;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.acct.Doc;
import org.compiere.acct.DocLine;
import org.compiere.acct.DocTax;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.model.MTax;
import org.compiere.model.ProductCost;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;

import com.unicore.model.MUNSCashReconciliation;
import com.unicore.model.MUNSEDCReconciliation;
import com.unicore.model.MUNSPOSConfiguration;
import com.unicore.model.MUNSPOSConfigurationCurr;
import com.unicore.model.MUNSPOSRecap;
import com.unicore.model.MUNSPOSRecapLine;
import com.unicore.model.MUNSPOSRecapLineMA;
import com.unicore.model.MUNSSalesReconciliation;
import com.unicore.model.MUNSVoucherBook;
import com.unicore.model.MUNSVoucherCode;
import com.uns.util.UNSApps;

/**
 * @author Burhani Adam
 *
 */
public class Doc_UNSSalesReconciliation extends Doc{

	public Doc_UNSSalesReconciliation(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSSalesReconciliation recon = null;
	@Override
	protected String loadDocumentDetails()
	{	
		recon = (MUNSSalesReconciliation) getPO();
		setDateAcct(recon.getDateTrx());
		setDateDoc(recon.getDateTrx());
		return null;
	}

	@Override
	public BigDecimal getBalance() {

		return Env.ZERO;
	}

	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		MUNSPOSConfiguration config = MUNSPOSConfiguration.getActive(getTrxName(), recon.getAD_Org_ID());
		
		MUNSPOSRecap[] recaps = recon.getRecap();
		String sql1 = "SELECT C_BPartner_ID FROM C_BPartner WHERE Value = ?";
		int bp = DB.getSQLValue(getTrxName(), sql1, recon.getC_BP_Group().getValue());
		
		MUNSCashReconciliation[] cash = recon.getCash();
		for(int i=0;i<cash.length;i++)
		{
			FactLine dr = null;
			DocLine docLine = new DocLine(cash[i], this);
			if(cash[i].getCashDepositAmtB1().signum() > 0)
			{
				MAccount crAcct = getAcct(cash[i], as);
				dr = fact.createLine(docLine, crAcct, cash[i].getC_Currency_ID(),
						cash[i].getCashDepositAmt(), null);
				dr.setAD_Org_ID(cash[i].getAD_Org_ID());
				dr.setDescription("Cash " + crAcct.getAccount().getName());
				if(bp > 0)
					dr.setC_BPartner_ID(bp);
			}
			
			if(cash[i].getPayableRefundAmt().signum() > 0)
			{
				MAccount drAcct = getAccount(Doc.ACCTTYPE_CashRefundPay, as);
				dr = fact.createLine(docLine, drAcct, cash[i].getC_Currency_ID(),
						cash[i].getPayableRefundAmt(), null);
				dr.setAD_Org_ID(cash[i].getAD_Org_ID());
				dr.setDescription("Refund " + drAcct.getAccount().getName());
				if(bp > 0)
					dr.setC_BPartner_ID(bp);
			}
		}
		
		MUNSEDCReconciliation[] edc = recon.getEDC();
		for(int i=0;i<edc.length;i++)
		{
			FactLine cr = null;
			DocLine docLine = new DocLine(edc[i], this);
			docLine.setDateAcct(recon.getDateTrx());
			if(edc[i].getTotalAmt().signum() > 0)
			{
				String sql = "SELECT EDCAR_Acct FROM UNS_CardType WHERE UNS_CardType_ID = ?";
				int validComb = DB.getSQLValue(getTrxName(), sql, edc[i].getUNS_CardType_ID());
				MAccount crAcct = MAccount.get(getCtx(), validComb); 
				cr = fact.createLine(docLine, crAcct, recon.getC_Currency_ID(),
						edc[i].getTotalAmt(), null);
				cr.setAD_Org_ID(edc[i].getAD_Org_ID());
				cr.setDescription("Card " + DB.getSQLValueString(getTrxName(), "SELECT Name FROM UNS_CardType WHERE UNS_CardType_ID = ?",
						edc[i].getUNS_CardType_ID()));
				if(bp > 0)
					cr.setC_BPartner_ID(bp);
			}
			
			if(edc[i].getPayableRefundAmt().signum() > 0)
			{
				MAccount crAcct = getAccount(Doc.ACCTTYPE_EDCRefundPay, as);
				cr = fact.createLine(docLine, crAcct, recon.getC_Currency_ID(), null,
						edc[i].getPayableRefundAmt());
				cr.setAD_Org_ID(edc[i].getAD_Org_ID());
				cr.setDescription("Card " + DB.getSQLValueString(getTrxName(), "SELECT Name FROM UNS_CardType WHERE UNS_CardType_ID = ?",
						edc[i].getUNS_CardType_ID()) + " - " + crAcct.getAccount().getName());
				if(bp > 0)
					cr.setC_BPartner_ID(bp);
			}
		}
		
		Timestamp endOfNov2019 = TimeUtil.getDay(2019, 11, 30);
		boolean isAfterEndOfNov2019 = getDateAcct().after(endOfNov2019);
		
		for(int i = 0; i < recaps.length; i++)
		{
			Hashtable<String, BigDecimal> mapAcct = new Hashtable<>();
			setC_BPartner_ID(recaps[i].getStore_ID());
			String store = recaps[i].getStore().getValue();
			DocLine docLine = null;
			MUNSPOSRecapLine[] lines = getLines(recaps[i]);
			FactLine dr = null;
			FactLine cr = null;
			MAccount crAcct = null;
			MAccount drAcct = null;
			for(int j=0;j<lines.length;j++)
			{
				MUNSPOSRecapLineMA[] mas = lines[j].getMAs();
				for(int k=0;k<mas.length;k++)
				{
					docLine = new DocLine(lines[j], this);
					docLine.setM_Locator_ID(mas[k].getM_Locator_ID());
					BigDecimal costs = null;
					if(!lines[j].isConsignment())
					{
						docLine.setM_AttributesetInstance_ID(mas[k].getM_AttributeSetInstance_ID());
						docLine.setQty(mas[k].getQty(), true);
						costs = docLine.getProductCosts(as, lines[j].getAD_Org_ID(), false, "UNS_POSRecapLine_ID=?");
						crAcct = docLine.getAccount(ProductCost.ACCTTYPE_P_Asset, as);
					}
					else
					{
						crAcct = getAccount(ACCTTYPE_ConsignmentProduct, as);
						costs = (lines[j].getCostAmt().divide(lines[j].getQty())).multiply(mas[k].getQty());
					}
					
					drAcct = docLine.getAccount(ProductCost.ACCTTYPE_P_Cogs, as);
					
					if (costs == null && isAfterEndOfNov2019)
					{
						if(p_Error == null)
							p_Error = "No Costs on";
						
						p_Error +=  " (" + store + "-" + lines[j].getM_Product().getValue() +
								"-" + mas[k].getM_AttributeSetInstance().getDescription() + ")";
//						log.log(Level.WARNING, p_Error);
//						return null;
					}
					else if (costs ==null)
						continue;
					
					if(p_Error != null)
						continue;
					String key = crAcct.getC_ValidCombination_ID() + "-" + "C";
					BigDecimal costCr = mapAcct.get(key);
					if(costCr == null)
						costCr = Env.ZERO;
					mapAcct.put(key, costs.add(costCr));
					
					key = drAcct.getC_ValidCombination_ID() + "-" + "D"; 
					BigDecimal costDr = mapAcct.get(key);
					if(costDr == null)
						costDr = Env.ZERO;
					mapAcct.put(key, costs.add(costDr));
				}
			}
			
			if(p_Error != null)
				continue;
			
			Enumeration<String> mapKey = mapAcct.keys();
			while (mapKey.hasMoreElements())
			{
				String key = mapKey.nextElement();
				if(key.endsWith("D"))
				{
					int idCombination = Integer.parseInt(key.replace("-D", ""));
					BigDecimal costs = mapAcct.get(key);
					dr = fact.createLine(docLine, MAccount.get(getCtx(), idCombination), recon.getC_Currency_ID(), costs, null);
					if (dr == null)
					{
						p_Error = "FactLine DR not created.";
						log.log(Level.WARNING, p_Error);
						return null;
					}
					dr.setAD_Org_ID(recaps[i].getAD_Org_ID());
					dr.setDescription("COGS - " + MAccount.get(getCtx(), idCombination).getAccount().getName());
				}
				else
				{
					int idCombination = Integer.parseInt(key.replace("-C", ""));
					BigDecimal costs = mapAcct.get(key);
					if(costs == null || costs.compareTo(Env.ZERO) == 0)
						continue;
					cr = fact.createLine(docLine, MAccount.get(getCtx(), idCombination), recon.getC_Currency_ID(), null, costs);
					if (cr == null)
					{
						p_Error = "FactLine CR not created.";
						log.log(Level.WARNING, CLogger.retrieveErrorString(p_Error));
						return null;
					}
					cr.setAD_Org_ID(recaps[i].getAD_Org_ID());
					cr.setDescription("COGS - " + MAccount.get(getCtx(), idCombination).getAccount().getName());
				}
			}
			
			docLine = new DocLine(recaps[i], this);
			int receivables_ID = getValidCombination_ID(Doc.ACCTTYPE_C_Receivable, as);
			MAccount acct = MAccount.get(getCtx(), receivables_ID);
			String sql = "SELECT SUM(p.DiscountAmt) FROM UNS_POSTrx p"
						+ " WHERE p.DateAcct = ? AND p.C_BPartner_ID = ?";
			BigDecimal discHeaderPOS = DB.getSQLValueBD(getTrxName(), sql, recon.getDateTrx(), recaps[i].getStore_ID());
			if(discHeaderPOS == null)
				discHeaderPOS = Env.ZERO;
			BigDecimal revenue =(recaps[i].getTotalRevenueAmt().add(recaps[i].getTotalDiscAmt()))
					.subtract(recaps[i].getTotalTaxAmt()).subtract(recaps[i].getTotalServiceChargeAmt())
					.subtract(discHeaderPOS);
			cr = fact.createLine(docLine, acct, recon.getC_Currency_ID(),
					null, revenue);
			cr.setAD_Org_ID(recaps[i].getAD_Org_ID());
			cr.setDescription("Revenue - " + acct.getAccount().getName());
			
			if(recaps[i].getTotalDiscAmt().compareTo(Env.ZERO) != 0)
			{
				drAcct = getAccount(Doc.ACCTTYPE_C_TradeDiscountGrant, as);
				dr = fact.createLine(docLine, drAcct, recon.getC_Currency_ID(), recaps[i].getTotalDiscAmt(), null);
				dr.setDescription("Discount - " + drAcct.getAccount().getName());
			}
			if(recaps[i].getTotalServiceChargeAmt().compareTo(Env.ZERO) != 0)
			{
				crAcct = getAccount(Doc.ACCTTYPE_ServiceCharge, as);
				cr = fact.createLine(docLine, crAcct, recon.getC_Currency_ID(), null, recaps[i].getTotalServiceChargeAmt());
				cr.setDescription("Service Charge - " + crAcct.getAccount().getName());
			}
			if(recaps[i].getTotalTaxAmt().compareTo(Env.ZERO) != 0)
			{
				MTax tax = MTax.get(getCtx(), config.getC_Tax_ID());
				DocTax docTax = new DocTax(tax.get_ID(), tax.getName(), tax.getRate(),
						Env.ZERO, Env.ZERO, true);
				crAcct = docTax.getAccount(DocTax.ACCTTYPE_TaxDue, as);
				cr = fact.createLine(docLine, crAcct, recon.getC_Currency_ID(), null, recaps[i].getTotalTaxAmt());
				cr.setDescription("Tax - " + crAcct.getAccount().getName());
			}
			
			if(recaps[i].getTotalVoucherAmt().compareTo(Env.ZERO) != 0)
			{
				String __sql = "SELECT ConvertedAmt, TrxNo,(SELECT DateAcct FROM UNS_POSPayment WHERE "
						+ " UNS_POSPayment_ID = UNS_PaymentTrx.UNS_POSPayment_ID), C_Currency_ID FROM UNS_PaymentTrx "
						+ " WHERE IsActive = ? AND PaymentMethod = ? AND UNS_POS_Session_ID IN (SELECT "
						+ " UNS_POS_Session_ID FROM UNS_POSTrx WHERE UNS_POSTrx_ID IN (SELECT UNS_POSTrx_ID "
						+ " FROM UNS_POSTrxLine WHERE UNS_POSRecapLine_ID IN (SELECT UNS_POSRecapLine_ID "
						+ " FROM UNS_POSRecapLine WHERE UNS_POSRecap_ID = ?)))";
				List<Object[]> accounts = new ArrayList<>();
				List<List<Object>> vouchers = DB.getSQLArrayObjectsEx(
						getTrxName(), __sql, "Y", "3", recaps[i].getUNS_POSRecap_ID());
				for (int j=0; j<vouchers.size(); j++)
				{
					BigDecimal amount = (BigDecimal) vouchers.get(j).get(0);
					String voucherCode = (String) vouchers.get(j).get(1);
					Timestamp date = TimeUtil.trunc((Timestamp) vouchers.get(j).get(2), TimeUtil.TRUNC_DAY);
					int currencyID = Integer.valueOf(vouchers.get(j).get(3).toString());
					MUNSVoucherCode vCodeModel = MUNSVoucherCode.getCode(
							getCtx(), date, voucherCode, currencyID, getTrxName());
					Object[] row = null;
					for (int k=0; k<accounts.size(); k++)
					{
						if (vCodeModel == null && voucherCode.equals(accounts.get(k)[0]))
						{
							row = accounts.get(k);
							break;
						}
						
						int vchBookID = vCodeModel.getUNS_VoucherBook_ID();
						if (vchBookID == (int)accounts.get(k)[0])
						{
							row = accounts.get(k);
							break;
						}
					}
					
					if (row == null)
					{
						row = new Object[3];
						MAccount account = null;
						if(vCodeModel == null)
						{
							account = MAccount.get(getCtx(), UNSApps.getRefAsInt(UNSApps.ACCT_CashVoucher));
							row[0] = voucherCode;
						}
						else {
							MUNSVoucherBook book = vCodeModel.getParent();
							row[0] = book.getUNS_VoucherBook_ID();
							if (book.isPostDocument())
								account = MAccount.get(getCtx(), UNSApps.getRefAsInt(UNSApps.ACCT_CashVoucher));
							else
								account = MCharge.getAccount(book.getC_Charge_ID(), as);
						}
						row[1] = account;
						row[2] = Env.ZERO;
						accounts.add(row);
					}
					
					amount = ((BigDecimal) row[2]).add(amount);
					row[2] = amount;
				}
				
				for (int j=0; j<accounts.size(); j++)
				{
					MAccount account = (MAccount) accounts.get(j)[1];
					BigDecimal amount = (BigDecimal) accounts.get(j)[2];
					dr = fact.createLine(docLine, account, recon.getC_Currency_ID(), amount);
					if (dr == null)
					{
						p_Error = "DR not created!!";
						return null;
					}
					dr.setDescription("Voucher - " + account.getAccount().getName());
				}
			}
			
			if(recaps[i].getTotalRoundingAmt().signum() > 0)
			{
				crAcct = getAccount(Doc.ACCTTYPE_POSRounding, as);
				cr = fact.createLine(docLine, crAcct, recon.getC_Currency_ID(), null, recaps[i].getTotalRoundingAmt());
				cr.setDescription("Rounding (+) - " + crAcct.getAccount().getName());
			}
			
			if(recaps[i].getTotalShortCashierAmt1().signum() != 0)
			{
				crAcct = getAccount(Doc.ACCTTYPE_ShortCashierCredit, as);
				cr = fact.createLine(docLine, crAcct, recon.getC_Currency_ID(),
						null, recaps[i].getTotalShortCashierAmt1());
				cr.setAD_Org_ID(recaps[i].getAD_Org_ID());
				cr.setDescription("Short Cashier - " + crAcct.getAccount().getName());
			}
			if(recaps[i].getTotalShortCashierAmt().signum() != 0)
			{
				drAcct = getAccount(Doc.ACCTTYPE_ShortCashierDebit, as);
				dr = fact.createLine(docLine, drAcct, recon.getC_Currency_ID(),
						recaps[i].getTotalShortCashierAmt(), null);
				dr.setAD_Org_ID(recaps[i].getAD_Org_ID());
				dr.setDescription("Excess Cashier - " + drAcct.getAccount().getName());
			}
			if(recaps[i].getTotalEstimationShortCashierAmt().compareTo(Env.ZERO) != 0)
			{
				drAcct = getAccount(Doc.ACCTTYPE_EstimationShortCashier, as);
				dr = fact.createLine(docLine, drAcct, recon.getC_Currency_ID(), recaps[i].getTotalEstimationShortCashierAmt(), null);
				dr.setDescription("Sales");
			}
			if(recaps[i].getTotalRoundingAmt().signum() < 0)
			{
				drAcct = getAccount(Doc.ACCTTYPE_POSRounding, as);
				dr = fact.createLine(docLine, drAcct, recon.getC_Currency_ID(), recaps[i].getTotalRoundingAmt(), null);
				dr.setDescription("Rounding (-) - " + drAcct.getAccount().getName());
			}
		}
		
		if(p_Error != null && isAfterEndOfNov2019)
		{
			log.log(Level.WARNING, p_Error);
			return null;
		}
		
		p_Error = recon.createTransaction(as);
		if (p_Error != null)
			return null;
		
		facts.add(fact);
		return facts;
	}
	
	private MUNSPOSRecapLine[] getLines(MUNSPOSRecap recap)
	{
		List<MUNSPOSRecapLine> list = new ArrayList<>();
		
		String sql = "SELECT prl.* FROM UNS_POSRecapLine prl"
				+ " WHERE prl.UNS_POSRecap_ID = ?";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try
		{
			stmt = DB.prepareStatement(sql, getTrxName());
			stmt.setInt(1, recap.get_ID());
			rs = stmt.executeQuery();
			while (rs.next())
			{
				list.add(new MUNSPOSRecapLine(getCtx(), rs, getTrxName()));
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		finally
		{
			DB.close(rs, stmt);
		}
		
		return list.toArray(new MUNSPOSRecapLine[list.size()]);
	}
	
	private MAccount getAcct(MUNSCashReconciliation cash, MAcctSchema as)
	{
		MUNSPOSConfigurationCurr curr = MUNSPOSConfigurationCurr.get(
				getCtx(), cash.getAD_Org_ID(), cash.getC_Currency_ID(), getTrxName());
		
		if(curr == null || curr.getC_BankAccount_ID() <= 0)
		{
			p_Error = "Bank/Cash Account not set for currency " + cash.getC_Currency().getISO_Code();
			log.log(Level.WARNING, p_Error);
			return null;
		}
		
		String sql = "SELECT B_Asset_Acct FROM C_BankAccount_Acct WHERE C_BankAccount_ID=? AND C_AcctSchema_ID=?";
		int validCombination = DB.getSQLValue(getTrxName(), sql, curr.getC_BankAccount_ID(), as.get_ID());
		
		return new MAccount(getCtx(), validCombination, getTrxName());
	}
}