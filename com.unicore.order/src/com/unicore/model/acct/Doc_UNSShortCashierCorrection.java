/**
 * 
 */
package com.unicore.model.acct;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.compiere.acct.Doc;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCurrency;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MBPartner;
import com.unicore.model.MUNSPOSConfigurationCurr;
import com.unicore.model.MUNSShortCashierCorrection;

/**
 * @author Burhani Adam
 *
 */
public class Doc_UNSShortCashierCorrection extends Doc{

	public Doc_UNSShortCashierCorrection(MAcctSchema as, Class<?> clazz,
			ResultSet rs, String defaultDocumentType, String trxName) {
		super(as, clazz, rs, defaultDocumentType, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSShortCashierCorrection m_PO = null;

	@Override
	protected String loadDocumentDetails()
	{
		m_PO = (MUNSShortCashierCorrection) getPO();
		setDateAcct(m_PO.getDateDoc());
		setDateDoc(m_PO.getDateDoc());
		setC_BPartner_ID(MBPartner.get(getCtx(), m_PO.getC_BP_Group().getValue()).get_ID());
		return null;
	}

	@Override
	public BigDecimal getBalance() {
		// TODO Auto-generated method stub
		return Env.ZERO;
	}

	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as)
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		
		String sql = "SELECT Base1Currency_ID, Base2Currency_ID, SUM(CorrectionAmtB1), SUM(CorrectionAmtB2),"
				+ " SUM(OriginalAmtB1), SUM(OriginalAmtB2)"
				+ " FROM UNS_ShortCashierCorr_Line WHERE UNS_ShortCashierCorrection_ID = ?"
				+ " GROUP BY Base1Currency_ID, Base2Currency_ID";
		List<Object> oo = DB.getSQLValueObjectsEx(getTrxName(), sql, m_PO.get_ID());
		int currencyB1 = ((BigDecimal) oo.get(0)).intValue();
		int currencyB2 = ((BigDecimal) oo.get(1)).intValue();
		BigDecimal amtB1 = (BigDecimal) oo.get(2);
		amtB1 = amtB1.subtract((BigDecimal) oo.get(4));
		BigDecimal amtB2 = (BigDecimal) oo.get(3);
		amtB2 = amtB2.subtract((BigDecimal) oo.get(5));
		MAccount drAcct = null;
		FactLine dr = null;
		MAccount crAcct = getAccount(Doc.ACCTTYPE_ShortCashierDebit, as);
		FactLine cr = null;
		if(currencyB1 == as.getC_Currency_ID() && amtB1.signum() != 0)
		{	
			drAcct = getAcct(m_PO, currencyB1, as);
			dr = fact.createLine(null, drAcct, currencyB1, null, amtB1);
			dr.setAmtSourceDr(amtB2);
			cr = fact.createLine(null, crAcct, currencyB1, amtB1, null);
			cr.setAmtSourceCr(amtB2);
		}
		else if(currencyB2 == as.getC_Currency_ID() && amtB2.signum() != 0)
		{	
			drAcct = getAcct(m_PO, currencyB2, as);
			dr = fact.createLine(null, drAcct, currencyB2, null, amtB2);
			dr.setAmtSourceDr(amtB1);
			cr = fact.createLine(null, crAcct, currencyB2, amtB2, null);
			cr.setAmtSourceCr(amtB1);
		}
		
		facts.add(fact);
		return facts;
	}
	
	private MAccount getAcct(MUNSShortCashierCorrection docs, int C_Currency_ID, MAcctSchema as)
	{
		MCurrency currency = MCurrency.get(getCtx(), C_Currency_ID);
		MUNSPOSConfigurationCurr curr = MUNSPOSConfigurationCurr.get(
				getCtx(), docs.getAD_Org_ID(), currency.get_ID(), getTrxName());
		
		if(curr == null || curr.getC_BankAccount_ID() <= 0)
		{
			p_Error = "Bank/Cash Account not set for currency " + currency.getISO_Code();
			log.log(Level.WARNING, p_Error);
			return null;
		}
		
		String sql = "SELECT B_Asset_Acct FROM C_BankAccount_Acct WHERE C_BankAccount_ID=? AND C_AcctSchema_ID=?";
		int validCombination = DB.getSQLValue(getTrxName(), sql, curr.getC_BankAccount_ID(), as.get_ID());
		
		return new MAccount(getCtx(), validCombination, getTrxName());
	}
}