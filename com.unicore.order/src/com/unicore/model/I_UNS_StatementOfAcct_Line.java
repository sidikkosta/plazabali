/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_StatementOfAcct_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_StatementOfAcct_Line 
{

    /** TableName=UNS_StatementOfAcct_Line */
    public static final String Table_Name = "UNS_StatementOfAcct_Line";

    /** AD_Table_ID=1000423 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AdjustmentQty */
    public static final String COLUMNNAME_AdjustmentQty = "AdjustmentQty";

	/** Set Adjustment Qty	  */
	public void setAdjustmentQty (BigDecimal AdjustmentQty);

	/** Get Adjustment Qty	  */
	public BigDecimal getAdjustmentQty();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BeginningStock */
    public static final String COLUMNNAME_BeginningStock = "BeginningStock";

	/** Set Beginning Stock	  */
	public void setBeginningStock (BigDecimal BeginningStock);

	/** Get Beginning Stock	  */
	public BigDecimal getBeginningStock();

    /** Column name ConversionQty */
    public static final String COLUMNNAME_ConversionQty = "ConversionQty";

	/** Set Conversion Qty	  */
	public void setConversionQty (BigDecimal ConversionQty);

	/** Get Conversion Qty	  */
	public BigDecimal getConversionQty();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name EndingStock */
    public static final String COLUMNNAME_EndingStock = "EndingStock";

	/** Set Ending Stock	  */
	public void setEndingStock (BigDecimal EndingStock);

	/** Get Ending Stock	  */
	public BigDecimal getEndingStock();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name MovementQty */
    public static final String COLUMNNAME_MovementQty = "MovementQty";

	/** Set Movement Quantity.
	  * Quantity of a product moved.
	  */
	public void setMovementQty (BigDecimal MovementQty);

	/** Get Movement Quantity.
	  * Quantity of a product moved.
	  */
	public BigDecimal getMovementQty();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name PriceActual */
    public static final String COLUMNNAME_PriceActual = "PriceActual";

	/** Set Unit Price.
	  * Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual);

	/** Get Unit Price.
	  * Actual Price
	  */
	public BigDecimal getPriceActual();

    /** Column name PushMoneyAmt */
    public static final String COLUMNNAME_PushMoneyAmt = "PushMoneyAmt";

	/** Set Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt);

	/** Get Push Money Amount	  */
	public BigDecimal getPushMoneyAmt();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name ReceiptQty */
    public static final String COLUMNNAME_ReceiptQty = "ReceiptQty";

	/** Set Receipt Qty	  */
	public void setReceiptQty (BigDecimal ReceiptQty);

	/** Get Receipt Qty	  */
	public BigDecimal getReceiptQty();

    /** Column name ReturnedQty */
    public static final String COLUMNNAME_ReturnedQty = "ReturnedQty";

	/** Set Returned Quantity	  */
	public void setReturnedQty (BigDecimal ReturnedQty);

	/** Get Returned Quantity	  */
	public BigDecimal getReturnedQty();

    /** Column name SellingPrice */
    public static final String COLUMNNAME_SellingPrice = "SellingPrice";

	/** Set Selling Price	  */
	public void setSellingPrice (BigDecimal SellingPrice);

	/** Get Selling Price	  */
	public BigDecimal getSellingPrice();

    /** Column name TotalLines */
    public static final String COLUMNNAME_TotalLines = "TotalLines";

	/** Set Total Lines.
	  * Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines);

	/** Get Total Lines.
	  * Total of all document lines
	  */
	public BigDecimal getTotalLines();

    /** Column name UNS_StatementOfAccount_ID */
    public static final String COLUMNNAME_UNS_StatementOfAccount_ID = "UNS_StatementOfAccount_ID";

	/** Set Statement Of Account	  */
	public void setUNS_StatementOfAccount_ID (int UNS_StatementOfAccount_ID);

	/** Get Statement Of Account	  */
	public int getUNS_StatementOfAccount_ID();

	public com.unicore.model.I_UNS_StatementOfAccount getUNS_StatementOfAccount() throws RuntimeException;

    /** Column name UNS_StatementOfAcct_Line_ID */
    public static final String COLUMNNAME_UNS_StatementOfAcct_Line_ID = "UNS_StatementOfAcct_Line_ID";

	/** Set Statement Of Account Lines	  */
	public void setUNS_StatementOfAcct_Line_ID (int UNS_StatementOfAcct_Line_ID);

	/** Get Statement Of Account Lines	  */
	public int getUNS_StatementOfAcct_Line_ID();

    /** Column name UNS_StatementOfAcct_Line_UU */
    public static final String COLUMNNAME_UNS_StatementOfAcct_Line_UU = "UNS_StatementOfAcct_Line_UU";

	/** Set UNS_StatementOfAcct_Line_UU	  */
	public void setUNS_StatementOfAcct_Line_UU (String UNS_StatementOfAcct_Line_UU);

	/** Get UNS_StatementOfAcct_Line_UU	  */
	public String getUNS_StatementOfAcct_Line_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
