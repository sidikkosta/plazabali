/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_SessionEDCSummary
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_SessionEDCSummary extends PO implements I_UNS_SessionEDCSummary, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180925L;

    /** Standard Constructor */
    public X_UNS_SessionEDCSummary (Properties ctx, int UNS_SessionEDCSummary_ID, String trxName)
    {
      super (ctx, UNS_SessionEDCSummary_ID, trxName);
      /** if (UNS_SessionEDCSummary_ID == 0)
        {
			setPayableRefundAmt (Env.ZERO);
// 0
			setPayableRefundAmtB1 (Env.ZERO);
// 0
			setUNS_CardType_ID (0);
			setUNS_EDC_ID (0);
			setUNS_POS_Session_ID (0);
			setUNS_SessionEDCSummary_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_SessionEDCSummary (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_SessionEDCSummary[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Payable Refund Amount.
		@param PayableRefundAmt 
		The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt)
	{
		set_Value (COLUMNNAME_PayableRefundAmt, PayableRefundAmt);
	}

	/** Get Payable Refund Amount.
		@return The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payable Refund Amount B1.
		@param PayableRefundAmtB1 
		The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB1 (BigDecimal PayableRefundAmtB1)
	{
		set_Value (COLUMNNAME_PayableRefundAmtB1, PayableRefundAmtB1);
	}

	/** Get Payable Refund Amount B1.
		@return The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payable Refund Amount B2.
		@param PayableRefundAmtB2 
		The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB2 (BigDecimal PayableRefundAmtB2)
	{
		set_Value (COLUMNNAME_PayableRefundAmtB2, PayableRefundAmtB2);
	}

	/** Get Payable Refund Amount B2.
		@return The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Amount.
		@param TotalAmt 
		Total Amount
	  */
	public void setTotalAmt (BigDecimal TotalAmt)
	{
		set_ValueNoCheck (COLUMNNAME_TotalAmt, TotalAmt);
	}

	/** Get Total Amount.
		@return Total Amount
	  */
	public BigDecimal getTotalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Amount B1.
		@param TotalAmtB1 Total Amount B1	  */
	public void setTotalAmtB1 (BigDecimal TotalAmtB1)
	{
		set_Value (COLUMNNAME_TotalAmtB1, TotalAmtB1);
	}

	/** Get Total Amount B1.
		@return Total Amount B1	  */
	public BigDecimal getTotalAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Amount B2.
		@param TotalAmtB2 Total Amount B2	  */
	public void setTotalAmtB2 (BigDecimal TotalAmtB2)
	{
		set_Value (COLUMNNAME_TotalAmtB2, TotalAmtB2);
	}

	/** Get Total Amount B2.
		@return Total Amount B2	  */
	public BigDecimal getTotalAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_CardType getUNS_CardType() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_CardType)MTable.get(getCtx(), com.unicore.model.I_UNS_CardType.Table_Name)
			.getPO(getUNS_CardType_ID(), get_TrxName());	}

	/** Set Card Type.
		@param UNS_CardType_ID Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID)
	{
		if (UNS_CardType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CardType_ID, Integer.valueOf(UNS_CardType_ID));
	}

	/** Get Card Type.
		@return Card Type	  */
	public int getUNS_CardType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CardType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_EDC getUNS_EDC() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_EDC)MTable.get(getCtx(), com.unicore.model.I_UNS_EDC.Table_Name)
			.getPO(getUNS_EDC_ID(), get_TrxName());	}

	/** Set EDC.
		@param UNS_EDC_ID EDC	  */
	public void setUNS_EDC_ID (int UNS_EDC_ID)
	{
		if (UNS_EDC_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_EDC_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_EDC_ID, Integer.valueOf(UNS_EDC_ID));
	}

	/** Get EDC.
		@return EDC	  */
	public int getUNS_EDC_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_EDC_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDC Session Summary.
		@param UNS_SessionEDCSummary_ID EDC Session Summary	  */
	public void setUNS_SessionEDCSummary_ID (int UNS_SessionEDCSummary_ID)
	{
		if (UNS_SessionEDCSummary_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SessionEDCSummary_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SessionEDCSummary_ID, Integer.valueOf(UNS_SessionEDCSummary_ID));
	}

	/** Get EDC Session Summary.
		@return EDC Session Summary	  */
	public int getUNS_SessionEDCSummary_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SessionEDCSummary_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_SessionEDCSummary_UU.
		@param UNS_SessionEDCSummary_UU UNS_SessionEDCSummary_UU	  */
	public void setUNS_SessionEDCSummary_UU (String UNS_SessionEDCSummary_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_SessionEDCSummary_UU, UNS_SessionEDCSummary_UU);
	}

	/** Get UNS_SessionEDCSummary_UU.
		@return UNS_SessionEDCSummary_UU	  */
	public String getUNS_SessionEDCSummary_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_SessionEDCSummary_UU);
	}
}