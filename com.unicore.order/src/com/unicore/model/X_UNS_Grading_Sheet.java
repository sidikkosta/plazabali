/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_Grading_Sheet
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_Grading_Sheet extends PO implements I_UNS_Grading_Sheet, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20151030L;

    /** Standard Constructor */
    public X_UNS_Grading_Sheet (Properties ctx, int UNS_Grading_Sheet_ID, String trxName)
    {
      super (ctx, UNS_Grading_Sheet_ID, trxName);
      /** if (UNS_Grading_Sheet_ID == 0)
        {
			setDeductionAmt (Env.ZERO);
// 0
			setUNS_Grading_ID (0);
			setUNS_Grading_Sheet_ID (0);
			setUNS_WeighbridgeTicket_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_Grading_Sheet (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_Grading_Sheet[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		throw new IllegalArgumentException ("C_UOM_ID is virtual column");	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Deduction Amount.
		@param DeductionAmt 
		The amount to be deducted to the original (total) calculation
	  */
	public void setDeductionAmt (BigDecimal DeductionAmt)
	{
		set_Value (COLUMNNAME_DeductionAmt, DeductionAmt);
	}

	/** Get Deduction Amount.
		@return The amount to be deducted to the original (total) calculation
	  */
	public BigDecimal getDeductionAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DeductionAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		throw new IllegalArgumentException ("M_Product_ID is virtual column");	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_Grading getUNS_Grading() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_Grading)MTable.get(getCtx(), com.unicore.model.I_UNS_Grading.Table_Name)
			.getPO(getUNS_Grading_ID(), get_TrxName());	}

	/** Set Grading Component.
		@param UNS_Grading_ID Grading Component	  */
	public void setUNS_Grading_ID (int UNS_Grading_ID)
	{
		if (UNS_Grading_ID < 1) 
			set_Value (COLUMNNAME_UNS_Grading_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Grading_ID, Integer.valueOf(UNS_Grading_ID));
	}

	/** Get Grading Component.
		@return Grading Component	  */
	public int getUNS_Grading_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Grading_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Grading Sheet.
		@param UNS_Grading_Sheet_ID Grading Sheet	  */
	public void setUNS_Grading_Sheet_ID (int UNS_Grading_Sheet_ID)
	{
		if (UNS_Grading_Sheet_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Grading_Sheet_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Grading_Sheet_ID, Integer.valueOf(UNS_Grading_Sheet_ID));
	}

	/** Get Grading Sheet.
		@return Grading Sheet	  */
	public int getUNS_Grading_Sheet_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Grading_Sheet_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_Grading_Sheet_UU.
		@param UNS_Grading_Sheet_UU UNS_Grading_Sheet_UU	  */
	public void setUNS_Grading_Sheet_UU (String UNS_Grading_Sheet_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_Grading_Sheet_UU, UNS_Grading_Sheet_UU);
	}

	/** Get UNS_Grading_Sheet_UU.
		@return UNS_Grading_Sheet_UU	  */
	public String getUNS_Grading_Sheet_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_Grading_Sheet_UU);
	}

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_WeighbridgeTicket)MTable.get(getCtx(), com.unicore.model.I_UNS_WeighbridgeTicket.Table_Name)
			.getPO(getUNS_WeighbridgeTicket_ID(), get_TrxName());	}

	/** Set Weighbridge Ticket.
		@param UNS_WeighbridgeTicket_ID Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID)
	{
		if (UNS_WeighbridgeTicket_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_WeighbridgeTicket_ID, Integer.valueOf(UNS_WeighbridgeTicket_ID));
	}

	/** Get Weighbridge Ticket.
		@return Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_WeighbridgeTicket_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}