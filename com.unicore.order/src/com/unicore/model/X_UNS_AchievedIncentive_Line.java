/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_AchievedIncentive_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_AchievedIncentive_Line extends PO implements I_UNS_AchievedIncentive_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150126L;

    /** Standard Constructor */
    public X_UNS_AchievedIncentive_Line (Properties ctx, int UNS_AchievedIncentive_Line_ID, String trxName)
    {
      super (ctx, UNS_AchievedIncentive_Line_ID, trxName);
      /** if (UNS_AchievedIncentive_Line_ID == 0)
        {
			setUNS_AchievedIncentive_ID (0);
			setUNS_AchievedIncentive_Line_ID (0);
			setUNS_AcvIncentiveByPeriod_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_AchievedIncentive_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_AchievedIncentive_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_PaymentAllocate getC_PaymentAllocate() throws RuntimeException
    {
		return (org.compiere.model.I_C_PaymentAllocate)MTable.get(getCtx(), org.compiere.model.I_C_PaymentAllocate.Table_Name)
			.getPO(getC_PaymentAllocate_ID(), get_TrxName());	}

	/** Set Allocate Payment.
		@param C_PaymentAllocate_ID 
		Allocate Payment to Invoices
	  */
	public void setC_PaymentAllocate_ID (int C_PaymentAllocate_ID)
	{
		if (C_PaymentAllocate_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_PaymentAllocate_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_PaymentAllocate_ID, Integer.valueOf(C_PaymentAllocate_ID));
	}

	/** Get Allocate Payment.
		@return Allocate Payment to Invoices
	  */
	public int getC_PaymentAllocate_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_PaymentAllocate_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_AchievedIncentive getUNS_AchievedIncentive() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_AchievedIncentive)MTable.get(getCtx(), com.unicore.model.I_UNS_AchievedIncentive.Table_Name)
			.getPO(getUNS_AchievedIncentive_ID(), get_TrxName());	}

	/** Set Achieved Incentive.
		@param UNS_AchievedIncentive_ID Achieved Incentive	  */
	public void setUNS_AchievedIncentive_ID (int UNS_AchievedIncentive_ID)
	{
		if (UNS_AchievedIncentive_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AchievedIncentive_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AchievedIncentive_ID, Integer.valueOf(UNS_AchievedIncentive_ID));
	}

	/** Get Achieved Incentive.
		@return Achieved Incentive	  */
	public int getUNS_AchievedIncentive_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AchievedIncentive_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Achieved Incentive Line.
		@param UNS_AchievedIncentive_Line_ID Achieved Incentive Line	  */
	public void setUNS_AchievedIncentive_Line_ID (int UNS_AchievedIncentive_Line_ID)
	{
		if (UNS_AchievedIncentive_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AchievedIncentive_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AchievedIncentive_Line_ID, Integer.valueOf(UNS_AchievedIncentive_Line_ID));
	}

	/** Get Achieved Incentive Line.
		@return Achieved Incentive Line	  */
	public int getUNS_AchievedIncentive_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AchievedIncentive_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_AchievedIncentive_Line_UU.
		@param UNS_AchievedIncentive_Line_UU UNS_AchievedIncentive_Line_UU	  */
	public void setUNS_AchievedIncentive_Line_UU (String UNS_AchievedIncentive_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_AchievedIncentive_Line_UU, UNS_AchievedIncentive_Line_UU);
	}

	/** Get UNS_AchievedIncentive_Line_UU.
		@return UNS_AchievedIncentive_Line_UU	  */
	public String getUNS_AchievedIncentive_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_AchievedIncentive_Line_UU);
	}

	/** Set Achieved Incentive By Period.
		@param UNS_AcvIncentiveByPeriod_ID Achieved Incentive By Period	  */
	public void setUNS_AcvIncentiveByPeriod_ID (int UNS_AcvIncentiveByPeriod_ID)
	{
		if (UNS_AcvIncentiveByPeriod_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AcvIncentiveByPeriod_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AcvIncentiveByPeriod_ID, Integer.valueOf(UNS_AcvIncentiveByPeriod_ID));
	}

	/** Get Achieved Incentive By Period.
		@return Achieved Incentive By Period	  */
	public int getUNS_AcvIncentiveByPeriod_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AcvIncentiveByPeriod_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}