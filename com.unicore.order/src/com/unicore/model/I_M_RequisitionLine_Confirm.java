/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for M_RequisitionLine_Confirm
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_M_RequisitionLine_Confirm 
{

    /** TableName=M_RequisitionLine_Confirm */
    public static final String Table_Name = "M_RequisitionLine_Confirm";

    /** AD_Table_ID=1000361 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Approval */
    public static final String COLUMNNAME_Approval = "Approval";

	/** Set Approval.
	  * The approval document
	  */
	public void setApproval (String Approval);

	/** Get Approval.
	  * The approval document
	  */
	public String getApproval();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Charge_ID */
    public static final String COLUMNNAME_C_Charge_ID = "C_Charge_ID";

	/** Set Charge.
	  * Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID);

	/** Get Charge.
	  * Additional document charges
	  */
	public int getC_Charge_ID();

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name ConfirmedQty */
    public static final String COLUMNNAME_ConfirmedQty = "ConfirmedQty";

	/** Set Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public void setConfirmedQty (BigDecimal ConfirmedQty);

	/** Get Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public BigDecimal getConfirmedQty();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name M_RequisitionConfirm_ID */
    public static final String COLUMNNAME_M_RequisitionConfirm_ID = "M_RequisitionConfirm_ID";

	/** Set Requisition Confirm	  */
	public void setM_RequisitionConfirm_ID (int M_RequisitionConfirm_ID);

	/** Get Requisition Confirm	  */
	public int getM_RequisitionConfirm_ID();

	public I_M_RequisitionConfirm getM_RequisitionConfirm() throws RuntimeException;

    /** Column name M_RequisitionLine_Confirm_ID */
    public static final String COLUMNNAME_M_RequisitionLine_Confirm_ID = "M_RequisitionLine_Confirm_ID";

	/** Set Requisition Line Confirm	  */
	public void setM_RequisitionLine_Confirm_ID (int M_RequisitionLine_Confirm_ID);

	/** Get Requisition Line Confirm	  */
	public int getM_RequisitionLine_Confirm_ID();

    /** Column name M_RequisitionLine_Confirm_UU */
    public static final String COLUMNNAME_M_RequisitionLine_Confirm_UU = "M_RequisitionLine_Confirm_UU";

	/** Set M_RequisitionLine_Confirm_UU	  */
	public void setM_RequisitionLine_Confirm_UU (String M_RequisitionLine_Confirm_UU);

	/** Get M_RequisitionLine_Confirm_UU	  */
	public String getM_RequisitionLine_Confirm_UU();

    /** Column name M_RequisitionLine_ID */
    public static final String COLUMNNAME_M_RequisitionLine_ID = "M_RequisitionLine_ID";

	/** Set Requisition Line.
	  * Material Requisition Line
	  */
	public void setM_RequisitionLine_ID (int M_RequisitionLine_ID);

	/** Get Requisition Line.
	  * Material Requisition Line
	  */
	public int getM_RequisitionLine_ID();

	public org.compiere.model.I_M_RequisitionLine getM_RequisitionLine() throws RuntimeException;

    /** Column name PriceActual */
    public static final String COLUMNNAME_PriceActual = "PriceActual";

	/** Set Unit Price.
	  * Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual);

	/** Get Unit Price.
	  * Actual Price
	  */
	public BigDecimal getPriceActual();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name QtyOnHand */
    public static final String COLUMNNAME_QtyOnHand = "QtyOnHand";

	/** Set On Hand Quantity.
	  * On Hand Quantity
	  */
	public void setQtyOnHand (BigDecimal QtyOnHand);

	/** Get On Hand Quantity.
	  * On Hand Quantity
	  */
	public BigDecimal getQtyOnHand();

    /** Column name QtyOnHand_v */
    public static final String COLUMNNAME_QtyOnHand_v = "QtyOnHand_v";

	/** Set On Hand Quantity.
	  * On Hand Quantity
	  */
	public void setQtyOnHand_v (BigDecimal QtyOnHand_v);

	/** Get On Hand Quantity.
	  * On Hand Quantity
	  */
	public BigDecimal getQtyOnHand_v();

    /** Column name Reason */
    public static final String COLUMNNAME_Reason = "Reason";

	/** Set Reason	  */
	public void setReason (String Reason);

	/** Get Reason	  */
	public String getReason();

    /** Column name Requested_By */
    public static final String COLUMNNAME_Requested_By = "Requested_By";

	/** Set Requested_By	  */
	public void setRequested_By (String Requested_By);

	/** Get Requested_By	  */
	public String getRequested_By();

    /** Column name TotalLines */
    public static final String COLUMNNAME_TotalLines = "TotalLines";

	/** Set Total Lines.
	  * Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines);

	/** Get Total Lines.
	  * Total of all document lines
	  */
	public BigDecimal getTotalLines();

    /** Column name UOMConversionL1_ID */
    public static final String COLUMNNAME_UOMConversionL1_ID = "UOMConversionL1_ID";

	/** Set UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID);

	/** Get UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException;

    /** Column name UOMConversionL2_ID */
    public static final String COLUMNNAME_UOMConversionL2_ID = "UOMConversionL2_ID";

	/** Set UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID);

	/** Get UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException;

    /** Column name UOMConversionL3_ID */
    public static final String COLUMNNAME_UOMConversionL3_ID = "UOMConversionL3_ID";

	/** Set UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID);

	/** Get UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException;

    /** Column name UOMConversionL4_ID */
    public static final String COLUMNNAME_UOMConversionL4_ID = "UOMConversionL4_ID";

	/** Set UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID);

	/** Get UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException;

    /** Column name UOMQtyL1 */
    public static final String COLUMNNAME_UOMQtyL1 = "UOMQtyL1";

	/** Set UOM Qty L1.
	  * The converted quantity without decimal parts from product's base UOM quantity to the Level 1 UOM conversion.
	  */
	public void setUOMQtyL1 (BigDecimal UOMQtyL1);

	/** Get UOM Qty L1.
	  * The converted quantity without decimal parts from product's base UOM quantity to the Level 1 UOM conversion.
	  */
	public BigDecimal getUOMQtyL1();

    /** Column name UOMQtyL2 */
    public static final String COLUMNNAME_UOMQtyL2 = "UOMQtyL2";

	/** Set UOM Qty L2.
	  * The rest of quantity (without decimal part) that is not full converted into 1 of UOM Conversion L1
	  */
	public void setUOMQtyL2 (BigDecimal UOMQtyL2);

	/** Get UOM Qty L2.
	  * The rest of quantity (without decimal part) that is not full converted into 1 of UOM Conversion L1
	  */
	public BigDecimal getUOMQtyL2();

    /** Column name UOMQtyL3 */
    public static final String COLUMNNAME_UOMQtyL3 = "UOMQtyL3";

	/** Set UOM Qty L3.
	  * The rest of quantity (without decimal part) that is not full converted into 1 of UOM Conversion L2
	  */
	public void setUOMQtyL3 (BigDecimal UOMQtyL3);

	/** Get UOM Qty L3.
	  * The rest of quantity (without decimal part) that is not full converted into 1 of UOM Conversion L2
	  */
	public BigDecimal getUOMQtyL3();

    /** Column name UOMQtyL4 */
    public static final String COLUMNNAME_UOMQtyL4 = "UOMQtyL4";

	/** Set UOM Qty L4.
	  * The rest of quantity (without decimal part) that is not full converted into 1 of UOM Conversion L3
	  */
	public void setUOMQtyL4 (BigDecimal UOMQtyL4);

	/** Get UOM Qty L4.
	  * The rest of quantity (without decimal part) that is not full converted into 1 of UOM Conversion L3
	  */
	public BigDecimal getUOMQtyL4();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
