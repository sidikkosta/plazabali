/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_PreOrder
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_PreOrder 
{

    /** TableName=UNS_PreOrder */
    public static final String Table_Name = "UNS_PreOrder";

    /** AD_Table_ID=1000249 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_User_ID */
    public static final String COLUMNNAME_AD_User_ID = "AD_User_ID";

	/** Set User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID);

	/** Get User/Contact.
	  * User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID();

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException;

    /** Column name AttAddress */
    public static final String COLUMNNAME_AttAddress = "AttAddress";

	/** Set Attention Address	  */
	public void setAttAddress (String AttAddress);

	/** Get Attention Address	  */
	public String getAttAddress();

    /** Column name AttName */
    public static final String COLUMNNAME_AttName = "AttName";

	/** Set Attention Name	  */
	public void setAttName (String AttName);

	/** Get Attention Name	  */
	public String getAttName();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_Order_ID */
    public static final String COLUMNNAME_C_Order_ID = "C_Order_ID";

	/** Set Order.
	  * Order
	  */
	public void setC_Order_ID (int C_Order_ID);

	/** Get Order.
	  * Order
	  */
	public int getC_Order_ID();

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CreateOthersCustomer */
    public static final String COLUMNNAME_CreateOthersCustomer = "CreateOthersCustomer";

	/** Set Create Others Customer	  */
	public void setCreateOthersCustomer (String CreateOthersCustomer);

	/** Get Create Others Customer	  */
	public String getCreateOthersCustomer();

    /** Column name CreateSO */
    public static final String COLUMNNAME_CreateSO = "CreateSO";

	/** Set Create SO	  */
	public void setCreateSO (String CreateSO);

	/** Get Create SO	  */
	public String getCreateSO();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DateFinish */
    public static final String COLUMNNAME_DateFinish = "DateFinish";

	/** Set Finish Date.
	  * Finish or (planned) completion date
	  */
	public void setDateFinish (Timestamp DateFinish);

	/** Get Finish Date.
	  * Finish or (planned) completion date
	  */
	public Timestamp getDateFinish();

    /** Column name DatePromised */
    public static final String COLUMNNAME_DatePromised = "DatePromised";

	/** Set Date Promised.
	  * Date Order was promised
	  */
	public void setDatePromised (Timestamp DatePromised);

	/** Get Date Promised.
	  * Date Order was promised
	  */
	public Timestamp getDatePromised();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocAction */
    public static final String COLUMNNAME_DocAction = "DocAction";

	/** Set Document Action.
	  * The targeted status of the document
	  */
	public void setDocAction (String DocAction);

	/** Get Document Action.
	  * The targeted status of the document
	  */
	public String getDocAction();

    /** Column name DocStatus */
    public static final String COLUMNNAME_DocStatus = "DocStatus";

	/** Set Document Status.
	  * The current status of the document
	  */
	public void setDocStatus (String DocStatus);

	/** Get Document Status.
	  * The current status of the document
	  */
	public String getDocStatus();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsNeedAppointment */
    public static final String COLUMNNAME_IsNeedAppointment = "IsNeedAppointment";

	/** Set Is Need Appointment.
	  * To indicate if the shipment need appointment with the customer
	  */
	public void setIsNeedAppointment (boolean IsNeedAppointment);

	/** Get Is Need Appointment.
	  * To indicate if the shipment need appointment with the customer
	  */
	public boolean isNeedAppointment();

    /** Column name IsOneShipment */
    public static final String COLUMNNAME_IsOneShipment = "IsOneShipment";

	/** Set One Shipment	  */
	public void setIsOneShipment (boolean IsOneShipment);

	/** Get One Shipment	  */
	public boolean isOneShipment();

    /** Column name IsSOTrx */
    public static final String COLUMNNAME_IsSOTrx = "IsSOTrx";

	/** Set Sales Transaction.
	  * This is a Sales Transaction
	  */
	public void setIsSOTrx (boolean IsSOTrx);

	/** Get Sales Transaction.
	  * This is a Sales Transaction
	  */
	public boolean isSOTrx();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ProcessedOn */
    public static final String COLUMNNAME_ProcessedOn = "ProcessedOn";

	/** Set Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn);

	/** Get Processed On.
	  * The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name Shipper */
    public static final String COLUMNNAME_Shipper = "Shipper";

	/** Set Shipper	  */
	public void setShipper (String Shipper);

	/** Get Shipper	  */
	public String getShipper();

    /** Column name UNS_OthersCustomer_ID */
    public static final String COLUMNNAME_UNS_OthersCustomer_ID = "UNS_OthersCustomer_ID";

	/** Set Others Customer (Serba-Serbi)	  */
	public void setUNS_OthersCustomer_ID (int UNS_OthersCustomer_ID);

	/** Get Others Customer (Serba-Serbi)	  */
	public int getUNS_OthersCustomer_ID();

	public com.unicore.model.I_UNS_OthersCustomer getUNS_OthersCustomer() throws RuntimeException;

    /** Column name UNS_PreOrder_ID */
    public static final String COLUMNNAME_UNS_PreOrder_ID = "UNS_PreOrder_ID";

	/** Set Pre Order	  */
	public void setUNS_PreOrder_ID (int UNS_PreOrder_ID);

	/** Get Pre Order	  */
	public int getUNS_PreOrder_ID();

    /** Column name UNS_PreOrder_UU */
    public static final String COLUMNNAME_UNS_PreOrder_UU = "UNS_PreOrder_UU";

	/** Set UNS_PreOrder_UU	  */
	public void setUNS_PreOrder_UU (String UNS_PreOrder_UU);

	/** Get UNS_PreOrder_UU	  */
	public String getUNS_PreOrder_UU();

    /** Column name UNS_Rayon_ID */
    public static final String COLUMNNAME_UNS_Rayon_ID = "UNS_Rayon_ID";

	/** Set Rayon	  */
	public void setUNS_Rayon_ID (int UNS_Rayon_ID);

	/** Get Rayon	  */
	public int getUNS_Rayon_ID();

    /** Column name UNS_Shipping_Track_ID */
    public static final String COLUMNNAME_UNS_Shipping_Track_ID = "UNS_Shipping_Track_ID";

	/** Set Shipping Track.
	  * The reference into which track the shippment will be grouped
	  */
	public void setUNS_Shipping_Track_ID (int UNS_Shipping_Track_ID);

	/** Get Shipping Track.
	  * The reference into which track the shippment will be grouped
	  */
	public int getUNS_Shipping_Track_ID();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
