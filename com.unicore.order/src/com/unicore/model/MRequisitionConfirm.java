package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MPeriod;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPO;
import org.compiere.model.MProductPrice;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

import com.unicore.base.model.MRequisition;
import com.unicore.base.model.MRequisitionLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.model.IUNSApprovalInfo;
import com.uns.model.MUNSConfirmationFlow;
import com.uns.model.MUNSConfirmationFlowLine;

public class MRequisitionConfirm extends X_M_RequisitionConfirm implements
		DocAction, DocOptions, IUNSApprovalInfo {

	
	/**
	 * @author Mr.Thunder
	 */
	
	private static final long serialVersionUID = 8184887824826168130L;

	String m_processMsg = null;
	boolean m_justPrepared = false;
	private MRequisition m_request = null;
	private MRequisitionLineConfirm[] m_confirmlines = null;
	
	public static final String Approve = "APP";
	public static final String Reject = "RJT";
	
	private boolean m_confirmed = false;
	private boolean m_lastConfirm = true;
	private boolean onConfirm = false;
	private String m_desc = null;
	
	public MRequisitionConfirm(Properties ctx, int M_RequisitionConfirm_ID,
			String trxName) {
		
		super(ctx, M_RequisitionConfirm_ID, trxName);
		
	}
	
	public MRequisitionConfirm(Properties ctx , ResultSet rs, String trxname)
	{
		super(ctx, rs, trxname);
	}
	
	public MRequisition getRequest()
	{
		if(m_request == null)
		{
			m_request = new MRequisition(getCtx(), getM_Requisition_ID(), get_TrxName());
		}
		return m_request;
	}
	
	public static MRequisitionConfirm getCreate (Properties ctx, MRequisition request, String trxName)
	{
		MRequisitionConfirm retVal = 
				Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, MRequisitionConfirm.Table_Name
				, "M_Requisition_ID = ?", trxName)
				.setParameters(request.getM_Requisition_ID()).first();
		
		if(retVal == null)
		{
			retVal = new MRequisitionConfirm(ctx, 0, trxName);
			retVal.setM_Requisition_ID(request.getM_Requisition_ID());
			retVal.setAD_Org_ID(request.getAD_Org_ID());
			retVal.setAD_Client_ID(request.getAD_Client_ID());
			retVal.setAD_User_ID(request.getAD_User_ID());
			retVal.setDateRequisition(request.getDateDoc());
			retVal.setDateRequired(request.getDateRequired());
			retVal.setC_BPartner_ID(request.getC_BPartner_ID());
			retVal.setDescription(request.getDescription());
			retVal.setIsApproved(false);
			retVal.setHelp(request.getHelp());
			retVal.setPriorityRule(request.getPriorityRule());
			retVal.setM_Warehouse_ID(request.getM_Warehouse_ID());
			retVal.setM_PriceList_ID(request.getM_PriceList_ID());
			retVal.setDateDoc(request.getDateDoc());
			int docType = MDocType.getDocType("PRC");
			if(docType < 0)
				throw new AdempiereException("Cannot get Document Type");
			
			retVal.setC_DocType_ID(docType);
			
			if(!retVal.save())
				return null;
			
			MRequisitionLine[] lines = request.getLines();
			for(MRequisitionLine line : lines)
			{
				MRequisitionLineConfirm confirmline = new MRequisitionLineConfirm(ctx, 0, trxName);
				confirmline.setM_RequisitionConfirm_ID(retVal.getM_RequisitionConfirm_ID());
				confirmline.setAD_Org_ID(line.getAD_Org_ID());
				confirmline.setLine(line.getLine());
				confirmline.setM_RequisitionLine_ID(line.getM_RequisitionLine_ID());
				confirmline.setC_BPartner_ID(line.getC_BPartner_ID());
				confirmline.setRequested_By(line.getRequested_By());
				confirmline.setM_Product_ID(line.getM_Product_ID());
				confirmline.setC_Charge_ID(line.getC_Charge_ID());
				confirmline.setQty(line.getQty());
				confirmline.setConfirmedQty(line.getQty());
				confirmline.setC_UOM_ID(line.getC_UOM_ID());
				confirmline.setDescription(line.getDescription());
				confirmline.setPriceActual(line.getPriceActual());
				confirmline.setTotalLines(line.getLineNetAmt());
				confirmline.setQtyOnHand(line.getQty());
				if(!confirmline.save())
					return null;
			}
		}
		retVal.m_request = request;
		return retVal;
		
	}
	
	public static MRequisitionConfirm getCreateNextConfirm (Properties ctx, MRequisitionConfirm confirm, String trxName)
	{
		MRequisitionConfirm retVal = 
				Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, MRequisitionConfirm.Table_Name
				, "PrevConfirm_ID = ?", trxName)
				.setParameters(confirm.getM_RequisitionConfirm_ID()).first();
		
		if(retVal == null)
		{
			retVal = new MRequisitionConfirm(ctx, 0, trxName);
			retVal.setM_Requisition_ID(confirm.getM_Requisition_ID());
			retVal.setAD_Org_ID(confirm.getAD_Org_ID());
			retVal.setAD_Client_ID(confirm.getAD_Client_ID());
			retVal.setAD_User_ID(confirm.getAD_User_ID());
			retVal.setDateRequisition(confirm.getDateRequisition());
			retVal.setDateRequired(confirm.getDateRequired());
			retVal.setC_BPartner_ID(confirm.getC_BPartner_ID());
			retVal.setDescription(confirm.getDescription());
			retVal.setIsApproved(false);
			retVal.setHelp(confirm.getHelp());
			retVal.setPriorityRule(confirm.getPriorityRule());
			retVal.setM_Warehouse_ID(confirm.getM_Warehouse_ID());
			retVal.setC_DocType_ID(confirm.getC_DocType_ID());
			retVal.setPrevConfirm_ID(confirm.getM_RequisitionConfirm_ID());
			retVal.setM_PriceList_ID(confirm.getM_PriceList_ID());
			if(!retVal.save())
				return null;
			
			MRequisitionLineConfirm[] lines = confirm.getLines();
			for(MRequisitionLineConfirm line : lines)
			{
				MRequisitionLineConfirm confirmline = new MRequisitionLineConfirm(ctx, 0, trxName);
				confirmline.setM_RequisitionConfirm_ID(retVal.getM_RequisitionConfirm_ID());
				confirmline.setAD_Org_ID(line.getAD_Org_ID());
				confirmline.setLine(line.getLine());
				confirmline.setM_RequisitionLine_ID(line.getM_RequisitionLine_ID());
				confirmline.setC_BPartner_ID(line.getC_BPartner_ID());
				confirmline.setRequested_By(line.getRequested_By());
				confirmline.setM_Product_ID(line.getM_Product_ID());
				confirmline.setC_Charge_ID(line.getC_Charge_ID());
				confirmline.setQty(line.getQty());
				confirmline.setConfirmedQty(line.getConfirmedQty());
				confirmline.setC_UOM_ID(line.getC_UOM_ID());
				confirmline.setReason(line.getReason());
				confirmline.setDescription(line.getDescription());
				confirmline.setPriceActual(line.getPriceActual());
				confirmline.setTotalLines(line.getTotalLines());
				confirmline.setQtyOnHand(line.getQtyOnHand());
				if(!confirmline.save())
					return null;
			}
			
			
		}
		
		return retVal;	
	}
	
	public MRequisitionLineConfirm[] getLines()
	{
		if(m_confirmlines != null){
			set_TrxName(m_confirmlines, get_TrxName());
			return m_confirmlines;
		}
		final String whereClause = "M_RequisitionConfirm_ID = ?";
		List<MRequisitionLineConfirm> list = new Query(getCtx(), I_M_RequisitionLine_Confirm.Table_Name,
													whereClause, get_TrxName()).setParameters(getM_RequisitionConfirm_ID())
													.setOrderBy(MRequisitionLineConfirm.COLUMNNAME_Line)
													.list();
		m_confirmlines = new MRequisitionLineConfirm[list.size()];
		list.toArray(m_confirmlines);
		return m_confirmlines;
	}
	
	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		
		List<Object[]> list = new ArrayList<>();
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{String.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{BigDecimal.class, true});
		list.add(new Object[]{String.class, true});
		
		return list;
	}

	@Override
	public String[] getDetailTableHeader() {
		String def[] = new String[]{"Supplier","Product/Charge","Qty Request - UOM",
				"Qty Confirmed","Current Stock", "Price", "Total","Description"};
		
		return def;
	}

	@Override
	public List<Object[]> getDetailTableContent() {
		List<Object[]> list = new ArrayList<>();
		
		String sql = "SELECT COALESCE(bp.Name, '--') AS SectionOfDepartement,"
				+ " COALESCE(p.Name, c.Name) AS ProductORCharge,"
				+ " ROUND(rl.Qty,2) || ' ' || uom.UOMSymbol AS QTYReq,"
				+ " ROUND(rl.ConfirmedQty,2) || ' ' || uom.UOMSymbol AS QTYConfirmed,"
				+ " rl.PriceActual AS Price, rl.TotalLines AS Total,"
				+ " COALESCE(rl.Description, '--') AS Description,"
				+ " rl.M_Product_ID AS ProductID"
				+ " FROM M_RequisitionLine_Confirm rl"
				+ " LEFT JOIN C_BPartner bp ON bp.C_BPartner_ID = rl.C_BPartner_ID"
				+ " LEFT JOIN M_Product p ON p.M_Product_ID = rl.M_Product_ID"
				+ " LEFT JOIN C_Charge c ON c.C_Charge_ID = rl.C_Charge_ID"
				+ " INNER JOIN C_UOM uom ON uom.C_UOM_ID = rl.C_UOM_ID"
				+ " WHERE rl.M_RequisitionConfirm_ID=?";
		
		PreparedStatement stat = null;
		ResultSet rs = null;
		
		try{
			stat = DB.prepareStatement(sql, get_TrxName());
			stat.setInt(1, get_ID());
			rs = stat.executeQuery();
			
			while(rs.next())
			{
				int count = 0;
				sql = "SELECT COALESCE(SUM(QtyOnHand),0) FROM M_StorageOnHand WHERE M_Product_ID = ?"
						+ " AND M_Locator_ID IN (SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID = ?"
						+ " AND UPPER(value) NOT LIKE UPPER(?))";
				BigDecimal qtyonhand = DB.getSQLValueBD(get_TrxName(), sql, rs.getInt("ProductID"), getM_Warehouse_ID() , "%transit%");
				
				Object[] rowData = new Object[8];
				rowData[count] = rs.getObject("SectionOfDepartement");
				rowData[++count] = rs.getObject("ProductORCharge");
				rowData[++count] = rs.getObject("QtyReq");
				rowData[++count] = rs.getObject("QtyConfirmed");
				rowData[++count] = qtyonhand;
				rowData[++count] = rs.getObject("Price");
				rowData[++count] = rs.getObject("Total");
				rowData[++count] = rs.getObject("Description");
				
				list.add(rowData);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			DB.close(rs, stat);
		}
		return list;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		
		return index;
	
	}

	@Override
	public boolean processIt(String processAction) {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}

	@Override
	public String prepareIt() {
		if(log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(getDocStatus().equals(DOCSTATUS_Drafted))
		{
			if(getConfirmByUser_ID() > 0 && getConfirmByUser_ID() != Env.getAD_User_ID(getCtx()))
				throw new AdempiereException("You don't have the privileges. You are not user can confirm this document");
//			else if(getConfirmByRole_ID() > 0 && getConfirmByRole_ID() != Env.getAD_Role_ID(getCtx()))
//				throw new AdempiereException("You don't have the privileges. Your role does not allow to confirm this document");
		}
		
		MRequisitionLineConfirm[] lines = getLines();
	
		if(lines.length == 0)
			throw new AdempiereException("@NoLines@");
		
		for(MRequisitionLineConfirm line : lines)
		{
			if (line.getC_BPartner_ID() == 0)
			{
				m_processMsg = "Please define vendor before do next process.";
				return DOCSTATUS_Invalid;
			}
			if(line.getApproval() == null)
				throw new AdempiereException("You must declare the requisition line is approved or rejected");
			
			MRequisitionLine reqLine = new MRequisitionLine(getCtx(), line.getM_RequisitionLine_ID(), get_TrxName());
			reqLine.setPriceActual(line.getPriceActual());
			reqLine.saveEx();
		}
		
		if(getDateDoc() == null)
			setDateDoc(new Timestamp(System.currentTimeMillis()));
		if(getDateConfirm() == null)
			setDateConfirm(new Timestamp(System.currentTimeMillis()));
		
		MPeriod.testPeriodOpen(getCtx(), getDateDoc(), MDocType.DOCBASETYPE_RequisitionConfirm, getAD_Org_ID());
		
		if(!onConfirm && getConfirmByUser_ID() <= 0)
			setConfirmByUser_ID(Env.getContextAsInt(getCtx(), "AD_User_ID"));
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	@Override
	public String completeIt() {

		getRequest();
		
		if(!m_justPrepared)
		{
			String status = prepareIt();
			if(!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		int IDConfirm = getM_RequisitionConfirm_ID();
		int seqNo = 1;
		for(int i=0; i<999999;i++)
		{
			String sql = "SELECT PrevConfirm_ID FROM M_RequisitionConfirm WHERE M_RequisitionConfirm_ID = ?";
			int prevConfirm = DB.getSQLValue(get_TrxName(), sql, IDConfirm);
			
			if(prevConfirm <= 0)
			{
				break;
			}
			else
			{
				seqNo = seqNo+1;
				IDConfirm = prevConfirm;
			}
		}
		
		MUNSConfirmationFlow flow = MUNSConfirmationFlow.get(getCtx(), Table_ID, getC_DocType_ID(), get_TrxName());
		if(flow != null)
		{
			MUNSConfirmationFlowLine line = flow.getLinebySequence(seqNo+1);
			if(line != null)
			{
				MRequisitionConfirm confirm = getCreateNextConfirm(getCtx(), this, get_TrxName());
				if(confirm==null)
				{
					m_processMsg = "Error when try to create confirm";
					return DocAction.STATUS_Invalid;
				}
				
				if(!onConfirm && (confirm.getConfirmByUser_ID() <= 0 || confirm.getConfirmByRole_ID() <= 0))
				{
					confirm.setConfirmByUser_ID(line.getAD_User_ID());
					confirm.setConfirmByRole_ID(line.getAD_Role_ID());
					confirm.saveEx();
				}
				
				if(getConfirmByUser_ID() <= 0 && !onConfirm)	
					setConfirmByUser_ID(Env.getAD_User_ID(getCtx()));
				
				if((confirm.getDocStatus().equals(DOCSTATUS_Drafted) 
						|| confirm.getDocStatus().equals(DOCSTATUS_InProgress)
						|| confirm.getDocStatus().equals(DOCSTATUS_Invalid)
						|| confirm.getDocStatus().equals(DOCSTATUS_Approved)
						|| confirm.getDocStatus().equals(DOCSTATUS_NotApproved))
					&& !m_confirmed)
				{
					setProcessed(true);
					m_processMsg = "Please complete requisition confirm above first. "+confirm.getDocumentNo();
					return DocAction.STATUS_InProgress;
				}
			}
		}
		
		if(!isApproved())
			approveIt();
		
		if(getPrevConfirm_ID() > 0)
		{
			try{
				MRequisitionConfirm confirm = new MRequisitionConfirm(getCtx(), getPrevConfirm_ID(), get_TrxName());
				
				confirm.setConfirmed(true);
				confirm.setLastConfirm(false);
				confirm.setOnConfirm(true);
				confirm.processIt(DOCACTION_Complete);
				confirm.saveEx();
					
			}
			catch (Exception ex){
				ex.printStackTrace();
				m_processMsg = ex.getMessage();
			}
			
			if(m_processMsg != null)
				return DocAction.STATUS_Invalid;
		}
		
		if(m_lastConfirm)
		{
			m_request.setConfirmed(true);
			
			if(!updateRequest())
			{
				m_processMsg = "Error when update Requisition";
				return DocAction.STATUS_Invalid;
			}
			
			if(!m_request.processIt(DOCACTION_Complete)
					|| !m_request.save())
				return DocAction.STATUS_Invalid;
		}
		
		MRequisitionLineConfirm[] lines = getLines();
		
		for(int i=0;i<lines.length;i++)
		{
			m_processMsg = createUpdatePriceList(lines[i]);
			if(m_processMsg != null)
				return DocAction.STATUS_Invalid;
			m_processMsg = createUpdateProductPO(lines[i]);
			if(m_processMsg != null)
				return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		
		if(log.isLoggable(Level.INFO))
			log.info("voidIt - " +toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		//check next confirm
		String sql = "SELECT 1 FROM M_RequisitionConfirm WHERE PrevConfirm_ID = ? AND DocStatus NOT IN ('VO','RE')";
		boolean exists = DB.getSQLValue(get_TrxName(), sql, getM_RequisitionConfirm_ID()) > 0;
		if(exists && !m_confirmed)
		{
			m_processMsg = "Cannot Void. There was a Next Confirm Document not void";
			return false;
		}
		
//		if(!isExistPO())
//		{
//			m_processMsg = "The Requisition has used in order";
//			return false;
//		}
		
		if(!m_confirmed)
		{
			if(m_confirmlines == null)
				getLines();
		
			for(int i=0; i<m_confirmlines.length;i++)
			{
				m_confirmlines[i].setApproval(Reject);
				m_confirmlines[i].saveEx();
			}
			
			setDesc("-- NOT APPROVED By "+Env.getContext(getCtx(), "#AD_User_Name")+" --");
		}
		
		if(getPrevConfirm_ID() > 0)
		{
			try{
				MRequisitionConfirm confirm = new MRequisitionConfirm(getCtx(), getPrevConfirm_ID(), get_TrxName());
	
				confirm.setConfirmed(true);
				confirm.setLastConfirm(false);
				confirm.processIt(DOCACTION_Void);
				confirm.setDescription(confirm.getDescription()+" || "+m_desc);
				confirm.saveEx();
					
			}
			catch (Exception ex){
				ex.printStackTrace();
				m_processMsg = ex.getMessage();
			}
			
			if(m_processMsg != null)
				return false;
		}
		
		if(m_lastConfirm)
		{
			setDescription(getDescription()+" || -- REJECTED by "+Env.getContext(getCtx(), "#AD_User_Name")+" --");
			if(!updateRequest())
			{
				m_processMsg = "Error when update Requisition";
				return false;
			}
		}
		
		m_processMsg= ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		setProcessed(true);
		setDocStatus(STATUS_Closed);
		setDocAction(ACTION_None);
		
		return true;
	}

	@Override
	public boolean closeIt() {
		
		if(log.isLoggable(Level.INFO))
			log.info("CloseIt - " +toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if(m_processMsg != null)
			return false;
		
		m_processMsg= ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = "Disallowed Close"; 
		return false;
	}

	@Override
	public boolean reverseCorrectIt() {
		m_processMsg = "Disallowed reverseCorrect"; 
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		m_processMsg = "Disallowed reverseAccrual"; 
		return false;
	}

	@Override
	public boolean reActivateIt() {
		m_processMsg = "Disallowed Reactive"; 
		return false;
	}

	@Override
	public String getSummary() {
	
		return null;
	}

	@Override
	public String getDocumentInfo() {
		
		return Msg.getElement(getCtx(), "M_RequisitionConfirm") + " " + getDocumentNo();
	}

	@Override
	public File createPDF() {
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}

	public File createPDF (File file)
	{
	
		return null;
	}
	
	@Override
	public String getProcessMsg() {
		
		return m_processMsg;
	}
	
	public boolean isComplete()
	{
		String ds = getDocStatus();
		return DOCSTATUS_Completed.equals(ds) 
			|| DOCSTATUS_Closed.equals(ds)
			|| DOCSTATUS_Reversed.equals(ds);
	}

	@Override
	public int getDoc_User_ID() {
		
		return 0;
	}

	@Override
	public int getC_Currency_ID() {

		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		
		return null;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {

		return true;
	}
	
	public boolean updateRequest()
	{
		if(m_request==null)
			getRequest();
		
		MRequisitionLine[] reqlines = m_request.getLines();
		if(null == reqlines)
			return false;
		
		for(MRequisitionLine reqline : reqlines)
		{
			BigDecimal qty = reqline.getQty();
			String desc = reqline.getDescription();
			
			if(m_confirmlines==null)
				getLines();
			
			for(int i = 0; i < m_confirmlines.length; i++)
			{
				if(reqline.getM_RequisitionLine_ID() == m_confirmlines[i].getM_RequisitionLine_ID())
				{
					desc = m_confirmlines[i].getDescription();
					
					if(m_confirmlines[i].getApproval().equals(Approve))
					{
						qty = m_confirmlines[i].getConfirmedQty();
					}
					else
					{
						qty = Env.ZERO;
						desc += "|| Rejected by "+Env.getContext(getCtx(), "#AD_User");
					}
					
					reqline.setPriceActual(m_confirmlines[i].getPriceActual());
					
					break;
				}
			}
			reqline.setDescription(desc);
			reqline.setQtyRequest(reqline.getQty());
			reqline.setQty(qty);
			reqline.setLineNetAmt();
			if(!reqline.save())
			{
				m_processMsg = "Error When try to update requisition line.";
				return false;
			}
				
		}
		m_request.setDescription(getDescription());
		m_request.saveEx();
		
		return true;
	}
	
//	public boolean isExistPO()
//	{
//		getRequest();
//		MRequisitionLine[] reqlines = m_request.getLines();
//		
//		for(MRequisitionLine reqline : reqlines)
//		{
//			if(reqline.getC_OrderLine_ID() > 0)
//				return false;
//		}
//		
//		return true;
//	}
	
	public void setConfirmed(boolean confirmed){
		m_confirmed = confirmed;
	}
	
	public void setLastConfirm(boolean last) {
		m_lastConfirm = last;
	}
	
	public void setOnConfirm(boolean confirm) {
		onConfirm = confirm;
	}
	
	public void setDesc(String desc) {
		m_desc = desc;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
	
	private String createUpdatePriceList(MRequisitionLineConfirm lines)
	{
		MPriceList pl = new MPriceList(getCtx(), getM_PriceList_ID(), get_TrxName());
		
		MPriceListVersion plv = MPriceListVersion.getSameValidFrom(getCtx(), pl.get_ID(), getDateConfirm(), get_TrxName());
		
		if(plv == null)
		{
			plv = new MPriceListVersion(pl);
			plv.setValidFrom(getDateConfirm());
			if(!plv.save())
				return "Failed when trying create new version price.";
		}
		
		MProductPrice pp = MProductPrice.get(getCtx(), plv.get_ID(), lines.getM_Product_ID(), get_TrxName());
		if(pp == null)
		{
			pp = new MProductPrice(plv, lines.getM_Product_ID(),
					lines.getPriceActual(), lines.getPriceActual(), lines.getPriceActual());
		}
		else
			pp.setPriceList(lines.getPriceActual());
		if(!pp.save())
			return "Failed when trying create new version price.";
		
		return null;
	}
	
	private String createUpdateProductPO(MRequisitionLineConfirm lines)
	{
		String sql = "UPDATE M_Product_PO set IsCurrentVendor = 'N'"
				+ " WHERE M_Product_ID = ?";
		DB.executeUpdate(sql, lines.getM_Product_ID(), get_TrxName());
		
		MProductPO pp = MProductPO.getOfPartnerProduct(getCtx(), lines.getM_Product_ID(), lines.getC_BPartner_ID(),
				get_TrxName());
		
		if(pp == null)
		{
			pp = new MProductPO(getCtx(), 0, get_TrxName());
			pp.setAD_Org_ID(lines.getM_Product().getAD_Org_ID());
			pp.setC_BPartner_ID(lines.getC_BPartner_ID());
			pp.setIsCurrentVendor(true);
			pp.setM_Product_ID(lines.getM_Product_ID());
			pp.setVendorProductNo(lines.getM_Product().getSKU() + lines.getC_BPartner().getValue());
			if(!pp.save())
				return "Failed when trying create new Product PO.";
		}
		else
		{
			pp.setIsActive(true);
			pp.setIsCurrentVendor(true);
			if(!pp.save())
				return "Failed when trying create new Product PO.";
		}
		
		return null;
	}
}