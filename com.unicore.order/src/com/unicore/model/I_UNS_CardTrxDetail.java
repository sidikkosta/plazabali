/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_CardTrxDetail
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_CardTrxDetail 
{

    /** TableName=UNS_CardTrxDetail */
    public static final String Table_Name = "UNS_CardTrxDetail";

    /** AD_Table_ID=1000394 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BatchNo */
    public static final String COLUMNNAME_BatchNo = "BatchNo";

	/** Set Batch No	  */
	public void setBatchNo (String BatchNo);

	/** Get Batch No	  */
	public String getBatchNo();

    /** Column name CardholderName */
    public static final String COLUMNNAME_CardholderName = "CardholderName";

	/** Set Cardholder Name	  */
	public void setCardholderName (String CardholderName);

	/** Get Cardholder Name	  */
	public String getCardholderName();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name InvoiceNo */
    public static final String COLUMNNAME_InvoiceNo = "InvoiceNo";

	/** Set Invoice No	  */
	public void setInvoiceNo (String InvoiceNo);

	/** Get Invoice No	  */
	public String getInvoiceNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name OriginalInvoiceNo */
    public static final String COLUMNNAME_OriginalInvoiceNo = "OriginalInvoiceNo";

	/** Set Original Invoice No	  */
	public void setOriginalInvoiceNo (String OriginalInvoiceNo);

	/** Get Original Invoice No	  */
	public String getOriginalInvoiceNo();

    /** Column name TraceNo */
    public static final String COLUMNNAME_TraceNo = "TraceNo";

	/** Set Trace No	  */
	public void setTraceNo (String TraceNo);

	/** Get Trace No	  */
	public String getTraceNo();

    /** Column name TransactionType */
    public static final String COLUMNNAME_TransactionType = "TransactionType";

	/** Set Transaction Type	  */
	public void setTransactionType (String TransactionType);

	/** Get Transaction Type	  */
	public String getTransactionType();

    /** Column name UNS_CardTrxDetail_ID */
    public static final String COLUMNNAME_UNS_CardTrxDetail_ID = "UNS_CardTrxDetail_ID";

	/** Set Card Trx Detail	  */
	public void setUNS_CardTrxDetail_ID (int UNS_CardTrxDetail_ID);

	/** Get Card Trx Detail	  */
	public int getUNS_CardTrxDetail_ID();

    /** Column name UNS_CardTrxDetail_UU */
    public static final String COLUMNNAME_UNS_CardTrxDetail_UU = "UNS_CardTrxDetail_UU";

	/** Set UNS_CardTrxDetail_UU	  */
	public void setUNS_CardTrxDetail_UU (String UNS_CardTrxDetail_UU);

	/** Get UNS_CardTrxDetail_UU	  */
	public String getUNS_CardTrxDetail_UU();

    /** Column name UNS_CardType_ID */
    public static final String COLUMNNAME_UNS_CardType_ID = "UNS_CardType_ID";

	/** Set Card Type	  */
	public void setUNS_CardType_ID (int UNS_CardType_ID);

	/** Get Card Type	  */
	public int getUNS_CardType_ID();

	public com.unicore.model.I_UNS_CardType getUNS_CardType() throws RuntimeException;

    /** Column name UNS_EDC_ID */
    public static final String COLUMNNAME_UNS_EDC_ID = "UNS_EDC_ID";

	/** Set EDC	  */
	public void setUNS_EDC_ID (int UNS_EDC_ID);

	/** Get EDC	  */
	public int getUNS_EDC_ID();

	public com.unicore.model.I_UNS_EDC getUNS_EDC() throws RuntimeException;

    /** Column name UNS_PaymentTrx_ID */
    public static final String COLUMNNAME_UNS_PaymentTrx_ID = "UNS_PaymentTrx_ID";

	/** Set Payment Transaction	  */
	public void setUNS_PaymentTrx_ID (int UNS_PaymentTrx_ID);

	/** Get Payment Transaction	  */
	public int getUNS_PaymentTrx_ID();

	public com.unicore.model.I_UNS_PaymentTrx getUNS_PaymentTrx() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
