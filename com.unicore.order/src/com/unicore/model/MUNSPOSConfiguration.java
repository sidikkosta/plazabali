package com.unicore.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.compiere.util.CCache;
import org.compiere.util.Env;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * 
 */

/**
 * @author nurse
 *
 */
public class MUNSPOSConfiguration extends X_UNS_POSConfiguration 
{
	private static CCache<Integer,MUNSPOSConfiguration> s_cache = new CCache<Integer,MUNSPOSConfiguration>(Table_Name, 20);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 398294115445761987L;
	private MUNSPOSConfigurationCurr[] m_cashs = null;
	
	public static MUNSPOSConfiguration getActive (String trxName, int orgID)
	{
		MUNSPOSConfiguration config = s_cache.get(orgID);
		if (config != null)
			return config;
		
		String clause = " AD_Org_ID = ? OR AD_Org_ID = ? ";
		config = Query.get(Env.getCtx(), 
				UNSOrderModelFactory.EXTENSION_ID, Table_Name, clause, 
				trxName).setParameters(orgID, 0).
				setOnlyActiveRecords(true).first();
		
		if (config != null)
			s_cache.put(orgID, config);
		
		return config;
	}
	
	public MUNSPOSConfigurationCurr[] getCashCurrency (boolean requery)
	{
		if (m_cashs != null && !requery)
		{
			set_TrxName( m_cashs, get_TrxName());
			return m_cashs;
		}
		List<MUNSPOSConfigurationCurr> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSPOSConfigurationCurr.Table_Name, Table_Name + "_ID = ? ", 
				get_TrxName()).setParameters(get_ID()).list();
		m_cashs = new MUNSPOSConfigurationCurr[list.size()];
		list.toArray(m_cashs);
		
		return m_cashs;
	}
	
	/**
	 * @param ctx
	 * @param UNS_POSConfiguration_ID
	 * @param trxName
	 */
	public MUNSPOSConfiguration(Properties ctx, int UNS_POSConfiguration_ID,
			String trxName) 
	{
		super(ctx, UNS_POSConfiguration_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSConfiguration(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	public BigDecimal getRoundingAmount (BigDecimal amount)
	{
		String roundType = getPaymentRounding();
		BigDecimal roundAmount;
		if (PAYMENTROUNDING_10.equals(roundType))
		{
			if (amount.abs().compareTo(BigDecimal.TEN) == -1)
				roundAmount = amount.negate();
			else
			{
				roundAmount = amount.divide(BigDecimal.TEN, 0, RoundingMode.HALF_DOWN);
				roundAmount = roundAmount.multiply(BigDecimal.TEN);
				roundAmount = amount.subtract(roundAmount);
			}
		}
		else if (PAYMENTROUNDING_100.equals(roundType))
		{
			if (amount.abs().compareTo(Env.ONEHUNDRED) == -1)
				roundAmount = amount.negate();
			else
			{
				roundAmount = amount.divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_DOWN);
				roundAmount = roundAmount.multiply(Env.ONEHUNDRED);
				roundAmount = amount.subtract(roundAmount);
			}
		}
		else if (PAYMENTROUNDING_1000.equals(roundType))
		{
			if (amount.abs().compareTo(Env.ONETHOUSAND) == -1)
				roundAmount = amount.negate();
			else
			{
				roundAmount = amount.divide(Env.ONETHOUSAND, 0, RoundingMode.HALF_DOWN);
				roundAmount = roundAmount.multiply(Env.ONETHOUSAND);
				roundAmount = amount.subtract(roundAmount);
			}
		}
		else if (PAYMENTROUNDING_HalfDown50.equals(roundType))
		{
			BigDecimal remainder = amount.remainder(Env.ONE);
			int intAmt = amount.intValue();
			String amtStr = Integer.toString(intAmt);
			if (amtStr.length() > 2)
				amtStr = amtStr.substring(amtStr.length()-2, amtStr.length());
			intAmt = Integer.valueOf(amtStr);
			if (intAmt > 75)
			{
				roundAmount = Env.ONEHUNDRED.subtract(BigDecimal.valueOf(intAmt));
			}
			else if (intAmt > 25)
			{
				roundAmount = new BigDecimal(50).subtract(BigDecimal.valueOf(intAmt));
			}
			else
			{
				roundAmount = Env.ZERO.subtract(BigDecimal.valueOf(intAmt));
			}
			roundAmount = roundAmount.add(remainder);
			if (amount.signum() == -1)
				roundAmount = roundAmount.abs();
			else
				roundAmount = roundAmount.abs().negate();
		}
		else if (PAYMENTROUNDING_HalfUp50.equals(roundType))
		{
			BigDecimal remainder = amount.remainder(Env.ONE);
			int intAmt = amount.intValue();
			String amtStr = Integer.toString(intAmt);
			if (amtStr.length() > 2)
				amtStr = amtStr.substring(amtStr.length()-2, amtStr.length());
			intAmt = Integer.valueOf(amtStr);
			if (intAmt > 74)
			{
				roundAmount = Env.ONEHUNDRED.subtract(new BigDecimal(intAmt));
			}
			else if (intAmt > 24)
			{
				roundAmount = new BigDecimal(50).subtract(new BigDecimal(intAmt));
			}
			else
			{
				roundAmount = Env.ZERO.subtract(BigDecimal.valueOf(intAmt));
			}
			
			roundAmount = roundAmount.add(remainder);
			if (amount.signum() == -1)
				roundAmount = roundAmount.abs();
			else
				roundAmount = roundAmount.abs().negate();
			
			if ((roundAmount.add(amount)).remainder(new BigDecimal(50)).signum () != 0)
				roundAmount = roundAmount.negate();
		}
		else
		{
			roundAmount = Env.ZERO;
		}
		
		return roundAmount;
	}
}
