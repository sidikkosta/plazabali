/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInOutLineMA;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTransaction;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MInvoiceLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.unicore.ui.ISortTabRecord;
import com.uns.base.model.Query;
import com.uns.model.MUNSArmada;
import com.uns.model.X_UNS_ArmadaType;
import com.uns.util.MessageBox;
import com.uns.util.UNSApps;
/**
 * @author AzHaidar
 *
 */
public class MUNSWeighbridgeTicket extends X_UNS_WeighbridgeTicket implements
		DocAction, DocOptions, ISortTabRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4050489406139706593L;
	
	String m_processMsg = null;
	private boolean m_justPrepared = false;
	private boolean m_force = false;
	private int m_M_Movement_ID = 0;
	private int m_M_MovementConfirm_ID = 0;

	/**
	 * @param ctx
	 * @param UNS_WeighbridgeTicket_ID
	 * @param trxName
	 */
	public MUNSWeighbridgeTicket(Properties ctx, int UNS_WeighbridgeTicket_ID,
			String trxName) {
		super(ctx, UNS_WeighbridgeTicket_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSWeighbridgeTicket(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
    protected boolean beforeSave(boolean newRecord) 
    {
		//check document exist
		if(newRecord)
		{
			String sql = "SELECT 1 FROM UNS_WeighbridgeTicket WHERE documentno = ? "
					+ "AND C_DocType_ID = ? "
					+ "AND AD_Org_ID = ?";
			Boolean docexist = DB.getSQLValueEx(get_TrxName(), sql, getDocumentNo(), getC_DocType_ID(), getAD_Org_ID()) > 0;
			if(docexist){
				log.saveError("Document Exist", "This Document No has been created");
				return false;
			}
		}
		
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		if (m_isOnSortTabRecordAction) 
		{
			if(!newRecord && is_ValueChanged(COLUMNNAME_C_Invoice_ID))
			{
				// Seharusnya masuk ke sini dari modul/window Invoice (POP Supplier) saat select ticket.
				int C_Invoice_ID = getC_Invoice_ID();
				if (C_Invoice_ID > 0) 
				{
					// selected in the invoice pop supplier's ticket selection.
					MInvoiceLine invLine = new MInvoiceLine(new MInvoice(getCtx(), C_Invoice_ID, get_TrxName()));
					
					MOrderLine oLine = new MOrderLine(getCtx(), getC_OrderLine_ID(), get_TrxName());
					
					invLine.setC_OrderLine_ID(getC_OrderLine_ID());
					invLine.setC_Tax_ID(oLine.getC_Tax_ID());
					if (MDocType.DOCBASETYPE_RegradedTicket.equals(dt.getDocBaseType()))
					{
						invLine.setQty(getReflection().negate());
						invLine.addDescription("Potongan Buah Busuk / Tandan Tua.");
						invLine.set_ValueOfColumn("", invLine.getQtyInvoiced());
						invLine.setPrice(oLine.getPriceEntered());
						int C_Charge_ID = UNSApps.getRefAsInt(UNSApps.POT_BH_BUSUK);
						invLine.setC_Charge_ID(C_Charge_ID);
					}
					else
					{
						invLine.setM_Product_ID(oLine.getM_Product_ID(), true);
						invLine.setQty(getNettoI());
						invLine.set_ValueOfColumn("QtyInvoiceTicket", 
								analyzeInvoiceTicketQty());

						invLine.setM_InOutLine_ID(DB.getSQLValue(get_TrxName(),
								"SELECT M_InOutLine_ID FROM M_InOutLine WHERE M_InOut_ID=?", getM_InOut_ID()));
					}
					
					invLine.setPriceList(oLine.getPriceEntered());
					if (!invLine.save()) {
						return false;
					}
					setC_InvoiceLine_ID(invLine.get_ID());
				}
				else { 
					// removed from the purchasing invoice selection.
					//int C_InvoiceLine_ID = getC_InvoiceLine_ID();
					setC_InvoiceLine_ID(0);
					int oldInvLine_ID = get_ValueOldAsInt(COLUMNNAME_C_InvoiceLine_ID);
					MInvoiceLine il = new MInvoiceLine(getCtx(), oldInvLine_ID, get_TrxName());
					il.setM_InOutLine_ID(-1);
					if(!il.save()) return false;
					//setC_Invoice_ID(0);
				}
			}
			
			if(!newRecord && is_ValueChanged(COLUMNNAME_UNS_UnLoadingCost_ID))
			{
				if(getUNS_UnLoadingCost_ID() > 0)
				{
					MUNSUnLoadingCostLine ucl = new MUNSUnLoadingCostLine(getCtx(), 0, get_TrxName());
					ucl.setUNS_UnLoadingCost_ID(getUNS_UnLoadingCost_ID());
					ucl.setUNS_WeighbridgeTicket_ID(get_ID());
					ucl.saveEx();
					setUNS_UnLoadingCost_Line_ID(ucl.get_ID());
				}
				else
				{
					setUNS_UnLoadingCost_Line_ID(-1);
					int oldCostLine = get_ValueOldAsInt(COLUMNNAME_UNS_UnLoadingCost_Line_ID);
					MUNSUnLoadingCostLine lcl = new MUNSUnLoadingCostLine(getCtx(), oldCostLine, get_TrxName());
					lcl.deleteEx(true);
				}
			}
			
			if(!newRecord && is_ValueChanged(COLUMNNAME_UNS_ReflectionRevision_ID))
			{
				if(getUNS_ReflectionRevision_ID() > 0)
				{
					MUNSGradingSheetAll grad = MUNSGradingSheetAll.get(getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
					MUNSReflectionRevisionLine revL = new MUNSReflectionRevisionLine(getCtx(), 0, get_TrxName());
					revL.setUNS_ReflectionRevision_ID(getUNS_ReflectionRevision_ID());
					revL.setUNS_WeighbridgeTicket_ID(get_ID());
					revL.setDescription(grad.getDescription());
					revL.setDura(grad.getDura());
					revL.saveEx();
					setUNS_ReflectionRevision_Line_ID(revL.get_ID());
				}
				else
				{
					int oldRevLine = getUNS_ReflectionRevision_Line_ID();
					setUNS_ReflectionRevision_Line_ID(-1);
					MUNSReflectionRevisionLine revL = new MUNSReflectionRevisionLine(getCtx(), oldRevLine, get_TrxName());
					revL.deleteEx(true);
				}
			}
			
			return true;
		}
		
		if (newRecord || is_ValueChanged(COLUMNNAME_UNS_ArmadaType_ID)) {
			setVehicleType(new X_UNS_ArmadaType(getCtx(), getUNS_ArmadaType_ID(), get_TrxName()).getVehicleType());
		}
		
		if(getC_OrderLine_ID() > 0 && getC_Order_ID() == 0)
			setC_Order_ID(getC_OrderLine().getC_Order_ID());
		
		if (!dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GeneralWeighbridgeTicket)
			&& getC_Order_ID() == 0)
		{
			log.saveError("OrderMustBeSelected", "You have to select an order for this type of document.");
			return false;
		}
		
		//General Ticket
		if(dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GeneralWeighbridgeTicket))
		{
				
		}
		
		if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GradedTicket)) 
		{
			MUNSGradingSheetAll gsa = MUNSGradingSheetAll.get(getCtx(), get_ID(), get_TrxName());
			
			if (!newRecord && is_ValueChanged(COLUMNNAME_UNS_ArmadaType_ID)) 
			{
				gsa.setDefaultTare(MUNSArmada.getDefaultTare(getUNS_ArmadaType_ID(), get_TrxName()));
				if("DUMP".equals(getVehicleType()))
					gsa.setIsDumpTruck(true);
				else
					gsa.setIsDumpTruck(false);
				
			}
			
			if (!newRecord && 
					(!isSplitOrder() && is_ValueChanged(COLUMNNAME_IsSplitOrder)
					|| is_ValueChanged(COLUMNNAME_UNS_ArmadaType_ID)
					|| is_ValueChanged(COLUMNNAME_GrossWeight)
					|| is_ValueChanged(COLUMNNAME_Tare))
				) 
			{
				BigDecimal newReflection = gsa.resetReflection(this);
				
				setReflection(newReflection);
				setOriginalReflection(newReflection);
			}
		}
		
		if (!newRecord && is_ValueChanged(COLUMNNAME_C_DocType_ID))
		{
			//MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
			MDocType oldDT = MDocType.get(getCtx(), get_ValueOldAsInt(COLUMNNAME_C_DocType_ID));
			
			if (oldDT.getDocBaseType().equals(MDocType.DOCBASETYPE_GradedTicket))
			{
				String sql = "SELECT 1 FROM UNS_Grading_Sheet WHERE UNS_WeighbridgeTicket_ID=" + get_ID();
				boolean gradingExists = DB.getSQLValueEx(get_TrxName(), sql) > 0;
				
				if (gradingExists)
				{
					String msg = "The change of Document Type will remove existing grading records.\n "
							+ "Please select Yes to remove gradings, No to keep it !";
					String title = "Remove existing grading records?";
					int answer = 
							MessageBox.showMsg(this, getCtx(), msg, title, 
									MessageBox.YESNOCANCEL, MessageBox.ICONQUESTION);
					
					if (answer == MessageBox.RETURN_YES) 
					{
						sql = "DELETE FROM UNS_Grading_Sheet WHERE UNS_WeighbridgeTicket_ID=" + get_ID();
						int count = DB.executeUpdate(sql, get_TrxName());
						log.info("Grading components deleted #" + count);
						
						setReflection(Env.ZERO);
						setNettoII(getNettoI());
					}
					else if (answer == MessageBox.RETURN_CANCEL) {
						log.saveError("ChangesCanceled", "The last changes was not saved.");
						return false;
					}
				}
			}
		}
		
		BigDecimal nettoII = getNettoI().subtract(getReflection());
		setNettoII(nettoII);
		setQtyPayment(analyzeInvoiceTicketQty());
		return true;
    } // beforeSave
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{ 
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		
		if (!m_isOnSortTabRecordAction && !isReplication())
		{
			if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GradedTicket)
					|| MDocType.DOCBASETYPE_RegradedTicket.equals(dt.getDocBaseType()))
			{
				MUNSGradingSheetAll gsa = 
						Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
								MUNSGradingSheetAll.Table_Name, "UNS_WeighbridgeTicket_ID=?", get_TrxName())
						.setParameters(getUNS_WeighbridgeTicket_ID())
						.first();
				
				if (gsa == null)
				{
					gsa = new MUNSGradingSheetAll(getCtx(), 0, get_TrxName());
					gsa.setAD_Org_ID(getAD_Org_ID());
					gsa.setUNS_WeighbridgeTicket_ID(getUNS_WeighbridgeTicket_ID());
					gsa.setDateDoc(getDateDoc());
					gsa.setDefaultTare(MUNSArmada.getDefaultTare(getUNS_ArmadaType_ID(), get_TrxName()));
					gsa.setIsQtyPercentage(true);
					if("DUMP".equals(getVehicleType()))
						gsa.setIsDumpTruck(true);
					else
						gsa.setIsDumpTruck(false);
					//gsa.setGrossWeight(getGrossWeight());
					//gsa.setTare(getTare());

//					MUNSOrderContract ctr = 
//							Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
//									MUNSOrderContract.Table_Name, "C_Order_ID = ?", get_TrxName())
//								.setParameters(getC_Order_ID())
//								.first();
//					
//					BigDecimal compulsoryPercentage = ctr.getFreshCompulsoryDeduction();
//					BigDecimal oldBunchPercentage = Env.ZERO;
//					
//					if (gsa.getFFBReceiptCondition() != null
//							&& gsa.getFFBReceiptCondition().equals(MUNSGradingSheetAll.FFBRECEIPTCONDITION_DominantlyBacklog) 
//							&& ctr.getBacklogCompulsoryDeduction().compareTo(ctr.getFreshCompulsoryDeduction()) > 0)
//					{
//						//compulsoryPercentage = ctr.getBacklogCompulsoryDeduction();
//						oldBunchPercentage = ctr.getBacklogCompulsoryDeduction().subtract(compulsoryPercentage);
//					}
//					
//					//BigDecimal NettoIForGSA = getNettoI();
//					//if (this.getTare().signum() > 0)
//					BigDecimal NettoIForGSA = getGrossWeight().subtract(gsa.getDefaultTare());
//					
//					if (compulsoryPercentage.signum() > 0)
//					{
//						BigDecimal compulsoryDeduction = 
//								NettoIForGSA.multiply(compulsoryPercentage)
//								.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_HALF_UP);
//						gsa.setPercent_CompulsoryDeduction(compulsoryPercentage);
//						gsa.setEst_CompulsoryDeduction(compulsoryDeduction);
//					}
//					
//					if (oldBunchPercentage.signum() > 0)
//					{
//						BigDecimal oldBunchDeduction = NettoIForGSA.multiply(oldBunchPercentage)
//								.divide(Env.ONEHUNDRED, 2, BigDecimal.ROUND_HALF_UP);
//						gsa.setPercent_OldBunch(oldBunchPercentage);
//						gsa.setEst_OldBunch(oldBunchDeduction);
//					}
					
					if (MDocType.DOCBASETYPE_RegradedTicket.equals(dt.getDocBaseType()))
					{
						gsa.setFFBReceiptCondition(MUNSGradingSheetAll.FFBRECEIPTCONDITION_GraderOverwritten);
						BigDecimal percentReflection = DB.getSQLValueBD(
								get_TrxName(), 
								"SELECT COALESCE(RottenBunchDeduction, 0) FROM UNS_Order_Contract "
								+ " WHERE C_Order_ID = ? ORDER BY IsActive DESC",
								getC_Order_ID());
						if (null == percentReflection)
						{
							percentReflection = Env.ZERO;
						}
						
						gsa.setPercent_RottenBunch(percentReflection);
					}
					gsa.saveEx();
				} 
			}
			else if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket)
					&& (!isSplitOrder() || (isSplitOrder() && isOriginalTicket())))
			{
				MUNSDespatchNotes despatch = MUNSDespatchNotes.getCreate(this);
				if (null == despatch)
				{
					log.log(Level.SEVERE, "Can't create Despatch Document.");
					return false;
				}
				
				if(getUNS_BPartner_Driver_ID() == 0 && getDriverLicense() != null)
				{
					
					int driver_id = DB.getSQLValue(get_TrxName(), "SELECT UNS_BPartner_Driver_ID FROM UNS_BPartner_Driver WHERE "
							+ "C_BPartner_ID = ? AND Name = ? AND DriverLicense = ? AND isActive = 'Y' " , getTransporter_ID(),
							getVehicleDriverName(), getDriverLicense());
					if(driver_id <= 0)
					{
						driver_id =  DB.getSQLValue(get_TrxName(), "SELECT UNS_BPartner_Driver_ID FROM UNS_BPartner_Driver WHERE "
								+ "C_BPartner_ID = ? AND Name = ? AND isActive = 'Y' " , getTransporter_ID(),
								getVehicleDriverName());
						if(driver_id > 0)
						{
							throw new AdempiereException("Duplicate Name. Please add some letters into the name");
						}
						
						MUNSBPartnerDriver driver = new MUNSBPartnerDriver(getCtx(), 0, get_TrxName());
						driver.setAD_Org_ID(getAD_Org_ID());
						driver.setC_BPartner_ID(getTransporter_ID());
						driver.setName(getVehicleDriverName());
						driver.setDriverLicense(getDriverLicense());
						if(!driver.save())
						{
							log.log(Level.SEVERE, "Can't Create Business Partner Driver");
							return false;
						}
						driver_id = driver.getUNS_BPartner_Driver_ID();
					}
					DB.executeUpdate("UPDATE UNS_WeighbridgeTicket SET UNS_BPartner_Driver_ID = "+driver_id
							+ " "
							+ "WHERE UNS_WeighbridgeTicket_ID = ? ", getUNS_WeighbridgeTicket_ID(), get_TrxName());
				}
			 
			}
		}
		else if (!newRecord && is_ValueChanged(COLUMNNAME_C_Invoice_ID)
				&& dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GradedTicket)
				&& getC_Invoice_ID() == 0)
		{
			int C_InvoiceLine_ID = (Integer) get_ValueOld(COLUMNNAME_C_InvoiceLine_ID);
			MInvoiceLine invLine = new MInvoiceLine(getCtx(), C_InvoiceLine_ID, get_TrxName());
			if (!invLine.delete(false)) {
				return false;
			}
		}
		else if (!newRecord && is_ValueChanged(COLUMNNAME_C_InvoiceLine_ID) 
				&& dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket))
		{
			// Seharusnya masuk ke sini dari modul/window Invoice (POP Customer) saat select ticket.
			
			//String sql = "SELECT SUM(NettoII) FROM UNS_WeighbridgeTicket WHERE C_InvoiceLine_ID=?";
			
			//if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket)) {
			String sql = "SELECT SUM(wbtc.NettoII - wbtc.qtyclaimed) FROM UNS_WBTicket_Confirm wbtc "
						+ " INNER JOIN UNS_WeighbridgeTicket wb "
						+ "		ON wb.UNS_WeighbridgeTicket_ID= wbtc.UNS_WeighbridgeTicket_ID "
						+ " WHERE wb.C_InvoiceLine_ID=?";
			//}
			
			int C_InvoiceLine_ID = getC_InvoiceLine_ID() == 0 ? 
					(Integer) get_ValueOld(COLUMNNAME_C_InvoiceLine_ID) : getC_InvoiceLine_ID();
			
			BigDecimal totalQty = DB.getSQLValueBDEx(get_TrxName(), sql, C_InvoiceLine_ID);
	
			//ketika unselect id invoiceline null. totalQty = null
//			BigDecimal totalQty = DB.getSQLValueBDEx(get_TrxName(), sql, getC_InvoiceLine_ID());
			
			if (totalQty == null)
			{
				sql = "SELECT COUNT(*) FROM UNS_WeighbridgeTicket WHERE C_InvoiceLine_ID=?";
				int countTickets = DB.getSQLValueEx(get_TrxName(), sql, getC_InvoiceLine_ID());;
				
				if (countTickets == 0)
					totalQty = Env.ZERO;
				else {
					log.saveError("CannotCountTotalQty", CLogger.retrieveErrorString("CannotCountTotalQty"));
					return false;
				}
			}
			
			MInvoiceLine invLine = new MInvoiceLine(getCtx(), C_InvoiceLine_ID, get_TrxName());
			invLine.setQty(totalQty);
			invLine.saveEx();
		}
		
		m_isOnSortTabRecordAction = false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String processAction) throws Exception 
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		//setProcessing(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		StringBuilder errInfo = new StringBuilder();
		
		if (getTimeIn() == null || getTimeOut() == null) {
			errInfo.append("Time-In and Time-Out are mandatory;");
		}
		else {
			boolean timeInOutValid = getTimeOut().after(getTimeIn());
			if (!timeInOutValid) {
				errInfo.append("Time-Out greater than Time-In;");
			}
		}
		
		if (getGrossWeight().signum() <= 0 || getTare().signum() == 0) {
			errInfo.append("Gross Weight & Tare are mandatory;");
		}
		
		if(getM_Warehouse_ID() <= 0){
			errInfo.append("Warehouse is mandatory");
		}
		
		if(getM_Locator_ID() <= 0){
			errInfo.append("Locator is mandatory");
		}
		
		if(getC_Order_ID() > 0 && getM_Product_ID() <= 0)
		{
			errInfo.append("Product is mandatory");
		}
		
		if (errInfo.length() > 0)
		{
			m_processMsg = errInfo.toString();
			return DOCSTATUS_Invalid;
		}
		
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		
		if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket))
		{
//			if (!isSplitOrder() || (isSplitOrder() && isOriginalTicket()))
//			{
//				MUNSWBTicketConfirm confirm = MUNSWBTicketConfirm.getCreate(getCtx(), this, get_TrxName());
//				
//				if (confirm == null){
//					m_processMsg = "Failed preparing document. Cannot create Customer's Ticket Confirmation document.";
//					return DOCSTATUS_Invalid;
//				}
//			}
//			
			//MUNSWBTicketConfirm.getCreate(getCtx(), this, get_TrxName()); 
			//MUNSDespatchNotes despatchNotes = 
			
			if (isSplitOrder() && isOriginalTicket() && !isReplication())
			{
				MUNSWeighbridgeTicket splittedTicket = new MUNSWeighbridgeTicket(
						getCtx(), getSplittedTicket_ID(), get_TrxName());
				if (splittedTicket.getUNS_WeighbridgeTicket_ID() == 0)
				{
					PO.copyValues(this, splittedTicket);
				}
				
				BigDecimal splitNetto1 = getOriginalNettoI().subtract(getNettoI());
				BigDecimal splitNetto2 = getOriginalNettoII().subtract(getNettoII());
				
				String sql = "SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID=? AND M_Product_ID=?";
				int newOL = DB.getSQLValueEx(get_TrxName(), sql, getSplittedOrder_ID(), getM_Product_ID());
				
				splittedTicket.setClientOrg(this);
				splittedTicket.setDocumentNo(getDocumentNo() + "_"); 
				splittedTicket.setNettoI(splitNetto1);
				splittedTicket.setNettoII(splitNetto2);
				splittedTicket.setOriginalReflection(getOriginalReflection());
				splittedTicket.setIsOriginalTicket(false);
				splittedTicket.setSplittedOrder_ID(this.getC_Order_ID());
				splittedTicket.setC_Order_ID(this.getSplittedOrder_ID());
				splittedTicket.setC_OrderLine_ID(newOL);
				splittedTicket.setSplittedTicket_ID(this.get_ID());
				
				if (!splittedTicket.save()) {
					m_processMsg = "Failed preparing document. "
							+ "Cannot create/update the splitted ticket for this ticket.";
					return DocAction.STATUS_Invalid;
				}
				
				this.setSplittedTicket_ID(splittedTicket.get_ID());
			}
			
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	} // prepareIt
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		if (!isReplication() && getDocAction().equals(DOCACTION_Prepare))
		{
			return DOCSTATUS_InProgress;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		StringBuilder info = new StringBuilder();
		String replica = MSysConfig.getValue(MSysConfig.IS_REPLICA, "N");
		boolean isReplica = "Y".equals(replica);
		
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		
		if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket))
		{	
			if (!isReplication())
			{
				MUNSDespatchNotes despatch = MUNSDespatchNotes.getCreate(this);
				
				despatch.calculateQtyPortion();
				if(despatch.isOverwriteDespatchTime())
				{
					despatch.setTimeInNumber(getTimeInNumber());
					despatch.setStartDespatch(getTimeIn());
					despatch.setTimeOutNumber(getTimeOutNumber());
					despatch.setEndDespatch(getTimeOut());
				}
				despatch.saveEx();
				
				if (!despatch.getDocStatus().equals(DOCSTATUS_Completed)
						&& !despatch.getDocStatus().equals(DOCSTATUS_Closed)
						&& !isForce()) 
				{
					info.append("Please complete Despatch-Notes No#" + despatch.getDocumentNo());
					m_processMsg = info.toString();
					setProcessed(true);
					return DocAction.STATUS_InProgress;
				}
				
				BigDecimal nettoII = isSplitOrder() ? getOriginalNettoII() : getNettoII();
				
				if(nettoII.compareTo(despatch.getTotalQty()) != 0)
				{
					info.append("Total Qty in Despatch-Notes is not same with the original ticket. Despatch-Notes No#"+despatch.getDocumentNo());
					return DocAction.STATUS_InProgress;	
				}
				
			}
			
			if (isSplitOrder() && isOriginalTicket()) {
				MUNSWeighbridgeTicket splittedTicket = new MUNSWeighbridgeTicket(
						getCtx(), getSplittedTicket_ID(), get_TrxName()); 

				if (!splittedTicket.getDocStatus().equals(DOCSTATUS_Completed)
						&& !splittedTicket.getDocStatus().equals(DOCSTATUS_Closed)) 
				{
					splittedTicket.setSplittedTicket_ID(get_ID());
					splittedTicket.setForce(isForce());
					try {
						if (!splittedTicket.processIt(DOCACTION_Complete)) {
							m_processMsg = "Failed completing document. Cannot auto complete the splitted ticket. "
									+ "Please try to complete it manually Document No #" 
									+ splittedTicket.getDocumentNo();
							return DOCSTATUS_Invalid;
						}
					}
					catch (Exception ex) {
						ex.printStackTrace();
						m_processMsg = ex.getMessage();
						return DOCSTATUS_Invalid;
					}
				}
				
				if (!splittedTicket.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Could not save split ticket.");
					return DOCSTATUS_Invalid;
				}
			}
			
			if (!isReplica) 
			{
				if (isSplitOrder() && !isOriginalTicket() && getSplittedTicket_ID() == 0)
				{
					m_processMsg = "Waiting Original Ticket.";
					return DOCSTATUS_InProgress;
				}
				m_processMsg = move(false, getDateDoc());
				if (null != m_processMsg)
				{
					return DOCSTATUS_Invalid;
				}
				
				MUNSWBTicketConfirm confirm = MUNSWBTicketConfirm.getCreate(getCtx(), this, get_TrxName());
				
				if (confirm == null){
					m_processMsg = "Failed preparing document. Cannot create Customer's Ticket Confirmation document.";
					return DOCSTATUS_Invalid;
				}
		
			}
			
			
//			MUNSWBTicketConfirm customerWBConfirm = MUNSWBTicketConfirm.getCreate(getCtx(), this, get_TrxName());
//			
//			if (!customerWBConfirm.getDocStatus().equals(DOCSTATUS_Completed)
//					&& !customerWBConfirm.getDocStatus().equals(DOCSTATUS_Closed)) 
//			{
//				info.append("Please complete Customer's Weighbridge Ticket confirmation #" 
//					+ customerWBConfirm.getDocumentNo());
//				m_processMsg = info.toString();
//				setProcessed(true);
//				return DocAction.STATUS_InProgress;
//			}
		}
		else if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GradedTicket))
		{
			MUNSGradingSheetAll grading = MUNSGradingSheetAll.get(getCtx(), get_ID(), get_TrxName());
			
			if (!grading.getDocStatus().equals(DOCSTATUS_Completed) &&
					!grading.getDocStatus().equals(DOCSTATUS_Closed))
			{
				m_processMsg = "Cannot complete ticket. Please complete the Grading Sheet first.";
				return DOCSTATUS_InProgress;
			}
		}
		
		if (!dt.getDocBaseType().equals(MDocType.DOCBASETYPE_GeneralWeighbridgeTicket)
				&& ! MDocType.DOCBASETYPE_RegradedTicket.equals(dt.getDocBaseType())
				&& !dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket)
				&& !isReplica)
		{
			m_processMsg = processShipReceipt(DOCACTION_Complete);
			if (m_processMsg != null)
			{
				return DOCSTATUS_Invalid;
			}
		}
		
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		
		if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket)) {
			//String invoiceCreationMsg = generateInvoice();
			//info.append(invoiceCreationMsg);
		}
		
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			if (info.length() > 0)
				info.append(" - ");
			info.append(valid);
			m_processMsg = info.toString();
			return DocAction.STATUS_Invalid;
		}

		setProcessed(true);	
		m_processMsg = info.toString();
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt
	
	
	/**
	 * Generate ship/receipt based on this ticket.
	 * @return return an error message if generate process was failed, or null if success.
	 */
	public MInOut generateShipReceipt()
	{
		if (getC_Order_ID() == 0)
			return null;
		
		MOrder order = (MOrder) getC_Order();
		
		MInOut io = new MInOut(getCtx(), getM_InOut_ID(), get_TrxName());
		if (io.isComplete())
		{
			return io;
		}
		else if (io.get_ID() == 0)
		{
			io.setAD_Org_ID(getAD_Org_ID());
			io.setIsSOTrx(isSOTrx());
			io.setC_BPartner_ID(getC_BPartner_ID());
			io.setC_BPartner_Location_ID(order.getC_BPartner_Location_ID());
			
			MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
			
			if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_EmptyBunchTicket)) 
			{
				String sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType=? AND Name=?";
				int C_DocType_ID = DB.getSQLValueEx(
						get_TrxName(), sql, MDocType.DOCBASETYPE_MaterialDelivery, "MM Vendor Return");
				io.setC_DocType_ID(C_DocType_ID);
				io.setMovementType(MInOut.MOVEMENTTYPE_VendorReturns);
			}
			else {
				io.setC_DocType_ID();
				io.setMovementType(isSOTrx()? MInOut.MOVEMENTTYPE_CustomerShipment : MInOut.MOVEMENTTYPE_VendorReceipts);
			}
			
			
			io.setMovementDate(getDateDoc());
			io.setDateAcct(getDateDoc());
			
			io.setC_Order_ID(getC_Order_ID());
			io.setAD_User_ID(getAD_User_ID());
			io.setM_Warehouse_ID(getM_Warehouse_ID());
			io.setSalesRep_ID(getAD_User_ID());
			io.setDeliveryViaRule(MInOut.DELIVERYVIARULE_Delivery);
			io.saveEx();
		}
		
		MInOutLine[] ioLines = io.getLines();
		MInOutLine theIOL = null;
		
		for (MInOutLine iol : ioLines)
		{
			if (iol.getM_Product_ID() == getM_Product_ID())
				theIOL = iol;
		}
		
		if (theIOL == null)
		{
			if (ioLines.length > 0)
				theIOL = ioLines[0];
			else
				theIOL = new MInOutLine(io);
		}
		
		String sql = "SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID=? AND M_Product_ID=?";
		int C_OrderLine_ID = DB.getSQLValueEx(get_TrxName(), sql, order.get_ID(), getM_Product_ID());
		
		BigDecimal qty = getNettoII();
		
//		if (order.isSOTrx()) {
//			MUNSWBTicketConfirm ticketConfirm = MUNSWBTicketConfirm.get(getCtx(), get_ID(), get_TrxName());
//			if (ticketConfirm != null) {
//				qty = getNettoII();
//			}
//		}
		
		int M_Locator_ID = 0;
		if (order.isSOTrx())
		{
			M_Locator_ID = ((MWarehouse) getM_Warehouse()).
					getIntransitCustomerLocator_ID(true);
		}
		else
		{
			M_Locator_ID = getM_Locator_ID();
			qty = getNettoI();
		}
		
		theIOL.setC_OrderLine_ID(C_OrderLine_ID);
		theIOL.setM_Product_ID(getM_Product_ID());
		theIOL.setC_UOM_ID(MProduct.get(getCtx(), getM_Product_ID()).getC_UOM_ID());
		theIOL.setM_Locator_ID(M_Locator_ID);
		theIOL.setQty(qty);
		theIOL.saveEx();
		
		MUNSWeighBridgeTicketMA[] mas = MUNSWeighBridgeTicketMA.get(get_TrxName(), get_ID());
		for (int i=0; i<mas.length; i++) {
			MInOutLineMA ioLineMA = new MInOutLineMA(
					theIOL, mas[i].getM_AttributeSetInstance_ID(), 
					mas[i].getMovementQty(), mas[i].getDateMaterialPolicy(), 
					false);
			ioLineMA.saveEx();
		}
		
		return io;
	}
	
	/**
	 * 
	 * @return
	 */
	@Deprecated
	String generateInvoice()
	{
		MOrder order = new MOrder(getCtx(), getC_Order_ID(), get_TrxName());
		List<MOrderLine> beInvoicedLines = new ArrayList<MOrderLine>();
		
		if (order.getInvoiceRule().equals(MOrder.INVOICERULE_ImmediateAfterAllLineDelivered))
		{
			MOrderLine[] orderLines = order.getLines();
			for (MOrderLine oLine : orderLines)
			{
				if (oLine.getM_Product_ID() == getM_Product_ID() 
						&& oLine.getQtyDelivered().compareTo(oLine.getQtyOrdered()) >= 0)
				{
					beInvoicedLines.add(oLine);
					break;
				}
			}
		}
		else if (order.getInvoiceRule().equals(MOrder.INVOICERULE_ImmediateAfterAllLineDelivered))
		{
			MOrderLine[] orderLines = order.getLines();
			for (MOrderLine oLine : orderLines)
			{
				if (oLine.getQtyDelivered().compareTo(oLine.getQtyOrdered()) < 0)
				{
					return null;
				}
				beInvoicedLines.add(oLine);
			}
		}
		else if (order.getInvoiceRule().equals(MOrder.INVOICERULE_AfterDelivery)
				|| order.getInvoiceRule().equals(MOrder.INVOICERULE_Immediate))
		{
			MOrderLine oLine = 
					Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
							"C_Order_ID=? AND M_Product_ID=?", get_TrxName())
						.setParameters(getC_Order_ID(), getM_Product_ID())
						.first();
			
			if (oLine == null)
				return "Cannot create invoice after delivery, because the product of " 
						+ MProduct.get(getCtx(), getM_Product_ID()) + " not included on the contract. "
								+ "\nPlease fixing the contract, and create the invoice manually.";
			
			beInvoicedLines.add(oLine);
		}
		
		if (beInvoicedLines.size() == 0)
			return null;
		
		MUNSWBTicketConfirm ticketConfirm = MUNSWBTicketConfirm.get(getCtx(), get_ID(), get_TrxName());
		
		int ARDocType_ID = MDocType.getDocType(MDocType.DOCBASETYPE_ARInvoice);
		
		MInvoice inv = new MInvoice(order, ARDocType_ID, ticketConfirm.getTimeOut());
		
		if (!inv.save()) {
			return "Cannot create invoice. It's failed when saving the created invoice. \n Error: " 
					+ CLogger.retrieveErrorString("Unknown error. \nPlease create it manually");
		}
		
		for (MOrderLine orderLine : beInvoicedLines)
		{
			MInvoiceLine invLine = new MInvoiceLine(inv);
			
			invLine.setOrderLine(orderLine);
			
			BigDecimal qtyToInvoice = orderLine.getQtyDelivered();
			
			if (order.getInvoiceRule().equals(MOrder.INVOICERULE_AfterDelivery)
					|| order.getInvoiceRule().equals(MOrder.INVOICERULE_Immediate))
				qtyToInvoice = this.getNettoII();
			
			invLine.setQty(qtyToInvoice);//.add(getNettoII()));
			
			if (!invLine.save()) {
				return "Cannot create invoice line for product [" + MProduct.get(getCtx(), getM_Product_ID()) +
						"]. It's failed when saving the line. \n Error: " 
						+ CLogger.retrieveErrorString("Unknown error. \nPlease create it manually");
			}
		}
		
		if (!inv.processIt(DOCACTION_Complete))
		{
			return "Failed when completing the new invoice. Please create it manually. " 
					+ CLogger.retrieveErrorString("Unknown error.");
		}
		
		inv.saveEx();
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		String replica = MSysConfig.getValue(MSysConfig.IS_REPLICA, "N");
		boolean isReplica = "Y".equals(replica);
		
		if(null != existinInInvoice())
			return false;
		
		if (getC_DocType().getDocBaseType().equals(MDocType.DOCBASETYPE_SalesTicket))
		{

			MUNSWBTicketConfirm tConfirm = MUNSWBTicketConfirm.get(
					getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
			if (null != tConfirm)
			{
				tConfirm.setReplication(isReplication());
				if (tConfirm.getDocStatus().equals(DOCSTATUS_Completed)
						|| tConfirm.getDocStatus().equals(DOCSTATUS_Closed)
						|| tConfirm.getDocStatus().equals(DOCSTATUS_InProgress)
						|| tConfirm.getDocStatus().equals(DOCSTATUS_Approved))
				{
					m_processMsg = "Please void customer ticket confirmation first";
					return false;
				}
				else if (tConfirm.getDocStatus().equals(DOCSTATUS_Drafted))
				{
					try
					{
						boolean ok = tConfirm.processIt(DOCACTION_Void);
						if (!ok)
						{
							m_processMsg = tConfirm.getProcessMsg();
							return false;
						}
						
						tConfirm.saveEx();
					}
					catch (Exception ex)
					{
						m_processMsg = ex.getMessage();
						return false;
					}
				}
			}
			
			MUNSDespatchNotes despatch = MUNSDespatchNotes.get(this);
			
			if (null != despatch && !despatch.getDocStatus().
					equals(DOCSTATUS_Voided)
					&& !despatch.getDocStatus().equals(DOCSTATUS_Reversed)
					&& !isForce() && !isReplication())
			{
				try
				{
					setForce(true);
					boolean ok = despatch.processIt(DOCACTION_Void);
					if (!ok)
					{
						m_processMsg = despatch.getProcessMsg();
						return false;
					}
					setForce(false);
					despatch.saveEx();
				}
				catch (Exception ex)
				{
					m_processMsg = ex.getMessage();
					return false;
				}
			}
			
			if (!isReplica && DOCSTATUS_Completed.equals(getDocStatus()) && !isCancelled()) {
				m_processMsg = move(true, getDateDoc());
				if (null !=  m_processMsg)
				{
					return false;
				}
			}
		}
		else if (getC_DocType().getDocBaseType()
				.equals(MDocType.DOCBASETYPE_GradedTicket) && !isReplication())
		{
			MUNSGradingSheetAll grading = MUNSGradingSheetAll.get(
					getCtx(), get_ID(), get_TrxName());
			String status = grading.getDocStatus();
			if (!DOCSTATUS_Reversed.equals(status)
					&& !DOCSTATUS_Voided.equals(status))
			{
				try
				{
					boolean success = grading.processIt(DOCACTION_Void);
					if (!success)
					{
						m_processMsg = "Failed when trying to void grading. "
								+ grading.getProcessMsg();
						return false;
					}
					grading.saveEx();
					
					if  (getM_InOut_ID() > 0)
					{
						MInOut io = (MInOut) getM_InOut();
						boolean ok = io.processIt(DOCACTION_Void);
						if(!ok)
							m_processMsg = io.getProcessMsg();
						if (null != m_processMsg)
						{
							return false;
						}
					}
				}
				catch (Exception ex)
				{
					m_processMsg = ex.getMessage();
					return false;
				}
			}
		}
		
		
		if (isSplitOrder() && !isReplication()) {
			MUNSWeighbridgeTicket splittedTicket = 
					Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
							"SplittedTicket_ID=" + get_ID(), get_TrxName())
					.first();

			if (this.isOriginalTicket() 
					&& !splittedTicket.getDocStatus().equals(DOCSTATUS_Voided)
					&& !splittedTicket.getDocStatus().equals(DOCSTATUS_Reversed)) 
			{
				splittedTicket.setForce(true);
				try {
					if (!splittedTicket.processIt(DOCACTION_Void)) {
						m_processMsg = "Failed when try to void splitted ticket. Document No #" 
								+ splittedTicket.getDocumentNo() +"#" + splittedTicket.getProcessMsg();
						return false;
					}
					splittedTicket.saveEx();
				}
				catch (Exception ex) {
					ex.printStackTrace();
					m_processMsg = ex.getMessage();
					return false;
				}
			}
			else if (!this.isOriginalTicket()						
					&& !splittedTicket.getDocStatus().equals(DOCSTATUS_Voided)
					&& !splittedTicket.getDocStatus().equals(DOCSTATUS_Reversed)
					&& !isForce()) 
			{
				m_processMsg = "Please void original ticket first. the original ticket of Document No #" 
						+ splittedTicket.getDocumentNo() + " first!";
				return false;
			}
		}
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		for (int i=0; i<100; i++)
		{
			String docNo = getDocumentNo() + "_VO" + i;
			StringBuilder sqlBuilder = new StringBuilder("SELECT 1 FROM ")
			.append(Table_Name).append(" WHERE ").append(COLUMNNAME_DocumentNo)
			.append(" = ? ");
			String sql = sqlBuilder.toString();
			int value = DB.getSQLValue(get_TrxName(), sql, docNo);
			if (value > 0)
				continue;
			
			setDocumentNo(docNo);
			
			break;
		}
		String addDescription = "**Voided By " + Env.getContext(getCtx(), "#AD_User_Name") + "**";
		setDescription(getDescription() == null ? addDescription : getDescription() + " || " + addDescription);
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;	

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		//	: Grand Total = 123.00 (#1)
		sb.append(": ").
			append("Netto-1").append("=").append(getNettoI())
			.append("Netto-2").append("=").append(getNettoII()).append(")");
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo()
	{
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		return dt.getNameTrl() + " " + getDocumentNo();
	}	//	getDocumentInfo

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
		ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.SHIPMENT, getC_Order_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if(format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo ("", format.getJasperProcess_ID());
			pi.setRecord_ID ( getC_Order_ID() );
			pi.setIsBatch(true);
			
			ServerProcessCtl.process(pi, null);
			
			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	}	//	createPDF
	
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{	
		return getAD_User_ID();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt()
	{
		return getNettoII();
	}	//	getApprovalAmt

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index)
	{
		// If status = Completed, add "Void" in the list
		if (docStatus.equals(DocumentEngine.STATUS_Completed))
		{
			options[index++] = DocumentEngine.ACTION_Void;
		}	
		return index;
	}
	
	public String existinInInvoice()
	{
		m_processMsg = null;
		if(getC_InvoiceLine() != null)
		{
			String sql = "SELECT DocumentNo FROM C_Invoice WHERE C_Invoice_ID = (SELECT C_Invoice_ID FROM C_InvoiceLine WHERE"
					+ " C_InvoiceLine_ID=?) AND DocStatus NOT IN ('VO', 'RE')";
			String docNo = DB.getSQLValueString(get_TrxName(), sql, getC_InvoiceLine_ID());
			
			if(null != docNo)
				m_processMsg = "Ticket have been used in the invoice document no " + docNo;
		}
		return m_processMsg;
	}

	public String move (boolean isVoid, Timestamp Date)
	{
		BigDecimal movementQty = getNettoII();
		if (movementQty.compareTo(Env.ZERO) == -1) {
			return "Over MA Qty";
		} else if (!isVoid) {
			try
			{
				checkMaterialPolicy(movementQty);
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
			}
			
			if (null != m_processMsg) {
				return m_processMsg;
			}
		}
		
		MUNSWeighBridgeTicketMA[] mas = MUNSWeighBridgeTicketMA.get(
				get_TrxName(), get_ID());

		MWarehouse whs = MWarehouse.get(getCtx(), getM_Warehouse_ID());
		int intransitLoc_ID = whs.getIntransitCustomerLocator_ID(true);
		
		for (int i=0; i<mas.length; i++)
		{
			MUNSWeighBridgeTicketMA ma = mas[i];

			if (!MStorageOnHand.add(getCtx(), whs.get_ID(), 
					ma.getM_Locator_ID(), getM_Product_ID(), 
					ma.getM_AttributeSetInstance_ID(), 
					isVoid ? ma.getMovementQty() : ma.getMovementQty().negate(), 
							ma.getDateMaterialPolicy(), 
					get_TrxName()))
			{
				String sql = "SELECT Concat(Value,'-',Name) From M_Product WHERE M_Product_ID = ?";
				String info = DB.getSQLValueString(null, sql, getM_Product_ID());
				if (null == info)
				{
					info = "unknown";
				}
				
				return "Can't correct inventory onhand (MA) " + info;
			}
			
			MTransaction trxTo = new MTransaction(getCtx(), getAD_Org_ID(), 
					MTransaction.MOVEMENTTYPE_SOWeighbridgeTicket_,
					ma.getM_Locator_ID(), getM_Product_ID(), 
					ma.getM_AttributeSetInstance_ID(), 
					isVoid ? ma.getMovementQty() : ma.getMovementQty().negate(), 
					Date, get_TrxName());
			trxTo.setUNS_WeighbridgeTicket_ID (get_ID());
			try
			{
				trxTo.saveEx();
			}
			catch (Exception ex)
			{
				String msg = ex.getMessage();
				if (null == msg) msg = "";
				return "Failed when try to create transaction :: " + msg;
			}
			
			if (!MStorageOnHand.add(getCtx(), whs.get_ID(), 
					intransitLoc_ID, getM_Product_ID(), 
					ma.getM_AttributeSetInstance_ID(), 
					isVoid ? ma.getMovementQty().negate() :ma.getMovementQty(), 
							ma.getDateMaterialPolicy(), 
					get_TrxName()))
			{
				String sql = "SELECT Concat(Value,'-',Name) From M_Product WHERE M_Product_ID = ?";
				String info = DB.getSQLValueString(null, sql, getM_Product_ID());
				if (null == info)
				{
					info = "unknown";
				}
				
				return "Can't correct inventory onhand (MA) " + info;
			}
			
			//TODO add transaction type fofr weighbride and 
			//uns_weighbridgeticket_id on m_transaction
			MTransaction trxFrom = new MTransaction(getCtx(), getAD_Org_ID(), 
					MTransaction.MOVEMENTTYPE_SOWeighbridgeTicketPlus,
					intransitLoc_ID, getM_Product_ID(), 
					ma.getM_AttributeSetInstance_ID(), 
					isVoid ? ma.getMovementQty().negate() :ma.getMovementQty(), 
					Date, get_TrxName());
			trxFrom.setUNS_WeighbridgeTicket_ID (get_ID());
			try
			{
				trxFrom.saveEx();
			}
			catch (Exception ex)
			{
				String msg = ex.getMessage();
				if (null == msg) msg = "";
				return "Failed when try to create transaction :: " + msg;
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param movementQty
	 */
	private void checkMaterialPolicy (BigDecimal movementQty)
	{
		int recordDel = MUNSWeighBridgeTicketMA.deleteAutoGenerated(
				get_TrxName(), get_ID());
		if (recordDel > 0) {
			log.info(recordDel + " MA are Deleted");
		} 
		if (movementQty.signum() == 0) {
			return;
		}
		
		MUNSDespatchNotes note = MUNSDespatchNotes.get(this);
		if (null == note) {
			m_processMsg = "Could not find Despatch Note Document.";
			return;
		}
		Vector<Object[]> locatorQtyList = note.getListQtyByLocator();
		BigDecimal qtyToDeliver = movementQty;
		MUNSWeighBridgeTicketMA[] manuals = MUNSWeighBridgeTicketMA.get(
				get_TrxName(), get_ID());
		MUNSWeighBridgeTicketMA[]  splitmas = MUNSWeighBridgeTicketMA.get(
				get_TrxName(), getSplittedTicket_ID());
		
		for (int i=0; i<locatorQtyList.size(); i++) {
			MStorageOnHand[] onhands = MStorageOnHand.getOfLocator(
					getCtx(), getM_Product_ID(), 
					(Integer) locatorQtyList.get(i)[0], get_TrxName());
			BigDecimal partialQtyToDeliver = (BigDecimal) locatorQtyList.get(i)[1];
			for (int k=0; k<splitmas.length; k++) {
				if (splitmas[k].getM_Locator_ID() == (Integer) locatorQtyList.get(i)[0]) {
					partialQtyToDeliver = partialQtyToDeliver.subtract(
							splitmas[k].getMovementQty());
				}
			}
			BigDecimal comparator = partialQtyToDeliver;
			if (comparator.signum() == 0) {
				continue;
			} else if (comparator.compareTo(qtyToDeliver) == 1) {
				comparator = qtyToDeliver;
			}
			qtyToDeliver = qtyToDeliver.subtract(comparator);
			partialQtyToDeliver = partialQtyToDeliver.subtract(comparator);
			
			for (int j=0; j<onhands.length && comparator.signum() > 0; j++) {
				MStorageOnHand onhand = onhands[j];
				for (int k=0; k<manuals.length; k++) {
					if (manuals[k].getM_Locator_ID() == onhand.getM_Locator_ID()
							&& manuals[k].getM_AttributeSetInstance_ID() == 
							onhand.getM_AttributeSetInstance_ID() &&
							manuals[k].getDateMaterialPolicy().equals(
									onhand.getDateMaterialPolicy())) {
						onhand.setQtyOnHand(onhand.getQtyOnHand().
								subtract(manuals[k].getMovementQty()));
						comparator = comparator.subtract(
								manuals[k].getMovementQty());
					}
				}
				
				if (onhand.getQtyOnHand().compareTo(comparator) >= 0) {
					MUNSWeighBridgeTicketMA ma = new MUNSWeighBridgeTicketMA(
							this,onhand.getM_Locator_ID(), 
							onhand.getM_AttributeSetInstance_ID(), 
							comparator, 
							onhand.getDateMaterialPolicy(), true);
					ma.saveEx();
					comparator = Env.ZERO;
				}
				else
				{
					MUNSWeighBridgeTicketMA ma = MUNSWeighBridgeTicketMA
							.addOrCreate(
									this, onhand.getM_Locator_ID(), 
									onhand.getM_AttributeSetInstance_ID(), 
									onhand.getQtyOnHand(), 
									onhand.getDateMaterialPolicy());
					ma.saveEx();
					comparator = comparator.subtract(
							onhand.getQtyOnHand());
				}
				
				if (comparator.signum() == 0)
				{
					break;
				}
			}
			
			if (comparator.signum() != 0) {
				MUNSWeighBridgeTicketMA ma = MUNSWeighBridgeTicketMA.addOrCreate(
						this, (Integer) locatorQtyList.get(i)[0], 0, 
						comparator, getDateDoc());
				ma.saveEx();
			} else if (qtyToDeliver.signum() == 0) {
				break;
			}
		}
		
		if (qtyToDeliver.signum() != 0 && isOriginalTicket()) {
			m_processMsg = "Could not match Netto II and proportion on despatch";
		}
	}
	
	public void setForce (boolean force)
	{
		this.m_force = force;
	}
	
	public boolean isForce ()
	{
		return this.m_force;
	}
	
	public String processShipReceipt (String action)
	{
		MInOut io = generateShipReceipt();
		if (io == null)
		{
			return "Failed when creating " + (isSOTrx()? "Shipment" : "Receipt");			
		}
		
		setM_InOut_ID(io.get_ID());
		saveEx();
		String shipReceipt = isSOTrx()? "Shipment" : "Receipt";
		
		if (isValidAction(io.getDocStatus(), action))
		{
			boolean success = io.processIt(action);
			if (!success)
			{
				return "Auto complete " + shipReceipt + " failed: " 
						+ io.getProcessMsg();
			}
			
			io.saveEx();
		}
		
		return null;
	}
	
	private boolean isValidAction (String status, String action)
	{
		if (DOCACTION_Complete.equals(action))
		{
			return !DOCSTATUS_Completed.equals(status)
					&& !DOCSTATUS_Reversed.equals(status)
					&& !DOCSTATUS_Voided.equals(status);
		}
		else if (DOCACTION_Void.equals(action))
		{
			return !DOCSTATUS_Reversed.equals(status)
					&& !DOCSTATUS_Voided.equals(status);
		}
		else if (DOCACTION_Reverse_Correct.equals(action)
				|| DOCACTION_Reverse_Accrual.equals(action))
		{
			return DOCSTATUS_Completed.equals(status);
		}
		
		return false;
	}

	private boolean m_isOnSortTabRecordAction = false;
	
	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) {
		m_isOnSortTabRecordAction = true;
		return null;
	}

	@Override
	public String beforeRemoveSelection() {
		m_isOnSortTabRecordAction = true;
		return null;
	}
	
	
	protected BigDecimal analyzeInvoiceTicketQty ()
	{
		if(getRevisionQty().signum() != 0)
			return getQtyPayment();
		
		BigDecimal qty = getNettoI();
		BigDecimal invoiceFlatDeduction = DB.getSQLValueBD(
				get_TrxName(), " SELECT MAX(InvoiceFlatDeduction) "
						+ " FROM UNS_Order_Contract WHERE C_Order_ID = ? "
						+ " AND IsActive = 'Y'", 
				getC_Order_ID());
		
		if(null==invoiceFlatDeduction)
			invoiceFlatDeduction = Env.ZERO;
		
		BigDecimal deduction = Env.ZERO;
		
		if (invoiceFlatDeduction.signum() > 0)
		{
			deduction = qty.multiply(invoiceFlatDeduction);
			deduction = deduction.divide(Env.ONEHUNDRED, 10, 
					RoundingMode.HALF_UP);
			deduction = deduction.setScale(0, RoundingMode.HALF_UP);
		}
		else
		{
			deduction = getReflection();
		}

		qty = qty.subtract(deduction);
		
		return qty;
	}
	
	public int getM_Movement_ID ()
	{
		if (m_M_Movement_ID == 0)
		{
			String sql = "SELECT M_Movement_ID FROM M_MovementLine WHERE "
					+ " M_MovementLine_ID = ?";
			m_M_Movement_ID = DB.getSQLValue(get_TrxName(), sql, 
					getM_MovementLine_ID());
		}
		
		return m_M_Movement_ID;
	}
	
	public void setM_Movement_ID (int M_Movement_ID)
	{
		m_M_Movement_ID = M_Movement_ID;
	}
	
	@Override
	public void setM_MovementLine_ID(int M_MovementLine_ID)
	{
		if (M_MovementLine_ID != getM_MovementLine_ID())
			setM_Movement_ID(0);
		
		super.setM_MovementLine_ID(M_MovementLine_ID);
	}
	
	public int getM_MovementConfirm_ID ()
	{
		if (m_M_MovementConfirm_ID == 0)
		{
			String sql = "SELECT M_MovementConfirm_ID FROM M_MovementLineConfirm "
					+ " WHERE M_MovementLineConfirm_ID = ?";
			m_M_MovementConfirm_ID = DB.getSQLValue(get_TrxName(), sql, 
					getM_MovementLineConfirm_ID());
		}
		
		return m_M_MovementConfirm_ID;
	}
	
	public void setM_MovementConfirm_ID (int M_MovementConfirm_ID)
	{
		m_M_MovementConfirm_ID = M_MovementConfirm_ID;
	}
	
	@Override
	public void setM_MovementLineConfirm_ID (int M_MovementLineConfirm_ID)
	{
		if (M_MovementLineConfirm_ID != getM_MovementLineConfirm_ID())
			setM_MovementConfirm_ID(0);
		
		super.setM_MovementLineConfirm_ID(M_MovementLineConfirm_ID);
	}
	
}
