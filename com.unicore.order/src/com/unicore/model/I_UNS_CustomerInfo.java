/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_CustomerInfo
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_CustomerInfo 
{

    /** TableName=UNS_CustomerInfo */
    public static final String Table_Name = "UNS_CustomerInfo";

    /** AD_Table_ID=1000398 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BoardingPass */
    public static final String COLUMNNAME_BoardingPass = "BoardingPass";

	/** Set Boarding Pass	  */
	public void setBoardingPass (String BoardingPass);

	/** Get Boarding Pass	  */
	public String getBoardingPass();

    /** Column name C_Country_ID */
    public static final String COLUMNNAME_C_Country_ID = "C_Country_ID";

	/** Set Country.
	  * Country 
	  */
	public void setC_Country_ID (int C_Country_ID);

	/** Get Country.
	  * Country 
	  */
	public int getC_Country_ID();

	public org.compiere.model.I_C_Country getC_Country() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CustomerType */
    public static final String COLUMNNAME_CustomerType = "CustomerType";

	/** Set Customer Type	  */
	public void setCustomerType (String CustomerType);

	/** Get Customer Type	  */
	public String getCustomerType();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name Finger */
    public static final String COLUMNNAME_Finger = "Finger";

	/** Set Finger	  */
	public void setFinger (byte[] Finger);

	/** Get Finger	  */
	public byte[] getFinger();

    /** Column name FlightNo */
    public static final String COLUMNNAME_FlightNo = "FlightNo";

	/** Set Flight Number	  */
	public void setFlightNo (String FlightNo);

	/** Get Flight Number	  */
	public String getFlightNo();

    /** Column name GateNo */
    public static final String COLUMNNAME_GateNo = "GateNo";

	/** Set Gate Number	  */
	public void setGateNo (String GateNo);

	/** Get Gate Number	  */
	public String getGateNo();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name NIK */
    public static final String COLUMNNAME_NIK = "NIK";

	/** Set NIK	  */
	public void setNIK (String NIK);

	/** Get NIK	  */
	public String getNIK();

    /** Column name PassengerType */
    public static final String COLUMNNAME_PassengerType = "PassengerType";

	/** Set Passenger Type	  */
	public void setPassengerType (String PassengerType);

	/** Get Passenger Type	  */
	public String getPassengerType();

    /** Column name PassportNo */
    public static final String COLUMNNAME_PassportNo = "PassportNo";

	/** Set Passport Number	  */
	public void setPassportNo (String PassportNo);

	/** Get Passport Number	  */
	public String getPassportNo();

    /** Column name UNS_AirLine_ID */
    public static final String COLUMNNAME_UNS_AirLine_ID = "UNS_AirLine_ID";

	/** Set Air Line	  */
	public void setUNS_AirLine_ID (int UNS_AirLine_ID);

	/** Get Air Line	  */
	public int getUNS_AirLine_ID();

	public com.unicore.model.I_UNS_AirLine getUNS_AirLine() throws RuntimeException;

    /** Column name UNS_CustomerInfo_ID */
    public static final String COLUMNNAME_UNS_CustomerInfo_ID = "UNS_CustomerInfo_ID";

	/** Set Customer Info	  */
	public void setUNS_CustomerInfo_ID (int UNS_CustomerInfo_ID);

	/** Get Customer Info	  */
	public int getUNS_CustomerInfo_ID();

    /** Column name UNS_CustomerInfo_UU */
    public static final String COLUMNNAME_UNS_CustomerInfo_UU = "UNS_CustomerInfo_UU";

	/** Set UNS_CustomerInfo_UU	  */
	public void setUNS_CustomerInfo_UU (String UNS_CustomerInfo_UU);

	/** Get UNS_CustomerInfo_UU	  */
	public String getUNS_CustomerInfo_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VIPType */
    public static final String COLUMNNAME_VIPType = "VIPType";

	/** Set VIP Type	  */
	public void setVIPType (String VIPType);

	/** Get VIP Type	  */
	public String getVIPType();
}
