/**
 * 
 */
package com.unicore.model;

import java.util.Properties;

import org.compiere.model.Query;

/**
 * @author ALBURHANY
 *
 */
public class MUNSConfirmPrinting extends X_UNS_ConfirmPrinting {

	/**
	 * 
	 */
	private static final long serialVersionUID = -960826190216298655L;

	public MUNSConfirmPrinting(Properties ctx, int UNS_ConfirmPrinting_ID,
			String trxName) {
		super(ctx, UNS_ConfirmPrinting_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSConfirmPrinting get(Properties ctx, int UNS_PL_Confirm_ID, String TypeSL, String trxName)
	{
		String whereClause = "UNS_PL_Confirm_ID = ? AND TypeSL = ?";
		
		MUNSConfirmPrinting confPrint = new Query(ctx, MUNSConfirmPrinting.Table_Name, whereClause, trxName)
											.setParameters(UNS_PL_Confirm_ID, TypeSL).setOrderBy(COLUMNNAME_CopyOfPrint)
													.first();
		return (MUNSConfirmPrinting) confPrint;
	}
}
