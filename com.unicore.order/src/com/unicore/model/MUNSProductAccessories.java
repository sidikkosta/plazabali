/**
 * 
 */
package com.unicore.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author ALBURHANY
 *
 */
public class MUNSProductAccessories extends X_UNS_ProductAccessories {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6418168865930091236L;

	public MUNSProductAccessories(Properties ctx,
			int UNS_ProductAccessories_ID, String trxName) {
		super(ctx, UNS_ProductAccessories_ID, trxName);
	}
	
	public MUNSProductAccessories(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public static MUNSProductAccessories[] getAccessoriesLines(Properties ctx, int M_Product_ID, String trxName)
	{
		ArrayList<MUNSProductAccessories> list = new ArrayList<MUNSProductAccessories>();
		
		String sql = "SELECT UNS_ProductAccessories_ID FROM UNS_ProductAccessories "
				+ "WHERE M_Product_ID = ? AND isActive='Y'";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, M_Product_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add(new MUNSProductAccessories(ctx, rs.getInt(1),
						trxName));
			rs.close();
			pstmt.close();
			pstmt = null;
		} catch (SQLException ex) {
			throw new AdempiereException("Unable to load production details",
					ex);
		} finally {
			DB.close(rs, pstmt);
		}

		MUNSProductAccessories[] retValue = new MUNSProductAccessories[list.size()];
		list.toArray(retValue);
		
		return retValue;
	}
	
	public static MUNSProductAccessories getOfProduct(Properties ctx, int M_Product_ID, int ProductAccessories_ID, String trxName)
	{
		MUNSProductAccessories accessories = null;
		String whereClause = "M_Product_ID= ? AND ProductAccessories_ID = ?";
		accessories = new Query(ctx, Table_Name, whereClause, trxName)
			.setParameters(M_Product_ID, ProductAccessories_ID)
			.first();
		
		return accessories;
	}

	public MUNSAccessoriesLine[] getLines() {
		// TODO Auto-generated method stub
		return getLines();
	}
	
	public boolean beforeSave (boolean newRecord)
	{
		if(getQty().compareTo(Env.ZERO) <= 0)
			return false;
		
		return true;
	}
}