/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSHandleRequests extends X_UNS_HandleRequests {

	String errMsg = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7467140923895281876L;

	public MUNSHandleRequests(Properties ctx, int UNS_HandleRequests_ID,
			String trxName) {
		super(ctx, UNS_HandleRequests_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSHandleRequests (Properties ctx, ResultSet rs, String trxName)
	{
		super (ctx, rs, trxName);
	}
	
	public static MUNSHandleRequests[] getByOrderLine(Properties ctx, int C_OrderLine_ID, String trxName)
	{
		List<MUNSHandleRequests> list = Query.get(
				ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
				COLUMNNAME_C_OrderLine_ID + "=?", trxName).
				setParameters(C_OrderLine_ID).list();
		MUNSHandleRequests[] results = new MUNSHandleRequests[list.size()];
		list.toArray(results);
		return results;
	}
	
	public static MUNSHandleRequests getByMoveLine(Properties ctx, int M_MovementLine_ID, String trxName)
	{
		MUNSHandleRequests handle = null;
		
		String sql = "SELECT UNS_HandleRequests_ID FROM UNS_HandleRequests WHERE M_MovementLine_ID=?";
		int id = DB.getSQLValue(trxName, sql, M_MovementLine_ID);
		
		if(id<=0)
			return null;
		
		handle = new MUNSHandleRequests(ctx, id, trxName);
		
		return handle;
	}
}