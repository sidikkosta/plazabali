/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_SessionCashAccount
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_SessionCashAccount extends PO implements I_UNS_SessionCashAccount, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180925L;

    /** Standard Constructor */
    public X_UNS_SessionCashAccount (Properties ctx, int UNS_SessionCashAccount_ID, String trxName)
    {
      super (ctx, UNS_SessionCashAccount_ID, trxName);
      /** if (UNS_SessionCashAccount_ID == 0)
        {
			setBeginningBalance (Env.ZERO);
// 0
			setBeginningBalanceBase1 (Env.ZERO);
// 0
			setBeginningBalanceBase2 (Env.ZERO);
// 0
			setC_Currency_ID (0);
			setCashDepositAmt (Env.ZERO);
// 0
			setCashDepositAmtB1 (Env.ZERO);
// 0
			setCashDepositAmtB2 (Env.ZERO);
// 0
			setCashIn (Env.ZERO);
// 0
			setCashInBase1 (Env.ZERO);
// 0
			setCashInBase2 (Env.ZERO);
// 0
			setCashOut (Env.ZERO);
// 0
			setCashOutBase1 (Env.ZERO);
// 0
			setCashOutBase2 (Env.ZERO);
// 0
			setEndingBalance (Env.ZERO);
// 0
			setEndingBalanceBase1 (Env.ZERO);
// 0
			setEndingBalanceBase2 (Env.ZERO);
// 0
			setEstimationShortCashierAmt (Env.ZERO);
// 0
			setEstimationShortCashierAmtBase1 (Env.ZERO);
// 0
			setEstimationShortCashierAmtBase2 (Env.ZERO);
// 0
			setIsUsedForChange (false);
// N
			setPayableRefundAmt (Env.ZERO);
// 0
			setShortCashierAmt (Env.ZERO);
// 0
			setShortCashierAmtBase1 (Env.ZERO);
// 0
			setShortCashierAmtBase2 (Env.ZERO);
// 0
			setUNS_POS_Session_ID (0);
			setUNS_POSTerminalCashAcct_ID (0);
			setUNS_SessionCashAccount_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_SessionCashAccount (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_SessionCashAccount[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Beginning Balance.
		@param BeginningBalance 
		Balance prior to any transactions
	  */
	public void setBeginningBalance (BigDecimal BeginningBalance)
	{
		set_Value (COLUMNNAME_BeginningBalance, BeginningBalance);
	}

	/** Get Beginning Balance.
		@return Balance prior to any transactions
	  */
	public BigDecimal getBeginningBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BeginningBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Begining Balance Base 1.
		@param BeginningBalanceBase1 Begining Balance Base 1	  */
	public void setBeginningBalanceBase1 (BigDecimal BeginningBalanceBase1)
	{
		set_Value (COLUMNNAME_BeginningBalanceBase1, BeginningBalanceBase1);
	}

	/** Get Begining Balance Base 1.
		@return Begining Balance Base 1	  */
	public BigDecimal getBeginningBalanceBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BeginningBalanceBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Begining Balance Base 2.
		@param BeginningBalanceBase2 Begining Balance Base 2	  */
	public void setBeginningBalanceBase2 (BigDecimal BeginningBalanceBase2)
	{
		set_Value (COLUMNNAME_BeginningBalanceBase2, BeginningBalanceBase2);
	}

	/** Get Begining Balance Base 2.
		@return Begining Balance Base 2	  */
	public BigDecimal getBeginningBalanceBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BeginningBalanceBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cash Deposit Amount.
		@param CashDepositAmt Cash Deposit Amount	  */
	public void setCashDepositAmt (BigDecimal CashDepositAmt)
	{
		set_Value (COLUMNNAME_CashDepositAmt, CashDepositAmt);
	}

	/** Get Cash Deposit Amount.
		@return Cash Deposit Amount	  */
	public BigDecimal getCashDepositAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashDepositAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Deposit Amount Base 1.
		@param CashDepositAmtB1 Cash Deposit Amount Base 1	  */
	public void setCashDepositAmtB1 (BigDecimal CashDepositAmtB1)
	{
		set_Value (COLUMNNAME_CashDepositAmtB1, CashDepositAmtB1);
	}

	/** Get Cash Deposit Amount Base 1.
		@return Cash Deposit Amount Base 1	  */
	public BigDecimal getCashDepositAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashDepositAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Deposit Amount Base 2.
		@param CashDepositAmtB2 Cash Deposit Amount Base 2	  */
	public void setCashDepositAmtB2 (BigDecimal CashDepositAmtB2)
	{
		set_Value (COLUMNNAME_CashDepositAmtB2, CashDepositAmtB2);
	}

	/** Get Cash Deposit Amount Base 2.
		@return Cash Deposit Amount Base 2	  */
	public BigDecimal getCashDepositAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashDepositAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash In.
		@param CashIn Cash In	  */
	public void setCashIn (BigDecimal CashIn)
	{
		set_Value (COLUMNNAME_CashIn, CashIn);
	}

	/** Get Cash In.
		@return Cash In	  */
	public BigDecimal getCashIn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashIn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash In Base 1.
		@param CashInBase1 Cash In Base 1	  */
	public void setCashInBase1 (BigDecimal CashInBase1)
	{
		set_Value (COLUMNNAME_CashInBase1, CashInBase1);
	}

	/** Get Cash In Base 1.
		@return Cash In Base 1	  */
	public BigDecimal getCashInBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashInBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash In Base 2.
		@param CashInBase2 Cash In Base 2	  */
	public void setCashInBase2 (BigDecimal CashInBase2)
	{
		set_Value (COLUMNNAME_CashInBase2, CashInBase2);
	}

	/** Get Cash In Base 2.
		@return Cash In Base 2	  */
	public BigDecimal getCashInBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashInBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Out.
		@param CashOut Cash Out	  */
	public void setCashOut (BigDecimal CashOut)
	{
		set_Value (COLUMNNAME_CashOut, CashOut);
	}

	/** Get Cash Out.
		@return Cash Out	  */
	public BigDecimal getCashOut () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashOut);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Out Base 1.
		@param CashOutBase1 Cash Out Base 1	  */
	public void setCashOutBase1 (BigDecimal CashOutBase1)
	{
		set_Value (COLUMNNAME_CashOutBase1, CashOutBase1);
	}

	/** Get Cash Out Base 1.
		@return Cash Out Base 1	  */
	public BigDecimal getCashOutBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashOutBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cash Out Base 2.
		@param CashOutBase2 Cash Out Base 2	  */
	public void setCashOutBase2 (BigDecimal CashOutBase2)
	{
		set_Value (COLUMNNAME_CashOutBase2, CashOutBase2);
	}

	/** Get Cash Out Base 2.
		@return Cash Out Base 2	  */
	public BigDecimal getCashOutBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CashOutBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Ending balance.
		@param EndingBalance 
		Ending  or closing balance
	  */
	public void setEndingBalance (BigDecimal EndingBalance)
	{
		set_Value (COLUMNNAME_EndingBalance, EndingBalance);
	}

	/** Get Ending balance.
		@return Ending  or closing balance
	  */
	public BigDecimal getEndingBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EndingBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ending Balance Base 1.
		@param EndingBalanceBase1 Ending Balance Base 1	  */
	public void setEndingBalanceBase1 (BigDecimal EndingBalanceBase1)
	{
		set_Value (COLUMNNAME_EndingBalanceBase1, EndingBalanceBase1);
	}

	/** Get Ending Balance Base 1.
		@return Ending Balance Base 1	  */
	public BigDecimal getEndingBalanceBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EndingBalanceBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ending Balance Base 2.
		@param EndingBalanceBase2 Ending Balance Base 2	  */
	public void setEndingBalanceBase2 (BigDecimal EndingBalanceBase2)
	{
		set_Value (COLUMNNAME_EndingBalanceBase2, EndingBalanceBase2);
	}

	/** Get Ending Balance Base 2.
		@return Ending Balance Base 2	  */
	public BigDecimal getEndingBalanceBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EndingBalanceBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Estimation Short Cashier Amount.
		@param EstimationShortCashierAmt Estimation Short Cashier Amount	  */
	public void setEstimationShortCashierAmt (BigDecimal EstimationShortCashierAmt)
	{
		set_Value (COLUMNNAME_EstimationShortCashierAmt, EstimationShortCashierAmt);
	}

	/** Get Estimation Short Cashier Amount.
		@return Estimation Short Cashier Amount	  */
	public BigDecimal getEstimationShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EstimationShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Estimation Short Cashier Amount Base 1.
		@param EstimationShortCashierAmtBase1 Estimation Short Cashier Amount Base 1	  */
	public void setEstimationShortCashierAmtBase1 (BigDecimal EstimationShortCashierAmtBase1)
	{
		set_Value (COLUMNNAME_EstimationShortCashierAmtBase1, EstimationShortCashierAmtBase1);
	}

	/** Get Estimation Short Cashier Amount Base 1.
		@return Estimation Short Cashier Amount Base 1	  */
	public BigDecimal getEstimationShortCashierAmtBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EstimationShortCashierAmtBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Estimation Short Cashier Amount Base 2.
		@param EstimationShortCashierAmtBase2 Estimation Short Cashier Amount Base 2	  */
	public void setEstimationShortCashierAmtBase2 (BigDecimal EstimationShortCashierAmtBase2)
	{
		set_Value (COLUMNNAME_EstimationShortCashierAmtBase2, EstimationShortCashierAmtBase2);
	}

	/** Get Estimation Short Cashier Amount Base 2.
		@return Estimation Short Cashier Amount Base 2	  */
	public BigDecimal getEstimationShortCashierAmtBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_EstimationShortCashierAmtBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Used For Change ?.
		@param IsUsedForChange Used For Change ?	  */
	public void setIsUsedForChange (boolean IsUsedForChange)
	{
		set_Value (COLUMNNAME_IsUsedForChange, Boolean.valueOf(IsUsedForChange));
	}

	/** Get Used For Change ?.
		@return Used For Change ?	  */
	public boolean isUsedForChange () 
	{
		Object oo = get_Value(COLUMNNAME_IsUsedForChange);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Payable Refund Amount.
		@param PayableRefundAmt 
		The payable amount to refund (to customer)
	  */
	public void setPayableRefundAmt (BigDecimal PayableRefundAmt)
	{
		set_Value (COLUMNNAME_PayableRefundAmt, PayableRefundAmt);
	}

	/** Get Payable Refund Amount.
		@return The payable amount to refund (to customer)
	  */
	public BigDecimal getPayableRefundAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payable Refund Amount B1.
		@param PayableRefundAmtB1 
		The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB1 (BigDecimal PayableRefundAmtB1)
	{
		set_Value (COLUMNNAME_PayableRefundAmtB1, PayableRefundAmtB1);
	}

	/** Get Payable Refund Amount B1.
		@return The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmtB1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Payable Refund Amount B2.
		@param PayableRefundAmtB2 
		The payable amount to refund (to customer) Currency Base 1
	  */
	public void setPayableRefundAmtB2 (BigDecimal PayableRefundAmtB2)
	{
		set_Value (COLUMNNAME_PayableRefundAmtB2, PayableRefundAmtB2);
	}

	/** Get Payable Refund Amount B2.
		@return The payable amount to refund (to customer) Currency Base 1
	  */
	public BigDecimal getPayableRefundAmtB2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PayableRefundAmtB2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount.
		@param ShortCashierAmt 
		The amount differences between cash-fisically with cash to deposit in system.
	  */
	public void setShortCashierAmt (BigDecimal ShortCashierAmt)
	{
		set_Value (COLUMNNAME_ShortCashierAmt, ShortCashierAmt);
	}

	/** Get Short Cashier Amount.
		@return The amount differences between cash-fisically with cash to deposit in system.
	  */
	public BigDecimal getShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount Base 1.
		@param ShortCashierAmtBase1 Short Cashier Amount Base 1	  */
	public void setShortCashierAmtBase1 (BigDecimal ShortCashierAmtBase1)
	{
		set_Value (COLUMNNAME_ShortCashierAmtBase1, ShortCashierAmtBase1);
	}

	/** Get Short Cashier Amount Base 1.
		@return Short Cashier Amount Base 1	  */
	public BigDecimal getShortCashierAmtBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmtBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount Base 2.
		@param ShortCashierAmtBase2 Short Cashier Amount Base 2	  */
	public void setShortCashierAmtBase2 (BigDecimal ShortCashierAmtBase2)
	{
		set_Value (COLUMNNAME_ShortCashierAmtBase2, ShortCashierAmtBase2);
	}

	/** Get Short Cashier Amount Base 2.
		@return Short Cashier Amount Base 2	  */
	public BigDecimal getShortCashierAmtBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmtBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_POSTerminalCashAcct getUNS_POSTerminalCashAcct() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POSTerminalCashAcct)MTable.get(getCtx(), com.unicore.model.I_UNS_POSTerminalCashAcct.Table_Name)
			.getPO(getUNS_POSTerminalCashAcct_ID(), get_TrxName());	}

	/** Set Cash Account.
		@param UNS_POSTerminalCashAcct_ID Cash Account	  */
	public void setUNS_POSTerminalCashAcct_ID (int UNS_POSTerminalCashAcct_ID)
	{
		if (UNS_POSTerminalCashAcct_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminalCashAcct_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSTerminalCashAcct_ID, Integer.valueOf(UNS_POSTerminalCashAcct_ID));
	}

	/** Get Cash Account.
		@return Cash Account	  */
	public int getUNS_POSTerminalCashAcct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSTerminalCashAcct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Session Cash Account.
		@param UNS_SessionCashAccount_ID Session Cash Account	  */
	public void setUNS_SessionCashAccount_ID (int UNS_SessionCashAccount_ID)
	{
		if (UNS_SessionCashAccount_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_SessionCashAccount_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_SessionCashAccount_ID, Integer.valueOf(UNS_SessionCashAccount_ID));
	}

	/** Get Session Cash Account.
		@return Session Cash Account	  */
	public int getUNS_SessionCashAccount_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_SessionCashAccount_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_SessionCashAccount_UU.
		@param UNS_SessionCashAccount_UU UNS_SessionCashAccount_UU	  */
	public void setUNS_SessionCashAccount_UU (String UNS_SessionCashAccount_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_SessionCashAccount_UU, UNS_SessionCashAccount_UU);
	}

	/** Get UNS_SessionCashAccount_UU.
		@return UNS_SessionCashAccount_UU	  */
	public String getUNS_SessionCashAccount_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_SessionCashAccount_UU);
	}
}