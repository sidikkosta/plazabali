/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_Order_Contract
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_Order_Contract 
{

    /** TableName=UNS_Order_Contract */
    public static final String Table_Name = "UNS_Order_Contract";

    /** AD_Table_ID=1000275 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AccountNo */
    public static final String COLUMNNAME_AccountNo = "AccountNo";

	/** Set Account No.
	  * Account Number
	  */
	public void setAccountNo (String AccountNo);

	/** Get Account No.
	  * Account Number
	  */
	public String getAccountNo();

    /** Column name AccountNo2 */
    public static final String COLUMNNAME_AccountNo2 = "AccountNo2";

	/** Set Account No-2.
	  * The second bank account number in same bank with different currency with Account No-1
	  */
	public void setAccountNo2 (String AccountNo2);

	/** Get Account No-2.
	  * The second bank account number in same bank with different currency with Account No-1
	  */
	public String getAccountNo2();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name BacklogCompulsoryDeduction */
    public static final String COLUMNNAME_BacklogCompulsoryDeduction = "BacklogCompulsoryDeduction";

	/** Set Backlog Compulsory.
	  * The compulsory deduction in % if goods receipt have (lot of) backlog/restan
	  */
	public void setBacklogCompulsoryDeduction (BigDecimal BacklogCompulsoryDeduction);

	/** Get Backlog Compulsory.
	  * The compulsory deduction in % if goods receipt have (lot of) backlog/restan
	  */
	public BigDecimal getBacklogCompulsoryDeduction();

    /** Column name BankAddress */
    public static final String COLUMNNAME_BankAddress = "BankAddress";

	/** Set Bank Address.
	  * The address of the bank of which the bank account registered
	  */
	public void setBankAddress (String BankAddress);

	/** Get Bank Address.
	  * The address of the bank of which the bank account registered
	  */
	public String getBankAddress();

    /** Column name Beneficiary */
    public static final String COLUMNNAME_Beneficiary = "Beneficiary";

	/** Set Beneficiary.
	  * Business Partner to whom payment is made
	  */
	public void setBeneficiary (String Beneficiary);

	/** Get Beneficiary.
	  * Business Partner to whom payment is made
	  */
	public String getBeneficiary();

    /** Column name C_BankAccount_ID */
    public static final String COLUMNNAME_C_BankAccount_ID = "C_BankAccount_ID";

	/** Set Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public void setC_BankAccount_ID (int C_BankAccount_ID);

	/** Get Cash / Bank Account.
	  * Account at the Bank or Cash account
	  */
	public int getC_BankAccount_ID();

	public org.compiere.model.I_C_BankAccount getC_BankAccount() throws RuntimeException;

    /** Column name C_Bank_ID */
    public static final String COLUMNNAME_C_Bank_ID = "C_Bank_ID";

	/** Set Bank.
	  * Bank
	  */
	public void setC_Bank_ID (int C_Bank_ID);

	/** Get Bank.
	  * Bank
	  */
	public int getC_Bank_ID();

	public org.compiere.model.I_C_Bank getC_Bank() throws RuntimeException;

    /** Column name C_BP_BankAccount_ID */
    public static final String COLUMNNAME_C_BP_BankAccount_ID = "C_BP_BankAccount_ID";

	/** Set Partner Bank Account.
	  * Bank Account of the Business Partner
	  */
	public void setC_BP_BankAccount_ID (int C_BP_BankAccount_ID);

	/** Get Partner Bank Account.
	  * Bank Account of the Business Partner
	  */
	public int getC_BP_BankAccount_ID();

	public org.compiere.model.I_C_BP_BankAccount getC_BP_BankAccount() throws RuntimeException;

    /** Column name C_Currency2_ID */
    public static final String COLUMNNAME_C_Currency2_ID = "C_Currency2_ID";

	/** Set Currency 2.
	  * The Currency for this second record
	  */
	public void setC_Currency2_ID (int C_Currency2_ID);

	/** Get Currency 2.
	  * The Currency for this second record
	  */
	public int getC_Currency2_ID();

	public org.compiere.model.I_C_Currency getC_Currency2() throws RuntimeException;

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name C_Order_ID */
    public static final String COLUMNNAME_C_Order_ID = "C_Order_ID";

	/** Set Order.
	  * Order
	  */
	public void setC_Order_ID (int C_Order_ID);

	/** Get Order.
	  * Order
	  */
	public int getC_Order_ID();

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DeliveryTerm */
    public static final String COLUMNNAME_DeliveryTerm = "DeliveryTerm";

	/** Set Delivery Term.
	  * The term of delivery that indicates how the goods to be delivered to buyer. This is to follow the international term of delivery (FOB, FOD, etc.)
	  */
	public void setDeliveryTerm (String DeliveryTerm);

	/** Get Delivery Term.
	  * The term of delivery that indicates how the goods to be delivered to buyer. This is to follow the international term of delivery (FOB, FOD, etc.)
	  */
	public String getDeliveryTerm();

    /** Column name FreshCompulsoryDeduction */
    public static final String COLUMNNAME_FreshCompulsoryDeduction = "FreshCompulsoryDeduction";

	/** Set Fresh Compulsory.
	  * The compulsory deduction in % if goods receipt is (mostly) fresh
	  */
	public void setFreshCompulsoryDeduction (BigDecimal FreshCompulsoryDeduction);

	/** Get Fresh Compulsory.
	  * The compulsory deduction in % if goods receipt is (mostly) fresh
	  */
	public BigDecimal getFreshCompulsoryDeduction();

    /** Column name InvoiceFlatDeduction */
    public static final String COLUMNNAME_InvoiceFlatDeduction = "InvoiceFlatDeduction";

	/** Set Invoice Flat Deduction	  */
	public void setInvoiceFlatDeduction (BigDecimal InvoiceFlatDeduction);

	/** Get Invoice Flat Deduction	  */
	public BigDecimal getInvoiceFlatDeduction();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsMandatoryWithholding */
    public static final String COLUMNNAME_IsMandatoryWithholding = "IsMandatoryWithholding";

	/** Set Mandatory Withholding.
	  * Monies must be withheld
	  */
	public void setIsMandatoryWithholding (boolean IsMandatoryWithholding);

	/** Get Mandatory Withholding.
	  * Monies must be withheld
	  */
	public boolean isMandatoryWithholding();

    /** Column name Notes */
    public static final String COLUMNNAME_Notes = "Notes";

	/** Set Notes	  */
	public void setNotes (String Notes);

	/** Get Notes	  */
	public String getNotes();

    /** Column name PPhAfterUnloadingCost */
    public static final String COLUMNNAME_PPhAfterUnloadingCost = "PPhAfterUnloadingCost";

	/** Set PPh After Unloading Cost	  */
	public void setPPhAfterUnloadingCost (boolean PPhAfterUnloadingCost);

	/** Get PPh After Unloading Cost	  */
	public boolean isPPhAfterUnloadingCost();

    /** Column name QualityAgreement */
    public static final String COLUMNNAME_QualityAgreement = "QualityAgreement";

	/** Set Quality Agreement.
	  * The aggreement for quality to deliver
	  */
	public void setQualityAgreement (String QualityAgreement);

	/** Get Quality Agreement.
	  * The aggreement for quality to deliver
	  */
	public String getQualityAgreement();

    /** Column name QualityTerm */
    public static final String COLUMNNAME_QualityTerm = "QualityTerm";

	/** Set Quality Term.
	  * The term of quality to be delivered/receipt.
	  */
	public void setQualityTerm (String QualityTerm);

	/** Get Quality Term.
	  * The term of quality to be delivered/receipt.
	  */
	public String getQualityTerm();

    /** Column name RottenBunchDeduction */
    public static final String COLUMNNAME_RottenBunchDeduction = "RottenBunchDeduction";

	/** Set Rotten Bunch Deduction (%)	  */
	public void setRottenBunchDeduction (BigDecimal RottenBunchDeduction);

	/** Get Rotten Bunch Deduction (%)	  */
	public BigDecimal getRottenBunchDeduction();

    /** Column name SwiftCode */
    public static final String COLUMNNAME_SwiftCode = "SwiftCode";

	/** Set Swift code.
	  * Swift Code or BIC
	  */
	public void setSwiftCode (String SwiftCode);

	/** Get Swift code.
	  * Swift Code or BIC
	  */
	public String getSwiftCode();

    /** Column name UNS_Order_Contract_ID */
    public static final String COLUMNNAME_UNS_Order_Contract_ID = "UNS_Order_Contract_ID";

	/** Set Additional Contract Order	  */
	public void setUNS_Order_Contract_ID (int UNS_Order_Contract_ID);

	/** Get Additional Contract Order	  */
	public int getUNS_Order_Contract_ID();

    /** Column name UNS_Order_Contract_UU */
    public static final String COLUMNNAME_UNS_Order_Contract_UU = "UNS_Order_Contract_UU";

	/** Set UNS_Order_Contract_UU	  */
	public void setUNS_Order_Contract_UU (String UNS_Order_Contract_UU);

	/** Get UNS_Order_Contract_UU	  */
	public String getUNS_Order_Contract_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name WeighCalcBased */
    public static final String COLUMNNAME_WeighCalcBased = "WeighCalcBased";

	/** Set Weigh Calc Based.
	  * Whose weight calucation will be used, buyer or seller.
	  */
	public void setWeighCalcBased (String WeighCalcBased);

	/** Get Weigh Calc Based.
	  * Whose weight calucation will be used, buyer or seller.
	  */
	public String getWeighCalcBased();
}
