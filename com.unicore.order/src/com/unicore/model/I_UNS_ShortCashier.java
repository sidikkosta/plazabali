/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ShortCashier
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ShortCashier 
{

    /** TableName=UNS_ShortCashier */
    public static final String Table_Name = "UNS_ShortCashier";

    /** AD_Table_ID=1000496 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name Cashier_ID */
    public static final String COLUMNNAME_Cashier_ID = "Cashier_ID";

	/** Set Cashier	  */
	public void setCashier_ID (int Cashier_ID);

	/** Get Cashier	  */
	public int getCashier_ID();

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name ShortCashierAmt */
    public static final String COLUMNNAME_ShortCashierAmt = "ShortCashierAmt";

	/** Set Short Cashier Amount.
	  * The amount differences between cash-fisically with cash to deposit in system.
	  */
	public void setShortCashierAmt (BigDecimal ShortCashierAmt);

	/** Get Short Cashier Amount.
	  * The amount differences between cash-fisically with cash to deposit in system.
	  */
	public BigDecimal getShortCashierAmt();

    /** Column name ShortCashierAmtBase1 */
    public static final String COLUMNNAME_ShortCashierAmtBase1 = "ShortCashierAmtBase1";

	/** Set Short Cashier Amount Base 1	  */
	public void setShortCashierAmtBase1 (BigDecimal ShortCashierAmtBase1);

	/** Get Short Cashier Amount Base 1	  */
	public BigDecimal getShortCashierAmtBase1();

    /** Column name ShortCashierAmtBase2 */
    public static final String COLUMNNAME_ShortCashierAmtBase2 = "ShortCashierAmtBase2";

	/** Set Short Cashier Amount Base 2	  */
	public void setShortCashierAmtBase2 (BigDecimal ShortCashierAmtBase2);

	/** Get Short Cashier Amount Base 2	  */
	public BigDecimal getShortCashierAmtBase2();

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException;

    /** Column name UNS_POSRecap_ID */
    public static final String COLUMNNAME_UNS_POSRecap_ID = "UNS_POSRecap_ID";

	/** Set POS Recapitulation	  */
	public void setUNS_POSRecap_ID (int UNS_POSRecap_ID);

	/** Get POS Recapitulation	  */
	public int getUNS_POSRecap_ID();

    /** Column name UNS_ShortCashier_ID */
    public static final String COLUMNNAME_UNS_ShortCashier_ID = "UNS_ShortCashier_ID";

	/** Set Short Cashier	  */
	public void setUNS_ShortCashier_ID (int UNS_ShortCashier_ID);

	/** Get Short Cashier	  */
	public int getUNS_ShortCashier_ID();

    /** Column name UNS_ShortCashier_UU */
    public static final String COLUMNNAME_UNS_ShortCashier_UU = "UNS_ShortCashier_UU";

	/** Set UNS_ShortCashier_UU	  */
	public void setUNS_ShortCashier_UU (String UNS_ShortCashier_UU);

	/** Get UNS_ShortCashier_UU	  */
	public String getUNS_ShortCashier_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
