/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for UNS_CustomerInfo
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_CustomerInfo extends PO implements I_UNS_CustomerInfo, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20180921L;

    /** Standard Constructor */
    public X_UNS_CustomerInfo (Properties ctx, int UNS_CustomerInfo_ID, String trxName)
    {
      super (ctx, UNS_CustomerInfo_ID, trxName);
      /** if (UNS_CustomerInfo_ID == 0)
        {
			setUNS_CustomerInfo_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_CustomerInfo (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_CustomerInfo[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Boarding Pass.
		@param BoardingPass Boarding Pass	  */
	public void setBoardingPass (String BoardingPass)
	{
		set_Value (COLUMNNAME_BoardingPass, BoardingPass);
	}

	/** Get Boarding Pass.
		@return Boarding Pass	  */
	public String getBoardingPass () 
	{
		return (String)get_Value(COLUMNNAME_BoardingPass);
	}

	public org.compiere.model.I_C_Country getC_Country() throws RuntimeException
    {
		return (org.compiere.model.I_C_Country)MTable.get(getCtx(), org.compiere.model.I_C_Country.Table_Name)
			.getPO(getC_Country_ID(), get_TrxName());	}

	/** Set Country.
		@param C_Country_ID 
		Country 
	  */
	public void setC_Country_ID (int C_Country_ID)
	{
		if (C_Country_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Country_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Country_ID, Integer.valueOf(C_Country_ID));
	}

	/** Get Country.
		@return Country 
	  */
	public int getC_Country_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Country_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** General = G */
	public static final String CUSTOMERTYPE_General = "G";
	/** Air Crew = A */
	public static final String CUSTOMERTYPE_AirCrew = "A";
	/** Employee = E */
	public static final String CUSTOMERTYPE_Employee = "E";
	/** Set Customer Type.
		@param CustomerType Customer Type	  */
	public void setCustomerType (String CustomerType)
	{

		set_Value (COLUMNNAME_CustomerType, CustomerType);
	}

	/** Get Customer Type.
		@return Customer Type	  */
	public String getCustomerType () 
	{
		return (String)get_Value(COLUMNNAME_CustomerType);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Finger.
		@param Finger Finger	  */
	public void setFinger (byte[] Finger)
	{
		set_Value (COLUMNNAME_Finger, Finger);
	}

	/** Get Finger.
		@return Finger	  */
	public byte[] getFinger () 
	{
		return (byte[])get_Value(COLUMNNAME_Finger);
	}

	/** Set Flight Number.
		@param FlightNo Flight Number	  */
	public void setFlightNo (String FlightNo)
	{
		set_Value (COLUMNNAME_FlightNo, FlightNo);
	}

	/** Get Flight Number.
		@return Flight Number	  */
	public String getFlightNo () 
	{
		return (String)get_Value(COLUMNNAME_FlightNo);
	}

	/** Set Gate Number.
		@param GateNo Gate Number	  */
	public void setGateNo (String GateNo)
	{
		set_Value (COLUMNNAME_GateNo, GateNo);
	}

	/** Get Gate Number.
		@return Gate Number	  */
	public String getGateNo () 
	{
		return (String)get_Value(COLUMNNAME_GateNo);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set NIK.
		@param NIK NIK	  */
	public void setNIK (String NIK)
	{
		set_Value (COLUMNNAME_NIK, NIK);
	}

	/** Get NIK.
		@return NIK	  */
	public String getNIK () 
	{
		return (String)get_Value(COLUMNNAME_NIK);
	}

	/** Departure = D */
	public static final String PASSENGERTYPE_Departure = "D";
	/** In-Transit = I */
	public static final String PASSENGERTYPE_In_Transit = "I";
	/** Arrival = A */
	public static final String PASSENGERTYPE_Arrival = "A";
	/** Set Passenger Type.
		@param PassengerType Passenger Type	  */
	public void setPassengerType (String PassengerType)
	{

		set_Value (COLUMNNAME_PassengerType, PassengerType);
	}

	/** Get Passenger Type.
		@return Passenger Type	  */
	public String getPassengerType () 
	{
		return (String)get_Value(COLUMNNAME_PassengerType);
	}

	/** Set Passport Number.
		@param PassportNo Passport Number	  */
	public void setPassportNo (String PassportNo)
	{
		set_Value (COLUMNNAME_PassportNo, PassportNo);
	}

	/** Get Passport Number.
		@return Passport Number	  */
	public String getPassportNo () 
	{
		return (String)get_Value(COLUMNNAME_PassportNo);
	}

	public com.unicore.model.I_UNS_AirLine getUNS_AirLine() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_AirLine)MTable.get(getCtx(), com.unicore.model.I_UNS_AirLine.Table_Name)
			.getPO(getUNS_AirLine_ID(), get_TrxName());	}

	/** Set Air Line.
		@param UNS_AirLine_ID Air Line	  */
	public void setUNS_AirLine_ID (int UNS_AirLine_ID)
	{
		if (UNS_AirLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_AirLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_AirLine_ID, Integer.valueOf(UNS_AirLine_ID));
	}

	/** Get Air Line.
		@return Air Line	  */
	public int getUNS_AirLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_AirLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Customer Info.
		@param UNS_CustomerInfo_ID Customer Info	  */
	public void setUNS_CustomerInfo_ID (int UNS_CustomerInfo_ID)
	{
		if (UNS_CustomerInfo_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_CustomerInfo_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_CustomerInfo_ID, Integer.valueOf(UNS_CustomerInfo_ID));
	}

	/** Get Customer Info.
		@return Customer Info	  */
	public int getUNS_CustomerInfo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_CustomerInfo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_CustomerInfo_UU.
		@param UNS_CustomerInfo_UU UNS_CustomerInfo_UU	  */
	public void setUNS_CustomerInfo_UU (String UNS_CustomerInfo_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_CustomerInfo_UU, UNS_CustomerInfo_UU);
	}

	/** Get UNS_CustomerInfo_UU.
		@return UNS_CustomerInfo_UU	  */
	public String getUNS_CustomerInfo_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_CustomerInfo_UU);
	}

	/** Membership = M */
	public static final String VIPTYPE_Membership = "M";
	/** No Membership = N */
	public static final String VIPTYPE_NoMembership = "N";
	/** Set VIP Type.
		@param VIPType VIP Type	  */
	public void setVIPType (String VIPType)
	{

		set_Value (COLUMNNAME_VIPType, VIPType);
	}

	/** Get VIP Type.
		@return VIP Type	  */
	public String getVIPType () 
	{
		return (String)get_Value(COLUMNNAME_VIPType);
	}
}