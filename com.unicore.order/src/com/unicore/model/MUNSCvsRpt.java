/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MCurrency;
import org.compiere.model.MDocType;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.compiere.wf.MWorkflow;

import com.unicore.base.model.MInOut;
import com.unicore.base.model.MInOutLine;
import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MInvoiceLine;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.MUNSBPCanvasser;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.unicore.model.process.InvoiceCalculateDiscount;
import com.uns.base.model.Query;
import com.uns.util.MessageBox;

/**
 * @author root
 * 
 */
public class MUNSCvsRpt extends X_UNS_Cvs_Rpt implements DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -791773179004172113L;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private MUNSCvsRptProduct[] m_lines = null;
	private int M_Currency_ID = 0;
	private int m_precission = 0;
	private int M_PriceList_ID = 0;
	private int M_Warehouse_ID = 0;
	private boolean createShipmentInvoice = true;

	/**
	 * @param ctx
	 * @param UNS_Cvs_Rpt_ID
	 * @param trxName
	 */
	public MUNSCvsRpt(Properties ctx, int UNS_Cvs_Rpt_ID, String trxName) {
		super(ctx, UNS_Cvs_Rpt_ID, trxName);
		initialRequirements();
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCvsRpt(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		initialRequirements();
	}

	private void initialRequirements() {
		if (getUNS_Cvs_Rpt_ID() <= 0)
			return;

		MCurrency cur = MCurrency.get(getCtx(), "IDR");
		M_Currency_ID = cur.get_ID();
		m_precission = cur.getStdPrecision();
		M_PriceList_ID = DB
				.getSQLValue(
						get_TrxName(),
						(new StringBuilder(
								"SELECT o.M_PriceList_ID FROM C_Order o WHERE ")
								.append("o.C_BPartner_ID = ? AND ")
								.append(" (o.DocStatus = 'CO' OR o.DocStatus = 'CL')")
								.append(" AND o.DatePromised <= ? ")
								.append(" ORDER BY o.DatePromised DESC"))
								.toString(),
						getC_BPartner_ID(), getDateDoc());
		M_Warehouse_ID = DB
				.getSQLValue(
						get_TrxName(),
						(new StringBuilder(
								"SELECT M_Warehouse_ID FROM M_Locator WHERE ")
								.append("M_Locator_ID = (SELECT M_Locator_ID FROM UNS_BP_Canvasser WHERE UNS_BP_Canvasser_ID =")
								.append("(SELECT UNS_BP_Canvasser_ID FROM UNS_BP_Canvasser WHERE C_BPartner_ID = ?))"))
								.toString(), getC_BPartner_ID());
	}

	@Override
	public boolean processIt(String action) throws Exception {
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		log.info(toString());
		return true;
	}

	@Override
	public boolean invalidateIt() {
		log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		MUNSCvsRptProduct[] lines = getLines(true);
		if (null == lines || lines.length == 0) {
			m_processMsg = "@No Lines@";
			return DocAction.STATUS_Invalid;
		}

		int totalZero = 0;
		for (int i = 0; i < lines.length; i++) {
			MUNSCvsRptProduct product = lines[i];
			if (product.getQtySold().signum() == 0) {
				log.log(Level.INFO, "Zero Qty Sold.");
				totalZero++;
			}
		}
		
		if(totalZero == lines.length)
			createShipmentInvoice = false;

		MUNSBPCanvasser cvsInfo = MUNSBPCanvasser.getOf(getC_BPartner_ID(),
				get_TrxName());
		if (null == cvsInfo) {
			throw new AdempiereException("Canvasser Info is not set");
		}

		if (!cvsInfo.isMustReturnRemainingItems()) {
			if (cvsInfo.getLastPhysical() == null) {
				cvsInfo.setLastPhysical(getDateDoc());
				cvsInfo.saveEx();
			}

			int daysPhysical = cvsInfo.getDaysPhysical();
			StringBuilder sb = new StringBuilder("SELECT TIMESTAMP '")
					.append(cvsInfo.getLastPhysical())
					.append("' + INTERVAL '1 DAY' * ").append(daysPhysical);

			String sql = sb.toString();

			Timestamp dateMustPhysical = DB.getSQLValueTS(get_TrxName(), sql);

			if (getDateDoc().compareTo(dateMustPhysical) >= 0) {
				m_processMsg = "Can't Commplete processing document. please create physical First";
				return DocAction.STATUS_Invalid;
			}
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		m_justPrepared = true;

		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		log.info(toString());
		if (!isApproved())
			setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		log.info(toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		log.info(toString());

		if (!m_justPrepared) {
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		MUNSBPCanvasser cvsInfo = MUNSBPCanvasser.getOf(getC_BPartner_ID(),
				get_TrxName());
		cvsInfo.setLastReport(getDateDoc());
		cvsInfo.saveEx();
		
		if(createShipmentInvoice)
		{
			if(getC_Invoice_ID() == 0 || getM_InOut_ID() == 0) {
				m_processMsg = createShipmentInvoice();
				if (!Util.isEmpty(m_processMsg, true)) {
					return DocAction.STATUS_Invalid;
				}
			}

			MInvoice invoice = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
			if(!invoice.isComplete()) 
			{
				String message = "Automaticaly complete Invoice Document ? ";
				int retVal = MessageBox.showMsg(this, getCtx(), message, "Auto Complete Confirmation"
						, MessageBox.YESNO, MessageBox.ICONWARNING);
				
				if(retVal != MessageBox.RETURN_YES)
				{
					m_processMsg = "Please complete Invoice Document before do next process.";
					return DocAction.STATUS_InProgress;
				}
			}
			
			m_processMsg = completingShipmentInvoicePayment();
			if (!Util.isEmpty(m_processMsg, true)) {
				return DocAction.STATUS_Invalid;
			}
			
		}
		else if(!createShipmentInvoice)
		{
			if(getC_Invoice_ID() > 0)
			{
				MInvoice invoice = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
				try
				{
					invoice.deleteEx(true);
				} 
				catch (Exception e)
				{
					log.log(Level.SEVERE, "Failed on delete invoice");
					if(!invoice.getDocStatus().equals(MInvoice.DOCSTATUS_Drafted))
					{
						try
						{
							ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(invoice, DocAction.ACTION_Void);
							if(pi.isError())
							{
								m_processMsg = pi.getSummary();
								return DocAction.STATUS_Invalid;
							}
						}
						catch (Exception ee)
						{
							m_processMsg = ee.getMessage();
							return DocAction.STATUS_Invalid;
						}
					}
				}
			}
			if(getM_InOut_ID() > 0)
			{
				MInOut inOut = new MInOut(getCtx(), getM_InOut_ID(), get_TrxName());
				try
				{
					inOut.deleteEx(true);
				} 
				catch (Exception e)
				{
					log.log(Level.SEVERE, "Failed on delete invoice");
					if(!inOut.getDocStatus().equals(MInvoice.DOCSTATUS_Drafted))
					{
						try
						{
							ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(inOut, DocAction.ACTION_Void);
							if(pi.isError())
							{
								m_processMsg = pi.getSummary();
								return DocAction.STATUS_Invalid;
							}
						}
						catch (Exception ee)
						{
							m_processMsg = ee.getMessage();
							return DocAction.STATUS_Invalid;
						}
					}
				}
			}
		}
		
		if(cvsInfo.getQtyOnHand().signum() != 0 && cvsInfo.isMustReturnRemainingItems())
		{
			m_processMsg = "Please return remaining items into warehouse before completing this document.";
			return DocAction.STATUS_InProgress;
		}
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		if (DOCSTATUS_Closed.equals(getDocStatus())
				|| DOCSTATUS_Reversed.equals(getDocStatus())
				|| DOCSTATUS_Voided.equals(getDocStatus())) {
			m_processMsg = "Document Closed: " + getDocStatus();
			return false;
		}

		setDocStatus(DOCSTATUS_Voided); 
		saveEx();

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() {
		log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		return false;
	}

	@Override
	public boolean reActivateIt() {
		return false;
	}

	@Override
	public String getSummary() {
		return (new StringBuilder("Canvas Report ").append(getName()))
				.toString();
	}

	@Override
	public String getDocumentInfo() {
		return getSummary();
	}

	@Override
	public File createPDF() {
		return null;
	}

	@Override
	public String getProcessMsg() {
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		return M_Currency_ID;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		return Env.ZERO;
	}

	/**
	 * 
	 * @param requery
	 * @return
	 */
	public MUNSCvsRptProduct[] getLines(boolean requery) {
		if (null != m_lines && !requery) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}

		List<MUNSCvsRptProduct> list = Query
				.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
						MUNSCvsRptProduct.Table_Name, Table_Name + "_ID = ?",
						get_TrxName()).setParameters(get_ID()).list();

		m_lines = new MUNSCvsRptProduct[list.size()];
		list.toArray(m_lines);

		return m_lines;
	}

	/**
	 * Create Shipment adn Invoice for kanvas report, and than update the delver
	 * qty and invoice qty of canvas order.
	 * 
	 * @return
	 */
	private String createShipmentInvoice() {
		String msg = null;
		try {
			MUNSBPCanvasser cvsInfo = MUNSBPCanvasser.getOf(getC_BPartner_ID(),
					get_TrxName());
			MInvoice invoice = new MInvoice(getCtx(), 0, get_TrxName());
			invoice.setIsSOTrx(true);
			invoice.setAD_Org_ID(getAD_Org_ID());
			invoice.setAD_OrgTrx_ID(getAD_Org_ID());
			invoice.setBPartner((MBPartner) getC_BPartner());
			invoice.setC_Currency_ID(getC_Currency_ID());
			invoice.setC_DocTypeTarget_ID();
			invoice.setC_DocType_ID(invoice.getC_DocTypeTarget_ID());
			invoice.setDateAcct(getDateDoc());
			invoice.setDateInvoiced(getDateDoc());
			invoice.setDateOrdered(getDateDoc());
			invoice.setM_PriceList_ID(M_PriceList_ID);
			invoice.setPaymentRule(MInvoice.PAYMENTRULE_Cash);
			invoice.setSalesRep_ID(getSalesRep_ID());
			invoice.saveEx();
			MInOut shipment = new MInOut(invoice,
					MDocType.getDocType(MDocType.DOCBASETYPE_MaterialDelivery),
					getDateDoc(), M_Warehouse_ID);
			shipment.setC_Invoice_ID(invoice.get_ID());
			shipment.saveEx();

			setC_Invoice_ID(invoice.get_ID());
			setM_InOut_ID(shipment.get_ID());
			saveEx();

			MUNSCvsRptProduct[] listProduct = getLines(false);
			Hashtable<Integer, BigDecimal> mapProduct = new Hashtable<>();
			for (MUNSCvsRptProduct product : listProduct) 
			{
				if(product.getQtySold().signum() == 0)
					continue;
				
				BigDecimal tmp = mapProduct.get(product.getM_Product_ID());

				if (null == tmp)
					tmp = Env.ZERO;

				tmp = tmp.add(product.getQtySold());
				mapProduct.put(product.getM_Product_ID(), tmp);

				MInvoiceLine invoiceLine = new MInvoiceLine(invoice);
				MInOutLine shipmentLine = new MInOutLine(shipment);

				invoiceLine.setM_Product_ID(product.getM_Product_ID());
				invoiceLine.setA_Processed(false);
				invoiceLine.setAD_OrgTrx_ID(invoice.getAD_Org_ID());
				invoiceLine.setC_UOM_ID(product.getM_Product().getC_UOM_ID());
				invoiceLine.setDescription("::Auto create from Canvas Report");
				invoiceLine.setisProductBonuses(false);
				invoiceLine.setM_AttributeSetInstance_ID(product
						.getM_AttributeSetInstance_ID());
				invoiceLine.setPrice(product.getPriceActual());
				invoiceLine.setPriceLimit(product.getPriceLimit());
				invoiceLine.setPriceList(product.getPriceList());
				invoiceLine.setQty(product.getQtySold());
				invoiceLine.setQtyBonuses(Env.ZERO);
				
				shipmentLine.setInvoiceLine(invoiceLine,
						product.getM_Locator_ID(), product.getQtySold());
				shipmentLine.setQty(product.getQtySold());
				shipmentLine.setM_Locator_ID(cvsInfo.getM_Locator_ID());
				shipmentLine.setM_AttributeSetInstance_ID(product.getM_AttributeSetInstance_ID());
				shipmentLine.saveEx();

				invoiceLine.setM_InOutLine_ID(shipmentLine.get_ID());
				invoiceLine.saveEx();
				product.setM_InOutLine_ID(shipmentLine.get_ID());
				product.setC_InvoiceLine_ID(invoiceLine.get_ID());
				product.saveEx();
			}

			for (Integer product : mapProduct.keySet()) 
			{
				MOrderLine[] lines = MOrderLine.getFOFO(getC_BPartner_ID(),
						product, get_TrxName());
				for (MOrderLine line : lines) {
					BigDecimal ordered = line.getQtyOrdered();
					BigDecimal delvered = line.getQtyDelivered();
					BigDecimal remaining = ordered.subtract(delvered);
					BigDecimal unallocated = mapProduct.get(product);
					BigDecimal toFilled = Env.ZERO;
					if (remaining.compareTo(unallocated) == -1)
						toFilled = remaining;
					else
						toFilled = unallocated;

					toFilled = toFilled.add(delvered);
					line.setQtyDelivered(toFilled);
					line.setQtyInvoiced(toFilled);
					line.saveEx();
				}
			}
			
			invoice.setProcessInfo(this.getProcessInfo());
			
			UNSInvoiceDiscountModel dm = new UNSInvoiceDiscountModel(invoice);
			Thread worker = new Thread(new InvoiceCalculateDiscount(dm));
			worker.run();
			
			for(MInvoiceLine line : invoice.getLines())
			{
				for(MUNSCvsRptProduct product : getLines(false))
				{
					if(line.getM_Product_ID() != product.get_ID())
						continue;
					
					product.setPriceActual(line.getPriceActual());
					product.setPriceLimit(line.getPriceLimit());
					product.setPriceList(line.getPriceList());
					product.setLineNetAmt();
					product.saveEx();
				}
			}
		} 
		catch (Exception e) 
		{
			msg = e.getMessage();
		}
		return msg;
	}

	private String completingShipmentInvoicePayment() {
		MInvoice invoice = new MInvoice(getCtx(), getC_Invoice_ID(),
				get_TrxName());
		MInOut shipment = new MInOut(getCtx(), getM_InOut_ID(), get_TrxName());

		try {
			if(!invoice.isComplete())
			{
				ProcessInfo invoiceInfo = MWorkflow.runDocumentActionWorkflow(invoice, DocAction.ACTION_Complete);
				if(invoiceInfo.isError())
				{
					return invoiceInfo.getSummary();
				}
			}
			
			if(!shipment.isComplete())
			{
				ProcessInfo inOutInfo = MWorkflow.runDocumentActionWorkflow(shipment, DocAction.ACTION_Complete);
				if(inOutInfo.isError())
				{
					return inOutInfo.getSummary();
				}
			}
		} catch (Exception e) {
			return e.getMessage();
		}
		return null;
	}

	public int getPrecission() {
		return m_precission;
	}

	public String createLines() {
		MUNSBPCanvasser cvsInfo = MUNSBPCanvasser.getOf(getC_BPartner_ID(),
				get_TrxName());
		if (null == cvsInfo)
			throw new AdempiereException("Canvasser info is not set.");
		MStorageOnHand[] storages = MStorageOnHand.getOfLocator(getCtx(),
				cvsInfo.getM_Locator_ID(), get_TrxName());

		for (MStorageOnHand storage : storages) {
			MUNSCvsRptProduct product = new MUNSCvsRptProduct(this);
			product.setC_UOM_ID(storage.getM_Product().getC_UOM_ID());
			product.setIsActive(true);
			product.setM_AttributeSetInstance_ID(storage
					.getM_AttributeSetInstance_ID());
			product.setM_Locator_ID(storage.getM_Locator_ID());
			product.setM_Product_ID(storage.getM_Product_ID());
			product.setPrice();
			product.setLineNetAmt();
			product.saveEx();
		}
		return null;
	}
}
