/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MInvoiceLine;

/**
 * @author Burhani Adam
 *
 */
public class MUNSMatchInvoiceCN extends X_UNS_MatchInvoiceCN
 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2636814236395994689L;

	/**
	 * @param ctx
	 * @param UNS_MatchInvoiceCN_ID
	 * @param trxName
	 */
	public MUNSMatchInvoiceCN(Properties ctx, int UNS_MatchInvoiceCN_ID,
			String trxName) {
		super(ctx, UNS_MatchInvoiceCN_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSMatchInvoiceCN(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
		if(newRecord)
		{
			String sql = "SELECT COUNT(*) FROM UNS_MatchInvoiceCN WHERE C_InvoiceLine_ID = ?"
					+ " AND M_InOutLine_ID = ? AND IsQuantityBased = ?";
			int count = DB.getSQLValue(get_TrxName(), sql, getC_InvoiceLine_ID(), getM_InOutLine_ID(), isQuantityBased() ? "Y" : "N");
			if(count > 0)
			{
				log.saveError("Error", "Duplicate line in same based found");
				return false;
			}
		}
			
		if(!isQuantityBased() && getBalance().compareTo(Env.ZERO) == -1)
		{
			log.saveError("Error", "Balance amount must be biger than of zero or same");
			return false;
		}
		
		if(isQuantityBased())
		{
			BigDecimal totalQty = getQtySubtract().add
					(MUNSMatchInvoiceCN.getTotalQtyApplied(getM_InOutLine_ID(), get_ID(), get_TrxName())); 
			if(totalQty.compareTo(getQty()) == 1)
			{
				log.saveError("", "Over qty subtract");
				return false;
			}
		}
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean Success)
	{
		return upHeader(false, newRecord);
	}
	
	protected boolean beforeDelete()
	{
		return upHeader(true, false);
	}
	
	public static BigDecimal getTotalAmountApplied(int M_InOutLine_ID, int RecordID, String trxName)
	{
		String sql = "SELECT COALESCE(SUM(m.AmountApplied),0) FROM UNS_MatchInvoiceCN m"
				+ " WHERE m.M_InOutLine_ID = ? AND EXISTS (SELECT 1 FROM C_InvoiceLine il WHERE"
				+ " il.C_InvoiceLine_ID = m.C_InvoiceLine_ID AND EXISTS"
				+ " (SELECT 1 FROM C_Invoice i WHERE i.C_Invoice_ID = il.C_Invoice_ID"
				+ " AND i.DocStatus NOT IN ('VO', 'RE'))) AND m.UNS_MatchInvoiceCN_ID <> ?";
		BigDecimal total = DB.getSQLValueBD(trxName, sql, M_InOutLine_ID, RecordID);
		if(total == null)
			total = Env.ZERO;
		
		return total;
	}
	
	public static BigDecimal getTotalQtyApplied(int M_InOutLine_ID, int RecordID, String trxName)
	{
		String sql = "SELECT COALESCE(SUM(m.QtySubtract),0) FROM UNS_MatchInvoiceCN m"
				+ " WHERE m.M_InOutLine_ID = ? AND EXISTS (SELECT 1 FROM C_InvoiceLine il WHERE"
				+ " il.C_InvoiceLine_ID = m.C_InvoiceLine_ID AND EXISTS"
				+ " (SELECT 1 FROM C_Invoice i WHERE i.C_Invoice_ID = il.C_Invoice_ID"
				+ " AND i.DocStatus NOT IN ('VO', 'RE'))) AND m.UNS_MatchInvoiceCN_ID <> ?";
		BigDecimal total = DB.getSQLValueBD(trxName, sql, M_InOutLine_ID, RecordID);
		if(total == null)
			total = Env.ZERO;
		
		return total;
	}
	
	private boolean upHeader(boolean isDelete, boolean newRecord)
	{
		if(getC_InvoiceLine_ID() <= 0)
			return true;
		
		MInvoiceLine line = new MInvoiceLine(getCtx(), getC_InvoiceLine_ID(), get_TrxName());
		BigDecimal oldValue = Env.ZERO;
		if(isQuantityBased())
		{
			if(!newRecord)
				oldValue = (BigDecimal) get_ValueOld(COLUMNNAME_QtySubtract);
			if(!isDelete)
			{
				line.setQty((line.getQtyEntered().add(oldValue)).subtract(getQtySubtract()));
			}
			else
				line.setQty(line.getQtyEntered().add(getQtySubtract()));
		}
		else
		{
			if(!newRecord)
				oldValue = (BigDecimal) get_ValueOld(COLUMNNAME_AmountApplied);
			if(!isDelete)
			{
				line.setDiscountAmt(line.getDiscountAmt().subtract(oldValue).add(getAmountApplied()));
				if(oldValue.compareTo(getAmountApplied()) == -1)
				{
					if(getAmountApplied().subtract(oldValue).compareTo(line.getLineNetAmt()) == 1)
					{
						log.saveError("Error", "Over amount credit note");
						return false;
					}
				}
			}
			else
				line.setDiscountAmt(line.getDiscountAmt().subtract(getAmountApplied()));
		}
		
		return line.save();
	}
	
	public static BigDecimal getPrevAmount(int M_InOutLine_ID, String trxName)
	{
		String sql = "SELECT COALESCE(Amount,0) FROM UNS_MatchInvoiceCN"
				+ " WHERE M_InOutLine_ID = ?";
		BigDecimal prevAmount = DB.getSQLValueBD(trxName, sql, M_InOutLine_ID);
		if(prevAmount == null)
			prevAmount = Env.ZERO;
			
		return prevAmount;
	}
	
	public static MUNSMatchInvoiceCN getCreate(Properties ctx, int C_InvoiceLine_ID, int M_InOutLine_ID, String trxName)
	{
		MUNSMatchInvoiceCN cn = null;
		
		cn = new Query(ctx, Table_Name, COLUMNNAME_C_InvoiceLine_ID + "=? AND "
				+ COLUMNNAME_M_InOutLine_ID + "=?", trxName)
						.setParameters(C_InvoiceLine_ID, M_InOutLine_ID).first();
		
//		if(cn == null)
//		{
//			String sql = "SELECT COALESCE(Amount,0) FROM UNS_MatchInvoiceCN"
//					+ " WHERE M_InOutLine_ID = ?";
//			BigDecimal prevAmount = DB.getSQLValueBD(trxName, sql, iol.get_ID());
//			cn = new MUNSMatchInvoiceCN(ctx, 0, trxName);
//			cn.setAD_Org_ID(DB.getSQLValue(trxName, 
//					"SELECT AD_Org_ID FROM C_InvoiceLine WHERE C_InvoiceLine_ID = ?",
//							C_InvoiceLine_ID));
//			cn.setC_InvoiceLine_ID(C_InvoiceLine_ID);
//			cn.setM_InOutLine_ID(iol.get_ID());
//			cn.setM_Product_ID(iol.getM_Product_ID());
//			cn.setAmount(prevAmount == null ? Env.ZERO : prevAmount);
//			cn.setQty(iol.getMovementQty());
//			cn.saveEx();
//		}
		
		return cn;
	}
	
	public static BigDecimal getPrice(Properties ctx, int C_InvoiceLine_ID, int M_InOutLine_ID, String trxName)
	{
		BigDecimal price = Env.ZERO;
		//coba cari ke invoice penjualan sebelumnya
		String sql = "SELECT il.PriceActual FROM C_InvoiceLine il WHERE EXISTS"
				+ " (SELECT 1 FROM M_InOut io WHERE io.C_Invoice_ID = il.C_Invoice_ID"
				+ " AND EXISTS (SELECT 1 FROM M_RMA rma WHERE rma.InOut_ID = io.M_InOut_ID"
				+ " AND EXISTS (SELECT 1 FROM M_InOut ioo WHERE ioo.M_RMA_ID = rma.M_RMA_ID"
				+ " AND EXISTS (SELECT 1 FROM M_InOutLine ill WHERE ill.M_InOUt_ID = ioo.M_InOut_ID"
				+ " AND ill.M_InOutLine_ID = ?))))";
		price = DB.getSQLValueBD(trxName, sql, M_InOutLine_ID);
		if(price == null || price.compareTo(Env.ZERO) == 0)
		{
			//coba cari ke price list invoicenya.
			MInvoiceLine line = new MInvoiceLine(ctx, C_InvoiceLine_ID, trxName);
			sql = "SELECT PriceStd FROM M_ProductPrice pp"
					+ " INNER JOIN M_PriceList_Version plv ON"
					+ " plv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID"
					+ " WHERE pp.M_Product_ID = ? AND plv.ValidFrom <= ?"
					+ " AND plv.M_PriceList_ID = ? AND pp.IsActive = 'Y'"
					+ " ORDER BY plv.ValidFrom DESC";
			price = DB.getSQLValueBD(trxName, sql, line.getM_Product_ID(), line.getC_Invoice().getDateInvoiced(),
					line.getC_Invoice().getM_PriceList_ID());
			if(price == null)
				price = Env.ZERO;
		}
		
		return price;
	}
}