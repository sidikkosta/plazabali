/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ShortCashierCorr_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ShortCashierCorr_Line 
{

    /** TableName=UNS_ShortCashierCorr_Line */
    public static final String Table_Name = "UNS_ShortCashierCorr_Line";

    /** AD_Table_ID=1000503 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Base1Currency_ID */
    public static final String COLUMNNAME_Base1Currency_ID = "Base1Currency_ID";

	/** Set Base 1 Currency	  */
	public void setBase1Currency_ID (int Base1Currency_ID);

	/** Get Base 1 Currency	  */
	public int getBase1Currency_ID();

	public org.compiere.model.I_C_Currency getBase1Currency() throws RuntimeException;

    /** Column name Base2Currency_ID */
    public static final String COLUMNNAME_Base2Currency_ID = "Base2Currency_ID";

	/** Set Base 2 Currency	  */
	public void setBase2Currency_ID (int Base2Currency_ID);

	/** Get Base 2 Currency	  */
	public int getBase2Currency_ID();

	public org.compiere.model.I_C_Currency getBase2Currency() throws RuntimeException;

    /** Column name Cashier_ID */
    public static final String COLUMNNAME_Cashier_ID = "Cashier_ID";

	/** Set Cashier	  */
	public void setCashier_ID (int Cashier_ID);

	/** Get Cashier	  */
	public int getCashier_ID();

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException;

    /** Column name C_Currency_ID */
    public static final String COLUMNNAME_C_Currency_ID = "C_Currency_ID";

	/** Set Currency.
	  * The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID);

	/** Get Currency.
	  * The Currency for this record
	  */
	public int getC_Currency_ID();

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException;

    /** Column name CorrectionAmt */
    public static final String COLUMNNAME_CorrectionAmt = "CorrectionAmt";

	/** Set Correction Amt Periodic	  */
	public void setCorrectionAmt (BigDecimal CorrectionAmt);

	/** Get Correction Amt Periodic	  */
	public BigDecimal getCorrectionAmt();

    /** Column name CorrectionAmtB1 */
    public static final String COLUMNNAME_CorrectionAmtB1 = "CorrectionAmtB1";

	/** Set Correction Amount Base 1	  */
	public void setCorrectionAmtB1 (BigDecimal CorrectionAmtB1);

	/** Get Correction Amount Base 1	  */
	public BigDecimal getCorrectionAmtB1();

    /** Column name CorrectionAmtB2 */
    public static final String COLUMNNAME_CorrectionAmtB2 = "CorrectionAmtB2";

	/** Set Correction Amount Base 2	  */
	public void setCorrectionAmtB2 (BigDecimal CorrectionAmtB2);

	/** Get Correction Amount Base 2	  */
	public BigDecimal getCorrectionAmtB2();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateTrx */
    public static final String COLUMNNAME_DateTrx = "DateTrx";

	/** Set Transaction Date.
	  * Transaction Date
	  */
	public void setDateTrx (Timestamp DateTrx);

	/** Get Transaction Date.
	  * Transaction Date
	  */
	public Timestamp getDateTrx();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name OriginalAmt */
    public static final String COLUMNNAME_OriginalAmt = "OriginalAmt";

	/** Set Original Amount	  */
	public void setOriginalAmt (BigDecimal OriginalAmt);

	/** Get Original Amount	  */
	public BigDecimal getOriginalAmt();

    /** Column name OriginalAmtB1 */
    public static final String COLUMNNAME_OriginalAmtB1 = "OriginalAmtB1";

	/** Set Original Amount Base 1	  */
	public void setOriginalAmtB1 (BigDecimal OriginalAmtB1);

	/** Get Original Amount Base 1	  */
	public BigDecimal getOriginalAmtB1();

    /** Column name OriginalAmtB2 */
    public static final String COLUMNNAME_OriginalAmtB2 = "OriginalAmtB2";

	/** Set Original Amount Base 2	  */
	public void setOriginalAmtB2 (BigDecimal OriginalAmtB2);

	/** Get Original Amount Base 2	  */
	public BigDecimal getOriginalAmtB2();

    /** Column name Store_ID */
    public static final String COLUMNNAME_Store_ID = "Store_ID";

	/** Set Store	  */
	public void setStore_ID (int Store_ID);

	/** Get Store	  */
	public int getStore_ID();

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException;

    /** Column name UNS_Employee_ID */
    public static final String COLUMNNAME_UNS_Employee_ID = "UNS_Employee_ID";

	/** Set Employee	  */
	public void setUNS_Employee_ID (int UNS_Employee_ID);

	/** Get Employee	  */
	public int getUNS_Employee_ID();

    /** Column name UNS_POS_Session_ID */
    public static final String COLUMNNAME_UNS_POS_Session_ID = "UNS_POS_Session_ID";

	/** Set POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID);

	/** Get POS Session	  */
	public int getUNS_POS_Session_ID();

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException;

    /** Column name UNS_ShortCashierCorrection_ID */
    public static final String COLUMNNAME_UNS_ShortCashierCorrection_ID = "UNS_ShortCashierCorrection_ID";

	/** Set Short Cashier Correction	  */
	public void setUNS_ShortCashierCorrection_ID (int UNS_ShortCashierCorrection_ID);

	/** Get Short Cashier Correction	  */
	public int getUNS_ShortCashierCorrection_ID();

	public com.unicore.model.I_UNS_ShortCashierCorrection getUNS_ShortCashierCorrection() throws RuntimeException;

    /** Column name UNS_ShortCashierCorr_Line_ID */
    public static final String COLUMNNAME_UNS_ShortCashierCorr_Line_ID = "UNS_ShortCashierCorr_Line_ID";

	/** Set Short Cashier Correction Line	  */
	public void setUNS_ShortCashierCorr_Line_ID (int UNS_ShortCashierCorr_Line_ID);

	/** Get Short Cashier Correction Line	  */
	public int getUNS_ShortCashierCorr_Line_ID();

    /** Column name UNS_ShortCashierCorr_Line_UU */
    public static final String COLUMNNAME_UNS_ShortCashierCorr_Line_UU = "UNS_ShortCashierCorr_Line_UU";

	/** Set UNS_ShortCashierCorr_Line_UU	  */
	public void setUNS_ShortCashierCorr_Line_UU (String UNS_ShortCashierCorr_Line_UU);

	/** Get UNS_ShortCashierCorr_Line_UU	  */
	public String getUNS_ShortCashierCorr_Line_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
