package com.unicore.model;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;

import org.apache.ecs.xhtml.object;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ServerProcessCtl;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.util.ErrorMsg;

/**
 * 
 */

/**
 * @author AzHaidar
 *
 */
public class MUNSDespatchNotes extends X_UNS_Despatch_Notes implements
		DocAction, DocOptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2199805181584888371L;

	String m_processMsg = null;
	private boolean m_justPrepared = false;
	//TODO add seal length at the table and then remove this.
	private final int SEAL_LENGTH = 7; 
	private final String ZERO = "0";
	private final String SEAL_SPLITTER = ";";
	private final String RANGE_SPLITTER = "-";
	private final String SEALNO_SPLITTER = ",";
	private List<String> m_currentSealNo = null;
	private MUNSWeighbridgeTicket m_ticket = null;
	public static final String LOC_COLUMN_PATTERN = "Locator";
	public static final String QTY_COLUMN_PATTERN = "QtyLoc";
	public static final String PRC_COLUMN_PATTERN = "PercentageLoc";
	public static final String PRODUCT_CPO = "CPO";
	public static final String PRODUCT_KRNL = "KRNL";

	/**
	 * @param ctx
	 * @param UNS_Despatch_Notes_ID
	 * @param trxName
	 */
	public MUNSDespatchNotes(Properties ctx, int UNS_Despatch_Notes_ID,
			String trxName) {
		super(ctx, UNS_Despatch_Notes_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSDespatchNotes(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public static MUNSDespatchNotes get (MUNSWeighbridgeTicket ticket)
	{
		MUNSDespatchNotes note = Query.get(ticket.getCtx(), 
				UNSOrderModelFactory.EXTENSION_ID, 
				MUNSDespatchNotes.Table_Name, 
						"UNS_WeighbridgeTicket_ID=?", ticket.get_TrxName())
						.setParameters(ticket.get_ID())
						.first();
		
		if (null == note && ticket.isSplitOrder() && !ticket.isOriginalTicket()
				&& ticket.getSplittedTicket_ID() > 0)
		{
			MUNSWeighbridgeTicket split = new MUNSWeighbridgeTicket(
					ticket.getCtx(), ticket.getSplittedTicket_ID(), 
					ticket.get_TrxName());
			note = get(split);
		}
		
		if (null != note) {
			note.setTicket(ticket);
		}
		
		return note;
	}
	
	public static MUNSDespatchNotes getCreate(MUNSWeighbridgeTicket ticket)
	{
		MUNSDespatchNotes despatch = get(ticket);
		if (despatch != null)
			return despatch;
		
		despatch = new MUNSDespatchNotes(
				ticket.getCtx(), 0, ticket.get_TrxName());
		despatch.setClientOrg(ticket);
		despatch.setUNS_WeighbridgeTicket_ID(ticket.get_ID());
		despatch.setM_Product_ID(ticket.getM_Product_ID());
		despatch.set_ValueOfColumn("DateDoc", ticket.getDateDoc());
		despatch.setLocator1_ID(ticket.getM_Locator_ID());
		despatch.setQtyLoc1(ticket.isSplitOrder() ? ticket.getOriginalNettoII() : ticket.getNettoII());
		despatch.saveEx();
		despatch.setIsQtyPercentage(true);
		despatch.setTicket(ticket);
		
		return despatch;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#processIt(java.lang.String)
	 */
	@Override
	public boolean processIt(String processAction) throws Exception 
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#unlockIt()
	 */
	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		//setProcessing(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#invalidateIt()
	 */
	@Override
	public boolean invalidateIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#prepareIt()
	 */
	@Override
	public String prepareIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
//		if (getTicket().getTare().signum() <= 0 || getTicket().getGrossWeight().signum() <= 0)
//		{
//			m_processMsg = "The despatch ticket contains zero tare or gross weight.";
//			return DOCSTATUS_Invalid;
//		}
		
		if (getLocator1_ID() == 0 && getLocator2_ID() == 0 && getLocator3_ID() == 0)
		{
			m_processMsg = "Undefined locator";
			return DOCSTATUS_Invalid;
		}
		else if (getLocator1_ID() > 0 && getQtyLoc1().signum() == 0
				|| getQtyLoc1().signum() > 0 && getLocator1_ID() == 0)
		{
			m_processMsg = "Mandatory quantity or locator 1";
			return DOCSTATUS_Invalid;
		}
		else if (getLocator2_ID() > 0 && getQtyLoc2().signum() == 0
				|| getQtyLoc2().signum() > 0 && getLocator2_ID() == 0)
		{
			m_processMsg = "Mandatory quantity or locator 2";
			return DOCSTATUS_Invalid;
		}
		else if (getLocator3_ID() > 0 && getQtyLoc3().signum() == 0
				|| getQtyLoc3().signum() > 0 && getLocator3_ID() == 0)
		{
			m_processMsg = "Mandatory quantity or locator 3";
			return DOCSTATUS_Invalid;
		}
		
		String product = DB.getSQLValueString(get_TrxName(), "SELECT value FROM M_Product WHERE M_Product_ID = ? ",
				getM_Product_ID());
		if(product.equals(PRODUCT_CPO))
		{
			if(getFFA().signum()==0 || getMI().signum()==0)
			{
				m_processMsg = "FFA or MI cannot fill by zero quantity";
				return DOCSTATUS_Invalid;
			}
		}
		else if(product.equals(PRODUCT_KRNL))
		{
			if(getMoisture().signum()==0 || getDirt().signum()==0)
			{
				m_processMsg = "Moisture or Dirt cannot fill by zero quantity";
				return DOCSTATUS_Invalid;
			}
		}
		
		calculateQtyPortion();
//		if (!isValidQtyProportion())
//		{
//			m_processMsg = "Invalid Qty Portion.";
//			return DOCSTATUS_Invalid;
//		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	} // prepareIt
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#approveIt()
	 */
	@Override
	public boolean approveIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#rejectIt()
	 */
	@Override
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#completeIt()
	 */
	@Override
	public String completeIt() 
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		StringBuilder info = new StringBuilder();
		
//		m_processMsg = processTickets(DOCACTION_Complete);
//		if (null != m_processMsg)
//		{
//			return DOCSTATUS_Invalid;
//		}
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			if (info.length() > 0)
				info.append(" - ");
			info.append(valid);
			m_processMsg = info.toString();
			return DocAction.STATUS_Invalid;
		}
		
		if(!isQtyPercentage())
		{
			if(m_ticket.isSplitOrder())
				{
				m_ticket.setOriginalNettoI(getTotalQty().add(m_ticket.getReflection()));
				m_ticket.setOriginalNettoII(getTotalQty());
				}
			else
				{
				m_ticket.setNettoI(getTotalQty().add(m_ticket.getReflection()));
				m_ticket.setNettoII(getTotalQty());
				}
			
			m_ticket.setGrossWeight(getTotalQty().add(m_ticket.getReflection()).add(m_ticket.getTare()));
			m_ticket.saveEx();
		}
		
		setProcessed(true);	
		m_processMsg = info.toString();
		//
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	} // completeIt
	
	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#voidIt()
	 */
	@Override
	public boolean voidIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		m_processMsg = processTickets(DOCACTION_Void);
		if (m_processMsg != null)
		{
			return false;
		}
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#closeIt()
	 */
	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseCorrectIt()
	 */
	@Override
	public boolean reverseCorrectIt() 
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;
		
		return voidIt();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reverseAccrualIt()
	 */
	@Override
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;
		
		return false;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#reActivateIt()
	 */
	@Override
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;	

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		setDocAction(DOCACTION_Complete);
		setProcessed(false);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getSummary()
	 */
	@Override
	public String getSummary() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		//	: Grand Total = 123.00 (#1)
		sb.append(": ").
			append("Start").append("=").append(getStartDespatch())
			.append("End").append("=").append(getEndDespatch()).append(")");
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDocumentInfo()
	 */
	@Override
	public String getDocumentInfo()
	{
		return  "Customer Weighbridge Ticket Confirmation " + getDocumentNo();
	}	//	getDocumentInfo

	
	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#createPDF()
	 */
	@Override
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
		ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.SHIPMENT, getUNS_Despatch_Notes_ID(), get_TrxName());
		if (re == null)
			return null;
		MPrintFormat format = re.getPrintFormat();
		// We have a Jasper Print Format
		// ==============================
		if(format.getJasperProcess_ID() > 0)
		{
			ProcessInfo pi = new ProcessInfo ("", format.getJasperProcess_ID());
			pi.setRecord_ID ( getUNS_Despatch_Notes_ID() );
			pi.setIsBatch(true);
			
			ServerProcessCtl.process(pi, null);
			
			return pi.getPDFReport();
		}
		// Standard Print Format (Non-Jasper)
		// ==================================
		return re.getPDF(file);
	}	//	createPDF
	
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getDoc_User_ID()
	 */
	@Override
	public int getDoc_User_ID() 
	{	
		return getAD_User_ID();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getC_Currency_ID()
	 */
	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.DocAction#getApprovalAmt()
	 */
	@Override
	public BigDecimal getApprovalAmt()
	{
		return Env.ZERO;
	}	//	getApprovalAmt
	
	protected boolean calculateQtyPortion () {		
		MUNSWeighbridgeTicket ticket = getTicket();
		BigDecimal total = ticket.isSplitOrder() ? ticket.getOriginalNettoII() 
				: ticket.getNettoII();
		int i = 0;
		int columnExists = 1;
		while (columnExists != -1) {			
			BigDecimal percentage = Env.ZERO;
			BigDecimal qty = Env.ZERO;
			String percentageCol = PRC_COLUMN_PATTERN + "" + ++i;
			String quantityCol = QTY_COLUMN_PATTERN + "" + i;
			columnExists = get_ColumnIndex(percentageCol);
			if (columnExists == -1) {
				continue;
			} else if (isQtyPercentage()) {
				percentage = (BigDecimal) get_Value(percentageCol);
				if (null == percentage) {
					percentage = Env.ZERO;
				}
				qty = percentage.multiply(total).divide(Env.ONEHUNDRED, 4, 
						RoundingMode.HALF_UP);
			} else {
				qty = (BigDecimal) get_Value(quantityCol);
				if (null == qty) {
					qty = Env.ZERO;
				}
				percentage = qty.divide(total, 4, RoundingMode.HALF_UP).
						multiply(Env.ONEHUNDRED);
			}
			
			set_Value (percentageCol, percentage);
			set_Value (quantityCol, qty);
		}
		return true;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (!isReplication())
		{
			if (!calculateQtyPortion()) {
				return false;
			}
//			if (!isValidQtyProportion()) {
//				ErrorMsg.setErrorMsg(getCtx(), 
//						"InvalidQtyLocatorPortion", "Invalid quantity portion of locator");
//				return false;
//			}
			if(isQtyPercentage())
			{
				if(getTotalPercentage().compareTo(Env.ONEHUNDRED) != 0){
				ErrorMsg.setErrorMsg(getCtx(), "Invalid Percentage", "Invalid Percentage Portion of Locator");
				return false;
				}
			}
			if(!isSealNumeric())
				addSealNumbers(parsingSealNo());			
		}
		
		return super.beforeSave(newRecord);
	}
	
	public boolean isValidQtyProportion () {
		BigDecimal weighBridgeQty = getTicket().isSplitOrder() ? 
				getTicket().getOriginalNettoII() : getTicket().getNettoII();
		BigDecimal total = getTotalQty();
		return total.compareTo(weighBridgeQty) == 0;
	}
	
	private String parsingSealNo (int start, int end)
	{
		String retValue = "";
		if (start > end || (start == 0 && end == 0))
		{
			return retValue;
		}
		
		for (int i=start; i<=end; i++)
		{
			if (!Util.isEmpty(retValue, true))
			{
				retValue += ",";
			}
			String tmp = "" + i;
			
			while (tmp.length() < SEAL_LENGTH)
			{
				tmp = ZERO + tmp;
			}
			
			retValue += tmp;
		}
		
		return retValue;
	}
	
	private String parsingSealFromTo ()
	{
		int range = getSealNoTo() - getSealNoFrom();
		
		if (range < 0)
		{
			setSealNoTo(getSealNoFrom());
		}
		
		return parsingSealNo(getSealNoFrom(), getSealNoTo());
	}
	
	private String parsingSealNo ()
	{
		String sealNumber = parsingSealFromTo();
		String curSealNumber = get_ValueAsString("SealNumber");
		if (null == curSealNumber)
		{
			curSealNumber = "";
		}
		
		String[] sealArray = curSealNumber.split(SEAL_SPLITTER);
		
		for (int i=0; i<sealArray.length; i++)
		{
			String range = sealArray[i];
			String[] rangeArray = range.split(RANGE_SPLITTER);

			String startString = "";
			String endString = "";
			
			if (rangeArray.length == 2)
			{
				startString = rangeArray[0];
				endString = rangeArray[1];
			}
			else if (rangeArray.length == 1)
			{
				startString = rangeArray[0];
				endString = rangeArray[0];
			}
			
			if (startString.contains(",")
					|| endString.contains(","))
			{
				continue;
			}
			
			try
			{
				int start = toInt(startString);
				int end = toInt(endString);
				String seal =  parsingSealNo(start, end);
				if (Util.isEmpty(sealNumber, true))
				{
					sealNumber = seal;
				}
				else if (!Util.isEmpty(seal, true))
				{
					sealNumber = seal + "," + sealNumber;
				}
			}
			catch (Exception ex)
			{
				sealNumber = startString + "," + endString + "," + sealNumber;
			}
		}
		
		return sealNumber;
	}
	
	private Integer toInt (String digit) throws Exception
	{
		char idx0 = digit.charAt(0);
		boolean ok = Character.isDigit(idx0) && idx0 != '0'; 
		
		if (!ok)
		{
			if (digit.length() <=1)
			{
				throw new Exception("Invalid parameter digit.");
			}
			
			digit = digit.substring(0,1);
			return toInt(digit);
		}
		
		return new Integer(digit);
	}
	
	public List<String> getCurrentSealNo ()
	{
		if (null != m_currentSealNo)
		{
			return m_currentSealNo;
		}
		
		m_currentSealNo = new ArrayList<String>();
		String sealNo = get_ValueAsString("SealNumber");
		
		if (Util.isEmpty(sealNo, true))
		{
			return m_currentSealNo;
		}
		else if (!sealNo.contains(SEALNO_SPLITTER))
		{
			return m_currentSealNo;
		}
		
		String[] sealNumbers = sealNo.split(SEALNO_SPLITTER);
		for (int i=0; i<sealNumbers.length; i++)
		{
			if (sealNumbers[i].contains(SEAL_SPLITTER))
			{
				int idx = sealNumbers[i].indexOf(";");
				sealNumbers[i] = sealNumbers[i].substring(0, idx);
			}
			
			m_currentSealNo.add(sealNumbers[i]);
		}
		
		return m_currentSealNo;
	}
	
	public void addSealNo (String sealNo)
	{
		if (isExistingSealNo(sealNo))
		{
			return;
		}
		
		getCurrentSealNo().add(sealNo);
	}
	
	/**
	 * 
	 * @param sealNumber
	 */
	public void addSealNumbers (String sealNumber)
	{
		String[] seals = sealNumber.split(SEALNO_SPLITTER);
		for (int i=0; i<seals.length; i++)
		{
			if (!Util.isEmpty(seals[i], true))
			{
				addSealNo(seals[i]);
			}
		}
		
		set_ValueOfColumn("SealNumber", sealToString());
	}
	
	public String sealToString ()
	{
		String seal = "";
		Collections.sort(getCurrentSealNo());
		
		for (int i=0; i<getCurrentSealNo().size(); i++)
		{
			if (!Util.isEmpty(seal, true))
			{
				seal += ",";
			}
			
			seal += getCurrentSealNo().get(i);
		}
		
		return seal;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean isExistingSealNo (String value)
	{
		for (int i=0; i<getCurrentSealNo().size(); i++)
		{
			if (value.equals(getCurrentSealNo().get(i)))
			{
				return true;
			}
		}
		return false;
	}
	
	public String processTickets (String action)
	{
		MUNSWeighbridgeTicket ticket = getTicket();
		if (ticket.isForce())
		{
			return null;
		}
		
		if (ticket.getDocStatus().equals(DOCSTATUS_Closed)
				|| ticket.getDocStatus().equals(DOCSTATUS_Reversed)
				|| ticket.getDocStatus().equals(DOCSTATUS_Voided))
		{
			return null;
		}
		else if (DOCSTATUS_Completed.equals(ticket.getDocStatus())
				&& DOCACTION_Complete.equals(action))
		{
			return null;
		}
		
		ticket.setProcessInfo(getProcessInfo());
		ticket.setForce(true);
		
		try
		{
			boolean ok = ticket.processIt(action);
			if (!ok)
			{
				return ticket.getProcessMsg();
			}
			
			ticket.saveEx();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		
		return null;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (docStatus.equals(DOCSTATUS_Completed))
		{
			options[index++] = DOCACTION_Void;
		}
		
		return index;
	}
	
	public void setTicket (MUNSWeighbridgeTicket ticket)
	{
		if (ticket.get_ID() != getUNS_WeighbridgeTicket_ID()
				&& ticket.getSplittedTicket_ID() != getUNS_WeighbridgeTicket_ID())
		{
			log.log(Level.WARNING, "Invalid Ticket");
			return;
		}
		
		this.m_ticket = ticket;
	}
	
	public MUNSWeighbridgeTicket getTicket ()
	{
		if (null == m_ticket)
		{
			m_ticket = new MUNSWeighbridgeTicket(
					getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
		}
		
		return m_ticket;
		
	}
	
	/**
	 * Getting vector from an array of objects. the length of an array of 
	 * objects is certainly 2. index 0 is filled by locator and index 1 is 
	 * filled by quantity
	 * @return {@link Vector} of {@link object}[]
	 * 
	 */
	public Vector<Object[]> getListQtyByLocator () {
		Vector<Object[]> productLocatorQty = new Vector<>();
		int i=1;
		int columnExists = 0;
		while (columnExists != -1) {
			String locColumn = LOC_COLUMN_PATTERN + "" + i + "_ID";
			String qtyColumn = QTY_COLUMN_PATTERN + "" + i;
			columnExists = get_ColumnIndex(locColumn);
			if (columnExists == -1) {
				continue;
			}
			columnExists = get_ColumnIndex(qtyColumn);
			if (columnExists == -1) {
				continue;
			}
			
			int locator_id = get_ValueAsInt(locColumn);
			BigDecimal qty = (BigDecimal) get_Value(qtyColumn);
			if (null == qty) {
				qty = Env.ZERO;
			} else if (locator_id != 0 || qty.signum() != 0) {
				boolean recordExistings = false;
				for (int j=0; j<productLocatorQty.size(); j++) {
					if ((int) productLocatorQty.get(j)[0] == locator_id) {
						recordExistings = true;
						productLocatorQty.get(j)[1] = 
								((BigDecimal)productLocatorQty.get(j)[1]).add(qty); 
					}
				}
				if (recordExistings) {
					++i;
					continue;
				}
				productLocatorQty.add(new Object[]{locator_id, qty});
			}
			++i;
		}
		return productLocatorQty;
	}
	
	public BigDecimal getTotalQty ()
	{
		return getQtyLoc1().add(getQtyLoc2()).add(getQtyLoc3());
	}
	
	public BigDecimal getTotalPercentage()
	{
		return getPercentageLoc1().add(getPercentageLoc2()).add(getPercentageLoc3());
	}

}
