/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_ShortCashier
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_ShortCashier extends PO implements I_UNS_ShortCashier, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190103L;

    /** Standard Constructor */
    public X_UNS_ShortCashier (Properties ctx, int UNS_ShortCashier_ID, String trxName)
    {
      super (ctx, UNS_ShortCashier_ID, trxName);
      /** if (UNS_ShortCashier_ID == 0)
        {
			setCashier_ID (0);
			setShortCashierAmt (Env.ZERO);
// 0
			setShortCashierAmtBase1 (Env.ZERO);
// 0
			setShortCashierAmtBase2 (Env.ZERO);
// 0
			setUNS_POS_Session_ID (0);
			setUNS_ShortCashier_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_ShortCashier (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_ShortCashier[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_User getCashier() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getCashier_ID(), get_TrxName());	}

	/** Set Cashier.
		@param Cashier_ID Cashier	  */
	public void setCashier_ID (int Cashier_ID)
	{
		if (Cashier_ID < 1) 
			set_Value (COLUMNNAME_Cashier_ID, null);
		else 
			set_Value (COLUMNNAME_Cashier_ID, Integer.valueOf(Cashier_ID));
	}

	/** Get Cashier.
		@return Cashier	  */
	public int getCashier_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Cashier_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Short Cashier Amount.
		@param ShortCashierAmt 
		The amount differences between cash-fisically with cash to deposit in system.
	  */
	public void setShortCashierAmt (BigDecimal ShortCashierAmt)
	{
		set_Value (COLUMNNAME_ShortCashierAmt, ShortCashierAmt);
	}

	/** Get Short Cashier Amount.
		@return The amount differences between cash-fisically with cash to deposit in system.
	  */
	public BigDecimal getShortCashierAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount Base 1.
		@param ShortCashierAmtBase1 Short Cashier Amount Base 1	  */
	public void setShortCashierAmtBase1 (BigDecimal ShortCashierAmtBase1)
	{
		set_Value (COLUMNNAME_ShortCashierAmtBase1, ShortCashierAmtBase1);
	}

	/** Get Short Cashier Amount Base 1.
		@return Short Cashier Amount Base 1	  */
	public BigDecimal getShortCashierAmtBase1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmtBase1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Short Cashier Amount Base 2.
		@param ShortCashierAmtBase2 Short Cashier Amount Base 2	  */
	public void setShortCashierAmtBase2 (BigDecimal ShortCashierAmtBase2)
	{
		set_Value (COLUMNNAME_ShortCashierAmtBase2, ShortCashierAmtBase2);
	}

	/** Get Short Cashier Amount Base 2.
		@return Short Cashier Amount Base 2	  */
	public BigDecimal getShortCashierAmtBase2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ShortCashierAmtBase2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public com.unicore.model.I_UNS_POS_Session getUNS_POS_Session() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_POS_Session)MTable.get(getCtx(), com.unicore.model.I_UNS_POS_Session.Table_Name)
			.getPO(getUNS_POS_Session_ID(), get_TrxName());	}

	/** Set POS Session.
		@param UNS_POS_Session_ID POS Session	  */
	public void setUNS_POS_Session_ID (int UNS_POS_Session_ID)
	{
		if (UNS_POS_Session_ID < 1) 
			set_Value (COLUMNNAME_UNS_POS_Session_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_POS_Session_ID, Integer.valueOf(UNS_POS_Session_ID));
	}

	/** Get POS Session.
		@return POS Session	  */
	public int getUNS_POS_Session_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POS_Session_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set POS Recapitulation.
		@param UNS_POSRecap_ID POS Recapitulation	  */
	public void setUNS_POSRecap_ID (int UNS_POSRecap_ID)
	{
		if (UNS_POSRecap_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_POSRecap_ID, Integer.valueOf(UNS_POSRecap_ID));
	}

	/** Get POS Recapitulation.
		@return POS Recapitulation	  */
	public int getUNS_POSRecap_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_POSRecap_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Short Cashier.
		@param UNS_ShortCashier_ID Short Cashier	  */
	public void setUNS_ShortCashier_ID (int UNS_ShortCashier_ID)
	{
		if (UNS_ShortCashier_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_ShortCashier_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_ShortCashier_ID, Integer.valueOf(UNS_ShortCashier_ID));
	}

	/** Get Short Cashier.
		@return Short Cashier	  */
	public int getUNS_ShortCashier_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ShortCashier_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_ShortCashier_UU.
		@param UNS_ShortCashier_UU UNS_ShortCashier_UU	  */
	public void setUNS_ShortCashier_UU (String UNS_ShortCashier_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_ShortCashier_UU, UNS_ShortCashier_UU);
	}

	/** Get UNS_ShortCashier_UU.
		@return UNS_ShortCashier_UU	  */
	public String getUNS_ShortCashier_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_ShortCashier_UU);
	}
}