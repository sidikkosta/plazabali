/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MPaymentTerm;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;

import com.unicore.base.model.MInOut;
import com.unicore.base.model.MInOutLine;
import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;
import com.uns.model.IUNSApprovalInfo;

/**
 * @author Menjangan
 *
 */
public class MUNSCreditAgreement extends X_UNS_CreditAgreement implements DocAction, DocOptions, 
IUNSApprovalInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6386582075474591175L;

	/**
	 * @param ctx
	 * @param UNS_CreditAgreement_ID
	 * @param trxName
	 */
	public MUNSCreditAgreement(Properties ctx, int UNS_CreditAgreement_ID,
			String trxName) {
		super(ctx, UNS_CreditAgreement_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSCreditAgreement(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		List<Object[]> list = new ArrayList<>();
		list.add(new Object[]{BigDecimal.class, false});
		list.add(new Object[]{Integer.class, false});
		list.add(new Object[]{BigDecimal.class, false});
		list.add(new Object[]{BigDecimal.class, false});
		list.add(new Object[]{BigDecimal.class, false});
		return list;
	}

	@Override
	public String[] getDetailTableHeader() 
	{
		String[] headers = new String[] {
			Msg.translate(getCtx(),COLUMNNAME_TotalLoanAmt),
			Msg.translate(getCtx(), COLUMNNAME_InstallmentPeriod),
			Msg.translate(getCtx(), COLUMNNAME_MonthlyInstallmentAmt),
			Msg.translate(getCtx(), COLUMNNAME_MarginAmt),
			Msg.translate(getCtx(), COLUMNNAME_Margin)
		};
		return headers;
	}

	@Override
	public List<Object[]> getDetailTableContent() {
		List<Object[]> list = new ArrayList<>();
		Object[] rows = new Object[] {
				getTotalLoanAmt(), getInstallmentPeriod(), getMonthlyInstallmentAmt(),
				getMarginAmt(), getMargin()
		};
		list.add(rows);
		return list;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) 
	{
		if (DOCSTATUS_Completed.equals(docStatus))
		{
			options[index++] = DOCSTATUS_Voided;
		}
		
		return index;
	}

	@Override
	public boolean processIt(String action) throws Exception 
	{
		writeLog(Level.INFO, "Process-It");
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt() 
	{
		writeLog(Level.INFO, "Unlock-It");
		return true;
	}

	@Override
	public boolean invalidateIt() 
	{
		writeLog(Level.INFO, "Invalidate-It");
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt() {
		writeLog(Level.INFO, "Prepare-It");
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_PREPARE);
		if (null != m_processMsg )
		{
			return DOCSTATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_PREPARE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		this.m_justPrepared = true;
		setProcessed(true);
		return DOCSTATUS_InProgress;
	}

	@Override
	public boolean approveIt() 
	{
		writeLog(Level.INFO, "Approve-It");
		setProcessed(true);
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() 
	{
		writeLog(Level.INFO, "Reject-It");
		setProcessed(false);
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt() 
	{
		writeLog(Level.INFO, "Complete-It");
		
		if (!m_justPrepared)
		{
			String msg = prepareIt();
			if (!DOCSTATUS_InProgress.equals(msg))
			{
				return DOCSTATUS_Invalid;
			}
		}
		
		if (DOCACTION_Prepare.equals(getDocAction()))
		{
			return DOCSTATUS_InProgress;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_COMPLETE);
		if (null != m_processMsg)
		{
			return DOCSTATUS_Invalid;
		}
		
		if(!isApproved())
			approveIt();
		
		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DOCSTATUS_Completed;
	}

	@Override
	public boolean voidIt() 
	{
		writeLog(Level.INFO, "Void-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		getSchedules(false);
		for (int i=0; i<m_lines.length; i++)
		{
			if (!m_lines[i].cancelIt())
			{
				m_processMsg = m_lines[i].getErrorMsg();
				return false;
			}
			
			if (!m_lines[i].save())
			{
				m_processMsg = CLogger.retrieveErrorString("Failed when try to cancel schedule");
				return false;
			}
		}
		
		if (getM_InOut_ID() > 0)
		{
			MInOut inOut = new MInOut(getCtx(), getM_InOut_ID(), get_TrxName());
			if (!"RE".equals(inOut.getDocStatus()) && !"VO".equals(inOut.getDocStatus()))
			{
				boolean ok = inOut.processIt(DOCACTION_Void);
				if (!ok)
				{
					m_processMsg = inOut.getProcessMsg();
					return false;
				}
				
				if (!inOut.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Failed when try to void Shipment");
					return false;
				}
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_VOID);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt() 
	{
		writeLog(Level.INFO, "Close-It");
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_BEFORE_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, 
				ModelValidator.TIMING_AFTER_CLOSE);
		if (null != m_processMsg)
		{
			return false;
		}
		
		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean reverseCorrectIt() 
	{
		return voidIt();
	}

	@Override
	public boolean reverseAccrualIt() 
	{
		return voidIt();
	}

	@Override
	public boolean reActivateIt() 
	{
		m_processMsg = "Invalid action Reactivate-It";
		return false;
	}

	@Override
	public String getSummary() 
	{
		String summary = "(Document no [" + getDocumentNo() + "], Business Partner ["
				+ getC_BPartner().getValue() + "-" + getC_BPartner().getName() + "], "
				+ " Loan Amount [" + getTotalLoanAmt() + "], Monthly Installment ["
				+ getMonthlyInstallmentAmt() + "])";
		return summary;
	}

	@Override
	public String getDocumentInfo() 
	{
		return getSummary();
	}

	@Override
	public File createPDF() 
	{
		return null;
	}

	@Override
	public String getProcessMsg() 
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() 
	{
		return 0;
	}

	@Override
	public int getC_Currency_ID() 
	{
		return 303; //TODO static value
	}

	@Override
	public BigDecimal getApprovalAmt() 
	{
		return getTotalLoanAmt();
	}

	/**********************************************************************************
	 * 
	 **********************************************************************************/
	
	private MUNSCreditPaySchedule[] m_lines = null;
	private boolean m_justPrepared = false;
	private String m_processMsg = null;
	private MBPartner m_partner = null;
	
	/**
	 * 
	 * @param requery
	 * @return
	 */
	public MUNSCreditPaySchedule[] getSchedules (boolean requery)
	{
		if (null != this.m_lines && !requery)
		{
			set_TrxName(this.m_lines, get_TrxName());
			return this.m_lines;
		}
		
		List<MUNSCreditPaySchedule> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSCreditPaySchedule.Table_Name, Table_Name + "_ID = ?", 
				get_TrxName()).setParameters(get_ID()).
				setOnlyActiveRecords(true).setOrderBy(
						MUNSCreditPaySchedule.COLUMNNAME_Line).
						list();
		
		this.m_lines = new MUNSCreditPaySchedule[list.size()];
		list.toArray(this.m_lines);
		
		return this.m_lines;
	}
	
	public MUNSCreditPaySchedule getPaySchedule (int scheduleSequence)
	{
		getSchedules(false);
		for (int i=0; i<m_lines.length; i++)
		{
			if (scheduleSequence == m_lines[i].getLine())
				return m_lines[i];
		}
		return null;
	}
	
	private int createNewPaymentTerm (int months, int days)
	{
		String name = "+ " + months + " Bulan Tanggal " + days;		
		MPaymentTerm term = new MPaymentTerm(getCtx(), 0, get_TrxName());
		term.setAD_Org_ID(0);
		term.setIsDueFixed(true);
		term.setAfterDelivery(false);
		term.setDescription("::Auto Generated From Credit Agreement::" + name);
		term.setFixMonthDay(days);
		term.setFixMonthOffset(months);
		term.setFixMonthCutoff(days);
		term.setIsActive(true);
		term.setIsValid(true);
		term.setName(name);
		term.setNetDays(0);
		term.setPaymentTermUsage("B");
		if (!term.save(get_TrxName()))
			return -1;
		return term.getC_PaymentTerm_ID();
	}
	
	public int getCreatePaymentTerm (int months, int days)
	{
		int C_PaymentTerm_ID = getC_PaymentTerm_ID(months, days);
		if (C_PaymentTerm_ID <= 0)
			C_PaymentTerm_ID = createNewPaymentTerm(months, days);
		return C_PaymentTerm_ID;
	}
	
	public int getC_PaymentTerm_ID (int months, int days)
	{
		int result =0;
		StringBuilder sqlBuilder = new StringBuilder("SELECT C_PaymentTerm_ID FROM ")
		.append(" C_PaymentTerm WHERE IsActive = ? AND IsDueFixed = ? AND NetDays = ? ")
		.append(" AND FixMonthDay = ? AND FixMonthOffset = ? AND PaymentTermUsage ")
		.append(" IN (?,?) AND (AD_Org_ID = 0 OR AD_Org_ID = ?) ORDER BY AD_Org_ID DESC ");
		result = DB.getSQLValue(get_TrxName(), sqlBuilder.toString(), "Y", 
				"Y", 0, days, months, "B", "S", getAD_Org_ID());
		return result;
	}
	
	public boolean doCreateSchedule (boolean reorderPaidSchedule)
	{
		int monthsBetween = TimeUtil.getMonthsBetween(getDateDoc(), getDateFirstInstallment());
		int topMonths = monthsBetween;
		if (topMonths < 0)
		{
			m_processMsg = "Document Date before Date First Installment";
			return false;
		}
		
		int topDays = TimeUtil.getCalendar(getDateFirstInstallment()).get(Calendar.DATE);		
		int installmentPeriod = getInstallmentPeriod();
		Calendar calendar = TimeUtil.getCalendar(getDateDoc());
		
		for (int i=0; i<installmentPeriod; i++)
		{
			int scheduleSequence = i+1;
			MUNSCreditPaySchedule schedule = getPaySchedule(scheduleSequence);
			calendar.add(Calendar.MONTH, topMonths == 0 ? 0 : 1);
			int maxDateOnMonth = calendar.getMaximum(Calendar.DATE);
			int tmptopDays = topDays;
			if (topDays > maxDateOnMonth)
			{
				topDays = maxDateOnMonth;
			}
			calendar.set(Calendar.DATE, topDays);
			Timestamp dueDate = new Timestamp(calendar.getTimeInMillis());
			int C_PaymentTerm_ID = getCreatePaymentTerm(topMonths, topDays);
			if (C_PaymentTerm_ID <= 0)
			{
				m_processMsg = "Could not initialize Payment Term!!!";
				return false;
			}
			topDays = tmptopDays;
			if (null == schedule)
			{
				schedule = new MUNSCreditPaySchedule(this);
				schedule.setLine(i+1);
				schedule.setReferenceNo(getReferenceNo() + "-" + schedule.getLine());
				schedule.setInvoiceAmt(getMonthlyInstallmentAmt());
				schedule.setPaidAmt(Env.ZERO);
			}

			schedule.setScheduledDate(dueDate);
			if (!schedule.save())
				return false;
			

			MUNSCreditPaySchedule nextSchedule = null;
			for (int j=scheduleSequence; j<installmentPeriod && !schedule.isPaid() 
					&& reorderPaidSchedule; j++)
			{
				MUNSCreditPaySchedule tmp = getPaySchedule(j+1);
				if (tmp != null && tmp.isPaid())
				{
					nextSchedule = tmp;
					break;
				}
			}
			
			for (int j=scheduleSequence; j<installmentPeriod && reorderPaidSchedule 
					&& !schedule.isPaid() && null == nextSchedule; j++)
			{
				MUNSCreditPaySchedule tmp = getPaySchedule(j+1);
				if (tmp != null && !tmp.isPaid() && tmp.getPaidAmt().signum() > 0)
					nextSchedule = tmp;
			}
			
			if (nextSchedule != null)
			{
				int JtmpInv_ID = nextSchedule.getC_Invoice_ID();
				BigDecimal JtmpPaidAmt = nextSchedule.getPaidAmt();
				Timestamp JtmpPaidDate = nextSchedule.getPaidDate();
				boolean JtmpIsPaid = nextSchedule.isPaid();
				
				nextSchedule.setC_Invoice_ID(schedule.getC_Invoice_ID());
				nextSchedule.setPaidAmt(schedule.getPaidAmt());
				nextSchedule.setPaidDate(schedule.getPaidDate());
				nextSchedule.setIsPaid(schedule.isPaid());
				nextSchedule.saveEx();
				
				schedule.setC_Invoice_ID(JtmpInv_ID);
				schedule.setPaidAmt(JtmpPaidAmt);
				schedule.setPaidDate(JtmpPaidDate);
				schedule.setIsPaid(JtmpIsPaid);
				schedule.saveEx();
			}
			
			int scheduledInvoice_ID = schedule.getC_Invoice_ID();
			
			if (scheduledInvoice_ID > 0)
			{
				String sql = "SELECT DocStatus FROM C_Invoice WHERE C_Invoice_ID = ?";
				String docStatus = DB.getSQLValueString(get_TrxName(), sql, scheduledInvoice_ID);
				if ("CO".equals(docStatus) || "CL".equals(docStatus))
				{
					sql = "UPDATE C_Invoice SET DateInvoiced = ?, DateAcct = ?, C_PaymentTerm_ID = ? "
							+ " WHERE C_Invoice_ID = ?";
					int executionResult = DB.executeUpdate(sql, new Object[] {getDateDoc(),getDateAcct(),
							C_PaymentTerm_ID, scheduledInvoice_ID}, false, get_TrxName());
					if (executionResult == -1)
					{
						m_processMsg = CLogger.retrieveErrorString("Could not update invoice");
						return false;
					}
					++topMonths;
					continue;
				}
				if ("DR".equals(docStatus) || "IP".equals(docStatus) || "IN".equals(docStatus))
				{
					MInvoice inv = new MInvoice(getCtx(), scheduledInvoice_ID, get_TrxName());
					inv.setDateInvoiced(getDateDoc());
					inv.setDateAcct(getDateAcct());
					if (!inv.save())
					{
						m_processMsg = CLogger.retrieveErrorString("Could not update invoice");
						return false;
					}
					boolean ok = inv.processIt(DOCACTION_Complete);
					if (!ok)
					{
						m_processMsg = inv.getProcessMsg();
						return false;
					}
					
					if (inv.save())
					{
						m_processMsg = CLogger.retrieveErrorString("Could not save invoice");
						return false;
					}
					
					++topMonths;
					continue;
				}
				
				schedule.setC_Invoice_ID(-1);
			}
			
			MInvoice invoice = schedule.createInvoice(C_PaymentTerm_ID);
			if (null == invoice)
			{
				m_processMsg = schedule.getErrorMsg();
				return false;
			}
			
			schedule.setC_Invoice_ID(invoice.get_ID());
			schedule.setInvoiceAmt(invoice.getGrandTotal());
			
			if (!schedule.save())
			{
				m_processMsg = "Failed to save schedule, caused by : " 
						+ CLogger.retrieveErrorString("Unknown");
				return false;
			}
			
			++topMonths;
		}
		
		return true;
	}
	
	public void writeLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		
		log.log(level, msg);
	}
	
	public MBPartner getPartner ()
	{
		if (null == m_partner || m_partner.get_ID() != getC_BPartner_ID()) {
			m_partner = MBPartner.get(getCtx(), getC_BPartner_ID());
		}
		
		return m_partner;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (null == getNIP() || !getNIP().equals(getPartner().getValue ()))
			setNIP(getPartner().getValue());
		
		if (getPriceActual().compareTo(getPriceLimit()) < 0)
		{
			log.saveError("PriceActualBelowPriceLimit", "Price Actual below the Price Limit");
			return false;
		}
		
		if (!doInitTotals())
			return false;
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterDelete (boolean success)
	{
		getSchedules(false);
		for (int i=0; i<m_lines.length; i++)
		{
			if (!m_lines[i].delete(true))
			{
				m_processMsg = CLogger.retrieveErrorString("Failed when try to delete schedule");
				return false;
			}
		}
		
		if (getM_InOut_ID() > 0)
		{
			MInOut inout = new MInOut(getCtx(), getM_InOut_ID(), get_TrxName());
			if (!inout.isProcessed() && !inout.delete(true))
				return false;
			else if (!"RE".equals(inout.getDocStatus()) && !"VO".equals(inout.getDocStatus()))
			{
				boolean ok = inout.processIt(DOCACTION_Void);
				if (!ok)
				{
					log.log(Level.SEVERE, inout.getProcessMsg());
					return false;
				}
				
				if (!inout.save())
					return false;
			}
		}
		
		return super.afterDelete(success);
	}
	
	private boolean doInitTotals ()
	{
		BigDecimal priceList = getPriceList();
		BigDecimal discountAmt = getDiscountAmt();
		BigDecimal downPayment = getTDP();
		BigDecimal qty = getQty();
		
		BigDecimal lineNetAmt = priceList.multiply(qty);
		lineNetAmt = lineNetAmt.subtract(discountAmt);
		setLineNetAmt (lineNetAmt);
		
		BigDecimal priceActual = lineNetAmt.divide(qty, 10, RoundingMode.UP);
		setPriceActual(priceActual);
		
		BigDecimal grandTotal = lineNetAmt.subtract(downPayment);
		setGrandTotal(grandTotal);
		
		BigDecimal totalLoanAmt = grandTotal.add(getMarginAmt());
		setTotalLoanAmt(totalLoanAmt);
		
		BigDecimal totalperiod = new BigDecimal(getInstallmentPeriod());
		BigDecimal monthlyInstallmentAmt = totalLoanAmt.divide(totalperiod, 0, RoundingMode.UP);
		setMonthlyInstallmentAmt(monthlyInstallmentAmt);
		return true;
	}
	
	public boolean createShipment (int M_Locator_ID, Timestamp shipDate)
	{
		int M_Warehouse_ID = DB.getSQLValue(get_TrxName(), 
				"SELECT M_Warehouse_ID FROM M_Locator WHERE M_Locator_ID = ?", M_Locator_ID);
		
		MInOut io = new MInOut(getCtx(), 0, get_TrxName());
		io.setAD_Org_ID(getAD_Org_ID());
		io.setIsSOTrx(true);
		io.setC_BPartner_ID(getC_BPartner_ID());
		io.setC_BPartner_Location_ID(getC_BPartner_Location_ID());
		io.setC_DocType_ID();
		io.setMovementType(MInOut.MOVEMENTTYPE_CustomerShipment);
		io.setMovementDate(shipDate);
		io.setDateAcct(shipDate);
		io.setAD_User_ID(getAD_User_ID());
		io.setM_Warehouse_ID(M_Warehouse_ID);
		io.setSalesRep_ID(getAD_User_ID());
		io.setDeliveryViaRule(MInOut.DELIVERYVIARULE_Delivery);
		
		if (!io.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not save Shipment Document");
			return false;
		}
		
		MInOutLine iol = new MInOutLine(io);
		iol.setM_Product_ID(getM_Product_ID());
		
		int C_UOM_ID = DB.getSQLValue(get_TrxName(),
				"SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?", getM_Product_ID());
		
		iol.setC_UOM_ID(C_UOM_ID);
		
		iol.setM_Locator_ID(M_Locator_ID);
		iol.setQty(getQty());
		if (!iol.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not save Shipment Document");
			return false;
		}
		
		boolean ok = io.processIt(DOCACTION_Complete);
		if (!ok)
		{
			m_processMsg = io.getProcessMsg();
			return false;
		}
		
		if (!io.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Failed when try to save Shipment.");
			return false;
		}
		
		setM_InOut_ID(io.get_ID());
		if (!save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not update Credit Agreement");
			return false;
		}
		
		return true;
	}
	
	public static String doUpdateByPaySchedule (int C_Invoice_ID, String trxName)
	{
		String sql = "SELECT UNS_CreditAgreement_ID FROM UNS_CreditPaySchedule WHERE C_Invoice_ID = ?";
		int UNS_CreditAgreement_ID = DB.getSQLValue(trxName, sql, C_Invoice_ID);
		if (UNS_CreditAgreement_ID == 0)
			return null;
		
		sql = "SELECT COALESCE (SUM (PaidAmt), 0) "
				+ " FROM UNS_CreditPaySchedule WHERE UNS_CreditAgreement_ID = ?";
		BigDecimal tPaidAmt = DB.getSQLValueBD(trxName, sql, UNS_CreditAgreement_ID);
		
		sql = "SELECT COUNT (*) "
				+ " FROM UNS_CreditPaySchedule WHERE UNS_CreditAgreement_ID = ? AND IsPaid = 'Y'";
		int counter = DB.getSQLValue(trxName, sql, UNS_CreditAgreement_ID);
		sql = "SELECT InstallmentPeriod FROM UNS_CreditAgreement WHERE UNS_CreditAgreement_ID = ?";
		int installmentPeriod = DB.getSQLValue(trxName, sql, UNS_CreditAgreement_ID);
		boolean isPaid = installmentPeriod == counter;
		
		sql = "UPDATE UNS_CreditAgreement SET TotalPaidAmt = ?, PeriodPaid = ?, IsPaid = ?"
				+ " WHERE UNS_CreditAgreement_ID = ?";
		
		int updateOK = DB.executeUpdate(sql, new Object[] {tPaidAmt, counter, 
				isPaid ? "Y" : "N", UNS_CreditAgreement_ID}, false, trxName);
		
		if (updateOK == -1)
		{
			return "Faled when try to update Credit Agreement.";
		}		
		
		return null;
	}
	
	public boolean doCreatePO (int Vendor_ID, Timestamp dateOrdered, int M_Warehose_ID, 
			int M_PriceList_ID, boolean autoComplete)
	{
		MOrder order = new MOrder(getCtx(), 0, get_TrxName());
		MBPartner partner = MBPartner.get(getCtx(), Vendor_ID);
		
		order.setClientOrg(getAD_Client_ID(), getAD_Org_ID());
		order.setBPartner(partner);
		order.setBill_BPartner_ID(partner.get_ID());
		
		String sql = "SELECT C_BPartner_Location_ID FROM C_BPartner_Location WHERE "
				+ " C_BPartner_ID = ? AND IsActive = ? ORDER BY IsBillTo DESC";
		int location_ID = DB.getSQLValue(get_TrxName(), sql, Vendor_ID, "Y");
		
		if (location_ID <= 0)
		{
			m_processMsg = "Vendor has no address";
			return false;
		}
		
		order.setC_BPartner_Location_ID(location_ID);
		order.setBill_Location_ID(location_ID);
		order.setC_Currency_ID(getC_Currency_ID());
		order.setIsSOTrx(false);
		order.setC_DocTypeTarget_ID();
		order.setDateAcct(dateOrdered);
		order.setDateOrdered(dateOrdered);
		order.setDatePromised(dateOrdered);
		order.setPriorityRule(MOrder.PRIORITYRULE_High);
		order.setM_PriceList_ID(M_PriceList_ID);
		order.setM_Warehouse_ID(M_Warehose_ID);
		if (!order.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not save PO document");
			return false;
		}
		
		MOrderLine line = new MOrderLine(order);
		line.setM_Product_ID(getM_Product_ID());
		line.setQty(getQty());
		
		int C_UOM_ID = DB.getSQLValue(get_TrxName(), 
				"SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?", getM_Product_ID());
		line.setC_UOM_ID(C_UOM_ID);
		line.setPrice();
		if (!line.save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not save PO Line document");
			return false;
		}
		
		if (autoComplete)
		{
			order.load(get_TrxName());
			try
			{
				boolean ok = order.processIt(DOCACTION_Complete);
				if (!ok)
				{
					m_processMsg = order.getProcessMsg();
					return false;
				}
				
				if (!order.save())
				{
					m_processMsg = CLogger.retrieveErrorString("Could not save PO document");
					return false;
				}
			}
			catch (Exception ex)
			{
				m_processMsg = ex.getMessage();
				return false;
			}
		}
			
		setC_Order_ID(order.get_ID());
		
		if (!save())
		{
			m_processMsg = CLogger.retrieveErrorString("Could not save Credit Agreement");
			return false;
		}
		
		return true;
	}
	
	public MInvoice[] getInvoices ()
	{
		String wc = "C_Invoice_ID IN (SELECT C_Invoice_ID FROM "
				+ " UNS_CreditPaySchedule WHERE UNS_CreditAgreement_ID = ? ORDER BY Line)";
		List<MInvoice> list = new Query(getCtx(), "C_Invoice", wc, 
				get_TrxName()).setParameters(getUNS_CreditAgreement_ID()).
				setOrderBy("DateInvoiced").list();
		MInvoice[] invs = new MInvoice[list.size()];
		list.toArray(invs);
		return invs;
	}
	
	
	public static MUNSCreditAgreement[] gets (String trxName, Properties ctx, 
			String whereClause, String orderBy, Object... params)
	{
		MUNSCreditAgreement[] result = null;
		if (null == orderBy)
			orderBy = COLUMNNAME_DocumentNo;
		List<MUNSCreditAgreement> agreements = Query.get(ctx, 
				UNSOrderModelFactory.EXTENSION_ID, Table_Name, whereClause, 
				trxName).setParameters(params).setOrderBy(orderBy).
				list();
		result = new MUNSCreditAgreement[agreements.size()];
		agreements.toArray(result);
		
		return result;
	}
	
	public static MUNSCreditAgreement[] getCompletedByDate (String trxName, Properties ctx, 
			Timestamp from, Timestamp to)
	{
		StringBuilder sb = new StringBuilder(" DocStatus IN (?,?) ");
		List<Object> list = new ArrayList<>();
		list.add("CO");
		list.add("CL");
		if (from != null && to != null)
		{
			sb.append(" AND DateDoc BETWEEN ? AND ? ");
			list.add(from);
			list.add(to);
		}
		else if (from != null)
		{
			sb.append(" AND DateDoc >= ?");
			list.add(from);
		}
		else if (to != null)
		{
			sb.append(" AND DateDoc <= ?");
			list.add(to);
		}
		
		return gets(trxName, ctx, sb.toString(), COLUMNNAME_DateDoc, 
				list.toArray(new Object[list.size()]));
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}
}