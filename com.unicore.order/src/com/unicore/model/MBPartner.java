/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;


/**
 * @author Burhani Adam
 *
 */
public class MBPartner extends org.compiere.model.MBPartner {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7088414077336321727L;

	/**
	 * 
	 */
	public MBPartner(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param ctx
	 * @param C_Invoice_ID
	 * @param trxName
	 */
	public MBPartner(Properties ctx, int C_BPartner_ID, String trxName)
	{
		super(ctx, C_BPartner_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	private MUNSPOSTerminalCashAcct[] m_lines = null;
	
	public MUNSPOSTerminalCashAcct[] getLines (boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSPOSTerminalCashAcct> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, 
				MUNSPOSTerminalCashAcct.Table_Name, MUNSPOSTerminalCashAcct.COLUMNNAME_Store_ID + " = ?", 
				get_TrxName()).setOnlyActiveRecords(true).setParameters(get_ID()).
				list();
		m_lines = new MUNSPOSTerminalCashAcct[list.size()];
		list.toArray(m_lines);
		
		return m_lines;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if (!loadCashAcounts())
			return false;
		
		return super.afterSave(newRecord, success);
	}
	
	public boolean loadCashAcounts ()
	{
		getLines(false);
		MUNSPOSConfiguration config = MUNSPOSConfiguration.getActive(get_TrxName(), getAD_Org_ID());
		MUNSPOSConfigurationCurr[] currencies = config.getCashCurrency(true);
		for (int i=0; i<currencies.length; i++)
		{
			if (getCurrency(currencies[i].getC_Currency_ID()) != null)
				continue;
			
			MUNSPOSTerminalCashAcct cashAcct = new MUNSPOSTerminalCashAcct(getCtx(), 0, get_TrxName());
			cashAcct.setAD_Org_ID(getAD_Org_ID());
			cashAcct.setStore_ID(get_ID());
			cashAcct.setC_Currency_ID(currencies[i].getC_Currency_ID());
			String sql = "SELECT ISO_Code FROM C_Currency WHERE C_Currency_ID = ?";
			String icode = DB.getSQLValueString(get_TrxName(), sql, currencies[i].getC_Currency_ID());
			String name = getName() + " " + icode;
			cashAcct.setIsUsedForChange(currencies[i].isUsedForChange());
			cashAcct.setName(name);
			if (!cashAcct.save())
				return false;
		}
		return true;
	}
	
	public MUNSPOSTerminalCashAcct getCurrency (int currencyID)
	{
		getLines(false);
		for (int i=0; i<m_lines.length; i++)
			if (m_lines[i].getC_Currency_ID() == currencyID)
				return m_lines[i];
		return null;
	}
}