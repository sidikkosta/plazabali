/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_UnLoadingConfig_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_UnLoadingConfig_Line extends PO implements I_UNS_UnLoadingConfig_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20161030L;

    /** Standard Constructor */
    public X_UNS_UnLoadingConfig_Line (Properties ctx, int UNS_UnLoadingConfig_Line_ID, String trxName)
    {
      super (ctx, UNS_UnLoadingConfig_Line_ID, trxName);
      /** if (UNS_UnLoadingConfig_Line_ID == 0)
        {
			setAmount (Env.ZERO);
			setisAllArmada (false);
// N
			setIsDumpTruck (false);
// N
			setisFixCalculation (false);
// N
			setShippedBy (null);
			setUNS_ArmadaType_ID (0);
			setUNS_UnLoadingConfig_ID (0);
			setUNS_UnLoadingConfig_Line_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UnLoadingConfig_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UnLoadingConfig_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amount 
		Amount in a defined currency
	  */
	public void setAmount (BigDecimal Amount)
	{
		set_Value (COLUMNNAME_Amount, Amount);
	}

	/** Get Amount.
		@return Amount in a defined currency
	  */
	public BigDecimal getAmount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Netto I = NI */
	public static final String CALCULATEBASE_NettoI = "NI";
	/** Netto II = NII */
	public static final String CALCULATEBASE_NettoII = "NII";
	/** Set Calculate Base.
		@param CalculateBase Calculate Base	  */
	public void setCalculateBase (String CalculateBase)
	{

		set_Value (COLUMNNAME_CalculateBase, CalculateBase);
	}

	/** Get Calculate Base.
		@return Calculate Base	  */
	public String getCalculateBase () 
	{
		return (String)get_Value(COLUMNNAME_CalculateBase);
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set All Armada Type.
		@param isAllArmada All Armada Type	  */
	public void setisAllArmada (boolean isAllArmada)
	{
		set_Value (COLUMNNAME_isAllArmada, Boolean.valueOf(isAllArmada));
	}

	/** Get All Armada Type.
		@return All Armada Type	  */
	public boolean isAllArmada () 
	{
		Object oo = get_Value(COLUMNNAME_isAllArmada);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Calculated Remaining Qty.
		@param IsCalcRemainingQty Calculated Remaining Qty	  */
	public void setIsCalcRemainingQty (boolean IsCalcRemainingQty)
	{
		set_Value (COLUMNNAME_IsCalcRemainingQty, Boolean.valueOf(IsCalcRemainingQty));
	}

	/** Get Calculated Remaining Qty.
		@return Calculated Remaining Qty	  */
	public boolean isCalcRemainingQty () 
	{
		Object oo = get_Value(COLUMNNAME_IsCalcRemainingQty);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Is Dump Truck.
		@param IsDumpTruck Is Dump Truck	  */
	public void setIsDumpTruck (boolean IsDumpTruck)
	{
		set_Value (COLUMNNAME_IsDumpTruck, Boolean.valueOf(IsDumpTruck));
	}

	/** Get Is Dump Truck.
		@return Is Dump Truck	  */
	public boolean isDumpTruck () 
	{
		Object oo = get_Value(COLUMNNAME_IsDumpTruck);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Fix Calculation.
		@param isFixCalculation Fix Calculation	  */
	public void setisFixCalculation (boolean isFixCalculation)
	{
		set_Value (COLUMNNAME_isFixCalculation, Boolean.valueOf(isFixCalculation));
	}

	/** Get Fix Calculation.
		@return Fix Calculation	  */
	public boolean isFixCalculation () 
	{
		Object oo = get_Value(COLUMNNAME_isFixCalculation);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_Product_Category getM_Product_Category() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product_Category)MTable.get(getCtx(), org.compiere.model.I_M_Product_Category.Table_Name)
			.getPO(getM_Product_Category_ID(), get_TrxName());	}

	/** Set Product Category.
		@param M_Product_Category_ID 
		Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID)
	{
		if (M_Product_Category_ID < 1) 
			set_Value (COLUMNNAME_M_Product_Category_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_Category_ID, Integer.valueOf(M_Product_Category_ID));
	}

	/** Get Product Category.
		@return Category of a Product
	  */
	public int getM_Product_Category_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Air = Air */
	public static final String SHIPPEDBY_Air = "Air";
	/** Darat = DRT */
	public static final String SHIPPEDBY_Darat = "DRT";
	/** Set Shipped By.
		@param ShippedBy Shipped By	  */
	public void setShippedBy (String ShippedBy)
	{

		set_Value (COLUMNNAME_ShippedBy, ShippedBy);
	}

	/** Get Shipped By.
		@return Shipped By	  */
	public String getShippedBy () 
	{
		return (String)get_Value(COLUMNNAME_ShippedBy);
	}

	/** Set Armada Type.
		@param UNS_ArmadaType_ID Armada Type	  */
	public void setUNS_ArmadaType_ID (int UNS_ArmadaType_ID)
	{
		if (UNS_ArmadaType_ID < 1) 
			set_Value (COLUMNNAME_UNS_ArmadaType_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_ArmadaType_ID, Integer.valueOf(UNS_ArmadaType_ID));
	}

	/** Get Armada Type.
		@return Armada Type	  */
	public int getUNS_ArmadaType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_ArmadaType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public com.unicore.model.I_UNS_UnLoadingConfig getUNS_UnLoadingConfig() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_UnLoadingConfig)MTable.get(getCtx(), com.unicore.model.I_UNS_UnLoadingConfig.Table_Name)
			.getPO(getUNS_UnLoadingConfig_ID(), get_TrxName());	}

	/** Set UnLoading Configurator.
		@param UNS_UnLoadingConfig_ID UnLoading Configurator	  */
	public void setUNS_UnLoadingConfig_ID (int UNS_UnLoadingConfig_ID)
	{
		if (UNS_UnLoadingConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_ID, Integer.valueOf(UNS_UnLoadingConfig_ID));
	}

	/** Get UnLoading Configurator.
		@return UnLoading Configurator	  */
	public int getUNS_UnLoadingConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UnLoading Configurator Line.
		@param UNS_UnLoadingConfig_Line_ID UnLoading Configurator Line	  */
	public void setUNS_UnLoadingConfig_Line_ID (int UNS_UnLoadingConfig_Line_ID)
	{
		if (UNS_UnLoadingConfig_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_Line_ID, Integer.valueOf(UNS_UnLoadingConfig_Line_ID));
	}

	/** Get UnLoading Configurator Line.
		@return UnLoading Configurator Line	  */
	public int getUNS_UnLoadingConfig_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnLoadingConfig_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UnLoadingConfig_Line_UU.
		@param UNS_UnLoadingConfig_Line_UU UNS_UnLoadingConfig_Line_UU	  */
	public void setUNS_UnLoadingConfig_Line_UU (String UNS_UnLoadingConfig_Line_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UnLoadingConfig_Line_UU, UNS_UnLoadingConfig_Line_UU);
	}

	/** Get UNS_UnLoadingConfig_Line_UU.
		@return UNS_UnLoadingConfig_Line_UU	  */
	public String getUNS_UnLoadingConfig_Line_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UnLoadingConfig_Line_UU);
	}
}