/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.base.model.MInvoiceLine;

/**
 * @author nurse
 *
 */
public class MUNSInvDeptAllocation extends X_UNS_InvDeptAllocation 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 423989821653468538L;

	/**
	 * @param ctx
	 * @param UNS_InvDeptAllocation_ID
	 * @param trxName
	 */
	public MUNSInvDeptAllocation(Properties ctx, int UNS_InvDeptAllocation_ID,
			String trxName) {
		super(ctx, UNS_InvDeptAllocation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSInvDeptAllocation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	
	private MInvoiceLine m_parent = null;
	
	public MInvoiceLine getParent ()
	{
		if (m_parent == null)
			m_parent = new MInvoiceLine(getCtx(), getC_InvoiceLine_ID(), get_TrxName());
		return m_parent;
	}
	
	public MUNSInvDeptAllocation (MInvoiceLine parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setC_InvoiceLine_ID(parent.get_ID());
		m_parent = parent;
	}
	
	@Override
	protected boolean beforeSave (boolean newRecord)
	{
		if (newRecord || is_ValueChanged(COLUMNNAME_Qty))
		{
			String sql = "SELECT SUM (qty) FROM UNS_InvDeptAllocation WHERE "
					+ " C_InvoiceLine_ID = ? AND UNS_InvDeptAllocation_ID <> ? AND "
					+ " IsActive = ?";
			BigDecimal otherQty = DB.getSQLValueBD(
					get_TrxName(), sql, getC_InvoiceLine_ID(), get_ID(), "Y");
			if (otherQty == null)
				otherQty = Env.ZERO;
			BigDecimal tobeAllocate = DB.getSQLValueBD(
					get_TrxName(), 
					"SELECT QtyInvoiced FROM C_InvoiceLine WHERE C_InvoiceLine_ID = ?", 
					getC_InvoiceLine_ID());
			if (tobeAllocate == null)
				tobeAllocate = Env.ZERO;
			
			BigDecimal currentQty = otherQty.add(getQty());
			if (currentQty.compareTo(tobeAllocate) == 1)
			{
				log.log(Level.SEVERE, "Over qty allocation");
				return false;
			}
		}
		
		return super.beforeSave(newRecord);
	}
}
