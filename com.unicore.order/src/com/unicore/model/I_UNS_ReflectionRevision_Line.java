/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_ReflectionRevision_Line
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_ReflectionRevision_Line 
{

    /** TableName=UNS_ReflectionRevision_Line */
    public static final String Table_Name = "UNS_ReflectionRevision_Line";

    /** AD_Table_ID=1000329 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name Dura */
    public static final String COLUMNNAME_Dura = "Dura";

	/** Set Dura.
	  * Dura (Kernel Tebal / Buah Tipis)
	  */
	public void setDura (BigDecimal Dura);

	/** Get Dura.
	  * Dura (Kernel Tebal / Buah Tipis)
	  */
	public BigDecimal getDura();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name isFlatDeduction */
    public static final String COLUMNNAME_isFlatDeduction = "isFlatDeduction";

	/** Set Flat Deduction	  */
	public void setisFlatDeduction (boolean isFlatDeduction);

	/** Get Flat Deduction	  */
	public boolean isFlatDeduction();

    /** Column name NettoI */
    public static final String COLUMNNAME_NettoI = "NettoI";

	/** Set Netto-I.
	  * The nett weight of the first group calculation
	  */
	public void setNettoI (BigDecimal NettoI);

	/** Get Netto-I.
	  * The nett weight of the first group calculation
	  */
	public BigDecimal getNettoI();

    /** Column name NettoII */
    public static final String COLUMNNAME_NettoII = "NettoII";

	/** Set Netto-II.
	  * The nett weight of the second group calculation
	  */
	public void setNettoII (BigDecimal NettoII);

	/** Get Netto-II.
	  * The nett weight of the second group calculation
	  */
	public BigDecimal getNettoII();

    /** Column name QtyPayment */
    public static final String COLUMNNAME_QtyPayment = "QtyPayment";

	/** Set Qty Payment	  */
	public void setQtyPayment (BigDecimal QtyPayment);

	/** Get Qty Payment	  */
	public BigDecimal getQtyPayment();

    /** Column name ReflectionFlat */
    public static final String COLUMNNAME_ReflectionFlat = "ReflectionFlat";

	/** Set Reflection Flat	  */
	public void setReflectionFlat (BigDecimal ReflectionFlat);

	/** Get Reflection Flat	  */
	public BigDecimal getReflectionFlat();

    /** Column name ReflectionFlatPerc */
    public static final String COLUMNNAME_ReflectionFlatPerc = "ReflectionFlatPerc";

	/** Set Reflection Flat %	  */
	public void setReflectionFlatPerc (BigDecimal ReflectionFlatPerc);

	/** Get Reflection Flat %	  */
	public BigDecimal getReflectionFlatPerc();

    /** Column name ReflectionPercent */
    public static final String COLUMNNAME_ReflectionPercent = "ReflectionPercent";

	/** Set Reflection %	  */
	public void setReflectionPercent (BigDecimal ReflectionPercent);

	/** Get Reflection %	  */
	public BigDecimal getReflectionPercent();

    /** Column name ReflectionTicket */
    public static final String COLUMNNAME_ReflectionTicket = "ReflectionTicket";

	/** Set Reflection Ticket	  */
	public void setReflectionTicket (BigDecimal ReflectionTicket);

	/** Get Reflection Ticket	  */
	public BigDecimal getReflectionTicket();

    /** Column name RevisionPercent */
    public static final String COLUMNNAME_RevisionPercent = "RevisionPercent";

	/** Set Revision %	  */
	public void setRevisionPercent (BigDecimal RevisionPercent);

	/** Get Revision %	  */
	public BigDecimal getRevisionPercent();

    /** Column name RevisionQty */
    public static final String COLUMNNAME_RevisionQty = "RevisionQty";

	/** Set Revision Qty	  */
	public void setRevisionQty (BigDecimal RevisionQty);

	/** Get Revision Qty	  */
	public BigDecimal getRevisionQty();

    /** Column name UNS_ReflectionRevision_ID */
    public static final String COLUMNNAME_UNS_ReflectionRevision_ID = "UNS_ReflectionRevision_ID";

	/** Set Reflection Revision	  */
	public void setUNS_ReflectionRevision_ID (int UNS_ReflectionRevision_ID);

	/** Get Reflection Revision	  */
	public int getUNS_ReflectionRevision_ID();

	public com.unicore.model.I_UNS_ReflectionRevision getUNS_ReflectionRevision() throws RuntimeException;

    /** Column name UNS_ReflectionRevision_Line_ID */
    public static final String COLUMNNAME_UNS_ReflectionRevision_Line_ID = "UNS_ReflectionRevision_Line_ID";

	/** Set Revision Line	  */
	public void setUNS_ReflectionRevision_Line_ID (int UNS_ReflectionRevision_Line_ID);

	/** Get Revision Line	  */
	public int getUNS_ReflectionRevision_Line_ID();

    /** Column name UNS_ReflectionRevision_Line_UU */
    public static final String COLUMNNAME_UNS_ReflectionRevision_Line_UU = "UNS_ReflectionRevision_Line_UU";

	/** Set UNS_ReflectionRevision_Line_UU	  */
	public void setUNS_ReflectionRevision_Line_UU (String UNS_ReflectionRevision_Line_UU);

	/** Get UNS_ReflectionRevision_Line_UU	  */
	public String getUNS_ReflectionRevision_Line_UU();

    /** Column name UNS_WeighbridgeTicket_ID */
    public static final String COLUMNNAME_UNS_WeighbridgeTicket_ID = "UNS_WeighbridgeTicket_ID";

	/** Set Weighbridge Ticket	  */
	public void setUNS_WeighbridgeTicket_ID (int UNS_WeighbridgeTicket_ID);

	/** Get Weighbridge Ticket	  */
	public int getUNS_WeighbridgeTicket_ID();

	public com.unicore.model.I_UNS_WeighbridgeTicket getUNS_WeighbridgeTicket() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
