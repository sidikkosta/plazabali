/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_PreOrder
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_PreOrder extends PO implements I_UNS_PreOrder, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160114L;

    /** Standard Constructor */
    public X_UNS_PreOrder (Properties ctx, int UNS_PreOrder_ID, String trxName)
    {
      super (ctx, UNS_PreOrder_ID, trxName);
      /** if (UNS_PreOrder_ID == 0)
        {
			setC_BPartner_ID (0);
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocumentNo (null);
			setIsNeedAppointment (false);
// N
			setProcessed (false);
			setUNS_PreOrder_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_PreOrder (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_PreOrder[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Attention Address.
		@param AttAddress Attention Address	  */
	public void setAttAddress (String AttAddress)
	{
		set_Value (COLUMNNAME_AttAddress, AttAddress);
	}

	/** Get Attention Address.
		@return Attention Address	  */
	public String getAttAddress () 
	{
		return (String)get_Value(COLUMNNAME_AttAddress);
	}

	/** Set Attention Name.
		@param AttName Attention Name	  */
	public void setAttName (String AttName)
	{
		set_Value (COLUMNNAME_AttName, AttName);
	}

	/** Get Attention Name.
		@return Attention Name	  */
	public String getAttName () 
	{
		return (String)get_Value(COLUMNNAME_AttName);
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create Others Customer.
		@param CreateOthersCustomer Create Others Customer	  */
	public void setCreateOthersCustomer (String CreateOthersCustomer)
	{
		set_Value (COLUMNNAME_CreateOthersCustomer, CreateOthersCustomer);
	}

	/** Get Create Others Customer.
		@return Create Others Customer	  */
	public String getCreateOthersCustomer () 
	{
		return (String)get_Value(COLUMNNAME_CreateOthersCustomer);
	}

	/** Set Create SO.
		@param CreateSO Create SO	  */
	public void setCreateSO (String CreateSO)
	{
		set_Value (COLUMNNAME_CreateSO, CreateSO);
	}

	/** Get Create SO.
		@return Create SO	  */
	public String getCreateSO () 
	{
		return (String)get_Value(COLUMNNAME_CreateSO);
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_ValueNoCheck (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Finish Date.
		@param DateFinish 
		Finish or (planned) completion date
	  */
	public void setDateFinish (Timestamp DateFinish)
	{
		set_ValueNoCheck (COLUMNNAME_DateFinish, DateFinish);
	}

	/** Get Finish Date.
		@return Finish or (planned) completion date
	  */
	public Timestamp getDateFinish () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFinish);
	}

	/** Set Date Promised.
		@param DatePromised 
		Date Order was promised
	  */
	public void setDatePromised (Timestamp DatePromised)
	{
		set_Value (COLUMNNAME_DatePromised, DatePromised);
	}

	/** Get Date Promised.
		@return Date Order was promised
	  */
	public Timestamp getDatePromised () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DatePromised);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=135 */
	public static final int DOCACTION_AD_Reference_ID=135;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Approve = AP */
	public static final String DOCACTION_Approve = "AP";
	/** Reject = RJ */
	public static final String DOCACTION_Reject = "RJ";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Void = VO */
	public static final String DOCACTION_Void = "VO";
	/** Close = CL */
	public static final String DOCACTION_Close = "CL";
	/** Reverse - Correct = RC */
	public static final String DOCACTION_Reverse_Correct = "RC";
	/** Reverse - Accrual = RA */
	public static final String DOCACTION_Reverse_Accrual = "RA";
	/** Invalidate = IN */
	public static final String DOCACTION_Invalidate = "IN";
	/** Re-activate = RE */
	public static final String DOCACTION_Re_Activate = "RE";
	/** <None> = -- */
	public static final String DOCACTION_None = "--";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Unlock = XL */
	public static final String DOCACTION_Unlock = "XL";
	/** Wait Complete = WC */
	public static final String DOCACTION_WaitComplete = "WC";
	/** Confirmed = CF */
	public static final String DOCACTION_Confirmed = "CF";
	/** Finished = FN */
	public static final String DOCACTION_Finished = "FN";
	/** Cancelled = CN */
	public static final String DOCACTION_Cancelled = "CN";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	/** DocStatus AD_Reference_ID=131 */
	public static final int DOCSTATUS_AD_Reference_ID=131;
	/** Drafted = DR */
	public static final String DOCSTATUS_Drafted = "DR";
	/** Completed = CO */
	public static final String DOCSTATUS_Completed = "CO";
	/** Approved = AP */
	public static final String DOCSTATUS_Approved = "AP";
	/** Not Approved = NA */
	public static final String DOCSTATUS_NotApproved = "NA";
	/** Voided = VO */
	public static final String DOCSTATUS_Voided = "VO";
	/** Invalid = IN */
	public static final String DOCSTATUS_Invalid = "IN";
	/** Reversed = RE */
	public static final String DOCSTATUS_Reversed = "RE";
	/** Closed = CL */
	public static final String DOCSTATUS_Closed = "CL";
	/** Unknown = ?? */
	public static final String DOCSTATUS_Unknown = "??";
	/** In Progress = IP */
	public static final String DOCSTATUS_InProgress = "IP";
	/** Waiting Payment = WP */
	public static final String DOCSTATUS_WaitingPayment = "WP";
	/** Waiting Confirmation = WC */
	public static final String DOCSTATUS_WaitingConfirmation = "WC";
	/** Set Document Status.
		@param DocStatus 
		The current status of the document
	  */
	public void setDocStatus (String DocStatus)
	{

		set_Value (COLUMNNAME_DocStatus, DocStatus);
	}

	/** Get Document Status.
		@return The current status of the document
	  */
	public String getDocStatus () 
	{
		return (String)get_Value(COLUMNNAME_DocStatus);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Is Need Appointment.
		@param IsNeedAppointment 
		To indicate if the shipment need appointment with the customer
	  */
	public void setIsNeedAppointment (boolean IsNeedAppointment)
	{
		set_Value (COLUMNNAME_IsNeedAppointment, Boolean.valueOf(IsNeedAppointment));
	}

	/** Get Is Need Appointment.
		@return To indicate if the shipment need appointment with the customer
	  */
	public boolean isNeedAppointment () 
	{
		Object oo = get_Value(COLUMNNAME_IsNeedAppointment);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set One Shipment.
		@param IsOneShipment One Shipment	  */
	public void setIsOneShipment (boolean IsOneShipment)
	{
		set_Value (COLUMNNAME_IsOneShipment, Boolean.valueOf(IsOneShipment));
	}

	/** Get One Shipment.
		@return One Shipment	  */
	public boolean isOneShipment () 
	{
		Object oo = get_Value(COLUMNNAME_IsOneShipment);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Sales Transaction.
		@param IsSOTrx 
		This is a Sales Transaction
	  */
	public void setIsSOTrx (boolean IsSOTrx)
	{
		set_Value (COLUMNNAME_IsSOTrx, Boolean.valueOf(IsSOTrx));
	}

	/** Get Sales Transaction.
		@return This is a Sales Transaction
	  */
	public boolean isSOTrx () 
	{
		Object oo = get_Value(COLUMNNAME_IsSOTrx);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed On.
		@param ProcessedOn 
		The date+time (expressed in decimal format) when the document has been processed
	  */
	public void setProcessedOn (BigDecimal ProcessedOn)
	{
		set_Value (COLUMNNAME_ProcessedOn, ProcessedOn);
	}

	/** Get Processed On.
		@return The date+time (expressed in decimal format) when the document has been processed
	  */
	public BigDecimal getProcessedOn () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ProcessedOn);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Process Now.
		@param Processing Process Now	  */
	public void setProcessing (boolean Processing)
	{
		set_Value (COLUMNNAME_Processing, Boolean.valueOf(Processing));
	}

	/** Get Process Now.
		@return Process Now	  */
	public boolean isProcessing () 
	{
		Object oo = get_Value(COLUMNNAME_Processing);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Shipper.
		@param Shipper Shipper	  */
	public void setShipper (String Shipper)
	{
		set_Value (COLUMNNAME_Shipper, Shipper);
	}

	/** Get Shipper.
		@return Shipper	  */
	public String getShipper () 
	{
		return (String)get_Value(COLUMNNAME_Shipper);
	}

	public com.unicore.model.I_UNS_OthersCustomer getUNS_OthersCustomer() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_OthersCustomer)MTable.get(getCtx(), com.unicore.model.I_UNS_OthersCustomer.Table_Name)
			.getPO(getUNS_OthersCustomer_ID(), get_TrxName());	}

	/** Set Others Customer (Serba-Serbi).
		@param UNS_OthersCustomer_ID Others Customer (Serba-Serbi)	  */
	public void setUNS_OthersCustomer_ID (int UNS_OthersCustomer_ID)
	{
		if (UNS_OthersCustomer_ID < 1) 
			set_Value (COLUMNNAME_UNS_OthersCustomer_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_OthersCustomer_ID, Integer.valueOf(UNS_OthersCustomer_ID));
	}

	/** Get Others Customer (Serba-Serbi).
		@return Others Customer (Serba-Serbi)	  */
	public int getUNS_OthersCustomer_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_OthersCustomer_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Pre Order.
		@param UNS_PreOrder_ID Pre Order	  */
	public void setUNS_PreOrder_ID (int UNS_PreOrder_ID)
	{
		if (UNS_PreOrder_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_ID, Integer.valueOf(UNS_PreOrder_ID));
	}

	/** Get Pre Order.
		@return Pre Order	  */
	public int getUNS_PreOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_PreOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_PreOrder_UU.
		@param UNS_PreOrder_UU UNS_PreOrder_UU	  */
	public void setUNS_PreOrder_UU (String UNS_PreOrder_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_PreOrder_UU, UNS_PreOrder_UU);
	}

	/** Get UNS_PreOrder_UU.
		@return UNS_PreOrder_UU	  */
	public String getUNS_PreOrder_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_PreOrder_UU);
	}

	/** Set Rayon.
		@param UNS_Rayon_ID Rayon	  */
	public void setUNS_Rayon_ID (int UNS_Rayon_ID)
	{
		if (UNS_Rayon_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_Rayon_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_Rayon_ID, Integer.valueOf(UNS_Rayon_ID));
	}

	/** Get Rayon.
		@return Rayon	  */
	public int getUNS_Rayon_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Rayon_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Shipping Track.
		@param UNS_Shipping_Track_ID 
		The reference into which track the shippment will be grouped
	  */
	public void setUNS_Shipping_Track_ID (int UNS_Shipping_Track_ID)
	{
		if (UNS_Shipping_Track_ID < 1) 
			set_Value (COLUMNNAME_UNS_Shipping_Track_ID, null);
		else 
			set_Value (COLUMNNAME_UNS_Shipping_Track_ID, Integer.valueOf(UNS_Shipping_Track_ID));
	}

	/** Get Shipping Track.
		@return The reference into which track the shippment will be grouped
	  */
	public int getUNS_Shipping_Track_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_Shipping_Track_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}