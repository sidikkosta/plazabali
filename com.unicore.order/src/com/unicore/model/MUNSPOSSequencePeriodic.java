/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSPOSSequencePeriodic extends X_UNS_POS_SequencePeriodic 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3446616010781720573L;
	private MUNSPOSSequence m_parent = null;
	
	/**
	 * @param ctx
	 * @param UNS_POS_SequencePeriodic_ID
	 * @param trxName
	 */
	public MUNSPOSSequencePeriodic(Properties ctx,
			int UNS_POS_SequencePeriodic_ID, String trxName) 
	{
		super(ctx, UNS_POS_SequencePeriodic_ID, trxName);
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSPOSSequencePeriodic(Properties ctx, ResultSet rs, String trxName) 
	{
		super(ctx, rs, trxName);
	}

	public MUNSPOSSequence getParent ()
	{
		if (m_parent == null)
			m_parent = new MUNSPOSSequence(getCtx(), getUNS_POS_Sequence_ID(), get_TrxName());
		return m_parent;
	}
	
	public MUNSPOSSequencePeriodic (MUNSPOSSequence parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setUNS_POS_Sequence_ID(parent.get_ID());
		setCurrent(parent.getStartNo());
		setNext(parent.getStartNo());
	}
	
	public static MUNSPOSSequencePeriodic get (String trxName, int sequenceID, String key)
	{
		String wc = "UNS_POS_Sequence_ID = ? AND UniqueCode = ?";
		return Query.get(
				Env.getCtx(), UNSOrderModelFactory.EXTENSION_ID, Table_Name, wc, trxName).
				setParameters(sequenceID, key).firstOnly();
	}
	
	public static MUNSPOSSequencePeriodic getCreate (MUNSPOSSequence sequence, String key)
	{
		MUNSPOSSequencePeriodic periodic = get(sequence.get_TrxName (), sequence.get_ID(), key);
		if (periodic == null)
		{
			periodic = new MUNSPOSSequencePeriodic(sequence);
			periodic.setUniqueCode(key);
			if (!periodic.save())
				return null;
		}
		return periodic;
	}
	
	public String getNextSequence ()
	{
		StringBuilder trxBuild = new StringBuilder();
		String prefix = getParent().getPrefix();
		if (null != prefix)
		{
			trxBuild.append(prefix);
		}
		
		int max = getParent().getLength();
		String format = "%" + max + "d";
		String trxNo = String.format(format, getNext()).replace(" ", "0");		
		trxBuild.append(trxNo);
		
		String sufix = getParent().getSufix();
		if (sufix != null)
		{
			trxBuild.append(sufix);
		}
		
		trxNo = trxBuild.toString();
		setCurrent(getNext());
		setNext(getCurrent() + 1);
		return trxNo;
	}
	
	public static String getNextSequence (MUNSPOSSequence sequence, String key)
	{
		MUNSPOSSequencePeriodic periodic = getCreate(sequence, key);
		DB.getDatabase().forUpdate(periodic, 0);
		String next = periodic.getNextSequence();
		periodic.saveEx();
		return next;
	}
}
