/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class MUNSReflectionRevisionLine extends X_UNS_ReflectionRevision_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8005486042356890383L;
	private MUNSWeighbridgeTicket m_Ticket = null;

	/**
	 * @param ctx
	 * @param UNS_ReflectionRevision_Line_ID
	 * @param trxName
	 */
	public MUNSReflectionRevisionLine(Properties ctx,
			int UNS_ReflectionRevision_Line_ID, String trxName) {
		super(ctx, UNS_ReflectionRevision_Line_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSReflectionRevisionLine(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean beforeSave(boolean newRecord)
	{
//		BigDecimal convertReflection = Env.ZERO;
	
		if(getUNS_WeighbridgeTicket_ID() > 0 && newRecord)
		{
			m_Ticket = new MUNSWeighbridgeTicket(getCtx(), getUNS_WeighbridgeTicket_ID(), get_TrxName());
			setAD_Org_ID(m_Ticket.getAD_Org_ID());
			setNettoI(m_Ticket.getNettoI());
			setNettoII(m_Ticket.getNettoII());
			setC_BPartner_ID(m_Ticket.getC_BPartner_ID());
			
			BigDecimal invoiceFlatDeduction = DB.getSQLValueBD(
					get_TrxName(), " SELECT MAX(InvoiceFlatDeduction) "
							+ " FROM UNS_Order_Contract WHERE C_Order_ID = ? "
							+ " AND IsActive = 'Y'", 
					m_Ticket.getC_Order_ID());
			
			if(invoiceFlatDeduction.signum() != 0)
			{
				setisFlatDeduction(true);
				BigDecimal deduction = getNettoI().multiply(invoiceFlatDeduction);
				deduction = deduction.divide(Env.ONEHUNDRED, 5, 
						RoundingMode.UP);
				deduction = deduction.setScale(0, RoundingMode.UP);
				setReflectionFlat(deduction);
				setReflectionFlatPerc(invoiceFlatDeduction);
			}
		
			BigDecimal percent = m_Ticket.getReflection().divide(getNettoI(), 5, RoundingMode.UP);
			setReflectionPercent(percent.multiply(Env.ONEHUNDRED));
			setReflectionTicket(m_Ticket.getReflection());
		}
		
//		BigDecimal reflection = isFlatDeduction() ? getReflectionFlat() : getReflectionTicket();
//		BigDecimal qtyPayment = getNettoI().subtract(reflection);
		
//		if((newRecord && getRevisionQty().compareTo(Env.ZERO) != 0)
//				|| (!newRecord && is_ValueChanged(COLUMNNAME_RevisionQty)))
//		{
//			convertReflection = (getRevisionQty().divide(qtyPayment.add(reflection), 5, RoundingMode.UP)).multiply(Env.ONEHUNDRED);
//			qtyPayment = qtyPayment.subtract(getRevisionQty()).add(reflection);
//			setRevisionPercent(convertReflection);
//		}
//		else if((newRecord && getRevisionPercent().compareTo(Env.ZERO) != 0)
//				|| (!newRecord && is_ValueChanged(COLUMNNAME_RevisionPercent)))
//		{
//			convertReflection = (getRevisionPercent().multiply((qtyPayment.add(reflection)))).divide(Env.ONEHUNDRED, 5, RoundingMode.UP);
//			qtyPayment = qtyPayment.subtract(convertReflection).add(reflection);
//			setRevisionQty(convertReflection);
//		}
		
//		setQtyPayment(qtyPayment);
		
		return true;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if(m_Ticket != null && m_Ticket.getUNS_ReflectionRevision_Line_ID() <=0)
		{
			m_Ticket.setUNS_ReflectionRevision_ID(getUNS_ReflectionRevision_ID());
			m_Ticket.setUNS_ReflectionRevision_Line_ID(get_ID());
			m_Ticket.saveEx();
		}
		
		return true;
	}
	
	protected boolean beforeDelete()
	{
		String sql = "UPDATE UNS_WeighbridgeTicket SET UNS_ReflectionRevision_Line_ID = null, UNS_ReflectionRevision_ID = null"
				+ " WHERE UNS_WeighbridgeTicket_ID = ?";
		DB.executeUpdate(sql, getUNS_WeighbridgeTicket_ID(), get_TrxName());
		return true;
	}
}
