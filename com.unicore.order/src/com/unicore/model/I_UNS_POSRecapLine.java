/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_POSRecapLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_POSRecapLine 
{

    /** TableName=UNS_POSRecapLine */
    public static final String Table_Name = "UNS_POSRecapLine";

    /** AD_Table_ID=1000414 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CostAmt */
    public static final String COLUMNNAME_CostAmt = "CostAmt";

	/** Set Cost Value.
	  * Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt);

	/** Get Cost Value.
	  * Value with Cost
	  */
	public BigDecimal getCostAmt();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DiscountAmt */
    public static final String COLUMNNAME_DiscountAmt = "DiscountAmt";

	/** Set Discount Amount.
	  * Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt);

	/** Get Discount Amount.
	  * Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt();

    /** Column name ETBBCode */
    public static final String COLUMNNAME_ETBBCode = "ETBBCode";

	/** Set ETBB Code	  */
	public void setETBBCode (String ETBBCode);

	/** Get ETBB Code	  */
	public String getETBBCode();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsConsignment */
    public static final String COLUMNNAME_IsConsignment = "IsConsignment";

	/** Set Consignment	  */
	public void setIsConsignment (boolean IsConsignment);

	/** Get Consignment	  */
	public boolean isConsignment();

    /** Column name isRefund */
    public static final String COLUMNNAME_isRefund = "isRefund";

	/** Set Refund	  */
	public void setisRefund (boolean isRefund);

	/** Get Refund	  */
	public boolean isRefund();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name PushMoneyAmt */
    public static final String COLUMNNAME_PushMoneyAmt = "PushMoneyAmt";

	/** Set Push Money Amount	  */
	public void setPushMoneyAmt (BigDecimal PushMoneyAmt);

	/** Get Push Money Amount	  */
	public BigDecimal getPushMoneyAmt();

    /** Column name PushMoneyPerUnit */
    public static final String COLUMNNAME_PushMoneyPerUnit = "PushMoneyPerUnit";

	/** Set Push Money Per Unit	  */
	public void setPushMoneyPerUnit (BigDecimal PushMoneyPerUnit);

	/** Get Push Money Per Unit	  */
	public BigDecimal getPushMoneyPerUnit();

    /** Column name Qty */
    public static final String COLUMNNAME_Qty = "Qty";

	/** Set Quantity.
	  * Quantity
	  */
	public void setQty (BigDecimal Qty);

	/** Get Quantity.
	  * Quantity
	  */
	public BigDecimal getQty();

    /** Column name RevenueAmt */
    public static final String COLUMNNAME_RevenueAmt = "RevenueAmt";

	/** Set Revenue Amount	  */
	public void setRevenueAmt (BigDecimal RevenueAmt);

	/** Get Revenue Amount	  */
	public BigDecimal getRevenueAmt();

    /** Column name SalesAmt */
    public static final String COLUMNNAME_SalesAmt = "SalesAmt";

	/** Set Sales Amount	  */
	public void setSalesAmt (BigDecimal SalesAmt);

	/** Get Sales Amount	  */
	public BigDecimal getSalesAmt();

    /** Column name ServiceChargeAmt */
    public static final String COLUMNNAME_ServiceChargeAmt = "ServiceChargeAmt";

	/** Set Service Charge Amount	  */
	public void setServiceChargeAmt (BigDecimal ServiceChargeAmt);

	/** Get Service Charge Amount	  */
	public BigDecimal getServiceChargeAmt();

    /** Column name SKU */
    public static final String COLUMNNAME_SKU = "SKU";

	/** Set SKU.
	  * Stock Keeping Unit
	  */
	public void setSKU (String SKU);

	/** Get SKU.
	  * Stock Keeping Unit
	  */
	public String getSKU();

    /** Column name TaxAmt */
    public static final String COLUMNNAME_TaxAmt = "TaxAmt";

	/** Set Tax Amount.
	  * Tax Amount for a document
	  */
	public void setTaxAmt (BigDecimal TaxAmt);

	/** Get Tax Amount.
	  * Tax Amount for a document
	  */
	public BigDecimal getTaxAmt();

    /** Column name UNS_POSRecap_ID */
    public static final String COLUMNNAME_UNS_POSRecap_ID = "UNS_POSRecap_ID";

	/** Set POS Recapitulation	  */
	public void setUNS_POSRecap_ID (int UNS_POSRecap_ID);

	/** Get POS Recapitulation	  */
	public int getUNS_POSRecap_ID();

	public I_UNS_POSRecap getUNS_POSRecap() throws RuntimeException;

    /** Column name UNS_POSRecapLine_ID */
    public static final String COLUMNNAME_UNS_POSRecapLine_ID = "UNS_POSRecapLine_ID";

	/** Set POS Recapitulation Line	  */
	public void setUNS_POSRecapLine_ID (int UNS_POSRecapLine_ID);

	/** Get POS Recapitulation Line	  */
	public int getUNS_POSRecapLine_ID();

    /** Column name UNS_POSRecapLine_UU */
    public static final String COLUMNNAME_UNS_POSRecapLine_UU = "UNS_POSRecapLine_UU";

	/** Set UNS_POSRecapLine_UU	  */
	public void setUNS_POSRecapLine_UU (String UNS_POSRecapLine_UU);

	/** Get UNS_POSRecapLine_UU	  */
	public String getUNS_POSRecapLine_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
