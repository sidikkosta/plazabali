/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSStorageReservation extends X_UNS_StorageReservation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3665997234357377358L;

	/**
	 * @param ctx
	 * @param UNS_StorageReservation_ID
	 * @param trxName
	 */
	public MUNSStorageReservation(Properties ctx,
			int UNS_StorageReservation_ID, String trxName) {
		super(ctx, UNS_StorageReservation_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSStorageReservation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public static MUNSStorageReservation addOrCreate(Properties ctx, int M_Warehouse_ID, int M_Product_ID,
			int M_AttributeSetInstance_ID, BigDecimal Qty, String trxName)
	{
		MUNSStorageReservation value = MUNSStorageReservation.get(
				ctx, M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID, trxName);
		
		if(value == null)
		{
			value = new MUNSStorageReservation(ctx, 0, trxName);
			value.setM_Warehouse_ID(M_Warehouse_ID);
			value.setAD_Org_ID(value.getM_Warehouse().getAD_Org_ID());
			value.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
			value.setM_Product_ID(M_Product_ID);
			value.setQty(Qty);
			value.setReservationType(RESERVATIONTYPE_Requisition);
			value.saveEx();
		}
		else
		{
			value.setQty(value.getQty().add(Qty));
			value.saveEx();
		}
		
		return value;
	}
	
	public static MUNSStorageReservation get(Properties ctx, int M_Warehouse_ID, int M_Product_ID,
			int M_AttributeSetInstance_ID, String trxName)
	{
		MUNSStorageReservation value = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID,
				Table_Name, "M_Warehouse_ID = ? AND M_Product_ID = ? AND M_AttributeSetInstance_ID = ?",
				trxName).setParameters(M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID)
					.firstOnly();
		
		return value;
	}
}