/**
 * 
 */
package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.AverageCostingNegativeQtyException;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartner;
import org.compiere.model.MCost;
import org.compiere.model.MCostDetail;
import org.compiere.model.MDocType;
import org.compiere.model.MProduct;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;

import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author Burhani Adam
 *
 */
public class MUNSSalesReconciliation extends X_UNS_SalesReconciliation implements DocAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6126300017340985701L;

	public MUNSSalesReconciliation(Properties ctx,
			int UNS_SalesReconciliation_ID, String trxName) {
		super(ctx, UNS_SalesReconciliation_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSSalesReconciliation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MUNSSalesReconciliation[");
		sb.append(get_ID()).append("-").append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "UNS_SalesReconciliation_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{	
		
		return true;
	}	
	
	protected boolean beforeDelete()
	{
		return false;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
//		if(isMainServer())
//		{
			m_processMsg = validateSession();
			if(null != m_processMsg)
				return DocAction.STATUS_Invalid;
//		}
//		else
//		{
//			MUNSPOSRecapLine[] lines = getRecapLines();
//			for(int i=0;i<lines.length;i++)
//			{
//				generatesMA(lines[i]);
//			}
//		}
			
		if(!isValidated())
		{
			m_processMsg = "Please run validation record first or checklis Validated.";
			setProcessed(false);
			return DocAction.STATUS_Invalid;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		if(isReplication())
		{
			return STATUS_InProgress; 
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}
//		
//		if(isMainServer())
//		{
//			m_processMsg = createTransaction();
//			if(null != m_processMsg)
//				return DocAction.STATUS_Invalid;
//			m_processMsg = createStatement();
//			if(null != m_processMsg)
//				return DocAction.STATUS_Invalid;
//		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reActivateIt - " + toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;

	//	setProcessed(false);
		if (! reverseCorrectIt())
			return false;

		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		return true;
	}	//	reActivateIt

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessMsg() {
		// TODO Auto-generated method stub
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean approveIt() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean rejectIt() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public static MUNSSalesReconciliation getCreate(Properties ctx, int C_BPartner_ID, 
			java.sql.Timestamp dateTrx, int AD_Org_ID, int C_Currency_ID, String trxName)
	{
		MBPartner bp = MBPartner.get(ctx, C_BPartner_ID);
		String whereClause = "C_BP_Group_ID = ? AND DateTrx = ? AND AD_Org_ID = ? AND Processed = 'N'";
		MUNSSalesReconciliation record = Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, Table_Name, 
						whereClause, trxName).setParameters(bp.getC_BP_Group_ID(), dateTrx, AD_Org_ID)
							.first();
		
		if(record == null)
		{
			record = new MUNSSalesReconciliation(ctx, 0, trxName);
			record.setAD_Org_ID(AD_Org_ID);
			record.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_SalesReconciliation));
			record.setC_BP_Group_ID(bp.getC_BP_Group_ID());
			record.setDateTrx(dateTrx);
			record.setC_Currency_ID(C_Currency_ID);
			record.saveEx();
		}
		
		return record;
	}
	
	private String validateSession()
	{
		String msg = null; 
		String sql = "SELECT ss.DocumentNo, COALESCE(cashier.RealName, cashier.Name) FROM UNS_POS_Session ss"
				+ " INNER JOIN UNS_POSTrx trx ON trx.UNS_POS_Session_ID = ss.UNS_POS_Session_ID"
				+ " INNER JOIN UNS_POSTrxLine tl ON tl.UNS_POSTrx_ID = trx.UNS_POSTrx_ID"
				+ " INNER JOIN UNS_POSRecapLine rl ON rl.UNS_POSRecapLine_ID = tl.UNS_POSRecapLine_ID"
				+ " INNER JOIN UNS_POSRecap pr ON pr.UNS_POSRecap_ID = rl.UNS_POSRecap_ID"
				+ " INNER JOIN AD_User cashier ON cashier.AD_User_ID = ss.Cashier_ID"
				+ " WHERE pr.UNS_SalesReconciliation_ID = ? AND ss.DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')"
				+ " GROUP BY ss.DocumentNo, cashier.RealName, cashier.Name";
		
		List<List<Object>> value = DB.getSQLArrayObjectsEx(get_TrxName(), sql, get_ID());
		if(value == null)
			return null;
		
		for(int i=0;i<value.size();i++)
		{
			if(null == msg)
				msg = "Please complete pos session first."
						+ " " + value.get(i).get(0) + "-" + value.get(i).get(1) + ";";
			else
				msg += " " + value.get(i).get(0) + "-" + value.get(i).get(1) + ";";
		}
		
		return msg;
	}
	
//	private boolean isMainServer()
//	{
//		String mainServer = MSysConfig.getValue(MSysConfig.MAIN_SERVER, "");
//		String serverType = MSysConfig.getValue(MSysConfig.SERVER_TYPE, "");
//		
//		return !Util.isEmpty(mainServer, true) && mainServer.equals(serverType); 
//	}
	
	public String createTransaction(MAcctSchema as)
	{
		String sql = "SELECT rl.M_Product_ID, l.M_Warehouse_ID, l.M_locator_ID,"
				+ " ma.M_AttributeSetInstance_ID, ma.Qty, rl.UNS_POSRecapLine_ID, rl.IsConsignment,"
				+ " ma.DateMaterialPolicy"
				+ " FROM UNS_POSRecapLine_MA ma"
				+ " INNER JOIN UNS_POSRecapLine rl ON rl.UNS_POSRecapLine_ID = ma.UNS_POSRecapLine_ID"
				+ " INNER JOIN UNS_POSRecap pr ON pr.UNS_POSRecap_ID = rl.UNS_POSRecap_ID"
				+ " INNER JOIN M_Locator l ON l.M_Locator_ID = ma.M_Locator_ID"
				+ " WHERE pr.UNS_SalesReconciliation_ID = ?";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		Timestamp endOfNov2019 = TimeUtil.getDay(2019, 11, 30);
		boolean isAfterEndOfNov2019 = getDateTrx().after(endOfNov2019);
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, get_ID());
			rs = stmt.executeQuery();
			while (rs.next()) {
				int M_Product_ID = rs.getInt(1);
				int M_Warehouse_ID = rs.getInt(2);
//				int M_Locator_ID = rs.getInt(3);
				int asi = rs.getInt(4);
				BigDecimal qty = rs.getBigDecimal(5);
				int POSRecapLineID = rs.getInt(6);
				
				if(qty.compareTo(Env.ZERO) == 0)
					continue;
				
				/**String moveType = null;
				if(qty.compareTo(Env.ZERO) > 0)
					moveType = MTransaction.MOVEMENTTYPE_POSOrder;
				else
					moveType = MTransaction.MOVEMENTTYPE_POSReturn;
				
				if (!MStorageOnHand.add(getCtx(), M_Warehouse_ID, M_Locator_ID,
						M_Product_ID, asi, qty.negate(), rs.getTimestamp(8), get_TrxName()))
						return "Cannot correct Inventory OnHand (MA) - " + CLogger.retrieveErrorString("Error");
				
				MTransaction trx =
						new MTransaction(getCtx(), getAD_Org_ID(), moveType,
								M_Locator_ID, M_Product_ID, asi
								, qty.negate(), getDateTrx(), get_TrxName());
				if (!trx.save())
					return "Transaction not inserted. " + CLogger.retrieveErrorString("Error");
					*/
				
//				MAcctSchema[] accts = MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID());
//				MAcctSchema acct = null;
//
//				for(int i=0;i<accts.length;i++)
//				{
//					if(accts[i].getC_Currency_ID() == getC_Currency_ID())
//					{
//						acct = accts[i];
//						break;
//					}
//				}
				
				String cm = rs.getString(7).equals("Y") ? MCost.COSTINGMETHOD_StandardCosting
								: MCost.COSTINGMETHOD_AverageInvoice;
				if(rs.getString(7) == null || !rs.getString(7).equals("Y"))
				{
					BigDecimal cost = Env.ZERO;
					MCostDetail cd = MCostDetail.get(
							getCtx(), "UNS_POSRecapLine_ID=?", POSRecapLineID, asi, 
							as.get_ID(), get_TrxName());
					if (cd == null)
						cost = MCost.getCurrentCost(MProduct.get(getCtx(), M_Product_ID),
								asi, as, getAD_Org_ID(), cm, qty, POSRecapLineID, true, get_TrxName());
					else
						cost = cd.getCurrentCostPrice().multiply(qty);
					
					try
					{
						if (!com.uns.base.model.MCostDetail.createPOSRecapLine(
								as, getAD_Org_ID(), UNSOrderModelFactory.EXTENSION_ID,
								M_Product_ID, asi,
								POSRecapLineID, 0, cost.abs(), qty.negate(), null, get_TrxName()))
						{
							if (isAfterEndOfNov2019)
								return "Failed to create cost detail record";
						}
					}
					catch (AverageCostingNegativeQtyException e)
					{
						if (isAfterEndOfNov2019) {
							MProduct p = MProduct.get(getCtx(), M_Product_ID);
							String _sql = "SELECT Description FROM M_AttributeSetInstance WHERE M_AttributeSetInstance_ID = ?";
							String asiDesc = DB.getSQLValueString(get_TrxName(), _sql, asi);
							MWarehouse w = MWarehouse.get(getCtx(), M_Warehouse_ID);
							StringBuilder erBuild = new StringBuilder("Average Cost Negative Product=");
							erBuild.append(p.getSKU()).append(", ETBB=").append(asiDesc).append(", Shop=")
							.append(w.getValue());
							String errMsg = erBuild.toString();
							return errMsg;
						}
					}
				}
			}
		} catch (SQLException e) {
			return e.getMessage();
		}
		
		return null;
	}
	
//	@SuppressWarnings("unused")
//	private String createStatement()
//	{
//		MUNSCashReconciliation[] cash = getCash();
//		
//		for(int i=0;i<cash.length;i++)
//		{
//			if(cash[i].getC_BankAccount_ID() <= 0)
//				return "Bank Account for " + cash[i].getC_Currency().getISO_Code()
//						+ " not yet define.";
//			if(cash[i].getC_BankStatementLine_ID() > 0)
//				continue;
//			
//			MBankStatement bst = MBankStatement.getOpen(cash[i].getC_BankAccount_ID(), get_TrxName(), true);
//			MBankStatementLine line = new MBankStatementLine(bst);
//			line.setTransactionType(MBankStatementLine.TRANSACTIONTYPE_ARTransaction);
//			line.setStmtAmt(cash[i].getCashDepositAmt());
//			line.saveEx();
//		}
//		
//		return null;
//	}
	
	public MUNSCashReconciliation[] getCash()
	{
		List<MUNSCashReconciliation> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSCashReconciliation.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSCashReconciliation[list.size()]);
	}
	
	public MUNSEDCReconciliation[] getEDC()
	{
		List<MUNSEDCReconciliation> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSEDCReconciliation.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSEDCReconciliation[list.size()]);
	}
	
	public MUNSPOSRecap[] getRecap()
	{
		List<MUNSPOSRecap> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSPOSRecap.Table_Name, Table_Name + "_ID=?", get_TrxName())
					.setParameters(get_ID()).list();
		
		return list.toArray(new MUNSPOSRecap[list.size()]);
	}
	
	@SuppressWarnings("unused")
	private void generatesMA(MUNSPOSRecapLine rLine)
	{
		String sql = "SELECT l.UNS_POSTrxLine_ID FROM UNS_POSTrxLine l"
				+ " INNER JOIN UNS_POSTrx t ON t.UNS_POSTrx_ID = l.UNS_POSTrx_ID"
				+ " WHERE t.DateAcct = ? AND t.C_BPartner_ID = ? AND t.DocStatus IN ('CO', 'CL')"
				+ " AND l.M_Product_ID = ?";
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setTimestamp(1, getDateTrx());
			stmt.setInt(2, rLine.getUNS_POSRecap().getStore_ID());
			stmt.setInt(3, rLine.getM_Product_ID());
			rs = stmt.executeQuery();
			
			while(rs.next())
			{
				MUNSPOSTrxLineMA[] mas = MUNSPOSTrxLineMA.gets(rs.getInt(1), get_TrxName());
				BigDecimal qty = MUNSPOSRecapLineMA.getManualQty(rLine.get_ID(), get_TrxName());
				qty = rLine.getQty().subtract(qty);
				for(int i=0;i<mas.length;i++)
				{
					MUNSPOSRecapLineMA ma = MUNSPOSRecapLineMA.getCreate(getCtx(), rLine,
							mas[i].getM_AttributeSetInstance_ID(), mas[i].getDateMaterialPolicy(),
								mas[i].getM_Locator_ID(), get_TrxName());
					if(qty.compareTo(mas[i].getMovementQty()) > 0)
					{
						ma.setQty(ma.getQty().add(mas[i].getMovementQty()));
						qty = qty.subtract(mas[i].getMovementQty());
						ma.saveEx();
					}
					else
					{
						ma.setQty(ma.getQty().add(qty));
						ma.saveEx();
						break;
					}
				}
			}
		} catch (SQLException e) {
			throw new AdempiereException("Failed when trying generate MA Lines.");
		}
	}
	
	@SuppressWarnings("unused")
	private MUNSPOSRecapLine[] getRecapLines()
	{
		String whereClause = "EXISTS (SELECT 1 FROM UNS_POSRecap r WHERE"
				+ " r.UNS_POSRecap_ID = UNS_POSRecapLine.UNS_POSRecapLine_ID"
				+ " AND r.UNS_SalesReconciliation_ID = ?)";
		
		List<MUNSPOSRecapLine> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSPOSRecapLine.Table_Name, whereClause, get_TrxName()).setParameters(get_ID())
					.list();
		
		return list.toArray(new MUNSPOSRecapLine[list.size()]);
	}
	
	public static void reCalcGrandTotal(int recordID, String trxName)
	{
		String sql = "UPDATE UNS_SalesReconciliation SET GrandTotal = TotalEDCAmt + TotalSalesAmt"
				+ " WHERE UNS_SalesReconciliation_ID = ?";
		DB.executeUpdate(sql, recordID, trxName);
	}
	
	public MUNSAttributeNoCost[] getNoCost(String whereClause)
	{
		if(whereClause == null)
			whereClause = Table_Name + "_ID= ?";
		else
			whereClause += " AND " + Table_Name + "_ID= ?";
		List<MUNSAttributeNoCost> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSAttributeNoCost.Table_Name,  whereClause,
					get_TrxName()).setParameters(get_ID()).list();
		
		return list.toArray(new MUNSAttributeNoCost[list.size()]);
	} 
}