/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.DB;
import org.compiere.util.Env;

import com.uns.model.MProduct;

/**
 * @author menjangan
 *
 */
public class MUNSIncentive extends X_UNS_Incentive {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MUNSIncentiveSchema m_parent = null;

	/**
	 * @param ctx
	 * @param UNS_Incentive_ID
	 * @param trxName
	 */
	public MUNSIncentive(Properties ctx, int UNS_Incentive_ID, String trxName) {
		super(ctx, UNS_Incentive_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSIncentive(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MUNSIncentiveSchema getParent()
	{
		if(null != m_parent)
			return m_parent;
		
		m_parent = (MUNSIncentiveSchema) getUNS_IncentiveSchema();
		return m_parent;
	}
	
	public boolean isSalesIncentive()
	{
		return getIncentiveType().equals(INCENTIVETYPE_SalesIncentive);
	}
	
	public boolean isBillingIncentive()
	{
		return getIncentiveType().equals(INCENTIVETYPE_BillingIncentive);
	}
	
	/**
	 * 
	 * @param M_Product_ID
	 * @return
	 */
	public boolean IsInMyProductCategory(int M_Product_ID)
	{
		if(getM_Product_Category_ID() <= 0)
			return true;
		
		MProduct product = new MProduct(getCtx(), M_Product_ID, get_TrxName());
		boolean isIn = product.getM_Product_Category_ID() == getM_Product_Category_ID();
		
		return isIn;
	}
	
	/**
	 * 
	 * @param M_Product_ID
	 * @return
	 */
	public boolean isValidProduct(int M_Product_ID)
	{
		boolean validProduct = getM_Product_ID() == 0;
		if(!validProduct)
			validProduct = getM_Product_ID() == M_Product_ID;
		if(!validProduct)
			validProduct = IsInMyProductCategory(M_Product_ID);
		return validProduct;
	}
	
	/**
	 * 
	 * @param Outlet_Grade_ID
	 * @param Outlet_Type_ID
	 * @param cost
	 * @param top
	 * @param isNewOutlet
	 * @return
	 */
	public BigDecimal calculateBillingincentive(int Outlet_Grade_ID, int Outlet_Type_ID, BigDecimal cost, int top, boolean isNewOutlet)
	{
		if(!INCENTIVETYPE_BillingIncentive.equals(getIncentiveType()))
			return Env.ZERO;
		else if(!IsValidOutletGrade(Outlet_Grade_ID))
			return Env.ZERO;
		else if(!isValidOutletType(Outlet_Type_ID))
			return Env.ZERO;
		else if(top > getPaymentTerm())
			return Env.ZERO;
		
		BigDecimal incentive = cost.multiply(getIncentive()).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_DOWN);
		
		if(isNewOutlet)
			incentive = incentive.add(cost.multiply(getIncentiveNewOutlet()).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_DOWN));
		
		return incentive;
	}
	
	/**
	 * 
	 * @param M_Product_ID
	 * @param isNewOutlet
	 * @param cost
	 * @return
	 */
	public BigDecimal calculateSalesIncentive(int M_Product_ID, boolean isNewOutlet, BigDecimal cost)
	{
		if(!INCENTIVETYPE_SalesIncentive.equals(getIncentiveType()))
			return Env.ZERO;
		else if(!isValidProduct(M_Product_ID))
			return Env.ZERO;
		
		BigDecimal retVal = cost.multiply(getIncentive()).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_DOWN);
		
		if(isNewOutlet)
			retVal = retVal.add(cost.multiply(getIncentiveNewOutlet()).divide(Env.ONEHUNDRED, 0, RoundingMode.HALF_DOWN));
		
		return retVal;
	}
	
	/**
	 * 
	 * @param outletGrade
	 * @return
	 */
	public boolean IsValidOutletGrade(int outletGrade)
	{
		boolean retval = getUNS_Outlet_Grade_ID() <= 0 || getUNS_Outlet_Grade_ID() == outletGrade;
		return retval;
	}
	
	/**
	 * 
	 * @param outletType
	 * @return
	 */
	public boolean isValidOutletType(int outletType)
	{
		boolean retval = getUNS_Outlet_Type_ID() <= 0 || getUNS_Outlet_Type_ID() == outletType;
		return retval;
	}
	
	public int getPaymentTerm()
	{
		int paymentTerm = 0;
		if(getC_PaymentTerm_ID() <= 0)
			return paymentTerm;
		
		String sql = "SELECT COALESCE(NetDays, 0) FROM C_PaymentTerm WHERE C_PaymentTerm_ID = ?";
		paymentTerm = DB.getSQLValue(get_TrxName(), sql, getC_PaymentTerm_ID());
		return paymentTerm;
	}
}
