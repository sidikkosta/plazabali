/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for UNS_StoreConfiguration
 *  @author iDempiere (generated) 
 *  @version Release 2.1
 */
@SuppressWarnings("all")
public interface I_UNS_StoreConfiguration 
{

    /** TableName=UNS_StoreConfiguration */
    public static final String Table_Name = "UNS_StoreConfiguration";

    /** AD_Table_ID=1000429 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Company.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Tax_ID */
    public static final String COLUMNNAME_C_Tax_ID = "C_Tax_ID";

	/** Set Tax.
	  * Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID);

	/** Get Tax.
	  * Tax identifier
	  */
	public int getC_Tax_ID();

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DefaultPassengerType */
    public static final String COLUMNNAME_DefaultPassengerType = "DefaultPassengerType";

	/** Set Default Passenger Type	  */
	public void setDefaultPassengerType (String DefaultPassengerType);

	/** Get Default Passenger Type	  */
	public String getDefaultPassengerType();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsPB1Printed */
    public static final String COLUMNNAME_IsPB1Printed = "IsPB1Printed";

	/** Set PB1 Tax Printed ?	  */
	public void setIsPB1Printed (boolean IsPB1Printed);

	/** Get PB1 Tax Printed ?	  */
	public boolean isPB1Printed();

    /** Column name IsPB1Tax */
    public static final String COLUMNNAME_IsPB1Tax = "IsPB1Tax";

	/** Set PB 1 Tax ?	  */
	public void setIsPB1Tax (boolean IsPB1Tax);

	/** Get PB 1 Tax ?	  */
	public boolean isPB1Tax();

    /** Column name IsServChgPrinted */
    public static final String COLUMNNAME_IsServChgPrinted = "IsServChgPrinted";

	/** Set Service Charge Printed ?	  */
	public void setIsServChgPrinted (boolean IsServChgPrinted);

	/** Get Service Charge Printed ?	  */
	public boolean isServChgPrinted();

    /** Column name IsServiceCharge */
    public static final String COLUMNNAME_IsServiceCharge = "IsServiceCharge";

	/** Set Service Charge ?	  */
	public void setIsServiceCharge (boolean IsServiceCharge);

	/** Get Service Charge ?	  */
	public boolean isServiceCharge();

    /** Column name ServiceCharge */
    public static final String COLUMNNAME_ServiceCharge = "ServiceCharge";

	/** Set Service Charge	  */
	public void setServiceCharge (BigDecimal ServiceCharge);

	/** Get Service Charge	  */
	public BigDecimal getServiceCharge();

    /** Column name Store_ID */
    public static final String COLUMNNAME_Store_ID = "Store_ID";

	/** Set Store	  */
	public void setStore_ID (int Store_ID);

	/** Get Store	  */
	public int getStore_ID();

	public org.compiere.model.I_C_BPartner getStore() throws RuntimeException;

    /** Column name UNS_StoreConfiguration_ID */
    public static final String COLUMNNAME_UNS_StoreConfiguration_ID = "UNS_StoreConfiguration_ID";

	/** Set Store Configuration	  */
	public void setUNS_StoreConfiguration_ID (int UNS_StoreConfiguration_ID);

	/** Get Store Configuration	  */
	public int getUNS_StoreConfiguration_ID();

    /** Column name UNS_StoreConfiguration_UU */
    public static final String COLUMNNAME_UNS_StoreConfiguration_UU = "UNS_StoreConfiguration_UU";

	/** Set UNS_StoreConfiguration_UU	  */
	public void setUNS_StoreConfiguration_UU (String UNS_StoreConfiguration_UU);

	/** Get UNS_StoreConfiguration_UU	  */
	public String getUNS_StoreConfiguration_UU();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
