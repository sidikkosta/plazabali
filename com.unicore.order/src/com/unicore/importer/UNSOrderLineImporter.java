/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Properties;
import jxl.Sheet;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPrice;
import org.compiere.model.MProductPricing;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Trx;
import com.unicore.base.model.MOrder;
import com.uns.importer.ImporterValidation;

/**
 * @author Burhani Adam
 *
 */
public class UNSOrderLineImporter implements ImporterValidation {

	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, PO> m_PORefMap = null;
	
	/**
	 * 
	 */
	public UNSOrderLineImporter(Properties ctx, Sheet sheet, String trxName) {
		super();
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}
	
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		MOrder order = new MOrder(m_ctx, po.get_ValueAsInt("C_Order_ID"), m_trxName);
		MOrderLine line = (MOrderLine) po;
		line.setAD_Org_ID(order.getAD_Org_ID());
		BigDecimal price = (BigDecimal) po.get_Value("PriceEntered");
		Integer productID = (Integer) po.get_Value("M_Product_ID");
		if (productID == null)
			return "Product not found";
		String sql = "SELECT C_UOM_ID FROM M_Product WHERE M_Product_ID = ?";
		int uomID = DB.getSQLValue(m_trxName, sql, productID);
		line.set_ValueNoCheck("C_UOM_ID", uomID);
		
		sql = "SELECT plv.M_PriceList_Version_ID FROM M_ProductPrice pp "
				+ "	INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
				+ " WHERE pp.M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
				+ " ORDER BY plv.ValidFrom DESC";
		int result = DB.getSQLValue(m_trxName, sql, productID, order.getM_PriceList_ID()
									, order.getDateOrdered());
		MProductPrice pp = null;
		MPriceListVersion plv = null;
		if(result <= 0)
		{
			plv = MPriceListVersion.get(m_ctx, order.getM_PriceList_ID(), 
					order.getDateOrdered(), m_trxName);
			if(plv == null)
			{
				plv = new MPriceListVersion((MPriceList) order.getM_PriceList());
				plv.setValidFrom(order.getDateOrdered());
				plv.setName();
				plv.saveEx();
			}
			
			pp = new MProductPrice(m_ctx, plv.get_ID(), productID, m_trxName);
			pp.setPrices(price, price, price);
			pp.saveEx();
		}
		else
		{
			plv = new MPriceListVersion(m_ctx, result, m_trxName);
			pp = MProductPrice.get(m_ctx, plv.get_ID(), productID, m_trxName);
			if(pp == null)
			{
				pp = new MProductPrice(m_ctx, plv.get_ID(), productID, m_trxName);
				pp.setPrices(price, price, price);
				pp.saveEx();
			}
		}
		
		MProductPricing pPrice = 
				new MProductPricing (productID, order.getC_BPartner_ID(), line.getQtyEntered(), 
						true, order.getC_BPartner_Location_ID());
		pPrice.setM_PriceList_ID(order.getM_PriceList_ID());
		pPrice.setM_PriceList_Version_ID(plv.get_ID());
		pPrice.setPriceDate(order.getDateOrdered());
		
//		line.set_ValueNoCheck("PriceEntered", pPrice.getPriceStd());
		line.set_ValueNoCheck("PriceActual", line.getPriceEntered());
		line.set_ValueNoCheck("PriceLimit", line.getPriceEntered());
		line.set_ValueNoCheck("PriceList", line.getPriceEntered());
//		line.set_ValueNoCheck("DiscountAmt", pPrice.getDiscountAmt(line.getQtyEntered()));
		
		Trx trx = Trx.get(m_trxName, false);
		try {
			trx.commit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
		
	}
}