/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.WeakHashMap;
import java.util.logging.Level;
import jxl.Sheet;
import org.compiere.model.MBPartner;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.uns.base.model.Query;
import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Menjangan
 *
 */
public class UNSCreditPOSPaymentImportner implements ImporterValidation 
{
	private Properties m_ctx = null;
	private Sheet m_sheet = null;
	private String m_trxName = null;
	private final String COLUMN_NIP = "NIP";
	private final String COLUMN_DATE_TRX = "Date Trx";
	private final String COLUMN_STATUS = "Status";
	private final String COLUMN_BANKACCOUNT = "Bank Account";
	private WeakHashMap<String, Integer> m_mapBankAccount = null;
	private final String COLUMN_TENDERTYPE = "Tender Type";
	
	public Properties getCtx ()
	{
		return this.m_ctx;
	}
	
	public void set_TrxName (String trxName)
	{
		this.m_trxName = trxName;
	}
	
	public String get_TrxName ()
	{
		return this.m_trxName;
	}
	
	public Sheet getSheet ()
	{
		return this.m_sheet;
	}
	
	public void setSheet (Sheet sheet)
	{
		this.m_sheet = sheet;
	}
	
	public void setCtx (Properties ctx)
	{
		this.m_ctx = ctx;
	}
	
	public UNSCreditPOSPaymentImportner (Properties ctx, Sheet sheet, String trxName)
	{
		setCtx(ctx);
		setSheet(sheet);
		setTrxName(trxName);
		this.m_mapBankAccount = new WeakHashMap<>();
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, 
	 * org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		if (freeColVals.get(COLUMN_NIP) == null)
			return SimpleImportXLS.DONT_SAVE;
		
		String status = (String) freeColVals.get(COLUMN_STATUS);
		if ("T".equals(status))
			return SimpleImportXLS.DONT_SAVE;
		
		Object bankAcc = freeColVals.get(COLUMN_BANKACCOUNT);
		if (bankAcc == null)
			return "Mandatory column Bank Account";
		
		Integer bankAccountID = m_mapBankAccount.get((String) bankAcc);
		if (null == bankAccountID)
		{
			String sql = "SELECT C_BankAccount_ID FROM C_BankAccount WHERE Value = ?";
			bankAccountID = DB.getSQLValue(get_TrxName(), sql, bankAcc);
		}
		if (bankAccountID == 0)
			return "Could not find bank account " + bankAcc.toString();
		
		MPaymentAllocate alloc = (MPaymentAllocate) po;
		Integer partner_ID = (Integer) freeColVals.get(COLUMN_NIP);
		
		if (null == partner_ID)
			return "Can't find Member";
		
		Timestamp dateTrx = (Timestamp) freeColVals.get(COLUMN_DATE_TRX);
		
		if ("S".equals(status) && alloc.getPayToOverUnderAmount().signum() == 0)
			return SimpleImportXLS.DONT_SAVE;
		
		alloc.setWriteOffAmt(Env.ZERO);
		alloc.setOverUnderAmt(Env.ZERO);
		alloc.setDiscountAmt(Env.ZERO);
		
		BigDecimal paidAmt = alloc.getPayToOverUnderAmount();
		String sql = "SELECT InvoiceOpen (?,?)";
		BigDecimal invOpen = DB.getSQLValueBD(get_TrxName(), sql, alloc.getC_Invoice_ID(), 0);
		alloc.setInvoiceAmt(invOpen);
		if (invOpen.signum() == 0)
		{
			return SimpleImportXLS.DONT_SAVE;
		}
		
		alloc.setAmount(paidAmt);
		BigDecimal overUnder = invOpen.subtract(paidAmt);
		//TODO witholding amount is not calculated
		overUnder = overUnder.subtract(alloc.getDiscountAmt());
		alloc.setWriteOffAmt(Env.ZERO);
		alloc.setOverUnderAmt(overUnder);
		if ("L".equals(status) && alloc.getPayToOverUnderAmount().signum() == 0)
		{
			alloc.setPayToOverUnderAmount(invOpen);
			alloc.setAmount(invOpen);
			alloc.setOverUnderAmt(Env.ZERO);
		}
		
		MPayment payment = null;
		if (alloc.getC_Payment_ID() == 0 )
		{	
			MBPartner partner = MBPartner.get(getCtx(), partner_ID);
			String tenderType = (String) freeColVals.get(COLUMN_TENDERTYPE);
			int org_ID = Env.getAD_Org_ID(Env.getCtx());
			if (org_ID == 0) {
				sql = "SELECT AD_Org_ID FROM C_BankAccount WHERE C_BankAccount_ID = ?";			
				org_ID = DB.getSQLValue(get_TrxName(), sql, bankAccountID);
			}
			String wc = "C_BPartner_ID = ? AND TRUNC (CAST (DateTrx AS DATE)) = "
					+ " TRUNC (CAST (? AS DATE)) AND TenderType = ? AND C_BankAccount_ID = ? "
					+ " AND DocStatus IN ('DR','IP','IN')";
			payment = new Query(getCtx(), MPayment.Table_Name, wc, get_TrxName()).
					setParameters(partner_ID, dateTrx, tenderType, bankAccountID).first();
			if (null == payment)
			{
				payment = new MPayment(getCtx(), 0, get_TrxName());
				payment.setAD_Org_ID(org_ID);
				payment.setBankAccountDetails(bankAccountID);
				payment.setBankCash(bankAccountID, true, tenderType);
				payment.setDateTrx(dateTrx);
				payment.setDateAcct(dateTrx);
				payment.setC_BPartner_ID(partner.get_ID());
				payment.setIsReceipt(true);
				payment.setC_DocType_ID(payment.isReceipt());
				if (!payment.save())
					return CLogger.retrieveErrorString("Failed when try to save Payment.");
			}
			
			alloc.setC_Payment_ID(payment.get_ID());
			poRefMap.put(Integer.toString(payment.getC_Payment_ID()), payment);
		}
		else
		{
			payment = (MPayment) poRefMap.get(Integer.toString(alloc.getC_Payment_ID()));
			if (null == payment)
			{
				payment = new MPayment(getCtx(), alloc.getC_Payment_ID(), get_TrxName());
				poRefMap.put(Integer.toString(payment.get_ID()), payment);
			}
		}
		
		String docstatus = payment.getDocStatus();
		
		if (!"DR".equals(docstatus) && !"IP".equals(docstatus) && !"IN".equals(docstatus))
		{
			CLogger.get().log(Level.INFO, "Could not process processed document!!!");
			poRefMap.remove(Integer.toString(payment.getC_Payment_ID()));
			return SimpleImportXLS.DONT_SAVE;
		}
		
		if ("L".equals(status) && alloc.getOverUnderAmt().signum() != 0)
		{
			alloc.setWriteOffAmt(alloc.getOverUnderAmt());
			alloc.setOverUnderAmt(Env.ZERO);
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, 
	 * org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, 
	 * org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) 
	{
		Enumeration<PO> en = poRefMap.elements();
		while (en.hasMoreElements())
		{
			MPayment payment = (MPayment) en.nextElement();
			payment.load(get_TrxName());
			boolean ok = payment.processIt(DocAction.ACTION_Complete);
			if (!ok)
				return payment.getProcessMsg();
			if (!payment.save())
				return CLogger.retrieveErrorString("After complete all row - could not save payment");
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) 
	{
		set_TrxName(trxName);
	}

}
