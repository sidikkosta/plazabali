package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.PO;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.base.model.MInvoice;
import com.uns.base.model.Query;
import com.uns.importer.ImporterValidation;
import com.uns.model.MUNSChequeReconciliation;
import com.uns.model.process.SimpleImportXLS;
import com.uns.model.process.UNSGiroAction;

public class UNSPaymentImporter implements ImporterValidation {

	private Properties m_ctx = null;
	private Sheet m_sheet = null;
	private String m_trxName = null;
	private final String COLUMN_ISSOTRX = "IsSOTrx";
	private final String COLUMN_DATETRX = "Transaction Date";
	private final String COLUMN_DESCPAYMENT = "Description Payment";
	private final String COLUMN_BANKACCOOUNT = "Cash/Bank Account";
	private final String COLUMN_TENDERTYPE = "Tender Type";
	private final String COLUMN_GIRONO = "No. Giro";
	private final String COLUMN_DATEDISBURST = "Disburstment Date"; 
	private MPayment m_payment = null;
	private Hashtable<String, Timestamp> m_giroMap = new Hashtable<>();  
	private final String m_descPay =  " **_Import by System_** ";
	
	public Properties getCtx() {
		return this.m_ctx;
	}
	
	public void setCtx(Properties ctx) {
		this.m_ctx = ctx;
	}
	
	public String get_TrxName() {
		return this.m_trxName;
	}
	
	public void set_TrxName(String trxName) {
		this.m_trxName = trxName;
	}
	
	public Sheet getSheet() {
		return this.m_sheet;
	}
	
	public void setSheet(Sheet sheet) {
		this.m_sheet = sheet;
	}
	
	
	public UNSPaymentImporter(Properties ctx, Sheet sheet , String trxName) {
		setCtx(ctx);
		setTrxName(trxName);
		setSheet(sheet);
	}

	@SuppressWarnings("unused")
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		
		String tenderType = freeColVals.get(COLUMN_TENDERTYPE).toString();
		String giroNo = "";
		if(tenderType.equals("K"))
		{
			giroNo = freeColVals.get(COLUMN_GIRONO).toString().replaceAll(" ", "");
			if(giroNo == null)
				return "Please define Cheque No";
		}
		
		MPaymentAllocate allocate = (MPaymentAllocate) po;
//		BigDecimal paid = (BigDecimal) freeColVals.get(COLUMN_PaidAmt);
//		BigDecimal wo = (BigDecimal) freeColVals.get(COLUMN_WriteOffAmt);
//		allocate.setOverUnderAmt(allocate.getInvoiceAmt().subtract(paid).subtract(wo));
		
		boolean isSOTrx = (boolean) freeColVals.get(COLUMN_ISSOTRX);
		Timestamp dateTrx = (Timestamp) freeColVals.get(COLUMN_DATETRX);
		String bankaccount = freeColVals.get(COLUMN_BANKACCOOUNT).toString();
		if(bankaccount == null)
			return "Mandatory Column Bank Account";
		
		String sql = "SELECT C_BankAccount_ID FROM C_BankAccount WHERE TRIM(Value) = TRIM(?)";
		int bankAccount_ID = DB.getSQLValue(get_TrxName(), sql, bankaccount);
		if(bankAccount_ID <= 0)
			return "Could not found Bank Account "+bankaccount;
		
		MInvoice invoice = new MInvoice(getCtx(), allocate.getC_Invoice_ID(), get_TrxName());
		if(invoice == null)
			return "Could not found Invoice";
		
		if(!invoice.getDocStatus().equals(MInvoice.STATUS_Completed)
				&& !invoice.getDocStatus().equals(MInvoice.STATUS_Closed))
		{
			return SimpleImportXLS.DONT_SAVE;
		}
		if(invoice.isPaid())
			return SimpleImportXLS.DONT_SAVE;
			
//		String check = "SELECT COUNT(*) FROM C_PaymentAllocate WHERE"
//				+ " C_Invoice_ID = ? AND EXISTS "
//				+ " (SELECT 1 FROM C_Payment p WHERE p.C_Payment_ID = C_PaymentAllocate.C_Payment_ID"
//				+ " AND p.DateTrx = TRUNC(CAST(? AS DATE)) AND p.C_BankAccount_ID = ?"
//				+ " AND p.TenderType = ? AND (CASE WHEN p.TenderType = 'K' THEN"
//				+ " p.CheckNo = REPLACE(COALESCE(?, ''), ' ', '') ELSE 1=1 END))";
//		boolean exists = DB.getSQLValue(m_trxName, check,
//				new Object[]{invoice.get_ID(), dateTrx}
//					, bankAccount_ID, tenderType,
//						giroNo == null ? "" : giroNo) > 0 ? true : false;
		String check = "SELECT C_PaymentAllocate.C_Payment_ID FROM C_PaymentAllocate WHERE"
				+ " C_Invoice_ID = ? AND EXISTS "
				+ " (SELECT 1 FROM C_Payment p WHERE p.C_Payment_ID = C_PaymentAllocate.C_Payment_ID"
				+ " AND p.Description = ?"
				+ " AND p.DocStatus NOT IN ('VO', 'RE'))";
		int exists = DB.getSQLValue(m_trxName, check, invoice.get_ID(),
				freeColVals.get(COLUMN_DESCPAYMENT).toString().concat(m_descPay));
		if(exists > 0)
		{
			m_payment = new MPayment(getCtx(), exists, get_TrxName());
			poRefMap.put(Integer.toString(m_payment.getC_Payment_ID()), m_payment);
			if(m_payment.getTenderType().equals("K")
					&& freeColVals.get(COLUMN_DATEDISBURST) != null)
			{
				Timestamp dateDisburst = (Timestamp) freeColVals.get(COLUMN_DATEDISBURST);
				m_giroMap.put(m_payment.getCheckNo(), dateDisburst); 
			}
			return SimpleImportXLS.DONT_SAVE;
		}
		
		if(allocate.getC_Payment_ID() <= 0)
		{
			String sqql = "SELECT AD_Org_ID FROM C_BankAccount WHERE C_BankAccount_ID = ?";
			int org_id = DB.getSQLValue(get_TrxName(), sqql, bankAccount_ID);
			
			StringBuilder wc = new StringBuilder("C_BPartner_ID = ? AND TRUNC (CAST (DateTrx AS DATE)) = "
					+ " TRUNC (CAST (? AS DATE)) AND TenderType = ? AND C_BankAccount_ID = ? "
					+ " AND DocStatus IN ('DR','IP','IN') AND AD_Org_ID = ?");
			if(tenderType.equals("K"))
			{
				wc.append(" AND CheckNo = REPLACE('"+freeColVals.get(COLUMN_GIRONO).toString()+"', ' ', '')");
			}
			
			m_payment = new Query(getCtx(), MPayment.Table_Name, wc.toString(), get_TrxName())
					.setParameters(invoice.getC_BPartner_ID(), dateTrx, tenderType, bankAccount_ID
						, org_id).first();
			
			if(m_payment == null)
			{
				m_payment = new MPayment(getCtx(), 0, get_TrxName());
				m_payment.setAD_Org_ID(org_id);
				m_payment.setBankAccountDetails(bankAccount_ID);
				m_payment.setBankCash(bankAccount_ID, isSOTrx, tenderType);
				if(tenderType.equals("K"))
					m_payment.set_ValueOfColumn("CheckNo", freeColVals.get(COLUMN_GIRONO).toString().replaceAll(" ", ""));
				m_payment.setDateTrx(dateTrx);
				m_payment.setDateAcct(dateTrx);
				m_payment.setC_BPartner_ID(invoice.getC_BPartner_ID());
				m_payment.setC_DocType_ID(isSOTrx);
				//currency static IDR
				m_payment.setC_Currency_ID(303);
				m_payment.setDescription(freeColVals.get(COLUMN_DESCPAYMENT).toString().concat(m_descPay));
				if(!m_payment.save())
					return CLogger.retrieveErrorString("Failed when try to save Payment.");
			}
			
			allocate.setAD_Org_ID(org_id);
			
			allocate.setC_Payment_ID(m_payment.getC_Payment_ID());
			
			poRefMap.put(Integer.toString(m_payment.getC_Payment_ID()), m_payment);
			if(m_payment.getTenderType().equals("K")
					&& freeColVals.get(COLUMN_DATEDISBURST) != null)
			{
				Timestamp dateDisburst = (Timestamp) freeColVals.get(COLUMN_DATEDISBURST);
				m_giroMap.put(m_payment.getCheckNo(), dateDisburst); 
			}
		}
		
		//check duplicate payment allocate
		String sqql = "SELECT C_PaymentAllocate_ID FROM C_PaymentAllocate WHERE C_Payment_ID = ?"
				+ " AND IsActive = 'Y' AND C_PaymentAllocate_ID <> ? AND C_Invoice_ID = ?";
		int allocate_ID = DB.getSQLValue(get_TrxName(), sqql, allocate.getC_Payment_ID(),
				allocate.getC_PaymentAllocate_ID(), allocate.getC_Invoice_ID());
		if(allocate_ID > 0)
			allocate.setC_PaymentAllocate_ID(allocate_ID);
			
		
		allocate.setAD_Org_ID(invoice.getAD_Org_ID());
		
//		allocate.setWriteOffAmt(Env.ZERO);
//		allocate.setOverUnderAmt(Env.ZERO);
//		allocate.setDiscountAmt(Env.ZERO);
		
		BigDecimal paidAmt = allocate.getPayToOverUnderAmount();
		String sqll = "SELECT InvoiceOpen (?,?)";
		BigDecimal invOpen = DB.getSQLValueBD(get_TrxName(), sqll, allocate.getC_Invoice_ID(), 0);
		allocate.setInvoiceAmt(invOpen);
		if (invOpen.signum() == 0)
		{
			return SimpleImportXLS.DONT_SAVE;
		}
		
		allocate.setAmount(paidAmt);
		BigDecimal overUnder = invOpen.subtract(paidAmt);
		
		overUnder = overUnder.subtract(allocate.getWriteOffAmt());
		allocate.setDiscountAmt(Env.ZERO);
		allocate.setOverUnderAmt(overUnder);
		
		String docstatus = m_payment.getDocStatus();
		
		if (!"DR".equals(docstatus) && !"IP".equals(docstatus) && !"IN".equals(docstatus))
		{
			CLogger.get().log(Level.INFO, "Could not process processed document!!!");
			poRefMap.remove(Integer.toString(m_payment.getC_Payment_ID()));
			return SimpleImportXLS.DONT_SAVE;
		}
		
		String msg = unLinkedInvoice(invoice, allocate.getC_PaymentAllocate_ID());
		if(!Util.isEmpty(msg, true))
			return msg;
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {

		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		
		Enumeration<PO> en = poRefMap.elements();
		while (en.hasMoreElements()) 
		{
			MPayment payment = (MPayment) en.nextElement();
			payment.load(get_TrxName());
			try {
				if(!payment.isComplete())
				{
					boolean ok = payment.processIt(DocAction.ACTION_Complete) && payment.save();
					if(!ok)
						throw new AdempiereException(payment.getProcessMsg());	
				}
			} catch (Exception e) {
				throw new AdempiereException(payment.getDocumentNo() + " - " + CLogger.retrieveErrorString("Error"));
			}
//			if(!payment.save())
//				return CLogger.retrieveErrorString("After Complete All Row - could not save payment");
		}
		
		Set<String> giros = m_giroMap.keySet();
		for(String giro : giros)
		{
			MUNSChequeReconciliation cheque = MUNSChequeReconciliation.getInstance(giro, getCtx(), get_TrxName());
			if(cheque.getStatus().equals(MUNSChequeReconciliation.STATUS_HandoverVendor))
			{
				UNSGiroAction action = new UNSGiroAction(getCtx(), cheque,
						MUNSChequeReconciliation.STATUS_Disbursement, m_giroMap.get(giro), "-", get_TrxName());
				try{
					action.doIt();
				}
				catch (Exception ex){
					return ex.getMessage(); 
				}
			}			
		}
		
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {

		return null;
	}
	
	@Override
	public void setTrxName(String trxName) {
		set_TrxName(trxName);
	}
	
	public String unLinkedInvoice(MInvoice invoice, int allocate_ID) {
		
		String sql = "SELECT pal.C_PaymentAllocate_ID AS Allocate FROM C_Payment pay"
				+ " INNER JOIN C_PaymentAllocate pal ON pal.C_Payment_ID = pay.C_Payment_ID"
				+ " WHERE pal.C_Invoice_ID = ? AND pay.DocStatus IN ('DR')"
				+ " AND pal.C_PaymentAllocate_ID <> ? AND"
				+ " pay.Description NOT LIKE ?";
		
		ResultSet rs = null;
		PreparedStatement sta = null;
		
		try {
			sta = DB.prepareStatement(sql, get_TrxName());
			sta.setInt(1, invoice.getC_Invoice_ID());
			sta.setInt(2, allocate_ID);
			sta.setString(3, "%"+m_descPay+"%");
			rs = sta.executeQuery();
			
			while(rs.next()) {
				
				MPaymentAllocate allocate = new MPaymentAllocate(getCtx(), rs.getInt("Allocate"), get_TrxName());
				allocate.deleteEx(true);
			}
			
		} catch (Exception e) {
			return e.getMessage();
		}
		finally {
			DB.close(rs, sta);
		}
	
		return null;
	}

}
