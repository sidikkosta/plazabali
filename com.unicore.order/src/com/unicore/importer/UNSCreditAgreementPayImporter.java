/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import jxl.Sheet;
import org.compiere.model.MInvoice;
import org.compiere.model.MPayment;
import org.compiere.model.MPaymentAllocate;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import com.unicore.model.MUNSCreditAgreement;
import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Menjangan
 *
 */
public class UNSCreditAgreementPayImporter implements ImporterValidation {

	private Properties m_ctx;
	private Sheet m_sheet;
	private String m_trxName;
	private final String COLUMNNAME_CREDIT_AGREEMENT = "UNS_CreditAgreement_ID";
	private final String COLUMNNAME_CHARGE = "C_Charge_ID";
	private final String COLUMNNAME_BANK_ACCOUNT = "C_BankAccount_ID";
	private final String COLUMNNAME_OPEN_AMOUNT = "Open Amount";
	private Hashtable<Integer, List<MPaymentAllocate>> m_mapAllocate;
	private List<MPayment> m_payments;
	private final String COLUMNNAME_DATE_TRX = "Date Trx";
	private final String COLUMNNAME_DATE_ACCT = "Date Acct";
	private final String DESCRIPTION = "::AUTO GENERATED::";

	public UNSCreditAgreementPayImporter (Properties ctx, Sheet sheet, String trxName)
	{
		this.m_ctx = ctx;
		this.m_sheet = sheet;
		this.m_trxName = trxName;
		this.m_payments = new ArrayList<>();
		this.m_mapAllocate = new Hashtable<>();
	}
	
	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		Integer creditAgreementID = (Integer)freeColVals.get(
				COLUMNNAME_CREDIT_AGREEMENT);
		Integer chargeID = (Integer) freeColVals.get(COLUMNNAME_CHARGE);
		Integer bankAccountID = (Integer) freeColVals.get(COLUMNNAME_BANK_ACCOUNT);
		Timestamp dateTrx = (Timestamp) freeColVals.get(COLUMNNAME_DATE_TRX);
		Timestamp dateAcct = (Timestamp) freeColVals.get(COLUMNNAME_DATE_ACCT);
		BigDecimal realOpen = (BigDecimal) freeColVals.get(COLUMNNAME_OPEN_AMOUNT);
		
		if (creditAgreementID == null || creditAgreementID == 0)
			return "null or 0 credit agreement";
		if (chargeID == null || chargeID == 0)
			return "null or 0 charge!!!";
		if (bankAccountID == null || bankAccountID == 0)
			return "null or 0 bank account";
		if (realOpen == null)
			realOpen = Env.ZERO;
		if (dateTrx == null)
			dateTrx = TimeUtil.trunc(Env.getContextAsDate(getCtx(), "#Date"), TimeUtil.TRUNC_DAY);
		else
			dateTrx = TimeUtil.trunc(dateTrx, TimeUtil.TRUNC_DAY);
		if (dateAcct == null)
			dateAcct = TimeUtil.trunc(Env.getContextAsDate(getCtx(), "#Date"), TimeUtil.TRUNC_DAY);
		else
			dateAcct = TimeUtil.trunc(dateAcct, TimeUtil.TRUNC_DAY);
		
		MUNSCreditAgreement agreement = new MUNSCreditAgreement(
				getCtx(), creditAgreementID, get_TrxName());
		
		
		String sql = "SELECT SUM (InvoiceOpenToDate (C_Invoice_ID, 0, ?)) FROM C_Invoice WHERE C_Invoice_ID IN "
				+ " (SELECT C_Invoice_ID FROM UNS_CreditPaySchedule WHERE UNS_CreditAgreement_ID = ?)";
		BigDecimal balance = DB.getSQLValueBD(get_TrxName(), sql, dateTrx, creditAgreementID);
		BigDecimal remainingAmt = balance.subtract(realOpen);
		
		if (remainingAmt.signum() < 0)
		{
			BigDecimal invalidAmt = remainingAmt.abs();
			String wc = "C_BPartner_ID = ? AND Description = ? AND DocStatus IN (?) AND DateTrx <= ?";
			List<MPayment> pays = new Query(getCtx(), MPayment.Table_Name, wc, get_TrxName()).setParameters(agreement.getC_BPartner_ID(),
					DESCRIPTION,"CO", dateTrx).list();
			for (int i=0; i<pays.size() && invalidAmt.signum() > 0; i++)
			{
				MPayment pay = pays.get(i);
				pay.set_TrxName(get_TrxName());
				if (!pay.processIt(DocAction.ACTION_Void))
					return pay.getProcessMsg();
				if (!pay.save())
					return CLogger.retrieveErrorString("Could not update pyament");
				invalidAmt = invalidAmt.subtract(pay.getTotalAmt());
			}
			
			agreement.load(get_TrxName());
			balance = DB.getSQLValueBD(get_TrxName(), sql, dateTrx, creditAgreementID);
			remainingAmt = balance.subtract(realOpen);
			if (!agreement.doCreateSchedule(false))
				return agreement.getProcessMsg();
		}
		
		if (remainingAmt.signum() <= 0)
			return SimpleImportXLS.DONT_SAVE;
		
		MInvoice[] invoices = agreement.getInvoices();
		MPayment payment = null;
		for (int i=0; i<m_payments.size(); i++)
		{
			if (m_payments.get(i).getC_BPartner_ID() == agreement.getC_BPartner_ID())
			{
				payment = m_payments.get(i);
				payment.load(get_TrxName());
				break;
			}
		}
		
		if (payment == null)
		{
			//try to load from db
			String wc = "C_BPartner_ID = ? AND Description = ? AND DocStatus NOT IN (?,?,?,?)";
			payment = new Query(getCtx(), "C_Payment", wc, get_TrxName())
			.setParameters(agreement.getC_BPartner_ID(),
					DESCRIPTION,"CO","CL","RE","VO")
			.firstOnly();
			if (payment != null)
				m_payments.add(payment);
		}
		if (payment == null)
		{
			payment = new MPayment(getCtx(), 0, get_TrxName());
			payment.setBankCash(bankAccountID, true, MPayment.TENDERTYPE_Cash);
			sql = "SELECT AD_Org_ID FROM C_BankAccount WHERE "
					+ " C_BankAccount_ID = ?";
			int orgID = DB.getSQLValue(get_TrxName(), sql, bankAccountID);
			payment.setAD_Org_ID(orgID);
			payment.setC_Charge_ID(chargeID);
			payment.setChargeAmt(remainingAmt);
			payment.setC_BPartner_ID(agreement.getC_BPartner_ID());
			payment.setDateAcct(dateAcct);
			payment.setDateTrx(dateTrx);
			payment.setDescription(DESCRIPTION);
			if (!payment.save())
				return CLogger.retrieveErrorString("Could not save payment!!!");
			
			m_payments.add(payment);
		}
		
		List<MPaymentAllocate> allocates = (List<MPaymentAllocate>)
				m_mapAllocate.get(payment.get_ID());
		if (allocates == null)
		{
			allocates = MPaymentAllocate.getAsList(payment);
			m_mapAllocate.put(payment.get_ID(), allocates);
		}
		
		for (int i=0; i<invoices.length; i++)
		{
			if (remainingAmt.signum() == 0)
				break;
			sql = "SELECT InvoiceOpen (?,?)";
			BigDecimal sysOpen = DB.getSQLValueBD(
					get_TrxName(), sql, invoices[i].getC_Invoice_ID(), 0);
			if (sysOpen.signum() == 0)
				continue;
			BigDecimal willBePay = sysOpen;
			
			if (willBePay.compareTo(remainingAmt) == 1)
				willBePay = remainingAmt;
			
			MPaymentAllocate allocate = null;
			
			for (int j=0; j<allocates.size(); j++)
			{
				if (allocates.get(j).getC_Invoice_ID() == invoices[i].getC_Invoice_ID())
				{
					allocate = allocates.get(j);
					break;
				}
			}
			
			if (allocate == null)
			{
				allocate = new MPaymentAllocate(getCtx(), 0, get_TrxName());
				allocate.setAD_Org_ID(invoices[i].getAD_Org_ID());
				allocate.setC_Payment_ID(payment.get_ID());
				allocate.setC_Invoice_ID(invoices[i].getC_Invoice_ID());
				allocates.add(allocate);
			}

			if (invoices[i].getDateInvoiced().after(payment.getDateTrx())
					|| invoices[i].getDateAcct().after(payment.getDateAcct()))
			{
				payment.setDateTrx(TimeUtil.addDays(invoices[i].getDateInvoiced(), 1));
				payment.setDateAcct(TimeUtil.addDays(invoices[i].getDateAcct(), 1));
				if (!payment.save())
					return CLogger.retrieveErrorString("Could not update payment");
			}
			
			allocate.setAmount(willBePay);
			allocate.setPayToOverUnderAmount(willBePay);
			allocate.setOverUnderAmt(sysOpen.subtract(willBePay));
			allocate.setInvoiceAmt(sysOpen); 

			if (!allocate.save())
				return CLogger.retrieveErrorString("Could not save allocate");
			
			remainingAmt= remainingAmt.subtract(willBePay);
		}
		
		return SimpleImportXLS.DONT_SAVE;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) 
	{
		for (int i=0; i<m_payments.size(); i++)
		{
			String sql = "SELECT SUM(PayToOverUnderAmount) FROM C_PaymentAllocate WHERE C_Payment_ID = ?";
			BigDecimal chAmt = DB.getSQLValueBD(get_TrxName(), sql, m_payments.get(i).getC_Payment_ID());
			if (chAmt == null)
				chAmt = Env.ZERO;
			m_payments.get(i).setChargeAmt(chAmt);
			if (!m_payments.get(i).save(get_TrxName()))
				return CLogger.retrieveErrorString("Could not save payment!!!");
			m_payments.get(i).load(get_TrxName());
			if (m_payments.get(i).getPayAmt().signum() != 0)
				return "Pay Amount Not Zero !!!";
			boolean ok = m_payments.get(i).processIt(DocAction.ACTION_Complete);
			if (!ok)
				return m_payments.get(i).getProcessMsg();
			if (!m_payments.get(i).save())
				return CLogger.retrieveErrorString("Could not save payment");
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) 
	{
		this.m_trxName = trxName;
	}

	public Properties getCtx ()
	{
		return this.m_ctx;
	}
	
	public Sheet getSheet ()
	{
		return this.m_sheet;
	}
	
	public String get_TrxName ()
	{
		return this.m_trxName;
	}
}
