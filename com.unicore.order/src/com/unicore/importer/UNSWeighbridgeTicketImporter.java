/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.DB;

import com.unicore.model.MUNSDespatchNotes;
import com.unicore.model.MUNSWBTicketConfirm;
import com.unicore.model.MUNSWeighbridgeTicket;
import com.uns.importer.ImporterValidation;

/**
 * @author ALBURHANY
 *
 */
public class UNSWeighbridgeTicketImporter implements ImporterValidation {

	private Properties m_Ctx = null;
	private String m_trxName = null;
	private String m_processMsg = null;
	
	static final String COL_StartDespacth = "Start Despacth";
	static final String COL_EndDespacth = "End Despacth";
	static final String COL_TotalSeal = "Total Seal";
	static final String COL_SealNo_From = "Seal No. From";
	static final String COL_SealNo_To = "Seal No. To";
	static final String COL_FFA = "FFA";
	static final String COL_MI = "MI";
	static final String COL_Temperature = "Temperature";
	static final String COL_IsSplitedOrder = "IsSplitedOrder";
	static final String COL_OriginOrder = "OriginOrder";
	static final String COL_TimeIn = "Time In";
	static final String COL_TimeOut = "Time Out";
	static final String COL_SealNumeric = "SealNumeric";
	static final String COL_SealNumber = "SealNumber";
	static final String COL_ContractNo = "Contract No";
	static final int M_Warehouse_ID = 1000065;
	static final int CPO_Locator_ID = 1000324;
	static final String COL_NettoCS = "Netto CS";
	
	public UNSWeighbridgeTicketImporter(Properties ctx, Sheet sheet, String trxName)
	{
		m_Ctx = ctx;
		//m_Sheet = sheet;
		m_trxName = trxName;
	}
	
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
//		String sql = "SELECT C_Order_ID FROM C_Order WHERE POReference=? OR ReferenceNo=?";
//		int C_Order_ID = DB.getSQLValue(null, sql, new Object[]{(String) freeColVals.get(COL_ContractNo), (String) freeColVals.get(COL_ContractNo)});
		if(freeColVals.get(COL_ContractNo) == null)
			throw new AdempiereException("Cannot get Order for Number " + freeColVals.get(COL_ContractNo));
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{	
		MUNSWeighbridgeTicket wt = new MUNSWeighbridgeTicket(po.getCtx(), po.get_ID(), po.get_TrxName());
		boolean isSplitedOrder = freeColVals.get(COL_IsSplitedOrder).equals("N") ? false : true;
		if(isSplitedOrder)
		{
			String sql = "SELECT C_Order_ID FROM C_Order WHERE POReference=? OR ReferenceNo=?";
			int C_Order_ID = DB.getSQLValue(m_trxName, sql,
					new Object[]{freeColVals.get(COL_OriginOrder), freeColVals.get(COL_OriginOrder)});
			int SplittedTicket_ID = getSplittedOrder(wt, C_Order_ID, (Integer) freeColVals.get(COL_SealNo_From),
					(Integer) freeColVals.get(COL_SealNo_To));
			wt.setIsSplitOrder(true);
			wt.setIsOriginalTicket(false);
			wt.setSplittedTicket_ID(SplittedTicket_ID);
			wt.setSplittedOrder_ID(C_Order_ID);
			wt.saveEx();
			
			MUNSWeighbridgeTicket wtOld = new MUNSWeighbridgeTicket(m_Ctx, SplittedTicket_ID, m_trxName);
			wtOld.setIsSplitOrder(true);
			wtOld.setSplittedOrder_ID(wt.getC_Order_ID());
			wtOld.setSplittedTicket_ID(wt.get_ID());
			wtOld.saveEx();
		}
		wt.setAD_Org_ID(wt.getC_Order().getAD_Org_ID());
		wt.setIsSOTrx(true);
		wt.setM_Warehouse_ID(M_Warehouse_ID);
		wt.setM_Locator_ID(CPO_Locator_ID);
		wt.saveEx();
		MUNSDespatchNotes dn = getDespacthOfWeighbridgeTicket(po.get_ID());
		if(null == dn)
			throw new AdempiereException("Failed when trying get Weighbridge Ticket");
		
		//dn.setStartDespatch((Timestamp) freeColVals.get(COL_StartDespacth));
		dn.setStartDespatch(wt.getTimeIn());
		//dn.setEndDespatch((Timestamp) freeColVals.get(COL_EndDespacth));
		dn.setEndDespatch(wt.getTimeOut());
		dn.setTotalSeal((Integer) freeColVals.get(COL_TotalSeal));
		boolean isSealNumeric = freeColVals.get(COL_SealNumeric).equals("Y") ? true : false;
		if(isSealNumeric)
		{
			dn.set_ValueOfColumn("SealNumber", (String) freeColVals.get(COL_SealNumber));
			String[] sealNumber = ((String) freeColVals.get(COL_SealNumber)).split(", ");
			int SealNoFrom = 0;
			int SealNoTo = 0;
			for(int i=0; i<sealNumber.length; i++)
			{
				Integer SealNumbers = new Integer(sealNumber[i]);
				if(SealNoFrom < SealNumbers)
					SealNoFrom = SealNumbers;
				if(SealNoTo > SealNumbers)
					SealNoTo = SealNumbers;
			}
			dn.setSealNoFrom(SealNoFrom);
			dn.setSealNoTo(SealNoTo);
			dn.set_ValueOfColumn("SealNumeric", true);
		}
		else
		{
			dn.setSealNoFrom((Integer) freeColVals.get(COL_SealNo_From));
			dn.setSealNoTo((Integer) freeColVals.get(COL_SealNo_To));
		}
		dn.setMI((BigDecimal) freeColVals.get(COL_MI));
		dn.setStartDespatch(wt.getTimeIn());
		dn.setEndDespatch(wt.getTimeOut());
		dn.setTemperature((Integer) freeColVals.get(COL_Temperature));
		dn.setFFA((BigDecimal) freeColVals.get(COL_FFA));
		try
		{
			dn.saveEx();	
			boolean ok = dn.processIt(DocAction.ACTION_Complete);
			if (!ok)
			{
				m_processMsg = dn.getProcessMsg();
				return m_processMsg;
			}
			dn.saveEx();
			
			ok = wt.processIt(DocAction.ACTION_Complete);
			if(!ok)
			{
				m_processMsg = wt.getProcessMsg();
				return m_processMsg;
			}
			wt.saveEx();
		}
		catch (Exception ex)
		{
			return m_processMsg;
		}
		
		MUNSWBTicketConfirm wtc = MUNSWBTicketConfirm.get(m_Ctx, wt.get_ID(), m_trxName);
		wtc.setGrossWeight((BigDecimal) freeColVals.get(COL_NettoCS));
		try
		{
			wtc.saveEx();	
			boolean ok = wtc.processIt(DocAction.ACTION_Complete);
			if (!ok)
			{
				m_processMsg = wtc.getProcessMsg();
				return m_processMsg;
			}
			wtc.saveEx();
		}
		catch (Exception ex)
		{
			return m_processMsg;
		}

		return m_processMsg;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
	}
	
	public MUNSDespatchNotes getDespacthOfWeighbridgeTicket(int UNS_WeighbridgeTicket_ID)
	{
		if(UNS_WeighbridgeTicket_ID <= 0)
			return null;
		
		MUNSDespatchNotes dn = null;
		
		dn = new Query(m_Ctx, MUNSDespatchNotes.Table_Name, " UNS_WeighbridgeTicket_ID=?", m_trxName)
				.setParameters(UNS_WeighbridgeTicket_ID).first();
		
		return dn;
	}
	
	public int getSplittedOrder(MUNSWeighbridgeTicket wt, int C_Order_ID, int SealNoFrom, int SealNoTo)
	{
		int SplittedOrder_ID = 0;
		
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(UNS_WeighbridgeTicket_ID),'; ') FROM UNS_WeighbridgeTicket WHERE DocStatus NOT IN ('VO', 'RE')"
				+ " AND C_Order_ID=? AND VehicleNo=?"
				+ " AND UNS_WeighbridgeTicket_ID IN (SELECT UNS_WeighbridgeTicket_ID FROM UNS_Despatch_Notes"
				+ " WHERE SealNoFrom=? AND SealNoTo=?)";
		SplittedOrder_ID = DB.getSQLValue(wt.get_TrxName(), sql, new Object[]{C_Order_ID, wt.getVehicleNo(), SealNoFrom, SealNoTo});
//		String a = DB.getSQLValueString(m_trxName, sql, new Object[]{C_Order_ID, wt.getVehicleNo(), SealNoFrom, SealNoTo});
		
		return SplittedOrder_ID;
	}
}