/**
 * 
 */
package com.unicore.importer;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.compiere.model.PO;
import org.compiere.util.DB;

import com.unicore.base.model.MRequisitionLine;
import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Burhani Adam
 *
 */
public class UNSRequisitionLineImporter implements ImporterValidation {

	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, PO> m_PORefMap = null;
	
	/**
	 * 
	 */
	public UNSRequisitionLineImporter(Properties ctx, Sheet sheet, String trxName) {
		super();
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		Integer productID = (Integer) po.get_Value("M_Product_ID");
		Integer parentID = (Integer) po.get_Value("M_Requisition_ID");
		
		String sql = "SELECT M_RequisitionLine_ID FROM M_RequisitionLine"
				+ " WHERE M_Product_ID = ? AND M_Requisition_ID = ?";
		int existsID = DB.getSQLValue(m_trxName, sql, productID, parentID);
		if(existsID > 0)
		{
			MRequisitionLine l = new MRequisitionLine(m_ctx, existsID, m_trxName);
			l.setQty(l.getQty().add((BigDecimal) po.get_Value("Qty")));
			l.saveEx();
			return SimpleImportXLS.DONT_SAVE;
		}
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
		
	}
}