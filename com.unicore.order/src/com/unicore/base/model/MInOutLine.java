/**
 * 
 */
package com.unicore.base.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.ui.ISortTabRecord;

/**
 * @author menjangan
 *
 */
public class MInOutLine extends org.compiere.model.MInOutLine implements ISortTabRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param ctx
	 * @param M_InOutLine_ID
	 * @param trxName
	 */
	public MInOutLine(Properties ctx, int M_InOutLine_ID, String trxName) {
		super(ctx, M_InOutLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MInOutLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param inout
	 */
	public MInOutLine(MInOut inout) {
		super(inout);
		// TODO Auto-generated constructor stub
	}
	
	private MOrderLine m_OrderLine = null;
	public BigDecimal getPriceOrder(boolean isDiscountAfterTrx)
	{
		if(null == m_OrderLine)
		{
			m_OrderLine = new MOrderLine(getCtx(), getC_OrderLine_ID(), get_TrxName());
		}
		BigDecimal price = Env.ZERO;
		if(isDiscountAfterTrx)
		{
			price = m_OrderLine.getPriceEntered();
		}
		else
		{
			price = m_OrderLine.getPriceList();
		}
		return price;
	}
	
	public BigDecimal getAmtOrder(boolean isDiscountAfterTrx)
	{
		BigDecimal price = getPriceOrder(isDiscountAfterTrx);
		BigDecimal amt = price.multiply(getConfirmedQty());
		return amt;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{		
		//checkShippmentPL
//		int cShipPL = DB.getSQLValue(get_TrxName(), "SELECT uns_packinglist_id from uns_packinglist_order where m_inout_id = "+getM_InOut_ID());
//		
//		if(cShipPL>0)
//		{
//			int docPL = DB.getSQLValue(get_TrxName(), "SELECT documentno from uns_packinglist where uns_packinglist_id = "+cShipPL);
//			if(is_ValueChanged(COLUMNNAME_QtyEntered)==true||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL1)==true
//					||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL2)==true ||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL3)==true
//					||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL4)==true ||is_ValueChanged(COLUMNNAME_M_Product_ID)==true)
//				
//			{
//				throw new AdempiereException("Could not Update this record because reference PLOrder : "+docPL);
//			}
//			
//		}
		
		if(!isSameProductType())
			throw new AdempiereException("Product type must be the same as product in receipt line.");
		
		if((newRecord || is_ValueChanged(COLUMNNAME_QtyEntered)) && getReversalLine_ID() == 0)
			setMovementQty(getQtyEntered());
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		
		if(getC_OrderLine_ID() > 0 && getParent().isSOTrx() && is_ValueChanged(COLUMNNAME_C_OrderLine_ID))
		{
			setDescription(Util.isEmpty(getDescription()) ? getC_OrderLine().getDescription()
					: getDescription() + " | " + getC_OrderLine().getDescription());
		}
		return super.afterSave(newRecord, success);
	}
	
	protected boolean beforeDelete()
	{
		
		int cShipPL = DB.getSQLValue(get_TrxName(), "SELECT uns_packinglist_order_id from uns_packinglist_order where m_inout_id = "+getM_InOut_ID());
		
		if(cShipPL>0)
		{
			int docPL = DB.getSQLValue(get_TrxName(), "SELECT documentno from uns_packinglist where uns_packinglist_id = "+cShipPL);
			throw new AdempiereException("Could not delete this record because reference PLOrder : "+docPL);
		}
		return true;
		
	}
	
	@Override
	protected boolean afterDelete(boolean success) {
		
		// unlink m_inoutline_id on UnnoticedMReceiptLine
		String sql = "UPDATE UNS_UnnoticedMReceiptLine SET M_InOutLine_ID = null"
				+ " WHERE M_InOutLine_ID = ?";
		
		int ok = DB.executeUpdate(sql, getM_InOutLine_ID(), get_TrxName());
		if(ok < 0)
			throw new AdempiereException("Could not update uns_unnoticedmreceiptline");
		
		return super.afterDelete(success);
	}

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID)
	{
		if(get_ValueAsInt(COLUMNNAME_C_InvoiceLine_ID) > 0)
		{
			 MInvoiceLine line = new MInvoiceLine(getCtx(), get_ValueAsInt(COLUMNNAME_C_InvoiceLine_ID), get_TrxName());
			 MInvoice inv = MInvoice.get(getCtx(), line.getC_Invoice_ID());
			 BigDecimal allocatedAmt = DB.getSQLValueBD(get_TrxName(), "SELECT InvoiceOpen(?,0)", inv.get_ID());
			 allocatedAmt = inv.getGrandTotal().subtract(allocatedAmt);
			 if(line.getQtyEntered().compareTo(getMovementQty()) == 1)
				 line.setQtyEntered(line.getQtyEntered().subtract(getMovementQty()));
			 else
			 {
				 line.setQtyEntered(Env.ZERO);
				 line.setIsActive(false);
			 }
			 line.saveEx();
			 set_ValueNoCheck(COLUMNNAME_C_InvoiceLine_ID, get_ValueAsInt(COLUMNNAME_C_InvoiceLine_ID));
			 if(inv.getGrandTotal().compareTo(allocatedAmt) == -1)
				 return "Open Amount > Grand Total";
		}
		return null;
	}

	@Override
	public String beforeRemoveSelection()
	{
		if(get_ValueAsInt(COLUMNNAME_C_InvoiceLine_ID) > 0)
		{
			 MInvoiceLine line = new MInvoiceLine(getCtx(), get_ValueAsInt(COLUMNNAME_C_InvoiceLine_ID), get_TrxName());
			 line.setQtyEntered(line.getQtyEntered().add(getMovementQty()));
			 line.setIsActive(true);
			 line.saveEx();
			 set_ValueNoCheck(COLUMNNAME_C_InvoiceLine_ID, null);
			 saveEx();
		}
		return null;
	}
	
	//is product has type same with other or not
	public boolean isSameProductType()
	{
		String sql = "SELECT COUNT(*) FROM M_InOutLine il"
				+ " INNER JOIN M_Product p ON p.M_Product_ID = il.M_Product_ID"
				+ " WHERE il.M_InOut_ID = ? AND il.M_InOutLine_ID <> ?"
				+ " AND p.IsConsignment = ?";
		int count = DB.getSQLValue(get_TrxName(), sql, getM_InOut_ID(),
				getM_InOutLine_ID(), getM_Product().isConsignment() ? "N" : "Y");
		
		if(count > 0)
			return false;
		
		return true;
	}
}
