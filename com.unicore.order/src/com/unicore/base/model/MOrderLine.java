/**
 * 
 */
package com.unicore.base.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MBPartnerProduct;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
//import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTax;
import org.compiere.model.MTaxCategory;
import org.compiere.model.MUOMConversion;
import org.compiere.model.MWarehouse;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MUNSAccessoriesLine;
import com.unicore.model.MUNSDiscountTrx;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author UNTA-Andy
 * 
 */
public class MOrderLine extends org.compiere.model.MOrderLine
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2461216245462830089L;
	private MOrder m_parent;

	//** UntaSoft customization -- AzHaidar **/
	protected boolean m_recalcBasedOnDiscountAmt = false;
	protected boolean m_recalcBasedOnDiscount	 = false;
	protected boolean m_recalcBasedOnQtyBonuses	 = false;

	/**
	 * @param ctx
	 * @param C_OrderLine_ID
	 * @param trxName
	 */
	public MOrderLine(Properties ctx, int C_OrderLine_ID, String trxName)
	{
		super(ctx, C_OrderLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param order
	 */
	public MOrderLine(MOrder order)
	{
		super(order);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MOrderLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.MOrderLine#getParent()
	 */
	@Override
	public MOrder getParent()
	{
		if (m_parent == null)
			m_parent = new MOrder(getCtx(), getC_Order_ID(), get_TrxName());

		return m_parent;
	}

	/*
	 * @return line without saving
	 */
	public static MOrderLine get(MOrder parent, int M_Product_ID, boolean createNew)
	{
		MOrderLine retValue = null;
		for (MOrderLine line : parent.getLines(true, null))
		{
			if (line.getM_Product_ID() == M_Product_ID)
			{
				retValue = line;
				break;
			}
		}
		
		if(!createNew)
			return retValue;

		if (retValue == null)
		{
			retValue = new MOrderLine(parent);
			retValue.setProduct(MProduct.get(parent.getCtx(), M_Product_ID));
			retValue.setisProductBonuses(true);
			parent.addLines(retValue);
			retValue.setQty(Env.ZERO);
		}

		retValue.setHeaderInfo(parent);
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.MOrderLine#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		MOrder order = (MOrder) getParent();
		if(!order.isSOTrx())
		{
			MBPartnerProduct bpProduct = MBPartnerProduct.get(getM_Product_ID(), getC_Order().getC_BPartner_ID(), get_TrxName());
			
			if(bpProduct != null)
				setVendorProductNo(bpProduct.getVendorProductNo());
			else
				setVendorProductNo(getM_Product().getName());
		}

		setQtyOrdered(getQtyEntered().add(getQtyBonuses()));
		
		if(!newRecord && is_ValueChanged(COLUMNNAME_QtyEntered))
		{
			if(!Util.isEmpty(MUNSAccessoriesLine.CreateAccessories(
					getCtx(), getM_Product_ID(), getQtyEntered(), get_ID(), Table_Name, get_TrxName(), false)))
				return false;
		}
		
		boolean retval = super.beforeSave(newRecord);

		return retval;
	}
	
	/**
	 * Recalculate the discount and discount amount based on price and discount field changes.
	 */
	public void setDiscount()
	{
		if (m_recalcBasedOnDiscountAmt || m_recalcBasedOnDiscount || m_recalcBasedOnQtyBonuses)
		{
			BigDecimal PriceList = getPriceList();
			BigDecimal PriceActual = PriceList;
			BigDecimal PriceEntered = PriceList;
			
			if (m_recalcBasedOnDiscount)
			{
				PriceActual = BigDecimal.valueOf(
						PriceList.doubleValue() * (100.0 - getDiscount().doubleValue())/100.0);
				BigDecimal discAmt = BigDecimal.valueOf(
						PriceList.doubleValue() * (getDiscount().doubleValue()/100.0) 
						* getQtyOrdered().doubleValue());
				
				if (discAmt.scale() > m_precision) {
					discAmt = discAmt.setScale(m_precision, BigDecimal.ROUND_HALF_DOWN);
				}
				set_Value(COLUMNNAME_DiscountAmt, discAmt);
			}
			else if (m_recalcBasedOnDiscountAmt)
			{
				PriceActual = BigDecimal.valueOf(
						PriceList.doubleValue() - (getDiscountAmt().doubleValue() / getQtyOrdered().doubleValue()));
				
				BigDecimal Discount = Env.ZERO;
				if (PriceList.signum() != 0)
					Discount = BigDecimal.valueOf(
							(PriceList.doubleValue() - PriceActual.doubleValue()) / PriceList.doubleValue() * 100.0);
				
				if (Discount.scale() > 2)
					Discount = Discount.setScale(2, BigDecimal.ROUND_HALF_UP);
				set_Value(COLUMNNAME_Discount, Discount);
			}
			else if(m_recalcBasedOnQtyBonuses)
			{
				PriceList = getPriceList();
				PriceActual = BigDecimal.valueOf(
						PriceList.doubleValue() - 
						(getQtyBonuses().doubleValue() * PriceList.doubleValue() / getQtyOrdered().doubleValue()));
				
				BigDecimal DiscAmt  = PriceList.multiply(getQtyBonuses());
				BigDecimal Discount = BigDecimal.valueOf(
						(DiscAmt.doubleValue() / getQtyOrdered().doubleValue()));
				
				if (Discount.scale() > 2)
					Discount = Discount.setScale(2, BigDecimal.ROUND_HALF_UP);
				set_Value(COLUMNNAME_Discount, Discount);
				set_Value(COLUMNNAME_DiscountAmt, DiscAmt);
			}
			
			if (PriceActual.scale() > m_precision)
				PriceActual = PriceActual.setScale(m_precision, BigDecimal.ROUND_HALF_UP);
		
			PriceEntered = MUOMConversion.convertProductFrom(
					getCtx(), getM_Product_ID(), getC_UOM_ID(), PriceActual);
			if (PriceEntered == null)
				PriceEntered = PriceActual;
			
			set_Value(COLUMNNAME_PriceActual, PriceActual);
			set_Value(COLUMNNAME_PriceEntered, PriceEntered);
		}
	}
	
	/**
	 * Set a flag so that the discount will be recalculated if only the discount value was set and changed.
	 */
	@Override
	public void setDiscount(BigDecimal discount)
	{
		Object oldVal = get_ValueOld (COLUMNNAME_Discount);
		if (((oldVal == null || ((BigDecimal) oldVal).signum() == 0) 
				&& discount != null && discount.signum() != 0) 
			||
			(oldVal != null && discount != null && discount.compareTo((BigDecimal) oldVal) != 0))
		{
			m_recalcBasedOnDiscount = true;
		}

		super.setDiscount(discount);
	} // setDiscount

	/**
	 * Set a flag so that the discount will be recalculated if only the discount amount value was set and changed.
	 */
	@Override
	public void setDiscountAmt(BigDecimal discountAmt)
	{
		Object oldVal = get_ValueOld (COLUMNNAME_DiscountAmt);
		if (((oldVal == null || ((BigDecimal) oldVal).signum() == 0) 
				&& discountAmt != null && discountAmt.signum() != 0) 
			||
			(oldVal != null && discountAmt != null && discountAmt.compareTo((BigDecimal) oldVal) != 0))
		{
			m_recalcBasedOnDiscountAmt = true;
		}

		super.setDiscountAmt(discountAmt);
	} // setDiscountAmt

	/**
	 * Set a flag so that the discount will be recalculated if only the discount amount value was set and changed.
	 */
	@Override
	public void setQtyBonuses(BigDecimal qtyBonuses)
	{
		Object oldVal = get_ValueOld (COLUMNNAME_QtyBonuses);
		if (((oldVal == null || ((BigDecimal) oldVal).signum() == 0) 
				&& qtyBonuses != null && qtyBonuses.signum() != 0) 
			||
			(oldVal != null && qtyBonuses != null && qtyBonuses.compareTo((BigDecimal) oldVal) != 0))
		{
			m_recalcBasedOnQtyBonuses = true;
		}

		super.setQtyBonuses(qtyBonuses);
	} // setQtyBonuses

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.MOrderLine#setDiscount()
	 * 
	 * @Override public void setDiscount() { // DO NOTHING //super.setDiscount(); }
	 */

	/**
	 * Calculate Extended Amt. May or may not include tax
	 */
	public void setLineNetAmt()
	{
		if (getQtyOrdered().signum() == 0) {
			super.setLineNetAmt(Env.ZERO);
			return;
		}
		
		BigDecimal bd = BigDecimal.valueOf(
				(getPriceList().doubleValue() - (getDiscountAmt().doubleValue() / getQtyOrdered().doubleValue()))
				* getQtyOrdered().doubleValue());

		boolean documentLevel = getTax().isDocumentLevel(this);

		// juddm: Tax Exempt & Tax Included in Price List & not Document Level - Adjust Line Amount
		// http://sourceforge.net/tracker/index.php?func=detail&aid=1733602&group_id=176962&atid=879332
		if (isTaxIncluded() && !documentLevel && getPriceList().compareTo(Env.ZERO) > 0)
		{
			BigDecimal taxStdAmt = Env.ZERO, taxThisAmt = Env.ZERO;

			MTax orderTax = getTax();
			MTax stdTax = null;

			// get the standard tax
			if (getProduct() == null)
			{
				if (getCharge() != null) // Charge
				{
					stdTax =
							new MTax(getCtx(), ((MTaxCategory) getCharge().getC_TaxCategory()).getDefaultTax()
									.getC_Tax_ID(), get_TrxName());
				}

			}
			else
				// Product
				stdTax =
						new MTax(getCtx(), ((MTaxCategory) getProduct().getC_TaxCategory()).getDefaultTax()
								.getC_Tax_ID(), get_TrxName());

			if (stdTax != null)
			{
				if (log.isLoggable(Level.FINE))
				{
					log.fine("stdTax rate is " + stdTax.getRate());
					log.fine("orderTax rate is " + orderTax.getRate());
				}

				taxThisAmt = taxThisAmt.add(orderTax.calculateTax(bd, isTaxIncluded(), getPrecision()));
				taxStdAmt = taxStdAmt.add(stdTax.calculateTax(bd, isTaxIncluded(), getPrecision()));

				bd = bd.subtract(taxStdAmt).add(taxThisAmt);

				if (log.isLoggable(Level.FINE))
					log.fine("Price List includes Tax and Tax Changed on Order Line: New Tax Amt: "
							+ taxThisAmt + " Standard Tax Amt: " + taxStdAmt + " Line Net Amt: " + bd);
			}

		}
		int precision = getPrecision();
		if (bd.scale() > precision)
			bd = bd.setScale(precision, BigDecimal.ROUND_HALF_UP);
		super.setLineNetAmt(bd);
	} // setLineNetAmt
	
	private MUNSDiscountTrx[] m_discountsTrx = null;
	
	public MUNSDiscountTrx[] getDiscountsTrx(boolean requery)
	{
		if(null != m_discountsTrx && !requery)
		{
			set_TrxName(m_discountsTrx, get_TrxName());
			return m_discountsTrx;
		}
		
		List<MUNSDiscountTrx> list = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSDiscountTrx.Table_Name
				, MUNSDiscountTrx.COLUMNNAME_C_OrderLine_ID + "=?", get_TrxName())
				.setParameters(getC_OrderLine_ID())
				.setOrderBy(MUNSDiscountTrx.COLUMNNAME_SeqNo).list();
		
		m_discountsTrx = new MUNSDiscountTrx[list.size()];
		list.toArray(m_discountsTrx);
		
		return m_discountsTrx;		
	}
	
	public MLocator getDefaultLocator()
	{
		MLocator loc = MLocator.getDefault((MWarehouse) getM_Warehouse());
		if(null == loc)
			throw new AdempiereUserError("No default locator from warehouse " 
							+ getM_Warehouse().getName());
		return loc;
	}
	
	
	/**
	 * Get Array of Canvas Order line. order by canvas order date.
	 * FOFO (First Order First Out)
	 * @param C_BPartner_ID
	 * @param M_Product_ID
	 * @param trxName
	 * @return
	 */
	public static MOrderLine[] getFOFO(int C_BPartner_ID, int M_Product_ID, String trxName)
	{
		StringBuilder sb = new StringBuilder("SELECT * FROM ").append(Table_Name)
				.append(" WHERE ").append(COLUMNNAME_M_Product_ID).append("= ? ")
				.append(" AND ").append(COLUMNNAME_C_Order_ID).append(" IN (SELECT ")
				.append(COLUMNNAME_C_Order_ID).append(" FROM ").append(MOrder.Table_Name)
				.append(" WHERE ").append(MOrder.COLUMNNAME_C_BPartner_ID).append(" = ?")
				.append(" AND (").append(COLUMNNAME_QtyOrdered).append("-")
				.append(COLUMNNAME_QtyDelivered).append(")").append("<> 0")
				.append(" ORDER BY ").append(MOrder.COLUMNNAME_DateOrdered).append(")");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		String sql = sb.toString();
		List<MOrderLine> list = new ArrayList<>();
		MOrderLine[] retVals = null;
		
		try
		{
			st = DB.prepareStatement(sql, trxName);
			st.setInt(1, M_Product_ID);
			st.setInt(2, C_BPartner_ID);
			rs = st.executeQuery();
			while (rs.next())
			{
				MOrderLine record = new MOrderLine(Env.getCtx(), rs, trxName);
				list.add(record);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		retVals = new MOrderLine[list.size()];
		list.toArray(retVals);
		
		return retVals;
	}
	
	public static BigDecimal reCalcQtyPacked(int C_OrderLine_ID, String trxName)
	{
		BigDecimal QtyPacked = Env.ZERO;
		
		String sql = "SELECT COALESCE(SUM(MovementQty),0) FROM UNS_PackingList_Line WHERE C_OrderLine_ID = ?"
				+ " AND UNS_PackingList_Order_ID IN (SELECT UNS_PackingList_Order_ID FROM UNS_PackingList_Order"
				+ " WHERE UNS_PackingList_ID IN (SELECT UNS_PackingList_ID FROM UNS_PackingList WHERE DocStatus NOT IN"
				+ " ('VO', 'RE')))";
		QtyPacked = DB.getSQLValueBD(trxName, sql, C_OrderLine_ID);
		
		return QtyPacked; 
	}
}
