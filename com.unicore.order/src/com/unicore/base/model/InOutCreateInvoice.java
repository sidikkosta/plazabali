package com.unicore.base.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MCurrency;
import com.unicore.base.model.MInOut;
import com.unicore.base.model.MInOutLine;
import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MInvoiceLine;
import org.compiere.model.MInvoicePaySchedule;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderPaySchedule;
import org.compiere.model.MPriceList;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MUNSAccessoriesLine;

public class InOutCreateInvoice extends SvrProcess
{
	/**	Shipment					*/
	protected int 	p_M_InOut_ID = 0;
	/** Document Date				*/
	protected Timestamp p_DateDoc;
	/**	Price List Version			*/
	protected int		p_M_PriceList_ID = 0;
	/** Document No					*/
	protected String	p_InvoiceDocumentNo = null;
	/** Use Same Reference No		*/
	protected boolean	p_UseSameReferenceNo = false;
	/** Document Type				*/
	protected int		p_DocType_ID = 0;
	
	@Override
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("DateDoc"))
				p_DateDoc = para[i].getParameterAsTimestamp();
			else if (name.equals("M_PriceList_ID"))
				p_M_PriceList_ID = para[i].getParameterAsInt();
			else if (name.equals("InvoiceDocumentNo"))
				p_InvoiceDocumentNo = (String)para[i].getParameter();
			else if (name.equals("UseSameReferenceNo"))
				p_UseSameReferenceNo = para[i].getParameterAsBoolean();
			else if (name.equals("C_DocType_ID"))
				p_DocType_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_M_InOut_ID = getRecord_ID();
	}	//	prepare

	@SuppressWarnings("unused")
	@Override
	protected String doIt () throws Exception
	{
		if (log.isLoggable(Level.INFO)) log.info("M_InOut_ID=" + p_M_InOut_ID 
			+ ", M_PriceList_ID=" + p_M_PriceList_ID
			+ ", InvoiceDocumentNo=" + p_InvoiceDocumentNo);
		if (p_M_InOut_ID == 0)
			throw new IllegalArgumentException("No Shipment");
		//
		MInOut ship = new MInOut (getCtx(), p_M_InOut_ID, get_TrxName());
		if (ship.get_ID() == 0)
			throw new IllegalArgumentException("Shipment not found");
		
		if(!ship.isProcessed())
			throw new AdempiereException("Document not processed, cannot process.");
		
		if(ship.isFullConsigment())
			throw new AdempiereException("There was consignment product exists on receipt line.");
//		if(s)
		
		String sql = "SELECT DocumentNo FROM C_Invoice WHERE C_Invoice_ID = ? AND DocStatus NOT IN ('VO', 'RE')";
		String docNo = DB.getSQLValueString(get_TrxName(), sql, 
				ship.getC_Invoice_ID());
		if (!Util.isEmpty(docNo, true)) {
			return "The invoice has already generated, invoice document no : "
					+ docNo;
		}
		
		if (!MInOut.DOCSTATUS_Completed.equals(ship.getDocStatus()))
			;//throw new IllegalArgumentException("Shipment not completed");
		
		MInvoice invoice = new MInvoice (ship, p_DateDoc);
		if(p_UseSameReferenceNo && !Util.isEmpty(ship.getPOReference(), true))
			invoice.setReferenceNo(ship.getPOReference());
		if(p_DocType_ID > 0)
		{
			invoice.setC_DocType_ID(p_DocType_ID);
			invoice.setC_DocTypeTarget_ID(p_DocType_ID);
		}
		// Should not override pricelist for RMA
		if (p_M_PriceList_ID <= 0)
		{
			if(ship.getC_Order_ID() > 0)
				p_M_PriceList_ID = ship.getC_Order().getM_PriceList_ID();
			else
			{
				MPriceList pl = MPriceList.getDefault(getCtx(), ship.isSOTrx());
				if(pl == null)
				{
					throw new AdempiereException("Default price list not set.");
				}
				else
					p_M_PriceList_ID = pl.get_ID();
			}
		}
		
		invoice.setM_PriceList_ID(p_M_PriceList_ID);
		
		if (p_InvoiceDocumentNo != null && p_InvoiceDocumentNo.length() > 0)
			invoice.setDocumentNo(p_InvoiceDocumentNo);
		if (!invoice.save())
			throw new IllegalArgumentException("Cannot save Invoice");
		MInOutLine[] shipLines = ship.getLines(false);
		for (int i = 0; i < shipLines.length; i++)
		{
			MInOutLine sLine = shipLines[i];
			if(!sLine.isProductAccessories())
			{
				MInvoiceLine line = new MInvoiceLine(invoice);
				line.setShipLine(sLine);
				if (sLine.sameOrderLineUOM())
					line.setQtyEntered(sLine.getQtyEntered());
				else
					line.setQtyEntered(sLine.getMovementQty());
				line.setQtyInvoiced(sLine.getMovementQty());
				line.saveEx();
			}
			else
			{
				MInOutLine parentLine = new MInOutLine(getCtx(), sLine.getInOutLine_ID(), get_TrxName());
				if(parentLine == null)
					throw new IllegalArgumentException("Parent Line ID not found");
				
				String sqql = "SELECT UNS_ProductAccessories_ID FROM UNS_ProductAccessories WHERE M_Product_ID=?"
						+ " AND ProductAccessories_ID = ?";
				int idProductAcc = DB.getSQLValue(get_TrxName(), sqql,parentLine.getM_Product_ID() ,sLine.getM_Product_ID());
				if(idProductAcc <= 0)
					throw new IllegalArgumentException("Product Accessories not found");
				
				MUNSAccessoriesLine accline = new MUNSAccessoriesLine(getCtx(), 0, get_TrxName());
				accline.setAD_Org_ID(sLine.getAD_Org_ID());
				accline.setUNS_ProductAccessories_ID(idProductAcc);
				accline.setProductAccessories_ID(sLine.getM_Product_ID());
				accline.setQty(sLine.getQtyEntered());
				accline.setC_InvoiceLine_ID(parentLine.getC_InvoiceLine_ID());
				accline.saveEx();

			}

		}
				
		if (invoice.getC_Order_ID() > 0) {
			MOrder order = new MOrder(getCtx(), invoice.getC_Order_ID(), get_TrxName());
			invoice.setPaymentRule(order.getPaymentRule());
			invoice.setC_PaymentTerm_ID(order.getC_PaymentTerm_ID());
			invoice.saveEx();
			ship.setC_Invoice_ID(invoice.get_ID());
			invoice.load(invoice.get_TrxName()); // refresh from DB
			// copy payment schedule from order if invoice doesn't have a current payment schedule
			MOrderPaySchedule[] opss = MOrderPaySchedule.getOrderPaySchedule(getCtx(), order.getC_Order_ID(), 0, get_TrxName());
			MInvoicePaySchedule[] ipss = MInvoicePaySchedule.getInvoicePaySchedule(getCtx(), invoice.getC_Invoice_ID(), 0, get_TrxName());
			if (ipss.length == 0 && opss.length > 0) {
				BigDecimal ogt = order.getGrandTotal();
				BigDecimal igt = invoice.getGrandTotal();
				BigDecimal percent = Env.ONE;
				if (ogt.compareTo(igt) != 0)
					percent = igt.divide(ogt, 10, BigDecimal.ROUND_HALF_UP);
				MCurrency cur = MCurrency.get(order.getCtx(), order.getC_Currency_ID());
				int scale = cur.getStdPrecision();
			
				for (MOrderPaySchedule ops : opss) {
					MInvoicePaySchedule ips = new MInvoicePaySchedule(getCtx(), 0, get_TrxName());
					PO.copyValues(ops, ips);
					if (percent != Env.ONE) {
						BigDecimal propDueAmt = ops.getDueAmt().multiply(percent);
						if (propDueAmt.scale() > scale)
							propDueAmt = propDueAmt.setScale(scale, BigDecimal.ROUND_HALF_UP);
						ips.setDueAmt(propDueAmt);
					}
					ips.setC_Invoice_ID(invoice.getC_Invoice_ID());
					ips.setAD_Org_ID(ops.getAD_Org_ID());
					ips.setProcessing(ops.isProcessing());
					ips.setIsActive(ops.isActive());
					ips.saveEx();
				}
			}
			invoice.validatePaySchedule();
			invoice.saveEx();
		}
		
		ship.setC_Invoice_ID(invoice.get_ID());
		ship.saveEx();
		
		addLog(invoice.getC_Invoice_ID(), invoice.getDateInvoiced(), invoice.getGrandTotal(), invoice.getDocumentNo(), invoice.get_Table_ID(), invoice.getC_Invoice_ID());
		
		return invoice.getDocumentNo();
	}	//	InOutCreateInvoice
	
}	//	InOutCreateInvoice

				
