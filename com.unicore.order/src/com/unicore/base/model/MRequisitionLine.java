package com.unicore.base.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MSysConfig;
import org.compiere.util.DB;

import com.unicore.model.MUNSRequisitionLineMA;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;


public class MRequisitionLine extends org.compiere.model.MRequisitionLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8568755468048972771L;

	public MRequisitionLine (Properties ctx, int M_RequisitionLine_ID, String trxName)
	{
		super(ctx, M_RequisitionLine_ID, trxName);
	}	//	MRequisitionLine
	
	public MRequisitionLine (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MRequisitionLine
	
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		super.setFromOrder(true);
		Boolean retVal = super.beforeSave(newRecord);
		setLineNetAmt(getQty().multiply(getPriceActual()));
		
		if(getM_Product_ID() > 0)
		{
			String sql = "SELECT COUNT(*) FROM M_RequisitionLine WHERE M_Product_ID = ?"
					+ " AND M_RequisitionLine_ID <> ? AND M_Requisition_ID = ?";
			int count = DB.getSQLValue(get_TrxName(), sql, getM_Product_ID(), get_ID(), getM_Requisition_ID());
			
			if(count > 0)
			{
				log.saveError("Error", "Duplicate product on one request.");
				return false;
			}
		}
		
		return retVal;
	}
	
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		boolean generateMA = MSysConfig.getBooleanValue(MSysConfig.REQUISITION_USE_STORAGE_RESERVATION, false);
		if(generateMA && getM_Requisition().getC_DocType_ID() == 1000594)
		{
			if(newRecord)
			{
				MRequisition req = new MRequisition(getCtx(), getM_Requisition_ID(), get_TrxName());
				req.checkMaterialPolicy(this, getQty());
			}
			else if(is_ValueChanged(COLUMNNAME_Qty))
			{
				String sql = "DELETE FROM UNS_RequisitionLineMA WHERE M_RequisitionLine_ID = ?";
				DB.executeUpdate(sql, get_ID(), get_TrxName());
				MRequisition req = new MRequisition(getCtx(), getM_Requisition_ID(), get_TrxName());
				req.checkMaterialPolicy(this, getQty());
			}
		}
		
		return true;
	}
	
	public MUNSRequisitionLineMA[] getMAs()
	{
		List<MUNSRequisitionLineMA> mas = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID,
				MUNSRequisitionLineMA.Table_Name, Table_Name + "_ID=?", get_TrxName()).setParameters(get_ID())
					.list();
		
		return mas.toArray(new MUNSRequisitionLineMA[mas.size()]);
	}
	
	protected boolean beforeDelete()
	{
		String sql = "DELETE FROM UNS_RequisitionLineMA WHERE M_RequisitionLine_ID=?";
		DB.executeUpdate(sql, get_ID(), get_TrxName());
		
		return true;
	}
}