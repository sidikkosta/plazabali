/**
 * 
 */
package com.unicore.base.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MProduct;
import org.compiere.model.MUOMConversion;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MUNSHandleRequests;
import com.unicore.model.MUNSInvDeptAllocation;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;


/**
 * @author UNTA-Andy
 * 
 */
public class MInvoiceLine extends org.compiere.model.MInvoiceLine
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7530443759523381705L;
	private MInvoice m_parent;

	//** UntaSoft customization -- AzHaidar **/
	protected boolean m_recalcBasedOnDiscountAmt = false;
	protected boolean m_recalcBasedOnDiscount	 = false;
	protected boolean m_recalcBasedOnQtyBonuses	 = false;

	/**
	 * @param ctx
	 * @param C_InvoiceLine_ID
	 * @param trxName
	 */
	public MInvoiceLine(Properties ctx, int C_InvoiceLine_ID, String trxName)
	{
		super(ctx, C_InvoiceLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param invoice
	 */
	public MInvoiceLine(MInvoice invoice)
	{
		super(invoice);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MInvoiceLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.MInvoiceLine#getParent()
	 */
	@Override
	public MInvoice getParent()
	{
		if (m_parent == null)
			m_parent = new MInvoice(getCtx(), getC_Invoice_ID(), get_TrxName());
		return m_parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.compiere.model.MInvoiceLine#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord)
	{
		
		is_ValueChanged(COLUMNNAME_C_Invoice_ID);
		//ini harusnya sum dulu order itu udah masuk ke packing list berapa, jangan compare yang saat ini aja.
		//checkInvPL
//		int cInvPL = DB.getSQLValue(get_TrxName(), "SELECT uns_packinglist_order_id from uns_packinglist_order where c_invoice_id = "+getC_Invoice_ID());
//		
//		if(cInvPL>0)
//		{
//			int orderID = DB.getSQLValue(get_TrxName(), "SELECT C_Order_ID From C_Invoice where c_Invoice_ID ="+getC_Invoice_ID());
//			MOrder order = new MOrder(getCtx(), orderID, get_TrxName());
//			for(org.compiere.model.MOrderLine getOrderLine : order.getLines("", ""))
//			{
//				if(getOrderLine.getM_Product_ID()!=getM_Product_ID())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this product cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getQtyEntered()!=getQtyEntered())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this QtyEntered cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getQtyBonuses()!=getQtyBonuses())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this QtyBonuses cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getC_Tax_ID()!=getC_Tax_ID())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this Tax cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getUOMConversionL1()!=getUOMConversionL1())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this Convertion L1 cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getUOMConversionL2()!=getUOMConversionL2())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this Convertion L2 cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getUOMConversionL3()!=getUOMConversionL3())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this Convertion L3 cannot deference with Order "+order.getDocumentNo());
//				}
//				
//				else if(getOrderLine.getUOMConversionL4()!=getUOMConversionL4())
//				{
//					throw new AdempiereException("Could not update this record because "
//							+ "this Convertion L4 cannot deference with Order "+order.getDocumentNo());
//				}
//			}
//		}
		
//		int cInvPL = DB.getSQLValue(get_TrxName(), "SELECT uns_packinglist_id from uns_packinglist_order where c_invoice_id = "+getC_Invoice_ID());
//		
//		if(cInvPL>0)
//		{
//			int docPL = DB.getSQLValue(get_TrxName(), "SELECT documentno from uns_packinglist where uns_packinglist_id = "+cInvPL);
//			if(is_ValueChanged(COLUMNNAME_M_Product_ID)==true||is_ValueChanged(COLUMNNAME_QtyEntered)==true
//					||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL1)==true||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL2)==true 
//					||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL3)==true||is_ValueChanged(COLUMNNAME_UOMQtyEnteredL4)==true
//					||is_ValueChanged(COLUMNNAME_UOMQtyBonusesL1)==true||is_ValueChanged(COLUMNNAME_UOMQtyBonusesL2)==true 
//					||is_ValueChanged(COLUMNNAME_UOMQtyBonusesL3)==true||is_ValueChanged(COLUMNNAME_UOMQtyBonusesL4)==true)
//			{
//				throw new AdempiereException("Could not update this record because reference PLOrder : "+docPL);
//			}
//		}
//		
		if (newRecord)
		{	
			MProduct p = MProduct.get(getCtx(), getM_Product_ID());
			boolean isItem = p != null && p.isItem();
			if (getC_OrderLine_ID() != 0  && !isItem)
			{
				MUNSHandleRequests[] requests = MUNSHandleRequests.getByOrderLine(
						getCtx(), getC_OrderLine_ID(), 
						get_TrxName());
				if (requests.length ==1)
				{
					String sql = "SELECT C_BPartner_ID FROM M_Requisition WHERE M_Requisition_ID = "
							+ " (SELECT M_Requisition_ID FROM M_RequisitionLine WHERE M_RequisitionLine_ID = ?)";
					int secOfDeptID = DB.getSQLValue(get_TrxName(), sql, requests[0].getM_RequisitionLine_ID());
					setSectionOfDept_ID(secOfDeptID);
					setIsMultiAllocation(false);
				}
				else if (requests.length > 1)
					setIsMultiAllocation(true);
				
				MInvoiceLine[] lines = getParent().getLines(true);
				MOrderLine oLine = new MOrderLine(getCtx(), getC_OrderLine_ID(), get_TrxName());
				
				if (oLine.getRef_OrderLine_ID() > 0 && lines.length > 0)
				{
					for (MInvoiceLine line : lines)
					{
						if (line.getC_OrderLine_ID() == oLine.getRef_OrderLine_ID())
						{
							setRef_InvoiceLine_ID(line.get_ID());
							line.save();
						}
					}
				}
			}
		}
		
		if (is_ValueChanged("QtyInvoiceTicket"))
		{
			BigDecimal qtyInvTicket = (BigDecimal) get_Value("QtyInvoiceTicket");
			if (qtyInvTicket.signum() != 0 && !getParent().isSOTrx())
			{
				BigDecimal discountAmt = getQtyInvoiced().subtract(qtyInvTicket);
				discountAmt = discountAmt.multiply(getPriceList());
				setDiscountAmt(discountAmt);
			}
		}
		
	
		setQtyInvoiced(getQtyEntered().add(getQtyBonuses()));
		
		return super.beforeSave(newRecord);
	}
	
	@Override
	public boolean afterSave (boolean newRecord, boolean success)
	{
		if(getC_OrderLine_ID() > 0 && getParent().isSOTrx() && is_ValueChanged(COLUMNNAME_C_OrderLine_ID))
		{
			setDescription(Util.isEmpty(getDescription()) ? getC_OrderLine().getDescription()
					: getDescription() + " | " + getC_OrderLine().getDescription());
		}
		
		if (newRecord && getC_OrderLine_ID() > 0)
		{
			MUNSHandleRequests[] requests = MUNSHandleRequests.getByOrderLine(
					getCtx(), getC_OrderLine_ID(), 
					get_TrxName());
			BigDecimal unallocated = getQtyInvoiced();
			for (int i=0; unallocated.signum() == 1 && requests.length > 1 
					&& i<requests.length; i++)
			{
				String sql = "SELECT C_BPartner_ID FROM M_Requisition WHERE M_Requisition_ID = "
						+ " (SELECT M_Requisition_ID FROM M_RequisitionLine WHERE M_RequisitionLine_ID = ?)";
				BigDecimal tobeAllocated = requests[i].getQty();
				if (tobeAllocated.compareTo(unallocated) == 1)
					tobeAllocated = unallocated;
				int secOfDeptID = DB.getSQLValue(get_TrxName(), sql, requests[i].getM_RequisitionLine_ID());
				MUNSInvDeptAllocation aloc = new MUNSInvDeptAllocation(this);
				aloc.setQty(tobeAllocated);
				aloc.setSectionOfDept_ID(secOfDeptID);
				if (!aloc.save())
					return false;
				unallocated = unallocated.subtract(tobeAllocated);
			}
		}
		return super.afterSave(newRecord, success);
	}
	
	protected boolean beforeDelete ()
	{
		
		int cInvPL = DB.getSQLValue(get_TrxName(), "SELECT uns_packinglist_id from uns_packinglist_order where c_invoice_id = "+getC_Invoice_ID());
		
		if(cInvPL>0)
		{
			int docPL = DB.getSQLValue(get_TrxName(), "SELECT documentno from uns_packinglist where uns_packinglist_id = "+cInvPL);
			throw new AdempiereException("Could not delete this record because reference PLOrder : "+docPL);
		}
		return true;
		
	}
	/**
	 * Recalculate the discount and discount amount based on price and discount field changes.
	 */
	@Override
	protected void analyzeDiscount()
	{
		if (m_recalcBasedOnDiscountAmt || m_recalcBasedOnDiscount || m_recalcBasedOnQtyBonuses)
		{
			BigDecimal PriceList = getPriceList();
			BigDecimal PriceActual = PriceList;
			BigDecimal PriceEntered = PriceList;
			
			if (m_recalcBasedOnDiscountAmt)
			{
				PriceActual = BigDecimal.valueOf(
						PriceList.doubleValue() - (getDiscountAmt().doubleValue() / getQtyInvoiced().doubleValue()));
				
				BigDecimal Discount = Env.ZERO;
				if (PriceList.signum() != 0)
					Discount = BigDecimal.valueOf(
							(PriceList.doubleValue() - PriceActual.doubleValue()) / PriceList.doubleValue() * 100.0);
				
				if (Discount.scale() > 2)
					Discount = Discount.setScale(2, BigDecimal.ROUND_HALF_UP);
				set_Value(COLUMNNAME_Discount, Discount);
			}
			else if (m_recalcBasedOnDiscount)
			{
				PriceActual = BigDecimal.valueOf(
						PriceList.doubleValue() * (100.0 - getDiscount().doubleValue())/100.0);
				BigDecimal discAmt = BigDecimal.valueOf(
						PriceList.doubleValue() * (getDiscount().doubleValue()/100.0) 
						* getQtyInvoiced().doubleValue());
				
				if (discAmt.scale() > m_precision) {
					discAmt = discAmt.setScale(m_precision, BigDecimal.ROUND_HALF_DOWN);
				}
				set_Value(COLUMNNAME_DiscountAmt, discAmt);
			}
			else if(m_recalcBasedOnQtyBonuses)
			{
				PriceList = getPriceList();
				PriceActual = BigDecimal.valueOf(
						PriceList.doubleValue() - 
						(getQtyBonuses().doubleValue() * PriceList.doubleValue() / getQtyInvoiced().doubleValue()));
				
				BigDecimal DiscAmt  = PriceList.multiply(getQtyBonuses());
				BigDecimal Discount = BigDecimal.valueOf(
						(DiscAmt.doubleValue() / getQtyInvoiced().doubleValue()));
				
				if (Discount.scale() > 2)
					Discount = Discount.setScale(2, BigDecimal.ROUND_HALF_UP);
				set_Value(COLUMNNAME_Discount, Discount);
				set_Value(COLUMNNAME_DiscountAmt, DiscAmt);
			}
			
			if (PriceActual.scale() > m_precision)
				PriceActual = PriceActual.setScale(m_precision, BigDecimal.ROUND_HALF_UP);
		
			PriceEntered = MUOMConversion.convertProductFrom(
					getCtx(), getM_Product_ID(), getC_UOM_ID(), PriceActual);
			if (PriceEntered == null)
				PriceEntered = PriceActual;
			setPrice(PriceEntered);
//			set_Value(COLUMNNAME_PriceActual, PriceActual);
//			set_Value(COLUMNNAME_PriceEntered, PriceEntered);
		}
		else {
			super.analyzeDiscount();
		}
	}
	
	/**
	 * Set a flag so that the discount will be recalculated if only the discount value was set and changed.
	 */
	@Override
	public void setDiscount(BigDecimal discount)
	{
		Object oldVal = get_ValueOld (COLUMNNAME_Discount);
		if (((oldVal == null || ((BigDecimal) oldVal).signum() == 0) 
				&& discount != null && discount.signum() != 0) 
			||
			(oldVal != null && discount != null && discount.compareTo((BigDecimal) oldVal) != 0))
		{
			m_recalcBasedOnDiscount = true;
		}

		super.setDiscount(discount);
	} // setDiscount

	/**
	 * Set a flag so that the discount will be recalculated if only the discount amount value was set and changed.
	 */
	@Override
	public void setDiscountAmt(BigDecimal discountAmt)
	{
		Object oldVal = get_ValueOld (COLUMNNAME_DiscountAmt);
		if (((oldVal == null || ((BigDecimal) oldVal).signum() == 0) 
				&& discountAmt != null && discountAmt.signum() != 0) 
			||
			(oldVal != null && discountAmt != null && discountAmt.compareTo((BigDecimal) oldVal) != 0))
		{
			m_recalcBasedOnDiscountAmt = true;
		}

		super.setDiscountAmt(discountAmt);
	} // setDiscountAmt

	/**
	 * Set a flag so that the discount will be recalculated if only the discount amount value was set and changed.
	 */
	@Override
	public void setQtyBonuses(BigDecimal qtyBonuses)
	{
		Object oldVal = get_ValueOld (COLUMNNAME_QtyBonuses);
		if (((oldVal == null || ((BigDecimal) oldVal).signum() == 0) 
				&& qtyBonuses != null && qtyBonuses.signum() != 0) 
			||
			(oldVal != null && qtyBonuses != null && qtyBonuses.compareTo((BigDecimal) oldVal) != 0))
		{
			m_recalcBasedOnQtyBonuses = true;
		}

		super.setQtyBonuses(qtyBonuses);
	} // setQtyBonuses
	
	private MUNSInvDeptAllocation[] m_deptAllocs = null;
	
	public  MUNSInvDeptAllocation[] getDeptAllocations (boolean requery)
	{
		if (m_deptAllocs != null && !requery)
		{
			set_TrxName(m_deptAllocs, get_TrxName());
			return m_deptAllocs;
		}
		
		List<MUNSInvDeptAllocation> list = Query.get(
				getCtx(), 
				UNSOrderModelFactory.EXTENSION_ID, 
				MUNSInvDeptAllocation.Table_Name, Table_Name +"_ID=?", 
				get_TrxName()).setParameters(get_ID()).list();
		m_deptAllocs = new MUNSInvDeptAllocation[list.size()];
		list.toArray(m_deptAllocs);
		return m_deptAllocs;
	}

} // MInvoiceLine
