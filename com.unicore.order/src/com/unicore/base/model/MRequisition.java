package com.unicore.base.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.I_M_RequisitionLine;

import com.unicore.base.model.MRequisitionLine;

import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MLocator;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.MRequisitionConfirm;
import com.unicore.model.MUNSRequisitionLineMA;
import com.unicore.model.MUNSStorageReservation;
import com.unicore.model.X_M_RequisitionConfirm;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.model.IUNSApprovalInfo;
import com.uns.model.MUNSConfirmationFlow;
import com.uns.model.MUNSConfirmationFlowLine;
import com.uns.base.model.Query;

public class MRequisition extends org.compiere.model.MRequisition implements DocAction,
		DocOptions, IUNSApprovalInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -672925105355004570L;
	
	private MRequisitionLine[] m_lines = null;
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private boolean m_confirmed = false;
	private int m_user_id = -1;
	private int m_role_id = -1;

	public MRequisition (Properties ctx, int M_Requisition_ID , String trxName)
	{
		super(ctx , M_Requisition_ID , trxName);
	}
	
	public MRequisition (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MRequisition
	
	
	public MRequisitionLine[] getLines()
	{
		if (m_lines != null) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
 	 	final String whereClause = I_M_RequisitionLine.COLUMNNAME_M_Requisition_ID+"=?";
	 	List <MRequisitionLine> list = 
	 			Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, I_M_RequisitionLine.Table_Name
	 					, whereClause, get_TrxName()).setParameters(getM_Requisition_ID())
	 					.setOrderBy(I_M_RequisitionLine.COLUMNNAME_Line).list();

		m_lines = new MRequisitionLine[list.size()];
		list.toArray (m_lines);
		return m_lines;
	}	//	getLines
	
	
	@Override
	public String completeIt()
	{
//		Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		boolean needConfirm = getC_DocType().isShipConfirm();
		
		// Set the definite document number after completed (if needed)
//			setDefiniteDocumentNo();
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		//DocType Requisition Confirm
		if(needConfirm)
		{
			int docType = MDocType.getDocType("PRC");
			MUNSConfirmationFlow flow = MUNSConfirmationFlow.get(getCtx(), X_M_RequisitionConfirm.Table_ID, docType, get_TrxName());
			if(flow != null)
			{
				MUNSConfirmationFlowLine line = flow.getLinebySequence(1);
				if(line == null)
				{
					m_processMsg = "No Flow Line...";
					return DocAction.STATUS_Invalid;
				}
				
				if(line.getAD_User_ID() > 0)
					m_user_id = line.getAD_User_ID();
				else if(line.getAD_Role_ID() > 0)
					m_role_id = line.getAD_Role_ID();
				
				MRequisitionConfirm confirm = MRequisitionConfirm.getCreate(getCtx(), this, get_TrxName());
				if(confirm==null)
				{
					m_processMsg = "Error when try to create confirm";
					return DocAction.STATUS_Invalid;
				}
				
				if(confirm.getDocStatus().equals(DOCSTATUS_Drafted) && !m_confirmed)
				{
					confirm.setConfirmByUser_ID(m_user_id);
					confirm.setConfirmByRole_ID(m_role_id);
					confirm.saveEx();
				}
				
				if((confirm.getDocStatus().equals(DOCSTATUS_Drafted) 
						|| confirm.getDocStatus().equals(DOCSTATUS_InProgress)
						|| confirm.getDocStatus().equals(DOCSTATUS_Invalid)
						|| confirm.getDocStatus().equals(DOCSTATUS_Approved)
						|| confirm.getDocStatus().equals(DOCSTATUS_NotApproved))
					&& !m_confirmed)
				{
					setProcessed(true);
					m_processMsg = "Please complete requisition confirm first. "+confirm.getDocumentNo();
					return DocAction.STATUS_InProgress;
				}
			}
		}
		
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}
		
		boolean generateMA = MSysConfig.getBooleanValue(MSysConfig.REQUISITION_USE_STORAGE_RESERVATION, false);
		if(generateMA)
		{
			MRequisitionLine[] lines = getLines();
			for(int i=0;i<lines.length;i++)
			{
				MUNSRequisitionLineMA[] mas = lines[i].getMAs();
				for(int j=0;j<mas.length;j++)
				{
					MUNSStorageReservation.addOrCreate(
							getCtx(), getDestinationWarehouse_ID(), lines[i].getM_Product_ID(),
								mas[j].getM_AttributeSetInstance_ID(), mas[j].getMovementQty(), get_TrxName());
				}
			}
		}
		
		if(!needConfirm)
			return super.completeIt();

		//
		super.setQtyOnHand();
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		
//		if (docStatus.equals(DocumentEngine.STATUS_Completed))
//		{
//			options[index++] = DOCACTION_Close;
//		}
		
		return super.customizeValidActions(docStatus, processing, orderType, isSOTrx, AD_Table_ID, docAction, options, index);
	}
	
	@Override
	public boolean voidIt() {
		
		if(log.isLoggable(Level.INFO))
			log.info("VoidIt - " +toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		if(getDocStatus().equals(STATUS_InProgress))
			throw new AdempiereException("This Requisition has not been confirmed");
		
		String sql = "SELECT 1 FROM M_RequisitionConfirm WHERE M_Requisition_ID = ? AND DocStatus NOT IN ('VO','RE')";
		boolean exist = DB.getSQLValue(get_TrxName(), sql, getM_Requisition_ID()) > 0;
		
		if(exist)
			throw new AdempiereException("Can't void this requisition. Please void Requisition Confirm first.");
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		return true;
	}
	
	@Override
	public boolean reActivateIt() {
		
		m_processMsg = "Disallowed Reactive";
		return false;
	}
	
	public void setConfirmed(boolean confirm){
		m_confirmed = confirm;
	}
	
	private MLocator loc = null;
	public MLocator getLocator()
	{
		if(loc != null)
			return loc;
		else
		{
			loc = MLocator.get(getCtx(), getM_Warehouse_ID(), getC_BPartner().getValue());
			return loc;
		}
	}
	
	public void checkMaterialPolicy(MRequisitionLine line,BigDecimal qtyToDeliver)
	{
		if(Env.ZERO.compareTo(qtyToDeliver)==0)
			return;
		
		int no = MUNSRequisitionLineMA.deleteRequisitionLineMA(line.getM_RequisitionLine_ID(), get_TrxName());
		if (no > 0)
			if (log.isLoggable(Level.CONFIG)) log.config("Delete old #" + no);
		
		MProduct product = MProduct.get(getCtx(), line.getM_Product_ID());
		String MMPolicy = product.getMMPolicy();
		MWarehouse whs = MWarehouse.get(getCtx(), getDestinationWarehouse_ID());
		
		MLocator[] loc = whs.getLocators(true);
		
		for(int i=0;i<loc.length;i++)
		{
			if(loc[i].isInTransit())
				continue;
			
			MStorageOnHand[] storages = MStorageOnHand.getWarehouse(getCtx(), 0, line.getM_Product_ID(), 0, 
					null, MClient.MMPOLICY_FiFo.equals(MMPolicy), true, loc[i].get_ID(), get_TrxName());

			MUNSRequisitionLineMA[] manualMAs = MUNSRequisitionLineMA.get(getCtx(), line.get_ID(), get_TrxName());

			for (MStorageOnHand storage: storages)
			{
				if(storage.getM_Locator_ID() != loc[i].getM_Locator_ID())
					continue;
				MUNSRequisitionLineMA manualMA = null;
				
				for (MUNSRequisitionLineMA ma : manualMAs)
				{
					if (ma.getM_AttributeSetInstance_ID() == storage.getM_AttributeSetInstance_ID()) {
						manualMA = ma;
						break;
					}
				}
				
				if (manualMA != null)
				{
					storage.setQtyOnHand(storage.getQtyOnHand().subtract(manualMA.getMovementQty()));
				}
				
				MUNSStorageReservation reserve = MUNSStorageReservation.get(getCtx(),
						getDestinationWarehouse_ID(), line.getM_Product_ID(), storage.getM_AttributeSetInstance_ID(),
							get_TrxName());
				
				if(reserve != null)
				{
					storage.setQtyOnHand(storage.getQtyOnHand().subtract(reserve.getQty()));
				}
				
				if (storage.getQtyOnHand().compareTo(Env.ZERO) <= 0)
					continue;
				
				if (storage.getQtyOnHand().compareTo(qtyToDeliver) >= 0)
				{
					MUNSRequisitionLineMA ma = MUNSRequisitionLineMA.addOrCreate(line, 
							storage.getM_AttributeSetInstance_ID(),
							qtyToDeliver,storage.getDateMaterialPolicy(),loc[i].get_ID(),false);
					ma.saveEx();		
					qtyToDeliver = Env.ZERO;
					if (log.isLoggable(Level.FINE)) log.fine( ma + ", QtyToDeliver=" + qtyToDeliver);		
				}
				else
				{
					MUNSRequisitionLineMA ma = MUNSRequisitionLineMA.addOrCreate(line, 
							storage.getM_AttributeSetInstance_ID(),
							storage.getQtyOnHand(),storage.getDateMaterialPolicy(),loc[i].get_ID(), false);
					ma.saveEx();	
					qtyToDeliver = qtyToDeliver.subtract(storage.getQtyOnHand());
					if (log.isLoggable(Level.FINE)) log.fine( ma + ", QtyToDeliver=" + qtyToDeliver);		
				}
				if (qtyToDeliver.signum() == 0)
					break;
			}
		}
	}
}