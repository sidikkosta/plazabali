/**
 * 
 */
package com.unicore.base.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MWarehouse;
import org.compiere.process.DocOptions;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MUNSBonusClaim;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.unicore.util.AutoCompletion;
import com.uns.base.model.Query;
import com.uns.util.MessageBox;

/**
 * @author UNTA-Andy
 * 
 */
public class MInOut extends org.compiere.model.MInOut implements AutoCompletion, DocOptions
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6132125295464291402L;

	/**
	 * @param ctx
	 * @param M_InOut_ID
	 * @param trxName
	 */
	public MInOut(Properties ctx, int M_InOut_ID, String trxName)
	{
		super(ctx, M_InOut_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MInOut(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MInOut(MUNSBonusClaim bonusClaim)
	{
		super(bonusClaim.getCtx(), 0, bonusClaim.get_TrxName());
		setAD_Org_ID(bonusClaim.getAD_Org_ID());
		setAD_OrgTrx_ID(bonusClaim.getAD_Org_ID());
		setC_BPartner_ID(bonusClaim.getC_BPartner_ID());
		setC_BPartner_Location_ID(bonusClaim.getC_BPartner_Location_ID());
		setIsSOTrx(bonusClaim.isSOTrx());
		setC_DocType_ID();
		setDateAcct(bonusClaim.getDateAcct());
		setDateOrdered(bonusClaim.getDateDoc());
		setDateReceived(bonusClaim.getDateDoc());
		setPriorityRule(PRIORITYRULE_Low);
		setMovementType(isSOTrx() ? MOVEMENTTYPE_CustomerShipment : MOVEMENTTYPE_VendorReceipts);
		setDeliveryRule(DELIVERYRULE_Availability);
		setDeliveryViaRule(DELIVERYVIARULE_Delivery);
		setDescription("::Auto generate from bonus claim " + bonusClaim.getDocumentNo());
	}

	/**
	 * @param order
	 * @param C_DocTypeShipment_ID
	 * @param movementDate
	 */
	public MInOut(MOrder order, int C_DocTypeShipment_ID, Timestamp movementDate)
	{
		super(order, C_DocTypeShipment_ID, movementDate);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param invoice
	 * @param C_DocTypeShipment_ID
	 * @param movementDate
	 * @param M_Warehouse_ID
	 */
	public MInOut(MInvoice invoice, int C_DocTypeShipment_ID, Timestamp movementDate,
			int M_Warehouse_ID)
	{
		super(invoice, C_DocTypeShipment_ID, movementDate, M_Warehouse_ID);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param original
	 * @param C_DocTypeShipment_ID
	 * @param movementDate
	 */
	public MInOut(org.compiere.model.MInOut original, int C_DocTypeShipment_ID, Timestamp movementDate)
	{
		super(original, C_DocTypeShipment_ID, movementDate);
		// TODO Auto-generated constructor stub
	}

	public static MInOut[] getInOutByDate(Properties ctx, Timestamp dateFrom, Timestamp dateTo,
			boolean isSoTrx, String trxName)
	{
		String dateCheck;
		if (isSoTrx)
		{
			dateCheck = MInOut.COLUMNNAME_ShipDate;
		}
		else
		{
			dateCheck = MInOut.COLUMNNAME_DateReceived;
		}

		StringBuilder whereClauseFinal =
				new StringBuilder(dateCheck).append(" BETWEEN '").append(dateFrom)
				.append("' AND '").append(dateTo).append("'");

		List<MInOut> list =
				Query.get(ctx, UNSOrderModelFactory.EXTENSION_ID, MInOut.Table_Name,  
						whereClauseFinal.toString(), trxName).setOrderBy(
						dateCheck).list();

		//
		return list.toArray(new MInOut[list.size()]);
	}
	
	protected boolean beforeSave(boolean newRecord)
	{	
		if(!isSOTrx())
			return super.beforeSave(newRecord);
		
		if(getC_DocType_ID() != MDocType.getDocType(MDocType.DOCBASETYPE_MaterialDelivery))
			return super.beforeSave(newRecord);
		
		if(get_ValueAsInt("SequenceShipment") > 0)
			return super.beforeSave(newRecord);
		
		if(getC_BPartner_ID() > 0)
		{
			MBPartner bp = MBPartner.get(getCtx(), getC_BPartner_ID());
			int lastSequence = bp.get_ValueAsInt("LastSequenceShipment");
			bp.set_ValueOfColumn("LastSequenceShipment", lastSequence + 1);
			if(!bp.save())
			{
				log.saveError("Error", "Cannot update sequence shipment Business Partner");
				return false;
			}
			set_ValueOfColumn("SequenceShipment", lastSequence + 1);
		}
		
		return super.beforeSave(newRecord);
	}
	
	/**
	 * 
	 * @param isDiscountAfterTrx
	 * @return
	 */
	public BigDecimal getAmtOrder(boolean isDiscountAfterTrx)
	{
		BigDecimal amtOrder = Env.ZERO;
		if(getC_Order_ID() <= 0)
			return amtOrder;
		
		for(MInOutLine line : getLines(false))
		{
			amtOrder = amtOrder.add(line.getAmtOrder(isDiscountAfterTrx));
		}
		return amtOrder;
	}
	
	private MInOutLine[] m_lines = null;
	
	public MInOutLine[] getLines (boolean requery)
	{
		if (m_lines != null && !requery) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		List<MInOutLine> list = Query.get(getCtx(), UNSOrderModelFactory.EXTENSION_ID, MInOutLine.Table_Name
				, "M_InOut_ID=?", get_TrxName())
		.setParameters(getM_InOut_ID())
		.setOrderBy(MInOutLine.COLUMNNAME_Line)
		.list();
		//
		m_lines = new MInOutLine[list.size()];
		list.toArray(m_lines);
		return m_lines;
	}	//	getMInOutLines

	@Override
	public void resetDocStatus() {
		set_ValueNoCheck("DocStatus", "DR");
		set_ValueNoCheck("DocAction", "CO");
		set_ValueNoCheck("Processed", "N");
		disableModelValidation();
		saveEx();
		enableModelValidation();
	}

	@Override
	public String doAutoComplete() 
	{
		if (!isSOTrx() || getMovementType().equals(MOVEMENTTYPE_CustomerReturns))
		{
			try {
				if (!processIt(DOCACTION_Complete))
					return m_processMsg;
			}
			catch (Exception ex) {
				ex.printStackTrace();
				return ex.getMessage();
			}
			return null;
		}		
		
		Hashtable<String, BigDecimal> requiredMap = new Hashtable<String, BigDecimal>();
		
		int WHS_AirBatu = 1000062;
		//int WHS_HO = 1000061;
		int WHS_PVC = 1000063;
		//int LOC_AB = 
		int WHS_FROM = WHS_AirBatu;
		if (getM_Warehouse_ID() == WHS_AirBatu)
			;//return null;
		
		if (getM_Warehouse_ID() ==  WHS_AirBatu)
			WHS_FROM = WHS_PVC;
		
		MWarehouse whsFrom = MWarehouse.get(getCtx(), WHS_FROM);
		
		MInOutLine[] iolList = getLines(true);
		
		for (MInOutLine iol : iolList)
		{
			MProduct product = MProduct.get(getCtx(), iol.getM_Product_ID());
			
			if (!product.isActive()) {
				String shipreceipt = isSOTrx()? "Shipment" : "Receipt";
				return "Product [" + product + "] is inactivated but still used at " + shipreceipt + " #" + getDocumentNo();
			}
			
			if (product.isService())
				continue;
			
			String key = iol.getM_Product_ID() + "__" + iol.getM_Locator_ID() 
					+ "__" + iol.getM_AttributeSetInstance_ID() ;
			
			BigDecimal movementQty = requiredMap.get(key);
			if (movementQty == null) {
				movementQty = iol.getMovementQty();
			}
			else {
				movementQty = movementQty.add(iol.getMovementQty());
			}
			requiredMap.put(key, movementQty);
		}

		MMovement movement = null;
		Hashtable<String, BigDecimal> insufficientQtyMap = new Hashtable<String, BigDecimal>();
		
		for (String key : requiredMap.keySet())
		{
			String[] ids = key.split("__");
			int productID = Integer.valueOf(ids[0]);
			int locatorID = Integer.valueOf(ids[1]);
			int asiID = Integer.valueOf(ids[2]);
			
			BigDecimal availableQty = 
					MStorageOnHand.getQtyOnHandForLocator(productID, locatorID, asiID, get_TrxName());
			
			BigDecimal movementQty = requiredMap.get(key);
			
			BigDecimal differentQty = availableQty.subtract(movementQty);
			
			if (differentQty.signum() >= 0)
				continue;
			
			MLocator loc = MLocator.get(getCtx(), locatorID);
			
			differentQty = differentQty.abs();
			
			MStorageOnHand[] sohList = 
					MStorageOnHand.getOfProduct(p_ctx, getAD_Org_ID(), productID, get_TrxName());
			
			int orgFrom = getAD_Org_ID();
			boolean isFromOtherOrg = false;
			
			if(sohList.length == 0) 
			{
				MProduct product = MProduct.get(getCtx(), productID);

				if (product.getM_Product_Category_ID() == 1000189) // it's PVC
					whsFrom = MWarehouse.get(getCtx(), WHS_PVC);
				
				orgFrom = whsFrom.getAD_Org_ID();
				
				sohList = MStorageOnHand.getOfProduct(p_ctx, whsFrom.getAD_Org_ID(), productID, get_TrxName());
				isFromOtherOrg = true;
			}
			
			if(sohList.length == 0) {
				insufficientQtyMap.put(key, differentQty);
				continue;
			}
			
			while (true)
			{
				Hashtable<Integer, BigDecimal> locQtyMap = new Hashtable<Integer, BigDecimal>();
				
				for (MStorageOnHand soh : sohList)
				{
					if (soh.getQtyOnHand().signum() <= 0 || soh.getM_Locator_ID() == locatorID
							|| soh.getM_Locator_ID() == 1000244 || soh.getM_Locator_ID() == 1000248
							|| soh.getM_Locator_ID() == 1000250 || soh.getM_Locator_ID() == 1000245
							|| soh.getM_Locator_ID() == 1000249 || soh.getM_Locator_ID() == 1000246)
						continue;
					
					BigDecimal locQty = locQtyMap.get(soh.getM_Locator_ID());
					
					if (locQty == null)
						locQty = Env.ZERO;
					locQty = locQty.add(soh.getQtyOnHand());
					locQtyMap.put(soh.getM_Locator_ID(), locQty);
				}
				
				if (locQtyMap.size() == 0 && isFromOtherOrg)
					break;
				
				for (Integer availableLoc_ID : locQtyMap.keySet())
				{
					availableQty = (BigDecimal) locQtyMap.get(availableLoc_ID);
	
					if (availableLoc_ID == locatorID || availableQty.signum() <= 0)
						continue;
					
					if (movement == null)
					{
						String sql = "SELECT C_DocType_ID FROM C_DocType WHERE DocBaseType=? AND IsInTransit='N'";
						int movementDT_ID = DB.getSQLValueEx(get_TrxName(), sql, MDocType.DOCBASETYPE_MaterialMovement);
						
						movement = new MMovement(getCtx(), 0, get_TrxName());
						movement.setAD_Org_ID(orgFrom);
						movement.setMovementDate(getMovementDate());
						movement.setC_DocType_ID(movementDT_ID);
						movement.setDestinationWarehouse_ID(loc.getM_Warehouse_ID());
						movement.setSalesRep_ID(Env.getAD_User_ID(getCtx()));
						movement.setDescription("{**AtC**} Auto Move** for shipment of " + getDocumentNo());
						movement.saveEx();
					}
					
					BigDecimal qtyToMove = differentQty;
					
					if (availableQty.compareTo(qtyToMove) < 0)
						qtyToMove = availableQty;
					
					MMovementLine ml = new MMovementLine(movement);
					ml.setM_Product_ID(productID);
					ml.setM_Locator_ID(availableLoc_ID);
					ml.setM_LocatorTo_ID(locatorID);
					ml.setMovementQty(qtyToMove);
					ml.setConfirmedQty(qtyToMove);
					ml.saveEx();
					
					differentQty = differentQty.subtract(qtyToMove);
				}

				if (differentQty.signum() > 0 && !isFromOtherOrg)
				{
					MProduct product = MProduct.get(getCtx(), productID);
					if (product.getM_Product_Category_ID() == 1000189) // it's PVC
						whsFrom = MWarehouse.get(getCtx(), WHS_PVC);
					
					sohList = MStorageOnHand.getOfProduct(p_ctx, whsFrom.getAD_Org_ID(), productID, get_TrxName());
					orgFrom = whsFrom.getAD_Org_ID();
					isFromOtherOrg = true;
					if(sohList.length == 0)
						break;
				}
				else 
					break;
			}
			
			if (differentQty.compareTo(Env.ZERO) > 0)
				insufficientQtyMap.put(key, differentQty);
		}
		
		if (movement != null) {
//			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(movement, DocAction.ACTION_Complete);
//			if(pi.isError())
//				return pi.getSummary();
			if (!movement.processIt(DOCACTION_Complete))
				return "Failed when fulfilling input qty for shipment#" + getDocumentNo();
			movement.saveEx();
		}
		
		if (insufficientQtyMap.size() > 0)
		{
			//Create initial inventory.
			MInventory inventory = new MInventory((MWarehouse) getM_Warehouse(), get_TrxName());
			inventory.setC_DocType_ID(MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventoryImport));
			inventory.setMovementDate(getMovementDate());
			inventory.setDescription("{**AtC**} Auto initialized for Shipment#" + getDocumentNo());
			if (!inventory.save()) {
				log.saveError("Failed while initilizing stock for Shipment#" + getDocumentNo(), 
								"Failed while initilizing stock for Shipment#" + getDocumentNo());
				return "Failed while initilizing stock for Shipment#" + getDocumentNo();
			}
			
			for (String key : insufficientQtyMap.keySet())
			{
				String[] ids = key.split("__");
				int M_Product_ID = Integer.valueOf(ids[0]);
				int locatorID = Integer.valueOf(ids[1]);
				//int asiID = Integer.valueOf(ids[2]);
				
				BigDecimal requiredQty = insufficientQtyMap.get(key);
				
				MInventoryLine il = 
						new MInventoryLine(inventory, locatorID, M_Product_ID, 0, Env.ZERO, requiredQty, Env.ZERO);
				int M_PriceList_ID_1 = 1000101; // Harga Pembelian
				int M_PriceList_ID_2 = 1000099; // Harga Toko.
				int M_PriceList_ID_3 = 1000104; // PL Auto Initial Purposes Only.
				int M_PriceList_ID_4 = 1000100; // Harga Konsumen.
				Timestamp dateDoc = getMovementDate();
				
				String sql = 
						"SELECT pp.PriceList FROM M_ProductPrice pp "
						+ " INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
						+ " INNER JOIN M_PriceList pl ON plv.M_PriceList_ID=pl.M_PriceList_ID "
						+ " WHERE pp.M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
						+ " ORDER BY plv.ValidFrom DESC";
				
				BigDecimal priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_1, dateDoc);				
				BigDecimal percentage = Env.ONE;
				
				if (priceList == null || priceList.signum() <= 0)
				{
					String sqlCost = "SELECT CurrentCostPrice FROM M_Cost WHERE M_Product_ID=? AND M_CostElement_ID=1000014";
					priceList = DB.getSQLValueBDEx(get_TrxName(), sqlCost, M_Product_ID);
				}
					
				if (priceList == null || priceList.signum() <= 0) 
				{ 
					// Get it from previous initial stock.
					MInventoryLine invLine = 
							new Query(getCtx(), MInventoryLine.Table_Name, 
									"QtyCount>0 AND NewCostPrice>0 AND M_Product_ID=?", get_TrxName())
							.setParameters(M_Product_ID).first();
					if (invLine != null)
						priceList = invLine.getNewCostPrice().divide(invLine.getQtyCount(), 2, BigDecimal.ROUND_HALF_UP);
					percentage = Env.ONE;
				}
				
				if (priceList == null || priceList.signum() <= 0) {
					priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_2, dateDoc);
					percentage = BigDecimal.valueOf(0.75);
				}
				if (priceList == null || priceList.signum() <= 0) {
					priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_3, dateDoc);
					percentage = BigDecimal.valueOf(0.65);
				}
				if (priceList == null || priceList.signum() <= 0) {
					priceList = DB.getSQLValueBDEx(get_TrxName(), sql, M_Product_ID, M_PriceList_ID_4, dateDoc);
					percentage = BigDecimal.valueOf(0.55);
				}
				
				if (priceList == null || priceList.signum() <= 0)
					return "Cannot find price list when completing Shipping Doc #" + getDocumentNo() 
							+ " for product of " + MProduct.get(getCtx(), M_Product_ID);
				
				BigDecimal cost = priceList.multiply(percentage);
				
				BigDecimal totalCost = cost.multiply(requiredQty);
				
				il.setNewCostPrice(totalCost);
				if (!il.save()) {
					String msg = "Error while saving initial inventory line for product of " 
								+ MProduct.get(getCtx(), M_Product_ID);
					log.saveError(msg, msg);
					return msg;
				}				
			}

//			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(inventory, DocAction.ACTION_Complete);
//			if(pi.isError())
//				return pi.getSummary();
			if (!inventory.processIt(DOCACTION_Complete))
				return "Failed when fulfilling input qty for shipment#" + getDocumentNo();
			inventory.saveEx();
		}
		
		try {
//			ProcessInfo pi = MWorkflow.runDocumentActionWorkflow(this, DocAction.ACTION_Complete);
//			if(pi.isError())
//			{
//				return pi.getSummary() + " Shipment No: " + getDocumentNo();
//			}
			if (!processIt(DOCACTION_Complete))
				return m_processMsg;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return ex.getMessage();
		}
		return null;
	}
	
	@Override
	public int customizeValidActions(String docStatus, Object processing, String orderType, String isSOTrx,
			int AD_Table_ID, String[] docAction, String[] options, int index) {
		
		return super.customizeValidActions(docStatus, processing, orderType, isSOTrx,
				AD_Table_ID, docAction, options, index);
	}
	
	@Override
	protected boolean afterDelete(boolean success) {
		
		// unlink m_inoutline_id on UnnoticedMReceiptLine
		String sql = "UPDATE UNS_UnnoticedMReceiptLine SET M_InOutLine_ID = null"
				+ " WHERE UNS_UnnoticedMReceipt_ID = (SELECT UNS_UnnoticedMReceipt_ID"
				+ " FROM UNS_UnnoticedMReceipt WHERE M_InOut_ID = ? AND"
				+ " DOcStatus NOT IN ('RE','VO'))";
		
		int ok = DB.executeUpdate(sql, getM_InOut_ID(), get_TrxName());
		if(ok < 0)
			throw new AdempiereException("Could not update uns_unnoticedmreceiptline");
		
		//unlink m_inou_id on unnoticedmreceipt
		sql = "UPDATE UNS_UnnoticedMReceipt SET M_InOut_ID = null,"
				+ " NoticeStatus = 'WR' WHERE M_InOut_ID = ? AND"
				+ " DOcStatus NOT IN ('RE','VO')";
		ok = DB.executeUpdate(sql, getM_InOut_ID(), get_TrxName());
		if(ok < 0)
			throw new AdempiereException("Could not update uns_unnoticedmreceipt");
		 
		return super.afterDelete(success);
	}
	
	@Override
	public String prepareIt() {
	
		if(!checkQtyInOutLine())
			throw new AdempiereException("Transaction Cancelled");
		
		return super.prepareIt();
	}
	
	public boolean checkQtyInOutLine()
	{
	
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG('#LineNo:' || il.Line || '_SKU:' || p.value) , ' ; <br> ') FROM M_InOutLine il"
				+ " INNER JOIN M_Product p ON p.M_Product_ID = il.M_Product_ID"
				+ " WHERE il.M_InOut_ID = ? AND il.QtyEntered = 0";
		String val = DB.getSQLValueString(get_TrxName(), sql, getM_InOut_ID());
		
		if(!Util.isEmpty(val, true))
		{
			int retVal = MessageBox.showMsg(getCtx(), getProcessInfo(), "There was Line with Qty 0. <br>"+ val + "Do you want to delete it"
					+ " and continue process complete?."
					, "Check Qty on Line", MessageBox.YESNO, MessageBox.ICONQUESTION);
			if (MessageBox.RETURN_YES == retVal)
			{
				MInOutLine[] lines =getLines(false);
				for(int i=0; i<lines.length; i++)
				{
					if(lines[i].getQtyEntered().signum() == 0)
						lines[i].deleteEx(true);
				}
			}
			else
			{
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public boolean voidIt() {
		
		//check invoice first
		String msg = checkInvoice();
		if(msg != null)
			throw new AdempiereException(msg);
		
		return super.voidIt();
	}
	
	@Override
	public boolean reverseAccrualIt() {
		
		//check invoice first
		String msg = checkInvoice();
		if(msg != null)
			throw new AdempiereException(msg);
		
		return super.reverseAccrualIt();
	}
	
	@Override
	public boolean reverseCorrectIt() {
		
		//check invoice first
		String msg = checkInvoice();
		if(msg != null)
			throw new AdempiereException(msg);
		
		return super.reverseCorrectIt();
	}
	
	public String checkInvoice()
	{
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DISTINCT(iv.DocumentNo)), ' ; <br>'::character varying)"
				+ " FROM C_Invoice iv"
				+ " INNER JOIN C_InvoiceLine ivl ON ivl.C_Invoice_ID = iv.C_Invoice_ID"
				+ " INNER JOIN M_InOutLine iol ON iol.M_InOutLine_ID = ivl.M_InOutLine_ID"
				+ " WHERE iol.M_InOut_ID = ? AND iol.IsActive = 'Y' AND iv.DocStatus NOT IN ('RE','VO')";
		String val = DB.getSQLValueString(get_TrxName(), sql, getM_InOut_ID());
		
		if(!Util.isEmpty(val, true))
			return "There was Invoice not void, please void it first.<br> Document No Invoice : <br>"+val;
		
		return null;
	}
}