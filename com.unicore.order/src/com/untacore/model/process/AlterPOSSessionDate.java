/**
 * 
 */
package com.untacore.model.process;

import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.model.MUNSCashReconciliation;
import com.unicore.model.MUNSEDCReconciliation;
import com.unicore.model.MUNSPOSRecap;
import com.unicore.model.MUNSPOSSession;
import com.unicore.model.MUNSPOSTrx;
import com.unicore.model.MUNSPOSTrxLine;
import com.uns.util.UNSApps;


/**
 * @author AzHaidar
 *
 */
public class AlterPOSSessionDate extends SvrProcess 
{
	private int			m_POSTerminal_ID;
	private	String		m_NewShift;
	private Timestamp	m_PrevShiftDate;
	private Timestamp	m_NewShiftDate;
	
	/**
	 * 
	 */
	public AlterPOSSessionDate() {
	}
	
	private String location 	= UNSApps.SERVER_LOCATION;
	private String main			= UNSApps.SERVER_MAIN;
	private String type		= UNSApps.SERVER_TYPE;

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			if (param.getParameterName().equals("NewShift")) {
				m_NewShift = param.getParameterAsString();
			}
			else if (param.getParameterName().equals("NewShiftDate")) {
				m_NewShiftDate = param.getParameterAsTimestamp();
			}
		}		
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		/**
		String sql = "SELECT UNS_POS_Session_ID "
				+ "FROM UNS_POS_Session "
				+ "WHERE UNS_POSTerminal_ID=? AND Shift=? AND DateAcct=? AND DocumentNo=?";
		
		int UNS_POS_Session_ID = DB.getSQLValue(get_TrxName(), sql, 
				m_POSTerminal_ID, m_PrevShift, m_PrevShiftDate, m_DocumentNo);
		
		if (UNS_POS_Session_ID <= 0) {
			return "Cannot find POS Session with specified parameters.";
		}
		**/
		MUNSPOSSession session = new MUNSPOSSession(getCtx(), getRecord_ID(), get_TrxName());
		
		if (session.get_ID() <= 0)
			throw new AdempiereException("Wrong POS Session");
		m_POSTerminal_ID = session.getUNS_POSTerminal_ID();
		m_PrevShiftDate = session .getDateAcct();
		int UNS_POS_Session_ID = session.get_ID();
		
		String sql = "SELECT 1 FROM UNS_SalesReconciliation "
				+ "WHERE DateTrx=? "
				+ "	AND C_BP_Group_ID=(SELECT C_BP_Group_ID FROM C_BPartner "
				+ "				WHERE C_BPartner_ID=(SELECT Store_ID FROM UNS_POSTerminal "
				+ "								WHERE UNS_POSTerminal_ID=?))";
		
		int reconciled = DB.getSQLValueEx(get_TrxName(), sql, m_PrevShiftDate, m_POSTerminal_ID);
		
		if (reconciled > 0)
		{
			if(type.equals(main))
				throw new AdempiereException("Cannot modify POS transaction. It's been reconciled.");
			else if(type.equals(location))
			{
				if(session.getDocStatus().equals("CO")
						|| session.getDocStatus().equals("CL"))
				{
					MUNSCashReconciliation.createFrom(session, true);
					MUNSEDCReconciliation.createFrom(session, true);
					session.setTotalSalesB1(session.getTotalSalesB1().negate());
					session.setTotalDiscAmt(session.getTotalDiscAmt().negate());
					session.setTotalServiceCharge(session.getTotalServiceCharge().negate());
					session.setTotalShortCashierAmt(session.getTotalShortCashierAmt().negate());
					session.setTotalEstimationShortCashierAmt(session.getTotalEstimationShortCashierAmt().negate());
					session.setTotalTaxAmt(session.getTotalTaxAmt().negate());
					session.setVoucherAmt(session.getVoucherAmt().negate());
					session.setReturnAmt(session.getReturnAmt().negate());
					MUNSPOSRecap.createFrom(session);
				}
			}
		}
		
		
		String sqlUpdate = "UPDATE UNS_POS_Session SET Shift=?, DateAcct=? WHERE UNS_POS_Session_ID=?";
		
		int count = DB.executeUpdateEx(sqlUpdate, 
				new Object[]{m_NewShift, m_NewShiftDate, UNS_POS_Session_ID}, 
				get_TrxName());
		
		StringBuffer msg = new StringBuffer("POS Session updated#" + count + "; ");
		
		sqlUpdate = "UPDATE UNS_POSTrx SET DateAcct=?, DateDoc=? WHERE UNS_POS_Session_ID=?";
		
		count = DB.executeUpdateEx(sqlUpdate, 
				new Object[]{m_NewShiftDate, m_NewShiftDate, UNS_POS_Session_ID}, 
				get_TrxName());
		
		msg.append("POSTrx updated#" + count + "; ");
		
		sqlUpdate = "UPDATE UNS_POSPayment SET DateAcct=? WHERE UNS_POSTrx_ID IN "
				+ "		(SELECT UNS_POSTrx_ID FROM UNS_POSTrx WHERE UNS_POS_Session_ID=?)";
	
		count = DB.executeUpdateEx(sqlUpdate, 
				new Object[]{m_NewShiftDate, UNS_POS_Session_ID}, 
				get_TrxName());
		
		msg.append("POSPayment updated#" + count + "; ");
		
		if(type.equals(location))
		{
			MUNSPOSTrx[] trx = session.getLines(true);
			
			for(int i=0;i<trx.length;i++)
			{
				trx[i].setGrandTotal(trx[i].getGrandTotal().negate());
				trx[i].setServiceCharge(trx[i].getServiceCharge().negate());
				trx[i].setTaxAmt(trx[i].getTaxAmt().negate());
				trx[i].setDiscountAmt(trx[i].getDiscountAmt().negate());
				trx[i].setTotalAmt(trx[i].getTotalAmt().negate());
				MUNSPOSTrxLine[] pLines = trx[i].getLines(true);
				
				for(int j=0;j<pLines.length;j++)
				{
					if(pLines[j].getUNS_POSRecapLine_ID() <= 0)
						continue;
					pLines[j].setDiscountAmt(pLines[j].getDiscountAmt().negate());
					pLines[j].setQtyOrdered(pLines[j].getQtyOrdered().negate());
					pLines[j].setLineAmt(pLines[j].getLineAmt().negate());
					pLines[j].setLineNetAmt(pLines[j].getLineNetAmt().negate());
					pLines[j].setTaxAmt(pLines[j].getTaxAmt().negate());
					pLines[j].setServiceCharge(pLines[j].getServiceCharge().negate());
					pLines[j].setUNS_POSRecapLine_ID(-1);
				}

				String err = trx[i].createReconciliation(isReturn(trx[i]), true);
				if(null != err)
					throw new AdempiereException();
			}
			
			if(session.getDocStatus().equals("CO")
					|| session.getDocStatus().equals("CL"))
			{
				session.load(get_TrxName());
				MUNSCashReconciliation.createFrom(session, false);
				MUNSEDCReconciliation.createFrom(session, false);
				MUNSPOSRecap.createFrom(session);
			}
		}
		
		return msg.toString();
	} // doIt
	
	private boolean isReturn (MUNSPOSTrx trx)
	{
		String sql = "SELECT DocBaseType FROM C_DocType WHERE C_DocType_ID = ?";
		String docBaseType = DB.getSQLValueString(get_TrxName(), sql, trx.getC_DocType_ID());

		return MDocType.DOCBASETYPE_POSReturn.equals(docBaseType);
	}
}