package com.untacore.model.process;

import java.math.BigDecimal;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.unicore.model.MUNSWBTicketConfirm;

public class AddOrChangeQtyClaimed extends SvrProcess {

	public BigDecimal p_qty = Env.ZERO;
	public String p_reason = null;
	public MUNSWBTicketConfirm m_wbconfirm;
	
	public AddOrChangeQtyClaimed() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] param = getParameter();
		for(ProcessInfoParameter para : param)
		{
			String name = para.getParameterName();
			if(name.equals("QtyClaimed"))
				p_qty = para.getParameterAsBigDecimal();
			else if(name.equals("Reason"))
				p_reason = para.getParameterAsString();
			else
				throw new AdempiereException("Unknown Parameter Name :"+name);
		}
		
		if(getRecord_ID() > 0)
			m_wbconfirm = new MUNSWBTicketConfirm(getCtx(), getRecord_ID(), get_TrxName());
		else
			throw new AdempiereException("Only can access from window");

	}

	@Override
	protected String doIt() throws Exception {
		
		if(m_wbconfirm == null)
			throw new AdempiereException("Cannot found WB Confirm");
		
		if(p_qty == null || p_reason == null)
			throw new AdempiereException("Mandatory all parameter");
		
		m_wbconfirm.setQtyClaimed(p_qty);
		
		try {
			
			m_wbconfirm.saveEx();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		
		return null;
	}

}
