/**
 * 
 */
package com.untacore.model.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductPrice;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import java.util.Hashtable;

/**
 * @author Burhani Adam
 *
 */
public class GeneratePriceByPurchasing extends SvrProcess {
	
	private MPriceList m_list = null;
	private double m_percent = 0;
	private boolean m_isPriceAfterDisc; 
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			String nama = param.getParameterName();
			if(nama.equals("Percent"))
				m_percent = param.getParameterAsInt();
			else if(nama.equals("isPriceAfterDisc"))
				m_isPriceAfterDisc = param.getParameterAsBoolean();
		}
		
		m_list = new MPriceList(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() 
	{
		int created = 0;
		if(!m_list.isSOPriceList())
			return "This is purchasing price, can't use this process.";
		
		double upPercent = m_percent == 0 ? 0 : (m_percent / 100);
		
		String sql = "SELECT MAX(pv.ValidFrom), pp.M_Product_ID FROM M_PriceList_Version pv"
						+ " INNER JOIN M_ProductPrice pp ON pp.M_PriceList_Version_ID = pv.M_PriceList_Version_ID"
						+ " WHERE pv.M_PriceList_ID=? GROUP BY pp.M_Product_ID";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Hashtable<Timestamp, MPriceListVersion> mapVersion = new Hashtable<>();
		try
		{
			stmt = DB.prepareStatement(sql, get_TrxName());
			stmt.setInt(1, m_list.get_ID());
			rs = stmt.executeQuery();
			while(rs.next())
			{
				String getInvoice = "SELECT l.C_InvoiceLine_ID FROM C_InvoiceLine l"
										+ " INNER JOIN C_Invoice i ON i.C_Invoice_ID = l.C_Invoice_ID"
										+ " WHERE i.DocStatus IN ('CO', 'CL') AND i.isSOTrx = 'N' AND l.M_Product_ID = ?"
										+ " ORDER BY i.DateInvoiced DESC";
				int lineID = DB.getSQLValue(get_TrxName(), getInvoice, rs.getInt(2));
				if(lineID > 0)
				{
					String getDate = "SELECT DateInvoiced FROM C_Invoice WHERE C_Invoice_ID ="
							+ " (SELECT C_Invoice_ID FROM C_InvoiceLine WHERE C_InvoiceLine_ID=?)";
					java.sql.Timestamp dateInv = DB.getSQLValueTS(get_TrxName(), getDate, lineID);
					
					if(rs.getTimestamp(1).compareTo(dateInv) < 0)
					{						
						String getPrice = m_isPriceAfterDisc ? "PriceActual" : "PriceList";
						StringBuilder sb = new StringBuilder();
						sb.append("SELECT ").append(getPrice).append(" FROM C_InvoiceLine WHERE C_InvoiceLine_ID=?");
						BigDecimal price = DB.getSQLValueBD(get_TrxName(), sb.toString(), lineID);
						BigDecimal upAmount = price.multiply(BigDecimal.valueOf(upPercent));
						BigDecimal currentPrice = price.add(upAmount);
						
						String checkExists = "SELECT 1 FROM M_ProductPrice pp"
								+ " INNER JOIN M_PriceList_Version pv ON pv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID"
								+ " WHERE pv.ValidFrom = ? AND pp.M_Product_ID = ? AND pp.PriceList = ?";
						int count = DB.getSQLValue(get_TrxName(), checkExists, new Object[]{dateInv, rs.getInt(2), currentPrice});
						
						if(count > 1)
							continue;
						
						MPriceListVersion pv = mapVersion.get(dateInv);
						if(pv == null)
						{
							pv = new MPriceListVersion(getCtx(), 0, get_TrxName());
							pv.setM_PriceList_ID(m_list.get_ID());
							pv.setValidFrom(dateInv);
							pv.setName("::Auto Generated from Generate Price By Purchasing::");
							pv.saveEx();
			
							mapVersion.put(pv.getValidFrom(), pv);
						}
						
						MProductPrice pp = new MProductPrice(getCtx(), 0, 0, get_TrxName());
						pp.setM_PriceList_Version_ID(pv.get_ID());
						pp.setM_Product_ID(rs.getInt(2));
						pp.setPrices(currentPrice, currentPrice, currentPrice);
						pp.saveEx();
						created += 1;
					}
				}
			}
		}
		catch(SQLException e)
		{
			throw new AdempiereException(e.getMessage());
		}
		finally 
		{
			DB.close(rs, stmt);
		}
		return created + " record has created";
	}

}
