/**
 * 
 */
package com.untacore.model.process;

import java.util.HashMap;
import java.util.List;

import org.compiere.model.I_M_PriceList_Version;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;

import com.unicore.base.model.MOrder;
import com.unicore.base.model.MOrderLine;
import com.unicore.model.MUNSOrderContract;
import com.unicore.model.factory.UNSOrderModelFactory;
import com.uns.base.model.Query;

/**
 * @author AzHaidar
 *
 */
public class RenewPurchasingContract extends SvrProcess 
{
	private int		m_PriceList_ID; 
	
	/**
	 * 
	 */
	public RenewPurchasingContract() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			if (param.getParameterName().equals("M_PriceList_ID")) {
				m_PriceList_ID = param.getParameterAsInt();
			}
		}
		
		if (getRecord_ID() > 0)
			m_PriceList_ID = getRecord_ID();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		String whereClause = 
				"DocStatus='CO' AND M_PriceList_ID=? AND OrderContractType=? "
				+ " AND DateOrdered < (SELECT MAX(plv.ValidFrom) FROM M_PriceList pl "
				+ "		INNER JOIN M_PriceList_Version plv ON pl.M_PriceList_ID=plv.M_PriceList_ID "
				+ "		WHERE pl.M_PriceList_ID=?)";
		
		List<MOrder> NBOrderList = Query.get(
				getCtx(), UNSOrderModelFactory.EXTENSION_ID, MOrder.Table_Name, whereClause, get_TrxName())
				.setParameters(m_PriceList_ID, MOrder.ORDERCONTRACTTYPE_Non_BindingContractOrder, m_PriceList_ID)
				.setOrderBy(MOrder.COLUMNNAME_C_BPartner_ID + ", " + MOrder.COLUMNNAME_DateOrdered + " DESC")
				.list();
		
		I_M_PriceList_Version currentVersion = (I_M_PriceList_Version)
				new Query(getCtx(), I_M_PriceList_Version.Table_Name, "M_PriceList_ID=?", get_TrxName())
				.setParameters(m_PriceList_ID)
				.setOrderBy(I_M_PriceList_Version.COLUMNNAME_ValidFrom + " DESC")
				.first();
		
		int count = 0;
		int countNoOrderTerms = 0;
		
		HashMap<Integer, Object> vendorContractRenewed = new HashMap<Integer, Object>();
		
		for (MOrder nbo : NBOrderList)
		{
			if (vendorContractRenewed.get(nbo.getC_BPartner_ID()) != null)
				continue;
			
			MOrder newNBO = new MOrder(getCtx(), 0, get_TrxName());
			
			PO.copyValues(nbo, newNBO);
			
			newNBO.setDateAcct(currentVersion.getValidFrom());
			newNBO.setDateOrdered(currentVersion.getValidFrom());
			newNBO.setDatePromised(currentVersion.getValidFrom());
			newNBO.setC_DocTypeTarget_ID(nbo.getC_DocTypeTarget_ID());
			newNBO.setC_DocType_ID(nbo.getC_DocType_ID());
			newNBO.setOrderContractType(nbo.getOrderContractType());
			newNBO.setDocStatus(MOrder.DOCSTATUS_Drafted);
			newNBO.setDocAction(MOrder.DOCACTION_Complete);
			newNBO.setProcessed(false);
			newNBO.saveEx();
			
			MUNSOrderContract additionalTerms = Query.get(
					getCtx(), UNSOrderModelFactory.EXTENSION_ID, MUNSOrderContract.Table_Name, 
					"C_Order_ID=" + nbo.getC_Order_ID(), get_TrxName())
					.first();
			MUNSOrderContract newOrderTerms = null;
			if (additionalTerms != null)
			{
				newOrderTerms = new MUNSOrderContract(getCtx(), 0, get_TrxName());
				PO.copyValues(additionalTerms, newOrderTerms);
				newOrderTerms.setC_Order_ID(newNBO.getC_Order_ID());
				newOrderTerms.saveEx();
			}
			
			List<MOrderLine> lines = Query.get(
					getCtx(), UNSOrderModelFactory.EXTENSION_ID, MOrderLine.Table_Name, "C_Order_ID=?", get_TrxName())
					.setParameters(nbo.getC_Order_ID())
					.list();
			
			for (MOrderLine line : lines)
			{
				MOrderLine newLine = new MOrderLine(newNBO);
				newLine.setM_Product_ID(line.getM_Product_ID());
				newLine.setQty(line.getQtyEntered());
				newLine.setLine(line.getLine());
				newLine.setC_Tax_ID(line.getC_Tax_ID());
				newLine.saveEx();
			}
			
			if (newOrderTerms != null) 
			{
				if (!newNBO.processIt(MOrder.DOCACTION_Complete))
				{
					CLogger.retrieveErrorString(newNBO.getProcessMsg());
					return "Error while completing the new contract. [" + newNBO.getProcessMsg() + "]";
				}
				newNBO.saveEx();
			}
			else {
				countNoOrderTerms++;
			}
			
			vendorContractRenewed.put(newNBO.getC_BPartner_ID(), newNBO.getC_BPartner_Location_ID());
			
//			if (!nbo.processIt(MOrder.DOCACTION_Close))
//			{
//				return "Error while closing the old order (contract)";
//			}
//			nbo.saveEx();
			
			// close the other contract with the same vendor from the same location 
			// which is the status still 'completed'. 
//			for (int i=0; i < NBOrderList.size(); i++)
//			{
//				MOrder otherNBO = NBOrderList.get(i);
//				
//				if (otherNBO.getC_BPartner_ID() == nbo.getC_BPartner_ID() && 
//						otherNBO.getC_BPartner_Location_ID() == nbo.getC_BPartner_Location_ID() &&
//						otherNBO.getDocStatus().equals(MOrder.STATUS_Completed))
//				{
//					if (!otherNBO.processIt(MOrder.DOCACTION_Close))
//					{
//						return "Error while closing the old order (contract)";
//					}
//					otherNBO.saveEx();
//				}
//			}

			count++;
		}
		
		return count + " New-Contract created. There are " + countNoOrderTerms + 
				" were created without additional contract terms.";
	} // doIt

}
