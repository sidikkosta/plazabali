/**
 * 
 */
package com.untacore.model.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.model.MUNSCustomerInfo;

/**
 * @author Harry
 *
 */
public class ResolveTruncatedBoardingPass extends SvrProcess {

	private String m_DefaultAirPort = "CGK";
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		String sql = "SELECT ci.FlightNo, ci.BoardingPass, pos.DateTrx, ci.PassengerType, ci.UNS_CustomerInfo_ID "
					+ " FROM UNS_CustomerInfo ci"
					+ " INNER JOIN UNS_POSTrx pos ON ci.UNS_CustomerInfo_ID=pos.UNS_CustomerInfo_ID"
					+ " WHERE (ci.BoardingPass IS NULL OR LENGTH(TRIM(ci.BoardingPass)) < 35) "
					+ "		AND ci.BoardingPass NOT LIKE '% -#'";
		
		String msg = "Completed.";
		PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
		ResultSet rs = stmt.executeQuery();
		Hashtable<String, String> cachedFlightRoute = new Hashtable<String, String>();
		Hashtable<Integer, String> newBoardingPassMap = new Hashtable<Integer, String>();
		
		boolean continueUpdate = true;
		
		try {
			while(rs.next())
			{
				String fNo = rs.getString(1);
				String bp = rs.getString(2);
				Date flightDate = rs.getDate(3);
				String passengerType = rs.getString(4);
				int ci_ID = rs.getInt(5);
				
				if (fNo == null || fNo.trim().isEmpty())
					continue;
				
				String strPart1="";
				String strPart2="";
				
				for (int i=0; i < fNo.length(); i++)
				{
					if(fNo.length() > (i+1) && Character.isLetter(fNo.charAt(i+1)))
						continue;
					strPart1 = fNo.substring(0, i+1).trim();
					
					if (fNo.length() > i+1)
					{
						strPart2 = fNo.substring(i+1).trim();
						if(strPart2.charAt(0) == '0')
							strPart2 = strPart2.substring(1);
					}
					break;
				}
				
				String fNoTmp = strPart1 + strPart2;
				String fromToFlight = cachedFlightRoute.get(fNoTmp);
				
				if (fromToFlight == null)
				{
					String sqlSearch = "SELECT airport_from || airport_to FROM UNS_Flights WHERE Name=?";
					String airportFromTo = DB.getSQLValueStringEx(get_TrxName(), sqlSearch, fNoTmp);
					
					if (airportFromTo == null) {
						if (MUNSCustomerInfo.PASSENGERTYPE_Departure.equals(passengerType))
							airportFromTo = "*" + m_DefaultAirPort + "**";
						else
							airportFromTo = "**" + m_DefaultAirPort + "*";
					}
					
					fromToFlight = airportFromTo + strPart1;
					
					cachedFlightRoute.put(fNoTmp, fromToFlight);
				}
				
				String flightJulianDate = getJulianDate(flightDate);
				
				String newBPStr = (bp != null)? bp + " ": "";
				
				if (bp != null && bp.length() > 30)
				{
					newBPStr = newBPStr.substring(0, bp.length()-2); // include the last space.
				}
				
				String theFN = strPart2;
				for (int i = strPart2.length(); i < 4; i++)
				{
					theFN = "0" + theFN;
				}
				
				newBPStr += fromToFlight + " " + theFN + " " + flightJulianDate + " -#";
				
				newBoardingPassMap.put(ci_ID, newBPStr);
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
			msg = e.getMessage();
			continueUpdate = false;
		}
		finally {
			DB.close(rs, stmt);
		}
		
		if (continueUpdate)
		{
			for (int ci_ID : newBoardingPassMap.keySet())
			{
				String newBP = newBoardingPassMap.get(ci_ID);
				
				String updateBP = "UPDATE UNS_CustomerInfo SET BoardingPass=? WHERE UNS_CustomerInfo_ID=?";
				DB.executeUpdateEx(updateBP, new Object[]{newBP, ci_ID}, get_TrxName());
			}
		}
		
		return msg;
	} //doIt

	public static String getJulianDate(Date date) {
		StringBuilder sb = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return sb.append(String.format("%03d", cal.get(Calendar.DAY_OF_YEAR))).toString();
	} //getJulianDate
	
}