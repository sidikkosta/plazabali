package com.untacore.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Util;

import com.unicore.base.model.MInOut;

public class VoidInvoice extends SvrProcess {

	private String p_reason = null;
	private boolean replacement = false;
	private MInvoice oldInv = null;
	
	public VoidInvoice() {
		
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] param = getParameter();
		for(ProcessInfoParameter para : param)
		{
			String nama = para.getParameterName();
			if(nama.equals("Reason"))
				p_reason = para.getParameterAsString();
			else if(nama.equals("CreateReplacement"))
				replacement = para.getParameterAsBoolean();
			else
				throw new AdempiereException("Unknown paramater name :"+nama);
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Cannot void null document");
		else
			oldInv = new MInvoice(getCtx(), getRecord_ID(), get_TrxName());
	}

	@Override
	protected String doIt() throws Exception {
		
		if(oldInv == null)
			throw new AdempiereException("Cannot void null document");
		
		if(Util.isEmpty(p_reason, true))
			throw new AdempiereException("You have to explain the reason why void it");
		
		String msg = null;
		String descOldInv = oldInv.getDescription();
		String voNo = oldInv.getDocumentNo();
		
		try {
			
			if(replacement)
			{
				String oldDocNo = oldInv.getDocumentNo();
				
				for(int i=0;i<100;i++)
				{
					voNo = oldInv.getDocumentNo()+"_VO"+i;
					String sql = "SELECT 1 FROM C_Invoice WHERE DocumentNo = ?";
					int val = DB.getSQLValue(get_TrxName(), sql, voNo);
					if(val > 0)
						continue;
					
					oldInv.setDocumentNo(oldInv.getDocumentNo()+"^");
					oldInv.saveEx();
					
					break;
				}
				
				MInvoice newInv = createNewInv(oldDocNo);
				newInv.setDescription(descOldInv);
				newInv.saveEx();
				
				msg = "Invoice has been created "+newInv.getDocumentNo();
				
				//matching with PackingList Order
				
				String sql = "UPDATE UNS_PackingList_Order SET C_Invoice_ID = ?"
						+ " WHERE C_Invoice_ID = ?";
				int ok = DB.executeUpdateEx(sql, new Object[]{newInv.getC_Invoice_ID()
						, oldInv.getC_Invoice_ID()}, get_TrxName());
				if(ok < 0)
					throw new AdempiereException("Error when try to update Packing List Order");
				
				//update shipment
				String sqql = "SELECT M_InOut_ID FROM M_InOut WHERE"
						+ " C_Invoice_ID = ? AND DocStatus NOT IN ('VO','RE')";
				int inout_ID = DB.getSQLValue(get_TrxName(), sqql, oldInv.getC_Invoice_ID());
				if(inout_ID > 0)
				{
					MInOut inout = new MInOut(getCtx(), inout_ID, get_TrxName());
					if(inout != null)
					{
						inout.setC_Invoice_ID(newInv.getC_Invoice_ID());
						inout.saveEx();
					}
				}
			}
			else
			{
				msg = "Invoice has been void";
			}
			
			if(!oldInv.processIt(MInvoice.ACTION_Void))
				throw new AdempiereException(oldInv.getProcessMsg());
			
			if(Util.isEmpty(oldInv.getDescription(), true))
				oldInv.setDescription("**"+p_reason);
			else
				oldInv.setDescription(oldInv.getDescription().concat(" ** "+p_reason));
			
			oldInv.setDocumentNo(voNo);
			oldInv.saveEx();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		
		return msg;
	}
	
	public MInvoice createNewInv(String documentNo) {
		
		MInvoice invoice = MInvoice.copyFrom(oldInv, oldInv.getDateInvoiced(), oldInv.getDateAcct(),
				oldInv.getC_DocTypeTarget_ID(), oldInv.isSOTrx(), false, get_TrxName(), true, documentNo);
		
		try {
			
			invoice.saveEx();
			
			updateLine(invoice);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		
		return invoice;
	}
	
	public void updateLine(MInvoice newInv) {
		
		MInvoiceLine[] newLine = newInv.getLines(true);
		
		for(int i=0;i<newLine.length;i++)
		{
			String sql = "SELECT M_InOutLine_ID FROM C_InvoiceLine WHERE"
					+ " C_Invoice_ID = ? AND C_OrderLine_ID = ? AND isActive = 'Y'";
			int ioLine_ID = DB.getSQLValue(get_TrxName(), sql,
					oldInv.getC_Invoice_ID(), newLine[i].getC_OrderLine_ID());
			
			if(ioLine_ID <= 0)
				throw new AdempiereException("Cannot found inout line at invoice line");
			
			newLine[i].setM_InOutLine_ID(ioLine_ID);
			if(!newLine[i].save())
				throw new AdempiereException("Error when try to update new invoice line");
		}
	}
}
