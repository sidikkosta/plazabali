package com.untacore.model.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Util;

public class CreateInvoiceFromOrder extends SvrProcess {

	private MOrder m_order = null;
	private boolean autoComplete = false;
	
	public CreateInvoiceFromOrder() {
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] param = getParameter();
		
		for(ProcessInfoParameter para : param)
		{
			String name = para.getParameterName();
			if(name.equals("IsAutocomplete"))
				autoComplete = para.getParameterAsBoolean();
			else
				throw new AdempiereException("Unknown Parameter name : "+name);
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Only can access from window");
		else
			m_order = new MOrder(getCtx(), getRecord_ID(), get_TrxName());
	}

	@Override
	protected String doIt() throws Exception {
		
		if(m_order == null)
			throw new AdempiereException("Cannot found Order");
		
		String msg = null;
		
		String sql = "SELECT DocumentNo FROM C_Invoice WHERE C_Order_ID = ?"
				+ " AND DocStatus NOT IN ('VO','RE')";
		String docNo = DB.getSQLValueString(get_TrxName(), sql, getRecord_ID());
		if(!Util.isEmpty(docNo, true))
			return "Invoice has already created. Document No "+docNo;
		
		try {
			
			MInvoice invoice = m_order.createInvoice((MDocType) m_order.getC_DocType(), null, m_order.getDatePromised());
			if(invoice == null)
				throw new AdempiereException("Error when try to create Invoice. "+m_order.getProcessMsg());
			
			if(autoComplete)
			{
				if(!invoice.processIt(MInvoice.ACTION_Complete))
					throw new AdempiereException(invoice.getProcessMsg());
				
				invoice.saveEx();
			}
			
			msg = "Invoice has been created. Document No "+invoice.getDocumentNo();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AdempiereException(e.getMessage());
		}
		
		return msg;
	}

}
