/**
 * 
 */
package com.untacore.model.process;

import java.math.BigDecimal;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;

import com.unicore.base.model.MInvoice;
import com.unicore.base.model.MInvoiceLine;

import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;

import com.unicore.model.MUNSUnLoadingCostLine;

/**
 * @author AzHaidar
 *
 */
public class GenerateUnloadingCost extends SvrProcess 
{
	
	private MInvoice m_SrcInvoice = null;
	private String m_Message = null;
	private boolean m_isDeductInv = false;

	/**
	 * 
	 */
	public GenerateUnloadingCost() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("isDeductInvoice"))
				m_isDeductInv = param.getParameterAsString().equals("Y");
		}
		m_SrcInvoice = new MInvoice(getCtx(), getRecord_ID(), get_TrxName());
	
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (m_SrcInvoice == null)
			return "This process can only be running on an invoice document.";
		
		if(null != validationTicket(m_SrcInvoice.get_ID()))
				throw new AdempiereException(m_Message);
		
		String sqql = "SELECT DocumentNo FROM C_Invoice WHERE CN_Invoice_ID = ? AND DocStatus NOT IN ('VO','RE') ";
		String DocNo = DB.getSQLValueString(get_TrxName(), sqql, m_SrcInvoice.getC_Invoice_ID());
		if(!Util.isEmpty(DocNo, true))
			return "There is an invoice has been generated. Document No "+DocNo;
		
		String ssql = "SELECT PPhAfterUnloadingCost FROM UNS_Order_Contract WHERE C_Order_ID = ?";
		String pauc = DB.getSQLValueString(get_TrxName(), ssql, m_SrcInvoice.getC_Order_ID());
		
		if("Y".equals(pauc))
		{
			MInvoiceLine[] lines = m_SrcInvoice.getLines();
			for(MInvoiceLine line : lines)
			{
				BigDecimal qtyInvTicket = (BigDecimal) line.get_Value("QtyInvoiceTicket"); 
				BigDecimal discAmt = line.getQtyInvoiced().subtract(qtyInvTicket);
				discAmt = discAmt.multiply(line.getPriceList());
				line.setDiscountAmt(discAmt);
				if(!line.save())
					throw new AdempiereException("Erorr");
				
				String sql = "SELECT ucl.Amount FROM UNS_WeighbridgeTicket wb INNER JOIN UNS_UnloadingCost_Line ucl "
						+ "ON ucl.UNS_UnloadingCost_Line_ID = wb.UNS_UnloadingCost_Line_ID WHERE wb.C_InvoiceLine_ID = ?";
				BigDecimal UnloadingAmt = DB.getSQLValueBD(get_TrxName(), sql, line.getC_InvoiceLine_ID());
				line.setDiscountAmt(line.getDiscountAmt().add(UnloadingAmt));
				if(!line.save())
					throw new AdempiereException("Error");
			}
			
			return "Unloading cost success added to discount"; 
		}
		
		String sql = "SELECT C_Charge_ID FROM C_Charge WHERE Name=?";
		int unloadCharge_ID = DB.getSQLValueEx(get_TrxName(), sql, "FFB Unloading Cost");
		
		String description = "{**Generated from Invoice Document #" + m_SrcInvoice.getDocumentNo() + " **}";
		
		MInvoice unloadInv = new MInvoice(getCtx(), 0, get_TrxName());
		PO.copyValues(m_SrcInvoice, unloadInv);
		unloadInv.setClientOrg(
				m_SrcInvoice.getAD_Client_ID(), m_SrcInvoice.getAD_Org_ID());
		
		unloadInv.setDocumentNo(null);
		unloadInv.setC_DocTypeTarget_ID(m_isDeductInv ? MDocType.DOCBASETYPE_APCreditMemo : MDocType.DOCBASETYPE_ARInvoice);
		unloadInv.setCN_Invoice_ID(m_isDeductInv ? m_SrcInvoice.get_ID() : -1);
		unloadInv.setDescription(description);
		unloadInv.setDocStatus(MInvoice.DOCSTATUS_Drafted);
		unloadInv.setDocAction(MInvoice.DOCACTION_Complete);
		unloadInv.setProcessed(false);
		unloadInv.saveEx();
		
		BigDecimal totalUnloadingCost = Env.ZERO;
		MInvoiceLine[] invLines = m_SrcInvoice.getLines();
		
		for (MInvoiceLine iLine : invLines)
		{
//			sql = "SELECT wbt.NettoI, cost.Cost, cost.IsVolumeBased FROM UNS_WeighbridgeTicket wbt "
//					+ " INNER JOIN UNS_FFB_LoadUnload_Cost cost ON wbt.UNS_ArmadaType_ID=cost.UNS_ArmadaType_ID "
//					+ " WHERE wbt.C_InvoiceLine_ID=" + iLine.get_ID();
			
			sql = "SELECT UNS_WeighbridgeTicket_ID FROM UNS_WeighbridgeTicket WHERE C_InvoiceLine_ID = ?";
			int UNS_WeighbridgeTicket_ID = DB.getSQLValue(get_TrxName(), sql, iLine.get_ID());

			MUNSUnLoadingCostLine costLine = MUNSUnLoadingCostLine.getByTicket(getCtx(), UNS_WeighbridgeTicket_ID, get_TrxName());
			
//			sql = "SELECT lcl.UNS_UnLoading FROM UNS_UnLoadingCost_Line lcl WHERE lcl.UNS_WeighbridgeTicket_ID = ? AND"
//					+ " EXISTS (SELECT * FROM UNS_UnLoadingCost lc WHERE lc.UNS_UnLoadingCost_ID = lcl.UNS_UnLoadingCost_ID)";
			
//			PreparedStatement stmt = DB.prepareStatement(sql, get_TrxName());
//			ResultSet rs = null;
//			BigDecimal lineUnloadingCost = Env.ZERO;
//			
//			try {
//				rs = stmt.executeQuery();
//				
//				while (rs.next())
//				{
//					BigDecimal netto1 = rs.getBigDecimal(1);
//					BigDecimal cost = rs.getBigDecimal(2);
//					boolean isVolumeBased = rs.getString(3).equals("Y");
//					
//					if (isVolumeBased)
//						lineUnloadingCost = lineUnloadingCost.add(cost.multiply(netto1));
//					else
//						lineUnloadingCost = lineUnloadingCost.add(cost);
//				}
//			}
//			catch (SQLException ex) {
//				
//			}
//			finally {
//				DB.close(rs, stmt);
//			}
			
			MInvoiceLine unloadInvLine = new MInvoiceLine(unloadInv);
			unloadInvLine.setLine(iLine.getLine());
			unloadInvLine.setC_Charge_ID(unloadCharge_ID);
			unloadInvLine.setQty(1);
			unloadInvLine.setPrice(costLine.getAmount());
			unloadInvLine.setPriceList(costLine.getAmount());
			unloadInvLine.setC_Tax_ID(1000006); // Standard.
			unloadInvLine.setDescription(description + " --Line No#" + iLine.getLine());
			unloadInvLine.setProcessed(false);
			unloadInvLine.saveEx();
			
			String upTicket = "UPDATE UNS_WeighbridgeTicket SET UnLoadInvoiceLine_ID=? WHERE UNS_WeighbridgeTicket_ID=?";
			boolean success = DB.executeUpdate(upTicket, new Object[]{unloadInvLine.get_ID(), UNS_WeighbridgeTicket_ID},
								false, get_TrxName()) >= 0 ? true : false;
			if(!success)
				throw new AdempiereException("Failed when trying update ticket");
			
			totalUnloadingCost = totalUnloadingCost.add(costLine.getAmount());
		}
		
		return "Unloading cost generated #" + unloadInv.getDocumentNo() + " #Rp" + totalUnloadingCost.setScale(2);
	}
	
	public String validationTicket(int C_Invoice_ID)
	{
		m_Message = null;
		
		String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(wbt.DocumentNo), '; ') FROM UNS_WeighbridgeTicket wbt"
				+ " WHERE wbt.C_InvoiceLine_ID IN (SELECT C_InvoiceLine_ID FROM C_InvoiceLine WHERE C_Invoice_ID = ?)"
				+ " AND NOT EXISTS (SELECT * FROM UNS_UnLoadingCost_Line line WHERE wbt.UNS_WeighbridgeTicket_ID"
				+ " = line.UNS_WeighbridgeTicket_ID AND EXISTS (SELECT * FROM UNS_UnLoadingCost lc"
				+ " WHERE lc.DocStatus IN ('CO', 'CL') AND lc.UNS_UnLoadingCost_ID = line.UNS_UnLoadingCost_ID))";
		String listDocNo = DB.getSQLValueString(get_TrxName(), sql, C_Invoice_ID);
		if(!Util.isEmpty(listDocNo, true))
			m_Message = "ticket not found in unloading cost (" + listDocNo + ")";
		
		return m_Message;
	}
}