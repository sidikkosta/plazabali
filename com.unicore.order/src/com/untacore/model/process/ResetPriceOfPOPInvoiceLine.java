/**
 * 
 */
package com.untacore.model.process;

import java.math.BigDecimal;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.Util;

import com.unicore.base.model.MInvoiceLine;

/**
 * @author AzHaidar
 *
 */
public class ResetPriceOfPOPInvoiceLine extends SvrProcess 
{
	private int				m_InvoiceLine_ID;
	private BigDecimal		m_NewPrice;
	private String			m_Reason;	
	
	/**
	 * 
	 */
	public ResetPriceOfPOPInvoiceLine() {
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter param : params)
		{
			if (param.getParameterName().equals("PriceList")) {
				m_NewPrice = param.getParameterAsBigDecimal();
			}
			else if (param.getParameterName().equals("Reason")) {
				m_Reason = param.getParameterAsString();
			}
		}
		
		m_InvoiceLine_ID = getRecord_ID();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (m_NewPrice == null || m_NewPrice.signum() <= 0)
			throw new AdempiereUserError("The new price must be greater than zero.");
		if (Util.isEmpty(m_Reason, true))
			throw new AdempiereUserError("The \"Reason\" of this new price is mandatory.");
		
		MInvoiceLine invLine = new MInvoiceLine(getCtx(), m_InvoiceLine_ID, get_TrxName());
		
		invLine.setPriceList(m_NewPrice);
		BigDecimal qtyInvTicket = (BigDecimal) invLine.get_Value("QtyInvoiceTicket");
		BigDecimal discountAmt = invLine.getQtyInvoiced().subtract(qtyInvTicket);
		discountAmt = discountAmt.multiply(m_NewPrice);
		invLine.setDiscountAmt(discountAmt);
		
		invLine.addDescription(m_Reason);
		
		if(!invLine.save())
			throw new AdempiereException(
					"Failed when saving new price: " + CLogger.retrieveErrorString("NoErrorMsg."));
		
		return "Price reset successfully.";
	} // doIt

}
