/**
 * 
 */
package com.untacore.model.importer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.Adempiere;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MLocator;
import org.compiere.model.MMovementLineMA;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.jfree.util.Log;

import com.unicore.base.model.MMovement;
import com.unicore.base.model.MMovementLine;
import com.uns.importer.ImporterValidation;
import com.uns.model.process.SimpleImportXLS;

/**
 * @author Burhani Adam
 *
 */
public class UNSMovementPBImporter implements ImporterValidation
{
	
	protected Properties 	m_ctx = null;
	protected String		m_trxName = null;
	protected Sheet			m_sheet	= null;
	protected Hashtable<String, MMovement> m_MoveMap = new Hashtable<>();
	protected Hashtable<String, MWarehouse> m_WhsMap = new Hashtable<>();
	
	static final String COL_NoMove			= "NoMove";
	static final String COL_MoveDate		= "MoveDate";
	static final String	COL_From			= "From";
	static final String COL_To				= "To";
	static final String COL_BCCode			= "BCCode";
	static final String COL_ETBBCode		= "ETBBCode";
	static final String COL_ReqNo			= "ReqNo";
	static final String COL_CustomNo		= "CustomNo";
	static final String COL_BCDate			= "BCDate";

	/**
	 * 
	 */
	public UNSMovementPBImporter() {
		// TODO Auto-generated constructor stub
	}
	
	public UNSMovementPBImporter(Properties ctx, Sheet sheet, String trxName) {
		
		m_ctx = ctx;
		m_trxName = trxName;
		m_sheet = sheet;
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		MMovementLine line = (MMovementLine) po;
		line.setEventImport(true);
		MWarehouse whsFrom = m_WhsMap.get((String) freeColVals.get(COL_From));
		if(whsFrom == null)
		{
			String sql = "SELECT M_Warehouse_ID FROM M_Warehouse WHERE TRIM(Name) = TRIM(?)";
			int whsFromID = DB.getSQLValue(m_trxName, sql, freeColVals.get(COL_From));
			if(whsFromID <= 0)
				throw new AdempiereException("Not Found warehouse with name " + freeColVals.get(COL_From));
			whsFrom = MWarehouse.get(m_ctx, whsFromID);
			m_WhsMap.put((String) freeColVals.get(COL_From), whsFrom);
		}
		
		MWarehouse whsTo = m_WhsMap.get((String) freeColVals.get(COL_To));
		if(whsTo == null)
		{
			String sql = "SELECT M_Warehouse_ID FROM M_Warehouse WHERE TRIM(Name) = TRIM(?)";
			int whsToID = DB.getSQLValue(m_trxName, sql, freeColVals.get(COL_To));
			if(whsToID <= 0)
				throw new AdempiereException("Not Found warehouse with name " + freeColVals.get(COL_To));
			whsTo = MWarehouse.get(m_ctx, whsToID);
			m_WhsMap.put((String) freeColVals.get(COL_To), whsTo);
		}
		
//		String dateString = (String) freeColVals.get(COL_MoveDate);
		Timestamp date = (Timestamp) freeColVals.get(COL_MoveDate);
//		Timestamp dateMPolicy = (Timestamp) freeColVals.get(COL_BCDate);
		
		MMovement move = m_MoveMap.get((String) freeColVals.get(COL_NoMove));
		if(move == null)
		{
			String sql = "SELECT M_Movement_ID FROM M_Movement WHERE POReference = ? AND Description = ?"
					+ " AND DocStatus NOT IN ('CO', 'CL', 'VO', 'RE')";
			int existMove = DB.getSQLValue(m_trxName, sql, (String) freeColVals.get(COL_NoMove), "**::IMPORT(20190306)::**");
			if(existMove > 0)
				move = new MMovement(m_ctx, existMove, m_trxName);
			else
			{
				move = new MMovement(m_ctx, 0, m_trxName);
				move.setDocumentNo((String) freeColVals.get(COL_NoMove));
				move.setAD_Org_ID(whsFrom.getAD_Org_ID());
				move.setPOReference((String) freeColVals.get(COL_NoMove));
				move.setMovementDate(date);
				move.setM_Warehouse_ID(whsFrom.get_ID());
				move.setDestinationWarehouse_ID(whsTo.get_ID());
				move.setC_DocType_ID(1000585);
				move.setDescription("**::IMPORT(20190306)::**");
				move.setBCCode((String) freeColVals.get(COL_CustomNo));
				move.setBCDate((Timestamp) freeColVals.get(COL_BCDate));
				move.saveEx();
			}
			m_MoveMap.put((String) freeColVals.get(COL_NoMove), move);
		}
		
		org.compiere.model.MMovementLine existsLine = MMovementLine.get(m_ctx, move.get_ID(),
				MLocator.getDefault(whsFrom).get_ID(), MLocator.getDefault(whsTo).get_ID(),
					line.getM_Product_ID(), m_trxName);
		
		if(existsLine != null)
		{
			existsLine.setEventImport(true);
			existsLine.setMovementQty(existsLine.getMovementQty().add(line.getMovementQty()));
			existsLine.saveEx();
			MAttributeSetInstance asi = MAttributeSetInstance.get(m_ctx, m_trxName, (String) freeColVals.get(COL_BCCode),
					existsLine.getM_Product_ID());
			if(asi == null)
			{
				if(m_message.toString().equals(""))
					m_message.append("ASI not found for " + freeColVals.get(COL_BCCode).toString() + " on row " + currentRow);
				else
					m_message.append("\nASI not found for " + freeColVals.get(COL_BCCode).toString() + " on row " + currentRow);
			}
			else
			{
//				if(dateMPolicy == null)
					Timestamp dateMPolicy = asi.getCreated();
				MMovementLineMA ma = MMovementLineMA.addOrCreate(existsLine, asi.get_ID(), line.getMovementQty(),
						dateMPolicy, false);
				ma.saveEx();
			}
			return SimpleImportXLS.DONT_SAVE;
		}
		
		line.setM_Movement_ID(move.get_ID());
		line.setM_Locator_ID(MLocator.getDefault(whsFrom).get_ID());
		line.setM_LocatorTo_ID(MLocator.getDefault(whsTo).get_ID());
		line.setAD_Org_ID(move.getAD_Org_ID());
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow)
	{
		MMovementLine line = (MMovementLine) po;
		MAttributeSetInstance asi = MAttributeSetInstance.get(m_ctx, m_trxName, (String) freeColVals.get(COL_BCCode),
				line.getM_Product_ID());
		if(asi == null)
		{
			if(m_message.toString().equals(""))
				m_message.append("ASI not found for " + freeColVals.get(COL_BCCode).toString() + " on row " + currentRow);
			else
				m_message.append("\nASI not found for " + freeColVals.get(COL_BCCode).toString() + " on row " + currentRow);
		}
		else
		{
//			Timestamp dateMPolicy = (Timestamp) freeColVals.get(COL_BCDate);
//			if(dateMPolicy == null)
			Timestamp dateMPolicy = asi.getCreated();
			MMovementLineMA ma = MMovementLineMA.addOrCreate(line, asi.get_ID(), line.getMovementQty(),
					dateMPolicy, false);
			ma.saveEx();
		}
		return null;
	}

	private StringBuilder m_message = new StringBuilder();
	private String logDirectory;
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos)
	{
		if(!m_message.toString().equals(""))
		{
			logDirectory = Adempiere.getAdempiereHome().trim();
			if (!logDirectory.endsWith("/") && !logDirectory.endsWith("\\"))
				logDirectory+= File.separator;
			logDirectory = logDirectory + "log" + File.separator;

			//create packout folder if needed
			File logDirectoryFile = new File(logDirectory);
			if (!logDirectoryFile.exists()) {
				boolean success = logDirectoryFile.mkdirs();
				if (!success) {
					Log.info("Failed to create target directory. " + logDirectory);
				}
			}
			
			Timestamp date = new Timestamp(System.currentTimeMillis());
			String dateProcess = date.toString().replace(":", "-");
			
			File newFile = new File(logDirectory + "ImportMovement " + dateProcess + ".txt");
			
			FileWriter fr;
			try {
				fr = new FileWriter(newFile);
				BufferedWriter bw = new BufferedWriter(fr);
				
				for (String eachLine : m_message.toString().split("\n"))
				{
					bw.write(eachLine);
					bw.newLine();
				}
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTrxName(String trxName) {
		// TODO Auto-generated method stub
		
	}
}