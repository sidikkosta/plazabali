package com.untacore.model.importer;

import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.compiere.model.PO;
import org.compiere.util.DB;

import com.uns.importer.ImporterValidation;

public class UNSInventoryImporter implements ImporterValidation 
{

	private Sheet m_sheet = null;
	private Properties m_ctx = null;
	private String m_trxName = null;
	
	public UNSInventoryImporter (Properties ctx, Sheet sheet, String trxName)
	{
		this.m_ctx = ctx;
		this.m_sheet = sheet;
		this.m_trxName = trxName;
	}
	
	public UNSInventoryImporter() 
	{
		super ();
	}

	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		if (po.get_ID() > 0)
		{
			return null;
		}
		
		int AD_Org_ID = DB.getSQLValue(
				get_TrxName(), 
				"SELECT AD_Org_ID FROM M_Locator WHERE M_locator_ID = ?",
				po.get_ValueAsInt("M_Locator_ID"));
		po.set_ValueOfColumn("InventoryType", "D");
		po.setAD_Org_ID(AD_Org_ID);
		
		return null;
	}

	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		return null;
	}

	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) 
	{
		return null;
	}

	@Override
	public Hashtable<String, PO> getPOReferenceMap() 
	{
		return null;
	}

	@Override
	public void setTrxName(String trxName) 
	{
		this.m_trxName = trxName;
	}
	
	public Sheet getSheet ()
	{
		return this.m_sheet;
	}
	
	public String get_TrxName ()
	{
		return this.m_trxName;
	}
	
	public Properties getCtx ()
	{
		return this.m_ctx;
	}
}
