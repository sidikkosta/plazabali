/**
 * 
 */
package com.untacore.model.importer;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Properties;

import jxl.Sheet;

import org.compiere.model.MAcctSchema;
import org.compiere.model.MCost;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MOrg;
import org.compiere.model.MProduct;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.unicore.model.factory.UniCoreMaterialManagementModelFactory;
import com.uns.base.model.Query;
import com.uns.importer.ImporterValidation;

/**
 * @author AzHaidar
 *
 */
public class StockOpnameImporter implements ImporterValidation {

	private Properties m_Ctx = null;
	//private Sheet m_Sheet = null;
	private String m_trxName = null;
	
	final static String 	COL_MOVEMENTDATE = "MovementDate";
	final static String		COL_WHS	= "Warehouse";
	final static String		COL_LOC = "Locator";
	final static String		COL_PRODUCT = "M_Product_ID";
	final static String		COL_QTY_COUNT = "QtyCount";
	MAcctSchema 	m_as = null;
	
	final static int[] 	ORGS = new int[]{1000039, 1000041, 1000042};

	/**
	 * 
	 */
	public StockOpnameImporter(Properties ctx, Sheet sheet, String trxName)
	{
		m_Ctx = ctx;
		//m_Sheet = sheet;
		m_trxName = trxName;
		m_as = new MAcctSchema(m_Ctx, 1000010, m_trxName);
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#beforeSave(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String beforeSave(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) 
	{
		MInventoryLine il = (MInventoryLine) po;
		
		int whs_ID = (Integer) freeColVals.get(COL_WHS);
		MWarehouse whs = MWarehouse.get(m_Ctx, whs_ID);
		MProduct product = MProduct.get(m_Ctx, il.getM_Product_ID());

		String movementDateStr = (String) freeColVals.get(COL_MOVEMENTDATE);
		DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		Timestamp movementDate = null;
		try {
			movementDate = new Timestamp(dateFormat.parse(movementDateStr).getTime());
		}catch (Exception ex) {
			return ex.getMessage();
		}
		
		
		MCost cost = MCost.get(m_Ctx, il.getAD_Client_ID(), whs.getAD_Org_ID(), 
				il.getM_Product_ID(), 1000010, 1000010, 1000014, 0, m_trxName);
		
		BigDecimal costOfOtherSrc = null;
		if (cost == null || cost.getCurrentCostPrice().signum() == 0) {
			costOfOtherSrc = 
					getCostOf(whs.getAD_Client_ID(), whs.getAD_Org_ID(), 
							  il.getM_Product_ID(), movementDate, m_trxName);
		}
		
		int docType_ID = (cost == null)? 1000433 : 1000376; // Initial or physical.
		String noCostStr = "";
		if (cost == null) 
			noCostStr = (costOfOtherSrc == null)? "--InitNoCost--" : "--InitWithCost--";

		String key = noCostStr + "_" + docType_ID + "_" + whs.getValue();
		
		MInventory inv = (MInventory) poRefMap.get(key);
		MInventory costAdjustment = null;
		
		if (inv == null)
		{
			String whereClause = "MovementDate=? AND C_DocType_ID=? AND AD_Org_ID=? AND M_Warehouse_ID=?";
			
			if (noCostStr.length() > 0)
				whereClause += " AND Description LIKE '%" + noCostStr + "%'";
			
			inv = Query.get(m_Ctx, UniCoreMaterialManagementModelFactory.EXTENSION_ID, 
							MInventory.Table_Name, whereClause, m_trxName)
							.setParameters(movementDate, docType_ID, whs.getAD_Org_ID(), whs.get_ID())
							.first();
			if (inv == null)
			{
				inv = new MInventory(whs, m_trxName);
				
				inv.setC_DocType_ID(docType_ID);			
				inv.setMovementDate(movementDate);
				inv.setDescription(noCostStr + "{** Stock Opname @ 2015.11.29 @ Adjustment @ 2015.11.30 **}");
				inv.saveEx();
			}
			
			poRefMap.put(key, inv);
		}
		
		il.setM_Inventory_ID(inv.get_ID());
		
		if (cost != null && cost.getCurrentCostPrice().signum() > 0)
		{
			int costAdj_ID = 1000380;
			String keyCostAdjustment = costAdj_ID + "_" + whs.getValue();

			boolean productCostAdjustmentExists = false;
			
			costAdjustment = (MInventory) poRefMap.get(keyCostAdjustment);
			if (costAdjustment == null)
			{
				String whereClause = "MovementDate=? AND C_DocType_ID=? AND AD_Org_ID=?";
				costAdjustment = Query.get(m_Ctx, UniCoreMaterialManagementModelFactory.EXTENSION_ID, 
								MInventory.Table_Name, whereClause, m_trxName)
								.setParameters(movementDate, costAdj_ID, whs.getAD_Org_ID())
								.first();
				
				if (costAdjustment == null)
				{
					costAdjustment = new MInventory(m_Ctx, 0, m_trxName);
					
					costAdjustment.setAD_Org_ID(whs.getAD_Org_ID());
					costAdjustment.setC_DocType_ID(costAdj_ID);	
					costAdjustment.setCostingMethod(MInventory.COSTINGMETHOD_AverageInvoice);
					costAdjustment.setMovementDate(movementDate);
					costAdjustment.setDescription("{** Stock Opname @ 2015.11.29 @ Adjustment @ 2015.11.30 **}");
					costAdjustment.saveEx();
				}				
				poRefMap.put(keyCostAdjustment, costAdjustment);
			}
			else {
				String sql = "SELECT 1 FROM M_InventoryLine WHERE M_Inventory_ID=? AND M_Product_ID=?";
				int exists = DB.getSQLValueEx(m_trxName, sql, costAdjustment.get_ID(), product.get_ID());
				
				if (exists > 0)
					productCostAdjustmentExists = true;
			}
			
			if (!productCostAdjustmentExists)
			{
				MInventoryLine adjLine = new MInventoryLine(m_Ctx, 0, m_trxName);
				adjLine.setM_Inventory_ID(costAdjustment.get_ID());
				adjLine.setAD_Org_ID(whs.getAD_Org_ID());
				adjLine.setM_Product_ID(product.get_ID());
				adjLine.setNewCostPrice(cost.getCurrentCostPrice());
				adjLine.setCurrentCostPrice(cost.getCurrentCostPrice());
				adjLine.saveEx();
			}
		}
		
		BigDecimal qtyCount = il.getQtyCount();
		BigDecimal totalAdjusment = Env.ZERO;
		
		String movementDateClause = "MovementDate BETWEEN '2015-11-30 00:00:01' AND '2015-11-30 23:59:59'";
		
		/** Add with completed output/input on 30 Nop 2015 */
		String sql = "SELECT COALESCE(SUM(MovementQty), 0) FROM UNS_Production_Detail pd "
				+ " INNER JOIN UNS_Production p ON pd.UNS_Production_ID=p.UNS_Production_ID"
				+ " WHERE p.DocStatus IN ('CO', 'CL') AND p.Description LIKE '%{**AtC**}%' "
				+ "		AND pd.M_Product_ID=? AND pd.M_Locator_ID=? AND p." + movementDateClause;
		
		BigDecimal inoutQty = DB.getSQLValueBDEx(m_trxName, sql, il.getM_Product_ID(), il.getM_Locator_ID());
		totalAdjusment = totalAdjusment.add(inoutQty);
		
		sql = "SELECT COALESCE(SUM(MovementQty), 0) FROM M_InOutLine iol "
				+ " INNER JOIN M_InOut io ON iol.M_InOut_ID=io.M_InOut_ID "
				+ " WHERE io.DocStatus IN ('CO', 'CL') AND io.IsSOTrx='N' "
				+ "		AND io.Description LIKE '%{**AtC**}%' "
				+ "		AND iol.M_Product_ID=? AND iol.M_Locator_ID=? AND io." + movementDateClause;
		
		BigDecimal receiptQty = DB.getSQLValueBDEx(m_trxName, sql, il.getM_Product_ID(), il.getM_Locator_ID());
		totalAdjusment = totalAdjusment.add(receiptQty);
		
		BigDecimal adjustedQtyCount = qtyCount.add(totalAdjusment);
		
		/** Tambah dengan retur yang akan datang (setelah tanggal 29 Nop). **/
		sql = "SELECT COALESCE(SUM(iol.MovementQty), 0) FROM M_InOutLine iol "
				+ " INNER JOIN M_InOut io ON iol.M_InOut_ID=io.M_InOut_ID "
				+ " WHERE io.DocStatus IN ('CO', 'CL') AND io.MovementDate > '2015-11-29' "
				+ "		AND iol.M_Product_ID=? AND iol.M_Locator_ID=? AND io.MovementType='C+'";

		BigDecimal futureReturn = DB.getSQLValueBDEx(m_trxName, sql, il.getM_Product_ID(), il.getM_Locator_ID());
		if (futureReturn == null)
			futureReturn = Env.ZERO;
		
		adjustedQtyCount = adjustedQtyCount.add(futureReturn);
		
		if (adjustedQtyCount.signum() < 0)
			adjustedQtyCount = Env.ZERO;
		
		il.setQtyCount(adjustedQtyCount);
		
		if (inv.getC_DocType_ID() == 1000433 && costOfOtherSrc != null) { // Initial stock inventory.
			il.setNewCostPrice(costOfOtherSrc.multiply(il.getQtyCount()));
			return null;
		}
		
		/** Set the qtyBook **/
		sql = "SELECT SUM(QtyOnHand) FROM M_StorageOnHand WHERE M_Locator_ID=? AND M_Product_ID=?";
		BigDecimal qtyBook = DB.getSQLValueBDEx(m_trxName, sql, il.getM_Locator_ID(), il.getM_Product_ID());
		
		if (qtyBook == null)
			qtyBook = Env.ZERO;
		
		il.setQtyBook(qtyBook);
		
		return null;
	}

	/**
	 * 
	 * @param M_Product_ID
	 * @param dateDoc
	 * @param trxName
	 * @return
	 */
	BigDecimal getCostOf(int AD_Client_ID, int AD_Org_ID, int M_Product_ID, Timestamp dateDoc, String trxName)
	{
		MCost costOfOtherOrg = null;
		
		if (AD_Org_ID != ORGS[0]) {
			costOfOtherOrg = MCost.get(m_Ctx, AD_Client_ID, ORGS[0], 
					M_Product_ID, 1000010, 1000010, 1000014, 0, m_trxName);
		}
		
		BigDecimal possibleMaxCost = BigDecimal.valueOf(7000000);
		
		if ((costOfOtherOrg == null || costOfOtherOrg.getCurrentCostPrice().signum() == 0
			|| costOfOtherOrg.getCurrentCostPrice().compareTo(possibleMaxCost) > 0) 
				&& AD_Org_ID != ORGS[1]) {
			costOfOtherOrg = MCost.get(m_Ctx, AD_Client_ID, ORGS[1], 
					M_Product_ID, 1000010, 1000010, 1000014, 0, m_trxName);
		}
		if ((costOfOtherOrg == null || costOfOtherOrg.getCurrentCostPrice().signum() == 0
			|| costOfOtherOrg.getCurrentCostPrice().compareTo(possibleMaxCost) > 0) 
				&& AD_Org_ID != ORGS[2]) {
			costOfOtherOrg = MCost.get(m_Ctx, AD_Client_ID, ORGS[2], 
					M_Product_ID, 1000010, 1000010, 1000014, 0, m_trxName);
		}
		
		if (costOfOtherOrg != null && costOfOtherOrg.getCurrentCostPrice().signum() > 0 
				&& costOfOtherOrg.getCurrentCostPrice().compareTo(possibleMaxCost) <= 0) {
			return costOfOtherOrg.getCurrentCostPrice();
		}
		
		int M_PriceList_ID_1 = 1000101; // Harga Pembelian
		int M_PriceList_ID_2 = 1000099; // Harga Toko.
		int M_PriceList_ID_3 = 1000104; // PL Auto Initial Purposes Only.
		int M_PriceList_ID_4 = 1000100; // Harga Konsumen.
		
		String sql = 
				"SELECT pp.PriceList FROM M_ProductPrice pp "
				+ " INNER JOIN M_PriceList_Version plv ON pp.M_PriceList_Version_ID=plv.M_PriceList_Version_ID "
				+ " INNER JOIN M_PriceList pl ON plv.M_PriceList_ID=pl.M_PriceList_ID "
				+ " WHERE pp.M_Product_ID=? AND plv.M_PriceList_ID=? AND plv.ValidFrom <= ? "
				+ " ORDER BY plv.ValidFrom DESC";
		
		BigDecimal priceList = DB.getSQLValueBDEx(m_trxName, sql, M_Product_ID, M_PriceList_ID_1, dateDoc);				
		BigDecimal percentage = Env.ONE;
		
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(m_trxName, sql, M_Product_ID, M_PriceList_ID_2, dateDoc);
			percentage = BigDecimal.valueOf(0.75);
		}
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(m_trxName, sql, M_Product_ID, M_PriceList_ID_3, dateDoc);
			percentage = BigDecimal.valueOf(0.65);
		}
		if (priceList == null || priceList.signum() <= 0) {
			priceList = DB.getSQLValueBDEx(m_trxName, sql, M_Product_ID, M_PriceList_ID_4, dateDoc);
			percentage = BigDecimal.valueOf(0.55);
		}
		
		if (priceList == null || priceList.signum() <= 0)
		{
			if (costOfOtherOrg != null && costOfOtherOrg.getCurrentCostPrice().signum() > 0)
				return costOfOtherOrg.getCurrentCostPrice();
			else
				return null;
		}
		
		BigDecimal cost = priceList.multiply(percentage);

		return cost;
	}
	
	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveRow(java.util.Hashtable, org.compiere.model.PO, java.util.Hashtable, int)
	 */
	@Override
	public String afterSaveRow(Hashtable<String, PO> poRefMap, PO po,
			Hashtable<String, Object> freeColVals, int currentRow) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#afterSaveAllRow(java.util.Hashtable, org.compiere.model.PO[])
	 */
	@Override
	public String afterSaveAllRow(Hashtable<String, PO> poRefMap, PO[] pos) 
	{
		String csvOfInventoryID = null;
		
		for (PO invtPO : poRefMap.values())
		{
			MInventory inventory = (MInventory) invtPO;
			
			//MDocType dt = MDocType.get(m_Ctx, inventory.getC_DocType_ID());
			if (inventory.getC_DocType_ID() != 1000376)
				continue;
			
			opnameOtherProducts(inventory);
			
			if (csvOfInventoryID == null)
				csvOfInventoryID = String.valueOf(inventory.get_ID());
			else
				csvOfInventoryID += ", " + String.valueOf(inventory.get_ID());
		}
		
		String errorMsg = updateOpnamedMCost(csvOfInventoryID);
		
		return errorMsg;
	}
	
	/**
	 * 
	 * @param inventory
	 * @return
	 */
	void opnameOtherProducts(MInventory inventory)
	{
		String sql = "SELECT soh.AD_Org_ID, soh.M_Locator_ID, soh.M_Product_ID, SUM(soh.QtyOnHand) "
				+ " FROM M_StorageOnHand soh "
				+ " WHERE soh.M_Locator_ID IN (SELECT M_Locator_ID FROM M_Locator WHERE M_Warehouse_ID=?)"
				+ " 	AND NOT EXISTS (SELECT 1 FROM M_InventoryLine il "
				+ "			WHERE il.M_Locator_ID=soh.M_Locator_ID AND il.M_Product_ID=soh.M_Product_ID "
				+ "			AND il.M_Inventory_ID=?) "
				+ " GROUP BY soh.AD_Org_ID, soh.M_Locator_ID, soh.M_Product_ID "
				+ " ORDER BY soh.M_Locator_ID, soh.M_Product_ID";
		
		PreparedStatement st = null;
		ResultSet rs = null;

		try
		{
			st = DB.prepareStatement(sql, m_trxName);
			st.setInt(1, inventory.getM_Warehouse_ID());
			st.setInt(2, inventory.getM_Inventory_ID());
			rs = st.executeQuery();
			
			while (rs.next())
			{
				int M_Locator_ID = rs.getInt(2);
				int M_Product_ID = rs.getInt(3);
				BigDecimal qtyBook = rs.getBigDecimal(4);
				
				if (qtyBook.signum() == 0)
					continue;
				
				MInventoryLine newLine = 
						new MInventoryLine(inventory, M_Locator_ID, M_Product_ID, 0, qtyBook, Env.ZERO);
				newLine.set_TrxName(m_trxName);
				newLine.saveEx();

				sql = "SELECT 1 FROM M_Cost WHERE AD_Org_ID=? AND M_Product_ID=? AND M_CostElement_ID=1000014";
				boolean exists = DB.getSQLValueEx(m_trxName, sql, newLine.getAD_Org_ID(), M_Product_ID) > 0;
				
				if (!exists) {
					BigDecimal theCost = getCostOf(newLine.getAD_Client_ID(), newLine.getAD_Org_ID(), 
							M_Product_ID, inventory.getMovementDate(), m_trxName);
					MCost newCost = 
							new MCost(MProduct.get(m_Ctx, M_Product_ID), 0, m_as, newLine.getAD_Org_ID(), 1000014);
					newCost.setCurrentQty(qtyBook);
					newCost.setCurrentCostPrice(theCost);
					newCost.setCumulatedQty(qtyBook);
					newCost.setCumulatedAmt(qtyBook.multiply(theCost));
					newCost.saveEx();
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			DB.close(rs, st);
		}
	}
	
	/**
	 * 
	 */
	String updateOpnamedMCost(String csvOfInventoryID) 
	{
		String sql = "SELECT il.AD_Org_ID, il.M_Product_ID, SUM(il.QtyBook) FROM M_InventoryLine il "
				+ " WHERE il.M_Inventory_ID IN (" + csvOfInventoryID + ") "
				+ " GROUP BY il.AD_Org_ID, il.M_Product_ID";

		String errMsg = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		try
		{
			st = DB.prepareStatement(sql, m_trxName);
			rs = st.executeQuery();
			
			while (rs.next())
			{
				int AD_Org_ID = rs.getInt(1);
				int M_Product_ID = rs.getInt(2);
				BigDecimal qtyBook = rs.getBigDecimal(3);
				
				sql = "UPDATE M_Cost SET CurrentQty=? WHERE AD_Org_ID=? AND M_Product_ID=?";
				int count = DB.executeUpdateEx(sql, new Object[]{qtyBook, AD_Org_ID, M_Product_ID}, m_trxName);
				
				if (count <= 0) {
					errMsg = "Cannot update MCost for Org of " + MOrg.get(m_Ctx, AD_Org_ID) + " for Product of " +
							MProduct.get(m_Ctx, M_Product_ID);
					break;
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			DB.close(rs, st);
		}
		return errMsg;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#getPOReferenceMap()
	 */
	@Override
	public Hashtable<String, PO> getPOReferenceMap() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.uns.importer.ImporterValidation#setTrxName(java.lang.String)
	 */
	@Override
	public void setTrxName(String trxName) {
		m_trxName = trxName;
	}

}
