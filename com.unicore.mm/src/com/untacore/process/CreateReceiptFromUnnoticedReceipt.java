package com.untacore.process;

import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

import com.unicore.model.MUNSUnnoticedMReceipt;
import com.unicore.model.MUNSUnnoticedMReceiptLine;

public class CreateReceiptFromUnnoticedReceipt extends SvrProcess {

	private int p_Order_ID;
	private Timestamp p_MovementDate;
	private MUNSUnnoticedMReceipt m_unnoticed;
	private MOrder order;
	
	public CreateReceiptFromUnnoticedReceipt() {
		super();
	}

	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter para : params)
		{
			String name = para.getParameterName();
			if(name.equals("C_Order_ID"))
				p_Order_ID = para.getParameterAsInt();
			else if(name.equals("MovementDate"))
				p_MovementDate = para.getParameterAsTimestamp();
			else
				throw new AdempiereException("Unknown Parameter : "+name);
		}
		
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Only can acces from window");
		else
			m_unnoticed = new MUNSUnnoticedMReceipt(getCtx(), getRecord_ID(), get_TrxName());
	}

	@Override
	protected String doIt() throws Exception {
		
		order = new MOrder(getCtx(), p_Order_ID, get_TrxName());
		
		MInOut inout = new MInOut(getCtx(), 0, get_TrxName());
		
		inout.setAD_Org_ID(m_unnoticed.getAD_Org_ID());
		inout.setIsSOTrx(false);
		inout.setC_Order_ID(p_Order_ID);
		inout.setC_DocType_ID();
		inout.setMovementDate(p_MovementDate);
		inout.setDateAcct(p_MovementDate);
		inout.setC_BPartner_ID(order.getC_BPartner_ID());
		inout.setC_BPartner_Location_ID(order.getC_BPartner_Location_ID());
		
		String sql = "SELECT M_Warehouse_ID FROM M_Warehouse"
				+ " WHERE AD_Org_ID = ? AND isActive = 'Y'";
		int wrhouse_id = DB.getSQLValue(get_TrxName(), sql, inout.getAD_Org_ID());
		if(wrhouse_id > 0)
			inout.setM_Warehouse_ID(wrhouse_id);
		
		inout.setMovementType(MInOut.MOVEMENTTYPE_VendorReceipts);
		if(!inout.save())
			throw new AdempiereException("Error when try to save Material Receipt");
		
		m_unnoticed.setM_InOut_ID(inout.getM_InOut_ID());
		m_unnoticed.saveEx();
		
		String msg = createLine(inout);
		StringBuilder processMsg = new StringBuilder("Succes Create Receipt #Document No : ")
			.append(inout.getDocumentNo()).append(". ").append(msg);
		
		return processMsg.toString();
	}
	
	private String createLine(MInOut inout) {
		
		String msg = null;
		int count = 0;
		
		MUNSUnnoticedMReceiptLine[] lines = m_unnoticed.getLine();
		for(MUNSUnnoticedMReceiptLine line : lines)
		{
			if(line.getM_Product_ID() <= 0)
				continue;
			
			int orderLine_ID = 0;
			
			MOrderLine[] orderLines = order.getLines();
			
			for(MOrderLine orderLine : orderLines)
			{
				if(orderLine.getM_Product_ID() != line.getM_Product_ID())
					continue;

				orderLine_ID = orderLine.getC_OrderLine_ID();
				
				MInOutLine inoutLine = new MInOutLine(inout);
				inoutLine.setM_Product_ID(orderLine.getM_Product_ID());
				inoutLine.setC_UOM_ID(orderLine.getC_UOM_ID());
				inoutLine.setM_Locator_ID(line.getM_Locator_ID());
				inoutLine.setQty(line.getMovementQty());
				inoutLine.setM_AttributeSetInstance_ID(orderLine.getM_AttributeSetInstance_ID());
				inoutLine.setDescription(line.getDescription());
				inoutLine.setC_OrderLine_ID(orderLine_ID);
				
				if(!inoutLine.save())
					throw new AdempiereException("Error when try to create receipt line");
				
				line.setM_InOutLine_ID(inoutLine.getM_InOutLine_ID());
				line.saveEx();
				
				break;
			}
			
			if(orderLine_ID == 0)
			{
				String sql = "SELECT Concat(value,'_',name) FROM M_Product"
						+ " WHERE M_Product_ID = ?";
				String product = DB.getSQLValueString(get_TrxName(), sql, line.getM_Product_ID());
				
				throw new AdempiereException("Cannot found #"+product+" on Order Line");
			}
			
			count++;
		}
		
		msg = count+" line.";
		return msg;
	}
}
