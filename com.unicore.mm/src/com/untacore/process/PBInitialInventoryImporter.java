/**
 * 
 */
package com.untacore.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.Adempiere;
import org.compiere.model.MAttribute;
import org.compiere.model.MAttributeInstance;
import org.compiere.model.MAttributeSet;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MDocType;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MLocator;
import org.compiere.model.MWarehouse;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.compiere.model.MInventory;
import org.compiere.model.MProduct;

import com.unicore.model.MUNSInventoryLineCost;

/**
 * @author nurse
 *
 */
public class PBInitialInventoryImporter extends SvrProcess {

	private int COL_IDX = 0;
	private final int COLUMN_LOC = COL_IDX++;
	private final int COLUMN_SKU = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_TARIFF_HS = COL_IDX++;
	private final int COLUMN_ETBB = COL_IDX++;
	private final int COLUMN_ETBB_DATE = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_CAT = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_CLASS = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_BRAND = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_DESCRIPTION = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_SUPPL = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_ARTICLE_NO = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_UOM = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_BEGIN_BAL = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_TRANS_IN = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_RECEIPT = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_SALES = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_TRANS_OUT = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_RET_SUPPL = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_PACKING = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_ADJUSTMENT = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_RO = COL_IDX++;
	private final int COLUMN_END_BAL = COL_IDX++;
	@SuppressWarnings("unused")
	private final int COLUMN_RETAIL = COL_IDX++;
	private final int COLUMN_COST = COL_IDX++;
	private final int COLUMN_ORI_COST = COL_IDX++;
	private String p_fileName = null;
	private String p_delimiter = null;
	private List<PO> l_cache;
	private List<MProduct> l_product;
	private Timestamp p_dateCount = null;
	private int m_docTypeID = -1;
	private int p_currencyID = 0;
	private StringBuilder m_unresolved;
	
	/**
	 * 
	 */
	public PBInitialInventoryImporter() 
	{
		super ();
		l_product = new ArrayList<>();
		l_cache = new ArrayList<>();
		m_unresolved = new StringBuilder("");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if (params[i].getParameterName().equals("FileName"))
				p_fileName = params[i].getParameterAsString();
			else if (params[i].getParameterName().equals("Delimiter"))
				p_delimiter = params[i].getParameterAsString();
			else if (params[i].getParameterName().equals("DateCount"))
				p_dateCount = params[i].getParameterAsTimestamp();
			else if ("C_Currency_ID".equals(params[i].getParameterName()))
				p_currencyID = params[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter " + params[i].getParameterName());
		}
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (p_currencyID <= 0)
			throw new AdempiereException("Mandatory parameter Currency");
		if (Util.isEmpty(p_fileName, true))
			throw new AdempiereException("Mandatory parameter File Name");
		if (Util.isEmpty(p_delimiter, true))
			p_delimiter = "]";
		File file = new File(p_fileName);
		if (!file.exists())
			throw new AdempiereException("Could not find file " + p_fileName);
		if (m_docTypeID <= 0)
			m_docTypeID = MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventoryImport);
		String line;
		BufferedReader reader = null;
		try
		{
			processUI.statusUpdate("Load record from file");
			reader = new BufferedReader(new FileReader(file));
			List<Object[]> rows = new ArrayList<>();
			while ((line = reader.readLine()) != null)
			{
				String[] values = line.split(p_delimiter);
				Object[] row = new Object[COL_IDX];
				rows.add(row);
				for (int i=0; i<values.length; i++)
				{
					row[i] = values[i];
				}
			}
			processUI.statusUpdate("Total record to be process " + rows.size());
			importInventory(rows);
		}
		catch (Exception ex)
		{
			throw new AdempiereException(ex.getMessage());
		}
		finally
		{
			if (reader != null)
				reader.close();
		}

		String unresolved = m_unresolved.toString();
		if (!Util.isEmpty(unresolved, true))
		{
			String dir = Adempiere.getAdempiereHome().trim();
			if (!dir.endsWith("\\") && !dir.endsWith("/"))
				dir += File.separator;
			String fileName = "UnresolvedInitialInventory" + System.currentTimeMillis();
			fileName = dir + fileName;

			processUI.statusUpdate("Writing unresolved to file " + fileName);

			File out = new File(fileName);
			file.createNewFile();
			FileWriter fw = null;
			BufferedWriter bw = null;
			try
			{
				out.createNewFile();
				fw = new FileWriter(out);
				bw = new BufferedWriter(fw);
				bw.write(unresolved);
				bw.flush();
			}
			catch (IOException ex)
			{
				throw new AdempiereException(ex.getMessage());
			}
			finally
			{
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			}
			
			processUI.download(file);
		}
		
		return "File imported successfully";
	}
	
	private void importInventory (List<Object[]> rows) throws Exception
	{
		String msg = null;
		for (int i=1; i<rows.size(); i++)
		{
			processUI.statusUpdate("Processing " + i + " from " + rows.size());
			if ((msg = importInventory(rows.get(i))) != null)
				throw new AdempiereException(msg);
			if ((i % 100) == 0)
				DB.commit(true, get_TrxName());
		}
	}
	
	private void addUnresolved (Object[] row)
	{
		for (int i=0; i<row.length; i++)
		{
			m_unresolved.append(row[i]);
			if (i<(row.length-1))
				m_unresolved.append(p_delimiter);
		}
		m_unresolved.append("\n");
	}
	
	private String importInventory (Object[] row)
	{
		Object oo = row[COLUMN_LOC];
		if (oo == null)
		{
			addUnresolved(row);
			return null;
		}
		String loc = oo.toString().trim();
		oo = row[COLUMN_SKU];
		if (oo == null)
		{
			addUnresolved(row);
			return null;
		}
		String sku = oo.toString().trim();
		oo = row[COLUMN_ETBB];
		if (oo == null)
		{
			addUnresolved(row);
			return null;
		}
		String etbb = oo.toString().trim();
		if (etbb.length() < 8)
			etbb = String.format("%8s", etbb).replace(" ", "0");
		if (etbb.length() > 8)
			etbb = etbb.substring(0,8);
		oo = row[COLUMN_ETBB_DATE];
		if (oo == null)
		{
			addUnresolved(row);
			return null;
		}
		String etbbDateStr = oo.toString();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date d;
		try 
		{
			d = format.parse(etbbDateStr);
		}
		catch (ParseException e) 
		{
			return e.getMessage();
		}
		Timestamp etbbDate = new Timestamp(d.getTime());
		oo = row[COLUMN_END_BAL];
		if (oo == null)
		{
			addUnresolved(row);
			return null;
		}
		BigDecimal qtyCount = new BigDecimal(oo.toString().trim());
		if (qtyCount.signum() <= 0)
		{
			m_docTypeID = MDocType.getDocType(MDocType.DOCBASETYPE_MaterialPhysicalInventory);
		}
		
		MWarehouse wh = initWarehouse(loc);
		if (wh == null)
		{
			addUnresolved(row);
			return null;
		}
		MInventory inv = initInventory(wh);
		if (inv == null)
		{
			addUnresolved(row);
			return null;
		}
		
		MProduct product = initProduct(sku);
		if (product == null)
		{
			addUnresolved(row);
			return null;
		}
		
		MAttributeSetInstance instanceAttr = initAttrInstance (etbb, product, etbbDate);
		if (instanceAttr == null || instanceAttr.getM_AttributeSetInstance_ID() == 0)
		{
			addUnresolved(row);
			return null;
		}
		MLocator locator = wh.getDefaultLocator();
		if (locator == null)
		{
			addUnresolved(row);
			return null;
		}
		MInventoryLine line = MInventoryLine.get(inv, locator.get_ID(), product.get_ID(), 
				instanceAttr.getM_AttributeSetInstance_ID());
		if (line == null)
		{
			line = new MInventoryLine(
					inv, locator.get_ID(), product.get_ID(), instanceAttr.getM_AttributeSetInstance_ID(), 
					Env.ZERO, Env.ZERO);
			line.setInventoryType(MInventoryLine.INVENTORYTYPE_InventoryDifference);
		}
		
		line.setDescription(etbb);
		line.setQtyRealCount(qtyCount);
		line.setDateCount(p_dateCount);
		if (qtyCount.signum() > 0)
		{
			oo = row[COLUMN_COST];
			if (oo == null)
			{
				addUnresolved(row);
				return null;
			}
			BigDecimal costs = new BigDecimal(oo.toString().trim());
			line.setNewCostPrice(qtyCount.multiply(costs));
		}
		
		if (!line.save())
			return CLogger.retrieveErrorString("Failed when try to save Inventory Line " + product.getSKU() + etbb);
		
		if (qtyCount.signum() > 0)
		{
			oo = row[COLUMN_ORI_COST];
			if (oo == null)
			{
				addUnresolved(row);
				return null;
			}
			BigDecimal convertedCosts = new BigDecimal(oo.toString().trim());
			//static for 2 currency
			String sql = "SELECT C_Currency_ID FROM C_AcctSchema WHERE AD_Client_ID = ? AND C_Currency_ID <> ?";
			int currencyID = DB.getSQLValue(get_TrxName(), sql, getAD_Client_ID(), inv.getC_Currency_ID());
			if (currencyID <= 0)
			{
				addUnresolved(row);
				return null;
			}
			MUNSInventoryLineCost costLine = MUNSInventoryLineCost.getCreate(line, currencyID);
			costLine.setCurrentCostPrice(convertedCosts);
			if (!costLine.save())
				return CLogger.retrieveErrorString("Failed when try to save Cost Line");
		}
		
		return null;
	}
	
	private MAttributeSetInstance initAttrInstance (String etbb, MProduct product, Timestamp etbbDate)
	{
		MAttributeSetInstance instance = null;
		String sql = "SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance"
				+ " WHERE Description = ? AND M_AttributeSet_ID = ?";
		int asiID = DB.getSQLValue(get_TrxName(), sql, etbb, product.getM_AttributeSet_ID());
		if (asiID > 0)
			instance = new MAttributeSetInstance(getCtx(), asiID, get_TrxName());
		else
		{
			instance = MAttributeSetInstance.create(getCtx(), product, get_TrxName());
			MAttributeSet attrSet = instance.getMAttributeSet();
			MAttribute[] attrs = attrSet.getMAttributes(true);
			String bcCode = etbb.substring(0, 5);
			String year = etbb.substring(5, 6);
			String port = etbb.substring(6,8);
			
			for (int i=0; i<attrs.length; i++)
			{
				String val = null;
				if (attrs[i].getName().equalsIgnoreCase("BC Code"))
				{
					val = bcCode;
				}
				else if (attrs[i].getName().equalsIgnoreCase("Year"))
				{
					val = year;
				}
				else if (attrs[i].getName().equalsIgnoreCase("Port"))
				{
					val = port;
				}
				else
					continue;
				MAttributeInstance attInstance = MAttributeInstance.getNoSave(
						getCtx(), instance.getM_AttributeSetInstance_ID(),
						attrs[i].getM_Attribute_ID(), get_TrxName());
				if (null == attInstance)
					continue;

				attInstance.setValue(val);
				attInstance.saveEx();
			}
			
			instance.setDescription();
			instance.saveEx();
		}
		
		instance.setCreated(etbbDate);
		instance.load(get_TrxName());
		return instance;
	}

	private MProduct initProduct (String sku)
	{
		MProduct product = null;
		for (int i=0; i<l_product.size(); i++)
		{
			product = l_product.get(i);
			if (product.getSKU().equals(sku))
				break;
			product = null;
		}
		if (product == null)
		{
			product = MProduct.getOfSKU(getCtx(), sku, get_TrxName());
			if (product != null)
				l_product.add(product);
		}
		
		return product;
	}
	
	private MInventory initInventory (MWarehouse wh)
	{
		MInventory inv = null;
		for (int i=0; i<l_cache.size(); i++)
		{
			if (l_cache.get(i) instanceof MInventory == false)
				continue;
			inv = (MInventory)l_cache.get(i);
			if (inv.getM_Warehouse_ID() == wh.get_ID() && inv.getC_DocType_ID() == m_docTypeID
					&& inv.getMovementDate().equals(p_dateCount))
				break;
			inv = null;
		}
		if (inv == null)
		{
			String sql = "SELECT * FROM M_Inventory WHERE C_DocType_ID = ? AND MovementDate = ? AND M_Warehouse_ID = ? "
					+ " AND Processed = ? AND IsActive = ?";
			PreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				st = DB.prepareStatement(sql, get_TrxName());
				st.setInt(1, m_docTypeID);
				st.setTimestamp(2, p_dateCount);
				st.setInt(3, wh.get_ID());
				st.setString(4, "N");
				st.setString(5, "Y");
				rs = st.executeQuery();
				if (rs.next())
				{
					inv = new MInventory(getCtx(), rs, get_TrxName());
					l_cache.add(inv);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		
		if (inv == null)
		{
			inv = new MInventory(wh, get_TrxName());
			inv.setC_DocType_ID(m_docTypeID);
			inv.setMovementDate(p_dateCount);
			inv.setDateAcct(p_dateCount);
			inv.setC_Currency_ID (p_currencyID); //TODO
			if (!inv.save())
				return null;
			l_cache.add(inv);
		}
		
		return inv;
	}
	
	private MWarehouse initWarehouse (String name)
	{
		MWarehouse wh = null;
		for (int i=0; i<l_cache.size(); i++)
		{
			if (l_cache.get(i) instanceof MWarehouse == false)
				continue;
			wh = (MWarehouse)l_cache.get(i);
			if (wh.getValue().equals(name))
				break;
			wh = null;
		}
		if (wh == null)
		{
			String sql = "SELECT * FROM M_Warehouse WHERE UPPER (Value) LIKE ?";
			PreparedStatement st = null;
			ResultSet rs = null;
			String param = "%" + name.toUpperCase().trim();
			try
			{
				st = DB.prepareStatement(sql, get_TrxName());
				st.setString(1, param);
				rs = st.executeQuery();
				if (rs.next())
				{
					wh = new MWarehouse(getCtx(), rs, get_TrxName());
					l_cache.add(wh);
				}
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				DB.close(rs, st);
			}
		}
		
		return wh;
	}
}
