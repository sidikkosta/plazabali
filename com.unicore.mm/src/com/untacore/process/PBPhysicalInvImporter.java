/**
 * 
 */
package com.untacore.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import org.compiere.model.MAttribute;
import org.compiere.model.MAttributeInstance;
import org.compiere.model.MAttributeSet;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInventoryLineMA;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.uns.base.model.MInventory;

/**
 * @author nurse
 *
 */
public class PBPhysicalInvImporter extends SvrProcess 
{
	private String p_fileName = null;
	private MInventory m_inventory = null;
	private String p_separator = null;
	
	/**
	 * 
	 */
	public PBPhysicalInvImporter() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() 
	{
		ProcessInfoParameter[] params = getParameter();
		for (int i=0; i<params.length; i++)
		{
			if ("FileName".equals(params[i].getParameterName()))
				p_fileName = params[i].getParameterAsString();
			else if ("Separator".equals(params[i].getParameterName()))
				p_separator = params[i].getParameterAsString();
			else
				log.log(Level.WARNING, "Unknown parameter " + params[i].getParameterName());
		}
		m_inventory = new MInventory(getCtx(), getRecord_ID(), get_TrxName());
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (p_separator.equals("|"))
			p_separator = "\\|";
		int counter = 0;
		File file = new File(p_fileName);
		if (!file.exists())
			return "Can't find file " + p_fileName;
		BufferedReader reader = new BufferedReader(new FileReader(file));
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String line;
		Hashtable<String, MInventoryLine> mapILine = new Hashtable<>();
		while ((line = reader.readLine()) != null)
		{
			if (counter == 0)
			{
				counter++;
				writeLog(Level.INFO, line);
				continue;
			}
			String[] columns = line.split(p_separator);
			if (columns == null || columns.length != 6)
				continue;
			int idx = 0;
			String sku = columns[idx++];
			String etbb = columns[idx++];
			String qtyStr = columns[idx++];
			String dateStr = columns[idx++];
			String timeStr = columns[idx++];
			String locStr = columns[idx++];
			String dateTimeStr = dateStr + " " + timeStr;
			Date date = format.parse(dateTimeStr);
			Timestamp dateCount = new Timestamp(date.getTime());
			BigDecimal qtyCount = new BigDecimal(qtyStr);
			
			StringBuilder sb = new StringBuilder(sku).append(" | ")
					.append(etbb).append(" | ").append(qtyStr).append(" | ")
					.append(dateStr).append(" | ").append(timeStr).append(" | ")
					.append(locStr);
			String msg = sb.toString();
			writeLog(Level.INFO, msg);
			
			int locatorID = DB.getSQLValue(get_TrxName(), "SELECT M_Locator_ID FROM M_Locator WHERE Value = ?", locStr);
			int productID = DB.getSQLValue(get_TrxName(), "SELECT M_Product_ID FROM M_Product WHERE SKU = ?", sku);
			String key = locatorID + "-" + productID;
			
			MInventoryLine iLine = mapILine.get(key);
			if (iLine == null)
			{
				iLine = MInventoryLine.get(m_inventory, locatorID, productID, 0);
				if (iLine != null)
				{
					String sql = "DELETE FROM M_InventoryLineMA WHERE M_InventoryLine_ID = ?";
					DB.executeUpdate(sql, iLine.get_ID(), get_TrxName(), 0);
					iLine.setQtyCount(Env.ZERO);
					iLine.setQtytransacted(Env.ZERO);
					iLine.setQtyRealCount(Env.ZERO);
					iLine.setDateCount(null);
					mapILine.put(key, iLine);
				}
			}
			
			if (iLine == null)
			{
				iLine = new MInventoryLine(m_inventory, locatorID, productID, 0, Env.ZERO, Env.ZERO);
				mapILine.put(key, iLine);
			}
			if (sku == null)
				sku = "";
			if (etbb == null)
				etbb = "";
			
			int asiID = DB.getSQLValue(
					get_TrxName(), 
					"SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance WHERE Description = ? "
					+ " AND EXISTS (SELECT M_AttributeSetInstance_ID FROM M_StorageOnHand WHERE "
					+ " M_AttributeSetInstance_ID = M_AttributeSetInstance.M_AttributeSetInstance_ID AND M_Product_ID = ? "
					+ " AND M_Locator_ID = ?) ", etbb, productID, locatorID);
			if (asiID <= 0)
				asiID = DB.getSQLValue(
						get_TrxName(), 
						"SELECT M_AttributeSetInstance_ID FROM M_AttributeSetInstance WHERE Description = ?", etbb);
			MAttributeSetInstance instance = null;
			if (asiID <= 0)
			{
				if (etbb != null && etbb.length() == 8)
				{
					instance = MAttributeSetInstance.create(
							getCtx(), MProduct.get(getCtx(), productID), get_TrxName());
					MAttributeSet attrSet = instance.getMAttributeSet();
					MAttribute[] attrs = attrSet.getMAttributes(true);
					String bcCode = etbb.substring(0, 5);
					String year = etbb.substring(5, 6);
					String port = etbb.substring(6,8);
					
					for (int i=0; i<attrs.length; i++)
					{
						String val = null;
						if (attrs[i].getName().equalsIgnoreCase("BC Code"))
						{
							val = bcCode;
						}
						else if (attrs[i].getName().equalsIgnoreCase("Year"))
						{
							val = year;
						}
						else if (attrs[i].getName().equalsIgnoreCase("Port"))
						{
							val = port;
						}
						else
							continue;
						
						MAttributeInstance attInstance = MAttributeInstance.getNoSave(
								getCtx(), instance.getM_AttributeSetInstance_ID(),
								attrs[i].getM_Attribute_ID(), get_TrxName());
						if (null == attInstance)
							continue;

						attInstance.setValue(val);
						attInstance.saveEx();
					}
					instance.setDescription();
					instance.saveEx();
					asiID = instance.get_ID();
				}
			}
			else
				instance = MAttributeSetInstance.get(getCtx(), asiID, productID);
			
			if (iLine.getDateCount() == null || iLine.getDateCount().before(dateCount))
				iLine.setDateCount(dateCount);
			
			iLine.setQtyRealCount(iLine.getQtyRealCount ().add (qtyCount));
			iLine.saveEx();
			
			MStorageOnHand[] storages = MStorageOnHand.getWarehouse(
					getCtx(), m_inventory.getM_Warehouse_ID(), productID, asiID, null, 
					true, false, locatorID, get_TrxName());
			for (int i=0; i<storages.length; i++)
			{
				BigDecimal qtyOnHand = storages[i].getQtyOnHand();
				MInventoryLineMA ma = new MInventoryLineMA(
						iLine, storages[i].getM_AttributeSetInstance_ID(), 
						qtyOnHand, storages[i].getDateMaterialPolicy(), 
						false);
				ma.setQtyCount (qtyCount);
				ma.setDateCount (dateCount);
				ma.saveEx();
				qtyCount = Env.ZERO;
			}
			
			if (qtyCount.signum() != 0)
			{
				MInventoryLineMA ma = new MInventoryLineMA(
						iLine, asiID, 
						qtyCount.negate(), m_inventory.getMovementDate(), 
						false);
				ma.setQtyCount (qtyCount);
				ma.setDateCount (dateCount);
				ma.saveEx();
			}
			counter ++;
		}
		
		reader.close();
		
		return counter + " record imported";
	}

	private void writeLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		log.log(level, msg);
	}
}
