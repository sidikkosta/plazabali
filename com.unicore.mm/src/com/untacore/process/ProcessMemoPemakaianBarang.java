/**
 * 
 */
package com.untacore.process;

import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.ProcessUtil;
import org.compiere.model.MInventory;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Trx;
import org.compiere.util.Util;


/**
 * @author AzHaidar
 *
 */
public class ProcessMemoPemakaianBarang extends SvrProcess 
{
	private String	m_Applicant;
	private String	m_ApplicantPosition;
	
	/**
	 * 
	 */
	public ProcessMemoPemakaianBarang() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] params = getParameter();
		for(ProcessInfoParameter param : params)
		{
			if(param.getParameterName().equals("ApplicantName"))
				m_Applicant = param.getParameterAsString();
			else if(param.getParameterName().equals("ApplicantPositionName"))
				m_ApplicantPosition = param.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + param.getParameterName());
		}
	} //prepare

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception 
	{
		if (Util.isEmpty(m_Applicant, true))
			throw new AdempiereUserError("Applicant Name is mandatory.");
		if (Util.isEmpty(m_ApplicantPosition, true))
			throw new AdempiereUserError("Applicant Position is mandatory.");
		
		MInventory usage = new MInventory(getCtx(), getRecord_ID(), get_TrxName());
		usage.setApplicant(m_Applicant);
		usage.setApplicantPosition(m_ApplicantPosition);
		if (!usage.save())
			throw new AdempiereException("Error while set the applicant parameter into the usage header.");
		
		int AD_Process_ID = MProcess.getProcess_ID("JasperMemoPemakaianBarang", get_TrxName());
		
		MProcess proc = new MProcess(getCtx(), AD_Process_ID, get_TrxName());
	    MPInstance instance = new MPInstance(proc, getRecord_ID());
	    instance.saveEx();
	    
	    ProcessInfo poInfo = new ProcessInfo("JasperMemoPemakaianBarang", AD_Process_ID);
	    poInfo.setRecord_ID(getRecord_ID());
	    poInfo.setAD_PInstance_ID(instance.getAD_PInstance_ID());
		
	    // need to commit in order to allow jasper to view the data
	    Trx trx = Trx.get(get_TrxName(), true);
	    trx.commit();

	    ProcessUtil.startJavaProcess(getCtx(), poInfo, trx);
	    
	    return "DONE";
	}

}
