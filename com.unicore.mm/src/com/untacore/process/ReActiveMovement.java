package com.untacore.process;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.SvrProcess;

import com.unicore.base.model.MMovement;

public class ReActiveMovement extends SvrProcess {

	/**
	 * @author Thunder
	 */
	
	MMovement m_move = null;
	
	public ReActiveMovement() {
		super();
	}

	@Override
	protected void prepare() {
		
		if(getRecord_ID() > 0)
			m_move = new MMovement(getCtx(), getRecord_ID(), get_TrxName());
		else
			throw new AdempiereException("Only can acces from window");

	}

	@Override
	protected String doIt() throws Exception {
		
		if(m_move == null)
			throw new AdempiereException("Cannot found MMovement");
		 
		if(!m_move.getDocStatus().equals(MMovement.DOCSTATUS_InProgress))
			return "Disallowed Re-active";
		
		m_move.setProcessed(false);
		m_move.setDocStatus(MMovement.DOCSTATUS_Drafted);
		m_move.setDocAction(MMovement.DOCACTION_Complete);
		
		m_move.saveEx();
		
		return null;
	}

}
