/**
 * 
 */
package com.unicore.base.doc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.compiere.acct.Doc;
import org.compiere.acct.DocLine;
import org.compiere.acct.Fact;
import org.compiere.acct.FactLine;
import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MCharge;
import org.compiere.model.MDocType;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInventoryLineMA;
import org.compiere.model.MProduct;
import org.compiere.model.ProductCost;
import org.compiere.util.Env;

import com.uns.base.model.MCostDetail;

/**
 * @author nurse
 *
 */
public class Doc_Inventory extends Doc 
{
	private MInventory m_inventory = null;
	
	/**
	 * @param as
	 * @param clazz
	 * @param rs
	 * @param defaultDocumentType
	 * @param trxName
	 */
	public Doc_Inventory(MAcctSchema as, Class<?> clazz, ResultSet rs,
			String defaultDocumentType, String trxName) 
	{
		super(as, clazz, rs, defaultDocumentType, trxName);
	}
	
	public Doc_Inventory (MAcctSchema as, ResultSet rs, String trxName)
	{
		this (as, MInventory.class, rs, null, trxName);
	}
	
	private List<DocLine> loadDocumentDetailMAS (MInventoryLine line)
	{
		MInventoryLineMA[] mas = MInventoryLineMA.get(getCtx(), line.get_ID(), getTrxName());
		List<DocLine> list = new ArrayList<>();
		for (int i=0; i<mas.length; i++)
		{
			DocLine dl = new DocLine(line, this);
			dl.setM_AttributesetInstance_ID(mas[i].getM_AttributeSetInstance_ID());
			dl.setQty(mas[i].getMovementQty(), false);
			dl.setReversalLine_ID(line.getReversalLine_ID());
			list.add(dl);
		}
		
		return list;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#loadDocumentDetails()
	 */
	@Override
	protected String loadDocumentDetails() 
	{
		setC_Currency_ID (NO_CURRENCY);
		m_inventory = (MInventory)getPO();
		setDateDoc (m_inventory.getMovementDate());
		setDateAcct(m_inventory.getDateAcct());
		MInventoryLine[] lines = m_inventory.getLines(false);
		List<DocLine> list = new ArrayList<>();
		for (int i=0; i<lines.length; i++)
		{
			if (lines[i].getM_AttributeSetInstance_ID() == 0)
			{
				list.addAll(loadDocumentDetailMAS(lines[i]));
				continue;
			}
			DocLine dl = new DocLine(lines[i], this);
			dl.setQty(lines[i].getQtyInternalUse(), false);
			dl.setReversalLine_ID(lines[i].getReversalLine_ID());
		}
		
		p_lines = new DocLine[list.size()];
		list.toArray(p_lines);
		return null;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#getBalance()
	 */
	@Override
	public BigDecimal getBalance() 
	{
		return Env.ZERO;
	}
	
	private ArrayList<Fact> createFactsStockAdjustment (ArrayList<Fact> facts, MAcctSchema as)
	{
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		setC_Currency_ID(as.getC_Currency_ID());
		for (int i=0; i<p_lines.length; i++)
		{
			MAccount crAccount = null;
			if (p_lines[i].getC_Charge_ID() != 0)
				crAccount = MCharge.getAccount(p_lines[i].getC_Charge_ID(), as);
			else
				crAccount = getAccount(Doc.ACCTTYPE_InvDifferences, as);
			
			MAccount drAccount = p_lines[i].getAccount(
					ProductCost.ACCTTYPE_P_Asset, as);
			BigDecimal costs = Env.ZERO;
			if (!isReversal())
			{
				costs = p_lines[i].getProductCosts(as, p_lines[i].getAD_Org_ID(), false, "M_InventoryLine_ID=?");
				if (costs == null)
				{
					p_Error = "No Costs for " + p_lines[i].getProduct().getName();
					return null;
				}
			}
			
			FactLine dr = fact.createLine(p_lines[i], drAccount, getC_Currency_ID(), costs);
			if (dr == null)
			{
				p_Error = "DR not created " + p_lines[i].toString();
				return null;
			}
			dr.setM_Locator_ID(p_lines[i].getM_Locator_ID());
			if (isReversal())
			{
				//	Set AmtAcctDr from Original Phys.Inventory
				if (!dr.updateReverseLine (MInventory.Table_ID,
						((MInventory) getPO()).getReversal_ID(), 
						((MInventoryLine)p_lines[i].getPO()).getReversalLine_ID(),Env.ONE))
				{
					p_Error = "Original Physical Inventory not posted yet";
					return null;
				}
				costs = dr.getAcctBalance();
			}
			
			
			FactLine cr = fact.createLine(p_lines[i], crAccount, getC_Currency_ID(), costs.negate());
			if (cr == null)
			{
				p_Error = "CR not created " + p_lines[i].toString();
				return null;
			}
			
			cr.setM_Locator_ID(p_lines[i].getM_Locator_ID());
			if (isReversal())
			{
				//	Set AmtAcctDr from Original Phys.Inventory
				if (!cr.updateReverseLine (MInventory.Table_ID,
						((MInventory) getPO()).getReversal_ID(), 
						((MInventoryLine)p_lines[i].getPO()).getReversalLine_ID(),Env.ONE))
				{
					p_Error = "Original Physical Inventory not posted yet";
					return null;
				}
				costs = cr.getAcctBalance();
			
			
			if (!MCostDetail.createInventory(as, p_lines[i].getAD_Org_ID(), 
					p_lines[i].getM_Product_ID(), p_lines[i].getM_AttributeSetInstance_ID(), 
					p_lines[i].get_ID(), 0, costs, p_lines[i].getQty(), p_lines[i].getDescription(), 
					getTrxName()))
			{
				p_Error = "Failed to create cost detail record";
				return null;
			}}
		}
		
		facts.add(fact);
		return facts;
	}

	/* (non-Javadoc)
	 * @see org.compiere.acct.Doc#createFacts(org.compiere.model.MAcctSchema)
	 */
	@Override
	public ArrayList<Fact> createFacts(MAcctSchema as) 
	{
		ArrayList<Fact> facts = new ArrayList<Fact>();
		if (!m_inventory.getC_DocType().getDocSubTypeInv().equals(MDocType.DOCSUBTYPEINV_ProductConversion))
			return facts;
		if ("Stock Adjustment".equals(m_inventory.getC_DocType().getName()))
			return createFactsStockAdjustment(facts, as);
		
		Fact fact = new Fact(this, as, Fact.POST_Actual);
		setC_Currency_ID(as.getC_Currency_ID());
		FactLine fLine;
		List<DocLine> output = new ArrayList<>();
		Hashtable<Integer, BigDecimal> mapCosts = new Hashtable<>();
		int C_Currency_ID = getC_Currency_ID() > 0 ? getC_Currency_ID() : as.getC_Currency_ID();
		BigDecimal totalOutputQty = Env.ZERO;
		for (int i=0; i<p_lines.length; i++)
		{
			MProduct product = p_lines[i].getProduct();
			
			if(product.isConsignment())
				continue;
			
			BigDecimal costs = Env.ZERO;
			if (!isReversal())
			{
				if (p_lines[i].getQty().signum() == 1)
				{
					output.add(p_lines[i]);
					totalOutputQty = totalOutputQty.add(p_lines[i].getQty());
					continue;
				}
				costs = p_lines[i].getProductCosts(as, p_lines[i].getAD_Org_ID(), false, "M_InventoryLine_ID=?");
				if (costs == null)
				{
					p_Error = "No Costs for " + p_lines[i].getProduct().getName();
					return null;
				}
			}
			else
			{
				costs = Env.ONE;
			}
			//  Inventory       DR      CR
			fLine = fact.createLine(p_lines[i],
						p_lines[i].getAccount(ProductCost.ACCTTYPE_P_Asset, as),
						C_Currency_ID, costs);
			if (fLine != null)
			{
				fLine.setM_Locator_ID(p_lines[i].getM_Locator_ID());
				if (isReversal())
				{
					//	Set AmtAcctDr from Original Phys.Inventory
					if (!fLine.updateReverseLine (MInventory.Table_ID,
							((MInventory) getPO()).getReversal_ID(), 
							((MInventoryLine)p_lines[i].getPO()).getReversalLine_ID(),Env.ONE))
					{
						p_Error = "Original Physical Inventory not posted yet";
						return null;
					}
					costs = fLine.getAcctBalance();
				}
				
				if (!MCostDetail.createInventory(as, p_lines[i].getAD_Org_ID(), 
						p_lines[i].getM_Product_ID(), p_lines[i].getM_AttributeSetInstance_ID(), 
						p_lines[i].get_ID(), 0, costs, p_lines[i].getQty(), p_lines[i].getDescription(), 
						getTrxName()))
				{
					p_Error = "Failed to create cost detail record";
					return null;
				}
				
				
				BigDecimal cogs = mapCosts.get(p_lines[i].getM_AttributeSetInstance_ID());
				if (cogs == null)
					cogs = Env.ZERO;
				cogs = cogs.add(costs);
				mapCosts.put(p_lines[i].getM_AttributeSetInstance_ID(), cogs);
			}
		}
		
		for (int i=0; i<output.size(); i++)
		{
			MProduct product = output.get(i).getProduct();
			
			if(product.isConsignment())
				continue;
			
			BigDecimal myCosts = Env.ZERO;
			Enumeration<Integer> keys = mapCosts.keys();
			BigDecimal proportion = output.get(i).getQty().divide(
					totalOutputQty, 10, RoundingMode.HALF_EVEN);
			
			while (keys.hasMoreElements())
			{
				Integer key = keys.nextElement();
				if (key == output.get(i).getM_AttributeSetInstance_ID())
					myCosts = myCosts.add(mapCosts.get(key));
				else
				{
					
					boolean skip = false;
					for (int j=0; j<output.size(); j++)
						if (output.get(j).getM_AttributeSetInstance_ID() == key)
						{
							skip = true;
							break;
						}
					if (skip)
						continue;
					BigDecimal costs = mapCosts.get(key);
					costs = costs.multiply(proportion);
					myCosts = myCosts.add(costs);
				}
			}
			output.get(i).setAmount(myCosts.negate());
		}
		
		for (int i=0; i<output.size(); i++)
		{
			BigDecimal costs = output.get(i).getAmtSource();
			
			fLine = fact.createLine(output.get(i),
					output.get(i).getAccount(ProductCost.ACCTTYPE_P_Asset, as),
					C_Currency_ID, costs);
			if (fLine != null)
			{
				fLine.setM_Locator_ID(output.get(i).getM_Locator_ID());
				if (isReversal())
				{
					if (!fLine.updateReverseLine (MInventory.Table_ID,
							((MInventory) getPO()).getReversal_ID(), 
							((MInventoryLine)output.get(i).getPO()).getReversalLine_ID(),Env.ONE))
					{
						p_Error = "Original Physical Inventory not posted yet";
						return null;
					}
					costs = fLine.getAcctBalance();
				}
				
				if (!MCostDetail.createInventory(as, output.get(i).getAD_Org_ID(), 
						output.get(i).getM_Product_ID(), output.get(i).getM_AttributeSetInstance_ID(), 
						output.get(i).get_ID(), 0, costs, output.get(i).getQty(), output.get(i).getDescription(), 
						getTrxName()))
				{
					p_Error = "Failed to create cost detail record";
					return null;
				}
			}
		}
		facts.add(fact);
		return facts;
	}

}
