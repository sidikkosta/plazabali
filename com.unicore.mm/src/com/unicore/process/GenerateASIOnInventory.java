/**
 * 
 */
package com.unicore.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Enumeration;
import java.util.Hashtable;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MDocType;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInventoryLineMA;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.Env;

/**
 * @author Burhani Adam
 *
 */
public class GenerateASIOnInventory extends SvrProcess {

	/**
	 * 
	 */
	public GenerateASIOnInventory() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		if(getRecord_ID() <= 0)
			throw new AdempiereException("Must run from window.");
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		MInventory inventory = new MInventory(getCtx(), getRecord_ID(), get_TrxName());
		MDocType dt = MDocType.get(getCtx(), inventory.getC_DocType_ID());
		if(dt.getDocSubTypeInv() == null || !dt.getDocSubTypeInv().equals(MDocType.DOCSUBTYPEINV_ProductConversion))
			return null;
		Hashtable<String, BigDecimal> mapASI = new Hashtable<>();
		MInventoryLine[] lines = inventory.getLines(true);
//		int sourceID = 0;
		BigDecimal totalQty = Env.ZERO;
		BigDecimal qtySource = Env.ZERO;
		int productID = 0;
		
		for(int i=0;i<lines.length;i++)
		{
			if(lines[i].getQtyInternalUse().compareTo(Env.ZERO) == -1)
			{
//				if(qtySource.compareTo(Env.ZERO) != 0)
//					throw new AdempiereException("Quantity smaller than zero can't be more than one");
				
				qtySource = qtySource.add(lines[i].getQtyInternalUse());
//				sourceID = lines[i].get_ID();
				productID = lines[i].getM_Product_ID();
				MInventoryLineMA[] mas = MInventoryLineMA.get(getCtx(), lines[i].get_ID(), get_TrxName());
				BigDecimal qtyMa = Env.ZERO;
				for(int j=0;j<mas.length;j++)
				{
					mapASI.put(lines[i].getM_Product_ID() + ";" + mas[j].getM_AttributeSetInstance_ID(), mas[j].getMovementQty());
					qtyMa = qtyMa.add(mas[j].getMovementQty());
				}
				if(qtyMa.compareTo(lines[i].getQtyInternalUse()) != 0)
					throw new AdempiereUserError("QtyMA <> QtyLine on line " + lines[i].getLine());
			}
			else
			{
				if(totalQty.compareTo(Env.ZERO) != 0)
					throw new AdempiereException("Output more than one record.!");
				totalQty = lines[i].getQtyInternalUse();
			}
		}
		
		qtySource = qtySource.abs();
		
		if(qtySource.compareTo(Env.ZERO) == 0)
			return "";
		
		if(totalQty.compareTo(Env.ZERO) == 0)
			return "";
		
//		MInventoryLineMA[] maSource = MInventoryLineMA.get(getCtx(), sourceID, get_TrxName());
		
//		Hashtable<Integer, BigDecimal> mapASI = new Hashtable<>();
//		for(int i=0;i<maSource.length;i++)
//		{
//			mapASI.put(maSource[i].getM_AttributeSetInstance_ID(), maSource[i].getMovementQty());
//		}
		
		for(int j=0;j<lines.length;j++)
		{
//			if(lines[j].get_ID() == sourceID)
//				continue;
			if(lines[j].getQtyInternalUse().compareTo(Env.ZERO) == -1)
				continue;
			
			int success = MInventoryLineMA.deleteAllInventoryLineMA(lines[j].get_ID(), get_TrxName());
			if(success < 0)
				throw new AdempiereException("Error when trying delete attributes.");
			
			BigDecimal qtyMA = MInventoryLineMA.getManualQty(lines[j].get_ID(), get_TrxName());
			BigDecimal balance = lines[j].getQtyInternalUse().subtract(qtyMA);
			BigDecimal qtyAllocated = Env.ZERO;
			BigDecimal divide = lines[j].getQtyInternalUse().divide(qtySource, 2, RoundingMode.HALF_DOWN);
			Enumeration<String> mapID = mapASI.keys();
			while (mapID.hasMoreElements())
			{
				if(balance.compareTo(Env.ZERO) == 0)
					break;
				String key = mapID.nextElement();
				String[] keys =  key.split(";");
				productID = Integer.parseInt(keys[0]);
				int asiID = Integer.parseInt(keys[1]);
				BigDecimal asiQty = mapASI.get(key);
				asiQty = asiQty.multiply(divide);
				asiQty = asiQty.abs();
				asiQty = asiQty.setScale(0, RoundingMode.UP);
				if(asiQty.compareTo(Env.ZERO) <= 0)
					continue;
				qtyAllocated = asiQty;
				if(balance.compareTo(qtyAllocated) == -1)
					qtyAllocated = balance;
				balance = balance.subtract(qtyAllocated);
					
				MAttributeSetInstance asiFrom = MAttributeSetInstance.get(getCtx(), get_TrxName(), asiID, 0);
				MAttributeSetInstance asiTo = MAttributeSetInstance.get(getCtx(), get_TrxName(), asiFrom.getDescription(), productID);
				asiTo.setDescription(asiFrom.getDescription());
				asiTo.saveEx();
				
				MInventoryLineMA ma = MInventoryLineMA.addOrCreate(lines[j],
						asiTo.get_ID(), qtyAllocated, null, false);
				ma.saveEx();
			}
		}
		
		inventory.setGenerateASI("Y");
		inventory.saveEx();
		
		return "Success";
	}
}