/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_InventoryLineCost
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_InventoryLineCost extends PO implements I_UNS_InventoryLineCost, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20190130L;

    /** Standard Constructor */
    public X_UNS_InventoryLineCost (Properties ctx, int UNS_InventoryLineCost_ID, String trxName)
    {
      super (ctx, UNS_InventoryLineCost_ID, trxName);
      /** if (UNS_InventoryLineCost_ID == 0)
        {
			setC_Currency_ID (0);
			setCurrentCostPrice (Env.ZERO);
// 0
			setM_InventoryLine_ID (0);
			setUNS_InventoryLineCost_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_InventoryLineCost (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_InventoryLineCost[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Current Cost Price.
		@param CurrentCostPrice 
		The currently used cost price
	  */
	public void setCurrentCostPrice (BigDecimal CurrentCostPrice)
	{
		set_Value (COLUMNNAME_CurrentCostPrice, CurrentCostPrice);
	}

	/** Get Current Cost Price.
		@return The currently used cost price
	  */
	public BigDecimal getCurrentCostPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CurrentCostPrice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_InventoryLine getM_InventoryLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_InventoryLine)MTable.get(getCtx(), org.compiere.model.I_M_InventoryLine.Table_Name)
			.getPO(getM_InventoryLine_ID(), get_TrxName());	}

	/** Set Phys.Inventory Line.
		@param M_InventoryLine_ID 
		Unique line in an Inventory document
	  */
	public void setM_InventoryLine_ID (int M_InventoryLine_ID)
	{
		if (M_InventoryLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_InventoryLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_InventoryLine_ID, Integer.valueOf(M_InventoryLine_ID));
	}

	/** Get Phys.Inventory Line.
		@return Unique line in an Inventory document
	  */
	public int getM_InventoryLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InventoryLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Inventory Line Cost.
		@param UNS_InventoryLineCost_ID Inventory Line Cost	  */
	public void setUNS_InventoryLineCost_ID (int UNS_InventoryLineCost_ID)
	{
		if (UNS_InventoryLineCost_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_InventoryLineCost_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_InventoryLineCost_ID, Integer.valueOf(UNS_InventoryLineCost_ID));
	}

	/** Get Inventory Line Cost.
		@return Inventory Line Cost	  */
	public int getUNS_InventoryLineCost_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_InventoryLineCost_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_InventoryLineCost_UU.
		@param UNS_InventoryLineCost_UU UNS_InventoryLineCost_UU	  */
	public void setUNS_InventoryLineCost_UU (String UNS_InventoryLineCost_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_InventoryLineCost_UU, UNS_InventoryLineCost_UU);
	}

	/** Get UNS_InventoryLineCost_UU.
		@return UNS_InventoryLineCost_UU	  */
	public String getUNS_InventoryLineCost_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_InventoryLineCost_UU);
	}
}