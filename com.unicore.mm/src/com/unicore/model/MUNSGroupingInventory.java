package com.unicore.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Util;


public class MUNSGroupingInventory extends X_UNS_GroupingInventory implements
		DocAction {

	/**
	 * @author Hamba Allah
	 */
	private static final long serialVersionUID = -5427797282897409343L;
	
	private String m_processMsg = null;
	private boolean m_justPrepared = false;
	private MUNSGroupingInventoryLine[] m_lines = null;

	public MUNSGroupingInventory(Properties ctx, int UNS_GroupingInventory_ID,
			String trxName) {
		super(ctx, UNS_GroupingInventory_ID, trxName);
		
	}
	
	public MUNSGroupingInventory(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		
	}

	@Override
	public boolean processIt(String action) throws Exception {
		
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (action, getDocAction());
	}

	@Override
	public boolean unlockIt() {
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		return true;
	}

	@Override
	public boolean invalidateIt() {
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}

	public MUNSGroupingInventoryLine[] getLines(boolean requery)
	{
		if (m_lines != null && !requery) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		List<MUNSGroupingInventoryLine> list = new Query(
				getCtx(), MUNSGroupingInventoryLine.Table_Name, MUNSGroupingInventory.Table_Name+"_ID = ?", get_TrxName())
				.setParameters(get_ID())
				.list();
		
		m_lines = list.toArray(new MUNSGroupingInventoryLine[list.size()]);
		
		return m_lines;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		getLines(false);
		
		if(!newRecord && is_ValueChanged(COLUMNNAME_M_Warehouse_ID))
		{
			if(m_lines.length > 0)
			{
				log.saveError("Error Save", "Has Line, cannot change warehouse");
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public String prepareIt() {
		
		if(log.isLoggable(Level.INFO))
			log.info(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		getLines(false);
		if(m_lines.length == 0)
		{
			m_processMsg = "@No Lines@";
			return DocAction.STATUS_Invalid;
		}
		
		String errMsgDuplicatedDoc = null;
		String errMsgStatusDoc = null;
		for(MUNSGroupingInventoryLine line : m_lines)
		{
			MInventory inv = (MInventory) line.getM_Inventory();
			String docError = line.checkDuplicated();
			if(!Util.isEmpty(docError, true))
			{
				if(errMsgDuplicatedDoc == null)
					errMsgDuplicatedDoc = "Inv_ID: "+inv.getDocumentNo()+" on "+docError;
				else
					errMsgDuplicatedDoc = errMsgDuplicatedDoc+"; Inv_ID: "+inv.getDocumentNo()+" on "+docError;
			}
			if(!inv.getDocStatus().equals(MInventory.DOCSTATUS_Completed)
					&& !inv.getDocStatus().equals(MInventory.DOCSTATUS_Closed))
			{
				if(errMsgStatusDoc == null)
					errMsgStatusDoc = "Inventory No : "+inv.getDocumentNo();
				else
					errMsgStatusDoc = errMsgStatusDoc+"; "+inv.getDocumentNo();
			}
			
		}
		
		if(errMsgDuplicatedDoc != null)
			m_processMsg = "Duplicate grouping. "+errMsgDuplicatedDoc;
		if(errMsgStatusDoc != null)
		{
			if(m_processMsg == null)
				m_processMsg = "";
			
			m_processMsg = m_processMsg + 
					"\n. There was inventory not complete. Please complete first. "+errMsgStatusDoc;
		}
		
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		m_justPrepared = true;
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt() {
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt() {
		if (log.isLoggable(Level.INFO)) log.info(toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}

	@Override
	public String completeIt() {
		
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
//		for(MUNSGroupingInventoryLine line : m_lines)
//		{
//			MInventory inv = new MInventory(getCtx(), line.getM_Inventory_ID(), get_TrxName());
//			
//			if(inv.getDocStatus().equals(MInventory.DOCSTATUS_Completed)
//					|| inv.getDocStatus().equals(MInventory.DOCSTATUS_Closed))
//				continue;
//			
//			//action complete inventory
//			try {
//				
//				if(!inv.processIt(MInventory.DOCACTION_Complete))
//				{
//					m_processMsg = inv.getProcessMsg();
//					return DocAction.STATUS_Invalid;
//				}
//				
//				inv.saveEx();
//				
//			} catch (Exception e) {
//				e.printStackTrace();
//				m_processMsg = inv.getProcessMsg();
//				return DocAction.STATUS_Invalid;
//			}
//		}
		
		MUNSGroupingInventoryLine newInvLine = createOthersInventory();
		if(newInvLine != null)
		{
			MInventory inv = (MInventory) newInvLine.getM_Inventory();
			if(!inv.getDocStatus().equals(MInventory.DOCSTATUS_Completed)
					&&!inv.getDocStatus().equals(MInventory.DOCSTATUS_Closed))
			{
				m_processMsg = "Please complete inventory first. Document No : "+inv.getDocumentNo();
				setProcessed(true);
				return DocAction.STATUS_InProgress;
			}
		
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if(m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt() {

		if (log.isLoggable(Level.INFO))
			log.config(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if(m_processMsg != null)
			return false;
		
		getLines(false);
		for(MUNSGroupingInventoryLine line : m_lines)
		{
			if(!line.isAutoGenerated())
				continue;
			
			MInventory inventory = new MInventory(getCtx(), line.getM_Inventory_ID(), get_TrxName());
			//reverse it
			try {
				
				if(!inventory.processIt(DOCACTION_Void) || !inventory.save())
				{
					m_processMsg = inventory.getProcessMsg();
					return false;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				m_processMsg = inventory.getProcessMsg();
				return false;
			}
		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public boolean closeIt() {
		if (log.isLoggable(Level.INFO))
			log.config(toString());
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if(m_processMsg != null)
			return false;
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if(m_processMsg != null)
			return false;
		
		return true;
	}

	@Override
	public boolean reverseCorrectIt() {
		
		m_processMsg = "Disallowed Reverse Correct It";
		
		return false;
	}

	@Override
	public boolean reverseAccrualIt() {
		
		m_processMsg = "Disallowed Reverse Accrual It";
		
		return false;
	}

	@Override
	public boolean reActivateIt() {
		
		m_processMsg = "Disallowed Re-Activate";
		
		return false;
	}

	@Override
	public String getSummary() {
		
		return null;
	}

	@Override
	public String getDocumentInfo() {
		StringBuilder info = new StringBuilder();
		
		info.append(getCreated());
		
		return info.toString();
	}

	@Override
	public File createPDF() {
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}
	
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF

	@Override
	public String getProcessMsg() {
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID() {
		
		return 0;
	}

	@Override
	public int getC_Currency_ID() {
		
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt() {
		
		return null;
	}
	
	public MUNSGroupingInventoryLine createOthersInventory()
	{
		MUNSGroupingInventoryLine newGroupLine = null;
		//get grouping line was created before
		String getSql = "SELECT UNS_GroupingInventory_Line il"
				+ " INNER JOIN M_Inventory inv ON inv.M_Inventory_ID = il.M_Inventory_ID"
				+ " WHERE inv.DocStatus IN ('CO','CL') AND il.IsAutoGenerated = 'Y'"
				+ " AND il.IsActive = 'Y' AND il.UNS_GroupingInventory_ID = ?";
		int newGroupLineID = DB.getSQLValue(
				get_TrxName(), getSql, getUNS_GroupingInventory_ID());
		
		if(newGroupLineID > 0)
		{
			newGroupLine = new MUNSGroupingInventoryLine(getCtx(), newGroupLineID, get_TrxName());
			return newGroupLine;
		}
		
		String locators = initLocator(ischeckAllLocator());
		
		String sql = "SELECT M_Product_ID, M_Locator_ID FROM M_StorageOnHand soh WHERE"
				+ " QtyOnHand != 0 AND M_Locator_ID IN ("+locators
				+ " ) AND NOT EXISTS(SELECT * FROM M_InventoryLine il"
					+ " INNER JOIN UNS_GroupingInventory_Line gil ON gil.M_Inventory_ID = il.M_Inventory_ID AND gil.isActive = 'Y'"
					+ " WHERE gil.UNS_GroupingInventory_ID = ? AND il.M_Product_ID = soh.M_Product_ID AND il.M_Locator_ID = soh.M_Locator_ID)"
				+ " AND NOT EXISTS(SELECT * FROM M_Transaction WHERE M_Product_ID = soh.M_Product_ID"
					+ " AND Created >= ?) GROUP BY M_Product_ID,M_Locator_ID";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			
			String sqlDate = "SELECT MIN(ma.DateCount) FROM M_InventoryLineMA ma"
					+ " INNER JOIN M_InventoryLine il ON il.M_InventoryLine_ID = ma.M_InventoryLine_ID"
					+ " INNER JOIN UNS_GroupingInventory_Line gil ON gil.M_Inventory_ID = il.M_Inventory_ID"
					+ " WHERE gil.UNS_GroupingInventory_ID = ? AND gil.isActive = 'Y'";
			Timestamp dateCount = DB.getSQLValueTS(get_TrxName(), sqlDate, get_ID());
			
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, getUNS_GroupingInventory_ID());
			st.setTimestamp(2, dateCount);
			rs = st.executeQuery();
			
			MInventory newInv = null;
			
			while(rs.next())
			{	
				if(newInv == null)
				{
					MDocType[] dts = MDocType.getOfDocBaseType(getCtx(), MDocType.DOCBASETYPE_MaterialPhysicalInventory);
					MDocType dt = null;
					for(int i=0; i<dts.length; i++)
					{
						if(dts[i].getDocSubTypeInv().equals(MDocType.DOCSUBTYPEINV_PhysicalInventory))
						{
							dt = dts[i];
							break;
						}
							
					}
					
					newInv = new MInventory(getCtx(), 0, get_TrxName());
					newInv.setClientOrg(getAD_Client_ID(), getAD_Org_ID());
					newInv.setM_Warehouse_ID(getM_Warehouse_ID());
					newInv.setC_DocType_ID(dt.get_ID());
					newInv.setDateAcct(getDateDoc());
					newInv.setMovementDate(getDateDoc());
					newInv.setDescription("**Auto generate from Grouping Inventory No: "+getDocumentNo());
					newInv.saveEx();
				}
				
				MInventoryLine newLine = new MInventoryLine(newInv, rs.getInt(2), rs.getInt(1), 0, null, Env.ZERO);
				newLine.saveEx();
				
			}
			
			if(newInv != null)
			{
//				if(!newInv.processIt(MInventory.DOCACTION_Complete))
//					throw new AdempiereException(newInv.getProcessMsg());
//				
//				newInv.saveEx();
				
				//set Inventory to group
				newGroupLine = new MUNSGroupingInventoryLine(getCtx(), 0, get_TrxName());
				newGroupLine.setClientOrg(getAD_Client_ID(), getAD_Org_ID());
				newGroupLine.setUNS_GroupingInventory_ID(get_ID());
				newGroupLine.setM_Inventory_ID(newInv.get_ID());
				newGroupLine.setProcessGenerate(true);
				
				newGroupLine.saveEx();
				
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new AdempiereException("Error when try to create other document Inventory."
					+ " Please contact administrator");
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return newGroupLine;
	}
	
	public String initLocator(boolean isAllLocator) {
		
		String retVal = null;
		
		if(!isAllLocator)
		{
			String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DISTINCT(M_Locator_ID)), ',') FROM M_InventoryLine il"
					+ " INNER JOIN UNS_GroupingInventory_Line gil ON gil.M_Inventory_ID = il.M_Inventory_ID"
					+ " WHERE gil.UNS_GroupingInventory_ID = ? AND gil.isActive = 'Y'";
			retVal = DB.getSQLValueString(get_TrxName(), sql, getUNS_GroupingInventory_ID());
			
			//check if inventory list has intransit
			String sql2 = sql.concat(" AND EXISTS(SELECT 1 FROM M_Locator WHERE M_Warehouse_ID = ?"
					+ " AND UPPER(value) LIKE UPPER('%intransit%') AND M_Locator_ID = il.M_Locator_ID)");
			String intransit = DB.getSQLValueString(get_TrxName(), sql2, getUNS_GroupingInventory_ID(), getM_Warehouse_ID());
			
			if(!Util.isEmpty(intransit, true))
				retVal = retVal.concat(","+intransit);
		}
		else
		{
			String sql = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DISTINCT(M_Locator_ID)), ',') FROM M_Locator loc"
					+ " WHERE M_Warehouse_ID = ? AND IsActive = 'Y' AND NOT EXISTS(SELECT 1 FROM"
						+ " M_Locator WHERE M_Locator_ID = loc.M_Locator_ID AND UPPER(value) LIKE UPPER('%intransit%'))";
			retVal = DB.getSQLValueString(get_TrxName(), sql, getM_Warehouse_ID());
			
			//check if inventory list has intransit
			String sql2 = "SELECT ARRAY_TO_STRING(ARRAY_AGG(DISTINCT(M_Locator_ID)), ',') FROM M_InventoryLine il"
					+ " INNER JOIN UNS_GroupingInventory_Line gil ON gil.M_Inventory_ID = il.M_Inventory_ID"
					+ " WHERE gil.UNS_GroupingInventory_ID = ? AND gil.isActive = 'Y'"
					+ " AND EXISTS(SELECT 1 FROM M_Locator WHERE M_Warehouse_ID = ?"
					+ " AND UPPER(value) LIKE UPPER('%intransit%') AND M_Locator_ID = il.M_Locator_ID)";
			
			String intransit = DB.getSQLValueString(get_TrxName(), sql2, getUNS_GroupingInventory_ID(), getM_Warehouse_ID());
			
			if(!Util.isEmpty(intransit, true))
				retVal = retVal.concat(","+intransit);
		}
		
		return retVal;
	}

}
