/**
 * 
 */
package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;

/**
 * @author HandiWindu
 *
 */
public class MUNSUnnoticedMReceiptLine extends X_UNS_UnnoticedMReceiptLine 
	{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8179600198728791949L;
	/**
	 * @param ctx
	 * @param UNS_UnnoticedMReceiptLine_ID
	 * @param trxName
	 */
	public MUNSUnnoticedMReceiptLine(Properties ctx,
			int UNS_UnnoticedMReceiptLine_ID, String trxName) {
		super(ctx, UNS_UnnoticedMReceiptLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSUnnoticedMReceiptLine(Properties ctx, ResultSet rs,
			String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
    public boolean beforeSave (boolean newRecord)
    {
    	if(newRecord && isParentProcessed())
    		throw new AdempiereException("Cannot create new. The parent has been processed");
    	
    	//set	Product
    	if (getM_Product_ID() > 0)
    	{
    		StringBuffer SQL = new StringBuffer	
    		("SELECT name FROM M_Product WHERE M_Product_ID=?");
			String nameProduct = DB.getSQLValueStringEx(get_TrxName(), SQL.toString(), getM_Product_ID());
			setProduct(nameProduct);
    	}
    	//set status notice
    	
    	if (is_ValueChanged("M_InOutLine_ID"))
    	{
	    	new StringBuffer	
	    			("SELECT UNS_UnnoticedMReceiptLine_ID from UNS_UnnoticedMReceiptLine "
	    					+ "inner join UNS_UnnoticedMReceipt on "
	    					+ "UNS_UnnoticedMReceipt.UNS_UnnoticedMReceipt_ID=UNS_UnnoticedMReceiptLine.UNS_UnnoticedMReceiptLine_ID "
	    					+ "where UNS_UnnoticedMReceipt.DocStatus='CO' And "
	    					+ "UNS_UnnoticedMReceiptLine.UNS_UnnoticedMReceiptLine_ID <> ? "
	    					+ "and UNS_UnnoticedMReceipt.UNS_UnnoticedMReceipt_ID= ? and "
	    					+ "(UNS_UnnoticedMReceiptLine.m_inoutline_ID < 0 or "
	    					+ "UNS_UnnoticedMReceiptLine.M_InOutLine_ID is null) ");
	    	String sql = "SELECT 1 FROM UNS_UnNoticedMReceiptLine WHERE (M_InOutLine_ID IS NULL OR M_InOutLine_ID <= 0)"
	    			+ " AND UNS_UnNoticedMReceiptLine_ID <> ? AND UNS_UnNoticedMReceipt_ID = ?";
	    	boolean completed = DB.getSQLValue(get_TrxName(), sql, new Object[]{get_ID(), getUNS_UnnoticedMReceipt_ID()}) > 0 ? false : true;
	    	if(!completed)
	    	{
	    		return true;
	    	}
	    	
//	    	int result = DB.getSQLValue(get_TrxName(), SQL.toString(), new Object[]{get_ID(), getUNS_UnnoticedMReceipt_ID()}); 
//	    	
//	    	//if(result.equals(""))
//	    	if(result > 0)
//	    	{
//	    		return true;
//	    	}
	    	
	    	else 
	    	{
	    	sql = ("Update UNS_UnnoticedMReceipt set NoticeStatus='RC' Where "
	    			+ " UNS_UnnoticedMReceipt_ID=?");
			int idUnnoticedMReceipt = DB.executeUpdate(sql, getUNS_UnnoticedMReceipt_ID(), get_TrxName());
			if(idUnnoticedMReceipt < 0)
				throw new AdempiereException("Failed when trying update parent status");
	    	}
    	}
    	return true;
    
    }
    
    private boolean isParentProcessed() {
    	
    	String sql = "SELECT Processed FROM UNS_UnnoticedMReceipt WHERE"
    			+ " UNS_UnnoticedMReceipt_ID = ? ";
    	boolean processed = DB.getSQLValueString(get_TrxName(), sql, getUNS_UnnoticedMReceipt_ID()).equals("Y");
    	
    	return processed;
    }
}

