/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for UNS_UnnoticedMReceiptLine
 *  @author iDempiere (generated) 
 *  @version Release 2.1 - $Id$ */
public class X_UNS_UnnoticedMReceiptLine extends PO implements I_UNS_UnnoticedMReceiptLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171220L;

    /** Standard Constructor */
    public X_UNS_UnnoticedMReceiptLine (Properties ctx, int UNS_UnnoticedMReceiptLine_ID, String trxName)
    {
      super (ctx, UNS_UnnoticedMReceiptLine_ID, trxName);
      /** if (UNS_UnnoticedMReceiptLine_ID == 0)
        {
			setC_UOM_ID (0);
			setM_Locator_ID (0);
			setUNS_UnnoticedMReceipt_ID (0);
        } */
    }

    /** Load Constructor */
    public X_UNS_UnnoticedMReceiptLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_UNS_UnnoticedMReceiptLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_InOutLine getM_InOutLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_InOutLine)MTable.get(getCtx(), org.compiere.model.I_M_InOutLine.Table_Name)
			.getPO(getM_InOutLine_ID(), get_TrxName());	}

	/** Set Shipment/Receipt Line.
		@param M_InOutLine_ID 
		Line on Shipment or Receipt document
	  */
	public void setM_InOutLine_ID (int M_InOutLine_ID)
	{
		if (M_InOutLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_InOutLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_InOutLine_ID, Integer.valueOf(M_InOutLine_ID));
	}

	/** Get Shipment/Receipt Line.
		@return Line on Shipment or Receipt document
	  */
	public int getM_InOutLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InOutLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Movement Quantity.
		@param MovementQty 
		Quantity of a product moved.
	  */
	public void setMovementQty (BigDecimal MovementQty)
	{
		set_ValueNoCheck (COLUMNNAME_MovementQty, MovementQty);
	}

	/** Get Movement Quantity.
		@return Quantity of a product moved.
	  */
	public BigDecimal getMovementQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MovementQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Product.
		@param Product Product	  */
	public void setProduct (String Product)
	{
		set_Value (COLUMNNAME_Product, Product);
	}

	/** Get Product.
		@return Product	  */
	public String getProduct () 
	{
		return (String)get_Value(COLUMNNAME_Product);
	}

	public com.unicore.model.I_UNS_UnnoticedMReceipt getUNS_UnnoticedMReceipt() throws RuntimeException
    {
		return (com.unicore.model.I_UNS_UnnoticedMReceipt)MTable.get(getCtx(), com.unicore.model.I_UNS_UnnoticedMReceipt.Table_Name)
			.getPO(getUNS_UnnoticedMReceipt_ID(), get_TrxName());	}

	/** Set UNS_UnnoticedMReceipt_ID.
		@param UNS_UnnoticedMReceipt_ID UNS_UnnoticedMReceipt_ID	  */
	public void setUNS_UnnoticedMReceipt_ID (int UNS_UnnoticedMReceipt_ID)
	{
		if (UNS_UnnoticedMReceipt_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnnoticedMReceipt_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnnoticedMReceipt_ID, Integer.valueOf(UNS_UnnoticedMReceipt_ID));
	}

	/** Get UNS_UnnoticedMReceipt_ID.
		@return UNS_UnnoticedMReceipt_ID	  */
	public int getUNS_UnnoticedMReceipt_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnnoticedMReceipt_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UnnoticedMReceiptLine_ID.
		@param UNS_UnnoticedMReceiptLine_ID UNS_UnnoticedMReceiptLine_ID	  */
	public void setUNS_UnnoticedMReceiptLine_ID (int UNS_UnnoticedMReceiptLine_ID)
	{
		if (UNS_UnnoticedMReceiptLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_UNS_UnnoticedMReceiptLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_UNS_UnnoticedMReceiptLine_ID, Integer.valueOf(UNS_UnnoticedMReceiptLine_ID));
	}

	/** Get UNS_UnnoticedMReceiptLine_ID.
		@return UNS_UnnoticedMReceiptLine_ID	  */
	public int getUNS_UnnoticedMReceiptLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UNS_UnnoticedMReceiptLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UNS_UnnoticedMReceiptLine_UU.
		@param UNS_UnnoticedMReceiptLine_UU UNS_UnnoticedMReceiptLine_UU	  */
	public void setUNS_UnnoticedMReceiptLine_UU (String UNS_UnnoticedMReceiptLine_UU)
	{
		set_ValueNoCheck (COLUMNNAME_UNS_UnnoticedMReceiptLine_UU, UNS_UnnoticedMReceiptLine_UU);
	}

	/** Get UNS_UnnoticedMReceiptLine_UU.
		@return UNS_UnnoticedMReceiptLine_UU	  */
	public String getUNS_UnnoticedMReceiptLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_UNS_UnnoticedMReceiptLine_UU);
	}
}