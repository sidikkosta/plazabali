package com.unicore.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

import com.unicore.model.MUNSUnnoticedMReceipt;


public class CalloutNoticedReceipt implements IColumnCallout {

	public CalloutNoticedReceipt() {
		// TODO Auto-generated constructor stub
		}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		// TODO Auto-generated method stub
		switch (mField.getColumnName()) {
		case MUNSUnnoticedMReceipt.COLUMNNAME_C_BPartner_ID :
			return bPartnerName(ctx, WindowNo, mTab, mField, value, oldValue);
		case MUNSUnnoticedMReceipt.COLUMNNAME_C_BPartner_Location_ID :
			return bPartnerLocation(ctx, WindowNo, mTab, mField, value, oldValue);
		default :
		return null;
		}
	}
	public String bPartnerName(Properties ctx, int WindowNo, GridTab mTab, 
			GridField mField, Object value, Object oldValue){
		if(null == value)
		{
			
			mTab.setValue(MUNSUnnoticedMReceipt.COLUMNNAME_Name, null);
			return null;
		}	
			String SQL = "SELECT Name From C_BPartner where "
					+ " C_BPartner_ID=? AND isActive='Y' ";
			String name = DB.getSQLValueString(null, SQL, value);
			mTab.setValue(MUNSUnnoticedMReceipt.COLUMNNAME_Name, name);
			
			return null;
		
			}
	public String bPartnerLocation(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue){
		if(null == value)
		{
			mTab.setValue(MUNSUnnoticedMReceipt.COLUMNNAME_Location, null);
			
			return null;
			
		}	
			String SQL = "SELECT Name From C_BPartner_Location where "
					+ " C_BPartner_Location_ID=? AND isActive='Y'";
			String Location = DB.getSQLValueString(null, SQL,value);
			mTab.setValue(MUNSUnnoticedMReceipt.COLUMNNAME_Location, Location);
			
			return null;
		
	}
}

