package com.unicore.model.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

import com.unicore.model.MUNSUnnoticedMReceipt;
import com.unicore.model.MUNSUnnoticedMReceiptLine;

public class CalloutNoticeReceiptLine implements IColumnCallout {

	public CalloutNoticeReceiptLine() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
		// TODO Auto-generated method stub
		switch (mField.getColumnName()) {
		case MUNSUnnoticedMReceiptLine.COLUMNNAME_M_Product_ID :
			return nameProduct(ctx, WindowNo, mTab, mField, value, oldValue);
		case MUNSUnnoticedMReceiptLine.COLUMNNAME_C_UOM_ID :
			return setUom(ctx, WindowNo, mTab, mField, value, oldValue);
		default :
		return null;
		}
	}
	public String nameProduct(Properties ctx, int WindowNo, GridTab mTab, 
			GridField mField, Object value, Object oldValue){
		if(null == value)
		{
			
			mTab.setValue(MUNSUnnoticedMReceipt.COLUMNNAME_Name, null);
			return null;
		}	
			String SQL = "SELECT Name From M_Product where "
					+ " M_Product_ID=? AND isActive='Y' ";
			String product = DB.getSQLValueString(null, SQL, value);
			mTab.setValue(MUNSUnnoticedMReceiptLine.COLUMNNAME_Product, product);
			
			return null;
		
			}
	public String setUom(Properties ctx, int WindowNo, GridTab mTab, 
			GridField mField, Object value, Object oldValue){
		return null;
	}

}
