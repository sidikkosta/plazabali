/**
 * 
 */
package com.unicore.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;
import org.compiere.model.MInventoryLine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import com.unicore.model.factory.UniCoreMaterialManagementModelFactory;
import com.uns.base.model.Query;

/**
 * @author nurse
 *
 */
public class MUNSInventoryLineCost extends X_UNS_InventoryLineCost 
{

	private MInventoryLine m_parent = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 7552272157081344574L;
	
	public MUNSInventoryLineCost (MInventoryLine parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setM_InventoryLine_ID(parent.get_ID());
		m_parent = parent;
	}

	/**
	 * @param ctx
	 * @param UNS_InventoryLineCost_ID
	 * @param trxName
	 */
	public MUNSInventoryLineCost(Properties ctx, int UNS_InventoryLineCost_ID,
			String trxName) {
		super(ctx, UNS_InventoryLineCost_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MUNSInventoryLineCost(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public static MUNSInventoryLineCost[] getCosts (String trxName, int inventoryLineID, boolean onlyActiveRecord)
	{
		String where = "M_InventoryLine_ID = ? ";
		List<MUNSInventoryLineCost> list = Query.get(
				Env.getCtx(), UniCoreMaterialManagementModelFactory.EXTENSION_ID, Table_Name, 
				where, trxName).
				setParameters(inventoryLineID).
				setOnlyActiveRecords(onlyActiveRecord).list();
		MUNSInventoryLineCost[] costs = new MUNSInventoryLineCost[list.size()];
		list.toArray(costs);
		return costs;
	}
	
	public static BigDecimal getCosts (String trxName, int inventoryLine_ID, int currencyID)
	{
		String sql = "SELECT CurrentCostPrice FROM UNS_InventoryLineCost WHERE M_InventoryLine_ID = ? AND C_Currency_ID = ?";
		BigDecimal costs = DB.getSQLValueBD(trxName, sql, inventoryLine_ID, currencyID);
		return costs;
	}
	
	public static MUNSInventoryLineCost getCreate (MInventoryLine line, int C_Currency_ID)
	{
		String wc = "M_InventoryLine_ID = ? AND C_Currency_ID = ?";
		MUNSInventoryLineCost costs = Query.get(
				Env.getCtx(), UniCoreMaterialManagementModelFactory.EXTENSION_ID, Table_Name, 
				wc, line.get_TrxName()).
				setParameters(line.get_ID(), C_Currency_ID).
				setOnlyActiveRecords(true).firstOnly();
		if (costs == null)
		{
			costs = new MUNSInventoryLineCost(line);
			costs.setC_Currency_ID(C_Currency_ID);
		}
		costs.m_parent = line;
		return costs;
	}
	
	public MInventoryLine getParent ()
	{
		if (m_parent == null)
			m_parent = new MInventoryLine(getCtx(), getM_InventoryLine_ID(), get_TrxName());
		return m_parent;
	}
}
