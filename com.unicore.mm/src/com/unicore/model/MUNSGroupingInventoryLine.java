package com.unicore.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MInventory;
import org.compiere.util.DB;
import org.compiere.util.Util;

public class MUNSGroupingInventoryLine extends X_UNS_GroupingInventory_Line {

	/**
	 * @author Hamba Allah 
	 */
	private static final long serialVersionUID = -5115420589198527784L;
	
	private MUNSGroupingInventory m_parent = null;
	private boolean processGenerate = false;

	public MUNSGroupingInventoryLine(Properties ctx,
			int UNS_GroupingInventory_Line_ID, String trxName) {
		super(ctx, UNS_GroupingInventory_Line_ID, trxName);
		
	}
	
	public MUNSGroupingInventoryLine(Properties ctx,
			ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		
	}
	
	public void setClientOrg(int AD_Client_ID, int AD_Org_ID)
	{
		setAD_Client_ID(AD_Client_ID);
		setAD_Org_ID(AD_Org_ID);
	}
	
	public MUNSGroupingInventory getParent()
	{
		if(m_parent != null)
			return m_parent;
		
		m_parent = new MUNSGroupingInventory(getCtx(), getUNS_GroupingInventory_ID(), get_TrxName());
		
		return m_parent;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if(newRecord && getLine() <= 0)
		{
			String sql = "SELECT COALESCE(MAX(Line),0)+10 FROM UNS_GroupingInventory_Line"
					+ " WHERE UNS_GroupingInventory_ID= ?";
			int line = DB.getSQLValue(get_TrxName(), sql, getUNS_GroupingInventory_ID());
			setLine(line);
		}
		
		if(getParent().isProcessed() && !processGenerate)
		{
			log.saveError("Error Save", "Document header has been processed");
			return false;
		}
		
		String sql = "SELECT DocStatus FROM M_Inventory WHERE M_Inventory_ID = ?";
		String invDocStatus = DB.getSQLValueString(get_TrxName(), sql, getM_Inventory_ID());
		if(invDocStatus.equals(MInventory.DOCSTATUS_Reversed)
				|| invDocStatus.equals(MInventory.DOCSTATUS_Voided))
		{
			log.saveError("Error Save", "Document Inventory was voided. Cannot grouping it.");
			return false;
		}
		
		String duplicateNo = checkDuplicated();
		if(duplicateNo != null)
		{
			log.saveError("Duplicated Inventory", "There was exists invetory on Grouping Inventory No :"+duplicateNo);
			return false;
		}
		
		return true;
	}
	
	public String checkDuplicated()
	{
		
		String sql = "SELECT DocumentNo FROM UNS_GroupingInventory gi"
				+ " INNER JOIN UNS_GroupingInventory_Line gil ON gil.UNS_GroupingInventory_ID = gi.UNS_GroupingInventory_ID"
				+ " WHERE gil.M_Inventory_ID = ? AND gil.isActive = 'Y' AND gi.DocStatus NOT IN ('RE','VO')"
				+ " AND gil.UNS_GroupingInventory_Line_ID <> ?";
		
		String docNo = DB.getSQLValueString(get_TrxName(), sql, getM_Inventory_ID(), getUNS_GroupingInventory_Line_ID());
		if(!Util.isEmpty(docNo, true))
			return docNo;
		
		return null;
	}
	
	public void setProcessGenerate(boolean processGenerate)
	{
		this.processGenerate = processGenerate;
	}

}
