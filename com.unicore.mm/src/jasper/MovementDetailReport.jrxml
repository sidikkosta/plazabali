<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MovementDetail" language="groovy" pageWidth="1684" pageHeight="1296" orientation="Landscape" columnWidth="1644" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="c85da36d-1d38-4ab5-9376-569340508033">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="1353"/>
	<property name="ireport.y" value="0"/>
	<parameter name="StartDate" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="EndDate" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="WarehouseFrom_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="WarehouseTo_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT * FROM (SELECT m.DocumentNo AS DocumentNo,
COALESCE (m.POReference,'-') AS refno,
COALESCE (rq.ReferenceNo,'-') AS refrq,
m.MovementDate AS datemove,
wh.value as locfr,
whto.value as locto,
wh.NAME as locfrNAME,
whto.NAME as loctoNAME,
pd.sku as sku,
pd.articleno as articleno,
hs.code as hscode,
bp.value as supp,
pd.Name as productdesc,
asi.description as batchno,
COALESCE(UOM.UOMSymbol, '-') AS unit,
rq.DocumentNo AS reqno,
rq.DateDoc AS datereq,
rl.qty AS qtyReq,
mi.movementqty as qtymove,
COALESCE (mima.movementqty,0) as qtysentasi,
now(),
asi.created as dateasi,
m.bccode AS bccode,
mc.ReceiptDate AS Datereceipt,
COALESCE (CASE WHEN mc.DocStatus IN ('CO','CL') THEN mima.movementqty ELSE 0 END,0) AS qtyConfirm,
'L' AS Type,
COALESCE((SELECT pp.PriceStd FROM M_ProductPrice pp INNER JOIN M_PriceList_Version plv ON plv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID AND plv.ValidFrom <= $P{EndDate} INNER JOIN M_PriceList pl ON pl.M_PriceList_ID = plv.M_PriceList_ID AND pl.IsSOPriceList = 'N' AND pl.C_Currency_ID = bpp.C_Currency_ID WHERE pp.M_Product_ID = mi.M_Product_ID ORDER BY plv.ValidFROM DESC LIMIT 1),0) AS Price

FROM M_Movement m

LEFT JOIN M_MovementLine mi ON mi.M_Movement_ID=m.M_Movement_ID
INNER JOIN M_Warehouse wh ON wh.M_Warehouse_ID=m.M_Warehouse_ID
INNER JOIN M_Warehouse whto ON whto.M_Warehouse_ID=m.DestinationWarehouse_ID
LEFT OUTER JOIN M_Product pd ON pd.M_Product_ID=mi.M_product_ID
LEFT OUTER JOIN C_UOM UOM ON UOM.C_UOM_ID=pd.C_UOM_ID
LEFT OUTER JOIN M_Product_PO bpp ON bpp.M_Product_ID=pd.M_Product_ID AND isCurrentVendor='Y'
LEFT OUTER JOIN C_BPartner bp ON bp.C_BPartner_ID=bpp.C_BPartner_ID
LEFT OUTER JOIN UNS_HandleRequests hr ON hr.M_MovementLine_ID=mi.M_MovementLine_ID
LEFT OUTER JOIN M_Requisitionline rl ON rl.M_Requisitionline_ID=hr.M_Requisitionline_ID
LEFT OUTER JOIN M_Requisition rq ON rq.M_Requisition_ID=rl.M_Requisition_ID
LEFT JOIN  M_MovementLineMA mima ON mima.M_MovementLine_ID=mi.M_MovementLine_ID
LEFT OUTER JOIN M_Attributesetinstance asi ON asi.M_AttributesetInstance_ID=mima.M_AttributesetInstance_ID
LEFT OUTER JOIN UNS_HSCode hs ON hs.UNS_HSCode_ID=pd.UNS_HSCode_ID
LEFT JOIN M_MovementLineConfirm mlc ON mlc.M_MovementLine_ID=mi.M_MovementLine_ID
LEFT JOIN M_MovementConfirm mc ON mc.M_MovementConfirm_ID=mlc.M_MovementConfirm_ID AND mc.Docstatus IN ('CO', 'CL')

WHERE m.DocStatus IN ('CO','CL')  AND m.MovementDate Between $P{StartDate} AND $P{EndDate}
AND (CASE WHEN $P{WarehouseFrom_ID} IS NOT NULL THEN wh.M_Warehouse_ID=$P{WarehouseFrom_ID} ELSE 1=1 END)
AND (CASE WHEN $P{WarehouseTo_ID} IS NOT NULL THEN whto.M_Warehouse_ID=$P{WarehouseTo_ID} ELSE 1=1 END)

UNION ALL

SELECT m.DocumentNo AS DocumentNo,
COALESCE (m.POReference,'-') AS refno,
'-' AS refrq,
m.MovementDate AS datemove,
wh.value as locfr,
'-' as locto,
wh.NAME as locfrNAME,
'-' as loctoNAME,
pd.sku as sku,
pd.articleno as articleno,
hs.code as hscode,
bp.value as supp,
pd.Name as productdesc,
asi.description as batchno,
COALESCE(UOM.UOMSymbol, '-') AS unit,
rq.DocumentNo AS reqno,
rq.DateDoc AS datereq,
rl.qty AS qtyReq,
mi.movementqty as qtymove,
COALESCE(mima.movementqty,0) as qtysentasi,
now(),
asi.created as dateasi,
m.bccode AS bccode,
m.MovementDate AS Datereceipt,
COALESCE(mima.movementqty,0) AS qtyConfirm,
'RS' AS Type,
COALESCE((SELECT pp.PriceStd FROM M_ProductPrice pp INNER JOIN M_PriceList_Version plv ON plv.M_PriceList_Version_ID = pp.M_PriceList_Version_ID AND plv.ValidFrom <= $P{EndDate} INNER JOIN M_PriceList pl ON pl.M_PriceList_ID = plv.M_PriceList_ID AND pl.IsSOPriceList = 'N' AND pl.C_Currency_ID = bpp.C_Currency_ID WHERE pp.M_Product_ID = mi.M_Product_ID ORDER BY plv.ValidFROM DESC LIMIT 1),0) AS Price

FROM M_InOut m

LEFT JOIN M_InOutLine mi ON mi.M_InOut_ID=m.M_InOut_ID
INNER JOIN M_Warehouse wh ON wh.M_Warehouse_ID=m.M_Warehouse_ID
LEFT OUTER JOIN M_Product pd ON pd.M_Product_ID=mi.M_product_ID
LEFT OUTER JOIN C_UOM UOM ON UOM.C_UOM_ID=pd.C_UOM_ID
LEFT OUTER JOIN M_Product_PO bpp ON bpp.M_Product_ID=pd.M_Product_ID AND isCurrentVendor='Y'
LEFT OUTER JOIN C_BPartner bp ON bp.C_BPartner_ID=bpp.C_BPartner_ID
LEFT OUTER JOIN M_RMAline rl ON rl.M_RMAline_ID=mi.M_RMAline_ID
LEFT OUTER JOIN M_RMA rq ON rq.M_RMA_ID=rl.M_RMA_ID
LEFT JOIN  M_InOutLineMA mima ON mima.M_InOutLine_ID=mi.M_InOutLine_ID
LEFT OUTER JOIN M_Attributesetinstance asi ON asi.M_AttributesetInstance_ID=mima.M_AttributesetInstance_ID
LEFT OUTER JOIN UNS_HSCode hs ON hs.UNS_HSCode_ID=pd.UNS_HSCode_ID

WHERE m.C_DocType_ID=1000491 AND m.DocStatus IN ('CO','CL') AND m.MovementDate Between $P{StartDate} AND $P{EndDate}
AND (CASE WHEN $P{WarehouseFrom_ID} IS NOT NULL THEN wh.M_Warehouse_ID=$P{WarehouseFrom_ID} ELSE 1=1 END)) AS mv
ORDER BY DateMove,DocumentNo,sku]]>
	</queryString>
	<field name="documentno" class="java.lang.String"/>
	<field name="refno" class="java.lang.String"/>
	<field name="refrq" class="java.lang.String"/>
	<field name="datemove" class="java.sql.Timestamp"/>
	<field name="locfr" class="java.lang.String"/>
	<field name="locto" class="java.lang.String"/>
	<field name="locfrname" class="java.lang.String"/>
	<field name="loctoname" class="java.lang.String"/>
	<field name="sku" class="java.lang.String"/>
	<field name="articleno" class="java.lang.String"/>
	<field name="hscode" class="java.lang.String"/>
	<field name="supp" class="java.lang.String"/>
	<field name="productdesc" class="java.lang.String"/>
	<field name="batchno" class="java.lang.String"/>
	<field name="unit" class="java.lang.String"/>
	<field name="reqno" class="java.lang.String"/>
	<field name="datereq" class="java.sql.Timestamp"/>
	<field name="qtyreq" class="java.math.BigDecimal"/>
	<field name="qtymove" class="java.math.BigDecimal"/>
	<field name="qtysentasi" class="java.math.BigDecimal"/>
	<field name="now" class="java.sql.Timestamp"/>
	<field name="dateasi" class="java.sql.Timestamp"/>
	<field name="bccode" class="java.lang.String"/>
	<field name="datereceipt" class="java.sql.Timestamp"/>
	<field name="qtyconfirm" class="java.math.BigDecimal"/>
	<field name="type" class="java.lang.String"/>
	<field name="price" class="java.math.BigDecimal"/>
	<variable name="No" class="java.lang.Number" resetType="Column" incrementType="Column">
		<variableExpression><![CDATA[$V{No} == null ? 1 : $V{No}+1]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="qtyreq_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qtyreq}]]></variableExpression>
	</variable>
	<variable name="qtymove_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qtymove}]]></variableExpression>
	</variable>
	<variable name="qtysentasi_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qtysentasi}]]></variableExpression>
	</variable>
	<variable name="qtyconfirm_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qtyconfirm}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="80" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="310" height="20" uuid="f7e55cbf-9ecc-4f4f-9624-180551c06cca"/>
				<textElement>
					<font size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[MOVEMENT DETAIL REPORT]]></text>
			</staticText>
			<textField>
				<reportElement x="1545" y="0" width="99" height="20" uuid="c2918791-ad2a-469b-827a-a5a33df5237e"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy h.mm a">
				<reportElement x="1485" y="20" width="159" height="20" uuid="e7fe0987-399d-40f3-81e1-4f501bf4f00e"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Printed "+new SimpleDateFormat("dd MMM yyyy hh:mm a").format($F{now})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="80" y="20" width="230" height="20" uuid="a1b206c7-eab6-4ae5-9309-912c56f5f247"/>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+
new SimpleDateFormat("dd/MM/yyyy").format($P{StartDate})+" to "+
new SimpleDateFormat("dd/MM/yyyy").format($P{EndDate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="80" height="20" uuid="57c30ab5-a9db-4052-ae87-4faea9d3b356"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Period]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="80" y="40" width="135" height="20" uuid="79e9d75b-475e-4a7e-b7ef-11e81aadc1e4"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{WarehouseFrom_ID}==null ? ": All Warehouse" : $F{locfr}+"("+$F{locfrname}+")"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="80" y="60" width="135" height="20" uuid="a53455c2-4191-4cb4-801f-2618d01bf8c4"/>
				<textElement>
					<font fontName="SansSerif"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{WarehouseTo_ID}==null ? ": All Destination" : $F{locto}+"("+$F{loctoname}+")"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="40" width="80" height="20" uuid="ffe25528-3a24-4374-a8e2-d6fb288d492e"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[From]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="60" width="80" height="20" uuid="202f8dec-ea68-4ba8-9da8-75714d191428"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[To]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="80" height="30" uuid="62f54216-6edb-493a-b1d7-83edd7bbc1a3"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Movement No]]></text>
			</staticText>
			<staticText>
				<reportElement x="160" y="0" width="55" height="30" uuid="8cf61e9b-4da9-4ef5-8376-71a00b15917e"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="375" y="0" width="50" height="30" uuid="e2076330-059e-4cdf-a47c-d41a8ac7762c"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[SKU]]></text>
			</staticText>
			<staticText>
				<reportElement x="425" y="0" width="70" height="30" uuid="c6b04f68-1fd4-4282-9fbb-cf16586b0afc"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Article No]]></text>
			</staticText>
			<staticText>
				<reportElement x="845" y="0" width="50" height="30" uuid="29cdd497-bd9f-4c36-bab0-cd297a9ea0d8"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Custom No]]></text>
			</staticText>
			<staticText>
				<reportElement x="955" y="0" width="55" height="30" uuid="9fd53c7c-870c-4e8c-b38a-30c93665a2df"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="215" y="0" width="30" height="30" uuid="5f9032e8-2234-4e38-97ee-a40c9d324c52"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Type]]></text>
			</staticText>
			<staticText>
				<reportElement x="310" y="0" width="65" height="30" uuid="fd7bec1b-7060-4529-b03e-c0c4fbc2dca6"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[To]]></text>
			</staticText>
			<staticText>
				<reportElement x="245" y="0" width="65" height="30" uuid="9e6414dd-6717-406c-9b04-a0c211f10549"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[From]]></text>
			</staticText>
			<staticText>
				<reportElement x="495" y="0" width="70" height="30" uuid="5cc4a8ac-44eb-4b37-8db0-f516bed9b21d"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Tarif HS]]></text>
			</staticText>
			<staticText>
				<reportElement x="565" y="0" width="50" height="30" uuid="6dd00b9d-3cc8-40ee-84d5-ea3bb96ef913"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Suppl]]></text>
			</staticText>
			<staticText>
				<reportElement x="615" y="0" width="230" height="30" uuid="2ad8ba8f-7e7b-49ea-b6bd-6985ae85cb17"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Description]]></text>
			</staticText>
			<staticText>
				<reportElement x="895" y="0" width="60" height="30" uuid="fdf6429e-aa97-4eba-9505-5e650c0e5ffd"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Batch No]]></text>
			</staticText>
			<staticText>
				<reportElement x="1040" y="0" width="80" height="30" uuid="f502be2e-c800-4000-a53a-eec82fc2c23a"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Requesition No]]></text>
			</staticText>
			<staticText>
				<reportElement x="1200" y="0" width="55" height="30" uuid="acaec0e6-2137-4d0d-b5a7-f71bb2473367"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Request Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="1255" y="0" width="60" height="30" uuid="2b5d6e60-a02d-47e0-b613-4367b3adba04"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Qty]]></text>
			</staticText>
			<staticText>
				<reportElement x="1315" y="0" width="55" height="30" uuid="5ca173fb-11f5-4950-bc6f-4aa9a37f27f8"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Sent Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="1370" y="0" width="60" height="30" uuid="3ec90370-92cb-42d9-9ecb-5680fb54d459"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Qty]]></text>
			</staticText>
			<staticText>
				<reportElement x="1545" y="0" width="99" height="30" uuid="807110e9-96ae-436b-aa8a-f7b6d6a247cd"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Cost]]></text>
			</staticText>
			<staticText>
				<reportElement x="1485" y="0" width="60" height="30" uuid="a0732c32-a963-41eb-8cec-c12f14a5acb5"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Qty]]></text>
			</staticText>
			<staticText>
				<reportElement x="1430" y="0" width="55" height="30" uuid="0e11374e-7ce9-47d5-b7cc-87da43b798ef"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Received Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="1010" y="0" width="30" height="30" uuid="d050f0bd-8663-41ea-a009-fa05a37c0222"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Unit]]></text>
			</staticText>
			<staticText>
				<reportElement x="80" y="0" width="80" height="30" uuid="39b196bc-9feb-4507-8485-8cf6baa01a0f"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Reference Move No]]></text>
			</staticText>
			<staticText>
				<reportElement x="1120" y="0" width="80" height="30" uuid="158b37d9-928f-4861-9b94-d01444f40cad"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Reference Request No]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="19" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="80" height="19" uuid="a25202ba-3fa2-4c4e-8d5c-e062d55ab8a9"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{documentno}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="160" y="0" width="55" height="19" uuid="63b5fc05-0c5b-4b29-9fbc-0d0e93459e31"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{datemove}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="425" y="0" width="70" height="19" uuid="9c01f366-4ec9-4d06-b2bc-c652f05c4b48"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{articleno}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="245" y="0" width="65" height="19" uuid="582497d2-3d97-4d31-908b-040e1054bb35"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{locfr}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="310" y="0" width="65" height="19" uuid="8b215880-b3db-471d-97ea-e608f178487a"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{locto}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="375" y="0" width="50" height="19" uuid="945ce397-af16-4422-82a8-5a0235ba9f11"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{sku}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="495" y="0" width="70" height="19" uuid="027fb7e9-5db6-4e34-96f1-97ff08ba42aa"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{hscode}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="565" y="0" width="50" height="19" uuid="092c5614-982e-475b-93ad-fe8f741e7b59"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{supp}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="895" y="0" width="60" height="19" uuid="185d27b3-0e80-4da1-837d-f25e58e124ea"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{batchno}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1010" y="0" width="30" height="19" uuid="0df3286c-bfcb-4f67-824a-9574007e7ca0"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{unit}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1200" y="0" width="55" height="19" uuid="fa311d16-6e3b-4aee-b380-e8054e3d0e3f"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{datereq}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1315" y="0" width="55" height="19" uuid="7a7786f1-72ca-475b-b70d-cb555d9becc7"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{datemove}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1255" y="0" width="60" height="19" uuid="898b032c-eebc-4b04-abdb-42f854ceabe0"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qtyreq}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1545" y="0" width="99" height="19" uuid="f01b71f4-391a-4844-9827-4da3f01766f3"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{price}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1370" y="0" width="60" height="19" uuid="ff2d02e8-eefc-4336-8162-7d81e78cbd24"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qtysentasi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="845" y="0" width="50" height="19" uuid="f3ac999b-7113-4060-8b9c-e2d3d6735619"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{bccode}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="955" y="0" width="55" height="19" uuid="2752b272-b3f3-400c-87cf-22bb9e9077c1"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{datereq}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1485" y="0" width="60" height="19" uuid="b3eb5085-953a-4968-a30d-fe524f52cf51"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qtyconfirm}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1430" y="0" width="55" height="19" uuid="55382678-9088-454d-8ad6-e03c39eb3fbb"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{datereceipt}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="615" y="0" width="230" height="19" uuid="b0f44bb7-b6f9-4ec0-8956-e2d6ae7702c2"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{productdesc}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="215" y="0" width="30" height="19" uuid="6d303c7f-5a08-4410-a4dd-a67084e52d65"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{type}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1040" y="0" width="80" height="19" uuid="8e4bf549-92bd-438d-8b80-c10dffcb702d"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reqno} == null ? "-" : $F{reqno}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="80" y="0" width="80" height="19" uuid="9177d003-38cf-4863-b8ca-89e7121c79e7"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{refno}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1120" y="0" width="80" height="19" uuid="5bf30de0-69ba-41f1-b1cf-73e12537826a"/>
				<box leftPadding="3" rightPadding="3">
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
					<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{refrq} == null ? "-" : $F{refrq}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="20">
			<textField pattern="###0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1370" y="0" width="60" height="20" uuid="527f0d17-fc94-4d81-80a7-f83dd9f58fac"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{qtysentasi_1}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1255" y="0" width="60" height="20" uuid="82cb6426-fdfb-4fae-80ef-cb023113c752"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{qtyreq_1}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="1485" y="0" width="60" height="20" uuid="72568dbb-1bb9-40a9-926a-a0c6a8b7add0"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{qtyconfirm_1}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="0" width="1255" height="20" uuid="993dae80-c2ac-45a5-9cc5-44a68f3ffc16"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL]]></text>
			</staticText>
			<staticText>
				<reportElement x="1545" y="0" width="99" height="20" uuid="a8a15ea2-4b77-4493-8940-ccf4c8112065"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="1315" y="0" width="55" height="20" uuid="f3b49542-c75b-4920-820c-600cf198e92c"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="1430" y="0" width="55" height="20" uuid="76c2edeb-2ded-4422-91ac-57b0edf7ff69"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Double"/>
					<bottomPen lineWidth="0.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
