/**
 * 
 */
package com.unicore.validator;

import java.util.logging.Level;

import org.apache.xpath.operations.String;
import org.compiere.model.MClient;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.Ini;

import com.unicore.replication.client.DesktopReplicationClient;

/**
 * @author nurse
 *
 */
public class ReplicationValidator implements ModelValidator 
{
	private int p_clientID = 0;
	CLogger log = CLogger.getCLogger(ReplicationValidator.class);
	
	/**
	 * 
	 */
	public ReplicationValidator() 
	{
		super ();
	}

	/* (non-Javadoc)
	 * @see org.compiere.model.ModelValidator#initialize(org.compiere.model.ModelValidationEngine, org.compiere.model.MClient)
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) 
	{
		if(client != null)
		{
			p_clientID = client.getAD_Client_ID();
			log.log(Level.INFO, client.toString());
		}
		else
			log.log(Level.INFO, "Initializing global validator -" + this.toString());
		
		if (Ini.isClient())
		{
			Thread thread = new Thread(DesktopReplicationClient.getInstance());
			thread.setName("Desktop-Replication");
			thread.start();
		}
	}

	@Override
	public int getAD_Client_ID() 
	{
		return p_clientID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) 
	{
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception 
	{
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) 
	{
		return null;
	}

}
