/**
 * 
 */
package com.unicore.replication.client;

import java.util.Properties;
import java.util.logging.Level;

import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.adempiere.process.rpl.XMLHelper;
import org.adempiere.process.rpl.imp.ImportHelper;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.compiere.model.MIMPProcessor;
import org.compiere.util.CLogger;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.w3c.dom.Document;

/**
 * @author nurse
 *
 */
public class QueueMsgListener implements MessageListener 
{
	private Connection m_connection = null;
	private Session m_session = null;
	private Properties m_ctx = null;
	private String m_url = null;
	private String m_queueName = null;
	private String m_subscriptionName = null;
	private String m_user;
	private String m_password;
	private String m_clientID;
	private CLogger log = CLogger.getCLogger(getClass());
	private int m_maxRedeliveries = -1;
	private boolean m_isRunning = false;
	private MIMPProcessor m_impProcessor = null;
	
	public void setImpProcessor (MIMPProcessor processor)
	{
		m_impProcessor = processor;
	}
	
	
	/**
	 * 
	 */
	public QueueMsgListener(Properties ctx, String protocol, String host, int port, 
			String subscriptionName, String queueName, String clientID, String userName, 
			String password, String options, String trxName) 
	{
		writeLog(Level.INFO, "Starting Q Listener" + toString());
		m_ctx = ctx;
		m_user = userName;
		m_password = password;
		m_clientID = clientID;
		m_queueName = queueName;
		m_subscriptionName = subscriptionName;
		
		if (Util.isEmpty(protocol, true))
			protocol = "tcp";
		
		if (Util.isEmpty(host, true))
			host = "localhost";
		
		if (port == 0)
			port = 7777;
		
		
		StringBuilder uriBilder = new StringBuilder(protocol).append("://")
				.append(host).append(":").append(port);
		if (!Util.isEmpty(options, true)) {
			uriBilder.append("?").append(options);
		}
		
		m_url = uriBilder.toString();
	}
	
	public void start () throws JMSException
	{
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(m_url);
		factory.getRedeliveryPolicy().setMaximumRedeliveries(m_maxRedeliveries);
		
		if (!Util.isEmpty(m_user, true) && !Util.isEmpty(m_password, true))
			m_connection = factory.createConnection(m_user, m_password);
		else
			m_connection = factory.createConnection();
		
		writeLog(Level.INFO, "Connection of ActiveMQ succesfully created");
		m_connection.setExceptionListener(new ExceptionListener() 
		{	
			@Override
			public void onException(JMSException exception) 
			{
				try 
				{
					stop();
				}
				catch (JMSException e) 
				{
					e.printStackTrace();
					m_isRunning = false;
					return;
				}
			}
		});
		if (m_connection.getClientID() == null) 
		{
			try 
			{
				m_connection.setClientID(m_clientID);
			}
			catch (Exception ex) 
			{
				m_connection.close();
				return;
			}
		} 
		else 
		{
			if (m_connection.getClientID().equals(m_clientID)) 
			{
				m_connection.close();
				return;
			} 
			else 
			{
				try 
				{
					m_connection.setClientID(m_clientID);	
				} 
				catch (Exception ex) 
				{
					m_connection.close();
					return;
				}
			}
		}
		
		m_session = m_connection.createSession(true, Session.SESSION_TRANSACTED);
		Queue queue = m_session.createQueue(m_queueName);
		MessageConsumer cunsomer = m_session.createConsumer(queue);
		cunsomer.setMessageListener(this);
		
		m_connection.start();		
		m_isRunning = true;
	}
	

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	@Override
	public void onMessage(Message message) 
	{
		if (message instanceof TextMessage) 
		{
			Trx localTrx = Trx.get(Trx.createTrxName("RPL"), true);
			String localTrxName = localTrx.getTrxName();
			try 
			{
				TextMessage txtMessage = (TextMessage) message;
				String text = txtMessage.getText();
				Document documentToBeImported = XMLHelper.createDocumentFromString(text);
				StringBuffer result = new StringBuffer();
				ImportHelper impHelper = new ImportHelper(m_ctx);
				impHelper.setImpProcessor(m_impProcessor);
				impHelper.importXMLDocument(result, documentToBeImported, localTrxName);	
				m_session.commit();
				localTrx.commit();				
			} 
			catch (JMSException e1)
			{
				try {
					m_session.rollback();
					stop();
				}
				catch (JMSException e2) 
				{
					e2.printStackTrace();
					m_isRunning = false;
				}
				
				localTrx.rollback();
				e1.printStackTrace();
			}
			catch (Exception e) 
			{
				try {
					m_session.rollback();
					stop();
				}
				catch (JMSException e2) 
				{
					e2.printStackTrace();
					m_isRunning = false;
				}
				
				localTrx.rollback();
				e.printStackTrace();
			}
			finally
			{
				localTrx.close();
				localTrxName =null;
				localTrx = null;
				
			}
		}
	}

	private void writeLog (Level level, String msg) 
	{
		if (!log.isLoggable(level)) 
		{
			return;
		}
		
		log.log(level, msg);
	}
	
	@Override
	public String toString () 
	{
		StringBuilder sb = new StringBuilder("url : ").append(m_url)
				.append(" | queue name : ").append(m_queueName).append(" | ")
				.append("client ID : ").append(m_clientID).append("| SubscriptionName :")
				.append(m_subscriptionName);	
		String retVal = sb.toString();
		return retVal;
	}
	
	public boolean isRunning ()
	{
		return m_isRunning;
	}
	
	public void stop() throws JMSException 
	{
		writeLog(Level.INFO, "Stop Queue Listener " + toString());
		if (null != m_session) 
		{
			m_session.close();
		}
		if (null != m_connection) 
		{
			m_connection.close();
		}
		m_isRunning = false;
	}
	
	public void setMaxRedeliveries (int maxRedeliveries)
	{
		this.m_maxRedeliveries = maxRedeliveries;
	}
}
