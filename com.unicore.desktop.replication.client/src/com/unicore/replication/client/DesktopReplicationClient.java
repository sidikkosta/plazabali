/**
 * 
 */
package com.unicore.replication.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import javax.jms.JMSException;
import org.compiere.model.MIMPProcessor;
import org.compiere.model.X_IMP_ProcessorParameter;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Util;

/**
 * @author nurse
 *
 */
public class DesktopReplicationClient implements Runnable
{

	private static final DesktopReplicationClient m_client = new DesktopReplicationClient();
	private static final Properties m_ctx = new Properties(Env.getCtx());
	private List<QueueMsgListener> m_listeners = new ArrayList<>();
	private CLogger log = CLogger.getCLogger(getClass());
	private boolean m_stop = false;
	private boolean m_reinit = false;
	private MIMPProcessor[] m_processor = null;
	
	public static DesktopReplicationClient getInstance ()
	{
		return m_client;
	}
	
	public void reinit ()
	{
		m_reinit = true;
	}
	
	private void manage () throws JMSException
	{
		while (!m_stop)
		{
			if (m_reinit)
			{
				for (int i=0; i<m_listeners.size(); i++)
				{
					m_listeners.get(i).stop();
					m_listeners.remove(i);
					--i;
				}
				for (int i=0; i<m_processor.length; i++)
				{
					try
					{
//						m_ctx.setProperty("AD_Client_ID", "1000015");
						registerListener(m_processor[i]);
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
				}
				m_reinit = false;
			}
			for (int i=0; i<m_listeners.size(); i++)
			{
				if (!m_listeners.get(i).isRunning())
				{
					try
					{
						m_listeners.get(i).start();
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
			try 
			{
				Thread.sleep(3000);
			}
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			if (m_listeners.size() == 0)
				m_reinit = true;
		}
	}

	@Override
	public void run() 
	{
		m_processor = MIMPProcessor.getActive(m_ctx);
		for (int i=0; i<m_processor.length; i++)
		{
			try
			{
				m_ctx.setProperty("AD_Client_ID", Integer.toString(m_processor[i].getAD_Client_ID()));
				registerListener(m_processor[i]);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		try 
		{
			manage();
		}
		catch (JMSException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void registerListener (MIMPProcessor processor) throws Exception
	{
		X_IMP_ProcessorParameter[] params = processor.getIMP_ProcessorParameters(
				processor.get_TrxName());
		
		String host = processor.getHost();
		int port = processor.getPort();
		String account = processor.getAccount();
		String password = processor.getPasswordInfo();
		
		// mandatory parameters!
		String qName = "ActiveMQ.DLQ";
		String protocol = "tcp";
		String subsName = "Undefined";
		String options = null;
		String clientID = "Undefined";
		int maxRedeliveries = -1;
		
		for (int i = 0; i < params.length; i++) 
		{
			writeLog(Level.INFO, "ProcesParameter          Value = " + params[i].getValue());
			writeLog(Level.INFO, "ProcesParameter ParameterValue = " + params[i].getParameterValue());//
    		if (params[i].getValue().equals("queueName"))
    			qName = params[i].getParameterValue();
    		else if (params[i].getValue().equals("protocol"))
    			protocol = params[i].getParameterValue();
    		else if (params[i].getValue().equals("subscriptionName"))
    			subsName = params[i].getParameterValue();
    		else if (params[i].getValue().equals("clientID"))
    			clientID = params[i].getParameterValue();
    		else if (params[i].getValue().equals("MaxRedeliveries"))
    			maxRedeliveries = new Integer(params[i].getParameterValue());        			
    		else
    			log.log(Level.SEVERE, "Unknown parameter " + params[i].getValue());
    	}
		
		writeLog(Level.INFO, "Connection Info " + protocol + "://" + host + ":" + port);
		writeLog(Level.INFO, "Queue Name : " + qName);
		writeLog(Level.INFO, "Subscription Name " + subsName);
		writeLog(Level.INFO, "Client ID : " + clientID);
		
		if (Util.isEmpty(qName, true))
			throw new Exception("Missing "+X_IMP_ProcessorParameter.Table_Name+" with key 'topicName'!");
		if (protocol == null || protocol.length() == 0)
			throw new Exception("Missing "+X_IMP_ProcessorParameter.Table_Name+" with key 'protocol'!");
		if (clientID == null || clientID.length() == 0)
			throw new Exception("Missing "+X_IMP_ProcessorParameter.Table_Name+" with key 'clientID'!");
		QueueMsgListener listener = new QueueMsgListener(
				m_ctx, protocol, host, port, subsName, qName, clientID, 
				account, password, options, null);	
		listener.setImpProcessor(processor);
		listener.setMaxRedeliveries(maxRedeliveries);
		writeLog(Level.INFO, "Starting Queue Listener... ");
	    listener.start();
	    m_listeners.add(listener);
	}
	
	private void writeLog (Level level, String msg)
	{
		if (!log.isLoggable(level))
			return;
		log.log(level, msg);
	}
}
